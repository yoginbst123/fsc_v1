@extends('front-section.app')
@section('main-content')
<style>
.tess{
text-align: left;
float: right;
font-size: 11px;margin-right: 20px;}
.star-required1{color: #f5f5f5;}
.fsc-reg-sub-header {
    padding-left: 0;}
.fsc-reg-sub-header-div{border-radius: 0;padding: 6px 4%;border-top: 2px solid #fff;border-bottom: 2px solid #fff;}

</style>
<div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 fsc-content-head">
			<h4>{{Request::segment(4)}} Registration</h4>
		</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:20px ">
@foreach($business as $bus)
@if(Request::segment(3)==$bus->id)
@if(Request::segment(5))
@if(Request::segment(7))
<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="text-align: center;">

@else
<div class="col-lg-5 col-md-5 col-sm-6 col-xs-12" style="text-align: RIGHT;">

@endif
@else
@if(Request::segment(7))
@else
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center;">
@endif
@endif
@if(Request::segment(4)=='Business')
<img src="http://financialservicecenter.net/public/frontcss/images/business.png" class="img-responsive">
@endif

@if(Request::segment(4)=='Personal')
<img src="{{url('public/business/',$bus->bussiness_image_name)}}" class="img-responsive">
@endif

@if(Request::segment(4)=='Non-Profit Organization')
<img src="{{url('public/business/',$bus->bussiness_image_name)}}" class="img-responsive">
@endif
@if(Request::segment(4)=='Service Industry')
<img src="http://financialservicecenter.net/public/frontcss/images/service.png" class="img-responsive">
@endif
@if(Request::segment(4)=='Profession')
<img src="http://www.ussdevelopment.com/financialservicecenter/assets/images/register/1496291354Profession.png"  class="img-responsive">
@endif
@if(Request::segment(4)=='Investor')
<img src="http://www.ussdevelopment.com/financialservicecenter/assets/images/register/1496291329Investor.png" class="img-responsive">
@endif

</div>
@endif
@endforeach
@foreach($category as $cate)
@if(Request::segment(5)==$cate->id)
            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12" style="font-size: 3em; text-align: center;padding-top:10px">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            </div>
@if(Request::segment(7))
      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="text-align: left;">
      @else
      <div class="col-lg-4 col-md-4 col-sm-3 col-xs-12" style="text-align: left;">
@endif
                <img src="{{url('public/category/',$cate->business_cat_image)}}" width="100%">
            </div>

@endif
@endforeach

         
@foreach($categorybusiness as $cate1)
@if(Request::segment(7)==$cate1->id)
            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12" style="font-size: 3em; text-align: center;padding-top:10px">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="text-align: left;">
                <img src="{{url('public/businessbrandcategory/',$cate1->business_brand_category_image)}}" width="100%">
            </div>

@endif
@endforeach
            </div>
	</div>	
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-section-head">
			<h4>Registration</h4>
		</div>
	</div>	
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">	 
		<form id="registrationForm" action="{{route('comman-registration.store')}}" name="registrationForm" method="post">
		{{csrf_field()}}					
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-content-box">			
			<div class="alert alert-success" role="alert" id="success_message">Success <i class="glyphicon glyphicon-thumbs-up"></i> Thanks for contacting us, we will get back to you shortly.</div>				
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">				
				    <input type="hidden" id="business_id" name="business_id" value="{{Request::segment(3)}}" />
					<input type="hidden" id="business_cat_id" name="business_cat_id" value="{{Request::segment(5)}}" />
					<input type="hidden" id="business_brand_id" name="business_brand_id" value="{{Request::segment(6)}}" />
					<input type="hidden" id="business_brand_category_id" name="business_brand_category_id" value="{{Request::segment(7)}}" />
					<input type="hidden" id="user_type" name="user_type" value="{{Request::segment(4)}}" />
				<div class="form-group{{ $errors->has('company_name') ? ' has-error' : '' }}">
					<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
						<label class="fsc-form-label">Company Name : <span class="star-required">*</span>
<span class="tess">(Legal Name)</span></label>
					</div>
					<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
						<input type="text" class="form-control fsc-input" id="company_name" name="company_name" placeholder="Company Name">
						@if ($errors->has('company_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('company_name') }}</strong>
                                    </span>
                                @endif
					</div>
				</div>	
				</div>		
				<div class="form-group{{ $errors->has('business_name') ? ' has-error' : '' }}">	
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
					<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
						<label class="fsc-form-label">Business Name : <span class="star-required">*</span><span class="tess">(DBA Name)</span></label>
					</div>
					<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
						<input type="text" class="form-control fsc-input" id="business_name" name="business_name" placeholder="Business Name">
						@if ($errors->has('business_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('business_name') }}</strong>
                                    </span>
                                @endif
					</div>
				</div>
				</div>
				

				
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-reg-sub-header-div" style="background-color: #d1d1d1;">
					<h3 class="Libre fsc-reg-sub-header">Business Location :</h3>
				</div>
				
				
				
				<div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
					<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
						<label class="fsc-form-label">Business Address 1 : <span class="star-required">*</span></label>
					</div>
					<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
						<input type="text" class="form-control fsc-input" id="address1" placeholder="Address" name="address" placeholder="">
						@if ($errors->has('address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
					</div>
				</div></div>
				
				<div class="form-group{{ $errors->has('address1') ? ' has-error' : '' }}">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
					<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
						<label class="fsc-form-label">Business Address 2 : <span class="star-required1">*</span></label>
					</div>
					<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
						<input type="text" placeholder="Address" class="form-control fsc-input" id="address2" name="address1" placeholder="">
						@if ($errors->has('address1'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address1') }}</strong>
                                    </span>
                                @endif
					</div>
				</div>
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
					<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
						<label class="fsc-form-label">City / State / Zip : <span class="star-required">*</span></label>
					</div>
					<div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}" style="margin-top:10px">
					<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
						<input type="text" class="form-control fsc-input" id="city" name="city" placeholder="City">
						@if ($errors->has('city'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                @endif
					</div>
					</div>
					<div class="form-group{{ $errors->has('stateId') ? ' has-error' : '' }}">
					<div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
						<div class="dropdown" style="margin-top: 1%;">
							<select name="stateId" id="stateId" class="form-control fsc-input">
								<option value="">State</option>
								<option value='AK'>AK</option>
								<option value='AS'>AS</option>
								<option value='AZ'>AZ</option>
								<option value='AR'>AR</option>
								<option value='CA'>CA</option>
								<option value='CO'>CO</option>
								<option value='CT'>CT</option>
								<option value='DE'>DE</option>
								<option value='DC'>DC</option>
								<option value='FM'>FM</option>
								<option value='FL'>FL</option>
								<option value='GA'>GA</option>
								<option value='GU'>GU</option>
								<option value='HI'>HI</option>
								<option value='ID'>ID</option>
								<option value='IL'>IL</option>
								<option value='IN'>IN</option>
								<option value='IA'>IA</option>
								<option value='KS'>KS</option>
								<option value='KY'>KY</option>
								<option value='LA'>LA</option>
								<option value='ME'>ME</option>
								<option value='MH'>MH</option>
								<option value='MD'>MD</option>
								<option value='MA'>MA</option>
								<option value='MI'>MI</option>
								<option value='MN'>MN</option>
								<option value='MS'>MS</option>
								<option value='MO'>MO</option>
								<option value='MT'>MT</option>
								<option value='NE'>NE</option>
								<option value='NV'>NV</option>
								<option value='NH'>NH</option>
								<option value='NJ'>NJ</option>
								<option value='NM'>NM</option>
								<option value='NY'>NY</option>
								<option value='NC'>NC</option>
								<option value='ND'>ND</option>
								<option value='MP'>MP</option>
								<option value='OH'>OH</option>
								<option value='OK'>OK</option>
								<option value='OR'>OR</option>
								<option value='PW'>PW</option>
								<option value='PA'>PA</option>
								<option value='PR'>PR</option>
								<option value='RI'>RI</option>
								<option value='SC'>SC</option>
								<option value='SD'>SD</option>
								<option value='TN'>TN</option>
								<option value='TX'>TX</option>
								<option value='UT'>UT</option>
								<option value='VT'>VT</option>
								<option value='VI'>VI</option>
								<option value='VA'>VA</option>
								<option value='WA'>WA</option>
								<option value='WV'>WV</option>
								<option value='WI'>WI</option>
								<option value='WY'>WY</option>
							</select>
							@if ($errors->has('stateId'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('stateId') }}</strong>
                                    </span>
                                @endif
							</div>
						</div>
					</div>
					<div class="form-group{{ $errors->has('zip') ? ' has-error' : '' }}">
					<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
						<input type="text" class="form-control fsc-input" id="zip" maxlength="5" name="zip" placeholder="Zip">
						@if ($errors->has('zip'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('zip') }}</strong>
                                    </span>
                                @endif
					</div>
				</div>
				</div>
				<div class="form-group{{ $errors->has('countryId') ? ' has-error' : '' }}">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
				
					<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
						<label class="fsc-form-label">Country : <span class="star-required">*</span></label>
					</div>
					<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
						<div class="dropdown">
							<select name="countryId" id="countryId" class="form-control fsc-input">
								<option value="">Country</option>
								<option value='USA'>USA</option>
							</select>
							@if ($errors->has('countryId'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('countryId') }}</strong>
                                    </span>
                                @endif
						</div>
					</div>
				</div>
				</div>
			
				<div class="form-group{{ $errors->has('business_no') ? ' has-error' : '' }}">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
					<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
						<label class="fsc-form-label">Business Tel : <span class="star-required">*</span></label>
					</div>
					<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
						<input type="tel" class="form-control fsc-input bfh-phone" data-format=" (ddd) ddd-dddd" id="business_no" name="business_no"  placeholder="(845)555-1212">
				
					</div>
				</div>
				</div>
				<div class="form-group{{ $errors->has('business_fax') ? ' has-error' : '' }}">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
					<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
						<label class="fsc-form-label">Business Fax : <span class="star-required1">*</span></label>
					</div>
					<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
						<input type="text" class="form-control fsc-input bfh-phone" data-format="  (ddd) ddd-dddd" id="business_fax" name="business_fax" placeholder="Business Fax">
						@if ($errors->has('business_fax'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('business_fax') }}</strong>
                                    </span>
                                @endif
					</div>
				</div>
				</div>

				<div class="form-group{{ $errors->has('website') ? ' has-error' : '' }}">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
					<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
						<label class="fsc-form-label">Web Address : <span class="star-required1">*</span></label>
					</div>
					<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
						<input type="text" class="form-control fsc-input" id="website" name="website" placeholder="Website address">
						@if ($errors->has('website'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('website') }}</strong>
                                    </span>
                                @endif
					</div>
				</div>
				</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-reg-sub-header-div" style="background-color: #d1d1d1;">
					<h3 class="Libre fsc-reg-sub-header">Contact Person Information:</h3>
				</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:1%;">
					<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
						<label class="fsc-form-label">Name : <span class="star-required">*</span></label>
					</div>
					<div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">	
					<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin" >
						<input type="text" class="form-control  fsc-input" id="first_name1" name="first_name" placeholder="First Name">
						@if ($errors->has('first_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
					</div>
					</div>
					<div class="form-group{{ $errors->has('middle_name') ? ' has-error' : '' }}">
					<div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
						<input type="text" class="form-control fsc-input " id="middle_name" maxlength="1" name="middle_name" placeholder="Middle">
						@if ($errors->has('middle_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('middle_name') }}</strong>
                                    </span>
                                @endif
					</div>
					</div>
					<div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
					<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
						<input type="text" class="form-control fsc-input" id="last_name1" name="last_name" placeholder="Last name">
						@if ($errors->has('last_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
					</div>
					</div>
				</div>


<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:1%;">
					<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
						<label class="fsc-form-label">Telephone 1 : <span class="star-required">*</span></label>
					</div>
					<div class="form-group{{ $errors->has('telephone1') ? ' has-error' : '' }}">	
					<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin" >
						<input type="text" class="form-control fsc-input bfh-phone" data-format="  (ddd) ddd-dddd" id="telephone1" name="telephone1" placeholder="Telephone 1">
						@if ($errors->has('telephone1'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('telephone1') }}</strong>
                                    </span>
                                @endif
					</div>
					</div>
					<div class="form-group{{ $errors->has('telephone1type') ? ' has-error' : '' }}">
					<div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
						<select  class="form-control fsc-input" id="telephone1type" name="telephone1type">
<option>Type</option>
<option value="Office">Office</option>
<option value="Mobile">Mobile</option>
<option value="Resid">Resid</option>
</select>
					</div>
					</div>
					<div class="form-group{{ $errors->has('telephone1ext') ? ' has-error' : '' }}">
					<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
						<input type="text" readonly class="form-control fsc-input" maxlength="5" id="telephone1ext" name="telephone1ext" placeholder="Ext">
						
					</div>
					</div>
				</div>



<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:1%;">
					<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
						<label class="fsc-form-label">Telephone 2 : <span class="star-required1">*</span></label>
					</div>
					<div class="form-group{{ $errors->has('telephone1') ? ' has-error' : '' }}">	
					<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin" >
						<input type="text" class="form-control fsc-input bfh-phone" data-format="  (ddd) ddd-dddd" id="telephone2" name="telephone2" placeholder="Telephone 2">
						
					</div>
					</div>
					<div class="form-group{{ $errors->has('telephone2type') ? ' has-error' : '' }}">
					<div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
						<select  class="form-control fsc-input" id="telephone2type" name="telephone2type">
<option>Type</option>
<option value="Office">Office</option>
<option value="Mobile">Mobile</option>
<option value="Resid">Resid</option>
</select>
					</div>
					</div>
					<div class="form-group{{ $errors->has('telephone2ext') ? ' has-error' : '' }}">
					<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
						<input type="text" readonly class="form-control fsc-input" maxlength="5" id="telephone2ext" name="telephone2ext" placeholder="Ext">
						
					</div>
					</div>
				</div>



				<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
					<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
						<label class="fsc-form-label">Email : <span class="star-required1">*</span> </label>
					</div>
					<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
						<input type="email" class="form-control fsc-input" id="email" name='email' placeholder="abc@abc.com">
						
					</div>
				</div>
				</div>
				
				
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:3%;">
					<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12"></div>
					<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
						<button type="submit" class="btn btn-primary btn-lg fsc-form-submit">SUBMIT</button>
					</div>
					<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
						<button type="reset" class="btn btn-default btn-lg fsc-form-submit" >RESET</button>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				
	

					</div>
				</div>
				
			</div>
		</form>
	</div>
	
</div>

<script>$("#business_no").mask("(999) 999-9999");
$(".ext").mask("999");
$("#business_fax").mask("(999) 999-9999");
$("#mobile_no").mask("(999) 999-9999");
$(".usapfax").mask("(999) 999-9999");
$("#zip").mask("9999");
</script>
<script>
$.ajaxSetup({
    headers:
    {
        'X-CSRF-Token': $('input[name="_token"]').val()
    }
});
  $(document).ready(function() {
    $('#registrationForm').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },

		fields: {
			company_name: {
                validators: {
                        stringLength: {
                        min: 1,
                    },
                        notEmpty: {
                        message: 'Please Enter Your Company Name'
                    }
                }
            },
			business_name: {
                validators: {
                        stringLength: {
                        min: 1,
                    },
                        notEmpty: {
                        message: 'Please Enter Your Business Name'
                    }
                }
			},
			legalname: {
                validators: {
                        stringLength: {
                        min: 1,
                    },
                        notEmpty: {
                        message: 'Please Enter Your Legal Name'
					},
					regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'The Legal Name can consist of alphabetical characters and spaces only'
                    }
                }
			},
			dbaname: {
                validators: {
                        stringLength: {
                        min: 1,
                    },
                        notEmpty: {
                        message: 'Please Enter Your DBA Name'
					},
					regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'The DBA Name can consist of alphabetical characters and spaces only'
                    }
                }
            },

            first_name: {
                validators: {
                        stringLength: {
                        min: 1,
                    },
                        notEmpty: {
                        message: 'Please Enter Your First Name'
					},
					regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'The First Name can consist of alphabetical characters and spaces only'
                    }
                }
            },
			middle_name: {
                validators: {
                     stringLength: {
                        min: 1,
                    },
                    notEmpty: {
                        message: 'Please Enter Your Middle Name'
                    },
					regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'The Middle name can consist of alphabetical characters and spaces only'
                    }
                }
            },
             last_name: {
                validators: {
                     stringLength: {
                        min: 1,
                    },
                    notEmpty: {
                        message: 'Please Enter Your Last Name'
					},
					
					regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'The Last name can consist of alphabetical characters and spaces only'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Your Email Address'
					},				
                    emailAddress: {
                        message: 'Please Enter Your Valid Email Address'
					},					
				
					remote: {
						url: "{{ URL::to('/checkclient') }}",
						data: function(validator) {
							return {
								email: validator.getFieldElements('email').val()
							}
						},
						message: 'This Email Id ALready exit.'
					}
						
                }
            },
            address: {
                validators: {
                     stringLength: {
						min: 1,
						message: 'Please Enter Your 8 Charactor'
                    },
                    notEmpty: {
                        message: 'Please Enter Your Address'
                    }
                }
            },
			/*address1: {
                validators: {
                     stringLength: {
						min: 1,
						message: 'Please Enter Your 8 Charactor'
                    },
                    notEmpty: {
                        message: 'Please Enter Your Address'
                    }
                }
            },*/
            city: {
                validators: {
                     stringLength: {
						min: 1,
						
                    },
                    notEmpty: {
                        message: 'Please Enter Your City'
                    },
					regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'The City can consist of alphabetical characters and spaces only'
                    }
                }
            },
            stateId: {
                validators: {
                    notEmpty: {
                        message: 'Please Select Your State'
                    }
                }
            },
			countryId: {
                validators: {
                    notEmpty: {
                        message: 'Please Select Your Country'
                    }
                }
            },
            zip: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Your Zip Code'
                    }					
                }
            },
			business_no: {
                validators: {
                    stringLength: {
                        min: 15,
                        message:'Please enter at least 10 characters and no more than 10'
                    },
                    notEmpty: {
                        message: 'Please Enter Your Business Phone Number'
                    },
                    business_no: {
                        country: 'USA',
                        message: 'Please supply a vaild phone number with area code'
                    }
                }
            },
telephone1: {
        validators: {
            stringLength: {
                min: 15,
                       
                        message:'Please enter at least 10 characters and no more than 10'
                    },
            notEmpty: {
                message: 'Please Enter Phone No.'
            }, 
            
            telephone1: {
                        country: 'USA',
                        message: 'Please supply a vaild phone number with area code'
                    },           
        }
    },
    telephone1type: {
        validators: {
            notEmpty: {
                message: 'Please Select Phone Type'
            },            
        }
    },
			
         
            }
        })
        .on('success.form.bv', function(e) {
            $('#success_message').slideDown({ opacity: "show" }, "slow") // Do something ...
                $('#registrationForm').data('bootstrapValidator').resetForm();
            // Prevent form submission
            e.preventDefault();
            // Get the form instance
            var $form = $(e.target);

            // Get the BootstrapValidator instance
            var bv = $form.data('bootstrapValidator');

            // Use Ajax to submit form data
            $.post($form.attr('action'), $form.serialize(), function(result) {
               // console.log(result);
            }, 'json');
        });
});


</script>
<script>
var dat1 = $('#telephone1ext').val();
$('#telephone1type').on('change', function() {

if(this.value=='Office')
{
document.getElementById('telephone1ext').removeAttribute('readonly');
$('#telephone1ext').val(); 
}
else
{
document.getElementById('telephone1ext').readOnly =true;
$('#telephone1ext').val('');
}
})
</script>
<script>
var dat1 = $('#telephone2ext').val();
$('#telephone2type').on('change', function() {

if(this.value=='Office')
{
document.getElementById('telephone2ext').removeAttribute('readonly');
$('#telephone2ext').val(); 
}
else
{
document.getElementById('telephone2ext').readOnly =true;
$('#telephone2ext').val('');
}
})
</script>
@endsection()
