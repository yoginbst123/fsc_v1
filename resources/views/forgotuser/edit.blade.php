@extends('front-section.app')
@section('main-content')<br><br><br><br><br>
<div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 fsc-content-head">
			<center><h4>Username Reset</h4></center>
		</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 50px;">
    <form class="form-horizontal" method="POST" action="{{ route('forgotuser.update',$business->id) }}">
    {{ csrf_field() }}
{{method_field('PATCH')}}
 
    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
        <label for="email" class="col-md-4 control-label">E-Mail Address</label>

        <div class="col-md-6">
            <input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" required autofocus>
            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>
    </div>
    
    <div class="form-group">
        <div class="col-md-6 col-md-offset-4">
            <button type="submit" class="btn btn-primary">
                Reset Username
            </button>
        </div>
    </div>
</form>
		
	</div>	
	</div>







@endsection
