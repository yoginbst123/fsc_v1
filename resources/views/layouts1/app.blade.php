<!DOCTYPE html>
<html lang="lang="{{ app()->getLocale() }}">
<head>
@include('layouts.head')	
</head>
<body class="sidebar-mini fixed" ng-app="formExample">
<div class="wrapper">
@include('layouts.header')
    @section('main-content')
    @show
@include('layouts.leftsidebar')
@include('layouts.footer')
</div>
</body>
</html>
