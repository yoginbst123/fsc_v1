<header class="main-header  hidden-print">  
<a class="logo" href="{{url('home')}}">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><img  style="width:44px" src="{{URL::asset('public/dashboard/images/fsc_logo.png')}}" alt="" /></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><img src="{{URL::asset('public/dashboard/images/fsc_logo.png')}}" alt="" /></span>
    </a>  
			
			<nav class="navbar navbar-static-top">
				<a class="sidebar-toggle" href="#" data-toggle="offcanvas"></a>
				<div class="navbar-custom-menu">
<div class="topbar"><h1><span class="first-letter">F</span>inancial <span class="first-letter">S</span>ervice <span class="first-letter">C</span>enter</h1></div>
				@guest @else 
								
				
				
					<ul class="top-nav">
						<li><a href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-calendar"></i><span class="dropdown_admin"><!--{{ date('l')}}--> {{ date('F-d-Y')}}</span></a></li>
						<li><a class="dropdown-toggle" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user fa-lg"></i><span class="dropdown_admin">{{ Auth::user()->name }}</span>  &nbsp;&nbsp;<!--<img src="http://api.hostip.info/flag.php" width="30" height="20" border="0" alt="Your Choice">--></a>
<ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
<i class="fa fa-sign-out"></i><span>Logout</span>
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
</li>
					</ul>
					 @endguest
				</div>
			</nav>
		</header>