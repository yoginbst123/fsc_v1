@extends('client.layouts.app')
@section('title', 'Edit Technical Support')
@section('main-content')
<div class="content-wrapper">
	<div class="page-title content-header">
		<h1>Technical Support</h1>
	</div>
<div class="row">
		<div class="col-md-12">
		    <br>
			<div class="card">
				<div class="card-body">
                   
					<form method="post" action="{{route('clientsupport.update',$homecontent->id)}}" class="form-horizontal" id="homecontent" name="homecontent" enctype="multipart/form-data">
					{{csrf_field()}}{{method_field('PATCH')}}
					<div class="form-group">
                   <label class="control-label col-md-3">Date / Day / Time:</label>
                     <div class="col-md-2" style="width: 130px;">
                        <div class="">
                           <input type="text" name="date" id="date" class="form-control"  value="{{$homecontent->date}}" placeholder="Date">
                        </div>
                     </div>
                      
                     <div class="col-md-2" style="width: 130px;">
                        <div class="">
                           <input type="text" name="day" id="day" class="form-control"  value="{{$homecontent->day}}" placeholder="Day">
                        </div>
                     </div>
                     
                     <div class="col-md-2" style="width: 115px;">
                        <div class="">
                           <input type="text" name="time" id="time" class="form-control"  value="{{$homecontent->time}}" placeholder="Time">
                        </div>
                       
                     </div>
                  </div>
						<div class="form-group{{ $errors->has('to') ? ' has-error' : '' }}">
							<label class="control-label col-md-3">To :</label>
							<div class="col-md-8">
							     <select class="form-control"id="to" name="to">
							          <option Value=""> Select</option>
                                                @foreach($employee1 as $emp)
                                                @if(!empty($emp->technical_support))
                                                <option value="{{$emp->id}}" @if($emp->id==$homecontent->to_supporter) selected @endif>{{$emp->technical_support}} ({{$emp->firstName.' '.$emp->middleName.' '.$emp->lastName}})</option>
                                                @endif
                                                  @if(!empty($emp->timing_support))
                                                <option value="{{$emp->id}}" @if($emp->id==$homecontent->to_supporter) selected @endif>{{$emp->timing_support}} ({{$emp->firstName.' '.$emp->middleName.' '.$emp->lastName}})</option>
                                                @endif
                                                @if(!empty($emp->system_support))
                                                <option value="{{$emp->id}}" @if($emp->id==$homecontent->to_supporter) selected @endif>{{$emp->system_support}} ({{$emp->firstName.' '.$emp->middleName.' '.$emp->lastName}})</option>
                                                @endif
                                                @if(!empty($emp->other_support))
                                                <option value="{{$emp>id}}" @if($emp->id==$homecontent->to_supporter) selected @endif>{{$emp->other_support}} ({{$emp->firstName.' '.$emp->middleName.' '.$emp->lastName}})</option>
                                                @endif
                                                
                                                @endforeach
                                 </select>
						
							</div>
						</div>			
						<div class="form-group{{ $errors->has('subject') ? ' has-error' : '' }}">
							<label class="control-label col-md-3">Subject :</label>
							<div class="col-md-8">
								<input name="subject" type="text" id="subject" value="{{$homecontent->subject}}" class="form-control">

								@if ($errors->has('subject'))
										<span class="help-block">
											<strong>{{ $errors->first('subject') }}</strong>
										</span>
									@endif							
							</div>
						</div>
							
					<div class="form-group{{ $errors->has('details') ? ' has-error' : '' }}">
						<label class="control-label col-md-3">Details :</label>
						<div class="col-md-8">
						<div class="box-body pad">
							  <textarea id="editor1" name="details" rows="10" cols="80">{{$homecontent->details}}</textarea>
					  </div>
							@if ($errors->has('details'))
									<span class="help-block">
										<strong>{{ $errors->first('details') }}</strong>
									</span>
								@endif	
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Technical Answer :</label>
						<div class="col-md-8">
						<div class="box-body pad">
						    @if(Auth::user()->id=='30')
							  <textarea id="editor2" name="answer" rows="10" cols="80" readonly>{{$homecontent->answer}}</textarea>
							  @else
							  {!!$homecontent->answer!!}
							  <input  name="answer" type="hidden" value="{{$homecontent->answer}}">
							  @endif
					  </div>
						
						</div>
					</div>
						<div class="form-group">
						<label class="control-label col-md-3">Attachment :</label>
						<div class="col-md-8">
						<div class="box-body pad">
							 <label class="file-upload btn btn-primary">
                Browse for file ... <input type="file" class="form-control fsc-input" style="opacity:0" id="attachment" name="attachment" placeholder="Select Document">
            </label> <img src="{{asset('public/attachment','')}}/{{$homecontent->attachment}}" title="{{$homecontent->subject}}" alt="{{$homecontent->subject}}" width="100px">
					  </div>
								
						</div>
					</div>
					<div class="card-footer">
						<div class="col-md-2 col-md-offset-3">
									<input class="btn_new_save" type="submit" value="save">
									</div>
									<div class="col-md-2 row">
									<a class="btn_new_cancel" href="https://financialservicecenter.net/client/cli-employee">Cancel</a> 
									</div>
					</div>

						
					</form>
				</div>
			</div>
		</div>
	</div>
	
</div>
<script>
    $(document).ready(function(){
       $('#type').on('change', function(){
         if($('#type').val()=='Resposibilty')
         {
             $('.emp').show();
         }
         else
         {
            $('.emp').hide();  
         }
         
       });
    });
    
</script>
<style>
    input[type="file"] {
    display: block;
    position: absolute;
}
</style>
<script type="text/javascript">
 $( "#date" ).datepicker({
    'dateFormat':'yy-mm-dd',
    onSelect: function(dateText){ alert();
        var seldate = $(this).datepicker('getDate');
        seldate = seldate.toDateString();
        seldate = seldate.split(' ');
        var weekday=new Array();
            weekday['Mon']="Monday";
            weekday['Tue']="Tuesday";
            weekday['Wed']="Wednesday";
            weekday['Thu']="Thursday";
            weekday['Fri']="Friday";
            weekday['Sat']="Saturday";
            weekday['Sun']="Sunday";
        var dayOfWeek = weekday[seldate[0]];
        $('#day').val(dayOfWeek);
    }
}); 
  $(document).ready(function() {
    $("#date").change(function() {
      var startdate= $("#date").val();
      var monthNames = [
        "Jan", "Feb", "Mar",
        "Apr", "May", "Jun", "Jul",
        "Aug", "Sep", "Oct",
        "Nov", "Dec"
      ];
    var weekday = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
      var durtion=$('#duration').val();
      var  date = new Date(startdate);
      var day = weekday[date.getDay()];
      var monthly=30;
      var weekly=7;
      var bimonthly=15;
      var biweekly=14;
      var monthss=monthNames[(date.getMonth())];
      var yearss=date.getFullYear();
      var yyy=yearss % 4 ;
      //if(yyy)
      //{
      //alert('true');
      //}
      //else
      //{
      //alert('false');
      //}
      if(durtion == "Weekly")
      {
        var totaldays=6;
      }
      else if(durtion == "Monthly")
      {
        if(monthss == 'Jan' || monthss == 'Mar' || monthss == 'May' || monthss == 'Jul' || monthss == 'Aug' || monthss == 'Oct' || monthss == 'Dec')
        {
          var totaldays=30;
        }
        else if(monthss == 'Feb')
        {
          //if(years / 4 = 0)
          if(yyy ==0)
          {
            var totaldays=28;
          }
          else if(yyy ==1)
          {
            var totaldays=27;
          }
        }
        else if(monthss == 'Apr' || monthss == 'Jun' || monthss == 'Sep' || monthss == 'Nov' )
        {
          var totaldays=29;
        }
      }
      else if(durtion == "Bi-Weekly")
      {
        var totaldays=13;
      }
      else if(durtion == "Bi-Monthly")
      {
        var totaldays=14;
      }
    // var vv = day + totaldays;
      
    date.setDate(date.getDate() + totaldays);// alert(vv);
      var date1 = ("0" + (date.getMonth() + 1)).slice(-2)  + "/" + ("0" + date.getDate()).slice(-2) + "/" + date.getFullYear();// alert(date.getDate())
     // alert(date1);
      var newdate = new Date(date1);
        var day1 = weekday[newdate.getDay()];
       //alert(newdate);
      var date2=monthNames[(date.getMonth())] + "/" + date.getDate() + "/" + date.getFullYear() ;
     // $('#sch_end_date').val(date1);
      $('#day').val(day);
      $('#sch_end_day').val(day1);
      //document.write(date2);
    });
    $("#duration").change(function() {
      $('#sch_end_date').val('');
      $('#sch_start_date').val('');
    });
  });
</script>
@endsection()