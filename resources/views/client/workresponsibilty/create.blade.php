@extends('client.layouts.app')
@section('title', 'Add Work Responsibilty')
@section('main-content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>Add Work Resposibilty</h1>
    </section>
    <!-- Main content -->
    <section class="content">
	<div class="row">
		<div class="col-md-12">
				<div class="box box-success">
			     <br>
				<div class="col-md-12">			
					<form method="post" action="{{route('workresponsibilty.store')}}" class="form-horizontal" id="content" name="content" enctype="multipart/form-data">
					{{csrf_field()}}
						<div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
							<label class="control-label col-md-3">Type :</label>
							<div class="col-md-8">
								<select name="type" type="type" id="type" class="form-control">
 <option value="">---Select---</option>
 <option value="Rules">Rules</option>
 <option value="Resposibilty">Resposibilty</option>
                                                                </select>	
								@if ($errors->has('type'))
										<span class="help-block">
											<strong>{{ $errors->first('type') }}</strong>
										</span>
									@endif							
							</div>
						</div>	
							<div class="form-group{{ $errors->has('type') ? ' has-error' : '' }} emp" style="display:none">
							<label class="control-label col-md-3">Employee :</label>
							<div class="col-md-8">
								<select name="employee_id" id="employee_id" class="form-control">
                                      <option value="">---Select Employee---</option>
                                      @foreach($employee as $as)
                                              <option value="{{$as->id}}">{{ucfirst($as->firstName)}}</option>
                                      @endforeach
                                </select>	
								@if ($errors->has('type'))
										<span class="help-block">
											<strong>{{ $errors->first('type') }}</strong>
										</span>
									@endif							
							</div>
						</div>	
						
							<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
							<label class="control-label col-md-3">Title :</label>
							<div class="col-md-8">
								<input name="title" type="text" id="title" class="form-control">

								@if ($errors->has('title'))
										<span class="help-block">
											<strong>{{ $errors->first('title') }}</strong>
										</span>
									@endif							
							</div>
						</div>	
						<div class="form-group{{ $errors->has('rules') ? ' has-error' : '' }}">
							<label class="control-label col-md-3">Rules / Resposibilty :</label>
							<div class="col-md-8">
							<div class="">
								  <textarea id="editor1" name="rules" rows="10" cols="80"></textarea>
						  </div>
								@if ($errors->has('rules'))
										<span class="help-block">
											<strong>{{ $errors->first('rules') }}</strong>
										</span>
									@endif	
							</div>
						</div>
						<div class="card-footer">
						    <div class="form-group">
							<label class="control-label col-md-3"></label>
							<div class="col-md-2">
<input class="btn_new_save btn-primary1" type="submit" name="submit" value="Save">
							</div>
							<div class="col-md-2 row">
<a class="btn_new_cancel" href="{{url('client/workresponsibilty')}}">Cancel</a> 
							</div>
						</div>
						  </div>
						
					</form>
				</div>
			</div>
		</div>
	</div>
	</section>
</div>
<script>
    $(document).ready(function(){
       $('#type').on('change', function(){
         if($('#type').val()=='Resposibilty')
         {
             $('.emp').show();
         }
         else
         {
            $('.emp').hide();  
         }
         
       });
    });
    
</script>

@endsection()