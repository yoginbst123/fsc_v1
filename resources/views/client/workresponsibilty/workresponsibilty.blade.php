@extends('client.layouts.app')
@section('title', 'Work Responsibilty')
@section('main-content')
<div class="content-wrapper">
	   <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>Work Resposibilty</h1>
    </section>
    <!-- Main content -->
    <section class="content">
	<div class="row">
	
		<div class="col-md-12">
				<div class="card">
			      <div class="box-header">
              <div class="box-tools pull-right">
					<div class="table-title">
						<a href="{{route('workresponsibilty.create')}}" class="btn btn-primary">Add New Work Resposibilty</a>
					</div>
              </div>
            </div>
				<div class="col-md-12">

					@if ( session()->has('success') )
    <div class="alert alert-success alert-dismissable">{{ session()->get('success') }}</div>
@endif
@if ( session()->has('error') )
    <div class="alert alert-danger alert-dismissable">{{ session()->get('error') }}</div>
@endif  
					<div class="table-responsive">
						<table class="table table-hover table-bordered" id="sampleTable3">
							<thead>
								<tr>
									<th>#</th>
<th>Type</th>
<th>Title</th>
<th>Rules</th>
<th>Action</th>
									
								</tr>
							</thead>
							<tbody>
							@foreach($homecontent as $sli)                          
								<tr>
									<td>{{$loop->index + 1}}</td>
									<td>{!!$sli->type!!}</td>
                                    <td>{!!$sli->title!!}</td>
                                    <td>{!!$sli->rules!!}</td>
											
									<td>
										<a class="btn btn-success" href="{{route('workresponsibilty.edit', $sli->id)}}"><i class="fa fa-edit"></i></a>
										
                                        <form action="{{ route('workresponsibilty.destroy',$sli->id) }}" method="post" style="display:none" id="delete-id-{{$sli->id}}">
                                        {{csrf_field()}} {{method_field('DELETE')}}
                                        </form>
										<a class="btn btn-danger" onclick="if(confirm('Are you sure, You want to delete this record ?'))
                                                                                  {event.preventDefault();document.getElementById('delete-id-{{$sli->id}}').submit();} else{event.preventDefault();}" href=""><i class="fa fa-trash"></i></a>
									</td>
								</tr>
								@endforeach
                             </tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</section>		
</div>


	
@endsection()