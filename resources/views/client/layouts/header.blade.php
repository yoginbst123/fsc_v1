<?php
$ccountry = "America/New_York";
date_default_timezone_set($ccountry);
$time =date_default_timezone_get();
?>
<style>
    .topbar {
    box-shadow: 3px 3px 5px #191c1f91;
    padding: 12px 20px;
    background-color: #fff;
    border-radius: 0;
    float: left;
    margin: 16px 0;
}
.new_company_name div {
    width: 60%;
    display: inline-block;
    font-size: 16px;
    color: #286db5;
    font-weight: normal;
    font-style: normal !important;
    margin: 0;
}
#countryList{
    border:none !important;
}
.top_company.top-company-name-bg {
    background: #fff;
    border-radius: 5px;
    margin: 10px 0;
    padding: 0;
}
.top_company {
    float: left;
    margin: 34px 0 0 0;
}
.top_company {
    display: block;
}
.new_company_name label {
    width: 36%;
    display: inline-block;
    font-size: 14px;
    color: #FFF;
    background: #2775ae;
    font-weight: 600;
    margin: 0 2% 0 0;
    text-align: left;
    padding: 4px 10px;
}
.newcmpname {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    height: 77px;
}
.new_company_name {
    float: left;
    width: 100%;
    background: #FFF;
    padding: 0;
    border: 1px solid #286db5;
    margin: 4px 0;
}
.new_client_id p {
    display: block;
    font-size: 18px;
    color: #FFF;
    font-weight: bold;
    font-style: normal !important;
    margin: 0;
}
.new_client_id {
    float: left;
    width: 100%;
    background: #26bdaf;
    padding: 16px 0;
    border: 1px solid #098478;
    text-align: center;
    margin: 9px 10px;
}
.top_company.top-company-name-bg h3 {
    margin: 0; padding:0px;
    color: #fff;
    background: #12186b;
    padding: 10px;
    border-radius: 5px 5px 0 0px;
    font-size: 18px;
    min-width: 230px;
    text-align: center;
}
.topbar h3{margin:0px!important; padding:0px 0px!important;}
.top_company p{padding:7px 0px!important; margin:0px!important;}
.top_company.top-company-name-bg p {
    color: #000;
    padding-left: 0;
    padding: 10px;
    text-align: center;
    display: block;
    font-size: 18px;
}
.top_company.top-company-name-bg {
    background: #fff;
    border-radius: 5px;
    margin: 10px 0;
    padding: 0;
}
.top_company.top-company-name-bg .top_date, .top_time, .top_day {
    padding: 3px 0;
}
.top_company.top-company-name-bg .top_date, .top_time, .top_day {
    width: 100%;
}
.top_date {
    float: left;
    width: 39%;
    padding: 4px 0;
    margin: 0 1%;
    text-align: left;
}
.top_company.top-company-name-bg i {
    color: #000;
}
.top_date i, .top_time i, .top_day i {
    margin-right: 12px;
    font-size: 14px;
    color: #FFF;
    margin-top: 6px !important;
}
.top_company.top-company-name-bg span {
    color: #000;
    font-size: 14px;
}
.main-header .navbar-custom-menu, .main-header .navbar-right{width:95%;}
.headright{
    display:flex; flex-direction:row; justify-content:flex-end;
}.headright .top-company-name-bg{margin:10px 15px!important;}
.logoutlink{display:flex; flex-direction:row; justify-content:center; align-items:center;}
.logoutlink a{color:#ffffff!important; padding:5px;}
</style>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"> </script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.4.1/moment-timezone-with-data-2010-2020.min.js"> </script>    
<script type="text/javascript">   
   $(function(){
  setInterval(function(){
    var divUtc = $('#divUTC');
    var divLocal = $('#divLocal');  
    divUtc.text(moment.utc().format('YYYY-MM-DD HH:mm:ss a'));      
    var localTime  = moment.utc(divUtc.text()).toDate();
     $('#divUsa').text(moment.tz('{{$time}}').format('hh:mm:ss a'));
  },1000);
});
</script>  
<header class="main-header">
    <!-- Logo -->
    <a href="{{url('client/home')}}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini">
      <img style="width:44px" src="{{URL::asset('public/dashboard/images/fsc_logo.png')}}" alt="" />
      </span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">
      <img src="{{URL::asset('public/dashboard/images/fsc_logo.png')}}" alt="" class="img-responsive" />
      </span> 
      </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        @guest @else @endguest
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
        <?php
        
        if(isset($_REQUEST['filename'])!='')
        {
            $data=DB::table('commonregisters')->where('filename','=',$_REQUEST['filename'])->get();
            $db1=DB::table('commonregisters')->where('id','=',$data[0]->id)->get();
           
        }
        else
        {
            $db1=DB::table('commonregisters')->where('id','=',Auth::user()->user_id)->get();
           
        }
        
        ?>
    
    
      <div class="navbar-custom-menu">
        <!--<div class="topbar"><h1><span class="first-letter">F</span>inancial <span class="first-letter">S</span>ervice <span class="first-letter">C</span>enter</h1></div>-->
        <div class="col-md-12">
            <div class="col-md-6">
                <div style="width:78%;float:left;">
                  	<div class="newcmpname">
                        <div class="new_company_name">
                            <label>Company Name :</label>
                            <div>{{ $db1[0]->company_name }}</div>
                        </div>
                        <div class="new_company_name">
                            <label>Business Name :</label>
                            <div>{{ $db1[0]->business_name }}</div>
                        </div>
                    </div>
                    
                     <div style="width:300px">
                <div class="dropdown" style="margin-top: 1%;">
                    
                    <select class="form-control fsc-input" id="changefilename">
                    
                    <?php
                    
                    if(isset($_REQUEST['filename'])!='')
                    {
                        
                       
                            $db2=DB::table('commonregisters')->where('id','=',Auth::user()->user_id)->get(); 
                            $db3=DB::table('commonregisters')->where('id','=',$db2[0]->id)->first(); 
                            $commonuser2 = DB::table('commonregisters')->select('commonregisters.productid','commonregisters.check_status','commonregisters.company_name','commonregisters.filename','commonregisters.business_id','commonregisters.first_name','commonregisters.last_name','commonregisters.status')->where('status','=','Active')->orderby('filename','asc')->get();
                            $strposlink2=$db3->productid;
                            $splittedstringposlink2=explode(",",$strposlink2);
                           ?>
                           
                           @foreach($commonuser2 as $subcustomer1)
                                <?php 
                                if($subcustomer1->filename != $db3->filename)
                                {
                                  if($subcustomer1->business_id =='6')
                                  {
                                        $entityname=$subcustomer1->first_name.' '.$subcustomer1->last_name;
                                  }
                                  else
                                  {
                                        $entityname=$subcustomer1->company_name;
                                  }
                          
                    ?>    
                            
                                    @foreach($splittedstringposlink2 as $key => $value) 
                                        @if($value ==$subcustomer1->filename && $subcustomer1->check_status==1)
                                        <form method="post">
                                           <option value="{{$subcustomer1->filename}}" @if($db1[0]->filename==$subcustomer1->filename) selected @endif>{{$subcustomer1->filename}} ({{$entityname}})</option> 
                                        </form>
                                        @endif 
                                    @endforeach
                                <?php 
                                } 
                                else if($db3->filename!='')
                                { 
                                   if($subcustomer1->business_id =='6')
                                  {
                                        $entityname=$subcustomer1->first_name.' '.$subcustomer1->last_name;
                                  }
                                  else
                                  {
                                        $entityname=$subcustomer1->company_name;
                                  } 
                                ?>
                                    <form method="post">
                                       <option value="000">{{$db3->filename}} ({{$entityname}})</option> 
                                    </form>

                                <?php 
                                }
                                ?>
                            @endforeach
                           
                           <?php
            
                    }
                    else
                    {
                        ?>
                            <?php 
                                $db2=DB::table('commonregisters')->where('id','=',Auth::user()->user_id)->get(); 
                                $db3=DB::table('commonregisters')->where('id','=',$db2[0]->id)->first(); 
                                $commonuser = DB::table('commonregisters')->select('commonregisters.productid','commonregisters.check_status','commonregisters.company_name','commonregisters.filename','commonregisters.business_id','commonregisters.first_name','commonregisters.last_name','commonregisters.status')->where('status','=','Active')->orderby('filename','asc')->get();
                                $strposlink=$db3->productid;
                                $splittedstringposlink=explode(",",$strposlink);
                            ?>       
                            @foreach($commonuser as $subcustomer1)
                                <?php 
                                if($subcustomer1->filename != $db3->filename)
                                {
                                  if($subcustomer1->business_id =='6')
                                  {
                                        $entityname=$subcustomer1->first_name.' '.$subcustomer1->last_name;
                                  }
                                  else
                                  {
                                        $entityname=$subcustomer1->company_name;
                                  }
                                ?>
                                    @foreach($splittedstringposlink as $key => $value) 
                                        @if($value ==$subcustomer1->filename && $subcustomer1->check_status==1)
                                        <form method="post">
                                           <option value="{{$subcustomer1->filename}}">{{$subcustomer1->filename}} ({{$entityname}})</option> 
                                        </form>
                                        @endif 
                                    @endforeach
                                <?php 
                                } 
                                else if($db3->filename!='')
                                { 
                                      if($subcustomer1->business_id =='6')
                                      {
                                            $entityname=$subcustomer1->first_name.' '.$subcustomer1->last_name;
                                      }
                                      else
                                      {
                                            $entityname=$subcustomer1->company_name;
                                      }
                                ?>
                                    <form method="post">
                                       <option value="000">{{$db3->filename}} ({{$entityname}})</option> 
                                    </form>

                                <?php 
                                }
                                ?>
                            @endforeach
                           
                           <?php
                    }
                        
                     ?>
                    </select>
                    
                </div>
            </div>
                </div>
                
                <div style="width:22%;float:right;">
                  	<div class="new_client_id">
                        <p>{{ $db1[0]->filename }} </p>
                    </div>
                </div>
      
            </div>
            
           
            
            <div class="col-md-6">
                  <div class="headright">
                      <div class="top_company top-company-name-bg" style="height:80px;">
            			<h3>Client</h3>
            			<p>
            			    <?php
                            if(isset($_REQUEST['filename'])!='')
                            { 
                                $data=DB::table('commonregisters')->where('filename','=',$_REQUEST['filename'])->get();
                                ?>
                                @include('client.userinfo',array('userinfo'=> DB::table('commonregisters')->where('id','=',$data[0]->id)->get()))
                                <?php
                            }
                            else
                            {
                                ?>
                                @include('client.userinfo',array('userinfo'=> DB::table('commonregisters')->where('id','=',Auth::user()->user_id)->get()))
                                <?php
                            }
            			 ?>    
            			</p>
            		</div>
                  
                  <div class="top_date_section top_company top-company-name-bg pull-right" style="padding:5px 10px;margin-bottom:5px !Important;">
                    <!--  <div class="top_date_section top_company top-company-name-bg pull-right" style="padding: 3px 15px;">-->
					<div class="top_date"><i class="fa fa-calendar"></i><span>July-03-2020</span></div>
					<div class="top_day"><i class="fa fa-calendar-o"></i><span>{{ date('l')}}</span></div>
					<div class="top_time"><i class="fa fa-clock-o"></i> <span class="dropdown_admin" id="divUsa"></span></div>
				</div>
				<div class="logoutlink">
				     <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i></a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
				</div>
                </div>
                  
            </div>
          </div>
        
        <ul class="nav navbar-nav" style="display:none;">
          <!-- Messages: style can be found in dropdown.less-->
          <li class="messages-menu">
		
			</li>
          <li class="messages-menu">
           
          </li>
          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
           <i class="fa fa-calendar"></i>  <br> <i class="fa fa-clock"></i>
            </a>
          </li>
          <!-- Tasks: style can be found in dropdown.less -->
          <li class="dropdown tasks-menu">
            </li>
         
          <!-- Control Sidebar Toggle Button -->
          <li>
           
          </li>
        </ul>
      </div>
    </nav>
  </header>
