<!DOCTYPE html>
<html lang="lang="{{ app()->getLocale() }}">
<head>
@include('client.layouts.head')	
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
@include('client.layouts.header')
    @section('main-content')
    @show
@include('client.layouts.leftsidebar')
@include('client.layouts.footer')
</div>
</body>
</html>
