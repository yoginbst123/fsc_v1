<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>FSC - Client</title>
<link rel="shortcut icon" href="{{URL::asset('public/dashboard/images/favicon.png')}}" type="image/x-icon">


  <link rel="stylesheet" href="{{URL::asset('public/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{URL::asset('public/adminlte/bower_components/font-awesome/css/font-awesome.min.css')}}">
  <link rel="stylesheet" href="{{URL::asset('public/adminlte/bower_components/Ionicons/css/ionicons.min.css')}}">
  <link rel="stylesheet" href="{{URL::asset('public/adminlte/dist/css/AdminLTE.min.css')}}">
  <link rel="stylesheet" href="{{URL::asset('public/adminlte/dist/css/skins/_all-skins.min.css')}}">
  <link rel="stylesheet" href="{{URL::asset('public/adminlte/bower_components/morris.js/morris.css')}}">
  <link rel="stylesheet" href="{{URL::asset('public/adminlte/bower_components/jvectormap/jquery-jvectormap.css')}}">
  <link rel="stylesheet" href="{{URL::asset('public/adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
  <link rel="stylesheet" href="{{URL::asset('public/adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">
  <link rel="stylesheet" href="{{URL::asset('public/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">
  <link rel="stylesheet" href="{{URL::asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{URL::asset('public/adminlte/custom.css')}}">
  <script src="{{URL::asset('public/adminlte/bower_components/jquery/dist/jquery.min.js')}}"></script>
  <script src="{{URL::asset('public/adminlte/bower_components/jquery-ui/jquery-ui.min.js')}}"></script>
  <script src="{{URL::asset('public/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
  <script src="{{URL::asset('public/adminlte/bower_components/raphael/raphael.min.js')}}"></script>
  <script src="{{URL::asset('public/adminlte/bower_components/morris.js/morris.min.js')}}"></script>
  <script src="{{URL::asset('public/adminlte/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>
  <script src="{{URL::asset('public/adminlte/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
  <script src="{{URL::asset('public/adminlte/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
  <script src="{{URL::asset('public/adminlte/bower_components/jquery-knob/dist/jquery.knob.min.js')}}"></script>
  <script src="{{URL::asset('public/adminlte/bower_components/moment/min/moment.min.js')}}"></script>
  <script src="bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
  <script src="{{URL::asset('public/adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
  <script src="{{URL::asset('public/adminlte/bower_components/ckeditor/ckeditor.js')}}"></script>
  <script src="{{URL::asset('public/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
  <script src="{{URL::asset('public/adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
  <script src="{{URL::asset('public/adminlte/bower_components/fastclick/lib/fastclick.js')}}"></script>
  <script src="{{URL::asset('public/adminlte/dist/js/adminlte.min.js')}}"></script>
  <script src="{{URL::asset('public/adminlte/dist/js/pages/dashboard.js')}}"></script>
  <script src="{{URL::asset('public/adminlte/dist/js/demo.js')}}"></script>
  <script src="{{URL::asset('public/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
  <script src="{{URL::asset('public/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>  
  <script type="text/javascript" src="{{URL::asset('public/dashboard/js/dataTables.buttons.min.js')}}"></script>
  <script type="text/javascript" src="{{URL::asset('public/dashboard/js/buttons.flash.min.js')}}"></script>
  <script type="text/javascript" src="{{URL::asset('public/dashboard/js/jszip.min.js')}}"></script>
  <script type="text/javascript" src="{{URL::asset('public/dashboard/js/pdfmake.min.js')}}"></script>
  <script type="text/javascript" src="{{URL::asset('public/dashboard/js/vfs_fonts.js')}}"></script>
  <script type="text/javascript" src="{{URL::asset('public/dashboard/js/buttons.html5.min.js')}}"></script>
  <script type="text/javascript" src="{{URL::asset('public/dashboard/js/buttons.print.min.js')}}"></script>
  <script type="text/javascript" src="{{URL::asset('public/frontcss/js/bootstrap-formhelpers.min.js')}}"></script>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.3/jquery.timepicker.min.css">
  <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.3/jquery.timepicker.min.js"></script>

<script>
  $(function () {
    CKEDITOR.replace('details')

  })
  $(function () {
    CKEDITOR.replace('answer')
  
  })
</script>

<style>
.modal-dialog {width: 60%;margin: 30px auto;}
.sw-main .sw-container{ z-index:9}
footer p {text-align: center;margin-top: 20px;color: #054b94;font-size: 17px;}
.tess {font-size: 11px;width: 100%; display:block;}
.fixed .main-header, .fixed .main-sidebar, .fixed .left-side {overflow-y: inherit;}
#smartwizard .nav-pills {background-color: #e6e6e6;}
</style>
<script type="text/javascript">
$(document).ready(function(){
	var url = window.location;
$('ul.sidebar-menu a').filter(function() {
    return this.href == url;
}).parent().siblings().removeClass('active').end().addClass('active');
$('ul.treeview-menu a').filter(function() {
    return this.href == url;
}).parentsUntil(".sidebar-menu > .treeview-menu").siblings().removeClass('active').end().addClass('active');
});
</script>

<script>
   $("#business_no").mask("(999) 999-9999");
   $(".ext").mask("999");
   $("#business_fax").mask("(999) 999-9999");
   $(".business_fax1").mask("(999) 999-9999");
   $(".residence_fax").mask("(999) 999-9999");
   $("#mobile_no").mask("(999) 999-9999");
   $(".cell").mask("(999) 999-9999");
   $(".telephone").mask("(999) 999-9999");
   $(".usapfax").mask("(999) 999-9999");
   $("#zip").mask("9999");
   $("#mailing_zip").mask("9999");
   $("#bussiness_zip").mask("9999");
</script>

@guest
@else
<?php 
$remdays = Auth::user()->enddate;
 $no1 = date('Y-m-d', strtotime('-3 days', strtotime($remdays)));
$no2 = date('Y-m-d', strtotime('-2 days', strtotime($remdays)));
$no3 = date('Y-m-d', strtotime('-1 days', strtotime($remdays)));
$no4 = date('Y-m-d', strtotime('-0 days', strtotime($remdays)));
$beforethreeday = strtotime($no1);
$enddate = strtotime($remdays);
$currentday = date('Y-m-d');
if($currentday==$no1 || $currentday==$no2 || $currentday==$no3) 
{
?>
@if (session()->has('success') )
<script>
$(window).load(function(){        
   $('#myModal').modal('show');
}); 
</script>
@endif
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Change Password</h4>
      </div>
      <div class="modal-body">
        <p><?php if($currentday==$no1){?>Please Change Your Password then Your Password Expired after 3 days<?php }?><?php if($currentday==$no2){?>Please Change Your Password then Your Password Expired after 2 days<?php }?><?php if($currentday==$no3){?>Please Change Your Password then Your Password Expired after 1 days<?php }?></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<?php 
}
elseif($currentday == $no4)
{
?>
<style>
.modal-content {width: 700px;}
</style>

<script type="text/javascript">
$(document).ready(function(){
$("#myModal").modal({
		backdrop:'static',
		keyword:'false',
		show: true,
	});
	});	
</script>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Change Password</h4>
      </div>
      <div class="modal-body">
<form method="post" class="form-horizontal changepassword" enctype="multipart/form-data"  action="{{route('userchangepassword.update',Auth::user()->id)}}" id="changepassword1">
                  {{csrf_field()}} 
                  {{method_field('PATCH')}}
                  <div class="row">
                     <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group {{ $errors->has('oldpassword') ? ' has-error' : '' }}">
                           <label class="control-label col-md-3">Old Password :</label>
                           <div class="col-md-8">
                              <input type="password" class="form-control" id="oldpassword" value="" name="oldpassword">						
                              @if ($errors->has('oldpassword'))
                              <span class="help-block">
                              <strong>{{ $errors->first('oldpassword') }}</strong>
                              </span>
                              @endif	
                              @if ( session()->has('error') )
                              <span class="help-block" style="color:red">
                              <strong>{{ session()->get('error') }}</strong>
                              </span>
                              @endif 
                           </div>
                        </div>
                        <div class="form-group  {{ $errors->has('password') ? ' has-error' : '' }}">
                           <label class="control-label col-md-3">New Password :</label>
                           <div class="col-md-8">
                              <input name="password" value="" type="password" id="password" class="form-control" />
                              @if ($errors->has('password'))
                              <span class="help-block">
                              <strong>{{ $errors->first('password') }}</strong>
                              </span>
                              @endif
                           </div>
                        </div>
                        <div class="form-group  {{ $errors->has('confirmpassword') ? ' has-error' : '' }}">
                           <label class="control-label col-md-3">Confirm Password :</label>
                           <div class="col-md-8">
                              <input name="confirmpassword" value="" type="password" id="confirmpassword" class="form-control"/>
                              @if ($errors->has('confirmpassword'))
                              <span class="help-block">
                              <strong>{{ $errors->first('confirmpassword') }}</strong>
                              </span>
                              @endif
                           </div>
                        </div>
                        <div class="form-group  {{ $errors->has('resetdays') ? ' has-error' : '' }}">
                           <label class="control-label col-md-3">Reset Days :</label>
                           <div class="col-md-4">
                              <select name="resetdays" value="" id="resetdays" class="form-control">
                                 <option Value="">---Select Reset Days---</option>
                                  <option value="30" @if(Auth::user()->reset_day=='30') selected @endif>30</option>
                                 <option value="90" @if(Auth::user()->reset_day=='90') selected @endif>90</option>
                                 <option value="120" @if(Auth::user()->reset_day=='120') selected @endif>120</option>
                              </select>
                             
                           </div>
                           <div class="col-md-4">
                             <input name="reset_date" id="reset_date" value="" class="form-control fsc-input" readonly="">
                           </div>
                        </div>
                     </div>
                  </div>
				  <div class="modal-footer">
	  <input class="btn btn-primary icon-btn" type="submit" name="submit" value="Change Password">
      </div>
                  <br>
               </form>
      </div>
      
    </div>

  </div>
</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js"></script>
<script>
   $.ajaxSetup({
       headers:
       {
           'X-CSRF-Token': $('input[name="_token"]').val()
       }
   });
   $(document).ready(function() {
   
   $('.changepassword').bootstrapValidator({        
           feedbackIcons: {
               valid: 'glyphicon glyphicon-ok',
               invalid: 'glyphicon glyphicon-remove',
               validating: 'glyphicon glyphicon-refresh'
           },
   		fields: {
   			newpassword: {
           	validators: {
               notEmpty: {
                   message: 'The password is required and cannot be empty'
               },
               regexp:
            	{
            
           	regexp: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8}/,
   
           	message: 'The password should contain Minimum 8 and Maximum 12 characters at least 1 Uppercase Alphabet, 1 Lowercase Alphabet, 1 Number and 1 Special Character:'
   	   		},
   	 
   				different: {
   					field: 'oldpassword',
   					message: 'The password cannot be the same as Current Password'
   				}
           	}
       	},
    			oldpassword: {
                   validators: {
                       notEmpty: {
                           message: 'Please Enter Your Current Password'
   					},	
                        remote: {
                           message: 'The Password is not available',
                           url: '{{ URL::to('client/checkpassword1') }}',
                           data: {
                               type: 'oldpassword'
                           },
                           type: 'POST'
                       }																		
                   }
               },
               cpassword: {
                   validators: {  
   					notEmpty: {  
   						message: 'The confirm password is required and can\'t be empty'  
   					},  
   					identical: {  
   						field: 'newpassword',  
   						message: 'The password and its confirm are not the same'  
   					},  
   					different: {  
   						field: 'oldpassword',  
   						message: 'The password can\'t be the same as Old Password'  
   					}  
   				}  
               }
             }).on('success.form.bv', function(e) {
               $('.changepassword').slideDown({ opacity: "show" }, "slow") // Do something ...
                   $('.changepassword').data('bootstrapValidator').resetForm();
               e.preventDefault();
               var $form = $(e.target);
               var bv = $form.data('bootstrapValidator');
               $.post($form.attr('action'), $form.serialize(), function(result) {
               }, 'json');
           });
   });
</script>
<?php 
$remdays = Auth::user()->enddate;
$no1 = date('Y-m-d', strtotime('-3 days', strtotime($remdays)));
$no2 = date('Y-m-d', strtotime('-2 days', strtotime($remdays)));
$no3 = date('Y-m-d', strtotime('-1 days', strtotime($remdays)));
$no4 = date('Y-m-d', strtotime('-0 days', strtotime($remdays)));
$beforethreeday = strtotime($no1);
$enddate = strtotime($remdays);
$currentday = date('Y-m-d');
if($currentday==$no1 || $currentday==$no2 || $currentday==$no3) 
{
?>
@if ( session()->has('success') )
<script>
$(window).load(function(){        
   $('#myModal').modal('show');
}); 
</script>
@endif
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Change Password</h4>
      </div>
      <div class="modal-body">
        <p><?php if($currentday==$no1){?>Please Change Your Password then Your Password Expired after 3 days<?php }?><?php if($currentday==$no2){?>Please Change Your Password then Your Password Expired after 2 days<?php }?><?php if($currentday==$no3){?>Please Change Your Password then Your Password Expired after 1 days<?php }?></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<?php 
}
elseif($currentday == $no4)
{
?>
<style>
.modal-content {width: 700px;}
</style>

<script type="text/javascript">
$(document).ready(function(){
$("#myModal").modal({
		backdrop:'static',
		keyword:'false',
		show: true,
	});
	});	
</script>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Change Password</h4>
      </div>
      <div class="modal-body">
	  <form method="post" class="form-horizontal changepassword" enctype="multipart/form-data"  action="{{route('userchangepassword.update', Auth::user()->id)}}" id="changepassword"> 
                  {{csrf_field()}} 
                  {{method_field('PATCH')}}
                  <div class="row">
                     <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group {{ $errors->has('oldpassword') ? ' has-error' : '' }}">
                           <label class="control-label col-md-3">Old Password : </label>
                           <div class="col-md-8">
                              <input type="password" class="form-control" id="oldpassword" value="" name="oldpassword">						@if ($errors->has('oldpassword'))
                              <span class="help-block">
                              <strong>{{ $errors->first('oldpassword') }}</strong>
                              </span>
                              @endif	
                              @if ( session()->has('error') )
                              <span class="help-block" style="color:red">
                              <strong>{{ session()->get('error') }}</strong>
                              </span>
                              @endif 
                           </div>
                        </div>
                        <div class="form-group  {{ $errors->has('newpassword') ? ' has-error' : '' }}">
                           <label class="control-label col-md-3">New Password :</label>
                           <div class="col-md-8">
                              <input name="newpassword" value="" type="password" id="newpassword" class="form-control" />
                              <div id="messages"></div>
                              @if ($errors->has('newpassword'))
                              <span class="help-block">
                              <strong>{{ $errors->first('newpassword') }}</strong>
                              </span>
                              @endif
                           </div>
                        </div>
                        <div class="form-group  {{ $errors->has('cpassword') ? ' has-error' : '' }}">
                           <label class="control-label col-md-3">Confirm Password :</label>
                           <div class="col-md-8">
                              <input name="cpassword" value="" type="password" id="cpassword" class="form-control"/>
                              @if ($errors->has('cpassword'))
                              <span class="help-block">
                              <strong>{{ $errors->first('cpassword') }}</strong>
                              </span>
                              @endif
                           </div>
                        </div>
                        <div class="form-group  {{ $errors->has('resetdays') ? ' has-error' : '' }}">
                           <label class="control-label col-md-3">Reset Days :</label>
                           <div class="col-md-8">
                              <select name="resetdays" value="" id="resetdays" class="form-control">
                                  <option>---Select Reset Days---</option>
                                  <option value="30" @if(Auth::user()->resetdays=='30') selected @endif>30</option>
                                 <option value="90" @if(Auth::user()->resetdays=='90') selected @endif>90</option>
                                 <option value="120" @if(Auth::user()->resetdays=='120') selected @endif>120</option>
                              </select>
                           </div>
                        </div>
                     </div>
                  </div>
     <div class="modal-footer">
	     <input class="btn btn-primary icon-btn" type="submit" name="submit" value="Change Password">
     </div><br>
    </form>
      </div>
    </div>
  </div>
</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js"></script>
<script>
   $.ajaxSetup({
       headers:
       {
           'X-CSRF-Token': $('input[name="_token"]').val()
       }
   });
   $(document).ready(function() {
   
   $('.changepassword').bootstrapValidator({        
           feedbackIcons: {
               valid: 'glyphicon glyphicon-ok',
               invalid: 'glyphicon glyphicon-remove',
               validating: 'glyphicon glyphicon-refresh'
           },
   		fields: {
   			newpassword: {
           	validators: {
               notEmpty: {
                   message: 'The password is required and cannot be empty'
               },
               regexp:
            	{
            
           	regexp: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8}/,
   
           	message: 'The password should contain Minimum 8 and Maximum 12 characters at least 1 Uppercase Alphabet, 1 Lowercase Alphabet, 1 Number and 1 Special Character:'
   	   		},
   	 
   				different: {
   					field: 'oldpassword',
   					message: 'The password cannot be the same as Current Password'
   				}
           	}
       	},
    			oldpassword: {
                   validators: {
                       notEmpty: {
                           message: 'Please Enter Your Current Password'
   					},	
                       remote: {
                           message: 'The Password is not available',
                           url: '{{ URL::to('client/checkpassword1') }}',
                           data: {
                               type: 'oldpassword'
                           },
                           type: 'POST'
                       }																		
                   }
               },
               cpassword: {
                   validators: {  
   					notEmpty: {  
   						message: 'The confirm password is required and can\'t be empty'  
   					},  
   					identical: {  
   						field: 'newpassword',  
   						message: 'The password and its confirm are not the same'  
   					},  
   					different: {  
   						field: 'oldpassword',  
   						message: 'The password can\'t be the same as Old Password'  
   					}  
   				}  
               }
               }
 }).on('success.form.bv', function(e) {
               $('.changepassword').slideDown({ opacity: "show" }, "slow") // Do something ...
                   $('.changepassword').data('bootstrapValidator').resetForm();
               // Prevent form submission
               e.preventDefault();
               // Get the form instance
               var $form = $(e.target);
   
               // Get the BootstrapValidator instance
               var bv = $form.data('bootstrapValidator');
   
               // Use Ajax to submit form data
               $.post($form.attr('action'), $form.serialize(), function(result) {
                  // console.log(result);
               }, 'json');
           });
   });
</script>
<?php 
}
?>
<?php 
}
?>
@if(Auth::user()->active==0 )
<script type="text/javascript">
   $(document).ready(function(){
   $("#myModal").modal({
   		backdrop:'static',
   		keyword:'false',
   		show: true,
   	});
   	});	
</script>
<style type="text/css">
#registrationForm .tab-content {margin-top: 20px;}
.modal{z-index:99999999;}
</style>

<div id="myModal" class="modal fade">
   <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" style="font-size:30px;">×</button>
          </div>
         <div class="modal-body">
            @if(count($errors) > 0)
            @foreach($errors->all() as $error)
            <p class="alert alert-danger">{{ $error }}</p>
            @endforeach
            @endif

            <form enctype='multipart/form-data' action="{{url('https://financialservicecenter.net/client/home')}}/{{Auth::user()->id}}" id="registrationForm"  method="post">
               <!-- SmartWizard html -->
               {{csrf_field()}} {{method_field('PATCH')}}
<div id="smartwizard" >
               <ul class="nav nav-pills">
        <li class="active"><a href="#step-1" data-toggle="tab">Login Information</a></li>
        <li><a href="#step-2" data-toggle="tab">General Information</a></li>
        <li><a href="#step-3"  data-toggle="tab">Contact Information</a></li>
    </ul>
                  <br>
                  <div class="tab-content" style="max-height:450px; overflow:auto;">
                     <div class="tab-pane active" data-step="0" id="step-1">
                        <div id="form-step-0" role="form" data-toggle="validator">
                           @if ( session()->has('success') )
                           <div class="alert alert-success alert-dismissable">{{ session()->get('success') }}</div>
                           @endif
                           @if ( session()->has('error') )
                           <div class="alert alert-danger alert-dismissable">{{ session()->get('error') }}</div>
                           @endif 
<div class="form-group">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                 <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">		
                                    <label class="fsc-form-label">Username : </label>
                                 </div>
                                 <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                    <input type="email" readonly class="form-control fsc-input" id="email1" name="email" placeholder="abc@abc.com" value="{{Auth::user()->email}}" required>
                                    <div class="help-block with-errors"></div>
                                 </div>
                              </div>
                           </div>
                          <!-- <div class="form-group">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                 <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                    <label class="fsc-form-label" for="password">Current Password :<span class="star-required">*</span> </label>
                                 </div>
                                 <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                    <input type="password" class="form-control fsc-input" name="oldpassword" id="oldpassword" placeholder="Enter Your Current Password" required>		
                                    <div class="help-block with-errors"></div>
                                 </div>
                              </div>
                           </div>-->
                           <div class="form-group">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                 <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                    <label class="fsc-form-label" for="newpassword">New Password : <span class="star-required">*</span></label>
                                 </div>
                                 <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                    <input type="password" class="form-control fsc-input" name="newpassword" id="newpassword" placeholder="Enter Your New Password" required>
                                    <div class="help-block with-errors"></div>
                                    <div id="messages"></div>
                                 </div>
                              </div>
                           </div>
                         <div class="form-group">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                 <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                    <label class="fsc-form-label" for="newpassword">Confirm Password : <span class="star-required">*</span></label>
                                 </div>
                                 <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                    <input type="password" class="form-control fsc-input" name="cpassword" id="cpassword" placeholder="Enter Your Confirm Password" required>
                                   
                              </div>
                           </div>
                           </div>

                          <div class="form-group">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                 <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                    <label class="fsc-form-label" for="newpassword">Reset Days : <span class="star-required">*</span></label>
                                 </div>
                                 <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                     <select name="resetdays" value="" id="resetdays" class="form-control">
                                 <option>---Select Reset Days---</option>                                                      
                                 <option value="30" @if(Auth::user()->resetdays=='30') selected @endif>30</option>
                                 <option value="90" @if(Auth::user()->resetdays=='90') selected @endif>90</option>
                                 <option value="120" @if(Auth::user()->resetdays=='120') selected @endif>120</option>
                              </select>  
                                    <div class="help-block with-errors"></div>
                                 </div>
                                  <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                   <input name="reset_date" id="reset_date" value="{{Auth::user()->enddate}}" class="form-control fsc-input" readonly="">
                                 
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                 <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                    <label class="fsc-form-label" for="question1">Question 1 : <span class="star-required">*</span></label>
                                 </div>
                                 <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                    <select name="question1" id="question1" class="form-control fsc-input" required>
                                       <option value="">---Select---</option>
                                       <option value="What was your favorite place to visit as a child?">What was your favorite place to visit as a child?</option>
                                       <option value="Who is your favorite actor, musician, or artist?">Who is your favorite actor, musician, or artist?</option>
                                       <option value="What is the name of your favorite pet?">What is the name of your favorite pet?</option>
                                       <option value="In what city were you born?">In what city were you born?</option>
                                       <option value="What is the name of your first school?">What is the name of your first school?</option>
                                    </select>
                                    <div class="help-block with-errors"></div>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                 <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                    <label class="fsc-form-label" for="answer1">Answer 1 : <span class="star-required">*</span></label>
                                 </div>
                                 <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                    <input name="answer1" value="" required type="text" id="answer1" class="form-control fsc-input">
                                    <div class="help-block with-errors"></div>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                 <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                    <label class="fsc-form-label" for="question2">Question 2 : <span class="star-required">*</span></label>
                                 </div>
                                 <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                    <select name="question2" id="question2" required class="form-control fsc-input">
                                       <option value="">---Select---</option>
                                       <option value="What is your favorite movie?">What is your favorite movie?</option>
                                       <option value="What was the make of your first car?">What was the make of your first car?</option>
                                       <option value="What is your favorite color?">What is your favorite color?</option>
                                       <option value="What is your father's middle name?">What is your father's middle name?</option>
                                       <option value="What is the name of your first grade teacher?">What is the name of your first grade teacher?</option>
                                    </select>
                                    <div class="help-block with-errors"></div>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                 <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                    <label class="fsc-form-label" for="answer2">Answer 2 : <span class="star-required">*</span></label>
                                 </div>
                                 <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                    <input name="answer2" value="" required type="text" id="answer2" class="form-control fsc-input">
                                    <div class="help-block with-errors"></div>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                 <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                    <label class="fsc-form-label" for="question3">Question 3 : <span class="star-required">*</span></label>
                                 </div>
                                 <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                    <select name="question3" id="question3" required class="form-control fsc-input">
                                       <option value="">---Select---</option>
                                       <option value="What was your high school mascot?">What was your high school mascot?</option>
                                       <option value="Which is your favorite web browser?">Which is your favorite web browser?</option>
                                       <option value="In what year was your father born?">In what year was your father born?</option>
                                       <option value="What is the name of your favorite childhood friend?">What is the name of your favorite childhood friend?</option>
                                       <option value="What was your favorite food as a child?">What was your favorite food as a child?</option>
                                    </select>
                                    <div class="help-block with-errors"></div>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                 <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                    <label class="fsc-form-label" for="answer3">Answer 3 : <span class="star-required">*</span></label>
                                 </div>
                                 <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                    <input name="answer3" value="" required type="text" id="answer3" class="form-control fsc-input">
                                    <div class="help-block with-errors"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="tab-pane" id="step-2" data-step="2">
                        <div id="form-step-2" role="form" data-toggle="validator">
                           <div class="form-group">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                 <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">		
                                    <label class="fsc-form-label">Business Name : <span class="star-required">*</span></label>
                                 </div>
                                 <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                    <select name="business_id" id="business_id" disabled class="form-control fsc-input category">
                                         <option value="">---Select---</option>
                                       @foreach($business as $bus)
                                          <option value="{{$bus->id}}"  @if($common->business_id==$bus->id) selected @endif>{{$bus->bussiness_name}}</option>
                                       @endforeach
                                    </select>
                                    <div class="help-block with-errors"></div>
                                 </div>
                              </div>
                           </div>
                           @if($common->business_cat_id)
                           <div class="form-group">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                 <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">		
                                    <label class="fsc-form-label">Business Category  : <span class="star-required">*</span></label>
                                 </div>
                                 <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                    <select name="business_cat_id" id="business_cat_id" disabled class="form-control fsc-input category1">
                                      <option value="">---Select---</option>
                                       @foreach($category as $cat)
                                       <option value="{{$cat->id}}" @if($common->business_cat_id==$cat->id) selected @endif>{{$cat->business_cat_name}}</option>
                                       @endforeach	
                                    </select>
                                    <div class="help-block with-errors"></div>
                                 </div>
                              </div>
                           </div>
                           @endif
                           @if($common->business_brand_id)
                           <div class="form-group">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                 <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">		
                                    <label class="fsc-form-label">Business Brand : <span class="star-required">*</span></label>
                                 </div>
                                 <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                    <select name="business_brand_id" id="business_brand_id" disabled class="form-control fsc-input">
                                          <option value="">---Select---</option>
                                       @foreach($businessbrand as $brand)
                                       <option value="{{$brand->id}}" @if($common->business_brand_id==$brand->id) selected @endif>{{$brand->business_brand_name}}</option>
                                       @endforeach		                             
                                    </select>
                                    <div class="help-block with-errors"></div>
                                 </div>
                              </div>
                           </div>
                           @endif
                           @if($common->business_brand_category_id)
                           <div class="form-group">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                 <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">		
                                    <label class="fsc-form-label">Business Brand Category: <span class="star-required">*</span></label>
                                 </div>
                                 <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                    <select name="business_brand_category_id" id="business_brand_category_id" disabled class="form-control fsc-input">
                                          <option value="">---Select---</option>
                                       @foreach($cd as $cd1)
                                            <option value="{{$cd1->id}}"  @if($common->business_brand_category_id ==$cd1->id) selected @endif>{{$cd1->business_brand_category_name}}</option>
                                       @endforeach		                                
                                    </select>
                                    <div class="help-block with-errors"></div>
                                 </div>
                              </div>
                           </div>
                           @endif
                           <div class="form-group">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                 <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">		
                                    <label class="fsc-form-label">Company Name : <span class="star-required">*</span><span class="tess">(Legal Name)</span></label>
                                 </div>
                                 <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                    <input type="text" class="form-control fsc-input" id="company_name" name="company_name" value="{{$common->company_name}}" required placeholder="Company Name">
                                    <div class="help-block with-errors"></div>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                 <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">		
                                    <label class="fsc-form-label">Business Name : <span class="star-required">*</span><span class="tess">(DBA Name)</span></label>
                                 </div>
                                 <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                    <input type="text" class="form-control fsc-input" id="business_name" name="business_name" value="{{$common->business_name}}" placeholder="Business Name" required>
                                    <div class="help-block with-errors"></div>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-reg-sub-header-div" style="background-color: #dadada;margin: 20px 0;">
                              <h3 style="color: #333333;padding: 8px 0;margin: 0;font-size: 18px;">Business Location :</h3>
                           </div>
                          <!-- -->
                           <div class="form-group">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                 <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">		
                                    <label class="fsc-form-label">Address 1 : <span class="star-required">*</span><span class="tess" style="right: -12px;">(Physical Address)</span></label>
                                 </div>
                                 <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                    <input type="text" class="form-control fsc-input" id="address" name="address" placeholder="Enter Your Address" value="{{$common->address}}" required>
                                    <div class="help-block with-errors"></div>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                 <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">		
                                    <label class="fsc-form-label">Address 2 : <span class="tess" style="right: -12px; margin: 0 16px 0 0;">(Physical Address)</span></label>
                                 </div>
                                 <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                    <input type="text" class="form-control fsc-input" id="address1" name="address1" placeholder="Enter Your Address" value="{{$common->address1}}" >
                                    <div class="help-block with-errors"></div>
                                 </div>
                              </div>
                           </div>
                            <div class="form-group">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                 <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">		
                                    <label class="fsc-form-label">Country : <span class="star-required">*</span></label>
                                 </div>
                                 <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                    <select name="countryId" id="countryId" class="form-control fsc-input" required>
                                       <option value="IND" @if($common->countryId=='IND') selected @endif>IND</option>
                                       <option value="USA" @if($common->countryId=='USA') selected @endif>USA</option>
                                    </select>
                                    <div class="help-block with-errors"></div>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                 <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">		
                                    <label class="fsc-form-label">City / State / Zip : <span class="star-required">*</span></label>
                                 </div>
                                 <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                    <input type="text" class="form-control fsc-input" id="city" name="city" placeholder="City" value="{{$common->city}}" required>
                                    <div class="help-block with-errors"></div>
                                 </div>
                                 <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                    <select name="stateId" id="stateId" class="form-control fsc-input" required>
                                       <option value="{{$common->stateId}}" selected>{{$common->stateId}}</option>
                                       <option value="ID">ID</option>
                                       <option value="AK">AK</option>
                                       <option value="AS">AS</option>
                                       <option value="AZ">AZ</option>
                                       <option value="AR">AR</option>
                                       <option value="CA">CA</option>
                                       <option value="CO">CO</option>
                                       <option value="CT">CT</option>
                                       <option value="DE">DE</option>
                                       <option value="DC">DC</option>
                                       <option value="FM">FM</option>
                                       <option value="FL">FL</option>
                                       <option value="GA">GA</option>
                                       <option value="GU">GU</option>
                                       <option value="HI">HI</option>
                                       <option value="ID">ID</option>
                                       <option value="IL">IL</option>
                                       <option value="IN">IN</option>
                                       <option value="IA">IA</option>
                                       <option value="KS">KS</option>
                                       <option value="KY">KY</option>
                                       <option value="LA">LA</option>
                                       <option value="ME">ME</option>
                                       <option value="MH">MH</option>
                                       <option value="MD">MD</option>
                                       <option value="MA">MA</option>
                                       <option value="MI">MI</option>
                                       <option value="MN">MN</option>
                                       <option value="MS">MS</option>
                                       <option value="MO">MO</option>
                                       <option value="MT">MT</option>
                                       <option value="NE">NE</option>
                                       <option value="NV">NV</option>
                                       <option value="NH">NH</option>
                                       <option value="NJ">NJ</option>
                                       <option value="NM">NM</option>
                                       <option value="NY">NY</option>
                                       <option value="NC">NC</option>
                                       <option value="ND">ND</option>
                                       <option value="MP">MP</option>
                                       <option value="OH">OH</option>
                                       <option value="OK">OK</option>
                                       <option value="OR">OR</option>
                                       <option value="PW">PW</option>
                                       <option value="PA">PA</option>
                                       <option value="PR">PR</option>
                                       <option value="RI">RI</option>
                                       <option value="SC">SC</option>
                                       <option value="SD">SD</option>
                                       <option value="TN">TN</option>
                                       <option value="TX">TX</option>
                                       <option value="UT">UT</option>
                                       <option value="VT">VT</option>
                                       <option value="VI">VI</option>
                                       <option value="VA">VA</option>
                                       <option value="WA">WA</option>
                                       <option value="WV">WV</option>
                                       <option value="WI">WI</option>
                                       <option value="WY">WY</option>
                                    </select>
                                    <div class="help-block with-errors"></div>
                                 </div>
                                 <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                    <input type="text" class="form-control fsc-input" maxlength="5" id="zip" name="zip" placeholder="Postel Code" value="{{$common->zip}}" required>
                                    <div class="help-block with-errors"></div>
                                 </div>
                              </div>
                           </div>
                          
                          <div class="form-group">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                 <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">		
                                    <label class="fsc-form-label">Mailing Address : </label>
                                 </div>
                                 <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                   <label style="text-align:left!important;"><input type="checkbox" class="" name="billingtoo" onclick="FillBilling(this.form)"> Same As Above Address</label>
                                 </div>
                              </div>
                           </div>
                            <div class="form-group">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                 <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">		
                                    <label class="fsc-form-label">Mailing Address 1 : </label>
                                 </div>
                                 <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                    <input type="text" class="form-control fsc-input" id="mailing_address" name="mailing_address" placeholder="Enter Your Address" value="{{$common->address}}" required>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                 <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">		
                                    <label class="fsc-form-label">Mailing Address 2 :</label>
                                 </div>
                                 <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                    <input type="text" class="form-control fsc-input" id="mailing_address1" name="mailing_address1" placeholder="Enter Your Address" value="{{$common->address1}}">
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                 <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">		
                                    <label class="fsc-form-label">City / State / Zip : </label>
                                 </div>
                                 <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                    <input type="text" class="form-control fsc-input" id="mailing_city" name="mailing_city" placeholder="City" value="{{$common->city}}">
                                    <div class="help-block with-errors"></div>
                                 </div>
                                 <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                    <select name="mailing_state" id="mailing_state" class="form-control fsc-input">
                                       <option value="{{$common->stateId}}" selected>{{$common->stateId}}</option>
                                       <option value="ID">ID</option>
                                       <option value="AK">AK</option>
                                       <option value="AS">AS</option>
                                       <option value="AZ">AZ</option>
                                       <option value="AR">AR</option>
                                       <option value="CA">CA</option>
                                       <option value="CO">CO</option>
                                       <option value="CT">CT</option>
                                       <option value="DE">DE</option>
                                       <option value="DC">DC</option>
                                       <option value="FM">FM</option>
                                       <option value="FL">FL</option>
                                       <option value="GA">GA</option>
                                       <option value="GU">GU</option>
                                       <option value="HI">HI</option>
                                       <option value="ID">ID</option>
                                       <option value="IL">IL</option>
                                       <option value="IN">IN</option>
                                       <option value="IA">IA</option>
                                       <option value="KS">KS</option>
                                       <option value="KY">KY</option>
                                       <option value="LA">LA</option>
                                       <option value="ME">ME</option>
                                       <option value="MH">MH</option>
                                       <option value="MD">MD</option>
                                       <option value="MA">MA</option>
                                       <option value="MI">MI</option>
                                       <option value="MN">MN</option>
                                       <option value="MS">MS</option>
                                       <option value="MO">MO</option>
                                       <option value="MT">MT</option>
                                       <option value="NE">NE</option>
                                       <option value="NV">NV</option>
                                       <option value="NH">NH</option>
                                       <option value="NJ">NJ</option>
                                       <option value="NM">NM</option>
                                       <option value="NY">NY</option>
                                       <option value="NC">NC</option>
                                       <option value="ND">ND</option>
                                       <option value="MP">MP</option>
                                       <option value="OH">OH</option>
                                       <option value="OK">OK</option>
                                       <option value="OR">OR</option>
                                       <option value="PW">PW</option>
                                       <option value="PA">PA</option>
                                       <option value="PR">PR</option>
                                       <option value="RI">RI</option>
                                       <option value="SC">SC</option>
                                       <option value="SD">SD</option>
                                       <option value="TN">TN</option>
                                       <option value="TX">TX</option>
                                       <option value="UT">UT</option>
                                       <option value="VT">VT</option>
                                       <option value="VI">VI</option>
                                       <option value="VA">VA</option>
                                       <option value="WA">WA</option>
                                       <option value="WV">WV</option>
                                       <option value="WI">WI</option>
                                       <option value="WY">WY</option>
                                    </select>
                                 </div>
                                 <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                    <input type="text" class="form-control fsc-input" id="mailing_zip" name="mailing_zip" placeholder="Postel Code" value="{{$common->zip}}">
                                 </div>
                              </div>
                           </div>
                  
                           <div class="form-group">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                 <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">		
                                    <label class="fsc-form-label">Telephone : <span class="star-required">*</span></label>
                                 </div>
                                 <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                    <input type="text" class="form-control fsc-input phone" id="business_no" value="{{$common->business_no}}" name="business_no" placeholder="(999) 999-9999" required>
                                 </div>
                                  <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                   <select class="form-control fsc-input" id="businesstype" name="businesstype">
                                     <option value="">Type</option>
                                     <option value="Office" selected="">Office</option>
                                     <option value="Mobile">Mobile</option>
                                     <option value="Resid">Res</option>
                                    </select>
                                 </div>
                                 <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                   <input readonly="" class="form-control fsc-input" maxlength="5" id="businessext" name="businessext" placeholder="Ext" type="text" value="">
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                 <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">		
                                    <label class="fsc-form-label">Business Fax : </label>
                                 </div>
                                 <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                    <input type="text" class="form-control fsc-input phone" value="{{$common->business_fax}}" id="business_fax" name="business_fax" placeholder="(999) 999-9999">		
                                 </div>
                                
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                 <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">		
                                    <label class="fsc-form-label">Web Address : </label>
                                 </div>
                                 <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                    <input type="text" class="form-control fsc-input" id="website" value="{{$common->website}}" name="website"  placeholder="Website address">		
                               </div>
                              </div>
                           </div>
                           <select style="visibility:hidden;" name="user_type" id="user_type" class="form-control fsc-input category1">
                              <option value='{{$common->user_type}}' selected>{{$common->user_type}}</option>
                           </select>
                        </div>
                     </div>
                     <div class="tab-pane" id="step-3" data-step="3">
                     <div id="form-step-3" role="form" data-toggle="validator">
                        <div class="form-group">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">		
                                    <label class="fsc-form-label">Name : </label>
                                 </div>
                                 <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                    <select type="text" class="form-control fsc-input" id="nametype" name="nametype">
                                                <option value="mr" @if($common->nametype=='mr') selected="" @endif>Mr.</option>
                                                <option value="mrs" @if($common->nametype=='mrs') selected="" @endif>Mrs.</option>
                                                <option value="miss" @if($common->nametype=='miss') selected="" @endif>Miss.</option>
                                     </select>
                                 </div>
                                  <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                    <input type="text" class="form-control fsc-input" id="first_name1" name="first_name" placeholder="First Name" value="{{$common->first_name}}" required>
                                    <div class="help-block with-errors"></div>
                                 </div>
                                 <div class="col-lg-1 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                    <div class="row">
                                    <input type="text" class="form-control fsc-input" id="middle_name1" name="middle_name" placeholder="Middle Name" value="{{$common->middle_name}}">
                                    </div>
                                 </div>
                                 <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                    <input type="text" class="form-control fsc-input" id="last_name1" name="last_name" placeholder="Last Name" value="{{$common->last_name}}" required>
                                    <div class="help-block with-errors"></div>
                                 </div>
                              </div>
                           </div>
                            <div class="form-group">
                                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <label class="control-label col-md-4">Address 1 :</label>
                                          <div class="col-md-8">
                                             <input type="text" class="form-control" id="contact_address1" name="contact_address1" value="{{$common->contact_address1}}">				
                                          </div>
                                       </div>
                                       </div>
                                       <div class="form-group" style="margin-top:2%;">
                                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                          <label class="control-label col-md-4">Address 2 :</label>
                                          <div class="col-md-8">
                                             <input type="text" class="form-control" id="contact_address2" name="contact_address2" value="{{$common->contact_address2}}">				
                                            </div>
                                         </div>
                                       </div>
                                       <div class="form-group">
                                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                          <label class="control-label col-md-4">City / State / Zip :</label>
                                          <div class="col-md-3">
                                             <input name="city_1" value="{{$common->city_1}}" type="text" id="city_1" class="txtOnly form-control">
                                            </div>
                                          <div class="">
                                             <div class="col-md-3">
                                                <select name="state" id="stateId1" class="form-control fsc-input">
                                                   <option value="GA">GA</option>
                                                   <option value="AK">AK</option>
                                                   <option value="AS">AS</option>
                                                   <option value="AZ">AZ</option>
                                                   <option value="AR">AR</option>
                                                   <option value="CA">CA</option>
                                                   <option value="CO">CO</option>
                                                   <option value="CT">CT</option>
                                                   <option value="DE">DE</option>
                                                   <option value="DC">DC</option>
                                                   <option value="FM">FM</option>
                                                   <option value="FL">FL</option>
                                                   <option value="GA">GA</option>
                                                   <option value="GU">GU</option>
                                                   <option value="HI">HI</option>
                                                   <option value="ID">ID</option>
                                                   <option value="IL">IL</option>
                                                   <option value="IN">IN</option>
                                                   <option value="IA">IA</option>
                                                   <option value="KS">KS</option>
                                                   <option value="KY">KY</option>
                                                   <option value="LA">LA</option>
                                                   <option value="ME">ME</option>
                                                   <option value="MH">MH</option>
                                                   <option value="MD">MD</option>
                                                   <option value="MA">MA</option>
                                                   <option value="MI">MI</option>
                                                   <option value="MN">MN</option>
                                                   <option value="MS">MS</option>
                                                   <option value="MO">MO</option>
                                                   <option value="MT">MT</option>
                                                   <option value="NE">NE</option>
                                                   <option value="NV">NV</option>
                                                   <option value="NH">NH</option>
                                                   <option value="NJ">NJ</option>
                                                   <option value="NM">NM</option>
                                                   <option value="NY">NY</option>
                                                   <option value="NC">NC</option>
                                                   <option value="ND">ND</option>
                                                   <option value="MP">MP</option>
                                                   <option value="OH">OH</option>
                                                   <option value="OK">OK</option>
                                                   <option value="OR">OR</option>
                                                   <option value="PW">PW</option>
                                                   <option value="PA">PA</option>
                                                   <option value="PR">PR</option>
                                                   <option value="RI">RI</option>
                                                   <option value="SC">SC</option>
                                                   <option value="SD">SD</option>
                                                   <option value="TN">TN</option>
                                                   <option value="TX">TX</option>
                                                   <option value="UT">UT</option>
                                                   <option value="VT">VT</option>
                                                   <option value="VI">VI</option>
                                                   <option value="VA">VA</option>
                                                   <option value="WA">WA</option>
                                                   <option value="WV">WV</option>
                                                   <option value="WI">WI</option>
                                                   <option value="WY">WY</option>
                                                </select>
                                               </div>
                                          </div>
                                          <div class="">
                                             <div class="col-md-2">
                                                <input name="zip_1" value="{{$common->zip_1}}" type="text" id="zip_1" class="form-control">
                                             </div>
                                          </div>
                                          </div>
                                       </div>
                                       <div class="form-group">
                                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                          <label class="control-label col-md-4">Telephone No. 1:</label>
                                          <div class="col-md-3">
                                             <input name="mobile_1" placeholder="(999) 999-9999" value="{{$common->etelephone1}}" type="tel" id="mobile_1" class="form-control phone">
                                           </div>
                                          <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin ">
                                             <select name="mobiletype_1" id="mobiletype_1" class="form-control fsc-input" style="height:auto">
                                                <option value="Home" @if($common->eteletype1=='Home') selected @endif>Office</option>
                                                <option value="Mobile" @if($common->eteletype1=='Mobile') selected @endif>Mobile</option>
                                                <option value="Other" @if($common->eteletype1=='Other') selected @endif>Other</option>
                                             </select>
                                              </div>
                             <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                              <input class="form-control fsc-input" id="ext2_1" maxlength="5" name="ext2_1" readonly value="{{$common->eext1}}" placeholder="Ext" type="text">
                            </div>
                                </div>
                                </div>
                               <div class="form-group">
                                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                          <label class="control-label col-md-4">Telephone No. 2:</label>
                                          <div class="col-md-3">
                                             <input name="mobile_2" placeholder="(999) 999-9999" value="{{$common->etelephone2}}" type="tel" id="mobile_2" class="form-control phone">
                                          </div>
                                          <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin ">
                                             <select name="mobiletype_2" id="mobiletype_2" class="form-control fsc-input" style="height:auto">
                                                <option value="Home" @if($common->eteletype2	=='Home') selected @endif>Office</option>
                                                <option value="Mobile" @if($common->eteletype2	=='Mobile') selected @endif>Mobile</option>
                                                <option value="Other" @if($common->eteletype2 =='Other') selected @endif>Other</option>
                                             </select>
                                          </div>
                              <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                              <input class="form-control fsc-input" readonly id="ext2_2" maxlength="5" name="ext2_2" value="{{$common->eext2}}" placeholder="Ext" type="text">
                            </div>
                                       </div>
                                       <div class="form-group">
                                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                          <label class="control-label col-md-4">Fax No. :</label>
                                          <div class="col-md-3">
                                             <input name="contact_fax_1" placeholder="(999) 999-9999" value="{{$common->contact_fax_1}}" type="tel" id="contact_fax_1" class="phone form-control">
                                             </div>
                                       </div>
                                    </div>
                              <div class="form-group">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                 <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">		
                                    <label class="fsc-form-label">Email : </label>
                                 </div>
                                 <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                    <input type="email" readonly class="form-control fsc-input" id="email" name="email" placeholder="abc@abc.com" value="{{$common->email}}">
                                    <div class="help-block with-errors"></div>
                                 </div>
                              </div>
                           </div>
                         </div>
                           <div class="form-group">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;margin-bottom:2%;">
                           <input type="submit" value="Save" class="pull-right btn btn-success" style="margin-right:15px;">
                           </div>
                           </div>                           
                        </div>
                     </div>
                  </div>
               </div>
               <ul class="pager wizard">
                    <li class="previous first" style="display:none;"><a href="#">First</a></li>
                    <li class="previous"><a href="#">Previous</a></li>
                    <li class="next last" style="display:none;"><a href="#">Last</a></li>
                    <li class="next"><a href="#" style="background:#49ddff; color:#fff;">Next</a></li>
                   <!-- <li  class="btn btn-primary" style="float: right;padding: 2px 6px;"><a style="background-color: transparent;border: none;color:#fff;" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><span>Close</span></a></li> -->
               </ul>
            </form>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript" src="{{URL::asset('public/dashboard/js/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('public/dashboard/js/bootstrap.validation.min.js')}}"></script>  
<script  type="text/javascript"src="{{URL::asset('public/dashboard/js/jquery.bootstrap.wizard.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('public/frontcss/js/bootstrap-formhelpers.min.js')}}"></script>
<script type="text/javascript">
$.ajaxSetup({
    headers:
    { 'X-CSRF-Token': $('input[name="_token"]').val();}
});
  $(document).ready(function() {
    function adjustIframeHeight() {
      var $body   = $('body'),
          $iframe = $body.data('iframe.fv');
      if ($iframe) {
          // Adjust the height of iframe
          $iframe.height($body.height());
      }
  }

    $('#registrationForm').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        excluded: ':disabled',
		fields: {

            
            personname: {
        validators: {
            notEmpty: {
                message: 'Please Enter Person Name'
            },            
        }
    },
    relation: {
        validators: {
            notEmpty: {
                message: 'Please Enter Relation'
            },            
        }
    },
    eaddress1: {
        validators: {
            notEmpty: {
                message: 'Please Enter Address'
            },            
        }
    },
    per_city: {
        validators: {
            notEmpty: {
                message: 'Please Enter City'
            }, 
            regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'The City can consist of alphabetical characters and spaces only'
                    }           
        }
    },
    per_stateId: {
        validators: {
            notEmpty: {
                message: 'Please Select State'
            },            
        }
    },
    per_zip: {
        validators: {
            notEmpty: {
                message: 'Please Select Country'
            },            
        }
    },
    comments: {
        validators: {
            notEmpty: {
                message: 'Please Enter Comments'
            },            
        }
    },
    etelephone1: {
        validators: {
            
            notEmpty: {
                message: 'Please Enter Phone No.'
            }, 
            
            etelephone1: {
                        country: 'USA',
                        message: 'Please supply a vaild phone number with area code'
                    },           
        }
    },
    eteletype1: {
        validators: {
            notEmpty: {
                message: 'Please Enter Phone Type'
            },            
        }
    },
    newpassword: {
        validators: {
            notEmpty: {
                message: 'The password is required and cannot be empty'
            },
            regexp:
       {
         
         regexp: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8}/,

        message: 'The password should contain Minimum 8 and Maximum 12 characters at least 1 Uppercase Alphabet, 1 Lowercase Alphabet, 1 Number and 1 Special Character:'
       },
           
        }
    },
 
cpassword: {
                   validators: {  
   					notEmpty: {  
   						message: 'The confirm password is required and can\'t be empty'  
   					},  
   					identical: {  
   						field: 'newpassword',  
   						message: 'The password and its confirm are not the same'  
   					},  
   			 
   				}  
               },
            pf1: {
                validators: {
                    notEmpty: {
                        message: 'Please upload Your Proof'
					},	
                    pf1: {
                        extension: 'doc,docx,pdf,zip,rtf',
                        type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf,application/zip',
                        maxSize: 5*1024*1024,
                        message: 'The selected file is not valid, it should be (doc,docx,pdf,zip,rtf) and 5 MB at maximum.'
                    }						
					
						
                }
            },
           
    question1: {
        validators: {
            notEmpty: {
                message: 'Please Select Your Question 1'
            }
        }
    },
    question2: {
        validators: {
            notEmpty: {
                message: 'Please Select Your Question 2'
            }
        }
    },
    question3: {
        validators: {
            notEmpty: {
                message: 'Please Select Your Question 3'
            }
        }
    },
    answer1: {
        validators: {
            notEmpty: {
                message: 'Please Select Your Answer 1'
            }
        }
    },
    answer2: {
        validators: {
            notEmpty: {
                message: 'Please Select Your Answer 2'
            }
        }
    },
    answer3: {
        validators: {
            notEmpty: {
                message: 'Please Select Your Answer 3'
            }
        }
    },
    business_id: {
        validators: {
            message: 'Please Select Your Business Name',
        }
    },
    business_cat_id: {
        validators: {
            message: 'Please Select Your Business Category Name',
        }
    },
    business_brand_id: {
        validators: {
            message: 'Please Select Your Business Brand Name',
        }
    },
    business_brand_category_id: {
        validators: {
            message: 'Please Select Your Business Brand Category Name',
        }
    },
    company_name: {
        validators: {
            message: 'Please Select Your Company Name',
        }
    },
    business_name: {
        validators: {
            message: 'Please Select Your Business Name',
        }
    },
            first_name1: {
                validators: {
                        stringLength: {
                        min: 2,
                    },
                        notEmpty: {
                        message: 'Please Enter Your First Name'
					},
					regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'The First Name can consist of alphabetical characters and spaces only'
                    }
                }
            },
		
             last_name1: {
                validators: {
                     stringLength: {
                        min: 2,
                    },
                    notEmpty: {
                        message: 'Please Enter Your Last Name'
					},
					
					regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'The Last name can consist of alphabetical characters and spaces only'
                    }
                }
            },
            
            address: {
                validators: {
                 
                    notEmpty: {
                        message: 'Please Enter Your Address'
                    }
                }
            },
			
            city: {
                validators: {
                     stringLength: {
						min: 2,
						
                    },
                    notEmpty: {
                        message: 'Please Enter Your City'
                    },
					regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'The City can consist of alphabetical characters and spaces only'
                    }
                }
            },
            stateId: {
                validators: {
                    notEmpty: {
                        message: 'Please Select Your State'
                    }
                }
            },
			countryId: {
                validators: {
                    notEmpty: {
                        message: 'Please Select Your Country'
                    }
                }
            },
            zip: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Your Zip Code'
                    }					
                }
            },
			business_no: {
                validators: {
                  
                    notEmpty: {
                        message: 'Please Enter Your Business Phone Number'
                    },
                    business_no: {
                        country: 'USA',
                        message: 'Please supply a vaild phone number with area code'
                    }
                }
            },

			        
            }
        }).bootstrapWizard({
            tabClass: 'nav nav-pills',
            onTabClick: function(tab, navigation, index) {
                return validateTab(index);
            },
            onNext: function(tab, navigation, index) {
                var numTabs    = $('#registrationForm').find('.tab-pane').length,
                    isValidTab = validateTab(index - 1);
                if (!isValidTab) {
                    return false;
                }
      
                if (index === numTabs) {
                    $('#completeModal').modal();
                }
      
                return true;
            },
            onPrevious: function(tab, navigation, index) {
                return validateTab(index + 1);
            },
            onTabShow: function(tab, navigation, index) {
                // Update the label of Next button when we are at the last tab
                var numTabs = $('#registrationForm').find('.tab-pane').length;
                $('#registrationForm')
                    .find('.next')
                        .removeClass('disabled')    // Enable the Next button
                        .find('a')
                        .html(index === numTabs - 1 ? 'End' : 'Next');
      
                // You don't need to care about it
                // It is for the specific demo
                adjustIframeHeight();
            }
        });
      
      function validateTab(index) {
        var fv   = $('#registrationForm').data('formValidation'), // FormValidation instance
            // The current tab
            $tab = $('#registrationForm').find('.tab-pane').eq(index);
      
        // Validate the container
        fv.validateContainer($tab);
      
        var isValidStep = fv.isValidContainer($tab);
        if (isValidStep === false || isValidStep === null) {
            // Do not jump to the target tab
            return false;
        }
      
        return true;
      }
      });
</script>
<script>
    $(document).ready(function(){
        $('#resetdays').on('change',function(){
           var reset = parseInt($('#resetdays').val()); 
           var date = new Date();
         var t = new Date(); 
		//var n = $("#resetdays").val(); 
		//alert(offset);
		t.setDate(t.getDate() + reset);
		var month = "0"+(t.getMonth()+1);
		var date = "0"+t.getDate();
		
		const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "June","July", "Aug", "Sept", "Oct", "Nov", "Dec"];
		const d = new Date();
		
		month = month.slice(-2);
		date = date.slice(-2);
		//var date = date +"/"+month+"/"+t.getFullYear();
		var date = monthNames[t.getMonth()]+"-"+date+"-"+t.getFullYear();
           $('#reset_date').val(date);
        });
    });
</script>
<script>
   $(document).ready(function(){
   	$(document).on('change','.category', function()
   	{
   		//console.log('htm');
   		var id = $(this).val();
   		$.get('{!!URL::to('getcat1')!!}?id='+id, function(data)
   		{  $('#user_type').empty();
              $.each(data, function(index, subcatobj)
   		   {
   			   $('#user_type').append('<option value="'+subcatobj.bussiness_name+'">'+subcatobj.bussiness_name+'</option>');
   		   })
   
   		});
   			
   	});
   });
</script>
<script>
   $(document).ready(function(){
   	$(document).on('change','.category', function()
   	{ 
   		//console.log('htm');
   		var id = $(this).val();//alert(id);
   		$.get('{!!URL::to('getRequest2')!!}?id='+id, function(data)
   		{  
   			$('#business_cat_id').empty();
              $.each(data, function(index, subcatobj)
   		   {
   			   $('#business_cat_id').append('<option value="'+subcatobj.id+'">'+subcatobj.business_cat_name+'</option>');
   		   })
   
   		});
   			
   	});
   });
</script>
<script>
$(function() {
    $('#eext1').hide(); 
    $('#eteletype1').change(function(){
        if($('#eteletype1').val() == 'Office') {
            $('#eext1').show(); 
            $('#eext5').hide();
        } else {
            $('#eext1').hide(); 
            $('#eext5').show(); 
        } 
    });
});
</script>
<script>
$(function() {
    $('#eext2').hide(); 
    $('#eteletype2').change(function(){
        if($('#eteletype2').val() == 'Office') {
            $('#eext2').show(); 
            $('#eext6').hide();
        } else {
            $('#eext2').hide(); 
            $('#eext6').show(); 
        } 
    });
});
</script>
@endif
@endguest