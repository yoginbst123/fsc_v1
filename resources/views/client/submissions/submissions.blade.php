@extends('client.layouts.app')
@section('title', 'Submissions')
@section('main-content')

<style>
    .main-box {
    background-color: #fff;
    padding: 4px 4px;
    margin-bottom: 10px;
}
.submission-list-box-bg .box3 {
    background: #c5dbe8;
}
.submission-list-box-bg .box3 h2 {
    padding: 32px 7px 24px;
}
.box3 h2 {
    display: inline-block;
    background-color: #1b6fa4;
    color: #fff;
    margin-top: 0;
    margin-bottom: 0;
    padding: 25px 7px 17px;
    font-weight: 600;
    font-size: 37px;
    border-top-left-radius: 10px;
    border-top-right-radius: 10px;
    border-right: 5px solid;
}
.information-btn {
    width: 100%;
    background-color: #0d4680;
    border-color: #0d4680;
}
.submission-list-box-bg .main-image {
    width: 169px;
    float: right;
    margin: 10px 3px 11px 0;
    background: #fff;
    padding-left: 10px;
    border-radius: 28px 0 0 28px;
    padding-top: 3px;
    padding-bottom: 3px;
}
.imgicon {
    background: #fff;
    display: block;
    margin-top: -6px;
    width: 35px;
    height: 35px;
    float: left;
    margin-right: 10px;
    float: left;
    margin-right: 10px;
    border-radius: 2px;
    padding: 3px;
    border: 1px solid #12186b;
}
.imgicon img {
    max-width: 100%;
    height: 27px;
    margin: 0px auto;
    display: block;
}
</style>
<div class="content-wrapper">
      <section class="content-header">
      <h1 style="display:flex; flex-direction:row; justify-content:space-between"> <span class="imgicon"> <img src="https://www.financialservicecenter.net/public/images/Submission-05-05.png" alt="img"></span> <span>Submission</span> <span>View/Edit</span></h1>
    </section>

<div class="row">
	
		<div class="col-md-12">
			<div class="box box-success">
			      <div class="box-header">
              
              <div class="box-tools pull-right">
                
              </div>
            </div>
				<div class="col-md-12">
					                      
                    
                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 submission-list-box-bg">
<div class="main-box">
<div class="box3" style="border-top: none;margin-bottom: 2px;">

              <h2>0</h2>
			
				<img src="https://financialservicecenter.net/public/submission/RequestInfo.png" alt="" class="main-image">
					<a href="https://financialservicecenter.net/fac-Bhavesh-0554/submissionrequest" class="btn btn-info information-btn">More info</a>
				</div>
</div>
			</div>
			<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 submission-list-box-bg">
<div class="main-box">
<div class="box3" style="border-top: none;margin-bottom: 2px;">

              <h2>0</h2>
			
				<img src="https://financialservicecenter.net/public/submission/TaxIssueReporting.png" alt="" class="main-image">
					<a href="https://financialservicecenter.net/fac-Bhavesh-0554/submissionrequest" class="btn btn-info information-btn">More info</a>
				</div>
</div>
			</div>
			<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 submission-list-box-bg">
<div class="main-box">
<div class="box3" style="border-top: none;margin-bottom: 2px;">

              <h2>0</h2>
			
				<img src="https://financialservicecenter.net/public/submission/SaleTaxReporting.png" alt="" class="main-image">
					<a href="https://financialservicecenter.net/fac-Bhavesh-0554/submissionrequest" class="btn btn-info information-btn">More info</a>
				</div>
</div>
			</div>
			<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 submission-list-box-bg">
<div class="main-box">
<div class="box3" style="border-top: none;margin-bottom: 2px;">

              <h2>0</h2>
			
				<img src="https://financialservicecenter.net/public/submission/PayrollReporting.png" alt="" class="main-image">
					<a href="https://financialservicecenter.net/fac-Bhavesh-0554/submissionrequest" class="btn btn-info information-btn">More info</a>
				</div>
</div>
			</div>
			<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 submission-list-box-bg">
<div class="main-box">
<div class="box3" style="border-top: none;margin-bottom: 2px;">

              <h2>0</h2>
			
				<img src="https://financialservicecenter.net/public/submission/EmployeeTimeReporting.png" alt="" class="main-image">
					<a href="https://financialservicecenter.net/fac-Bhavesh-0554/submissionrequest" class="btn btn-info information-btn">More info</a>
				</div>
</div>
			</div>
			<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 submission-list-box-bg">
<div class="main-box">
<div class="box3" style="border-top: none;margin-bottom: 2px;">

              <h2>0</h2>
			
				<img src="https://financialservicecenter.net/public/submission/TobaccoTax.png" alt="" class="main-image">
					<a href="https://financialservicecenter.net/fac-Bhavesh-0554/submissionrequest" class="btn btn-info information-btn">More info</a>
				</div>
</div>
			</div>
			<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 submission-list-box-bg">
<div class="main-box">
<div class="box3" style="border-top: none;margin-bottom: 2px;">

              <h2>0</h2>
			
				<img src="https://financialservicecenter.net/public/submission/BankStatement.png" alt="" class="main-image">
					<a href="https://financialservicecenter.net/fac-Bhavesh-0554/submissionrequest" class="btn btn-info information-btn">More info</a>
				</div>
</div>
			</div>
			<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 submission-list-box-bg">
<div class="main-box">
<div class="box3" style="border-top: none;margin-bottom: 2px;">

              <h2>0</h2>
				
				<img src="https://financialservicecenter.net/public/submission/CredditCard.png" alt="" class="main-image">
					<a href="https://financialservicecenter.net/fac-Bhavesh-0554/submissionrequest" class="btn btn-info information-btn">More info</a>
				</div>
</div>
			</div>
			<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 submission-list-box-bg">
<div class="main-box">
<div class="box3" style="border-top: none;margin-bottom: 2px;">

              <h2>0</h2>
				
				<img src="https://financialservicecenter.net/public/submission/Fuel.png" alt="" class="main-image">
					<a href="https://financialservicecenter.net/fac-Bhavesh-0554/submissionrequest" class="btn btn-info information-btn">More info</a>
				</div>
</div>
			</div>
			<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 submission-list-box-bg">
<div class="main-box">
<div class="box3" style="border-top: none;margin-bottom: 2px;">

              <h2>0</h2>
			
				<img src="https://financialservicecenter.net/public/submission/Licsense-BT.png" alt="" class="main-image">
					<a href="https://financialservicecenter.net/fac-Bhavesh-0554/submissionrequest" class="btn btn-info information-btn">More info</a>
				</div>
</div>
			</div>
			<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 submission-list-box-bg">
<div class="main-box">
<div class="box3" style="border-top: none;margin-bottom: 2px;">

              <h2>0</h2>
				
				<img src="https://financialservicecenter.net/public/submission/Loan.png" alt="" class="main-image">
					<a href="https://financialservicecenter.net/fac-Bhavesh-0554/submissionrequest" class="btn btn-info information-btn">More info</a>
				</div>
</div>
			</div>
			<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 submission-list-box-bg">
<div class="main-box">
<div class="box3" style="border-top: none;margin-bottom: 2px;">

              <h2>0</h2>
				
				<img src="https://financialservicecenter.net/public/submission/Other.png" alt="" class="main-image">
					<a href="https://financialservicecenter.net/fac-Bhavesh-0554/submissionrequest" class="btn btn-info information-btn">More info</a>
				</div>
</div>
			</div>
			                    
                    
					<div class="table-responsive">
						<div id="sampleTable3_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer"><div class="dt-buttons">   <button class="dt-button buttons-excel buttons-html5" tabindex="0" aria-controls="sampleTable3"><span><i class="fa fa-file-excel-o"></i>&nbsp; Excel</span></button> <button class="dt-button buttons-pdf buttons-html5" tabindex="0" aria-controls="sampleTable3"><span><i class="fa fa-file-pdf-o"></i>&nbsp;  PDF</span></button> <button class="dt-button buttons-print" tabindex="0" aria-controls="sampleTable3"><span><i class="fa fa-print"></i>&nbsp; Print</span></button> </div><div id="sampleTable3_filter" class="dataTables_filter"><label>Search:<input type="search" class="form-control input-sm" placeholder="" aria-controls="sampleTable3"></label></div><table class="table table-hover table-bordered dataTable no-footer" id="sampleTable3" role="grid" aria-describedby="sampleTable3_info">
							<thead>
								<tr role="row"><th class="sorting_asc" tabindex="0" aria-controls="sampleTable3" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Date: activate to sort column descending" style="width: 31px;">Date</th><th class="sorting" tabindex="0" aria-controls="sampleTable3" rowspan="1" colspan="1" aria-label="Service Name: activate to sort column ascending" style="width: 50px;">Service Name</th><th class="sorting" tabindex="0" aria-controls="sampleTable3" rowspan="1" colspan="1" aria-label="Name: activate to sort column ascending" style="width: 39px;">Name</th><th class="sorting" tabindex="0" aria-controls="sampleTable3" rowspan="1" colspan="1" aria-label="Email: activate to sort column ascending" style="width: 38px;">Email</th><th class="sorting" tabindex="0" aria-controls="sampleTable3" rowspan="1" colspan="1" aria-label="Telephone: activate to sort column ascending" style="width: 69px;">Telephone</th><th class="sorting" tabindex="0" aria-controls="sampleTable3" rowspan="1" colspan="1" aria-label="Action: activate to sort column ascending" style="width: 44px;">Action</th></tr>
							</thead>
							<tbody>
						
                             <tr class="odd"><td valign="top" colspan="6" class="dataTables_empty">No data available in table</td></tr></tbody>
						</table><div class="dataTables_info" id="sampleTable3_info" role="status" aria-live="polite">Showing 0 to 0 of 0 entries</div><div class="dataTables_paginate paging_simple_numbers" id="sampleTable3_paginate"><ul class="pagination"><li class="paginate_button previous disabled" id="sampleTable3_previous"><a href="#" aria-controls="sampleTable3" data-dt-idx="0" tabindex="0">Previous</a></li><li class="paginate_button next disabled" id="sampleTable3_next"><a href="#" aria-controls="sampleTable3" data-dt-idx="1" tabindex="0">Next</a></li></ul></div></div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	
</div>
@endsection()