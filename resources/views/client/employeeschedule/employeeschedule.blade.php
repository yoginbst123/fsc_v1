@extends('client.layouts.app')
@section('title', 'Employee Schedule')
@section('main-content') 
<style>
.datepicker{
    z-index:99999 !important;
}
.modal-dialog { width: 82%;margin: 30px auto;}
.table-bordered, .table-bordered > tbody > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > td, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > thead > tr > th {
border: 1px solid #616161;text-align: center}
@media print{.logofsc{margin-top:25px!important;}}
</style>
<style>
    .buttons-pdf {
    font-size: 0px !important;
    border-radius: 3px;
    padding: 8px 10px !important;
    background: #00a65a !important;
    border-color: #008d4c !important;
        color: #fff;
}
.buttons-print {
    font-size: 0px !important;
    border-radius: 3px;
    padding: 8px 10px !important;
    background: #3c8dbc !important;
            border-color: #367fa9 !important;
                color: #fff;
}
.buttons-excel{
    font-size:0px !important;
    border-radius: 3px;
        padding: 8px 10px !important;
         background-color: #e08e0b !important;
          color: #fff;
    border-color: #e08e0b !important;

}
.buttons-excel:hover{
     background: #d58512 !important;

}
.buttons-pdf:hover{
     background: #008d4c !important;
}

.buttons-print:hover{
     background: #367fa9 !important;
}


.fa{
    font-size: 16px !important;
}
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h2>List of Employee Schedule</h2>
    </section>
    <!-- Main content -->
    <section class="content">
   <div class="row">
      <div class="col-md-12">
        <div class="box box-success">
			      <div class="box-header">
              <div class="box-tools pull-right">
              </div>
            </div>
				<div class="col-md-12">
               <form enctype='multipart/form-data' class="form-horizontal changepassword" action="" id="changepassword"  method="GET">
                  {{csrf_field()}}
                  <div class="row">
                     <div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-1">
                        <div class="form-group">
                           <label class="control-label col-md-3">Duration : <span class="star-required"></span></label>
                           <div class="col-sm-4">
                              <div class="">
                                 <select class="form-control" name="duration" id="duration">
                                    <option value="">---Select---</option>
                                    <option value="Bi-Weekly">Bi-Weekly</option>
                                    <option value="Monthly">Monthly</option>
                                    <option value="Bi-Monthly">Bi-Monthly</option>
                                    <option value="Weekly">Weekly</option>
                                 </select>
                              </div>
                           </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="focusedinput" class="col-md-3 control-label">Employee Name : <span class="required"> </span></label>
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                <select class="form-control" name="emp_name" id="emp_name">
                                        <option value="">---Select Employee---</option>
                                </select>
                           
                            </div>
                        </div>
                        
                        <div class="form-group ">
                           <label for="focusedinput" class="col-md-3 control-label">Period : <span class="required"></span></label>
                           <div class="col-sm-8" style="margin:0 !important;padding:0 !important;">
                              <div class="col-sm-3">
                                 <input class="form-control" name="startdate" id="startdate" type="text">
                              </div>
                              <label for="focusedinput" class="control-label" style="width: 0px;float: left;margin-right: 1px;position: relative;left: -6px;">To</label>
                              <div class="col-sm-3">
                                 <input class="form-control" name="enddate" id="enddate" type="text">
                              </div>
                           </div>
                        </div>
                        
                        <div class="form-group ">
                           <label for="focusedinput" class="col-md-3 control-label"></label>
                           <div class="col-sm-8" style="margin:0 !important;padding:0 !important;">
                              <div class="col-sm-3">
                                 <button class="btn-success btn" type="submit" name="search" style="width: 70%;"> Ok</button>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
         </form>
          </div>
          <div class="col-md-12">
              <br>
               <br>
              @if(isset($_REQUEST['startdate']) && isset($_REQUEST['enddate'])) 
               <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><button class="print-link btn btn-primary no-print pull-right" style="margin-right:15px;" onclick="jQuery('#ele1').print()"><i class="fa fa-print"></i> Print</button></h4>
        </div> 
        <div class="modal-body">
           <div id="ele1" class="table-responsive">
              
		 	     <center> <img src="https://financialservicecenter.net/public/dashboard/images/fsc_logo.png" alt="" class="img-responsive logofsc" style="width:150px; margin-top:15px;"></center>
		 	    <center><p style="padding:10px 10px 5px; font-size:18px;"><b>Schedule</b></p></center>
		 	     <center><p style="padding:5px font-size:18px;"><b>Period : {{$_REQUEST['startdate']}} To {{$_REQUEST['enddate']}}</b>  </p></center>
                  <table class="table table-hover table-bordered">
        <thead>
            
            <tr>
                <th></th>
                <th></th>
                <th></th>
<?php
          $start = date("d",strtotime($_REQUEST['startdate']));
          $start3 = date("d-m-Y",strtotime($_REQUEST['startdate']));
          $start3 = strtotime($start3);
          $m = date("m",strtotime($_REQUEST['startdate']));
          $y = date("Y",strtotime($_REQUEST['startdate']));
          $start1 = date("d",strtotime($_REQUEST['enddate']));
          $start4 = date("d-m-Y",strtotime($_REQUEST['enddate']));
          $start4 = strtotime($start4);
for($i=$start3; $i<=$start4; $i+=86400)
{

?>
<th><?php echo $day = date("m-d", $i);?></th>
    <?php
} ?>

</tr>
                <tr>
                <th>Emp. #</th>
                <th>Employee <br> Name</th>
                <th></th>
               <?php
               for($i=$start3; $i<=$start4; $i+=86400) 
       {?>
      
      <th><?php echo $day = date("D", $i);?></th>
       <?php }?>

            </tr>
        </thead>

<tbody>
@foreach($emp1 as $em)
                <tr>
                    <td>{{$em->employee_id}}</td>
                    <td>{{$em->firstName.' '.$em->middleName.' '.$em->lastName }}</td>
                    <td></td>
                    @foreach($ss as $em3)
                    @if($em->id ==$em3->emp_name)
                    @foreach($employee1 as $em2)
                    
                    @if($em2->emp_sch_id ==$em3->id)
                    <td>{{$em2->clockin}}</td>
                    @endif
                    @endforeach 
                    @endif
                    @endforeach 

                </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                 @foreach($ss as $em3)
                 @if($em->id ==$em3->emp_name)
                @foreach($employee1 as $em2)
                @if($em2->emp_sch_id ==$em3->id)
                <td>{{$em2->clockout}}</td>
                @else

                @endif
                @endforeach 
                
                @endif
                 @endforeach 
                          
                </tr>
                
                @endforeach   

        <?php
                 $start = date("d",strtotime($_REQUEST['startdate']));
                 $start3 = date("d-m-Y",strtotime($_REQUEST['startdate']));
                 $start3 = strtotime($start3);
                  $m = date("m",strtotime($_REQUEST['startdate']));
                  $y = date("Y",strtotime($_REQUEST['startdate']));
                  $start1 = date("d",strtotime($_REQUEST['enddate']));
                  $start4 = date("d-m-Y",strtotime($_REQUEST['enddate']));
                  $start4 = strtotime($start4);?>
                @foreach($emp1 as $em)
        <tr>
            <td>{{$em->employee_id}}</td>
            <td>{{$em->firstName.' '.$em->middleName.' '.$em->lastName }}</td>
            <td>In</td>
        <?php
        for($i=$start3; $i<=$start4; $i+=86400) 
        {
             $day = date("Y-m-d", $i);?>
            <td>
            @foreach($employee2 as $timerr) 
            @if($timerr->employee_id==$em->id)
            <?php $in = date('H:i',strtotime($timerr->emp_in));?>
            @if($day==$timerr->emp_in_date)  @if($timerr->emp_in==null) -- @else{{ date("g:i a", strtotime($in))}}  @endif @else @endif @else @endif @endforeach </td>
        <?php }?>

            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>Out</td>
       <?php
                for($i=$start3; $i<=$start4; $i+=86400) 
                       { $day = date("Y-m-d", $i);?>
                <td>
                 @foreach($employee2 as $timerr) 
                 @if($timerr->employee_id==$em->id)
                  <?php $out = date('H:i',strtotime($timerr->emp_out));?>
                    @if($day==$timerr->emp_in_date)  @if($timerr->emp_out==null) -- @else{{ date("g:i a", strtotime($out))}}  @endif @else @endif @else @endif @endforeach </td>
                <?php }?>
                          
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>Break In</td>
       <?php
        for($i=$start3; $i<=$start4; $i+=86400) 
        { 
            $day = date("Y-m-d", $i);?>
                <td>
             @foreach($employee2 as $timerr) 
             @if($timerr->employee_id==$em->id)
              <?php $launch_in = date('H:i',strtotime($timerr->launch_in));?>
                @if($day==$timerr->emp_in_date)  @if($timerr->launch_in==null) -- @else{{ date("g:i a", strtotime($launch_in))}}  @endif @else @endif @else @endif @endforeach </td>
            <?php }?>
                          
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>Break out</td>
            <?php
            for($i=$start3; $i<=$start4; $i+=86400) 
                   { $day = date("Y-m-d", $i);?>
                <td>
                 @foreach($employee2 as $timerr) 
                 @if($timerr->employee_id==$em->id)
                  <?php $launch_out = date('H:i',strtotime($timerr->launch_out));?>
                    @if($day==$timerr->emp_in_date)  @if($timerr->launch_out==null) -- @else{{ date("g:i a", strtotime($launch_out))}}  @endif @else @endif @else @endif @endforeach </td>
                <?php }?>
                      
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>Break In 1</td>
       <?php
            for($i=$start3; $i<=$start4; $i+=86400) 
            {
                $day = date("Y-m-d", $i);?>
                <td>
                 @foreach($employee2 as $timerr) 
                 @if($timerr->employee_id==$em->id)
                  <?php $launch_in_second = date('H:i',strtotime($timerr->launch_in_second));?>
                    @if($day==$timerr->emp_in_date)  @if($timerr->launch_in_second==null) -- @else{{ date("g:i a", strtotime($launch_in_second))}}  @endif @else @endif @else @endif @endforeach </td>
                <?php }?>
              
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>Break out 1</td>
            <?php
            for($i=$start3; $i<=$start4; $i+=86400) 
            {
            $day = date("Y-m-d", $i);?>
                <td>
                 @foreach($employee2 as $timerr) 
                 @if($timerr->employee_id==$em->id)
                  <?php $launch_out_second = date('H:i',strtotime($timerr->launch_out_second));?>
                    @if($day==$timerr->emp_in_date)  @if($timerr->launch_out_second==null) -- @else{{ date("g:i a", strtotime($launch_out_second))}}  @endif @else @endif @else @endif @endforeach </td>
                <?php }?>
                      
            </tr>
            
            
            @endforeach 
        </tbody>
    </table>
<table class="table table-hover table-bordered" border="1">
       <!-- <thead>
            
            <tr>
                <th></th>
                <th></th>
                <th></th>
<?php
         $start = date("d",strtotime($_REQUEST['startdate']));
         $start3 = date("d-m-Y",strtotime($_REQUEST['startdate']));
         $start3 = strtotime($start3);
          $m = date("m",strtotime($_REQUEST['startdate']));
          $y = date("Y",strtotime($_REQUEST['startdate']));
          $start1 = date("d",strtotime($_REQUEST['enddate']));
          $start4 = date("d-m-Y",strtotime($_REQUEST['enddate']));
          $start4 = strtotime($start4);
for($i=$start3; $i<=$start4; $i+=86400)
{
?>
<th><?php echo $day = date("m-d", $i);?></th>
    <?php
} ?>

</tr>
 <tr>
                <th>Emp. #</th>
                <th>Emp. Name</th>
                <th></th>
               <?php
               for($i=$start3; $i<=$start4; $i+=86400) 
       {?>
      
      <th><?php echo $day = date("D", $i);?></th>
       <?php }?>

            </tr>
        </thead>

<tbody>-->
    
  

</tbody>

</table>
<iframe name="print_frame" width="0" height="0" frameborder="0" src="about:blank"></iframe>
</div>
        </div>
       
      </div>
      
    </div>
  </div>
  


        
              <br>
              <br>
		 	 <div  class="table-responsive">
		 	    <center> <img src="https://financialservicecenter.net/public/dashboard/images/fsc_logo.png" alt="" class="img-responsive" style="width:150px;"></center>
		 	    <center><p style="padding:10px 10px 5px; font-size:18px;"><b>Schedule</b></p></center>
		 	     <center><p style="padding:5px font-size:18px;"><b>Period : {{$_REQUEST['startdate']}} To {{$_REQUEST['enddate']}}</b>  <button class="buttons-print" data-toggle="modal" data-target="#myModal" style="margin-bottom: 15px;
    position: absolute; right: 15px; margin-top: -15px;"><i class="fa fa-print"></i> Print</button></p></center>
		 	     
                  <table class="table table-hover table-bordered">
        <thead>
            
            <tr>
                <th></th>
                <th></th>
                <th></th>
<?php
          $start = date("d",strtotime($_REQUEST['startdate']));
          $start3 = date("d-m-Y",strtotime($_REQUEST['startdate']));
          $start3 = strtotime($start3);
          $m = date("m",strtotime($_REQUEST['startdate']));
          $y = date("Y",strtotime($_REQUEST['startdate']));
          $start1 = date("d",strtotime($_REQUEST['enddate']));
          $start4 = date("d-m-Y",strtotime($_REQUEST['enddate']));
          $start4 = strtotime($start4);
for($i=$start3; $i<=$start4; $i+=86400)
{

?>
<th><?php echo $day = date("m-d", $i);?></th>
    <?php
} ?>

</tr>
 <tr>
                <th>Emp. #</th>
                <th>Empployee <br> Name</th>
                <th></th>
               <?php
               for($i=$start3; $i<=$start4; $i+=86400) 
       {?>
      
      <th><?php echo $day = date("D", $i);?></th>
       <?php }?>

            </tr>
        </thead>

<tbody>
@foreach($emp1 as $em)
               <tr>
                <td>{{$em->employee_id}}</td>
                <td>{{$em->firstName.' '.$em->middleName.' '.$em->lastName }}</td>
                <td></td>
              
@foreach($ss as $em3)
@if($em->id ==$em3->emp_name)
@foreach($employee1 as $em2)

@if($em2->emp_sch_id ==$em3->id)
<td>{{$em2->clockin}}</td>
@endif
@endforeach 
@endif
 @endforeach 

         </tr>
          <tr>
                <td></td>
                <td></td>
                 <td></td>
 @foreach($ss as $em3)
 @if($em->id ==$em3->emp_name)
@foreach($employee1 as $em2)
@if($em2->emp_sch_id ==$em3->id)
<td>{{$em2->clockout}}</td>
@else

@endif
@endforeach 

@endif
 @endforeach 
          
</tr>

@endforeach   

<?php
         $start = date("d",strtotime($_REQUEST['startdate']));
         $start3 = date("d-m-Y",strtotime($_REQUEST['startdate']));
         $start3 = strtotime($start3);
          $m = date("m",strtotime($_REQUEST['startdate']));
          $y = date("Y",strtotime($_REQUEST['startdate']));
          $start1 = date("d",strtotime($_REQUEST['enddate']));
          $start4 = date("d-m-Y",strtotime($_REQUEST['enddate']));
          $start4 = strtotime($start4);?>
@foreach($emp1 as $em)
               <tr>
                <td>{{$em->employee_id}}</td>
                <td>{{$em->firstName.' '.$em->middleName.' '.$em->lastName }}</td>
                <td>In</td>
                <?php
for($i=$start3; $i<=$start4; $i+=86400) 
       { $day = date("Y-m-d", $i);?>
<td>
 @foreach($employee2 as $timerr) 
 @if($timerr->employee_id==$em->id)
  <?php $in = date('H:i',strtotime($timerr->emp_in));?>
    @if($day==$timerr->emp_in_date)  @if($timerr->emp_in==null) -- @else{{ date("g:i a", strtotime($in))}}  @endif @else @endif @else @endif @endforeach </td>
<?php }?>

         </tr>
          <tr>
                <td></td>
                <td></td>
                <td>Out</td>
       <?php
for($i=$start3; $i<=$start4; $i+=86400) 
       { $day = date("Y-m-d", $i);?>
<td>
 @foreach($employee2 as $timerr) 
 @if($timerr->employee_id==$em->id)
  <?php $out = date('H:i',strtotime($timerr->emp_out));?>
    @if($day==$timerr->emp_in_date)  @if($timerr->emp_out==null) -- @else{{ date("g:i a", strtotime($out))}}  @endif @else @endif @else @endif @endforeach </td>
<?php }?>
          
</tr>
    <tr>
                <td></td>
                <td></td>
                <td>Break In</td>
       <?php
for($i=$start3; $i<=$start4; $i+=86400) 
       { $day = date("Y-m-d", $i);?>
<td>
 @foreach($employee2 as $timerr) 
 @if($timerr->employee_id==$em->id)
  <?php $launch_in = date('H:i',strtotime($timerr->launch_in));?>
    @if($day==$timerr->emp_in_date)  @if($timerr->launch_in==null) -- @else{{ date("g:i a", strtotime($launch_in))}}  @endif @else @endif @else @endif @endforeach </td>
<?php }?>
          
</tr>
    <tr>
                <td></td>
                <td></td>
                <td>Break out</td>
       <?php
for($i=$start3; $i<=$start4; $i+=86400) 
       { $day = date("Y-m-d", $i);?>
<td>
 @foreach($employee2 as $timerr) 
 @if($timerr->employee_id==$em->id)
  <?php $launch_out = date('H:i',strtotime($timerr->launch_out));?>
    @if($day==$timerr->emp_in_date)  @if($timerr->launch_out==null) -- @else{{ date("g:i a", strtotime($launch_out))}}  @endif @else @endif @else @endif @endforeach </td>
<?php }?>
          
</tr>
   <tr>
                <td></td>
                <td></td>
                <td>Break In 1</td>
       <?php
for($i=$start3; $i<=$start4; $i+=86400) 
       { $day = date("Y-m-d", $i);?>
<td>
 @foreach($employee2 as $timerr) 
 @if($timerr->employee_id==$em->id)
  <?php $launch_in_second = date('H:i',strtotime($timerr->launch_in_second));?>
    @if($day==$timerr->emp_in_date)  @if($timerr->launch_in_second==null) -- @else{{ date("g:i a", strtotime($launch_in_second))}}  @endif @else @endif @else @endif @endforeach </td>
<?php }?>
          
</tr>
    <tr>
                <td></td>
                <td></td>
                <td>Break out 1</td>
       <?php
for($i=$start3; $i<=$start4; $i+=86400) 
       { $day = date("Y-m-d", $i);?>
<td>
 @foreach($employee2 as $timerr) 
 @if($timerr->employee_id==$em->id)
  <?php $launch_out_second = date('H:i',strtotime($timerr->launch_out_second));?>
    @if($day==$timerr->emp_in_date)  @if($timerr->launch_out_second==null) -- @else{{ date("g:i a", strtotime($launch_out_second))}}  @endif @else @endif @else @endif @endforeach </td>
<?php }?>
          
</tr>


@endforeach 
</tbody>
</table>
<table class="table table-hover table-bordered" border="1">
       <!-- <thead>
            
            <tr>
                <th></th>
                <th></th>
                <th></th>
<?php
         $start = date("d",strtotime($_REQUEST['startdate']));
         $start3 = date("d-m-Y",strtotime($_REQUEST['startdate']));
         $start3 = strtotime($start3);
          $m = date("m",strtotime($_REQUEST['startdate']));
          $y = date("Y",strtotime($_REQUEST['startdate']));
          $start1 = date("d",strtotime($_REQUEST['enddate']));
          $start4 = date("d-m-Y",strtotime($_REQUEST['enddate']));
          $start4 = strtotime($start4);
for($i=$start3; $i<=$start4; $i+=86400)
{
?>
<th><?php echo $day = date("m-d", $i);?></th>
    <?php
} ?>

</tr>
 <tr>
                <th>Emp. #</th>
                <th>Emp. Name</th>
                <th></th>
               <?php
               for($i=$start3; $i<=$start4; $i+=86400) 
       {?>
      
      <th><?php echo $day = date("D", $i);?></th>
       <?php }?>

            </tr>
        </thead>

<tbody>-->
    
  

</tbody>

</table>
<iframe name="print_frame" width="0" height="0" frameborder="0" src="about:blank"></iframe>
</div>
<p style="color:red;font-weight:bold">NOTE : <span style="color:black;font-weight:normal">Employee Responsibility :</span></p>  
<ul>
    <li>Record  clock in and out immediately when they come in/out.</li>
    <li>All the employee make sure that time recorded paid for that in their payroll.</li>
    <li>If not please notify immediately to payroll department.</li>
    <li>Leave request get approved before 14 days in advance.</li>
    <li>All the employee must take lunch brake as per company rules.</li>
    <li>If you have any question contact office manager immediately.</li>
    <li>Office Lunch Hrs. 1.00 p.m. to 2.00 p.m.</li>
    <li>Work as per schdule time - After schdule hrs will not be consider.</li>
    <li>Overtime will not be granted without prior approval or sign by supervisor.</li>
</ul>
 @else 		    
 @endif
          </div>
         </div>
      </div>
   </div>
   
    </section>
     </div>  
<script>
   $(document).ready(function(){
   	$(document).on('change','#duration', function()
   	{
   		//console.log('htm');
   		var type2 = 'clientemployee';
   		var id = $(this).val();//alert(id);
   		$.get('{!!URL::to('getdurationempclient2')!!}?id='+id+'&type1=' + type2, function(data)
   		{  
            $('#emp_name').empty();
            $('#emp_name').append('<option value="">All</option>');
            $.each(data, function(index, subcatobj)
   		    {        
                // $('#duration').val(subcatobj.duration);
                $('#emp_name').append('<option value="'+subcatobj.id+'">'+subcatobj.firstName+' '+subcatobj.middleName+' '+subcatobj.lastName+'</option>');
            })
   
   		});
   			
   	});
   });
</script> 
<script>
  $(document).ready(function(){ 

$("#startdate").datepicker({
format: "mm/dd/yyyy"});

  
});
  $(document).ready(function(){ 
$("#enddate").datepicker({
format: "mm/dd/yyyy"});
});

</script>
  <script type="text/javascript">
  $(document).ready(function() {
    $("#startdate").change(function() {
      var startdate= $("#startdate").val();
      var monthNames = [
        "Jan", "Feb", "Mar",
        "Apr", "May", "Jun", "Jul",
        "Aug", "Sep", "Oct",
        "Nov", "Dec"
      ];
    var weekday = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
      var durtion=$('#duration').val();
      var  date = new Date(startdate);//alert(date);
      var day = weekday[date.getDay()];
      var monthly=30;
      var weekly=7;
      var bimonthly=15;
      var biweekly=14;
      var monthss=monthNames[(date.getMonth())];
      var yearss = date.getFullYear();
      var yyy=yearss % 4 ;
      
      
      if(durtion == "Weekly")
      {
        var totaldays=6;
       var totalday1=4;
      }
      else if(durtion == "Monthly")
      { 
        if(monthss == 'Jan' || monthss == 'Mar' || monthss == 'May' || monthss == 'Jul' || monthss == 'Aug' || monthss == 'Oct' || monthss == 'Dec')
        {
          var totaldays=30;
          var totalday1=5;
        }
        else if(monthss == 'Feb')
        {  
            var totaldays=27;//alert();
            var totalday1=5;
          
        }
        else if(monthss == 'Apr' || monthss == 'Jun' || monthss == 'Sep' || monthss == 'Nov' )
        {
          var totaldays=29;
          var totalday1=5;
        }
      }
      else if(durtion == "Bi-Weekly")
      {
        var totaldays=13;
        var totalday1=4;
      }
      else if(durtion == "Bi-Monthly")
      {
        var totaldays=14;
       var totalday1=4;
      }
     var vad = date.setDate(date.getDate() + totaldays);
    
      var date1 = ("0" + (date.getMonth() + 1)).slice(-2)  + "/" + ("0" + date.getDate()).slice(-2) + "/" + date.getFullYear();
      var vad1 = date.setDate(date.getDate() + totalday1);
    // alert(vad1);
     var date3 = ("0" + (date.getMonth() + 1)).slice(-2)  + "/" + ("0" + (date.getDate())).slice(-2) + "/" + date.getFullYear();
     // alert(date3);
      var newdate = new Date(date1);
        var day1 = weekday[newdate.getDay()];
        var newdate1 = new Date(date3);
        var day2 = weekday[newdate1.getDay()];
       //alert(newdate);
      var date2=monthNames[(date.getMonth())] + "/" + date.getDate() + "/" + date.getFullYear() ;
      $('#enddate').val(date1);
      
    });
    
    $("#duration").change(function() {
      $('#startdate').val('');
      $('#enddate').val('');
       
    });
  });
  

      
</script>
<script src="https://www.jqueryscript.net/demo/jQuery-Plugin-To-Print-Any-Part-Of-Your-Page-Print/jQuery.print.js"></script>
<script type='text/javascript'>
            //<![CDATA[
            $(function() {
                $("#ele2").find('.print-link').on('click', function() {
                    //Print ele2 with default options
                    $.print("#ele2");
                });

                $("#ele4").find('button').on('click', function() {
                    //Print ele4 with custom options
                    $("#ele4").print({
                        //Use Global styles
                        globalStyles : false,

                        //Add link with attrbute media=print
                        mediaPrint : false,

                        //Custom stylesheet
                        stylesheet : "http://fonts.googleapis.com/css?family=Inconsolata",

                        //Print in a hidden iframe
                        iframe : false,

                        //Don't print this
                        noPrintSelector : ".avoid-this",

                        //Add this on top
                        append : "Free jQuery Plugins!!!<br/>",

                        //Add this at bottom
                        prepend : "<br/>jQueryScript.net!"
                    });
                });

                // Fork https://github.com/sathvikp/jQuery.print for the full list of options
            });
            //]]>

        </script>
<style>
    .table > thead > tr > th {
    background: #98c4f2;
    min-width: 75px;
}
@page {
        size: landscape;
        margin: 0;
    }
    @media print {
        html, body {
            width: 410mm;
            height: 297mm;  
             font: 20px;
        }
        #ele1{
            margin: 0;
            border: initial;
            border-radius: initial;
            width: initial;
            min-height: initial;
            box-shadow: initial;
            background: initial;
            page-break-after: always;
        }
    }
</style>
@endsection