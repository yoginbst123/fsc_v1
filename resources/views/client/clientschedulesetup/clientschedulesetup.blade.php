@extends('client.layouts.app')
@section('title', 'Schedule Formula Setup')
@section('main-content')

<div class="content-wrapper">
     <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>Schedule Formula Setup </h1>
    </section>
    <!-- Main content -->
    <section class="content">
  
   <div class="row">
      <div class="col-md-12">
         	<div class="box box-success card">
			      <div class="box-header">
              <h3 class="box-title"></h3>
              <div class="box-tools pull-right">
                <div class="table-title">
                  <a class="btn btn-primary" href="{{route('clientschedulesetup.create')}}">Add New Schedule</a>
               </div>
              </div>
            </div>
			    <div class="col-md-12">
               
               @if ( session()->has('success') )
               <div class="alert alert-success alert-dismissable">{{session()->get('success') }}</div>
               @endif
               <div class="table-responsive">
                  <table class="table table-hover table-bordered" id="sampleTable3">
                     <thead>
                        <tr style="text-align:center">
                            <th style="text-align:center">No. </th>
                            <th style="text-align:center">Employee Name </th>
                            <th style="text-align:center">Employer City </th>
                            <th style="text-align:center">Duration </th>
                            <th style="text-align:center">Start Date </th>
                            <th style="text-align:center">End Date </th>
                            <th style="text-align:center">Action</th>
                        </tr>
                     </thead>
                     <tbody>
                       <?php $cnts=0;?>
                           @foreach($sch as $employ)
                           <?php $cnts++;?>
                           <tr class="cc{{$employ->cid}}">
                                                                 
                              <td style="text-align:center"><?php echo $cnts;?></td>
                              <td style="text-align:center">{{$employ->firstName}} {{$employ->middleName}} {{$employ->lastName}}</td>
                              <td style="text-align:center">{{$employ->emp_city}}</td>
                              <td style="text-align:center">{{$employ->duration}}</td>
                              <td style="text-align:center">{{$employ->sch_start_date}}</td>
                              <td style="text-align:center">{{$employ->sch_end_date}}</td>
                              <th style="text-align:center">
                                 <a class="btn-action btn-view-edit" href="{{route('clientschedulesetup.edit', $employ->cid)}}"><i class="fa fa-edit"></i></a>
                                 <form action="{{ route('clientschedulesetup.destroy',$employ->cid) }}" method="post" style="display:none" id="delete-id-{{$employ->cid}}">
                                    {{csrf_field()}} {{method_field('DELETE')}}
                                 </form>
                                 <a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record ?'))
                                    {event.preventDefault();document.getElementById('delete-id-{{$employ->cid}}').submit();} else{event.preventDefault();}" href=""><i class="fa fa-trash"></i></a></td>
                            </tr>  
                           @endforeach
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div> </section>
</div>
<style>th{ text-align:center}</style>
@endsection()

