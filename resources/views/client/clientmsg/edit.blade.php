@extends('client.layouts.app')
@section('title', 'Edit Message')
@section('main-content')
<div class="content-wrapper">
       <div class="page-title">
           
		<h1>Message</h1>
	</div>
   <div class="row">
      <div class="col-md-12">
         <div class="card">
            <div class="card-body">
               <form method="post" action="#" class="form-horizontal" id="content" name="content" enctype="multipart/form-data">
                  {{csrf_field()}} {{method_field('PATCH')}}
<div class="form-group{{ $errors->has('employee') ? ' has-error' : '' }}">
                     <label class="control-label col-md-3">Employee Name :</label>
                     <div class="col-md-8">
                        <div class="box-body pad">
                           <?php $ex=  explode(',',$task->employeeid);?>
                             <input type="text" value=" @foreach( $ex as $tk) @if($tk==$employee->id) {{ucwords($employee->first_name.' '.$employee->middle_name.' '.$employee->last_name)}} @endif @endforeach" readonly  name="employee" id="employee" class="form-control fsc-input">
                        </div>
                       
                     </div>
                  </div>
                  <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                     <label class="control-label col-md-3">Subject :</label>
                     <div class="col-md-8">
                        <div class="box-body pad">
                           <input type="text" value="{{$task->title}}" name="title" id="title" class="form-control fsc-input"  readonly>
                            <input type="hidden" value="{{$task->admin_id}}" name="admin_id" id="admin_id" class="form-control fsc-input">
                            
                        </div>
                        @if ($errors->has('title'))
                        <span class="help-block">
                        <strong>{{ $errors->first('title') }}</strong>
                        </span>
                        @endif	
                     </div>
                  </div>
                  <div class="form-group{{ $errors->has('rlt_msg') ? ' has-error' : '' }}">
                   <label class="control-label col-md-3">Message Related To  :</label>
                     <div class="col-md-8">
                        <div class="box-body pad">
                          <select name="rlt_msg" id="rlt_msg" class="form-control fsc-input">
                              <option value="">---Select---</option>
                              <option value="Inceom Tax Return Preparation Inquire" @if($task->rlt_msg=='Inceom Tax Return Preparation Inquire') selected @endif>Inceom Tax Return Preparation Inquire</option>
                              <option value="Sales Tax Return"  @if($task->rlt_msg=='Sales Tax Return') selected @endif>Sales Tax Return</option>
                              <option value="License Related"  @if($task->rlt_msg=='License Related') selected @endif>License Related</option>
                              <option value="Tax Issue Related"  @if($task->rlt_msg=='Tax Issue Related') selected @endif>Tax Issue Related</option>
                              <option value="Update on Status on work"  @if($task->rlt_msg=='Update on Status on work') selected @endif>Update on Status on work</option>
                           </select>
                        </div>
                      	
                     </div>
                  </div>
                   <div class="form-group{{ $errors->has('call_back') ? ' has-error' : '' }}">
                   <label class="control-label col-md-3">Call Back :</label>
                     <div class="col-md-8">
                        <div class="box-body pad">
                          <select name="call_back" id="call_back" class="form-control fsc-input">
                              <option value="">---Select---</option>
                              <option value="Yes" @if($task->call_back=='Yes') selected @endif>Yes</option>
                              <option value="No" @if($task->call_back=='No') selected @endif>No</option>
                           </select>
                        </div>
                      	
                     </div>
                  </div>
                  <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                     <label class="control-label col-md-3">Description :</label>
                     <div class="col-md-8">
                        <div class="box-body pad">
                           <textarea id="editor1" name="description" rows="10" cols="80">{!!$task->content!!}</textarea>
                        </div>
                        @if ($errors->has('description'))
                        <span class="help-block">
                        <strong>{{ $errors->first('description') }}</strong>
                        </span>
                        @endif	
                     </div>
                  </div>
                  
                  <div class="card-footer">
                     <div class="row">
                        <div class="col-md-8 col-md-offset-3">
                            <a class="btn btn-primary icon-btn upload-image" href="{{url('fscemployee/getmsg')}}"><< Back</a>
                           <a class="btn btn-primary icon-btn upload-image" href="{{url('fscemployee/getmsg')}}">Cancel</a>
                        </div>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection()