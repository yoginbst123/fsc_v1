@extends('client.layouts.app')
@section('title', 'Employee')
@section('main-content')
<div class="content-wrapper">
<section class="content-header">
      <h1> Employee</h1>
    </section>
	<div class="">
	<br>
		<div class="col-md-12">
			<div class="card">
				<div class="card-body">
				    <div class="box-header">
              <h3 class="box-title"></h3>
              <div class="box-tools pull-right">
                <div class="table-title">
                  <a href="{{url('client/cli-employee/create')}}" class="btn btn-primary">Add Employee</a>
               </div>
              </div>
            </div>
				
					@if ( session()->has('success') )
    <div class="alert alert-success alert-dismissable">{{session()->get('success') }}</div>
@endif
				 <div class="table-responsive">
                  <table class="table table-hover table-bordered" id="sampleTable3">
                     <thead>
                        <tr>
<th>Type</th>
                           <th>EE / User ID </th>

                           <th>Employee Name</th>
                           <th>Email ID</th>
                           <th>Tel. Number</th>
                          
                           <th>Status</th>
                           <th>Action</th>
                        </tr>
                     </thead>
                     <tbody>
@foreach($employee as $employ)
                        <tr><td style="text-transform: capitalize;">{{$employ->type}}</td>
                           <td>{{$employ->employee_id}}</td>

                           <td>{{$employ->firstName}} {{$employ->middleName}} {{$employ->lastName}}</td>
                           <td>{{$employ->email}}</td>
                           <td>{{$employ->telephoneNo1}}</td>
                          
                           <td>@if($employ->check==1)<a class="btn btn-success" href="">Active</a> @else <a class="btn-action btn-delete" href="">Inactive</a> @endif</td>
                           <td>
@if($employ->newemp==1)<a class="" href="{{route('cli-employee.edit',$employ->cid)}}"><img src="{{asset('public/dashboard/images/newimage.gif')}}" alt="" width="50px"></a><br>@endif
<a class="btn btn-success" href="{{route('cli-employee.edit',$employ->id)}}"><i class="fa fa-edit"></i></a>
                                    <form action="{{route('cli-employee.destroy',$employ->id)}}" method="post" style="display:none" id="delete-id-{{$employ->id}}">
                                        {{csrf_field()}} {{method_field('DELETE')}}
                                    </form>
				<a class="btn btn-danger" href="#" onclick="if(confirm('Are you sure, You want to delete this record ?'))
                                                                                {event.preventDefault();document.getElementById('delete-id-{{$employ->id}}').submit();} else{event.preventDefault();}"><i class="fa fa-trash"></i></a>
								</td>
                        </tr>
@endforeach
                     </tbody>
                  </table>
               </div>
				</div>
			</div>
		</div>
		
	</div>
	
</div>
@endsection()