<div class="col-lg-9 col-md-8 col-sm-12 col-xs-12" style="padding:0px;">
	<div class="well well-lg" style="margin-top: 1%;">
		<h4 class="fsc-home-text-head" style='text-align:left'>Welcome and thank you for visiting our Website.</h4>
		<br>
		<h4 class="fsc-home-text">In addition to providing you with a profile of our firm and the services we provide, we hope this Website will become a helpful resource tool to you and to our valued clients.</h4>
		<br><br>
		<h4 class="fsc-home-text">Financial Service Center has been helping small, medium and large firms manage their Accounting and Finances for over 20 years. We offer you the advantage of managing your varied financial obligations and concerns from one central location.</h4>
		<br><br>
		<h4 class="fsc-home-text">While browsing through our Website, please feel free to contact us with any questions or comments you may have, we'd love to hear from you. We pride ourselves on being proactive and responsive to our clients' inquiries and suggestions.</h4>
	</div>
	<div class="fsc-well" style="margin-top:3%; width:100%;">
		<a href="what-we-do.php" class="fsc-home-wedo-head">This is what we do.</a>
	</div>
</div>