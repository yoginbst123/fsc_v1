@extends('fscemployee.layouts.app')
@section('main-content')
<div class="content-wrapper">
       <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>Email</h1>
    </section>
    <!-- Main content -->
    <section class="content">
	<div class="row">
		<div class="col-md-12">
		<div class="box box-success">
			      <div class="box-header">
        
              <div class="box-tools pull-right">
                	<div class="table-title">
					
					
					</div>
              </div>
            </div>
				<div class="col-md-12">

				
@if ( session()->has('success') )
    <div class="alert alert-success alert-dismissable">{{ session()->get('success') }}</div>
@endif
@if ( session()->has('error') )
    <div class="alert alert-danger alert-dismissable">{{ session()->get('error') }}</div>
@endif  
					<div class="table-responsive">
					<table class="table table-hover table-bordered" id="sampleTable3">
							<thead>
								<tr>
								    <th style="text-align:center">Sr No.</th>
									<th style="text-align:center">Name </th>
									<th style="text-align:center">Email </th>
									<th style="text-align:center">Get Email</th>
									<th style="text-align:center">Password</th>
									<th style="text-align:center">Action</th>
								</tr>
							</thead>
							<tbody>
							    @foreach($email as $a)
							    
								<tr>
									<td><center>{{$loop->index+1}}</center></td>
									<td>
									
									{{ Auth::user()->fname.' '.Auth::user()->mname.' '.Auth::user()->lname}} 
								</td>
									<td>{{$a->email}}</td>
									<td><center><a href="https://{{$a->access_address}}" class="btn-action btn-view-edit" target="_blank">Access</a></center></td>
									<td><input type="password" value="{{$a->password}}" disabled id="myInput_{{$a->id}}"> &nbsp; &nbsp; &nbsp;&nbsp;   <lable  onclick="myFunction{{$a->id}}()"><input type="checkbox">Show</label></td>
									<td><a class="btn-action btn-view-edit" href="{{route('eemail.edit', $a->id)}}"><i class="fa fa-edit"></i></a>
                                     </td>
								</tr>
								<script>
function myFunction{{$a->id}}() {
    var x = document.getElementById("myInput_{{$a->id}}");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}
</script>
								@endforeach
                             </tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		
	</div>	
	</section>
</div>


@endsection()