@extends('clientemployee.layouts.app')
@section('title', 'Vendor')
@section('main-content')
<style>
label{float:left} th{ text-align:center !important}
.input-sm{width: 110% !important;margin-left: 10px !important;}
.dataTables_filter{display:none;}
.dt-buttons{
    padding-bottom:10px;
}
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header page-title" style="height:50px;">
     		<div class="row col-md-12">
     		    <div class="col-md-7" style="text-align:right;">
     		        <h1>List of Vendor</h1>
     		    </div>
     		    <div class="col-md-5" style="text-align:right;">
     		        <h1>Add / View / Edit</h1>
     		    </div>
     		</div>
    </section>
    <!-- Main content -->
    <section class="content">		
	<div class="row">
		<div class="col-md-12">
			<div class="box box-success">
			      <div class="box-header">
                   <div class="col-md-9" style="margin-left:-1.6%">
              <div class="row">
              <div class="col-md-1"><label style="margin-left:28%;margin-top: 11px;">Filter: </label></div>
              <div class="col-md-3">
                <select name="choice" style="width: 92%;margin-left: 4px;" id="choice" class="form-control">
        <option value="1">All</option>
        <option value="Active" selected>Active</option>
        <option value="Pending">Pending</option>
        <option value="Hold">Hold</option>
        <option value="In-Active">Inactive</option>
    </select>
    </div>
    <div class="col-md-1"><label style="margin-left:28%;margin-top: 11px;">Search: </label></div>
    <div class="col-md-4">
    <select name="types" style="width: 92%;margin-left: 4px;" id="types" class="form-control">
        <option value="All">All</option>
        <option value="Type Of Service">Type Of Service</option>
        <option value="Vendor Name">Vendor Name</option>
        <option value="Tele">Tele #</option>
        <option value="Email">Email</option>
    </select>
    </div>
     <div class="col-md-3">
     <table style="width: 100%; margin: 0 auto 2em auto;" cellspacing="0" cellpadding="3" border="0">
        <tbody>
               <tr id="filter_global">
                <td align="center"><input type="text" class="global_filter form-control" id="global_filter" placeholder="All Search"></td>
            </tr>
            <tr id="filter_col2" data-column="1" style="display:none">
                <td align="center"><input type="text" class="column_filter form-control" id="col1_filter" placeholder="Type Of Service"></td>
            </tr>
            <tr id="filter_col3" data-column="2" style="display:none">
                <td align="center"><input type="text" class="column_filter form-control" id="col2_filter" placeholder="Vendor Name"></td>
            </tr>
            <tr id="filter_col4" data-column="3" style="display:none">
                <td align="center"><input type="text" class="column_filter form-control" id="col3_filter" placeholder="Tele"></td>
            </tr>
            <tr id="filter_col5" data-column="4" style="display:none">
                <td align="center"><input type="text" class="column_filter form-control" id="col4_filter" placeholder="Email"></td>
            </tr>
        </tbody>
    </table>
    </div>
    </div>
              </div>
             	<div class="col-md-3">
					<div class="table-title">
						<a href="{{url('fscemployee/empvendor/create')}}"><i class="fa fa-plus"></i>&nbsp; Add New Vendor</a>
					</div><br>
					<br>
					</div>
            </div>
				<div class="col-md-12">
					<div class="table-title">
					</div>
					@if(session()->has('success'))
                          <div class="alert alert-success alert-dismissable">{{session()->get('success')}}</div>
                    @endif
					<div class="table-responsive">
						<table class="table table-hover table-bordered" id="example">
							<thead>
								<tr>
								   <th>No</th>
								   <th>Type Of Service</th>
								   <th>Vendor Name</th>
							       <th>Tele #</th>
							       <th>Status</th>
								   <th>Action</th>
								</tr>
							</thead>							
							<tbody>
                             @foreach($employee1 as $employ)
                                  <?php $str1=$employ->business_catagory_name; $splittedstring1=explode(",",$str1);?>
                        <tr><td style="text-transform: capitalize;text-align:center;">{{$loop->index+1}}</td>
                       <td style="text-transform: capitalize;">
                           @foreach($category as $cate) @foreach($splittedstring1 as $key => $value) @if($value ==$cate->id) {{$cate->business_cat_name}}<br> @endif @endforeach @endforeach</td>
                           <td >{{$employ->business_name}}</td>
                            <td>{{$employ->telephoneNo1}}</td>
                          <td><center>@if($employ->check==1)<a class="btn-action btn-view-edit" href="" style="background:#689203 !important; text-align:center">Active</a> @else <a class="btn-action btn-delete" href="">Inactive</a> @endif</center></td>
                           <td style="text-align:center;">@if($employ->newemp==1)<a class="" href="{{route('empvendor.edit',$employ->cid)}}"><img src="{{asset('public/dashboard/images/newimage.gif')}}" alt="" width="50px"></a><br>@endif
<a class="btn-action btn-view-edit" style="background:#367fa9 !important"  href="{{route('empvendor.edit',$employ->id)}}"><i class="fa fa-edit"></i></a>
                                    <form action="{{route('empvendor.destroy',$employ->id)}}" method="post" style="display:none" id="delete-id-{{$employ->id}}">
                                        {{csrf_field()}} {{method_field('DELETE')}}
                                    </form><a class="btn-action btn-delete" href="#" onclick="if(confirm('Are you sure, You want to delete this record ?')){event.preventDefault();document.getElementById('delete-id-{{$employ->id}}').submit();} else{event.preventDefault();}"><i class="fa fa-trash"></i></a>
								</td>
                        </tr>
                      @endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	</section>
</div>
<script>
$(document).ready(function() {
    var table = $('#example').DataTable( {
        "ordering": true,
        dom: 'Bfrtip',
        "columnDefs": [ {
            "searchable": false,
            "orderable": true,
            "targets": 0
        }],
        "order": [[1,'asc']],
        buttons: [
                 {
                extend:    'copyHtml5',
                text:      '<i class="fa fa-files-o"></i> &nbsp; Copy',
                title: $('h1').text(),
                  exportOptions: {
                     columns: [0,1, 2, 3,4,5,6], 
               }
            },
            {
                extend:    'excelHtml5',
                text:      '<i class="fa fa-file-excel-o"></i>&nbsp; Excel',
                title: $('h1').text(),
                  exportOptions: {
                  columns: [0,1, 2, 3,4,5,6],
             }
            },
            {
                extend: 'csvHtml5',
                text:   '<i class="fa fa-file-text-o"></i> &nbsp; CSV',
                title: $('h1').text(),
                  exportOptions: {
        columns: [0,1, 2, 3,4,5,6], 
    }
            },
            {
                extend:    'pdfHtml5',
                text:      '<i class="fa fa-file-pdf-o"></i>&nbsp;  PDF',
                title: $('h1').text(),
                exportOptions: {
        columns: [0,1, 2, 3,4,5,6],
    }
            },
            {
           extend: 'print',
          text: '<i class="fa fa-print"></i>&nbsp; Print',
         title: $('h1').text(),
         exportOptions: {
          columns: ':not(.no-print)'
      },
      footer: true,
      autoPrint: true
    },
        ],
    } );
$('input.global_filter').on( 'keyup click', function () {
        filterGlobal();
    } );
 
    $('input.column_filter').on( 'keyup click', function () {
        filterColumn( $(this).parents('tr').attr('data-column') );
    } );
table.on( 'order.dt search.dt', function () {
        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
    
     table.columns(7).search('^(?:(?!In-Active).)*$\r?\n?', true, false).draw();
     table.columns(7).search('^(?:(?!Approval).)*$\r?\n?', true, false).draw();  
$("#choice").on("change",function(){
 var _val = $(this).val();
 if(_val == 'In-Active'){   
        table.columns(7).search(_val).draw();
  }
  else if(_val == 'Active'){
        table.columns(7).search(_val).draw();
        table.columns(7).search('^(?:(?!In-Active).)*$\r?\n?', true, false).draw();
        table.columns(7).search(_val).draw();
        table.columns(7)
  }
  else if(_val == 'Approval'){ 
        table.columns(7).search(_val).draw();
        table.columns(7).search('^(?:(?!In-Active).)*$\r?\n?', true, false).draw();
        table.columns(7).search('^(?:(?!Active).)*$\r?\n?', true, false).draw();
  }
  else{
        table.columns().search('').draw(); 
  }
  })
} );
function filterGlobal () {
    $('#example').DataTable().search(
        $('#global_filter').val(),
        $('#global_regex').prop('checked'),
        $('#global_smart').prop('checked')
    ).draw();
}
function filterColumn ( i ) {
    $('#example').DataTable().column( i ).search(
        $('#col'+i+'_filter').val(),
        $('#col'+i+'_regex').prop('checked'),
        $('#col'+i+'_smart').prop('checked')
    ).draw();
}
$( "#types" ).on('change',function() {
  // For unique choice
  var selVal = $( "#Type Of Service option:selected" ).val(); 
   if(selVal=='Type Of Service')  
  {
      $('#filter_global').hide();
      $('#filter_col3').hide();
      $('#filter_col4').hide();
      $('#filter_col5').hide();
      $('#filter_col6').hide();
      $('#filter_col7').hide();
      $('#filter_col2').show();
  }
  else if(selVal=='EE / User ID')  
  {
        $('#filter_col7').hide();
      $('#filter_global').hide();
      $('#filter_col2').hide();
      $('#filter_col4').hide();
      $('#filter_col5').hide();
      $('#filter_col6').hide();
      $('#filter_col3').show();
  }
    else if(selVal=='Employee Name')  
  {
      $('#filter_global').hide();
      $('#filter_col2').hide();
      $('#filter_col3').hide();
      $('#filter_col5').hide();
      $('#filter_col6').hide();
      $('#filter_col4').show();  
      $('#filter_col7').hide();
  }
   else if(selVal=='Email ID')  
  {
      $('#filter_global').hide();
      $('#filter_col2').hide();  
      $('#filter_col7').hide();
      $('#filter_col3').hide();
      $('#filter_col4').hide();
      $('#filter_col6').hide();
      $('#filter_col5').show();
  }
  else if(selVal=='Tel. Number')  
  {
      $('#filter_global').hide();
      $('#filter_col2').hide();
      $('#filter_col3').hide();
      $('#filter_col4').hide();
      $('#filter_col5').hide();  
      $('#filter_col7').hide();
      $('#filter_col6').show();
  }
  else if(selVal=='Tel. Number')  
  {
      $('#filter_global').hide();
      $('#filter_col2').hide();
      $('#filter_col3').hide();
      $('#filter_col4').hide();
      $('#filter_col5').hide();  
      $('#filter_col6').hide();
      $('#filter_col7').show();
  }
  else{
      $('#filter_global').show();
       $('#filter_col3').hide();
      $('#filter_col4').hide();
      $('#filter_col5').hide();
      $('#filter_col6').hide();  
      $('#filter_col7').hide();
      $('#filter_col2').hide();
  }
});
</script>
@endsection()