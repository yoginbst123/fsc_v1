@extends('clientemployee.layouts.app')
@section('title', 'Report')
@section('main-content') 
<div class="content-wrapper">
    <div class="page-title">
      <h1>Search</h1>
    </div>
     <section class="content" style="background-color: #fff;">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
        <div class="card-body">
        <form enctype='multipart/form-data' class="form-horizontal" action="{{route('report.index')}}" method="get">  
{{ method_field('PUT') }}           
{{csrf_field()}} 
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="form-group ">
                <label for="focusedinput" class="col-md-3 control-label">Period : <span class="required"> * </span></label>
                <div class="col-sm-8" style="margin:0 !important;padding:0 !important;">
                  <div class="col-sm-3">
                    <input class="validate[required] form-control hasDatepicker" name="startdate" data-date-format="yyyy-mm-dd" id="min" type="text">
                  </div>
                  <label for="focusedinput" class="col-sm-1 control-label">To</label>
                  <div class="col-sm-3">
                    <input class="validate[required] form-control hasDatepicker" data-date-format="yyyy-mm-dd" name="enddate" id="max" type="text">
                  </div>
                </div>
              </div>
              <div class="form-group ">
                <label for="focusedinput" class="col-md-3 control-label"></label>
                <div class="col-sm-8" style="margin:0 !important;padding:0 !important;">
                  <div class="col-sm-3">
                    <button class="btn-success btn" type="submit" name="search"> Search</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          </div>
          </div>
          <br>
        </form>
      </div>
    </div>
<div class="table-responsive">
  <style>
   .table > thead > tr > th {
    background: #98c4f2;
    text-align: center;
}
td{text-align: center !important;}
.form-group {margin-bottom: 15px;width: 100%;float: left;}
   </style>
   <?php 
      if(empty($_GET['enddate']) && empty($_GET['startdate']))
      {
      //echo "<p>Data not Found</p>";
      }
      else
      {
      ?>

   <div class="row">
      <div class="col-md-12">
         <div class="card" style="background:#fff">
            <div class="card-body" id="dvContents">
                <button type='button' id='btn' style="margin:10px 15px 10px 15px" class='pull-right btn btn-primary' value='Print' onclick='printDiv();'><i class="fa fa-print"></i> Print</button>
                <button type="button" id="btnExport" style="margin:10px 0 10px 15px" value="PDF" class="pull-right btn btn-primary"><i class="fa fa-file-pdf-o"></i> PDF</button>
                <button style="margin:10px 0 10px 15px" class="pull-right btn btn-primary" id="btnexcel"><i class="fa fa-file-text-o"></i> Excel</button>
                
                <br><br>
             <table class="table table-hover table-bordered" id="sampleTable3">
                  <thead>
                      <tr>
                 <th colspan="9" style="background: #fff;"><h2 class="text-center"><img src="https://financialservicecenter.net/public/dashboard/images/fsc_logo.png" width="140px" alt=""></h2></th>
                 <th style="display:none"></th>
                 <th style="display:none"></th>        
                 <th style="display:none"></th>        
                <th style="display:none"></th>        
                <th style="display:none"></th> 
                <th style="display:none"></th> 
                <th style="display:none"></th> 
                <th style="display:none"></th> 
                </tr>
                  @foreach($emp as $e)
               @if($e->id==$employee->id)
                 <tr>
                 <th colspan="3"><p class="text-center">Employee Id: <span style="margin-left:5px">{{ucfirst($e->employee_id)}}</span></p></th>
                 <th colspan="3"><p class="text-center">Employee Name: <span style="margin-left:5px">{{ucfirst($e->firstName.' '.$e->middleName.' '.$e->lastName)}}</span></p></th>
                 <th colspan="3"><p class="text-center"><b>Period: <span style="margin-left:5px"><?php echo $_REQUEST['startdate'];?> To <?php echo $_REQUEST['enddate'];?></span></b></p></th>
                 <th style="display:none"></th>
                 <th style="display:none"></th>        
                 <th style="display:none"></th>        
                 <th style="display:none"></th>        
                 <th style="display:none"></th> 
                 <th style="display:none"></th> 
                 </tr>
                @endif
               @endforeach
                     <tr>
                        <th>Date <br> Day</th>
                        <th>Clock <br> In</th>
                        <th>Break 1 <br> In / Out</th>
                        <th>Break 2 <br> In / Out</th>
                        <th>Clock <br> Out</th>
                        <th>Total <br> Hours(D)</th>
                        <th style="width: 300px;">Employee <br> Notes</th>
                        <th>IP Address</th>
                        <th>Action</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php
$startDate = strtotime($_GET['startdate']);
$endDate = strtotime($_GET['enddate']);
$interval = $endDate - $startDate;
$days = floor($interval / (60 * 60 * 24));

//echo $days . " until Christmas";
                        function time_to_decimal($time) {
                            $timeArr = explode(':', $time);
                            $decTime = (($timeArr[0]*60) + ($timeArr[1]) + ($timeArr[2]/60))/60;
                            return $decTime;
                        }
                        $sum = 0;
                        $sum2 = 0;
                        $sum3 = 0;
                        $overtime = 0;
                        $overtime3 = 0;
?>
 <?php 
$startDate =date('Y-m-d',strtotime($_REQUEST['startdate'])); 
$endDate = date('Y-m-d',strtotime($_REQUEST['enddate']));
    $resultDays = array('Monday' => 0, 
    'Tuesday' => 0, 
    'Wednesday' => 0, 
    'Thursday' => 0, 
    'Friday' => 0, 
    'Saturday' => 0, 
    'Sunday' => 0); 
  
    // change string to date time object 
    $startDate = new DateTime($startDate); 
    $endDate = new DateTime($endDate); 
    // iterate over start to end date 
    while($startDate <= $endDate ){ 
        // find the timestamp value of start date 
        $timestamp = strtotime($startDate->format('d-m-Y')); 
        $weekDay1 = date('Y-m-d', $timestamp);    
        // find out the day for timestamp and increase particular day 
       $weekDay = date('l', $timestamp); 
         if($weekDay == 'Sunday' || $weekDay == 'Saturday'){ ?> 
                     <tr bgcolor="lightgray" @if($weekDay == 'Sunday') class="div_<?php echo $weekDay1;?>" @endif @if($weekDay == 'Saturday') class="div1_<?php echo $weekDay1;?>" @endif>
                        <td>{{ date("M-d Y",strtotime($weekDay1))}}<br>{{ $weekDay }} </td>
                        <?php if($weekDay == 'Sunday' || $weekDay == 'Saturday') { ?> <td colspan="9"></td>
                          <td style="display: none;"></td>
                        <td style="display: none;"></td>
                        <td style="display: none;" ></td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td><?php }  
                        else { 
                        ?>
                        <?php 
                        }?>
                     </tr>  
                     <?php }?>
                      @foreach($employee2 as $emp)
                   @if($weekDay1==date("Y-m-d",strtotime($emp->emp_in_date)))
                    <?php
                        $in = date('H:i',strtotime($emp->emp_in));
                        $out = date('H:i',strtotime($emp->emp_out));
                        $lunch_in = date('H:i',strtotime($emp->launch_in));
                        $lunch_out = date('H:i',strtotime($emp->launch_out));
                        $lunch_in1 = date('H:i',strtotime($emp->launch_in_second));
                        $lunch_out1 = date('H:i',strtotime($emp->launch_out_second));
                        if($emp->emp_out==NULL)
                        {
                         $total ="00:00:00";
                        }
                        else
                        {
                          $total = date("H:i:s",strtotime($emp->total));  
                        }
                        if($emp->breaktime==NULL)
                        {
                        $breaktime ="00:00:00";
                        }
                        else
                        {
                        $breaktime = date("H:i:s",strtotime($emp->breaktime));
                        }
                        if($emp->breaktime==NULL)
                        {
                        $breaktime ="00:00:00";
                        }
                        else
                        {
                        $breaktime = date("H:i:s",strtotime($emp->breaktime));
                        }
                        if($emp->breaktime1==NULL)
                        {
                        $breaktime1 ="00:00:00";
                        }
                        else
                        {
                        $breaktime1 = date("H:i:s",strtotime($emp->breaktime1));
                        }
                        $total1 = time_to_decimal($total)-(time_to_decimal($breaktime)+time_to_decimal($breaktime1));
                        $sum2 +=$total1;
                        $time3 = "08.00";
                        if($total1>=$time3)
                        {
                          $overtime2 = $total1 - $time3;  
                          $overtime +=$overtime2;
                        }
                        else
                        {
                          $overtime="0.00";  
                        }
                        ?>
                     @endif
                     @endforeach
                      @foreach($employee3 as $emp)
                   @if($weekDay1==date("Y-m-d",strtotime($emp->emp_in_date)))
                    <?php
                        $in = date('H:i',strtotime($emp->emp_in));
                        $out = date('H:i',strtotime($emp->emp_out));
                        $lunch_in = date('H:i',strtotime($emp->launch_in));
                        $lunch_out = date('H:i',strtotime($emp->launch_out));
                        $lunch_in1 = date('H:i',strtotime($emp->launch_in_second));
                        $lunch_out1 = date('H:i',strtotime($emp->launch_out_second));
                        if($emp->emp_out==NULL)
                        {
                         $total ="00:00:00";
                        }
                        else
                        {
                          $total = date("H:i:s",strtotime($emp->total));  
                        }
                        if($emp->breaktime==NULL)
                        {
                        $breaktime ="00:00:00";
                        }
                        else
                        {
                        $breaktime = date("H:i:s",strtotime($emp->breaktime));
                        }
                        if($emp->breaktime==NULL)
                        {
                        $breaktime ="00:00:00";
                        }
                        else
                        {
                        $breaktime = date("H:i:s",strtotime($emp->breaktime));
                        }
                        if($emp->breaktime1==NULL)
                        {
                        $breaktime1 ="00:00:00";
                        }
                        else
                        {
                        $breaktime1 = date("H:i:s",strtotime($emp->breaktime1));
                        }
                        $total1 = time_to_decimal($total)-(time_to_decimal($breaktime)+time_to_decimal($breaktime1));
                        $sum3 +=$total1;
                        $time11 = "08.00";
                        if($total1>=$time11)
                        {
                         $overtime2 = $total1 - $time11; 
                        $overtime3 +=$overtime2;
                        }
                        else
                        {
                          $overtime3="00.00";  
                        }
                        ?>
                     @endif
                     @endforeach
                     
                     
                @foreach($employee1 as $emp)
                   @if($weekDay1==date("Y-m-d",strtotime($emp->emp_in_date)))
                    <?php
                        $in = date('H:i',strtotime($emp->emp_in));
                        $out = date('H:i',strtotime($emp->emp_out));
                        $lunch_in = date('H:i',strtotime($emp->launch_in));
                        $lunch_out = date('H:i',strtotime($emp->launch_out));
                        $lunch_in1 = date('H:i',strtotime($emp->launch_in_second));
                        $lunch_out1 = date('H:i',strtotime($emp->launch_out_second));
                        if($emp->emp_out==NULL)
                        {
                         $total ="00:00:00";
                        }
                        else
                        {
                          $total = date("H:i:s",strtotime($emp->total));  
                        }
                        if($emp->breaktime==NULL)
                        {
                        $breaktime ="00:00:00";
                        }
                        else
                        {
                        $breaktime = date("H:i:s",strtotime($emp->breaktime));
                        }
                        if($emp->breaktime==NULL)
                        {
                        $breaktime ="00:00:00";
                        }
                        else
                        {
                        $breaktime = date("H:i:s",strtotime($emp->breaktime));
                        }
                        if($emp->breaktime1==NULL)
                        {
                        $breaktime1 ="00:00:00";
                        }
                        else
                        {
                        $breaktime1 = date("H:i:s",strtotime($emp->breaktime1));
                        }
                        $total1 = time_to_decimal($total)-(time_to_decimal($breaktime)+time_to_decimal($breaktime1));
                        $sum +=$total1;
                        ?>
                        @if(date("l",strtotime($emp->emp_in_date)) == 'Sunday') <style>.div_<?php echo $weekDay1;?>{ display:none}</style> @endif
                         @if(date("l",strtotime($emp->emp_in_date)) == 'Saturday') <style>.div1_<?php echo $weekDay1;?>{ display:none}</style> @endif
                         @if(date("l",strtotime($emp->emp_in_date)) == 'Monday') <style>.div2_<?php echo $weekDay1;?>{ display:none}</style> @endif
                         @if(date("l",strtotime($emp->emp_in_date)) == 'Tuesday') <style>.div3_<?php echo $weekDay1;?>{ display:none}</style> @endif
                         @if(date("l",strtotime($emp->emp_in_date)) == 'Wednesday') <style>.div4_<?php echo $weekDay1;?>{ display:none}</style> @endif
                         @if(date("l",strtotime($emp->emp_in_date)) == 'Friday') <style>.div5_<?php echo $weekDay1;?>{ display:none}</style> @endif
                         @if(date("l",strtotime($emp->emp_in_date)) == 'Thursday') <style>.div6_<?php echo $weekDay1;?>{ display:none}</style> @endif
                     <tr>
                        <td>{{ date("M-d Y",strtotime($weekDay1))}}<br>{{ $weekDay}} </td>
                        <td  @if($emp->clock_in_status=='2') style="color:red" @endif>{{ date("g:i a", strtotime($in))}}</td>
                        <td  @if($emp->launch_in_status=='2') style="color:red" @endif>@if($emp->launch_in== null) --/-- @else {{ date("g:i a", strtotime($lunch_in))}}<br>{{date("g:i a", strtotime($lunch_out))}}@endif</td>
                        <td @if($emp->launch_in_status_1=='2') style="color:red" @endif>@if($emp->launch_in_second == null) --/-- @else {{ date("g:i a", strtotime($lunch_in1))}}<br>{{date("g:i a", strtotime($lunch_out1))}}@endif</td>
                        <td @if($emp->clock_out_status=='2') style="color:red" @endif>@if($emp->emp_out== null) <span style="color:red">Running</span> @else{{ date("g:i a", strtotime($out))}}@endif</td>
                        <td>@if($emp->emp_out== null) --/-- @else{{number_format($total1, 2)}}@endif</td>
                        <td>@if($emp->work_note== null)  @else <button type="button" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="{{$emp->work_note}} ">
 Preview
</button>@endif</td>
                        <td @if($emp->ip_address=='Manually Add') style="color:red" @endif>@if($emp->ip_address== null) --/-- @else{{$emp->ip_address}} @endif</td>
                        <td id="cur_{{$emp->id}}" style="width: 66px;"><center><a  role='button' data-toggle="modal" data-target="#myModal2_{{$emp->id}}" href="#" style="background: #1685cc;color: #fff;padding: 5px;border-radius: 5px;" data-id='{{$emp->id}}'><i class="fa fa-edit"></i></a>
                        <a href="#" id="{{$emp->id}}" class="delete" style="background:red;color: #fff;padding: 5px;border-radius: 5px;"><i class="fa fa-trash"></i></a></center>
                        </td>
                     </tr>
                   @endif
                     @endforeach 
                     <?php 
                        if($weekDay == 'Monday' || $weekDay == 'Tuesday' || $weekDay == 'Wednesday' || $weekDay == 'Thursday' || $weekDay == 'Friday'){ ?> 
                     <tr bgcolor="#C9A5A5" style="font-weight:bold;color:black" @if($weekDay == 'Monday') class="div2_<?php echo $weekDay1;?>" @endif @if($weekDay == 'Tuesday') class="div3_<?php echo $weekDay1;?>" @endif @if($weekDay == 'Friday') class="div5_<?php echo $weekDay1;?>" @endif @if($weekDay == 'Thursday') class="div6_<?php echo $weekDay1;?>" @endif @if($weekDay == 'Wednesday') class="div4_<?php echo $weekDay1;?>" @endif>
                        <td>{{ date("M-d Y",strtotime($weekDay1))}}<br>{{ $weekDay }} </td>
                        <?php if($weekDay == 'Monday' || $weekDay == 'Tuesday' || $weekDay == 'Wednesday' || $weekDay == 'Thursday' || $weekDay == 'Friday') { ?> <td colspan="8"></td><?php }  else { ?>
                        <?php } ?>
                          <td style="display: none;"></td>
                        <td style="display: none;"></td>
                        <td style="display: none;" ></td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
        </tr>  
                     <?php } ?>
                     <?php
        
        // increase startDate by 1 
        $startDate->modify('+1 day'); 
    } 
  
?><tr style="background:rgb(255,255,0)">
    <td colspan="3" style="width: 10%;background: #f5b605;"><?php if($days > '13'){ } else {?><span style="font-size: 16px;"><center>1st Week: Reg. Hrs. <b> <?php if($sum2>40){ $gg =$sum2-40.00; $gg3 =$sum2-$gg;?> {{number_format($gg3, 2)}} / OT Hrs. {{number_format($gg, 2)}}<?php }else{ ?>{{number_format($sum2, 2)}} / OT Hrs. 0.00<?php }?></b></center></span><?php }?></td>
                        <td style="width: 10%;" colspan="3"><span style="font-size: 16px;"><center>Total Hrs.: Reg. Hrs.<b> <?php  if($sum > 80){ $gg5 =$sum-80.00; $gg6 =$sum-$gg5; ?> {{number_format($gg6, 2)}} / OT Hrs. {{number_format($gg5, 2)}} <?php } else{?>{{number_format($sum, 2)}}<?php } ?></b></center></span></td>
                        <td style="width: 10%;background: #f5b605;" colspan="3"><?php if($days > '13'){ } else {?><span style="font-size: 16px;"><center>2nd Week: Reg. Hrs. <b><?php  if($sum3>40){ $gg1 =$sum3-40.00; $gg2 =$sum3-$gg1; ?> {{number_format($gg2, 2)}} / OT Hrs. {{number_format($gg1, 2)}}<?php } else{ ?>{{number_format($sum3, 2)}} / OT Hrs. 0.00 <?php }?> </b></center></span><?php }?></td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                        <td style="display: none;" ></td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                     </tr>
                     
                  </tbody>
               </table>
               <div><label for="checkbox"><input type="checkbox" name="checkshhet" id="checkbox"> I confirmed above timesheet is correct.</label></div>
               <a style="display:none" class="btn btn-success" id="img-clck">Send</a>
            </div>
         </div>
      </div>
   </div>
   <?php }?>
		
	</div>		
</div>

</div>
 </section>
</div>

<script>
$(document).ready(function () {
    var ckbox = $('#checkbox');

    $('input').on('click',function () {
        if (ckbox.is(':checked')) {
            $('#img-clck').show();
        } else {
            $('#img-clck').hide();
        }
    });
});
 $(function() {

    $('#min, #max').datepicker({
        showOn: "both",
        beforeShow: customRange,
        dateFormat: "mm-dd-yy",
    });

});

function customRange(input) {

    if (input.id == 'max') {
        var minDate = new Date($('#min').val());
        minDate.setDate(minDate.getDate() + 1)

        return {
            minDate: minDate

        };
    }

    return {}

}
</script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.22/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.debug.js"></script>
    <script src="https://www.aspsnippets.com/demos/scripts/table2excel.js"></script>
    <script type="text/javascript">
        $("body").on("click", "#img-clck", function () {
            html2canvas($('#sampleTable3')[0], {
                onrendered: function (canvas) {
                    var data = canvas.toDataURL();
                    var docDefinition = {
                        content: [{
                            image: data,
                            width: 500
                        }]
                    };
                    pdfMake.createPdf(docDefinition).download("Financial-Report.pdf");
                }
            });
        });
              $("body").on("click", "#btnExport", function () {
            html2canvas($('#sampleTable3')[0], {
                onrendered: function (canvas) {
                    var data = canvas.toDataURL();
                    var docDefinition = {
                        content: [{
                            image: data,
                            width: 500
                        }]
                    };
                    pdfMake.createPdf(docDefinition).download("Financial-Report.pdf");
                }
            });
        });
</script>  
 <script type="text/javascript">
    $(function () {
        $("#btnexcel").click(function () {
            $("#sampleTable3").table2excel({
                filename: "Financial-Report.xls"
            });
        });
    });
function printDiv() {
  var divToPrint = document.getElementById('sampleTable3');
  var newWin = window.open('', 'Print-Window');
  newWin.document.open();
  newWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</body></html>');
  newWin.document.close();
  setTimeout(function() {
    newWin.close();
  }, 10);
}
</script>

@endsection





