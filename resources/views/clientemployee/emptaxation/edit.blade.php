@extends('clientemployee.layouts.app')
@section('main-content')
<style>
   .taxation-radio-tab{ float:left; margin-left:20%; width:80%; }
   .taxation-radio-tab li{ float:left; list-style: none;padding-left: 22px; font-size: 20px;}
       .select2-container--default .select2-selection--single,.form-control{ background: #ccc !important;pointer-events: none;}
       .btn-primary1{pointer-events: none;}
</style>
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="page-title content-header">
      <h1>License</h1>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="box box-success">
               <div class="box-header">
                  <div class="box-tools pull-right">
                  </div>
               </div>
               <div class="col-md-12">
                  <form method="post" action="{{route('emptaxation.update',$position->id)}}" class="form-horizontal" enctype="multipart/form-data">
                     {{csrf_field()}}{{method_field('PATCH')}}
                     <div id="tabs">
                        <ul class="tabs taxation-radio-tab" style="display:none">
                           <!--<li id="tab-1"><label for="tab1"><input name="tab" id="tab1" type="radio" @if($position->type1=='Taxation') checked @endif value="Taxation" /> Taxation</label></li>-->
                           <li id="tab-2"><label for="tab2"><input name="tab" id="tab2" type="radio" @if($position->type1=='License') checked @endif  value="License" /> License</label></li>
                        </ul>
                        <div class="tab_container">
                           <!--<div id="forsale" class="tab_content" @if($position->type1=='License') style="display:none" @endif>
                           <div class="form-group {{ $errors->has('type') ? ' has-error' : '' }}">
                              <label class="control-label col-md-3">Type of Entity / Form  :</label>
                              <div class="col-md-4">
                                 <select class="form-control fsc-input" name="typeofservice" id="typeofservice">
                                   
                                    <option value="Type of Business" @if($position->type=='Type of Business') selected @endif>Type of Business</option>
                                    <option value="C Corporation" @if($position->type=='C Corporation') selected @endif>C Corporation</option>
                                    <option value="S Corporation"@if($position->type=='S Corporation') selected @endif>S Corporation</option>
                                    <option value="Single Member LLC" @if($position->type=='Single Member LLC') selected @endif>Single Member LLC</option>
                                    <option value="Double Member LLC" @if($position->type=='Double Member LLC') selected @endif>Double Member LLC</option>
                                 </select>
                                 @if ($errors->has('type'))
                                 <span class="help-block">
                                 <strong>{{ $errors->first('type') }}</strong>
                                 </span>
                                 @endif
                              </div>
                           </div>
                           
                            <div @if($position->type=='Type of Business') @else style="display:none" @endif class="form-group {{ $errors->has('business_catagory_name') ? ' has-error' : '' }}" id="business_catagory_name_2">
												<label class="control-label col-md-3">Type of Business : <span class="star-required"></span></label>
												<div class="col-md-4 ac">
													<div class="row">
														<div class="col-md-12">
															<select name="business_catagory_name" id="business_catagory_name" class="form-control fsc-input category1">
															<option value=''>---Select Business Category---</option>
															@foreach($category as $cate)
															<option value='{{$cate->id}}' @if($position->business_catagory_name==$cate->id) selected @endif>{{$cate->business_cat_name}}</option>
															@endforeach
															</select>
														</div>
													</div>
													@if ($errors->has('business_catagory_name'))
										<span class="help-block">
											<strong>{{ $errors->first('business_catagory_name') }}</strong>
										</span>
									@endif
												</div>
											</div>
                           <div class="form-group {{ $errors->has('question') ? ' has-error' : '' }}">
                              <label class="control-label col-md-3">Type of Form  :</label>
                              <div class="col-md-4">
                                 <input name="question" type="text" id="question" class="form-control" value="{{$position->question}}" />          
                              </div>
                           </div>
                           <div class="form-group {{ $errors->has('expiredate') ? ' has-error' : '' }}">
                              <label class="control-label col-md-3">Expire Date :</label>
                              <div class="col-md-4">
                                 <input name="expiredate" type="text" id="expiredate" class="form-control" value="{{$position->date}}" />          
                              </div>
                           </div>
                           <div class="card-footer">
                              <div class="col-md-2 col-md-offset-3">
                                 <input class="btn_new_save btn-primary1" style="margin-left:-5%" type="submit" name="submit" value="Save">
                              </div>
                              <div class="col-md-2 row">
                                 <a class="btn_new_cancel" style="margin-left:-5%" href="{{url('fac-Bhavesh-0554/taxation')}}">Cancel</a> 
                              </div>
                           </div>
                        </div>-->
                        <div id="wanted" class="tab_content">
                        <div class="form-group">
                           <label class="control-label col-md-3">Type of License :</label>
                           <div class="col-md-4">
                              <input type="text" class="form-control textonly" placeholder="Type of License" id="typeoflicense" name="typeoflicense" value="{{$position->question}}">
                           </div>
                        </div>
                        <div class="form-group">
                                 <label class="control-label col-md-3">Type of Bussiness :</label>
                                 <div class="col-md-4">
                                   <select class="form-control category" id="bussiness_name" name="bussiness_name">
                                    <option value=''>Please select Business</option>
                                    @foreach($business as $bus)								
									<option value='{{$bus->id}}'  @if($position->business_catagory_name==$bus->id) selected @endif>{{$bus->bussiness_name}}</option>								
                                    @endforeach
								</select>
                                 </div>
                              </div>
                               <div class="form-group {{ $errors->has('business_catagory_name') ? ' has-error' : '' }}">
							<label class="control-label col-md-3">Business Category Name :</label>
							<div class="col-md-4">
								<select class="form-control category1" id="business_catagory_name" name="business_catagory_name">
								<option value=""  disabled>Please select Business Category</option>	
								@foreach($category as $cate)
															<option value='{{$cate->id}}' @if($position->business_catagory_name==$cate->id) selected @endif>{{$cate->business_cat_name}}</option>
															@endforeach	
								</select>
@if ($errors->has('business_catagory_name'))
										<span class="help-block">
											<strong>{{ $errors->first('business_catagory_name') }}</strong>
										</span>
									@endif
							</div>
						</div>
                        <div class="form-group">
                           <label class="control-label col-md-3">Authority Level</label>
                           <div class="col-md-4">
                              <select class="form-control fsc-input" name="typeofservice1" id="typeofservice1">
                                 <option value=""> ---Select--- </option>
                                 <option value="City" @if($position->type=='City') selected @endif>City</option>
                                 <option value="County" @if($position->type=='County') selected @endif>County</option>
                                 <option value="Local" @if($position->type=='Local') selected @endif>Local</option>
                                 <option value="State" @if($position->type=='State') selected @endif>State</option>
                                 <option value="Federal" @if($position->type=='Federal') selected @endif>Federal</option>
                              </select>
                           </div>
                        </div>
                          <div class="form-group class="countyname" @if($position->type=='City')  @elseif($position->type=='County') @elseif($position->type=='Local') @elseif($position->type=='State') @elseif($position->type=='Federal') @else style="display:none" @endif>
                           <label class="control-label col-md-3">State </label>
                           <div class="col-md-4">
                              <select class="form-control bfh-states" id="bfh-states" name="state" data-country="USA" data-state="{{$position->state}}"></select>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-md-3">Authority Name</label>
                           <div class="col-md-4">
                              <input type="text" class="form-control" placeholder="Authority Name" value="{{$position->authorityname}}" id="authorityname" name="authorityname">
                           </div>
                        </div>
                        <div class="countyname" @if($position->type=='City')  @elseif($position->type=='County') @elseif($position->type=='Local') @elseif($position->type=='State') @elseif($position->type=='Federal') @else style="display:none" @endif>
                      
                        <div class="form-group city"  @if($position->type=='City') @elseif($position->type=='County')  style="display:none" @elseif($position->type=='Local')  style="display:none" @elseif($position->type=='State')  style="display:none" @elseif($position->type=='Federal') style="display:none" @else style="display:none" @endif>
                        <label class="control-label col-md-3">City Name </label>
                        <div class="col-md-4">
                           <input type="text" class="form-control" placeholder="City Name" id="cityname" value="{{$position->cityname}}" name="cityname">
                        </div>
                     </div>
                     <div class="form-group city1" @if($position->type=='City') @elseif($position->type=='County') @elseif($position->type=='Local')  style="display:none" @elseif($position->type=='State')  style="display:none" @elseif($position->type=='Federal') style="display:none"@else style="display:none" @endif>
                     <label class="control-label col-md-3">County Name</label>
                     <div class="col-md-4">
                        <select type="text" class="form-control" id="county" name="county">
                           <option value="{{$position->countyname}}">{{$position->countyname}}</option>
                           @foreach($taxstate as $v)
                           <option value="{{$v->county}}"  @if($v->county==$position->countyname) selected @endif>{{$v->county}}</option>
                           @endforeach
                        </select>
                     </div>
               </div>
               <div class="form-group city1"  @if($position->type=='City') @elseif($position->type=='County') @elseif($position->type=='Local')  style="display:none" @elseif($position->type=='State')  style="display:none" @elseif($position->type=='Federal') style="display:none" @else style="display:none" @endif>
               <label class="control-label col-md-3">County Number </label>
               <div class="col-md-4">
               <input type="text" class="form-control" placeholder="County Number" value="{{$position->countysnumber}}" id="countynumber" name="countynumber">
               </div>
            </div>
            <div class="form-group">
            <label class="control-label col-md-3">Website</label>
            <div class="col-md-4">
            <input type="text" class="form-control" placeholder="Website" id="website" value="{{$position->website}}" name="website">
            </div> 
            </div>
            <div class="form-group">
            <label class="control-label col-md-3">Telephone #</label>
            <div class="col-md-4">
            <input type="text" class="form-control phone" data-format=" (ddd) ddd-dddd" value="{{$position->telephone}}" placeholder="Telephone" id="telephone" name="telephone">
            </div> 
            </div>
            <div class="form-group">
            <label class="control-label col-md-3">Fax #</label>
            <div class="col-md-4">
            <input type="text" class="form-control phone" data-format=" (ddd) ddd-dddd" value="{{$position->fax}}" placeholder="Fax" id="fax" name="fax">
            </div> 
            </div>
            <div class="form-group">
            <label class="control-label col-md-3">Renew</label>
            <div class="col-md-4">
            <div class="renew-radio">
            <label for="yes"><input type="radio" name="renew" id="yes"  @if($position->renew=='Yes') checked @endif value="Yes"> Yes</label>
            <label for="no"><input type="radio" name="renew" id="no"  @if($position->renew=='No') checked @endif value="No"> No</label></div>
            </div> 
            </div>
            
              <div class="form-group"  @if($position->renew=='Yes') @else style="display:none" @endif id="renewal4">
            <label class="control-label col-md-3">Renewal-Website:</label>
            <div class="col-md-4">
                      <input type="text" class="form-control" value="{{$position->renewalwebsite}}" placeholder="Renewal Website" id="renewalwebsite" name="renewalwebsite">
            </div> 
         </div>
            <div class="form-group"  @if($position->renew=='Yes') @else style="display:none" @endif id="renewal1">
            <label class="control-label col-md-3">Renewal</label>
            <div class="col-md-4">
            <select type="text" class="form-control" id="renewal" name="renewal">
            <option value="">--Select--</option> / 
            <option value="Universal" @if($position->renewal=='Universal') selected @endif>Universal</option>
            <option value="Individual" @if($position->renewal=='Individual') selected @endif>Individual</option>
            </select>
            </div> 
         </div>
         
        
         
         <div class="form-group"  @if($position->renew=='Yes') @else style="display:none" @endif @if($position->renewal=='Universal') @else style="display:none" @endif  id="renewal2">
         <label class="control-label col-md-3">License Expire Date</label>
         <div class="col-md-2">
         <select type="text" class="form-control" id="duedate" name="duedate">
         <option value="">Month</option>
         <?php
            for ($i = 1; $i <= 12; $i++)
            {
                $month_name = date('F', mktime(0, 0, 0, $i, 1, 2011));?>
         <option value="<?php echo $month_name;?>" @if($position->date==$month_name) selected @endif><?php echo $month_name;?></option>
         <?php }
            ?>
         </select>
         </div>
         <div class="col-md-2">
         <?php
            $date = '2003-09-01';
            $end = '2003-09-' . date('t', strtotime($date)); //get end date of month
            ?>
         <select type="text" class="form-control" id="duedate1" name="duedate1">
         <option value="">Date</option> 
         <?php while(strtotime($date) <= strtotime($end)) {
            $day_num = date('d', strtotime($date));
            $day_name = date('l', strtotime($date));
            $date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
             // echo "<td>$day_num <br/> $day_name</td>";
            ?>
         <option value="<?php echo $day_num;?>" @if($position->duedate1==$day_num) selected @endif><?php echo $day_num;?></option> <?php
            }?>
         </select>
         </div>
      </div>
      <div class="form-group"  @if($position->renew=='Yes') @else style="display:none" @endif @if($position->renewal=='Universal') @else style="display:none" @endif id="renewal3">
      <label class="control-label col-md-3">Renewal Due Date</label>
      <div class="col-md-2"> 
      <select type="text" class="form-control" id="expiredate" name="expiredate">
      <option value="">Month</option>
      <?php
         for ($i = 1; $i <= 12; $i++)
         {
             $month_name = date('F', mktime(0, 0, 0, $i, 1, 2011));?>
      <option value="<?php echo $month_name;?>" @if($position->duedate==$month_name) selected @endif><?php echo $month_name;?></option>
      <?php
         }
         ?>
      </select>
      </div>
      <?php
         $date = '2003-09-01';
         $end = '2003-09-' . date('t', strtotime($date)); //get end date of month
         ?>
      <div class="col-md-2">
      <select type="text" class="form-control" id="expiredate2" name="expiredate2">
      <option value="">Date</option> 
      <?php while(strtotime($date) <= strtotime($end)) {
         $day_num = date('d', strtotime($date));
         $day_name = date('l', strtotime($date));
         $date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
         // echo "<td>$day_num <br/> $day_name</td>";
         ?>
      <option value="<?php echo $day_num;?>" @if($position->expiredate2==$day_num) selected @endif><?php echo $day_num;?></option> <?php
         }?>
      </select>
      </div>
</div>
</div>
<div class="card-footer">
<div class="col-md-2 col-md-offset-3">
<input class="btn_new_save btn-primary1" style="margin-left:-5%" type="submit" name="submit" value="Save">
</div>
<div class="col-md-2 row">
<a class="btn_new_cancel" style="margin-left:-5%" href="{{url('fscemployee/emptaxation')}}">Cancel</a> 
</div>
</div>
</div>
</div>
</div>									
</form>
</div>
</div>
</div>
</div>
</section>	
</div>
<script>
   $(document).ready(function () {
   $('input[type=radio][name=tab]').on('change', function() {
       if (this.value == 'Taxation') { 
          $('#wanted').hide();
        $('#forsale').show();
      }
      else if (this.value == 'License') {
          $('#wanted').show();
        $('#forsale').hide();
      }
   });
   });
      $(document).ready(function () {
   $('#typeofservice').on('change', function() {
       if (this.value == 'Type of Business') { 
          
        $('#business_catagory_name_2').show();
      }
      else  {

        $('#business_catagory_name_2').hide();
      }
   });
   });
   $(document).ready(function(){
    $(document).on('change','#renewal',function(){
       var valu = $(this).val();
       if(valu=='Universal')
       {
         $('#renewal2').show();
               $('#renewal3').show();                 $('#renewal4').show();  
       }
       else{
        $('#renewal2').hide();
               $('#renewal3').hide(); $('#renewal4').hide(); 
       }
    });
    
})
   $(document).ready(function(){
      $('#typeofservice1').on('change',function(){
          var id = this.value;
          if(id=='City')
          {
           $('.city').show();
           $('.city1').show();
             $('.countyname').show();
             $('.state').hide();
             $('.faderal').hide();
          }
          else if(id=='County')
          {
              $('.state').hide();
              $('.city1').show();
              $('.countyname').show();
              $('.city').hide();
              $('.faderal').hide();
          }
          else if(id=='Local')
          {
       $('.city').hide();
               $('.countyname').show();
             $('.state').hide();
             $('.city1').hide();
             $('.faderal').show();
          }
           else if(id=='State')
          {
              $('.city').hide();
               $('.countyname').show();
               $('.city1').hide();
             $('.state').show();
             $('.faderal').hide();
          }
           else if(id=='Federal')
          {
             // alert('federal');
              $('.city').hide();
               $('.countyname').show();
             $('.state').hide();
             $('.city1').hide();
             $('.faderal').show();
          }
          else
          {
               $('#city').hide();
             $('.countyname').hide();
             $('.state').hide();
              $('.faderal').hide();
          }
      });
   });
</script>
<script>
   $(document).ready(function(){
        $(document).on('click','#yes',function(){
           var value = $(this).val();
           if(value=='Yes')
             {
                  $('#renewal1').show();
                  $('#renewal2').show();$('#renewal4').show();
                  $('#renewal3').show();
               }
               else{
                   $('#renewal1').hide();
               }
       });
   });
    $(document).ready(function(){
       $(document).on('click','#no',function(){
           
           var value = $(this).val();
           
           if(value=='No')
             {
                  $('#renewal1').hide();
                  $('#renewal2').hide();$('#renewal4').hide();
                  $('#renewal3').hide();
               }
               else{
                   $('#renewal2').hide();
               }
       });
   });
   $(document).ready(function(){
   	$(document).on('change','#county', function()
   	{
   		var id = $(this).val();
   		$.get('{!!URL::to('getcountycod')!!}?id='+id, function(data)
   		{  
           $('#countynumber').empty();
              $.each(data, function(index, subcatobj)
   		   {
              $('#countynumber').val(subcatobj.countycode);
   		   })
   		});
   	});
   });
</script>
<script>
   $(document).ready(function(){
   	$(document).on('change','#bfh-states', function()
   	{        
   		var id = $(this).val(); //`alert(id);
   		$.get('{!!URL::to('getcountcount')!!}?id='+id+'&state=' + id, function(data)
   		{  
               $('#county').empty();
               $('#county').append('<option value="">Select</option>');
              
              $.each(data, function(index, subcatobj)
   		   {
   		            $('#county').append('<option value="'+subcatobj.county+'">'+subcatobj.county+'</option>');
   		   })
   		});
   	});
   });
    $(document).ready(function(){
     /***phone number format***/
     $(".phone").keypress(function (e) {
       if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
         return false;
       }
       var curchr = this.value.length;
       var curval = $(this).val();
       if (curchr == 3 && curval.indexOf("(") <= -1) {
         $(this).val("(" + curval + ")" + " ");
       } else if (curchr == 4 && curval.indexOf("(") > -1) {
         $(this).val(curval + ")-");
       } else if (curchr == 5 && curval.indexOf(")") > -1) {
         $(this).val(curval + "-");
       } else if (curchr == 9) {
         $(this).val(curval + "-");
         $(this).attr('maxlength', '14');
       }
     });
      // $('#expiredate,#expiredate,#duedate').datepicker();
   });
</script>
<script>
$(document).ready(function(){
	$(document).on('change','.category', function()
	{
		//console.log('htm');
		var id = $(this).val();
		$.get('{!!URL::to('getRequest')!!}?id='+id, function(data)
		{  $('#business_catagory_name').empty();
           $.each(data, function(index, subcatobj)
		   {
			   $('#business_catagory_name').append('<option value="'+subcatobj.id+'">'+subcatobj.business_cat_name+'</option>');
		   })

		});
			
	});
});
</script>
@endsection()