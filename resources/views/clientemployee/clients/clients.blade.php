@extends('clientemployee.layouts.app')
@section('main-content')
<style>
label{float:left;}
.active-green-color-bg{ background:#689203 !important; }
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header page-title" style="height:50px;">
     		<div class="row col-md-12">
     		    <div class="col-md-7" style="text-align:right;">
     		        <h1>List of Client</h1>
     		    </div>
     		    <div class="col-md-5" style="text-align:right;">
     		        <h1> View / Edit</h1>
     		    </div>
     		</div>
    </section>
        	<!-- Main content -->
    <section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-success">
			     <div class="box-header">
              <div class="box-tools pull-right">
              </div>
            </div>
				<div class="col-md-12">
					@if(session()->has('success'))
                       <div class="alert alert-success alert-dismissable">{{session()->get('success')}}</div>
                    @endif
					<div class="table-responsive">
						<table class="table table-hover table-bordered" id="example">
							<thead>
								<tr>
									<th>No</th>
									<th>Client No</th>
									<th>Client Company Name</th>
									<th>Type of Business</th>
									<th>Contact Name</th>
									<th>Telephone #</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>							
							<tbody>
                            @foreach($common as $com)
								<tr>
									<td style="text-align:center;">{{$loop->index+1}}</td>
									<td>{{$com->filename}}</td>
									<td>{{$com->company_name}}</td>
									<td>{{$com->bussiness_name}}</td>
									<td>{{$com->first_name}} {{$com->middle_name}} {{$com->last_name}}</td>
									<td>{{$com->business_no}}</td>
							    	<td style="text-align:center"> @if($com->status=='Active')<a style="display:none" class="btn-action  btn-view-edit" href="#">2</a> <a style="background:#689203 !important; text-align:center" class="btn-action  btn-view-edit" href="#">Active</a> @elseif($com->status=='Approval')<a style="display:none" class="btn-action  btn-view-edit" href="#">3</a><a style="background-color: Yellow !important; text-align:center;color:#000 !important" class="btn-action  btn-view-edit" href="#">Approval</a>  @elseif($com->status=='Pending')<a style="display:none" class="btn-action  btn-view-edit" href="#">4</a><a style="background-color: Orange !important; text-align:center" class="btn-action  btn-view-edit" href="#">Pending</a> @elseif($com->status=='Hold')<a style="display:none" class="btn-action  btn-view-edit" href="#">5</a> <a class="btn-action btn-delete" href="#">Hold</a> @else <a style="display:none" class="btn-action  btn-view-edit" href="#">6</a> <a class="btn-action btn-delete" href="#">Inactive</a>  @endif</td>
									<td style="text-align:center;">@if($com->newclient==1)<a class="" href="{{route('clients.edit',$com->cid)}}"><img src="{{asset('public/dashboard/images/newimage.gif')}}" alt="" width="50px"></a><br>@endif
                                         <a class="btn-action btn-view-edit" href="{{route('clients.edit',$com->cid)}}"><i class="fa fa-edit"></i></a>
										<a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record ?')) {event.preventDefault();document.getElementById('delete-id-{{$com->cid}}').submit();} else{event.preventDefault();}" href="{{route('clientsetup.destroy',$com->cid)}}"><i class="fa fa-trash"></i></a>
<form action="{{ route('clientsetup.destroy',$com->cid) }}" method="post" style="display:none" id="delete-id-{{$com->cid}}">
                                        {{csrf_field()}} {{method_field('DELETE')}}
                                        </form>
									</td>
								</tr>
							@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	 </section>
</div>
<script>
$(document).ready(function() {
    var table = $('#example').DataTable( {
        dom: 'Bfrtip',
    "columnDefs": [ {
            "searchable": true,
            "orderable": true,
            "targets": [0]
        } ],
      "order": [[ 2, 'asc' ]],
        buttons: [{
                extend:'copyHtml5',
                text:'<i class="fa fa-files-o"></i> &nbsp; Copy',
                //titleAttr: 'Copy',
                title: $('h2').text(),
                exportOptions: {
                   columns: [0,1,2,3,4,5,6,7], // Only name, email and role
    }
            },
            {
                extend:'excelHtml5',
                text:'<i class="fa fa-file-excel-o"></i>&nbsp; Excel',
               // titleAttr: 'Excel',
                title: $('h2').text(),
                 customize: function( xlsx ) {
                var sheet = xlsx.xl.worksheets['sheet1.xml'];
$('row c[r^="A"]',sheet).attr('s','51'); 
                       //   $('row c[r^="C"]',sheet).attr('s','51'); 
                          $('row:first c',sheet).attr('s','51');
            },
                exportOptions: {
                columns: [0,1,2,3,4,5], // Only name, email and role
    }
            },
            {
                extend:'csvHtml5',
                text:'<i class="fa fa-file-text-o"></i> &nbsp; CSV',
                title: $('h2').text(),
                exportOptions: {
                columns: [0,1,2,3,4,5,6,7], // Only name, email and role
    }
            },
            {
                extend:'pdfHtml5',
                text:'<i class="fa fa-file-pdf-o"></i>&nbsp;  PDF',
                customize: function (doc) {
						//Remove the title created by datatTables
						doc.content.splice(0,1);
						//Create a date string that we use in the footer. Format is dd-mm-yyyy
						var now = new Date();
						var jsDate = now.getDate()+'-'+(now.getMonth()+1)+'-'+now.getFullYear();
						var logo = 'data:image/jpeg;base64,{{$logo->logourl}}';
						doc.pageMargins = [20,60,20,20];
						doc.defaultStyle.fontSize = 10;
						doc.styles.tableHeader.fontSize = 10;
						doc['header']=(function() {
							return {
								columns: [{
									    alignment: 'center',
										image: logo,
										width:60,margin: [10,10,-230,10]
									},{
										alignment: 'center',
										text: 'List of Client',
										fontSize: 12,
										margin: [-110,45,0,10],
									},],
								margin: [20, 0, 0,52],alignment: 'center',
							}
						});
						var objLayout = {};
						objLayout['hLineWidth'] = function(i) { return 2; };
						objLayout['vLineWidth'] = function(i) { return 2; };
						objLayout['hLineColor'] = function(i) { return '#ccc'; };
						objLayout['vLineColor'] = function(i) { return '#ccc'; };
						objLayout['paddingLeft'] = function(i) { return 14; };
						objLayout['paddingRight'] = function(i) { return 14; };
						doc.content[0].layout = objLayout;
				},
                exportOptions: {
                columns: [0,1,2,3,4,5], // Only name, email and role
    }
            },
            {
           extend: 'print',
           text: '<i class="fa fa-print"></i>&nbsp; Print',
           customize: function ( win ) 
           {
                    $(win.document.body)
                        .css( 'font-size', '10pt' )
                        .prepend(
                            '<center><img src="https://financialservicecenter.net/public/business/{{$logo->logo}}"/><br style="text-align:center;">FSC Client</center>'
                        );
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );
                },
               title: $('h2').text(), 
           exportOptions: {
            columns: [0,1,2,3,4,5],
      },
      footer: true,
      autoPrint: true
    },
        ],
        
     
    } );
$('input.global_filter').on('keyup click', function() {
        filterGlobal();
    } );
 
    $('input.column_filter').on( 'keyup click', function() {
        filterColumn( $(this).parents('tr').attr('data-column'));
    } );
  table.on( 'order.dt search.dt', function () {
       table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
          cell.innerHTML = i + 1;
          table.cell(cell).invalidate('dom'); 
       } );
    } ).draw();
    table.columns(7)
        .search('^(?:(?!6|3|4|5|1).)*$\r?\n?', true, false)
        .draw();
        
    $("#choice").on("change",function(){
 var _val = $(this).val();//alert(_val);
 
 if(_val == '6'){   
        table.columns(7).search(_val).draw();
        
  }
  else if(_val == '2'){  
  table.columns(7).search(_val).draw();
//  table.columns(8).search(_val).draw();
   }
  else if(_val == '3'){  //alert();
         table.columns(7).search(_val).draw();
  }
   else if(_val == '4'){  //alert();
         table.columns(7).search(_val).draw();
  }
  else if(_val == '5'){  //alert();
         table.columns(7).search(_val).draw();
  }
  else{
        table.columns().search('').draw(); 
  }
  });
    $("#choice1").on("change",function(){
 var _val = $(this).val();//alert(_val);
 if(_val == '1'){   
          table.columns().search('').draw(); 
  }   
   @foreach($common as $com)
else if(_val == '{{$com->business_name}}'){   
        table.columns(3).search(_val).draw();
  }
  @endforeach
  else{
        table.columns().search('').draw(); 
  }
  })
});
function filterGlobal() {
    $('#example').DataTable().search(
        $('#global_filter').val(),
        $('#global_regex').prop('checked'),
        $('#global_smart').prop('checked')
    ).draw();
}
function filterColumn(i) {
    $('#example').DataTable().column( i ).search(
        $('#col'+i+'_filter').val(),
        $('#col'+i+'_regex').prop('checked'),
        $('#col'+i+'_smart').prop('checked')
    ).draw();
}
$("#types").on('change',function() {
  // For unique choice
  var selVal = $( "#types option:selected" ).val(); 
   if(selVal=='Type')  
  {
      $('#filter_global').hide();
      $('#filter_col3').hide();
      $('#filter_col4').hide();
      $('#filter_col5').hide();
      $('#filter_col6').hide();
      $('#filter_col7').hide();
      $('#filter_col2').show();
  }
  else if(selVal=='EE / User ID')  
  {
        $('#filter_col7').hide();
      $('#filter_global').hide();
      $('#filter_col2').hide();
      $('#filter_col4').hide();
      $('#filter_col5').hide();
      $('#filter_col6').hide();
      $('#filter_col3').show();
  }
    else if(selVal=='Employee Name')  
  {
      $('#filter_global').hide();
      $('#filter_col2').hide();
      $('#filter_col3').hide();
      $('#filter_col5').hide();
      $('#filter_col6').hide();
      $('#filter_col4').show();  
      $('#filter_col7').hide();
  }
   else if(selVal=='Email ID')  
  {
      $('#filter_global').hide();
      $('#filter_col2').hide();  
      $('#filter_col7').hide();
      $('#filter_col3').hide();
      $('#filter_col4').hide();
      $('#filter_col6').hide();
      $('#filter_col5').show();
  }
  else if(selVal=='Tel. Number')  
  {
      $('#filter_global').hide();
      $('#filter_col2').hide();
      $('#filter_col3').hide();
      $('#filter_col4').hide();
      $('#filter_col5').hide();  
      $('#filter_col7').hide();
      $('#filter_col6').show();
  }
  else if(selVal=='Tel. Number')  
  {
      $('#filter_global').hide();
      $('#filter_col2').hide();
      $('#filter_col3').hide();
      $('#filter_col4').hide();
      $('#filter_col5').hide();  
      $('#filter_col6').hide();
      $('#filter_col7').show();
  }
  else{
      $('#filter_global').show();
       $('#filter_col3').hide();
      $('#filter_col4').hide();
      $('#filter_col5').hide();
      $('#filter_col6').hide();  
      $('#filter_col7').hide();
      $('#filter_col2').hide();
  }
});
</script>
@endsection()