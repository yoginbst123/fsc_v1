@extends('clientemployee.layouts.app')
@section('title', 'Client Report')
@section('main-content') 
<style>
.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{padding:0px !important;vertical-align:inherit !important;}
.headerclr{background:#ffff99 !important;}
.table > thead > tr > th{background:none !important;}
.addagemodal .form-control:focus, .addagemodal .form-control:hover{background:#ffff99;}
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h2>List of FSC Client Report</h2>
    </section>
    <section class="content">
   <div class="row">
      <div class="col-md-12">
        <div class="box box-success">
			      <div class="box-header">
              <div class="box-tools pull-right">
              </div>
            </div>
				<div class="col-md-12 col-md-offset-1">
               <form enctype='multipart/form-data' class="form-horizontal changepassword" action="{{route('empclientreport.index')}}" id="changepassword"  method="GET">
                  {{csrf_field()}}
                  <div class="row">
                     <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group ">
                           <label for="focusedinput" class="col-md-3 control-label">Client : <span class="required"> * </span></label>
                           <div class="col-sm-4">
                              <div class="">
                                 <select class="form-control" name="status" id="status">
                                    <option value="1">All</option>
                                    <option value="Active">Active</option>
                                    <option value="Inactive">InActive</option>
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="form-group ">
                           <label for="focusedinput" class="col-md-3 control-label">Type of Client : <span class="required"> * </span></label>
                           <div class="col-sm-4">
                              <div class="">
                                 <select class="form-control" name="emp_name" id="emp_name" required>
                                    <option value="">Select</option>
                                     @foreach($business as $cate)
                                          <option value='{{$cate->id}}'>{{$cate->bussiness_name}}</option>
                                     @endforeach
                                   </select>
                              </div>
                           </div>
                        </div>
                        <div class="form-group ">
                           <label for="focusedinput" class="col-md-3 control-label">Type of Service : <span class="required"> * </span></label>
                           <div class="col-sm-4">
                              <div class="">
                                 <select class="form-control" name="monthlytype" id="monthlytype" required>
                                    <option value="">Select</option>
                                    @foreach($period as $period1)
                                    <option value="{{$period1->id}}">{{$period1->period}}</option>
                                    @endforeach
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="form-group ">
                           <label for="focusedinput" class="col-md-3 control-label"></label>
                           <div class="col-sm-8" style="margin:0 !important;padding:0 !important;">
                              <div class="col-sm-3">
                                 <button class="btn-success btn" type="submit" style="width: 50%;" name="search"> Ok</button>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  </form>
                  </div>
<div class="col-md-12 col-sm-12 col-xs-12">  
<?php if(!empty($_REQUEST['emp_name'])){?>
<div class="table-responsive"><br>
<center><img src="https://financialservicecenter.net/public/dashboard/images/fsc_logo.png" width="80px" class="img-responsive"></center>
<br>
<table class="table table-hover table-bordered" id="sampleTable3">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Client #</th>
      <th scope="col">Client Legal Name</th>
      <th scope="col">DBA Name</th>
      <th scope="col">Contact Person name</th>
      <th scope="col">Tel #</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
      <?php $count= count($client); if($count>0){?>
    @foreach($client as $client1)
    <tr>
      <th scope="row">{{$loop->index+1}}</th>
      <td>{{$client1->filename}}</td>
      <td>{{$client1->company_name}}</td>
      <td>{{$client1->business_name}}</td>
      <td>{{$client1->firstname.' '.$client1->middlename.' '.$client1->lastname}}</td>
      <td>{{$client1->business_no}}</td>
      <td><a class="btn-action btn-view-edit btn-primary" style="background:#367fa9 !important" href="#"><i class="fa fa-edit"></i></a></td>
    </tr>
    @endforeach
    <?php } 
    else
    { ?>
 <tr>
      <th scope="row"></th>
      <td></td>
      <td></td>
      <td>Not found data</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <?php }?>
  </tbody>
</table>
            </div>
            <?php }?>
                </div>
         </div>
         <br>
      </div>
   </div>
</div>
<script>
$.ajaxSetup({
    headers:
    {
        'X-CSRF-Token': $('input[name="_token"]').val()
    }
});
</script>
<style>
.table > thead > tr > th {background: #98c4f2;text-align: center;}
td{text-align: center !important;}
.form-group {margin-bottom: 15px;width: 100%;float: left;}
   </style>
@endsection