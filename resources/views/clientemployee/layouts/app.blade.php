<!DOCTYPE html>
<html lang="lang="{{ app()->getLocale() }}">
<head>
@include('clientemployee.layouts.head')	
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
@include('clientemployee.layouts.header')
    @section('main-content')
    @show
@include('clientemployee.layouts.leftsidebar')
@include('clientemployee.layouts.footer')
</div>
</body>
</html>
