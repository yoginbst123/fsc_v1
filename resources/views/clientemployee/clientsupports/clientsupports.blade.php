@extends('clientemployee.layouts.app')
@section('title', 'Technical Support')
@section('main-content') 
 <div class="content-wrapper">
        	
	<section class="content-header page-title" style="height:50px;">
     		<div class="row col-md-12">
     		    <div class="col-md-7" style="text-align:right;">
     		        <h1>List of Technical Support</h1>
     		    </div>
     		    <div class="col-md-5" style="text-align:right;">
     		        <h1>View / Edit</h1>
     		    </div>
     		</div>
    </section>
	<section class="content" style="background-color: #fff;">
	<div class="row">
	
		<div class="col-md-12">
			<div class="card">
				<div class="card-body">
				
					<div class="table-responsive">
						<table class="table table-hover table-bordered" id="sampleTable3">
						    <thead>
								<tr>
									<th>No</th>
									<th>Subject</th>
									<th>Details</th>
									<th>Status</th>
									<th>View</th>
								</tr>
							</thead>							
							<tbody>
                           @foreach($rules as $rul)
                           <tr>
                               <td style="text-align:center;">{{$loop->index+1}}</td>
                               <td>{{$rul->subject}}</td>
                               <td style="width:60%">{!!$rul->details!!}</td>
                               <td style="text-align:center;">@if($rul->answer=='') <a href="#" class="btn btn-danger" style="padding: 6px 12px;">Pending</a> @else <a href="#" style="padding: 6px 12px;" class="btn btn-success">Done</a> @endif</td>
                               <td style="text-align:center;"><a class="btn-action btn-view-edit" href="{{route('clientsupports.edit',$rul->id)}}"><i class="fa fa-edit"></i></a></td>
                            
                           </tr>
                           @endforeach
							</tbody>
							
						</table>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	</section>
</div>
@endsection





