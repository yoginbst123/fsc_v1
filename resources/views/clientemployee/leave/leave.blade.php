@extends('clientemployee.layouts.app')
@section('title', 'Leave')
@section('main-content') 
 <div class="content-wrapper">
	<section class="content-header page-title" style="height:50px;">
     		<div class="row col-md-12">
     		    <div class="col-md-7" style="text-align:right;">
     		        <h1>List of Leave</h1>
     		    </div>
     		    <div class="col-md-5" style="text-align:right;">
     		        <h1>Add / View / Edit</h1>
     		    </div>
     		</div>
    </section>
	 <section class="content" style="background-color: #fff;">
	<div class="row">
	
		<div class="col-md-12">
			<div class="card">
			    <div class="table-title">
						<a href="{{url('fscemployee/leave/create')}}">Add New Leave</a>
						<br><br>
					</div>
				<div class="card-body">
					
					@if(session()->has('success') )
    <div class="alert alert-success alert-dismissable">{{session()->get('success') }}</div>
                   @endif
					<div class="table-responsive">
						<table class="table table-hover table-bordered" id="sampleTable3">
						
							<thead>
								<tr>
									<th>No</th>
									<th>Start Date</th>
									<th>Total Days</th>
									<th>End Date</th>
										<th>Leave Reason</th>
									<th>Leave Status</th>
									
											<th>Creation Date</th>
									<th>Action</th>
								</tr>
							</thead>							
							<tbody>
                            @foreach($leave as $com)
								<tr>
									<td style="text-align:center;">{{$loop->index+1}}</td>
									<td style="text-align:center;">{{$com->start_date}}</td>
									<td style="text-align:center;">{{$com->total_days}}</td>
									<td style="text-align:center;">{{$com->end_date}}</td>
									<td style="width:40%;">{!! $com->leave_reason !!}</td>
									<td style="text-align:center;">@if($com->status=='1') <p style="color:#eaaf2a">Pending</p> @elseif($com->status=='2') <p style="color:blue">Approve</p> @else <p style="color:red">Cancel</p> @endif</td>
									<td style="text-align:center;">{{$com->creation_date}}</td>
									<td style="text-align:center;"><a class="btn-action btn-view-edit" href="{{route('leave.edit',$com->id)}}"><i class="fa fa-edit"></i></a></td>
								</tr>
							@endforeach
							</tbody>
							
						</table>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	</section>
</div>
@endsection





