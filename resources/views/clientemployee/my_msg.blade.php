@extends('clientemployee.layouts.app')
@section('main-content') 
<div class="content-wrapper">
    <div class="page-title">
      <h1>Search</h1>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="card">
        <div class="card-body">
        <form enctype='multipart/form-data' class="form-horizontal changepassword" action="http://financialservicecenter.net/user/userchangepassword/3" id="changepassword"  method="post">
          <input type="hidden" name="_token" value="0gi3MyyDqk1C5PGpCflmG1lmlber6a1hnZ21Gajj">
          <input type="hidden" name="_method" value="PATCH">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="form-group ">
                <label for="focusedinput" class="col-md-3 control-label">Search By Message: <span class="required"> * </span></label>
                <div class="col-md-4">
                  <input name="searchtext" value="" placeholder="" class="form-control" id="searchtext" type="text">
                </div>
                <div class="col-md-4">
                  <button class="btn-success btn" type="submit" name="search"> Search</button>
                </div>
              </div>
            </div>
          </div>
          </div>
          </div>
          <br>
        </form>
      </div>
    </div>
    <br>
    <div class="page-title">
      <h1>Message Details</h1>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="portlet box blue">
          <div class="portlet-title">
            <div class="caption"> Message Details </div>
          </div>
          <div class="portlet-body">
            <div class="table-responsive">
              <div id="errorlist"></div>
              <table class="table table-bordered" id="myTable">
                <thead>
                  <tr>
                    <th onclick="sortTable(0)">Assign By <a href="javascript:void(0);" class="arrow_list down_arrow"></a></th>
                    <th>Subject</th>
                    <th>Creation Date</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td></td>
                    <td>FDFDFD</td>
                    <td>Dec-22-2017</td>
                    <td class="action"><a class="on-default view-row" onclick="status_call('','48')" href="empmessage.php?id=48"> <img src="images/View.png" alt=""></a> <a class="on-default view-row" onclick="return confirm('Are you sure?')" href="viewempmessage.php?id=48"> <img src="images/Delete.png" alt=""></a></td>
                  </tr>
                </tbody>
              </table>
              <table>
                <tfoot>
                  <tr>
                    <td colspan="6"><div class="footer_mid" style="width:80%;">
                        <div class="footer_right">
                          <div class="footer_left" style="height:34px; ">
                            <table cellspacing="0" cellpadding="0" border="0">
                              <tbody>
                                <tr align="left">
                                  <td colspan="5"><p style="font-weight:bold;font-size:14px;">Showing 1 of 1 Record</p></td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div></td>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection





