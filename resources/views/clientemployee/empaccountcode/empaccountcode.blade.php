@extends('clientemployee.layouts.app')
@section('main-content')
<style>
label{float:left;}
</style>
<div class="content-wrapper">
	 <!-- Content Header (Page header) -->
    <section class="content-header page-title" style="height:50px;">
     		<div class="row col-md-12">
     		    <div class="col-md-7" style="text-align:right;">
     		        <h1>List of Account Code</h1>
     		    </div>
     		    <div class="col-md-5" style="text-align:right;">
     		        <h1>Add / View / Edit</h1>
     		    </div>
     		</div>
    </section>
    <!-- Main content -->
    <section class="content">
	<div class="row">
	    <div class="col-md-12">
<div class="box box-success">
   <div class="box-header">
      <div class="col-md-9" style="margin-left:-1.6%">
         <div class="row">
        
         </div>
      </div>
      <div class="col-md-3">
         <div class="table-title">
            <!--<a href="#">Add New</a>-->
         </div>
         <br>
         <br>
      </div>
   </div>
   <div class="col-md-12">
      <div class="table-title">
      </div>
      @if(session()->has('success'))
         <div class="alert alert-success alert-dismissable">{{session()->get('success') }}</div>
      @endif
      <div class="table-responsive">
						<table class="table table-hover table-bordered" id="sampleTable3">
							<thead>
								<tr>
									<th style="width:7%;">No</th>
									<th style="width:15%">Account Code</th>
									<th>Account Name</th>
                 					<th>Account Belongs</th>
                 					<th style="width:10%;text-align:center;">Action</th>
								</tr>
							</thead>
							<tbody>
                                @foreach($accountcode as $bus)
								<tr>
									<td>{{$loop->index + 1}}</td>
									<td>{{$bus->accountcode}}</td>
									<td>{{$bus->account_name}}</td>	
                                    <td>{{$bus->account_belongs_to}}</td>	
	                                <td style="text-align:center;"><a class="btn-action btn-view-edit" href="{{route('empaccountcode.edit', $bus->id)}}"><i class="fa fa-edit"></i></a></td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
      </div>
</div>
</div>	
	</div>
	</section>
</div>
	<style>.content-wrapper {height: 100%;}</style>
@endsection()