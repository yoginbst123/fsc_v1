<!-- header-->
<div class="agileits_main jarallax">
  <div class="container-fluid">
  <div class="w3ls-header">
   <div class="w3layouts-header-top">
  	<div class="w3-header-top-grids">
	 <div class="w3-header-top-left">
	 						<div class="agileinfo-social-grids">
							<ul>
								<li><a href="#"><span class="fa fa-facebook"></span></a></li>
								<li><a href="#"><span class="fa fa-twitter"></span></a></li>
								<li><a href="#"><span class="fa fa-rss"></span></a></li>
								<li><a href="#"><span class="fa fa-vk"></span></a></li>
							</ul>
						</div>
	 </div>
					<div class="w3-header-top-right">

						<div class="w3-header-top-right-text">
							<p><span class="fa fa-home" aria-hidden="true"></span> 1st Street , New York</p>
						</div>
						<div class="clearfix"> </div>
					</div>
					<div class="clearfix"> </div>
				</div>
			</div>
	<div class="w3-agile-logo">
	<div class="head-wl">
	<div class="email">
	<p><span class="fa fa-envelope"  aria-hidden="true"></span> <a href="mailto:mail@example.com" class="info"> info@photos.com</a></p>
	
	</div>
		<div class="headder-w3">
		<h1><a href="index.html">Anchor</a></h1>
		</div>
		<div class="tele">
		<p><span  class="fa fa-phone" aria-hidden="true"></span> +1 234 567 8901</p>
		</div>
		<div class="clearfix"> </div>
		</div>
	</div>
	<div class="w3layouts-nav-right">
		<div class="fat-nav">
			<div class="fat-nav__wrapper">
				<ul>
					<li><a href="index.html" >Home</a></li>
					<li><a href="#about"  class="scroll" >About Us</a></li>
					<li><a href="#gallery"  class="scroll" >Gallery</a></li>
					<li><a href="#services"  class="scroll" >Services</a></li>
					<li><a href="#clients"  class="scroll" >Clients</a></li>
					<li><a href="#contact"  class="scroll" >Contact</a></li>
				</ul>
			</div>
		</div>
	</div>		
		</div>
	<div class="clearfix"></div>
</div>	
<!-- banner -->
<div class="w3_banner">
		<div class="container">
			<div class="slider">
				<div class="callbacks_container">
				   <ul class="rslides callbacks callbacks1" id="slider4">
						<li>							
							<div class="banner_text_w3layouts">
								<h3>welcome to Corporate</h3>
								<p>simply dummy text </p>
								<div class="w3-button">
									<a href="#about"  class="scroll" >Read More</a>
								</div>
							</div>
						</li>
						<li>	
							<div class="banner_text_w3layouts">
								<h3>Contrary to popular belief</h3>
								<p>Lorem Ipsum is </p>
								<div class="w3-button">
									<a href="#about"  class="scroll" >Read More</a>
								</div>
							</div>
						</li>
						<li>	
							<div class="banner_text_w3layouts">
								<h3>root of Corporate</h3>
								<p>simply dummy text is</p>
								<div class="w3-button">
									<a href="#about"  class="scroll" >Read More</a>
								</div>
							</div>
						</li>
						
					</ul>
				</div>
			
		   </div>
		</div>   
	</div>	
<!-- //banner -->
</div>
<!-- //header -->
<!-- about-->
<div class="about-us" id="about">
  <div class="container">
    <div class="about-head">
	  <h3> About us </h3>
        <div class=" about-inner">	   
		 <div class="col-md-3 ab-grid img-1">
	        <img class="ab-img" src="images/img1.jpg" alt="img1">
		 </div>
			 <div class="col-md-3 ab-grid img-2">
	         <img class="ab-img" src="images/img1.jpg" alt="img1">
		 </div>
		  <div class="col-md-3 ab-grid head-one">
		  <h4>Lorem Ipsum </h4>
		   <p>accumsan non arcu eu, vulputate egestas orci.
				Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet,
		</p>
		  </div>
		  		  <div class="col-md-3 ab-grid head-two">
		  <h4>Lorem Ipsum </h4>
		   <p>accumsan non arcu eu, vulputate egestas orci.
				Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet,
	           </p>
		  </div>
		 	<div class="clearfix"> </div>
			</div>
       <div class=" matter">
 		 <p>accumsan non arcu eu, vulputate egestas orci.
				Lorem ipsum dolor sit amet, consectetur adipiscing elit.accumsan non arcu eu, vulputate egestas orci.
				Lorem ipsum dolor sit amet, consectetur adipiscing elit.accumsan non arcu eu, vulputate egestas orci.
				Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
       </div> 
	</div>
  </div>
</div>
	<!-- //about-->

<!--service-section-starts-here-->
<div class="services" id="services">
	<div class="container">
	<div class="services-us">
		<h3>Services</h3>
		<div class="agile-service-grids">
			<div class="col-md-4 w3ls-svr-bottom-grid">
				<div class="col-sm-3 col-xs-3 agileits-ma"> 
					<div class="w3ls-svr-icon">
						<span class="fa fa-laptop" aria-hidden="true"></span>
					</div>
				</div>
				<div class="col-sm-9 col-xs-9 agileits-ma">
					<div class="svr-bt-text-w3ls">
						<h5>Lorem ipsum </h5>
						<p> accumsan non arcu eu, vulputate egestas orci.
				Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
						<div class="col-md-4  w3ls-svr-bottom-grid">
				<div class="col-sm-3 col-xs-3 agileits-ma"> 
					<div class="w3ls-svr-icon">
			<span class="fa fa-cog" aria-hidden="true"></span>
					</div>
				</div>
				<div class="col-sm-9 col-xs-9 agileits-ma">
					<div class="svr-bt-text-w3ls">
						<h5>Lorem ipsum </h5>
						<p> accumsan non arcu eu, vulputate egestas orci.
				Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
						<div class="col-md-4 w3ls-svr-bottom-grid">
				<div class="col-sm-3 col-xs-3 agileits-ma"> 
					<div class="w3ls-svr-icon">
				<span class="fa fa-cogs" aria-hidden="true"></span>
					</div>
				</div>
				<div class="col-sm-9 col-xs-9 agileits-ma">
					<div class="svr-bt-text-w3ls">
						<h5>	Lorem ipsum </h5>
						<p> accumsan non arcu eu, vulputate egestas orci.
				Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="clearfix"></div>
		</div>
	</div>
</div>	
<!--//service-section-end-here-->
<!-- gallery -->
<div class="agile_services" id="gallery">

		<h3 >Our gallery</h3>

		<div class="container">
			<div class="col-md-4 agileinfo_portfolio_grid project">
				<a href="images/g1.jpg" class="lsb-preview" data-lsb-group="header">
					<div class="agileits_portfolio_grid project_icon">
						<img src="images/g1.jpg" alt=" " class="img-responsive" />
					</div>
				</a>
			</div>
			<div class="col-md-4 agileinfo_portfolio_grid project">
				<a href="images/g2.jpg" class="lsb-preview" data-lsb-group="header">
					<div class="agileits_portfolio_grid project_icon">
						<img src="images/g2.jpg" alt=" " class="img-responsive" />
					</div>
				</a>
			</div>
			<div class="col-md-4 agileinfo_portfolio_grid project">
				<a href="images/g3.jpg" class="lsb-preview" data-lsb-group="header">
					<div class="agileits_portfolio_grid project_icon">
						<img src="images/g3.jpg" alt=" " class="img-responsive" />
					</div>
				</a>
			</div>
			<div class="col-md-4 agileinfo_portfolio_grid project">
				<a href="images/g6.jpg" class="lsb-preview" data-lsb-group="header">
					<div class="agileits_portfolio_grid project_icon">
						<img src="images/g6.jpg" alt=" " class="img-responsive" />
					</div>
				</a>
			</div>
			<div class="col-md-4 agileinfo_portfolio_grid project">
				<a href="images/g2.jpg" class="lsb-preview" data-lsb-group="header">
					<div class="agileits_portfolio_grid project_icon">
						<img src="images/g2.jpg" alt=" " class="img-responsive" />
					</div>
				</a>
			</div>
			<div class="col-md-4 agileinfo_portfolio_grid project">
				<a href="images/g4.jpg" class="lsb-preview" data-lsb-group="header">
					<div class="agileits_portfolio_grid project_icon">
						<img src="images/g4.jpg" alt=" " class="img-responsive" />
					</div>
				</a>
			</div>
			<div class="clearfix"> </div>
	
		</div>
	</div>
	<!--//gallery -->
	<!--testimonials-->
	<div class="agile_testimonials" id="clients">
	<div class="container">
	  <div class="clients-inn">
		<div class="col-md-6 testmonial_agile_info">
			<h3 class="agile_heading two">What Clients Say</h3>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus at placerat ante. Praesent nulla nunc, pretium dapibus
				efficitur in, auctor eget elit. Lorem ipsum dolor sit amet fusce eget erat nunc..</p>
		</div>
		<div class="col-md-6 clients_agile_slider">
			<div id="owl-demo" class="owl-carousel owl-theme">
				<div class="item">
					<div class="agile_tesimonials_content">
						<div class="about-midd-main">
							<img class="agile-img" src="images/t1.jpg" alt="img" >
							<h4>ketty way</h4>
							<p> Lorem ipsum adipiscing elit, sed do eiusmod idunt ut labore. sed do eiusmod </p>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="agile_tesimonials_content">
						<div class="about-midd-main">
							<img class="agile-img" src="images/t2.jpg" alt="img" >
							<h4>cleark Hill</h4>
							<p> Lorem ipsum adipiscing elit, sed do eiusmod idunt ut labore. sed do eiusmod </p>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="agile_tesimonials_content">
						<div class="about-midd-main">
							<img class="agile-img" src="images/t3.jpg" alt="img">
							<h4>willson Doe</h4>
							<p> Lorem ipsum adipiscing elit, sed do eiusmod idunt ut labore. sed do eiusmod </p>
						</div>
					</div>
				</div>

			</div>
			<div class="clearfix"> </div>
		</div>
		<div class="clearfix"> </div>
		  </div>
		</div>
	</div>
	<!-- //testimonials-->
<!-- contact -->
	<div class="contact" id="contact">
		<div class="container">
			<div class="agile-contact-grids">
			     <h3>Contact</h3>
				<div class="col-md-6 agile-contact-left">
					<div class="contact-form">
						<h4>Contact us</h4>
						<form action="#" method="post">
							<div class="styled-input agile-styled-input-top">
							  <input type="text" name="Name" required="">
				                <label>Name</label>
								<span></span>
							</div>
							<div class="styled-input">
				
								<input type="email" name="Email" required=""> 
								<label>Email</label>
								<span></span>
						    </div>
							<div class="styled-input">
								<input type="text" name="Subject" required="">
								<label>Subject</label>
								<span></span>
							</div>
							<div class="styled-input">
								<textarea name="Message" required=""></textarea>
								
								<label>Message</label>
						
								<span></span>
							</div>
							<input type="submit" value="SEND">
						</form>
					</div>
				</div>
				<div class="col-md-6 agile-contact-right">
					<div class="agileits-map">
						<h4>Our Location</h4>
					</div>
					<div class="map-grid">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d387142.8400948023!2d-74.25819082602831!3d40.70583163923578!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c24fa5d33f083b%3A0xc80b8f06e177fe62!2sNew+York%2C+NY%2C+USA!5e0!3m2!1sen!2sin!4v1470295981376" allowfullscreen></iframe>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<!-- //contact -->

<!--subscribe-->
  <div class="subscribe">
    <div class="container">
      <div class="rows">
        <div class="col-md-6 letter-logo">
          <div class="click">
		  <span class="fa fa-envelope" aria-hidden="true"></span>
		  </div>
            <h3>subscribe 
		   me</h3>
           </div>
         <div class="col-md-6 letter-sub">
             <form action="#" method="post">
					<input type="email" Name="Email" placeholder="EMAIL" required="">
					<div class="send-button agileits w3layouts">
						<button class="btn btn-primary">click me</button>
					</div>	
					</form>
          </div>
		  <div class="clearfix"></div>
        </div>
      </div>
    </div>