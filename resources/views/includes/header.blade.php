<!-- header-->
<div class="agileits_main jarallax" id="home">
  <div class="container-fluid">
  <div class="w3ls-header">
   <div class="w3layouts-header-top">
  	<div class="w3-header-top-grids">
	 <div class="w3-header-top-left">
	 						<div class="agileinfo-social-grids">
							<ul>
								<li><a href="#"><span class="fa fa-facebook"></span></a></li>
								<li><a href="#"><span class="fa fa-twitter"></span></a></li>
								<li><a href="#"><span class="fa fa-rss"></span></a></li>
								<li><a href="#"><span class="fa fa-vk"></span></a></li>
							</ul>
						</div>
	 </div>
					<div class="w3-header-top-right">

						<div class="w3-header-top-right-text">
							<p><span class="fa fa-home" aria-hidden="true"></span> 1st Street , New York</p>
						</div>
						<div class="clearfix"> </div>
					</div>
					<div class="clearfix"> </div>
				</div>
			</div>
	<div class="w3-agile-logo">
	<div class="head-wl">
	<div class="email">
	<p><span class="fa fa-envelope"  aria-hidden="true"></span> <a href="mailto:mail@example.com" class="info"> info@photos.com</a></p>
	
	</div>
		<div class="headder-w3">
		@include('includes.logo',array('logo'=> DB::table('add_logos')->get()))
		</div>
		<div class="tele">
		<p><span  class="fa fa-phone" aria-hidden="true"></span> +1 234 567 8901</p>
		</div>
		<div class="clearfix"> </div>
		</div>
	</div>
	<div class="w3layouts-nav-right">
		<div class="fat-nav">
			<div class="fat-nav__wrapper">
				<ul>
					@include('includes.nav', array('navigation' => DB::table('addnavigations') -> get()))
				</ul>
			</div>
		</div>
	</div>		
		</div>
	<div class="clearfix"></div>
</div>	
<!-- banner -->
<div class="w3_banner">
		<div class="container">
			<div class="slider">
				<div class="callbacks_container">
				   <ul class="rslides callbacks callbacks1" id="slider4">
						<li>							
							<div class="banner_text_w3layouts">
								<h3>welcome to Corporate</h3>
								<p>simply dummy text </p>
								<div class="w3-button">
									<a href="#about"  class="scroll" >Read More</a>
								</div>
							</div>
						</li>
						<li>	
							<div class="banner_text_w3layouts">
								<h3>Contrary to popular belief</h3>
								<p>Lorem Ipsum is </p>
								<div class="w3-button">
									<a href="#about"  class="scroll" >Read More</a>
								</div>
							</div>
						</li>
						<li>	
							<div class="banner_text_w3layouts">
								<h3>root of Corporate</h3>
								<p>simply dummy text is</p>
								<div class="w3-button">
									<a href="#about"  class="scroll" >Read More</a>
								</div>
							</div>
						</li>
						
					</ul>
				</div>
			
		   </div>
		</div>   
	</div>	
<!-- //banner -->
</div>
<!-- //header -->