@extends('fac-Bhavesh-0554.layouts.app')

@section('main-content')
<div class="content-wrapper">
	 <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>Service Image</h1>
    </section>
     <!-- Main content -->
    <section class="content">
	<div class="row">
	
		<div class="col-md-12">
		<div class="box box-success">
			      <div class="box-header">
              <h3 class="box-title">Service Image</h3>
              <div class="box-tools pull-right">
                
              </div>
            </div>
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table table-hover table-bordered" id="sampleTable3">
							<thead>
								<tr>
									<th>#</th>
									<th>Service Image</th>							
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
                                @foreach($serviceimages as$bus)
                          
								<tr>
									<td>{{$loop->index + 1}}</td>
									
									<td><img src="{{asset('public/serviceimage','')}}/{{$bus->serviceimage}}" alt="" class="main-image" /></td>		<td>
										<a class="btn-action btn-view-edit" href="{{route('serviceimages.edit', $bus->id)}}"><i class="fa fa-edit"></i></a>
                                        <form action="{{ route('serviceimages.destroy',$bus->id) }}" method="post" style="display:none" id="delete-id-{{$bus->id}}">
                                        {{csrf_field()}} {{method_field('DELETE')}}
                                        </form>
                                        
										<a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record ?'))
                                                                                  {event.preventDefault();document.getElementById('delete-id-{{$bus->id}}').submit();} else{event.preventDefault();}" href=""><i class="fa fa-trash"></i></a>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	</section	>
</div>
@endsection()