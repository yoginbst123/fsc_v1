@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>Answer</h1>
    </section>
    <!-- Main content -->
    <section class="content">
	<div class="row">
		<div class="col-md-12">
				<div class="box box-success">
			      <div class="box-header">
             
              <div class="box-tools pull-right">
                
              </div>
            </div>
				<div class="col-md-12">
             <form method="post" action="{{route('answer.store')}}" class="form-horizontal" id="positionname" name="positionname" enctype="multipart/form-data">
					{{csrf_field()}}
						<div class="form-group {{ $errors->has('answer') ? ' has-error' : '' }}">
							<label class="control-label col-md-3">Answer Name :</label>
							<div class="col-md-4">
<input name="q_id" type="hidden" id="q_id" class="form-control" value="{{ Request::segment(4)}}" />
								<input name="answer" type="text" id="answer" class="form-control" value="" />                                                           
								@if ($errors->has('answer'))
										<span class="help-block">
											<strong>{{ $errors->first('answer') }}</strong>
										</span>
									@endif
							</div>
						</div>					
						<div class="card-footer">
							<div class="row">
								<div class="col-md-8 col-md-offset-3">
									<input class="btn btn-primary icon-btn" type="submit" name="submit" value="Add">
									<input class="btn btn-primary icon-btn" type="submit" name="submit" value="Cancel" style="background:red !important;">
								</div>
							</div>
						</div>
						
					</form>
				</div>
			</div>
		</div>
	</div>
	</div>
			</div>
		</div>
	</div>
</section>	
</div>
</div>
@endsection()