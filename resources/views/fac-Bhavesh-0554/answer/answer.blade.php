@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
<div class="content-wrapper">
	<div class="page-title">
		<h1>Question</h1>
	</div>
	<div class="row">
	
		<div class="col-md-12">
			<div class="card">
				<div class="card-body">
					<div class="table-title">
						<h3></h3>
						<a href="{{route('question.create')}}">Add New Question</a>
					</div>
					@if ( session()->has('success') )
    <div class="alert alert-success alert-dismissable">{{session()->get('success') }}</div>
                               @endif
					<div class="table-responsive">
						<table class="table table-hover table-bordered" id="sampleTable3">
							<thead>
								<tr>
									<th>#</th>
									<th>Question </th>
<th>Add Answer</th>							
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
                                @foreach($position as $pos)
                          
								<tr>
									<td>{{$loop->index + 1}}</td>
									<td>{{$pos->question}}</td>
<td><a class="btn-action btn-view-edit" href="{{url('/admin/answer',$pos->id)}}">Add Answer</a></td>
									<td>
										<a class="btn-action btn-view-edit" href="{{route('question.edit', $pos->id)}}"><i class="fa fa-edit"></i></a>
                                        <form action="{{ route('question.destroy',$pos->id) }}" method="post" style="display:none" id="delete-id-{{$pos->id}}">
                                        {{csrf_field()}} {{method_field('DELETE')}}
                                        </form>
                                        
										<a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record ?'))
                                                                                  {event.preventDefault();document.getElementById('delete-id-{{$pos->id}}').submit();} else{event.preventDefault();}" href=""><i class="fa fa-trash"></i></a>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		
	</div>
		
</div>
	
@endsection()