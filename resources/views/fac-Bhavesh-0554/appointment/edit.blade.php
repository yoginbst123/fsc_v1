@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
<style>
    ul li {
        list-style: decimal;
    }

    .input-daterange {
        text-align: left;
        width: auto;
    }

    .select2-container--default .select2-selection--single .select2-selection__arrow b {
        margin-top: 3px;
    }

    .select2-container--default .select2-selection--single .select2-selection__rendered {
        color: #000;
        line-height: 32px;
        font-weight: bold;
    }

    .datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-bottom {
        display: none !important;
    }

    .input-daterange input:last-child {
        border-radius: 3px;
        text-align: left;
    }

    .input-daterange input:first-child {
        border-radius: 3px;
        text-align: left;
    }

    .select2-container .select2-selection--single {
        height: 40px;
        border-radius: 3px;
        border: 2px solid #2fa6f2 !important;
        font-size: 16px !important;
        outline: 0;
        color: #000;
        padding: 6px 6px;
        background-color: transparent !important;
    }

    .select2-container--default .select2-selection--multiple {
        height: 40px;
        border-radius: 3px;
        border: 2px solid #2fa6f2;
        font-size: 16px !important;
        outline: 0;
        color: #000;
        padding: 6px 6px;
    }

    .input-daterange {
        width: auto;
    }

    .datepicker {
        z-index: 99999 !important;
    }

    .ui-timepicker-container {
        z-index: 999999 !important
    }
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="page-title content-header">
        <h1>Edit Appointment</h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-success">
                    <div class="box-header">

                        <div class="box-tools pull-right">
                            <div class="table-title">

                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="panel with-nav-tabs panel-primary">
                            <!--<div class="panel-heading">
							<ul class="nav nav-tabs" id="myTab">
								<li class="active"><a href="#tab1primary" data-toggle="tab">Appointment</a></li>
								<li><a data-toggle="tab" href="#tab2primary" class="hvr-shutter-in-horizontal">Schedule</a></li>
							</ul>
						</div>-->
                            <div class="panel-body">
                                <div class="tab-content">
                                    <div class="tab-pane fade in active" id="tab1primary">

                                        <form method="post" action="{{route('appointment.update',$appoinment->id)}}" class="form-horizontal" enctype="multipart/form-data">
                                            {{csrf_field()}}{{method_field('PATCH')}}

                                            <div class="form-group">
                                                <label class="control-label col-md-2">Regarding :</label>
                                                <div class="col-md-7">
                                                    <div class="">
                                                        <select name="regarding" id="regarding" class="form-control fsc-input">
                                                            <option value="">---Select---</option>
                                                            @if(isset($regards))
                                                            @foreach($regards as $val)
                                                                <option value="{{$val->short_name}}" @if($val->short_name == $appoinment->regard_short) {{'selected'}} @endif> {{$val->regarding}} @if(!empty($val->short_name)) ({{$val->short_name}}) @endif </option>
                                                            @endforeach
                                                            @endif
                                                            <!--<option value="License Related">License Related</option>-->
                                                            <!--<option value="Income Tax Return Preparation Inquire">Income Tax Return Preparation Inquiry</option>-->
                                                            <!--<option value="Business Income Tax Return Preparation">Business Income Tax Return Preparation (BITR)</option>-->
                                                            <!--<option value="Personal Income Tax Return Preparation">Personal Income Tax Return Preparation (PITR)</option>-->
                                                            <!--<option value="Payroll Related">Payroll Related</option>-->
                                                            <!--<option value="Sales Tax Return">Sales Tax Return</option>-->
                                                            <!--<option value="Tax Issue Related">Tax Issue Related</option>-->
                                                            <!--<option value="Update on Status on work">Update on Status on work</option>-->
                                                            <!--<option value="Other">Other</option>-->
                                                        </select>
                                                        <input type="hidden" value="{{$appoinment->regarding}}" id="regarding_shortname" name="regarding_shortname" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label class="control-label col-md-2">Appointment With :</label>
                                                <div class="col-md-4">
                                                    <select name="employeeid" id="employee" class="form-control fsc-input">
                                                        <option value="">---Select---</option>
                                                        @foreach($employee as $emp)
                                                        <option value="{{$emp->employee_id}}" @if($emp->employee_id == $appoinment->appo_with) {{'selected'}} @endif>{{$emp->firstName.' '.$emp->middleName.' '.$emp->lastName }} @if(!empty($emp->teamname)) ({{$emp->teamname}}) @endif </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <label class="control-label col-md-1">Name :</label>
                                                <div class="col-md-2">
                                                    <div class="">
                                                        <select name="withwhome" id="withwhome" class="form-control fsc-input">
                                                            <option value="">---Select---</option>
                                                            <option value="Approval" @if(!empty($appoinment->client_id)) selected @endif>Client</option>
                                                            <option value="Other" @if(empty($appoinment->client_id)) selected @endif>Other</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group client" @if(empty($appoinment->client_id)) style="display:none" @endif>
                                                <label class="control-label col-md-2">Client Name :</label>
                                                <div class="col-md-4">
                                                    <div class="">
                                                        <select name="clientname" id="clientname" class="form-control fsc-input" style="width:100%">
                                                            <option value="">---Select---</option>
                                                            @foreach($clientdata as $cl)
                                                                <option value="{{$cl['filename']}}" @if($appoinment->client_id==$cl['filename']) selected @endif>{{$cl['filename']}} | {{$cl['company_name']}} | {{$cl['business_no']}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label class="control-label col-md-2 client1" style="@if(empty($appoinment->client_id)) display:none @endif">Client ID :</label>
                                                <div class="col-md-2 client1" @if(empty($appoinment->client_id)) style="display:none" @endif>
                                                    <div class="box-body">
                                                        <input type="text" name="clientno" readonly id="clientno" value="{{$appoinment->client_id}}" class="form-control">
                                                    </div>
                                                </div>
                                                <label class="control-label col-md-2">Type Of Work :</label>
                                                <div class="col-md-3">
                                                    <select name="work_type_id" id="work_type_id" class="form-control fsc-input">
                                                        <option value="">---Select---</option>
                                                        @foreach($work_type as $work)
                                                            <option value="{{$work->short_name}}" @if($work->short_name == $appoinment->work_type_id) {{'selected'}} @endif>{{$work->worktype}}</option>
                                                        @endforeach
                                                    </select>
                                                    <input type="hidden" value="{{$appoinment->work_type}}" id="work_type" name="work_type" />
                                                </div>
                                            </div>
                                            <div class="form-group input-daterange">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <!--<div class="col-md-2">-->
                                                        <!--    <div class="Branch">-->
                                                        <!--        <h1>Appointment Date</h1>-->
                                                        <!--    </div>-->
                                                        <!--</div>-->
                                                        <label class="control-label col-md-2">Start Date :</label>
                                                        <div class="col-md-2">
                                                            <input type="text" name="startdate" id="startdate" value="{{$appoinment->startdate}}" class="form-control">
                                                        </div>
                                                        <div class="col-md-2" style="width:12.6%;">
                                                            <p name="" id="" placeholder="Day" class="form-control day1" disabled></p>
                                                        </div>
                                                        <div id="_enddate">
                                                            <!--<label class="control-label pull-left">To :</label>-->
                                                            <div class="col-md-2">
                                                                <input type="text" name="enddate1" id="enddate1" class="form-control" value="{{$appoinment->enddate}}">
                                                            </div>
                                                            <div class="col-md-2" style="width:12.6%;">
                                                                <p name="" id="" placeholder="Day" class="form-control day2" disabled></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group {{ $errors->has('branchtype') ? ' has-error' : '' }}">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <!--<div class="col-md-2">-->
                                                        <!--    <div class="Branch">-->
                                                        <!--        <h1>Time</h1>-->
                                                        <!--    </div>-->
                                                        <!--</div>-->
                                                        <label class="control-label col-md-2">Start Time :</label>
                                                        <div class="col-md-2">
                                                            <input type="text" name="starttime" id="starttime" class="form-control" value="{{$appoinment->starttime}}">
                                                        </div>
                                                        <!--<label class="control-label pull-left">To :</label>-->
                                                        <div class="col-md-2" style="width:12.6%;">
                                                            <input type="text" name="endtime" id="endtime" class="form-control" value="{{$appoinment->endtime}}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--<div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">-->
                                            <!--    <div class="col-md-2">-->
                                            <!--        <div class="Branch">-->
                                            <!--            <h1>With Whom</h1>-->
                                            <!--        </div>-->
                                            <!--    </div>-->

                                            <!--</div>												-->
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Participant :</label>
                                                <div class="col-md-7">
                                                    @php $data = array() @endphp
                                                    @if(!empty($appoinment->participant))
                                                    @php $data = explode(',',$appoinment->participant) @endphp
                                                    @endif
                                                    <select name="participants[]" id="participant" class="form-control fsc-input" multiple>

                                                        <option value="">Select</option>
                                                        @foreach($employee as $emp)
                                                        <option value="{{$emp->employee_id}}" @if(in_array($emp->employee_id,$data)) {{'selected'}} @endif>{{$emp->firstName.' '.$emp->middleName.' '.$emp->lastName }} @if(!empty($emp->teamname)) ({{$emp->teamname}}) @endif </option>
                                                        @endforeach
                                                    </select>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Priority :</label>
                                                <div class="col-md-7">
                                                    <select name="priority" id="priority" class="form-control">
                                                        <option value="">Select</option>
                                                        <option value="Time Sensitive" @if($appoinment->priority == "Time Sensitive") {{'selected'}} @endif>Time Sensitive</option>
                                                        <option value="Urgent" @if($appoinment->priority == "Urgent") {{'selected'}} @endif>Urgent</option>
                                                        <option value="Regular" @if($appoinment->priority == "Regular") {{'selected'}} @endif>Regular</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-2">Details :</label>
                                                <div class="col-md-7">
                                                    <textarea class="form-control" name='details'>{{$appoinment->details}}</textarea>
                                                </div>
                                            </div>
                                            <div class="card-footer">
                                                <div class="row">
                                                    <div class="col-md-8 col-md-offset-2">
                                                        <input class="btn btn-primary icon-btn" type="submit" name="submit" value="Update">
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <!--<div class="tab-pane fade" id="tab2primary">
					    <form enctype='multipart/form-data' class="form-horizontal changepassword" action="" id="changepassword"  method="GET"> 
        	{{csrf_field()}}
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
            
              <div class="form-group ">
                <label for="focusedinput" class="col-md-3 control-label">Schedule  Date : <span class="required"> * </span></label>
                <div class="col-sm-8" style="margin:0 !important;padding:0 !important;">
                  <div class="col-sm-3">
                    <input class="form-control" name="startdate" id="startdate" type="text">
                  </div>
                  <label for="focusedinput" class="col-sm-1 control-label">To</label>
                  <div class="col-sm-3">
                    <input class="form-control" name="enddate" id="enddate" type="text">
                  </div>
                </div>
              </div>
              <div class="form-group ">
                <label for="focusedinput" class="col-md-2 control-label"></label>
                <div class="col-sm-8" style="margin:0 !important;padding:0 !important;">
                  <div class="col-sm-3">
                    <button class="btn-success btn" type="submit" name="search"> Search</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        
          <br>
        </form> 
		 @if(isset($_REQUEST['startdate']) && isset($_REQUEST['enddate'])) 
		 	 <div class="table-responsive">
                  <table class="table table-hover table-bordered">
        <thead>
            <tr>
                <th></th>
                <th></th>
                <th></th>
            <?php
            $start = date("d", strtotime($_REQUEST['startdate']));
            $m = date("m", strtotime($_REQUEST['startdate']));
            $y = date("Y", strtotime($_REQUEST['startdate']));
            $start1 = date("d", strtotime($_REQUEST['enddate']));

            for ($i = $start; $i <= $start1; $i++) {
                $p = $i;
                $p = sprintf("%02d", $i);
            ?>
<th><?php echo $m . '-' . $p; ?></th>
    <?php
            } ?>
</tr>
 <tr>
                <th>Emp. #</th>
                <th>Emp. Name</th>
                <th></th>
               <?php
                for ($i = $start; $i <= $start1; $i++) {
                    $p = $i;
                    $p = sprintf("%02d", $i);
                    $today = $y . '-' . $m . '-' . $p;
                ?>
    <th><?php echo substr(date('l', strtotime($today)), 0, 3); ?></th>
    <?php
                } ?>
            </tr>
        </thead>

<tbody>
@foreach($emp1 as $em)
               <tr>
                <td>{{$em->employee_id}}</td>
                <td>{{$em->firstName.' '.$em->middleName.' '.$em->lastName }}</td>
                <td>In</td>
               <?php
                for ($i = $start; $i <= $start1; $i++) {
                    $p = $i;
                    $p = sprintf("%02d", $i);
                    $today = $y . '-' . $m . '-' . $p;
                ?>
    <td>
 @foreach($employee1 as $em2) 
 @if($em2->employee_id==$em->id)
  <?php $in = date('H:i', strtotime($em2->emp_in)); ?>
    @if($today==$em2->emp_in_date)  @if($em2->emp_in==null) -- @else{{ date("g:i a", strtotime($in))}}  @endif @else @endif @else @endif @endforeach </td>	
    <?php
                } ?>
         </tr>
          <tr>
                <td></td>
                <td></td>
                <td>Out</td>
                <?php
                for ($i = $start; $i <= $start1; $i++) {
                    $p = $i;
                    $p = sprintf("%02d", $i);
                    $today = $y . '-' . $m . '-' . $p;

                ?>
    <td>@foreach($employee1 as $em2) @if($em2->employee_id==$em->id)
    <?php $out = date('H:i', strtotime($em2->emp_out)); ?> @if($today==$em2->emp_in_date) @if($em2->emp_out==null)-- @else  {{ date("g:i a", strtotime($out))}} @endif  @else  @endif @endif @endforeach</td>	
    <?php
                } ?>
</tr>

<tr>
                <td></td>
                <td></td>
                <td>L In</td>
                <?php
                for ($i = $start; $i <= $start1; $i++) {
                    $p = $i;
                    $p = sprintf("%02d", $i);
                    $today = $y . '-' . $m . '-' . $p;
                ?>
    <td>@foreach($employee1 as $em2) @if($em2->employee_id==$em->id)
    <?php $lunch_in = date('H:i', strtotime($em2->launch_in)); ?> @if($today==$em2->emp_in_date) @if($em2->launch_in==null)-- @else {{ date("g:i a", strtotime($lunch_in))}} @endif @else  @endif @endif @endforeach</td>	
    <?php
                } ?>
</tr>
<tr>
                <td></td>
                <td></td>
                <td>L Out</td>
                <?php
                for ($i = $start; $i <= $start1; $i++) {
                    $p = $i;
                    $p = sprintf("%02d", $i);
                    $today = $y . '-' . $m . '-' . $p;
                ?>
    <td>@foreach($employee1 as $em2) @if($em2->employee_id==$em->id)
    <?php $lunch_out = date('H:i', strtotime($em2->launch_out));
    ?> @if($today==$em2->emp_in_date) @if($em2->launch_out==null) -- @else {{date("g:i a", strtotime($lunch_out))}} @endif  @else  @endif @endif @endforeach</td>	
    <?php
                } ?>
</tr>

<tr>
                <td></td>
                <td></td>
                <td>L1 In</td>
                <?php
                for ($i = $start; $i <= $start1; $i++) {
                    $p = $i;
                    $p = sprintf("%02d", $i);
                    $today = $y . '-' . $m . '-' . $p;
                ?>
    <td>@foreach($employee1 as $em2) @if($em2->employee_id==$em->id)
    <?php $lunch_in1 = date('H:i', strtotime($em2->launch_in_second)); ?> @if($today==$em2->emp_in_date) @if($em2->launch_in_second==null)-- @else {{ date("g:i a", strtotime($lunch_in1))}} @endif @else  @endif @endif @endforeach</td>	
    <?php
                } ?>
</tr>
<tr>
                <td></td>
                <td></td>
                <td>L1 Out</td>
                <?php
                for ($i = $start; $i <= $start1; $i++) {
                    $p = $i;
                    $p = sprintf("%02d", $i);
                    $today = $y . '-' . $m . '-' . $p;
                ?>
    <td>@foreach($employee1 as $em2) @if($em2->employee_id==$em->id)
    <?php $lunch_out1 = date('H:i', strtotime($em2->launch_out_second));
    ?> @if($today==$em2->emp_in_date) @if($em2->launch_out_second==null) -- @else {{date("g:i a", strtotime($lunch_out1))}} @endif  @else  @endif @endif @endforeach</td>	
    <?php
                } ?>
</tr>
@endforeach   
     <tr>
                <td></td>
                <td></td>
                <td></td>

    <td>Total Hours : <b> {{number_format($sum, 2)}}</b></td>

</tr>
</tbody>


</table>
               </div>
		 @else 		    
		
               @endif
<p style="color:red;font-weight:bold">NOTE : <span style="color:black;font-weight:normal">Employee Responsibility :</span></p>  
<ul>
    <li>Record  clock in and out immediately when they come in/out.</li>
    <li>All the employee make sure that time recorded paid for that in their payroll.</li>
    <li>If not please notify immediately to payroll department.</li>
    <li>Leave request get approved before 14 days in advance.</li>
    <li>All the employee must take lunch brake as per company rules.</li>
    <li>If you have any question contact office manager immediately.</li>
    <li>Office Lunch Hrs. 1.00 p.m. to 2.00 p.m.</li>
    <li>Work as per schdule time - After schdule hrs will not be consider.</li>
    <li>Overtime will not be granted without prior approval or sign by supervisor.</li>
</ul>
					    </div>
					<div class="tab-pane fade" id="tab3primary">Time Sheet</div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <script type="text/javascript">
        $("#starttime").timepicker({
            use24hours: true
        });
        $("#endtime").timepicker({
            use24hours: true
        });
    </script>
    <script>
        var $start = $(".input-daterange").find('#startdate');
        var $end = $(".input-daterange").find('#enddate');
        $(".input-daterange").datepicker({
            orientation: "bottom left",
            autoclose: true,
            start: '+1d',
        });
        $end.on('show', function(e) {
            var date = $start.datepicker('getDate');
            date = moment(date).add(1, 'year').toDate();
            $end.datepicker('setEndDate', date);
        });
    </script>
    <script>
        $('#employee,#regarding,#withwhome,#clientname,#participant,#work_type_id').select2();
        $('#employee,#regarding,#withwhome,#clientname').on('select2:opening select2:closing', function(event) {
            var $searchfield = $(this).parent().find('.select2-search__field');
            $searchfield.prop('disabled', true);
        });
    </script>


    <script>
        $(document).ready(function() {

            $('#_enddate').hide();
            var work_type = $('#work_type_id').find(':selected').val();

            if (work_type == 'W') {
                $('#_enddate').show();
            } else {
                $('#_enddate').hide();
            }

            $('#work_type_id').on('change', function() {
                var id = $(this).find(':selected').text();
                $('#work_type').val(id);
                if (id == "Work") {
                    $('#_enddate').show();
                } else {
                    $('#_enddate').hide();
                }
            });

            
            

            $(document).on('change', '#withwhome', function() {
                var id = $(this).val();
                //	alert(id);
                $.get('{!!URL::to(' / clientid ')!!}?id=' + id, function(data) {
                    $('#clientname').empty();
                    $('.client').hide();
                    $('.client1').hide();
                    $('#clientname').append('<option value="">---Select Client---</option>');
                    $.each(data, function(index, subcatobj) {
                        if (id == 'Approval') {
                            $('.client').show();
                            $('#clientname').append('<option value="' + subcatobj.id + '">' + subcatobj.first_name + ' ' + subcatobj.middle_name + ' ' + subcatobj.last_name + '</option>');
                        }
                    })
                });
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $(document).on('change', '#clientname', function() {
                var id = $(this).val();
                //alert(id);
                $.get('{!!URL::to(' / clientid ')!!}?id=' + id, function(data) {
                    $('#clientno').empty();
                    $('.client1').hide();
                    //$('#clientno').append('<option value="">---Select Client---</option>');
                    $.each(data, function(index, subcatobj) {
                        $('.client1').show();
                        $('#clientno').val(subcatobj.business_no);
                    })
                });
            });
        });
    </script>
    <script type="text/javascript">
        $('#startdate,#enddate').datepicker({
            autoclose: true,
        });

        $('#startdate,#enddate').on('show', function(e) {
            console.debug('show', e.date, $(this).data('stickyDate'));

            if (e.date) {
                $(this).data('stickyDate', e.date);
            } else {
                $(this).data('stickyDate', null);
            }
        });

        $('#startdate,#enddate').on('hide', function(e) {
            console.debug('hide', e.date, $(this).data('stickyDate'));
            var stickyDate = $(this).data('stickyDate');

            if (!e.date && stickyDate) {
                console.debug('restore stickyDate', stickyDate);
                $(this).datepicker('setDate', stickyDate);
                $(this).data('stickyDate', null);
            }
        });
    </script>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/qtip2/3.0.3/jquery.qtip.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
                localStorage.setItem('activeTab', $(e.target).attr('href'));
            });
            var activeTab = localStorage.getItem('activeTab');
            if (activeTab) {
                $('#myTab a[href="' + activeTab + '"]').tab('show');
            }
        });

        $(document).ready(function() {
            $("#startdate").datepicker({
                dateFormat: "dd-mm-yy",
                onSelect: function(dateText, inst) {
                    var date = $.datepicker.parseDate(inst.settings.dateFormat || $.datepicker._defaults.dateFormat, dateText, inst.settings);
                    var dateText = $.datepicker.formatDate("DD", date, inst.settings);
                    $(".day1").html(dateText); // Just the day of week
                },
                minDate: 0
            });
            $("#enddate1").datepicker({
                dateFormat: "dd-mm-yy",
                onSelect: function(dateText, inst) {
                    var date = $.datepicker.parseDate(inst.settings.dateFormat || $.datepicker._defaults.dateFormat, dateText, inst.settings);
                    var dateText = $.datepicker.formatDate("DD", date, inst.settings);
                    $(".day2").html(dateText); // Just the day of week
                },
                minDate: 0
            });
        });
    </script>
    @endsection()