@extends('fac-Bhavesh-0554.layouts.app')

@section('main-content')
<div class="content-wrapper">

	<div class="page-title">
		<h1>Branch</h1>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-body">
             <form method="post" action="{{route('branch.store')}}" class="form-horizontal" id="businessname" name="businessname" enctype="multipart/form-data">
					{{csrf_field()}}
<div class="form-group {{ $errors->has('branchtype') ? ' has-error' : '' }}">
							<label class="control-label col-md-3">Branch Type:</label>
							<div class="col-md-4">
								<select name="branchtype" id="branchtype" class="form-control">
                                                            <option value="">---Branch Type---</option>         
                                                            <option value="FSC">FSC</option>
                                                     
                                                                 </select>                                                
                                                              @if($errors->has('branchtype'))
										<span class="help-block">
											<strong>{{ $errors->first('branchtype') }}</strong>
										</span>
									@endif
							</div>
						</div>
						<div class="form-group {{ $errors->has('branch') ? ' has-error' : '' }}">
							<label class="control-label col-md-3">Branch Name :</label>
							<div class="col-md-4">
								<input name="branch" type="text" id="branch" class="form-control" value="" />                                                            @if ($errors->has('branch'))
										<span class="help-block">
										<strong>{{ $errors->first('branch') }}</strong>
										</span>
									@endif
							</div>
						</div>
						
						<div class="form-group {{ $errors->has('country') ? ' has-error' : '' }}">
							<label class="control-label col-md-3">Country :</label>
							<div class="col-md-4">
								<select name="country" id="country" class="form-control">
                                                                       <option value="USA">USA</option>
                                                                       <option value="IN">IN</option>
                                                                 </select>                                                
                                                              @if($errors->has('country'))
										<span class="help-block">
											<strong>{{ $errors->first('country') }}</strong>
										</span>
									@endif
							</div>
						</div>
						<div class="form-group {{ $errors->has('city') ? ' has-error' : '' }}">
							<label class="control-label col-md-3">City :</label>
							<div class="col-md-4">
								<input name="city" type="text" id="city" class="form-control" value="" />
								@if ($errors->has('city'))
										<span class="help-block">
											<strong>{{ $errors->first('city') }}</strong>
										</span>
									@endif
							</div>
						</div>					
						
						<div class="card-footer">
							<div class="row">
								<div class="col-md-8 col-md-offset-3">
									<input class="btn btn-primary icon-btn" type="submit" name="submit" value="Add">
								</div>
							</div>
						</div>
						
					</form>
				</div>
			</div>
		</div>
	</div>
	
</div>
@endsection()