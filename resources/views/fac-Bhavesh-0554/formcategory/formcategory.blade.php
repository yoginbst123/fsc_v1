@extends('fac-Bhavesh-0554.layouts.app')

@section('main-content')
<div class="content-wrapper">
	<div class="page-title">
		<h1>Form Category</h1>
	</div>
	<div class="row">
	
		<div class="col-md-12">
			<div class="card">
				<div class="card-body">
					<div class="table-title">
						<h3></h3>
						<a href="{{route('formcategory.create')}}">Add New Category</a>
					</div>
					<div class="table-responsive">
						<table class="table table-hover table-bordered" id="sampleTable3">
							<thead>
								<tr>
									<th>ID</th>
									<th>Category</th>
                                                                        <th>Type</th>
									
							           
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
                                @foreach($link as $bus)
                          
								<tr>
									<td>{{$loop->index + 1}}</td>
									<td>{{$bus->category}}</td>
                                                                        <td>{{$bus->type}}</td>
									

		<td>
										<a class="btn-action btn-view-edit" href="{{route('formcategory.edit', $bus->id)}}"><i class="fa fa-edit"></i></a>
                                        <form action="{{ route('formcategory.destroy',$bus->id) }}" method="post" style="display:none" id="delete-id-{{$bus->id}}">
                                        {{csrf_field()}} {{method_field('DELETE')}}
                                        </form>
                                        
										<a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record ?'))
                                                                                  {event.preventDefault();document.getElementById('delete-id-{{$bus->id}}').submit();} else{event.preventDefault();}" href=""><i class="fa fa-trash"></i></a>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		
	</div>
		
</div>
	
@endsection()