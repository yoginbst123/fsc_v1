@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
<div class="content-wrapper">
 
  <section class="page-title content-header">
     		  <h1>Supervisor  </h1>
    </section>
    <!-- Main content -->
    <section class="content">
   <div class="row">
      <div class="col-md-12">
         <div class="box box-success" style="padding-top:15px;">
            <div class="card-body col-md-12">
               <form method="post" action="{{route('supervisor.store')}}" class="form-horizontal" id="content" name="content" enctype="multipart/form-data">
                  {{csrf_field()}}
                  <div class="form-group{{ $errors->has('supervisorname') ? ' has-error' : '' }}">
                     <label class="control-label col-md-3">Supervisor Name :</label>
                     <div class="col-md-5">
                        <div class="">
                           <select name="supervisorname" id="supervisorname" class="form-control fsc-input">
                            <option value="">--- Select---</option>    
                            @foreach($emp as $ep)
                            <option value="{{$ep->id}}">{{$ep->firstName.' '.$ep->middleName.' '.$ep->lastName}}</option>    
                            @endforeach
                           </select>
                        </div>
                        @if ($errors->has('supervisorname'))
                        <span class="help-block">
                        <strong>{{ $errors->first('supervisorname')}}</strong>
                        </span>
                        @endif	
                     </div>
                  </div>
                  
                  <div class="form-group{{ $errors->has('supervisorcode') ? ' has-error' : '' }}">
                     <label class="control-label col-md-3">Supervisor Code :</label>
                     <div class="col-md-5">
                        <div class="">
                          <input type="text" name="supervisorcode" id="supervisorcode" class="form-control fsc-input">
                        </div>
                        @if ($errors->has('supervisorcode'))
                        <span class="help-block">
                        <strong>{{ $errors->first('supervisorcode') }}</strong>
                        </span>
                        @endif	
                     </div>
                  </div>
                  
                    <div class="card-footer">
                        <div class="row">
                            <label class="control-label col-md-3"></label>
                            <div class="col-xs-2" style="width:auto;">
                                <input class="btn_new_save btn-primary1" type="submit" name="submit" value="Save" style="padding:8px 25px;">
                            </div>
                            <div class="col-xs-2" style="width:auto;">
                                <a class="btn_new_cancel" href="{{url('fac-Bhavesh-0554/supervisor')}}" style="padding:8px 25px;">Cancel</a> 
                            </div>
                        </div>
                    </div>
               </form>
            </div>
         </div>
      </div>
   </div>
   </section>
</div>

<!--<script>
   $(document).ready(function() {
  var dateInput = $('input[name="date"]'); 
  var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : 'body';
  dateInput.datepicker({
    format: 'M-dd-yyyy',
    container: container,
    todayHighlight: true,
    autoclose: true,
    startDate: truncateDate(new Date())
  });

  $('#date').datepicker('setStartDate', truncateDate(new Date())); 
});

function truncateDate(date) {
  return new Date(date.getFullYear(), date.getMonth(), date.getDate());
}
</script>-->
@endsection()