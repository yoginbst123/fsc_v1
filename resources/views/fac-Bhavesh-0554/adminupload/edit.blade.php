@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
<div class="content-wrapper">
<section class="page-title content-header">
     	<h1>Edit Upload</h1>
    </section>
<?php $id1 = Auth::user()->business_license_jurisdiction;?>
<!-- Main content -->
    <section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-success">
			     <div class="box-header">
             
              <div class="box-tools pull-right">
                
              </div>
            </div>
				<div class="col-md-offset-1">
					<form method="post" action="{{route('adminupload.update',$adminupload->id)}}" class="form-horizontal col-md-12" id="businessname" name="businessname" enctype="multipart/form-data">
					{{csrf_field()}}{{method_field('PATCH')}}
					    <div class="col-md-12">
    						<div class="form-group {{ $errors->has('upload_name') ? ' has-error' : '' }}">
    							<label class="control-label col-md-3">Upload Name :</label>
    							<div class="col-md-6">
    
    <select name="upload_name" type="text" id="upload_name" class="form-control" value="">
    <option value=""> Select</option>
    <option value="SOS Certificate" @if($adminupload->upload_name=='SOS Certificate') selected @endif> SOS Certificate</option>
    <option value="SOS-AOI" @if($adminupload->upload_name=='SOS-AOI') selected @endif> SOS-AOI</option>
    <option value="{{ Auth::user()->business_license_jurisdiction}}"  @if($adminupload->upload_name==$id1) selected @endif>Business License {{ Auth::user()->business_license_jurisdiction}} {{ Auth::user()->business_license2}}</option>
    @foreach($admin_professional as $admin_pr)
    <option value="{{$admin_pr->pro_id}}" @if($adminupload->upload_name==$admin_pr->pro_id) selected @endif>Professional License {{$admin_pr->profession}} {{$admin_pr->profession_state}} {{$admin_pr->profession_license}}</option>
    @endforeach
    </select>
    								@if ($errors->has('upload_name'))
    										<span class="help-block">
    											<strong>{{ $errors->first('upload_name') }}</strong>
    										</span>
    									@endif
    							</div>
    						</div>
    						<div class="form-group">
    							<label class="control-label col-md-3">License :</label>
    							<div class="col-md-4">
    							    <label class="file-upload btn btn-primary">								
                                    <input type="file" name="upload" id="upload" class="form-control"/>Browse for file ... </label>
    							    <br>								
                                    <a href="{{asset('public/adminupload')}}/{{$adminupload->upload}}" download> Download Here</a>
    							</div>
                                <input type="hidden" name="upload1" id="upload1" value="{{$adminupload->upload}}">
    						</div>
    						
							<div class="form-group">
    							<label class="control-label col-md-3 col-xs-12 left_991">Renewal :</label>
    							<div class="col-lg-2 col-md-3 col-xs-9">
                                    <input type="text" name="license_period" id="license_period" value="{{$adminupload->license_period}}" class="form-control"/>
							    </div>
    							<div class="col-lg-2 col-md-3 col-xs-3">
    							    <div class="row">
    								    <span style="margin-top: 6px;display: inline-block;">Year</span>
    								</div>
    							</div>
    						</div>
							<div class="form-group">
    							<label class="control-label col-md-3">Renewal Date :</label>
    							<div class="col-lg-2 col-md-3">
                                    <input type="text" name="expired_date" id="expired_date"  placeholder="MM-dd" class="form-control" value="{{$adminupload->expired_date}}"/>
    							</div>
    						</div>
    						
                            <div class="form-group ">
                                <label class="col-md-3 col-xs-12 left_991 control-label " style="margin-top: 16px;">Days :</label>
                                <div class="col-md-2 col-xs-4">
                                    <span>Reminder</span>
                                    <input type="text" name="reminder" id="reminder" value="{{$adminupload->reminder}}" class="form-control quantity">
                                </div>
                                <div class="col-md-2 col-xs-4">
                                    <span>Notification</span>
                                    <input type="text" name="notification" id="notification" value="{{$adminupload->notification}}" class="form-control quantity">
                                </div>
                                <div class="col-md-2 col-xs-4">
                                    <span>Warning</span>
                                    <input type="text" name="warning" id="warning" value="{{$adminupload->warning}}" class="form-control quantity">
                                </div>
                            </div>
							<div class="form-group">
    							<label class="control-label col-md-3">Website Link :</label>
    							<div class="col-md-6">
                                    <input type="text" name="website_link" id="website_link" class="form-control" value="{{$adminupload->website_link}}"/>
                                    <p><span class="sub_label">This is for Renewal Website Link:</span></p>
    							</div>
    						</div>
    						<div class="form-group" style="margin-top:10px">
    							<label class="col-md-3 control-label">Name :</label>
    							<div class="col-md-6">
    								<input type="text" name="name" id="name" class="form-control" value="{{$adminupload->name}}" placeholder="Name">
    							</div>
    						</div>
    						<div class="form-group">
    							<label class="col-md-3 control-label">Main Website :</label>
    							<div class="col-md-6">
    								<input type="text" name="main_website" id="main_website" class="form-control" value="{{$adminupload->main_website}}" placeholder="Main Website">
    							</div>
    						</div>
    						<div class="form-group">
    							<label class="col-md-3 control-label">Telephone :</label>
    							<div class="col-lg-2 col-md-3">
    								<input type="text" name="telephone" id="telephone" class="form-control" value="{{$adminupload->telephone}}" placeholder="Telephone">
    							</div>
    						</div>
    					</div>	
						<div class="card-footer">
						    <label class="col-md-3 control-label"></label>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-xs-2" style="width:auto;">
                                        <input class="btn_new_save btn-primary1" type="submit" name="submit" style="padding: 8px 15px;" value="Upload">
                                    </div>
                                    <div class="col-xs-2" style="width:auto;">
                                        <input class="btn_new_save btn-primary1" type="submit" name="submit" style="padding: 8px 15px;" value="Save">
                                    </div>
                                    <div class="col-xs-2" style="width:auto;">
                                        <a class="btn_new_cancel" href="{{url('fac-Bhavesh-0554/')}}" style="padding: 8px 15px;">Cancel</a> 
                                    </div>
                                </div>
                            </div>
                        </div>
					</form>
				</div>
			</div>
		</div>
	</div>
	</section>
<!--</div>-->
<script>

   $(document).ready(function() {
  var dateInput = $('input[name="expired_date"]'); 
  var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : 'body';
  dateInput.datepicker({
    format: 'M-dd',
    container: container,
    todayHighlight: true,
    autoclose: true,
    startDate:false
  });

  $('#expired_date').datepicker(); 
});

function truncateDate(date) {
  return new Date(date.getFullYear(), date.getMonth(), date.getDate());
}
</script>
<script>
$("#telephone").mask("(999) 999-9999"); 
</script> 
@endsection()