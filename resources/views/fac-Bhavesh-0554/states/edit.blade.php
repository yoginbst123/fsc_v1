@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
<style>
    .professional_btn {width: 10%;}
</style>


<div class="content-wrapper">
   <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>State Tax Authorities / Form</h1>
    </section>
    <!-- Main content -->
    <section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-success">
			      <div class="box-header">
              <h3 class="box-title"></h3>
              <div class="box-tools pull-right">
                
              </div>
            </div>
				<div class="col-md-12">
					<form method="post" action="{{route('states.update', $bussiness->id)}}" class="form-horizontal" id="businessname" name="businessname" enctype="multipart/form-data">
						{{csrf_field()}}{{method_field('PATCH')}}
						
				<div class="form-group {{ $errors->has('typeofform') ? ' has-error' : '' }}">
							<label class="control-label col-md-3">Type of Tax :</label>
							<div class="col-md-4">
								<select type="text" class="form-control fsc-input" name="typeofform" id="typeofform">
									<option value="">---Select---</option>
									<option value="Income Tax" @if($bussiness->question1=='Income Tax') selected @endif>Income Tax</option>
									<option value="Payroll Tax"  @if($bussiness->question1=='Payroll Tax') selected @endif>Payroll Tax</option>
									<option value="Tabbaco Tax"  @if($bussiness->question1=='Tabbaco Tax') selected @endif>Tabbaco Tax</option>
									<option value="Sales Tax"  @if($bussiness->question1=='Sales Tax') selected @endif>Sales Tax</option>
								</select>
								@if ($errors->has('typeofform'))
								<span class="help-block">
								<strong>{{ $errors->first('typeofform') }}</strong>
								</span>
								@endif
							</div>
						</div>
						 <div id="hh" @if($bussiness->short_name=='Payroll') style="display:none" @endif value="Entity">
						<div class="form-group {{ $errors->has('authority_name') ? ' has-error' : '' }}">
							<label class="control-label col-md-3">Type of Entity :</label>
							<div class="col-md-4">
								<select type="text" class="form-control fsc-input" name="authority_name" id="authority_name" placeholder="Enter Your Company Name" >
									<option value="">---Select---</option>
								@foreach($entity as $entity1)
                                    <option value="{{$entity1->typeentity}}" @if($bussiness->authority_name_state==$entity1->typeentity) selected @endif>{{$entity1->typeentity}}</option>
                                    @endforeach
								</select>
								@if ($errors->has('authority_name'))
								<span class="help-block">
									<strong>{{ $errors->first('authority_name') }}</strong>
								</span>
								@endif
							</div>
							<div class="col-md-4"><button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i></button></div>
						</div>
						 <div class="form-group {{ $errors->has('state') ? ' has-error' : '' }}">
                     <label class="control-label col-md-3">State :</label>
                     <div class="col-md-4">
                        <select type="text" class="form-control fsc-input" name="state" id="state">
                            <option value="">Select</option>
                                    <option value="AL" @if($bussiness->state=='AL') selected @endif>AL</option>
                                   <option value="AK" @if($bussiness->state=='AK') selected @endif>AK</option>
                                   <option value="AS" @if($bussiness->state=='AS') selected @endif>AS</option>
                                   <option value="AZ" @if($bussiness->state=='AZ') selected @endif>AZ</option>
                                   <option value="AR" @if($bussiness->state=='AR') selected @endif>AR</option>
                                   <option value="AF" @if($bussiness->state=='AF') selected @endif>AF</option>
                                   <option value="AA" @if($bussiness->state=='AA') selected @endif>AA</option>
                                   <option value="AC" @if($bussiness->state=='AC') selected @endif>AC</option>
                                   <option value="AE" @if($bussiness->state=='AE') selected @endif>AE</option>
                                   <option value="AM" @if($bussiness->state=='AM') selected @endif>AM</option>
                                   <option value="AP" @if($bussiness->state=='AP') selected @endif>AP</option>
                                   
                                   <option value="CA" @if($bussiness->state=='CA') selected @endif>CA</option>
                                   <option value="CO" @if($bussiness->state=='CO') selected @endif>CO</option>
                                   <option value="CT" @if($bussiness->state=='CT') selected @endif>CT</option>
                                   <option value="DE" @if($bussiness->state=='DE') selected @endif>DE</option>
                                   <option value="DC" @if($bussiness->state=='DC') selected @endif>DC</option>
                                   <option value="FM" @if($bussiness->state=='FM') selected @endif>FM</option>
                                   <option value="FL" @if($bussiness->state=='FL') selected @endif>FL</option>
                                   <option value="GA" @if($bussiness->state=='GA') selected @endif>GA</option>
                                   <option value="GU" @if($bussiness->state=='GU') selected @endif>GU</option>
                                   <option value="HI" @if($bussiness->state=='HI') selected @endif>HI</option>
                                   <option value="ID" @if($bussiness->state=='ID') selected @endif>ID</option>
                                   <option value="IL" @if($bussiness->state=='IL') selected @endif>IL</option>
                                   <option value="IN" @if($bussiness->state=='IN') selected @endif>IN</option>
                                   <option value="IA" @if($bussiness->state=='IA') selected @endif>IA</option>
                                   <option value="KS" @if($bussiness->state=='KS') selected @endif>KS</option>
                                   <option value="KY" @if($bussiness->state=='KY') selected @endif>KY</option>
                                   <option value="LA" @if($bussiness->state=='LA') selected @endif>LA</option>
                                   <option value="ME" @if($bussiness->state=='ME') selected @endif>ME</option>
                                   <option value="MH" @if($bussiness->state=='MH') selected @endif>MH</option>
                                   <option value="MD" @if($bussiness->state=='MD') selected @endif>MD</option>
                                   <option value="MA" @if($bussiness->state=='MA') selected @endif>MA</option>
                                   <option value="MI" @if($bussiness->state=='MI') selected @endif>MI</option>
                                   <option value="MN" @if($bussiness->state=='MN') selected @endif>MN</option>
                                   <option value="MS" @if($bussiness->state=='MS') selected @endif>MS</option>
                                   <option value="MO" @if($bussiness->state=='MO') selected @endif>MO</option>
                                   <option value="MT" @if($bussiness->state=='MT') selected @endif>MT</option>
                                   <option value="NE" @if($bussiness->state=='NE') selected @endif>NE</option>
                                   <option value="NV" @if($bussiness->state=='NV') selected @endif>NV</option>
                                   <option value="NH" @if($bussiness->state=='NH') selected @endif>NH</option>
                                   <option value="NJ" @if($bussiness->state=='NJ') selected @endif>NJ</option>
                                   <option value="NM" @if($bussiness->state=='NM') selected @endif>NM</option>
                                   <option value="NY" @if($bussiness->state=='NY') selected @endif>NY</option>
                                   <option value="NC" @if($bussiness->state=='NC') selected @endif>NC</option>
                                   <option value="ND" @if($bussiness->state=='ND') selected @endif>ND</option>
                                   <option value="MP" @if($bussiness->state=='MP') selected @endif>MP</option>
                                   <option value="OH" @if($bussiness->state=='OH') selected @endif>OH</option>
                                   <option value="OK" @if($bussiness->state=='OK') selected @endif>OK</option>
                                   <option value="OR" @if($bussiness->state=='OR') selected @endif>OR</option>
                                   <option value="PW" @if($bussiness->state=='PW') selected @endif>PW</option>
                                   <option value="PA" @if($bussiness->state=='PA') selected @endif>PA</option>
                                   <option value="PR" @if($bussiness->state=='PR') selected @endif>PR</option>
                                   <option value="RI" @if($bussiness->state=='RI') selected @endif>RI</option>
                                   <option value="SC" @if($bussiness->state=='SC') selected @endif>SC</option>
                                   <option value="SD" @if($bussiness->state=='SD') selected @endif>SD</option>
                                   <option value="TN" @if($bussiness->state=='TN') selected @endif>TN</option>
                                   <option value="TX" @if($bussiness->state=='TX') selected @endif>TX</option>
                                   <option value="UT" @if($bussiness->state=='UT') selected @endif>UT</option>
                                   <option value="VT" @if($bussiness->state=='VT') selected @endif>VT</option>
                                   <option value="VI" @if($bussiness->state=='VI') selected @endif>VI</option>
                                   <option value="VA" @if($bussiness->state=='VA') selected @endif>VA</option>
                                   <option value="WA" @if($bussiness->state=='WA') selected @endif>WA</option>
                                   <option value="WV" @if($bussiness->state=='WV') selected @endif>WV</option>
                                   <option value="WI" @if($bussiness->state=='WI') selected @endif>WI</option>
                                   <option value="WY" @if($bussiness->state=='WY') selected @endif>WY</option>
                                 </select>
                       
                     </div>
                  </div>
						  <div class="form-group {{ $errors->has('filling_frequency') ? ' has-error' : '' }}">
                     <label class="control-label col-md-3">Filling Frequency :</label>
                     <div class="col-md-4">
                        <select type="text" class="form-control fsc-input" name="filling_frequency" id="filling_frequency">
                            <option value="">Select</option>
                                   <option value="Yearly" @if($bussiness->filling_frequency=='Yearly') selected @endif>Yearly</option>
                                   <option value="Quarterly" @if($bussiness->filling_frequency=='Quarterly') selected @endif>Quarterly</option>
                                   <option value="Monthly" @if($bussiness->filling_frequency=='Monthly') selected @endif>Monthly</option>
                                    </select>
                       
                     </div>
                  </div>

						  <div class="form-group">
							<label class="control-label col-md-3">Form Name :</label>
							<div class="col-md-4">
								<input type="text" name="formname" id="formname" class="form-control" value="{{$bussiness->formname_state}}"  placeholder="" />
							</div>
						</div>
						 <!--  <div class="form-group {{ $errors->has('typeofform') ? ' has-error' : '' }}">
                     <label class="control-label col-md-3">Type of Tax :</label>
                     <div class="col-md-4">
<select type="text" class="form-control fsc-input" name="typeofform" id="typeofform" placeholder="Enter Your Company Name" >
                                    <option value="">---Select---</option>
                                    <option value="Income Tax">Income Tax</option>
                                    <option value="Payroll Tax">Payroll Tax</option>
                                    <option value="Sales Tax">Sales Tax</option>
                                 </select>
                        @if ($errors->has('typeofform'))
                        <span class="help-block">
                        <strong>{{ $errors->first('typeofform') }}</strong>
                        </span>
                        @endif
                     </div>
                  </div>!-->
						<div class="form-group">
							<label class="control-label col-md-3">Due Date :</label>
							<div class="col-md-4">
								<input type="text" name="duedate" id="duedate" class="form-control" value="{{$bussiness->due_date_state}}"  placeholder="" />
							</div>
						</div>
						<div class="form-group">
							<!--<label class="control-label col-md-3">Extension Due Date  :</label>-->
							<label class="control-label col-md-3">Start Date  :</label>
							<div class="col-md-4">
								<!--<input type="text" name="extdate" id="extdate" class="form-control" placeholder="Start Date" value="{{$bussiness->extension_due_date_state}}" />-->
								<input type="text" name="start_date" id="start_date" class="form-control" placeholder="Start Date" value="{{$bussiness->start_date}}" />
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Authority Short Name :</label>
							<div class="col-md-4">
								<input type="text" name="authority_level" id="authority_level" class="form-control" value="{{$bussiness->authority_level_state}}"  placeholder="" />
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3">Authority Full Name :</label>
							<div class="col-md-4">
								<input type="text" name="authority_name1" id="authority_name1" class="form-control" value="{{$bussiness->authority_name1_state}}"  placeholder="" />
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3">Website :</label>
							<div class="col-md-4">
								<input type="text" name="website" id="website" class="form-control" value="{{$bussiness->website_state}}"  placeholder="" />
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3">Telephone # :</label>
							<div class="col-md-4">
								<input type="text" name="telephone" id="telephone" class="form-control" value="{{$bussiness->telephone_state}}"  placeholder="" />
							</div>
						</div>
						
						
						<div class="salestaxdiv" @if($bussiness->typeofform_state=='Sales Tax')  style="display:block" @else style="display:none" @endif style="display:none">
    						<div class="Branch subttl"  style="background:#00b1ce; border-color:#016373;">
                                <h4 class="text-left padleft20 bold" style="color:#fff;">Sales Tax Info </h4>
                            </div>
                        
    	                    <div class="col-md-1"></div>
                              <div class="col-md-6">
                               
                                <table class="table table-bordered table-striped" id="mytable">
                                     <tr id="row1">
                                         <th>Month</th>
                                         <th>Year</th>
                                         <th>Sales Tax Rate</th>
                                         <th><div class="col-md-6"><input type="button" class="add-row btn btn-primary" value="+"></th>
                                     </tr>
                                     <tr id="row2">
                                          @foreach($statetax as $taxstate)  
                                         <td>
                                             <div class="col-md-6">
                                                <input type="hidden" name="sid[]" value="{{$taxstate->id}}">
                                                 <input type="hidden" name="statesalesid" value="{{$taxstate->statesalesid}}">
                                                <select name="state_tax_month[]" id="state_tax_month" class="form-control fsc-input" style="width:100px;">
                                                                           <option value="">---Select month---</option>
                                                                           <option value="01" @if($taxstate->state_tax_month=='01') selected @endif>01</option>
                                                                           <option value="02" @if($taxstate->state_tax_month=='02') selected @endif>02</option>
                                                                           <option value="03" @if($taxstate->state_tax_month=='03') selected @endif>03</option>
                                                                           <option value="04" @if($taxstate->state_tax_month=='04') selected @endif>04</option>
                                                                           <option value="05" @if($taxstate->state_tax_month=='05') selected @endif>05</option>
                                                                           <option value="06" @if($taxstate->state_tax_month=='06') selected @endif>06</option>
                                                                           <option value="07" @if($taxstate->state_tax_month=='07') selected @endif>07</option>
                                                                           <option value="08" @if($taxstate->state_tax_month=='08') selected @endif>08</option>
                                                                           <option value="09" @if($taxstate->state_tax_month=='09') selected @endif>09</option>
                                                                           <option value="10" @if($taxstate->state_tax_month=='10') selected @endif>10</option>
                                                                           <option value="11" @if($taxstate->state_tax_month=='11') selected @endif>11</option>
                                                                           <option value="12" @if($taxstate->state_tax_month=='12') selected @endif>12</option>
                                                                        </select>
                                                
                                             </div>
                                         </td>
                                         <td>
                                             <div class="col-md-6">
                                                <select name="state_tax_year[]" id="state_tax_year" class="form-control fsc-input" style="width:100px;">
                                                                           <option value="">---Select year---</option>
                                                                           <option value="2020" @if($taxstate->state_tax_year=='2020') selected @endif>2020</option>
                                                                           <option value="2021" @if($taxstate->state_tax_year=='2021') selected @endif>2021</option>
                                                                           <option value="2022" @if($taxstate->state_tax_year=='2022') selected @endif>2022</option>
                                                                           <option value="2023" @if($taxstate->state_tax_year=='2023') selected @endif>2023</option>
                                                                           <option value="2024" @if($taxstate->state_tax_year=='2024') selected @endif>2024</option>
                                                                           <option value="2025" @if($taxstate->state_tax_year=='2025') selected @endif>2025</option>
                                                                        </select>
                                              
                                             </div>
                                         </td>
                                         <td><input type="text" name="state_tax_personal_rate[]" value="{{$taxstate->state_tax_personal_rate}}" id="state_tax_personal_rate" class="form-control txtinput_1"  placeholder="Rate"/></td>
                                         <td> <div class="col-md-6"><a href="{{route('states.statetaxdelete', $taxstate->id)}}"><input type="button" class='delete-row btn btn-danger' value="-"></div></a></td>
                                     </tr>
                                     	@endforeach
                                </table>
                            </div>
                        </div>        
                                
						
						<!--<div class="form-group">
							<label class="control-label col-md-3">Renew Link :</label>
							<div class="col-md-4">
								<input type="text" name="renewlink" id="renewlink" class="form-control" value="{{$bussiness->renewlink_state}}"  placeholder="" />
							</div>
						</div>-->
						</div>
						<div class="sss" @if($bussiness->short_name_state=='Payroll') @else style="display:none" @endif>
						<div class="form-group"  id="payroll">
							<label class="control-label col-md-3">Payroll :</label>
							<div class="col-md-9">
								<div class="check_tab">
									<input type="radio" id="Federal" @if($bussiness->payroll=='Federal') checked @endif  name="payroll" class="payroll" value="Federal">
									<label for="Federal">Federal</label>
								</div>
								<div class="check_tab">
									<input type="radio" id="State" @if($bussiness->payroll=='State') checked @endif  name="payroll" class="payroll" value="State">
									<label for="State">State</label>
								</div>
								<div class="check_tab">
									<input type="radio" id="County" @if($bussiness->payroll=='County') checked @endif  name="payroll" class="payroll" value="County">
									<label for="County">County</label>
								</div>
								<div class="check_tab">
									<input type="radio" id="Local" @if($bussiness->payroll=='Local') checked @endif  name="payroll" class="payroll" value="Local">
									<label for="Local">Local</label>
								</div>
							</div>
						</div>
						
						<div class="Branch"  id="federal">
							<div class="col-md-3" style="text-align:left;">
								<h1 id="fed">Federal</h1>
							</div>
							<div class="col-md-6">
								<h1 style="font-size:20px;">Payroll Tax</h1>
							</div>
						</div>
						
						<div class="federal_tabs_main" id="federal_tabs_main">
							<div class="federal_tabs">
								<div class="federal_tab federal_name">
									<label>Short Name</label>
									<input type="text" class="form-control" value="{{$bussiness->payroll_name}}" name="payroll_name" placeholder="">
								</div>
								<div class="federal_tab federal_depart">
									<label>Department Name</label>
									<input type="text" class="form-control"  value="{{$bussiness->payroll_department_name}}"  name="payroll_department_name" placeholder="">
								</div>
								<div class="federal_tab federal_link">
									<label>Link</label>
									<input type="text" class="form-control"  value="{{$bussiness->renewlink}}"  name="payroll_link" placeholder="">
								</div>
								<div class="federal_tab federal_tele">
									<label>Telephone #</label>
									<input type="text" class="form-control" placeholder="" value="{{$bussiness->telephone}}"  name="payroll_telephone" >
								</div>
							</div>
						</div>
						
						<div class="filing_tabs_main" id="filing_tabs_main">
							<h3 class="comm_title_one">Filing Frequency</h3>
							@if($fill)
							@foreach($fill as $fillable)
							@if(!empty($fillable->filing_frequency_form))
							<div class="filing_tabs">
								<div class="filing_tab filing_form">
									<label>Form #</label>
									<input type="hidden" class="form-control" placeholder="" value="{{$fillable->id}}" name="fillid[]" >
									<input type="text" class="form-control" value="{{$fillable->filing_frequency_form}}" name="filing_frequency_form[]" placeholder="">
								</div>
								<div class="filing_tab filing_name">
									<label>Form Name</label>
									
									<input type="text" class="form-control" value="{{$fillable->filing_frequency_name}}" name="filing_frequency_name[]"  placeholder="">
								</div>
								<div class="filing_tab filing_freq">
									<label>Filing Frquency</label>
									<select type="text" class="form-control-insu" id="filing_frequency" name="filing_frequency[]">
					<option value="Annually" @if($fillable->filing_frequency=='Yearly') selected @endif>Yearly</option>
					<option value="Monthly" @if($fillable->filing_frequency=='Monthly') selected @endif>Monthly</option>
					<option value="Quaterly" @if($fillable->filing_frequency=='Quaterly') selected @endif>Quaterly</option>
					</select>
								</div>
								<div class="filing_tab filing_due_date">
									<label>Due Date</label>
									<input type="text" class="form-control" value="{{$fillable->filing_frequency_due_date}}" name="filing_frequency_due_date[]"  placeholder="">
								</div>
								<div class="filing_add">
									<button type="button" id="add_row3" class="filing_addbtn btn-danger">Delete</button>
								</div>
								<div class="filing_formula">
									<button type="button" id="" class="btn_formula btn-default">Formula</button>
								</div>
							</div>
							@endif
							@endforeach
							@endif
							
							<div class="filing_tabs">
								<div class="filing_tab filing_form">
									<label>Form #</label>
									<input type="hidden" class="form-control" placeholder="" value="" name="fillid[]" >
									<input type="text" class="form-control" placeholder=""  name="filing_frequency_form[]" >
								</div>
								<div class="filing_tab filing_name">
									<label>Form Name</label>
									<input type="text" class="form-control" placeholder="" name="filing_frequency_name[]" >
								</div>
								<div class="filing_tab filing_freq">
									<label>Filing Frquency</label>
									<select type="text" class="form-control-insu" id="filing_frequency" name="filing_frequency[]">
				                    	<option value="Annually" >Yearly</option>
				                    	<option value="Monthly" >Monthly</option>
				                    	<option value="Quaterly" >Quaterly</option>
				                	</select>
								</div>
								<div class="filing_tab filing_due_date">
									<label>Due Date</label>
									<input type="text" class="form-control" placeholder="" name="filing_frequency_due_date[]" >
								</div>
								<div class="filing_add">
									<button type="button" id="add_row1" class="filing_addbtn btn-success">ADD</button>
								</div>
								<div class="filing_formula">
									<button type="button" id="" class="btn_formula btn-default">Formula</button>
								</div>
							</div>
						</div>
						
						<div class="payment_tabs_main">
							<h3 class="comm_title_two">Payment Frequency</h3>
							@if($pay)
								@foreach($pay as $fillable)
										@if(!empty($fillable->filing_frequency_form1))
							<div class="payment_tabs">
								<div class="payment_tab payment_form">
									<label>Form #</label>
									<input type="text" class="form-control" placeholder="" value="{{$fillable->filing_frequency_form1}}" name="filing_frequency_form1[]" >
									<input type="hidden" class="form-control" placeholder="" value="{{$fillable->id}}" name="pay_id[]" >
								</div>
								<div class="payment_tab payment_name">
									<label>Form Name</label>
									<input type="text" class="form-control" placeholder="" value="{{$fillable->filing_frequency_name1}}" name="filing_frequency_name1[]">
								</div>
								<div class="payment_tab payment_freq">
									<label>Payment Frquency</label>
										<select type="text" class="form-control-insu" id="filing_frequency1" name="filing_frequency1[]">
					<option value="Annually" @if($fillable->filing_frequency1=='Yearly') selected @endif>Yearly</option>
					<option value="Monthly" @if($fillable->filing_frequency1=='Monthly') selected @endif>Monthly</option>
					<option value="Quaterly" @if($fillable->filing_frequency1=='Quaterly') selected @endif>Quaterly</option>
					</select>
								
								</div>
								<div class="payment_tab payment_due_date">
									<label>Due Date</label>
									<input type="text" class="form-control" placeholder=""  value="{{$fillable->filing_frequency_due_date1}}" name="filing_frequency_due_date1[]">
								</div>
								<div class="payment_add">
									<button type="button" id="add_row4" class="payment_addbtn btn-danger">Delete</button>
								</div>
								<div class="payment_formula">
									<button type="button" id="" class="btn_formula btn-default">Formula</button>
								</div>
							</div>
							@endif
								@endforeach
								@endif
							<div class="payment_tabs">
								<div class="payment_tab payment_form">
									<label>Form #</label>
									<input type="text" class="form-control" placeholder="" name="filing_frequency_form1[]">
									<input type="hidden" class="form-control" placeholder="" value="" name="pay_id[]" >
								</div>
								<div class="payment_tab payment_name">
									<label>Form Name</label>
									<input type="text" class="form-control" placeholder="" name="filing_frequency_name1[]">
								</div>
								<div class="payment_tab payment_freq">
									<label>Payment Frquency</label>
									<select type="text" class="form-control-insu" id="filing_frequency1" name="filing_frequency1[]">
                    					<option value="Annually">Yearly</option>
                    					<option value="Monthly">Monthly</option>
                    					<option value="Quaterly">Quaterly</option>
                					</select>
								</div>
								<div class="payment_tab payment_due_date">
									<label>Due Date</label>
									<input type="text" class="form-control" placeholder="" name="filing_frequency_due_date1[]">
								</div>
								<div class="payment_add">
									<button type="button" id="add_row2" class="payment_addbtn btn-success">ADD</button>
								</div>
								<div class="payment_formula">
									<button type="button" id="" class="btn_formula btn-default">Formula</button>
								</div>
							</div>	</div>
						</div>
						
					
						<div class="card-footer">
						<div class="col-md-2 col-md-offset-3">
									<input class="btn_new_save btn-primary1" style="margin-left:-5%" type="submit" name="submit" value="Save">
									</div>
									<div class="col-md-2 row">
									<a class="btn_new_cancel" href="{{url('fac-Bhavesh-0554/states')}}">Cancel</a> 
									</div>
						</div>
						
					</form>
				</div>
			</div>
		</div>
	</div>
	 </section>
</div>

<script>
$(document).ready(function()
{
    $('#typeofform').on('change', function() 
    {
        if(this.value == 'Sales Tax')
        {
            $(".salestaxdiv").show();
        }
        else if(this.value == 'Income Tax')
        {
			$(".salestaxdiv").hide();
        }
        else if(this.value == 'Tabbaco Tax')
        {
            $(".salestaxdiv").hide();
        }
        
    });
    
    
    // //Personal Property Tax
    // $('#personal_filing').on('change', function() 
    // {
    //     if(this.value == 'No')
    //     {
    //         $('.uname').hide();
    //         $('.password').hide();
    //     }
    //     else if(this.value == 'Yes')
    //     {
    //         $('.uname').show();
    //         $('.password').show();
    //     }
    // });
    
    
  
});
</script>

<script>
$(document).ready(function()
{
  (function($) {
    var minNumber = -100;
    var maxNumber = 100;
      $('.spinner .btn:first-of-type').on('click', function() {
        if ($('.spinner input').val() == maxNumber) {
          return false;
        } else {
          $('.spinner input').val(parseInt($('.spinner input').val(), 10) + 5 + '%');
        }
      });

  $('.txtinput_1').on("blur", function() {
    var inputVal = parseFloat($(this).val().replace('%', '')) || 0
    if (minNumber > inputVal) {
      inputVal = -100;
    } else if (maxNumber < inputVal) {
      inputVal = 100;
    }
    $(this).val(inputVal + '.00%');
  });

      $('.spinner .btn:last-of-type').on('click', function() {
        if ($('.spinner input').val() == minNumber) {
          return false;
        } else {
          $('.spinner input').val(parseInt($('.spinner input').val(), 10) - 5 + '%');
        }
      });
})(jQuery);
    
</script>


<script type="text/javascript">

$(document).ready(function()
 {
  (function($) {
    var minNumber = -100;
    var maxNumber = 100;
      $('.spinner .btn:first-of-type').on('click', function() {
        if ($('.spinner input').val() == maxNumber) {
          return false;
        } else {
          $('.spinner input').val(parseInt($('.spinner input').val(), 10) + 5 + '%');
        }
      });

  $('.txtinput_1').on("blur", function() {
    var inputVal = parseFloat($(this).val().replace('%', '')) || 0
    if (minNumber > inputVal) {
      inputVal = -100;
    } else if (maxNumber < inputVal) {
      inputVal = 100;
    }
    $(this).val(inputVal + '.00%');
  });

      $('.spinner .btn:last-of-type').on('click', function() {
        if ($('.spinner input').val() == minNumber) {
          return false;
        } else {
          $('.spinner input').val(parseInt($('.spinner input').val(), 10) - 5 + '%');
        }
      });
})(jQuery);
  
    //state_tax_month,state_tax_year,state_tax_personal_rate
    
        var k=1;
        $(".add-row").click(function(){
            k++;
            var markup = "<tr><td><div class='col-md-6'><select name='state_tax_month[]' id='state_tax_month' class='form-control fsc-input' style='width:100px;'><option>---Select month---</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option></select><div></td><td><div class='col-md-6'><select name='state_tax_year[]' id='state_tax_year' class='form-control fsc-input' style='width:100px;'><option>---Select year---</option><option value='2020'>2020</option><option value='2021'>2021</option><option value='2022'>2022</option><option value='2023'>2023</option><option value='2024'>2024</option><option value='2025'>2025</option></select></div></td><td><input type='text' name='state_tax_personal_rate[]' id='state_tax_personal_rate' class='form-control txtinput_"+k+"'  placeholder='Rate'/></td><td><div class='col-md-6'> <button type='button' class='delete-row btn btn-danger' onClick='deleterow();'>-</button></div></td></tr>";
            $("#mytable").append(markup);
            var minNumber = -100;
            var maxNumber = 100;
            $('.txtinput_'+k).on("blur", function() {
                var inputVal = parseFloat($(this).val().replace('%', '')) || 0
                if (minNumber > inputVal)
                {
                    inputVal = -100;
                } else if (maxNumber < inputVal) {
                    inputVal = 100;
                }
                $('.txtinput_'+k).val(inputVal + '.00%');
            });
        });
    }); 
    
   
      $("body").on("click",".delete-row",function(){ 
        $(this).parents("tr").remove();
    });
    




//$(document).ready(function(){
//     var x = 1;
// $("#duedate").datepicker({
// 		autoclose: true,
// format: "M-dd",
// });
// 	var maxField = 10; //Input fields increment limitation
// 	var addButton = $('.add_button'); //Add button selector
// 	var wrapper = $('.filing_tabs_main'); //Input field wrapper
// 	var fieldHTML = '<div class="filing_tabs"><div class="filing_tab filing_form"><label>Form #</label><input type="hidden" class="form-control" placeholder="" value="" name="fillid[]" ><input type="text" class="form-control" placeholder="Form #" id="filing_frequency_form_'+x+'" name="filing_frequency_form[]"></div><div class="filing_tab filing_name"><label>Form Name</label><input type="text" name="filing_frequency_name[]" class="form-control" placeholder=""></div><div class="filing_tab filing_freq"><label>Filing Frquency</label><select type="text" class="form-control-insu" id="filing_frequency" name="filing_frequency[]"><option value="Annually">Yearly</option><option value="Monthly">Monthly</option><option value="Quaterly">Quaterly</option></select></div><div class="filing_tab filing_due_date"><label>Due Date</label><input type="text" class="form-control" placeholder="" name="filing_frequency_due_date[]"></div><div class="filing_formula"><button type="button" id="" class="btn_formula btn-default">Formula</button></div><div class="professional_btn"><a href="javascript:void(0);" id="remove_button_pro" class="btn_professional btn_professional_remove">Remove</a></div></div>'; //New input field html 
// 	 //Initial field counter is 1
// 	$(addButton).click(function(){ //Once add button is clicked
// 		if(x < maxField){ //Check maximum number of input fields
// 		x++; //Increment field counter
// 		$(wrapper).append(fieldHTML); // Add field html
// 		}
// 	});
// 	$(wrapper).on('click', '#add_row1', function(e){ //Once remove button is clicked
// 		e.preventDefault();
// 		$(wrapper).append(fieldHTML); //Remove field html
// 		x++; 
// 	});  
// 	$(wrapper).on('click', '#remove_button_pro', function(e){ //Once remove button is clicked
// 		e.preventDefault();
// 		$(this).parent().parent('.filing_tabs').remove(); //Remove field html
// 		x--; //Decrement field counter
// 	});
//});
</script>


<script type="text/javascript">
// $(document).ready(function(){

// 	var maxField = 10; //Input fields increment limitation
// 	var addButton = $('.add_button'); //Add button selector
// 	var wrapper = $('.payment_tabs_main'); //Input field wrapper
// 	var fieldHTML = '<div class="payment_tabs"><div class="payment_tab payment_form"><label>Form #</label><input type="hidden" class="form-control" placeholder="" value="" name="pay_id[]" ><input type="text" class="form-control" placeholder="" name="filing_frequency_form1[]"></div><div class="payment_tab payment_name"><label>Form Name</label><input type="text" class="form-control" placeholder="" name="filing_frequency_name1[]"></div><div class="payment_tab payment_freq"><label>Filing Frquency</label><select type="text" class="form-control-insu" id="filing_frequency1" name="filing_frequency1[]"><option value="Annually">Yearly</option><option value="Monthly">Monthly</option><option value="Quaterly">Quaterly</option></select></div><div class="payment_tab payment_due_date"><label>Due Date</label><input type="text" class="form-control" placeholder="" name="filing_frequency_due_date1[]"></div><div class="payment_formula"><button type="button" id="" class="btn_formula btn-default">Formula</button></div><div class="payment_add"><a href="javascript:void(0);" id="remove_button_pro1" class="btn_professional btn_professional_remove">Remove</a></div></div>'; //New input field html 
// 	var x = 1; //Initial field counter is 1
// 	$(addButton).click(function(){ //Once add button is clicked
// 		if(x < maxField){ //Check maximum number of input fields
// 		x++; //Increment field counter
// 		$(wrapper).append(fieldHTML); // Add field html
// 		}
// 	});
// 	$(wrapper).on('click', '#add_row2', function(e){ //Once remove button is clicked
// 		e.preventDefault();
// 		$(wrapper).append(fieldHTML); //Remove field html
// 		x++; 
// 	});  
// 	$(wrapper).on('click', '#remove_button_pro1', function(e){ //Once remove button is clicked
// 		e.preventDefault();
// 		$(this).parent().parent('.payment_tabs').remove(); //Remove field html
// 		x--; //Decrement field counter
// 	});
// });
</script>
<script>
    $(document).ready(function(){
        $('#typeofform').change(function (){
            var selectedText =  $('#typeofform').val();
           
             if(selectedText=='Payroll Tax')
            {
                $('#filing_tabs_main').show();
                $('#federal').show();
                $('#payroll').show();
                 $('#federal_tabs_main').show();
                 $('.payment_tabs_main').show();  
                  $('#hh').hide(); 
                    $('.sss').show(); 
            }
           else
            {
                $('#filing_tabs_main').hide();
                $('#federal').hide();
                $('#payroll').hide();
                 $('#federal_tabs_main').hide();
                 $('.payment_tabs_main').hide();  $('#hh').show(); 
            }
        })
    })
    
    
    $(document).ready(function () {
       $('.payroll').click(function () {
     var va= $(this).val();
    $('#fed').html(va);
       });

   });
</script>
  <script type="text/javascript">
   $(function () {
       $("#filing_frequency1").change(function () {
           var selectedText =  $('#filing_frequency1').val();
           var selectedText1 =  $('#federal_payment_frequency_quaterly').val();
           var selectedValue = $(this).val(); 
   if(selectedValue =='Annually')
   {
   $("#filing_frequency_due_date1").val('<?php echo "Jan".'-'.date("t").'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
   }
   else if(selectedValue =='Monthly')
   {
       if(selectedText=='')
       {
          $("#filing_frequency_due_date1").val('<?php echo date("M",strtotime("0 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');  
       }
       else
       {
   if(selectedText > '<?php echo date('M-d-Y');?>') 
    {
       $("#filing_frequency_due_date1").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');
    }
    else
    {
    $("#filing_frequency_due_date1").val('<?php echo date("M",strtotime("0 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');
   }
       }
   }
   else if(selectedValue =='Quaterly')
   {
       if(selectedText1 > '<?php echo date('M-d-Y');?>') 
    {
    //  $("#filing_frequency_due_date").val('');  
    }
    else
    { 
      $("#filing_frequency_due_date1").val('<?php echo date("M",strtotime("3 month",strtotime(date("M")))).'-'.date("t").'-'.date("Y");?>'); 
   }
   }
   else
   {
  
   }   
       });
   });
   </script>
    <script type="text/javascript">
   $(function () {
       $("#filing_frequency").change(function () {
           var selectedText =  $('#filing_frequency').val();
           var selectedText1 =  $('#federal_payment_frequency_quaterly').val();
           var selectedValue = $(this).val(); 
   if(selectedValue =='Annually')
   {
   $("#filing_frequency_due_date").val('<?php echo "Jan".'-'.date("t").'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
   }
   else if(selectedValue =='Monthly')
   {
       if(selectedText=='')
       {
          $("#filing_frequency_due_date").val('<?php echo date("M",strtotime("0 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');  
       }
       else
       {
   if(selectedText > '<?php echo date('M-d-Y');?>') 
    {
       $("#filing_frequency_due_date").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');
    }
    else
    {
    $("#filing_frequency_due_date").val('<?php echo date("M",strtotime("0 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');
   }
       }
   }
   else if(selectedValue =='Quaterly')
   {
       if(selectedText1 > '<?php echo date('M-d-Y');?>') 
    {
    //  $("#filing_frequency_due_date").val('');  
    }
    else
    { 
      $("#filing_frequency_due_date").val('<?php echo date("M",strtotime("3 month",strtotime(date("M")))).'-'.date("t").'-'.date("Y");?>'); 
   }
   }
   else
   {
  
   }   
       });
   });
   
   $("#payroll_telephone").mask("(999) 999-9999");
$("#telephone").mask("(999) 999-9999");
   
$("#duedate").datepicker({
		autoclose: true,
format: "M-dd",
});
// $("#extdate").datepicker({
// 		autoclose: true,
// format: "M-dd",
// });

$("#start_date").datepicker({
		autoclose: true,
format: "M-dd",
});
   </script>  <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Type of Entity</h4>
      </div>
      <div class="modal-body" style="display: inline-table;">
           <form action="" method="post" id="ajax2">
                {{csrf_field()}}
        <div class="form-group">
                   <label class="control-label col-md-3">Type of Entity :</label>
                     <div class="col-md-6">
                        <div class="">
                            <input type="text" name="newopt" id="newopt" class="form-control" placeholder="Type of Entity">
                        </div>
                     </div>
                     <div class="col-md-2">
                        <div class="">
                            <input type="button" id="addopt" class="btn btn-primary" value="Add Type of Entity">
                        </div>
                     </div>
                  </div>
                  </form>
                  <div class="form-group">
                   <label class="control-label col-md-3"></label>
                     
                  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script>
          $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  }
              });
           
             $(function () {
                $('#addopt').click(function () { //alert();
                    var newopt = $('#newopt').val();
                    if (newopt == '') {
                        alert('Please enter something!');
                        return;
                    }

                   //check if the option value is already in the select box
                    $('#purpose1 option').each(function (index) {
                        if ($(this).val() == newopt) {
                            alert('Duplicate option, Please enter new!');
                        }
                    })
                    $.ajax({
        type: "post",
        url: "{!!route('typeof.typeofentity')!!}",
        dataType: "json",
        data: $('#ajax2').serialize(),
        success: function(data){
             alert('Successfully Add');
             $('#authority_name').append('<option value=' + newopt + '>' + newopt + '</option>');
             $("#div").load(" #div > *");
             $("#newopt").val('');
        },
        error: function(data){
             alert("Error")
        }
    });
                   
                     $('#myModal').modal('hide');
                });
            });
            
</script>
@endsection()

