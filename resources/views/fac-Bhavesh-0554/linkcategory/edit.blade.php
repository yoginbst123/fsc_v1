@extends('fac-Bhavesh-0554.layouts.app')

@section('main-content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>Link Category</h1>
    </section>
    <!-- Main content -->
    <section class="content">	
	<div class="row">
		<div class="col-md-12">
				<div class="box box-success">
			      <div class="box-header">
  
              <div class="box-tools pull-right">
                
              </div>
            </div>
				<div class="col-md-12">
             <form method="post" action="{{route('linkcategory.update',$link->id)}}" class="form-horizontal" id="businessname" name="businessname" enctype="multipart/form-data">
					{{csrf_field()}}{{method_field('PATCH')}}
						<div class="form-group {{ $errors->has('category') ? ' has-error' : '' }}">
							<label class="control-label col-md-3">Category :</label>
							<div class="col-lg-5 col-md-8">
								<input name="category" type="text" id="category" class="form-control" value="{{$link->category}}"/>  @if ($errors->has('category'))
										<span class="help-block">
											<strong>{{ $errors->first('category') }}</strong>
										</span>
									@endif
							</div>
						</div>
<div class="form-group {{ $errors->has('type') ? ' has-error' : '' }}">
							<label class="control-label col-md-3">Type :</label>
							<div class="col-lg-5 col-md-8">



<select name="type" type="text" id="type" class="form-control" value="" >
<option value="">Select</option> 
<option value="Federal" @if($link->type=='Federal') selected @endif>Federal</option> 
<option value="State" @if($link->type=='State') selected @endif>State</option> 
<option value="County" @if($link->type=='County') selected @endif>County</option> 
<option value="Local City" @if($link->type=='Local City') selected @endif>Local City</option> 
<option value="Community Link" @if($link->type=='Community Link') selected @endif>Community Link</option> 
<option value="Other Link" @if($link->type=='Other Link') selected @endif>Other Link</option> 
</select>
								               @if ($errors->has('name'))
										<span class="help-block">
											<strong>{{ $errors->first('name') }}</strong>
										</span>
									@endif
							</div>
						</div>



						
						<div class="form-group {{ $errors->has('linkimage') ? ' has-error' : '' }}">
							<label class="control-label col-md-3">Image :</label>
							<div class="col-lg-5 col-md-8">
								<label class="file-upload btn btn-primary">
	                Browse for file ... <input name="linkimage" style="opecity:0" placeholder="Upload Service Image" id="linkimage" type="file">
	            </label>


                                        @if ($errors->has('linkimage'))
										<span class="help-block">
											<strong>{{ $errors->first('linkimage') }}</strong>
										</span>
									@endif
<input type="hidden" name="linkimage1" id="linkimage1" class="form-control"  value="{{$link->linkimage}}" /> 
 <img src="{{asset('public/linkcategory','')}}/{{$link->linkimage}}" title="{{$link->name}}" alt="{{$link->name}}" width="100px">
							</div>
						</div>
									
						
						<div class="card-footer">
						    <div class="row">
						        <div class="col-md-3"></div>
						        <div class="col-xs-2" style="width:155px;">
									<input class="btn_new_save btn-primary1 primary1" style="margin-left:-5%" type="submit" id="primary1" name="submit" value="Save">
								</div>
								<div class="col-xs-2" style="width:155px;">
									<a class="btn_new_cancel" style="margin-left:-5%" href="{{url('fac-Bhavesh-0554/linkcategory')}}">Cancel</a> 
								</div>
							</div>
						</div>
						
					</form>
				</div>
			</div>
		</div>
	</div>
	 </section>
<!--</div>-->
<script type="text/javascript">
    $(function () {
        $("#category").change(function () {
            var selectedText = $(this).find("option:selected").text();
            var selectedValue = $(this).val();
if(selectedValue=='Business For Sale')
{
document.getElementById('hidden_div').style.display = "block";
document.getElementById('hidden_div1').style.display = "none";
}
if(selectedValue=='Looking For Business')
{
document.getElementById('hidden_div1').style.display = "block";
document.getElementById('hidden_div').style.display = "none";
}
        });
    });
</script>
@endsection()