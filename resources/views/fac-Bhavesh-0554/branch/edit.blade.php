@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>Branch</h1>
    </section>
    <!-- Main content -->
    <section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-success">
			      <div class="box-header">
              
              <div class="box-tools pull-right">
                
              </div>
            </div>
				<div class="col-md-12">
             <form method="post" action="{{route('branch.update',$branch->id)}}" class="form-horizontal" id="businessname" name="businessname" enctype="multipart/form-data">
					{{csrf_field()}}{{method_field('PATCH')}}
<div class="form-group">
							<label class="control-label col-md-3">Branch Type:</label>
							<div class="col-lg-5 col-md-8">
<select name="branchtype" id="branchtype" class="form-control">
                                                            <option value="">---Branch Type---</option>         
                                                            <option value="FSC" @if($branch->positionid=='FSC') selected @endif>FSC</option>
                                                     
                                                                 </select>                                                
                                                             
							</div>
						</div>
								<div class="form-group">
							<label class="control-label col-md-3">Branch Name :</label>
							<div class="col-lg-5 col-md-8">
								<input name="branch" type="text" id="branch" class="form-control" value="{{$branch->branchname}}" />                                                         
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3">Country :</label>
							<div class="col-lg-5 col-md-8">
								<select name="country" id="country" class="form-control">
                                                                       <option value="USA" @if($branch->country=='USA') selected @endif>USA</option>
                                                                       <option value="IN" @if($branch->country=='IN') selected @endif>IN</option>
                                                                 </select>                                                
                                          
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">City :</label>
							<div class="col-lg-5 col-md-8">
								<input name="city" type="text" id="city" class="form-control" value="{{$branch->city}}" />
							
							</div>
						</div>			
						
						<div class="card-footer">
						    <div class="col-md-3"></div>
						    <div class="col-xs-2" style="width:155px;">
								<input class="btn_new_save btn-primary1" type="submit" name="submit" value="Save">
							</div>
							<div class="col-xs-2" style="width:155px;">
								<a class="btn_new_cancel" href="{{url('fac-Bhavesh-0554/branch')}}">Cancel</a> 
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	</section>
<!--</div>-->
@endsection()