@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
<style>
   label{float:left;}
   .dataTables_filter{
       margin-top: -50px;
       margin-left: 170px;
       position: absolute;
   }
   .page-title{
        padding: 8px 15px;

}
#example_filter{
        position: absolute;
    /*margin-left: 215px;*/
        margin-top: -4px;
}
.dt-buttons
{
    /*position: absolute !important;*/
    /*margin-top: -50px;*/
    /*margin-left: 86.6%;*/
    margin-bottom: 7px;
}
.buttons-pdf {
    font-size: 0px !important;
    border-radius: 3px;
    padding: 8px 10px !important;
    background: #fff !important;
    border-color: #c6c6c6  !important;
    color:red !important;
}
.buttons-print {
    font-size: 0px !important;
    border-radius: 3px;
    padding: 8px 10px !important;
    background: #3c8dbc !important;
            border-color: #367fa9 !important;
}
.buttons-excel{
    font-size:0px !important;
    border-radius: 3px;
        padding: 8px 10px !important;
        
         background: #00a65a !important;
    border-color: #008d4c !important;
        

}
.buttons-excel:hover{
     background: #008d4c !important;

}
.buttons-pdf:hover{
     background: #f6f6f6  !important;
}

.buttons-print:hover{
     background: #367fa9 !important;
}
.fa{
    font-size: 16px !important;
}
#example_filter{
    display:none;
}
.search-btn {
    position: absolute;
    top: 10px;
    right: -9px;
    background: transparent;
    border: transparent;
}
.box-tools {
    position: absolute !important;
    margin-top: 7px !important;
    margin-right: 150px !important;
}
.box-header > .box-tools {
    position: inherit;
    right: 10px;
    top: 5px;
}
@media only screen and (max-width: 991px){
    .table-title a {
        margin-top: 55px !important;
        margin-right: 0px !important;
    }
    .dt-buttons {
        margin-top: 10px;
    }
}
@media only screen and (max-width: 500px){
    .table-title a {
        margin-top: 100px !important;
    }
}
</style>
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header page-title" style="height:50px;">
      <div class=""  style="">
         <div class="" style="text-align:center;">
            <h2>List of Schedule Details <span class="right_title" style="text-align:right;padding-right: 20px;position: absolute;right: 0;">Add / View / Edit</span></h2>
         </div>
      </div>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header" style="padding-bottom:0px;">
                    <div class="col-md-12 resp_abs">
                        <div class="row">
                            <div class="col-md-3 cs1">
                                <label style="margin-top: 11px;margin-right:10px;">Filter: </label>
                                <select name="choice" style="margin-left: 4px;" id="choice" class="form-control">
                                    <option value="1">All</option>
                                    <option value="Weekly">Weekly</option>
                                    <option value="Bi-Weekly">Bi-Weekly</option>
                                    <option value="Bi-Weekly">Monthly</option>
                                </select>
                            </div>
                            <div class="col-md-3 cs3" style="">
                                <table style="width: 100%; margin: 0 auto 0 10px;" cellspacing="0" cellpadding="3" border="0">
                                <tbody>
                                    <tr id="filter_global">
                                        <td align="center"><input type="text" class="global_filter form-control" id="global_filter" placeholder="All Search"><button class="search-btn"><i class="fa fa-search"></i></button></td>
                                    </tr>
                                    <tr id="filter_col2" data-column="1" style="display:none">
                                        <td align="center"><input type="text" class="column_filter form-control" id="col1_filter" placeholder="Type"><button class="search-btn"><i class="fa fa-search"></i></button></td>
                                    </tr>
                                    <tr id="filter_col3" data-column="2" style="display:none">
                                        <td align="center"><input type="text" class="column_filter form-control" id="col2_filter" placeholder="EE / User Id"><button class="search-btn"><i class="fa fa-search"></i></button></td>
                                    </tr>
                                    <tr id="filter_col4" data-column="3" style="display:none">
                                        <td align="center"><input type="text" class="column_filter form-control" id="col3_filter" placeholder="Employee Name"><button class="search-btn"><i class="fa fa-search"></i></button></td>
                                    </tr>
                                    <tr id="filter_col5" data-column="4" style="display:none">
                                        <td align="center"><input type="text" class="column_filter form-control" id="col4_filter" placeholder="Email Id"><button class="search-btn"><i class="fa fa-search"></i></button></td>
                                    </tr>
                                    <tr id="filter_col6" data-column="5" style="display:none">
                                        <td align="center"><input type="text" class="column_filter form-control" id="col5_filter" placeholder="Tel. Number"><button class="search-btn"><i class="fa fa-search"></i></button></td>
                                    </tr>
                                </tbody>
                                </table>
                            </div>
                            <div class="col-md-2" style="padding-left: 0px; padding-right: 0px; width: 95px; margin-top:4px;">
                            </div>
                        </div>
                    </div>
                    
                
                  <div class="box-tools pull-right" data-toggle="tooltip" title="Status" style="z-index:9999;">
                    <div class="table-title">
    						<a href="{{route('schedule.create')}}">Add Schedule</a>
    					</div>
                  </div>
              </div>
                
               <div class="col-md-12">
                  
                  @if ( session()->has('success') )
                  <div class="alert alert-success alert-dismissable">{{session()->get('success') }}</div>
                  @endif
                  <div class="table-responsive">
                     <table class="table table-hover table-bordered" id="example">
                        <thead>
                           <tr style="text-align:center">
                              <th style="text-align:center" width="7%">No. </th>
                              <th style="text-align:center" width="33%%">Employee Name </th>
                              <th style="text-align:center" width="14%%">Employer City </th>
                              <th style="text-align:center" width="13%%">Duration </th>
                              <th style="text-align:center" width="13%%">Start Date </th>
                              <th style="text-align:center" width="13%%">End Date </th>
                              <th style="text-align:center" width="11%">Action</th>
                           </tr>
                        </thead>
                        <tbody>
                            <?php $cnts=0;?>
                           @foreach($sch as $employ)
                           <?php $cnts++;?>
                           <tr class="cc{{$employ->cid}}">
                             <!-- <td style="text-align:center">
                                 @foreach($emp as $employ1) @if($employ1->id==$employ->emp_name){{$employ1->employee_id}}  @if($employ1->check=='1') @else 
                                 <style>.cc{{$employ->id}}{ display:none}</style>
                                 @endif @endif @endforeach
                              </td>!-->
                              <td class="text-center"><?php echo $cnts;?></td>
                              <td>{{$employ->firstName}}</td>
                              <td>{{$employ->emp_city}}</td>
                              <td>{{$employ->duration}}</td>
                              <td style="text-align:center">{{$employ->sch_start_date}}</td>
                              <td style="text-align:center">{{$employ->sch_end_date}}</td>
                              <th style="text-align:center">
                                 <a class="btn-action btn-view-edit" href="{{route('schedule.edit', $employ->cid)}}"><i class="fa fa-edit"></i></a>
                                 <form action="{{ route('schedule.destroy',$employ->cid) }}" method="post" style="display:none" id="delete-id-{{$employ->cid}}">
                                    {{csrf_field()}} {{method_field('DELETE')}}
                                 </form>
                                 <a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record ?'))
                                    {event.preventDefault();document.getElementById('delete-id-{{$employ->cid}}').submit();} else{event.preventDefault();}" href=""><i class="fa fa-trash"></i></a></td>
                           </tr>
                           @endforeach
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
<!--</div>-->
<script>
$(document).ready(function() {
    var table = $('#example').DataTable( {
        dom: 'Bfrtlip',
    "columnDefs": [ {
            "searchable": true,
            "orderable": true,
            "targets": 0
        } ],
        "order": [[ 0, 'asc' ]],
        buttons: [
                 {
                extend:    'copyHtml5',
                text:      '<i class="fa fa-files-o"></i> &nbsp; Copy',
                titleAttr: 'Copy',
                title: $('h3').text(),
                  exportOptions: {
        columns: [0,1, 2, 3,4,5], // Only name, email and role
    }
            },
            {
                extend:    'excelHtml5',
                text:      '<i class="fa fa-file-excel-o"></i>&nbsp; Excel',
               titleAttr: 'Excel',
                title: $('h3').text(),
                 customize: function( xlsx ) {
                var sheet = xlsx.xl.worksheets['sheet1.xml'];
 $('row c', sheet).attr('s', '51');
            },
                  exportOptions: {
        columns: [0,1, 2, 3,4,5], // Only name, email and role
    }
            },
            {
                extend:    'csvHtml5',
                text:      '<i class="fa fa-file-text-o"></i> &nbsp; CSV',
                titleAttr: 'CSV',
                title: $('h3').text(),
                  exportOptions: {
        columns: [0,1, 2, 3,4,5], // Only name, email and role
    }
            },
            {
                extend:    'pdfHtml5',
                text:      '<i class="fa fa-file-pdf-o"></i>&nbsp;  PDF',
                
              customize: function (doc) {
						//Remove the title created by datatTables
						doc.content.splice(0,1);
						//Create a date string that we use in the footer. Format is dd-mm-yyyy
						var now = new Date();
						var jsDate = now.getDate()+'-'+(now.getMonth()+1)+'-'+now.getFullYear();
						
						doc.pageMargins = [20,60,20,20];
						doc.defaultStyle.fontSize = 10;
						doc.styles.tableHeader.fontSize = 10;
						doc['header']=(function() {
							return {
								columns: [{
									    alignment: 'left',
										image: logo,
										width: 50,margin: [200,5]
									},{
										alignment: 'CENTER',
										text: 'List of Client',
										fontSize: 20,
										margin: [10,35],
									},],
								margin: [20, 0, 0,12],alignment: 'center',
							}
						});
						var objLayout = {};
						objLayout['hLineWidth'] = function(i) { return 2; };
						objLayout['vLineWidth'] = function(i) { return 2; };
						objLayout['hLineColor'] = function(i) { return '#ccc'; };
						objLayout['vLineColor'] = function(i) { return '#ccc'; };
						objLayout['paddingLeft'] = function(i) { return 14; };
						objLayout['paddingRight'] = function(i) { return 14; };
						doc.content[0].layout = objLayout;
				},
				titleAttr: 'PDF',
                  exportOptions: {
        columns: [0,1, 2, 3,4,5], // Only name, email and role
    }
            },
            {
           extend: 'print',
          text: '<i class="fa fa-print"></i>&nbsp; Print',
          titleAttr: 'Print',
        customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '10pt' )
                        .prepend(
                            '<center><img src=""/></center>'
                        );
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );
                },
         exportOptions: {
          columns: [0,1, 2, 3,4,5]
      },
      footer: true,
      autoPrint: true
    },],
    } );
$('input.global_filter').on( 'keyup click', function () {
        filterGlobal();
    } );
 
    $('input.column_filter').on( 'keyup click', function () {
        filterColumn( $(this).parents('tr').attr('data-column') );
    } );
  table.on( 'order.dt search.dt', function () {
       table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
          cell.innerHTML = i + 1;
          table.cell(cell).invalidate('dom'); 
       } );
    } ).draw();
       table.columns(6)
        .search('^(?:(?!Inactive|New|1).)*$\r?\n?', true, false)
        .draw();
    $("#choice").on("change",function(){
 var _val = $(this).val();//alert(_val);
 
 if(_val == 'Inactive'){   
        table.columns(6).search(_val).draw();
  }
 else if(_val == 'New'){   
        table.columns(6).search(_val).draw();
  }
  else if(_val == 'Active'){  //alert();
         table.columns(6).search(_val).draw();
          table.columns(6)
        .search('^(?:(?!Inactive|New|1).)*$\r?\n?', true, false)
        .draw();
  }
  else{
        table
        .columns()
        .search('')
        .draw(); 
  }
  })
} );
   	</script>
@endsection()