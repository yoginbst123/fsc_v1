<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <title>Forgot Passoword</title>
</head>
<body>

<img src="https://financialservicecenter.net/public/dashboard/images/fsc_logo.png" alt="img">
<br><br>
<table border ='1' style='color: #333;font-family: Helvetica, Arial, sans-serif;width:100%;border-collapse:collapse; border-spacing: 0;' >
    <tr><td>You are receiving this email because we received a password reset request for your account.</td></tr>
    <tr style='background:#535da6;'><td colspan='2' style='text-align:center;font-size:18px;color:#FFF;padding:10px 0;'> {{ $messages }}</td></tr>
 <tr>
        <td style='width:40%;text-align:left;font-weight:bold;border:1px solid #CCC;height:30px;padding:0 10px;'>
            <p>Email :</p>
        </td>
        <td style='width:60%;text-align:left;border: 1px solid #CCC;padding:0 10px; height: 30px;'>
           <p>{{ $email }}</p>
        </td>
    </tr>
<tr style='background:#535da6;'><td colspan='2' style='text-align:center;font-size:18px;color:#FFF;padding:10px 0;'> Forgot Password Link :
 <a href="{{ $token }}" target="_blank" style="color:#fff;">Click Here</a>
</td></tr>
    <tr><td>If you did not request a password reset, Please disregard this notice no further action is required.</td></tr>
<tr><td>Regards,  with FSC management</td></tr>
</table>
</body>
</html>

