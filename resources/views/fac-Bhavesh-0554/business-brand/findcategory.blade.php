@extends('fac-Bhavesh-0554.layouts.app')

@section('main-content')
<div class="content-wrapper">
	
	<div class="row">
	
		<div class="col-md-12">
			<div class="card">
				<div class="card-body">
					<div class="table-title">
						<h3>Business Brand</h3>
						<a href="{{route('business-brand.create')}}">Add New</a>
					</div>
					<div class="table-responsive">
						<table class="table table-hover table-bordered" id="sampleTable3">
						
							<thead>
								<tr>
									<th>#</th>
									<th>Business Name</th>
									<th>Business Category</th>
									<th>Business Brand</th>
									<th>Business Brand Image</th>
									<th>Action</th>
								</tr>
							</thead>
							
							<tbody>
								<tr>
									<td>1</td>
									<td>Business</td>
									<td>Sandwich Store</td>
									<td>Non-Brand</td>
									<td><img src="images/Non-Brand.png" class="extra-small-image" alt="" /></td>
									<td>
										<a class="btn-action btn-view-edit" href="#">View/ Edit</a>
										<a class="btn-action btn-delete" href="#">Delete</a>
									</td>
								</tr>
								<tr>
									<td>2</td>
									<td>Business</td>
									<td>Sandwich Store</td>
									<td>Brand</td>
									<td><img src="images/Brand.png" class="extra-small-image" alt="" /></td>
									<td>
										<a class="btn-action btn-view-edit" href="#">View/ Edit</a>
										<a class="btn-action btn-delete" href="#">Delete</a>
									</td>
								</tr>
								<tr>
									<td>3</td>
									<td>Business</td>
									<td>Sandwich Store</td>
									<td>Non-Brand</td>
									<td><img src="images/Non-Brand.png" class="extra-small-image" alt="" /></td>
									<td>
										<a class="btn-action btn-view-edit" href="#">View/ Edit</a>
										<a class="btn-action btn-delete" href="#">Delete</a>
									</td>
								</tr>
								<tr>
									<td>4</td>
									<td>Business</td>
									<td>Sandwich Store</td>
									<td>Brand</td>
									<td><img src="images/Brand.png" class="extra-small-image" alt="" /></td>
									<td>
										<a class="btn-action btn-view-edit" href="#">View/ Edit</a>
										<a class="btn-action btn-delete" href="#">Delete</a>
									</td>
								</tr>
								<tr>
									<td>5</td>
									<td>Business</td>
									<td>Sandwich Store</td>
									<td>Non-Brand</td>
									<td><img src="images/Non-Brand.png" class="extra-small-image" alt="" /></td>
									<td>
										<a class="btn-action btn-view-edit" href="#">View/ Edit</a>
										<a class="btn-action btn-delete" href="#">Delete</a>
									</td>
								</tr>
								<tr>
									<td>6</td>
									<td>Business</td>
									<td>Sandwich Store</td>
									<td>Brand</td>
									<td><img src="images/Brand.png" class="extra-small-image" alt="" /></td>
									<td>
										<a class="btn-action btn-view-edit" href="#">View/ Edit</a>
										<a class="btn-action btn-delete" href="#">Delete</a>
									</td>
								</tr>
								<tr>
									<td>7</td>
									<td>Business</td>
									<td>Sandwich Store</td>
									<td>Non-Brand</td>
									<td><img src="images/Non-Brand.png" class="extra-small-image" alt="" /></td>
									<td>
										<a class="btn-action btn-view-edit" href="#">View/ Edit</a>
										<a class="btn-action btn-delete" href="#">Delete</a>
									</td>
								</tr>
								<tr>
									<td>8</td>
									<td>Business</td>
									<td>Sandwich Store</td>
									<td>Brand</td>
									<td><img src="images/Brand.png" class="extra-small-image" alt="" /></td>
									<td>
										<a class="btn-action btn-view-edit" href="#">View/ Edit</a>
										<a class="btn-action btn-delete" href="#">Delete</a>
									</td>
								</tr>
							</tbody>
							
						</table>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	
</div>

<script type="text/javascript">
	$('#sampleTable3').DataTable();
</script>


@endsection()