@extends('fac-Bhavesh-0554.layouts.app')

@section('main-content')
<style>
.pagination{ display:none}
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header page-title" style="height:50px;">
     		<div class="row col-md-12">
     		    <div class="col-md-7" style="text-align:right;">
     		        <h2>List of Sub subcategorys</h2>
     		    </div>
     		    <div class="col-md-5" style="text-align:right;">
     		        <h2>Add / View / Edit</h2>
     		    </div>
     		</div>
    </section>
    <!-- Main content -->
    <section class="content">
	<div class="row">
	<!--<div class="branch">Federal</div>-->
		<div class="col-md-12">
				<div class="box box-success">
			      <div class="box-header" style="padding:5px;">
             
              <div class="box-tools pull-right" style="position:absolute;margin-right:295px;margin-top:7px;z-index:9999;">
                <div class="table-title">
					
						<a href="{{route('subcategorys.create')}}">Add New</a> &nbsp;&nbsp; <a href="{{route('zipcode.create')}}" style="    margin-right: 4px;">Add CSV File</a>
					</div>
              </div>
            </div>
				<div class="col-md-12">
					
					@if ( session()->has('success') )
    <div class="alert alert-success alert-dismissable">{{session()->get('success') }}</div>
                               @endif
					<div class="table-responsive">
						<table class="table table-hover table-bordered" id="sampleTable3">
							<thead>
								<tr>
									<th>No</th>
									<th>Cat Name</th>	
                                    <th>Sub Cat Name</th>	
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
                                @foreach($categorys as $bus)
                          
								<tr>
									<td style="text-align:center;">{{$loop->index + 1}}</td>
									<td style="text-align:center;">{{$bus->categoryname}}</td>
									<td style="text-align:center;">{{$bus->subcategoryname}}</td>
									

	<td style="text-align:center;">
										<a class="btn-action btn-view-edit" href="{{route('subcategorys.edit', $bus->id)}}"><i class="fa fa-edit"></i></a>
                                        <form action="{{ route('subcategorys.destroy',$bus->id) }}" method="post" style="display:none" id="delete-id-{{$bus->id}}">
                                        {{csrf_field()}} {{method_field('DELETE')}}
                                        </form>
                                        
										<a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record ?'))
                                                                                  {event.preventDefault();document.getElementById('delete-id-{{$bus->id}}').submit();} else{event.preventDefault();}" href=""><i class="fa fa-trash"></i></a>
									</td>
								</tr>
								@endforeach

							</tbody>
						</table>
{!! $categorys->links('pagination') !!}
					</div>
				</div>
			</div>
		</div>
		
	</div>
		    </section>
</div>
	
@endsection()