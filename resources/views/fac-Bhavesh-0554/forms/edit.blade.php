@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
<div class="content-wrapper">
	 <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>Forms</h1>
    </section>
    <!-- Main content -->
    <section class="content">
	<div class="row">
		<div class="col-md-12">
				<div class="box box-success">
			      <div class="box-header">
             
              <div class="box-tools pull-right">
                
              </div>
            </div>
				<div class="col-md-12">
    
					<form method="post" class="form-horizontal" id="" action="{{route('forms.update',$form->id)}}" enctype="multipart/form-data">
					{{csrf_field()}}
					{{method_field('PATCH')}}
						<div class="form-group {{ $errors->has('form_department') ? ' has-error' : '' }}">
							<label class="control-label col-md-3">Department Name :</label>
							<div class="col-lg-6 col-md-8">
								<select class="form-control category1" id="form_department" name="form_department">
								 <option value="Federal" @if($form->form_department=='Federal') selected @endif>Federal </option>
                                    <option value="State" @if($form->form_department=='State') selected @endif>State </option>
                                    <option value="County" @if($form->form_department=='County') selected @endif>County </option>
                                    <option value="Local (City)" @if($form->form_department=='Local (City)') selected @endif>Local (City) </option>
                                    <option value="Other" @if($form->form_department=='Other') selected @endif>Other </option>
								</select>
                                    @if ($errors->has('form_department'))
										<span class="help-block">
											<strong>{{ $errors->first('form_department') }}</strong>
										</span>
									@endif
							</div>
						</div>
						<div class="form-group {{ $errors->has('category') ? ' has-error' : '' }}">
<label class="control-label col-md-3">Category :</label>
<div class="col-lg-6 col-md-8">
<select name="category" type="text" id="category" class="form-control">
<option value="{{$form->category}}">{{$form->category}}</option> 
</select> 
<span id="loader" style="display:none"><i class="fa fa-spinner fa-3x fa-spin"></i></span>
                                                        @if ($errors->has('category'))
										<span class="help-block">
											<strong>{{ $errors->first('category') }}</strong>
										</span>
									@endif
							</div>
						</div>
<div class="form-group {{ $errors->has('form_name') ? ' has-error' : '' }}">
							<label class="control-label col-md-3">Form Name :</label>
							<div class="col-lg-6 col-md-8">
								<input name="form_name" type="text" id="form_name" class="form-control" value="{{$form->form_name}}" />      
								 	@if ($errors->has('form_name'))
										<span class="help-block">
											<strong>{{ $errors->first('form_name') }}</strong>
										</span>
									@endif
							</div>
						</div>

                                               <div class="form-group {{ $errors->has('form_no') ? ' has-error' : '' }}">
							<label class="control-label col-md-3">Form No :</label>
							<div class="col-lg-6 col-md-8">
								<input name="form_no" type="text" id="form_no" class="form-control" value="{{$form->form_no}}" />      
								 	@if ($errors->has('form_no'))
										<span class="help-block">
											<strong>{{ $errors->first('form_no') }}</strong>
										</span>
									@endif
							</div>
						</div>
						<div id="weblink" class="form-group {{ $errors->has('link') ? ' has-error' : '' }}" @if($form->form_department=='Other') style="display:none" @endif>
							<label class="control-label col-md-3">Website Link :</label>
							<div class="col-lg-6 col-md-8">
								<input name="link" type="text" id="link" class="form-control" value="{{$form->link}}" />      
								 	@if ($errors->has('link'))
										<span class="help-block">
											<strong>{{ $errors->first('link') }}</strong>
										</span>
									@endif
							</div>
						</div>
						<div class="form-group {{ $errors->has('form_upload') ? ' has-error' : '' }}">
							<label class="control-label col-md-3">Upload Form :</label>
							<div class="col-lg-6 col-md-8">
<label class="file-upload btn btn-primary">
Browse for file ... <input name="form_upload" style="opecity:0" placeholder="Upload Service Image" id="form_upload" type="file">
</label>
<input name="form_upload1" type="hidden" id="form_upload1" class="form-control" value="{{$form->form_upload}}" /> 
<a href="{{url('formpdf')}}/{{$form->form_upload}}" download><i class="fa fa-file-pdf-o"></i> {{$form->form_upload}} </a>    
									@if ($errors->has('form_upload'))
										<span class="help-block">
											<strong>{{ $errors->first('form_upload') }}</strong>
										</span>
									@endif
							</div>
						</div>
						<div class="card-footer">
    						<div class="row">
    						    <div class="col-md-3"></div>
    							<div class="col-xs-2" style="width:155px;">
									<input class="btn_new_save btn-primary1 primary1" type="submit" id="primary1" name="submit" value="Save">
								</div>
									<div class="col-xs-2" style="width:155px;">
									<a class="btn_new_cancel" href="{{url('fac-Bhavesh-0554/forms')}}">Cancel</a> 
								</div>
						    </div>
						</div>
						
					</form>


				</div>
			</div>
		</div>
	</div>
	</section>
<!--</div>-->
<script>
$(document).ready(function(){
	$(document).on('change','.category1', function()
	{ 
	var id = $(this).val();//alert(id);
	if(id=='Other')
	{
	    $('#weblink').hide();
	}
	else
	{
	     $('#weblink').show();
	}
		$.get('{!!URL::to('/getform')!!}?id='+id, function(data)
		{ //alert(data);
            $('#category').empty();
           $.each(data, function(index, subcatobj)
		   {
			   $('#category').append('<option value="'+subcatobj.category+'">'+subcatobj.category+'</option>');
		   })

		});
			
	});
});
</script>
@endsection()