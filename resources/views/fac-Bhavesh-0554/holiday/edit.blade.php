@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
<style>
   .ui-timepicker-container{ z-index:999999 !important}
   .content-wrapper{z-index:99999999!important; position:relative;}
</style>
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="page-title content-header">
      <h1>Edit Holiday</h1>
      
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="box box-success">
               <div class="box-header">
                  <div class="box-tools pull-right">
                  </div>
               </div>
              
               <div class="card-body col-md-12">
                   <?php //echo $holiday->holiday_name;print_r($holiday);exit;?>
                  <form method="post" action="{{route('holiday.update',$holiday->id)}}" class="form-horizontal" id="holiday" name="holiday" autocomplete="off">
                     {{csrf_field()}} {{method_field('PATCH')}}       
                      
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('holiday_name') ? ' has-error' : '' }}">
                        <div class="form-group">
                           <label class="control-label col-md-3">Holiday Name : <span class="star-required">&nbsp;&nbsp;</span></label>
                                 <div class="col-lg-5 col-md-6">
                                     <input type="hidden"  name='id' value="{{$holiday->id}}">
                                    <input type="text" readonly class="form-control  fsc-input"  id="holiday_name"  name='holiday_name' placeholder="mm/dd/yyyy" value="{{$holiday->holiday_name}}">@if ($errors->has('duration'))
                                    
                                    
                                    <span class="help-block">
                                    <strong>{{ $errors->first('holiday_name') }}</strong>
                                    </span>
                                    @endif
                                 </div>
                        </div>
                     </div>
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('holiday_date') ? ' has-error' : '' }}">
                        <div class="form-group">
                           <label class="control-label col-md-3">Holiday Date : <span class="star-required">&nbsp;&nbsp;</span></label>
                           <div class="col-lg-5 col-md-6 fsc-element-margin">
                                    <input type="text" value="{{date('M-d yy',strtotime($holiday->holiday_date))}}" class="ttt form-control  fsc-input" style="text-align:left"   name='holiday_date'
                                    placeholder="Holiday Date" id="holiday_date">
                                    @if ($errors->has('holiday_date'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('holiday_date') }}</strong>
                                    </span>
                                    @endif
                                 </div>
                        </div>
                     </div>
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      
                       
                        <div class="card-footer">
                        <div class="form-group">
                        <label class="control-label col-md-3"></label>
                        <div class="col-xs-2" style="width:155px;">
                        <input class="btn_new_save btn-primary1" type="submit" name="submit" value="Save">
                        </div>
                        <div class="col-xs-2" style="width:155px;">
                        <a class="btn_new_cancel" href="{{url('fac-Bhavesh-0554/holiday')}}">Cancel</a>
                        </div>
                        </div>
                        </div>    
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                           <div class="" id="Register"></div>
                        </div>
                  
                  </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </section>
<!--</div>-->

<script>
   $(document).ready(function()
                     {   
     $("#holiday_date").datepicker({
       format: "M-dd yyyy",startDate: new Date()});
   
   });
</script>


@endsection()