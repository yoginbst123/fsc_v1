@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
<div class="content-wrapper">
    <section class="page-title content-header">
     		<h1>Add Income Detail </h1>
    </section>
    <section class="content">
	    <div class="row">
		    <div class="col-md-12">
			<div class="box box-success">
			    <div class="box-header">
                    <div class="box-tools pull-right">
                    </div>
                </div>
				
                <div class="table-responsive">
						<table class="table table-hover table-bordered" id="sampleTable2">
							<thead>
								<tr>
									<th style="width:10%">No</th>
									<th>Employer Name</th>
									<th>Wages</th>
									<th>State</th>
								</tr>
							</thead>
							<tbody class="someClass">
							</tbody>
							
						</table>
						<table class="table table-hover table-bordered" id="sampleTable2">
						    <tbody>
							<tr>
							    <td style='width:10%'><b>Total</b></td>
							    <td style='width:10%'></td>
							    <td style='width:10%'><input type="text" class="form-control income_number txtinput_1 totalamts2" name='wagestotalamounts' readonly></td>
							    <td style='width:10%'></td>
							</tr>
							</tbody>
						</table>
                        <div class="modal-footer">
            <div class="col-md-3 pull-right" style="padding-left:6px; margin-top:3px; padding-right:0px; text-align:right;">
                    <a href="{{url('fac-Bhavesh-0554/task/create')}}" class="btn btn-primary"  class="redius">New Task</a>&nbsp;&nbsp;&nbsp; 
                    <a href="{{url('fac-Bhavesh-0554/task')}}" class="btn btn-primary"  class="redius">Task List</a>
            </div>
        </div>
            </div>			        	
				
				
			</div>
		</div>
	    </div>
	</section>
</div>


<!-- Income Wages Modal Start-->
 <div id="myModalIncomeDetailShow" class="modal fade" role="dialog">
  <div class="modal-dialog" style="width:850px;">
    
                 
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Employer Wages Detail @if(isset($documentRow)!='')<?php print_r($documentRow);die; ?>@endif</h4>
      </div>
      <div class="modal-body">
            <div class="table-responsive">
						<table class="table table-hover table-bordered" id="sampleTable2">
							<thead>
								<tr>
									<th style="width:10%">No</th>
									<th>Employer Name</th>
									<th>Wages</th>
									<th>State</th>
								</tr>
							</thead>
							<tbody class="someClass">
							</tbody>
							
						</table>
						<table class="table table-hover table-bordered" id="sampleTable2">
						    <tbody>
							<tr>
							    <td style='width:10%'><b>Total</b></td>
							    <td style='width:10%'></td>
							    <td style='width:10%'><input type="text" class="form-control income_number txtinput_1 totalamts2" name='wagestotalamounts' readonly></td>
							    <td style='width:10%'></td>
							</tr>
							</tbody>
						</table>
                        <div class="modal-footer">
            <div class="col-md-3 pull-right" style="padding-left:6px; margin-top:3px; padding-right:0px; text-align:right;">
                    <a href="{{url('fac-Bhavesh-0554/task/create')}}" class="btn btn-primary"  class="redius">New Task</a>&nbsp;&nbsp;&nbsp; 
                    <a href="{{url('fac-Bhavesh-0554/task')}}" class="btn btn-primary"  class="redius">Task List</a>
            </div>
        </div>
            </div>

    
    </div>
    </div>
    <!-- Modal End -->



<script type="text/javascript">
$(document).ready(function()
 {
    $(".incomeDetailShow").click(function ()
    {
        var incomeid = $(this).attr('data-id');
        //alert(incomeid);
        $("#incomeid").val(incomeid);
        $('#myModalIncomeDetailShow').modal('show');
        $.get('{!!URL::to('getClientdata')!!}?incomeid='+incomeid, function(data)
            {  
                //console.log(data);exit;
                if(data == "")
                {
                    
                }
                else
                {
                    $('.someClass').html(data);
                    // $('#income_idin').val(data.income_id);
                    // $('#client_idin').val(data.client_id);
                    // $('#firstNamein').val(data.firstName);
                    // $('#lastNamein').val(data.lastName);
                    // $('#address1in').val(data.address1);
                    // $('#stateIdin').val(data.stateId);
                }
            });    
    }); 
        
    $('.wagestotal').blur(function ()
    {
        var sum = 0.00;
        $('.wagestotal').each(function()
        {
            sum += Number($(this).val());
        });
        
        $('.totalamts').val(sum);
         //here, you have your sum
    });
    
    $('.wagestotal2').blur(function ()
    {
        var sum = 0.00;
        $('.wagestotal2').each(function()
        {
            sum += Number($(this).val());
        });
        
        $('.totalamts2').val(sum);
         //here, you have your sum
    });
        
    $(".income_number").on("input", function(evt)
    {
		var self = $(this);
		self.val(self.val().replace(/[^\d].+/, ""));
		if ((evt.which < 48 || evt.which > 57)) 
		{
			evt.preventDefault();
		}
    });
    
    (function($)
    {
        var minNumber = -100;
        var maxNumber = 100;
        $('.spinner .btn:first-of-type').on('click', function()
        {
            if ($('.spinner input').val() == maxNumber) {
              return false;
            } else {
              $('.spinner input').val(parseInt($('.spinner input').val(), 10) + 5 + '%');
            }
        });

        $('.txtinput_1').on("blur", function()
        {
            var inputVal = parseFloat($(this).val().replace('%', '')) || 0
            if (minNumber > inputVal) {
              //inputVal = -100;
            } else if (maxNumber < inputVal) {
              //inputVal = 100;
            }
            $(this).val(inputVal + '.00');
        });

        $('.spinner .btn:last-of-type').on('click', function() 
        {
            if ($('.spinner input').val() == minNumber) {
              return false;
            } else {
              $('.spinner input').val(parseInt($('.spinner input').val(), 10) - 5 + '%');
            }
        });
    })(jQuery);

}); 

</script>



<style>
.select2-container--default .select2-selection--single .select2-selection__rendered{
    font-size:16px !important;
    padding:0;
     color:#000;
}
.p-l-10{ padding-left:10px;}
.select2-container--default .select2-selection--single .select2-selection__arrow b{ border-color:#000 transparent transparent transparent; }
.select2-container--default .select2-selection--single .select2-selection__arrow{ top:6px; right:4px; }
.select2-container {
    box-sizing: border-box;
    display: inline-block;
    margin: 0;
    position: relative;
    vertical-align: middle;
    width: 100% !important;
}.select2-container--default .select2-selection--single {
    background-color: #fff;
    /* border: 1px solid #aaa; */
    border-radius: 4px;
    border: 2px solid #2fa6f2;
    height:40px;
    padding:8px;
}</style>

<script>
$("#contactnumber").mask("(999) 999-9999");


</script> 


<script>
    $('.js-example-tags').select2({
    tags: true,
    tokenSeparators: [",", " "]
});

</script>
@endsection()