@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
<style>
.box-header > .box-tools {
    position: inherit;
    right: 10px;
    top: 0px;
}
@media only screen and (max-width: 991px){
    .table-title a {
        margin-top: 0px !important;
        margin-right: 0px !important;
    }
}
</style>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h2>Link</h2>
    </section>
    <!-- Main content -->
    <section class="content">	
	<div class="row">
		<div class="col-md-12">
				<div class="box box-success">
				    <div class="box-header">
             
              <div class="box-tools pull-right" style="">
                <div class="table-title">
					
						<a href="{{route('linkcategory.create')}}">Add New Category</a>
					</div> 
              </div>
            </div>
            
				<div class="col-md-12">

             <form method="post" action="{{route('link.store')}}" class="form-horizontal" id="businessname" name="businessname" enctype="multipart/form-data">
					{{csrf_field()}}
					<div class="form-group {{ $errors->has('type') ? ' has-error' : '' }}">
							<label class="control-label col-md-3">Type :</label>
							<div class="col-lg-5 col-md-8">
								<select name="type" class="form-control category1" value="" >
<option value="">Select</option> 
<option value="Federal">Federal</option> 
<option value="State">State</option> 
<option value="County">County</option> 
<option value="Local City">Local City</option> 
<option value="Community Link">FSC Community Link</option> 
<option value="Other Link">Other Link</option> 
</select>                                                            @if ($errors->has('type'))
										<span class="help-block">
											<strong>{{ $errors->first('type') }}</strong>
										</span>
									@endif
							</div>
						</div>

<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
							<label class="control-label col-md-3">Name :</label>
							<div class="col-lg-5 col-md-8">
								<input name="name" type="text" id="name" class="form-control" value=""/>  @if ($errors->has('name'))
										<span class="help-block">
											<strong>{{ $errors->first('name') }}</strong>
										</span>
									@endif
							</div>
						</div>
<div class="form-group {{ $errors->has('category') ? ' has-error' : '' }}">
<label class="control-label col-md-3">Category :</label>
<div class="col-lg-5 col-md-8">
<select name="category" type="text" id="category" class="form-control">
<option value="">Select</option> 
</select> 
<span id="loader" style="display:none"><i class="fa fa-spinner fa-3x fa-spin"></i></span>
                                                        @if ($errors->has('category'))
										<span class="help-block">
											<strong>{{ $errors->first('category') }}</strong>
										</span>
									@endif
							</div>
						</div>
						
							<div class="form-group dis {{ $errors->has('link') ? ' has-error' : '' }}" style="display:none">
							<label class="control-label col-md-3">Link :</label>
							<div class="col-lg-5 col-md-8">
								<input name="link" type="text"  id="link" class="form-control" value="" />                                                            @if ($errors->has('post_id'))
										<span class="help-block">
											<strong>{{ $errors->first('link') }}</strong>
										</span>
									@endif
							</div>
						</div>
<div class="divhide">


						<div class="form-group {{ $errors->has('post_id') ? ' has-error' : '' }}">
							<label class="control-label col-md-3">Post Id :</label>
							<div class="col-lg-5 col-md-8">
								<input name="post_id" type="text" readonly id="post_id" class="form-control" value="{{$cmppcode}}" />                                                            @if ($errors->has('post_id'))
										<span class="help-block">
											<strong>{{ $errors->first('post_id') }}</strong>
										</span>
									@endif
							</div>
						</div>
<div class="form-group {{ $errors->has('post_date') ? ' has-error' : '' }}">
							<label class="control-label col-md-3">Post Date :</label>
							<div class="col-lg-5 col-md-8">
								<input name="post_date" type="text" id="post_date" class="form-control" value="<?php echo date('M-d-Y');?>" readonly/>                                                            @if ($errors->has('post_date'))
										<span class="help-block">
											<strong>{{ $errors->first('post_date') }}</strong>
										</span>
									@endif
							</div>
						</div>
						<div class="form-group {{ $errors->has('linkimage') ? ' has-error' : '' }}">
							<label class="control-label col-md-3">Image :</label>
							<div class="col-lg-5 col-md-8">
							    
				<label class="file-upload btn btn-primary">
	                Browse for file ... 
	                <input name="linkimage[]" id="linkimage" style="opecity:0" placeholder="Upload Service Image" multiple type="file" value=""/>
	            </label>
							    @if ($errors->has('linkimage'))
										<span class="help-block">
											<strong>{{ $errors->first('linkimage') }}</strong>
										</span>
									@endif
							</div>
						</div>
						<div class="form-group {{ $errors->has('link') ? ' has-error' : '' }}">
							<label class="control-label col-md-3">Description :</label>
							<div class="col-md-8">
								<textarea id="editor1" name="description"></textarea>
                                                                   @if ($errors->has('description'))
										<span class="help-block">
											<strong>{{ $errors->first('description') }}</strong>
										</span>
									@endif
							</div>
						</div>					
						
<div class="form-group">
							<label class="control-label col-md-3">Client No. :</label>
							<div class="col-lg-5 col-md-8">

<select name="clientname"  class="form-control client_id">
<option value="">Select</option> 
@foreach($client as $client1)
@if(empty($client1->filename))
@else
<option value="{{$client1->filename}}">{{$client1->filename}}</option> 
@endif
@endforeach
</select> 
</div>
								 
                                                        
							</div>
						</div>
<div id="email_status"></div> 
						<div class="card-footer">
						    <div class="row">
						    <div class="col-md-3"></div>
							<div class="col-xs-2" style="width:155px;">
								<input class="btn_new_save btn-primary1 primary1" type="submit" id="primary1" name="submit" value="Save">
							</div>
							<div class="col-xs-2" style="width:155px;">
								<a class="btn_new_cancel" href="{{url('fac-Bhavesh-0554/link')}}">Cancel</a> 
							</div>
							</div>
						</div>
						
					</form>
				</div>
			</div>
		</div>
	</div>
	</section>
<!--</div>-->

<script type="text/javascript">
$(document).ready(function(){
	$(document).on('change','.client_id', function()
	{ 
		
		
		
		$.get('{!!URL::to('/clientno1')!!}?id='+id, function(data)
		{ //alert(data);
            $('#email_status').empty();
           $.each(data, function(index, subcatobj)
		   {
			   $('#email_status').append('<div class="table-responsive"><table class="table"><thead><tr><th>Client No.</th><th>Client Name</th><th>Client Phone No.</th><th>Client Email</th></tr></thead><tbody><tr><td>'+subcatobj.filename+'</td><td>'+subcatobj.first_name+'</td><td>'+subcatobj.business_no+'</td><td>'+subcatobj.email+'</td></tr></tbody></table></div>');
		   })

		});
			
	});
});
</script>

<script>
$(document).ready(function(){
	$(document).on('change','.category1', function()
	{ 
		
		var id = $(this).val(); //alert(id);
	
		if(id == 'Federal' || id=='State')
		{
		    $('.divhide').hide();$('.dis').hide();
		}
		else if(id == 'Other Link')
		{
		    $('.divhide').hide();
		    $('.dis').show();
		}
		else
		{
		    $('.dis').hide();
		    $('.divhide').show();
		}
		$.get('{!!URL::to('/getlink')!!}?id='+id, function(data)
		{ //alert(data);
            $('#category').empty();
           $.each(data, function(index, subcatobj)
		   {
			   $('#category').append('<option value="'+subcatobj.category+'">'+subcatobj.category+'</option>');
		   })

		});
			
	});
});
</script>

@endsection()