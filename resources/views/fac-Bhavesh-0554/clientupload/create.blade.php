@extends('fac-Bhavesh-0554.layouts.app')
<style>
.dt-buttons,#sampleTable3_paginate{display:none;}
div.dataTables_wrapper div.dataTables_filter label ,#example_info{display: none !important;}
</style>
@section('main-content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>Upload</h1>
    </section>
    <!-- Main content -->
    <section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-success">
			     <div class="box-header">
              <div class="box-tools pull-right">
              </div>
            </div>
				<div class="col-md-12">
<div class="table-responsive">
<div class="row">
							<label class="control-label col-md-3" style="text-align: right;">Search  :</label>
							<div class="col-md-4">
								<input type="search" class="form-control" id="search-inp"/>
                                                  							</div>
						</div>
						<table style="display: none;" class="table table-hover table-bordered" id="example">
							<thead>
								<tr>
									<th>No</th>
									<th>Client No</th>
									<th>Client Company Name</th>
									<th>Type of Business</th>
									<th>Contact Name</th>
									<th>Telephone #</th>
								</tr>
							</thead>							
							<tbody>
								@foreach($common as $com)
								<tr>
									<td>{{$loop->index+1}}</td>
									<td>{{$com->filename}}</td>
									<td>{{$com->company_name}}</td>
									<td>{{$com->bussiness_name}}</td>
									<td>{{$com->first_name}} {{$com->middle_name}} {{$com->last_name}}</td>
									<td>{{$com->business_no}}</td>
								</tr>
							@endforeach
							</tbody>
						</table>
					</div>
             <form method="post" action="{{route('clientupload.store')}}" class="form-horizontal" id="businessname" name="businessname" enctype="multipart/form-data">
					{{csrf_field()}}
						
<div class="form-group {{ $errors->has('client_number') ? ' has-error' : '' }}">
							<label class="control-label col-md-3">Client Name :</label>
							<div class="col-lg-6 col-md-9">
							<!--	<input name="client_number" type="text" id="client_number" class="form-control" value=""/>-->
								
								<select name="client_number" type="text" id="client_number" class="form-control" value="">
<option value=""> Select</option>
@foreach($common as $com)
<option value="{{$com->cid}}">{{$com->first_name}} {{$com->middle_name}} {{$com->last_name}} </option>
@endforeach
</select>
                                                  @if ($errors->has('client_number'))
										<span class="help-block">
											<strong>{{ $errors->first('client_number') }}</strong>
										</span>
									@endif
							</div>
						</div>
<div class="form-group {{ $errors->has('upload_name') ? ' has-error' : '' }}">
							<label class="control-label col-md-3">Upload Name :</label>
							<div class="col-lg-6 col-md-9">
								<select name="upload_name" type="text" id="upload_name" class="form-control" value="">
<option value=""> Select</option>
<option value="SOS Certificate"> SOS Certificate</option>
<option value="SOS-AOI"> SOS-AOI</option>
@foreach($professional as $admin_pr)
<option value="{{$admin_pr->pro_id}}">Other License {{$admin_pr->profession}} {{$admin_pr->profession_state}} </option>
@endforeach
</select>                                                   @if ($errors->has('upload_name'))
										<span class="help-block">
											<strong>{{ $errors->first('upload_name') }}</strong>
										</span>
									@endif
							</div>
						</div>
						
						<div class="form-group {{ $errors->has('upload') ? ' has-error' : '' }}">
							<label class="control-label col-md-3">Upload :</label>
							<div class="col-lg-6 col-md-9">
<label class="file-upload btn btn-primary">								
<input type="file" name="upload" id="upload" class="form-control"/>

                Browse for file ... 
            </label>
 @if ($errors->has('bussiness_image_name'))
										<span class="help-block">
											<strong>{{ $errors->first('upload') }}</strong>
										</span>
									@endif
							</div>
						</div>
							<div class="form-group">
							<label class="control-label col-md-3 col-xs-12 left_991">Renewal :</label>
							<div class="col-lg-2 col-md-3 col-xs-10">
<input type="text" name="license_period" id="license_period" class="form-control" placeholder="Renewal"/>
							</div>
							<div class="col-md-2 col-xs-2">
     						<div class="row">
								<span style="margin-top: 6px;display: inline-block;">Year</span>
								</div>
                            </div>
							
						</div>
							<div class="form-group">
							<label class="control-label col-md-3">Renewal Date :</label>
							<div class="col-lg-2 col-md-3">
<input type="text" name="expired_date" id="expired_date" class="form-control" placeholder="Mar/01/2010" placeholder="Renewal Date"/>
							</div>
						</div>
						<div class="form-group ">
							<label class="col-md-3 col-xs-12 control-label left_991" style="margin-top: 16px;">Days :</label>
							<div class="col-lg-2 col-md-3 col-xs-4">
							        <span>Reminder</span>
                        <input type="text" name="reminder" id="reminder" class="form-control quantity">
                            							</div>
							<div class="col-lg-2 col-md-3 col-xs-4">
							    <span>Notification</span>
                        <input type="text" name="notification" id="notification" class="form-control quantity">
                            							</div>
							<div class="col-lg-2 col-md-3 col-xs-4">
							        <span>Warning</span>
                        <input type="text" name="warning" id="warning" class="form-control quantity">
                            							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Website Link :</label>
							<div class="col-lg-6 col-md-9">
								<input type="text" name="website_link" id="website_link" class="form-control" value="" placeholder="Website Link">
								<p><span class="sub_label">This is for Renewal Website Link:</span></p>
							</div>
						
						</div>
						<div class="form-group" style="margin-top:10px">
							<label class="col-md-3 control-label">Name :</label>
							<div class="col-lg-6 col-md-9">
								<input type="text" name="name" id="name" class="form-control" value="" placeholder="Name">
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-3 control-label">Main Website :</label>
							<div class="col-lg-6 col-md-9">
								<input type="text" name="main_website" id="main_website" class="form-control" value="" placeholder="Main Website">
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-3 control-label">Telephone :</label>
							<div class="col-lg-2 col-md-3">
								<input type="text" name="telephone" id="telephone" class="form-control" value="" placeholder="">
							</div>
						</div>
								
						<div class="card-footer">
						    <div class="form-group">
							<label class="control-label col-md-3"></label>
							<div class="col-xs-2" style="width:155px">
                                <input class="btn_new_save btn-primary1" type="submit" name="submit" value="Upload">
							</div>
							<div class="col-xs-2" style="width:155px">
                                <a class="btn_new_cancel" href="{{url('fac-Bhavesh-0554/clientupload')}}">Cancel</a> 
							</div>
						</div>
						  
						
						</div>
						
					</form>
				</div>
			</div>
		</div>
	</div>
	 </section>
<!--</div>-->


<script src="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/d004434a5ff76e7b97c8b07c01f34ca69e635d97/src/js/bootstrap-datetimepicker.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<script>
$('#expired_date').datetimepicker({
    format: 'MM/DD/YYYY'
});	
</script>
<script>
   $(document).ready(function(){
   $("#client_number").keyup(function() {
   		//console.log('htm');
   		var id = $(this).val();
   		$.get('{!!URL::to('/client_number')!!}?id='+id, function(data)
   		{ 

 $('#upload_name').empty();
              $.each(data, function(index, subcatobj)
   		  {
$('#upload_name').append('<option value="'+subcatobj.profession+' '+subcatobj.profession_state+'">'+subcatobj.profession+' '+subcatobj.profession_state+'</option>');


   		   })
   
   		});
   			
   	});
   });





$(document).ready(function(){    
   var table= $('#example').DataTable({
        paging:false,      
        ordering:false,       
    });
    $('#search-inp').on('keyup',function(){
 table.search($(this).val()).draw() ;
document.getElementById('example').style.display = "inline-table";
});
    
});
</script>
<!--<script>
$(document).ready(function(){
   /* $("#expired_date").datepicker({
		autoclose: true,
		orientation: "top",
		endDate: "today"});
    */
$('#license_period').keyup( function(){
var period = $('#license_period').val();
var monthNames = new Array(
"Jan","Feb","Mar","Apr","May","Jun","July","Aug","Sept","Oct","Nov","Dec");
var now = new Date();
if(period=='')
{
 dd = now.getFullYear();   
}
else
{
dd = now.getFullYear() + parseInt(period);
}
var dk = monthNames[now.getMonth()] + " " + now.getDate() + ", " + dd;
  //  var date = new Date();
    $('#expired_date').val(dk); 
});
});
</script>-->
 <script>

   $(document).ready(function() {
  var dateInput = $('input[name="expired_date"]'); 
  var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : 'body';
  dateInput.datepicker({
    format: 'MM/dd/yyyy',
    container: container,
    todayHighlight: true,
    autoclose: true,
    startDate:false
  });

  $('#expired_date').datepicker(); 
});

function truncateDate(date) {
  return new Date(date.getFullYear(), date.getMonth(), date.getDate());
}
</script>
<script>
$("#telephone").mask("(999) 999-9999"); 
</script> 
@endsection()