@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
<style>
    .year{padding: 0px 0px 0 0px;
   
    line-height: 29px;
    border: 2px solid #286db5;
       
    height: 34px;
    float: left;
    border-radius: 0 3px 3px 0;
    width: 16%;
    text-align: center;
    border-left: transparent;

}
.content{
    padding-top:0px !important;
}
.box-header{
    padding:5px !important;
}
</style>
<div class="content-wrapper">
	<div class="page-title">
		<h1>CE Status</h1>
	</div>
	<div class="row">
		<div class="col-md-12" style="margin-top:-10px;">
			<div class="card">
				<div class="card-body">
				    	<div class="panel with-nav-tabs panel-primary">
						<div class="panel-heading">
							<ul class="nav nav-tabs" id="myTab">
							    <?php $i=1;?>
							    @foreach($professional as $prof)
								<li @if($i=='1') class="active" @endif><a href="{{ucwords($prof->profession)}}" data-toggle="tab">CE-{{ucwords($prof->profession)}}</a></li>
								<?php $i++;?>
								@endforeach
							</ul>
						</div>
				    <div class="panel-body">
								<div class="tab-content">
								      
								  
             <form method="post" action="{{route('cestatus.store')}}" class="form-horizontal"  enctype="multipart/form-data">
					{{csrf_field()}}
					<br>
		<div class="col-md-6">				
<div class="form-group">
   <div class="col-md-12"> 
                     <div class="row">
							<label class="control-label col-md-4">Profession :</label>
							<div class="col-md-8">
							    <input type="text" name="profession" id="profession" class="form-control">
							</div>
						
						</div>
						</div>
						</div>
<div class="form-group {{ $errors->has('branchtype') ? ' has-error' : '' }}">
    <div class="col-md-12">
        <div class="row">
							<label class="control-label col-md-4">Reporting Period :</label>
							<div class="col-md-8">
							    <input type="text" name="reporting_period" id="reporting_period"  style="border-radius: 3px 0 0 3px;width: 84%;border-right: transparent;float: left;" class="form-control"><span class="year">Year</span>
							</div>
						</div>
						</div>
						</div>
						
 <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
   
                   <label class="control-label col-md-4">Reporting Year :</label>
                     <div class="col-md-8">
                        <div class="box-body pad">
                         	    <input type="text" name="reporting_year" id="reporting_year" class="form-control">
                        </div>
                     </div>
                  </div>												
						<div class="form-group">
                   <label class="control-label col-md-4">Rules  :</label>
                     <div class="col-md-8">
                        <div class="box-body pad">
                         	    <input type="text" name="rules" id="rules" class="form-control">
                        </div>
                     </div>
                  </div>
						<div class="input_fields_wrap_notes">
												
													<div class="form-group">
															<label class="control-label col-md-4">Note :</label>
															<div class="col-md-7">
														<input name="notes[]" type="text" placeholder="Create Note" id="notes" class="textonly form-control">
															<input name="noteid[]" type="hidden" placeholder="" id="noteid" class="textonly form-control">
														</div>   
														<div class="col-md-1">
													<a class="btn btn-primary" onclick="education_field_note();">Add</a>
														</div>
													</div>
													
												<div id="input_fields_wrap_notes"></div>
						</div>
						</div>
   <div class="col-md-6">				
<div class="form-group">
   <div class="col-md-12"> 
                     <div class="row">
							<label class="control-label col-md-4"></label>
						
							   <label class="control-label col-md-4" style="text-align: center;">1 year</label>
							   <label class="control-label col-md-4" style="text-align: center;">Total</label>
							</div>
						</div>
						</div>
<div class="form-group">
   <div class="col-md-12"> 
                     <div class="row">
							<label class="control-label col-md-4">General Hrs. :</label>
							<div class="col-md-4">
							    <input type="text" name="gen_hour" id="gen_hour" class="form-control txt">
							    </div>
							    <div class="col-md-4 row"><input type="text" name="gen_hour_1" id="gen_hour_1" class="form-control txt1">
							</div>
						</div>
						</div>
						</div>
<div class="form-group {{ $errors->has('branchtype') ? ' has-error' : '' }}">
    <div class="col-md-12">
        <div class="row">
							<label class="control-label col-md-4">Special Hrs. :</label>
						
							    <div class="col-md-4">
							    <input type="text" class="form-control txt"  name="spe_hour" id="spe_hour">
							    </div>
							    <div class="col-md-4 row"><input type="text" name="spe_hour_1" id="spe_hour_1" class="form-control txt1">
							</div>
						</div>
						</div>
						</div>
						
 <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
   
                   <label class="control-label col-md-4">Total Hrs. Req. :</label>

                        <div class="box-body pad">
                         	   <div class="col-md-4">
							    <input type="text" name="sum" id="sum" class="form-control" readonly>
							    </div>
							    <div class="col-md-4 row"><input type="text" name="sum1" id="sum1" class="form-control" readonly>
							</div>
                        </div>
                     </div>
						</div>
						<div class="card-footer">
							<div class="row">
								<div class="col-md-8 col-md-offset-3">
									<input class="btn btn-primary icon-btn" type="submit" name="submit" value="cREATE">
								</div>
							</div>
						</div>
						</form>
				
				
			
					</div>
					</div>
					</div>
					<br>
					<div class="col-md-12 col-sm-12 col-xs-12">
													<div class="Branch">
														<h1>Current CE Status</h1>
													</div>
												</div>
					<form enctype='multipart/form-data' class="form-horizontal" action="" id=""  method="post"> 
        	{{csrf_field()}}
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="form-group ">
                <label for="focusedinput" class="col-md-2 control-label">Year :</label>
                <div class="col-sm-8">
                  <div class="col-sm-6">
                    <select class="form-control" name="year" id="year" type="text">
                        <option value="">---Select Year---</option>
                        <?php 
                        $year =30;
                        for($i=0;$i<=40;$i++){
                            $year=date('Y',strtotime("last day of -$i year"));
                        ?>
                        <option value="<?php echo $year;?>"><?php echo $year;?></option>
                        <?php }?>
                    </select>
                  </div>
                  
                </div>
              </div>
            </div>
          </div>
          <br>
        </form>	
        <div class="table-responsive">
            <table class="table table-hover table-bordered" id="sampleTable3">
  <thead>
    <tr>
      <th scope="col">Year</th>
      <th scope="col">General Hrs.</th>
      <th scope="col">Special Hrs.</th>
      <th scope="col">Total Hrs.</th>
      <th scope="col">Need Hrs. Special</th>
      <th scope="col">Need Hrs.</th>
      <th scope="col">By Due Date</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">2001</th>
      <th scope="row">1</th>
      <td>Mark</td>
      <td>Otto</td>
      <td>@mdo</td>
      <td>Otto</td>
      <td>@mdo</td>
      </tr>
    <tr>
        <th scope="row">2002</th>
      <th scope="row">2</th>
      <td>Jacob</td>
      <td>Thornton</td>
      <td>@fat</td>
      <td>Otto</td>
      <td>@mdo</td>
    </tr>
    <tr>
        <th scope="row">2003</th>
      <th scope="row">3</th>
      <td>Larry</td>
      <td>the Bird</td>
      <td>@twitter</td>
      <td>Otto</td>
      <td>@mdo</td>
    </tr>
  </tbody>
</table>
            
        </div>
        
				</div>
			</div>
		</div>
	</div>
</div>
<script>
var room1 = 0;
var z = room1  ;
function education_field_note() {
room1++;
z++;
var objTo = document.getElementById('input_fields_wrap_notes')
var divtest = document.createElement("div");
divtest.setAttribute("class", "form-group removeclass"+z);
divtest.innerHTML = '<label class="control-label col-md-4">Note '+ z +' :</label><div class="col-md-7"><input name="noteid[]" value="" type="hidden" placeholder="" id="noteid" class=""><input name="notes[]" value="" type="text" id="notes" placeholder="Create Note" class="textonly form-control" /></div></div><div class="col-md-1"> <button class="btn btn-danger" type="button" onclick="remove_education_fields('+ z +');"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span> </button></div></div></div>';
var rdiv = 'removeclass'+z;
var rdiv1 = 'Schoolname'+z;
objTo.appendChild(divtest)
}
function remove_education_fields(rid) {
$('.removeclass'+rid).remove();
z--;
room1--;
}
</script>
<script type="text/javascript">
$(document).ready(function(){

		//iterate through each textboxes and add keyup
		//handler to trigger sum event
		$(".txt").each(function() {

			$(this).keyup(function(){
				calculateSum();
			});
		});

	});

	function calculateSum() {

		var sum = 0;
		//iterate through each textboxes and add the values
		$(".txt").each(function() {

			//add only if the value is number
			if(!isNaN(this.value) && this.value.length!=0) {
				sum += parseFloat(this.value);
			}

		});
		//.toFixed() method will roundoff the final sum to 2 decimal places
	//	$("#sum").html(sum.toFixed(2));
		document.getElementById('sum').value = sum;
	}</script>
	
	<script type="text/javascript">
$(document).ready(function(){

		//iterate through each textboxes and add keyup
		//handler to trigger sum event
		$(".txt1").each(function() {

			$(this).keyup(function(){
				calculateSum1();
			});
		});

	});

	function calculateSum1() {

		var sum1 = 0;
		//iterate through each textboxes and add the values
		$(".txt1").each(function() {

			//add only if the value is number
			if(!isNaN(this.value) && this.value.length!=0) {
				sum1 += parseFloat(this.value);
			}

		});
		//.toFixed() method will roundoff the final sum to 2 decimal places
	//	$("#sum").html(sum.toFixed(2));
		document.getElementById('sum1').value = sum1;
}
</script>
<script>
$(document).ready(function(){
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        var currentTab = $(e.target).attr("href"); // get current tab
        var LastTab = $(e.relatedTarget).text(); // get last tab
        $("#profession").val(currentTab); 
        $(".last-tab span").html(LastTab);
    });
    
    	$("#reporting_year").datepicker({
		autoclose: true,
format: "mm/dd/yyyy",
		//endDate: "today"
	});
});
</script>

<script>
$(document).ready(function(){
	$(document).on('change','#year', function()
	{
		//console.log('htm');
		var id = $(this).val(); alert(id);
		$.get('{!!URL::to('getcountycod')!!}?id='+id, function(data)
		{  
           $('#business_license1').empty();
           $.each(data, function(index, subcatobj)
		   {
$('#business_license1').val(subcatobj.countycode);
	})
		});
	});
});
</script>
@endsection()