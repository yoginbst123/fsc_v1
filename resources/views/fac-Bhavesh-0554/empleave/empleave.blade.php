@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content') 
<style>
label{float:left;}
#sampleTable3_wrapper{
    margin-top:-7px;
}
.page-title{
        padding: 8px 15px;

}
.buttons-pdf {
    font-size: 0px !important;
    border-radius: 3px;
    padding: 8px 10px !important;
    background: #fff !important;
    border-color: #c6c6c6 !important;
    color: red !important;
}
.buttons-print {
    font-size: 0px !important;
    border-radius: 3px;
    padding: 8px 10px !important;
    background: #3c8dbc !important;
}
.buttons-pdf:hover{
     background: #008d4c !important;
}

.buttons-print:hover{
     background: #367fa9 !important;
}


.fa{
    font-size: 16px !important;
}
.box-header > .box-tools {
    position: inherit;
    right: 10px;
    top: 5px;
}
.box-tools {
    position: absolute !important;
    margin-top: 7px !important;
    margin-right: 150px !important;
}
@media only screen and (max-width: 991px){
.table-title a {
    margin-top: 0px !important;
    margin-right: -10px !important;
}
}
@media only screen and (max-width: 860px){
.box-header>.box-tools {
    position: relative !important;
    margin-right: 5px !important;
    margin-top: 0px !important;
}
}
@media only screen and (max-width: 500px){
    .box-header {
        padding: 10px !important;
    }
    div.dataTables_wrapper div.dataTables_filter {
        width: 100%;
        display: flex;
    }
    div.dataTables_wrapper div.dataTables_filter label {
        width: 84%;
    }
    .box-header>.box-tools {
        position: absolute !important;
        margin-top: 51px !important;
        margin-right: 120px !important;
    }
    .table-title a {
        margin-top: -34px !important;
        margin-right: 10px !important;
    }
}
</style>
 <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header page-title">
     		<div class="" style="">
     		    <div class="" style="text-align:center;">
     		        <h2>List of Leave <span class="right_title" style="text-align:right;padding-right: 20px !important;position: absolute;right: 0;">Add / View / Edit</span></h2>
     		    </div>

     		</div>
    </section>
    
    <!-- Main content -->
    <section class="content">
      		
	<div class="row">
	
		<div class="col-md-12">
			<div class="box box-success">
			    <div class="box-header" style="padding: 0px;">
			    <div class="box-tools pull-right" data-toggle="tooltip" title="Status" style="z-index:9999;">
                    <div class="table-title">
    					
    						<a href="{{url('fac-Bhavesh-0554/empleave/create')}}">Add New Leave</a>
    						<br>
    					</div>
                  </div>
                  </div>
			    
				<div class="card-body col-md-12">
					
					@if(session()->has('success') )
    <div class="alert alert-success alert-dismissable">{{session()->get('success') }}</div>
                   @endif
					<div class="table-responsive">
						<table class="table table-hover table-bordered" id="example">
						
							<thead>
								<tr>
									<th>#</th>
									<th>Employee Name</th>
									<th>Start Date</th>
									<th>Total Days</th>
									<th>End Date</th>
										<!--<th>Leave Reason</th>-->
									<th>Leave Status</th>
									
											<th>Creation Date</th>
									<th>Action</th>
								</tr>
							</thead>							
							<tbody>
                            @foreach($leave as $com)
								<tr>
									<td>{{$loop->index+1}}</td>
									<td>@foreach($emp as $emp1)
									@if($emp1->id==$com->employee_id) {{ucwords($emp1->name)}} @endif
									@endforeach</td>
									<td>{{$com->start_date}}</td>
									<td>{{$com->total_days}}</td>
									<td>{{$com->end_date}}</td>
								<!--	<td>{!! $com->leave_reason !!}</td>-->
									<td>@if($com->status=='1') <p style="color:#eaaf2a">Pending</p> @elseif($com->status=='2') <p style="color:blue">Approve</p> @else <p style="color:red">Cancel</p> @endif </td>
										<td> {{ $com->creation_date }} </td>
									<td>

										<a class="btn-action btn-view-edit" href="{{route('empleave.edit',$com->id)}}"><i class="fa fa-edit"></i></a>
							
									</td>
								</tr>
							@endforeach
							</tbody>
							
						</table>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	</section>
<!--</div>-->

<script>
$(document).ready(function() {
    var table = $('#example').DataTable( {
        dom: 'Bfrtlip',
    "columnDefs": [ {
            "searchable": true,
            "orderable": true,
            "targets": 0
        } ],
        "order": [[ 0, 'asc' ]],
        buttons: [
                 {
                extend:    'copyHtml5',
                text:      '<i class="fa fa-files-o"></i> &nbsp; Copy',
                titleAttr: 'Copy',
                title: $('h3').text(),
                  exportOptions: {
        columns: [0,1, 2, 3,4,5], // Only name, email and role
    }
            },
            {
                extend:    'excelHtml5',
                text:      '<i class="fa fa-file-excel-o"></i>&nbsp; Excel',
               titleAttr: 'Excel',
                title: $('h3').text(),
                 customize: function( xlsx ) {
                var sheet = xlsx.xl.worksheets['sheet1.xml'];
 $('row c', sheet).attr('s', '51');
            },
                  exportOptions: {
        columns: [0,1, 2, 3,4,5], // Only name, email and role
    }
            },
            {
                extend:    'csvHtml5',
                text:      '<i class="fa fa-file-text-o"></i> &nbsp; CSV',
                titleAttr: 'CSV',
                title: $('h3').text(),
                  exportOptions: {
        columns: [0,1, 2, 3,4,5], // Only name, email and role
    }
            },
            {
                extend:    'pdfHtml5',
                text:      '<i class="fa fa-file-pdf-o"></i>&nbsp;  PDF',
                
              customize: function (doc) {
						//Remove the title created by datatTables
						doc.content.splice(0,1);
						//Create a date string that we use in the footer. Format is dd-mm-yyyy
						var now = new Date();
						var jsDate = now.getDate()+'-'+(now.getMonth()+1)+'-'+now.getFullYear();
						
						doc.pageMargins = [20,60,20,20];
						doc.defaultStyle.fontSize = 10;
						doc.styles.tableHeader.fontSize = 10;
						doc['header']=(function() {
							return {
								columns: [{
									    alignment: 'left',
										image: logo,
										width: 50,margin: [200,5]
									},{
										alignment: 'CENTER',
										text: 'List of Client',
										fontSize: 20,
										margin: [10,35],
									},],
								margin: [20, 0, 0,12],alignment: 'center',
							}
						});
						var objLayout = {};
						objLayout['hLineWidth'] = function(i) { return 2; };
						objLayout['vLineWidth'] = function(i) { return 2; };
						objLayout['hLineColor'] = function(i) { return '#ccc'; };
						objLayout['vLineColor'] = function(i) { return '#ccc'; };
						objLayout['paddingLeft'] = function(i) { return 14; };
						objLayout['paddingRight'] = function(i) { return 14; };
						doc.content[0].layout = objLayout;
				},
				titleAttr: 'PDF',
                  exportOptions: {
        columns: [0,1, 2, 3,4,5], // Only name, email and role
    }
            },
            {
           extend: 'print',
          text: '<i class="fa fa-print"></i>&nbsp; Print',
          titleAttr: 'Print',
        customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '10pt' )
                        .prepend(
                            '<center><img src=""/></center>'
                        );
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );
                },
         exportOptions: {
          columns: [0,1, 2, 3,4,5]
      },
      footer: true,
      autoPrint: true
    },],
    } );
$('input.global_filter').on( 'keyup click', function () {
        filterGlobal();
    } );
 
    $('input.column_filter').on( 'keyup click', function () {
        filterColumn( $(this).parents('tr').attr('data-column') );
    } );
  table.on( 'order.dt search.dt', function () {
       table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
          cell.innerHTML = i + 1;
          table.cell(cell).invalidate('dom'); 
       } );
    } ).draw();
       table.columns(6)
        .search('^(?:(?!Inactive|New|1).)*$\r?\n?', true, false)
        .draw();
    $("#choice").on("change",function(){
 var _val = $(this).val();//alert(_val);
 
 if(_val == 'Inactive'){   
        table.columns(6).search(_val).draw();
  }
 else if(_val == 'New'){   
        table.columns(6).search(_val).draw();
  }
  else if(_val == 'Active'){  //alert();
         table.columns(6).search(_val).draw();
          table.columns(6)
        .search('^(?:(?!Inactive|New|1).)*$\r?\n?', true, false)
        .draw();
  }
  else{
        table
        .columns()
        .search('')
        .draw(); 
  }
  })
} );
   	</script>
@endsection





