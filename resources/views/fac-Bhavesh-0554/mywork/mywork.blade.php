@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
<style>
    .buttons-pdf {
    font-size: 0px !important;
    border-radius: 3px;
    padding: 8px 10px !important;
    background: #fff !important;
    border-color: #c6c6c6  !important;
    color:red !important;
}
.buttons-print {
    font-size: 0px !important;
    border-radius: 3px;
    padding: 8px 10px !important;
    background: #3c8dbc !important;
    border-color: #367fa9 !important;
}
.buttons-excel{
    font-size:0px !important;
    border-radius: 3px;
    padding: 8px 10px !important;
    background: #00a65a !important;
    border-color: #008d4c !important;
}
.buttons-excel:hover{
    background: #008d4c !important;
}
.buttons-pdf:hover{
    background: #f6f6f6  !important;
}
.buttons-print:hover{
    background: #367fa9 !important;
}
#clientList{
    margin:5px 0px 0px 3px;
    padding:0px 0px;
    list-style-type:none;
    max-height: 200px;
    overflow: auto; position: absolute;
    width:450px;
    z-index: 99;}
#clientList li{border-bottom:1px solid #025b90;
    background: #ef7c30;}
#clientList li a{padding:0px 10px 0px 0px !important;margin:0px !important; display:block; color:#fff!important;}
#clientList li:hover{ background:#dc6d23;}
#countryList li a{height:40px;}
#countryList li a span.bgcolors{     background: linear-gradient(to bottom, #b3dced 0%,#29b8e5 50%,#bce0ee 100%) !important;
    padding: 6px 0px; text-align:center;
    display: block;
    font-size: 20px;
    font-weight: bold;
    float: left;
    width: 60px;}
#countryList li a span.clientalign{display: flex;
    width:100px;
    float: left;
    line-height: 15px; font-size:13px;
    padding: 4px 0px;
    flex-direction: row;
    justify-content: flex-start;
    align-items: center;
    align-content: center;
    min-height: 40px; background:#038ee0;
    margin-left: 0;
    padding-left: 5px;}
#countryList li a span.entityname{display: flex;
    width:190px
    float: left;
    line-height: 15px;
    padding: 4px 0px; font-size:13px;
    flex-direction: row;
    justify-content: flex-start;
    align-items: center;
    align-content: center;
    min-height: 38px; padding-left:10px;
    margin-left: 10px;}
    .table tr td.text-center{text-align:center!important;}
      .modal-body ul{margin:0px; padding:0px;}
    .modal-body ul li{text-align:left!important; list-style-type:none!important; font-size:12px!important;}
    .modal.fade.in{padding-top:10px!important;}
    .custom_radio{
        height:17px;
        width:17px;
        vertical-align: middle;
    margin-top: -4px !important;
}
#clientList li a{height:40px;}
#clientList li a{height:40px;}
#clientList li a span.bgcolors{    
    background: linear-gradient(to bottom, #b3dced 0%,#29b8e5 50%,#bce0ee 100%) !important;
    padding: 6px 0px; text-align:center;
    display: block;
    font-size: 20px;
    font-weight: bold;
    float: left;
    width: 60px;
}
#clientList li a span.clientalign{
    display: flex;
    width:100px;
    float: left;
    line-height: 15px; font-size:13px;
    padding: 4px 0px;
    flex-direction: row;
    justify-content: flex-start;
    align-items: center;
    align-content: center;
    min-height: 40px; background:#038ee0;
    margin-left: 0;
    padding-left: 5px;
}
#clientList li a span.entityname{
    display: flex;
    width:190px
    float: left;
    line-height: 15px;
    padding: 4px 0px; font-size:13px;
    flex-direction: row;
    justify-content: flex-start;
    align-items: center;
    align-content: center;
    min-height: 38px; padding-left:10px;
    margin-left: 10px;
}
.datepicker-dropdown{
    z-index:10000 !important;
}
div.dataTables_wrapper div.dataTables_filter{
    margin-bottom:0px;
}
table.dataTable{
    margin-top:0px !important;
}
.fa{
    font-size: 16px !important;
}
@media (min-width: 768px){
.modal-dialog {
    width: 800px;
    margin: 30px auto;
}
}
label .error{
    color:red;
}
.required{
    color:red;
}
tr.group,
tr.group:hover {
    background-color: #ddd !important;
}
tr.group{
    border: 1px solid black;
}
tr.group td {
  border-bottom: 1px solid black;
}
table.table-bordered.dataTable tbody td{
      border-bottom: 0.5px solid black !important; 
}
.picker{
    display:none;
}
@media (max-width: 1340px){
    #sampleTable32{
        display: block; 
        overflow-x: auto;
        white-space: nowrap;
    }
}
@media (max-width: 1272px){
    .resp_table_cust{
        margin-top:0px !important;
    }
    .box-header .row .col-md-4{
        width:auto;
        z-index: 100;
        right: 130px;
        position: absolute;
    }
    .box-header > .box-tools{
        position: relative !important;
        margin-right: -5px !important;
    }
}
@media (max-width: 1110px){
    .nav-tabs > li {
        width: 24.5% !important;
        margin: 2px 0 0 3px;
    }
    .nav>li>a {
        padding: 10px 5px;
        font-size: 15px !important;
    }
}
@media (max-width: 991px){
    .nav-tabs > li {
        width: 48.5% !important;
        margin: 3px 0 3px 7px !important;
    }
    .nav>li>a {
        padding: 10px 5px;
        font-size: 16px !important;
    }
    .table-title a {
        margin-top: 0px !important;
        margin-right: 0px !important;
    }
}
@media (max-width: 666px){
 span.right_title {
    display: list-item;
    position: relative;
    margin-top: 5px;
    padding-right: 0px !important;
}   
.right_title span{
    padding-right:100px !important;
}
#livetime{
    padding-right:0px !important;
}
}
@media (max-width: 520px){
    .box-header .row .col-md-4 {
        width: 100%;
        z-index: 100;
        right: 0;
        position: absolute;
        text-align: center;
    }
    div.dataTables_wrapper div.dataTables_filter {
        width: 100%;
        display: flex;
    }
    div.dataTables_wrapper div.dataTables_filter label {
        width: 84%;
    }
    .table-title a {
        margin-top: 48px !important;
        margin-right: 150px !important;
    }
    .resp_table_cust {
        margin-top: 40px !important;
    }
    .box-header > .box-tools {
        position: absolute !important;
        margin-right: -10px !important;
    }
}
@media (max-width: 420px){
    .nav-tabs > li {
        width: 48.5% !important;
        margin: 3px 0 3px 4px !important;
    }
    .nav>li>a {
        padding: 10px 5px;
        font-size: 14px !important;
    }
    .box-header > .box-tools {
        position: absolute !important;
        margin-right: -10px !important;
    }
}
</style>
<?php
    $ccountry = "America/New_York";
    date_default_timezone_set($ccountry);
    $time =date_default_timezone_get();
    $day = date('l');
    //echo $commons;
?>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"> </script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.4.1/moment-timezone-with-data-2010-2020.min.js"> </script>
<script type="text/javascript">   
    $(function(){
        setInterval(function(){
            var divUtc = $('#divUTC');
            var divLocal = $('#divLocal');  
            //put UTC time into divUTC  
            divUtc.text(moment.utc().format('YYYY-MM-DD HH:mm:ss a'));      
            var localTime  = moment.utc(divUtc.text()).toDate();
            //localTime = moment(localTime).format('YYYY-MM-DD HH:mm:ss a');
            //divLocal.text(localTime);        
            //$('#divThai').text(moment.tz('Asia/Bangkok').format('YYYY-MM-DD HH:mm:ss a'));
            
            $('#livetime').text(moment.tz('{{$time}}').format('hh:mm a'));
        },1000);
    });
</script>
<div class="content-wrapper">
    <section class="content-header page-title" >
        <div class="" >
            <div class="">
                <h2>Today's Work
                <span class="left_title " style="padding-left: 20px;position: absolute;left: 0;" id="today_date">{{ date('m/d/Y')}}</span>
                <span class="right_title">
                <span class="" style="padding-right: 118px;position: absolute;right: 0;">{{$day}} / </span>
                <span class="" id="livetime" style="padding-right: 20px;position: absolute;right: 0;"></span></span></h2>
            </div>
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
	<div class="row">
	    <div class="panel-heading">
			<ul class="nav nav-tabs" id="myTab">
				<li style="margin-top:0px ;" class="active"><a href="#tabprimary" data-toggle="tab" class="alery">My Work</a></li>
				<li style="margin-top:0px ;"><a href="#tab1primary" data-toggle="tab" class="alery">Employee Work</a></li>
				<li style="margin-top:0px ;"><a href="#tab2primary" data-toggle="tab" class="alery">My Work-Under Progress</a></li>
				<li style="margin-top:0px ;"><a href="#tab3primary" data-toggle="tab" class="alery">My Work -Waiting</a></li>
			</ul>
		</div>
	    <div class="tab-content">
	        <div class="tab-pane fade in active" id="tabprimary">
	            <div class="col-md-12">
        			<div class="box box-success">
                        <div class="box-header" style="padding-top:0px;">
                            <div class="box-tools pull-right" style="position: absolute;margin-right: 132px;margin-top:7px;z-index:100;">
                                <div class="table-title">
                                    <a data-toggle="modal" data-target="#add_work" style="padding:7px 12px;cursor:pointer;">Add New Work</a>
                                </div>
                            </div>
                            <div class="row" style="margin-top:9px;">
                            <div class="col-md-5" style="width: 36.8%;"></div>
                            <div class="col-md-4" style="z-index:100;">
                                <a href="#" id="previous_day" data-diff="-1" class="btn btn-primary"><i class="fa fa-chevron-left"></i></a>
                                <input type="text" id="test" style="vertical-align: middle;height: 40px;border-radius: 3px;border: 2px solid #2fa6f2;font-size: 17px !important;padding: 6px 0px;text-align: center;font-weight: 700;width: 110px;">
                                <input type="text" id="day_text" style="vertical-align: middle;height: 40px;border-radius: 3px;border: 2px solid #2fa6f2;font-size: 17px !important;padding: 6px 0px;text-align: center;font-weight: 700;width: 115px;" value="Friday" readonly>
                                <a href="#" id="next_day" data-diff="1" class="btn btn-primary"><i class="fa fa-chevron-right"></i></a>
                            </div>
                            </div>
                        </div>
        				<div class="col-md-12 resp_table_cust" style="margin-top:-47px;">
        					@if (session()->has('success'))
        					    <div class="alert alert-success alert-dismissable">{{session()->get('success') }}</div>
                            @endif
        					<div class="table-responsive">
        						<table class="table table-hover table-bordered " id="sampleTable3" style="width:100% !important">
        							<thead>
        							    <tr>
        							        <th>id</th>
        							        <th>Priority</th>
        							        <th>Priority_ID</th>
        							        <th>Date</th>
        							        <th>Time</th>
        							        <th>Client ID</th>
        							        <th>Client Name</th>
        							        <th>Type of Work</th>
        							        <th>EstTime</th>
        							        <th style="width:10%">Status</th>
        							        <th>Action</th>
        						        </tr>
        							</thead>
        							<tbody>
                                	</tbody>
        						</table>
        					</div>
        				</div>
        			</div>
        		</div>  
        	</div>
        	<div class="tab-pane fade" id="tab1primary">
        	    <div class="col-md-12">
        			<div class="box box-success">
        			    <div class="box-header" style="padding-top:0px;">
                          <div class="box-tools pull-right" style="position: absolute;margin-right: 132px;margin-top:7px;z-index:100;">
                 <!--           <div class="table-title">-->
            					<!--<a  data-toggle="modal" data-target="#add_work">Add New Work</a>-->
            			  <!--  </div>-->
                         </div>
                        </div>
        				<div class="col-md-12">
        					@if (session()->has('success'))
        					    <div class="alert alert-success alert-dismissable">{{session()->get('success') }}</div>
                            @endif
        					<div class="table-responsive">
        						<table class="table table-hover table-bordered" id="sampleTable32">
        							<thead>
        							    <tr>
        							        <th>No</th>
        							        <th>Priority</th>
        							        <th>Priority_ID</th>
        							        <th>Date</th>
        							        <th>Time</th>
        							        <th>Client ID</th>
        							        <th>Client Name</th>
        							        <th>Type of Work</th>
        							        <th>EstTime</th>
        							        <th style="width:200px;">Status</th>
        							        <th>Action</th>
        						        </tr>
        							</thead>
        							<tbody>
        							        @foreach($mywork as $work)
        							        <tr>
            							        <td>{{$work->id}}</td>
            							        <td>{{$work->priority}}</td>
            							        <td>{{$work->priority_id}}</td>
            							        <td>@if($work->due_date != NULL){{date('m/d/Y',strtotime($work->due_date))}} @endif</td>
            							        <td></td>
            							        <td>{{$work->client_id}}</td>
            							        <td>{{$work->client_name}}</td>
            							        <td>{{$work->type_of_work}}</td>
            							        <td>{{$work->estimated_time}}</td>
            							        <td>
            							            <select class="form-control" name="status" id="">
                                                        <option value="">Select</option>
                                                        <option value="Under Progress" {{'Under Progress' == $work->status ? 'selected="selected"' : '' }}>Under Progress</option>
                                                        <option value="Hold" {{'Hold' == $work->status ? 'selected="selected"' : '' }}>Hold</option>
                                                        <option value="Waiting" {{'Waiting' == $work->status ? 'selected="selected"' : '' }}>Waiting</option>
                                                    </select>
            							        </td>
            							        <td>
            							            <a href="{{route('mywork.edit',$work->id)}}" class="btn-action btn-view-edit"><i class="fa fa-edit"></i></a>
            							            <a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record ?')){event.preventDefault();document.getElementById('delete-id-{{$work->id}}').submit();} else{event.preventDefault();}" href="{{route('mywork.destroy',$work->id)}}"><i class="fa fa-trash"></i></a>
                                                    <form action="{{ route('mywork.destroy',$work->id) }}" method="post" style="display:none" id="delete-id-{{$work->id}}">
                                                        {{csrf_field()}} {{method_field('DELETE')}}
                                                    </form>
            							        </td>
        							        </tr>
            							    @endforeach
                                	</tbody>
        						</table>
        					</div>
        				</div>
        			</div>
        		</div>
        	</div>
        	<div class="tab-pane fade" id="tab2primary">
        	    <div class="col-md-12">
        			<div class="box box-success">
        			    <div class="box-header" style="padding-top:0px;">
                          <div class="box-tools pull-right" style="position: absolute;margin-right: 132px;margin-top:7px;z-index:100;">
                 <!--           <div class="table-title">-->
            					<!--<a  data-toggle="modal" data-target="#add_work">Add New Work</a>-->
            			  <!--  </div>-->
                         </div>
                        </div>
        				<div class="col-md-12">
        					@if (session()->has('success'))
        					    <div class="alert alert-success alert-dismissable">{{session()->get('success') }}</div>
                            @endif
        					<div class="table-responsive">
        						<table class="table table-hover table-bordered" style="width:100%" id="sampleTableForUnderProgress">
        							<thead>
        							    <tr>
        							        <th>id</th>
        							        <th>Priority</th>
        							        <th>Priority_ID</th>
        							        <th>Date</th>
        							        <th>Time</th>
        							        <th>Client ID</th>
        							        <th>Client Name</th>
        							        <th>Type of Work</th>
        							        <th>EstTime</th>
        							        <th style="width:10%">Status</th>
        							        <th>Action</th>
        						        </tr>
        							</thead>
        							<tbody>
                                	</tbody>
        						</table>
        					</div>
        				</div>
        			</div>
        		</div>
        	</div>
        	<div class="tab-pane fade" id="tab3primary">
        	    <div class="col-md-12">
        			<div class="box box-success">
        			    <div class="box-header" style="padding-top:0px;">
                          <div class="box-tools pull-right" style="position: absolute;margin-right: 132px;margin-top:7px;z-index:100;">
                 <!--           <div class="table-title">-->
            					<!--<a  data-toggle="modal" data-target="#add_work">Add New Work</a>-->
            			  <!--  </div>-->
                         </div>
                        </div>
        				<div class="col-md-12">
        					@if (session()->has('success'))
        					    <div class="alert alert-success alert-dismissable">{{session()->get('success') }}</div>
                            @endif
        					<div class="table-responsive">
        						<table class="table table-hover table-bordered" style="width:100%"  id="sampleTableForWating">
        							<thead>
        							    <tr>
        							        <th>id</th>
        							        <th>Priority</th>
        							        <th>Priority_ID</th>
        							        <th>Date</th>
        							        <th>Time</th>
        							        <th>Client ID</th>
        							        <th>Client Name</th>
        							        <th>Type of Work</th>
        							        <th>EstTime</th>
        							        <th style="width:10%">Status</th>
        							        <th>Action</th>
        						        </tr>
        							</thead>
        							<tbody>
        							       
                                	</tbody>
        						</table>
        					</div>
        				</div>
        			</div>
        		</div>
        	</div>
        </div>
	</div>
	<div id="add_work" class="modal fade">
        <div class="modal-dialog modal-xl" style="width:70%">
            <form method="post" action="{{route('mywork.store')}}" class="form-horizontal" id="mywork_form">
                {{csrf_field()}}
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Add Work</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-md-4 control-label text-right">Priority : <span class="required">*</span></label>
                            <div class="col-md-3" style="width: 18%;">
                                <select class="form-control" id="work_priority" name="work_priority">
                                    <option value="">Select Priority</option>
                                    <option value="Very Urgent">Very Urgent</option>
                                    <option value="Time Sensitive">Time Sensitive</option>
                                    <option value="Regular">Regular</option>
                                </select>
                            </div>
                            <div id="showDate">
                                <label class="col-md-1" style="padding-top: 7px;width: auto;">Due Date : </label>
                                <div class="col-md-2" style="width:21%">
                                    <input type="text" class="form-control" name="due_date" id="due_date">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <label class="control-label col-md-4 text-right"><strong>Type Of Client :</strong></label>
                                <div class="col-md-4">
                                    <input type="radio" id="new_client" name="client_type" value="1">
                                    <label for="new_client" style="margin-left:5px;">New Client</label>
                                </div>
                                <div class="col-md-4">
                                    <input type="radio" id="already_client" name="client_type" value="2" checked>
                                    <label for="already_client" style="margin-left:5px;">Already Client</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row" id="search">
                            <label class="control-label col-md-4 text-right"><strong>Search Client :</strong></label>
                            <div class="col-md-6">
                                <input type="text" name="csearch" id="search_client_name" class="form-control" placeholder="Search">
                                <ul id="clientList"></ul>
                                <input type="hidden" name="client_id" id="client_id" value=""/>
                            </div>
                        </div>
                        <div class="form-group row" id="show_client_name">
                            <label class="col-md-4 control-label text-right">Client Name : <span class="required">*</span></label>
                            <div class="col-md-6">
                                <input type="text" id="client_name" name="client_name" class="form-control" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 control-label text-right">Type : <span class="required">*</span></label>
                            <div class="col-md-6">
                                <select class="form-control" id="types" name="types">
                                    <option value="">Select Type</option>
                                    <option value="Call">Call</option>
                                    <option value="Work">Work</option>
                                    <option value="Appointment">Appointment</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 control-label text-right">Type of Work : </label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="type_of_work" id="type_of_work">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 control-label text-right">Details : </label>
                            <div class="col-md-6">
                                <textarea rows="3" class="form-control" name="details"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 control-label text-right">Estimate Time : </label>
                            <div class="col-md-3">
                                <input type="text" class="form-control" name="est_time" id="est_time">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 control-label text-right">Status : </label>
                            <div class="col-md-3">
                                <select class="form-control" name="status">
                                    <option value="">Select</option>
                                    <option value="Under Progress">Under Progress</option>
                                    <option value="Hold">Hold</option>
                                    <option value="Waiting">Waiting</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 control-label text-right"></label>
                            <div class="col-md-3">
                                <input class="btn_new_save btn-primary1 primary1" type="submit" value="Save">
                            </div>
                            <div class="col-md-3">
                                <a class="btn_new_cancel" data-dismiss="modal">Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>

<script src="https://cdnjs.cloudflare.com/ajax/libs/pickadate.js/3.5.6/compressed/picker.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pickadate.js/3.5.6/compressed/picker.date.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pickadate.js/3.5.6/compressed/picker.time.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(e){
        $('#est_time').mask('99:99');
        //$('#showDate').hide();
        $('#due_date').datepicker();
        // $('#today_date').datepicker({
        //     success: function (data, textStatus) {
        //         alert(data);
        //     }
        // });
    //     $("#today_date").datepicker({
    //         onSelect: function(dateText) {
    //             console.log("Selected date: " + dateText + "; input's current value: " + this.value);
		  //      $(this).change();
		  //  }
		  //  }).on("change", function() {
    //                 console.log("Got change event from field");
    //     });
    $('#today_date').datepicker({  
        format: 'mm/dd/yyyy',  
    }).on('changeDate', function(e) {  
        $('#today_date').text(e.format());
        $(this).datepicker('hide'); 
    });  
        // var datepickertb = $("#today_date").datepicker({
        //     format: 'mm/dd/yyyy'
        // });
        // datepickertb.on('changeDate', function(ev){
        //     console.log(ev.format);
        //     datepickertb.datepicker('hide');
        // });
        $('#new_client').on('click',function(){
            $('#search').hide();
            $('#client_name').removeAttr("readonly");
        });
        $('#already_client').on('click',function(){
           $('#search').show();
           $('#client_name').attr("readonly","readonly");
        });
        $('#work_priority').on('change',function(){
   	       var selectedItem = $('#work_priority').find(":selected").val();
   	       //alert(selectedItem);
   	       if(selectedItem == "Regular" || selectedItem == "Appoinment"){
   	           $('#due_date').removeAttr("required");
   	           $('#showDate').hide();
   	       }else if(selectedItem == "Time Sensitive" || selectedItem == "Urgent"){
   	           $('#due_date').attr("required","required");
   	           $('#showDate').show();
   	       }else{
   	           $('#showDate').hide();
   	       }
   	    });
        $('#search_client_name').keyup(function(){ 
            var query = $(this).val();
            //alert(query);
            if(query != '')
            { 
             var _token = $('input[name="_token"]').val();
             $.ajax({
              url:"{{ route('admin.praposal_fetch') }}",
              method:"POST",
              data:{query:query, _token:_token},
              success:function(data){
                $('#clientList').fadeIn();
                $('#clientList').html(data);
              }
             });
            }
            else{
                $('#clientList').empty();
            }
        });
        $("#mywork_form").validate({
           rules: {
               work_priority: {
                   required: true,
               },
               types: {
                   required: true,
               },
               client_name: {
                   required: true,
               }
           },
           messages: {
               work_priority: {
                   required: "Please Select Priority"
               },
               types: {
                   required: "Please Select Types",
               },
               client_name: {
                   required: "Please Enter Client Name Or Search Client Name",
               }
           },
           highlight: function(e) {
               $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
               $(e).closest('.form-tab').removeClass('has-info').addClass('has-error');
           },
           success: function(e) {
               $(e).closest('.form-group').removeClass('has-error');
               $(e).closest('.form-tab').removeClass('has-error'); //.addClass('has-info');
               $(e).remove();
           },
           submitHandler: function(form) {
               form.submit();
           }
       });
    });
    function getClientdetailForPraposal(id)
    {
            //var query = $(this).val();
            //alert(id);
            if(id != '')
            { 
             var _token = $('input[name="_token"]').val();
             $.ajax({
                url:"{{ route('admin.getDetailPraposal') }}",
                method:"POST",
                data:{query:id, _token:_token},
                success:function(data){
                    var json = JSON.parse(data);
                     $("#lbl_title").empty();
                    for(var i =0;i<json.length;i++){
                        $('#client_id').val(json[i]['filename']);
                        //$('#client_name').val(json[i]['cid']+"-"+json[i]['name']);
                        $('#client_name').val(json[i]['name']);
                    }
                    $('#clientList').empty();
                }
             });
            }
        }
    function updateStatus(sel){
        var opt = sel.options[sel.selectedIndex];
        var price = opt.dataset.price;
        var value = opt.value;
        var url='{!!URL::to('fac-Bhavesh-0554/mywork/changeOrder')!!}/'+price+'/'+value;
        $.get(url, function(data){ 
            console.log(data);
        });
    }
    function myFunction(sel) {
      var opt = sel.options[sel.selectedIndex];
      var price = opt.dataset.price;
      console.log(price);
    }
var picker = $('#test').pickadate({
  format: 'mm/dd/yyyy',
}).pickadate('picker');
var date = new Date();
picker.set('select', new Date());
var weekday = new Array(7);
    weekday[0] = "Sunday";
    weekday[1] = "Monday";
    weekday[2] = "Tuesday";
    weekday[3] = "Wednesday";
    weekday[4] = "Thursday";
    weekday[5] = "Friday";
    weekday[6] = "Saturday";
  console.log(weekday[date.getDay()]);
$('#day_text').val(weekday[date.getDay()]);
$('#previous_day, #next_day').click(function(e) {
  e.preventDefault();
  setDate($(this).data('diff'));
})
function setDate(diff) {
  var date = new Date(picker.get('select').pick);
  var newDate = date.setDate(date.getDate() + diff);
  var weekday = new Array(7);
    weekday[0] = "Sunday";
    weekday[1] = "Monday";
    weekday[2] = "Tuesday";
    weekday[3] = "Wednesday";
    weekday[4] = "Thursday";
    weekday[5] = "Friday";
    weekday[6] = "Saturday";
  console.log(weekday[date.getDay()]);
  picker.set('select', newDate);
  $('#day_text').val(weekday[date.getDay()]);
}
$("#test").datepicker({
    autoclose: true,
    format: "mm/dd/yyyy",
});   
</script>
<!--</div>-->
@endsection()