@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
<div class="content-wrapper">
   <div class="page-title">
      <h1>Tax Authorities</h1>
   </div>
   <div class="row">
      <!--<div class="branch">Federal</div>-->
      <div class="col-md-12">
         <div class="card">
            <div class="card-body">
               <div class="Branch">
                  <h1>County / City</h1>
               </div>
               <br>
               <div class="table-title">
                  <h3></h3>
                  <a href=">Add New</a>
               </div>
               <br>@if ( session()->has('success') )
               <div class="alert alert-success alert-dismissable">{{session()->get('success') }}</div>
               @endif
               <div class="table-responsive">
                  <table class="table table-hover table-bordered" id="sampleTable3">
                     <thead>
                        <tr>
                           <th>#</th>
                           <th>County Name</th>
                           <th>County Code</th>
                           <th>Telephone</th>
                           <th>Website</th>
                           <th>Action</th>
                        </tr>
                     </thead>
                     <tbody>
                       
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection()