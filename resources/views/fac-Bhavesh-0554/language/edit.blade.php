@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
<div class="content-wrapper">
   <div class="page-title">
      <h1>Tax Authorities</h1>
   </div>
   <div class="row">
      <div class="col-md-12">
         <div class="card">
            <div class="card-body">
               <div class="Branch">
                  <h1>County / City</h1>
               </div>
               <br>
               <form method="post" action="" class="form-horizontal" id="businessname" name="businessname" enctype="multipart/form-data">
                  {{csrf_field()}}{{method_field('PATCH')}}
                 
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
<script>
   $(document).ready(function(){
   	$(document).on('change','.category', function()
   	{
   		//console.log('htm');
   		var id = $(this).val();
   		$.get('{!!URL::to('getcounty1')!!}?id='+id, function(data)
   		{  
               $('#tax_authority').empty();
   $('#tax_authority').append('<option value="">---Name of the Tax Authority---</option>');
              $.each(data, function(index, subcatobj)
   		   {
   	$('#tax_authority').append('<option value="'+subcatobj.county+'">'+subcatobj.county+'</option>');
   		   })
   
   		});
   			
   	});
   });
</script>
<script>
   $(document).ready(function(){
   	$(document).on('change','.category1', function()
   	{
   		//console.log('htm');
   		var id = $(this).val();
   		$.get('{!!URL::to('getcountycode1')!!}?id='+id, function(data)
   		{  
               $('#country_code').empty();
              $.each(data, function(index, subcatobj)
   		   {
   $('#country_code').val(subcatobj.countycode);
   	
   		   })
   
   		});
   			
   	});
   });
</script>
@endsection()