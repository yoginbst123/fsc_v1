<form action="{{route('fscemployeereport.update',$student->id)}}" method="post" id="ajax2">
            <div class="modal-body">
               {{ csrf_field() }} {{method_field('PATCH')}}
               <div class="col-md-12">
                  <div class="col-md-12">
                     <div class="form-group has-feedback">
                        <label for="confirmPassword" class="col-md-3 control-label">
                        Date
                        </label>
                        <div class="col-md-9 inputGroupContainer">
                           <div class="input-group">
                              <input type="text" value="{{$student->emp_in_date}}" class="form-control date" required  name="date" id="date" placeholder="Please Enter Your  Date">
                           </div>
                        </div>
                     </div>
                     <br>
                     <br>
                    
                     <div class="form-group has-feedback">
                        <label for="confirmPassword" class="col-md-3 control-label">
                        Clock In
                        </label>
                        <div class="col-md-3 inputGroupContainer">
                           <div class="input-group">
                             <select class="form-control" name="clockin" id="clockin" required>
                                 <option value="">Select</option>
                                 <?php for($i=0;$i<24;$i++){ $value = str_pad($i,2,"0",STR_PAD_LEFT);?>
                                 <option value="<?php echo $value;?>" <?php if(date("h", strtotime($student->emp_in)) == $value) { echo "selected";}?>><?php echo $value;?></option>
                                 <?php }?>
                             </select>
                             </div>
                             </div>
                             <div class="col-md-3 inputGroupContainer">
                                 <div class="">
                           <div class="input-group">
                             <select class="form-control" name="clockin_second" id="clockin_second" required>
                                 <option value="">Select</option>
                                 <?php for($i=00;$i<60;$i++){ $value = str_pad($i,2,"0",STR_PAD_LEFT);?>
                                 <option value="<?php echo $value;?>" <?php if(date("i", strtotime($student->emp_in)) == $value) { echo "selected";}?>><?php echo $value;?></option>
                                 <?php }?>
                             </select>
                             </div>
                              </div>
                        </div>
                         <div class="col-md-3 inputGroupContainer">
                           <div class="input-group">
                             <select class="form-control" name="clockin_am" id="clockin_am" required>
                                 <option value="">Select</option>
                                  <?php for($i=0;$i<60;$i++){$value = str_pad($i,2,"0",STR_PAD_LEFT);?>
                                 <option value="<?php echo $value;?>" <?php if(date("s", strtotime($student->emp_in)) == $value) { echo "selected";}?>><?php echo $value;?></option>
                                 <?php }?>
                             </select>
                             </div>
                        </div>
                     </div>
                     <br>
                     <br>
                      <div class="form-group has-feedback">
                        <label for="confirmPassword" class="col-md-3 control-label">
                        Lunch In
                        </label>
                        <div class="col-md-3 inputGroupContainer">
                           <div class="input-group">
                             <select class="form-control" name="lunchin">
                                 <option value="">Select</option>
                                 <?php for($i=0;$i<24;$i++){ $value = str_pad($i,2,"0",STR_PAD_LEFT);?>
                                 <option value="<?php echo $value;?>" <?php if($student->launch_in=='') {}else{if(date("h", strtotime($student->launch_in)) == $value) { echo "selected";}}?>><?php echo $value;?></option>
                                 <?php }?>
                             </select>
                             </div>
                             </div>
                             <div class="col-md-3 inputGroupContainer">
                                 <div class="">
                           <div class="input-group">
                             <select class="form-control" name="lunchin_second">
                                 <option value="">Select</option>
                                 <?php for($i=0;$i<60;$i++){ $value = str_pad($i,2,"0",STR_PAD_LEFT);?>
                                 <option value="<?php echo $value;?>"  <?php if($student->launch_in=='') {}else{ if(date("i", strtotime($student->launch_in)) == $value) { echo "selected";}}?>><?php echo $value;?></option>
                                 <?php }?>
                             </select>
                             </div>
                              </div>
                        </div>
                         <div class="col-md-3 inputGroupContainer">
                           <div class="input-group">
                             <select class="form-control" name="lunchin_am">
                                 <option value="">Select</option>
                                  <?php for($i=0;$i<60;$i++){$value = str_pad($i,2,"0",STR_PAD_LEFT);?>
                                 <option value="<?php echo $value;?>"  <?php if($student->launch_in=='') {}else{ if(date("s", strtotime($student->launch_in)) == $value) { echo "selected";}}?>><?php echo $value;?></option>
                                 <?php }?>
                             </select>
                             </div>
                        </div>
                        
                     </div>
                     <br>
                     <br>
                     <div class="form-group has-feedback">
                        <label for="confirmPassword"  class="col-md-3 control-label">
                        Lunch Out
                        </label>
                          <div class="col-md-3 inputGroupContainer">
                           <div class="input-group">
                             <select class="form-control" name="lunchout">
                                 <option value="">Select</option>
                                 <?php for($i=0;$i<24;$i++){$value = str_pad($i,2,"0",STR_PAD_LEFT); ?>
                                 <option value="<?php echo $value;?>"  <?php if($student->launch_out=='') {}else{ if(date("h", strtotime($student->launch_out)) == $value) { echo "selected";}}?>><?php echo $value;?></option>
                                 <?php }?>
                             </select>
                             </div>
                             </div>
                             <div class="col-md-3 inputGroupContainer">
                                 <div class="">
                           <div class="input-group">
                             <select class="form-control" name="lunchout_second">
                                 <option value="">Select</option>
                                 <?php for($i=0;$i<60;$i++){ $value = str_pad($i,2,"0",STR_PAD_LEFT);?>
                                 <option value="<?php echo $value;?>" <?php if($student->launch_out=='') {}else{ if(date("i", strtotime($student->launch_out)) == $value) { echo "selected";}}?>><?php echo $value;?></option>
                                 <?php }?>
                             </select>
                             </div>
                              </div>
                        </div>
                         <div class="col-md-3 inputGroupContainer">
                           <div class="input-group">
                             <select class="form-control" name="lunchout_am">
                                 <option value="">Select</option>
                                 <?php for($i=0;$i<60;$i++){ $value = str_pad($i,2,"0",STR_PAD_LEFT);?>
                                 <option value="<?php echo $value;?>" <?php if($student->launch_out=='') {}else{ if(date("s", strtotime($student->launch_out)) == $value) { echo "selected";}}?>><?php echo $value;?></option>
                                 <?php }?>
                             </select>
                             </div>
                        </div>
                     </div>
                     <br>
                     <br>
                     <div class="form-group has-feedback">
                        <label for="confirmPassword"  class="col-md-3 control-label">
                        Clock Out
                        </label>
                        
                        <div class="col-md-3 inputGroupContainer">
                           <div class="input-group">
                             <select class="form-control" name="clockout" id="clockout" required>
                                 <option value="">Select</option>
                                 <?php for($i=0;$i<24;$i++){ $value = str_pad($i,2,"0",STR_PAD_LEFT);?>
                                 <option value="<?php echo $value;?>" <?php if(date("h", strtotime($student->emp_out)) == $value) { echo "selected";}?>><?php echo $value;?></option>
                                 <?php }?>
                             </select>
                             </div>
                             </div>
                             <div class="col-md-3 inputGroupContainer">
                                 <div class="">
                           <div class="input-group">
                             <select class="form-control" name="clockout_second" id="clockout_second" required>
                                 <option value="">Select</option>
                                 <?php for($i=0;$i<60;$i++){ $value = str_pad($i,2,"0",STR_PAD_LEFT);?>
                                 <option value="<?php echo $value;?>" <?php if(date("i", strtotime($student->emp_out)) == $value) { echo "selected";}?>><?php echo $value;?></option>
                                 <?php }?>
                             </select>
                             </div>
                              </div>
                        </div>
                         <div class="col-md-3 inputGroupContainer">
                           <div class="input-group">
                             <select class="form-control" name="clockout_am" id="clockout_am" required>
                                 <option value="">Select</option>
                                <?php for($i=0;$i<60;$i++){ $value = str_pad($i,2,"0",STR_PAD_LEFT);?>
                                 <option value="<?php echo $value;?>" <?php if(date("s", strtotime($student->emp_out)) == $value) { echo "selected";}?>><?php echo $value;?></option>
                                 <?php }?>
                             </select>
                             </div>
                        </div>
                       
                     </div>
                     <br>
                     <br>
                     <div class="form-group has-feedback">
                        <label for="confirmPassword"  class="col-md-3 control-label">
                        Note
                        </label>
                        <div class="col-md-9 inputGroupContainer">
                           <div class="input-group">
                              <input type="text" class="form-control" required name="note" value="{{$student->work_note}}" placeholder="Please Enter Your  Note">
                           </div>
                        </div>
                     </div>
                     <br><br>
                  </div>
               </div>
            </div>
         
            <div class="modal-footer">
               <input type="submit" value="Ok" name="Ok" onClick="edit()" id="addopt2" style="position: relative;left:30px;" class="btn btn-primary pull-left col-md-offset-3">
               <!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
             </div>
            
         </form>
<!--<script>
function edit()
{  
         var date = $('#date').val();
         var clockin = $('#clockin').val();
         var clockin_second = $('#clockin_second').val();
         var clockin_am = $('#clockin_am').val();
         var clockout = $('#clockout').val();
         var clockout_second = $('#clockout_second').val();
         var clockout_am = $('#clockout_am').val();
         var datasring = '&date=' + date + '&clockin=' + clockin + '&clockin_second=' + clockin_second + '&clockin_am=' + clockin_am+ '&clockout=' + clockout + '&clockout_second=' + clockout_second + '&clockout_am=' + clockout_am;
        // alert(datasring);
         if(date=='')
         {
             alert('Please Select Your Date');
         }
         else if(clockin=='')
         {
             alert('Please Select Your Clockin Hour');
         }
         else if(clockin_second=='')
         {
         alert('Please Select Your Clockin MInute');
         }
         else if(clockin_am=='')
         {
         alert('Please Select Your Clockin Second');
         }
         else if(clockout=='')
         {
             alert('Please Select Your Clockout Hour');
         }
         else if(clockout_second=='')
         {
         alert('Please Select Your Clockout Minute');
         }
         else if(clockout_am=='')
         {
         alert('Please Select Your Clockout Second');
         }
         else{
        $.ajax({
        type: "post",
        url: "{{route('fscemployeereport.update',$student->id)}}",
        dataType: "json",
        data: datasring,
        success: function(data){
             $("tbody").load(" tbody > *");
             location.reload();//alert();
        },
        error: function(data){
             alert("Error")
        }
    });
        }
}
</script>-->