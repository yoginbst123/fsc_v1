<?php
$ccountry = "America/New_York";
date_default_timezone_set($ccountry);
$time =date_default_timezone_get();
//echo $commons;
?>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"> </script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.4.1/moment-timezone-with-data-2010-2020.min.js"> </script>
<script type="text/javascript">   
$(function(){
setInterval(function(){
var divUtc = $('#divUTC');
var divLocal = $('#divLocal');  
//put UTC time into divUTC  
divUtc.text(moment.utc().format('YYYY-MM-DD HH:mm:ss a'));      
var localTime  = moment.utc(divUtc.text()).toDate();
//localTime = moment(localTime).format('YYYY-MM-DD HH:mm:ss a');
//divLocal.text(localTime);        
//$('#divThai').text(moment.tz('Asia/Bangkok').format('YYYY-MM-DD HH:mm:ss a'));

$('#divUsa').text(moment.tz('{{$time}}').format('hh:mm:ss a'));
},1000);
});
</script> 

<style>
.select2-container--default .select2-selection--multiple .select2-selection__choice {
    background-color: #1d7dff;
    border: 1px solid #000;
    border-radius: 4px;
    cursor: default;
    float: left;
    margin-right: 5px;
    margin-top: 5px;
    padding: 0 5px;
    font-size: 18px;
}
.top_company.top-company-name-bg .top_date, .top_time, .top_day{ width:100%; }
.main-header .logo .logo-lg{ margin:10px auto; }
.main-sidebar{    padding-top: 114px;}
.top_name {     margin-top: 38px; }
.top_company { float:left; margin:34px 0 0 0; }
.top_company.top-company-name-bg{background: #fff; border-radius: 5px; margin: 14px 0;padding: 0;width:100%;}
.top_company.top-company-name-bg h3{margin: 0;color: #fff;background: #12186b;padding: 10px;border-radius: 5px 5px 0 0px; font-size:18px; 
/*min-width: 230px;*/
text-align: center;}
.top_company.top-company-name-bg p{color: #000;padding-left: 0;padding: 10px 2px;text-align: center;display: block; font-size:17px;}
.top_company.top-company-name-bg span{ color:#000; font-size:13px; }
.top_company.top-company-name-bg i{ color:#000;}
.top_company.top-company-name-bg .top_date, .top_time, .top_day{ padding:3px 0; }

.skin-blue .main-header .navbar .sidebar-toggle {
    top:0; left:0;z-index: 999; 
    border: 1px solid #ffffff;
    padding: 9px 13px;
    margin-left: 7px;
    margin-top: 40px;
}
.topbar{ margin:16px 0 16px 0px;  }
.content-header.page-title h2{margin: 0;
    font-size: 22px!important;
    font-weight: 600!important;
    color: #222!important;}

@media (max-width: 1100px) {
    .top_company.top-company-name-bg h3{ min-width:auto; }
}

@media (max-width: 900px) {
.top_company.top-company-name-bg{ width:100%; }   
}
 
@media (max-width: 767px) {
    .content {  margin-top: 0px !important;}
    .portlet.box.blue{  margin-top: 0px !important; top:30px;}   
    .top_company.top-company-name-bg { margin: 10px auto; float: none !important; display:table; }
    .main-header .navbar-custom-menu{margin-top: 0 !important;left: 0 !important;}
    .skin-blue .main-header .navbar .sidebar-toggle {  margin-top: 20px;position: absolute;   z-index: 999;   font-size: 20px;}
    .main-sidebar .sidebar{ top:0px; }
    .logo .img-responsive{      width: auto !important;}
}

.companycode{float: left;
    background: #26bdaf!important;
    padding: 5px 10px!important;
    border: 1px solid #098478!important;
    color: #fff!important;
    font-size: 20px!important;
    margin-top: -5px!important;}
    .countbox{
        background: none!important;
    border: 2px solid #ffffff!important;
    float: left!important;
    font-weight: bold;
    font-size: 18px;
    padding: 3px 11px;
    }
    .topbar{border-radius:10px; }
    .topbar img{max-width:100%;}
    .select2-container--default .select2-selection--multiple .select2-selection__rendered li {
    list-style: none !important;
}
.select2-container--default .select2-selection--multiple .select2-selection__choice__remove{
    color:#ffffff;
    margin-right:7px;
}
</style>
<header class="main-header">
    <!-- Logo -->
    <a href="{{url('/fac-Bhavesh-0554')}}" class="logo"style="border:solid 5px #003d6a  !important;">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><img style="width:44px" src="{{URL::asset('public/dashboard/images/fsc_logo.png')}}" alt="" class="img-responsive"/></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><img src="{{URL::asset('public/dashboard/images/fsc_logo.png')}}" alt="" class="img-responsive" /></span>
    </a>
<nav class="navbar navbar-static-top">
	
		<div class="">
		    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
			@guest
			@else   
			<div class="col-md-5">
    			<div class="topbar">
    			    <img src="https://financialservicecenter.net/public/images/fsclogo.png"/>
    			</div>
			</div>
			<div class="col-md-3 col-xs-12">
    			    <div class="top_company top-company-name-bg">
            			<h3>Admin</h3>
            			<p>{{Auth::user()->minss}} {{ Auth::user()->fname }} {{ Auth::user()->mname }} {{ Auth::user()->lname }}</p>
            		</div>
		    </div>
			    
			<div class="col-md-2 col-xs-12" style="padding-right:0px;">
				<div class="top_date_section top_company top-company-name-bg pull-right" style="padding: 3px 6px;">
					<div class="top_date"><i class="fa fa-calendar"></i><span>{{ date('F-d-Y')}}</span></div>
					<div class="top_day"><i class="fa fa-calendar-o"></i><span>{{ date('l')}}</span></div>
					<div class="top_time"><i class="fa fa-clock-o"></i><span id="divUsa"></span></div>
				</div>
		    </div>
			    

			
			<div class="navbar-custom-menu ">
				
					<div class="col-md-12">
						<div class="top_name">
							<li class="dropdown user user-menu" style="float: right; list-style: none;"><a href="index.php" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> 
								<span class="fa fa-user" style="font-size: 25px !important; color: #fff" aria-hidden="true"></span>
							</a>
								<ul class="dropdown-menu" style="border: 1px solid #021F4E !important;min-width: 120px; width:120px;padding:0px; right:-12px;position: absolute;margin-left: -100px;">
									<!-- User image -->

									<!-- Menu Body -->

									<!-- Menu Footer-->
									<li class="user-footer" style="border-bottom:1px solid #12186b">
										<a href="{{url('/fac-Bhavesh-0554/adminprofile')}}" style="background:#c1f0ff !important;padding:4px 5px;border:none;" class="btn btn-default btn-flat">Profile</a>
									</li>
									<li ><a href="{{ route('fac-Bhavesh-0554.logout') }}" style="background:#c1f0ff !important;padding:4px 5px;border:none;" class="btn btn-default btn-flat">Sign out</a></li>
								</ul></li>
						</div> 
					</div>
				</div>

			@endguest
		</div>
	</nav>
    <!-- Header Navbar: style can be found in header.less -->
    
  </header> 
