@guest
@else
<script type="text/javascript">
$(document).ready(function(){
	var url = window.location;

// for sidebar menu but not for treeview submenu
$('ul.sidebar-menu a').filter(function() {
    return this.href == url;
}).parent().siblings().removeClass('active').end().addClass('active');

// for treeview which is like a submenu
$('ul.treeview-menu a').filter(function() {
    return this.href == url;
}).parentsUntil(".sidebar-menu > .treeview-menu").siblings().removeClass('active').end().addClass('active');
$('')
});
</script>
<style>

.iconclientmanagement i.fa-clientmaagement, .iconclientmanagement i.fa-workmg, .iconclientmanagement i.fa-emg{padding:12px 0px!important;}
    .iconclientmanagement i.fa-clientmaagement:before{content:'C'!important; font-size:21px; font-weight:bold; font-family:TimesNewRoman !important;color:#103b68;}
    .iconclientmanagement i.fa-workmg:before{content:'W'!important; font-size:19px; font-weight:bold; font-family:TimesNewRoman !important;color:#103b68;}
    .iconclientmanagement i.fa-emg:before{content:'E'!important; font-size:19px; font-weight:bold; font-family:TimesNewRoman !important;color:#103b68;}   
</style>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
    <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li><a href="{{url('/fac-Bhavesh-0554')}}"><i class="fa fa-home sidebar-icon"></i> <span style="font-size:16px !important;">Dashboard</span></a></li>
       <li class="treeview">
				<a href="#"><i class="fa fa-user-circle-o sidebar-icon"></i> <span style="font-size:16px !important;">Admin Profile</span> <span class="pull-right-container">
				<!--<a href="#"><i class="fa fa-user-circle-o sidebar-icon"></i><span style="font-size:16px !important;">Admin Profile</span> <span class="pull-right-container">-->
              <i class="fa fa-angle-left pull-right"></i>
            </span></a>
				<ul class="treeview-menu">
					<li><a href="{{url('/fac-Bhavesh-0554/adminprofile')}}"><i class="fa fa-circle-o"></i> <span>Profile</span></a></li>
								<li><a href="{{url('/fac-Bhavesh-0554/changepassword')}}"><i class="fa fa-circle-o"></i> <span>Login Info</span></a></li>
								<li><a href="{{url('/fac-Bhavesh-0554/adminworkstatus')}}"><i class="fa fa-circle-o"></i> <span>Admin Work Status</span></a></li>
								<li><a href="{{route('cestatus.index')}}"><i class="fa fa-circle-o"></i> <span>CE Status</span></a></li>
									<li><a href="{{route('adminupload.index')}}"><i class="fa fa-circle-o"></i> <span>Upload</span></a></li>
							
								
				</ul>
			</li>
			
        <li class="treeview">
				<a href="#"><i class="fa fa-users sidebar-icon"></i> <span style="font-size:16px !important;">Employee / User</span> <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span></a>
				<ul class="treeview-menu">
					<li  class="{{ Request::path() == 'employment/create' ? 'active' : '' }}"><a href="{{route('employment.index')}}"><i class="fa fa-circle-o"></i> Hiring Ad</a></li>
					<li><a href="{{route('employeapplication.index')}}"><i class="fa fa-circle-o"></i> Application Rec'd</a></li>
					<li><a href="{{route('candidate.index')}}"><i class="fa fa-circle-o"></i> Candidate Data</a></li>
					<li><a href="{{route('employee.index')}}"><i class="fa fa-circle-o"></i> Add / Edit -- EE / User</a></li>
						<li class=""><a href="{{url('/fac-Bhavesh-0554/supervisor')}}"><i class="fa fa-circle-o"></i> Supervisor Code</a></li>
				</ul>
			</li>
			<li class="treeview">
				<a href="#"><i class="fa fa-user sidebar-icon"></i> <span style="font-size:16px !important;">FSC Client</span> <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span></a>
				<ul class="treeview-menu">
					<li><a href="{{url('/fac-Bhavesh-0554/prospect')}}"><i class="fa fa-circle-o"></i> Add / Edit FSC Prospect</a></li>
					
					<li><a href="{{url('/fac-Bhavesh-0554/customerconvert')}}"><i class="fa fa-circle-o"></i> Client Registration</a></li>
					<li><a href="{{url('/fac-Bhavesh-0554/customer')}}"><i class="fa fa-circle-o"></i>Add / Edit FSC Client</a></li>
				</ul>
			</li>
			<li class="treeview iconclientmanagement">
				<a href="#"><i style="background: linear-gradient(to bottom, #b3dced 0%,#29b8e5 50%,#bce0ee 100%) !important;" class="fa fa-clientmaagement sidebar-icon"></i> <span><span style="font-weight:bold;color:#103b68;font-size: 18px;">C</span>lient Management</span> <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span></a>	
				<ul class="treeview-menu">
				
					<li class=""><a href="{{route('clientsetup.index')}}"><i class="fa fa-chevron-right"></i> <span>Edit / View -- Client </span></a></li>
					<!--<li class=""><a href="{{url('/fac-Bhavesh-0554/employees')}}"><i class="fa fa-chevron-right"></i> View / Edit -- Employee</a></li>-->
				</ul>
			</li>
			<li class="treeview iconclientmanagement">
				<a href="#"><i style="background: linear-gradient(to bottom, #b3dced 0%,#29b8e5 50%,#bce0ee 100%) !important;" class="fa fa-workmg sidebar-icon"></i> <span><span style="font-weight:bold;color:#103b68;font-size: 18px;">W</span>ork Management</span> 
				<span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span></a>	
			
						<ul class="treeview-menu">
						    <li><a href="{{url('/fac-Bhavesh-0554/mywork')}}"><i class="fa fa-circle-o"></i> <span>Today's Work </span></a></li>
						    <li><a href="{{url('/fac-Bhavesh-0554/worknew')}}"><i class="fa fa-chevron-right"></i> <span>Work -- New </span></a></li>
						    <!--<li><a href="{{url('/fac-Bhavesh-0554/mywork')}}"><i class="fa fa-chevron-right"></i> <span>My Work </span></a></li>-->
						    
						    <li><a href="{{url('/fac-Bhavesh-0554/worktax')}}"><i class="fa fa-chevron-right"></i> <span>Work -- Regular </span></a></li>
						    
						    <li><a href="{{url('/fac-Bhavesh-0554/worktodo')}}"><i class="fa fa-chevron-right"></i> Work -- To Do</a></li>
						    <li><a href="{{url('/fac-Bhavesh-0554/workrecord')}}"><i class="fa fa-chevron-right"></i> <span>Work -- Record</span></a></li>
						   	<li><a href="{{url('/fac-Bhavesh-0554/workstatus')}}"><i class="fa fa-chevron-right"></i> <span>Work -- Status</span></a></li>
							
							
							<li><a href="{{url('/fac-Bhavesh-0554/submissionrequest')}}"><i class="fa fa-chevron-right"></i> <span>Work -- Submission</span> </a>
						
							</li>
							<li class=""><a href="{{route('clientupload.index')}}"><i class="fa fa-chevron-right"></i> <span> Work -- Upload</span></a></li>
							<li><a href="{{route('servicesprocess.index')}}"><i class="fa fa-circle-o"></i><span style="font-size:16px !important;"> Edit / View Service</span></a></li> 
						    <li><a href="{{url('/fac-Bhavesh-0554/task')}}"><i class="fa fa-circle-o"></i> Task</a></li>
				    
                            <li class="treeview">
                                <a href="#"><i class="fa fa-circle-o" aria-hidden="true"></i> <span>Message</span> <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                                </span></a>
                                <ul class="treeview-menu">
                                    <li><a href="{{url('/fac-Bhavesh-0554/msg/')}}"><i class="fa fa-chevron-right"></i> <span>Message  In</span></a></li>
                                    <li><a href="{{url('/fac-Bhavesh-0554/msgout')}}"><i class="fa fa-chevron-right"></i> <span>Message Out</span></a></li>
                                    <!--<li><a href="{{url('/fac-Bhavesh-0554/msg/create')}}"><i class="fa fa-circle-o"></i> New Message</a></li>
                                    <li><a href="{{url('/fac-Bhavesh-0554/msg/')}}"><i class="fa fa-circle-o"></i>  Edit / View Message</a></li>-->
                                </ul>
                            </li>
                            <li><a href="{{route('appointment.create')}}"><i class="fa fa-circle-o"></i> Appointment</a></li>
                            <!--<li class="treeview">
                                <a href="#"><i class="fa fa-circle-o"></i> <span>Calendar</span> <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                                </span></a>
                                <ul class="treeview-menu">
                                    <li><a href="{{url('/fac-Bhavesh-0554/appointment')}}"><i class="fa fa-chevron-right"></i> Appointment</a></li>	
                                    <li><a href="{{url('/fac-Bhavesh-0554/calender')}}"><i class="fa  fa-chevron-right"></i> Calendar</a></li>	
                                </ul>
                            </li>-->
					</ul>
			</li>
			<li class="treeview iconclientmanagement">
				<a href="#"><i style="background: linear-gradient(to bottom, #b3dced 0%,#29b8e5 50%,#bce0ee 100%) !important;" class="fa fa-emg sidebar-icon"></i> <span><span style="font-weight:bold;color:#103b68;font-size: 18px;">E</span>mployee  Management</span> <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span></a>	
				<ul class="treeview-menu">
				     <li class="treeview">
								<a href="#"><i class="fa fa-chevron-right"></i> <span>FSC Employee</span> <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span></a>
								<ul class="treeview-menu">
								 <li class="treeview">
								<a href="#"><i class="fa fa-chevron-right"></i> <span>FSC EE Schedule</span> <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span></a>
								<ul class="treeview-menu">
									<li><a href="{{route('schedule.create')}}"><i class="fa fa-angle-right"></i> Add Schedule</a></li>
									<li><a href="{{url('/fac-Bhavesh-0554/schedule')}}"><i class="fa fa-angle-right"></i> Edit / View Schedule </a></li>
								</ul>
							</li>
						  
							<li>
								<a href="{{url('/fac-Bhavesh-0554/empleave')}}"><i class="fa fa-chevron-right"></i> <span>FSC EE Leave App.</span> <span class="pull-right-container">
              
            </span></a>
								<!--<ul class="treeview-menu">-->
								<!--	<li><a href="{{url('/fac-Bhavesh-0554/empleave')}}"><i class="fa fa-angle-right"></i> Edit / View FSC EE Leave</a></li>-->
								<!--</ul>-->
							</li>
							<li><a href="{{route('rules.index')}}"><i class="fa fa-chevron-right"></i>FSC EE Rules / Resp.</a></li>
								</ul>
							</li>
				     
				    <li><a href="{{route('clientemployee.index')}}"><i class="fa fa-circle-o"></i> Client Employee</a></li>
				  
					
				</ul>
			</li>
				<li class="treeview">
				<a href="#"><i class="fa fa-book"></i>  <span style="font-size:16px !important;"> Report </span> <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span></a>	
				<ul class="treeview-menu">
				
					<li><a href="{{url('/fac-Bhavesh-0554/fscclientreport')}}"><i class="fa fa-chevron-right"></i> Client Report</a></li>
			
								<li class=""><a href="{{url('/fac-Bhavesh-0554/fscworkreport')}}"><i class="fa fa-circle-o"></i> Work Report</a></li>
								
									<li class="treeview"><a href="#"><i class="fa fa-circle-o"></i>  Employee Report <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span></a>
					<ul class="treeview-menu">
					    	<li class="treeview"><a href="#"><i class="fa fa-circle-o"></i> <span> FSC EE Report </span> <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span></a>
					<ul class="treeview-menu">
							<li><a href="{{url('/fac-Bhavesh-0554/fscemployeereport')}}"><i class="fa fa-chevron-right"></i> FSC EE Timesheet</a></li>
							<li><a href="{{url('/fac-Bhavesh-0554/ee-schedule')}}"><i class="fa fa-chevron-right"></i> FSC EE  Schedule</a></li>
							<li><a href="{{url('/fac-Bhavesh-0554/fsceeworkreport')}}"><i class="fa fa-chevron-right"></i> FSC EE / User  Work</a></li>
							
						</ul>
					</li>
						<li class="treeview"><a href="#"><i class="fa fa-circle-o"></i> Client EE Report <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span></a>
					<ul class="treeview-menu">
							<li><a href="{{url('/fac-Bhavesh-0554/clientreport')}}"><i class="fa fa-chevron-right"></i> Client EE Timesheet</a></li>
							<li><a href="{{url('/fac-Bhavesh-0554/clientschedules')}}"><i class="fa fa-chevron-right"></i> Client EE  Schedule</a></li>
						</ul>
				</li>
				
							
						</ul>
				</li>
								
				</ul>
			</li>
			</li>
				
			<!--<li><a href="#"><i class="fa fa-file-text sidebar-icon"></i><span>Report </span></a></li>-->
			<li class="treeview">
				<a href="#"><i class="fa fa-cog sidebar-icon"></i><span style="font-size:16px !important;">Setup</span> <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span></a>
			
				<ul class="treeview-menu">
				    	<li class="treeview">
						<a href="#"><i class="fa fa-circle-o" aria-hidden="true"></i> <span>Web Page</span> <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span></a>
						<ul class="treeview-menu">
						    <li><a href="#"><i class="fa fa-chevron-right"></i> Login Screen</a></li>
						    <li><a href="{{route('contact.index')}}"><i class="fa fa-chevron-right"></i> Contact Us</a></li>
						    <li><a href="{{url('/fac-Bhavesh-0554/slider')}}"><i class="fa fa-chevron-right"></i> Home Page Screen</a></li>
						    <li><a href="{{route('forms.index')}}"><i class="fa fa-chevron-right"></i> Form</a></li>
						    <li><a href="{{url('/fac-Bhavesh-0554/homecontent')}}"><i class="fa fa-chevron-right"></i> Home Content</a></li>
						    <li><a href="{{url('/fac-Bhavesh-0554/marque')}}"><i class="fa fa-chevron-right"></i> Marque</a></li>
							
								<li class="treeview">
								<a href="#"><i class="fa fa-chevron-right"></i> <span>Submission</span> 
								<span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span></a>
								<ul class="treeview-menu">
									<li><a href="{{url('/fac-Bhavesh-0554/submissionreq')}}"><i class="fa fa-angle-right"></i> Add Submission</a></li>
								
								</ul>
							</li>
						</ul>
					</li>
				<li class="treeview">
						<a href="#"><i class="fa fa-circle-o"></i> Office Setup  <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span></a>
						<ul class="treeview-menu">
						    		<li class="treeview">
						<a href="#"><i class="fa fa-circle-o" aria-hidden="true"></i> <span>Stationery</span> <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span></a>
						<ul class="treeview-menu">
						    	<li><a href="{{route('logo.index')}}"><i class="fa fa-chevron-right"></i> <span>Company Logo</span></a></li>
							
							<li><a href="{{route('businesscard.index')}}"><i class="fa fa-chevron-right"></i> Business Card</a></li>
											<li><a href="{{route('letterhead.index')}}"><i class="fa fa-chevron-right"></i> Letterhead</a></li>
											<!--<li><a href="#"><i class="fa  fa-chevron-right"></i> Share Certificate</a></li>-->
											 <li><a href="{{url('/fac-Bhavesh-0554/invoice')}}"><i class="fa fa-chevron-right"></i> Invoice</a></li>	
						   
						</ul>
					</li>
					<li><a href="{{url('/fac-Bhavesh-0554/email')}}"><i class="fa fa-chevron-right"></i>Email / Telephone Setup</a></li>
						    				<li><a class="sub" href="{{url('/fac-Bhavesh-0554/language')}}"><i class="fa fa-chevron-right"></i> Language</a></li>
				<li><a href="{{url('/fac-Bhavesh-0554/ethnic')}}"><i class="fa fa-chevron-right"></i> Ethnic</a></li>
				
						    <li><a href="{{url('/fac-Bhavesh-0554/holiday')}}"><i class="fa fa-chevron-right"></i> Holiday</a></li>
						
							
							<li><a href="{{url('/fac-Bhavesh-0554/emailsetup')}}"><i class="fa fa-angle-right"></i><span>Email Message Setup </span></a></li>
							<li><a style="padding-left: 15px !important;" href="{{route('howtodo.index')}}"><i class="fa fa-chevron-right"></i><span>How To Do</span></a></li>
								<!--<li class="treeview"><a href="#"><i class="fa fa-chevron-right" aria-hidden="true"></i> <span>How To Do</span>
							 <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span></a>
						
							    <ul class="treeview-menu" style="display: none;">
							<li><a style="padding-left: 15px !important;" href="{{route('howtodo.create')}}"><i class="fa fa-angle-right"></i> <span>New How To Do</span></a></li>
						
						</ul>
							</li>-->
							
							<li><a href="{{url('/fac-Bhavesh-0554/setup')}}"><i class="fa fa-chevron-right"></i> Price Setup</a></li>
							<li><a href="{{url('/fac-Bhavesh-0554/setup/appointment')}}"><i class="fa fa-chevron-right"></i> Appointment Setup</a></li>
						
						</ul>
						
						<ul class="treeview-menu">
							
							
						</ul>
						
					</li>
				<li class="treeview">
						<a href="#"><i class="fa fa-circle-o"></i> Business Setup  <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span></a>
						<ul class="treeview-menu">
							<li><a href="{{url('/fac-Bhavesh-0554/business')}}"><i class="fa fa-chevron-right"></i> Business</a></li>
							<li><a href="{{url('/fac-Bhavesh-0554/businesscategory')}}"><i class="fa fa-chevron-right"></i> Business Category</a></li>
							<li><a href="{{url('/fac-Bhavesh-0554/business-brand')}}"><i class="fa fa-chevron-right"></i> Business Brand</a></li>
							<li><a href="{{url('/fac-Bhavesh-0554/business-brand-category')}}"><i class="fa fa-chevron-right"></i> Business Brand Category</a></li>
						</ul>
					</li>
				
				<li><a href="{{url('/fac-Bhavesh-0554/license')}}"><i class="fa fa-chevron-right"></i> License Setup</a></li>	
					<li class="treeview">
								<a href="#"><i class="fa fa-circle-o"></i> Type Of Authority  <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span></a>
								<ul class="treeview-menu">
                                    <li><a href="#"><i class="fa fa-chevron-right"></i> City / Local</a></li>
								    <li><a href="{{url('/fac-Bhavesh-0554/taxstate')}}"><i class="fa fa-chevron-right"></i> County </a></li>
									<li><a href="{{url('/fac-Bhavesh-0554/taxation')}}"><i class="fa fa-chevron-right"></i> State</a></li>
								    <li><a href="{{url('/fac-Bhavesh-0554/taxfederal')}}"><i class="fa fa-chevron-right"></i> Federal</a></li>
								</ul>
							</li>
				
				<li  class="treeview"><a ><i class="fa fa-circle-o"></i> Bank / Vendor Setup <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span></a>
                                <ul class="treeview-menu">
                                    <li><a href="{{url('/fac-Bhavesh-0554/bankingsetup')}}"><i class="fa fa-chevron-right"></i> Bank </a></li>
                                    	<li><a href="{{url('/fac-Bhavesh-0554/vendor')}}"><i class="fa fa-chevron-right"></i> Vendor</a></li>
                            </ul>
            </li>	
				
				<li><a href="{{url('/fac-Bhavesh-0554/statetax')}}"><i class="fa fa-chevron-right"></i> State Tax</a></li>	
					
					
						
					<!--	<li><a href="{{url('/fac-Bhavesh-0554/question')}}"><i class="fa fa-chevron-right"></i> Interview</a></li>
					
						
							<li><a href="{{url('/fac-Bhavesh-0554/formationsetup')}}"><i class="fa fa-chevron-right"></i> Formation</a></li>	!-->
							
								<!--<li><a href="{{url('/fac-Bhavesh-0554/taxation')}}"><i class="fa fa-chevron-right"></i>License
</a></li>!-->
						<li class="treeview">
						<a href="#"><i class="fa fa-circle-o"></i> Link Setup <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span></a>
						<ul class="treeview-menu">
							<li><a href="{{route('link.create')}}"><i class="fa fa-chevron-right"></i> Add New Link</a></li>
							<li><a href="{{route('link.index')}}"><i class="fa fa-chevron-right"></i> Edit / View Link</a></li>
							<li><a href="{{route('linkcategory.index')}}"><i class="fa fa-chevron-right"></i> Edit / view Category</a></li>
						</ul>
					</li>
					<li class="treeview">
						<a href="#"><i class="fa fa-circle-o" aria-hidden="true"></i> <span> Other Setup </span> <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span></a>
						<ul class="treeview-menu">
							<li><a href="{{url('/fac-Bhavesh-0554/otherpage')}}"><i class="fa fa-chevron-right" aria-hidden="true"></i> <span>Position Setup</a></li>
							
								<li><a href="{{url('/fac-Bhavesh-0554/accountcode')}}"><i class="fa fa-chevron-right"></i> Account Code Setup</a></li>
					
						
						</ul>
					</li>
					
						<li><a href="{{url('/fac-Bhavesh-0554/schedulesetup')}}"><i class="fa fa-circle-o"></i> Schedule Formula Setup</a></li>
						
					<li><a href="{{route('services.index')}}"><i class="fa fa-circle-o"></i> Service</a></li>
					<li><a href="{{url('/fac-Bhavesh-0554/services/createmaster')}}"><i class="fa fa-circle-o"></i>Prospect Service</a></li>
					<li><a href="{{route('servicetype.index')}}"><i class="fa fa-circle-o"></i> Service Type</a></li>
					
				
				
						
								    
							
					
				<!--	<li class="treeview">
						<a href="#"><i class="fa fa-circle-o" aria-hidden="true"></i> <span>Zip Code</span> 
						<span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span></a>
						<ul class="treeview-menu">
							<li class=""><a href="{{route('citystate.index')}}"><i class="fa  fa-chevron-right"></i> <span> Edit / View State City</span></a></li>
						</ul>
					</li>
					
					<li class="treeview">
						<a href="#"><i class="fa fa-circle-o" aria-hidden="true"></i> <span>Category</span> 
						<span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span></a>
						<ul class="treeview-menu">
							<li class=""><a href="{{route('categorys.index')}}"><i class="fa  fa-chevron-right"></i> <span> Category </span></a></li>
							<li class=""><a href="{{route('subcategorys.index')}}"><i class="fa  fa-chevron-right"></i> <span> Sub Category </span></a></li>
						</ul>
					
					</li>
					
					<li class="treeview">
						<a href="#"><i class="fa fa-circle-o" aria-hidden="true"></i> <span>Question Type</span> 
						<span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span></a>
						<ul class="treeview-menu">
							<li class=""><a href="{{route('questionsection.index')}}"><i class="fa  fa-chevron-right"></i> <span> List Of Question Type </span></a></li>
						</ul>
					</li>!-->
				
				</ul>
			</li>
			<li class="treeview">
				<a href="#"><i class="fa fa-cogs sidebar-icon"></i><span style="font-size:16px !important;">Inquiry / Support</span>  <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span></a>	
				<ul class="treeview-menu">
				    <li class="treeview">
                        <a href="#"><i class="fa fa-circle-o"></i><span > Inquiry </span> <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i></span></a>
                        <ul class="treeview-menu" style="display: none;">
                            <li><a href="{{route('applyservice.index')}}"><i class="fa fa-circle-o"></i> Service Inquiry</a></li>
        					<li><a href="{{route('contactinquery.index')}}"><i class="fa fa-circle-o"></i> Contact Inquiry</a></li>
        					<li><a href="{{route('linkinquery.index')}}"><i class="fa fa-circle-o"></i> Link Inquiry</a></li>
                        </ul>
					</li>
				    <li class="treeview">
                        <a href="#"><i class="fa fa-circle-o"></i><span > Support </span> <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i></span></a>
                        <ul class="treeview-menu" style="display: none;">
                            <li ><a href="{{url('/fac-Bhavesh-0554/clienttechnical')}}"><i class="fa fa-circle-o"></i> <span>Client Support</span></a></li>
                            <li><a href="{{url('/fac-Bhavesh-0554/emptechnical')}}"><i class="fa fa-circle-o"></i> <span>Employee Support</span></a></li>
                            <li ><a href="#"><i class="fa fa-circle-o"></i> <span>Other Support</span></a></li>
                        </ul>
					</li>
					<li><a href="{{url('/fac-Bhavesh-0554/complaint')}}"><i class="fa fa-circle-o"></i> Complaint</a></li> 
					<li><a href="{{url('/fac-Bhavesh-0554/reviews')}}"><i class="fa fa-circle-o"></i> Review</a></li> 
					
				</ul>
			</li>
				<li class="treeview">
				<a href="#"><i class="fa fa-cogs sidebar-icon"></i><span style="font-size:16px !important;">Other</span>  <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span></a>	
				<ul class="treeview-menu">
					<li class=""><a href="{{url('/fac-Bhavesh-0554/addressbook')}}"><i class="fa fa-chevron-right"></i> <span>Address Book</span></a></li>
				
				</ul>
			</li>
		
			<li><a href="{{ route('fac-Bhavesh-0554.logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="fa fa-sign-out sidebar-icon"></i><span style="font-size:16px !important;">Logout</span></a></li>
			
			<form id="logout-form" action="{{ route('fac-Bhavesh-0554.logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
			
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

@endguest

