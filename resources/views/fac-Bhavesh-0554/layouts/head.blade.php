<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>FSC - Admin</title>
<link rel="shortcut icon" href="{{URL::asset('public/dashboard/images/favicon.png')}}" type="image/x-icon">
<link rel="stylesheet" href="{{URL::asset('public/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{URL::asset('public/adminlte/bower_components/font-awesome/css/font-awesome.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{URL::asset('public/adminlte/bower_components/Ionicons/css/ionicons.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{URL::asset('public/adminlte/dist/css/AdminLTE.min.css')}}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{URL::asset('public/adminlte/dist/css/skins/_all-skins.min.css')}}">
  <!-- Morris chart -->
  <link rel="stylesheet" href="{{URL::asset('public/adminlte/bower_components/morris.js/morris.css')}}">
  <!-- jvectormap -->
  <link rel="stylesheet" href="{{URL::asset('public/adminlte/bower_components/jvectormap/jquery-jvectormap.css')}}">
  <!-- Date Picker -->
  <link rel="stylesheet" href="{{URL::asset('public/adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{URL::asset('public/adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="{{URL::asset('public/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">
   <link rel="stylesheet" href="{{URL::asset('public/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
   <link rel="stylesheet" href="https://cdn.datatables.net/rowreorder/1.2.7/css/rowReorder.dataTables.min.css">
   
  <link rel="stylesheet" href="{{URL::asset('public/adminlte/style.css')}}">
<!-- jQuery UI 1.11.4 -->
 <script type="text/javascript"  src="{{URL::asset('public/adminlte/bower_components/jquery/dist/jquery.min.js')}}"></script>

<script src="{{URL::asset('public/adminlte/bower_components/jquery-ui/jquery-ui.min.js')}}"></script>
 

<!-- Bootstrap 3.3.7 -->

<script src="{{URL::asset('public/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

<script src="{{URL::asset('public/adminlte/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
<!-- datepicker -->
<script src="{{URL::asset('public/adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{URL::asset('public/adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<!-- Bootstrap WYSIHTML5 -->

<!-- AdminLTE App -->
<script src="{{URL::asset('public/adminlte/dist/js/adminlte.min.js')}}"></script>

<script src="{{URL::asset('public/adminlte/bower_components/ckeditor/ckeditor.js')}}"></script>
<script  type="text/javascript" src="{{URL::asset('public/dashboard/js/passwordst.js')}}"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.3/jquery.timepicker.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.3/jquery.timepicker.min.js"></script>

<!-- start Datatable Plugin -->

<script src="{{URL::asset('public/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>

<script src="https://cdn.datatables.net/plug-ins/1.10.22/pagination/input.js"></script>
<script src="https://cdn.datatables.net/rowreorder/1.2.7/js/dataTables.rowReorder.min.js"></script>
<script src="{{URL::asset('public/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('public/dashboard/js/dataTables.buttons.min.js')}}"></script>

<!-- End Datatable plugin -->


<script type="text/javascript" src="{{URL::asset('public/dashboard/js/buttons.flash.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('public/dashboard/js/jszip.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('public/dashboard/js/pdfmake.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('public/dashboard/js/vfs_fonts.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('public/dashboard/js/buttons.html5.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('public/dashboard/js/buttons.print.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('public/frontcss/js/bootstrap-formhelpers.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('public/dashboard/js/fullcalendar.min.js')}}"></script>
<script type='text/javascript' src="{{URL::asset('public/dashboard/js/jquery.maskedinput.js')}}"></script>
<script src="{{URL::asset('public/dashboard/js/jquery.maskedinput.min.js')}}"></script>

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/jstree.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

<script>
  $(function () {
    CKEDITOR.replace('editor1')
    $('#editor1').wysihtml5()
  })
  $(function () {
    CKEDITOR.replace('editor2')
    $('#editor2').wysihtml5()
  })
</script>
<style>.box-header > .box-tools {
    position: inherit;
    right: 10px;
    top: 5px;
}</style>

<style>
.buttons-copy,.buttons-csv{display:none;}
.fixed .main-header, .fixed .main-sidebar, .fixed .left-side {
     overflow-y: inherit; 
}
label.file-upload {
    position: relative;
    overflow: hidden;
    float: left;
 }
    input[type="file"] {
        position: absolute;
        top: 0;
        right: 0;
        min-width: 100%;
        min-height: 100%;
        font-size: 100px;
        text-align: right;
        filter: alpha(opacity=0);
        opacity: 0;
        outline: none;
        background: white;
        cursor: inherit;
        display: block;
    }

.nav-tabs>li>a {
    margin-right: 0px;
    color: #000 !important;
    font-size: 16px !important;
}


</style>
<!--<script type="text/javascript">
   $(document).ready(function(){
   	$('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
   		localStorage.setItem('activeTab', $(e.target).attr('href'));
   	});
   	var activeTab = localStorage.getItem('activeTab');
   	if(activeTab){
   		$('#myTab a[href="' + activeTab + '"]').tab('show');
   	}
   });
</script>-->


<style>
.card .card-footer {border-top: 1px solid #ddd;padding: 20px 0 0 0; margin-top: 10px;width: 100%;float: left;}
.card { display: inline-block;width: 100%;}
</style>
 @guest
@else
<?php 
$remdays = Auth::user()->end_date;
$no1 = date('Y-m-d', strtotime('-3 days', strtotime($remdays)));
$no2 = date('Y-m-d', strtotime('-2 days', strtotime($remdays)));
$no3 = date('Y-m-d', strtotime('-1 days', strtotime($remdays)));
$no4 = date('Y-m-d', strtotime('-0 days', strtotime($remdays)));
$beforethreeday = strtotime($no1);
$enddate = strtotime($remdays);
$currentday = date('Y-m-d');
if($currentday==$no1 || $currentday==$no2 || $currentday==$no3) 
{
?>

<script>
 $(window).on('load',function(){
        $('#myModal').modal('show');
    });
</script>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Change Password</h4>
      </div>
      <div class="modal-body">
        <p><?php if($currentday==$no1){?>Your password is expire on {{date('M-d-Y', strtotime($remdays))}}, please change it<?php }?><?php if($currentday==$no2){?>Your password is expire on {{date('M-d-Y', strtotime($remdays))}}, please change it<?php }?><?php if($currentday==$no3){?>Your password is expire on {{date('M-d-Y', strtotime($remdays))}}, please change it<?php }?></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<?php 
}
elseif($currentday == $no4)
{
?>
<style>
.modal-content {width: 700px;}
</style>
<script type="text/javascript">

$(document).ready(function(){
$("#myModal").modal({
		backdrop:'static',
		keyword:'false',
		show: true,
	});
	});	
</script>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Change Password</h4>
      </div>
      <div class="modal-body">
 <form enctype='multipart/form-data' class="form-horizontal changepassword" 
action="{{route('changepassword.update', Auth::user()->id)}}" id="changepassword"  method="post">
                  {{csrf_field()}}  {{method_field('PATCH')}}
                  <div class="row">
                     <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group {{ $errors->has('oldpassword') ? ' has-error' : '' }}">
                           <label class="control-label col-md-3">Old Password :</label>
                           <div class="col-md-9">
                              <input type="password" class="form-control" id="oldpassword" name="oldpassword"> <input type="hidden" class="form-control" id="flag" value="@if(empty(Auth::user()->flag)) 1 @else {{Auth::user()->flag+1}} @endif" name="flag">	
                              @if ($errors->has('oldpassword'))
                              <span class="help-block">
                              <strong>{{ $errors->first('oldpassword') }}</strong>
                              </span>
                              @endif
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-md-3">New Password :</label>
                           <div class="col-md-6">
                              <input name="newpassword" type="password" id="newpassword" class="form-control" />
                              <input name="email" type="hidden" id="email" value="vijay@businesssolutionteam.com" class="form-control" />
                              <div id="messages"></div>
  <div class="pwstrength_viewport_progress"></div>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-md-3">Confirm Password :</label>
                           <div class="col-md-6">
                              <input name="cpassword" type="password" id="cpassword" class="form-control"/>
                           </div>
                        </div>
                        <div class="form-group  {{ $errors->has('resetdays') ? ' has-error' : '' }}">
                           <label class="control-label col-md-3">Reset Days :</label>
                           <div class="col-md-2">
                              <select name="resetdays" id="resetdays" class="form-control">
                                 <option>---Select Reset Days---</option>
                                 <option value="30" @if(Auth::user()->reset_day=='30') selected @endif>30</option>
                                 <option value="60" @if(Auth::user()->reset_day=='60') selected @endif>60</option>
                                   <option value="90" @if(Auth::user()->reset_day=='90') selected @endif>90</option>
                                 <option value="120" @if(Auth::user()->reset_day=='120') selected @endif>120</option>
                              </select>
                              @if ($errors->has('resetdays'))
                              <span class="help-block">
                              <strong>{{ $errors->first('resetdays') }}</strong>
                              </span>
                              @endif
                           </div>
                             <div class="col-md-2">  <input readonly name="reset_date" type="text" id="reset_date" value="{{date('M-d-Y', strtotime(Auth::user()->end_date))}}" class="form-control"/></div>
                        </div>
                     </div>
                  </div>
                  <div class="card-footer">
                     <div class="row">
                       <center> <div class="col-md-6">
                           <button type="submit" class="btn btn-primary fsc-form-submit" style="">Save</button>
&nbsp&nbsp&nbsp&nbsp<a class="btn btn-primary icon-btn" href="{{url('fac-Bhavesh-0554/')}}">Cancel</a> 
                        </div></center>
                     </div>
                  </div>
                  <br>
               </form>
      </div>
      
    </div>

  </div>
</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js"></script>
<script>
   $.ajaxSetup({
       headers:
       {
           'X-CSRF-Token': $('input[name="_token"]').val()
       }
   });
   $(document).ready(function() {
   
   $('.changepassword').bootstrapValidator({        
           feedbackIcons: {
               valid: 'glyphicon glyphicon-ok',
               invalid: 'glyphicon glyphicon-remove',
               validating: 'glyphicon glyphicon-refresh'
           },
   		fields: {
   			newpassword: {
           	validators: {
               notEmpty: {
                   message: 'The password is required and cannot be empty'
               },
               regexp:
            	{
            
           	regexp: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8}/,
   
           	message: 'The password should contain Minimum 8 and Maximum 12 characters at least 1 Uppercase Alphabet, 1 Lowercase Alphabet, 1 Number and 1 Special Character:'
   	   		},
   	 
   				different: {
   					field: 'oldpassword',
   					message: 'The password cannot be the same as Current Password'
   				}
           	}
       	},
    			oldpassword: {
                   validators: {
                       notEmpty: {
                           message: 'Please Enter Your Current Password'
   					},	
                       remote: {
                           message: 'The Password is not available',
                           url: '{{ URL::to('admincheck') }}',
                           data: {
                               type: 'oldpassword'
                           },
                           type: 'POST'
                       }																		
                   }
               },
               cpassword: {
                   validators: {  
   					notEmpty: {  
   						message: 'The confirm password is required and can\'t be empty'  
   					},  
   					identical: {  
   						field: 'newpassword',  
   						message: 'The password and its confirm are not the same'  
   					},  
   					different: {  
   						field: 'oldpassword',  
   						message: 'The password can\'t be the same as Old Password'  
   					}  
   				}  
               }
               }
           }).on('success.form.bv', function(e) {
               $('.changepassword').slideDown({ opacity: "show" }, "slow") // Do something ...
                   $('.changepassword').data('bootstrapValidator').resetForm();
               // Prevent form submission
               e.preventDefault();
               // Get the form instance
               var $form = $(e.target);
   
               // Get the BootstrapValidator instance
               var bv = $form.data('bootstrapValidator');
   
               // Use Ajax to submit form data
               $.post($form.attr('action'), $form.serialize(), function(result) {
                  // console.log(result);
               }, 'json');
           });
   });
</script>
<script>
    $(document).ready(function(){
        $('#resetdays').on('change',function(){
           var reset = parseInt($('#resetdays').val()); 
           var date = new Date();
         var t = new Date(); 
		//var n = $("#resetdays").val(); 
		//alert(offset);
		t.setDate(t.getDate() + reset);
		var month = "0"+(t.getMonth()+1);
		var date = "0"+t.getDate();
		
		const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "June","July", "Aug", "Sept", "Oct", "Nov", "Dec"];
		const d = new Date();
		
		month = month.slice(-2);
		date = date.slice(-2);
		//var date = date +"/"+month+"/"+t.getFullYear();
		var date = monthNames[t.getMonth()]+"-"+date+"-"+t.getFullYear();
           $('#reset_date').val(date);
        });
    });
</script>
<?php 
$remdays = Auth::user()->end_date;
$no1 = date('Y-m-d', strtotime('-3 days', strtotime($remdays)));
$no2 = date('Y-m-d', strtotime('-2 days', strtotime($remdays)));
$no3 = date('Y-m-d', strtotime('-1 days', strtotime($remdays)));
$no4 = date('Y-m-d', strtotime('-0 days', strtotime($remdays)));
$beforethreeday = strtotime($no1);
$enddate = strtotime($remdays);
$currentday = date('Y-m-d');
if($currentday==$no1 || $currentday==$no2 || $currentday==$no3) 
{
?>
<script>
 $(window).on('load',function(){
        $('#myModal').modal('show');
    });
</script>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Change Password</h4>
      </div>
      <div class="modal-body">
        <p><?php if($currentday==$no1){?>Your password is expire on {{date('M-d-Y', strtotime($remdays))}}, please change it<?php }?><?php if($currentday==$no2){?>Your password is expire on {{date('M-d-Y', strtotime($remdays))}}, please change it<?php }?><?php if($currentday==$no3){?>Your password is expire on {{date('M-d-Y', strtotime($remdays))}}, please change it<?php }?></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<?php 
}
elseif($currentday == $no4)
{
?>
<style>
.modal-content {width: 700px;}
</style>

<script type="text/javascript">
$(document).ready(function(){
$("#myModal").modal({
		backdrop:'static',
		keyword:'false',
		show: true,
	});
	});	
</script>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Change Password</h4>
      </div>
      <div class="modal-body">
	   <form enctype='multipart/form-data' class="form-horizontal changepassword" action="{{route('changepassword.update', Auth::user()->id)}}" id="changepassword"  method="post">
                  {{csrf_field()}}  {{method_field('PATCH')}}
                  <div class="row">
                     <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group {{ $errors->has('oldpassword') ? ' has-error' : '' }}">
                           <label class="control-label col-md-3">Old Password :</label>
                           <div class="col-md-6">
                              <input type="password" class="form-control" id="oldpassword" name="oldpassword"> <input type="hidden" class="form-control" id="flag" value="@if(empty(Auth::user()->flag)) 1 @else {{Auth::user()->flag+1}} @endif" name="flag">	
                              @if ($errors->has('oldpassword'))
                              <span class="help-block">
                              <strong>{{ $errors->first('oldpassword') }}</strong>
                              </span>
                              @endif
                           </div>
                        </div>
                         <input name="email" type="hidden" id="email" value="vijay@businesssolutionteam.com" class="form-control" />
                        <div class="form-group">
                           <label class="control-label col-md-3">New Password :</label>
                           <div class="col-md-6">
                              <input name="newpassword" type="password" id="newpassword" class="form-control" />
                              <div id="messages"></div>
  <div class="pwstrength_viewport_progress"></div>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-md-3">Confirm Password :</label>
                           <div class="col-md-6">
                              <input name="cpassword" type="password" id="cpassword" class="form-control"/>
                           </div>
                        </div>
                        <div class="form-group  {{ $errors->has('resetdays') ? ' has-error' : '' }}">
                           <label class="control-label col-md-3">Reset Days :</label>
                           <div class="col-md-2">
                              <select name="resetdays" id="resetdays" class="form-control">
                                 <option>---Select Reset Days---</option>
                                 <option value="30" @if(Auth::user()->reset_day=='30') selected @endif>30</option>
                                 <option value="60" @if(Auth::user()->reset_day=='60') selected @endif>60</option>
                                   <option value="90" @if(Auth::user()->reset_day=='90') selected @endif>90</option>
                                 <option value="120" @if(Auth::user()->reset_day=='120') selected @endif>120</option>
                              </select>
                              @if ($errors->has('resetdays'))
                              <span class="help-block">
                              <strong>{{ $errors->first('resetdays') }}</strong>
                              </span>
                              @endif
                           </div>
                             <div class="col-md-2">  <input readonly name="reset_date" type="text" id="reset_date" value="{{date('M-d-Y', strtotime(Auth::user()->end_date))}}" class="form-control"/></div>
                        </div>
                     </div>
                  </div>
                  <div class="card-footer">
                     <div class="row">
                       <center> <div class="col-md-9">
                           <button type="submit" class="btn btn-primary fsc-form-submit" style="">Save</button>
&nbsp&nbsp&nbsp&nbsp<a class="btn btn-primary icon-btn" href="{{url('fac-Bhavesh-0554/')}}">Cancel</a> 
                        </div></center>
                     </div>
                  </div>
                  <br>
               </form>
      </div>
      
    </div>

  </div>
</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js"></script>
<script>
   $.ajaxSetup({
       headers:
       {
           'X-CSRF-Token': $('input[name="_token"]').val()
       }
   });
   $(document).ready(function() {
   
   $('.changepassword').bootstrapValidator({        
           feedbackIcons: {
               valid: 'glyphicon glyphicon-ok',
               invalid: 'glyphicon glyphicon-remove',
               validating: 'glyphicon glyphicon-refresh'
           },
   		fields: {
   			newpassword: {
           	validators: {
               notEmpty: {
                   message: 'The password is required and cannot be empty'
               },
               regexp:
            	{
            
           	regexp: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8}/,
   
           	message: 'The password should contain Minimum 8 and Maximum 12 characters at least 1 Uppercase Alphabet, 1 Lowercase Alphabet, 1 Number and 1 Special Character:'
   	   		},
   	 
   				different: {
   					field: 'oldpassword',
   					message: 'The password cannot be the same as Current Password'
   				}
           	}
       	},
    			oldpassword: {
                   validators: {
                       notEmpty: {
                           message: 'Please Enter Your Current Password'
   					},	
                       remote: {
                           message: 'The Password is not available',
                           url: '{{ URL::to('admincheck') }}',
                           data: {
                               type: 'oldpassword'
                           },
                           type: 'POST'
                       }																		
                   }
               },
               cpassword: {
                   validators: {  
   					notEmpty: {  
   						message: 'The confirm password is required and can\'t be empty'  
   					},  
   					identical: {  
   						field: 'newpassword',  
   						message: 'The password and its confirm are not the same'  
   					},  
   					different: {  
   						field: 'oldpassword',  
   						message: 'The password can\'t be the same as Old Password'  
   					}  
   				}  
               }
               }
}).on('success.form.bv', function(e) {
               $('.changepassword').slideDown({ opacity: "show" }, "slow") // Do something ...
                   $('.changepassword').data('bootstrapValidator').resetForm();
               // Prevent form submission
               e.preventDefault();
               // Get the form instance
               var $form = $(e.target);
   
               // Get the BootstrapValidator instance
               var bv = $form.data('bootstrapValidator');
   
               // Use Ajax to submit form data
               $.post($form.attr('action'), $form.serialize(), function(result) {
                  // console.log(result);
               }, 'json');
           });
   });
</script>
<script>
    $(document).ready(function(){
        $('#resetdays').on('change',function(){
           var reset = parseInt($('#resetdays').val()); 
           var date = new Date();
         var t = new Date(); 
		//var n = $("#resetdays").val(); 
		//alert(offset);
		t.setDate(t.getDate() + reset);
		var month = "0"+(t.getMonth()+1);
		var date = "0"+t.getDate();
		
		const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "June","July", "Aug", "Sept", "Oct", "Nov", "Dec"];
		const d = new Date();
		
		month = month.slice(-2);
		date = date.slice(-2);
		//var date = date +"/"+month+"/"+t.getFullYear();
		var date = monthNames[t.getMonth()]+"-"+date+"-"+t.getFullYear();
           $('#reset_date').val(date);
        });
    });
</script>
<?php 
}
?>
<?php 
}
?>
@endguest