@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
<div class="content-wrapper">
    <section class="content-header page-title" style="height:50px;">
     		<div >
     		    <div  style="text-align:center;">
     		        <h2>Tax Authorities </h2>
     		    </div>
     		    
     		</div>
    </section>
  
   <div class="row">
      <!--<div class="branch">Federal</div>-->
      <div class="col-md-12">
                <table align="center"
      border="0"
      cellpadding="10"
      cellspacing="0"
      style="
        border: 1px solid #ccc; background:#fff;
        width: 100%;
        max-width: 800px;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 13px;
        line-height: 18px;
      "
    >
      <tr>
          <td style="padding:15px;">
               <table align="center"
      border="0"
      cellpadding="10"
      cellspacing="0"
      style="
        border:0px solid #ccc; background:#fff;
        width: 100%;
        max-width: 800px;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 13px;
        line-height: 18px;
      "
    >
      <tr>
        <td style="text-align: left; width:100px;">
          <img src="https://financialservicecenter.net/public/dashboard/images/fsc_logo.png" style="width: 100px;" />
        </td>
        <td style="text-align: center;">
          <h3 style="font-size: 30px;">Invoice</h3>
        </td>
        <td style="width: 100px; text-align: right;">Page : 1</td>
      </tr>
      <tr>
        <td colspan="3" style="text-align: left; padding-top:10px;">
          <table border="0" style="width: 100%;">
            <tr>
              <td>
                <p style="margin: 0px 0px 5px; padding: 0px; font-size: 13px; text-align:left;">
                  4550 Jimmy Carter Blvd.<br />
                  Norcross, GA 30093<br />
                  USA
                </p>
                <p style="margin: 0px 0px 5px; padding: 0px; font-size: 13px;text-align:left;">
                  <strong>Voice:</strong> 770-270-5597 <br />
                  <strong>Fax:</strong> 770-414-5351
                </p>
                <p style="margin: 0px; padding: 0px; font-size: 13px; text-align:left;">
                  <strong>Bill To:</strong><br />
                  Kush Jay LLC <br />
                  6742 Bells Ferry Road <br />
                  Woodstock, GA 30189 <br />
                  U.S.A.
                </p>
              </td>
              <td style="text-align: right; width: 180px; vertical-align: top;">
                <p
                  style="text-align:right;
                    margin: 0px;
                    padding: 0px;
                    font-size: 13px;
                    line-height: 20px;
                  "
                >
                  <strong>Invoice No.:</strong> M-20-1320 <br />
                  <strong>Invoice Date:</strong> 8/3/2020 <br />
                </p>
              </td>
            </tr>
          </table>
        </td>
      </tr>

      <tr>
        <td colspan="3" style="text-align:left;"><strong>Customer ID:</strong> GA-403-1</td>
      </tr>
      <tr>
        <td colspan="3">
          <table
            style="width: 100%; border: 1px solid #ccc;"
            cellpadding="3"
            cellspacing="0"
          >
            <tr style="background: #f4f4f4;">
              <td style="border-bottom: 1px solid #ccc; text-align:left;">Customer PO</td>
              <td style="border-bottom: 1px solid #ccc; text-align:left;">Payment Terms</td>
              <td style="border-bottom: 1px solid #ccc; text-align:left;">Sales Rep ID</td>
              <td style="border-bottom: 1px solid #ccc; width: 150px;">
                Due Date
              </td>
            </tr>
            <tr>
              <td
                style="
                  border-bottom: 1px solid #ccc;
                  border-right: 1px solid #ccc;
                "
              >
                &nbsp;
              </td>
              <td
                style="
                  border-bottom: 1px solid #ccc;
                  border-right: 1px solid #ccc;
                "
              >
                &nbsp;
              </td>
              <td
                style="
                  border-bottom: 1px solid #ccc;
                  border-right: 1px solid #ccc;
                "
              >
                &nbsp;
              </td>
              <td style="border-bottom: 1px solid #ccc;">&nbsp;</td>
            </tr>
          </table>
          <table
            style="width: 100%; border: 1px solid #ccc; margin-top: 15px;"
            cellpadding="3"
            cellspacing="0"
          >
            <tr style="background: #f4f4f4;">
              <td style="border-bottom: 1px solid #ccc;text-align:left;">Description</td>
              <td
                style="
                  border-bottom: 1px solid #ccc;
                  width: 150px;
                  text-align: center;
                "
              >
                Amount
              </td>
            </tr>
            <tr>
              <td
                style="
                  border-bottom: 1px solid #ccc;
                  border-right: 1px solid #ccc;
                  height: 250px;
                  font-size: 12px; text-align:left;
                  vertical-align: top;
                "
              >
                SERVICE FEES FOR FORMATION OF LLC<br />
                INCLUDES CORPORATE KIT FOR LLC
              </td>
              <td
                style="
                  border-bottom: 1px solid #ccc;
                  border-right: 0px solid #ccc;
                  width: 150px;
                "
              >
                &nbsp;
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="3" style="vertical-align: top;">
          <table
            style="width: 100%; border: 1px solid #ccc; margin-top: 0px;"
            cellpadding="2"
            cellspacing="0"
          >
            <tr>
              <td></td>
              <td style="text-align: right;"><strong>Sub Total</strong></td>
              <td style="width: 150px; text-align: right;">550.00</td>
            </tr>
            <tr>
              <td></td>
              <td style="text-align: right;"><strong>Sales Tax</strong></td>
              <td style="width: 150px; text-align: right;"></td>
            </tr>
            <tr>
              <td></td>
              <td style="text-align: right;">
                <strong>Total Invoice Amount</strong>
              </td>
              <td style="width: 150px; text-align: right;">550.00</td>
            </tr>
            <tr>
              <td style="text-align: right;"><strong>Check No</strong></td>
              <td style="text-align: right;">
                <strong>Payment Received</strong>
              </td>
              <td style="width: 150px; text-align: right;">0.00</td>
            </tr>
            <tr>
              <td style="text-align: right;">&nbsp;</td>
              <td style="text-align: right; width: 300px;">
                <strong>Total</strong>
              </td>
              <td style="width: 150px; text-align: right;">550.00</td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="3" style="padding-top: 15px; text-align:left;">
          <strong>Notes:</strong>
        </td>
      </tr>
      <tr>
        <td colspan="3">
          <p style="margin: 0px; padding: 0px; font-size: 13px; text-align:left;">
            *The amount of this bill will be automatically deducted from your
            bank account on Aug 10, 2020
          </p>
          <p style="margin: 0px; padding: 0px; font-size: 13px; text-align:left;">
            *Please call the number on the bill at least a week prior to this
            date if you have made any bank account changes or have any questions
            about this bill.
          </p>
          <p style="margin: 0px; padding: 0px; font-size: 13px;text-align:left;">
            *Return ACH charge will be $ 30.00
          </p>
          <p style="margin: 0px; padding: 0px; font-size: 13px;text-align:left;">
            Late Charges will be assess for late payment after due date-- 18 %
          </p>
        </td>
      </tr>
    </table>
              
          </td>
    </tr>
    </table>
       
      </div>
   </div>
<!--</div>-->
@endsection()