@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>Add New Email / Telephone Extension</h1>
    </section>
    <!-- Main content -->
    <section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-success">
			      <div class="box-header">
              <div class="box-tools pull-right">
              </div>
            </div>
				<div class="col-md-12">			
					<form method="post" action="{{route('email.store')}}" class="form-horizontal" id="create_email_access" name="content" enctype="multipart/form-data">
				{{csrf_field()}}
						<div class="form-group {{ $errors->has('for_whom') ? ' has-error' : '' }}">
							<label class="control-label col-md-3">For Whom <span style="color:red">*</span> :</label>
							<div class="col-md-5">
							<select name="for_whom" id="for_whom" class="form-control fsc-input">
                              <option value="">---Select---</option>
                              @foreach($emp1 as $e)
                                    <option value="{{$e->id}}" 
                                    @foreach($email as $e1) 
                                        @if($e->id == $e1->for_whom) 
                                            style="display:none" 
                                        @endif  
                                    @endforeach>{{ucwords($e->firstName.' '.$e->middleName.' '.$e->lastName)}} (@if($e->type=='employee') EE @endif @if($e->type=='clientemployee') Client EE @endif @if($e->type=='user') User @endif)</option>
                               @endforeach
                               <option value="Other">Other</option>
                           </select>
								@if ($errors->has('for_whom'))
										<span class="help-block">
											<strong>{{ $errors->first('for_whom') }}</strong>
										</span>
									@endif							
							</div>
						</div>
						
<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
							<label class="control-label col-md-3">Email  Address <span style="color:red">*</span> :</label>
							<div class="col-md-5">
								<input name="email" type="text" id="email" class="form-control" value="" onchange="myFunction()" />	
								@if ($errors->has('email'))
										<span class="help-block">
											<strong>{{ $errors->first('email') }}</strong>
										</span>
									@endif							
							</div>
						</div>						
						<div class="form-group {{ $errors->has('access_address') ? ' has-error' : '' }}">
							<label class="control-label col-md-3">Access Address <span style="color:red">*</span> :</label>
							<div class="col-md-5">
							<div class="">
								  <input name="access_address" type="text" id="access_address" class="form-control">
						  </div>
								@if ($errors->has('access_address'))
										<span class="help-block">
											<strong>{{ $errors->first('access_address') }}</strong>
										</span>
									@endif	
							</div>
						</div>
						<div class="form-group">
						    <label class="control-label col-md-3">Telephone : </label>
						    <div class="col-md-5">
								<input name="telephone" type="text" id="telephone" class="form-control" value="" />	
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Ext. :</label>
							<div class="col-md-5">
							<div class="">
								  <input name="ext" type="text" id="ext" class="form-control">
						        </div>
							</div>
						</div>
						<div class="form-group showhidelocation">
							<label class="control-label col-md-3">Location :</label>
							<div class="col-md-5">
							<div class="">
								  <input name="location" type="text" id="location" class="form-control">
						        </div>
							</div>
						</div>
<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
							<label class="control-label col-md-3">Password <span style="color:red">*</span> :</label>
							<div class="col-md-5">
							<div class="">
								  <input name="password" type="password" id="myInput" class="form-control" autocomplete="new-password">  
								  
						  </div>
								@if ($errors->has('password'))
										<span class="help-block">
											<strong>{{ $errors->first('password') }}</strong>
										</span>
									@endif	
							</div>
							<div class="col-md-3" style="margin-top: 10px;">
							    <input type="checkbox" id="ans1" onclick="myFunctionone1()"> <label class="fsc-form-label" for="ans1">Show Password </label>
						    </div>
						</div>
  	<div class="card-footer">
						    <div class="form-group">
							<label class="control-label col-md-3"></label>
							<div class="col-md-2">
<input class="btn_new_save btn-primary1" type="submit" name="submit" value="Save">
							</div>
							<div class="col-md-2 row">
<a class="btn_new_cancel" href="{{url('fac-Bhavesh-0554/email')}}">Cancel</a> 
							</div>
						</div>
						  </div>
						
					</form>
				</div>
			</div>
		</div>
	</div>
	  </section>
<!--</div>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>

<script>

function myFunctionone1() {
       var x = document.getElementById("myInput");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }}
</script>
<script>
$(document).ready(function(){
    $("#telephone").mask("(999) 999-9999");
    $("#ext").mask("9999");
    $('#for_whom').on('change',function(){
	    var selectedval = $('#for_whom').find(":selected").val();
	    if(selectedval == "Other"){
	        $('.showhidelocation').hide();
	    }else{
	        $('.showhidelocation').show();
	    }
	});
	$(document).on('change','.category', function()
	{
		var id = $(this).val();//alert(id);
		$.get('{!!URL::to('/gemails')!!}?id='+id, function(data)
		{  $('#email').empty(); //alert();
           $.each(data, function(index, subcatobj)
		   {
			   $('#email').val(subcatobj.email);
		   })
		});
		
	});
	
	    $("#create_email_access").validate({
           rules: {
               email: {
                   required: true,
                   email:true
               },
               for_whom: {
                   required: true,
               },
               password: {
                   required: true,
               },
               access_address: {
                   required: true,
               }
           },
           messages: {
               email: {
                   required: "Please Select Priority",
                   email: "Please enter correct email format ex. aa@aa.com"
               },
               for_whom: {
                   required: "Please Select Types",
               },
               password: {
                   required: "Please Enter Client Name Or Search Client Name",
               },
               access_address: {
                   required: "Please Enter Access Address",
               }
           },
           highlight: function(e) {
               $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
               $(e).closest('.form-tab').removeClass('has-info').addClass('has-error');
           },
       
           success: function(e) {
               $(e).closest('.form-group').removeClass('has-error');
               $(e).closest('.form-tab').removeClass('has-error'); //.addClass('has-info');
               $(e).remove();
           },
           submitHandler: function(form) {
               form.submit();
               
           }
       });
	
	
});
</script>
@endsection()