@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>Edit -- Email / Extension</h1>
    </section>
    <!-- Main content -->
    <section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-success">
			      <div class="box-header">
              <div class="box-tools pull-right">
              </div>
            </div>
            <?php 
            //print_r($email);exit;?>
				<div class="col-md-12">			
					<form method="post" action="{{route('email.update',$email->ids)}}" class="form-horizontal" id="contact" name="content" enctype="multipart/form-data">
					{{csrf_field()}}{{method_field('PATCH')}}
						<div class="form-group{{ $errors->has('for_whom') ? ' has-error' : '' }}">
							<label class="control-label col-md-3">For Whom :</label>
							<div class="col-md-5">
								<select name="for_whom" id="for_whom" class="form-control fsc-input">
                                    <option value="">---Select---</option>
                                    @foreach($emp as $e)
                                        <option value="{{$e->id}}" @if($e->id == $email->for_whom) selected @endif>{{ucwords($e->firstName.' '.$e->middleName.' '.$e->lastName)}} (@if($e->type=='employee') EE @endif @if($e->type=='clientemployee') Client EE @endif @if($e->type=='user') User @endif)</option>
                                    @endforeach
                                    <option value="Other" @if($email->for_whom == 'Other') selected @endif>Others</option>
                                </select>
								@if ($errors->has('for_whom'))
										<span class="help-block">
											<strong>{{ $errors->first('for_whom') }}</strong>
										</span>
									@endif							
							</div>
						</div>
						
<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
							<label class="control-label col-md-3">Email  Address :</label>
							<div class="col-md-5">
								<input name="email" type="text" id="email" class="form-control" value="{{$email->email}}" />	
								@if ($errors->has('email'))
										<span class="help-block">
											<strong>{{ $errors->first('email') }}</strong>
										</span>
									@endif							
							</div>
						</div>						
						<div class="form-group{{ $errors->has('access_address') ? ' has-error' : '' }}">
							<label class="control-label col-md-3">Access Address :</label>
							<div class="col-md-5">
							<div class="">
								  <input name="access_address" type="text" id="access_address" class="form-control" value="{{$email->access_address}}">
						  </div>
								@if ($errors->has('access_address'))
										<span class="help-block">
											<strong>{{ $errors->first('access_address') }}</strong>
										</span>
									@endif	
							</div>
						</div>
						<div class="form-group">
						    <label class="control-label col-md-3">Telephone : </label>
						    <div class="col-md-5">
								<input name="telephone" type="text" id="telephone" class="form-control" value="{{$email->telephone}}" />	
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Ext. :</label>
							<div class="col-md-5">
							<div class="">
								  <input name="ext" type="text" id="ext"  value="{{$email->ext}}" class="form-control">
						        </div>
							</div>
						</div>
						<div class="form-group showhidelocation">
							<label class="control-label col-md-3">Location :</label>
							<div class="col-md-5">
							<div class="">
								  <input name="location" type="text" id="location" value="{{$email->location}}" class="form-control">
						        </div>
							</div>
						</div>
						
<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
							<label class="control-label col-md-3">Password :</label>
							<div class="col-md-5">
							<div class="">
							    <input name="password" type="password" id="myInput" class="form-control" value="{{$email->password}}">
						  </div>
								@if ($errors->has('password'))
										<span class="help-block">
											<strong>{{ $errors->first('password') }}</strong>
										</span>
									@endif	
							</div>
							<div class="col-md-3" style="margin-top: 10px;">
							    <input type="checkbox" id="ans1" onclick="myFunctionone1()"> <label class="fsc-form-label" for="ans1">Show Password </label>
						    </div>
						</div>
  	<div class="card-footer">
						    <div class="form-group">
							<label class="control-label col-md-3"></label>
							<div class="col-md-2">
<input class="btn_new_save btn-primary1" type="submit" name="submit" value="Save">
							</div>
							<div class="col-md-2 row">
<a class="btn_new_cancel" href="{{url('fac-Bhavesh-0554/email')}}">Cancel</a> 
							</div>
						</div>
						  </div>
						
					</form>
				</div>
			</div>
		</div>
	</div>
	  </section>
<!--</div>-->
<script>
        $("#telephone").mask("(999) 999-9999");
        $("#ext").mask("9999");
        var selectedval = $('#for_whom').find(":selected").val();
        if(selectedval == "Other"){
            $('.showhidelocation').hide();
        }else{
            $('.showhidelocation').show();
        }
        $('#for_whom').on('change',function(){
    	    var selectedval = $('#for_whom').find(":selected").val();
    	    if(selectedval == "Other"){
    	        $('.showhidelocation').hide();
    	    }else{
    	        $('.showhidelocation').show();
    	    }
    	});
</script>
<script>
function myFunctionone1() {
       var x = document.getElementById("myInput");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }}
</script>

@endsection()