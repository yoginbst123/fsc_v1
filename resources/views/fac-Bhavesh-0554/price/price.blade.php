@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
<style>
label{float:left;}
.box-tools{
        position:absolute !important;
        margin-top: 7px !important;
        margin-right: 150px !important;
    }
    .page-title {
    display: -ms-flexbox;
    -ms-flex-align: center;
    align-items: center;
    -ms-flex-pack: justify;
    justify-content: space-between;
    -ms-flex-direction: row;
    flex-direction: row;
    padding: 8px 18px !important;
    box-shadow: 0 1px 2px rgba(0,0,0,.1);
    background-color: #D6EBFA !important;
    text-align: center;
}
.buttons-pdf {
    font-size: 0px !important;
    border-radius: 3px;
    padding: 8px 10px !important;
    background: #fff !important;
    border-color: #c6c6c6  !important;
    color:red !important;
}
.buttons-print {
    font-size: 0px !important;
    border-radius: 3px;
    padding: 8px 10px !important;
    background: #3c8dbc !important;
            border-color: #367fa9 !important;
}
.buttons-excel{
    font-size:0px !important;
    border-radius: 3px;
    padding: 8px 10px !important;
    background: #00a65a !important;
    border-color: #008d4c !important;
}
.buttons-excel:hover{
     background: #008d4c !important;
}
.buttons-pdf:hover{
     background: #f6f6f6  !important;
}
.buttons-print:hover{
     background: #367fa9 !important;
}
.fa{
    font-size: 16px !important;
}
.content-header.page-title h2 {
    margin: 0;
    font-size: 22px!important;
    font-weight: 600!important;
    color: #222!important;
}
@media only screen and (max-width: 991px){
    .table-title a {
        margin-top: 0px !important; 
        margin-right: -10px !important;
    }
}
@media only screen and (max-width: 860px){
    .box-header>.box-tools{
        position: relative !important;
        margin-right: 5px !important;
        margin-top: 0px !important;
    }
}
@media only screen and (max-width: 490px){
    div.dataTables_wrapper div.dataTables_filter{
        width: 100%;
        display: flex;
    }
    div.dataTables_wrapper div.dataTables_filter label{
        width: 84%;
    }
    .table-title a {
        margin-top: -34px !important;
        margin-right: 10px !important;
    }
    .box-header>.box-tools {
        position: absolute !important;
        margin-top: 51px !important;
        margin-right: 120px !important;
    }
    .box-header {
        padding: 10px !important;
    }
}
</style>
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
    <section class="page-title content-header" style="height:50px !important;">
     		<h1 class="col-sm-12"><div class="col-sm-7" style="text-align:right;">@if(empty($_REQUEST['online'])) List of Service @else Online Access @endif Fees </div><div class="col-sm-5" style="text-align:right;">View / Edit</div></h1>
    </section>
    <!-- Main content -->
    <section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-success">
			      <div class="box-header" style="padding:0px;">
                  <div class="box-tools pull-right" style="position: absolute;margin-right: 132px;margin-top:7px;z-index:9999;">
                    <div class="table-title">
						<a href="{{route('price.create')}}?online={{$_REQUEST['online']}}">Add New</a>
					</div>
              </div>
            </div>
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table table-hover table-bordered" id="sampleTable345" >
							<thead>
								<tr>
									<th width="4%">No</th>
									<th>Currency</th>
									<th>Type of Service</th>
                 					<th>Service Period</th>
                 					<th width="40%">Note</th>
									<th width="6%">Action</th>
								</tr>
							</thead>
							<tbody>
                                @foreach($price as $bus)
                                
								<tr>
									<td class="text-center">{{$loop->index + 1}}</td>
									<td class="text-center">{{$bus->currency.' '.$bus->sign}}</td>
									<td>{{$bus->typeofservice}}</td>	
                                    <td>@if($bus->typeofservice =='Payroll Service') {{$bus->servicename}} @else {{$bus->period}} @endif</td>
                                    <td>{{$bus->serviceincludes}}</td>
                                    <td class="text-center">
										<a class="btn-action btn-view-edit" href="{{route('price.edit', $bus->id)}}?online={{$_REQUEST['online']}}"><i class="fa fa-edit"></i></a>
                                        <form action="{{ route('price.destroy',$bus->id) }}" method="post" style="display:none" id="delete-id-{{$bus->id}}">
                                        {{csrf_field()}} {{method_field('DELETE')}}
                                        </form>
                                    </td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	</section>	
<!--</div>-->
@endsection()