@extends('fac-Bhavesh-0554.layouts.app')

@section('main-content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>Question</h1>
    </section>
    <!-- Main content -->
    <section class="content">
	<div class="row">
		<div class="col-md-12">
				<div class="box box-success">
			      <div class="box-header">
             
              <div class="box-tools pull-right">
                
              </div>
            </div>
				<div class="col-md-12">
             <form method="post" action="{{route('question.store')}}" class="form-horizontal" id="positionname" name="positionname" enctype="multipart/form-data">
					{{csrf_field()}}
                            <div class="form-group {{ $errors->has('type') ? ' has-error' : '' }}">
    							<label class="control-label col-md-3">Question Type :</label>
    							<div class="col-lg-5 col-md-8">
                                            <select name="type" id="type" class="form-control">
                                                <option>-Select-</option> 
                                                @foreach($questionsection as $ques)
                                                <option alue="{{$ques->id}}">{{$ques->question_type}}</option>
                                                @endforeach
                                               
                                            </select>

								        @if ($errors->has('type'))
										<span class="help-block">
											<strong>{{ $errors->first('type') }}</strong>
										</span>
									@endif
							    </div>
						    </div>							
                            <div class="form-group {{ $errors->has('question') ? ' has-error' : '' }}">
							<label class="control-label col-md-3">Question Name :</label>
							<div class="col-lg-5 col-md-8">
								<input name="question" type="text" id="question" class="form-control" value="" />                                                            @if ($errors->has('question'))
										<span class="help-block">
											<strong>{{ $errors->first('question') }}</strong>
										</span>
									@endif
							</div>
						</div>					
						<div class="card-footer">
						    <div class="row">
						    <div class="col-md-3"></div>
							<div class="col-xs-2" style="width:155px;">
								<input class="btn_new_save btn-primary1" type="submit" name="submit" value="Save">
							</div>
							<div class="col-xs-2" style="width:155px;">
								<a class="btn_new_cancel" href="{{url('fac-Bhavesh-0554/question')}}">Cancel</a> 
							</div>
							</div>
						</div>
						
					</form>
				</div>
			</div>
		</div>
	</div>
</section>	
<!--</div>-->
@endsection()