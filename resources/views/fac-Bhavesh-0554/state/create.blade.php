@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
<div class="content-wrapper">
   <div class="page-title">
      <h1>Tax Authorities</h1>
   </div>
   <div class="row">
      <div class="col-md-12">
         <div class="card">
            <div class="card-body"><div class="Branch">
                                       <h1>State</h1>
                                    </div><br>
               <form method="post" action="{{route('state.store')}}" class="form-horizontal" id="businessname" name="businessname" enctype="multipart/form-data">
                  {{csrf_field()}}
                  <div class="form-group {{ $errors->has('authority_name') ? ' has-error' : '' }}">
                     <label class="control-label col-md-3">Name Of the Tax Authority :</label>
                     <div class="col-md-4">
                        <input type="text"  name="authority_name" id="authority_name" class="form-control"  placeholder="Name Of the Tax Authority" /> 
                        @if ($errors->has('authority_name'))
                        <span class="help-block">
                        <strong>{{ $errors->first('authority_name') }}</strong>
                        </span>
                        @endif
                     </div>
                  </div>
                  <div class="form-group {{ $errors->has('short_name') ? ' has-error' : '' }}">
                     <label class="control-label col-md-3">Short Name :</label>
                     <div class="col-md-4">
                        <input type="text" name="short_name" id=short_name" class="form-control"  placeholder="Short Name" /> 
                        @if ($errors->has('short_name'))
                        <span class="help-block">
                        <strong>{{ $errors->first('short_name') }}</strong>
                        </span>
                        @endif
                     </div>
                  </div>
                  <div class="form-group {{ $errors->has('type_of_tax') ? ' has-error' : '' }}">
                     <label class="control-label col-md-3">Type Of Tax :</label>
                     <div class="col-md-4">
                        <input type="text" name="type_of_tax" id="type_of_tax" class="form-control "  placeholder="Type Of Tax" /> 
                        @if ($errors->has('type_of_tax'))
                        <span class="help-block">
                        <strong>{{ $errors->first('type_of_tax') }}</strong>
                        </span>
                        @endif
                     </div>
                  </div>
	          <div class="form-group {{ $errors->has('telephone') ? ' has-error' : '' }}">
                     <label class="control-label col-md-3">Telehpone :</label>
                     <div class="col-md-4">
                        <input type="text" name="telephone" id="telephone" class="form-control num"  placeholder="Telephone" /> 
                        @if ($errors->has('telephone'))
                        <span class="help-block">
                        <strong>{{ $errors->first('telephone') }}</strong>
                        </span>
                        @endif
                     </div>
                  </div>
	          <div class="form-group {{ $errors->has('address') ? ' has-error' : '' }}">
                     <label class="control-label col-md-3">Address :</label>
                     <div class="col-md-4">
                        <input type="text" name="address" id="address" class="form-control "  placeholder="Address" /> 
                        @if ($errors->has('address'))
                        <span class="help-block">
                        <strong>{{ $errors->first('address') }}</strong>
                        </span>
                        @endif
                     </div>
                  </div>
	          <div class="form-group {{ $errors->has('city') ? ' has-error' : '' }}">
                     <label class="control-label col-md-3">City / State / Zip :</label>
                     <div class="col-md-2">
                        <input type="text" name="city" id="city" class="form-control"  placeholder="city" /> 
                        @if ($errors->has('city'))
                        <span class="help-block">
                        <strong>{{ $errors->first('city') }}</strong>
                        </span>
                        @endif
                     </div>
<div class="col-md-2"><select name="state" id="state" class="form-control fsc-input">
                                             <option value="">State</option>
                                             <option value="AK">AK</option>
                                             <option value="AS">AS</option>
                                             <option value="AZ">AZ</option>
                                             <option value="AR">AR</option>
                                             <option value="CA">CA</option>
                                             <option value="CO">CO</option>
                                             <option value="CT">CT</option>
                                             <option value="DE">DE</option>
                                             <option value="DC">DC</option>
                                             <option value="FM">FM</option>
                                             <option value="FL">FL</option>
                                             <option value="GA">GA</option>
                                             <option value="GU">GU</option>
                                             <option value="HI">HI</option>
                                             <option value="ID">ID</option>
                                             <option value="IL">IL</option>
                                             <option value="IN">IN</option>
                                             <option value="IA">IA</option>
                                             <option value="KS">KS</option>
                                             <option value="KY">KY</option>
                                             <option value="LA">LA</option>
                                             <option value="ME">ME</option>
                                             <option value="MH">MH</option>
                                             <option value="MD">MD</option>
                                             <option value="MA">MA</option>
                                             <option value="MI">MI</option>
                                             <option value="MN">MN</option>
                                             <option value="MS">MS</option>
                                             <option value="MO">MO</option>
                                             <option value="MT">MT</option>
                                             <option value="NE">NE</option>
                                             <option value="NV">NV</option>
                                             <option value="NH">NH</option>
                                             <option value="NJ">NJ</option>
                                             <option value="NM">NM</option>
                                             <option value="NY">NY</option>
                                             <option value="NC">NC</option>
                                             <option value="ND">ND</option>
                                             <option value="MP">MP</option>
                                             <option value="OH">OH</option>
                                             <option value="OK">OK</option>
                                             <option value="OR">OR</option>
                                             <option value="PW">PW</option>
                                             <option value="PA">PA</option>
                                             <option value="PR">PR</option>
                                             <option value="RI">RI</option>
                                             <option value="SC">SC</option>
                                             <option value="SD">SD</option>
                                             <option value="TN">TN</option>
                                             <option value="TX">TX</option>
                                             <option value="UT">UT</option>
                                             <option value="VT">VT</option>
                                             <option value="VI">VI</option>
                                             <option value="VA">VA</option>
                                             <option value="WA">WA</option>
                                             <option value="WV">WV</option>
                                             <option value="WI">WI</option>
                                             <option value="WY">WY</option>
                                          </select>
                        @if ($errors->has('state'))
                        <span class="help-block">
                        <strong>{{ $errors->first('') }}</strong>
                        </span>
                        @endif
                     </div>
<div class="col-md-2">
                        <input type="text" name="zip" id="" class="form-control"  placeholder="zip" /> 
                        @if ($errors->has('zip'))
                        <span class="help-block">
                        <strong>{{ $errors->first('zip') }}</strong>
                        </span>
                        @endif
                     </div>
                  </div>



<div class="form-group {{ $errors->has('website') ? ' has-error' : '' }}">
                     <label class="control-label col-md-3">Website :</label>
                     <div class="col-md-4">
                        <input type="text" name="website" id="website" class="form-control "  placeholder="website" /> 
                        @if ($errors->has('website'))
                        <span class="help-block">
                        <strong>{{ $errors->first('website') }}</strong>
                        </span>
                        @endif
                     </div>
                  </div>


<div class="form-group {{ $errors->has('website_link_name') ? ' has-error' : '' }}">
                     <label class="control-label col-md-3">Website Link Name :</label>
                     <div class="col-md-4">
                        <input type="text" name="website_link_name" id="website_link_name" class="form-control "  placeholder="website Link Name" /> 
                        @if ($errors->has('website_link_name'))
                        <span class="help-block">
                        <strong>{{ $errors->first('website_link_name') }}</strong>
                        </span>
                        @endif
                     </div>
                  </div>
<div class="form-group {{ $errors->has('website_link') ? ' has-error' : '' }}">
                     <label class="control-label col-md-3">Website Link :</label>
                     <div class="col-md-4">
                        <input type="text" name="website_link" id="website_link" class="form-control "  placeholder="website Link" /> 
                        @if ($errors->has('website_link'))
                        <span class="help-block">
                        <strong>{{ $errors->first('website_link') }}</strong>
                        </span>
                        @endif
                     </div>
                  </div>
                  <div class="card-footer">
                     <div class="row">
                        <div class="col-md-8 col-md-offset-3">
                           <input class="btn btn-primary icon-btn" type="submit" name="submit" value="save">
                        </div>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>


@endsection()

