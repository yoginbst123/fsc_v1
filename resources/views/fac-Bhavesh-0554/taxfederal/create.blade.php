@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
<style>
    .professional_btn {width: 10%;}
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>Federal Tax Authorities / Form</h1>
    </section>
    <!-- Main content -->
    <section class="content">
   <div class="row">
      <div class="col-md-12">
         <div class="box box-success">
                 
			      <div class="box-header">
             
              <div class="box-tools pull-right">
                
              </div>
            </div>
				<div class="col-md-12">
               <form method="post" action="{{route('taxfederal.store')}}" id="form_valid" class="form-horizontal" enctype="multipart/form-data">
                  {{csrf_field()}}
                  <div class="form-group">
                     <label class="control-label col-md-3">Type of Tax :</label>
                     <div class="col-md-4">
                                <select type="text" class="form-control fsc-input" name="typeofform" id="typeofform">
                                    <option value="">---Select---</option>
                                    <option value="Income Tax">Income Tax</option>
                                    <option value="Payroll Tax">Payroll Tax</option>
                            
                                </select>
                       
                     </div>
                  </div>
                    <!-- <div class="form-group {{ $errors->has('short_name') ? ' has-error' : '' }}">
                     <label class="control-label col-md-3">Type :</label>
                     <div class="col-md-4">
                         	<input type="hidden" name="type" id="rend" class="form-control" value="{{rand(55555,99999)}}"  placeholder="" />
                         <select type="text" class="form-control fsc-input" name="short_name" id="short_name" placeholder="Enter Your Company Name" >
                                    <option value="">---Select---</option>
                                   	<option value="Entity">Entity</option>
									<option value="Payroll">Payroll</option>
									<option value="Sales">Sales</option>
									<option value="Tobacco">Tobacco</option>
									<option value="COAM">COAM</option>
									<option value="License">License</option>
									<option value="Income">Income</option>
									<option value="Excise">Excise</option>
									<option value="GST">GST</option>
									<option value="VAT">VAT</option>
                                 </select>
                        @if ($errors->has('short_name'))
                        <span class="help-block">
                        <strong>{{ $errors->first('short_name') }}</strong>`
                        </span>
                        @endif
                     </div>-->
                     <!--<div class="col-md-4">
                         	<input type="radio" name="yearlytype" id="yearlytype" value="15"  placeholder="" /> 15 days
                         	
                         	<input type="radio" name="yearlytype" id="yearlytype" value="30"  placeholder="" /> 30 days
                         	<input type="radio" name="yearlytype" id="yearlytype" value="45"  placeholder="" /> 45 days
                         	<input type="radio" name="yearlytype" id="yearlytype" value="60"  placeholder="" /> 60 days
                         
                        
                     </div>
                  </div>-->
                  <div id="hh">
                 <div class="form-group {{ $errors->has('authority_name') ? ' has-error' : '' }}">
                     <label class="control-label col-md-3">Type of Entity :</label>
                     <div class="col-md-4">
                        <select type="text" class="form-control fsc-input" name="authority_name" id="authority_name" placeholder="Enter Your Company Name" >
                                    <option value="">---Select---</option>
                                    @foreach($entity as $entity1)
                                    <option value="{{$entity1->typeentity}}">{{$entity1->typeentity}}</option>
                                    @endforeach
                                 </select>
                        @if ($errors->has('authority_name'))
                        <span class="help-block">
                        <strong>{{ $errors->first('authority_name') }}</strong>
                        </span>
                        @endif
                     </div>
                     <div class="col-md-4"><button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i></button></div>
                  </div>
               

                        <div class="form-group">
							<label class="control-label col-md-3">Form Name :</label>
							<div class="col-md-4">
								<input type="text" name="formname" id="formname" class="form-control" placeholder="Form Name" />
							</div>
						</div>
						
						 <div class="form-group {{ $errors->has('filing_frequency') ? ' has-error' : '' }}">
                            <label class="control-label col-md-3">Filing Frequency :</label>
                            <div class="col-md-4">
                                <select type="text" class="form-control fsc-input" name="filling_frequency" id="filling_frequency">
                                    <option value="">---Select---</option>
                                   	<option value="Annually" >Annually</option>
			                    	<option value="Monthly" >Monthly</option>
			                    	<option value="Quaterly" >Quaterly</option>
                                 </select>
                                @if ($errors->has('filling_frequency'))
                                <span class="help-block">
                                <strong>{{ $errors->first('filling_frequency') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
						
						<div class="form-group">
							<label class="control-label col-md-3">Due Date :</label>
							<div class="col-md-4">
								<input type="text" name="duedate" id="duedate" class="form-control" placeholder="Due Date" />
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3">Extension Due Date  :</label>
							<div class="col-md-4">
								<input type="text" name="extdate" id="extdate" class="form-control" placeholder="Extension Due Date" />
							</div>
						</div>
						
                  	<div class="form-group">
							<label class="control-label col-md-3">Authority Short Name :</label>
							<div class="col-md-4">
								<input type="text" name="authority_level" id="authority_level" class="form-control" placeholder="Authority Level" />
							</div>
						</div>
                  	<div class="form-group">
							<label class="control-label col-md-3">Authority Full Name :</label>
							<div class="col-md-4">
								<input type="text" name="authority_name1" id="authority_name1" class="form-control" placeholder="Authority Name" />
							</div>
						</div>
							<div class="form-group">
							<label class="control-label col-md-3">Website :</label>
							<div class="col-md-4">
								<input type="text" name="website" id="website" class="form-control" placeholder="Website" />
							</div>
						</div>
							<div class="form-group">
							<label class="control-label col-md-3">Telephone # :</label>
							<div class="col-md-4">
								<input type="text" name="telephone" id="telephone" class="form-control" placeholder="Telephone" />
							</div>
						</div>
						<!--<div class="form-group">
							<label class="control-label col-md-3">Renew Link :</label>
							<div class="col-md-4">
								<input type="text" name="renewlink" id="renewlink" class="form-control" placeholder="Renew Link" />
							</div>
						</div>-->
						</div>
	     <!--               <div class="form-group" id="payroll" style="display:none">-->
						<!--	<label class="control-label col-md-3">Payroll :</label>-->
						<!--	<div class="col-md-9">-->
						<!--		<div class="check_tab">-->
						<!--			<input type="radio" id="Federal" class="payroll" name="payroll" value="Federal">-->
						<!--			<label for="Federal">Federal</label>-->
						<!--		</div>-->
						<!--		<div class="check_tab">-->
						<!--			<input type="radio" id="State" name="payroll" class="payroll"  value="State">-->
						<!--			<label for="State">State</label>-->
						<!--		</div>-->
						<!--		<div class="check_tab">-->
						<!--			<input type="radio" id="County" name="payroll" class="payroll"  value="County">-->
						<!--			<label for="County">County</label>-->
						<!--		</div>-->
						<!--		<div class="check_tab" >-->
						<!--			<input type="radio" id="Local" name="payroll" class="payroll"  value="Local">-->
						<!--			<label for="Local">Local</label>-->
						<!--		</div>-->
						<!--	</div>-->
						<!--</div>-->
	                    <div class="Branch" id="federal" style="display:none">
							<div class="col-md-3" style="text-align:left;">
								<h1 id="fed">Federal</h1>
							</div>
							<div class="col-md-6">
								<h1 style="font-size:20px;">Payroll Tax</h1>
							</div>
						</div>
						<div class="federal_tabs_main" id="federal_tabs_main"  style="display:none">
							<div class="federal_tabs">
								<div class="federal_tab federal_name" style="width:10% !important;">
									<label>Short Name</label>
									<input type="text" class="form-control" id="payroll_name" name="payroll_name" placeholder="Name">
								</div>
								<div class="federal_tab federal_depart" style="width:35% !important;">
									<label>Department Name</label>
									<input type="text" class="form-control" id="payroll_department_name" name="payroll_department_name" placeholder="Department Name">
								</div>
								<div class="federal_tab federal_link">
									<label>Link</label>
									<input type="text" class="form-control" id="payroll_link" name="payroll_link" placeholder="Payroll Link">
								</div>
								<div class="federal_tab federal_tele">
									<label>Telephone #</label>
									<input type="text" class="form-control" id="payroll_telephone" name="payroll_telephone" placeholder="Telephone">
								</div>
							</div>
						</div>
						<div class="filing_tabs_main" id="filing_tabs_main"  style="display:none">
							<h3 class="comm_title_one">Filing Frequency</h3>
							<div class="filing_tabs">
								<div class="filing_tab filing_freq" style="width:10% !important;">
									<label>Form #</label>
            							<select type="text" class="form-control-insu form_number row_1" id="filing_frequency_form_1" name="filing_frequency_form[]">
            							    <option>-Select-</option>
            					            <option value="940">940</option>
            				            	<option value="941">941</option>
            					            <option value="944">944</option>
            					        </select>
								</div>
								
								<div class="filing_tab filing_name" style="width:35% !important;">
									<label>Form Name</label>
									<input type="text" class="form-control form_name row_1" id="filing_frequency_name_1" placeholder="Form Name" name="filing_frequency_name[]" readonly>
								</div>
								
								<div class="filing_tab filing_freq">
									<label>Filing Frquency</label>
									<input type="text" class="form-control-insu row_1" id="filing_frequency_1" name="filing_frequency[]" readonly>
									    <!--<select type="text" class="form-control-insu row_1" id="filing_frequency_1" name="filing_frequency[]" disabled>-->
									    <!--    <option>-Select-</option>-->
            	<!--				            <option value="Annually">Annually</option>-->
            	<!--			            	<option value="Monthly">Monthly</option>-->
            	<!--				            <option value="Quaterly">Quaterly</option>-->
            	<!--				        </select>-->
								</div>
								
								<div class="filing_tab filing_due_date" style="width:10% !important;">
									<label>Due Date</label>
									<input type="text" class="form-control row_1" placeholder="Mar-1-2020" id="filing_frequency_due_date_1" name="filing_frequency_due_date[]" readonly>
								</div>
								<div class="filing_add">
									<button type="button" id="add_row1" class="filing_addbtn btn-success">ADD</button>
								</div>
							
							</div>
						</div>
						<div class="payment_tabs_main" style="display:none">
							<h3 class="comm_title_two">Payment Frequency</h3>
							<div class="payment_tabs">
								<div class="payment_tab payment_form" style="width:10% !important;">
									<label>Form #</label>
            							<select type="text" class="form-control-insu form_number pay_1" id="filing_frequency_form1_1" name="filing_frequency_form1[]">
            							    <option>-Select-</option>
            					            <option value="940">940</option>
            				            	<option value="941">941</option>
            					            <option value="944">944</option>
            					        </select>

								</div>
								<div class="payment_tab payment_name" style="width:35% !important;">
									<label>Form Name</label>
									<input type="text" class="form-control form_name pay_1" id="filing_frequency_name1_1" placeholder="Form Name" name="filing_frequency_name1[]" readonly>
								</div>
								<div class="payment_tab payment_freq">
									<label>Payment Frquency</label>
									    <input type="text" class="form-control-insu pay_1" name="filing_frequency1[]" id="filing_frequency1_1" readonly>
                 <!--   					<select type="text" class="form-control-insu pay_1" name="filing_frequency1[]" id="filing_frequency1_1" disabled>-->
									        <!--<option>-Select-</option>-->
            					<!--            <option value="Annually">Annually</option>-->
            				 <!--           	<option value="Monthly">Monthly</option>-->
            					<!--            <option value="Quaterly">Quaterly</option>-->
            					<!--        </select>-->
								</div>
								<div class="payment_tab payment_due_date" style="width:10% !important;">
									<label>Due Date</label>
									<input type="text" class="form-control pay_1" placeholder="Mar-1-2020" id="filing_frequency_due_date1_1" name="filing_frequency_due_date1[]" readonly>
								</div>
								<div class="payment_add">
									<button type="button" id="add_row2" class="payment_addbtn btn-success">ADD</button>
								</div>
							
							</div>
						</div>
						
						
                    <div class="card-footer">
                                    <div class="col-md-2 col-md-offset-3">
            						    <input class="btn_new_save btn-primary1" style="margin-left:-5%" type="submit" name="submit" value="Save">
            						</div>
									<div class="col-md-2 row">
									    <a class="btn_new_cancel" style="margin-left:-5%" href="{{url('fac-Bhavesh-0554/taxfederal')}}">Cancel</a> 
									</div>
                    </div>
               </form>
            </div>
         </div>
      </div>
   </div>
    </section>
</div>

<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script type="text/javascript">
    $(document).ready(function()
    {
		$("#form_valid").validate(
		{
				rules:{
						typeofform: {
							required: true,
								}
					},
				messages:{
						typeofform: {
								required: "Please select type of tax",
								}
					
			            }
		});

   });	
   
   

$(document).ready(function(){

    $('#filing_frequency_form_1').on('change', function() {
      if(this.value == '940')
      {
        $("#filing_frequency_name_1").val('Employers Annual Federal Un Employment (FUTA) tax return');
        $("#filing_frequency_1").val('Annually');
        //$("#filing_frequency_due_date_1").val('<?php echo "Jan".'-'.date("t").'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
      }
      else if(this.value == '941')
      {
        $("#filing_frequency_name_1").val('Employers Quarterly Federal tax return');
	    $("#filing_frequency_1").val('Quaterly');
	   //$("#filing_frequency_due_date_1").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
      }
      else if(this.value == '944')
      {
        $("#filing_frequency_name_1").val('Employers Annual Federal tax return');
		$("#filing_frequency_1").val('Annually');
		//$("#filing_frequency_due_date_1").val('<?php echo "Jan".'-'.date("t").'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
      }
    });
    
    
    
    $('#filing_frequency_form1_1').on('change', function() {
      if(this.value == '940')
      {
        $("#filing_frequency_name1_1").val('Employers Annual Federal Un Employment (FUTA) tax return');
        $("#filing_frequency1_1").val('Annually');
        //$("#filing_frequency_due_date1_1").val('<?php echo "Jan".'-'.date("t").'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
      }
      else if(this.value == '941')
      {
        $("#filing_frequency_name1_1").val('Employers Quarterly Federal tax return');
	    $("#filing_frequency1_1").val('Quaterly');
	    //$("#filing_frequency_due_date1_1").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
      
      }
      else if(this.value == '944')
      {
        $("#filing_frequency_name1_1").val('Employers Annual Federal tax return');
		$("#filing_frequency1_1").val('Annually');
	//	$("#filing_frequency_due_date1_1").val('<?php echo "Jan".'-'.date("t").'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
      }
    });

});


$(document).ready(function(){
    var k=1;
	var maxField = 10; //Input fields increment limitation
	var addButton = $('.add_button'); //Add button selector
	var wrapper = $('.filing_tabs_main'); //Input field wrapper
	$('#add_row1').click(function(){ //Once add button is clicked
	k++;
	

	var fieldHTML = '<div class="filing_tabs"><div class="filing_tab filing_freq" style="width:10% !important;"><label>Form #</label><select type="text" class="form-control-insu form_number row_'+k+'" id="filing_frequency_form_'+k+'" name="filing_frequency_form[]"><option>-Select-</option><option value="940">940</option><option value="941">941</option><option value="944">944</option></select></div><div class="filing_tab filing_name" style="width:35% !important;"><label>Form Name</label><input type="text" class="form-control form_name row_'+k+'" id="filing_frequency_name_'+k+'" placeholder="Form Name" name="filing_frequency_name[]" readonly></div><div class="filing_tab filing_freq"><label>Filing Frquency</label><input type="text" class="form-control-insu row_'+k+'" id="filing_frequency_'+k+'" name="filing_frequency[]" readonly></div><div class="filing_tab filing_due_date" style="width:10% !important;"><label>Due Date</label><input type="text" class="form-control row_'+k+'" placeholder="Jan-1-2010" id="filing_frequency_due_date_'+k+'" name="filing_frequency_due_date[]" readonly></div><div class="professional_btn"><a href="javascript:void(0);" id="remove_button_pro" class="btn_professional btn_professional_remove">Remove</a></div>';
	        $(wrapper).append(fieldHTML);
	        
	        $('.row_'+k).change(function()
	        {
                var thiss=this.value;
                if(thiss == '940')
                {
                    $("#filing_frequency_name_"+k).val('Employers Annual Federal Un Employment (FUTA) tax return');
                    $("#filing_frequency_"+k).val('Annually');
                    $("#filing_frequency_due_date_"+k).val('<?php echo "Jan".'-'.date("t").'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
                }
                else if(thiss == '941')
                {
                    $("#filing_frequency_name_"+k).val('Employers Quarterly Federal tax return');
            	    $("#filing_frequency_"+k).val('Quaterly');
                    $("#filing_frequency_due_date_"+k).val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
                }
                else if(thiss == '944')
                {
                    $("#filing_frequency_name_"+k).val('Employers Annual Federal tax return');
            		$("#filing_frequency_"+k).val('Annually');
            		$("#filing_frequency_due_date_"+k).val('<?php echo "Jan".'-'.date("t").'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
                }
            });
            
	});
	$(wrapper).on('click', '#remove_button_pro', function(e){ //Once remove button is clicked
		e.preventDefault();
		$(this).parent().parent('.filing_tabs').remove(); //Remove field html
		x--; //Decrement field counter
		k--;
	});
	

});
</script>


<script type="text/javascript">
$(document).ready(function(){
	var j=1;
	var maxField = 10; //Input fields increment limitation
	var addButton = $('.add_button'); //Add button selector
	var wrapper = $('.payment_tabs_main'); //Input field wrapper
	$('#add_row2').click(function(){ //Once add button is clicked
	j++;
	var fieldHTML = '<div class="payment_tabs"><div class="payment_tab payment_form" style="width:10% !important;"><label>Form #</label><select type="text" class="form-control-insu form_number pay_'+j+'" id="filing_frequency_form1_'+j+'" name="filing_frequency_form1[]" readonly><option>-Select-</option><option value="940">940</option><option value="941">941</option><option value="944">944</option></select></div><div class="payment_tab payment_name" style="width:35% !important;"><label>Form Name</label><input type="text" class="form-control form_name pay_'+j+'" id="filing_frequency_name1_'+j+'" placeholder="Form Name" name="filing_frequency_name1[]" readonly></div><div class="payment_tab payment_freq"><label>Payment Frquency</label><input type="text" class="form-control-insu pay_'+j+'" id="filing_frequency1_'+j+'" name="filing_frequency1[]" readonly></div><div class="payment_tab payment_due_date" style="width:10% !important;"><label>Due Date</label><input type="text" class="form-control pay_'+j+'" placeholder="Mar-1-2020" id="filing_frequency_due_date1_'+j+'" name="filing_frequency_due_date1[]" readonly></div><div class="payment_add"><a href="javascript:void(0);" id="remove_button_pro1" class="btn_professional btn_professional_remove">Remove</a></div></div>';
	        $(wrapper).append(fieldHTML);
            $('.pay_'+j).change(function()
	        {
                var thiss=this.value;
                if(thiss == '940')
                {
                    $("#filing_frequency_name1_"+j).val('Employers Annual Federal Un Employment (FUTA) tax return');
                    $("#filing_frequency1_"+j).val('Annually');
                    $("#filing_frequency_due_date1_"+j).val('<?php echo "Jan".'-'.date("t").'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
            		
                }
                else if(thiss == '941')
                {
                    $("#filing_frequency_name1_"+j).val('Employers Quarterly Federal tax return');
            	    $("#filing_frequency1_"+j).val('Quaterly');
            	    $("#filing_frequency_due_date1_"+j).val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
                   
                }
                else if(thiss == '944')
                {
                    $("#filing_frequency_name1_"+j).val('Employers Annual Federal tax return');
            		$("#filing_frequency1_"+j).val('Annually');
            	    $("#filing_frequency_due_date1_"+j).val('<?php echo "Jan".'-'.date("t").'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
                }
            });
	});
	$(wrapper).on('click', '#remove_button_pro1', function(e){ //Once remove button is clicked
		e.preventDefault();
		$(this).parent().parent('.payment_tabs').remove(); //Remove field html
		x--; //Decrement field counter
		j--;
	});
});
</script>

<script>
   $(document).ready(function(){
        $('#typeofform').change(function (){
            var selectedText =  $('#typeofform').val();
            if(selectedText=='Payroll Tax')
            {
                $('#filing_tabs_main').show();
                $('#federal').show();
                $('#payroll').show();
                $('#federal_tabs_main').show();
                $('.payment_tabs_main').show();  
                $('#hh').hide();
            }
            else 
            {
              
                $('#filing_tabs_main').hide();
                $('#federal').hide();
                $('#payroll').hide();
                $('#federal_tabs_main').hide();
                $('.payment_tabs_main').hide();  $('#hh').show();     
            }
        })
    })
    
    
    $(document).ready(function () {
       $('.payroll').click(function () {
     var va= $(this).val();
    $('#fed').html(va);
       });

   });
</script>


<script type="text/javascript">
   $(function () {
        $("#filing_frequency_form1_1").change(function ()
        {
            var selectedText =  $('#filing_frequency1_1').val();
            //alert(selectedText);
            if(selectedText =='Annually')
            {
                $("#filing_frequency_due_date1_1").val('<?php echo "Jan".'-'.date("t").'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
            }
            else if(selectedText =='Quaterly')
            {
                if(selectedText > '<?php echo date('M-d-Y');?>') 
                {
                    $("#filing_frequency_due_date1_1").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
                }
                else
                { 
                    $("#filing_frequency_due_date1_1").val('<?php echo date("M",strtotime("3 month",strtotime(date("M")))).'-'.date("t").'-'.date("Y");?>'); 
                }
           }
       });
   });
 
</script>


<script type="text/javascript">
   $(function () {
        $("#filing_frequency_form_1").change(function ()
        {
            var selectedText =  $('#filing_frequency_1').val();
           // alert(selectedText);
            if(selectedText =='Annually')
            {
                $("#filing_frequency_due_date_1").val('<?php echo "Jan".'-'.date("t").'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
            }
            else if(selectedText =='Quaterly')
            {
                if(selectedText > '<?php echo date('M-d-Y');?>') 
                {
                    $("#filing_frequency_due_date_1").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
                }
                else
                { 
                    $("#filing_frequency_due_date_1").val('<?php echo date("M",strtotime("3 month",strtotime(date("M")))).'-'.date("t").'-'.date("Y");?>'); 
                }
           }
       });
   });
 

    $("#duedate").datepicker({
    		autoclose: true,
    format: "M-dd",
    });
    $("#extdate").datepicker({
    		autoclose: true,
    format: "M-dd",
    });
    $("#payroll_telephone").mask("(999) 999-9999");
    $("#telephone").mask("(999) 999-9999");
</script>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Type of Entity</h4>
        </div>
        <div class="modal-body" style="display: inline-table;">
            <form action="" method="post" id="ajax2">
                {{csrf_field()}}
                <div class="form-group">
                    <label class="control-label col-md-3">Type of Entity :</label>
                    <div class="col-md-6">
                        <div class="">
                            <input type="text" name="newopt" id="newopt" class="form-control" placeholder="Type of Entity">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="">
                            <input type="button" id="addopt" class="btn btn-primary" value="Add Type of Entity">
                        </div>
                    </div>
                </div>
            </form>
            <div class="form-group">
                <label class="control-label col-md-3"></label>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>

    </div>
</div>

<script>


          $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  }
              });
           
             $(function () {
                $('#addopt').click(function () { //alert();
                    var newopt = $('#newopt').val();
                    if (newopt == '') {
                        alert('Please enter something!');
                        return;
                    }

                   //check if the option value is already in the select box
                    $('#purpose1 option').each(function (index) {
                        if ($(this).val() == newopt) {
                            alert('Duplicate option, Please enter new!');
                        }
                    })
                    $.ajax({
        type: "post",
        url: "{!!route('typeof.typeofentity')!!}",
        dataType: "json",
        data: $('#ajax2').serialize(),
        success: function(data){
             alert('Successfully Add');
             $('#authority_name').append('<option value=' + newopt + '>' + newopt + '</option>');
             $("#div").load(" #div > *");
             $("#newopt").val('');
        },
        error: function(data){
             alert("Error")
        }
    });
                   
                     $('#myModal').modal('hide');
                });
            });
            
</script>
@endsection()

