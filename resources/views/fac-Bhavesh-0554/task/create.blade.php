@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
<div class="content-wrapper">
      <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>New Task</h1>
    </section>
  <!-- Main content -->
    <section class="content">
   <div class="row">
      <div class="col-md-12">
         <div class="box box-success">
			      <div class="box-header">
              
              <div class="box-tools pull-right">
                
              </div>
            </div>
            <div class="card-body col-md-12">
               <form method="post" action="{{route('task.store')}}" class="form-horizontal" id="content" name="content" enctype="multipart/form-data">
                  {{csrf_field()}}
                   <div class="form-group">
                   <label class="control-label col-md-3">Date / Day / Time:</label>
                     <div class="col-lg-6 col-md-9">
                         <div class="row">
                     <div class="col-lg-4 col-md-4">
                        <div class="">
                           <input type="text" name="date" id="date" class="form-control" value="{{date('m-d-Y')}}" placeholder="Date" readonly>
                        </div>
                     </div>
                     <div class="col-lg-4 col-md-4 col-xs-6">
                        <div class="">
                           <input type="text" name="day" id="day" class="form-control" placeholder="Day" value="{{date('l')}}" readonly>
                        </div>
                     </div>
                    
                     <div class="col-lg-3 col-md-4 col-xs-6">
                        <div class="">
                           <input type="text" name="time" id="time" class="form-control" value="{{date("g:i a")}}" placeholder="Time" readonly>
                        </div>
                     </div>
                     </div></div>
                    </div>
                    <div class="form-group{{ $errors->has('employee') ? ' has-error' : '' }}">
                     <label class="control-label col-md-3">Employee / User :</label>
                     <div class="col-lg-6 col-md-9">
                        <div class="row">
                            <div class="col-lg-11 col-md-12">
                           <select name="employee" id="employee" class="form-control fsc-input">
                              <option value="">Select Employee</option>
                              @foreach($emp1 as $em)
                                       <option value='{{$em->id}}'>{{$em->firstName.' '.$em->middleName.' '.$em->lastName}}</option>
                              @endforeach
                            
                           </select>
                        </div>
                        </div>
                        @if ($errors->has('employee'))
                        <span class="help-block">
                        <strong>{{ $errors->first('employee') }}</strong>
                        </span>
                        @endif	
                     </div>
                  </div>
                  
                  
                  <div class="form-group{{ $errors->has('employee') ? ' has-error' : '' }}">
                     <label class="control-label col-md-3">For Whom :</label>
                     <div class="col-lg-6 col-md-9">
                        <div class="row">
                            <div class="col-lg-11 col-md-12">
                           <select name="whone" id="whone" class="form-control fsc-input">
                              <option value="">Select</option>
                              <option value='Client'>Client</option>
                              <option value='Other'>Other</option>
                           </select>
                        </div>
                        </div>	
                     </div>
                  </div>
                  <div class="form-group{{ $errors->has('client') ? ' has-error' : '' }}" id="clients" style="display:none;">
                     <label class="control-label col-md-3">Client :</label>
                     <div class="col-lg-6 col-md-9">
                        <div class="row">
                            <div class="col-lg-11 col-md-12">
                           <select name="client" id="client" class="form-control fsc-input">
                              <option value="">Select</option>
                             @foreach($customer as $cust)
                              <option value='{{$cust->id}}' >@if($cust->business_id !='6') {{$cust->company_name}} ({{$cust->filename}}) @else {{$cust->first_name.' '.$cust->last_name}} ({{$cust->filename}}) @endif</option>
                              @endforeach
                           </select>
                        </div>
                        </div>
                        	
                     </div>
                  </div>
                    <div class="form-group{{ $errors->has('other') ? ' has-error' : '' }}" style="display:none;" id="client1">
                     <label class="control-label col-md-3">Other :</label>
                     <div class="col-lg-6 col-md-9">
                        <div class="row">
                            <div class="col-lg-11 col-md-12">
                            <input type="text" name="othername" id="othername" class="form-control fsc-input">
                        </div>
                    	</div>
                     </div>
                  </div>
                  <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                     <label class="control-label col-md-3">Subject :</label>
                     <div class="col-lg-6 col-md-9">
                        <div class="row">
                            <div class="col-lg-11 col-md-12">
                           <input type="text" name="title" id="title" class="form-control fsc-input">
                            <input type="hidden" value="{{ Auth::user()->id}}" name="admin_id" id="admin_id" class="form-control fsc-input">
                        </div>
                        @if ($errors->has('title'))
                        <span class="help-block">
                        <strong>{{ $errors->first('title') }}</strong>
                        </span>
                        @endif	
                     </div>
                     </div>
                  </div>
                  
                  <div class="form-group">
                     <label class="control-label col-md-3">Priority :</label>
                     <div class="col-lg-6 col-md-9">
                        <div class="row">
                            <div class="col-lg-11 col-md-12">
                           <select name="priority" id="priority" class="form-control fsc-input">
                              
                              <option value='Regular'>Regular</option>
                             <option value='Urgent'>Urgent</option>
                             <option value='Very Urgent'>Very Urgent</option>
                             
                           </select>
                        </div>
                        </div>
                     </div>
                  </div>
                  
                  <div class="form-group">
                     <label class="control-label col-md-3">Due Date :</label>
                     <div class="col-lg-6 col-md-9">
                        <div class="row">
                            <div class="col-lg-11 col-md-12">
                           <input type="text" name="duedate" id="duedate" class="form-control fsc-input" readonly>
                        </div>
                        </div>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="control-label col-md-3">Description :</label>
                     <div class="col-lg-6 col-md-9">
                        <div class="row">
                            <div class="col-lg-11 col-md-12">
                           <textarea id="editor1" name="description" rows="10" cols="80"></textarea>
                        </div>
                        </div>
                     </div>
                  </div>
                  
                   <div class="form-group">
                     <label class="control-label col-md-3"></label>
                     <div class="col-md-8">
                        <div class="row">
                           <div class="card-footer">
                      <div class="col-xs-2" style="width:155px;">
									<input class="btn_new_save btn-primary1" type="submit" name="submit" value="Send">
									</div>
									<div class="col-xs-2" style="width:155px;">
									<a class="btn_new_cancel" href="{{url('fac-Bhavesh-0554/task')}}">Cancel</a> 
									</div>
                  
                  </div>
                        </div>
                     </div>
                  </div>
                  
                 
               </form>
            </div>
         </div>
      </div>
   </div>
       </section>
</div>

<script>
   $(document).ready(function() {
  var dateInput = $('input[name="duedate"]'); 
  var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : 'body';
  dateInput.datepicker({
    format: 'M-dd-yyyy',
    container: container,
    todayHighlight: true,
    autoclose: true,
    startDate: truncateDate(new Date())
  });

  $('#duedate').datepicker('setStartDate', truncateDate(new Date())); 
});

function truncateDate(date) {
  return new Date(date.getFullYear(), date.getMonth(), date.getDate());
}
</script>

<script>
        $(document).ready(function(){
        $('#whone').on('change', function() {
  if(this.value=='Client')
  {
      $('#clients').show();
      $('#client1').hide();
  }
  else if(this.value=='Other'){
      $('#clients').hide();
      $('#client1').show();
      
  }
  else
  {
      $('#clients').hide();
      $('#client1').hide();
  }
});
    });
</script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6/js/select2.min.js"></script>
<script>
$("#client").select2({
})
</script>
<style>
    .select2 {width:100% !important;}
    .select2-container .select2-selection--single {
   
    border: 2px solid #00468F;
}
.select2-container .select2-selection--single{ height:36px;}
</style
@endsection()