@extends('fac-Bhavesh-0554.layouts.app')

@section('main-content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>Formation Setup</h1>
    </section>
    <!-- Main content -->
    <section class="content">
	<div class="row">
		<div class="col-md-12">
				<div class="box box-success">
			      <div class="box-header">
              
              <div class="box-tools pull-right">
                
              </div>
            </div>
				<div class="col-md-12">
					<form method="post" action="{{route('formationsetup.update',$position->id)}}" class="form-horizontal" id="businessname" name="businessname" enctype="multipart/form-data">
					{{csrf_field()}}{{method_field('PATCH')}}
						<div class="form-group {{ $errors->has('question') ? ' has-error' : '' }}">
							<label class="control-label col-md-3">Formation Setup Name :</label>
							<div class="col-md-4">
 <select name="type" id="type" class="form-control">
                                             <option value="">Select</option> 
                                             <option value="Type of Entity" @if($position->type=='Type of Entity') selected @endif>Type of Entity</option>
                                             <option value="Type of Business" @if($position->type=='Type of Business') selected @endif>Type of Business</option>
                                            <option value="C Corporation" @if($position->type=='C Corporation') selected @endif>C Corporation</option>
                                                                        <option value="S Corporation"@if($position->type=='S Corporation') selected @endif>S Corporation</option>
                                                                        <option value="Single Member LLC" @if($position->type=='Single Member LLC') selected @endif>Single Member LLC</option>
                                                                        <option value="Double Member LLC" @if($position->type=='Double Member LLC') selected @endif>Double Member LLC</option>
                                          </select>								
@if ($errors->has('question'))
										<span class="help-block">
											<strong>{{ $errors->first('question') }}</strong>
										</span>
									@endif
								
							</div>
						</div>
						<div class="form-group {{ $errors->has('question') ? ' has-error' : '' }}">
							<label class="control-label col-md-3">Formation Setup Name :</label>
							<div class="col-md-4">
								<input name="question" type="text" id="question" class="form-control" value="{{$position->question}}" />                                                            @if ($errors->has('question'))
										<span class="help-block">
											<strong>{{ $errors->first('question') }}</strong>
										</span>
									@endif
							</div>
						</div>	
										
							<div class="form-group {{ $errors->has('expiredate') ? ' has-error' : '' }}">
							<label class="control-label col-md-3">Expire Date :</label>
							<div class="col-md-4">
								<input name="expiredate" type="text" id="expiredate" class="form-control" value="{{$position->date}}" />          
								
							</div>
						</div>	
						<div class="card-footer">
						<div class="col-md-2 col-md-offset-3">
									<input class="btn_new_save btn-primary1" style="margin-left:-5%" type="submit" name="submit" value="Save">
									</div>
									<div class="col-md-2 row">
									<a class="btn_new_cancel" style="margin-left:-5%" href="{{url('fac-Bhavesh-0554/question')}}">Cancel</a> 
									</div>
						</div>
						
					</form>
				</div>
			</div>
		</div>
	</div>
	</section>
</div>
@endsection()