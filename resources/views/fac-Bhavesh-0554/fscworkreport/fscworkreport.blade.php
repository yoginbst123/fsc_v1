@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content') 
<style>
.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{padding:5px 8px !important;vertical-align:inherit !important;}
.headerclr{background:#ffff99 !important;}
.table > thead > tr > th{background:none !important;}
.addagemodal .form-control:focus, .addagemodal .form-control:hover{background:#ffff99;}
span.required
{
    color:red;
}
.table > thead > tr > th{padding:8px!important;}
.subtitle{height: 25px;  max-width: 600px;  margin: 0px auto 15px;}
.subtitle span{font-size:16px; color:#000;}
.floatleft{float:left;}
.floatright{float:right;}
.reporttext{
    display:flex; 
    flex-direction:row; 
    justify-content:center; 
    width:100%;
    margin: 15px auto 0px;
    border: 2px solid #a5a5a5;
    width: 100%;
    padding-right: 0!important;
    position: relative;
    max-width: inherit!important;
    left: 0%;
    display: flex;
    flex-direction: row;
    background: #e2e2e2;
    padding: 5px 0px;
    height: 60px;
    margin-bottom: -105px;
    position: unset;
    margin-top: -10px;
}
.dt-buttons {
    margin-bottom: 80px;
}
.reporttext ul{margin: 0px; padding: 0px; display: flex; flex-direction: ROW; justify-content: space-between;width:100%;}
.reporttext ul li{list-style-type:none; margin:0px 10px; font-size:16px;}
.reporttable{margin-top:10px;}
.reporttable tr th{padding:5px 10px!important;}
.workreportbox{
    /*display:flex; */
    flex-direction:row; justfy-content:flex-start; align-items:flex-start;
}
.workreportbox ul { display:flex; flex-direction:row; justfy-content:flex-start; align-items:flex-start; list-style: none;
    list-style-type: none;
    margin: 0px;
    padding: 0px;}
.workreportbox ul li{ display:flex; flex-direction:row; justify-content:flex-start; align-items:flex-start; flex-wrap:wrap; margin:0px 5px;}
.workreportbox ul li label{width:100%; text-align:left!important;}
.workreportbox ul li  .btn-primary{height:40px; width:100px;}
.reporthd{background:#ffff99!important;}
.edittable tr td{padding:0px 5px!important;}
.form-control, .form-control-insu{
    padding: 6px 1px!important;
    font-size: 14.5px !important;
}
.reporttext label {
    margin-bottom: 0px;
}
form.form-horizontal.changepassword {
    font-size: 13px;
}
@media screen {
  #printSection {
      display: none;
  }
}

@media print {
   .close, .btn{display:none; font-size:0px;}
  body * {
    visibility:hidden;
  }
  #printSection, #printSection * {
    visibility:visible;
  }
  #printSection {
    position:absolute;
    left:0;
    top:0;
  }
}
@media (max-width:991px){
    div.dataTables_wrapper div.dataTables_filter {
        margin-top: -4px;
    }
    .dt-buttons {
        margin-top: 7px;
    }
}
</style>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h2>List of Work Report</h2>
    </section>
    <section class="content">
   <div class="row">
      <div class="col-md-12">
        <div class="box box-success">
			      <div class="box-header">
              <div class="box-tools pull-right">
              </div>
            </div>
				<div class="col-md-12">
               <form  class="form-horizontal changepassword" action="{{route('fscworkreport.index')}}"  method="post">
                  {{csrf_field()}}
                  <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12" style="padding-right:0px;">
                        <div class="workreportbox">
                            <div class="col-xs-2 fscw_client wr_1" style="padding-left:0px;">
                                 <label for="focusedinput" class="control-label ">Client  </label>
                                  <select name="mailing_client" id="mailing_client" class="form-control fsc-input">
                                      <option value="Active" <?php if(isset($_POST['mailing_client']) && $_POST['mailing_client'] =='Active') { echo 'selected';}?>>Active</option>
                                      <option value="Inactive" <?php if(isset($_POST['mailing_client']) && $_POST['mailing_client'] =='Inactive') { echo 'selected';}?>>Inactive</option>
                                </select>
                            </div>
                            <div class="col-xs-2 fscw_type_report wr_2" style="padding-left:0px;">
                                <label for="focusedinput" class="control-label" >Type of Report  <span class="required"> * </span></label>
                                 <select class="form-control" name="formation_status" id="formation_status" required>
                                <option value="">Select</option>
                                <option value="Corporation" <?php if(isset($_POST['formation_status']) && $_POST['formation_status'] =='Corporation') { echo 'selected';}?>>Corporation</option>
                                <option value="Taxation" <?php if(isset($_POST['formation_status']) && $_POST['formation_status'] =='Taxation') { echo 'selected';}?>>Taxation</option>
                                <option value="Task" <?php if(isset($_POST['formation_status']) && $_POST['formation_status'] =='Task') { echo 'selected';}?>>Task</option>
                                <option value="Resid. Mortgage" <?php if(isset($_POST['formation_status']) && $_POST['formation_status'] =='Resid. Mortgage') { echo 'selected';}?>>Resid. Mortgage</option>
                                <option value="Com. Mortgage" <?php if(isset($_POST['formation_status']) && $_POST['formation_status'] =='Com. Mortgage') { echo 'selected';}?>>Com. Mortgage</option>
                                <option value="License" <?php if(isset($_POST['formation_status']) && $_POST['formation_status'] =='License') { echo 'selected';}?>>License</option>
                             </select>
                            </div>
                            <div class="showtax col-xs-2 fscw_type_taxation wr_3" style="padding-left:0px;" <?php if(isset($_POST['taxation_type']) && $_POST['taxation_type'] !=''){ } else {?>  style="display:none;"<?php } ?>>
                                <label for="focusedinput" class="control-label" >Type of Taxation </label>
                                 <select class="form-control" name="taxation_type" id="taxation_type">
                                <option value="">Select</option>
                                <?php
                                foreach($taxtitle as $title)
                                {
                                ?>
                                <option value="<?php echo $title->id;?>" <?php if(isset($_POST['taxation_type'])) { if($title->id == $_POST['taxation_type']) { echo 'selected';}}?>><?php echo $title->title;?></option>
                                <?php
                                }
                                ?>
                                 </select>
                            </div>
                             <div class="col-xs-1 wr_4" style="padding-left:0px;">
                                 <label for="focusedinput" class="control-label ">State  </label>
                                   <select name="mailing_state" id="mailing_state" class="form-control fsc-input">
                                    <option value="ALL">All</option>
                                    <option value="AL" <?php if(isset($_POST['mailing_state']) && $_POST['mailing_state'] =='AL') { echo 'selected';}?>>AL</option>
                                    <option value="CT" <?php if(isset($_POST['mailing_state']) && $_POST['mailing_state'] =='CT') { echo 'selected';}?>>CT</option>
                                    <option value="GA" <?php if(isset($_POST['mailing_state']) && $_POST['mailing_state'] =='GA') { echo 'selected';}?>>GA</option>
                                    <option value="GS" <?php if(isset($_POST['mailing_state']) && $_POST['mailing_state'] =='GS') { echo 'selected';}?>>GS</option>
                                    <option value="LA" <?php if(isset($_POST['mailing_state']) && $_POST['mailing_state'] =='LA') { echo 'selected';}?>>LA</option>
                                    <option value="MJ" <?php if(isset($_POST['mailing_state']) && $_POST['mailing_state'] =='MJ') { echo 'selected';}?>>MJ</option>
                                    <option value="SC" <?php if(isset($_POST['mailing_state']) && $_POST['mailing_state'] =='SC') { echo 'selected';}?>>SC</option>
                                    <option value="TN" <?php if(isset($_POST['mailing_state']) && $_POST['mailing_state'] =='TN') { echo 'selected';}?>>TN</option>
                                 </select>
                            </div>
                            <div <?php if(isset($_POST['tax_subcategory']) && $_POST['tax_subcategory'] ==""){ ?> style="display:none;" <?php } ?>class="wr_5 taxationpersonalcategory fscw_cate_taxation col-xs-2" style="padding-left:0px;">
                                <label for="focusedinput" class="control-label" >Taxation Category </label>
                                <select class="form-control" name="tax_subcategory" id="tax_subcategory">
                                <option value="">Select</option>
                                <option value="Business" <?php if(isset($_POST['tax_subcategory']) && $_POST['tax_subcategory'] =='Business') { echo 'selected';}?>>Business</option>
                                <option value="Personal" <?php if(isset($_POST['tax_subcategory']) && $_POST['tax_subcategory'] =='Personal') { echo 'selected';}?>>Personal</option>
                               <!-- <option value="Individual Income Tax Return">Individual Income Tax Return</option>
                                <option value="Estimate Advance Income Tax">Estimate Advance Income Tax</option>
                                !-->
                                 </select>
                            </div>
                            <div <?php if(isset($_POST['subcategory']) && $_POST['subcategory'] =='') { ?> style="display:none;" <?php } ?> class="taxpersonalcategory wr_6 fscw_subcate_taxation col-xs-2" style="padding-left:0px;">
                                <label for="focusedinput" class="control-label" >Taxation  SubCategory </label>
                                <select class="form-control" name="subcategory" id="subcategory">
                                    <option value="">Select</option>
                                    <option value="7" <?php if(isset($_POST['subcategory']) && $_POST['subcategory'] =='7') { echo 'selected';}?>>Individual Income Tax Return</option>
                                    <option value="8" <?php if(isset($_POST['subcategory']) && $_POST['subcategory'] =='8') { echo 'selected';}?>>Estimate Advance Income Tax</option>
                                </select>
                            </div>
                           <!-- <li style="display:none;" class="taxationbusinesscategory">
                                <label for="focusedinput" class="control-label" >Taxation Category </label>
                                 <select class="form-control" name="formation_status" id="formation_status">
                                <option value="">Select</option>
                                <option value="Corporation">Corporation</option>
                                 </select>
                            </li>!-->
                            <div class="col-xs-1 fscw_year wr_7" style="padding-left:0px;">
                                <label for="focusedinput" class=" control-label">Year</label>
                                <select name="mailing_year" id="mailing_year" class="form-control fsc-input">
                                    <option value="2021" <?php if(isset($_POST['mailing_year']) && $_POST['mailing_year'] =='2021') { echo 'selected';}?>>2021</option>
                                    <option value="2020" <?php if(isset($_POST['mailing_year']) && $_POST['mailing_year'] =='2020') { echo 'selected';}?>>2020</option>
                                    <option value="2019" <?php if(isset($_POST['mailing_year']) && $_POST['mailing_year'] =='2019') { echo 'selected';}?>>2019</option>
                                </select>
                            </div>
                            <div class="col-xs-2 fscw_status wr_8" style="padding-left:0px;">
                                 <label for="focusedinput" class="control-label">Status <?php if(isset($_POST['status'])) { echo $_POST['status'];}?> </label>
                                  <select class="form-control"  name="status" id="status" >
                                     <option value="ALL">All</option>
                                     <!--<option class="2221" value="Active Owes Curr. Yr. AR" <?php if(isset($_POST['status']) && $_POST['status'] =='Active Owes Curr. Yr. AR') { echo 'selected';}?>>Active Owes Curr. Yr. AR</option>
                                     <option class="2221" value="Active Compliance" <?php if(isset($_POST['status']) && $_POST['status'] =='Active Compliance') { echo 'selected';}?>>Active Compliance</option>
                                     <option class="2221" value="Active/Noncompliance" <?php if(isset($_POST['status']) && $_POST['status'] =='Active/Noncompliance') { echo 'selected';}?>>Active/Noncompliance</option>
                                     <option class="2221" value="Admin. Dissolved" <?php if(isset($_POST['status']) && $_POST['status'] =='Admin. Dissolved') { echo 'selected';}?>>Admin. Dissolved</option>
                                     <option class="2221" value="Dis-Cancel-Termin" <?php if(isset($_POST['status']) && $_POST['status'] =='Dis-Cancel-Termin') { echo 'selected';}?>>Dis-Cancel-Termin</option>
                                    !-->
                                    <?php 
                                    if((isset($_POST['status']) && $_POST['status'] !='0')) 
                                    {
                                    if((isset($_POST['status']) && $_POST['status'] !='Original')) 
                                    {
                                        
                                    //if($_POST['status'] !='Original')
                                    
                                    ?>
                                    <option class="222" value="Active Owes Curr. Yr. AR" <?php if(isset($_POST['status']) && $_POST['status'] =='Active Owes Curr. Yr. AR') { echo 'selected';}?>>Active Owes Curr. Yr. AR</option>
                                     <option class="222" value="Active Compliance" <?php if(isset($_POST['status']) && $_POST['status'] =='Active Compliance') { echo 'selected';}?>>Active Compliance</option>
                                     <option class="222" value="Active/Noncompliance" <?php if(isset($_POST['status']) && $_POST['status'] =='Active/Noncompliance') { echo 'selected';}?>>Active/Noncompliance</option>
                                     <option class="222" value="Admin. Dissolved" <?php if(isset($_POST['status']) && $_POST['status'] =='Admin. Dissolved') { echo 'selected';}?>>Admin. Dissolved</option>
                                     <option class="222" value="Dis-Cancel-Termin" <?php if(isset($_POST['status']) && $_POST['status'] =='Dis-Cancel-Termin') { echo 'selected';}?>>Dis-Cancel-Termin</option>
                                    <?php 
                                    }
                                    }
                                    else if(!isset($_POST['status']))
                                    {
                                    ?>
                                    <option class="222" value="Active Owes Curr. Yr. AR" <?php if(isset($_POST['status']) && $_POST['status'] =='Active Owes Curr. Yr. AR') { echo 'selected';}?>>Active Owes Curr. Yr. AR 111</option>
                                     <option class="222" value="Active Compliance" <?php if(isset($_POST['status']) && $_POST['status'] =='Active Compliance') { echo 'selected';}?>>Active Compliance</option>
                                     <option class="222" value="Active/Noncompliance" <?php if(isset($_POST['status']) && $_POST['status'] =='Active/Noncompliance') { echo 'selected';}?>>Active/Noncompliance</option>
                                     <option class="222" value="Admin. Dissolved" <?php if(isset($_POST['status']) && $_POST['status'] =='Admin. Dissolved') { echo 'selected';}?>>Admin. Dissolved</option>
                                     <option class="222" value="Dis-Cancel-Termin" <?php if(isset($_POST['status']) && $_POST['status'] =='Dis-Cancel-Termin') { echo 'selected';}?>>Dis-Cancel-Termin</option>
                                    <?php
                                    }
                                    if((isset($_POST['status']) && ($_POST['status'] =='0')) || (isset($_POST['status']) && ($_POST['status'] =='Original'))) 
                                    {
                                    ?>
                                    <option class="111" value="0" <?php if(isset($_POST['status']) && $_POST['status'] =='0') { echo 'selected';}?>>Not Done</option>
                                     <option class="111" value="Original" <?php if(isset($_POST['status']) && $_POST['status'] =='Original') { echo 'selected';}?>>Done</option>
                                <?php
                                    }
                                    else if(empty($_POST['status']))
                                    {
                                    ?>
                                    <option class="111" value="0" <?php if(isset($_POST['status']) && $_POST['status'] =='0') { echo 'selected';}?>>Not Done</option>
                                     <option class="111" value="Original" <?php if(isset($_POST['status']) && $_POST['status'] =='Original') { echo 'selected';}?>>Done</option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div <?php if(isset($_POST['fillstatus']) && $_POST['fillstatus'] =='') { ?> style="display:none;" <?php } ?> class="showfill wr_9 fscw_filling_status col-xs-2" style="padding-left:0px;">
                                 <label for="focusedinput" class="control-label">Filling Status  </label>
                                  <select class="form-control"  name="fillstatus" id="fillstatus" >
                                     <option value="">Select</option>
                                     <option value="Extension" <?php if(isset($_POST['fillstatus']) && $_POST['fillstatus'] =='Extension') { echo 'selected';}?>>Extension</option>
                                  </select>
                            </div>
                            <div class="col-xs-2 fscw_fed_status showfed wr_10" style="padding-left:0px;" <?php if(isset($_POST['federalsstatus']) && $_POST['federalsstatus'] =='') { ?> style="display:none;" <?php } ?>>
                                <label for="focusedinput" class="control-label">Federal Status  </label>
                                <select class="form-control"  name="federalsstatus" id="federalsstatus" >
                                    <option value="">Select</option>
                                    <option value="EF Processing" <?php if(isset($_POST['federalsstatus']) && $_POST['federalsstatus'] =='EF Processing') { echo 'selected';}?>>EF Processing</option>
                                    <option value="Accepted" <?php if(isset($_POST['federalsstatus']) && $_POST['federalsstatus'] =='Extension') { echo 'selected';}?>>Accepted</option>
                                </select>
                            </div>
                            
                            <div class="col-xs-2 fscw_fed_status wr_11" style="padding-left:0px;" <?php if(isset($_POST['federalsmethod']) && $_POST['federalsmethod'] =='') { ?> style="display:none;" <?php } ?>>
                                <label for="focusedinput" class="control-label">Filing Method  </label>
                                <select class="form-control"  name="federalsmethod" id="federalsmethod" >
                                    <option value="">Select</option>
                                    <option value="E-File" <?php if(isset($_POST['federalsmethod']) && $_POST['federalsmethod'] =='E-File') { echo 'selected';}?>>E-File</option>
                                    <option value="Paperfile" <?php if(isset($_POST['federalsmethod']) && $_POST['federalsmethod'] =='Paperfile') { echo 'selected';}?>>Paperfile</option>
                                </select>
                            </div>
                            <div class="col-xs-2 wr_12" style="padding-left:0px;margin-top:27px;">
                                 <!--<label for="focusedinput" class="control-label">&nbsp; </label>-->
                                 <button class="btn-success btn" type="submit"  name="search" style="width:70px;"> Ok</button>
                                  <button class="btn-danger btn" type="button" name="search" style="margin-left:5px; margin-top:0px;"onClick="Refresh();"> Reset</button>
                            </div>
                        </div>
                        
                        
                        <div class="corporationbox" style="display:none;">
                            <div class="form-group" style="margin-left:140px;">
                                <div class="col-sm-2"  style="width:18%;">
                                  <div class="">
                                    
                                  </div>
                                </div>
                                <div class="col-sm-3">
                                    <!--<select name="status" id="status" class="form-control fsc-input" style="background-color:#00ef00 !important;color:#fff !important">-->
                                    <!--     <option value="">Status</option> -->
                                    <!--     <option value="Hold" class="Red">Hold</option> -->
                                    <!--     <option class="Orange" value="Pending">Pending</option>-->
                                    <!--    <option value="Approval" class="Green">Approve</option> -->
                                    <!--    <option value="Active" style="background-color:#00ef00 !important;color:#fff !important" selected="">Active</option> -->
                                    <!--    <option value="Inactive" class="Blue" style="background:blue;color:#fff">Inactive</option> -->
                                    <!--</select>-->
                                    <!--name="record_status"-->
                                </div>
                            </div>
                        </div>
                        
                    
                        <div class="form-group ">
                           <label for="focusedinput" class="col-md-5 control-label "></label>
                           <div class="col-sm-7" style="margin:0 !important;padding:0 !important;">
                              <div class="col-sm-3">
                                
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  </form>
                  
                  </div>
                  <?php 
                  if(isset($client)){
                      //print_r($_POST);exit;
                      $clientcount=count($client);
                      if($_POST['subcategory'] =='7')
                      {
                          $SUBC='Individual Income Tax Return';
                      }
                      else if($_POST['subcategory'] =='8')
                      {
                          $SUBC='Estimate Advance Income Tax';
                      
                      }
                      else
                      {
                          $SUBC='';
                      }
                  ?>
                  <div id="printMe">
                        <style>
				  @media print{
					  table{font-family: Verdana, Geneva, Tahoma, sans-serif;}
					  table tr td{padding:4px; border-bottom:1px solid #ccc;}
					   table tr th{padding:4px; border-bottom:1px solid #ccc;}
				  }
				</style>
                  <div class="col-md-12">
                       <div class="Branch"><h1 style="text-align:center;"><?php echo $SUBC;?> Report</h1></div>
                       <div class="clear"></div>
                       <center>
    <img src="https://financialservicecenter.net/public/dashboard/images/fsc_logo.png" width="80px" class="img-responsive">
</center>
<br>
                      <div class="fscreport work_report">
                          <div class="reporttext subtitle_cr">
                              <ul>
                                  <li><label>Count: </label><span class="hide_500"><br/></span> <?php echo $clientcount;?></li>
                                  
                                    <li><label>State: </label><span class="hide_500"><br/></span> <?php echo $_POST['mailing_state'];?></li>
                                  <li><label>Client: </label><span class="hide_500"><br/></span> <?php echo $_POST['mailing_client'];?></li>
                               <?php if(isset($_POST['formation_status']) && $_POST['formation_status'] !=''){?> 
                               <li><label>Type of Report: </label><span class="hide_500"><br/></span> <?php echo $_POST['formation_status'];?></li>
                               <?php 
                               }
                               ?>
                                
                                  <li><label>Year: </label><span class="hide_500"><br/></span> <?php echo $_POST['mailing_year'];?></li>
                                 
                                  <?php if(isset($_POST['status']) && $_POST['status'] !=''){?><li><label>Status:</label> <span class="hide_500"><br/></span>
                                  <?php if($_POST['status'] =='0') { echo 'Not Done';} else { echo $_POST['status'];}?></li>
                                  <?php } ?>
                                  <?php if(isset($_POST['fillstatus']) && $_POST['fillstatus'] !=''){?><li><label>FillStatus: </label><span class="hide_500"><br/></span> <?php echo $_POST['fillstatus'];?></li><?php } ?>
                                  <?php if(isset($_POST['federalsstatus']) && $_POST['federalsstatus'] !=''){?><li><label>FederalsStatus: </label><span class="hide_500"><br/></span> <?php echo $_POST['federalsstatus'];?></li><?php } ?>
                                  
                                  
                              </ul>
                           </div>
    <!--                    <button attr="printMe"  type="button" class="btn btn-primary btn-sm pull-right clkbutton" style="margin-right: 15px; position: ABSOLUTE; right:0px;-->
    <!--top: 0px;">Print</button>-->
  
                         <div class="table-responsive table_cr">
                             
                                <?php $cnt=0;
                                //echo count($client);
                               // print_r($_POST);EXIT;
                                ?>
                              @if($clientcount !='0')
                              <table class="table table-bordered table-striped reporttable"  id="sampleTable345" align="center" style="width:100%;">
                                <thead style="background: #ffff99;">
                                <tr>
                                     <th class="text-center reporthd" style="width:40px;">No.</th>
                                     <th class="text-left reporthd"  style="width:100px;text-align:center;">Client #</th>
                                     <th style="text-align:center;width:200px;" class="reporthd">Client Name</th>
                                    <?php if($_POST['status'] !='1'){?> 
                                    <th style="text-align:center;" class="reporthd">Email</th>
                                     <?php if($_POST['formation_status'] =='Taxation'){
                                     //exit('222222');?> 
                                    
                                     <th style="text-align:center;" class="reporthd">Form No.</th>
                                     <?php } ?>
                                     <th style="text-align:center;" class="reporthd">Contact Phone</th>
                                     
                                     <?php } 
                                     else if($_POST['status'] =='1')
                                     {?>
                                     <th style="text-align:center;" class="reporthd">Filling Method</th>
                                     <th style="text-align:center;" class="reporthd">Filling Date</th>
                                     <th style="text-align:center;" class="reporthd">Filling Status</th>
                                     <?php 
                                     }
                                    ?> 
                                    <th style="text-align:center;" class="reporthd">Status</th>
                                     <th class="text-center reporthd" style="width:60px;text-align:center;">Action</th>
                                 </tr>
                                 </thead>
                                 <tbody>
                                 @foreach($client as $clients)
                                 <?php //print_r($clients);exit; ?>
                                <?php $cnt++;?>
                                 <tr <?php if(isset($clients->federalsmethod) && $clients->federalsmethod =='Paperfile') { ?>style="background:#b9f0b2;"<?php } ?>>
                                     <td style="text-align:center !important;">{{$cnt}}</td>
                                     <td style="text-align:left !important;">{{$clients->filename}}</td>
                                     <td style="text-align:left !important;"><?php if(isset($clients->business_id) && $clients->business_id=='6') {  echo $clients->first_name .' '.$clients->middle_name.' '.$clients->last_name;} else {  echo $clients->company_name; } ?></td>
                                   <?php if(isset($_POST['status']) && $_POST['status'] !='1'){?>  <td style="text-align:left !important;">{{$clients->email}}</td>
                                     <?php if(isset($_POST['formation_status']) && $_POST['formation_status'] =='Taxation'){?><td>{{$clients->typeofcorp1}}</td><?php } ?>
                                     <td style="text-align:left !important;">{{$clients->etelephone1}}</td>
                                     <?php } 
                                     else if(isset($_POST['status']) && $_POST['status'] =='1'){?>
                                     <td style="text-align:left !important;"><?php echo $clients->federalsmethod;?></td>
                                     <td style="text-align:left !important;"><?php echo date('m/d/yy',strtotime($clients->federalsdate));?></td>
                                     <td style="text-align:left !important;"><?php echo $clients->federalsstatus;?></td>
                                    <?php 
                                     }
                                     else if($_POST['fillstatus'] =='0'){?>
                                     <td style="text-align:left !important;"><?php echo $clients->federalsstatus;?></td>
                                     
                                     <?php 
                                     }
                                     ?>
                                      
                                        <td><?php if(isset($clients->record_status)){ echo $clients->record_status;}?></td>
                                     <td class="text-center">
                                         <?php 
                   if(isset($clients->tids) && $clients->tids !='')
                   {
                        ?>
                      <a href="{{route('workstatustaxation.edit',$clients->tids)}}" class="btn-action btn-view-edit"><i class="fa fa-edit"></i></a>
                    <?php  
                    }
                    else
                    {
                        ?>
                      <a href="workstatus?id=<?php echo $clients->ids;?>" class="btn-action btn-view-edit"><i class="fa fa-edit"></i></a>
                        
                        <?php
                    }
                  // else
                //    {
                        ?>
                    
                    <!--<button type="button" class="btn-action btn-view-edit federalTaxation" data-id="{{$clients->ids}}"><i class="fa fa-edit"></i></button>
                    !--><?php
                    //}
                    ?></td>
                                 </tr>
                                 @endforeach
                                 </tbody>
                                 </table>
                                 @else
                                 <table class="table table-bordered table-striped reporttable"  id="sampleTable3" align="center" style="width:100%;margin-top:100px !important;">
                                <thead style="background: #ffff99;">
                                <tr>
                                     <th class="text-center reporthd" style="width:40px;">No.</th>
                                     <th class="text-left reporthd"  style="width:100px;text-align:center;">Client #</th>
                                     <th style="text-align:center;" class="reporthd">Client Name</th>
                                    <?php if($_POST['status'] !='1'){?> 
                                    <th style="text-align:center;" class="reporthd">Email</th>
                                     <th style="text-align:center;" class="reporthd">Contact Phone</th>
                                     <?php } 
                                     else if($_POST['status'] =='1')
                                     {?>
                                     <th style="text-align:center;" class="reporthd">Filling Method</th>
                                     <th style="text-align:center;" class="reporthd">Filling Date</th>
                                     <th style="text-align:center;" class="reporthd">Filling Status</th>
                                     <?php 
                                     }
                                    ?> 
                                    <th style="text-align:center;" class="reporthd">Status</th>
                                     <th class="text-center reporthd" style="width:60px;text-align:center;">Action</th>
                                 </tr>
                                 </thead>
                                 <tbody>
                                 <tr><td colspan="7">No Records Found</td></tr>
                                 </tbody>
                                 </table>
                                 @endif
                                 
                             
                         </div>
                      </div>
                  </div>
                  </div>
<div class="col-md-12 col-sm-12 col-xs-12">  
<?php 
if(!empty($_REQUEST['emp_name'])){ ?>

<center>
    <img src="https://financialservicecenter.net/public/dashboard/images/fsc_logo.png" width="80px" class="img-responsive">
</center>
<br>
<!--<h3 style="font-size:20px; text-align:center; font-size:20px; color:#000; margin-bottom:15px;"> Client Report</h3>
<div class="subtitle"><span class="floatleft">Type of Service: <b>Business</b></span> <span class="floatright">Type of Client: <b>Monthly</b></span></div>!-->
<div class="table-responsive">
<table class="table table-hover table-bordered" id="sampleTable1">
  <thead style="background: #ffff99;">
    <tr>
      <th>No</th>
      <th style="text-align:center;">Client #</th>
      <th style="text-align:center;">Client <span style="font-weight:initial;font-size:12px;">Legal Name</span></th>
      <th style="text-align:center;">DBA Name</th>
      <th style="text-align:center;">Contact Person Name</th>
      <th style="text-align:center;">Telephone</th>
      <th style="text-align:center;">Action</th>
    </tr>
  </thead>
  <tbody>
      <?php 
      echo $count= count($client); if($count>0){
      ?>
    @foreach($client as $client1)
    <tr>
      <th scope="row">{{$loop->index+1}}</th>
      <td style="text-align:left !important;">{{$client1->filename}}</td>
      <td style="text-align:left !important;">{{$client1->company_name}}</td>
      <td style="text-align:left !important;">{{$client1->business_name}}</td>
      <td style="text-align:left !important;">{{ucwords($client1->firstname.' '.$client1->middlename.' '.$client1->lastname)}}</td>
      <td>{{$client1->business_no}}</td>
      <td><a class="btn-action btn-view-edit btn-primary" style="background:#367fa9 !important;margin-top: 5px;" href="{{route('customer.edit',$client1->id)}}?status=1"><i class="fa fa-edit"></i></a></td>
    </tr>
    @endforeach
    <?php  } 
    else
    { ?>
 <tr>
      <th scope="row"></th>
      <td></td>
      <td></td>
      <td>Not found data</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <?php }?>
  </tbody>
</table>
            </div>
            <?php  }?>
                </div>
                <?php
                  }
                  ?>
         </div>
         <br>
      </div>
   </div>
<!--</div>-->
   <div id="federalModals" class="modal fade" role="dialog">
  <div class="modal-dialog" style="width:1000px;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><span id="clid" class="companycode"></span>Client - Income Tax Filing - (Form-1040) <span style="float:right; margin-right:5px;">Add Record</span></h4>
      </div>
      <div class="modal-body">
          <form class="form-horizontal" method="post" action="{{route('worktax.store')}}" enctype="multipart/form-data">
              {{csrf_field()}} 
              
              
              <input type="hidden" name="client_id" id="federalid">
              
          <div class="recordform">
              
            
            
                <table style="width:100%; max-width:590px; margin:0px auto;" class="taxtable edittable">
                <tr>
                      <TD style="WIDTH:200PX;">
                        <label class="control-label labels">Client Name:</label>
                        <input type="text" class="form-control clname" readonly>
                    </TD>
                    <td>
                        <label class="control-label labels">Filling Year</label>
                        <select class="form-control federalyear" name="federalsyear" id="federalsyear" required>
                        <option value="">Select</option>
                        <?php
                            
                            $now=date('M-d-Y');
                            if('Jul-25-2021'==$now)
                            {
                                $aa=date('Y')-2;
                                $bb=date('Y')-1;
                                ?>
                                <option value="<?php echo $aa;?>"><?php echo $aa;?></option>
                                <option value="<?php echo $bb;?>"><?php echo $bb;?></option>
                                <?php
                            }
                            else
                            {
                                $aa=date('Y')-1;
                                ?>
                                <option value="<?php echo $aa;?>"><?php echo $aa;?></option>
                                <?php
                            }
                            
                        ?>
                        
                        
                        
                    </select>
                        
                    </td>
                    <td>
                         <label class="control-label labels">Tax Return</label>
                         
                         <select class="form-control taxation" name="federalstax" required>
                            <option value="">Select</option>
                            <option value="Extension">Extension </option>
                            <option value="Original">Original</option>
                            <option value="Amendment">Amendment</option>
                        </select>
                    </td>
                    <td style="width:150px;">
                        <label class="control-label labels">Due Date</label>
                         <input type="text" class="form-control duedate" name="federalsduedate" placeholder="Mar-15-2020" readonly/>
                    </td>
                </tr>
            </table>
            
                <table class="taxtable table table-bordered" style="margin-top:20px;" id="taxationtable">
             <tr>
                
                  <td style="width:215px;" colspan="2">
                     <label class="form-label">Federal Form No.</label>
                      <input type="text" readonly class="form-control formno" name="federalsform"  style="width:100%; max-width:100%; margin:0px; padding:0px;" required>
                     
                 </td>
                  <td style="width:120px;">
                     <label class="form-label">Filling Method</label>
                      <select class="form-control filingmethod" name="federalsmethod" required>
                        <option value="">Select</option>
                        <option value="E-File">E-File</option>
                        <option value="Paperfile">Paperfile</option>
                    </select>
                     
                 </td>
                 <td style="width:120px;">
                     <label class="form-label">Filling Date</label>
                     <input type="text" class="form-control fdate fdatef" id="fillingdate" name="federalsdate" placeholder="MM/DD/YYYY" required/>
                     <input type="hidden" class="form-control" id="federalsfile" name="federalsfile" required/>
                      
                 </td>
                  <td style="width:150px;">
                     <label class="form-label">Status of Filling</label>
                     <select class="form-control" name="federalsstatus" required>
                        <option value="">Select</option>
                        <option class="hidest" value="Accepted">Accepted</option>
                        <option class="hidest" value="EF Processing">EF Processing</option>
                        <option  class="hidest" value="Pending">Pending</option>
                        <option class="hidest" value="Rejected">Rejected</option>
                    </select>
                     
                     
                 </td>
                  
                 <td>
                     <label class="form-label">Note</label>
                       <textarea class="form-control" name="federalsnote"></textarea>
                     
                 </td>
                 <td></td>
             </tr>
             <tr><td colspan="8" style="height:20px;"></td></tr>
              <tr>
                  <th>  <label class="form-label">Resi. State</label></th>
                  <th><label class="form-label">Form No.</label></th>
                  <th><label class="form-label">Filling Method</label></th>
                  <th><label class="form-label">Filling Date</label></th>
                  <th> <label class="form-label">Status of Filling</label></th>
                  <th><label class="form-label">Note</label></th>
                  <th></th>
              </tr>
              
              
             <tr>
                 <td>
                   
                     <select class="form-control" name="statetax[]" id="statetax">
                         
                         <option>Select</option>
                         @foreach($datastate as $datastate)
                         <option value="{{$datastate->code}}">{{$datastate->code}}</option>
                         @endforeach
                     </select>
                     
                      
                      
                     
                 </td>
                  <td>
                     
                      <input type="text" readonly class="form-control" name="stateformno[]" id="statefederalformno" style="width:100px"/>
                     
                 </td>
                  <td>
                    
                    <select class="form-control filingmethodstate" name="statemethod[]">
                        <option value="">Select</option>
                        <option value="E-File">E-File</option>
                        <option value="Paperfile">Paperfile</option>
                    </select>
                     
                 </td>
                  <td>
                     
                     <input type="text" class="form-control fdate fdates fdatess" id="fillingdate" name="statedate[]" placeholder="MM/DD/YYYY" required/>
                     <input type="hidden" class="form-control" id="statefile" name="statefile" required/>
                 </td>
                  <td>
                   
                     <select class="form-control" name="statestatus[]">
                        <option value="">Select</option>
                        <option class="hidests" value="Accepted">Accepted</option>
                        <option class="hidests" value="EF Processing">EF Processing</option>
                        <option class="hidests" value="Pending">Pending</option>
                        <option class="hidests" value="Rejected">Rejected</option>
                    </select>
                     
                     
                 </td>
                  
                 <td>
                     
                       <textarea class="form-control" name="statenote[]"></textarea>
                     
                 </td>
                 <td><a href="#" class="btn btn-primary add-rowform"><i class="fa fa-plus"></i></a></td>
             </tr>
         </table>
         
        
         
                <div class="row form-group saves">
                <label class="col-md-4 control-label">&nbsp;</label>
                <div class="col-md-2">
                    <input type="submit" class="btn_new_save primary" value="Save">
                </div>
                 <div class="col-md-2">
                    <a href="#" class="btn_new_cancel" data-dismiss="modal">Cancel</a>
                </div>
               
            </div>
            
            
         
            <input type="hidden" name="client_taxation_id" value="<?php if(isset($common->id)!=''){echo $common->id;}?>">
            <div class="row form-group">
                
                <div class="col-md-4 federals" style="display:none;">
                     
                </div>
                
                <div class="col-md-4 states" style="display:none;">
                     <select class="form-control statesyear" name="statesyear">
                        <option value="">Select</option>
                         <?php
                            $aa=date('Y')-1;
                            
                        ?>
                        <option value="<?php echo $aa;?>"><?php echo $aa;?></option>
                        
                        
                    </select>
                </div>
            </div>
             <div class="row form-group">
                <label class="col-md-3 control-label labels" style="display:none;">Tax Return</label>
                <div class="col-md-4 federals" style="display:none;">
                    
                </div>
                <div class="col-md-4 states" style="display:none;">
                    <select class="form-control taxation2" name="statestax">
                       <option value="">Select</option>
                        <option value="Extension">Extension </option>
                        <option value="Original">Original</option>
                        <option value="Amendment">Amendment</option>
                </select>
                </div>
            </div>
              <div class="row form-group">
                <label class="col-md-3 control-label labels"  style="display:none;">Form No.</label>
                <div class="col-md-4 federals" style="display:none;">
                   
                </div>
              
                <div class="col-md-4 states" style="display:none;">
                    <input type="text" class="form-control formno2" name="statesformno" readonly/>
                </div>
            </div>
              <div class="row form-group">
                <label class="col-md-3 control-label labels" style="display:none;">Due Date</label>
                <div class="col-md-4 federals" style="display:none;">
                   
                </div>
             
                <div class="col-md-4 states" style="display:none;">
                    <input type="text" class="form-control duedate2" name="statesduedate" placeholder="Mar-15-2020" readonly/>
                </div>
            </div>
            <div class="row form-group">
                <label class="col-md-3 control-label labels" style="display:none;">Filing Method </label>
                <div class="col-md-4 federals" style="display:none;">
                   
                </div>
               
                <div class="col-md-4 states" style="display:none;" >
                    <select class="form-control filingmethod2" name="statesmethod">
                        <option value="">Select</option>
                        <option value="E-File">E-File</option>
                        <option value="Paperfile">Paperfile</option>
                    </select>
                </div>
            </div>
            <div class="row form-group">
                <label class="col-md-3 control-label labels" style="display:none;">Filing Date</label>
                <div class="col-md-4 federals" style="display:none;">
                    
                </div>
              
                <div class="col-md-4 states" style="display:none;">
                    <input type="date" class="form-control" id="fillingdate2" name="statesdate"/>
                </div>
            </div>
             <div class="row form-group statusof">
                <label class="col-md-3 control-label labels" style="display:none;">Status of Filling </label>
                <div class="col-md-4 federals" style="display:none;" >
                    
                </div>
               
                <div class="col-md-4 states" style="display:none;">
                    <select class="form-control" name="statesstatus">
                        <option value="">Select</option>
                        <option class="hidestone" value="Accepted">Accepted</option>
                        <option class="hidestone" value="Rejected">Rejected</option>
                        <option class="hidestone" value="Pending">Pending</option>
                    </select>
                </div>
            </div>
             
              <div class="row form-group">
                <label class="col-md-3 control-label labels" style="display:none;">Note </label>
                <div class="col-md-4 federals" style="display:none;">
                 
                </div>
                
                <div class="col-md-4 states" style="display:none;">
                   <textarea class="form-control" name="statesnote"></textarea>
                </div>
            </div>
            <div class="row form-group saves" style="display:none;">
                <label class="col-md-5 control-label">&nbsp;</label>
                <div class="col-md-2">
                    <input type="submit" class="btn_new_save primary" value="Save">
                </div>
                 <div class="col-md-2">
                    <a href="#" class="btn_new_cancel" data-dismiss="modal">Cancel</a>
                </div>
               
            </div>
        </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<script>
/*$.ajaxSetup({
    headers:
    {'X-CSRF-Token': $('input[name="_token"]').val();}
});*/
</script>
<script>
$(document).ready(function() {
$('#sampleTable1').DataTable( {
        dom: 'Bfrtlip',
    "columnDefs": [ {
            "searchable": true,
            "orderable": true,
            "targets": 0
        } ],
        "order": [[ 0, 'asc' ]],
       buttons: [
                 {
                extend:    'copyHtml5',
                text:      '<i class="fa fa-files-o"></i> &nbsp; Copy',
                //titleAttr: 'Copy',
                title: $('h1').text(),
            },{
                extend:'excelHtml5',
                text:'<i class="fa fa-file-excel-o"></i>&nbsp; Excel',
               // titleAttr: 'Excel',
                title: $('h4').text(),
                 customize: function( xlsx ) {
                var sheet = xlsx.xl.worksheets['sheet1.xml'];
                        $('row c[r^="A"]',sheet).attr('s','51'); 
                       //   $('row c[r^="C"]',sheet).attr('s','51'); 
                          $('row:first c',sheet).attr('s','51');
            },
            exportOptions: {
                columns: [0,1,2,3,4,5,6], // Only name, email and role
                }
            },
            {
                extend:    'csvHtml5',
                text:      '<i class="fa fa-file-text-o"></i> &nbsp; CSV',
               // titleAttr: 'CSV',
                title: $('h1').text(),
            },
            {
           extend: 'print',
          text: '<i class="fa fa-print"></i>&nbsp; Print',
         customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '10pt' )
                        .prepend(
                            '<center><img src="https://financialservicecenter.net/public/business/{{$logo->logo}}"/></center>'
                        );
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );
                },
         exportOptions: {
          columns: [0,1,2,3,4,5,6]
      },
      footer: true,
      autoPrint: true
    },
        ],
    } );
});
</script>
<script>
    $(document).ready(function()
    {
        $(".clkbutton").click(function(){
        //alert();
            var attr=$(this).attr('attr');
           // alert(attr);
              var printContents = document.getElementById(attr).innerHTML;
        w=window.open();
        w.document.write(printContents);
        w.print();
        w.close();
        });
        function printDiv(divName) {
           // alert();
            
        var printContents = document.getElementById(divName).innerHTML;
        w=window.open();
        w.document.write(printContents);
        w.print();
        w.close();
    }
          $(".fdates").blur(function(){
          var fdatef=$('.fdatef').val();
             
          var thiss=$(this).val();
         // alert(thiss);
          if(thiss)
          {
              
          }
          else
          {
              $('.fdates').val(fdatef);
          }
             
         });

        $('.filingmethod').on('change', function()
        {
           var thiss=$(this).val();
           if(thiss =='Paperfile')
           {
               $('.hidest').hide();
           }
           else
           {
               $('.hidest').show();
           }
        });
        
        $('.filingmethodstate').on('change', function()
        {
           var thiss=$(this).val();
           if(thiss =='Paperfile')
           {
               $('.hidests').hide();
           }
           else
           {
               $('.hidests').show();
           }
        });
        
          $('#statetax').on('change', function()
        {
            var fdate=$('.fdate').val();
            $('.fdatess').val(fdate);
            
            var statetaxval=$("#statetax").val();
            $.get('{!!URL::to('getTaxstatedata')!!}?statetaxval='+statetaxval,function(data)
            {  
                //console.log(data);exit;
                if(data=='')
                {
                   $("#statefederalformno").val(); 
                }
                else
                {
                    $("#statefederalformno").val(data.taxform);
                }
                
            });
   
   });
      
        
        var cnt=1;
          
            $(".add-rowform").click(function()
            {
                cnt++;
                //$('').show();
                //var aa=$('.statetax2').html();
                var markup = ' <tr><td><select class="form-control statetax'+cnt+'" name="statetax[]" id="statetax'+cnt+'"><option>Select</option> @foreach($datastate2 as $datastate3)<option value="{{$datastate3->code}}">{{$datastate3->code}}</option>@endforeach</select></td><td><input type="text" readonly class="form-control formno" name="stateformno[]" id="statefederalformno'+cnt+'" style="width:100px"/></td><td><select class="form-control filingmethod" name="statemethod[]"><option value="">Select</option><option value="E-File">E-File</option><option value="Paperfile">Paperfile</option></select></td><td><input type="text" class="form-control fdatess'+cnt+'" id="fillingdate" name="statedate[]" id="federaldate'+cnt+'" placeholder="MM/DD/YYYY"/></td><td><select class="form-control" name="statestatus[]" id="federalstatus'+cnt+'"><option value="">Select</option> <option value="Accepted">Accepted</option><option value="EF Processing">EF Processing</option><option value="Pending">Pending</option><option value="Rejected">Rejected</option></select></td><td><textarea class="form-control" name="statenote[]" id="federalnote'+cnt+'"></textarea></td><td><a href="#" class="btn btn-danger removetrrow"><i class="fa fa-minus"></i></a></td></tr>';
                $("#taxationtable tbody").append(markup);
            
                $( function()
                {
                    $('.fdate'+cnt).datepicker({
                        autoclose: true
                    });
                });   
        
                $('.statetax'+cnt).on('change', function()
                {
                    var fdate=$('.fdate').val();
                    $('.fdatess'+cnt).val(fdate);
                        var statetaxval=$('.statetax'+cnt).val();
                       // alert(statetaxval);
                        $.get('{!!URL::to('getTaxstatedata')!!}?statetaxval='+statetaxval,function(data)
                        {  
                            if(data=='')
                            {
                               $('#statefederalformno'+cnt).val(); 
                            }
                            else
                            {
                                $('#statefederalformno'+cnt).val(data.taxform);
                            }
                            
                        });
               });
           
            });
            $("body").on("click",".removetrrow",function(){ 
                $(this).parent().parent().remove();
    });        
        
    });
    
</script>
<script>
 $( function()
    {
        $('.fdate').datepicker({
            autoclose: true
        });
    });
$(document).on('change','#status', function()
   {
          var thiss = $(this).val();
       //  alert(thiss);
          if(thiss =='0')
          {
            //  $('.222').hide();
              
              $('.showfill').show();
              $('.showfed').show();
              
          }
          else
          {
              //$('.222').show();
              
              $('.showfed').hide();
              
              $('.showfill').hide();
          }
 
   });
   
   
$(document).on('change','#formation_status', function()
   {
          var thiss = $(this).val();
         // alert(thiss);
       if(thiss =='Taxation')
       {
           $('.showtax').show();
           $('.222').hide();
           $('.2221').hide();
           
           $('.111').show();
           
           
       }
       else
       {
           $('.2221').show();
           
           $('.111').hide();
           $('.222').show();
           $('.showtax').hide();
       }
       if(thiss =='Corporation')
       {
           /*   $.get('{!!URL::to('getStateReport')!!}', function(data)
            {  
                //console.log(data);exit;
                if(data == "")
                {
                    
                }
                else
                {
                    
                }
                

                
            });
    */
       }
          if(thiss =='Taxation')
          {
              $('.taxationpersonalcategory').show();
          }
          else
          {
              $('.taxationpersonalcategory').hide();
          }
 
   });
   
    //$('.111').hide();
    
    $(document).on('change','#taxation_type', function()
   {
       var rthis=$(this).val();
       if(rthis =='6')
       {
           $('.taxationpersonalcategory').hide();
           
            $('.taxpersonalcategory').hide();
          
       }
       else
       {
           $('.taxationpersonalcategory').show();
            $('.taxpersonalcategory').show();
          
           
       }
   });
   $(document).on('change','#tax_subcategory', function()
   {
          var thiss = $(this).val();
       //  alert(thiss);
      // var aa=$("#status option:selected", this).attr("rel");
    //  alert(aa);
          if(thiss =='Personal')
          {
             $('.111').show();
              $('.222').hide();
              $('.taxpersonalcategory').show();
          }
          else
          {
              $('.111').hide();
              $('.222').show();
              $('.taxpersonalcategory').hide();
          }
 
   });
     $('.taxation').change(function()
        {
              var businessid='<?php if(isset($_REQUEST['id']) && $_REQUEST['id'] !='') { echo $common->business_id; }?>';
            
          var thiss=$(this).val();
          if(thiss == 'Extension')
          {
              if(businessid =='6')
              {
                  $('.formno').val('Form-4868');
              }
              else
              {
              $('.formno').val('Form-7004');    
              }
              
              $("select option[value='E-File']").show();
          }
          
          else if(thiss == 'Original')
          {
              $('.formno').val('Form-1040');
              $("select option[value='E-File']").show();
          }
          else if(thiss == 'Amendment')
          {
              $('.formno').val('Form-1040X');
              $("select option[value='E-File']").hide();
                     
          }
        });
      
 $(".federalTaxation").click(function ()
        {
            var federalid = $(this).attr('data-id');
          //  alert(federalid);
            $("#federalid").val(federalid);
            $('#federalModals').modal('show');
            $.get('{!!URL::to('getClientfederaldata2')!!}?federalid='+federalid, function(data)
            {  
                //console.log(data);exit;
                if(data == "")
                {
                    
                }
                else
                {
                    //alert(data.filename);
                    //$('#id').val(data.id);
                    var fullnames=data.first_name +' '+data.last_name;
                  $('#federalsyear').val(data.taxyears);
                    $('#clid').html(data.filename);
                    $('.clname').val(fullnames);
                    
                    
                   // $('#federalstax').val(data.federalstax);
                    //$('#federalsduedate').val(data.federalsduedate);
                    //$('#federalsduedate').val(date('d-m-Y',strtotime(data.federalsduedate)));
                    // $('#federalsform').val(data.federalsform);
                    // $('#federalsmethod').val(data.federalsmethod);
                    // $('.fillingdate').val(data.federalsdate);
                    // $('#federalsstatus').val(data.federalsstatus);
                    // $('#federalsfile').val(data.federalsfile);
                    // $('#federalsfile_1').html(data.federalsfile);
                    // $('#federalsnote').val(data.federalsnote);
                    
                    
                    var this1=$('#federalsyear').val();
                    var aa='<?php echo $aa=date('Y')-1;?>';
             
                    if(this1 == aa)
                    {
                        $(".duedate").val('Jul-15-<?php echo $aa+1;?>');
                 
                    }
                    
                }
                

                
            });
        });
       
 $(document).on('change','.category', function()
   {
  
    var id = $(this).val();
   $.get('{!!URL::to('getRequest')!!}?id='+id, function(data)
   {  
      
       //alert();
   if(data == "")
   {
   $('#business_catagory_name_2').hide();
   $('#business_catagory_name_4').hide();
   $('#business_catagory_name_3').hide();
   }
   else
   {
   $('#business_catagory_name_2').show();
   }
   $('#business_catagory_name').empty();
   $('#business_catagory_name_4').hide();
   $('#business_catagory_name_3').hide();
   $('#business_catagory_name').append('<option value="">---Select---</option>');
   $('#business_brand_category_name').append('<option value="">---Select---</option>');
   $('#business_brand_name').append('<option value="">---Select---</option>');
   $.each(data, function(index, subcatobj)
   {
   $('#business_catagory_name').append('<option value="'+subcatobj.id+'">'+subcatobj.business_cat_name+'</option>');
   //	$('#user_type').val('subcatobj.business_cat_name');
   })
   });
   });
   </script>
   <script>
    function Refresh() {
        window.parent.location = window.parent.location.href;
    }
</script>
<style>
.table > thead > tr > th {background: #98c4f2;text-align: center;}
td{text-align: center !important;}
.form-group {margin-bottom: 15px;width: 100%;float: left;}
   </style>
@endsection