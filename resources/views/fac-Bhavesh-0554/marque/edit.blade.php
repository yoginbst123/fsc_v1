@extends('fac-Bhavesh-0554.layouts.app')

@section('main-content')
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>Marque</h1>
    </section>
    <!-- Main content -->
    <section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-success">
			      <div class="box-header">
              
              <div class="box-tools pull-right">
                
              </div>
            </div>
				<div class="col-md-12">
             <form method="post" action="{{route('marque.update',$branch->id)}}" class="form-horizontal" id="businessname" name="businessname" enctype="multipart/form-data">
					{{csrf_field()}}{{method_field('PATCH')}}

						<div class="form-group {{ $errors->has('marque') ? ' has-error' : '' }}">
							<label class="control-label col-md-3">Name :</label>
							<div class="col-lg-6 col-md-9">
								<input name="marque" type="text" id="marque" class="form-control" value="{{$branch->marque}}" />                                                            @if ($errors->has('marque'))
										<span class="help-block">
										<strong>{{ $errors->first('marque') }}</strong>
										</span>
									@endif
							</div>
						</div>
						
						
						<div class="card-footer">
						    <div class="col-md-3"></div>
						    <div class="col-xs-2" style="width:155px;">
								<input class="btn_new_save btn-primary1 primary1" style="margin-left:-5%" type="button" id="primary1" name="submit" value="Save">
							</div>
							<div class="col-xs-2" style="width:155px;">
								<a class="btn_new_cancel" style="margin-left:-15%" href="{{url('fac-Bhavesh-0554/marque')}}">Cancel</a> 
							</div>
						</div>
						
					</form>
				</div>
			</div>
		</div>
	</div>
	</section>
<!--</div>-->
@endsection()