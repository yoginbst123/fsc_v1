@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>Home Content</h1>
    </section>
    <!-- Main content -->
    <section class="content">
<div class="row">
		<div class="col-md-12">
			<div class="box box-success">
			      <div class="box-header">
       
              <div class="box-tools pull-right">
                
              </div>
            </div>
				<div class="col-md-12">
                   
					<form method="post" action="{{route('homecontent.update',$homecontent->id)}}" class="form-horizontal" id="homecontent" name="homecontent" enctype="multipart/form-data">
					{{csrf_field()}}{{method_field('PATCH')}}
						<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
							<label class="control-label col-md-3">Title :</label>
							<div class="col-md-8">
								<input name="title" type="text" value="{{$homecontent->title}}" id="title" class="form-control"/>								
								@if ($errors->has('title'))
										<span class="help-block">
											<strong>{{ $errors->first('title') }}</strong>
										</span>
									@endif
							</div>
						</div>						
						<div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
						<label class="control-label col-md-3">Content :</label>
						<div class="col-md-8">
						
							  <textarea id="editor1" name="content" rows="10" cols="80">{!!$homecontent->content!!}</textarea>
					  
							@if ($errors->has('content'))
									<span class="help-block">
										<strong>{{ $errors->first('content') }}</strong>
									</span>
								@endif	
						</div>
					</div>
						<div class="card-footer">
						<div class="row">
						    <div class="col-md-3"></div>
						   <div class="col-xs-2" style="width:155px;">
								<input class="btn_new_save btn-primary1 primary1" type="button" id="primary1" name="submit" value="Save">
							</div>
							<div class="col-xs-2" style="width:155px;">
								<a class="btn_new_cancel" href="{{url('fac-Bhavesh-0554/homecontent')}}">Cancel</a> 
							</div>
						</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	    </section>
<!--</div>-->

@endsection()