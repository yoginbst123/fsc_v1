@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
<style>
label{float:right}
.dd {
    border:transparent;

}
.ui-timepicker-container{ z-index:999999 !important}
#sch_pay_date,#sch_start_date,#sch_end_date,#schedule_in_time,#schedule_out_time{ text-align:center;}
</style>
<div class="content-wrapper">
     <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>Add Schedule Formula Setup</h1>
    </section>
    <!-- Main content -->
    <section class="content">
  
   <div class="row">
      <div class="col-md-12">
         	<div class="box box-success">
			      <div class="box-header">
             
              <div class="box-tools pull-right">
              </div>
            </div>
				<div class="col-md-12">
               <form method="post" action="{{route('schedulesetup.store')}}" class="form-horizontal" id="content" name="content" enctype="multipart/form-data">
                  {{csrf_field()}}   
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('duration') ? ' has-error' : '' }}" style="margin-top: 1%;">
                        <label class="control-label col-md-3">Duration : <span class="star-required">*</span></label>
<div class="col-lg-7 col-md-8 col-sm-12 col-xs-12 fsc-element-margin">
                                       <div class="row">
                     <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                        <select  class="ttt form-control fsc-input" name='duration'  id="duration">
                            <option value="">---Select Duration---</option>
                            
                            <option value="Weekly">Weekly</option>
                            <option value="Bi-Weekly">Bi-Weekly</option>
                            <option value="Semi-Monthly">Semi-Monthly</option>
                            <option value="Monthly">Monthly</option>
                        </select>
                       
                     </div>
                   
                  </div>
                  </div>
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('sch_start_date') ? ' has-error' : '' }}" style="margin-top: 1%;">
                        <label class="control-label col-md-3">Schedule Start Date / Day: <span class="star-required">*</span></label>
<div class="col-lg-7 col-md-8 col-sm-12 col-xs-12 fsc-element-margin">
                                       <div class="row">
                     <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                      <input type="text" class="form-control txtOnly fsc-input startdate"  id='sch_start_date' name='sch_start_date' placeholder="Start Date">
                        @if ($errors->has('sch_start_date'))
                        <span class="help-block">
                        <strong>{{ $errors->first('sch_start_date') }}</strong>
                        </span>
                        @endif
                     </div>
                      <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                      <select  class="ttt form-control fsc-input" name='sch_start_day'  id="sch_start_day">
                        <option value="">---Select---</option>
                       <option value="Sunday">Sunday</option>
                        <option value="Monday">Monday</option>
                        <option value="Tuesday">Tuesday</option>
                        <option value="Wednesday">Wednesday</option>
                        <option value="Thursday">Thursday</option>
                        <option value="Friday">Friday</option>
                        <option value="Saturday">Saturday</option>
                     </select>
                       
                     </div>
                  </div>
             </div>
                  </div>
 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('sch_end_date') ? ' has-error' : '' }}" style="margin-top: 1%;">
                        <label class="control-label col-md-3">Schedule End Date  / Day : <span class="star-required">&nbsp;&nbsp;</span></label>
<div class="col-lg-7 col-md-8 col-sm-12 col-xs-12 fsc-element-margin">
                                       <div class="row">
                     <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                      <input type="text" class="form-control txtOnly fsc-input" readonly id="sch_end_date"  name='sch_end_date' placeholder="End Date">
                        @if ($errors->has('sch_end_date'))
                        <span class="help-block">
                        <strong>{{ $errors->first('sch_end_date') }}</strong>
                        </span>
                        @endif
                     </div>
                     <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                      <select  class="ttt form-control fsc-input" name='sch_end_day'  id="sch_end_day">
                        <option value="">---Select---</option>
                       <option value="Sunday">Sunday</option>
                        <option value="Monday">Monday</option>
                        <option value="Tuesday">Tuesday</option>
                        <option value="Wednesday">Wednesday</option>
                        <option value="Thursday">Thursday</option>
                        <option value="Friday">Friday</option>
                        <option value="Saturday">Saturday</option>
                     </select>
                       
                     </div>
                  </div>
</div>
                  </div>
                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('sch_pay_date') ? ' has-error' : '' }}" style="margin-top: 1%;">
                        <label class="control-label col-md-3">Pay Date  / Day : <span class="star-required">&nbsp;&nbsp;</span></label>
<div class="col-lg-7 col-md-8 col-sm-12 col-xs-12 fsc-element-margin">
                                       <div class="row">
                     <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                      <input type="text" class="form-control txtOnly fsc-input"  id="sch_pay_date"  name='sch_pay_date' placeholder="Pay Date">
                        @if ($errors->has('sch_pay_date'))
                        <span class="help-block">
                        <strong>{{ $errors->first('sch_pay_date') }}</strong>
                        </span>
                        @endif
                     </div>
                     <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                      <select  class="ttt form-control fsc-input" name='sch_pay_day'  id="sch_pay_day">
                        <option value="">---Select---</option>
                       <option value="Sunday">Sunday</option>
                        <option value="Monday">Monday</option>
                        <option value="Tuesday">Tuesday</option>
                        <option value="Wednesday">Wednesday</option>
                        <option value="Thursday">Thursday</option>
                        <option value="Friday">Friday</option>
                        <option value="Saturday">Saturday</option>
                     </select>
                       
                     </div>
                  </div>
</div>
                  </div>
                  <div class="field_wrapper">
                      <div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 1%;">
                        <label class="control-label col-md-3">Note : <span class="star-required">&nbsp;&nbsp;</span></label>
<div class="col-lg-7 col-md-8 col-sm-12 col-xs-12 fsc-element-margin">
                                       <div class="row">
                     <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                      <input type="text" class="form-control txtOnly fsc-input"  id="note"  name='note[]' placeholder="Note">
                      <input type="hidden" class="form-control txtOnly fsc-input"  id="noteid"  name="noteid[]" placeholder="Note">
                     </div>
                  </div>
</div>
                  </div>
                   
                    </div>
                    
                    </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 1%;">
                  
                 
                    <div class="card-footer">
                        <div class="col-md-3"></div>
					    <div class="col-md-9">
					        <div class="row">
					            <div class="col-xs-2" style="width:140px;">
									<input class="btn_new_save btn-primary1" type="submit" name="submit" value="Save">
								</div>
								<div class="col-xs-2" style="width:140px;">
									<a class="btn_new_cancel" href="{{url('fac-Bhavesh-0554/schedule')}}">Cancel</a> 
								</div>
								<div class="col-xs-2" style="width:140px;">
								    <a href="javascript:void(0);" class="add_button btn_new_cancel" title="Add field">Note</a>
								</div>
    						</div>
						</div>
				    </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                     <div class="" id="Register"></div>
                  </div>
              
            </div> </form>
         </div>
      </div>
   </div>
</div> </section>
</div>
</div>
</div>
   
<script type="text/javascript">
$( "#schedule_in_time" ).timepicker();
$( "#schedule_out_time" ).timepicker();
</script>
<script>
   $(document).ready(function(){
   	$(document).on('change','.category', function()
   	{
        var id = $(this).val();
        $.get('{!!URL::to('getschedule')!!}?id='+id, function(data)
   	{            // $('#emp_name').append('<option value="">---Select Employee---</option>');
   	
           $('#emp_name').empty();
           $('#emp_name').append('<option value="">---Select Employee---</option>');
           $.each(data, function(index, subcatobj)
             {
  		$('#emp_name').append('<option value="'+subcatobj.id+'">'+subcatobj.firstName+'</option>');
   	   })
           });   			
   	});
   });
</script>

  <script type="text/javascript">
  $(document).ready(function() {
    $("#sch_start_date").change(function() {
      var startdate= $("#sch_start_date").val();
      var monthNames = [
        "Jan", "Feb", "Mar",
        "Apr", "May", "Jun", "Jul",
        "Aug", "Sep", "Oct",
        "Nov", "Dec"
      ];
    var weekday = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
      var durtion=$('#duration').val();
      var  date = new Date(startdate);//alert(date);
      var day = weekday[date.getDay()];
      var monthly=30;
      var weekly=7;
      var bimonthly=15;
      var biweekly=14;
      var monthss=monthNames[(date.getMonth())];
      var yearss = date.getFullYear();
      var yyy=yearss % 4 ;
      if(yyy)
      {
   // alert(yyy);
      }
      else
      {
    //  alert('false');
      }
      if(durtion == "Weekly")
      {
        var totaldays=6;
       var totalday1=4;
      }
      else if(durtion == "Monthly")
      { 
        if(monthss == 'Jan' || monthss == 'Mar' || monthss == 'May' || monthss == 'Jul' || monthss == 'Aug' || monthss == 'Oct' || monthss == 'Dec')
        {
          var totaldays=30;
          var totalday1=5;
        }
        else if(monthss == 'Feb')
        {  
            var totaldays=27;//alert();
            var totalday1=5;
          
        }
        else if(monthss == 'Apr' || monthss == 'Jun' || monthss == 'Sep' || monthss == 'Nov' )
        {
          var totaldays=29;
          var totalday1=5;
        }
      }
      else if(durtion == "Bi-Weekly")
      {
        var totaldays=13;
        var totalday1=4;
      }
      else if(durtion == "Semi-Monthly")
      {
        var totaldays=14;
       var totalday1=4;
      }
     var vad = date.setDate(date.getDate() + totaldays);
    
      var date1 = ("0" + (date.getMonth() + 1)).slice(-2)  + "/" + ("0" + date.getDate()).slice(-2) + "/" + date.getFullYear();
      var vad1 = date.setDate(date.getDate() + totalday1);
    // alert(vad1);
     var date3 = ("0" + (date.getMonth() + 1)).slice(-2)  + "/" + ("0" + (date.getDate())).slice(-2) + "/" + date.getFullYear();
     // alert(date3);
      var newdate = new Date(date1);
        var day1 = weekday[newdate.getDay()];
        var newdate1 = new Date(date3);
        var day2 = weekday[newdate1.getDay()];
       //alert(newdate);
      var date2=monthNames[(date.getMonth())] + "/" + date.getDate() + "/" + date.getFullYear() ;
      $('#sch_end_date').val(date1);
      $('#sch_pay_date').val(date3);
       $('#sch_pay_day').val(day2);
      $('#sch_start_day').val(day);
      $('#sch_end_day').val(day1);
      //document.write(date2);
    });
    
    $("#duration").change(function() {
      $('#sch_end_date').val('');
      $('#sch_start_date').val('');
       $('#sch_pay_date').val('');
    });
  });
</script>
<script>
   $(document).ready(function(){
   	$(document).on('change','.emp', function()
   	{
   		//console.log('htm');
   		var id = $(this).val();//alert(id);

   		$.get('{!!URL::to('getduration')!!}?id='+id, function(data)
   		{  
               $('#duration').empty();
    
              $.each(data, function(index, subcatobj)
   		   {        
                    $('#duration').val(subcatobj.pay_frequency);
                          
   		   })
   
   		});
   			
   	});
   });
</script>

<script>
  $(document).ready(function(){ 

$("#sch_start_date").datepicker({
format: "mm/dd/yyyy"});

  
});
  $(document).ready(function(){ 
$("#sch_pay_date").datepicker({
format: "mm/dd/yyyy"});
});

</script>
<script type="text/javascript">
$(document).ready(function(){
    var maxField = 10; //Input fields increment limitation
    var addButton = $('.add_button'); //Add button selector
    var wrapper = $('.field_wrapper'); //Input field wrapper
    var fieldHTML = '<div class="dd"><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 1%;"><div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row"><label class="fsc-form-label">Note : <span class="star-required">&nbsp;&nbsp;</span></label></div><div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin"><div class="row"><div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin"><input type="hidden" class="form-control txtOnly fsc-input"  id="noteid"  name="noteid[]" placeholder="Note"><input type="text" class="form-control txtOnly fsc-input"  id="note"  name="note[]" placeholder="Note"></div><div class="col-md-3"></div></div></div></div><a href="javascript:void(0);" class="remove_button btn btn-danger"><i class="fa fa-trash"></i></a></div>'; //New input field html 
    var x = 1; //Initial field counter is 1
    
    //Once add button is clicked
    $(addButton).click(function(){
        //Check maximum number of input fields
        if(x < maxField){ 
            x++; //Increment field counter
            $(wrapper).append(fieldHTML); //Add field html
        }
    });
    
    //Once remove button is clicked
    $(wrapper).on('click', '.remove_button', function(e){ //alert();
        e.preventDefault();
        $(this).parent('.dd').remove(); //Remove field html
        x--; //Decrement field counter
    });
});
</script>
<style>
.dd {

    border: transparent;
    position: relative;
    top: -13px;
    width: 100%;
    float: left;
    height: 54px;

}
    .remove_button{float: left;position: relative;top: -37px;right: -56%;}
</style>

@endsection()

