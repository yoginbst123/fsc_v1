@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content') 
<style>
.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{padding:5px 8px !important;vertical-align:inherit !important;}
.headerclr{background:#ffff99 !important;}
.table > thead > tr > th{background:none !important;}
.addagemodal .form-control:focus, .addagemodal .form-control:hover{background:#ffff99;}
span.required
{
    color:red;
}
.dt-buttons {
    margin-bottom: 60px;
}
.countboxmian{}
/*.table > thead > tr > th{padding:8px!important;}*/
/*.subtitle{height: 25px;  max-width: 600px;  margin: 0px auto 15px; width: 100%;*/
/*    padding-right: 0!important;*/
/*    position: relative; max-width:inherit!important; */
/*    left:0%;*/
/*}*/
.subtitle{margin: 15px auto 0px; border: 2px solid #a5a5a5;
    width: 100%;
    padding-right: 0!important;
    position: relative;
    max-width: inherit!important;
    left: 0%;
    display: flex;
    height: 35px;
    margin-bottom: -85px;
    position: unset;
    margin-top: -10px;
    flex-direction: row;
    /*justify-content: center;*/
    background: #e2e2e2;
    padding: 5px 0px;}
.subtitle span{font-size:16px; color:#000;}
.floatleft{float:left;}
.floatright{float:right;}
.form-group{margin-left:0px!important; margin-right:0px!important;}
.clear{clear:both;}
.buttonarea{position:absolute; bottom:-48px; right:0px;}
.buttonarea a{font-size:16px;}

.reporttable tr td, .reporttable tr th{white-space:nowrap; padding:5px!important; text-align:left!important; border:1px solid #ccc!important;}
div.dataTables_wrapper div.dataTables_length select.form-control.input-sm{padding:0px!important;}
#example_wrapper .table tr td{padding:5px!important;}
/* Fixed Headers */

.dataTables_wrapper  .row:first-child{
    position:absolute; bottom:0px; z-index:99999;
}
@media print {
   .close, .btn{display:none; font-size:0px;}
  body * {
    visibility:hidden;
  }
  #printSection, #printSection * {
    visibility:visible;
  }
  #printSection {
    position:absolute;
    left:0;
    top:0;
  }
}

/*#example_info{margin-left:94%;}*/
form.form-horizontal {
    font-size: 13px;
}
.form-control, .form-control-insu {
    padding: 6px 1px!important;
    font-size: 14.5px !important;
}
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h2> FSC Client Report</h2>
    </section>
    <section class="content">
   <div class="">
      <div class="col-md-12" style="padding:0px;">
        <div class="box box-success">
			      <div class="box-header">
              <div class="box-tools pull-right">
              </div>
            </div>
				<div class="col-md-12">
               <form  class="form-horizontal" actio="{{route('fscclientreport.index')}}"   method="post">
                  {{csrf_field()}}
                  <div class="row">
                     <div class="col-md-12 col-sm-12 col-xs-12" style="padding-right:0px;">
                         <div class="col-xs-2 fscw_client cr_1" style="padding-left:0px;">
                             <div class="form-group">
                                 <label for="focusedinput" class="control-label">Client : <span class="required"> * </span></label>
                                  <select class="form-control" name="status" id="status">
                                    <option value="Active" <?php if(isset($_POST['status']) && $_POST['status']  =='Active') { echo 'selected';}?>>Active</option>
                                    <option value="1" <?php if(isset($_POST['status']) && $_POST['status'] =='1') { echo 'selected';}?>>All</option>
                                    <option value="Inactive" <?php if(isset($_POST['status']) && $_POST['status']  =='Inactive') { echo 'selected';}?>>Inactive</option>
                                 </select>
                            </div>
                         </div>
                         <div class="col-xs-2 fscc_type_business cr_2" style="padding-left:0px;">
                             <div class="form-group">
                                 <label for="focusedinput" class="control-label">Type of Business: </label>
                                    <select name="business_id" id="business_id" class="form-control fsc-input category">
                                        <option value="">Select</option>
                                        <option value="all" <?php if(isset($_POST['business_id']) && $_POST['business_id'] =='all') { echo 'selected';}?>>All</option>
                                        @foreach($business as $cate)
                                            <option value='{{$cate->id}}' <?php if(isset($_POST['business_id']) && $_POST['business_id']  ==$cate->id) { echo 'selected';}?>>{{$cate->bussiness_name}}</option>
                                        @endforeach
                                    </select>
                                <input class=" form-control fsc-input" type="hidden" value='' name="" id="user_type">
                            </div>
                         </div>
                         <div class="col-xs-3 type_of_service cr_3 fscc_type_ser" <?php if(isset($_POST['business_id']) && $_POST['business_id'] ==''){?> style="padding-left:0px;display:none;"<?php } ?> style="padding-left:0px;">
                             <div class="form-group">
                                 <label for="focusedinput" class="control-label">Type of Services: </label>
                                    <select name="type_of_service" id="type_of_service" class="form-control fsc-input servicetype">
                                        <option value="">Select</option>
                                        <option value="Accounting Service" <?php if(isset($_POST['type_of_service']) && $_POST['type_of_service'] =='Accounting Service') { echo 'selected';}?>>Accounting Service</option>
                                        <option value="Payroll Service" <?php if(isset($_POST['type_of_service']) && $_POST['type_of_service']  =='Payroll Service') { echo 'selected';}?>>Payroll Service</option>
                                        @foreach($taxtitles as $cate)
                                            <option value='{{$cate->id}}' <?php if(isset($_POST['type_of_service']) && $_POST['type_of_service'] ==$cate->id) { echo 'selected';}?>>{{$cate->title}}</option>
                                        @endforeach
                                    </select>
                                <input class=" form-control fsc-input" type="hidden" value='' name="" id="user_type">
                            </div>
                         </div>
                         <?php //echo $_POST['acperiod'];?>
                         <div class="col-xs-2 payroll_period cr_4" style="padding-left:0px;display:none;">
                            <div class="form-group">
                                <label for="focusedinput" class="control-label">Payroll Period: </label>
                                <select class="form-control" name="">
                                    <option value="">Select</option>
                                    <option value="Regular-8" <?php if(isset($_POST['acperiod']) && $_POST['acperiod']=='Regular-8') { echo 'selected';}?>>Regular With Accounting</option>
                                    <option value="Full-8" <?php if(isset($_POST['acperiod']) && $_POST['acperiod'] =='Full-8') { echo 'selected';}?>>Full Payroll Service</option>
                                    <option value="Limited-8" <?php if(isset($_POST['acperiod']) && $_POST['acperiod'] =='Limited-8') { echo 'selected';}?>>Limited Payroll Service</option>
                                    <option value="Paystubs-8" <?php if(isset($_POST['acperiod']) && $_POST['acperiod'] =='Paystubs-8') { echo 'selected';}?>>Paystubs Only</option>
                                    <option value="None-8" <?php if(isset($_POST['acperiod']) && $_POST['acperiod'] =='None-8') { echo 'selected';}?>>None</option>
                               </select>
                            </div>
                        </div>
                        <div class="col-xs-3 showpersonal fscc_type_ser cr_5" style="padding-left:0px;display:none;">
                            <div class="form-group">
                                <label for="focusedinput" class="control-label">Type of Service: </label>
                                <select name="personaltype" id="personaltype" class="form-control fsc-input hideyears personaltype">
                                    <option value="">Select</option>
                                    @foreach($taxtitles1 as $cate)
                                        <option value='{{$cate->id}}' <?php if(isset($_POST['personaltype']) && $_POST['personaltype'] ==$cate->id){ echo 'selected';}?> >{{$cate->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div style="display:none; padding-left:0px!important;" id="business_catagory_name_2" class="col-xs-3 fscc_busin_cate cr_6">
                            <label for="focusedinput" class="control-label" >Business Category: </label>
                            <select name="business_catagory_name" id="business_catagory_name" class="form-control fsc-input category1">
                                <option value=''>---Select Business Category Name---</option>
                                @foreach($category as $cate)
                                <option value='{{$cate->id}}' <?php if(isset($_POST['business_catagory_name']) && $_POST['business_catagory_name']  ==$cate->id) { echo 'selected';}?>>{{$cate->business_cat_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-xs-2 fscc_acc_per cr_7 accounting_period" <?php if(isset($_POST['acperiod']) && $_POST['acperiod'] ==''){ ?>style="padding-left:0px;display:none;"<?php } ?>  style="padding-left:0px;">
                            <div class="form-group">
                                <label for="focusedinput" class="control-label">Accounting Period:<?php //print_r($_POST);?> </label>
                                <select name="acperiod" id="" class="form-control fsc-input">
                                    <option value="">Select</option>
                                    <option value="10-2" <?php if(isset($_POST['acperiod']) && $_POST['acperiod'] =='10-2') { echo 'selected';}?>>Monthly</option>
                                    <option value="4-2" <?php if(isset($_POST['acperiod']) && $_POST['acperiod'] =='4-2') { echo 'selected';}?>>Quarterly</option>
                                    <option value="5-2" <?php if(isset($_POST['acperiod']) && $_POST['acperiod'] =='5-2') { echo 'selected';}?>>Half-Yearly</option>
                                    <option value="7-2" <?php if(isset($_POST['acperiod']) && $_POST['acperiod'] =='7-2') { echo 'selected';}?>>Yearly</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-2 fscc_acc_sw accsoftware cr_8" style="display:none;padding-left:0px;"> 
                            <div class="form-group">
                                <label for="focusedinput" class="control-label">Accounting Software:<?php //print_r($_POST);?> </label>
                                <select class="form-control" name="accounting_software">
                                    <option value="">Select</option>
                                    <option value="Sage">Sage</option>
                                    <option value="Quickbook">Quickbook</option> 
                                    <option value="Sage+Quickbook">Sage+Quickbook</option>   
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-2 fscc_acc_loc acclocation cr_9" style="display:none; padding-left:0px;">
                            <div class="form-group">
                                <label for="focusedinput" class="control-label">Accounting Location:<?php //print_r($_POST);?> </label>
                                <select class="form-control" name="accounting_location">
                                    <option value="">Select</option>
                                    <option value="On the Cloud">On the Cloud</option>
                                    <option value="FSC - Server">FSC - Server</option>
                                    <option value="Client Location">Client Location</option>   
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-2 fscc_period cr_10 periodhide" <?php if(isset($_POST['personalyear']) && $_POST['personalyear'] !=''){?>style="padding-left:0px;display:none;"<?php } ?> style="padding-left:0px;">
                            <label for="focusedinput" class="control-label">Period : </label>
                            <select class="form-control" name="monthlytype" id="monthlytype">
                                <option value="">Select</option>
                                @foreach($period as $period1)
                                <option value="{{$period1->id}}" <?php if(isset($_POST['monthlytype']) && $_POST['monthlytype'] ==$period1->id) { echo 'selected';}?>>{{$period1->period}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-xs-1 showyears cr_11 fscc_year" style="padding-left:0px;" <?php if(isset($_POST['personalyear']) ==''){?>style="padding-left:0px;display:none;"<?php } ?>>
                            <div class="form-group">
                                <label for="focusedinput" class="control-label">Year: </label>
                                <select name="personalyear" id="personalyear" class="form-control fsc-input">
                                    <option value="">Select</option>
                                    <option value="2021" <?php if(isset($_POST['personalyear']) && $_POST['personalyear'] =='2021') { echo 'selected';}?>>2021</option>
                                    <option value="2020" <?php if(isset($_POST['personalyear']) && $_POST['personalyear'] =='2020') { echo 'selected';}?>>2020</option>
                                    <option value="2019" <?php if(isset($_POST['personalyear']) && $_POST['personalyear'] =='2019') { echo 'selected';}?>>2019</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-2 fscc_btn cr_12" style="padding:0px;">
                            <button class="btn-success btn" type="submit" name="search" style="width:70px; margin-top:27px;"> Ok</button>
                            <button class="btn-danger btn" type="button" name="search" style="width:60px; margin-top:27px;"onClick="Refresh();"> Reset</button>
                        </div>
                           <!--<div class="form-group {{ $errors->has('business_catagory_name') ? ' has-error' : '' }}" style="display:none" id="business_catagory_name_2">-->
                            <!--           <label class="control-label col-md-3">Client Category Name : <span class="star-required">*</span></label>-->
                            <!--           <div class="col-md-6">-->
                            <!--              <div class="row">-->
                            <!--                 <div class="col-md-8">-->
                            <!--                    <select name="business_catagory_name" id="business_catagory_name" class="form-control fsc-input category1">-->
                            <!--                       <option value=''>---Select Business Category Name---</option>-->
                            <!--                       @foreach($category as $cates)-->
                            <!--                       <option value='{{$cates->id}}'>{{$cates->business_cat_name}}</option>-->
                            <!--                       @endforeach-->
                            <!--                    </select>-->
                            <!--                 </div>-->
                            <!--                 <div class="col-md-3 pull-right">-->
                            <!--                    <div id="image1"></div>-->
                            <!--                 </div>-->
                            <!--              </div>-->
                            <!--           </div>-->
                            <!--        </div>-->
                        
                        
                     </div>
                  </div>
                  </form>
                  
                  </div>
                  <div class="clear"></div>
                  <div style="border-top:1px solid #ccc; height:2px; width:100%; margin:10px 0px 10px; display:block;"></div>
                  
                  <div style="width:100%; overflow:auto; border:0px solid #000;" class="table-responsive">
<!--<table cellspacing="0" cellpadding="0" class="reporttable">
<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td bgcolor="#ccffff">DJ</td>
    <td colspan="2" bgcolor="#ffcc99">RP</td>
    <td bgcolor="#99ccff">CJ</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2" bgcolor="#ffff99" style="font-size:25px; font-weight:bold">WRITE UP SERVICE STATUS</td>
    <td bgcolor="#ffff99"> </td>
    <td bgcolor="#ffff99">&nbsp;</td>
    <td bgcolor="#ffff99">&nbsp;</td>
    <td bgcolor="#ffff99">&nbsp;</td>
    <td bgcolor="#ffff99">&nbsp;</td>
    <td bgcolor="#ffff99">&nbsp;</td>
    <td bgcolor="#ffff99">&nbsp;</td>
    <td bgcolor="#ffff99">&nbsp;</td>
    <td bgcolor="#ffff99">&nbsp;</td>
    <td bgcolor="#ffff99">&nbsp;</td>
    <td bgcolor="#ffff99">&nbsp;</td>
    <td bgcolor="#ffff99">&nbsp;</td>
    <td bgcolor="#ffff99">&nbsp;</td>
    <td bgcolor="#ffff99">&nbsp;</td>
    <td bgcolor="#ffff99">&nbsp;</td>
    <td bgcolor="#ffff99">&nbsp;</td>
    <td bgcolor="#ffff99">&nbsp;</td>
    <td bgcolor="#ffff99">&nbsp;</td>
    <td bgcolor="#ffff99">&nbsp;</td>
    <td bgcolor="#ffff99">&nbsp;</td>
    <td bgcolor="#ffff99">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>Vender Update</td>
    <td></td>
    <td> </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <th align="center" valign="bottom" bgcolor="#ffff99"><strong>No</strong></th>
    <th align="center" valign="bottom" bgcolor="#ffff99"><strong>File No</strong></th>
    <th align="center" valign="bottom" bgcolor="#ffff99"><strong>Company Name</strong></th>
    <th align="center" valign="bottom" bgcolor="#ffff99"><strong>Type of    Business</strong></th>
    <th align="center" valign="bottom" bgcolor="#ffff99"><strong>M / Q</strong></th>
    <th align="center" valign="bottom" bgcolor="#ffff99"><strong>Monthly</strong></th>
    <th align="center" valign="bottom" bgcolor="#ffff99"><strong>Qtrly</strong></th>
    <th align="center" valign="bottom" bgcolor="#ffff99"><strong>HY/Yrly</strong></th>
    <th align="center" valign="bottom" bgcolor="#ffff99">&nbsp;</th>
    <th align="center" valign="bottom" bgcolor="#ffff99"><strong>Only PR Service</strong></th>
    <th align="center" valign="bottom" bgcolor="#ffff99"><strong>Full PR Service</strong></th>
    <th align="center" valign="bottom" bgcolor="#ffff99"><strong>Only Acct</strong></th>
    <th align="center" valign="bottom" bgcolor="#ffff99"><strong>Regular Acct+PR+ST</strong></th>
    <th align="center" valign="bottom" bgcolor="#ffff99">&nbsp;</td>
    <th colspan="2" align="center" valign="bottom" bgcolor="#ffff99"><strong>Location</strong></th>
    <th align="center" valign="bottom" bgcolor="#ffff99"><strong>QB</strong></th>
    <th align="center" bgcolor="#ffff99">&nbsp;</th>
    <th align="center" valign="bottom" bgcolor="#ffff99"><strong>Team-AC</strong></th>
    <th colspan="2" align="center" valign="bottom" bgcolor="#ccffff"><strong>Team-DJ</strong></th>
    <th colspan="2" align="center" valign="bottom" bgcolor="#ffcc99"><strong>Team-RP</strong></th>
    <th align="center" valign="bottom" bgcolor="#99ccff"><strong>Team-JC</strong></th>
  </tr>
  <tr>
    <td colspan="2" align="center" bgcolor="#ffff99"><strong>Georgia</strong></td>
    <td align="center" bgcolor="#ffff99">&nbsp;</td>
    <td align="center" bgcolor="#ffff99">&nbsp;</td>
    <td align="center" bgcolor="#ffff99">&nbsp;</td>
    <td align="center" bgcolor="#ffff99">&nbsp;</td>
    <td align="center" bgcolor="#ffff99">&nbsp;</td>
    <td align="center" bgcolor="#ffff99">&nbsp;</td>
    <td align="center" bgcolor="#ffff99">&nbsp;</td>
    <td align="center" bgcolor="#ffff99">&nbsp;</td>
    <td align="center" bgcolor="#ffff99">&nbsp;</td>
    <td align="center" bgcolor="#ffff99">&nbsp;</td>
    <td align="center" bgcolor="#ffff99">&nbsp;</td>
    <td align="center" bgcolor="#ffff99">&nbsp;</td>
    <td align="center" bgcolor="#ffff99"><strong>Server</strong></td>
    <td align="center" bgcolor="#ffff99"><strong>Room-1</strong></td>
    <td align="center" bgcolor="#ffff99">&nbsp;</td>
    <td align="center" bgcolor="#ffff99">&nbsp;</td>
    <td align="center" bgcolor="#ccc0da"><strong>Antionte</strong></td>
    <td align="center" bgcolor="#ccffff"><strong>Gunjan</strong></td>
    <td align="center" bgcolor="#ccffff"><strong>Sunil</strong></td>
    <td align="center" bgcolor="#ffcc99"><strong>Rohit</strong></td>
    <td align="center" bgcolor="#ffcc99"><strong>2nd person</strong></td>
    <td align="center" bgcolor="#99ccff"><strong>Ankit</strong></td>
  </tr>
  <tr>
    <td align="center">1</td>
    <td>GA-01-012</td>
    <td>Comm3 Creative, Inc</td>
    <td>Graphic Designs</td>
    <td align="center">Monthly</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center"></td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center"></td>
    <td align="center" bgcolor="#92d050">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">2</td>
    <td>GA-109-3</td>
    <td>Sifan One Inc </td>
    <td>Liquor Store</td>
    <td align="center">Monthly</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center" bgcolor="#92d050">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">3</td>
    <td>GA-166</td>
    <td>Singadia Investment    of America Inc.</td>
    <td>Com. Property Rental</td>
    <td align="center" bgcolor="#cc99ff">Half-Yearly</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center" bgcolor="#92d050">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">4</td>
    <td>GA-201-2</td>
    <td>Mahadev Inc.</td>
    <td>Com. Property Rental</td>
    <td align="center" bgcolor="#ffcc99">Quarterly</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center" bgcolor="#92d050">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">5</td>
    <td>GA-219</td>
    <td>ZK, Corporation</td>
    <td>Restaurant-Indian</td>
    <td align="center">Monthly</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center"></td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center"></td>
    <td align="center" bgcolor="#92d050">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">6</td>
    <td>GA-226-1</td>
    <td>Mandeep of USA Inc.</td>
    <td>Tobacco Store</td>
    <td align="center" bgcolor="#ffcc99">Quarterly</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center"></td>
    <td align="center" bgcolor="#92d050">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
  </tr>
  <tr>
    <td align="center">7</td>
    <td>GA-231</td>
    <td>Servitix Pest    Control, Inc.</td>
    <td>Pest Control</td>
    <td align="center">Monthly</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center" bgcolor="#92d050">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
  </tr>
  <tr>
    <td align="center">8</td>
    <td>GA-251-1</td>
    <td>Sabrin, Inc.</td>
    <td>Fast Food Rest-DQ</td>
    <td align="center">Monthly</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center" bgcolor="#92d050">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
  </tr>
  <tr>
    <td align="center">9</td>
    <td>GA-258-1</td>
    <td>Shreeji Shlok Inc</td>
    <td>Conv. Store With Gas</td>
    <td align="center">Monthly</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center" bgcolor="#92d050">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
  </tr>
  <tr>
    <td align="center">10</td>
    <td>GA-260-7</td>
    <td>PKS Zevar LLC</td>
    <td>Jewellery-Indian</td>
    <td align="center">Monthly</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center" bgcolor="#92d050">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">11</td>
    <td>GA-266-7</td>
    <td>Shakti Gauri Inc.</td>
    <td>Hotel/Motel</td>
    <td align="center">Monthly</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center" bgcolor="#92d050">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">12</td>
    <td>GA-305-1</td>
    <td>Kiplistore Inc</td>
    <td>Mail Box Shipping</td>
    <td align="center" bgcolor="#ffcc99">Quarterly</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">13</td>
    <td>GA-314</td>
    <td>Travel Universe, Inc.</td>
    <td>Travel Agency</td>
    <td align="center">Monthly</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center" bgcolor="#92d050">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">14</td>
    <td>GA-318-1</td>
    <td>Avinash Patel LLC</td>
    <td>Liquor Store</td>
    <td align="center">Monthly</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center" bgcolor="#92d050">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">15</td>
    <td>GA-324</td>
    <td>Sai  World, LLC</td>
    <td>Sandwich Shop-Blimpie</td>
    <td align="center">Monthly</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center" bgcolor="#92d050">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">16</td>
    <td>GA-324-1</td>
    <td>Jay &amp; Sam, LLC</td>
    <td>Sandwich Shop-Blimpie</td>
    <td align="center">Monthly</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center" bgcolor="#92d050">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">17</td>
    <td>GA-331</td>
    <td>Macon Medical Clinic, LLC</td>
    <td bgcolor="#ff0000">Medical Services</td>
    <td align="center" bgcolor="#ffcc99">Quarterly</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center" bgcolor="#92d050">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center"><strong>17</strong></td>
    <td>&nbsp;</td>
    <td bgcolor="#ffcc00"><strong>TOTAL</strong></td>
    <td>&nbsp;</td>
    <td align="center"><strong>17 </strong></td>
    <td align="center" bgcolor="#ffc000"><strong>12</strong></td>
    <td align="center" bgcolor="#ffc000"><strong>4</strong></td>
    <td align="center" bgcolor="#ffc000"><strong>1</strong></td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center" bgcolor="#ffcc00"><strong>9 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>7 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>1 </strong></td>
    <td align="center" bgcolor="#ffcc00">&nbsp;</td>
    <td align="center" bgcolor="#ffcc00"><strong>3 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>5 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>3 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>2 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>4 </strong></td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td colspan="2">South Carolina</td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">1</td>
    <td>SC-002</td>
    <td>Jay Bhathiji Inc</td>
    <td>Conv. Store With Gas</td>
    <td align="center">Monthly</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center" bgcolor="#92d050">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center"><strong>1</strong></td>
    <td>&nbsp;</td>
    <td bgcolor="#ffcc00"><strong>TOTAL</strong></td>
    <td>&nbsp;</td>
    <td align="center"><strong>1 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>1 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center" bgcolor="#ffcc00"><strong>1 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00">&nbsp;</td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>1 </strong></td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" align="center">&nbsp;</td>
    <td colspan="2">Tennessee</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">1</td>
    <td>TN-001</td>
    <td>PC of America Corp.</td>
    <td>Grocery Store</td>
    <td align="center">Monthly</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">2</td>
    <td>TN-001-1</td>
    <td>J K Of America    Corporation </td>
    <td>-</td>
    <td align="center">Monthly</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center"><strong>2</strong></td>
    <td>&nbsp;</td>
    <td bgcolor="#ffcc00"><strong>TOTAL</strong></td>
    <td>&nbsp;</td>
    <td align="center"><strong>2 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>2 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center" bgcolor="#ffcc00"><strong>2 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00">&nbsp;</td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>2 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" align="center">&nbsp;</td>
    <td colspan="2">Alabama</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">1</td>
    <td>AL-003</td>
    <td>Milestone, Inc.</td>
    <td>Conv. Store With Gas</td>
    <td align="center">Monthly</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
  </tr>
  <tr>
    <td align="center">2</td>
    <td>AL-003-1</td>
    <td>M &amp; M Petroliem    LLC</td>
    <td>Fuel Dealer</td>
    <td align="center" bgcolor="#ffcc99">Quarterly</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">3</td>
    <td>AL-005</td>
    <td>Sharna Corp.</td>
    <td>Conv. Store With Gas</td>
    <td align="center">Monthly</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
    <td> </td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">3</td>
    <td>&nbsp;</td>
    <td bgcolor="#ffcc00"><strong>TOTAL</strong></td>
    <td>&nbsp;</td>
    <td align="center"><strong>3 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>2 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>1 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center" bgcolor="#ffcc00"><strong>2 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>1 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00">&nbsp;</td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>1 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>2 </strong></td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" align="center">&nbsp;</td>
    <td colspan="2">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
    <td bgcolor="#ffcc00"><strong>TOTAL</strong></td>
    <td>&nbsp;</td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00">&nbsp;</td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00">&nbsp;</td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">23 </td>
    <td><u>&nbsp;</u></td>
    <td bgcolor="#ffcc00"><strong>GRAND TOTAL</strong></td>
    <td>&nbsp;</td>
    <td align="center"><strong>23 </strong></td>
    <td align="center" valign="middle" bgcolor="#ffcc00"><strong>       17 </strong></td>
    <td align="center" valign="middle" bgcolor="#ffcc00"><strong>5 </strong></td>
    <td align="center" valign="middle" bgcolor="#ffcc00"><strong>1 </strong></td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center"><strong>14 </strong></td>
    <td align="center"><strong>8 </strong></td>
    <td align="center"><strong>1 </strong></td>
    <td align="center">&nbsp;</td>
    <td align="center"><strong>3 </strong></td>
    <td align="center"><strong>5 </strong></td>
    <td align="center"><strong>6 </strong></td>
    <td align="center"><strong>2 </strong></td>
    <td align="center"><strong>0 </strong></td>
    <td align="center"><strong>7 </strong></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
</table>
!--></div>
<div class="col-md-12 col-sm-12 col-xs-12">  
<?php
//echo "<pre>";
   // print_r($categoryfirst);die;
?>
<?php if(!empty($_REQUEST['status'])){ 
//exit('11');?>

<center>
    <img src="https://financialservicecenter.net/public/dashboard/images/fsc_logo.png" width="80px" class="img-responsive">
</center>
<br>
<?php //count($client1);?>
<div style="position:relative">
<!--<h3 style="font-size:20px; text-align:center; font-size:20px; color:#000; margin:0px 0px 10px;"> FSC Client Report</h3>-->
<div class="subtitle subtitle_cr"style="max-width:90%; padding-right:85px;height:35px;display:block !important;">
    <?php //echo "<pre>";
   //echo "<pre>"; print_r($_POST);?>
    <span class="floatleft pull-left countboxmian" style="margin-right:40px;margin-left: 10px; position:absolute;">Count: <b><?php echo count($clients1);?></b></span>
        <?php if(isset($_POST['accounting_location']) && $_POST['accounting_location'] !='') {?><span class=" pull-right" style="right:10px;position:absolute;">Accounting Location: <b><?php  echo $_POST['accounting_location'];?></b></span><?php } ?>
    <?php if(isset($taxyesas) && $taxyesas !='') {?><span class="floatleft pull-left" style="margin-left: 120px; position:absolute;">Year: <b><?php  echo $taxyesas;?></b></span><?php } ?>
    <?php if(isset($_POST['accounting_software']) && $_POST['accounting_software'] !='') {?><span class="" style="text-align: center;display: block;">Accounting Software: <b><?php  echo $_POST['accounting_software'];?></b></span><?php } ?>

    
    <?php if(isset($clients1[0]->taxation_service)) {?><span class=""  style="text-align: center;display: block;">Type of Service: <b><?php  echo $clients1[0]->title;?> </b></span><?php } ?>
  
   <?php if(isset($_POST['acperiod']) && $_POST['acperiod'] !='') {?><span class=""  style="text-align: center;display: block;">Type of Service: <b>Accounting Service </b></span><span class=""  style="text-align: right;display: block;margin-top:-21px;margin-right:10px;">Period: <b><?php echo $acperiods->period;?> </b></span><?php } ?>
   <?php if(isset($_POST['type_of_service']) && $_POST['type_of_service'] !='' && !isset($clients1[0]->taxation_service) && $_POST['acperiod'] =='') {?><span class=""  style="text-align: center;display: block;">Type of Service: <b><?php echo $_POST['type_of_service'];?> </b></span><?php } ?>
   
    <?php if(isset($businessfirst->bussiness_name)) {?><span class=""  style="text-align: center;display: block;">Type of Business: <b><?php echo $businessfirst->bussiness_name;?></b></span><?php } ?>
    <?php  if(isset($servicefirst->title)) { ?> <span class=""  style="text-align: right;display: block; padding-right:20px;margin-top: -21px;">Type of Service: <b><?php echo $servicefirst->title;?></b></span><?php }?>

     <?php if(isset($categoryfirst->business_cat_name)!="") {?>
     <span class=""  style="text-align: right;display: block; padding-right:10px;margin-top: -21px;"> Business Category: <b><?php if(isset($categoryfirst->business_cat_name)!="") { echo $categoryfirst->business_cat_name; }?></b></span>
     <?php }?>
   
    <!--<span class="floatright">Type of Client: <b></b></span></div>-->
<!--<div class="buttonarea">-->
<!--    <a href="#" class="btn btn-primary btn-sm" data-toggle="Print" title="Print" ><i class="fa fa-print"></i></a>-->
<!--    <a href="#" class="btn btn-success btn-sm" data-toggle="Pdf" title="Pdf"><i class="fa fa-file-pdf-o"></i></a>-->
<!--    <a href="#" class="btn btn-warning btn-sm" data-toggle="Excel" title="Excel"><i class="fa fa-file-excel-o"></i></a>-->
<!--</div>-->
</div>
<!--<button attr="printMe"  type="button" class="btn btn-primary btn-sm pull-right clkbutton" style="margin-top:-5%;margin-right: 15px; position: ABSOLUTE; right:0px;top: 0px;">Print</button>-->
  
<div id="printMe">
                        <style>
				  @media print{
					  table{font-family: Verdana, Geneva, Tahoma, sans-serif;}
					  table tr td{padding:4px; border-bottom:1px solid #ccc;}
					   table tr th{padding:4px; border-bottom:1px solid #ccc;}
				  }
				</style>
			
<div class="table-responsive table_cr">
     

      <?php 
     //print_r($clients1);
     $cnts=0;
     // $count= count($clients1); 
      if($client1>0){
      ?>
      <table class="table table-hover table-bordered " id="example">
  <thead style="background: #ffff99;">
    <tr>
      <th scope="col">No.</th>
      <th scope="col">Client ID</th>
      <th scope="col">Company <br> <span style="font-weight:initial;font-size:12px;">(Legal Name)</span></th>
      <th scope="col"><?php if(isset($_POST['business_id']) && $_POST['business_id']!='6') {?>Business Name <br> <span style="font-weight:initial;font-size:12px;">(DBA Name)</span><?php } else { ?> Email <?php } ?></th>
      <th scope="col">Contact Person Name</th>
      <th scope="col">Telephone</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    @foreach($clients1 as $client1)
    <?php $cnts++;?>
    <tr>
      <th scope="row">{{$cnts}}</th>
      <td style="text-align:left !important;">{{$client1->filename}}</td>
      <td style="text-align:left !important;">@if($client1->business_id =='6') {{ucwords($client1->first_name.' '.$client1->middle_name.' '.$client1->last_name)}} @else {{$client1->company_name}}@endif</td>
      <td class="titledba" style="text-align:left !important;"><?php if(isset($_POST['business_id']) && $_POST['business_id']!='6') {?>{{$client1->business_name}}<?php } else { ?>{{$client1->email}} <?php } ?></td>
      
      <td style="text-align:left !important;">{{ucwords($client1->contactnametype.' '.$client1->firstname.' '.$client1->middlename.' '.$client1->lastname)}}</td>
      <td>{{$client1->business_no}}</td>
      <td><a class="btn-action btn-view-edit btn-primary" href="{{route('customer.edit',$client1->id)}}"><i class="fa fa-edit"></i></a></td>
    </tr>
    
    @endforeach
    </tbody>
    </table>
    <?php } 
    
    else
    { ?>
    <table class="table table-hover table-bordered" id="sampleTable7" style="margin-top:100px !important;">
  <thead style="background: #ffff99;">
    <tr>
      <th scope="col">No.</th>
      <th scope="col">Client ID</th>
      <th scope="col">Company <br> <span style="font-weight:initial;font-size:12px;">(Legal Name)</span></th>
      <th scope="col"><?php if(isset($_POST['business_id']) && $_POST['business_id']!='6') {?>Business Name <br> <span style="font-weight:initial;font-size:12px;">(DBA Name)</span><?php } else { ?> Email <?php } ?></th>
      <th scope="col">Contact Person Name</th>
      <th scope="col">Telephone</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
 <tr>

      <td colspan="7">Not found data</td>

    </tr>
      </tbody>
</table>
    <?php }?>

            </div>
            
            <?php }?></div>
                </div>
         </div>
         <br>
      </div>
   </div>
</div>

<script>
//$.ajaxSetup({
  //  headers:
   // {'X-CSRF-Token': $('input[name="_token"]').val();}
//});
</script>
<script>
   	    (function ($) {
    function calcDisableClasses(oSettings) {
        var start = oSettings._iDisplayStart;
        var length = oSettings._iDisplayLength;
        var visibleRecords = oSettings.fnRecordsDisplay();
        var all = length === -1;
 
        // Gordey Doronin: Re-used this code from main jQuery.dataTables source code. To be consistent.
        var page = all ? 0 : Math.ceil(start / length);
        var pages = all ? 1 : Math.ceil(visibleRecords / length);
 
        var disableFirstPrevClass = (page > 0 ? '' : oSettings.oClasses.sPageButtonDisabled);
        var disableNextLastClass = (page < pages - 1 ? '' : oSettings.oClasses.sPageButtonDisabled);
 
        return {
            'first': disableFirstPrevClass,
            'previous': disableFirstPrevClass,
            'next': disableNextLastClass,
            'last': disableNextLastClass
        };
    }
 
    function calcCurrentPage(oSettings) {
        return Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength) + 1;
    }
 
    function calcPages(oSettings) {
        return Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength);
    }
 
    var firstClassName = 'first';
    var previousClassName = 'previous';
    var nextClassName = 'next';
    var lastClassName = 'last';
 
    var paginateClassName = 'paginate';
    var paginatePageClassName = 'paginate_page';
    var paginateInputClassName = 'paginate_input';
    var paginateTotalClassName = 'paginate_total';
 
    $.fn.dataTableExt.oPagination.input = {
        'fnInit': function (oSettings, nPaging, fnCallbackDraw) {
            var nFirst = document.createElement('span');
            var nPrevious = document.createElement('span');
            var nNext = document.createElement('span');
            var nLast = document.createElement('span');
            var nInput = document.createElement('input');
            var nTotal = document.createElement('span');
            var nInfo = document.createElement('span');
 
            var language = oSettings.oLanguage.oPaginate;
            var classes = oSettings.oClasses;
            var info = language.info || 'Page _INPUT_ of _TOTAL_';
 
            nFirst.innerHTML = language.sFirst;
            nPrevious.innerHTML = language.sPrevious;
            nNext.innerHTML = language.sNext;
            nLast.innerHTML = language.sLast;
 
            nFirst.className = firstClassName + ' ' + classes.sPageButton;
            nPrevious.className = previousClassName + ' ' + classes.sPageButton;
            nNext.className = nextClassName + ' ' + classes.sPageButton;
            nLast.className = lastClassName + ' ' + classes.sPageButton;
 
            nInput.className = paginateInputClassName;
            nTotal.className = paginateTotalClassName;
 
            if (oSettings.sTableId !== '') {
                nPaging.setAttribute('id', oSettings.sTableId + '_' + paginateClassName);
                nFirst.setAttribute('id', oSettings.sTableId + '_' + firstClassName);
                nPrevious.setAttribute('id', oSettings.sTableId + '_' + previousClassName);
                nNext.setAttribute('id', oSettings.sTableId + '_' + nextClassName);
                nLast.setAttribute('id', oSettings.sTableId + '_' + lastClassName);
            }
 
            nInput.type = 'text';
 
            info = info.replace(/_INPUT_/g, '</span>' + nInput.outerHTML + '<span>');
            info = info.replace(/_TOTAL_/g, '</span>' + nTotal.outerHTML + '<span>');
            nInfo.innerHTML = '<span>' + info + '</span>';
 
            nPaging.appendChild(nFirst);
            nPaging.appendChild(nPrevious);
            $(nInfo).children().each(function (i, n) {
                nPaging.appendChild(n);
            });
            nPaging.appendChild(nNext);
            nPaging.appendChild(nLast);
 
            $(nFirst).click(function() {
                var iCurrentPage = calcCurrentPage(oSettings);
                if (iCurrentPage !== 1) {
                    oSettings.oApi._fnPageChange(oSettings, 'first');
                    fnCallbackDraw(oSettings);
                }
            });
 
            $(nPrevious).click(function() {
                var iCurrentPage = calcCurrentPage(oSettings);
                if (iCurrentPage !== 1) {
                    oSettings.oApi._fnPageChange(oSettings, 'previous');
                    fnCallbackDraw(oSettings);
                }
            });
 
            $(nNext).click(function() {
                var iCurrentPage = calcCurrentPage(oSettings);
                if (iCurrentPage !== calcPages(oSettings)) {
                    oSettings.oApi._fnPageChange(oSettings, 'next');
                    fnCallbackDraw(oSettings);
                }
            });
 
            $(nLast).click(function() {
                var iCurrentPage = calcCurrentPage(oSettings);
                if (iCurrentPage !== calcPages(oSettings)) {
                    oSettings.oApi._fnPageChange(oSettings, 'last');
                    fnCallbackDraw(oSettings);
                }
            });
 
            $(nPaging).find('.' + paginateInputClassName).keyup(function (e) {
                // 38 = up arrow, 39 = right arrow
                if (e.which === 38 || e.which === 39) {
                    this.value++;
                }
                // 37 = left arrow, 40 = down arrow
                else if ((e.which === 37 || e.which === 40) && this.value > 1) {
                    this.value--;
                }
 
                if (this.value === '' || this.value.match(/[^0-9]/)) {
                    /* Nothing entered or non-numeric character */
                    this.value = this.value.replace(/[^\d]/g, ''); // don't even allow anything but digits
                    return;
                }
 
                var iNewStart = oSettings._iDisplayLength * (this.value - 1);
                if (iNewStart < 0) {
                    iNewStart = 0;
                }
                if (iNewStart >= oSettings.fnRecordsDisplay()) {
                    iNewStart = (Math.ceil((oSettings.fnRecordsDisplay()) / oSettings._iDisplayLength) - 1) * oSettings._iDisplayLength;
                }
 
                oSettings._iDisplayStart = iNewStart;
                oSettings.oInstance.trigger("page.dt", oSettings);
                fnCallbackDraw(oSettings);
            });
 
            // Take the brutal approach to cancelling text selection.
            $('span', nPaging).bind('mousedown', function () { return false; });
            $('span', nPaging).bind('selectstart', function() { return false; });
 
            // If we can't page anyway, might as well not show it.
            var iPages = calcPages(oSettings);
            if (iPages <= 1) {
                $(nPaging).hide();
            }
        },
 
        'fnUpdate': function (oSettings) {
            if (!oSettings.aanFeatures.p) {
                return;
            }
 
            var iPages = calcPages(oSettings);
            var iCurrentPage = calcCurrentPage(oSettings);
 
            var an = oSettings.aanFeatures.p;
            if (iPages <= 1) // hide paging when we can't page
            {
                $(an).hide();
                return;
            }
 
            var disableClasses = calcDisableClasses(oSettings);
 
            $(an).show();
 
            // Enable/Disable `first` button.
            $(an).children('.' + firstClassName)
                .removeClass(oSettings.oClasses.sPageButtonDisabled)
                .addClass(disableClasses[firstClassName]);
 
            // Enable/Disable `prev` button.
            $(an).children('.' + previousClassName)
                .removeClass(oSettings.oClasses.sPageButtonDisabled)
                .addClass(disableClasses[previousClassName]);
 
            // Enable/Disable `next` button.
            $(an).children('.' + nextClassName)
                .removeClass(oSettings.oClasses.sPageButtonDisabled)
                .addClass(disableClasses[nextClassName]);
 
            // Enable/Disable `last` button.
            $(an).children('.' + lastClassName)
                .removeClass(oSettings.oClasses.sPageButtonDisabled)
                .addClass(disableClasses[lastClassName]);
 
            // Paginate of N pages text
            $(an).find('.' + paginateTotalClassName).html(iPages);
 
            // Current page number input value
            $(an).find('.' + paginateInputClassName).val(iCurrentPage);
        }
    };
})(jQuery);
   	</script>
<script>
$(document).ready(function() {
    var table = $('#example').DataTable( {
        
        dom: 'Bfrtlip',
        "pagingType": "input",
        
    //   "sDom": '<"customercount"i>rt<"bottom"<"#refresh">flp><"clear">',
    "columnDefs": [ {
            "searchable": true,
            "orderable": true,
            "targets": [0],
        } ],
       "order": [[ 1, 'asc' ]],
        buttons: [
                 {
                extend:    'copyHtml5',
                text:      '<i class="fa fa-files-o"></i> &nbsp; Copy',
                //titleAttr: 'Copy',
                title: $('h1').text(),
            },{
                extend:'excelHtml5',
                text:'<i class="fa fa-file-excel-o"></i>&nbsp; Excel',
               // titleAttr: 'Excel',
                title: $('h4').text(),
                 customize: function( xlsx ) {
                var sheet = xlsx.xl.worksheets['sheet1.xml'];
                        $('row c[r^="A"]',sheet).attr('s','51'); 
                       //   $('row c[r^="C"]',sheet).attr('s','51'); 
                          $('row:first c',sheet).attr('s','51');
            },
            exportOptions: {
                columns: [0,1,2,3,4,5,6], // Only name, email and role
                }
            },
            {
                extend:    'csvHtml5',
                text:      '<i class="fa fa-file-text-o"></i> &nbsp; CSV',
               // titleAttr: 'CSV',
                title: $('h1').text(),
            },
            {
                extend:    'pdfHtml5',
                text:      '<i class="fa fa-file-pdf-o"></i>&nbsp;  PDF',
             customize: function (doc) {
						//Remove the title created by datatTables
						doc.content.splice(0,1);
						//Create a date string that we use in the footer. Format is dd-mm-yyyy
						var now = new Date();
						var jsDate = now.getDate()+'-'+(now.getMonth()+1)+'-'+now.getFullYear();
						var logo = 'data:image/jpeg;base64,{{$logo->logourl}}';
						doc.pageMargins = [20,60,20,20];
						doc.defaultStyle.fontSize = 10;
						doc.styles.tableHeader.fontSize = 10;
						doc['header']=(function() {
							return {
								columns: [{
									    alignment: 'left',
										image: logo,
										width: 50,margin: [200,5]
									},{
										alignment: 'CENTER',
										text: 'List of Client',
										fontSize: 20,
										margin: [50,35,50,40],
									},],
								margin: [20, 0, 0,12],alignment: 'center',
							}
						});
						var objLayout = {};
						objLayout['hLineWidth'] = function(i) { return 2; };
						objLayout['vLineWidth'] = function(i) { return 2; };
						objLayout['hLineColor'] = function(i) { return '#ccc'; };
						objLayout['vLineColor'] = function(i) { return '#ccc'; };
						objLayout['paddingLeft'] = function(i) { return 14; };
						objLayout['paddingRight'] = function(i) { return 14; };
						doc.content[0].layout = objLayout;
				},
                 exportOptions: {
                 columns: [0,1,2,3,4,5,6], // Only name, email and role
                }
            },
            {
           extend: 'print',
          text: '<i class="fa fa-print"></i>&nbsp; Print',
         customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '10pt' )
                        .prepend(
                            '<center><img src="https://financialservicecenter.net/public/business/{{$logo->logo}}"/></center>'
                        );
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );
                },
         exportOptions: {
          columns: [0,1,2,3,4,5,6]
      },
      footer: true,
      autoPrint: true
    },
        ],
      
    } );
$('input.global_filter').on('keyup click', function() {
        filterGlobal();
    });
    $('input.column_filter').on( 'keyup click', function() {
        filterColumn( $(this).parents('tr').attr('data-column'));
    });
  table.on( 'order.dt search.dt', function () {
       table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
          cell.innerHTML = i + 1;
          table.cell(cell).invalidate('dom'); });
    }).draw();
table.columns(6).search('^(?:(?!6|3|4|5|1).)*$\r?\n?', true, false).draw();
//console.log(table.columns(3));
//table.columns(8).search('^(?:(?!Approval).)*$\r?\n?', false, false).draw();
      $("#choice").on("change",function(){
 var _val = $(this).val();//alert(_val);
//alert(_val);
 if(_val == '6'){   
        table.columns(6).search(_val).draw();
  }
  else if(_val == '2'){  
  table.columns(6).search(_val).draw();
   }
  else if(_val == '3'){  //alert();
         table.columns(6).search(_val).draw();
  }
   else if(_val == '4'){  //alert();
         table.columns(6).search(_val).draw();
  }
  else if(_val == '5'){  //alert();
         table.columns(6).search(_val).draw();
  }
  else{
        table.columns().search('').draw(); 
  }
  });
    $("#choice1").on("change",function(){
 var _val = $(this).val();//alert(_val);
 
 if(_val == 'Business'){   
        table.columns(5).search(_val).draw();
  }
  else if(_val == 'Investor'){  
         table.columns(5).search(_val).draw();
          // table.columns(8).search('^(?:(?!Approval).)*$\r?\n?', false, false).draw();
  }
  else if(_val == 'Non-Profit Organization'){  //alert();
         table.columns(5).search(_val).draw();
  }
   else if(_val == 'Personal'){  //alert();
         table.columns(5).search(_val).draw();
  }
  else if(_val == 'Profession'){  //alert();
         table.columns(5).search(_val).draw();
  }
  else if(_val == 'Service Industry'){  //alert();
         table.columns(5).search(_val).draw();
  }
  else{
        table.columns().search('').draw(); 
  }
  })
});
function filterGlobal() {
    $('#example').DataTable().search(
        $('#global_filter').val(),
        $('#global_regex').prop('checked'),
        $('#global_smart').prop('checked')
    ).draw();
}
function filterColumn(i) {
    $('#example').DataTable().column( i ).search(
        $('#col'+i+'_filter').val(),
        $('#col'+i+'_regex').prop('checked'),
        $('#col'+i+'_smart').prop('checked')
    ).draw();
}
</script>
<script>
   $(document).ready(function(){
        $(".clkbutton").click(function(){
        //alert();
            var attr=$(this).attr('attr');
            alert(attr);
              var printContents = document.getElementById(attr).innerHTML;
        w=window.open();
        w.document.write(printContents);
        w.print();
        w.close();
        });
       
    $(document).on('change','.hideyears', function()
   {
        var id = $(this).val();
        if(id =='7')
        {
            
            $('.showyears').show();
        }
        else
        {
            $('.showyears').hide();
        }
       
   });
    $(document).on('change','.servicetype', function()
   {
       var thiss=$(this).val();
       
       if(thiss !='' ||thiss !='Accounting Service' || thiss !='Payroll Service')
       {
           $('.showyears').show();
           
       }
       else
       {
           $('.showyears').hide();
           
       }
       if(thiss =='Accounting Service')
       {
           $('.accounting_period').show();
           
       }
       else
       {
           $('.accounting_period').hide();
       }
   });
   
   $(document).on('change','#type_of_service', function()
   {
        var id = $(this).val();
        if(id !='')
        {
            $('.periodhide').hide();
            
        }
        else
        {
            $('.periodhide').show();
        }
       
   });
   var category=$('.category').val();
   if(category)
   {
            $('.periodhide').hide();
            $('.type_of_service').hide();
            $('.showyears').hide();
            $('#business_catagory_name_2').show();
       
   }
   $(document).on('change','.category', function()
   {
        //console.log('htm');
        var id = $(this).val();
        if(id)
        {
            $('.accsoftware').hide();
            $('.acclocation').hide();
            $('.periodhide').hide();
            $('.accounting_period').hide();
            
        }
        else
        {
            $('.accsoftware').show();
            $('.acclocation').show();
            $('.periodhide').show();
            $('.accounting_period').show();
            
        }
        if(id =='all' || id =='6')
        {
            $('.accsoftware').hide();
            $('.acclocation').hide();
            
        }
        
        else
        {
          //     $('.accsoftware').show();
            //$('.acclocation').show();
            
        }
        if(id =='all')
        {
            $('.type_of_service').show();
        }
        else
        {
            $('.type_of_service').hide();
        }
        if(id =='6')
        {  
            $('.showpersonal').show();
            
        }
        else
        {
            $('.showpersonal').hide();
            
        }
        $.get('{!!URL::to('getcat')!!}?id='+id, function(data)
        { 
           $('#user_type').empty();
           $.each(data, function(index, subcatobj)
           {
               // $('#user_type').val(subcatobj.bussiness_name);
         
           })
        });
   
           $.get('{!!URL::to('getRequests')!!}?id='+id, function(data)
           { 
              //alert(id);
               if(data == "")
               {
                   $('#business_catagory_name_2').hide();
                //   $('#business_catagory_name_4').hide();
                //   $('#business_catagory_name_3').hide();
               }
               else
               {
                    $('#business_catagory_name_2').show();
               }
               $('#business_catagory_name').empty();
            //   $('#business_catagory_name_4').hide();
            //   $('#business_catagory_name_3').hide();
            //   $('#business_catagory_name').append('<option value="">---Select---</option>');
            //   $('#business_brand_category_name').append('<option value="">---Select---</option>');
              $('#business_catagory_name').append('<option value="">---Select---</option>');
               $.each(data, function(index, subcatobj)
               {
                    $('#business_catagory_name').append('<option value="'+subcatobj.id+'">'+subcatobj.business_cat_name+'</option>');
                	//$('#user_type111').val('subcatobj.business_cat_name');
                	$('#business_catagory_name22').val(subcatobj.business_cat_name);
               })
           });
   
   
   
        });
   });
</script>
<script>
    function Refresh() {
        window.parent.location = window.parent.location.href;
    }
</script>
<script>
$(document).ready(function() {
     $('#example').DataTable();
     
     $(document).on('change','.personaltype', function()
  {
      var thiss=$(this).val();
    //  alert(thiss);
      if(thiss =='7')
      {
          $('#monthlytype').val('7');
      }
      else if(thiss =='8')
      {
          $('#monthlytype').val('4');
      }
});
});
</script>
<script>
/* $(document).on('change','.category', function()*/
/*   {*/
  
/*    var id = $(this).val();*/
/*   $.get('{!!URL::to('getRequest')!!}?id='+id, function(data)*/
/*   {  */
      
       //alert();
/*   if(data == "")*/
/*   {*/
/*   $('#business_catagory_name_2').hide();*/
/*   $('#business_catagory_name_4').hide();*/
/*   $('#business_catagory_name_3').hide();*/
/*   }*/
/*   else*/
/*   {*/
/*   $('#business_catagory_name_2').show();*/
/*   }*/
/*   $('#business_catagory_name').empty();*/
/*   $('#business_catagory_name_4').hide();*/
/*   $('#business_catagory_name_3').hide();*/
/*   $('#business_catagory_name').append('<option value="">---Select---</option>');*/
/*   $('#business_brand_category_name').append('<option value="">---Select---</option>');*/
/*   $('#business_brand_name').append('<option value="">---Select---</option>');*/
/*   $.each(data, function(index, subcatobj)*/
/*   {*/
/*   $('#business_catagory_name').append('<option value="'+subcatobj.id+'">'+subcatobj.business_cat_name+'</option>');*/
   //	$('#user_type').val('subcatobj.business_cat_name');
/*   })*/
/*   });*/
/*   });*/
  </script>
<style>
.table > thead > tr > th {background: #98c4f2;text-align: center;}
td{text-align: center !important;}
.form-group {margin-bottom: 0px;width: 100%;float: left;}
   </style>
   
   <script>
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();
});
</script>
@endsection