@extends('fac-Bhavesh-0554.layouts.app')

@section('main-content')
<div class="content-wrapper">

	<div class="page-title">
		<h1>Control</h1>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-body">
                   
					<form method="post" action="{{route('control.update',$branch->id)}}" class="form-horizontal" id="businessname" name="businessname" enctype="multipart/form-data">
					{{csrf_field()}}{{method_field('PATCH')}}
						<div class="form-group {{ $errors->has('statename') ? ' has-error' : '' }}">
							<label class="control-label col-md-3">State:</label>
							<div class="col-md-4">
								<select name="statename" id="statename" class="form-control fsc-input">

<option value="{{$branch->statename}}">{{$branch->statename}}</option>
<option value="AK">AK</option>
								<option value="AS">AS</option>
								<option value="AZ">AZ</option>
								<option value="AR">AR</option>
								<option value="CA">CA</option>
								<option value="CO">CO</option>
								<option value="CT">CT</option>
								<option value="DE">DE</option>
								<option value="DC">DC</option>
								<option value="FM">FM</option>
								<option value="FL">FL</option>
								<option value="GA">GA</option>
								<option value="GU">GU</option>
								<option value="HI">HI</option>
								<option value="ID">ID</option>
								<option value="IL">IL</option>
								<option value="IN">IN</option>
								<option value="IA">IA</option>
								<option value="KS">KS</option>
								<option value="KY">KY</option>
								<option value="LA">LA</option>
								<option value="ME">ME</option>
								<option value="MH">MH</option>
								<option value="MD">MD</option>
								<option value="MA">MA</option>
								<option value="MI">MI</option>
								<option value="MN">MN</option>
								<option value="MS">MS</option>
								<option value="MO">MO</option>
								<option value="MT">MT</option>
								<option value="NE">NE</option>
								<option value="NV">NV</option>
								<option value="NH">NH</option>
								<option value="NJ">NJ</option>
								<option value="NM">NM</option>
								<option value="NY">NY</option>
								<option value="NC">NC</option>
								<option value="ND">ND</option>
								<option value="MP">MP</option>
								<option value="OH">OH</option>
								<option value="OK">OK</option>
								<option value="OR">OR</option>
								<option value="PW">PW</option>
								<option value="PA">PA</option>
								<option value="PR">PR</option>
								<option value="RI">RI</option>
								<option value="SC">SC</option>
								<option value="SD">SD</option>
								<option value="TN">TN</option>
								<option value="TX">TX</option>
								<option value="UT">UT</option>
								<option value="VT">VT</option>
								<option value="VI">VI</option>
								<option value="VA">VA</option>
								<option value="WA">WA</option>
								<option value="WV">WV</option>
								<option value="WI">WI</option>
								<option value="WY">WY</option>
							</select>                                                            @if ($errors->has('statename'))
										<span class="help-block">
											<strong>{{ $errors->first('statename') }}</strong>
										</span>
									@endif
							</div>
						</div>
						
						
						<div class="form-group {{ $errors->has('controlname') ? ' has-error' : '' }}">
							<label class="control-label col-md-3">Control Name:</label>
							<div class="col-md-4">
								<input style="text-transform: capitalize;" name="controlname" type="text" id="controlname" maxlength="7" class="form-control" value="{{$branch->controlname}}" />
								@if ($errors->has('controlname'))
										<span class="help-block">
											<strong>{{ $errors->first('controlname') }}</strong>
										</span>
									@endif
							</div>
						</div>	
						<div class="card-footer">
							<div class="row">
								<div class="col-md-8 col-md-offset-3">
									<input class="btn btn-primary icon-btn" type="submit" name="submit" value="save">
								</div>
							</div>
						</div>
						
					</form>
				</div>
			</div>
		</div>
	</div>
	
</div>

@endsection()