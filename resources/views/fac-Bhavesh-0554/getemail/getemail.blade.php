@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
<div class="content-wrapper">
        <div class="page-title">
		<h1>Email</h1>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-body">

					<div class="table-title">
						<h3></h3>
					
					</div>
@if ( session()->has('success') )
    <div class="alert alert-success alert-dismissable">{{ session()->get('success') }}</div>
@endif
@if ( session()->has('error') )
    <div class="alert alert-danger alert-dismissable">{{ session()->get('error') }}</div>
@endif  
					<div class="table-responsive">
						<table class="table table-hover table-bordered">
							<thead>
								<tr>
								    <th>Sr No.</th>
									<th>Name </th>
									<th>Email </th>
									<th>Get Email</th>
									<th>Password</th>
								</tr>
							</thead>
							<tbody>
							    @foreach($email as $a)
								<tr>
								
									<td><center>{{$loop->index+1}}</center></td>
									<td>{{ Auth::user()->fname.' '.Auth::user()->mname.' '.Auth::user()->lname}}</td>
									<td>{{$a->email}}</td>
									<td><center><a href="http://{{$a->access_address}}" class="btn-action btn-view-edit" target="_blank">Access</a></center></td>
									<td><input type="password" value="{{$a->password}}" disabled id="myInput_{{$a->id}}"> &nbsp; &nbsp; &nbsp;&nbsp;  <lable  onclick="myFunction{{$a->id}}()"><input type="checkbox">Show</label></td>
								</tr>
								
								
								<script>
function myFunction{{$a->id}}() {
    var x = document.getElementById("myInput_{{$a->id}}");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}
</script>
								@endforeach
                             </tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		
	</div>		
</div>


@endsection()