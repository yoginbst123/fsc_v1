@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
<style>
body{
    font-size:13px !important;
}
.subttl{margin-bottom:10px;}
    .subttl h4{margin:0px; padding:0px;}
    .form-control{height:30px; padding:0px 5px;}
    .text-left {text-align:left;}
    .padleft20{padding-left:12px!important;}
    .wdth100{width:140px!important;}
    .wdth160{width:145px!important;}
    .wdth200{width:200px!important;}
    input[type=date]::-webkit-inner-spin-button, 
input[type=date]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}
 .table-bordered, .table-bordered > tbody > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > td, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > thead > tr > th {
    vertical-align:middle;
}
input[type=date] {
    -moz-appearance:textfield;
}
.bold{font-weight:bold;}
.clear{clear:both;}
.padleftzero{padding-left:0px!important;}
.formbox{margin-bottom:10px;}
.salestaxckbox label{display:block;}
.salestaxckbox span{font-weight:700;}
.salestaxckbox label{margin-left:6px!important;}
.salestaxckbox table tr td{border-right:1px solid #ccc!important; padding-top:4px;}
.salestaxckbox table tr td:last-child{border-left:0px!important; border-right:0px!important;}

</style>
<div class="content-wrapper">
      <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>County --- Taxation / License</h1>
    </section>
    <!-- Main content -->
    <section class="content">
    <div class="row">
        <div class="col-md-12">
         	<div class="box box-success">
			     <div class="box-header">
                    <div class="box-tools pull-right">
                    </div>
                </div>
				<div class="col-md-12">
                    <!--<div class="Branch">-->
                    <!--    <h1>County - Taxation - License</h1>-->
                    <!--</div><br>-->
            <form method="post" action="{{route('taxstate.update',$state->id)}}" class="form-horizontal" id="form_valid" enctype="multipart/form-data">
                  {{csrf_field()}}{{method_field('PATCH')}}
                        <div class="col-md-12">
                            <div class="Branch subttl" style="background:#ffff99!important;">
                                <h4 style="font-weight:bold;">County Information</h4>
                            </div>
                        </div>
                        <div class="formbox">
                             <div class="col-md-2" style="width:12%;">
                            <div class=" {{ $errors->has('state') ? ' has-error' : '' }}">
                            <label class="control-label">State</label>
                             <select name="state" id="state_county_first" class="form-control fsc-input" style="width:100px;"> <br/>
                                            @if($state->state)
                                            <option value="{{$state->state}}">{{$state->state}}</option>
                                            @else
                                                        <option value="">---Select State---</option>
                                            @endif                                          
                                                           <option value="AK">AK</option>
                                                           <option value="AS">AS</option>
                                                           <option value="AZ">AZ</option>
                                                           <option value="AR">AR</option>
                                                           <option value="CA">CA</option>
                                                           <option value="CO">CO</option>
                                                           <option value="CT">CT</option>
                                                           <option value="DE">DE</option>
                                                           <option value="DC">DC</option>
                                                           <option value="FM">FM</option>
                                                           <option value="FL">FL</option>
                                                           <option value="GA">GA</option>
                                                           <option value="GU">GU</option>
                                                           <option value="HI">HI</option>
                                                           <option value="ID">ID</option>
                                                           <option value="IL">IL</option>
                                                           <option value="IN">IN</option>
                                                           <option value="IA">IA</option>
                                                           <option value="KS">KS</option>
                                                           <option value="KY">KY</option>
                                                           <option value="LA">LA</option>
                                                           <option value="ME">ME</option>
                                                           <option value="MH">MH</option>
                                                           <option value="MD">MD</option>
                                                           <option value="MA">MA</option>
                                                           <option value="MI">MI</option>
                                                           <option value="MN">MN</option>
                                                           <option value="MS">MS</option>
                                                           <option value="MO">MO</option>
                                                           <option value="MT">MT</option>
                                                           <option value="NE">NE</option>
                                                           <option value="NV">NV</option>
                                                           <option value="NH">NH</option>
                                                           <option value="NJ">NJ</option>
                                                           <option value="NM">NM</option>
                                                           <option value="NY">NY</option>
                                                           <option value="NC">NC</option>
                                                           <option value="ND">ND</option>
                                                           <option value="MP">MP</option>
                                                           <option value="OH">OH</option>
                                                           <option value="OK">OK</option>
                                                           <option value="OR">OR</option>
                                                           <option value="PW">PW</option>
                                                           <option value="PA">PA</option>
                                                           <option value="PR">PR</option>
                                                           <option value="RI">RI</option>
                                                           <option value="SC">SC</option>
                                                           <option value="SD">SD</option>
                                                           <option value="TN">TN</option>
                                                           <option value="TX">TX</option>
                                                           <option value="UT">UT</option>
                                                           <option value="VT">VT</option>
                                                           <option value="VI">VI</option>
                                                           <option value="VA">VA</option>
                                                           <option value="WA">WA</option>
                                                           <option value="WV">WV</option>
                                                           <option value="WI">WI</option>
                                                           <option value="WY">WY</option>
                                                        </select>
                          </div>
                        
                        </div>
                              <div class="col-md-2 padleftzero" style="width:12%">
                                   <label class="control-label">County Name</label> <br/>
                                    <select name="county" id="county" class="form-control fsc-input">
                                         <option value="">--Select--</option> 
                                            @foreach($countys as $cou)
                                            @if($state->county!='')
                                                <option VALUE="{{$cou->county_name}}"  @if($cou->county_name==$state->county) selected @endif>{{$cou->county_name}}</option>
                                            @else
                                                <input type="hidden" name="codess" id="codess" value="{{$cou->county_code}}">
                                                <option value="{{$cou->county_name}}">{{$cou->county_name}}</option> 
                                            @endif
                                            @endforeach
                                    </select>
                              </div>
                      
                        
                        <div class="col-md-2 padleftzero" style="width:10.5%;">
                             <label class="control-label">County Code</label>
                                <input type="text" name="countycode" id="countycode" class="form-control" value="{{$state->countycode}}"  placeholder="County Code" style="width:90px;"  minlength="2" maxlength="6" readonly/> 
                             
                        </div>
                        
                         <div class="col-md-3 padleftzero" style="width:18%;">
                             <label class="control-label">County Authority Name</label>
                                <input type="text" name="county_authority_name" id="county_authority_name" class="form-control" value="{{$state->county_authority_name}}"  placeholder="County Authority Name"/> 
                             
                        </div>
                        
                        <div class="col-md-4 padleftzero" style="width:31%">
                             <label class="control-label">County Website</label>
                            <input type="text" class="form-control" placeholder="County Website" id="county_website" name="county_website" value="{{$state->county_website}}"/>
                        </div>
                        
                        <div class="col-md-2 padleftzero" style="width:16%">
                         <label class="control-label">County Telephone</label>
                          <input type="text" class="form-control" placeholder="County Telephone" id="county_telephone" name="county_telephone" value="{{$state->county_telephone}}">
                        </div>
                        
                        <div class="clear"></div>
                        </div>
                         <div class="formbox mrbtmzero">
                            <div class="col-md-6" style="width:46%">
                                <label class="control-label">County Address</label>
                              <input type="text" class="form-control" placeholder="County Address" id="county_address" name="county_address" value="{{$state->county_address}}"/>
                            </div>
                             <div class="col-md-2 padleftzero" style="width:16%">
                                  <label class="control-label">City</label>
                              <input type="text" class="form-control" placeholder="City" id="county_city" name="county_city" value="{{$state->county_city}}"/>
                            </div>
                             <div class="col-md-2 padleftzero" style="width:10%">
                                  <label class="control-label">State</label>
                                <input type="text" class="form-control" placeholder="State" id="county_state" name="county_state" value="{{$state->state}}" readonly/>
                            </div>
                            <div class="col-md-2 padleftzero" style="width:11%">
                                 <label class="control-label">Zip Code</label>
                                <input type="text" class="form-control" placeholder="ZIP Code" id="county_zip" name="county_zip" value="{{$state->county_zip}}"/>
                            </div>
                            <div class="col-md-2 padleftzero" style="width:15%">
                                 <label class="control-label">FAX</label>
                                <input type="text" class="form-control" placeholder="Fax" id="county_fax" name="county_fax" value="{{$state->county_fax}}"/>
                            </div>
                             <div class="clear"></div>
                           
                        </div>
                        
                       <!-- <div style="border-bottom:1px solid #ccc; margin:10px 0px; width:100%; display:inline-block;"></div -->
                        
                        <div class="clear"></div>
                        
                        <div class="col-md-12">
                        <div class="Branch subttl"  style="background:#00b1ce; border-color:#016373;">
                             <h4 class="text-left padleft20 bold" style="color:#fff;">Sales Tax Information </h4>
                        </div>
                        
                     <!--   <div class="col-md-1">-->
    	                <!--</div>-->
                        <div class="col-md-12">
                                <table class="table table-bordered table-striped" id="mytable">
                                     <tr id="row1">
                                         <!--<th>#Sr.</th>-->
                                         <th>Month</th>
                                         <th>Year</th>
                                         <th width="30%">State Sales Tax Rate<span style="font-size: 14px; padding:0px;visibility:hidden;"> + </span>County Sales Tax Rate</th>
                                         <th>County Sales Rate Type</th>
                                         <th style="padding-bottom: 6px !important;"><div class=""><input type="button" class="add-row btn btn-primary" value="+"></th>
                                     </tr>
                                     <tr id="row2">
                                          <?php $i=0;?>
                                          @foreach($county as $taxcounty)  
                                          <!--<td>-->
                                          <!--    <div class="col-md-3"><?php //echo $i;?></div>-->
                                          <!--</td>-->
                                         <td>
                                             <div class="">
                                                <input type="hidden" id="sid_<?php echo $i;?>" name="sid[]" value="{{$taxcounty->id}}">
                                                 <input type="hidden" id="countysalesid_<?php echo $i;?>" name="countysalesid" value="{{$taxcounty->countysalesid}}">
                                                <select name="county_tax_month[]" id="county_tax_month_<?php echo $i;?>" class="form-control fsc-input" style="width:80px;margin:auto">
                                                                           <option value="">Select month</option>
                                                                           <option value="01" @if($taxcounty->county_tax_month=='01') selected @endif>01</option>
                                                                           <option value="02" @if($taxcounty->county_tax_month=='02') selected @endif>02</option>
                                                                           <option value="03" @if($taxcounty->county_tax_month=='03') selected @endif>03</option>
                                                                           <option value="04" @if($taxcounty->county_tax_month=='04') selected @endif>04</option>
                                                                           <option value="05" @if($taxcounty->county_tax_month=='05') selected @endif>05</option>
                                                                           <option value="06" @if($taxcounty->county_tax_month=='06') selected @endif>06</option>
                                                                           <option value="07" @if($taxcounty->county_tax_month=='07') selected @endif>07</option>
                                                                           <option value="08" @if($taxcounty->county_tax_month=='08') selected @endif>08</option>
                                                                           <option value="09" @if($taxcounty->county_tax_month=='09') selected @endif>09</option>
                                                                           <option value="10" @if($taxcounty->county_tax_month=='10') selected @endif>10</option>
                                                                           <option value="11" @if($taxcounty->county_tax_month=='11') selected @endif>11</option>
                                                                           <option value="12" @if($taxcounty->county_tax_month=='12') selected @endif>12</option>
                                                                        </select>
                                                
                                             </div>
                                         </td>
                                         <td>
                                             <div class="">
                                                <select name="county_tax_year[]" id="county_tax_year_<?php echo $i;?>" class="form-control fsc-input" style="width:80px;margin:auto;">
                                                                           <option value="">Select year</option>
                                                                            <option value="2020" @if($taxcounty->county_tax_year=='2020') selected @endif>2020</option>
                                                                           <option value="2021" @if($taxcounty->county_tax_year=='2021') selected @endif>2021</option>
                                                                           <option value="2022" @if($taxcounty->county_tax_year=='2022') selected @endif>2022</option>
                                                                           <option value="2023" @if($taxcounty->county_tax_year=='2023') selected @endif>2023</option>
                                                                           <option value="2024" @if($taxcounty->county_tax_year=='2024') selected @endif>2024</option>
                                                                           <option value="2025" @if($taxcounty->county_tax_year=='2025') selected @endif>2025</option>
                                                                        </select>
                                              
                                             </div>
                                         </td>
                                         <td>
                                            <div class="" style="width:100%;display: inline-flex;">
                                                <input type="text" name="county_tax_personal_rate[]" style="width:120px;margin: auto;margin-right: 10px;" value="{{$taxcounty->county_tax_personal_rate}}" id="county_tax_personal_rate_<?php echo $i;?>" class="form-control txtinput_1"  placeholder="Rate"/>
                                                <h3 style="margin:0px;"> + </h3>
                                                <input type="text" name="" value="" id="" style="width:120px;margin: auto;margin-left: 10px;" class="form-control txtinput_1"  placeholder="county"/>
                                            </div>
                                         </td>
                                         
                                        <td style="padding-top:0px!important; padding-right:0px!important;">
                                                <div class="salestaxckbox">
                                                   <table style="margin:auto;">
                                                       <tr>
                                                           <td>
                                                               <span>M</span>
                                                            @if ($taxcounty->county_marta == '1')
                                                                <input type="checkbox" class="county_marta_<?php echo $i;?>" name="county_marta[]" id="county_marta_<?php echo $i;?>" value="1" checked>
                                                            @else
                                                                 <input type="checkbox" class="county_marta_<?php echo $i;?>" name="county_marta[]" id="county_marta_<?php echo $i;?>" value="1">
                                                            @endif
                                                                <label for="county_marta_<?php echo $i;?>" data-toggle="tooltip" data-placement="top" title="MARTA"></label>
                                                               
                                                                
                                                            </td>
                                                            <td>
                                                               
                                                               <span>L</span>
                                                            
                                                                <input type="checkbox"  class="county_lost_<?php echo $i;?>" name="county_lost[<?php echo $i;?>]" id="county_lost_<?php echo $i;?>"  value="1"  <?php if($taxcounty->county_lost == '1') { echo  'checked';}?>>
                                                                <label for="county_lost_<?php echo $i;?>" data-toggle="tooltip" data-placement="top" title="Lost"> </label> 
                                                                
                                                            </td>
                                                            <td>
                                                                <span>E</span>
                                                                <input type="checkbox" class="county_educational_<?php echo $i;?>" name="county_educational[<?php echo $i;?>]" id="county_educational_<?php echo $i;?>" 
                                                                value="1" <?php if($taxcounty->county_educational == '1') { echo  'checked';}?>>
                                                                <label for="county_educational_<?php echo $i;?>" data-toggle="tooltip" data-placement="top" title="Educational"></label>
                                                            </td>
                                                            
                                                            <td>
                                                                <span>H</span>
                                                            @if ($taxcounty->county_host == '1')
                                                                <input type="checkbox" class="county_host_<?php echo $i;?>" name="county_host[]" id="county_host_<?php echo $i;?>" value="1" checked>
                                                            @else
                                                                 <input type="checkbox" class="county_host_<?php echo $i;?>" name="county_host[]" id="county_host_<?php echo $i;?>" value="1">
                                                            @endif
                                                                <label for="county_host_<?php echo $i;?>" data-toggle="tooltip" data-placement="top" title="HOST or EHOST"></label>
                                                            </td>
                                                            
                                                            <td>
                                                                <span>S</span>
                                                            
                                                                <input type="checkbox" class="county_splost_<?php echo $i;?>" name="county_splost[<?php echo $i;?>]" id="county_splost_<?php echo $i;?>" value="1" <?php if($taxcounty->county_splost == '1') { echo  'checked';}?>>
                                                        
                                                                <label for="county_splost_<?php echo $i;?>" data-toggle="tooltip" data-placement="top" title="SPLOST"></label>
                                                            </td>
                                                             
                                                             <td>
                                                                <span>O</span>
                                                            @if ($taxcounty->county_other == '1')
                                                                <input type="checkbox" class="county_other_<?php echo $i;?>" name="county_other[]" id="county_other_<?php echo $i;?>" value="1" checked>
                                                            @else
                                                                 <input type="checkbox" class="county_other_<?php echo $i;?>" name="county_other[]" id="county_other_<?php echo $i;?>" value="1">
                                                            @endif
                                                                <label for="county_other_<?php echo $i;?>" data-toggle="tooltip" data-placement="top" title="Other"></label>
                                                            </td>
                                                             
                                                            <td> 
                                                            <span>T</span>
                                                            @if ($taxcounty->county_tsplost == '1')
                                                                <input type="checkbox" class="county_tsplost_<?php echo $i;?>" name="county_tsplost[]" id="county_tsplost_<?php echo $i;?>" value="1" checked>
                                                            @else
                                                                 <input type="checkbox" class="county_tsplost_<?php echo $i;?>" name="county_tsplost[]" id="county_tsplost_<?php echo $i;?>" value="1">
                                                            @endif
                                                                <label for="county_tsplost_<?php echo $i;?>" data-toggle="tooltip" data-placement="top" title="TSPLOST 1"></label>
                                                            </td>
                                                            
                                                            
                                                                
                                                            
                                                                
                                                            <td>
                                                                 <span>T2</span>
                                                            @if ($taxcounty->county_tsplost2 == '1')
                                                                <input type="checkbox" class="county_tsplost2<?php echo $i;?>" name="county_tsplost2[]" id="county_tsplost2_<?php echo $i;?>" value="1" checked>
                                                            @else
                                                                 <input type="checkbox" class="county_tsplost2<?php echo $i;?>" name="county_tsplost2[]" id="county_tsplost2_<?php echo $i;?>" value="1">
                                                            @endif
                                                                <label for="county_tsplost2_<?php echo $i;?>" data-toggle="tooltip" data-placement="top" title="TSPLOST 2">&nbsp;</label>
                                                            <td>
                                                                
                                                            <td>
                                                                <span>m</span>
                                                                
                                                                <input type="checkbox" class="county_m" name="county_m" id="county_m" value="1" checked>
                                                                <label for="county_m" data-toggle="tooltip" data-placement="top" title="Local MARTA (Atlanta MARTA)">&nbsp;</label>
                                                            <td>
                                                                
                                                            <td>
                                                                <span>Ta</span>
                                                            @if ($taxcounty->county_atlantata == '1')
                                                                <input type="checkbox" class="county_atlantata_<?php echo $i;?>" name="county_atlantata[]" id="county_atlantata_<?php echo $i;?>" value="1" checked>
                                                            @else
                                                                 <input type="checkbox" class="county_atlantata_<?php echo $i;?>" name="county_atlantata[]" id="county_atlantata_<?php echo $i;?>" value="1">
                                                            @endif
                                                                <label for="county_atlantata_<?php echo $i;?>" data-toggle="tooltip" data-placement="top" title="Atlanta TSPLOST">&nbsp;</label>
                                                            <td>
                                                                
                                                            <td>
                                                                <span>Tf</span>
                                                            @if ($taxcounty->county_filtontf == '1')
                                                                <input type="checkbox" class="county_filtontf_<?php echo $i;?>" name="county_filtontf[]" id="county_filtontf_<?php echo $i;?>" value="1" checked>
                                                            @else
                                                                 <input type="checkbox" class="county_filtontf_<?php echo $i;?>" name="county_filtontf[]" id="county_filtontf_<?php echo $i;?>" value="1">
                                                            @endif
                                                                <label for="county_filtontf_<?php echo $i;?>" data-toggle="tooltip" data-placement="top" title="Fulton TSPLOST">&nbsp;</label>
                                                            <td>
                                                        </tr>
                                                   </table>
                                                </div>
                                            </td>
                                         
                                         <th> <div class=""> <a href="{{ route('taxstate.countydelete',$taxcounty->id) }}" class="btn btn-danger" value="-"> -</a></div></th>
                                     </tr>
                                     <?php $i++;?>
                                     	@endforeach
                                </table>
                            </div>
             
                        
                    </div>
                       
                        <div class="clear"></div>
                        
                        <div class="col-md-12">
                                <div class="Branch subttl" style="background:#deeef7; border-color:#a3d3ef;">
                                    <h4 class="text-left padleft20 bold">Personal Property Tax Information </h4>
                                </div>
                                <div class="row" style="padding-left:15px;">
                                     <div class="col-md-2" style="width:13%">
                                          <label class="control-label">Form No.</label>
                                         <input type="text" class="form-control" placeholder="Form No" id="personal_form_number" name="personal_form_number" value="{{$state->personal_form_number}}" minlength="2" maxlength="50"/>
                                     </div>
                                     <div class="col-md-3 padleftzero" style="width:27%;">
                                          <label class="control-label">Form Name</label>
                                         <input type="text" class="form-control" placeholder="Form Name" id="personal_form_name" name="personal_form_name" value="{{$state->personal_form_name}}"  minlength="2" maxlength="50"/>
                                     </div>
                                     
                                      <div class="col-md-2 padleftzero" style="width:100px;">
                                           <label class="control-label">Tax Year</label>
                                        <select name="personal_tax_year" id="personal_tax_year" class="form-control fsc-input">
                                            <option value="">Select year</option>
                                            <option value="2020" @if($state->personal_tax_year==='2020') selected @endif>2020</option>
                                            <option value="2021" @if($state->personal_tax_year==='2021') selected @endif>2021</option>
                                            <option value="2022" @if($state->personal_tax_year==='2022') selected @endif>2022</option>
                                            <option value="2023" @if($state->personal_tax_year==='2023') selected @endif>2023</option>
                                            <option value="2024" @if($state->personal_tax_year==='2024') selected @endif>2024</option>
                                            <option value="2025" @if($state->personal_tax_year==='2025') selected @endif>2025</option>
                                        </select>
                                      </div>
                                     
                                      <div class="col-md-3 padleftzero" style="width:250px;">
                                           <label class="control-label">Filling Frequency</label>
                                         <input type="text" class="form-control" name="personal_filling_frequency" id="personal_filling_frequency" value="Annually" readonly>
                                            
                                                <!--<option value="">Select</option>-->
                                                <!--<option value="Annually" @if($state->personal_filling_frequency=='Annually') selected @endif>Annually</option>-->
                                                <!--<option value="Quarterly" @if($state->personal_filling_frequency=='Quarterly') selected @endif>Quarterly</option>-->
                                                <!--<option value="Monthly" @if($state->personal_filling_frequency=='Monthly') selected @endif>Monthly</option>-->
                                                
                                         <!--</select>-->
                                     </div>
                                    
                                     <div class="col-md-2 padleftzero" style="width:12%;">
                                         <label class="control-label">Due Date</label>
                                        <!--<input type="text" class="date-own form-control" id="personal_due_date" name="personal_due_date" value="{{$state->personal_due_date}}" placeholder="mm-dd-yyyy" readOnly/>-->
                                        <input type="text" class="form-control" id="personal_due_date" name="personal_due_date" value="Apr-01-2021" placeholder="mm-dd-yyyy" readOnly/>
                                     </div>
                            
                                     <div class="col-md-2 padleftzero" style="width:14%;">
                                         <label class="control-label">Telephone</label>
                                            <input type="text" class="form-control" placeholder="Telephone" id="personal_telephone" name="personal_telephone" value="{{$state->personal_telephone}}"/>
                                     </div>
                                </div>
                        
                                <div class="clear"></div>
                                <div class="row" style="padding-left:15px; margin-top:15px; margin-bottom:15px;">
                                    <div class="col-md-2" >
                                        <label class="control-label">Person Name</label>
                                      <input type="text" class="form-control" placeholder="Contact Person Name" id="personal_contact_name" name="personal_contact_name" value="{{$state->personal_contact_name}}"  minlength="2" maxlength="50"/>
                                    </div>
                                    <div class="col-md-2 padleftzero" style="width:14%;">
                                        <label class="control-label">Contact No.</label>
                                        <input type="text" class="form-control" placeholder="Contact Telephone" id="personal_secondary_telephone" name="personal_secondary_telephone" value="{{$state->personal_secondary_telephone}}"/>
                                    </div>
                                    <div class="col-md-4 padleftzero" style="width:25%;">
                                        <label class="control-label">Email</label>
                                        <input type="email" class="form-control" placeholder="Email" name="personal_email" value="{{$state->personal_email}}"/>
                                    </div>
                                     <div class="col-md-3 padleftzero" style="width:22%;">
                                         <label>Website</label> 
                                    <input type="text" class="form-control" placeholder="Website url" name="property_website" id="property_website" value="{{$state->property_website}}"/>
                                </div>
                                    <div class="col-md-2 padleftzero" style="width:10%;">
                                        
                                        <label>File Online</label> 
                                       
                                            <select class="form-control" name="personal_file">
                                                <option value="">---Select---</option>
                                                <option value="Yes"  @if($state->personal_file=='Yes') selected @endif>Yes</option>
                                                <option value="No"  @if($state->personal_file=='No') selected @endif>No</option>
                                            </select>
                                      
                                    </div>
                                   
                                 <div class="col-md-2 padleftzero" style="width:11%;">
                                        <label>Payment Online</label> 
                                            <select class="form-control" name="personal_payment_online">
                                                <option value="">---Select---</option>
                                                <option value="Yes" @if($state->personal_payment_online=='Yes') selected @endif>Yes</option>
                                                <option value="No" @if($state->personal_payment_online=='No') selected @endif>No</option>
                                            </select>
                                </div>
                                   
                                </div>
                                <div class="clear"></div>
                                
                                
                               
                             
                              <div class="clear"></div>
                                <label class="control-label col-md-1" style="margin-left:10px;">Note :</label>
                                <div class="col-md-10" style="padding-right:0px; width:89.2%;padding-bottom:15px !important;">
                                    <input type="text" name="personal_tax_note" id="personal_tax_note" value="{{$state->personal_tax_note}}" class="form-control"  placeholder="Note" /> 
                                </div>
                                <br>
                            </div>
                     
                     <div class="clear"></div>
                        
                        <div class="col-md-12">
                        <div class="Branch subttl" style="background:#deeef7; border-color:#a3d3ef;">
                             <h4 class="text-left padleft20 bold">Property Tax Information </h4>
                        </div>
                      
                    
                        <div class="row" style="padding-left:15px;">
                            <div class="col-md-2" style="width:13%;">
                                <label >Form No.</label> 
                                 <input type="text" class="form-control" placeholder="Form No." id="property_form_number" name="property_form_number" value="{{$state->property_form_number}}" />
                            </div>
                            <div class="col-md-3 padleftzero" style="width:27%;">
                                <label >Form Name</label> 
                                 <input type="text" class="form-control" placeholder="Form Name" id="property_form_name" name="property_form_name" value="{{$state->property_form_name}}"  minlength="2" maxlength="50"/>
                            </div>
                            
                            <!--<div class="col-md-2 padleftzero" style="width:100px;">-->
                            <!--    <select name="property_tax_year" id="property_tax_year" class="form-control fsc-input">-->
                            <!--        <option value="">---Select year---</option>-->
                            <!--        <option value="2020" @if($state->property_tax_year==='2020') selected @endif>2020</option>-->
                            <!--        <option value="2021" @if($state->property_tax_year==='2021') selected @endif>2021</option>-->
                            <!--        <option value="2022" @if($state->property_tax_year==='2022') selected @endif>2022</option>-->
                            <!--        <option value="2023" @if($state->property_tax_year==='2023') selected @endif>2023</option>-->
                            <!--        <option value="2024" @if($state->property_tax_year==='2024') selected @endif>2024</option>-->
                            <!--        <option value="2025" @if($state->property_tax_year==='2025') selected @endif>2025</option>-->
                            <!--    </select>-->
                            <!--  </div>-->
                            
                            
                            
                             <div class="col-md-3 padleftzero" style="width:140px;">
                                 <label >Filling Frequency</label> 
                                 <select class="form-control" name="property_filling_frequency" id="property_filling_frequency">
                                        <option value="">Select</option>
                                        <option value="Annually" @if($state->property_filling_frequency=='Annually') selected @endif>Annually</option>
                                        <option value="Quarterly" @if($state->property_filling_frequency=='Quarterly') selected @endif>Quarterly</option>
                                        <option value="Monthly" @if($state->property_filling_frequency=='Monthly') selected @endif>Monthly</option>
                                        
                                 </select>
                             </div>
                            
                             <div class="col-md-2 padleftzero"  style="width:12%;">
                                  <label >Due Date</label>
                                 <input type="text" class="form-control" id="property_due_date" name="property_due_date" value="{{$state->property_due_date}}" placeholder="mm-dd-yyyy" readOnly/>
                            </div>
                            
                            <div class="col-md-2 padleftzero"  style="width:14%;">
                                 <label >Telephone</label>
                                 <input type="text" class="form-control" placeholder="Telephone" id="property_telephone" name="property_telephone" value="{{$state->property_telephone}}"/>
                            </div>
                             <div class="col-md-3 padleftzero" style="width:20%;">
                                  <label >Person Name</label>
                                 <input type="text" class="form-control" placeholder="Contact Person Name" id="property_contact_name" name="property_contact_name" value="{{$state->property_contact_name}}"  minlength="2" maxlength="50"/>
                             </div>
                        </div>
                        <div class="clear"></div>
                        
                        <div class="row" style="padding-left:15px; margin-top:15px; margin-bottom:15px;">
                            
                             
                             <div class="col-md-2" style="width:20%;">
                                  <label >Contact No.</label>
                                 <input type="text" class="form-control" placeholder="Contact Telephone" id="property_secondary_telephone" name="property_secondary_telephone" value="{{$state->property_secondary_telephone}}"/>
                             </div>
                             
                             <div class="col-md-3 padleftzero" style="width:31%;">
                              <label >Email</label>
                                 <input type="email" class="form-control" placeholder="Email" name="property_email" value="{{$state->property_email}}"/>
                           
                        </div>
                              <div class="col-md-3 padleftzero">
                             <label >Website</label>
                            <input type="text" class="form-control" placeholder="Website url" name="property_website" id="property_website" value="{{$state->property_website}}"/>
                        </div>
                        <div class="col-md-3 padleftzero" style="width:23%;">
                            <label>File Online</label> 
                                <select class="form-control" name="property_file">
                                    <option value="">---Select---</option>
                                    <option value="Yes" @if($state->property_file=='Yes') selected @endif>Yes</option>
                                    <option value="No" @if($state->property_file=='No') selected @endif>No</option>
                                </select>
                        </div>
                        
                             
                        <div class="clear"></div>
                       
                         
                            <!--<div class="col-md-3 padleftzero" style="width:23%;">-->
                            <!--        <label class="col-md-5" style="padding-left: 0px;  padding-right: 0; width: 45%; padding-top:5px;font-size:13px !impotant;">Payment Online</label> -->
                            <!--        <div class="col-md-6" style="padding-right:0px;">-->
                            <!--            <select class="form-control" name="property_payment_online" >-->
                            <!--                <option value="">---Select---</option>-->
                            <!--                <option value="Yes" @if($state->property_payment_online=='Yes') selected @endif>Yes</option>-->
                            <!--                <option value="No" @if($state->property_payment_online=='No') selected @endif>No</option>-->
                            <!--            </select>-->
                            <!--        </div>-->
                            <!--</div> -->
                         </div>
                         
                        <div class="row" style="padding-left:15px; margin-top:15px; margin-bottom:15px;">
                            <div class=" col-md-12">
                            <label class="control-label col-md-1" style="margin-left:10px;">Note :</label>
                            <div class="col-md-10" style="padding-right:0px; width:89.5%;">
                                <input type="text" name="property_tax_note" id="property_tax_note" class="form-control" value="{{$state->property_tax_note}}"  placeholder="Note" /> 
                            </div>
                            </div>
    	                </div>
    	                   
    	                   
    	                   
    	                   
                       
          
        </div>
                     
                        <div class="clear"></div>
                        
                          <div class="col-md-12">
                                <div class="Branch subttl" style="background:#f7e4ba; border-color:#e0c588;">
                                    <h4 class="text-left padleft20 bold">Business Licence Information</h4>
                                </div>
                                <div class="row" style="padding-left:15px;">
                                     <div class="col-md-2" style="width:13%">
                                          <label>Form No.</label> 
                                         <input type="text" class="form-control" placeholder="Form No" id="licence_form_number" name="licence_form_number" value="{{$state->licence_form_number}}" minlength="2" maxlength="50"/>
                                     </div>
                                     <div class="col-md-3 padleftzero" style="width:27%;">
                                         <label>Form Name</label> 
                                         <input type="text" class="form-control" placeholder="Form Name" id="licence_form_name" name="licence_form_name" value="{{$state->licence_form_name}}"  minlength="2" maxlength="50"/>
                                     </div>
                                     
                                      <div class="col-md-2 padleftzero" style="width:100px;">
                                           <label>Tax Year</label> 
                                        <select name="licence_tax_year" id="licence_tax_year" class="form-control fsc-input">
                                            <option value="">---Select year---</option>
                                            <option value="2020" @if($state->licence_tax_year==='2020') selected @endif>2020</option>
                                            <option value="2021" @if($state->licence_tax_year==='2021') selected @endif>2021</option>
                                            <option value="2022" @if($state->licence_tax_year==='2022') selected @endif>2022</option>
                                            <option value="2023" @if($state->licence_tax_year==='2023') selected @endif>2023</option>
                                            <option value="2024" @if($state->licence_tax_year==='2024') selected @endif>2024</option>
                                            <option value="2025" @if($state->licence_tax_year==='2025') selected @endif>2025</option>
                                        </select>
                                      </div>
                                     
                                      <div class="col-md-3 padleftzero" style="width:250px;">
                                           <label>Filling Frequency</label> 
                                         <select class="form-control" name="licence_filling_frequency" id="licence_filling_frequency">
                                                <option value="">Select</option>
                                                <option value="Annually" @if($state->licence_filling_frequency=='Annually') selected @endif>Annually</option>
                                                <option value="Quarterly" @if($state->licence_filling_frequency=='Quarterly') selected @endif>Quarterly</option>
                                                <option value="Monthly" @if($state->licence_filling_frequency=='Monthly') selected @endif>Monthly</option>
                                                
                                         </select>
                                     </div>
                                   
                                     <div class="col-md-2 padleftzero" style="width:12%;">
                                          <label>Due Date</label> 
                                        <!--<input type="text" class="date-own form-control" id="personal_due_date" name="personal_due_date" value="{{$state->personal_due_date}}" placeholder="mm-dd-yyyy" readOnly/>-->
                                        <input type="text" class="form-control" id="licence_due_date" name="licence_due_date" value="{{$state->licence_due_date}}" placeholder="mm-dd-yyyy" readOnly/>
                                     </div>
                            
                                     <div class="col-md-2 padleftzero" style="width:14%;">
                                         <label>Telephone</label> 
                                            <input type="text" class="form-control" placeholder="Telephone" id="licence_telephone" name="licence_telephone" value="{{$state->licence_telephone}}"/>
                                     </div>
                                </div>
                        
                                <div class="clear"></div>
                                <div class="row" style="padding-left:15px; margin-top:15px; margin-bottom:15px;">
                                    <div class="col-md-2" >
                                        <label>Person Name</label> 
                                      <input type="text" class="form-control" placeholder="Contact Person Name" id="licence_contact_name" name="licence_contact_name" value="{{$state->licence_contact_name}}"  minlength="2" maxlength="50"/>
                                    </div>
                                    
                                    <div class="col-md-2 padleftzero" style="width:14%;">
                                        <label>Contact No.</label> 
                                        <input type="text" class="form-control" placeholder="Contact Telephone" id="licence_secondary_telephone" name="licence_secondary_telephone" value="{{$state->licence_secondary_telephone}}"/>
                                    </div>
                                    
                                    <div class="col-md-4 padleftzero" style="width:25%;">
                                        <label>Email</label> 
                                        <input type="email" class="form-control" placeholder="Email" name="licence_email" value="{{$state->licence_email}}"/>
                                    </div>
                                    
                                    
                                     <div class="col-md-3 padleftzero" style="width:22%;">
                                    <label>Website</label> 
                                    <input type="text" class="form-control" placeholder="Website url" name="licence_website" id="licence_website" value="{{$state->licence_website}}"/>
                                </div>
                                <div class="col-md-2 padleftzero" style="width:10%;">
                                        <label>File Online</label> 
                                        
                                            <select class="form-control" name="licence_file" id="licence_file">
                                                <option value="">---Select---</option>
                                                <option value="Yes"  @if($state->licence_file=='Yes') selected @endif>Yes</option>
                                                <option value="No"  @if($state->licence_file=='No') selected @endif>No</option>
                                            </select>
                                       
                                    </div>
                                     <div class="col-md-2 padleftzero" style="width:11%;">
                                        <label >Payment Online</label> 
                                            <select class="form-control" name="licence_payment_online" id="licence_payment_online">
                                                <option value="">---Select---</option>
                                                <option value="Yes" @if($state->licence_payment_online=='Yes') selected @endif>Yes</option>
                                                <option value="No" @if($state->licence_payment_online=='No') selected @endif>No</option>
                                            </select>
                                       
                                </div>
                                   
                                </div>
                                <div class="clear"></div>
                               
                                
                               
                             
                              <div class="clear"></div>
                                <label class="control-label col-md-1" style="margin-left:10px;margin-top:5px;text-align:right;">Note :</label>
                                <div class="col-md-11" style="padding-right:0px; width:89.2%;padding-bottom:15px !important;">
                                    <input type="text" name="licence_note" id="licence_note" value="{{$state->licence_note}}" class="form-control"  placeholder="Note" /> 
                                </div>
                                <br>
                                
                                 <div class="card-footer">
                            <div class="col-md-2 col-md-offset-3">
        						<input class="btn_new_save btn-primary1" style="margin-left:-5%" type="submit" name="submit" value="Save">
        					</div>
        					<div class="col-md-2 row">
        						<a class="btn_new_cancel" href="{{url('fac-Bhavesh-0554/taxstate')}}">Cancel</a> 
        					</div>
                        </div>
                                
                                
                                
                            </div>
                     
              </form>            
         </div>
      </div>
   </div>
     </section>
<!--</div>-->


<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>

<script>

      $('document').ready(function(){
         $('#state_county_first').on('change', function(){
            var statecounty = $("#state_county_first").val(); 
            var fullcounty=statecounty;
            console.log(fullcounty);           
            $.get('{!!URL::to('getCountytaxstate')!!}?filecounty='+fullcounty, function(data)
            {  
                //console.log(data);
                if(data == "")
                {
                    
                }
                else
                {
                    $('#county').html(data);
                  
                }
                

                
            });
            
        });
 
    });
</script>

<script>

    $('document').ready(function(){
         $('#county').on('change', function(){
            var id1 = $("#county").val(); 
            var fullid=id1;
            console.log(fullid);           
            $.get('{!!URL::to('getCountycodetax')!!}?filename='+fullid, function(data)
            {  
                console.log(data);
                if(data == "")
                {
                    
                }
                else
                {
                    $('#countycode').val(data.county_code);
                }
                
               
            
                
            });
            
        });
 
    });
</script>


<script type="text/javascript">

$(document).ready(function()
 {
  (function($) {
    var minNumber = -100;
    var maxNumber = 100;
      $('.spinner .btn:first-of-type').on('click', function() {
        if ($('.spinner input').val() == maxNumber) {
          return false;
        } else {
          $('.spinner input').val(parseInt($('.spinner input').val(), 10) + 5 + '%');
        }
      });

  $('.txtinput_1').on("blur", function() {
    var inputVal = parseFloat($(this).val().replace('%', '')) || 0
    if (minNumber > inputVal) {
      inputVal = -100;
    } else if (maxNumber < inputVal) {
      inputVal = 100;
    }
    $(this).val(inputVal + '.00%');
  });

      $('.spinner .btn:last-of-type').on('click', function() {
        if ($('.spinner input').val() == minNumber) {
          return false;
        } else {
          $('.spinner input').val(parseInt($('.spinner input').val(), 10) - 5 + '%');
        }
      });
})(jQuery);
  
    
        var k='<?php echo $wordCount?>';
        //var k=1;
        $(".add-row").click(function(){
            k++;
           // var markup = "<tr><td><div class='col-md-10'><select name='county_tax_month[]' id='county_tax_month' class='form-control fsc-input' style='width:100px;'><option>---Select month---</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option></select><div></td><td><div class='col-md-6'><select name='county_tax_year[]' id='county_tax_year' class='form-control fsc-input' style='width:100px;'><option>---Select year---</option><option value='2020'>2020</option><option value='2021'>2021</option><option value='2022'>2022</option><option value='2023'>2023</option><option value='2024'>2024</option><option value='2025'>2025</option></select></div></td><td><div class='col-md-3' style='width:100px;'><input type='text' name='county_tax_personal_rate[]' id='county_tax_personal_rate' class='form-control txtinput_"+k+"'  placeholder='Rate'/></div></td><td><div class='col-md-6'> <button type='button' class='delete-row btn btn-danger' onClick='deleterow();'>-</button></div></td></tr>";
            //markup = "<tr><td><div class='col-md-10'><select name='county_tax_month[]' id='county_tax_month_"+k+"' class='form-control fsc-input' style='width:100px;'><option>---Select month---</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option></select><div></td><td><div class='col-md-6'><select name='county_tax_year[]' id='county_tax_year["+k+"]' class='form-control fsc-input' style='width:100px;'><option>---Select year---</option><option value='2020'>2020</option><option value='2021'>2021</option><option value='2022'>2022</option><option value='2023'>2023</option><option value='2024'>2024</option><option value='2025'>2025</option></select></div></td><td><div class='col-md-3' style='width:100px;'><input type='text' name='county_tax_personal_rate[]' id='county_tax_personal_rate_"+k+"' class='form-control txtinput_"+k+"'  placeholder='Rate'/><td style='padding-top:0px!important; padding-right:0px!important;'><div class='salestaxckbox'><table><tr><td><span>L</span><input type='checkbox' name='county_lost[]' id='county_lost_"+k+"' value='1'><label for='county_lost_"+k+"' data-toggle='tooltip' data-placement='top' title='Lost'> </label></td><td><span>E</span><input type='checkbox' name='county_educational[]' id='county_educational_"+k+"' value='1'><label for='county_educational_"+k+"' data-toggle='tooltip' data-placement='top' title='Educational'></label></td><td><span>S</span><input type='checkbox' name='county_splost[]' id='county_splost_"+k+"' value='1'><label for='county_splost_"+k+"' data-toggle='tooltip' data-placement='top' title='SPLOST'></label></td><td><span>T</span><input type='checkbox' name='county_tsplost[]' id='county_tsplost_"+k+"' value='1'><label for='county_tsplost_"+k+"' data-toggle='tooltip' data-placement='top' title='TSPLOST1'></label></td><td><span>H</span><input type='checkbox' name='county_host[]' id='county_host_"+k+"' value='1'><label for='county_host_"+k+"' data-toggle='tooltip' data-placement='top' title='EHOST or EHOST'></label><td><td><span>M</span><input type='checkbox' name='county_marta[]' id='county_marta_"+k+"' value='1'><label for='county_marta_"+k+"' data-toggle='tooltip' data-placement='top' title='MARTA'></label><td><td><span>O</span><input type='checkbox' name='county_other[]' id='county_other_"+k+"' value='1'><label for='county_other_"+k+"' data-toggle='tooltip' data-placement='top' title='Other'></label><td><td><span>T2</span><input type='checkbox' name='county_tsplost2[]' id='county_tsplost2_"+k+"' value='1'><label for='county_tsplost2_"+k+"' data-toggle='tooltip' data-placement='top' title='TSPLOST2'>&nbsp;</label><td><td><span>TF</span><input type='checkbox' name='county_filtontf[]' id='county_filtontf_"+k+"' value='1'><label for='county_filtontf_"+k+"' data-toggle='tooltip' data-placement='top' title='Fulton TSPLOST'>&nbsp;</label><td><td><span>TA</span><input type='checkbox' name='county_atlantata[]' id='county_atlantata_"+k+"' value='1'><label for='county_atlantata_"+k+"' data-toggle='tooltip' data-placement='top' title='Atlanta TSPLOST'>&nbsp;</label><td></tr></table></div></div></td><td><div class='col-md-6'> <button type='button' class='delete-row btn btn-danger' onClick='deleterow();'>-</button></div></td></tr>";
             markup = "<tr><td><div class=''><select name='county_tax_month[]' id='county_tax_month_"+k+"' class='form-control fsc-input' style='width:80px;margin:auto'><option>Select month</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option></select><div></td><td><div class=''><select name='county_tax_year[]' id='county_tax_year_"+k+"' class='form-control fsc-input' style='width:80px;margin:auto;'><option>Select year</option><option value='2020'>2020</option><option value='2021'>2021</option><option value='2022'>2022</option><option value='2023'>2023</option><option value='2024'>2024</option><option value='2025'>2025</option></select></div></td><td><div class='' style='width:100%;display: inline-flex;'><input type='text' name='county_tax_personal_rate[]' id='county_tax_personal_rate_"+k+"' class='form-control txtinput_"+k+"' style='width:120px;margin: auto;margin-right: 10px;'  placeholder='Rate'/> <h3 style='margin:0px;'> + </h3> <input type='text' name='' id='' class='form-control' style='width:120px;margin: auto;margin-left: 10px;'  placeholder='County'/><td style='padding-top:0px!important; padding-right:0px!important;'><div class='salestaxckbox'><table style='margin:auto;'><tr><td><span>M</span><input type='checkbox' name='county_marta[]' id='county_marta_"+k+"' value='1'><label for='county_marta_"+k+"' data-toggle='tooltip' data-placement='top' title='MARTA'></label><td><td><span>L</span><input type='checkbox' name='county_lost[]' id='county_lost_"+k+"' value='1'><label for='county_lost_"+k+"' data-toggle='tooltip' data-placement='top' title='Lost'> </label></td><td><span>E</span><input type='checkbox' name='county_educational[]' id='county_educational_"+k+"' value='1'><label for='county_educational_"+k+"' data-toggle='tooltip' data-placement='top' title='Educational'></label></td><td><span>H</span><input type='checkbox' name='county_host[]' id='county_host_"+k+"' value='1'><label for='county_host_"+k+"' data-toggle='tooltip' data-placement='top' title='HOST or EHOST'></label></td><td><span>S</span><input type='checkbox' name='county_splost[]' id='county_splost_"+k+"' value='1'><label for='county_splost_"+k+"' data-toggle='tooltip' data-placement='top' title='SPLOST'></label></td><td><span>O</span><input type='checkbox' name='county_other[]' id='county_other_"+k+"' value='1'><label for='county_other_"+k+"' data-toggle='tooltip' data-placement='top' title='Other'></label></td><td><span>T</span><input type='checkbox' name='county_tsplost[]' id='county_tsplost_"+k+"' value='1'><label for='county_tsplost_"+k+"' data-toggle='tooltip' data-placement='top' title='TSPLOST 1'></label></td><td><span>T2</span><input type='checkbox' name='county_tsplost2[]' id='county_tsplost2_"+k+"' value='1'><label for='county_tsplost2_"+k+"' data-toggle='tooltip' data-placement='top' title='TSPLOST 2'>&nbsp;</label></td><td><span>m</span><input type='checkbox' name='county_m' id='county_m' value='1' checked=''><label for='county_m' data-toggle='tooltip' data-placement='top' title='' data-original-title='Local MARTA (Atlanta MARTA)'>&nbsp;</label></td><td><span>Ta</span><input type='checkbox' name='county_atlantata[]' id='county_atlantata_"+k+"' value='1'><label for='county_atlantata_"+k+"' data-toggle='tooltip' data-placement='top' title='Atlanta TSPLOST'>&nbsp;</label></td><td><span>Tf</span><input type='checkbox' name='county_filtontf[]' id='county_filtontf_"+k+"' value='1'><label for='county_filtontf_"+k+"' data-toggle='tooltip' data-placement='top' title='Fulton TSPLOST'>&nbsp;</label><td></tr></table></div></div></td><th><div class=''> <button type='button' class='delete-row btn btn-danger' onClick='deleterow();'>-</button></div></th></tr>";
            $("#mytable").append(markup);
            var minNumber = -100;
            var maxNumber = 100;
            $('.txtinput_'+k).on("blur", function() {
                var inputVal = parseFloat($(this).val().replace('%', '')) || 0
                if (minNumber > inputVal)
                {
                    inputVal = -100;
                } else if (maxNumber < inputVal) {
                    inputVal = 100;
                }
                $('.txtinput_'+k).val(inputVal + '.00%');
            });
        });
    }); 
    
   
      $("body").on("click",".delete-row",function(){ 
        $(this).parents("tr").remove();
    });
    



</script>

<script>
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
});
</script>

<script type="text/javascript">
    $(document).ready(function()
    {
        $('#state_county_first').on('change', function() 
        {
            var state=$("#state_county_first").val();
            $("#county_state").val(state);
        });
 
        $('#personal_filling_frequency').on('change', function() 
        {
            if(this.value == 'Monthly')
            {
                if(this.value == 'Monthly' && 'Jan'=='<?php echo date("M");?>')
                {
                    $("#personal_due_date").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Feb'=='<?php echo date("M");?>')
                {
                    $("#personal_due_date").val('<?php echo date("M",strtotime("01 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Mar'=='<?php echo date("M");?>')
                {
                    $("#personal_due_date").val('<?php echo date("M",strtotime("01 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Apr'=='<?php echo date("M");?>')
                {
                    $("#personal_due_date").val('<?php echo date("M",strtotime("01 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'May'=='<?php echo date("M");?>')
                {
                    $("#personal_due_date").val('<?php echo date("M",strtotime("01 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Jun'=='<?php echo date("M");?>')
                {
                    $("#personal_due_date").val('<?php echo date("M",strtotime("01 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Jul'=='<?php echo date("M");?>')
                {
                    $("#personal_due_date").val('<?php echo date("M",strtotime("01 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Aug'=='<?php echo date("M");?>')
                {
                    $("#personal_due_date").val('<?php echo date("M",strtotime("01 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Sep'=='<?php echo date("M");?>')
                {
                    $("#personal_due_date").val('<?php echo date("M",strtotime("01 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Oct'=='<?php echo date("M");?>')
                {
                    $("#personal_due_date").val('<?php echo date("M",strtotime("01 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Nov'=='<?php echo date("M");?>')
                {
                    $("#personal_due_date").val('<?php echo date("M",strtotime("01 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Dec'=='<?php echo date("M");?>')
                {
                    $("#personal_due_date").val('<?php echo date("M",strtotime("01 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
            }
            else if(this.value == 'Quarterly')
            {  
                if(this.value == 'Quarterly' && 'Jan'=='<?php echo date("M");?>')
                {
                    $("#personal_due_date").val('<?php echo date("M",strtotime("01 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'Feb'=='<?php echo date("M");?>')
                {
                    $("#personal_due_date").val('<?php echo date("M",strtotime("01 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'Mar'=='<?php echo date("M");?>')
                {
                    $("#personal_due_date").val('<?php echo date("M",strtotime("01 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'Apr'=='<?php echo date("M");?>')
                {
                    $("#personal_due_date").val('<?php echo date("M",strtotime("4 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'May'=='<?php echo date("M");?>')
                {
                    $("#personal_due_date").val('<?php echo date("M",strtotime("4 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'Jun'=='<?php echo date("M");?>')
                {
                    $("#personal_due_date").val('<?php echo date("M",strtotime("4 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'Jul'=='<?php echo date("M");?>')
                {   
                    $("#personal_due_date").val('<?php echo date("M",strtotime("7 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'Aug'=='<?php echo date("M");?>')
                {  
                    $("#personal_due_date").val('<?php echo date("M",strtotime("7 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'Sep'=='<?php echo date("M");?>')
                {
                    $("#personal_due_date").val('<?php echo date("M",strtotime("7 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'Oct'=='<?php echo date("M");?>')
                {
                     $("#personal_due_date").val('<?php echo "Jan".'-'."01".'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
                }
                else if(this.value == 'Quarterly' && 'Nov'=='<?php echo date("M");?>')
                {
                     $("#personal_due_date").val('<?php echo "Jan".'-'."01".'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
                }
                else if(this.value == 'Quarterly' && 'Dec'=='<?php echo date("M");?>')
                {
                     $("#personal_due_date").val('<?php echo "Jan".'-'."01".'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
                }
            }
            else if(this.value == 'Annually')
            { 
                $("#personal_due_date").val('<?php echo "Jan".'-'."01".'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
            }
        });
        
        
        $('#property_filling_frequency').on('change', function() 
        {
        
            if(this.value == 'Monthly')
            {
                if(this.value == 'Monthly' && 'Jan'=='<?php echo date("M");?>')
                {
                    $("#property_due_date").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Feb'=='<?php echo date("M");?>')
                {
                    $("#property_due_date").val('<?php echo date("M",strtotime("01 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Mar'=='<?php echo date("M");?>')
                {
                    $("#property_due_date").val('<?php echo date("M",strtotime("01 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Apr'=='<?php echo date("M");?>')
                {
                    $("#property_due_date").val('<?php echo date("M",strtotime("01 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'May'=='<?php echo date("M");?>')
                {
                    $("#property_due_date").val('<?php echo date("M",strtotime("01 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Jun'=='<?php echo date("M");?>')
                {
                    $("#property_due_date").val('<?php echo date("M",strtotime("01 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Jul'=='<?php echo date("M");?>')
                {
                    $("#property_due_date").val('<?php echo date("M",strtotime("01 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Aug'=='<?php echo date("M");?>')
                {
                    $("#property_due_date").val('<?php echo date("M",strtotime("01 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Sep'=='<?php echo date("M");?>')
                {
                    $("#property_due_date").val('<?php echo date("M",strtotime("01 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Oct'=='<?php echo date("M");?>')
                {
                    $("#property_due_date").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Nov'=='<?php echo date("M");?>')
                {
                    $("#property_due_date").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Dec'=='<?php echo date("M");?>')
                {
                    $("#property_due_date").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
            }
            else if(this.value == 'Quarterly')
            {  
                if(this.value == 'Quarterly' && 'Jan'=='<?php echo date("M");?>')
                {
                    $("#property_due_date").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'Feb'=='<?php echo date("M");?>')
                {
                    $("#property_due_date").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'Mar'=='<?php echo date("M");?>')
                {
                    $("#property_due_date").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'Apr'=='<?php echo date("M");?>')
                {
                    $("#property_due_date").val('<?php echo date("M",strtotime("4 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'May'=='<?php echo date("M");?>')
                {
                    $("#property_due_date").val('<?php echo date("M",strtotime("4 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'Jun'=='<?php echo date("M");?>')
                {
                    $("#property_due_date").val('<?php echo date("M",strtotime("4 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'Jul'=='<?php echo date("M");?>')
                {   
                    $("#property_due_date").val('<?php echo date("M",strtotime("7 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'Aug'=='<?php echo date("M");?>')
                {  
                    $("#property_due_date").val('<?php echo date("M",strtotime("7 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'Sep'=='<?php echo date("M");?>')
                {
                    $("#property_due_date").val('<?php echo date("M",strtotime("7 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'Oct'=='<?php echo date("M");?>')
                {
                     $("#property_due_date").val('<?php echo "Jan".'-'."01".'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
                }
                else if(this.value == 'Quarterly' && 'Nov'=='<?php echo date("M");?>')
                {
                     $("#property_due_date").val('<?php echo "Jan".'-'."01".'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
                }
                else if(this.value == 'Quarterly' && 'Dec'=='<?php echo date("M");?>')
                {
                     $("#property_due_date").val('<?php echo "Jan".'-'."01".'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
                }
            }
            else if(this.value == 'Annually')
            { 
                $("#property_due_date").val('<?php echo "Jan".'-'."01".'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
            }
        });
        
        
         
        $('#licence_filling_frequency').on('change', function() 
        {
        
            if(this.value == 'Monthly')
            {
                if(this.value == 'Monthly' && 'Jan'=='<?php echo date("M");?>')
                {
                    $("#licence_due_date").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Feb'=='<?php echo date("M");?>')
                {
                    $("#licence_due_date").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Mar'=='<?php echo date("M");?>')
                {
                    $("#licence_due_date").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Apr'=='<?php echo date("M");?>')
                {
                    $("#licence_due_date").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'May'=='<?php echo date("M");?>')
                {
                    $("#licence_due_date").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Jun'=='<?php echo date("M");?>')
                {
                    $("#licence_due_date").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Jul'=='<?php echo date("M");?>')
                {
                    $("#licence_due_date").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Aug'=='<?php echo date("M");?>')
                {
                    $("#licence_due_date").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Sep'=='<?php echo date("M");?>')
                {
                    $("#licence_due_date").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Oct'=='<?php echo date("M");?>')
                {
                    $("#licence_due_date").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Nov'=='<?php echo date("M");?>')
                {
                    $("#licence_due_date").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Dec'=='<?php echo date("M");?>')
                {
                    $("#licence_due_date").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
            }
            else if(this.value == 'Quarterly')
            {  
                if(this.value == 'Quarterly' && 'Jan'=='<?php echo date("M");?>')
                {
                    $("#licence_due_date").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'Feb'=='<?php echo date("M");?>')
                {
                    $("#licence_due_date").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'Mar'=='<?php echo date("M");?>')
                {
                    $("#licence_due_date").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'Apr'=='<?php echo date("M");?>')
                {
                    $("#licence_due_date").val('<?php echo date("M",strtotime("4 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'May'=='<?php echo date("M");?>')
                {
                    $("#licence_due_date").val('<?php echo date("M",strtotime("4 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'Jun'=='<?php echo date("M");?>')
                {
                    $("#licence_due_date").val('<?php echo date("M",strtotime("4 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'Jul'=='<?php echo date("M");?>')
                {   
                    $("#licence_due_date").val('<?php echo date("M",strtotime("7 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'Aug'=='<?php echo date("M");?>')
                {  
                    $("#licence_due_date").val('<?php echo date("M",strtotime("7 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'Sep'=='<?php echo date("M");?>')
                {
                    $("#licence_due_date").val('<?php echo date("M",strtotime("7 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'Oct'=='<?php echo date("M");?>')
                {
                     $("#licence_due_date").val('<?php echo "Jan".'-'."01".'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
                }
                else if(this.value == 'Quarterly' && 'Nov'=='<?php echo date("M");?>')
                {
                     $("#licence_due_date").val('<?php echo "Jan".'-'."01".'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
                }
                else if(this.value == 'Quarterly' && 'Dec'=='<?php echo date("M");?>')
                {
                     $("#licence_due_date").val('<?php echo "Jan".'-'."01".'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
                }
            }
            else if(this.value == 'Annually')
            { 
                $("#licence_due_date").val('<?php echo "Jan".'-'."01".'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
            }
        });
    });   
        

</script>

<script>
    $("#personal_telephone").mask("(999) 999-9999");
    $("#personal_secondary_telephone").mask("(999) 999-9999");
    $("#property_telephone").mask("(999) 999-9999");
    $("#property_secondary_telephone").mask("(999) 999-9999");
   
    $("#licence_telephone").mask("(999) 999-9999");
    $("#licence_secondary_telephone").mask("(999) 999-9999");
    $("#county_telephone").mask("(999) 999-9999");
    $("#county_fax").mask("(999) 999-9999");
   
</script>

@endsection()

