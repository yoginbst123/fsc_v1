@extends('fac-Bhavesh-0554.layouts.app')

@section('main-content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>Add -- Type of License</h1>
    </section>
    <!-- Main content -->
    <section class="content">
	<div class="row">
		<div class="col-md-12">
				<div class="box box-success">
			      <div class="box-header">
             
              <div class="box-tools pull-right">
                
              </div>
            </div>
				<div class="col-md-12">
             <form method="post" action="{{route('license.store')}}" class="form-horizontal" enctype="multipart/form-data">
					{{csrf_field()}}

                        <div class="form-group {{ $errors->has('licensename') ? ' has-error' : '' }}">
							<label class="control-label col-md-3">License Name  :</label>
							<div class="col-lg-5 col-md-8">
								<input name="licensename" type="text" id="licensename" class="form-control" value="" />          
								
							</div>
						</div>	
						
						
						<div class="card-footer">
						    <div class="row">
					        <div class="col-md-3"></div>
							<div class="col-md-2" style="width:155px;">
								<input class="btn_new_save btn-primary1" type="submit" name="submit" value="Save">
							</div>
							<div class="col-md-2" style="width:155px;">
								<a class="btn_new_cancel" href="{{url('fac-Bhavesh-0554/license')}}">Cancel</a> 
							</div>
							</div>
						</div>
						
					</form>
				</div>
			</div>
		</div>
	</div>
</section>	
</div>

<script>

	$('#renewalDropdown').on('change', function() {
      var renewalval = $('#renewalDropdown').val();
       //alert(renewalval);
      
      if(renewalval=='Yes'){
          //alert('');
          $('#duedate').show();
      } else{
           $('#duedate').hide();
      }
    });

	    

    function showstatecounty(){
       var authorityval = $('#authoritytype').val();
     // alert(authorityval)
       if(authorityval=='state'){
           $('#statediv').show();
             $('#countydiv').hide();
              $('#citydiv').hide();
       } else if(authorityval=='county'){
            $('#countydiv').show();
            $('#statediv').show();
             $('#citydiv').hide();
       }
       else if(authorityval=='city'){
            $('#citydiv').show();
            $('#statediv').show();
             $('#countydiv').hide();
       }
    }
    
    function changecounty(){
        var countyval = $('#countybox').val()
        if(countyval=='one'){
            $('#countyvalue').show();
        }
    }
</script>
@endsection()