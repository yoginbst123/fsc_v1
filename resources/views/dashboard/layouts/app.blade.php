<!DOCTYPE html>
<html>
<head>
@include('dashboard.layouts.head')
</head>
<body class="hold-transition skin-purple sidebar-mini">
<div class="wrapper">
@include('dashboard.layouts.header') 
@include('dashboard.layouts.sidebar')
@section('main-content')
@show
@include('dashboard.layouts.footer')
@include('dashboard.layouts.script')
</div>
</body>
</html>
