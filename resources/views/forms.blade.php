@extends('front-section.app')
@section('main-content')
<style>
a:focus, a:hover {
    color: #fff;
    text-decoration: none;
}
.fa-file-pdf-o{color:#E63625}
.fsc-section-head img{    margin-top: -4px;
    width: 23px;
}
.fsc-section-head h4 {
    padding: 10px;
}
</style>
<style>
.fsc-section-head img{    margin-top: -4px;
    width: 23px;
}
.collapse.in {
    display: block;
    border: 2px solid #428bca;
    background-color: #e9f7ff;
}
.fa-file-pdf-o{color:#E63625}
.fsc-section-head h4 {
    padding: 10px;
}
#accordion {
    margin-top: 4%;
}

.panel-heading  a:before {
   font-family: 'FontAwesome';
   content: "\f103";
   float: right;
   transition: all 0.5s;
font-size: 31px;
margin-top: -5px;
}
.panel-heading.active a:before {
	-webkit-transform: rotate(180deg);
	-moz-transform: rotate(180deg);
	transform: rotate(180deg);color: white;
} 
.panel-title{cursor: pointer;
color: white;
font-family: 'Proza Libre', serif;
font-size: 2em;
background-color: #428bca;
padding: 0%;
border-top-left-radius: 5px;
border-top-right-radius: 5px;
margin: 0;}
.panel-default > .panel-heading {
    color: #fff;
    background-color: #428bca;
    border-color: #ddd;
}
</style>
<div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 fsc-content-head">
			<h4>FORM</h4>
		</div>
	</div>
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                      Federal 
                    </a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
@foreach ($formvar as $form)
	@if ($form->form_department=='Federal') 
		<?php //file directory name
$path_parts = pathinfo($form->form_upload);
$fileExtension = $path_parts['extension'];
?>
<h2 style="font-size: 17px;color: #2b6ba5;">{{$form->form_no}}</h2>
		<h4 style="padding: 1% 0%;width: 100%;display: inline-table;border-bottom:2px solid #428aca;"><a download style="font-size:15px;color:#0A3867;width: 90%;float: left;" href="{{url('public/formpdf')}}/{{$form->form_upload}}" target="_blank"><i class="fa @if($fileExtension=='doc') fa-file @else fa-file-pdf-o @endif" style="color:#E63625"></i>&nbsp;&nbsp;{{$form->form_name}} &nbsp;&nbsp;&nbsp;&nbsp;<span style="float: right;">Download</span>
<p style="font-size: 12px;margin-top: 5px;">{{$form->category}}</p>
</a>
@if ($form->link=='') @else <a style="font-size:15px;color:#0A3867" href="{{$form->link}}" target="_blank"><span style="float:right">Link</span></a>@endif
		</h4>
	@endif
@endforeach	

                </div>
            </div>
        </div>
<div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingfive">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefive" aria-expanded="true" aria-controls="collapsefive">State</a>
                </h4>
            </div>
            <div id="collapsefive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfive">
                <div class="panel-body">
                @foreach ($formvar as $form)
	@if ($form->form_department=='State') 
	<?php //file directory name
$path_parts = pathinfo($form->form_upload);
$fileExtension = $path_parts['extension'];
?>
<h2 style="font-size: 17px;color: #2b6ba5;">{{$form->form_no}}</h2>
		<h4 style="padding: 1% 0%;width: 100%;display: inline-table;border-bottom:2px solid #428aca;"><a download style="font-size:15px;color:#0A3867;width: 90%;float: left;" href="{{url('public/formpdf')}}/{{$form->form_upload}}" target="_blank"><i class="fa @if($fileExtension=='doc') fa-file @else fa-file-pdf-o @endif"></i>&nbsp;&nbsp;{{$form->form_name}}  &nbsp;&nbsp;&nbsp;&nbsp;<span style="float: right;">Download</span>
<p style="font-size: 12px;margin-top: 5px;">{{$form->category}}</p></a>
@if ($form->link=='') @else <a style="font-size:15px;color:#0A3867" href="{{$form->link}}" target="_blank"><span style="float:right">Link</span></a>@endif
</h4>
	@endif
@endforeach	

                </div>
            </div>
        </div>
 <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingfour">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefour" aria-expanded="true" aria-controls="collapsefour">County</a>
                </h4>
            </div>
            <div id="collapsefour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfour">
                <div class="panel-body">
                  @foreach ($formvar as $form)
	@if ($form->form_department=='County') 
			<?php //file directory name
$path_parts = pathinfo($form->form_upload);
$fileExtension = $path_parts['extension'];
?>
<h2 style="font-size: 17px;color: #2b6ba5;">{{$form->form_no}}</h2>
		<h4 style="padding: 1% 0%;width: 100%;display: inline-table;border-bottom:2px solid #428aca;">
		    <a download style="font-size:15px;color:#0A3867;width: 90%;float: left;" href="{{url('public/formpdf')}}/{{$form->form_upload}}" target="_blank"><i class="fa @if($fileExtension=='doc') fa-file @else fa-file-pdf-o @endif"></i>&nbsp;&nbsp;{{$form->form_name}} &nbsp;&nbsp;&nbsp;&nbsp;<span style="float: right;">Downlaod</span>
<p style="font-size: 10px;margin-top: 5px;">{{$form->category}}</p></a>
@if ($form->link=='') @else <a download style="font-size:15px;color:#0A3867" href="{{$form->link}}" target="_blank"><span style="float:right">Link</span></a>@endif
</h4>
	@endif
@endforeach	

                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingTwo">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                     City / Local
                    </a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                <div class="panel-body">
                 @foreach($formvar as $form)
	@if ($form->form_department=='Local (City)')
			<?php //file directory name
$path_parts = pathinfo($form->form_upload);
$fileExtension = $path_parts['extension'];
?>
<h2 style="font-size: 17px;color: #2b6ba5;">{{$form->form_no}}</h2>
		<h4 style="padding: 1% 0%;width: 100%;display: inline-table;border-bottom:2px solid #428aca;"><a download style="font-size:15px;color:#0A3867;width: 90%;float: left;" href="{{url('public/formpdf')}}/{{$form->form_upload}}" target="_blank"><i class="fa @if($fileExtension=='doc') fa-file @else fa-file-pdf-o @endif"></i>&nbsp;&nbsp;{{$form->form_name}} &nbsp;&nbsp;&nbsp;&nbsp;<span style="float: right;">Download</span>
<p style="font-size: 12px;margin-top: 5px;">{{$form->category}}</p></a>
@if ($form->link=='') @else <a style="font-size:15px;color:#0A3867" href="{{$form->link}}" target="_blank"><span style="float:right">Link</span></a>@endif
</h4>
	@endif
@endforeach	
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingThree">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                       Other
                    </a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                <div class="panel-body">
  @foreach ($formvar as $form)
	@if ($form->form_department=='Other') 
			<?php //file directory name
$path_parts = pathinfo($form->form_upload);
$fileExtension = $path_parts['extension'];
?>
<h2 style="font-size: 17px;color: #2b6ba5;">{{$form->form_no}}</h2>
		<h4 style="padding: 1% 0%;width: 100%;display: inline-table;border-bottom:2px solid #428aca;"><a download style="font-size:15px;color:#0A3867;width: 90%;float: left;" href="{{url('public/formpdf')}}/{{$form->form_upload}}" target="_blank"><i class="fa @if($fileExtension=='doc') fa-file @else fa-file-pdf-o @endif"></i>&nbsp;&nbsp;{{$form->form_name}}  &nbsp;&nbsp;&nbsp;&nbsp;<span style="float: right;">Download</span>
<p style="font-size: 12px;margin-top: 5px;">{{$form->category}}</p></a>
@if ($form->link=='') @else <a style="font-size:15px;color:#0A3867" href="{{$form->link}}" target="_blank"><span style="float:right">Link</span></a>@endif
</h4>
	@endif
@endforeach	
                </div>
            </div>
        </div>
    </div><!-- panel-group -->
<div>
<!---<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-section-head collapsed" style="text-align:left;" data-toggle="collapse" data-target="#demo1" onclick="hideShowImage(1)" aria-expanded="false">
	<h4>Federal <img src="{{url('public/images/expand.png')}}" style="float: right;" id="expand1"><img src="{{url('public/images/collapse.png')}}" style="float: right; display: none;" id="collapse1"></h4>
</div>
<div class="collapse" id="demo1" aria-expanded="false" style="height: 0px;">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-content-box">
@foreach ($formvar as $form)
	@if ($form->form_department=='Federal') 
		<h4 style="padding: 1% 5%;"><a style="font-size:20px;color:#0A3867" href="{{url('public/formpdf')}}/{{$form->form_upload}}" target="_blank"><i class="fa fa-file-pdf-o" style="color:#E63625"></i>&nbsp;&nbsp;{{$form->form_no}}</a>

		<a style="font-size:20px;color:#0A3867" href="{{url('public/formpdf')}}/{{$form->form_upload}}" target="_blank" class="pull-right">&nbsp;&nbsp;Downlaod</a></h4>
	@endif
@endforeach	
	</div>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-section-head collapsed" style="text-align:left;" data-toggle="collapse" data-target="#demo2" onclick="hideShowImage(2)" aria-expanded="false">
	<h4>State <img src="{{url('public/images/expand.png')}}" style="float: right;" id="expand2"><img src="{{url('public/images/collapse.png')}}" style="float: right; display: none;" id="collapse2"></h4>
</div>
<div class="collapse" id="demo2" aria-expanded="false" style="height: 0px;">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-content-box">
@foreach ($formvar as $form)
	@if ($form->form_department=='State') 
		<h4 style="padding: 1% 5%;"><a style="font-size:20px;color:#0A3867" href="{{url('public/formpdf')}}/{{$form->form_upload}}" target="_blank"><i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp;{{$form->form_no}}</a>

		<a style="font-size:20px;color:#0A3867" href="{{url('public/formpdf')}}/{{$form->form_upload}}" target="_blank" class="pull-right">&nbsp;&nbsp;Downlaod</a></h4>
	@endif
@endforeach	
	</div>
</div>


<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-section-head collapsed" style="text-align:left;" data-toggle="collapse" data-target="#demo3" onclick="hideShowImage(3)" aria-expanded="false">
	<h4>County <img src="{{url('public/images/expand.png')}}" style="float: right;" id="expand3"><img src="{{url('public/images/collapse.png')}}" style="float: right; display: none;" id="collapse3"></h4>
</div>
<div class="collapse" id="demo3" aria-expanded="false" style="height: 0px;">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-content-box">
@foreach ($formvar as $form)
	@if ($form->form_department=='County') 
		<h4 style="padding: 1% 5%;"><a style="font-size:20px;color:#0A3867" href="{{url('public/formpdf')}}/{{$form->form_upload}}" target="_blank"><i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp;{{$form->form_no}}</a>

		<a style="font-size:20px;color:#0A3867" href="{{url('public/formpdf')}}/{{$form->form_upload}}" target="_blank" class="pull-right">&nbsp;&nbsp;Downlaod</a></h4>
	@endif
@endforeach	
	</div>
</div>


<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-section-head collapsed" style="text-align:left;" data-toggle="collapse" data-target="#demo4" onclick="hideShowImage(4)" aria-expanded="false">
	<h4>Local (City) <img src="{{url('public/images/expand.png')}}" style="float: right;" id="expand4"><img src="{{url('public/images/collapse.png')}}" style="float: right; display: none;" id="collapse4"></h4>
</div>
<div class="collapse" id="demo4" aria-expanded="false" style="height: 0px;">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-content-box">
@foreach ($formvar as $form)
	@if ($form->form_department=='Local (City)') 
		<h4 style="padding: 1% 5%;"><a style="font-size:20px;color:#0A3867" href="{{url('public/formpdf')}}/{{$form->form_upload}}" target="_blank"><i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp;{{$form->form_no}}</a>

		<a style="font-size:20px;color:#0A3867" href="{{url('public/formpdf')}}/{{$form->form_upload}}" target="_blank" class="pull-right">&nbsp;&nbsp;Downlaod</a></h4>
	@endif
@endforeach	
	</div>
</div>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-section-head collapsed" style="text-align:left;" data-toggle="collapse" data-target="#demo5" onclick="hideShowImage(5)" aria-expanded="false">
	<h4>Others <img src="{{url('public/images/expand.png')}}" style="float: right;" id="expand5"><img src="{{url('public/images/collapse.png')}}" style="float: right; display: none;" id="collapse5"></h4>
</div>
<div class="collapse" id="demo5" aria-expanded="false" style="height: 0px;">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-content-box">
@foreach ($formvar as $form)
	@if ($form->form_department=='Others') 
		<h4 style="padding: 1% 5%;"><a style="font-size:20px;color:#0A3867" href="{{url('public/formpdf')}}/{{$form->form_upload}}" target="_blank"><i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp;{{$form->form_no}}</a>

		<a style="font-size:20px;color:#0A3867" href="{{url('public/formpdf')}}/{{$form->form_upload}}" target="_blank" class="pull-right">&nbsp;&nbsp;Downlaod</a></h4>
	@endif
@endforeach	
	</div>-->
</div>
</div>
<script>
 $('.panel-collapse').on('show.bs.collapse', function () { 
    $(this).siblings('.panel-heading').addClass('active');
  });

  $('.panel-collapse').on('hide.bs.collapse', function () {
    $(this).siblings('.panel-heading').removeClass('active');
  });
</script>
@endsection()