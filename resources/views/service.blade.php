@extends('front-section.app')
@section('main-content')
<style>

.fsc-apply-btn {
    padding: 1.5% 3% !important;
    margin-top: 10% !important;
    margin-right: 54px !important;
}
ul {margin-left:16px;}
ul li h5{font-size:14px!important;}
</style>
<div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">
	<div class="row">
	@if ( session()->has('success') )
    <div class="alert alert-success alert-dismissable">{{ session()->get('success') }}</div>
@endif
@if ( session()->has('error') )
    <div class="alert alert-danger alert-dismissable">{{ session()->get('error') }}</div>
@endif
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 fsc-content-head">
			<h4>SERVICES</h4>			
		</div>				 
		@foreach($service as $ser)
                <div class="col-md-12 col-sm-12 col-xs-12 fsc-main-box" id="{{$ser->id}}">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-section-head">
			<h4>@if($ser->service_name == 'Accounting Bookkeeping & Taxation Service') Accounting / Bookkeeping & Taxation Service @else {{$ser->service_name}} @endif</h4>
		</div>		
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-content-box">
		
			<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
				<div id="myCarousel{{$ser->cid}}"class="carousel slide" data-ride="carousel">
				
					<ol class="carousel-indicators">
@foreach($serviceimg as $serimg)
@if($serimg->service_id==$ser->id)
<li data-target="#myCarousel{{$serimg->service_id}}" data-slide-to="{{$serimg->id}}" class="{{$serimg->active}}"></li>
@endif
@endforeach
					</ol>
					<div class="carousel-inner">
@foreach($serviceimg as $serimg)
@if($serimg->service_id==$ser->id)
						<div class="item {{$serimg->active}}">
		<img src="{{URL::asset('public/serviceimage')}}/{{$serimg->serviceimage}}" alt="Los Angeles">
					</div>
@endif
@endforeach
					</div>
				</div>
			</div>
			<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12" style='padding:0px'>
							{!!$ser->description!!}
				<div class="col-lg-12 col-md-12" style="padding-left:0px; margin-top:20px;">
					<a href="{{URL::to('apply-service/create',[$ser->id,$ser->service_name])}}" style="" class="fsc-apply-btn">Inquiry</a>
<a href="{{URL::to('applyservices/create',[$ser->id,$ser->service_name])}}" style="" class="fsc-apply-btn">Apply</a>
					<a href="{{url('/')}}" class="fsc-apply-btn">Home</a>
				</div>
				@if($ser->service_name == 'Residential Mortgage Services')<h4 class="libre" style="margin-top: 35px;display: inline-block;">GA Residential Mortgage License # 12099 / NMLS # 166290</h4>@endif
				</div>
		</div>
                </div>
		@endforeach	
	</div>
</div>
<script>
window.onload=function () {
$('.ser').addClass('active');

};
$("#accounting").click(function() {
    $('html, body').animate({
        scrollTop: $("#16").offset().top
    }, 1000);
$(".submenu").css("overflow", "visible");
$(".submenu").css("max-height", "100%");
$('.ser').addClass('active')
$( ".fsc-menu-link1").addClass("fsc-menu-link");
$( ".fsc-menu-link2").removeClass("fsc-menu-link");
$( ".fsc-menu-link3").removeClass("fsc-menu-link" );
$( ".fsc-menu-link4").removeClass("fsc-menu-link" );
$( ".fsc-menu-link5").removeClass("fsc-menu-link" );
$( ".fis").addClass("active");
$( ".second").removeClass("active");
$( ".third").removeClass("active");
$( ".fourth").removeClass("active");
$( ".five").removeClass("active");
});

$("#residential").click(function() {
    $('html, body').animate({
        scrollTop: $("#17").offset().top
    }, 1000);
$(".submenu").css("overflow", "visible");
$(".submenu").css("max-height", "100%");
$('.ser').addClass('active')
$( ".fsc-menu-link2").addClass("fsc-menu-link");
$( ".fsc-menu-link1").removeClass("fsc-menu-link");
$( ".fsc-menu-link5").removeClass("fsc-menu-link" );
$( ".fsc-menu-link4").removeClass("fsc-menu-link" );
$( ".fsc-menu-link3").removeClass("fsc-menu-link" );
$( ".second").addClass("active");
$( ".fis").removeClass("active");
$( ".third").removeClass("active");
$( ".fourth").removeClass("active");
$( ".five").removeClass("active");
fis

});
$("#commercial").click(function() {
    $('html, body').animate({
        scrollTop: $("#18").offset().top
    }, 1000);
$(".submenu").css("overflow", "visible");
$('.ser').addClass('active')
$(".submenu").css("max-height", "100%");
$( ".fsc-menu-link3").addClass("fsc-menu-link");
$( ".fsc-menu-link1").removeClass("fsc-menu-link");
$( ".fsc-menu-link2").removeClass("fsc-menu-link");
$( ".fsc-menu-link4").removeClass("fsc-menu-link");
$( ".fsc-menu-link5").removeClass("fsc-menu-link" );
$( ".third").addClass("active");
$( ".second").removeClass("active");
$( ".fis").removeClass("active");
$( ".fourth").removeClass("active");
$( ".five").removeClass("active");
});

$("#financial").click(function() {
    $('html, body').animate({
        scrollTop: $("#19").offset().top
    }, 1000);
$(".submenu").css("overflow", "visible");
$(".submenu").css("max-height", "100%");
$('.ser').addClass('active')
$( ".fsc-menu-link4").addClass("fsc-menu-link" );
$( ".fsc-menu-link1").removeClass("fsc-menu-link");
$( ".fsc-menu-link2").removeClass("fsc-menu-link");
$( ".fsc-menu-link3").removeClass("fsc-menu-link");
$( ".fsc-menu-link5").removeClass("fsc-menu-link" );
$( ".fourth").addClass("active");
$( ".second").removeClass("active");
$( ".fis").removeClass("active");
$( ".third").removeClass("active");
$( ".five").removeClass("active");
});

$("#insurance").click(function() {
    $('html, body').animate({
        scrollTop: $("#20").offset().top
    }, 1000);
$(".submenu").css("overflow", "visible");
$(".submenu").css("max-height", "100%");
$( ".fsc-menu-link5").addClass("fsc-menu-link" );
$('.ser').addClass('active')
$( ".fsc-menu-link1").removeClass("fsc-menu-link");
$( ".fsc-menu-link2").removeClass("fsc-menu-link");
$( ".fsc-menu-link3").removeClass("fsc-menu-link");
$( ".fsc-menu-link4").removeClass("fsc-menu-link");
$( ".five").addClass("active");
$( ".second").removeClass("active");
$( ".fis").removeClass("active");
$( ".third").removeClass("active");
$( ".fourth").removeClass("active");
});
</script>
@endsection