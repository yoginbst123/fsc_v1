@extends('front-section.app')
@section('main-content')
<style>
   .title-form-group #checkBox {
   margin-bottom: 15px;
   margin-top: 13px;
   }
   .nex1:hover, .nex1:focus {background-color:transparent;}
   .ag{position: relative;
   top: -25px;
   font-size: 13px;
   left: -100px;}
   .green-border {
   background-color: #003b6d !important;
   }
   .breadcrumb>li.ddd.active1 a{
   background: green !important;
   }
   .title-form-group p {
   font-size: 16px;
   line-height: 30px;
   }
   .fsc-apply-btn {
   padding: 5px 33px !important;
   margin-top: 0 !important;
   margin-right: 0 !important;font-size: 21px !important;
   }
   .tess{
   text-align: left;
   float: right;
   font-size: 11px;margin-right: 20px;}
   .star-required1{color: transparent;}
   .fsc-reg-sub-header {
   padding-left: 0;}
   .pager li>a, .pager li>span {
   width: 93px;
   }
   .nav-pills > li.active > a, .nav-pills > li.active > a:focus, .nav-pills > li.active > a:hover {
   color: #fff;
   background-color: green;
   }
   .bread
   .title-form-group p{ font-size:12px}
   li.next {
   width: 93px;
   font-size: 16px;
   padding: 0 0 10px 0;
   }
   .nav-pills > li > a {
   border-radius: 0 !important;
   }
   li.previous{
   width: 93px;
   font-size: 16px;
   padding: 0 0 10px 0;
   }
   .fsc-reg-sub-header-div{border-radius: 0;padding: 6px 4%;border-top: 2px solid #fff;border-bottom: 2px solid #fff;}
   .breadcrumb {
   padding: 0px;
   background: #D4D4D4;
   list-style: none; 
   overflow: hidden;
   margin-top: -20px;
   }
   .breadcrumb>li+li:before {
   padding: 0;
   }
   .breadcrumb li { 
   float: left; 
   width: 100%;
   text-align: center;
   font-size: 15px;
   }
   }
   .breadcrumb li.active a {
   background: brown;                   /* fallback color */
   background: green; 
   }
   .breadcrumb li.completed a {
   background: brown;                   /* fallback color */
   background: hsla(153, 57%, 51%, 1); 
   }
   .breadcrumb li.active a:after {
   border-left: 30px solid green ;
   }
   .breadcrumb li.completed a:after {
   border-left: 30px solid hsla(153, 57%, 51%, 1);
   } 
   .breadcrumb li a {width: 95%;
   color: white;
   text-decoration: none; 
   padding: 10px 0 10px 45px;
   position: relative; 
   display: block;
   float: left;
   }
   .breadcrumb li a:after { 
   content: " "; 
   display: block; 
   width: 0; 
   height: 0;
   border-top: 50px solid transparent;          
   /* Go big on the size, and let overflow hide */
   border-bottom: 50px solid transparent;
   border-left: 30px solid hsla(0, 0%, 83%, 1);
   position: absolute;
   top: 50%;
   margin-top: -50px; 
   left: 100%;
   z-index: 2; 
   }	
   .breadcrumb li a:before { 
   content: " "; 
   display: block; 
   width: 0; 
   height: 0;
   border-top: 50px solid transparent;           
   /* Go big on the size, and let overflow hide */
   border-bottom: 50px solid transparent;
   border-left: 30px solid white;
   position: absolute;
   top: 50%;
   margin-top: -50px; 
   margin-left: 1px;
   left: 100%;
   z-index: 1; 
   }	
   .breadcrumb li:first-child a {
   padding-left: 15px;
   width: 100%;
   border-radius: 0;background:green;
   }
   .breadcrumb li:nth-o a {
   padding-left: 15px;
   width: 100%;
   border-radius: 0;
   background: green;
   }
   .pager .disabled>a, .pager .disabled>a:focus, .pager .disabled>a:hover, .pager .disabled>span {
   color: #fff;
   cursor: not-allowed;
   background-color: #428bca;
   font-size: 14px;
   }.pager li>a, .pager li>span {
   display: inline-block;
   padding: 5px 14px;
   background-color: #0472d0;
   border: 1px solid #ddd;
   border-radius: 15px;
   font-size: 14px;
   color: #fff;
   }.error{    font-size: 16px;
   color: #ff0000;
   font-weight: normal;}
   .breadcrumb li a:hover { background:green !important; }
   .breadcrumb li a:hover:after { border-left-color: green  !important; }
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
<div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 fsc-content-head">
   <h4>{{Request::segment(7)}} Registration</h4>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:20px">
@foreach($business as $bus)
@if(Request::segment(3)==$bus->id)

@if(Request::segment(5))

@if(Request::segment(7))
<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="text-align: center">
   @else
   <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12" style="text-align: RIGHT">
      @endif
      @else
      @if(Request::segment(7))
      @else
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center">
         @endif
         @endif
         @if(Request::segment(4)=='Business')
         <img src="{{url('public/frontcss/images/business.png')}}"  style="width: 163px;height: 68px;">
         @endif
         @if(Request::segment(4)=='Personal')
         <img src="{{url('public/frontcss/images/Personal.png')}}"  style="width: 163px;height: 68px;
            ">
         @endif
         @if(Request::segment(4)=='Non-Profit Organization') 
         <img src="{{url('public/frontcss/images/Non-Profit-Org.png')}}"  style="width: 163px;height: 68px;
            ">
         @endif
         @if(Request::segment(4)=='Service Industry')
         <img src="{{url('public/frontcss/images/service.png')}}" style="width: 163px;height: 68px;">
         @endif
         @if(Request::segment(4)=='Profession')
         <img src="{{url('public/images/profession.png')}}" style="width: 163px;height: 68px;">
         @endif
         @if(Request::segment(4)=='Investor')
         <img src="{{url('public/images/Investor.png')}}"   style="width: 163px;height: 68px;">
         @endif
      </div>
      @endif
      @endforeach
      @foreach($category as $cate)
      @if(Request::segment(5)==$cate->id)
      <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12" style="font-size: 3em; text-align: center;padding-top:10px">
         <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      </div>
      @if(Request::segment(7))
      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="text-align: left;">
         @else
         <div class="col-lg-4 col-md-4 col-sm-3 col-xs-12" style="text-align: left;">
            @endif
            <img src="{{url('public/category/',$cate->business_cat_image)}}"  style="width: 163px;height: 68px;">
         </div>
         @endif
         @endforeach
         @foreach($categorybusiness as $cate1)
         @if(Request::segment(7)==$cate1->id)
         <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12" style="font-size: 3em; text-align: center;padding-top:10px">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
         </div>
         <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="text-align: left;">
            <img src="{{url('public/businessbrandcategory/',$cate1->business_brand_category_image)}}" style="width: 163px;height: 68px;">
         </div>
         @endif
         @endforeach
      </div>
   </div>
   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-section-head">
         <!--<h4>Registration</h4>-->
      </div>
   </div>
   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-content-box">
         <form enctype='multipart/form-data' action="{{route('comman-registration.store')}}" id="registrationForm"  method="post">
            <!-- SmartWizard html -->
            {{csrf_field()}}	
            <input type="hidden" id="business_id" name="business_id" value="{{Request::segment(3)}}" />
            <input type="hidden" id="business_cat_id" name="business_cat_id" value="{{Request::segment(5)}}" />
            <input type="hidden" id="business_brand_id" name="business_brand_id" value="{{Request::segment(6)}}" />
            <input type="hidden" id="business_brand_category_id" name="business_brand_category_id" value="{{Request::segment(7)}}" />
            <input type="hidden" id="user_type" name="user_type" value="{{Request::segment(4)}}" />
            <div id="smartwizard">
               <ul class="breadcrumb nav nav-pills">
                  <li class="active"><a href="#step-1" data-toggle="tab">Step-1</a></li>
                  <li class="ddd"><a href="#step-2" data-toggle="tab">Step-2</a></li>
                  <li><a href="#step-3" data-toggle="tab">Step-3</a></li>
               </ul>
               <br>
               <div class="tab-content">
                  <div class="tab-pane active" data-step="0" id="step-1">
                     <div id="form-step-0" role="form" data-toggle="validator">
                        @if(Request::segment(4)=='Personal')
                        <div class="form-group">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:1%;">
                              <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                 <label class="fsc-form-label">Name :  <span class="star-required">*</span></label>
                              </div>
                              <div class="form-group">
                                 <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                    <select class="form-control fsc-input" id="nametype" name="nametype">
                                       <option value="mr">Mr.</option>
                                       <option value="mrs">Mrs.</option>
                                       <option value="miss">Miss.</option>
                                    </select>
                                 </div>
                                 <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin" >
                                    <input type="text" class="form-control  fsc-input textonly" id="first_name" name="first_name" placeholder="First Name">
                                 </div>
                              </div>
                              <div class="form-group">
                                 <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                    <input type="text" class="form-control fsc-input textonly" id="middle_name" maxlength="1" name="middle_name" placeholder="Middle">
                                 </div>
                              </div>
                              <div class="form-group">
                                 <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                    <input type="text" class="form-control fsc-input textonly" id="last_name" name="last_name" placeholder="Last name">
                                 </div>
                              </div>
                           </div>
                        </div>
                        @endif
                        <div class="form-group">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:1%;">
                              <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                 <label class="fsc-form-label" for="company_name">Company Name :@if(Request::segment(4)=='Personal') <span class="star-required1">*</span> @else  <span class="star-required">*</span> @endif
                                 <span class="tess">(Legal Name)</span></label>
                              </div>
                              <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                 <input type="text" class="form-control fsc-input" name="company_name" id="company_name" placeholder="Enter Your Company Name">		
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:0%;">
                              <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                 <label class="fsc-form-label" for="">Business Name : @if(Request::segment(4)=='Personal') <span class="star-required1">*</span> @else  <span class="star-required">*</span> @endif<span class="tess">(DBA Name)</span> </label>
                              </div>
                              <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                 <input type="text" class="form-control fsc-input" id="business_name"  name="business_name" placeholder="Business Name">
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-reg-sub-header-div" style="background-color: #1265BD;color: #fff;margin-top: 1px;">
                           <h3 class="Libre fsc-reg-sub-header">@if(Request::segment(4)=='Personal') Basic Infomation @else Business Location @endif :</h3>
                        </div>
                        <div class="form-group">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:1%;">
                              <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                 <label class="fsc-form-label">@if(Request::segment(4)=='Personal')  @else Business @endif  Address 1 : <span class="star-required">*</span></label>
                              </div>
                              <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                 <input type="text" class="form-control fsc-input" id="address1" placeholder="Address" name="address" placeholder="">
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:1%;">
                              <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                 <label class="fsc-form-label">@if(Request::segment(4)=='Personal')  @else Business @endif Address 2 : <span class="star-required1">*</span></label>
                              </div>
                              <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                 <input type="text"  class="form-control fsc-input" id="business_address" name="business_address" placeholder="">
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:1%;">
                              <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                 <label class="fsc-form-label">Country : <span class="star-required">*</span></label>
                              </div>
                              <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                 <select name="countryId" id="countries_states1" class="form-control bfh-countries fsc-input" data-country="USA">
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:1%;">
                              <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                 <label class="fsc-form-label">   City / State / Zip : <span class="star-required">*</span></label>
                              </div>
                              <div class="">
                                 <div class="form-group">
                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                       <input type="text" class="form-control textonly fsc-input" id="city" name="city" placeholder="City">
                                    </div>
                                 </div>
                                 <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                    <div class="dropdown" style="margin-top: 1%;">
                                       <select name="stateId" id="stateId" class="form-control bfh-states" data-country="countries_states1">
                                          <option value="">State</option>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                       <input type="text" class="form-control fsc-input zip" id="zip" maxlength="6" name="zip" placeholder="Zip">
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:1%;">
                              <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                 <label class="fsc-form-label">@if(Request::segment(4)=='Personal') Telephone 1  @else Business Tel @endif    : <span class="star-required">*</span></label>
                              </div>
                              <div class="form-group">
                                 <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin" >
                                    <input type="tel" class="form-control fsc-input  bfh-phone" data-country="countries_states1" data-format="  (999) 999-9999" id="business_no" name="business_no" placeholder="(999) 999-9999">
                                 </div>
                              </div>
                              <div class="form-group">
                                 <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                    <select  class="form-control fsc-input" id="businesstype" name="businesstype">
                                       <option value="">Type</option>
                                       <option value="Business">Business</option>
                                       <option value="Office">Office</option>
                                       <option value="Mobile">Mobile</option>
                                       <option value="Resid">Res</option>
                                    </select>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                    <input type="text" readonly class="form-control fsc-input" maxlength="5" id="businessext" name="businessext" placeholder="Ext">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:1%;">
                              <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                 <label class="fsc-form-label">@if(Request::segment(4)=='Personal') @else Business @endif Fax : <span class="star-required1">*</span></label>
                              </div>
                              <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                 <input type="text" class="form-control fsc-input  bfh-phone" data-country="countries_states1" data-format="  (999) 999-9999" id="business_fax" name="business_fax" placeholder="Business Fax">
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:1%;">
                              <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                 <label class="fsc-form-label">Website Address : <span class="star-required1">*</span></label>
                              </div>
                              <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                 <input type="text" class="form-control fsc-input" id="website" name="website" placeholder="Website address">
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-reg-sub-header-div" style="background-color: #1265BD;color: #fff;margin-top:1%">
                           <h3 class="Libre fsc-reg-sub-header">Contact Person Information :</h3>
                        </div>
                        <div class="form-group">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:1%;">
                              <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                 <label class="fsc-form-label">Name : @if(Request::segment(4)=='Personal') <span class="star-required1">*</span> @else <span class="star-required">*</span>@endif</label>
                              </div>
                              <div class="col-lg-8">
                                 <div class="row">
                                    <div class="col-lg-3 col-md-2 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                       <div class="form-group">
                                          <select class="form-control fsc-input" id="nametype" name="nametype">
                                             <option value="mr">Mr.</option>
                                             <option value="mrs">Mrs.</option>
                                             <option value="miss">Miss.</option>
                                          </select>
                                       </div>
                                    </div>
                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin" >
                                       <div class="form-group">
                                          <input type="text" class="form-control  fsc-input textonly" id="first_name_1" @if(Request::segment(4)=='Personal') name="first_name_1" @else  name="first_name" @endif placeholder="First Name">@if(Request::segment(4)=='Personal')
                                          @endif 
                                       </div>
                                    </div>
                                    <div class="col-lg-1 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                       <div class="row">
                                          <div class="form-group">
                                             <input type="text" class="form-control fsc-input textonly" id="middle_name_1" maxlength="1" @if(Request::segment(4)=='Personal') name="middle_name_1" @else  name="middle_name" @endif placeholder="M">
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                       <div class="form-group">
                                          <input type="text" class="form-control fsc-input textonly" id="last_name_1" @if(Request::segment(4)=='Personal') name="last_name_1" @else  name="last_name" @endif placeholder="Last name">
                                       </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
                                       <label class="fsc-form-label" style="margin-top:0;width: 100%;float: left;text-align: left;display: inline-block;"><input type="checkbox" class="" name="billingtoo" onclick="FillBilling(this.form)">  &nbsp; Same As Above</label>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:1%;">
                              <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                 <label class="fsc-form-label">Telephone 1 : @if(Request::segment(4)=='Personal') <span class="star-required1">*</span> @else <span class="star-required">*</span>@endif</label>
                              </div>
                              <div class="form-group">
                                 <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin" >
                                    <input type="text" class="form-control fsc-input" data-country="countries_states1" data-format="  (999) 999-9999" placeholder="(999) 999-9999" id="telephone1" name="telephone1" placeholder="Telephone 1">
                                    @if(Request::segment(4)=='Personal')
                                    <label class="fsc-form-label"><input type="checkbox" class="" name="billingtoo1" onclick="FillBilling1(this.form)">  &nbsp; Same As Above</label>
                                    @endif
                                 </div>
                              </div>
                              <div class="form-group">
                                 <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                    <select  class="form-control fsc-input" id="telephone1type" name="telephone1type">
                                       <option value="">Type</option>
                                       <option value="Business">Business</option>
                                       <option value="Office">Office</option>
                                       <option value="Mobile">Mobile</option>
                                       <option value="Resid">Res</option>
                                    </select>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                    <input type="text" readonly class="form-control fsc-input" maxlength="5" id="telephone1ext" name="telephone1ext" placeholder="Ext">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:1%;">
                              <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                 <label class="fsc-form-label">Telephone 2 : @if(Request::segment(4)=='Personal') <span class="star-required1">*</span> @else <span class="star-required1">*</span>@endif</label>
                              </div>
                              <div class="form-group">
                                 <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin" >
                                    <input type="text" class="form-control fsc-input  bfh-phone" data-country="countries_states1" data-format="  (999) 999-9999" placeholder=" (999) 999-9999" id="telephone2" name="telephone2" placeholder="Telephone 2">
                                 </div>
                              </div>
                              <div class="form-group">
                                 <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                    <select  class="form-control fsc-input" id="telephone2type" name="telephone2type">
                                       <option value="">Type</option>
                                       <option value="Business">Business</option>
                                       <option value="Office">Office</option>
                                       <option value="Mobile">Mobile</option>
                                       <option value="Resid">Res</option>
                                    </select>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                    <input type="text" readonly class="form-control fsc-input" maxlength="5" id="telephone2ext" name="telephone2ext" placeholder="Ext">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:1%;">
                              <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                 <label class="fsc-form-label" for="">Email : <span class="star-required">*</span>
                                 </label>
                              </div>
                              <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                 <input type="email" class="form-control fsc-input" id="email" name='email' placeholder="abc@abc.com">
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;margin-bottom:2%;">
                           <ul class="pager wizard">
                              <li class="next"><a href="#" class="nex1">Next</a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="tab-pane" id="step-2" data-step="2">
                     <div id="form-step-2" role="form" data-toggle="validator">
                        <div class="form-section">
                           <div class="container">
                              <div class="row">
                                 <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                                    <div class="title-form-group">
                                       <h4>Bulletin</h4>
                                    </div>
                                 </div>
                                 <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                                    <div class="title-form-group">
                                       <h4>Sales Tax Reporting</h4>
                                       <p>Bulletin</p>
                                    </div>
                                 </div>
                                 <div class="col-lg-4 col-md-4 col-sm-3 col-xs-12">
                                    <div class="title-form-group">
                                       <br>
                                       <br>
                                       <p>Daily</p>
                                       <p>Monthly</p>
                                       <!--<p>Monthly</p>-->
                                       <p>Comparion by Week/Month/Year</p>
                                       <p>Notification of Sales Tax Payment</p>
                                       <p>Any time pulled out history for your sales report</p>
                                    </div>
                                 </div>
                                 <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                                    <div class="title-form-group">
                                       <h4>Check Mark</h4>
                                       <input id="checkBox" type="checkbox" value="Daily" name="bulletin[]"><br>
                                       <input id="checkBox" type="checkbox" value="Monthly" name="bulletin[]"><br>
                                       <input id="checkBox" type="checkbox" value="Comparion by Week/Month/Year" name="bulletin[]"><br>
                                       <input id="checkBox" type="checkbox" value="Notification of Sales Tax Payment" name="bulletin[]"><br>
                                       <input id="checkBox" type="checkbox" value="Any time pulled out history for your sales report" name="bulletin[]"><br>
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                                    <div class="title-form-group">
                                    </div>
                                 </div>
                                 <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                                    <div class="title-form-group">
                                       <h4>Employee Management</h4>
                                    </div>
                                 </div>
                                 <div class="col-lg-4 col-md-4 col-sm-3 col-xs-12">
                                    <div class="title-form-group">
                                       <br>
                                       <br>
                                       <p>Schedule</p>
                                       <p>Clock In/Out</p>
                                       <p>Their Work Report</p>
                                       <p>Reporting Hrs. for Payroll</p>
                                    </div>
                                 </div>
                                 <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                                    <div class="title-form-group">
                                       <h4>Check Mark</h4>
                                       <input id="checkBox" type="checkbox" value="Schedule" name="employee[]"><br>
                                       <input id="checkBox" type="checkbox" value="Clock In/Out" name="employee[]"><br>
                                       <input id="checkBox" type="checkbox" value="Their Work Report" name="employee[]"><br>
                                       <input id="checkBox" type="checkbox" value="Reporting Hrs. for Payroll" name="employee[]"><br>
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                                    <div class="title-form-group">
                                    </div>
                                 </div>
                                 <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                                    <div class="title-form-group">
                                       <h4>Taxation</h4>
                                    </div>
                                 </div>
                                 <div class="col-lg-4 col-md-4 col-sm-3 col-xs-12">
                                    <div class="title-form-group">
                                    </div>
                                 </div>
                                 <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                                    <div class="title-form-group">
                                       <br>
                                       <input id="checkBox" type="checkbox" value="Taxation" name="taxation">
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                                    <div class="title-form-group">
                                    </div>
                                 </div>
                                 <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                                    <div class="title-form-group">
                                       <h4>Due Dates</h4>
                                    </div>
                                 </div>
                                 <div class="col-lg-4 col-md-4 col-sm-3 col-xs-12">
                                    <div class="title-form-group">
                                       <br>
                                       <br>
                                       <p>Inform about due dates for the following</p>
                                       <p>Secertary of State annual Renewal</p>
                                       <p>Tobacco License</p>
                                    </div>
                                 </div>
                                 <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                                    <div class="title-form-group">
                                       <h4>Check Mark</h4>
                                       <input id="checkBox" type="checkbox" value="Inform about due dates for the following" name="due_date[]"><br>
                                       <input id="checkBox" type="checkbox" value="Secertary of State annual Renewal" name="due_date[]"><br>
                                       <input id="checkBox" type="checkbox" value="Tobacco License" name="due_date[]"><br>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-row">		
                                    <label style="font-size: 14px;text-align: center;display: inherit;"><input id="checkBox text1" type="checkbox" value="Terms and conditions and agreement" name="terms"  onclick="OnChangeCheckbox (this)"> Terms and conditions and agreement</label><a href="#" class="ag">View  Agreement</a>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;margin-bottom:2%;">
                           <ul class="pager wizard">
                              <li class="previous first" style="display:none;"><a href="#" class="btn btn-primary">First</a></li>
                              <li class="previous"><a href="#" class="nex1">Previous</a></li>
                              <li class="next last" style="display:none;"><a href="#" class="btn btn-primary">Last</a></li>
                              <li class="next"><a href="#" class="nex11">Next</a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="tab-pane" id="step-3" data-step="3">
                     <div id="form-step-3" role="form" data-toggle="validator">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-reg-sub-header-div">
                           <h3 class="Libre fsc-reg-sub-header">Price and Payment Info</h3>
                        </div>
                        <div class="form-group">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                              <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">		
                                 <label class="fsc-form-label">Monthly Fee : </label>
                              </div>
                              <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                 <input type="text" class="form-control fsc-input" onkeypress="return isNumberKey(event)" id="monthly_fee" name="monthly_fee" placeholder="Monthly Fee" value="">
                              </div>
                              <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-row">		
                                 <label class="fsc-form-label">Coupan Code : </label>
                              </div>
                              <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                 <input type="text" class="form-control fsc-input " id="coupan" name="coupan" placeholder="Coupan Code" value="">
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                              <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">		
                                 <label class="fsc-form-label">Yearly Fee :</label>
                              </div>
                              <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                 <input type="text" class="form-control fsc-input" onkeypress="return isNumberKey(event)" placeholder="Yearly Fee" name="yearly_fee" id="yearly_fee"  onblur="getTotal()">
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                              <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">		
                                 <label class="fsc-form-label">Amount : </label>
                              </div>
                              <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                 <input type="text" placeholder="Amount" onkeypress="return isNumberKey(event)" readonly class="form-control fsc-input" name="amount" id="amount" value=""> 
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                              <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">		
                                 <label class="fsc-form-label">Credit Card No : <span class="star-required">*</span></label>
                              </div>
                              <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                 <input type="text" class="form-control fsc-input" id="credit_card" name="credit_card" placeholder="Credit Card No" value="">
                              </div>
                           </div>
                        </div>
                        <div class="form-group" id="test">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                              <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">		
                                 <label class="fsc-form-label">Expire : <span class="star-required">*</span></label>
                              </div>
                              <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                 <div class="row">
                                    <div class="col-md-12">
                                       <input type="text" class="form-control fsc-input" readonly id="expire" name="expire" placeholder="Expire" value="">
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                              <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">		
                                 <label class="fsc-form-label">Security Code : <span class="star-required">*</span></label>
                              </div>
                              <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                 <div class="row">
                                    <div class="col-md-12">
                                       <input type="text" class="form-control fsc-input" maxlength="5" id="Security Code" name="security_code" placeholder="Security Code" value="">
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                              <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">		
                                 <label class="fsc-form-label">Credit Card Holder Name : <span class="star-required">*</span></label>
                              </div>
                              <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                 <input type="text" class="form-control fsc-input textonly" id="credit_card_holder_name" name="credit_card_holder_name" placeholder="Credit Card Holder Name" value="">
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                              <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">		
                                 <label class="fsc-form-label">Address : <span class="star-required">*</span></label>
                              </div>
                              <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                 <input type="text" class="form-control fsc-input" id="credit_address" name="credit_address" placeholder="Address" value="">
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                              <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">		
                                 <label class="fsc-form-label">City / State / Zip : <span class="star-required">*</span></label>
                              </div>
                              <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                 <input type="text" class="form-control fsc-input textonly" id="credit_city" name="credit_city" placeholder="City" value="" required>
                              </div>
                              <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                 <select name="credit_state" id="credit_state" class="form-control fsc-input" required>
                                    <option value="ID">ID</option>
                                    <option value="AK">AK</option>
                                    <option value="AS">AS</option>
                                    <option value="AZ">AZ</option>
                                    <option value="AR">AR</option>
                                    <option value="CA">CA</option>
                                    <option value="CO">CO</option>
                                    <option value="CT">CT</option>
                                    <option value="DE">DE</option>
                                    <option value="DC">DC</option>
                                    <option value="FM">FM</option>
                                    <option value="FL">FL</option>
                                    <option value="GA">GA</option>
                                    <option value="GU">GU</option>
                                    <option value="HI">HI</option>
                                    <option value="ID">ID</option>
                                    <option value="IL">IL</option>
                                    <option value="IN">IN</option>
                                    <option value="IA">IA</option>
                                    <option value="KS">KS</option>
                                    <option value="KY">KY</option>
                                    <option value="LA">LA</option>
                                    <option value="ME">ME</option>
                                    <option value="MH">MH</option>
                                    <option value="MD">MD</option>
                                    <option value="MA">MA</option>
                                    <option value="MI">MI</option>
                                    <option value="MN">MN</option>
                                    <option value="MS">MS</option>
                                    <option value="MO">MO</option>
                                    <option value="MT">MT</option>
                                    <option value="NE">NE</option>
                                    <option value="NV">NV</option>
                                    <option value="NH">NH</option>
                                    <option value="NJ">NJ</option>
                                    <option value="NM">NM</option>
                                    <option value="NY">NY</option>
                                    <option value="NC">NC</option>
                                    <option value="ND">ND</option>
                                    <option value="MP">MP</option>
                                    <option value="OH">OH</option>
                                    <option value="OK">OK</option>
                                    <option value="OR">OR</option>
                                    <option value="PW">PW</option>
                                    <option value="PA">PA</option>
                                    <option value="PR">PR</option>
                                    <option value="RI">RI</option>
                                    <option value="SC">SC</option>
                                    <option value="SD">SD</option>
                                    <option value="TN">TN</option>
                                    <option value="TX">TX</option>
                                    <option value="UT">UT</option>
                                    <option value="VT">VT</option>
                                    <option value="VI">VI</option>
                                    <option value="VA">VA</option>
                                    <option value="WA">WA</option>
                                    <option value="WV">WV</option>
                                    <option value="WI">WI</option>
                                    <option value="WY">WY</option>
                                 </select>
                              </div>
                              <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                 <input type="text" class="form-control fsc-input zip" maxlength="6"  id="credit_zip" name="credit_zip" placeholder="Pin Code" value="" required>
                                 <select class="form-control bfh-timezones" style="display:none" name="timezone" data-country="countries_states1"></select>
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;margin-bottom:2%;">
                              <ul class="pager wizard">
                                 <li class="previous"><a href="#" class="nex1">Previous</a></li>
                                 <li class="next last"><input type="submit" value="Ok" class="pull-right btn fsc-apply-btn"></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- <ul class="pager wizard">
               <li class="previous first" style="display:none;"><a href="#" class="btn btn-primary">First</a></li>
               <li class="previous"><a href="#" class="">Previous</a></li>
               <li class="next last" style="display:none;"><a href="#" class="btn btn-primary">Last</a></li>
               <li class="next"><a href="#" class="">Next</a></li>
               </ul>-->
         </form>
      </div>
   </div>
</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js"></script>
<script  type="text/javascript"src="https://cdn.jsdelivr.net/bootstrap.wizard/1.3.2/jquery.bootstrap.wizard.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
<script type="text/javascript">
   //$("#business_no").mask("(999)-999-9999");
   //$("#zip").mask("99999");
   //$("#businessext").mask("99999");
   //$("#telephone1ext").mask("99999");
   //$("#telephone2ext").mask("99999");
   //$("#business_fax").mask("(999)-999-9999");
   //$("#telephone1").mask("(999)-999-9999");
   //$("#telephone2").mask("(999)-999-9999");
   //$("#business_fax").mask("(999)-999-9999");
   
   $.ajaxSetup({headers:{'X-CSRF-Token': $('input[name="_token"]').val()}});
   $(document).ready(function() {
   
                  $.validator.addMethod("email", function(value, element) {
                   return this.optional(element) || /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(value);
               }, "Email Address is invalid: Please enter a valid email address.");
     var $validator = $("#registrationForm").validate({
       rules: { 
           
       @if(Request::segment(4)=='Personal')   
        first_name: {
           required: true,
          
          },
         
          last_name: {
           required: true,
         
          },
   
       @else
        first_name: {
           required: true,
          
          },
         
          last_name: {
           required: true,
      
          },
         company_name: {
           required: true,
         
         },
         business_name: {
           required: true,
          
          },
         
          @endif
        typeofservice: {
           required: true,
          
         },
         
          type_of_business: {
           required: true,
           
          },
         address
         : {
           required: true,
           
          },
          countryId: {
           required: true,
           
          },
          stateId: {
           required: true,
           
          },
          zip: {
           required: true,
           
          },
          city: {
           required: true,
           
          },
     credit_zip: {
           required: true,
           
          },
          telephone1: {
           required: true,
           
          },
         website: {
         required: false,
         url: true
       }
   ,
   
    'employee[]': {
                   required: true
               },
   'bulletin[]': {
                   required: true
               },
   taxation: {
                   required: true
               },
   'due_date[]': {
                   required: true
               },
          business_no: {
           required: true,
           
          },
   credit_card: {
     required: true,
     //creditcard: true,
     minlength: 13,
     maxlength: 16,
     digits: true,
    
   },
         email: {
           required: true,
           email: true,
   /*remote: {
       url: "{{ URL::to('/checkclient') }}",
       type: "post"
      }*/
          
          },
   terms: {
           required: true,
           
          },
          businesstype: {
           required: true,
           
          }
          ,
          telephone1type: {
           required: true,
           
          }
   
       },
    messages: {
   credit_card: {
     required: "Please enter a valid credit card number",
     minlength: "Please enter a valid credit card number",
     maxlength: "Please enter a valid credit card number",
     digits: "Your credit card number cannot contain spaces.",
     
   },
   "bulletin[]": "Please select Bulletin",
   "employee[]": "Please select Employee Management",
   
       "due_date[]": "Please select Due Dates",
    taxation: "Please select Taxation",
   
   email: {
               required: "Please Enter Your Email Address",
               email: "Please Enter A Valid Email Address",
               remote: "Email already in use!"            
           },
   typeofservice: {
               required: "Please Select Your Type of Service",
               //email: "Please Enter A Valid Email Address",            
           },
           city: {
               required: "Please Enter Your City",
               //email: "Please Enter A Valid Email Address",            
           },
           stateId: {
               required: "Please Select Your State",
               //email: "Please Enter A Valid Email Address",            
           },
           countryId: {
               required: "Please Select Your Country",
               //email: "Please Enter A Valid Email Address",            
           },
       @if(Request::segment(4)=='Personal')   
       first_name: {
               required: "Please Select Your First Name",
          pattern: "The Textbox string format is invalid",           
           },
   last_name: {
               required: "Please Select Your Last Name",
              pattern: "The Textbox string format is invalid",            
           },
       @else
        first_name: {
               required: "Please Select Your First Name",
               pattern: "The Textbox string format is invalid",          
           },
   last_name: {
               required: "Please Select Your Last Name",
               pattern: "The Textbox string format is invalid",          
           },
       company_name: {
               required: "Please Select Your Company Name",
               //email: "Please Enter A Valid Email Address",            
           },
   business_name: {
               required: "Please Select Your Business Name",
               //email: "Please Enter A Valid Email Address",            
           },
       @endif
   
   type_of_business: {
               required: "Please Select Your Type of Business",
               //email: "Please Enter A Valid Email Address",            
           },
   address: {
               required: "Please Select Your Business Address",
               //email: "Please Enter A Valid Email Address",            
           },
   zip: {
               required: "Please Enter Your zip",
               //email: "Please Enter A Valid Email Address",            
           },
   businesstype: {
               required: "Please Select Your Type",
               //email: "Please Enter A Valid Email Address",            
           },
           telephone1type: {
               required: "Please Select Your Type",
               //email: "Please Enter A Valid Email Address",            
           },
   credit_zip: {
               required: "Please Enter Your zip",
               //email: "Please Enter A Valid Email Address",            
           },
   
   business_no: {
               required: "Please Select Your Telephone ",                     
           },
   
   telephone1: {
               required: "Please Select Your Telephone ",                     
           },
   
   
   terms: {
               required: "Please Check Your Term and Condition",
                         
           },
   
       },
   errorElement: "em",
   				errorPlacement: function ( error, element ) {
   					// Add the `help-block` class to the error element
   					error.addClass( "help-block" );
   					// Add `has-feedback` class to the parent div.form-group
   					// in order to add icons to inputs
   					element.parents( ".form-group" ).addClass( "has-feedback" );
   					if ( element.prop( "type" ) === "checkbox" ) {
   						error.insertAfter( element.parent( "label" ) );
   					} else {
   						error.insertAfter( element );
   					}
   					// Add the span element, if doesn't exists, and apply the icon classes to it.
   					if ( !element.next( "span" )[ 0 ] ) {
   						$( "<span class='glyphicon glyphicon-remove form-control-feedback'></span>" ).insertAfter( element );
   					}
   				},
   				success: function ( label, element ) {
   					// Add the span element, if doesn't exists, and apply the icon classes to it.
   					if ( !$( element ).next( "span" )[ 0 ] ) {
   						$( "<span class='glyphicon glyphicon-ok form-control-feedback'></span>" ).insertAfter( $( element ) );
   					}
   				},
   				highlight: function ( element, errorClass, validClass ) {
   					 $(element).closest('.form-group, .has-feedback').removeClass('has-success').addClass('has-error')
   					$( element ).next( "span" ).addClass( "glyphicon-remove" ).removeClass( "glyphicon-ok" );
   				},
   				unhighlight: function ( element, errorClass, validClass ) {
   					$(element).closest('.form-group, .has-feedback').removeClass('has-error').addClass('has-success');
   					$( element ).next( "span" ).addClass( "glyphicon-ok" ).removeClass( "glyphicon-remove" );
   				}
     });
   
     $('#smartwizard').bootstrapWizard({
       'tabClass': 'nav nav-pills',
       'onNext': function(tab, navigation, index) {
         var $valid = $("#registrationForm").valid();
   if(index > 1)
   {
   $(".ddd").addClass('active1');
   }
   if(index == 1)
   { //alert();
   $(".ddd").removeClass('active1');
   }
         if (!$valid) {
           $validator.focusInvalid();
           return false;
         }
       },
       'onTabClick': function(activeTab, navigation, currentIndex, nextIndex) {
         if (nextIndex <= currentIndex) {
           return;
         }
         var $valid = $("#registrationForm").valid();
         if (!$valid) {
           $validator.focusInvalid();
           return false;
         }
         if (nextIndex > currentIndex+1){
          return false;
         }
       }
     });
   });
</script>
<script>
   var dat1 = $('#telephone1ext').val();
   $('#telephone1type').on('change', function() {
   
   if(this.value=='Office')
   {
   document.getElementById('telephone1ext').removeAttribute('readonly');
   $('#telephone1ext').val(); 
   }
   else
   {
   document.getElementById('telephone1ext').readOnly =true;
   $('#telephone1ext').val('');
   }
   })
</script>
<script>
   var dat1 = $('#telephone2ext').val();
   $('#telephone2type').on('change', function() {
   
   if(this.value=='Office')
   {
   document.getElementById('telephone2ext').removeAttribute('readonly');
   $('#telephone2ext').val(); 
   }
   else
   {
   document.getElementById('telephone2ext').readOnly =true;
   $('#telephone2ext').val('');
   }
   })
</script>
<script>
   var dat1 = $('#businessext').val();
   $('#businesstype').on('change', function() {
   
   if(this.value=='Office')
   {
   document.getElementById('businessext').removeAttribute('readonly');
   $('#businesstype').val(); 
   }
   else
   {
   document.getElementById('businessext').readOnly =true;
   $('#businessext').val('');
   }
   })
</script>
<script>
   jQuery.fn.getNum = function() {
       var val = $.trim($(this).val());
       if(val.indexOf(',') > -1) {
           val = val.replace(',', '.');
       }
       var num = parseFloat(val);
       var num = num.toFixed(2);
       if(isNaN(num)) {
           num = '';
       }
       return num;
   }
   
   $(function() {
   
       $('#monthly_fee,#yearly_fee,#amount').blur(function() {
           $(this).val($(this).getNum());
       });
   
   });
</script>
<script>
   $('input').blur(function(){
      $('input[name=amount]').val( +($('input[name=yearly_fee]').val()) +(+ $('input[name=monthly_fee]').val()) );
   });
   
</script>
<script>
   function FillBilling(f) { 
   if(f.billingtoo.checked == true) {
   // f.first_name_1.value = f.first_name.value;
    //f.middle_name_1.value = f.middle_name.value;
   // f.last_name_1.value = f.last_name.value;
       f.telephone1.value = f.business_no.value;
   f.telephone1type.value = f.businesstype.value;
   f.telephone1ext.value = f.businessext.value;
   }
   else{
    f.first_name_1.value = '';
    f.middle_name_1.value = '';
    f.last_name_1.value = '';
       f.telephone1.value ='';
   f.telephone1type.value = '';
   f.telephone1ext.value = '';
   }
   }
</script>
<script>
   function FillBilling1(f) {
   if(f.billingtoo1.checked == true) {
    f.telephone1.value = f.business_no.value;
   f.telephone1type.value = f.businesstype.value;
   f.telephone1ext.value = f.businessext.value;
   
   }else{
        f.telephone1.value ='';
   f.telephone1type.value = '';
   f.telephone1ext.value = '';
   
   }
   }
   }
</script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<script>
   $(document).ready(function() {
   var dateInput = $('input[name="expire"]'); // Our date input has the name "date"
   var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : 'body';
   dateInput.datepicker({
    format: 'M/yyyy',
    container: container,
    todayHighlight: true,
    autoclose: true,
    startDate: truncateDate(new Date()) // <-- THIS WORKS
   });
   
   $('#expire').datepicker('setStartDate', truncateDate(new Date()));
   // <-- SO DOES THIS
   });
   
   function truncateDate(date) {
   return new Date(date.getFullYear(), date.getMonth(), date.getDate());
   }
   
   
   $(document).ready(function(){
   /***phone number format***/
   $(".phone").keypress(function (e) {
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
      return false;
    }
    var curchr = this.value.length;
    var curval = $(this).val();
    if (curchr == 3 && curval.indexOf("(") <= -1) {
      $(this).val("(" + curval + ")" + " ");
    } else if (curchr == 4 && curval.indexOf("(") > -1) {
      $(this).val(curval + ")-");
    } else if (curchr == 5 && curval.indexOf(")") > -1) {
      $(this).val(curval + "-");
    } else if (curchr == 9) {
      $(this).val(curval + "-");
      $(this).attr('maxlength', '14');
    }
   });
   });
</script>
@endsection()