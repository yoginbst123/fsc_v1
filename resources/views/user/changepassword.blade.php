@extends('layouts.app')
@section('main-content')
<div class="content-wrapper">
   <div class="page-title">
      <h1>Change Password</h1>
   </div>
   <div class="row">
      <div class="col-md-12">
         <div class="card">
            <div class="card-body">
               @if(count($errors) > 0)
               @foreach($errors->all() as $error)
               <p class="alert alert-danger">{{ $error }}</p>
               @endforeach
               @endif  
               @if(session()->has('success'))
               <div class="alert alert-success alert-dismissable">{{ session()->get('success') }}</div>
               @endif
               @if ( session()->has('error') )
               <div class="alert alert-danger alert-dismissable">{{ session()->get('error') }}</div>
               @endif    
               <form method="post" class="form-horizontal" enctype="multipart/form-data" id="" action="{{ route('changepassword.update	',Auth::user()->id)}}">
                  {{csrf_field()}} 
                  {{method_field('PATCH')}}
                  <div class="row">
                     <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                           <label class="control-label col-md-3">Old Password :</label>
                           <div class="col-md-8">
                              <input type="password" class="form-control" id="oldpassword" value="" name="oldpassword">								   
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-md-3">New Password :</label>
                           <div class="col-md-8">
                              <input name="password" value="" type="password" id="password" class="form-control" />
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-md-3">Confirm Password :</label>
                           <div class="col-md-8">
                              <input name="cpassword" value="" type="password" id="cpassword" class="form-control"/>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="card-footer">
                     <div class="row">
                        <div class="col-md-8 col-md-offset-3">
                           <input class="btn btn-primary icon-btn" type="submit" name="submit" value="Change Password">
                        </div>
                     </div>
                  </div>
                  <br>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection()

