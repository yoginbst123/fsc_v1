@extends('front-section.app')
@section('main-content')
<style>
.has-feedback label ~ .form-control-feedback {
    top: 39px;
}
.fsc-form-submit {
    float: none;
    margin: 0 auto;
    display: table;
}
</style>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>

<div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">
@if ( session()->has('success') )
    <div class="alert alert-success alert-dismissable">{{ session()->get('success') }}</div>
@endif
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 fsc-content-head">
			<h4>Contact Us</h4>
		</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 fsc-section-head">
			<h4>Contact Us</h4>
		</div>
		<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12"></div>
		<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 fsc-section-head">
			<h4>Address</h4>
		</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
 <form class="form-horizontal" action="{{route('contacts.store')}}" id="contactForm" method="post">
             {{csrf_field()}}       
<div class="form-group">
                      <label for="name" class="fsc-form-label">Name</label>
<input type="text" class="form-control  fsc-input" id="name" name="name" placeholder="Full Name">
                    </div>
                    <div class="form-group">
                      <label for="email" class="fsc-form-label">Email</label>
                      <input type="email" class="form-control  fsc-input" id="email" name="email" placeholder="Email">
                    </div>
<div class="form-group">
                      <label for="telephone" class="fsc-form-label">Telephone No.</label>
                      <input type="tel" class="form-control fsc-input phone" placeholder="(999)-999-9999"  style="width:41%" id="telephone" name="telephone">
 </div>
                    <div class="form-group">
                      <label for="message" class="fsc-form-label">Message</label>
                     <textarea  class="form-control fsc-input" @if(!empty($link)) rows="3" @else rows="7" @endif name="message" placeholder="Message">@if(!empty($link)){{Request::segment(3)}} {{Request::segment(4)}}
                    @endif</textarea> 
    
                    <input type="hidden" class="" placeholder="Telephone No" value="@if(!empty($link)){{Request::segment(3)}}@endif">
                    <!--<input type="text" class="" placeholder="Telephone No" name="post_id">-->

                    </div>
                    <br>
<!--<div class="form-group">
        <label class="col-xs-6 col-md-4 col-sm-6 control-label" id=""></label>
        <div class="col-xs-4">
      <div class="g-recaptcha" data-sitekey="6Lcr7OAZAAAAABHegl_spRSD_UnwaTd8Z6sSNeHZ"></div>
      </div>
    </div>!-->
    
    
    <br>
                    <button type="submit" class="btn btn-primary btn-lg fsc-form-submit">Send</button>
                  </form>
</div>
		<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12"></div>
		<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
			<ul class="fsc-map-ul">
				<li><h4 class="fsc-contact-text">&nbsp;&nbsp;{{$contact->placename}}</h4></li>
			</ul>
			<ul class="fsc-phone-ul">
				<li><h4 class="fsc-contact-text">&nbsp;&nbsp;+1 - {{$contact->phone}}</h4></li>
			</ul>
			<ul class="fsc-fax-ul">
				<li><h4 class="fsc-contact-text">&nbsp;&nbsp;+1 - {{$contact->fax}}</h4></li>
			</ul>
			<ul class="fsc-mail-ul">
				<li><h4 class="fsc-contact-text">&nbsp;&nbsp;{{$contact->email}}</h4></li>
			</ul>
<div class="" style="margin-top:30px">
			<button class="btn btn-primary btn-lg fsc-form-submit" type="button" onclick="viewDirection()">Location Base Direction</button>
<br>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-section-head">
			<h4>Map</h4>
		</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="map"></div>
		</div>
		</div>
	</div>
</div>
<style>
	#map { height:250px; }
</style>
<script type="text/javascript">
	function initMap() {
		var uluru = {lat: {!!$contact->latitude!!}, lng: {!!$contact->longitude!!}};
		var map = new google.maps.Map(document.getElementById('map'), {
			zoom: 15,
			center: uluru
		});
		var contentString = '<div id="content">'+
		'<div id="siteNotice">'+
		'</div>'+
		'<h3 id="firstHeading" class="firstHeading">Financial Service Center</h3>'+
		'<div id="bodyContent">'+
		'<p>{{$contact->placename}}</p>'+
		'</div>'+
		'</div>';

		var infowindow = new google.maps.InfoWindow({
			content: contentString
		});
		var marker = new google.maps.Marker({
			position: uluru,
			map: map,
			title: 'Financial Service Center'
		});
		marker.addListener('click', function() {
			infowindow.open(map, marker);
		});
	}
	function viewDirection(){
		window.open("https://www.google.com/maps?saddr=current+location&daddr={{$contact->placename}}")
	}
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCB0jTpR9kuRRdOc2iU45FwDc3D6S86jFU&callback=initMap"></script>




<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js"></script>>

<script>
$(document).ready(function() {
 function randomNumber(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    }

   /* function generateCaptcha() {
        $('#captchaOperation').html([randomNumber(1, 100), '+', randomNumber(1, 200), '='].join(' '));
    }

    generateCaptcha();*/
    $('#contactForm').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
           name: {
                    
                    validators: {
                        notEmpty: {
                            message: 'The first name is required'
                        }
                    }
                },
               
                telephone: {
                    validators: {
                        notEmpty: {
                            message: 'The phone number is required'
                        },
                        regexp: {
                            message: 'The phone number can only contain the digits, spaces, -, (, ), + and .',
                            regexp: /^[0-9\s\-()+\.]+$/
                        }
                    }
                },
                email: {
                    validators: {
                        notEmpty: {
                            message: 'The email address is required'
                        },
                        emailAddress: {
                            message: 'The input is not a valid email address'
                        }
                    }
                },
                message: {
                    validators: {
                        notEmpty: {
                            message: 'The message is required'
                        },
                        stringLength: {
                            max: 700,
                            message: 'The message must be less than 700 characters long'
                        }
                    }
                },
            comment: {
                validators: {
                      stringLength: {
                        min: 10,
                        max: 200,
                        message:'Please enter at least 10 characters and no more than 200'
                    },
                    notEmpty: {
                        message: 'Please supply a description of your project'
                    }
                    }
                }
            }
        })
        .on('success.form.bv', function(e) {
            $('#success_message').slideDown({ opacity: "show" }, "slow") // Do something ...
                $('#contactForm').data('bootstrapValidator').resetForm();

            // Prevent form submission
            e.preventDefault();

            // Get the form instance
            var $form = $(e.target);

            // Get the BootstrapValidator instance
            var bv = $form.data('bootstrapValidator');

            // Use Ajax to submit form data
            $.post($form.attr('action'), $form.serialize(), function(result) {
                console.log(result);
            }, 'json');
        });
});
$(document).ready(function(){
  /***phone number format***/
  $(".phone").keypress(function (e) {
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
      return false;
    }
    var curchr = this.value.length;
    var curval = $(this).val();
    if (curchr == 3 && curval.indexOf("(") <= -1) {
      $(this).val("(" + curval + ")" + " ");
    } else if (curchr == 4 && curval.indexOf("(") > -1) {
      $(this).val(curval + ")-");
    } else if (curchr == 5 && curval.indexOf(")") > -1) {
      $(this).val(curval + "-");
    } else if (curchr == 9) {
      $(this).val(curval + "-");
      $(this).attr('maxlength', '14');
    }
  });
});
</script>
@endsection()