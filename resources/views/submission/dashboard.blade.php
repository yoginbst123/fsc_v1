@extends('submission.layouts.app')
@section('main-content')
<style>
   table tr th { border:solid 1px #000; }
   table tr td { border:solid 1px #000; }
   label.file-upload {
   position: relative;
   overflow: hidden;
   float: left;
   }
   .fsc-form-label{margin: 8px 0 0 0 !important;}
   input[type="file"] {
   position: absolute;
   top: 0;
   right: 0;
   min-width: 100%;
   min-height: 100%;
   font-size: 100px;
   text-align: right;
   filter: alpha(opacity=0);
   opacity: 0;
   outline: none;
   background: white;
   cursor: inherit;
   display: block;
   font-size:16px;
   }
   .table-title{ margin-bottom:15px; float:right; }
   .table-title a {
   display: inline-block;
   float: right;
   font-size: 14px;
   color: #FFF;
   background: #2196f3;
   padding: 4px 15px 6px;
   border: 1px solid #333;
   }
   .table { border:solid 1px #000; }
   .table thead tr th{ border:solid 1px #000; background:#ffff99 !important; }
   .table tbody tr td{ border:solid 1px #000; }
   .table tbody tr th{ border:solid 1px #000; background:#ffff99 !important; }
</style>
<div class="content-wrapper">
   <section class="content-header page-title">
      <h1>Dashboard</h1>
   </section>
   <section class="content">
      <div class="">
         <?php if(isset($_REQUEST['radio'])){?>
         <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="col-md-1 col-sm-1 col-xs-1"></div>
               <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 fsc-content-box">
                  <form class="form-horizontal" method="POST" id="client-login" action="">
                     <div id="smartwizard register_container">
                        <div class="tab-content">
                           <div id="step-1" role="form">
                              <div class="col-md-6 col-sm-6 col-xs-12" style="margin-top:0%;">
                                 <div class="form-group">
                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                       <label class="fsc-form-label">Client ID:<span class="star-required">*</span></label>
                                    </div>
                                    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                       <input id="clientid" type="text" value="{{$com2->filename}}" placeholder="Client ID:" class="form-control fsc-input" name="clientid" autofocus="">
                                    </div>
                                 </div>
                              </div>
                              
                              <div class="col-md-6 col-sm-6 col-xs-12" style="margin-top:0%;">
                                 <div class="form-group">
                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row" style=" width: 26%;">
                                       <label class="fsc-form-label">Client Name: <span class="star-required">*</span></label>
                                    </div>
                                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin" style=" width:57%">
                                       <input type="text" placeholder="Client Name" class="form-control fsc-input" name="clientname"  value="{{$com2->first_name.' '.$com2->middle_name.' '.$com2->last_name}}"autofocus="">
                                    </div>
                                 </div>
                              </div>
                              
                              <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:0%;">
                                 <div class="form-group">
                                    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                       <label class="fsc-form-label">Business Name: <span class="star-required">*</span></label>
                                    </div>
                                    <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                       <input type="text" placeholder="Business Name" class="form-control fsc-input" name="businessname"  value="{{$com2->business_name}}" autofocus="">
                                    </div>
                                 </div>
                              </div>
                              
                              <div class="col-md-6 col-sm-6 col-xs-12" style="margin-top:0%;">
                                 <div class="form-group">
                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                       <label class="fsc-form-label">Physical Address: <span class="star-required">*</span></label>
                                    </div>
                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                       <input type="text" placeholder="Physical Address" class="form-control fsc-input" name="address" value="{{$com2->address}}" autofocus="">
                                    </div>
                                 </div>
                              </div>
                              
                              <div class="col-md-6 col-sm-6 col-xs-12" style="margin-top:0%;">
                                 <div class="form-group">
                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row" style="width: 31%;">
                                       <label class="fsc-form-label">City / State / Zip: <span class="star-required">*</span></label>
                                    </div>
                                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin" style="width: 52%;">
                                       <input type="text" placeholder="City / State / Zip" class="form-control fsc-input" name="email" value="{{$com2->city}} / {{$com2->stateId}} / {{$com2->zip}}" autofocus="">
                                    </div>
                                 </div>
                              </div>
                              
                              
                              <div class="col-md-6 col-sm-6 col-xs-12" style="margin-top:0%;">
                                 <div class="form-group">
                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                       <label class="fsc-form-label">Period: <span class="star-required">*</span></label>
                                    </div>
                                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                       <div class="row">
                                           <div class="col-sm-6">
                                               <select type="text" class="form-control" id="duedate" name="duedate">
                                                    <option value="">Month</option>
<?php // set the month array 
$formattedMonthArray = array("1" => "January", "2" => "February", "3" => "March", "4" => "April","5" => "May", "6" => "June", "7" => "July", "8" => "August","9" => "September", "10" => "October", "11" => "November", "12" => "December",);
$monthArray = range(1, 12);
$date1 = date('m');
foreach ($monthArray as $month) 
{        // if you want to select a particular month
$selected = ($month == $date1) ? 'selected' : '';
// if you want to add extra 0 before the month uncomment the line below
//$month = str_pad($month, 2, "0", STR_PAD_LEFT);
echo '<option '.$selected.' value="'.$month.'">'.$formattedMonthArray[$month].'</option>';
}
?>
                        </select>
                        </div>
                        <div class="col-sm-6">
                            <?php
// set start and end year range
$yearArray = range(1980, 2050);
?>
                                               <select type="text" class="form-control" id="duedate" name="duedate">
                                                    <option value="">Select Year</option>
                                                   <?php $date = date('Y');
    foreach ($yearArray as $year) {
        // if you want to select a particular year
        $selected = ($year == $date) ? 'selected' : '';
        echo '<option '.$selected.' value="'.$year.'">'.$year.'</option>';
    }
    ?>
                                                </select>
                                           </div>
                                          </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
               <div class="table-responsive">
                  <table class="table table-bordered rowfy" id="myTable">
                     <thead>
                        <tr>
                           <th>No</th>
                           <th>Vendor Name</th>
                           <th>Invoice #</th>
                           <th>Invoice Dt</th>
                           <th>Amount</th>
                           <th>Amount</th>
                           <th>Amount</th>
                           <th>Shipping Charges</th>
                           <th>Total</th>
                           <th>Upload</th>
                           <th></th>
                        </tr>
                     </thead>
                     <tbody>
                        <tr>
                           <td>1 </td>
                           <td>
                              <select name="vendorname[]" type="text" id="vendorname" class="form-control">
                                 <option value=""> Select</option>
                                 @foreach($employee as $emp1)
                                 <option value="{{$emp1->id}}">{{$emp1->business_name}}</option>
                                 @endforeach
                              </select>
                           </td>
                           <td>-</td>
                           <td>-</td>
                           <td>-</td>
                           <td>-</td>
                           <td>-</td>
                           <td>-</td>
                           <td>-</td>
                           <td> <label class="file-upload btn btn-primary">								
                              <input type="file" name="upload[]" id="upload" class="form-control">Upload</label> 
                           </td>
                        </tr>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
     
         <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
               <h2 class=" text-center">AFFIDAVIT</h2>
               <p>I certify, under the penalties for filing false returns, that I have personal knowledge and understanding of statements made in this return and that the figures presented </p>
               <p>herein, including accompanying schedules, are true, correct and complete to the best of my knowledge and belief, and are filed in accordance with the law.</p>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
               <h2 class=" text-center">Bookkeeper </h2>
               <p class=" text-right">9/8/2016</p>
               <div class="table-responsive">
                  <table class="table table-bordered">
                     <thead>
                        <tr>
                           <th>Signature of Owner, Partner or Recognized Officer</th>
                           <th>Title</th>
                           <th>Date</th>
                        </tr>
                     </thead>
                     <tbody>
                        <tr>
                           <td>-</td>
                           <td>-</td>
                           <td>-</td>
                        </tr>
                     <tbody>       
                  </table>
               </div>
            </div>
         </div>
      </div>
      <?php }?>
</div>
</section> 
</div>
<script>
   $(document).on('click', '.rowfy-addrow', function(){
   let rowfyable = $(this).closest('table');
   let lastRow = $('tbody tr:last', rowfyable).clone();
   $('input', lastRow).val('');
   $('tbody', rowfyable).append(lastRow);
   $(this).removeClass('rowfy-addrow btn-success').addClass('rowfy-deleterow btn-danger').text('-');
   });
   /*Delete row event*/
   $(document).on('click', '.rowfy-deleterow', function(){
   $(this).closest('tr').remove();
   });
   /*Initialize all rowfy tables*/
   $('.rowfy').each(function(){
   $('tbody', this).find('tr').each(function(){
     $(this).append('<td><button class="btn btn-sm ' + ($(this).is(":last-child") ? 'rowfy-addrow btn-success">+' : 'rowfy-deleterow btn-danger">-')+'</button></td>');
   });
   });
</script>
@endsection
