<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<img src="http://financialservicecenter.net/public/frontcss/images/fsc_logo.png" alt="img" style="width:100px;">
<br><br>
<table border ='1' style='color: #333;font-family: Helvetica, Arial, sans-serif;width:100%;border-collapse:collapse; border-spacing: 0;' >
    <tr style='background:#535da6;'><td colspan='2' style='text-align:center;font-size:18px;color:#FFF;padding:10px 0;'>You have received New Apply Service</td></tr>
    <tr>
        <td style='width:40%;text-align:left;font-weight:bold;border:1px solid #CCC;height:30px;padding:0 10px;'>
            <p style="margin-bottom:0px;">Full Name :</p>
        </td>
        <td style='width:60%;text-align:left;padding:0 10px; border: 1px solid #CCC; padding:0 10px; height: 30px;'>
            
            <p style="margin-bottom:0px;">
               {{ $fullname}}
            </p>
        </td>
    </tr>
     <tr>
        <td style='width:40%;text-align:left;font-weight:bold;border:1px solid #CCC;height:30px;padding:0 10px;'>
            <p style="margin-bottom:0px;">Email :</p>
        </td>
        <td style='width:60%;text-align:left;padding:0 10px;border: 1px solid #CCC; height: 30px;'>
           
            <p style="margin-bottom:0px;">
               {{ $email }}
            </p>
        </td>
    </tr>
       <tr>
        <td style='width:40%;text-align:left;font-weight:bold;border:1px solid #CCC;height:30px;padding:0 10px;'>
            <p style="margin-bottom:0px;">Type of Service :</p>
        </td>
        <td style='width:60%;text-align:left;padding:0 10px;border: 1px solid #CCC; height: 30px;'>
           
            <p style="margin-bottom:0px;">
               {{ $typeofservice}}
            </p>
        </td>
    </tr>
    <tr>
        <td style='width:40%;text-align:left;font-weight:bold;border:1px solid #CCC;height:30px;padding:0 10px;'>
            <p style="margin-bottom:0px;">New Business :</p>
        </td>
        <td style='width:60%;text-align:left;padding:0 10px;border: 1px solid #CCC; height: 30px;'>
           
            <p style="margin-bottom:0px;">
               {{ $newbusiness1}} 
            </p>
        </td>
    </tr>
    <tr>
        <td style='width:40%;text-align:left;font-weight:bold;border:1px solid #CCC;height:30px;padding:0 10px;'>
            <p style="margin-bottom:0px;">Company Legal Name :	</p>
        </td>
        <td style='width:60%;text-align:left;padding:0 10px;border: 1px solid #CCC; height: 30px;'>
           
            <p style="margin-bottom:0px;">
               {{ $company_name}}
            </p>
        </td>
    </tr>
    <tr>
        <td style='width:40%;text-align:left;font-weight:bold;border:1px solid #CCC;height:30px;padding:0 10px;'>
            <p style="margin-bottom:0px;">Business Name (DBA) :</p>
        </td>
        <td style='width:60%;text-align:left;padding:0 10px;border: 1px solid #CCC; height: 30px;'>
           
            <p style="margin-bottom:0px;">
              {{ $business_name}}
            </p>
        </td>
    </tr>
     <tr>
        <td style='width:40%;text-align:left;font-weight:bold;border:1px solid #CCC;height:30px;padding:0 10px;'>
            <p style="margin-bottom:0px;">Type of Business :</p>
        </td>
        <td style='width:60%;text-align:left;padding:0 10px;border: 1px solid #CCC; height: 30px;'>
           
            <p style="margin-bottom:0px;">
                {{ $type_of_business}}
            </p>
        </td>
    </tr>
     <tr>
        <td style='width:40%;text-align:left;font-weight:bold;border:1px solid #CCC;height:30px;padding:0 10px;'>
            <p style="margin-bottom:0px;">Business Location Address :	</p>
        </td>
        <td style='width:60%;text-align:left;padding:0 10px;border: 1px solid #CCC; height: 30px;'>
           
            <p style="margin-bottom:0px;">
               {{ $business_address}}
            </p>
        </td>
    </tr>
</table>
</body>
</html>