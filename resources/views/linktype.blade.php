@extends('front-section.app')
@section('main-content')
<style>
.fsc-label {
    color: #428bca;
    list-style: none;
}
.pager .disabled>a, .pager .disabled>a:focus, .pager .disabled>a:hover, .pager .disabled>span {
    color: #fff;
    cursor: not-allowed;
    background-color: #428bca;
    font-size: 14px;
}
.pager li>a, .pager li>span {
    display: inline-block;
    padding: 5px 14px;
    background-color: #0472d0;
    border: 1px solid #ddd;
    border-radius: 15px;
    font-size: 14px;
    color: #fff;
}
.main-image{ height: 217px !important;}
.carousel-inner{border: 2px solid #428bca;margin-top: 19px !important;}
p{font-size:17px; padding:10px}
</style>
<div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">
<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 fsc-content-head">
			<h4>FSC Community Link</h4>
		</div>
	</div>

@if($employment->isEmpty())
<div class="row">
    <div class="fsc-content-box col-xs-12">
<center>        <h2>Not Availble at this time</h2></center>
    </div>  </div>                                    
@else



	@foreach($employment as $employ)
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

<div class="row">
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  fsc-section-head"><h4 class="datecount"><span class="">{{$employ->post_date}}</span></h2></div>
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  fsc-section-head"><center>
@foreach($data as $data1)
<img src="{{url('public/linkcategory/')}}/{{$data1->linkimage}}" style="width:35%">
@endforeach


</center></div>
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  fsc-section-head"><h4 class="datecount"><span class="">ID :  {{$employ->post_id}}</span></h2></div>
</div>
		
	</div>
	
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-content-box">
		<div class="" style="margin-top: 2%;">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 "><center><h3 style="display: inline-block; background-color: #103b68;padding: 10px 20px;color: #fff;">Business Detail</h3></center>
</div>
			
			<!--<div class="col-lg-4 col-md-4 col-sm-4 col-xs-10">
				<ul>
					<li class="fsc-label" style='min-height:40px;'>Post ID:</li>
					<li class="fsc-label" style='min-height:40px;'>Job Description :</li>
				</ul>
			</div>-->

<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">

            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
@if(empty($employ->linkimage))
@else
<?php $images = explode('|',$employ->linkimage); ?>
<?php $i=1;?>
@foreach($images as $image)
<li data-target="#carousel-example-generic" data-slide-to="{{$i+1}}" class="@if ($loop->first) active @endif"></li>
<?php $i++;?>
@endforeach 

@endif
</ol>
<div class="carousel-inner">
@if(empty($employ->linkimage))
<img src="{{url('public/images/')}}/noimage.png" class="img-responsive"> @else
<?php $images = explode('|',$employ->linkimage); ?>
@foreach($images as $image)
<div class="item @if ($loop->first) active @endif">
<img src="{{asset('public/link')}}/{{$image}}" alt="" class="main-image" />   
  </div>
@endforeach @endif
 </div> 
 <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span></a><a class="right carousel-control"
                        href="#carousel-example-generic" data-slide="next"><span class="glyphicon glyphicon-chevron-right">
                        </span></a>
 </div> 
 </div>                
<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
				

@if($employ->content==Null)
<center><p>Not Availble at this time</p></center>
@else
{!!$employ->content!!}
@endif
	

			</div>
			
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div style="text-align: center;">
				<a href="{{url('contacts/create',[$employ->post_id,$employ->name])}}" class="fsc-job-apply-btn">Contact Us</a>
			</div>
<br>
		</div>
	</div>
      @endforeach
{!! $employment->links('pagination') !!}
	@endif
</div>

@endsection()