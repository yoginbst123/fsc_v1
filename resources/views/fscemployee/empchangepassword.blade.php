@extends('fscemployee.layouts.app')
@section('title', 'Change Password')
@section('main-content')
@if(Auth::user()->flag >='3')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
<style>
   .card{pointer-events: none;}
   .field-icon {margin-top: 2px;}
        .passwordbox {margin-top:10px!important;}
  .passwordbox li{margin-bottom:0px!important; font-size:13px!important; width:100%;}
  .passwordbox li .fa.fa-times{color:red;}
  .error-list, .progress.progress-danger, .progress, .password-verdict{display:none!important;}
  .progress{display:none!important;}

</style>
@endif
<style>
.field-icon {margin-top: 2px;}
 .error-list, .progress.progress-danger, .progress, .password-verdict{display:none!important;}
  .progress{display:none!important;}
</style>
<div class="content-wrapper">
   <div class="page-title">
      <h1>Change Password</h1>
   </div>
       <section class="content" style="background-color: #fff;">
   <div class="row">
      <div class="col-md-12">
         <div class="card">
            @if ( session()->has('success') )
            <div class="alert alert-success alert-dismissable">{{ session()->get('success') }}</div>
            @endif
            @if ( session()->has('error') )
            <div class="alert alert-danger alert-dismissable">{{ session()->get('error') }}</div>
            @endif
            @if(Auth::user()->flag >='3')
            <div class="alert alert-danger">Change Password After 30 Minutes</div>
            @endif
            <div class="card-body">
               <form enctype='multipart/form-data' class="form-horizontal changepassword" 
action="{{route('empchangepassword.update', Auth::user()->id)}}" id="changepassword"  method="post">
                  {{csrf_field()}}  {{method_field('PATCH')}}
                  <div class="box-header">
                      <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                   <label class="control-label col-md-5">User Name :</label>
                                   <div class="col-md-7">
                                      <input type="text" class="form-control" id="" name="">	
                                      @if ($errors->has('oldpassword'))
                                      <span class="help-block">
                                      <strong>{{ $errors->first('oldpassword') }}</strong>
                                      </span>
                                      @endif
                                   </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group {{ $errors->has('oldpassword') ? ' has-error' : '' }}">
                                   <label class="control-label col-md-5">Old Password :</label>
                                   <div class="col-md-7">
                                      <input type="password" class="form-control" id="oldpassword" name="oldpassword"> <input type="hidden" class="form-control" id="flag" value="@if(empty(Auth::user()->flag)) 1 @else {{Auth::user()->flag+1}} @endif" name="flag">	
                                      @if ($errors->has('oldpassword'))
                                      <span class="help-block">
                                      <strong>{{ $errors->first('oldpassword') }}</strong>
                                      </span>
                                      @endif
                                   </div>
                                </div><input type="hidden" class="form-control" id="" value="{{ Auth::user()->email}}" name="email">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                   <label class="control-label col-md-5">New Password :</label>
                                   <div class="col-md-7">
                                     <!-- <input name="newpassword" type="password" id="newpassword" class="form-control" />
                                      <span toggle="#newpassword" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                      <div id="messages"></div>
                                        <div class="pwstrength_viewport_progress"></div> -->
                                        <input id="newpassword" type="password" name="newpassword" placeholder="" class="form-control input-md" data-placement="bottom" data-toggle="popover" data-container="body" type="button" data-html="true" onKeyUp="passwordStrength(this.value)">
                                    </div>
                                </div>
                                <div class="form-group">
                                   <label class="control-label col-md-5">Confirm Password :</label>
                                   <div class="col-md-7">
                                        <input name="cpassword" type="password" id="cpassword" class="form-control"/>
                                   </div>
                                </div>
                                <div class="form-group  {{ $errors->has('resetdays') ? ' has-error' : '' }}">
                                   <label class="control-label col-md-5">Reset Days :</label>
                                   <div class="col-md-2">
                                      <select name="resetdays" value="" id="resetdays" class="form-control">
                                         @if(empty(Auth::user()->resetdays)) 
                                         <option>---Reset Days---</option>
                                         @endif                                                                  
                                         <option value="30" @if(Auth::user()->resetdays=='30') selected @endif>30</option>
                                         <option value="90" @if(Auth::user()->resetdays=='90') selected @endif>90</option>
                                         <option value="120" @if(Auth::user()->resetdays=='120') selected @endif>120</option>
                                      </select>
                                      @if ($errors->has('resetdays'))
                                      <span class="help-block">
                                      <strong>{{ $errors->first('resetdays') }}</strong>
                                      </span>
                                      @endif
                                   </div>
                                   <label class="control-label col-md-2">Reset Date :</label>
                                     <div class="col-md-3">  
                                        <input name="resetdate" type="text" id="reset_date" value="{{Auth::user()->enddate}}" readonly class="form-control"/>
                                     </div>
                                </div>
                                
                            </div>
                            <div class="col-md-4">
                                <div id="popover-password">
                                    <ul class="list-unstyled passwordbox">
                                        <li class=""><span class="low-upper-case"><i class="fa fa-times" aria-hidden="true"></i></span>&nbsp; 1 lowercase &amp; 1 uppercase</li>
                                        <li class=""><span class="one-number"><i class="fa fa-times" aria-hidden="true"></i></span> &nbsp;1 number (0-9)</li>
                                        <li class=""><span class="one-special-char"><i class="fa fa-times" aria-hidden="true"></i></span> &nbsp;1 Special Character (!@#$%^&*).</li>
                                        <li class=""><span class="eight-character"><i class="fa fa-times" aria-hidden="true"></i></span>&nbsp; Atleast 8 Character</li>
                                        <li class=""><span class="password-match"><i class="fa fa-times" aria-hidden="true"></i></span>&nbsp; Both Passwords are Same</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        
                  </div>
                  <div class="card-footer">
												<div class="col-md-offset-3 col-md-7">
													<div class="row">
														<div class="col-md-3">
															<input class="btn_new_save btn-primary1 primary1" type="submit"  value="Save">
														</div>
														<div class="col-md-3">
															<a class="btn_new_cancel" href="{{url('fscemployee/empchangepassword')}}">Cancel</a> 
														</div>
															<div class="col-md-3">
														
														</div>
													</div>
												</div>
											</div>
               
                  <br>
               </form>
            </div>
         </div>
      </div>
   </div>
       </section>
</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js"></script>
<script>
   $.ajaxSetup({
       headers:
       {
           'X-CSRF-Token': $('input[name="_token"]').val()
       }
   });
   $(document).ready(function() {
   
   $('.changepassword').bootstrapValidator({        
           feedbackIcons: {
               valid: 'glyphicon glyphicon-ok',
               invalid: 'glyphicon glyphicon-remove',
               validating: 'glyphicon glyphicon-refresh'
           },
   		fields: {
   			newpassword: {
           	validators: {
               notEmpty: {
                   message: 'The password is required and cannot be empty'
               },
               regexp:
            	{
            
           	regexp: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8}/,
   
         //  	message: 'The password should contain Minimum 8 and Maximum 12 characters at least 1 Uppercase Alphabet, 1 Lowercase Alphabet, 1 Number and 1 Special Character:'
   	   		},
   	 
   				different: {
   					field: 'oldpassword',
   					message: 'The password cannot be the same as Current Password'
   				}
           	}
       	},
    			oldpassword: {
                   validators: {
                       notEmpty: {
                           message: 'Please Enter Your Current Password'
   					},	
                       remote: {
                           message: 'The Password is not available',
                           url: '{{ URL::to('fscemployee/checkpassword12') }}',
                           data: {
                               type: 'oldpassword'
                           },
                           type: 'POST'
                       }																		
                   }
               },
               cpassword: {
                   validators: {  
   					notEmpty: {  
   						message: 'The confirm password is required and can\'t be empty'  
   					},  
   					identical: {  
   						field: 'newpassword',  
   						message: 'The password and its confirm are not the same'  
   					},  
   					different: {  
   						field: 'oldpassword',  
   						message: 'The password can\'t be the same as Old Password'  
   					}  
   				}  
               }
               }
           }).on('success.form.bv', function(e) {
               $('.changepassword').slideDown({ opacity: "show" }, "slow") // Do something ...
                   $('.changepassword').data('bootstrapValidator').resetForm();
               // Prevent form submission
               e.preventDefault();
               // Get the form instance
               var $form = $(e.target);
   
               // Get the BootstrapValidator instance
               var bv = $form.data('bootstrapValidator');
   
               // Use Ajax to submit form data
               $.post($form.attr('action'), $form.serialize(), function(result) {
                  // console.log(result);
               }, 'json');
           });
   });
</script>
<script>
    $(document).ready(function(){
        $('#resetdays').on('change',function(){
           var reset = parseInt($('#resetdays').val()); 
           var date = new Date();
         var t = new Date(); 
		//var n = $("#resetdays").val(); 
		//alert(offset);
		t.setDate(t.getDate() + reset);
		var month = "0"+(t.getMonth()+1);
		var date = "0"+t.getDate();
		
		const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "June","July", "Aug", "Sept", "Oct", "Nov", "Dec"];
		const d = new Date();
		
		month = month.slice(-2);
		date = date.slice(-2);
		//var date = date +"/"+month+"/"+t.getFullYear();
		var date = monthNames[t.getMonth()]+"-"+date+"-"+t.getFullYear();
           $('#reset_date').val(date);
        });
    });
</script>

<Script>
  
$(document).ready(function() {
  

 $('#newpassword').keyup(function() {
            var password = $('#newpassword').val();
            if (checkStrength(password) == false) {
              //  $('#sign-up').attr('disabled', true);
            }
        });
        
        function checkStrength(password) {
            var strength = 0;


            //If password contains both lower and uppercase characters, increase strength value.
            if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) {
                strength += 1;
                $('.low-upper-case').addClass('text-success');
                $('.low-upper-case i').removeClass('fa-times').addClass('fa-check');
                $('#popover-password-top').addClass('hide');


            } else {
                $('.low-upper-case').removeClass('text-success');
                $('.low-upper-case i').addClass('fa-times').removeClass('fa-check');
                $('#popover-password-top').removeClass('hide');
            }

            //If it has numbers and characters, increase strength value.
            if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/)) {
                strength += 1;
                $('.one-number').addClass('text-success');
                $('.one-number i').removeClass('fa-times').addClass('fa-check');
                $('#popover-password-top').addClass('hide');

            } else {
                $('.one-number').removeClass('text-success');
                $('.one-number i').addClass('fa-times').removeClass('fa-check');
                $('#popover-password-top').removeClass('hide');
            }

            //If it has one special character, increase strength value.
            if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/)) {
                strength += 1;
                $('.one-special-char').addClass('text-success');
                $('.one-special-char i').removeClass('fa-times').addClass('fa-check');
                $('#popover-password-top').addClass('hide');

            } else {
                $('.one-special-char').removeClass('text-success');
                $('.one-special-char i').addClass('fa-times').removeClass('fa-check');
                $('#popover-password-top').removeClass('hide');
            }

            if (password.length > 7) {
                strength += 1;
                $('.eight-character').addClass('text-success');
                $('.eight-character i').removeClass('fa-times').addClass('fa-check');
                $('#popover-password-top').addClass('hide');

            } else {
                $('.eight-character').removeClass('text-success');
                $('.eight-character i').addClass('fa-times').removeClass('fa-check');
                $('#popover-password-top').removeClass('hide');
            }
            
            $("#cpassword").keyup(function()   { 
                if ($('#newpassword').val() == $('#cpassword').val()) {
                    strength += 1;
                    $('.password-match').addClass('text-success');
                    $('.password-match i').removeClass('fa-times').addClass('fa-check');
                    $('#popover-password-top').addClass('hide');
    
                } else {
                    $('.password-match').removeClass('text-success');
                    $('.password-match i').addClass('fa-times').removeClass('fa-check');
                    $('#popover-password-top').removeClass('hide');
                }
            });



            // If value is less than 2

            if (strength < 2) {
                $('#result').removeClass()
                $('#password-strength').addClass('progress-bar-danger');

                $('#result').addClass('text-danger').text('Very Week');
                $('#password-strength').css('width', '10%');
            } else if (strength == 2) {
                $('#result').addClass('good');
                $('#password-strength').removeClass('progress-bar-danger');
                $('#password-strength').addClass('progress-bar-warning');
                $('#result').addClass('text-warning').text('Week')
                $('#password-strength').css('width', '60%');
                return 'Week'
            } else if (strength == 4) {
                $('#result').removeClass()
                $('#result').addClass('strong');
                $('#password-strength').removeClass('progress-bar-warning');
                $('#password-strength').addClass('progress-bar-success');
                $('#result').addClass('text-success').text('Strength');
                $('#password-strength').css('width', '100%');

                return 'Strong'
            }

        }
        
 });
 </script>
@endsection()

