@extends('fscemployee.layouts.app')
@section('main-content')
<style>
.nav-tabs > li {
    width: 167px;
    margin: auto;
}
.nav-tabs>li>a {height: 46px;}
.btn-center {margin: auto;width: 250px;}
.Branch {
    width: 100%;margin: 20px 0 0 0;
    text-align: center;
    background: #428bca;border-top-left-radius: 5px;
    border-top-right-radius: 5px;
    padding: 5px;}
.Branch h1 {
    padding: 0px;
    margin: 0px 0 0 0;
    color: #fff;
    font-size: 24px;
}
label{float:right}
.nav-tabs>li>a {
    height: 44px;}
</style>
<div class="content-wrapper">
  <div class="page-title" style="height: 58px;">
      <div class="col-md-4"></div>
      <div class="col-md-4"><h1>{{$emp->firstName}} {{$emp->lastName}}</h1></div>
      <div class="col-md-4"><h1 class="pull-right">FSC Employee</h1></div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="card" style="display: inline-block;">
        <div class="card-body">
          <div class="panel with-nav-tabs panel-primary">
            <div class="panel-heading">
              <ul class="nav nav-tabs" id="myTab">
                <li class="active"><a href="#tab1primary" data-toggle="tab">Basic Information</a></li>
                <li><a data-toggle="tab" href="#tab2primary" class="hvr-shutter-in-horizontal">Contact Information</a></li>
                <li><a href="#tab3primary" data-toggle="tab" class="hvr-shutter-in-horizontal">Location Information</a></li>
                <li><a href="#tab4primary" data-toggle="tab" class="hvr-shutter-in-horizontal">Formation Information</a></li>
                <li><a href="#tab5primary" data-toggle="tab" class="hvr-shutter-in-horizontal">Special Notes</a></li>
<li><a href="#tab6primary" data-toggle="tab" class="hvr-shutter-in-horizontal">License Information</a></li>
              </ul>
            </div>
            <form method="post" action="{{route('empprofile.update',Auth::user()->id)}}" id="registrationForm" class="form-horizontal"  enctype="multipart/form-data">       
             {{csrf_field()}}{{method_field('PATCH')}}
              <div class="panel-body">
                <div class="tab-content">
                  <div class="tab-pane fade in active" id="tab1primary">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="Branch">
                          <h1>General Information</h1>
                        </div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  {{ $errors->has('employee_id') ? ' has-error' : '' }}" style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                         <label class="fsc-form-label">Employee ID : </label>
                        </div>
                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                          <div class="row">
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                             <input type="text" class="form-control fsc-input" name="employee_id" readonly id="employee_id" value="{{$emp->employee_id}}"> @if ($errors->has('employee_id'))
                        <span class="help-block">
                        <strong>{{ $errors->first('employee_id') }}</strong>
                        </span>
                        @endif
                            </div>
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                             <label class="fsc-form-label">Status : </label>
                            </div>
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                             <select name="check" id="check" class="form-control fsc-input">                          
                            <option value="0" @if($emp->check=='0') selected @endif>Inactive</option>                         
                            <option value="1" @if($emp->check=='1') selected @endif>Active</option>                                                                                 
                          </select>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  {{ $errors->has('firstName') ? ' has-error' : '' }}{{ $errors->has('middleName') ? ' has-error' : '' }}{{ $errors->has('lastName') ? ' has-error' : '' }}" style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Name : </label>
                        </div>
                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                          <div class="row">
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                              <input type="text" class="form-control fsc-input txtOnly " id="firstName" name="firstName" placeholder="First" value="{{$emp->firstName}}">@if ($errors->has('firstName'))
                        <span class="help-block">
                        <strong>{{ $errors->first('firstName') }}</strong>
                        </span>
                        @endif
                            </div>
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                              <input type="text" maxlength="1" class="form-control fsc-input txtOnly " id="middleName" name="middleName" placeholder="Middle" value="{{$emp->middleName}}">
@if ($errors->has('middleName'))
                        <span class="help-block">
                        <strong>{{ $errors->first('middleName') }}</strong>
                        </span>
                        @endif
                            </div>
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                              <input type="text" class="form-control fsc-input txtOnly " id="lastName" name="lastName" value="{{$emp->lastName}}" placeholder="Last">
@if ($errors->has('lastName'))
                        <span class="help-block">
                        <strong>{{ $errors->first('lastName') }}</strong>
                        </span>
                        @endif
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('address1') ? ' has-error' : '' }} " style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Address 1 : </label>
                        </div>
                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <input type="text" placeholder="Address 1" class="form-control fsc-input" name="address1" id="address1" value="{{$emp->address1}}">@if ($errors->has('address1'))
                        <span class="help-block">
                        <strong>{{ $errors->first('address1') }}</strong>
                        </span>
                        @endif
                        </div>
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Address 2 : </label>
                        </div>
                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <input type="text"placeholder="Address 2" class="form-control fsc-input" name="address2" id="address2" value="{{$emp->address2}}">
                        </div>
                      </div>
 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('city') ? ' has-error' : '' }}{{ $errors->has('stateId') ? ' has-error' : '' }}{{ $errors->has('zip') ? ' has-error' : '' }} " style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">City/State/Zip : </label>
                        </div>
                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <div class="row">
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                              <input type="text" class="form-control fsc-input" id="city" name="city" placeholder="City" value="{{$emp->city}}">@if ($errors->has('city'))
                        <span class="help-block">
                        <strong>{{ $errors->first('city') }}</strong>
                        </span>
                        @endif
                            </div>
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <div class="dropdown" style="margin-top: 1%;">
                                <select name="stateId" id="stateId" class="form-control fsc-input">
                                   @if(empty($emp->stateId))                                
                                      <option value=''>State</option>                                     
                                     @else <option value='{{$emp->stateId}}'>{{$emp->stateId}}</option>
                                     @endif                    
                                  <option value="ID">ID</option>
                                  <option value='AK'>AK</option>
                                  <option value='AS'>AS</option>
                                  <option value='AZ'>AZ</option>
                                  <option value='AR'>AR</option>
                                  <option value='CA'>CA</option>
                                  <option value='CO'>CO</option>
                                  <option value='CT'>CT</option>
                                  <option value='DE'>DE</option>
                                  <option value='DC'>DC</option>
                                  <option value='FM'>FM</option>
                                  <option value='FL'>FL</option>
                                  <option value='GA'>GA</option>
                                  <option value='GU'>GU</option>
                                  <option value='HI'>HI</option>
                                  <option value='ID'>ID</option>
                                  <option value='IL'>IL</option>
                                  <option value='IN'>IN</option>
                                  <option value='IA'>IA</option>
                                  <option value='KS'>KS</option>
                                  <option value='KY'>KY</option>
                                  <option value='LA'>LA</option>
                                  <option value='ME'>ME</option>
                                  <option value='MH'>MH</option>
                                  <option value='MD'>MD</option>
                                  <option value='MA'>MA</option>
                                  <option value='MI'>MI</option>
                                  <option value='MN'>MN</option>
                                  <option value='MS'>MS</option>
                                  <option value='MO'>MO</option>
                                  <option value='MT'>MT</option>
                                  <option value='NE'>NE</option>
                                  <option value='NV'>NV</option>
                                  <option value='NH'>NH</option>
                                  <option value='NJ'>NJ</option>
                                  <option value='NM'>NM</option>
                                  <option value='NY'>NY</option>
                                  <option value='NC'>NC</option>
                                  <option value='ND'>ND</option>
                                  <option value='MP'>MP</option>
                                  <option value='OH'>OH</option>
                                  <option value='OK'>OK</option>
                                  <option value='OR'>OR</option>
                                  <option value='PW'>PW</option>
                                  <option value='PA'>PA</option>
                                  <option value='PR'>PR</option>
                                  <option value='RI'>RI</option>
                                  <option value='SC'>SC</option>
                                  <option value='SD'>SD</option>
                                  <option value='TN'>TN</option>
                                  <option value='TX'>TX</option>
                                  <option value='UT'>UT</option>
                                  <option value='VT'>VT</option>
                                  <option value='VI'>VI</option>
                                  <option value='VA'>VA</option>
                                  <option value='WA'>WA</option>
                                  <option value='WV'>WV</option>
                                  <option value='WI'>WI</option>
                                  <option value='WY'>WY</option>
                                </select>
@if ($errors->has('stateId'))
                        <span class="help-block">
                        <strong>{{ $errors->first('stateId') }}</strong>
                        </span>
                        @endif
                              </div>
                            </div>
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                              <input type="text" class="form-control fsc-input" id="zip" name="zip" value="{{$emp->zip}}" placeholder="Zip">@if ($errors->has('zip'))
                        <span class="help-block">
                        <strong>{{ $errors->first('zip') }}</strong>
                        </span>
                        @endif
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('countryId') ? ' has-error' : '' }}" style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Country :</label>
                        </div>
                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                          <div class="row">
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <div class="dropdown">
                                <select name="countryId" id="countryId" class="form-control fsc-input">  
                                     @if(empty($emp->countryId))                                
                                      <option value=''>Country</option>
                                      <option value='USA'>USA</option>
                                     @else <option value='{{$emp->countryId}}'>{{$emp->countryId}}</option>
                                     @endif

@if ($errors->has('countryId'))
                        <span class="help-block">
                        <strong>{{ $errors->first('countryId') }}</strong>
                        </span>
                        @endif
                                </select>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                     
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('telephoneNo1') ? ' has-error' : '' }} {{ $errors->has('telephoneNo1Type') ? ' has-error' : '' }} " style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Telephone 1 :</label>
                        </div>
                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                          <div class="row">
                            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                              <input type="text" class="form-control fsc-input" id="telephoneNo1" name="telephoneNo1"  placeholder="(000)000-0000" value="{{$emp->telephoneNo1}}"> @if ($errors->has('telephoneNo1'))
                        <span class="help-block">
                        <strong>{{ $errors->first('telephoneNo1') }}</strong>
                        </span>
                        @endif
                            </div>
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <div class="dropdown" style="margin-top: 1%;">
                                <select name="telephoneNo1Type" id="telephoneNo1Type" class="form-control fsc-input">
                                 <option value="Mobile" @if($emp->telephoneNo1Type=='Mobile') selected @endif>Work</option>
                                      <option value="Resident" @if($emp->telephoneNo1Type=='Resident') selected @endif>Resident</option>
                                      <option value="Office"  @if($emp->telephoneNo1Type=='Office') selected @endif>Cell</option>
                                      <option value="Other" @if($emp->telephoneNo1Type=='Other') selected @endif>Other</option>
                                </select>
@if ($errors->has('telephoneNo1Type'))
                        <span class="help-block">
                        <strong>{{ $errors->first('telephoneNo1Type') }}</strong>
                        </span>
                        @endif
                              </div>
                            </div>
                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                              <input type="text" class="form-control fsc-input" readonly id="ext1" maxlength="3" name="ext1" value="{{$emp->ext1}}" placeholder="Ext">
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('telephoneNo2') ? ' has-error' : '' }} {{ $errors->has('telephoneNo2Type') ? ' has-error' : '' }} " style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Telephone 2 :</label>
                        </div>
                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                          <div class="row">
                            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                              <input type="text" class="form-control fsc-input" id="telephoneNo2" name="telephoneNo2"  placeholder="(000)000-0000"  value="{{$emp->telephoneNo2}}">
@if ($errors->has('telephoneNo2'))
                        <span class="help-block">
                        <strong>{{ $errors->first('telephoneNo2') }}</strong>
                        </span>
                        @endif
                            </div>
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <div class="dropdown" style="margin-top: 1%;">
                                <select name="telephoneNo2Type" id="telephoneNo2Type" class="form-control fsc-input">
                                
                                 <option value="Mobile" @if($emp->telephoneNo2Type=='Mobile') selected @endif>Work</option>
                                      <option value="Resident" @if($emp->telephoneNo2Type=='Resident') selected @endif>Resident</option>
                                      <option value="Office"  @if($emp->telephoneNo2Type=='Office') selected @endif>Cell</option>
                                      <option value="Other" @if($emp->telephoneNo2Type=='Other') selected @endif>Other</option>
                                </select>
@if ($errors->has('telephoneNo2Type'))
                        <span class="help-block">
                        <strong>{{ $errors->first('telephoneNo2Type') }}</strong>
                        </span>
                        @endif
                              </div>
                            </div>
                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                              <input type="text" class="form-control fsc-input" maxlength="3" readonly id="ext2" name="ext2" value="{{$emp->ext2}}" placeholder="Ext">
                            </div>
                          </div>
                        </div>
                      </div>
 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('fax') ? ' has-error' : '' }}" style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Fax:</label>
                        </div>
                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                          <div class="row">
                            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <div class="dropdown">
                               <input type="tel" class="form-control fsc-input" id="fax" name="fax" value="{{$emp->fax}}" placeholder="Fax">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Email : </label>
                        </div>
                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <input type="text" class="form-control fsc-input" id="email" readonly name="email" value="{{$emp->email}}" placeholder="Email">
                        </div>
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('photo') ? ' has-error' : '' }}" style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Select Photo : </label>
                        </div>
<div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                          <div class="row">
                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <input name="photo" placeholder="Upload Service Image" class="form-control fsc-input" id="photo" type="file">
<input name="photo1" placeholder="Upload Service Image" value="{{$emp->photo}}"class="form-control fsc-input" id="photo1" type="hidden"> <br><img style="width: 100px;height: 50px;" src="{{asset('public/employeeimage')}}/{{$emp->photo}}" alt=""  style="width:69px;display:none" id="blah">
@if ($errors->has('photo'))
                        <span class="help-block">
                        <strong>{{ $errors->first('photo') }}</strong>
                        </span>
                        @endif
                        </div>
                      </div>
                 </div>
                      </div>
                    </div>
                  </div>
                  <!---<div class="tab-pane fade" id="tab2primary">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div class="Branch">
                          <h1>Hiring Information</h1>
                        </div>
                      <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Hire Date : </label>
                        </div>
                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <div class="row">
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col"> 
                              <div class="dropdown" style="margin-top: 1%;">
<input name="hiremonth" type="text" value="{{$emp->hiremonth}}" id="hiremonth" class="form-control" readonly="readonly">
                                </select>
                              </div>
                            </div>
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <label class="fsc-form-label">Termination Date : </label>
                            </div>
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <input name="termimonth" type="text" value="{{$emp->termimonth}}" id="termimonth" class="form-control" readonly="readonly">
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12    " style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Note :</label>
                        </div>
                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <textarea name="tnote" id="tnote"  class="form-control fsc-input">{{$emp->tnote}}</textarea>
                            </div>
                            
                          </div>
                        </div>
                      </div>
                      
                      <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12    " style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Re-Hire Date :</label>
                        </div>
                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <div class="row">
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <div class="dropdown" style="margin-top: 1%;">
<input name="rehiremonth" type="text" value="{{$emp->rehiremonth}}" id="rehiremonth" class="form-control" readonly="readonly">
                                
                              </div>
                            </div>
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <div class="dropdown" style="margin-top: 1%;">
<label class="fsc-form-label">Termination Date :</label>
                               
                              </div>
                            </div>
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <div class="dropdown" style="margin-top: 1%;">
<input name="rehireyear" type="text" value="{{$emp->rehireyear}}" id="rehireyear" class="form-control" readonly="readonly">
                                
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
<div class="col-lg-10 col-md-12 col-sm-12 col-xs-12    " style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Note :</label>
                        </div>
                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <textarea name="tnote" id="tnote"  class="form-control fsc-input">{{$emp->tnote}}</textarea>
                            </div>
                            
                          </div>
                        </div>
                      </div>
                     
                    </div>
                    <div class="">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="Branch">
                          <h1>Branch / Department Information</h1>
                        </div>
                        <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12    " style="margin-top:2%;">
                          <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">Branch City:</label>
                          </div>
                          <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <div class="row">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                <div class="dropdown" style="margin-top: 1%;">
                                  <select name="branch_city" id="branch_city" class="form-control fsc-input category">
                                     @foreach($branch as $pos)  
                                     <option value="{{$pos->city}}" @if($emp->branch_city ==$pos->city) selected @endif>{{$pos->city}}</option>
                                    @endforeach
                                  </select>
                                </div>
                              </div>
                            </div>
                          </div>
</div>

 <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                          <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Branch Name:</label>
                          </div>
                          <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <div class="row">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                               <input type="text" readonly class="form-control fsc-input" id="branch_name" name="branch_name" placeholder="" value="{{$emp->branch_name}}">
                              </div>
                            </div>
                          </div>
                        </div>                        





                        
                        <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12    " style="margin-top:2%;">
                          <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">Position :</label>
                          </div>
                          <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <div class="row">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                <div class="dropdown" style="margin-top: 1%;">
                                  <select name="position" id="position" class="form-control fsc-input">
                                    <option value="" >---Select Position---</option>
                                  @foreach($position as $pos)  
                                  <option value="{{$pos->id}}" @if($emp->position ==$pos->id) selected @endif>{{$pos->position}}</option>
                                    @endforeach
                                  </select>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
<div class="col-lg-10 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                          <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Note :</label>
                          </div>
                          <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <div class="row">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                               <textarea name="note" id="note" class="form-control fsc-input">{{$emp->note}}</textarea>
                              </div>
                            </div>
                          </div>
                        </div>  
                        
                      </div>
                    </div>
                    <div class="">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="Branch">
                          <h1>Review Information</h1>
                        </div>

<div class="review">
@foreach($review1 as $re)

<div class="col-lg-10 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">First Review Days :</label>
                        </div>
                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <div class="row">
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <div class="dropdown" style="margin-top: 1%;">
<input type="text" class="form-control fsc-input" name="first_rev_day[]" id="first_rev_day" value="{{$re->first_rev_day}}">
                                
                              </div>
                            </div>
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <div class="dropdown" style="margin-top: 1%;">
<label class="fsc-form-label">First Review Date :</label>
                               
                              </div>
                            </div>
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <div class="dropdown" style="margin-top: 1%;">
<input name="reviewmonth[]" type="text" value="{{$re->reviewmonth}}" id="reviewmonth{{$re->id}}" class="form-control" readonly="readonly">
                                <input name="ree[]" type="hidden" value="{{$re->id}}" id="ree" class="form-control">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>  

<script>
$(document).ready(function() {
  var dateInput = $('input[name="reviewmonth"]'); // Our date input has the name "date"
  var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : 'body';
  dateInput.datepicker({
    format: 'M-dd-yyyy',
    container: container,
    todayHighlight: true,
    autoclose: true,
    startDate: truncateDate(new Date()) // <-- THIS WORKS
  });

  $('#reviewmonth{{$re->id}}').datepicker('setStartDate', truncateDate(new Date()));
 // <-- SO DOES THIS
});


 function truncateDate(date) {
  return new Date(date.getFullYear(), date.getMonth(), date.getDate());
}
</script>
                      
                        <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                          <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">Comments :</label>
                          </div>
                          <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <div class="row">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                <textarea name="hiring_comments[]" id="hiring_comments" class="form-control fsc-input">{{$re->hiring_comments}}</textarea>
                              </div>
                            </div>
</div>
                            </div>
                     

@endforeach


@if(empty($review->id))
<div class="col-lg-10 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">First Review Days :</label>
                        </div>
                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <div class="row">
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <div class="dropdown" style="margin-top: 1%;">
<input type="text" class="form-control fsc-input" name="first_rev_day[]" id="first_rev_day" value="">
                                
                              </div>
                            </div>
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <div class="dropdown" style="margin-top: 1%;">
<label class="fsc-form-label">First Review Date :</label>
                               
                              </div>
                            </div>
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <div class="dropdown" style="margin-top: 1%;">
<input name="reviewmonth[]" type="text" value="" id="reviewmonth" class="form-control date_Picker" readonly="readonly">
                                
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>                        
                        <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                          <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">Comments :</label>
                          </div>
                          <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <div class="row">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                <textarea name="hiring_comments[]" id="hiring_comments" rows="1" class="form-control fsc-input"></textarea>
                              </div>
                            </div>
                          </div>
<script>
$(document).ready(function() {
  var dateInput = $('input[name="reviewmonth"]'); // Our date input has the name "date"
  var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : 'body';
  dateInput.datepicker({
    format: 'M-dd-yyyy',
    container: container,
    todayHighlight: true,
    autoclose: true,
    startDate: truncateDate(new Date()) // <-- THIS WORKS
  });

  $('#reviewmonth').datepicker('setStartDate', truncateDate(new Date()));
 // <-- SO DOES THIS
});


 function truncateDate(date) {
  return new Date(date.getFullYear(), date.getMonth(), date.getDate());
}
</script>
    </div>                  
@endif

<div class="col-md-9">
<a href="javascript:void(0)" class="ad pull-right btn btn-primary" style="margin:10px 0"><i class="fa fa-plus" aria-hidden="true"></i> Add Review</a>    
  </div>
                        
                      </div>  </div>
                      </div>
					  </div>-->
                    
                  <div class="tab-pane fade" id="tab3primary">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div class="Branch">
                          <h1>Salary Information</h1>
                        </div>
                      <div class="col-md-10 col-sm-8 col-xs-12" style="margin-top:2%;"> 
                        
                        <div class="form-group">
                          <label class="control-label col-md-4">Pay Method :</label>
                          <div class="col-md-8">
                            <select name="pay_method" id="pay_method" class="form-control">
                              <option value="Salary" @if($emp->pay_method=='Salary') selected @endif>Salary</option>
                              <option value="Hourly" @if($emp->pay_method=='Hourly') selected @endif>Hourly</option>
                            </select>
                          </div>
                        </div>
 <div class="form-group">
                          <label class="control-label col-md-4">Pay Duration :</label>
                          <div class="col-md-8">
                            <select name="pay_frequency" id="pay_frequency" class="form-control">
                              <option value="Weekly" @if($emp->pay_frequency=='Weekly') selected @endif>Weekly</option>
                              <option value="Bi-Weekly" @if($emp->pay_frequency=='Bi-Weekly') selected @endif>Bi-Weekly</option>
                              <option value="Bi-Monthly" @if($emp->pay_frequency=='Semi-Monthly') selected @endif>Semi-Monthly</option>
                              <option value="Monthly" @if($emp->pay_frequency=='Monthly') selected @endif>Monthly</option>
                            </select>
                          </div>
                        </div>
 <div class="fieldGroup">
 @foreach($info as $in)
 <div id="field{{$in->id}}">
			<div class="form-group">                        
                          <label class="control-label col-md-4">Pay Rate  : </label>                       
                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <div class="row">
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col"> 
                              <div class="dropdown" style="margin-top: 1%;">
<input name="pay_scale[]" value="{{$in->pay_scale}}" type="text" id="pay_scale" class="form-control" />   
<input name="employee[]" value="{{$in->id}}" type="hidden" id="employee" class="form-control" />                               
                              </div>
                            </div>
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <label class="fsc-form-label">Effective Date : </label>
                            </div>
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <input name="effective_date[]" value="{{$in->effective_date}}" readonly type="text" id="effective_date{{$in->id}}" class="form-control" />
                            </div>
                          </div>
                        </div>
                      </div>
				
<script>
$(document).ready(function() {
  var dateInput = $('input[name="effective_date"]'); // Our date input has the name "date"
  var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : 'body';
  dateInput.datepicker({
    format: 'M-dd-yyyy',
    container: container,
    todayHighlight: true,
    autoclose: true,
    startDate: truncateDate(new Date()) // <-- THIS WORKS
  });

  $('#effective_date{{$in->id}}').datepicker('setStartDate', truncateDate(new Date()));
 // <-- SO DOES THIS
});


 function truncateDate(date) {
  return new Date(date.getFullYear(), date.getMonth(), date.getDate());
}
</script>
                        <div class="form-group">
                          <label class="control-label col-md-4">Note :</label>
                          <div class="col-md-8">
<textarea id="fields" name="fields[]" class="form-control fsc-input">{{$in->fields}}</textarea>                          
                          </div>
                        </div>

                      </div>					  
					  @endforeach
@if(empty($info1->id))
	 <div id="Pay Method">
			<div class="form-group">                        
                          <label class="control-label col-md-4">Pay Rate  : </label>                       
                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <div class="row">
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col"> 
                              <div class="dropdown" style="margin-top: 1%;">
<input name="pay_scale[]" value="" type="text" id="pay_scale" class="form-control" />  
                             <input name="employee[]" value="0" type="hidden" id="employee" class="form-control" />
                              </div>
                            </div>
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <label class="fsc-form-label">Effective Date : </label>
                            </div>
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <input name="effective_date[]" value="" type="text" id="effective_date" class="form-control date_Picker" />
                            </div>
                          </div>
                        </div>
                      </div>
				
                        <div class="form-group">
                          <label class="control-label col-md-4">Note :</label>
                          <div class="col-md-8">
<textarea id="fields" name="fields[]" class="form-control fsc-input"></textarea>                          
                          </div>
                        </div>

                      </div>
@endif	
</div>
                      </div>
<br>
<div class="form-group">
  <div class="col-md-10">
   
<a href="javascript:void(0)" class="addMore pull-right"><i class="fa fa-plus" aria-hidden="true"></i> Add Pay</a>
  </div>
</div>
                      
                    </div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div class="Branch">
                          <h1>Taxes Information</h1>
                        </div>
                      <div class="col-md-10 col-sm-8 col-xs-12" style="margin-top:2%;"> 
                        
                        <div class="form-group">
                          <label class="control-label col-md-4">Filling Status :</label>
                          <div class="col-md-8">
                            <select name="filling_status" id="filling_status" class="form-control">
                              <option value="Single" @if($emp->filling_status=='Single') selected @endif>Single</option>
                              <option value="MFJ" @if($emp->filling_status=='MFJ') selected @endif>MFJ</option>
                              <option value="MFJ" @if($emp->filling_status=='MFS') selected @endif>MFS</option>
                              <option value="MFJ" @if($emp->filling_status=='HoH') selected @endif>HoH</option>
                            </select>
                          </div>
                        </div>
			<div class="form-group" >                        
                          <label class="control-label col-md-4">Fedral Claim : </label>                       
                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <div class="row">
                            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col"> 
                              <div class="dropdown" style="margin-top: 1%;">
<input name="fedral_claim" value="{{$emp->fedral_claim}}" type="text" id="fedral_claim" onkeypress="return isNumberKey(event)" class="form-control" />                                  
                              </div>
                            </div>
                            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <label class="fsc-form-label">Additional Withholding : </label>
                            </div>
                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <input name="additional_withholding" onkeypress="return isNumberKey(event)" value="{{$emp->additional_withholding}}" type="text" id="additional_withholding" class="form-control" />
                            </div>
								<div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <input name="additional_attach" value="{{$emp->additional_attach}}" type="file" id="additional_attach" class="form-control" />
							  <input name="additional_attach1" value="{{$emp->additional_attach}}" type="hidden" id="additional_attach1" class="form-control" />
                            </div>
                          </div>
                        </div>
					
                      </div>
<div class="form-group">
                        
                          <label class="control-label col-md-4">State Claim  : </label>
                       
                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <div class="row">
                            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col"> 
                              <div class="dropdown" style="margin-top: 1%;">
<input name="state_claim" value="{{$emp->state_claim}}" type="text" id="state_claim" onkeypress="return isNumberKey(event)" class="form-control" />  
                                
                              </div>
                            </div>
                            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <label class="fsc-form-label">Additional Withholding : </label>
                            </div>
                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <input name="additional_withholding_1" onkeypress="return isNumberKey(event)" value="{{$emp->additional_withholding_1}}" type="text" id="additional_withholding_1" class="form-control" />
                            </div>
							<div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <input name="additional_attach_1" value="{{$emp->additional_attach_1}}" type="file" id="additional_attach_1" class="form-control" />
							  <input name="additional_attach2" value="{{$emp->additional_attach_1}}" type="hidden" id="additional_attach2" class="form-control" />
                            </div>
                          </div>
                        </div>
                      </div>
			<div class="form-group" >
                        
                          <label class="control-label col-md-4">Local Claim  : </label>
                       
                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <div class="row">
                            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col"> 
                              <div class="dropdown" style="margin-top: 1%;">
<input name="local_claim" value="{{$emp->local_claim}}" onkeypress="return isNumberKey(event)" type="text" id="local_claim" class="form-control" />  
                                
                              </div>
                            </div>
                            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <label class="fsc-form-label">Additional Withholding : </label>
                            </div>
                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <input name="additional_withholding_2" onkeypress="return isNumberKey(event)" value="{{$emp->additional_withholding_2}}" type="text" id="additional_withholding_2" class="form-control" />
                            </div>
							<div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <input name="additional_attach_2" value="{{$emp->additional_attach_2}}" type="file" id="additional_attach_2" class="form-control" />
							  <input name="additional_attach3" value="{{$emp->additional_attach_2}}" type="hidden" id="additional_attach3" class="form-control" />
                            </div>
                          </div>
                        </div>
                      </div>		
                      </div>
                    </div>	
                  </div>
                  <div class="tab-pane fade" id="tab4primary">
                    <div class="row">
<div class="Branch">
                          <h1>Personal Information</h1>
                        </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12    " style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Gender :</label>
                        </div>
                        <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <div class="row">
                   
 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <div class="dropdown" style="margin-top: 1%;">
                                <select name="gender" id="gender" class="form-control fsc-input">
                                  <option value="Male" @if($emp->gender=='Male') selected @endif>Male</option>
                                  <option value="Female" @if($emp->gender=='Female') selected @endif>Female</option>
                                </select>
                              </div>
 </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Marital Status :</label>
                        </div>
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <div class="row">
                            <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <div class="dropdown" style="margin-top: 1%;">
                                <select name="marital" id="marital" class="form-control fsc-input">
                                  <option value="Married" @if($emp->marital=='Married') selected @endif>Married</option>
                                  <option value="UnMarried" @if($emp->marital=='UnMarried') selected @endif>UnMarried</option>
                                </select>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Date Of Birth : </label>
                        </div>
                        <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <div class="dropdown" style="margin-top: 1%;">

<input type="text" class="form-control fsc-input" name="month" readonly id="month" value="{{$emp->month}}">
                               
                              </div>
                            </div>
                           
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Id Proof 1 :</label>
                        </div>
                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <div class="row">
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <div class="dropdown" style="margin-top: 1%;">
                                <select name="pf1" class="form-control fsc-input" id="pf1">
                                  <option value="">Select Proof</option>
                                  <option value="Voter Id" @if($emp->pf1=='Voter Id') selected @endif>Voter Id</option>
                                  <option value="Driving Licence" @if($emp->pf1=='Driving Licence') selected @endif>Driving Licence</option>
                                  <option value="Pan Card" @if($emp->pf1=='DPan Card') selected @endif>Pan Card</option>
                                  <option value="Pass Port" @if($emp->pf1=='Pass Port') selected @endif>Pass Port</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-form-col {{ $errors->has('pfid1') ? ' has-error' : '' }}">
                              <div class="dropdown" style="margin-top: 1%;">
                                <input name="pfid1" placeholder="Upload Service Image" class="form-control fsc-input" id="pfid1" type="file">

@if ($errors->has('pfid1'))
                        <span class="help-block">
                        <strong>{{ $errors->first('pfid1') }}</strong>
                        </span>
                        @endif


 <input name="pfid12" placeholder="Upload Service Image" class="form-control fsc-input" value="{{$emp->pfid1}}" id="pfid12" type="hidden">
 <input name="pfid22" placeholder="Upload Service Image" class="form-control fsc-input" value="{{$emp->pfid2}}" id="pfid22" type="hidden">
<img src="{{asset('public/employeeProof1')}}/{{$emp->pfid1}}" alt="" id="blah-1" src="#" alt="your image" style="margin-top:10px;width:69px;display:none">
                              </div>
                            </div>
                          </div>

                        </div>
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12    " style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Id Proof 2 :</label>
                        </div>
                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <div class="row">
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <div class="dropdown" style="margin-top: 1%;">
                                <select name="pf2" class="form-control fsc-input" id="pf2">
                                  <option value="">Select Proof</option>
                                 <option value="Voter Id" @if($emp->pf2=='Voter Id') selected @endif>Voter Id</option>
                                  <option value="Driving Licence" @if($emp->pf2=='Driving Licence') selected @endif>Driving Licence</option>
                                  <option value="Pan Card" @if($emp->pf2=='DPan Card') selected @endif>Pan Card</option>
                                  <option value="Pass Port" @if($emp->pf2=='Pass Port') selected @endif>Pass Port</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-form-col {{ $errors->has('pfid2') ? ' has-error' : '' }}">
                              <div class="dropdown" style="margin-top: 1%;">
                                <input name="pfid2" placeholder="Upload Service Image" class="form-control fsc-input" id="pfid2" type="file">
<img src="{{asset('public/employeeProof2')}}/{{$emp->pfid2}}" alt="" id="blah-2" src="#" alt="your image" style="margin-top:10px;width:69px;display:none"> @if ($errors->has('pfid2'))
                        <span class="help-block">
                        <strong>{{ $errors->first('pfid2') }}</strong>
                        </span>
                        @endif

                              </div>
                            </div>
                          </div>

                        </div>
                      </div>


<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12    " style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Resume :</label>
                        </div>
                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <div class="row">
                            
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-form-col {{ $errors->has('resume') ? ' has-error' : '' }}">
                              <div class="dropdown" style="margin-top: 1%;">
                                <input name="resume" placeholder="Upload Service Image" class="form-control fsc-input" id="resume" type="file">
 <input name="resume_1" placeholder="Upload Service Image" value="{{$emp->resume}}" class="form-control fsc-input" id="resume_1" type="hidden">
@if ($errors->has('resume'))
                        <span class="help-block">
                        <strong>{{ $errors->first('resume') }}</strong>
                        </span>
                        @endif
                              </div>
                            </div>
                          </div>

                        </div>
                      </div>
 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Type of Agreement :</label>
                        </div>
                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <div class="row">
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <div class="dropdown" style="margin-top: 1%;">
                                <select name="type_agreement" class="form-control fsc-input" id="type_agreement">
                                  <option value="">Type Of agreement</option>
                                 <option value="Voter Id" @if($emp->pf2=='Voter Id') selected @endif>Voter Id</option>
                                  <option value="Driving Licence" @if($emp->pf2=='Driving Licence') selected @endif>Driving Licence</option>
                                  <option value="Pan Card" @if($emp->pf2=='DPan Card') selected @endif>Pan Card</option>
                                  <option value="Pass Port" @if($emp->pf2=='Pass Port') selected @endif>Pass Port</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-form-col {{ $errors->has('agreement') ? ' has-error' : '' }}">
                              <div class="dropdown" style="margin-top: 1%;">
                                <input name="agreement" placeholder="Upload Service Image" class="form-control fsc-input" id="agreement" type="file">
<input name="agreement_1" value="{{$emp->agreement}}" placeholder="Upload Service Image" class="form-control fsc-input" id="agreement_1" type="hidden">
<img src="{{asset('public/agreement')}}/{{$emp->agreement}}" alt="" id="blah-3" src="#" alt="your image" style="margin-top:10px;width:69px;display:none">
@if ($errors->has('agreement'))
                        <span class="help-block">
                        <strong>{{ $errors->first('agreement') }}</strong>
                        </span>
                        @endif
                              </div>
                            </div>
                          </div>

                        </div>
                      </div>
                      <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <div class="Branch">
                            <h1>Emergency Contact Info</h1>
                          </div>
                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  " style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Contact Person Name : </label>
                        </div>
                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                          <div class="row">
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                              <input type="text" class="form-control fsc-input" id="firstName_1" name="firstName_1" placeholder="First" value="{{$emp->firstName_1}}">                            </div>
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                              <input type="text" maxlength="1" class="form-control fsc-input" id="middleName_1" name="middleName_1" placeholder="Middle" value="{{$emp->middleName_1}}">
                            </div>
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                              <input type="text" class="form-control fsc-input" id="lastName_1" name="lastName_1" value="{{$emp->lastName_1}}" placeholder="Last">
                            </div>
                          </div>
                        </div>
                      </div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  " style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Address 1 : </label>
                        </div>
                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <input type="text" placeholder="Address 1" class="form-control fsc-input" name="address11" id="address11" value="{{$emp->address11}}">                        </div>
                      </div>
                          
                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                              <label class="fsc-form-label">Address 2:</label>
                            </div>
                            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                              <input type="text" class="form-control fsc-input" name="eaddress1" id="eaddress1" value="{{$emp->eaddress1}}"><input type="hidden" class="form-control fsc-input" name="status" id="status" value="{{$emp->status}}">
                            </div>
                          </div>
                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12    " style="margin-top:2%;">
                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                              <label class="fsc-form-label">City/State/Zip : </label>
                            </div>
                            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                              <div class="row">
                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                  <input type="text" class="form-control fsc-input" id="ecity" name="ecity" placeholder="City" value="{{$emp->ecity}}">
                                </div>
                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                  <div class="dropdown" style="margin-top: 1%;">
                                    <select name="estate" id="estate" class="form-control fsc-input">
                                      <option value="ID">Select</option>
                                      <option value="ID">ID</option>
                                      <option value="AK">AK</option>
                                      <option value="AS">AS</option>
                                      <option value="AZ">AZ</option>
                                      <option value="AR">AR</option>
                                      <option value="CA">CA</option>
                                      <option value="CO">CO</option>
                                      <option value="CT">CT</option>
                                      <option value="DE">DE</option>
                                      <option value="DC">DC</option>
                                      <option value="FM">FM</option>
                                      <option value="FL">FL</option>
                                      <option value="GA">GA</option>
                                      <option value="GU">GU</option>
                                      <option value="HI">HI</option>
                                      <option value="ID">ID</option>
                                      <option value="IL">IL</option>
                                      <option value="IN">IN</option>
                                      <option value="IA">IA</option>
                                      <option value="KS">KS</option>
                                      <option value="KY">KY</option>
                                      <option value="LA">LA</option>
                                      <option value="ME">ME</option>
                                      <option value="MH">MH</option>
                                      <option value="MD">MD</option>
                                      <option value="MA">MA</option>
                                      <option value="MI">MI</option>
                                      <option value="MN">MN</option>
                                      <option value="MS">MS</option>
                                      <option value="MO">MO</option>
                                      <option value="MT">MT</option>
                                      <option value="NE">NE</option>
                                      <option value="NV">NV</option>
                                      <option value="NH">NH</option>
                                      <option value="NJ">NJ</option>
                                      <option value="NM">NM</option>
                                      <option value="NY">NY</option>
                                      <option value="NC">NC</option>
                                      <option value="ND">ND</option>
                                      <option value="MP">MP</option>
                                      <option value="OH">OH</option>
                                      <option value="OK">OK</option>
                                      <option value="OR">OR</option>
                                      <option value="PW">PW</option>
                                      <option value="PA">PA</option>
                                      <option value="PR">PR</option>
                                      <option value="RI">RI</option>
                                      <option value="SC">SC</option>
                                      <option value="SD">SD</option>
                                      <option value="TN">TN</option>
                                      <option value="TX">TX</option>
                                      <option value="UT">UT</option>
                                      <option value="VT">VT</option>
                                      <option value="VI">VI</option>
                                      <option value="VA">VA</option>
                                      <option value="WA">WA</option>
                                      <option value="WV">WV</option>
                                      <option value="WI">WI</option>
                                      <option value="WY">WY</option>
                                    </select>
                                  </div>
                                </div>
                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                  <input type="text" class="form-control fsc-input" id="ezipcode" name="ezipcode" value="{{$emp->ezipcode}}" placeholder="Zip">
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  " style="margin-top:2%;">
                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                              <label class="fsc-form-label">Telephone 1 :</label>
                            </div>
                            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <div class="row">
                                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                  <input type="text" class="form-control fsc-input" id="etelephone1" name="etelephone1" placeholder="(000)000-0000" value="{{$emp->etelephone1}}">
                                </div>
                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                  <div class="dropdown" style="margin-top: 1%;">
                                    <select name="eteletype1" id="eteletype1" class="form-control fsc-input">
                                     <option value="Mobile" @if($emp->eteletype1=='Mobile') selected @endif>Mobile</option>
                                      <option value="Resident" @if($emp->eteletype1=='Resident') selected @endif>Resident</option>
                                      <option value="Office"  @if($emp->eteletype1=='Office') selected @endif>Office</option>
                                      <option value="Other" @if($emp->eteletype1=='Other') selected @endif>Other</option>
                                    </select>
                                  </div>
                                </div>
                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                  <input type="text" class="form-control fsc-input" maxlength="3" value="{{$emp->eext1}}" readonly id="eext1" name="eext1"  placeholder="Ext">
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  " style="margin-top:2%;">
                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                              <label class="fsc-form-label">Telephone 2 :</label>
                            </div>
                            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <div class="row">
                                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                  <input type="text" class="form-control fsc-input" id="etelephone2" name="etelephone2" placeholder="(000) 000 0000" value="{{$emp->etelephone2}}">
                                </div>

                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                  <div class="dropdown" style="margin-top: 1%;">
                                    <select name="eteletype2" id="eteletype2" class="form-control fsc-input">
                                      <option value="Mobile" @if($emp->eteletype1=='Mobile') selected @endif>Mobile</option>
                                      <option value="Resident" @if($emp->eteletype1=='Resident') selected @endif>Resident</option>
                                      <option value="Office"  @if($emp->eteletype1=='Office') selected @endif>Office</option>
                                      <option value="Other" @if($emp->eteletype1=='Other') selected @endif>Other</option>
                                    </select>
                                  </div>
                                </div>
                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                  <input type="text" class="form-control fsc-input" maxlength="3" value="{{$emp->eext2}}" readonly id="eext2" name="eext2" 
 placeholder="Ext">
                                </div>
                              </div>
                            </div>
                          </div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                              <label class="fsc-form-label">Fax :</label>
                            </div>
                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
<div class="row">
                            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <input type="text" class="form-control fsc-input" name="efax" id="efax" value="{{$emp->efax}}">
                            </div>
                          </div>
</div>
                          </div>


<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                              <label class="fsc-form-label">E-mail :</label>
                            </div>
                            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                              <input type="text" class="form-control fsc-input" name="eemail" id="eemail" value="{{$emp->eemail}}">
                            </div>
                          </div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                              <label class="fsc-form-label">Relationship :</label>
                            </div>
                            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                              <input type="text" class="form-control fsc-input" name="relation" id="relation" value="{{$emp->relation}}">
                            </div>
                          </div>
                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                              <label class="fsc-form-label">Note For Emergency :</label>
                            </div>
                            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                              <textarea name="comments1" id="comments1" rows="1" class="form-control fsc-input">{{$emp->comments1}}</textarea>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="tab-pane fade" id="tab5primary">
                    <div class="row">
<div class="Branch">
                          <h1>Security Information</h1>
                        </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">User Name :</label>
                        </div>
                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <input id="uname" class="form-control fsc-input" placeholder="User Name" readonly  value="{{$emp->uname}}"  name="uname"  type="text">
                        </div>
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Password :</label>
                        </div>
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <input placeholder="Password" class="form-control fsc-input" readonly id="password" name="password1" value="{{$emp->password}}"  type="password">
                        </div>
                      </div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Reset Days : </label>
                        </div>
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <select name="reset" id="reset" class="form-control fsc-input" readonly  >
                            <option value="120">120</option>
                            
                                                        
                          </select>
                        </div>
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Question 1 : </label>
                        </div>
                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <select name="question1" id="question1" class="form-control fsc-input">
                            <option value="">Select</option>
                            <option value="What was your favorite place to visit as a child?">What was your favorite place to visit as a child?</option>
                            <option value="Who is your favorite actor, musician, or artist?">Who is your favorite actor, musician, or artist?</option>
                            <option value="What is the name of your favorite pet?">What is the name of your favorite pet?</option>
                            <option value="In what city were you born?" selected="selected">In what city were you born?</option>
                            <option value="What is the name of your first school?">What is the name of your first school?</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Answer 1 :</label>
                        </div>
                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <input name="answer1" value="{{$emp->answer1}}" placeholder="" class="form-control fsc-input" id="answer1" type="text">
                        </div>
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Question 2 :</label>
                        </div>
                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <select name="question2" id="question2" class="form-control fsc-input">
                            <option value="">Select</option>
                            <option value="What is your favorite movie?">What is your favorite movie?</option>
                            <option value="What was the make of your first car?">What was the make of your first car?</option>
                            <option value="What is your favorite color?" selected="selected">What is your favorite color?</option>
                            <option value="What is your father's middle name?">What is your fathers middle name?</option>
                            <option value="What is the name of your first grade teacher?">What is the name of your first grade teacher?</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Answer 2 :</label>
                        </div>
                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <input name="answer2" value="{{$emp->answer2}}" placeholder="" class="form-control fsc-input" id="answer2" type="text">
                        </div>
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Question 3 :</label>
                        </div>
                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <select name="question3" id="question3" class="form-control fsc-input">
                            <option value="">Select</option>
                            <option value="What was your high school mascot?">What was your high school mascot?</option>
                            <option value="Which is your favorite web browser?">Which is your favorite web browser?</option>
                            <option value="In what year was your father born?">In what year was your father born?</option>
                            <option value="What is the name of your favorite childhood friend?" selected="selected">What is the name of your favorite childhood friend?</option>
                            <option value="What was your favorite food as a child?">What was your favorite food as a child?</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Answer 3 :</label>
                        </div>
                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <input name="answer3" value="{{$emp->answer3}}" placeholder="" class="form-control fsc-input" id="answer3"  type="text">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="tab-pane fade" id="tab6primary">
                    <div class="row">
<div class="Branch">
                          <h1>other Information</h1>
                        </div>
<div class="responsibility">
@foreach($info3 as $in1)
<div id="responsibility{{$in1->id}}">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Work Responsibility : </label>
                        </div>
                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
 <input type="text" class="form-control fsc-input" name="work_responsibility[]" id="work_responsibility" value="{{$in1->work_responsibility}}">
 <input type="hidden" class="form-control fsc-input" name="work[]" id="work" value="{{$in1->id}}">                          
                        </div>
                      </div>
</div>
@endforeach
@if(empty($info2->id))

<div id="responsibility0">
 <input type="hidden" class="form-control fsc-input" name="work[]" id="work" value="0">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Work Responsibility : </label>
                        </div>
                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
 <input type="text" class="form-control fsc-input" name="work_responsibility[]" id="work_responsibility" value="">

                        
                        </div>
                      </div>
</div>
@endif
</div>
<div class="col-md-8" style="margin-top:15px">

<a href="javascript:void(0)" class="add pull-right btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> Add Field</a>
   
  </div>

                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Employee Rules : </label>
                        </div>
                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <input type="text" class="form-control fsc-input" name="employee_rules" id="employee_rules" value="{{$emp->employee_rules}}">
                        </div>
                      </div>
                      
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:3%;">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="btn-center">
                    <button type="submit" class="btn btn-primary  fsc-form-submit class="hvr-shutter-in-horizontal"">Save</button>&nbsp&nbsp&nbsp
                    <a type="submit" class="btn btn-primary  fsc-form-submit class="hvr-shutter-in-horizontal"">Cancel</a>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
<!-- copy of input fields group -->
<div class="form-group fieldGroupCopy" style="display: none;">
   <div class="">      
<input type="hidden" class="form-control fsc-input" name="employee[]" id="employee" value="">
      <div class="form-group"><label class="control-label col-md-4">Pay Rate  : </label><div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin"><div class="row"><div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col"><div class="dropdown" style="margin-top: 1%;"><input name="pay_scale[]" value="" type="text" id="pay_scale" class="form-control" /></div></div><div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col"><label class="fsc-form-label">Effective Date : </label></div><div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col"><input name="effective_date[]" value="" readonly  type="text" id="effective_date" class="form-control date_Picker" /></div></div></div></div><div class="form-group"><label class="control-label col-md-4">Note :</label><div class="col-md-8"><textarea id="fields" name="fields[]" class="form-control fsc-input"></textarea></div></div>
<div class="pull-right" style="margin-top: 10px;margin-bottom: 10px;"> 
         <a href="javascript:void(0)" class="btn btn-danger remove"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
      </div>
      </div>
<div class="clearfix"></div>
   </div>
</div>



<!-- copy of input fields group -->
<div class="form-group fieldGroupCopy-2" style="display: none;">
   <div class=""> 
<input type="hidden" class="form-control fsc-input" name="work[]" id="work" value="">     
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;"><div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row"><label class="fsc-form-label">Work Responsibility : </label></div><div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin"><input type="text" class="form-control fsc-input" name="work_responsibility[]" id="work_responsibility" value=""></div>
<div class="pull-right col-lg-5" style="margin-top: 10px;"> 
         <a href="javascript:void(0)" class="btn btn-danger remove"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
      </div>
      </div>
<div class="clearfix"></div>
   </div>
</div>
<!-- copy of input fields group -->
<div class="form-group fieldGroupCopy-3" style="display: none;">
   <div class="">      

      <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;"><div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row"><label class="fsc-form-label">First Review Days :</label></div><div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin"><div class="row"><div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col"><div class="dropdown" style="margin-top: 1%;"><input type="text" class="form-control fsc-input" name="first_rev_day[]" id="first_rev_day" value=""></div></div><div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col"><div class="dropdown" style="margin-top: 1%;"><label class="fsc-form-label">First Review Date :</label></div></div><div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col"><div class="dropdown" style="margin-top: 1%;"><input name="reviewmonth[]" readonly type="text" value="" id="reviewmonth" class="date_Picker form-control" readonly="readonly"></div></div></div></div></div><div class="col-lg-10 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;"><div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row"><label class="fsc-form-label">Comments :</label></div><div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin"><div class="row"><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-col"><textarea name="hiring_comments[]" id="hiring_comments" rows="1" class="form-control fsc-input"></textarea></div></div></div></div>

<div class="pull-right col-lg-4" style="margin-top: 10px;"> 
         <a href="javascript:void(0)" class="btn btn-danger remove"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
      </div>
      </div>
<div class="clearfix"></div>
   </div>
</div>
<script>
   $("#telephoneNo1").mask("(999) 999-9999");
   $(".ext").mask("999");
   $("#ext1").mask("999");
   $("#ext2").mask("999");
$("#eext1").mask("999");
   $("#eext2").mask("999");
   $("#telephoneNo2").mask("(999) 999-9999");
   $("#mobile_no").mask("(999) 999-9999");
   $("#fax").mask("(999) 999-9999");
   $("#etelephone2").mask("(999) 999-9999");
   $("#etelephone1").mask("(999) 999-9999");
 $("#computer_ip").mask("999.999.999.999");
   $("#zip").mask("9999");
 $("#ezipcode").mask("9999");
</script>
<script>
var date = $('#ext1').val({{$emp->ext1}});
$('#telephoneNo1Type').on('change', function() {

if(this.value=='Office')
{
document.getElementById('ext1').removeAttribute('readonly');
$('#ext1').val({{$emp->ext1}});
}
else
{
document.getElementById('ext1').readOnly =true;
$('#ext1').val('');
}
})
</script>
<script>
var dat1 = $('#ext2').val({{$emp->ext2}});
$('#telephoneNo2Type').on('change', function() {

if(this.value=='Office')
{
document.getElementById('ext2').removeAttribute('readonly');
$('#ext2').val({{$emp->ext2}}); 
}
else
{
document.getElementById('ext2').readOnly =true;
$('#ext2').val('');
}
})
</script>

<script>
$('#eteletype2').on('change', function() {

if(this.value=='Office')
{
$('#eext2').val({{$emp->eext2}});
document.getElementById('eext2').removeAttribute('readonly');
}
else
{
document.getElementById('eext2').readOnly =true;
$('#eext2').val('');
}
})
</script>
<script>
var dat2 = $('#eext1').val({{$emp->eext1}});
$('#eteletype1').on('change', function() {
if(this.value=='Office')
{
document.getElementById('eext1').removeAttribute('readonly');
$('#eext1').val({{$emp->eext1}});
}
else
{

document.getElementById('eext1').readOnly =true;
  $('#eext1').val('');
}
})
</script>
<script>
   $(document).ready(function() {
  var dateInput = $('input[name="hiremonth"]'); // Our date input has the name "date"
  var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : 'body';
  dateInput.datepicker({
    format: 'dd-M-yyyy',
    container: container,
    todayHighlight: true,
    autoclose: true,
    startDate: truncateDate(new Date()) // <-- THIS WORKS
  });

  $('#hiremonth').datepicker('setStartDate', truncateDate(new Date()));
 // <-- SO DOES THIS
});

function truncateDate(date) {
  return new Date(date.getFullYear(), date.getMonth(), date.getDate());
}


 $(document).ready(function() {
  var dateInput = $('input[name="termimonth"]'); // Our date input has the name "date"
  var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : 'body';
  dateInput.datepicker({
    format: 'dd-M-yyyy',
    container: container,
    todayHighlight: true,
    autoclose: true,
    startDate: truncateDate(new Date()) // <-- THIS WORKS
  });

  $('#termimonth').datepicker('setStartDate', truncateDate(new Date()));
 // <-- SO DOES THIS
});

function truncateDate(date) {
  return new Date(date.getFullYear(), date.getMonth(), date.getDate());
}


 $(document).ready(function() {
  var dateInput = $('input[name="rehiremonth"]'); // Our date input has the name "date"
  var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : 'body';
  dateInput.datepicker({
    format: 'dd-M-yyyy',
    container: container,
    todayHighlight: true,
    autoclose: true,
    startDate: truncateDate(new Date()) // <-- THIS WORKS
  });

  $('#rehiremonth').datepicker('setStartDate', truncateDate(new Date()));
 // <-- SO DOES THIS
});

function truncateDate(date) {
  return new Date(date.getFullYear(), date.getMonth(), date.getDate());
}
 $(document).ready(function() {
  var dateInput = $('input[name="rehireyear"]'); // Our date input has the name "date"
  var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : 'body';
  dateInput.datepicker({
    format: 'dd-M-yyyy',
    container: container,
    todayHighlight: true,
    autoclose: true,
    startDate: truncateDate(new Date()) // <-- THIS WORKS
  });

  $('#rehireyear').datepicker('setStartDate', truncateDate(new Date()));
 // <-- SO DOES THIS
});

function truncateDate(date) {
  return new Date(date.getFullYear(), date.getMonth(), date.getDate());
}

 $(document).ready(function() {
  var dateInput = $('input[name="reviewmonth"]'); // Our date input has the name "date"
  var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : 'body';
  dateInput.datepicker({
    format: 'dd-M-yyyy',
    container: container,
    todayHighlight: true,
    autoclose: true,
    startDate: truncateDate(new Date()) // <-- THIS WORKS
  });

  $('#reviewmonth').datepicker('setStartDate', truncateDate(new Date()));
 // <-- SO DOES THIS
});


 function truncateDate(date) {
  return new Date(date.getFullYear(), date.getMonth(), date.getDate());
}

 $(document).ready(function() {
  var dateInput = $('input[name="month"]'); // Our date input has the name "date"
  var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : 'body';
  dateInput.datepicker({
    format: 'dd-M-yyyy',
    container: container,
    todayHighlight: true,
    autoclose: true,
    startDate: truncateDate(new Date()) // <-- THIS WORKS
  });

  $('#month').datepicker('setStartDate', truncateDate(new Date()));
 // <-- SO DOES THIS
});


 function truncateDate(date) {
  return new Date(date.getFullYear(), date.getMonth(), date.getDate());
}
 $(document).ready(function() {
  var dateInput = $('input[name="effective_date"]'); // Our date input has the name "date"
  var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : 'body';
  dateInput.datepicker({
    format: 'dd-M-yyyy',
    container: container,
    todayHighlight: true,
    autoclose: true,
    startDate: truncateDate(new Date()) // <-- THIS WORKS
  });

  $('#effective_date').datepicker('setStartDate', truncateDate(new Date()));
 // <-- SO DOES THIS
});


 function truncateDate(date) {
  return new Date(date.getFullYear(), date.getMonth(), date.getDate());
}




</script>

<script>
   $(document).ready(function(){
       //group add limit
       var maxGroup = 120;
       
       //add more fields group
       $(".addMore").click(function(){
           if($('body').find('.fieldGroup').length < maxGroup){
               var fieldHTML = '<div class="fieldGroup">'+$(".fieldGroupCopy").html()+'</div>';
               $('body').find('.fieldGroup:last').after(fieldHTML);
           }else{
               alert('Maximum '+maxGroup+' Persons are allowed.');
           }
       });
       
       //remove fields group
       $("body").on("click",".remove",function(){ 
           $(this).parents(".fieldGroup").remove();
       });
   });
</script>
<script>
   $(document).ready(function(){
       //group add limit
       var maxGroup = 120;
       
       //add more fields group
       $(".add").click(function(){
           if($('body').find('.responsibility').length < maxGroup){
               var fieldHTML = '<div class="responsibility">'+$(".fieldGroupCopy-2").html()+'</div>';
               $('body').find('.responsibility:last').after(fieldHTML);
           }else{
               alert('Maximum '+maxGroup+' Persons are allowed.');
           }
       });
       
       //remove fields group
       $("body").on("click",".remove",function(){ 
           $(this).parents(".responsibility").remove();
       });
   });
</script>
<script>
   $(document).ready(function(){
       //group add limit
       var maxGroup = 120;
       
       //add more fields group
       $(".ad").click(function(){
           if($('body').find('.review').length < maxGroup){
               var fieldHTML = '<div class="review">'+$(".fieldGroupCopy-3").html()+'</div>';
               $('body').find('.review:last').after(fieldHTML);
           }else{
               alert('Maximum '+maxGroup+' Persons are allowed.');
           }
       });
       
       //remove fields group
       $("body").on("click",".remove",function(){ 
           $(this).parents(".review").remove();
       });
   });
</script>
<SCRIPT language=Javascript>
      <!--
      function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
      }
      //-->
   </SCRIPT>
<script type="text/javascript">
$(function() {
             
            $(document).on("click",".date_Picker",function(){        
                 
                    $(this).datepicker({                        
                            changeMonth: true,
                            changeYear: true,
                            format: 'dd-M-yyyy'                       
                        }).datepicker("show");
                });
                 
    });
 
</script>
<script>
jQuery.fn.getNum = function() {
    var val = $.trim($(this).val());
    if(val.indexOf(',') > -1) {
        val = val.replace(',', '.');
    }
    var num = parseFloat(val);
    var num = num.toFixed(2);
    if(isNaN(num)) {
        num = '';
    }
    return num;
}

$(function() {

    $('#additional_withholding,#additional_withholding_1,#additional_withholding_2').blur(function() {
        $(this).val($(this).getNum());
    });

});
</script>
<script>
 $(document).ready(function(){
   	$(document).on('change','.category', function()
   	{ 
   		//console.log('htm');
   		var id = $(this).val();
//alert(id);
   		$.get('{!!URL::to('getBranch1')!!}?id='+id, function(data)
   		{  
              $('#branch_name').empty();
              $.each(data, function(index, subcatobj)
   		   {
   			   $('#branch_name').val(subcatobj.branchname);
   		   })
   
   		});
   			
   	});
   });

</script>
<script>
$.ajaxSetup({
    headers:
    {
        'X-CSRF-Token': $('input[name="_token"]').val()
    }
});
  $(document).ready(function() {
 
$('#hiremonth').datepicker({
            format: 'dd-M-yyyy',
            assumeNearbyYear: true,
            autoclose: 'true',
            //startView: '',
            todayHighlight: true,
            calendarWeeks: true,
            daysOfWeekHighlighted: '0,6',
            weekStart: '1'
        })
        .on('changeDate', function(e) {
            // Revalidate the date field
            $('#registrationForm').formValidation('revalidateField', 'hiremonth');
        });




    $('#registrationForm').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },

		fields: {
			employee_id: {
                validators: {
                        stringLength: {
                        min: 1,
                    },
                        notEmpty: {
                        message: 'Please Enter Your Employee ID '
                    }
                }
            },
		

            firstName: {
                validators: {
                        stringLength: {
                        min: 1,
                    },
                        notEmpty: {
                        message: 'Please Enter Your First Name'
					},
					regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'The First Name can consist of alphabetical characters and spaces only'
                    }
                }
            },
			middleName: {
                validators: {
                     stringLength: {
                        min: 1,
                    },
                    notEmpty: {
                        message: 'Please Enter Your Middle Name'
                    },
					regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'The Middle name can consist of alphabetical characters and spaces only'
                    }
                }
            },
             lastName: {
                validators: {
                     stringLength: {
                        min: 1,
                    },
                    notEmpty: {
                        message: 'Please Enter Your Last Name'
					},
					
					regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'The Last name can consist of alphabetical characters and spaces only'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Your Email Address'
					},				
                    emailAddress: {
                        message: 'Please Enter Your Valid Email Address'
					},					
				
						
                }
            },
            address1: {
                validators: {
                     stringLength: {
						min: 1,
						message: 'Please Enter Your 8 Charactor'
                    },
                    notEmpty: {
                        message: 'Please Enter Your Address'
                    }
                }
            },
			address1: {
                validators: {
                     stringLength: {
						min: 1,
						message: 'Please Enter Your 8 Charactor'
                    },
                    notEmpty: {
                        message: 'Please Enter Your Address'
                    }
                }
            },
            city: {
                validators: {
                     stringLength: {
						min: 1,
						
                    },
                    notEmpty: {
                        message: 'Please Enter Your City'
                    },
					regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'The City can consist of alphabetical characters and spaces only'
                    }
                }
            },
            stateId: {
                validators: {
                    notEmpty: {
                        message: 'Please Select Your State'
                    }
                }
            },
			countryId: {
                validators: {
                    notEmpty: {
                        message: 'Please Select Your Country'
                    }
                }
            },

            zip: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Your Zip Code'
                    }					
                }
            },
			business_no: {
                validators: {
                    stringLength: {
                        min: 15,
                        message:'Please enter at least 10 characters and no more than 10'
                    },
                    notEmpty: {
                        message: 'Please Enter Your Business Phone Number'
                    },
                    business_no: {
                        country: 'USA',
                        message: 'Please supply a vaild phone number with area code'
                    }
                }
            },
			


         
            }
        })
        .on('success.form.bv', function(e) {
            $('#success_message').slideDown({ opacity: "show" }, "slow") // Do something ...
                $('#registrationForm').data('bootstrapValidator').resetForm();
            // Prevent form submission
            e.preventDefault();
            // Get the form instance
            var $form = $(e.target);

            // Get the BootstrapValidator instance
            var bv = $form.data('bootstrapValidator');

            // Use Ajax to submit form data
            $.post($form.attr('action'), $form.serialize(), function(result) {
               // console.log(result);
            }, 'json');
        });
});
</script>
<script>
$(document).ready( function() {
    	$(document).on('change', '.btn-file :file', function() {
		var input = $(this),
			label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		input.trigger('fileselect', [label]);
		});

		$('.btn-file :file').on('fileselect', function(event, label) {
		    
		    var input = $(this).parents('.input-group').find(':text'),
		        log = label;
		    
		    if( input.length ) {
		        input.val(log);
		    } else {
		        if( log ) alert(log);
		    }
	    
		});
		function readURL(input) {
		    if (input.files && input.files[0]) {
		        var reader = new FileReader();
		        
		        reader.onload = function (e) {
                            $('#blah').show(); 
		            $('#blah').attr('src', e.target.result);

 
		            
		        }
		        
		        reader.readAsDataURL(input.files[0]);
		    }
		}

		$("#photo").change(function(){
		    readURL(this);
		}); 	
	});


function readURL(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
$('#blah-2').show(); 
      $('#blah-2').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}

$("#pfid2").change(function() {
  readURL(this);
});





function readURL1(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
$('#blah-1').show(); 
      $('#blah-1').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}

$("#pfid1").change(function() {
  readURL1(this);
});



function readURL2(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
$('#blah-3').show(); 
      $('#blah-3').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}

$("#agreement").change(function() {
  readURL2(this);
});
</script>
@endsection()





