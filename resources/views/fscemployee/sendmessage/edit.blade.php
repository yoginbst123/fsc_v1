@extends('fscemployee.layouts.app')
@section('title', 'Edit Message')
@section('main-content')
<div class="content-wrapper">
   <div class="page-title">
      <h1>Message  </h1>
   </div>
    <section class="content" style="background-color: #fff;">
   <div class="row">
      <div class="col-md-12">
         <div class="card">
            <div class="card-body">
               <form method="post" action="{{route('sendmessage.update',$task->id)}}" class="form-horizontal" id="content" name="content" enctype="multipart/form-data">
                  {{csrf_field()}} {{method_field('PATCH')}}
                  <div style="pointer-events: none; background:#ccc; padding:10px 0">
  <div class="form-group">
                   <label class="control-label col-md-3">Date / Day / Time:</label>
                     <div class="col-md-2" style="width: 130px;">
                        <div class="">
                           <input type="text" name="date" id="date1" value="{{$task->date}}" class="form-control" placeholder="Date">
                        </div>
                     </div>
                      
                     <div class="col-md-2"  style="width: 130px;">
                        <div class="">
                           <input type="text" name="day" id="day" value="{{$task->day}}" class="form-control" placeholder="Day">
                        </div>
                     </div>
                     
                     <div class="col-md-2"  style="width: 115px;">
                        <div class="">
                           <input type="text" name="time" value="{{$task->time}}" id="time" class="form-control" placeholder="Time">
                        </div>
                       
                     </div>
                  </div>
                   <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                   <label class="control-label col-md-3">Massage From :</label>
                     <div class="col-md-4">
                        <div class="">
                          <select name="type" id="type" class="form-control fsc-input">
                              <option value="">---Select---</option>
                              <option value="Admin" @if($task->title=="Admin") selected @endif>Admin</option>
                              <option value="Active" @if($task->title=="Active") selected @endif>Client</option>
                              <option value="employee" @if($task->title=="employee") selected @endif>FSC-EE / User</option>
                              <option value="Other Person" @if($task->title=="Other Person") selected @endif>Other Person</option>
                           </select>
                        </div>
                        	
                     </div>
                  </div>
                  <div class="form-group{{ $errors->has('employee') ? ' has-error' : '' }}">
                     <label class="control-label col-md-3">Massage To :</label>
                     <div class="col-md-4">
                        <div class="">
                          <select name="employee" id="employee" class="form-control selectpicker fsc-input">
                             <option value="{{$employee1->id}}">{{$employee1->firstName.' '.$employee1->lastName}}</option>
                            
                           </select>
                        </div>
                        @if ($errors->has('employee'))
                        <span class="help-block">
                        <strong>{{ $errors->first('employee') }}</strong>
                        </span>
                        @endif	
                     </div>
                  </div>
                  
                 <div class="form-group client" style="display:none">
                   <label class="control-label col-md-3">Client Business Name :</label>
                     <div class="col-md-4">
                        <div class="">
                            <input type="text" name="busname" readonly id="busname" class="form-control">
                         
                        </div>
                     </div>
                  </div>
                  
                  
                   <div class="form-group client" style="display:none">
                   <label class="control-label col-md-3">Client Name :</label>
                     <div class="col-md-4">
                        <div class="">
                            <input type="text" name="clientname" readonly id="clientname" class="form-control">
                         
                        </div>
                     </div>
                  </div>
                  <div class="form-group client1" style="display:none">
                   <label class="control-label col-md-3">Client File # :</label>
                     <div class="col-md-4">
                        <div class="">
                          <input type="text" name="clientfile" readonly id="clientfile" class="form-control">
                        </div>
                     </div>
                  </div>
                  <div class="form-group client1" style="display:none">
                   <label class="control-label col-md-3">Client Telephone # :</label>
                     <div class="col-md-4">
                        <div class="">
                          <input type="text" name="clientno" readonly id="clientno" class="form-control">
                        </div>
                     </div>
                  </div>
                  <div class="form-group">
                   <label class="control-label col-md-3">Call Purpose :</label>
                     <div class="col-md-4">
                        <div class="">
                          <select name="purpose" id="purpose2" class="form-control fsc-input">
                              <option value="">---Select---</option>
                              @foreach($purpose as $pr)
                              <option value="{{$pr->purposename}}" @if($task->purpose==$pr->purposename) selected @endif>{{$pr->purposename}}</option>
                              @endforeach
                              
                           </select>
                        </div>
                     </div>
                    
                  </div>
                  
                  <div class="form-group client">
                   <label class="control-label col-md-3">Other :</label>
                     <div class="col-md-4">
                        <div class="">
                            <input type="text" name="other" value="{{$task->rlt_msg}}" id="other" class="form-control">
                         
                        </div>
                     </div>
                  </div>
                   <div class="form-group client">
                   <label class="control-label col-md-3">What is the Message :</label>
                     <div class="col-md-4">
                        <div class="">
                            <textarea type="text" name="othermsg"  value="" id="othermsg" class="form-control">{{$task->content}}</textarea>
                         
                        </div>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="control-label col-md-3">Message :</label>
                     <div class="col-md-4">
                        <div class="">
                          
                           <select name="purpose1" id="purpose" class="form-control fsc-input">
                              <option value="">---Select---</option>
                              <option value="3" @if($task->call_back=="3") selected @endif>Urgent</option>
                              <option value="1" @if($task->call_back=="1") selected @endif>Regular</option>
                              <option value="2" @if($task->call_back=="2") selected @endif>Important</option>
                           </select>
                            <input type="hidden" value="" name="other" id="other" class="form-control fsc-input">
                        </div>
                     </div>
                  </div>
                  </div>
                   <div class="form-group">
                   <label class="control-label col-md-3">Status :</label>
                     <div class="col-md-4">
                        <div class="">
                          <select name="status1" id="status1" class="form-control fsc-input">
                              <option value="">---Select---</option>
                               <option value="On Hold" class="Yellow" @if($task->status1=="On Hold") selected @endif>On Hold</option>
                              <option value="Forword" class="Red"  @if($task->status1=="Forword") selected @endif>Forword</option>
                              <option value="Under Progress" class="Blue" @if($task->status1=="Under Progress") selected @endif>Under Progress</option>
                              <option value="Done" class="Green" @if($task->status1=="Done") selected @endif>Done</option>
                             
                           </select>
                        </div>
                        	
                     </div>
                  </div>
                   <div class="form-group">
                     <label class="control-label col-md-3">What To Do :</label>
                     <div class="col-md-4">
                        <div class="">
                          <input type="text" name="whattodo" value="{{$task->whattodo}}" id="whattodo" class="form-control ex2">
                        </div>
                        	
                     </div>
                  </div> 
                  <div class="form-group">
                     <label class="control-label col-md-3">Instruction :</label>
                     <div class="col-md-4">
                        <div class="">
                          <input type="text" name="instruction" value="{{$task->instruction}}" id="instruction" class="form-control ex2">
                        </div>
                        	
                     </div>
                  </div> 
                  <div class="form-group client">
                   <label class="control-label col-md-3">Note :</label>
                     <div class="col-md-4">
                        <div class="">
                            <input type="text" name="notes" value="{{$task->forwordmsg}}" id="notes" class="form-control">
                        </div>
                     </div>
                  </div>
                   <div class="form-group client">
                   <label class="control-label col-md-3">Return Date :</label>
                     <div class="col-md-4">
                        <div class="">
                            <input type="text" name="returndate" value="{{$task->returndate}}" id="date" class="form-control">
                        </div>
                     </div>
                  </div>
                  
                  
                  
                  
                  <div class="card-footer">
                     <div class="row">
                        <div class="col-md-8 col-md-offset-3">
                              <div class="row">
                             <div class="col-md-3">
                           <input class="btn_new_save  btn-primary1 primary1" type="submit" name="submit" value="save">
                           </div>
                            <div class="col-md-3">
                           <a class="btn_new_cancel" href="{{url('/fscemployee/getmsg')}}">Cancel</a>
                        </div>
                        </div>
                        </div>
                        
                        
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
   </section>
</div>

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6/js/select2.min.js"></script>
<script>
   

$(".selectpicker").select2({

})


</script>
<script>
$(document).ready(function(){
	$(document).on('change','#type', function()
	{
		var id = $(this).val();
$('#employee').empty();
if(id=='Other Person')
{
    $('.client').show();
    $('.client1').show();
    document.getElementById('busname').removeAttribute('readonly');
    document.getElementById('clientname').removeAttribute('readonly');
    document.getElementById('clientfile').removeAttribute('readonly');
    document.getElementById('clientno').removeAttribute('readonly');
}

		$.get('{!!URL::to('/fscemployee/getmessage')!!}?id='+id, function(data)
		{  
            $('#employee').empty();
            $('#employee').append('<option value="">---Select---</option>');
           $.each(data, function(index, subcatobj)
		   {
//$('#employee1').val(subcatobj.firstName);
if(id=='employee')
{
  $('#employee').append('<option value="' + subcatobj.id + '">' + subcatobj.firstName + ' ' + subcatobj.middleName + ' ' + subcatobj.lastName + '</option>');  
 
}
if(id=='Approval')
{
 $('#employee').append('<option value="' + subcatobj.id + '">' + subcatobj.first_name + ' ' + subcatobj.middle_name + ' ' + subcatobj.last_name + '</option>');
}	
		   })

		});
			
	});
});



</script>
<script language="javascript">
    $(document).ready(function () {
        $("#date").datepicker({
            minDate: 0,
        });
    });
</script>
<script>
$(document).ready(function(){
	$(document).on('change','#employee', function()
	{
	    var selectedCountry = $("#type option:selected").val();
		var id = $(this).val(); //alert(selectedCountry);
		$.get('{!!URL::to('/fscemployee/getmessage')!!}?id='+id+'&state=' + selectedCountry, function(data)
		{  
           $('#clientname').empty();
           $('#clientname').empty();
           $('#clientno').empty();
           $('#busname').empty();
           $('#clientfile').empty();
           $.each(data, function(index, subcatobj)
		   {
            document.getElementById('busname').readOnly =true;
            document.getElementById('clientname').readOnly =true;
            document.getElementById('clientfile').readOnly =true;
            document.getElementById('clientno').readOnly =true;	
        //   alert(subcatobj.type);
if('employee'==subcatobj.type)
{
$('.client').show();
$('.client1').show();
$('#clientname').val(subcatobj.firstName);
$('#clientno').val(subcatobj.telephoneNo1);
$('#busname').val(subcatobj.business_name);
$('#clientfile').val(subcatobj.employee_id); 
}
if('Approval'==subcatobj.status)
{
     $('.client').show();
      $('.client1').show();
$('#clientname').val(subcatobj.first_name);
$('#clientno').val(subcatobj.business_no);
$('#busname').val(subcatobj.business_name);
$('#clientfile').val(subcatobj.filename);
}	
	})
	});
	});
});
</script>
<!--<script type="text/javascript">
  $(document).ready(function() {
    $("#date").change(function() {
        
      var startdate= $("#date").val();
      var monthNames = [
        "Jan", "Feb", "Mar",
        "Apr", "May", "Jun", "Jul",
        "Aug", "Sep", "Oct",
        "Nov", "Dec"
      ];
    var weekday = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
      var durtion=$('#duration').val();
      var  date = new Date(startdate);
      var day = weekday[date.getDay()];
      var monthly=30;
      var weekly=7;
      var bimonthly=15;
      var biweekly=14;
      var monthss=monthNames[(date.getMonth())];
      var yearss=date.getFullYear();
      var yyy=yearss % 4 ;
      //if(yyy)
      //{
      //alert('true');
      //}
      //else
      //{
      //alert('false');
      //}
      if(durtion == "Weekly")
      {
        var totaldays=6;
      }
      else if(durtion == "Monthly")
      {
        if(monthss == 'Jan' || monthss == 'Mar' || monthss == 'May' || monthss == 'Jul' || monthss == 'Aug' || monthss == 'Oct' || monthss == 'Dec')
        {
          var totaldays=30;
        }
        else if(monthss == 'Feb')
        {
          //if(years / 4 = 0)
          if(yyy ==0)
          {
            var totaldays=28;
          }
          else if(yyy ==1)
          {
            var totaldays=27;
          }
        }
        else if(monthss == 'Apr' || monthss == 'Jun' || monthss == 'Sep' || monthss == 'Nov' )
        {
          var totaldays=29;
        }
      }
      else if(durtion == "Bi-Weekly")
      {
        var totaldays=13;
      }
      else if(durtion == "Bi-Monthly")
      {
        var totaldays=14;
      }
    // var vv = day + totaldays;
      
    date.setDate(date.getDate() + totaldays);// alert(vv);
      var date1 = ("0" + (date.getMonth() + 1)).slice(-2)  + "/" + ("0" + date.getDate()).slice(-2) + "/" + date.getFullYear();// alert(date.getDate())
     // alert(date1);
      var newdate = new Date(date1);
        var day1 = weekday[newdate.getDay()];
       //alert(newdate);
      var date2=monthNames[(date.getMonth())] + "/" + date.getDate() + "/" + date.getFullYear() ;
     // $('#sch_end_date').val(date1);
    //  $('#day').val(day);
      $('#sch_end_day').val(day1);
      //document.write(date2);
    });
    $("#duration").change(function() {
      $('#sch_end_date').val('');
      $('#sch_start_date').val('');
    });
  });
</script>-->
<style>
    .select2-container .select2-selection--single {
    box-sizing: border-box;
    cursor: pointer;
    display: block;
    height: 39px; border-redius:4px;
    user-select: none;
    -webkit-user-select: none;
}
    
</style><div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Call Purpose</h4>
      </div>
      <div class="modal-body" style="display: inline-table;">
           <form action="" method="post" id="ajax2">
                {{csrf_field()}}
        <div class="form-group">
                   <label class="control-label col-md-3">Call Purpose :</label>
                     <div class="col-md-6">
                        <div class="">
                            <input type="text" name="newopt" id="newopt" class="form-control" placeholder="Call Purpose">
                        </div>
                     </div>
                     <div class="col-md-2">
                        <div class="">
                            <input type="button" id="addopt" class="btn btn-primary" value="Add Call Purpose">
                        </div>
                     </div>
                  </div>
                  </form>
                  <div class="form-group">
                   <label class="control-label col-md-3"></label>
                     
                  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script>
          $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  }
              });
           
             $(function () {
                $('#addopt').click(function () { //alert();
                    var newopt = $('#newopt').val();
                    if (newopt == '') {
                        alert('Please enter something!');
                        return;
                    }

                   //check if the option value is already in the select box
                    $('#purpose1 option').each(function (index) {
                        if ($(this).val() == newopt) {
                            alert('Duplicate option, Please enter new!');
                        }
                    })
                    $.ajax({
        type: "post",
        url: "{!!route('purpose.purposes')!!}",
        dataType: "json",
        data: $('#ajax2').serialize(),
        success: function(data){
             alert('Successfully Add');
             $('#purpose2').append('<option value=' + newopt + '>' + newopt + '</option>');
             $("#div").load(" #div > *");
             $("#newopt").val('');
        },
        error: function(data){
             alert("Error")
        }
    });
                   
                     $('#myModal').modal('hide');
                });
            });
            
</script>
<style>
    .select2-container .select2-selection--single {
    box-sizing: border-box;
    cursor: pointer;
    display: block;
    height: 39px; border-redius:4px;
    user-select: none;
    -webkit-user-select: none;
}
    
    .form-control1 {
    width: 100%;
    line-height: 1.44;
    color: #555!important;
    border: 2px solid #286db5;
    border-radius: 3px;
    transition: border-color ease-in-out .15s;
    padding: 3px 3px 7px 8px!important;
}
.Red{background-color:red;color:#fff}
.Blue{background-color:rgb(124, 124, 255) !important;color:#fff}
.Green{background-color:#00ef00 !important;color:#fff}
.Yellow{background-color:Yellow !important;}
</style>

<style>
    .select2 {width:100% !important;}
    .select2-container .select2-selection--single {
   
    border: 2px solid #00468F;
}
</style>
<script>
$(document).ready(function(){
    $("select#status1").change(function(){
        var selectedCountry = $(this).children("option:selected").val();
        var color = $("option:selected", this).attr("class");
          $("#status1").attr("class", color).addClass("form-control1 fsc-input");
    });
});
</script>
@endsection()