@extends('fscemployee.layouts.app')
@section('main-content')
<style>
label{float:left;}
.dt-buttons{margin-bottom:10px;}
.search-btn{position:absolute;top:10px;right:16px;background:transparent;border: transparent;}
.dt-buttons {
    margin-top: -41px;
    position: absolute;
    right:15px;
}
.page-title{
        padding: 8px 19px !important;
}
 .box-tools{
        position:absolute !important;
        margin-left: 330px !important;
    }

.edittable tr td{padding:5px 8px 5px 8px !important;}
.buttons-pdf {
    font-size: 0px !important;
    border-radius: 3px;
    padding: 8px 10px !important;
    background: #fff !important;
    border-color: #c6c6c6  !important;
    color:red !important;
}
.buttons-print {
    font-size: 0px !important;
    border-radius: 3px;
    padding: 8px 10px !important;
    background: #3c8dbc !important;
            border-color: #367fa9 !important;
}
.buttons-excel{
    font-size:0px !important;
    border-radius: 3px;
        padding: 8px 10px !important;
        
         background: #00a65a !important;
    border-color: #008d4c !important;
        

}
.buttons-excel:hover{
     background: #008d4c !important;

}
.buttons-pdf:hover{
     background: #f6f6f6  !important;
}

.buttons-print:hover{
     background: #367fa9 !important;
}
.alphabet{
    align-content;
background: #269ef3;
    color: #fff;
    padding: 2px 7px;
    font-weight: bold;}
    

.fa{
    font-size: 16px !important;
}
.Urgent
{ text-align:center!important;
   background:#a4d6f9;  
}
.Immedialtely {  background:red;text-align:center!important; }
.Regular {background:#fff; text-align:center!important;}
.imgicon {
    background: #fff;
    display: block;
    width:35px;
    float: left;
    margin-right: 10px;
    float: left;
    margin-right: 10px;
    border-radius: 2px;
    padding: 3px;
    border: 1px solid #12186b;
    margin-top: -7px;
    height: 35px;
}
.imgicon img{max-width:100%;}

.picker{
    display:none;
}
/*.picker{font-size:16px;text-align:left;line-height:1.2;color:#000;position:absolute;z-index:10000;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}*/
/*.picker__input{cursor:default}.picker__input.picker__input--active{border-color:#0089ec}*/


/*.picker__frame{position:absolute;margin:0 auto;min-width:256px;max-width:666px;width:100%;-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";filter:alpha(opacity=0);-moz-opacity:0;opacity:0;transition:all .15s ease-out}*/
/*.picker__wrap{display:table;width:100%;height:100%}@media (min-height:33.875em){.picker__frame{overflow:visible;top:auto;bottom:-100%;max-height:80%}*/
/*.picker__wrap{display:block}}.picker__box{background:#fff;display:table-cell;vertical-align:middle}*/

/*@media (min-height:33.875em){.picker__box{display:block;font-size:1.33em;border:1px solid #777;border-top-color:#898989;border-bottom-width:0;border-radius:5px 5px 0 0;box-shadow:0 12px 36px 16px rgba(0,0,0,.24)}}*/
/*@media (min-height:40.125em){.picker__frame{margin-bottom:7.5%}.picker__box{font-size:1.5em;border-bottom-width:1px;border-radius:5px}}.picker--opened .picker__frame{-webkit-transform:translateY(0);-ms-transform:translateY(0);transform:translateY(0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";filter:alpha(opacity=100);-moz-opacity:1;opacity:1}*/
/*@media (min-height:33.875em){.picker--opened .picker__frame{top:auto;bottom:0}}*/
</style>
<div class="content-wrapper">
    <section class="content-header page-title">
     		<div class="" style="padding-right:0px;">
     		    <div class="" style="text-align:center;">
     		        <!--<span><i class="fa fa-users" style="float:left;margin-top:5px;"></i></span>-->
     		        <h1><span class="left_title" style="padding-left: 20px;position: absolute;left: 0;">03/23/2021</span>Today's Work<span style="text-align:right;padding-right: 20px !important;position: absolute;right: 0;">Tuesday   |   7:20 am</span></h1>
     		    </div>

     		</div>
    </section>
      <section class="content">
   <div class="row">
      <div class="col-md-12">
         <div class="box box-success">
             <div class="box-header">
                 <div class="box-tools pull-right" style="position: absolute;margin-right: 132px;margin-top:9px;z-index:100;">
                    <div class="table-title">
    					<a data-toggle="modal" data-target="#add_work">Add New Work</a>
    				</div>
                  </div>
                <div class="col-md-8" style="padding-left:0px; padding-right:0px;width:80% !important;">
                    <div class="row">
                        <div class="col-md-6" style="display:inline-flex;"><label style="margin-left:10px;margin-top: 11px;width:100px;">Search : </label>
                            <table style="width: 100%; margin: 0 auto 2em auto;" cellspacing="0" cellpadding="3" border="0">
                                <tbody>
                                    <tr id="filter_global">
                                        <td align="center"><input type="text" class="global_filter form-control" id="global_filter" style="width:200px;" placeholder="All Search"></td>
                                    </tr>
                                    <tr id="filter_col2" data-column="3" style="display:none">
                                        <td align="center"><input type="text" class="column_filter form-control" id="col1_filter" placeholder="Client Id"><button class="search-btn"><i class="fa fa-search"></i></button></td>
                                    </tr>
                                    <tr id="filter_col3" data-column="4" style="display:none">
                                        <td align="center"><input type="text" class="column_filter form-control" id="col2_filter" placeholder="Client Name"><button class="search-btn"><i class="fa fa-search"></i></button></td>
                                    </tr>
                                    <tr id="filter_col6" data-column="5" style="display:none">
                                        <td align="center"><input type="text" class="column_filter form-control" id="col5_filter" placeholder="Filling Status"><button class="search-btn"><i class="fa fa-search"></i></button></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-6">
                            <a href="#" id="previous_day" data-diff="-1" class="btn btn-primary"><i class="fa fa-chevron-left"></i></a>
                            <input type="text" id="test" data-value="03/24/2021" style="vertical-align: middle;height: 40px;border-radius: 3px;border: 2px solid #2fa6f2;font-size: 17px !important;padding: 6px 0px;text-align: center;font-weight: 700;width: 130px;">
                            <input type="text" style="vertical-align: middle;height: 40px;border-radius: 3px;border: 2px solid #2fa6f2;font-size: 17px !important;padding: 6px 0px;text-align: center;font-weight: 700;width: 95px;" value="Friday" readonly>
                            <a href="#" id="next_day" data-diff="1" class="btn btn-primary"><i class="fa fa-chevron-right"></i></a>
                        </div>
                    </div>
                </div>
            
            </div>
        
            <div class="col-md-12" style="margin-top:-35px !important;">
               @if ( session()->has('success') )
               <div class="alert alert-success alert-dismissable">{{session()->get('success') }}</div>
               @endif
               <div class="table-responsive">
                  <table class="table table-hover table-bordered" id="example">
                     <thead>
                        <tr>
                            <th>No</th>
					        <th>Date</th>
					        <th>Time</th>
					        <th>Client ID</th>
					        <th>Client Name</th>
					        <th>Type of Work</th>
					        <th>EstTime</th>
					        <th>Status</th>
					        <th>Action</th>
                        </tr>
                     </thead>
                     <tbody>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div id="add_work" class="modal fade">
        <div class="modal-dialog modal-xl" style="width:70%">
            <form method="post" action="" class="form-horizontal" id="">
                
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Add Work</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-md-4 control-label text-right">Priority : <span class="required">*</span></label>
                            <div class="col-md-3" style="width: 18%;">
                                <select class="form-control" id="work_priority" name="work_priority">
                                    <option value="">Select Priority</option>
                                    <option value="Urgent">Urgent</option>
                                    <option value="Time Sensitive">Time Sensitive</option>
                                    <option value="Regular">Regular</option>
                                    <option value="Appointment">Appointment</option>
                                </select>
                            </div>
                            <div id="showDate">
                                <label class="col-md-1" style="padding-top: 7px;width: auto;">Due Date : </label>
                                <div class="col-md-2" style="width:21%">
                                    <input type="text" class="form-control" name="due_date" id="due_date">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <label class="control-label col-md-4 text-right"><strong>Type Of Client :</strong></label>
                                <div class="col-md-4">
                                    <input type="radio" id="new_client" name="client_type" value="1">
                                    <label for="new_client" style="margin-left:5px; float:initial;">New Client</label>
                                </div>
                                <div class="col-md-4">
                                    <input type="radio" id="already_client" name="client_type" value="2" checked>
                                    <label for="already_client" style="margin-left:5px; float:initial;">Already Client</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row" id="search">
                            <label class="control-label col-md-4 text-right"><strong>Search Client :</strong></label>
                            <div class="col-md-6">
                                <input type="text" name="csearch" id="search_client_name" class="form-control" placeholder="Search">
                                <ul id="clientList"></ul>
                                <input type="hidden" name="client_id" id="client_id" value=""/>
                            </div>
                        </div>
                        
                        <div class="form-group row" id="show_client_name">
                            <label class="col-md-4 control-label text-right">Client Name : <span class="required">*</span></label>
                            <div class="col-md-6">
                                <input type="text" id="client_name" name="client_name" class="form-control" readonly>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label class="col-md-4 control-label text-right">Type : <span class="required">*</span></label>
                            <div class="col-md-6">
                                <select class="form-control" id="types" name="types">
                                    <option value="">Select Type</option>
                                    <option value="Call">Call</option>
                                    <option value="Work">Work</option>
                                    <option value="Appointment">Appointment</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label class="col-md-4 control-label text-right">Type of Work : </label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="type_of_work" id="type_of_work">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 control-label text-right">Details : </label>
                            <div class="col-md-6">
                                <textarea rows="3" class="form-control" name="details"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 control-label text-right">Estimate Time : </label>
                            <div class="col-md-3">
                                <input type="text" class="form-control" name="est_time" id="est_time">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 control-label text-right">Status : </label>
                            <div class="col-md-3">
                                <select class="form-control" name="status">
                                    <option value="">Select</option>
                                    <option value="Under Progress">Under Progress</option>
                                    <option value="Hold">Hold</option>
                                    <option value="Waiting">Waiting</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 control-label text-right"></label>
                            <div class="col-md-3">
                                <input class="btn_new_save btn-primary1 primary1" type="submit" value="Save">
                            </div>
                            <div class="col-md-3">
                                <a class="btn_new_cancel" data-dismiss="modal">Cancel</a>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </form>
        </div>
    </div>
</section>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pickadate.js/3.5.6/compressed/picker.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pickadate.js/3.5.6/compressed/picker.date.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pickadate.js/3.5.6/compressed/picker.time.js"></script>

<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/pickadate.js/3.5.6/compressed/themes/default.css" />-->
<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/pickadate.js/3.5.6/compressed/themes/default.date.css" />-->
<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/pickadate.js/3.5.6/compressed/themes/default.time.css" />-->

<script>
$(document).ready(function() {
    var table = $('#example').DataTable( {
        dom: 'Bfrtlip',
        "pagingType": "input",
    "columnDefs": [ {
            "searchable": true,
            "orderable": true,
            "targets": 0
        } ],
        "order": [[ 0, 'asc' ]],
        buttons: [
                 {
                extend:    'copyHtml5',
                text:      '<i class="fa fa-files-o"></i> &nbsp; Copy',
                titleAttr: 'Copy',
                title: $('h3').text(),
                  exportOptions: {
        columns: [0,1, 2, 3,4,5], // Only name, email and role
    }
            },
            {
                extend:    'excelHtml5',
                text:      '<i class="fa fa-file-excel-o"></i>&nbsp; Excel',
               titleAttr: 'Excel',
                title: $('h3').text(),
                 customize: function( xlsx ) {
                var sheet = xlsx.xl.worksheets['sheet1.xml'];
 $('row c', sheet).attr('s', '51');
            },
                  exportOptions: {
        columns: [0,1, 2, 3,4,5], // Only name, email and role
    }
            },
            {
                extend:    'csvHtml5',
                text:      '<i class="fa fa-file-text-o"></i> &nbsp; CSV',
                titleAttr: 'CSV',
                title: $('h3').text(),
                  exportOptions: {
        columns: [0,1, 2, 3,4,5], // Only name, email and role
    }
            },
            {
                extend:    'pdfHtml5',
                text:      '<i class="fa fa-file-pdf-o"></i>&nbsp;  PDF',
                
              customize: function (doc) {
						//Remove the title created by datatTables
						doc.content.splice(0,1);
						//Create a date string that we use in the footer. Format is dd-mm-yyyy
						var now = new Date();
						var jsDate = now.getDate()+'-'+(now.getMonth()+1)+'-'+now.getFullYear();
						
						doc.pageMargins = [20,60,20,20];
						doc.defaultStyle.fontSize = 10;
						doc.styles.tableHeader.fontSize = 10;
						doc['header']=(function() {
							return {
								columns: [{
									    alignment: 'left',
										image: logo,
										width: 50,margin: [200,5]
									},{
										alignment: 'CENTER',
										text: 'List of Client',
										fontSize: 20,
										margin: [10,35],
									},],
								margin: [20, 0, 0,12],alignment: 'center',
							}
						});
						var objLayout = {};
						objLayout['hLineWidth'] = function(i) { return 2; };
						objLayout['vLineWidth'] = function(i) { return 2; };
						objLayout['hLineColor'] = function(i) { return '#ccc'; };
						objLayout['vLineColor'] = function(i) { return '#ccc'; };
						objLayout['paddingLeft'] = function(i) { return 14; };
						objLayout['paddingRight'] = function(i) { return 14; };
						doc.content[0].layout = objLayout;
				},
				titleAttr: 'PDF',
                  exportOptions: {
        columns: [0,1, 2, 3,4,5], // Only name, email and role
    }
            },
            {
           extend: 'print',
          text: '<i class="fa fa-print"></i>&nbsp; Print',
          titleAttr: 'Print',
        customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '10pt' )
                        .prepend(
                            '<center><img src=""/></center>'
                        );
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );
                },
         exportOptions: {
          columns: [0,1, 2, 3,4,5]
      },
      footer: true,
      autoPrint: true
    },],
    } );
$('input.global_filter').on( 'keyup click', function () {
        filterGlobal();
    } );
 
    $('input.column_filter').on( 'keyup click', function () {
        filterColumn( $(this).parents('tr').attr('data-column') );
    } );
  table.on( 'order.dt search.dt', function () {
       table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
          cell.innerHTML = i + 1;
          table.cell(cell).invalidate('dom'); 
       } );
    } ).draw();
     
    
} );
   	
function filterGlobal () {
    $('#example').DataTable().search(
        $('#global_filter').val(),
        $('#global_regex').prop('checked'),
        $('#global_smart').prop('checked')
    ).draw();
}
 
function filterColumn ( i ) {
    $('#example').DataTable().column( i ).search(
        $('#col'+i+'_filter').val(),
        $('#col'+i+'_regex').prop('checked'),
        $('#col'+i+'_smart').prop('checked')
    ).draw();
}
$( "#types" ).on('change',function() {
  // For unique choice
  var selVal = $( "#types option:selected" ).val(); 
 
  if(selVal=='Clientid')  
  {
      $('#filter_global').hide();
      $('#filter_col2').show();
      $('#filter_col5').hide();
      $('#filter_col6').hide();
      $('#filter_col3').hide();
  }
    else if(selVal=='Clientname')  
  {
      $('#filter_global').hide();
      $('#filter_col3').show();
      $('#filter_col2').hide();
      $('#filter_col4').hide();
  }
   else if(selVal=='Category')  
  {
      $('#filter_global').hide();
      $('#filter_col2').hide();
      $('#filter_col6').show();
      $('#filter_col3').hide();
      $('#filter_col4').hide();
  }
 
  else{
      $('#filter_global').show();
       $('#filter_col3').hide();
      $('#filter_col4').hide();
      $('#filter_col5').hide();
      $('#filter_col6').hide();
      $('#filter_col2').hide();
  }
}); 

var picker = $('#test').pickadate({
  format: 'mm/dd/yyyy'
}).pickadate('picker');

$('#previous_day, #next_day').click(function(e) {
  e.preventDefault();
  setDate($(this).data('diff'));
})
function setDate(diff) {
  var date = new Date(picker.get('select').pick);
  var newDate = date.setDate(date.getDate() + diff);
  picker.set('select', newDate)
}

$("#test").datepicker({
    autoclose: true,
    format: "mm/dd/yyyy",
});
</script>

<style>
    .dataTables_filter{display:none;}
</style>
@endsection()

