@extends('fscemployee.layouts.app')
@section('main-content')
<style>
.howtodopopup .form-group{margin-bottom:10px!important; display:block!important;}
.howtodopopup .form-group .form-control{width:100%;}
.buttons-pdf {
    font-size: 0px !important;
    border-radius: 3px;
    padding: 8px 10px !important;
    background: #fff !important;
    border-color: #c6c6c6  !important;
    color:red !important;
}
.buttons-print {
    font-size: 0px !important;
    border-radius: 3px;
    padding: 8px 10px !important;
    background: #3c8dbc !important;
    border-color: #367fa9 !important;
}
.buttons-excel{
    font-size:0px !important;
    border-radius: 3px;
    padding: 8px 10px !important;
    background: #00a65a !important;
    border-color: #008d4c !important;
}
.buttons-excel:hover{
    background: #008d4c !important;
}
.buttons-pdf:hover{
    background: #f6f6f6  !important;
}
.buttons-print:hover{
    background: #367fa9 !important;
}
.fa{
    font-size: 16px !important;
}
</style>
<div class="content-wrapper">
	   <!-- Content Header (Page header) -->
   
    <div class="page-title">
 	        <h1>How To Do <span class="pull-right" style="padding-right:15px;">View</span></h1>
    </div>
    <!-- Main content -->
    <section class="content">
	<div class="row">
	
		<div class="col-md-12">
			<div class="box box-success">
                <div class="box-header">
                    <div class="box-tools pull-right" style="position: absolute;margin-top: 17px;margin-right: 150px;z-index: 9999;">
                        <div class="table-title">
                            <!--<a href="{{route('howtodo.create')}}">Add How To Do</a>-->
                        </div>
                    </div>
                </div>
			<div class="col-md-12">

				@if ( session()->has('success') )
    <div class="alert alert-success alert-dismissable">{{ session()->get('success') }}</div>
@endif
@if ( session()->has('error') )
    <div class="alert alert-danger alert-dismissable">{{ session()->get('error') }}</div>
@endif  
					<div class="table-responsive">
						<table class="table table-hover table-bordered" id="example">
							<thead>
								<tr>
									<th>No</th>
                                    <th>Subject</th>
                                    <th>Software</th>
                                    <th>Website</th>
                                    <th style="width:10%;">Telephone </th>
                                    <th style="width:7%;">Action</th>
								</tr>
							</thead>
							<tbody>
							@foreach($homecontent as $sli)                          
								<tr>
									<td  class="text-center" style="text-align:center">{{$loop->index + 1}}</td>
                                    <td>{!!$sli->subject!!}</td>
                                    <td>{!!$sli->software!!}</td>
                                    <td>{!!$sli->website!!}</td>
                                    <td class="text-center">{!!$sli->telephone!!}</td>
									<td class="text-center">
								    	<a class="btn-action btn-view-edit btn btn-primary" href="#" data-toggle="modal" data-target="#myModal_<?php echo $sli->id;?>"><i class="fa fa-eye"></i></a>
										<!--<a class="btn-action btn-view-edit" href="{{route('howtodo.edit', $sli->id)}}"><i class="fa fa-edit"></i></a>-->
          <!--                              <form action="{{ route('howtodo.destroy',$sli->id) }}" method="post" style="display:none" id="delete-id-{{$sli->id}}">-->
          <!--                              {{csrf_field()}} {{method_field('DELETE')}}-->
          <!--                              </form>-->
                                        
										<!--<a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record ?'))-->
          <!--                                                                        {event.preventDefault();document.getElementById('delete-id-{{$sli->id}}').submit();} else{event.preventDefault();}" href=""><i class="fa fa-trash"></i></a>-->
                                                                                  
                                        <div id="myModal_<?php echo $sli->id;?>" class="modal fade howtodopopup" role="dialog">
  <div class="modal-dialog" style="width:70%;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title text-center"><img src="https://financialservicecenter.net/public/dashboard/images/fsc_logo.png" style="width:100px;"/> 
        <span style="border: 2px solid #ccc; width: 180px; display: block;  margin: 15px auto 0px;  padding: 10px; font-weight: bold;">How To Do</span></h4>
      </div>
      <div class="modal-body" style="max-height:350px; overflow:auto">
       <div class="row form-group">
           <div class="col-md-2 text-right">
               <label class="control-label">Subject:</label>
           </div>
           <div class="col-md-9">
               <input type="text" class="form-control" style="font-weight:inherit !important;background-color:#f7f70061 !important;text-align: center;" value="<?php echo $sli->subject;?>" readonly/>
           </div>
       </div>
      <?php $l=1; 
       $notecon = count($howtodo);?>
		@if($notecon != NULL)
       	@foreach($howtodo as $how)  
       	@if($sli->id == $how->howtodo_id)
       	<?php //print_r($how);?>
        <div class="row form-group">
           <div class="col-md-2  text-right">
               <label class="control-label">Step <?php echo $l; $l++;?>: </label>
           </div>
           <div class="col-md-9">
               <input type="text" class="form-control" style="font-weight:inherit !important;" value="<?php echo str_replace('"',"'",$how->steps);?>" readonly/>
               
           </div>
           <?php if($how->photo !='')
           {
           ?><div class="col-md-2"></div>
           <div class="col-md-9">	<img width="100%"  id="preview_image_5" src="{{asset('public/adminupload/')}}/{{$how->photo}}" /> <i id="loading5" class="fa fa-spinner fa-spin fa-3x fa-fw" style="position: absolute;left: 10%;top:10%;display: none"></i>
																</div>
           <?php 
           }
           ?>
       </div>
       @foreach($howtodo1 as $notes1)
														 @if($notes1->noteid==$how->id)
													
													<div class="form-group row">
													     <div class="col-md-2  text-right">
															<label class="control-label">Step <?php echo $l; $l++;?>:</label>
															 </div>
															<div class="col-md-9">
													<input type="text" class="form-control" value="<?php echo $notes1->steps;?>" readonly/>
														</div>  
														
														
														</div>
														<div><img src=""></div>
														@endif
														 @endforeach
        @endif
       @endforeach
       @endif

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
									</td>
								</tr>
							
								@endforeach
                             </tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</section>		
</div>


<!-- Modal -->


	<script>
$(document).ready(function() {
    var table = $('#example').DataTable( {
        dom: 'Bfrtlip',
    "columnDefs": [ {
            "searchable": true,
            "orderable": true,
            "targets": 0
        } ],
        "order": [[ 0, 'asc' ]],
        buttons: [
                 {
                extend:    'copyHtml5',
                text:      '<i class="fa fa-files-o"></i> &nbsp; Copy',
                titleAttr: 'Copy',
                title: $('h3').text(),
                  exportOptions: {
        columns: [0,1, 2, 3,4,5], // Only name, email and role
    }
            },
            {
                extend:    'excelHtml5',
                text:      '<i class="fa fa-file-excel-o"></i>&nbsp; Excel',
               titleAttr: 'Excel',
                title: $('h3').text(),
                 customize: function( xlsx ) {
                var sheet = xlsx.xl.worksheets['sheet1.xml'];
 $('row c', sheet).attr('s', '51');
            },
                  exportOptions: {
        columns: [0,1, 2, 3,4,5], // Only name, email and role
    }
            },
            {
                extend:    'csvHtml5',
                text:      '<i class="fa fa-file-text-o"></i> &nbsp; CSV',
                titleAttr: 'CSV',
                title: $('h3').text(),
                  exportOptions: {
        columns: [0,1, 2, 3,4,5], // Only name, email and role
    }
            },
            {
                extend:    'pdfHtml5',
                text:      '<i class="fa fa-file-pdf-o"></i>&nbsp;  PDF',
                
              customize: function (doc) {
						//Remove the title created by datatTables
						doc.content.splice(0,1);
						//Create a date string that we use in the footer. Format is dd-mm-yyyy
						var now = new Date();
						var jsDate = now.getDate()+'-'+(now.getMonth()+1)+'-'+now.getFullYear();
						
						doc.pageMargins = [20,60,20,20];
						doc.defaultStyle.fontSize = 10;
						doc.styles.tableHeader.fontSize = 10;
						doc['header']=(function() {
							return {
								columns: [{
									    alignment: 'left',
										image: logo,
										width: 50,margin: [200,5]
									},{
										alignment: 'CENTER',
										text: 'List of Client',
										fontSize: 20,
										margin: [10,35],
									},],
								margin: [20, 0, 0,12],alignment: 'center',
							}
						});
						var objLayout = {};
						objLayout['hLineWidth'] = function(i) { return 2; };
						objLayout['vLineWidth'] = function(i) { return 2; };
						objLayout['hLineColor'] = function(i) { return '#ccc'; };
						objLayout['vLineColor'] = function(i) { return '#ccc'; };
						objLayout['paddingLeft'] = function(i) { return 14; };
						objLayout['paddingRight'] = function(i) { return 14; };
						doc.content[0].layout = objLayout;
				},
				titleAttr: 'PDF',
                  exportOptions: {
        columns: [0,1, 2, 3,4,5], // Only name, email and role
    }
            },
            {
           extend: 'print',
          text: '<i class="fa fa-print"></i>&nbsp; Print',
          titleAttr: 'Print',
        customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '10pt' )
                        .prepend(
                            '<center><img src=""/></center>'
                        );
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );
                },
         exportOptions: {
          columns: [0,1, 2, 3,4,5]
      },
      footer: true,
      autoPrint: true
    },],
    } );
$('input.global_filter').on( 'keyup click', function () {
        filterGlobal();
    } );
 
    $('input.column_filter').on( 'keyup click', function () {
        filterColumn( $(this).parents('tr').attr('data-column') );
    } );
  table.on( 'order.dt search.dt', function () {
       table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
          cell.innerHTML = i + 1;
          table.cell(cell).invalidate('dom'); 
       } );
    } ).draw();
       table.columns(6)
        .search('^(?:(?!Inactive|New|1).)*$\r?\n?', true, false)
        .draw();
    $("#choice").on("change",function(){
 var _val = $(this).val();//alert(_val);
 
 if(_val == 'Inactive'){   
        table.columns(6).search(_val).draw();
  }
 else if(_val == 'New'){   
        table.columns(6).search(_val).draw();
  }
  else if(_val == 'Active'){  //alert();
         table.columns(6).search(_val).draw();
          table.columns(6)
        .search('^(?:(?!Inactive|New|1).)*$\r?\n?', true, false)
        .draw();
  }
  else{
        table
        .columns()
        .search('')
        .draw(); 
  }
  })
} );
   	</script>
@endsection()