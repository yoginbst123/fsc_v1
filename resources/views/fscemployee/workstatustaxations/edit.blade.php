@extends('fscemployee.layouts.app')
@section('main-content')
<div class="content-wrapper">
    <section class="page-title content-header">
     		<h1>Edit Income Detail 11</h1>
    </section>
    <section class="content">
	    <div class="row">
		    <div class="col-md-12">
			    <div class="box box-success">
    			    <div class="box-header">
                        <div class="box-tools pull-right">
                        </div>
                    </div>
                    
    				<div class="col-md-12">
                        <form class="form-horizontal" method="post" action="{{route('workstatustaxations.update')}}" enctype="multipart/form-data">
                        {{csrf_field()}} 
                            
                            <input type="hidden" name="federalid" value="{{$federaldata->id}}">
                            <input type="hidden" name="client_id" value="{{$federaldata->client_id}}">
                            <table style="width:100%; max-width:520px; margin:0px auto;" class="taxtable">
                                <tr>
                                    <td>
                                        <label class="control-label labels">Filling Year</label>
                                        <select class="form-control federalyear" name="federalsyear" required>
                                            
                                            <option value="">Select</option>
                                            @if($federaldata->federalsyear!='')   
                                            <?php
                                               
                                               
                                                    $aa=date('Y')-1;
                                                    $bb=date('Y');
                                                    ?>
                                                    <option value="<?php echo $aa;?>" @if($federaldata->federalsyear=='2019') selected @endif><?php echo $aa;?></option>
                                                    <option value="<?php echo $bb;?>" @if($federaldata->federalsyear=='2020') selected @endif><?php echo $bb;?></option>
                                                    <?php
                                               
                                    
                                                
                                            ?>    
                                            @else
                                            <?php
                                                
                                                $now=date('M-d-Y');
                                                if('Jul-25-2021'==$now)
                                                {
                                                    $aa=date('Y')-2;
                                                    $bb=date('Y')-1;
                                                    ?>
                                                    <option value="<?php echo $aa;?>"><?php echo $aa;?></option>
                                                    <option value="<?php echo $bb;?>"><?php echo $bb;?></option>
                                                    <?php
                                                }
                                                else
                                                {
                                                    $aa=date('Y')-1;
                                                    ?>
                                                    <option @if($federaldata->federalsyear!='') @else value="<?php echo $aa;?>" @endif><?php echo $aa;?></option>
                                                    <?php
                                                }
                                                
                                            ?>
                                            
                                            @endif
                                        </select>
                                    </td>
                                    <td>
                             <label class="control-label labels">Tax Return</label>
                             <?php $aa=date('Y')-1; ?>
                             <select class="form-control taxation" name="federalstax" required>
                                <option value="">Select</option>
                                <option value="Extension" @if($federaldata->federalstax=='Extension') selected @endif>Extension </option>
                                @foreach($Incometaxfederal as $Incometaxfederal1)
                                @if($Incometaxfederal1->federalsyear==$aa)
                                
                                @else
                                <option value="Original" @if($federaldata->federalstax=='Original') selected @endif>Original</option>
                                @endif
                                @endforeach
                                <option value="Amendment" @if($federaldata->federalstax=='Amendment') selected @endif>Amendment</option>
                            </select>
                        </td>
                                    <td>
                            <label class="control-label labels">Due Date</label>
                             <input type="text" class="form-control duedate" name="federalsduedate" value="{{$federaldata->federalsduedate}}" placeholder="Mar-15-2020" readonly/>
                        </td>
                                </tr>
                            </table>
                            
                            <table class="taxtable table table-bordered" style="margin-top:20px;">
                                <tr>
                                    <td>
                         <label class="form-label">Federal</label>
                         
                     </td>
                                    <td>
                         <label class="form-label">Form No.</label>
                          <input type="text" readonly class="form-control formno" name="federalsform" value="{{$federaldata->federalsform}}" style="width:100px"/ required>
                         
                     </td>
                                    <td>
                         <label class="form-label">Filling Method</label>
                          <select class="form-control filingmethod" name="federalsmethod" required>
                            <option value="">Select</option>
                            <option value="E-File" @if($federaldata->federalsmethod=='E-File') selected @endif>E-File</option>
                            <option value="Paperfile" @if($federaldata->federalsmethod=='Paperfile') selected @endif>Paperfile</option>
                        </select>
                         
                     </td>
                                    <td>
                         <label class="form-label">Filling Date</label>
                         <input type="text" class="form-control fdate" id="fillingdate" name="federalsdate" value="{{$federaldata->federalsdate}}" placeholder="MM/DD/YYYY" required readonly/>
                         
                     </td>
                                    <td>
                         <label class="form-label">Status of Filling</label>
                         <select class="form-control" name="federalsstatus" required>
                            <option value="">Select</option>
                            <option value="Accepted" @if($federaldata->federalsstatus=='Accepted') selected @endif>Accepted</option>
                            <option value="EF Processing" @if($federaldata->federalsstatus=='EF Processing') selected @endif>EF Processing</option>
                            <option value="Pending" @if($federaldata->federalsstatus=='Pending') selected @endif>Pending</option>
                            <option value="Rejected" @if($federaldata->federalsstatus=='Rejected') selected @endif>Rejected</option>
                        </select>
                         
                         
                     </td>
                             <!--       <td style="width:100px">-->
                             <!--    <label class="form-label">File</label>-->
                             <!--     <input type="file" class="form-control" name="federalsfile"/>-->
                             <!--</td>-->
                                    <td>
                         <label class="form-label">Note</label>
                           <textarea class="form-control" name="federalsnote">{{$federaldata->federalsnote}}</textarea>
                         
                     </td>
                                </tr>
                            </table>
                            <table class="taxtable table table-bordered" style="margin-top:20px;" id="taxationtable">
                                <tr>
                                  <th>  <label class="form-label">State</label></th>
                                  <th><label class="form-label">Form No.</label></th>
                                  <th><label class="form-label">Filling Method</label></th>
                                  <th><label class="form-label">Filling Date</label></th>
                                  <th> <label class="form-label">Status of Filling</label></th>
                                  
                                  <th><label class="form-label">Note</label></th>
                                  <th><a href="#" class="btn btn-primary add-rowform"><i class="fa fa-plus"></i></a></th>
                                </tr>
                              
                              
                                <tr>
                                @foreach($statedatatax as $statetax1)
                                    <td>
                                        <input type="hidden" name="taxid[]" value="{{$statetax1->id}}">
                                     <select class="form-control" name="statetax[]" id="statetax">
                                        <option>Select</option>
                                        <option>Select</option>
                                        @foreach($datastate3 as $datastate)
                                            <option value="{{$datastate->state}}" @if($statetax1->statetax==$datastate->state) selected @endif>{{$datastate->state}}</option>
                                        @endforeach
                                     </select>
                                     
                                      
                                   
                                     
                                 </td>
                                    <td>
                                     
                                      <input type="text" class="form-control" name="stateformno[]" id="statefederalformno" value="{{$statetax1->stateformno}}"  style="width:100px" readonly/>
                                     
                                 </td>
                                    <td>
                                    
                                    <select class="form-control filingmethod" name="statemethod[]">
                                        <option value="">Select</option>
                                        <option value="E-File" @if($statetax1->statemethod=='E-File') selected @endif>E-File</option>
                                        <option value="Paperfile" @if($statetax1->statemethod=='Paperfile') selected @endif>Paperfile</option>
                                    </select>
                                     
                                 </td>
                                    <td>
                                     
                                     <input type="text" class="form-control fdate" id="fillingdate" name="statedate[]" value="{{$statetax1->statedate}}" placeholder="MM/DD/YYYY" required readonly/>
                                     
                                 </td>
                                    <td>
                                   
                                     <select class="form-control" name="statestatus[]">
                                        <option value="">Select</option>
                                        <option value="Accepted" @if($statetax1->statestatus=='Accepted') selected @endif>Accepted</option>
                                        <option value="EF Processing" @if($statetax1->statestatus=='EF Processing') selected @endif>EF Processing</option>
                                        <option value="Pending" @if($statetax1->statestatus=='Pending') selected @endif>Pending</option>
                                        <option value="Rejected" @if($statetax1->statestatus=='Rejected') selected @endif>Rejected</option>
                                    </select>
                                     
                                     
                                 </td>
                                  
                                    
                                 <td>
                                     
                                       <textarea class="form-control" name="statenote[]">{{$statetax1->statenote}}</textarea>
                                     
                                 </td>
                                </tr>
                                @endforeach
                            </table>
             
                            <div class="row form-group saves">
                        <label class="col-md-4 control-label">&nbsp;</label>
                        <div class="col-md-2">
                            <input type="submit" class="btn_new_save primary" value="Save">
                        </div>
                         <div class="col-md-2">
                            <a href="#" class="btn_new_cancel" data-dismiss="modal">Cancel</a>
                        </div>
                       
                    </div>
                            
                        
            
                        </form>
    				</div>
    			</div>
		    </div>
	    </div>
	</section>
</div>


   <script>
   
    $( function()
    {
        $('.fdate').datepicker({
            autoclose: true
        });
    });
   
    $(document).ready(function()
    {
        
        
        $(".income_number").on("input", function(evt) 
        {
    		var self = $(this);
    		self.val(self.val().replace(/[^\d].+/, ""));
    		if ((evt.which < 48 || evt.which > 57)) 
    		{
    			evt.preventDefault();
    		}
        });
        
        $('.wagestotal').blur(function () {
        var sum = 0;
        $('.wagestotal').each(function() {
            sum += Number($(this).val());
        });
        $('.totalamts').val(sum);
        });
        
        
        $(".incomeDetail").click(function ()
        {
            var incomeid = $(this).attr('data-id');
            $("#incomeid").val(incomeid);
            $('#myModalIncomeUpdate').modal('show');
            $.get('{!!URL::to('getIncomedata')!!}?incomeid='+incomeid, function(data)
            {  
                //console.log(data);exit;
                if(data == "")
                {
                    
                }
                else
                {
                    $('#wrkyear').val(data.wrkyear);
                    $('#wagetotal').val(data.wagetotal);
                    $('#taxintrest').val(data.taxintrest);
                    $('#qulifieddividends').val(data.qulifieddividends);
                    $('#pension').val(data.pension);
                    $('#socialsecurity').val(data.socialsecurity);
                    $('#capital').val(data.capital);
                    $('#otherincome').val(data.otherincome);
                    $('#totalamount').val(data.totalamount);
                }
                

                
            });
        });
        
        
        $('#statetax').on('change', function()
        {
            var statetaxval=$("#statetax").val();
            $.get('{!!URL::to('getTaxstatedata')!!}?statetaxval='+statetaxval,function(data)
            {  
                //console.log(data);exit;
                if(data=='')
                {
                   $("#statefederalformno").val(); 
                }
                else
                {
                    $("#statefederalformno").val(data.taxform);
                }
                
            });
   
        });
   
   
        $('#statetaxstate').on('change', function()
        {
            var statetaxval=$("#statetaxstate").val();
            $.get('{!!URL::to('getTaxstatedata')!!}?statetaxval='+statetaxval,function(data)
            {  
                //console.log(data);exit;
                if(data=='')
                {
                   $("#statefederalformnostate").val(); 
                }
                else
                {
                    $("#statefederalformnostate").val(data.taxform);
                }
                
            });
   
        });
        
        
            var cnt=1;
            $(".add-rowform").click(function()
            {
                cnt++;
                //$('').show();
                //var aa=$('.statetax2').html();
                //var markup = ' <tr><td><select class="form-control statetax'+cnt+'" name="statetax[]" id="statetax'+cnt+'"><option>Select</option> @foreach($datastate2 as $datastate3)<option value="{{$datastate3->state}}">{{$datastate3->state}}</option>@endforeach</select></td><td><input type="text" readonly class="form-control formno" name="stateformno[]" id="statefederalformno'+cnt+'" style="width:100px"/></td><td><select class="form-control filingmethod" name="statemethod[]"><option value="">Select</option><option value="E-File">E-File</option><option value="Paperfile">Paperfile</option></select></td><td><input type="date" class="form-control" id="fillingdate" name="statedate[]" id="federaldate'+cnt+'"/></td><td><select class="form-control" name="statestatus[]" id="federalstatus'+cnt+'"><option value="">Select</option> <option value="Accepted">Accepted</option><option value="EF Processing">EF Processing</option><option value="Pending">Pending</option><option value="Rejected">Rejected</option></select></td><td style="width:100px"><input type="file" class="form-control" name="statefile[]" id="federalfile'+cnt+'" style="display:block; margin-top:0px;"/></td> <td><textarea class="form-control" name="statenote[]" id="federalnote'+cnt+'"></textarea></td><td><a href="#" class="btn btn-danger removetrrow"><i class="fa fa-minus"></i></a></td></tr>';
                var markup = ' <tr><td><select class="form-control statetax'+cnt+'" name="statetax[]" id="statetax'+cnt+'"><option>Select</option> @foreach($datastate2 as $datastate3)<option value="{{$datastate3->state}}">{{$datastate3->state}}</option>@endforeach</select></td><td><input type="text" readonly class="form-control formno" name="stateformno[]" id="statefederalformno'+cnt+'" style="width:100px"/></td><td><select class="form-control filingmethod" name="statemethod[]"><option value="">Select</option><option value="E-File">E-File</option><option value="Paperfile">Paperfile</option></select></td><td><input type="text" class="form-control fdate'+cnt+'" id="fillingdate" name="statedate[]" id="federaldate'+cnt+'" placeholder="MM/DD/YYYY" required readonly/></td><td><select class="form-control" name="statestatus[]" id="federalstatus'+cnt+'"><option value="">Select</option> <option value="Accepted">Accepted</option><option value="EF Processing">EF Processing</option><option value="Pending">Pending</option><option value="Rejected">Rejected</option></select></td> <td><textarea class="form-control" name="statenote[]" id="federalnote'+cnt+'"></textarea></td><td><a href="#" class="btn btn-danger removetrrow"><i class="fa fa-minus"></i></a></td></tr>';
                $("#taxationtable tbody").append(markup);
            
                $( function()
                {
                    $('.fdate'+cnt).datepicker({
                        autoclose: true
                    });
                    
                });
        
                $('.statetax'+cnt).on('change', function()
                {
                        var statetaxval=$('.statetax'+cnt).val();
                        $.get('{!!URL::to('getTaxstatedata')!!}?statetaxval='+statetaxval,function(data)
                        {  
                            if(data=='')
                            {
                               $('#statefederalformno'+cnt).val(); 
                            }
                            else
                            {
                                $('#statefederalformno'+cnt).val(data.taxform);
                            }
                            
                        });
               });
           
            });
            $("body").on("click",".removetrrow",function(){ 
                $(this).parent().parent().remove();
            });
        
        
        $(".stateTaxation").click(function ()
    {
            var stateid = $(this).attr('data-id');
            $("#stateid").val(stateid);
            $('#stateModals').modal('show');
            $.get('{!!URL::to('getClientstatedata')!!}?stateid='+stateid, function(data)
            {  
                //console.log(data);exit;
                if(data == "")
                {
                    
                }
                else
                {
                    $('#stateyear').val(data.stateyear);
                    $('.statetax').val(data.statetax);
                    $('#federalsduedate').val(data.federalsduedate);
                    //$('#federalsduedate').val(date('d-m-Y',strtotime(data.federalsduedate)));
                    $('.stateformno').val(data.stateformno);
                    $('#statemethod').val(data.statemethod);
                    $('.statedate').val(data.statedate);
                    $('#statestatus').val(data.statestatus);
                    $('#statefile').val(data.statefile);
                    $('#statefile_1').html(data.statefile);
                    $('#statenote').val(data.statenote);
                    
                }
                

                
            });
        });
        
        
        $(".federalTaxation").click(function ()
        {
            var federalid = $(this).attr('data-id');
            $("#federalid").val(federalid);
            $('#federalModals').modal('show');
            $.get('{!!URL::to('getClientfederaldata')!!}?federalid='+federalid, function(data)
            {  
                //console.log(data);exit;
                if(data == "")
                {
                    
                }
                else
                {

                    //$('#id').val(data.id);
                    $('#federalsyear').val(data.federalsyear);
                    $('#federalstax').val(data.federalstax);
                    $('#federalsduedate').val(data.federalsduedate);
                    //$('#federalsduedate').val(date('d-m-Y',strtotime(data.federalsduedate)));
                    $('#federalsform').val(data.federalsform);
                    $('#federalsmethod').val(data.federalsmethod);
                    $('.fillingdate').val(data.federalsdate);
                    $('#federalsstatus').val(data.federalsstatus);
                    $('#federalsfile').val(data.federalsfile);
                    $('#federalsfile_1').html(data.federalsfile);
                    $('#federalsnote').val(data.federalsnote);
                    
                }
                

                
            });
        });
    
        
        $('.type_user').on('change', function()
        {   
             //otherid,clientid,employeeuserid,vendorid
             //Client,EE-User,Vendor,Other
             var thisss=$('.type_user').val();
             if(thisss =='Client')
             {
                 $('#clientid').show();
                 $('#employeeuserid').hide();
                 $('#vendorid').hide();
                 $('#otherid').hide();
             }
             else if(thisss =='EE-User')
             {
                 $('#clientid').hide();
                 $('#employeeuserid').show();
                 $('#vendorid').hide();
                 $('#otherid').hide();
             }
             else if(thisss =='Vendor')
             {
                 $('#clientid').hide();
                 $('#employeeuserid').hide();
                 $('#vendorid').show();
                 $('#otherid').hide();
             }
             else if(thisss =='Other')
             {
                 $('#clientid').hide();
                 $('#employeeuserid').hide();
                 $('#vendorid').hide();
                 $('#otherid').show();
             }
        });
        
        $('.notetype_user').on('change', function()
        {   
             //otherid,clientid,employeeuserid,vendorid
             //Client,EE-User,Vendor,Other
             var thisss=$('.notetype_user').val();
             if(thisss =='Client')
             {
                 $('#noteclientid').show();
                 $('#noteemployeeuserid').hide();
                 $('#notevendorid').hide();
                 $('#noteotherid').hide();
             }
             else if(thisss =='EE-User')
             {
                 $('#noteclientid').hide();
                 $('#noteemployeeuserid').show();
                 $('#notevendorid').hide();
                 $('#noteotherid').hide();
             }
             else if(thisss =='Vendor')
             {
                 $('#noteclientid').hide();
                 $('#noteemployeeuserid').hide();
                 $('#notevendorid').show();
                 $('#noteotherid').hide();
             }
             else if(thisss =='Other')
             {
                 $('#noteclientid').hide();
                 $('#noteemployeeuserid').hide();
                 $('#notevendorid').hide();
                 $('#noteotherid').show();
             }
        });
    });
</script>
<script type="text/javascript">
    $(".notesID").click(function () 
        {
            var ids = $(this).attr('data-id');
            //alert(ids);
            $("#notesid").val(ids);
            $('#notesModal').modal('show');
            //console.log(ids);           
            $.get('{!!URL::to('getNotesemp')!!}?documentid='+ids, function(data)
            {  
                //console.log(data);exit;
                if(data == "")
                {
                    
                }
                else
                {
                    //console.log(data.id);exit;
                    $('#NoteID').val(data.id);
                    $('#creattiondate1').val(data.creattiondate);
                    $('#noteday1').val(data.noteday);
                    $('#notetime1').val(data.notetime);
                    $('#notetype_user1').val(data.notetype_user);
                    $('#noteclientid1').val(data.noteclientid);
                    $('#noteemployeeuserid1').val(data.noteemployeeuserid);
                    $('#notevendorid1').val(data.notevendorid);
                    $('#noteotherid1').val(data.noteotherid);
                    $('#noterelated1').val(data.noterelated);
                    $('#notesrelatedcat1').val(data.notesrelatedcat);
                    $('#notes1').val(data.notes);
                    
                    
                       
                                 var thisss=$('#notetype_user1').val();
                                 if(thisss =='Client')
                                 {
                                     $('.noteclientid2').show();
                                     $('.noteemployeeuserid2').hide();
                                     $('.notevendorid2').hide();
                                     $('.noteotherid2').hide();
                                 }
                                 else if(thisss =='EE-User')
                                 {
                                     $('.noteclientid2').hide();
                                     $('.noteemployeeuserid2').show();
                                     $('.notevendorid2').hide();
                                     $('.noteotherid2').hide();
                                 }
                                 else if(thisss =='Vendor')
                                 {
                                     $('.noteclientid2').hide();
                                     $('.noteemployeeuserid2').hide();
                                     $('.notevendorid2').show();
                                     $('.noteotherid2').hide();
                                 }
                                 else if(thisss =='Other')
                                 {
                                     $('.noteclientid2').hide();
                                     $('.noteemployeeuserid2').hide();
                                     $('.notevendorid2').hide();
                                     $('.noteotherid2').show();
                                 }
                    
                }
                

                
            });
        });

    $(".conversationID").click(function () 
        {
            var ids = $(this).attr('data-id');
            //alert(ids);
            $("#conversationid").val(ids);
            $('#conversationModal').modal('show');
            //console.log(ids);           
            $.get('{!!URL::to('getConversationdata')!!}?documentid='+ids, function(data)
            {  
                //console.log(data);exit;
                if(data == "")
                {
                    
                }
                else
                {
                    //console.log(data.id);exit;
                    $('#CovID').val(data.id);
                    $('#creattiondate1').val(data.creattiondate);
                    $('#day1').val(data.day);
                    $('#time1').val(data.time);
                    $('#type_user1').val(data.type_user);
                    $('#clientid1').val(data.clientid);
                    $('#employeeuserid1').val(data.employeeuserid);
                    $('#vendorid1').val(data.vendorid);
                    $('#otherid1').val(data.otherid);
                    $('#conrelatedname1').val(data.conrelatedname);
                    $('#condescription1').val(data.condescription);
                    $('#connotes1').val(data.connotes);
                    
                    
                       
                                 var thisss=$('#type_user1').val();
                                 if(thisss =='Client')
                                 {
                                     $('.clientid2').show();
                                     $('.employeeuserid2').hide();
                                     $('.vendorid2').hide();
                                     $('.otherid2').hide();
                                 }
                                 else if(thisss =='EE-User')
                                 {
                                     $('.clientid2').hide();
                                     $('.employeeuserid2').show();
                                     $('.vendorid2').hide();
                                     $('.otherid2').hide();
                                 }
                                 else if(thisss =='Vendor')
                                 {
                                     $('.clientid2').hide();
                                     $('.employeeuserid2').hide();
                                     $('.vendorid2').show();
                                     $('.otherid2').hide();
                                 }
                                 else if(thisss =='Other')
                                 {
                                     $('.clientid2').hide();
                                     $('.employeeuserid2').hide();
                                     $('.vendorid2').hide();
                                     $('.otherid2').show();
                                 }
                    
                }
                

                
            });
        });

        
        
        
        $(".formationID").click(function () 
        {
            var ids = $(this).attr('data-id');
            //alert(ids);
            $("#formationid").val(ids);
            $('#formationModal').modal('show');
            //console.log(ids);           
            $.get('{!!URL::to('getFormationdata')!!}?documentid='+ids, function(data)
            {  
                //console.log(data);exit;
                if(data == "")
                {
                    
                }
                else
                {
                    //console.log(data.id);exit;
                    var famt='$'+ data.formation_amount;
                    $('#id').val(data.id);
                    $('#client_id77').val(data.client_id);
                    $('#formation_yearbox77').val(data.formation_yearbox);
                    $('#formation_yearvalue77').val(data.formation_yearvalue);
                    $('#formation_amount77').val(famt);
                    $('#formation_payment77').val(data.formation_payment);
                    $('#record_status77').val(data.record_status);
                    $('#annualreceipt77').val(data.annualreceipt);
                    $('#annualreceipt2_77').html(data.annualreceipt);
                    $('#formation_work_officer77').val(data.formation_work_officer);
                    $('#formation_work_officer2_77').html(data.formation_work_officer);
                    
                    $('.record_paid_by88').val(data.record_paid_by);
                    $('#record_paid_by77').val(data.record_paid_by);
                    $('#formation_penalty77').val('$'+data.formation_penalty);
                    $('#work_processingfees77').val('$'+data.work_processingfees);
                    $('#record_totalamt77').val('$'+data.record_totalamt);
                    $('#paiddate77').val(data.paiddate);
                    $('#record_note77').val(data.record_note);
                    
                    var paidby2=$('.record_paid_by88').val();
                    //alert(paidby2);
                    if(paidby2 =='Client')
                    {
                         $('.hidepaid2').hide();
                    }
                    else if(paidby2 == 'FSC')
                    {
                         $('.hidepaid2').show();
                    }
                    else
                    {
                         $('.hidepaid2').hide();
                    }
                }
                

                
            });
        });
   
   
        $(".passingID").click(function () 
        {
            var ids = $(this).attr('data-id');
            //alert(ids);
            $("#idkl").val(ids);
            $('#myModal').modal('show');
            //console.log(ids);           
            $.get('{!!URL::to('getClientDocumentdata')!!}?documentid='+ids, function(data)
            {  
                console.log(data);
                if(data == "")
                {
                    
                }
                else
                {
                    //$('#county').html(data);
                    //console.log(data.id);exit;
                    $('#id').val(data.id);
                    $('#filename_idss').val(data.filename_id);
                    $('#client_id').val(data.client_id);
                    $('#documentsname').val(data.documentsname);
                    $('#clientdocument').val(data.clientdocument);
                    $('#clientdocument22').html(data.clientdocument);
                }
                

                
            });
        });
   </script>


<script>

    function federalclick()
    { 
        var checkBox = document.getElementById("federal");
        if (checkBox.checked == true){
        $('.labels').show();
        $('.saves').show();
        $('.federals').show();
        
       }
       else
       {
           
            $('.federals').hide();
          
       }
    }
   
   
    function stateclick()
    { 
        var checkBox1 = document.getElementById("states");
        if (checkBox1.checked == true)
        {
            $('.labels').show();
            $('.saves').show();
            $('.states').show();
        }
        else
        {
   
            $('.states').hide();
        }
   }
    
    
        
    
    $(document).ready(function()
    {
        
        $('.federalyear').change(function()
        {
             
             var this1=$(this).val();
             var aa='<?php echo $aa=date('Y')-1;?>';
             
             if(this1 == aa)
             {
                 $(".duedate").val('Apr-15-<?php echo $aa+1;?>');
                 
             }
            
            
            
            //  if(this1 == 'Extension')
            //  {
            //      $("select option[value='Extension']").hide();
            //      $(".duedate").val('Mar-15-<?php echo $aa+1;?>');
                 
            //  }
            // else if(this1 == bb)
            //  {
            //      $("select option[value='Extension']").hide();
            //      $(".duedate").val('Mar-15-<?php echo $aa+1;?>');
            //  }
            //  else if(this1 == cc)
            //  {
            //      $("select option[value='Extension']").show();
            //      $(".duedate").val('Mar-15-<?php echo $aa+1;?>');
            //  }
      
        });
      
      
        $('.statesyear').change(function()
        {
             
             var this1=$(this).val();
             //alert(this1);
             var aa='<?php echo $aa=date('Y');?>';
             var bb='<?php echo $bb=date('Y')+1;?>';
             var cc='<?php echo $cc=date('Y')+2;?>';
             if(this1 == aa)
             {
                 $("select option[value='Extension']").hide();
                 $(".duedate2").val('Apr-15-<?php echo $aa+1;?>');
                 
             }
            else if(this1 == bb)
             {
                 $("select option[value='Extension']").hide();
                 $(".duedate2").val('Apr-15-<?php echo $bb+1;?>');
             }
             else if(this1 == cc)
             {
                 $("select option[value='Extension']").show();
                 $(".duedate2").val('Apr-15-<?php echo $cc+1;?>');
             }
      
        });
      
      
        $('.taxation').change(function()
        {
              var businessid='<?php if(isset($_REQUEST['id']) && $_REQUEST['id'] !='') { echo $common->business_id; }?>';
            
          var thiss=$(this).val();
          if(thiss == 'Extension')
          {
              if(businessid =='6')
              {
                  $('.formno').val('Form-4868');
              }
              else
              {
              $('.formno').val('Form-7004');    
              }
              
              $("select option[value='E-File']").show();
          }
          
          else if(thiss == 'Original')
          {
              $('.formno').val('Form-1040');
              $("select option[value='E-File']").show();
          }
          else if(thiss == 'Amendment')
          {
              $('.formno').val('Form-1040X');
              $("select option[value='E-File']").hide();
                     
          }
        });
        
        $('.taxation2').change(function()
        {
            var businessid='<?php if(isset($_REQUEST['id']) && $_REQUEST['id'] !='') { echo $common->business_id; }?>';
            var thiss=$(this).val();
              if(thiss == 'Extension')
              {
                   if(businessid =='6')
                  {
                      $('.formno').val('Form-4868');
                  }
                  else
                  {
                  $('.formno').val('Form-7004');    
                  }
                  $("select option[value='E-File']").show();
              }
              
              else if(thiss == 'Original')
              {
                  $('.formno2').val('Form-1040');
                  $("select option[value='E-File']").show();
              }
              else if(thiss == 'Amendment')
              {
                  $('.formno2').val('Form-1040X');
                  $("select option[value='E-File']").hide();
                         
              }
            });
        
        
    
        $('.filingmethod').change(function()
        {
          var thiss=$(this).val();
          if(thiss == 'Paperfile')
          {
              $('.statusof').hide();
          }
          
          else
          {
              $('.statusof').show();
          }
        });
        
        $('.filingmethod2').change(function()
        {
          var thiss=$(this).val();
          if(thiss == 'Paperfile')
          {
              $('.statusof2').hide();
          }
          
          else
          {
              $('.statusof2').show();
          }
        });
      
    });
</script>


<script type="text/javascript">
$(document).ready(function()
 {
    $('.paiddate').datepicker();
     
    var paidby=$('.paidby').val();
    if(paidby =='Client')
    {
         $('.hidepaid').hide();
    
    }
    else if(paidby == 'FSC')
    {
         $('.hidepaid').show();
    }
    else
    {
         $('.hidepaid').hide();
    }
    

    
 $('.paidby').on('change',function()
 { 
     var thiss=$(this).val();
     if(thiss=='Client')
     {
         $('.annualfees').val('');
         $('.hidepaid').hide();
     }
     else if(thiss =='FSC')
     {
         var foryear=$('.formation_yearbox22').val();
         if(foryear =='1')
         {
             $('.annualfees').val('$50.00');
         }
         else if(foryear =='2')
         {
             $('.annualfees').val('$100.00');
         }
         else if(foryear =='3')
         {
             $('.annualfees').val('$150.00');
         }
         $('.hidepaid').show();
     }
     else
     {
         $('.hidepaid').hide();
     }
 });
 
 
    
    
 $('.paidby2').on('change',function()
 { 
     var thiss=$(this).val();
    // alert(thiss);
     if(thiss=='Client')
     {
         $('.hidepaid2').hide();
     }
     else if(thiss =='FSC')
     {
         $('.hidepaid2').show();
     }
     else
     {
         $('.hidepaid2').hide();
     }
 });
 
 
    $('.annualfees').on('blur',function()
 { 
        var annualfees=$('.annualfees').val();
        var ann=annualfees.replace('$','');
       // alert(annualfees);
        var sign53 =parseFloat(Math.round(ann * 100) / 100).toFixed(2);
	    var annual = sign53.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	    var dollar = '$';
	    var finalyannual=dollar.concat(annual);
        $(".annualfees").val(finalyannual);
});   

$('.penaltycharges').on('blur',function()
{ 
       
        var penaltyfees=$('.penaltycharges').val();
     var pen=penaltyfees.replace('$','');
        
        var sign53 =parseFloat(Math.round(pen * 100) / 100).toFixed(2);
	    var penaltyfess = sign53.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	    var dollar = '$';
	    var finalypenaltyfess=dollar.concat(penaltyfess);
        $(".penaltycharges").val(finalypenaltyfess);
});   


 $('.processingfees').on('blur',function()
 { 
    var processingfees=$('.processingfees').val();
    var proc=processingfees.replace('$','');
     
    var ccc= processingfees.replace('$','');
    var sign53 =parseFloat(Math.round(proc * 100) / 100).toFixed(2);
    var dollar = '$';
	var finalyprocessingfees=dollar.concat(sign53);   
	$(".processingfees").val(finalyprocessingfees);
	
	var annualfees=$('.annualfees').val();
    var aaa= annualfees.replace('$','');
    var penaltycharges=$('.penaltycharges').val();
    var bbb= penaltycharges.replace('$','');
    
    
    
    if(aaa)
    {
       var annfees= aaa;
    }
    else 
    {
        var annfees= '0.00';
    }
    
    if(bbb)
    {
        var penalty= bbb;
    }
    else
    {
        var penalty= '0.00';
    }
    
    if(ccc)
    {
        var processing= ccc;
    }
    else
    {
        var processing= '0.00';
    }
    
    var totalamts=parseFloat(annfees)+parseFloat(processing)+parseFloat(penalty);
        
        var sign54 =parseFloat(Math.round(totalamts * 100) / 100).toFixed(2);
	    var totalsamts = sign54.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	    //alert(totalsamts);
	    var finalytotalsamts=dollar.concat(totalsamts);
        $(".totalamt").val(finalytotalsamts);
});   
  
     
//  Add New Start

$('.annualfees22').on('blur',function()
 { 
        var annualfees=$('.annualfees22').val();
        var ann=annualfees.replace('$','');
        
        var sign53 =parseFloat(Math.round(ann * 100) / 100).toFixed(2);
	    var annual = sign53.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	    var dollar = '$';
	    var finalyannual=dollar.concat(annual);
        $(".annualfees22").val(finalyannual);
});   

$('.penaltycharges22').on('blur',function()
{ 
        var penaltyfees=$('.penaltycharges22').val();
        var pen=penaltyfees.replace('$','');
        
        var sign53 =parseFloat(Math.round(pen * 100) / 100).toFixed(2);
	    var penaltyfess = sign53.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	    var dollar = '$';
	    var finalypenaltyfess=dollar.concat(penaltyfess);
        $(".penaltycharges22").val(finalypenaltyfess);
});   


 $('.processingfees22').on('blur',function()
 { 
    var processingfees=$('.processingfees22').val();
    var ccc= processingfees.replace('$','');
    var sign53 =parseFloat(Math.round(ccc * 100) / 100).toFixed(2);
    var dollar = '$';
	var finalyprocessingfees=dollar.concat(sign53);   
	$(".processingfees22").val(finalyprocessingfees);
	
	var annualfees=$('.annualfees22').val();
    var aaa= annualfees.replace('$','');
    var penaltycharges=$('.penaltycharges22').val();
    var bbb= penaltycharges.replace('$','');
    
    
    
    if(aaa)
    {
       var annfees= aaa;
    }
    else 
    {
        var annfees= '0.00';
    }
    
    if(bbb)
    {
        var penalty= bbb;
    }
    else
    {
        var penalty= '0.00';
    }
    
    if(ccc)
    {
        var processing= ccc;
    }
    else
    {
        var processing= '0.00';
    }
    
    var totalamts=parseFloat(annfees)+parseFloat(processing)+parseFloat(penalty);
        
        var sign54 =parseFloat(Math.round(totalamts * 100) / 100).toFixed(2);
	    var totalsamts = sign54.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	    //alert(totalsamts);
	    var finalytotalsamts=dollar.concat(totalsamts);
        $(".totalamt22").val(finalytotalsamts);
});   
  
//  Add New End     
     
     
     $("#license_gross").on("input", function(evt) {
		var self = $(this);
		self.val(self.val().replace(/[^\d].+/, ""));
		if ((evt.which < 48 || evt.which > 57)) 
		{
			evt.preventDefault();
		}});  

    $("#license_fee").on("input", function(evt) {
		var self = $(this);
		self.val(self.val().replace(/[^\d].+/, ""));
		if ((evt.which < 48 || evt.which > 57)) 
		{
			evt.preventDefault();
		}});  

     
     
  (function($) {
    var minNumber = -100;
    var maxNumber = 100;
      $('.spinner .btn:first-of-type').on('click', function() {
        if ($('.spinner input').val() == maxNumber) {
          return false;
        } else {
          $('.spinner input').val(parseInt($('.spinner input').val(), 10) + 5 + '%');
        }
      });

  $('.txtinput_1').on("blur", function() {
    var inputVal = parseFloat($(this).val().replace('%', '')) || 0
    if (minNumber > inputVal) {
      //inputVal = -100;
    } else if (maxNumber < inputVal) {
      //inputVal = 100;
    }
    $(this).val(inputVal + '.00');
  });

      $('.spinner .btn:last-of-type').on('click', function() {
        if ($('.spinner input').val() == minNumber) {
          return false;
        } else {
          $('.spinner input').val(parseInt($('.spinner input').val(), 10) - 5 + '%');
        }
      });
})(jQuery);

    }); 

</script>


<script>
function getvalYear22()
{
    $('.penaltycharges').val('');
    $('.processingfees').val('');
    $('.totalamt').val('');
    
        var yearboxval = document.getElementById("formation_yearbox22").value;
        var yearvaluesub1 = '<?php $dd=date('Y'); echo $dd-1;?>'; 
        //var yearvaluesub1 = '2018'; 
        <?php
        if(isset($common->id)!='')
        {
            ?>
                var clientid = <?php echo $common->id;?>
            <?php
        }
        ?>
        
        //alert(yearvaluesub1);
        if(yearboxval!='')
        {
            //alert(yearboxval);
            for (var n = 1; n <= yearboxval; ++ n)
            {
                var total = parseFloat(yearvaluesub1)+ parseFloat(n);
                //$("#formation_yearvalue11").val(total);
                $.get('{!!URL::to('getFormationexistdata')!!}?totalid='+total,'clientid='+clientid, function(data)
                {  
                    //console.log(data);exit;
                    if(data>0)
                    {
                        $("#primary3").show();
                        $("#primary4").hide();
                        $("#yeardalreadyExist").show();
                        // $(".yeardalreadyExist1").show();
                        // $(".yeardalreadyExist2").hide();
                    }
                    else
                    {
                        $("#primary3").hide();
                        $("#primary4").show();
                        $("#yeardalreadyExist").hide();
                        // $(".yeardalreadyExist1").hide();
                        // $(".yeardalreadyExist2").show();
                    }
                    
                });
            }
            
            if(yearboxval=='')
            {
                $("#formation_yearvalue11").val(total);
                $("#formation_amount11").val('');
            }
            else if(yearboxval==1)
            {
                yearss1 = '<?php $dd=date('Y'); echo $dd;?>';
                $("#formation_yearvalue11").val(yearss1);
                $("#formation_amount11").val('$50.00');
            }
            else if(yearboxval==2)
            {
                yearss1 = '<?php $dd=date('Y'); echo $dd;?>';
                yearss2 = '<?php $dd=date('Y'); echo $dd+1;?>';
                yearss4 = ',';
	            var finalyyear2=yearss1.concat(yearss4).concat(yearss2);
                $("#formation_yearvalue11").val(finalyyear2);
                $("#formation_amount11").val('$100.00');
            }
            else if(yearboxval==3)
            {
                yearss1 = '<?php $dd=date('Y'); echo $dd;?>';
                yearss2 = '<?php $dd=date('Y'); echo $dd+1;?>';
                yearss3 = '<?php $dd=date('Y'); echo $dd+2;?>';
                yearss4 = ',';
                var finalyyear3=yearss1.concat(yearss4).concat(yearss2).concat(yearss4).concat(yearss3);
                $("#formation_yearvalue11").val(finalyyear3);
                $("#formation_amount11").val('$150.00');
            }
            else if(yearboxval==4)
            {   
                $("#formation_amount11").val('200.00');
            }
        }
        
        var yearvaluesub = document.getElementById("formation_yearvalue_already").value;
        
        
        
        
        if(yearvaluesub!='')
        {
            for (var n = 1; n <= yearboxval; ++ n)
            {
                var total = parseFloat(yearvaluesub)+ parseFloat(n);
                //$("#formation_yearvalue11").val(total);
                // $.get('{!!URL::to('getFormationexistdata')!!}?totalid='+total, function(data)
                $.get('{!!URL::to('getFormationexistdata')!!}?totalid='+total,'clientid='+clientid, function(data)
                {  
                    //console.log(data);exit;
                    if(data>0)
                    {
                        $("#yeardalreadyExist").show();
                        $(".yeardalreadyExist1").show();
                        $(".yeardalreadyExist2").hide();
                    }
                    else
                    {
                        $("#yeardalreadyExist").hide();
                        $(".yeardalreadyExist1").hide();
                        $(".yeardalreadyExist2").show();
                    }
                    
                });
            }
        }
        else
        {
            for (var n = 1; n <= yearboxval; ++ n)
            {
                var total = parseFloat(yearvaluesub1)+ parseFloat(n);
                //$("#formation_yearvalue11").val(total);
            }
        }
        
        
        
        if(yearboxval=='')
            {
                $("#formation_yearvalue11").val(total);
                $("#formation_amount11").val('');
            }
            else if(yearboxval==1)
            {
                yearss1 = '<?php $dd=date('Y'); echo $dd;?>';
                $("#formation_yearvalue11").val(yearss1);
                $("#formation_amount11").val('$50.00');
            }
            else if(yearboxval==2)
            {
                yearss1 = '<?php $dd=date('Y'); echo $dd;?>';
                yearss2 = '<?php $dd=date('Y'); echo $dd+1;?>';
                yearss4 = ',';
	            var finalyyear2=yearss1.concat(yearss4).concat(yearss2);
                $("#formation_yearvalue11").val(finalyyear2);
                $("#formation_amount11").val('$100.00');
            }
            else if(yearboxval==3)
            {
                yearss1 = '<?php $dd=date('Y'); echo $dd;?>';
                yearss2 = '<?php $dd=date('Y'); echo $dd+1;?>';
                yearss3 = '<?php $dd=date('Y'); echo $dd+2;?>';
                yearss4 = ',';
                var finalyyear3=yearss1.concat(yearss4).concat(yearss2).concat(yearss4).concat(yearss3);
                $("#formation_yearvalue11").val(finalyyear3);
                $("#formation_amount11").val('$150.00');
            }
            else if(yearboxval==4)
            {   
                $("#formation_amount11").val('200.00');
            }      
       
}
    
    

    
// function getvalYear33()
// {
//         var yearboxval = document.getElementById("formation_yearbox33").value;
//         var yearvaluesub = document.getElementById("formation_yearvalue_already").value;
        <?php
        if(isset($common->id)!='')
        {
            ?>
                var clientid = <?php echo $common->id;?>
            <?php
        }
        ?>
//         //alert(yearboxval);
        
//         if(yearboxval=='')
//         {
//           var yearv=$("#formation_yearvalue12").val('');
          
//         }
//         else if(yearboxval==1)
//         {
//             var yearv='2019';
//             $("#formation_yearvalue12").val('2019');
//             $("#formation_amount12").val('50.00');
            
//         }
//         else if(yearboxval==2)
//         {
//             var yearv='2020';
//             $("#formation_yearvalue12").val('2020');
//             $("#formation_amount12").val('100.00');
            
//         }
//         else if(yearboxval==3)
//         {
//             var yearv='2021';
//             var yearv=$("#formation_yearvalue12").val('2021');
//             $("#formation_amount12").val('150.00');
            
//         }
//         else if(yearboxval==4)
//         {
//             var yearv='2022';
//             $("#formation_yearvalue12").val('2022');
//             $("#formation_amount12").val('200.00');
            
//         }      

//         //console.log(yearv);exit;
//         if(yearv!='')
//         {
            
//             //$.get('{!!URL::to('getFormationexistdata')!!}?totalid='+yearv, function(data)
//             $.get('{!!URL::to('getFormationexistdata')!!}?totalid='+yearv,'clientid='+clientid, function(data)
//             {  
//                 //console.log(data);exit;
//                 if(data>0)
//                 {
//                     $("#yeardalreadyExist").show();
//                     $("#primary3").show();
//                     $("#primary4").hide();
                    
//                 }
//                 else
//                 {
//                     $("#yeardalreadyExist").hide();
//                     $("#primary3").hide();
//                     $("#primary4").show();
//                 }
                
//             });
           
//         }
// }







</script>


<script>
$(document).ready(function()
{

    $('#country_name').keyup(function()
    { 
        var query = $(this).val();
        if(query != '')
        { 
         var _token = $('input[name="_token"]').val();
         $.ajax({
          url:"{{ route('workstatus.fetch') }}",
          method:"POST",
          data:{query:query, _token:_token},
          success:function(data){
           $('#countryList').fadeIn();  
                    $('#countryList').html(data);
          }
         });
        }
    });

    $(document).on('click', 'tr', function(){  
        $('#country_name').val($(this).text());  
        $('#countryList').fadeOut();  
    });  

});

</script>


@endsection()