@extends('fscemployee.layouts.app')
@section('title', 'View AddressBook')
@section('main-content')
<div class="content-wrapper">

	<div class="page-title">
		<h1>Address Book </h1>
	</div>
<section class="content" style="background-color: #fff;">
    </br>
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-body">
					<form method="post" action="#" class="form-horizontal" id="businessname" name="businessname" enctype="multipart/form-data">
				<div class="form-group ">
							<label class="control-label col-md-3">Type :</label>
							<div class="col-md-4">
								<input name="controlname" type="text" id=""  class="form-control" readonly value="@if($gold->type=='Approval') Client @else {{ucwords($gold->type)}} @endif">
							</div>
						</div>
						<div class="form-group ">
							<label class="control-label col-md-3">Full Name :</label>
							<div class="col-md-4">
								<input name="controlname" type="text" id=""  class="form-control" readonly value="{{ucwords($gold->firstName).' '.ucwords($gold->middleName).' '.ucwords($gold->lastName)}}">
							</div>
						</div>
						<div class="form-group ">
							<label class="control-label col-md-3">Address :</label>
							<div class="col-md-4">
								<input name="controlname" type="text" id=""  class="form-control" readonly value="{{ucwords($gold->address1)}}">
							</div>
						</div>
						<div class="form-group ">
							<label class="control-label col-md-3">Country / State  / City :</label>
							<div class="col-md-1">
                                <input type="text" class="form-control" id="city" name="countryId" readonly placeholder="Country" value="{{$gold->countryId}}">
							</div>
							<div class="col-md-1">
                                <input type="text" class="form-control" id="city" name="countryId"  readonly placeholder="State" value="{{$gold->stateId}}">
							</div>
							<div class="col-md-2">
                                <input type="text" class="form-control" id="city" name="countryId"  readonly placeholder="City" value="{{$gold->city}}">
							</div>
						</div>
						<div class="form-group ">
							<label class="control-label col-md-3">Email :</label>
							<div class="col-md-4">
							    <input type="text" class="form-control" id="" name="company_email"  readonly value="{{$gold->email}}">
							</div>
						</div>
						<div class="form-group ">
							<label class="control-label col-md-3">Phone :</label>
							<div class="col-md-2">
							    <input name="telephone" type="tel" id="" value="{{$gold->telephoneNo1}}"  readonly class="form-control" placeholder="(000)000-0000">
					        </div>
							<!--<div class="col-md-2">
    							<select name="telephoneNo1Type" id="telephoneNo1Type" class="form-control fsc-input" style="height:auto">
                                    <option value="Office" selected="">Office</option><option value="Mobile">Mobile</option>
                                </select>
		                    </div>
							<div class="col-md-2">
							    <input class="form-control fsc-input" id="ext1" maxlength="5" name="ext1" value="" placeholder="Ext" type="text">
							</div>-->
						</div>
						
						<div class="card-footer">
							<div class="row">
								<div class="col-md-8 col-md-offset-3">
									<a href="{{url('/fscemployee/addressbook-fscemployee')}}" class="btn btn-primary icon-btn">Back</a>
								</div>
							</div>
						</div>
						
					</form>
				</div>
			</div>
		</div>
	</div>
	</section>
</div>

@endsection()