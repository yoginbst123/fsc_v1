<!DOCTYPE html>
<html lang="lang="{{ app()->getLocale() }}">
<head>
@include('fscemployee.layouts.head')	
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
@include('fscemployee.layouts.header')
    @section('main-content')
    @show
@include('fscemployee.layouts.leftsidebar')
@include('fscemployee.layouts.footer')
</div>
</body>
</html>
