<!DOCTYPE html>
<html lang="lang="{{ app()->getLocale() }}">
<head>
@include('admin.layouts.head')	
</head>
<body class="sidebar-mini fixed">
<div class="wrapper">
@include('admin.layouts.header')
    @section('main-content')
    @show
@include('admin.layouts.leftsidebar')

</div>
</body>
</html>