@extends('fscemployee.layouts.app')
@section('title', 'Technical Support')
@section('main-content')
<div class="content-wrapper">
	<div class="page-title">
		<h1>Technical Support</h1>
	</div>
		<section class="content" style="background-color: #fff;">
<div class="">
		<div class="col-md-12">
			<div class="card">
				<div class="card-body">
                   
					<form method="post" action="{{route('clientsupports.store')}}" class="form-horizontal" id="homecontent" name="homecontent" enctype="multipart/form-data">
					{{csrf_field()}}
							
							 <div class="form-group">
                   <label class="control-label col-md-3">Date / Day / Time:</label>
                     <div class="col-md-2" style="width: 130px;">
                        <div class="">
                           <input type="text" name="date" id="date" class="form-control" value="" placeholder="Date">
                        </div>
                     </div>
                      
                     <div class="col-md-2" style="width: 130px;">
                        <div class="">
                           <input type="text" name="day" id="day" class="form-control" placeholder="Day" value="">
                        </div>
                     </div>
                     
                     <div class="col-md-2" style="width: 115px;">
                        <div class="">
                           <input type="text" name="time" id="time" class="form-control" value="{{date("H:i a")}}" placeholder="Time">
                        </div>
                       
                     </div>
                  </div>
							<div class="form-group{{ $errors->has('to') ? ' has-error' : '' }}">
							<label class="control-label col-md-3">To :</label>
							<div class="col-md-8">
								<!--<input name="to" type="text" id="to" value="Mahendra Singh Baghel" readonly class="form-control">-->
                                            <select class="form-control"id="to" name="to">
                                                <option value="{{$tech->id}}">{{$tech->name}}</option>
                                            </select>
								@if ($errors->has('to'))
										<span class="help-block">
											<strong>{{ $errors->first('to') }}</strong>
										</span>
									@endif							
							</div>
						</div>
						
						<div class="form-group{{ $errors->has('subject') ? ' has-error' : '' }}">
							<label class="control-label col-md-3">Subject :</label>
							<div class="col-md-8">
								<input name="subject" type="text" id="subject" class="form-control">

								@if ($errors->has('subject'))
										<span class="help-block">
											<strong>{{ $errors->first('subject') }}</strong>
										</span>
									@endif							
							</div>
						</div>
						<div class="form-group{{ $errors->has('details') ? ' has-error' : '' }}">
						<label class="control-label col-md-3">Details :</label>
						<div class="col-md-8">
						<div class="">
							  <textarea id="editor1" name="details" rows="10" cols="80"></textarea>
					  </div>
							@if ($errors->has('details'))
									<span class="help-block">
										<strong>{{ $errors->first('details') }}</strong>
									</span>
								@endif	
						</div>
					</div>
						<div class="form-group">
						<label class="control-label col-md-3">Attachment :</label>
						<div class="col-md-8">
						<div class="">
							 <label class="file-upload btn btn-primary">
                Browse for file ... <input type="file" class="form-control fsc-input" style="opacity:0" id="attachment" name="attachment" placeholder="Select Document">
            </label>
					  </div>
								
						</div>
					</div>
				 <div class="card-footer">
												<div class="col-md-offset-3 col-md-7">
													<div class="row">
														<div class="col-md-3">
															<input class="btn_new_save btn-primary1 primary1" type="submit" name="submit" value="Save">
														</div>
														<div class="col-md-3">
															<a class="btn_new_cancel" href="{{url('fscemployee/clientsupports')}}">Cancel</a> 
														</div>
															<div class="col-md-3">
														
														</div>
													</div>
												</div>
											</div>
						
					</form>
				</div>
			</div>
		</div>
	</div>
	</section>
</div>
<script>
    $(document).ready(function(){
       $('#type').on('change', function(){
         if($('#type').val()=='Resposibilty')
         {
             $('.emp').show();
         }
         else
         {
            $('.emp').hide();  
         }
         
       });
    });
    
</script>
<style>
    input[type="file"] {
    display: block;
    position: absolute;
}
</style><script type="text/javascript">
  $(document).ready(function() {
    $("#date").change(function() {
      var startdate= $("#date").val();
      var monthNames = [
        "Jan", "Feb", "Mar",
        "Apr", "May", "Jun", "Jul",
        "Aug", "Sep", "Oct",
        "Nov", "Dec"
      ];
    var weekday = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
      var durtion=$('#duration').val();
      var  date = new Date(startdate);
      var day = weekday[date.getDay()];
      var monthly=30;
      var weekly=7;
      var bimonthly=15;
      var biweekly=14;
      var monthss=monthNames[(date.getMonth())];
      var yearss=date.getFullYear();
      var yyy=yearss % 4 ;
      //if(yyy)
      //{
      //alert('true');
      //}
      //else
      //{
      //alert('false');
      //}
      if(durtion == "Weekly")
      {
        var totaldays=6;
      }
      else if(durtion == "Monthly")
      {
        if(monthss == 'Jan' || monthss == 'Mar' || monthss == 'May' || monthss == 'Jul' || monthss == 'Aug' || monthss == 'Oct' || monthss == 'Dec')
        {
          var totaldays=30;
        }
        else if(monthss == 'Feb')
        {
          //if(years / 4 = 0)
          if(yyy ==0)
          {
            var totaldays=28;
          }
          else if(yyy ==1)
          {
            var totaldays=27;
          }
        }
        else if(monthss == 'Apr' || monthss == 'Jun' || monthss == 'Sep' || monthss == 'Nov' )
        {
          var totaldays=29;
        }
      }
      else if(durtion == "Bi-Weekly")
      {
        var totaldays=13;
      }
      else if(durtion == "Bi-Monthly")
      {
        var totaldays=14;
      }
    // var vv = day + totaldays;
      
    date.setDate(date.getDate() + totaldays);// alert(vv);
      var date1 = ("0" + (date.getMonth() + 1)).slice(-2)  + "/" + ("0" + date.getDate()).slice(-2) + "/" + date.getFullYear();// alert(date.getDate())
     // alert(date1);
      var newdate = new Date(date1);
        var day1 = weekday[newdate.getDay()];
       //alert(newdate);
      var date2=monthNames[(date.getMonth())] + "/" + date.getDate() + "/" + date.getFullYear() ;
     // $('#sch_end_date').val(date1);
      $('#day').val(day);
      $('#sch_end_day').val(day1);
      //document.write(date2);
    });
    $("#duration").change(function() {
      $('#sch_end_date').val('');
      $('#sch_start_date').val('');
    });
  });
   $( "#date" ).datepicker({
    'dateFormat':'yy-mm-dd',
    onSelect: function(dateText){ alert();
        var seldate = $(this).datepicker('getDate');
        seldate = seldate.toDateString();
        seldate = seldate.split(' ');
        var weekday=new Array();
            weekday['Mon']="Monday";
            weekday['Tue']="Tuesday";
            weekday['Wed']="Wednesday";
            weekday['Thu']="Thursday";
            weekday['Fri']="Friday";
            weekday['Sat']="Saturday";
            weekday['Sun']="Sunday";
        var dayOfWeek = weekday[seldate[0]];
        $('#day').val(dayOfWeek);
    }
}); 
</script>
@endsection()