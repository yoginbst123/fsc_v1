@extends('fscemployee.layouts.app')
@section('main-content')
<style>
    .search-btn {
    position: absolute;
    top: 46px;
    right: 16px;
    background:transparent;
    border:transparent;
}
.panel-primary{
    border-color:#ffffff !important;
}

/**************** dropdown list ********************/    
    
#countryList{margin:40px 0px 0px 0px;
    padding:0px 0px;
    border: 0px solid #ccc;
    max-height: 200px;
    overflow: auto; position: absolute;
    width: 360px;
    z-index: 99999;}

#countryList li {
    border-bottom: 1px solid #025b90;
    background: #ef7c30;
}
#countryList li a{padding:0px !important; display:block; color:#fff!important; height:40px; margin-left:0px !important;}
#countryList li a span.clientalign{display: flex;
    width: 100px;
    float: left;
    line-height: 15px;
    padding: 4px 0px;
    flex-direction: row;
    justify-content: flex-start;
    align-items: center;
    align-content: center;
    min-height: 40px; background:#038ee0;
    margin-left: 0;
    padding-left: 5px;}
    
    #countryList li a span.entityname{display: flex;
    width:230px;
    float: left;
    line-height: 15px;
    padding: 4px 0px; font-size:13px;
    flex-direction: row;
    justify-content: flex-start;
    align-items: center;
    align-content: center;
    min-height: 38px;
    margin-left: 10px;}




.new_images_sec .col-md-4{position:relative;}
.new_images_sec .col-md-4 .arrow{position:absolute; top:10px; right:-6px;}
.new_images_sec .col-md-4{position:relative;}
.new_images_sec .col-md-4 img{height:53px!important;}
.new_images_sec .col-md-4 .arrow{position:absolute; top:10px; right:-6px;}
.new_images_sec .col-md-4.lastimgbox .arrow{position:absolute; top:10px; left:-6px;}
.searchboxmain{float: left;  display: FLEX;  margin-top: 3px; justify-content: space-between;position:absolute;}
.searchboxmain .form-control{margin-right:10px!important;}
.clear{clear:both;}
/*.page-title h1{float:left; margin-left:15%;}*/
.page-title{padding:5px 15px 0px!important;}
.nav.nav-tabs li{width:10.6%!important;border: 1px solid #d4d4d4;}
.clear{clear:both;}
.mt25{margin-top:25px;}
.text-center{text-align:center!important;}
.text-right{text-align:right!important;}
.table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th{padding:6px 8px 8px 8px!important;}
.pointernone{pointer-events:none}
.pad20{padding:20px;}.content-header
.nav-tabs > li.active > a.yel, .nav-tabs > li.active > a.yel:focus, .nav-tabs > li.active > a.yel:hover {
       cursor:default;
}
.nav-tabs > li{
    margin: 0px 0 0 4px;
}
.nav > li > a.yel:hover, .nav > li > a.yel:active, .nav > li > a.yel:focus {
    border-color:#000 !important;
    color:#000 !important;
    background:#ffff99;
    border-radius: 5px !important;
}
.nav-tabs {
    padding: 12px;
    border: 1px solid #3598dc !important;
}
.btnaddmore{background: #337ab7; display:inline-block; margin-bottom:5px;
    padding: 6px 10px;
    color: #fff;
    border-radius: 4px}
.btnremove{background: #ff0000;
    padding: 6px 10px;
    color: #fff;
    border-radius: 4px}
.padzero{padding:0px!important;}
.officermainbox{border:1px solid #ccc; padding:20px; margin-bottom:30px;}
.table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
    padding: 6px 8px 8px 8px !important;
}
.table-bordered, .table-bordered > tbody > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > td, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > thead > tr > th
{border-color:#ccc!important;}
.custom-file-upload {
  border: 1px solid #ccc;
  display: inline-block;
  padding: 6px 12px;
  cursor: pointer;
}
.card ul li{width:24% !important;}
   .card ul.test li a{background:#ffcc66 !important;}
   .card ul.test li.active a{background:#00a0e3 !important;}
   .card ul li a{display: block;width: 100%;color: #333;text-transform: capitalize;background: linear-gradient(180deg, #fdff9a 30%, #e3e449 70%);border: 1px solid #979800 !important;}
   .card ul li a:hover{color: #333;
   background: linear-gradient(180deg, #fbff37 30%, #d3d40e 70%);border: 1px solid #333 !important;}
   .card ul li.active{color: #333;background: linear-gradient(180deg, #fbff37 30%, #d3d40e 70%);border: 1px solid #333 !important;}
   .card ul li a.active, .card ul li a:hover, .card ul li.active a:hover{color: #fff!important;background: #12186b!important;border: 1px solid #12186b !important;}
   .card .nav-tabs{border:0px!important;}
.feeschargesbox .form-control{text-align:right;}
.hrdivider{border-bottom: 2px solid #ccc!important; width: 100%; height: 1px; margin-top: 33px; }
.hrdivider2{margin-top: 14px;   margin-bottom: 30px; border-bottom: 2px solid #ccc!important; width: 100%; height: 1px;}
.officerchange .form-control{background:#fff!important;}
.add-row{background: #007bff; padding: 3px 5px;  color: #fff;  border-radius: 3px; cursor: pointer;}
.delete-row{background:#dc3545; padding: 3px 5px;  color: #fff;  border-radius: 3px; cursor: pointer;}
.nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover {
    color: #FFF !important;
    border: 1px solid #12186b!important;
    background: #12186b!important;
    cursor: default;
    border-radius: 8px;
}

#renewRecord .form-control{margin-bottom:15px;}
#renewRecord  label.text-right{text-align:right!important; width:100%;}
.new_company_name p{font-size:13px!important;}

.nav>li>a {
    position: relative;
    display: block;
    padding: 10px 5px;
    color: #000 !important;
}
input[type="file"]{position: relative!important; margin-top:11px;
    width: 100px!important;
    font-size: 12px!important;
    opacity: 1!important;}

.searchboxmain .form-control{height:34px!important;}
.searchboxmain .btn-action{background: #367fa9 !important;
    padding: 8px 15px!important;
    margin-top: 0px;}
    .btnview{background:#5fb114!important;}
    .btnaddrecord {
    width: 120px;
    float: right;
    cursor: pointer;
}
.panel.panel-default .panel-heading h4 {
    font-size: 20px!important;
    padding: 10px!important;
    background: #b3e4a6!important;
}
.panel-title {
    padding: 20px;
    background: #5e77de;
    color: #fff;
    width: 100%;
    float: left;
    margin: 0 0 4px 0;
}
.panel.panel-default .panel-heading h4 a:hover, .panel.panel-default .panel-heading h4 a {
    color: #103b68!important;
}
.panel-title a.collapsed .glyphicon-plus {
    display: block;
}
.panel-title a .glyphicon-plus {
    display: none;
}
.glyphicon {
    position: relative;
    top: 1px;
    display: inline-block;
    font-family: 'Glyphicons Halflings';
    font-style: normal;
    font-weight: 400;
    line-height: 1;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
}
.panel-title a.collapsed .glyphicon-minus {
    display: none;
}
.panel-title a .glyphicon-minus {
    display: block;
}
.ac_name_first {
    width: 97%!important;
}
.ac_name_first {
    float: left;
  
}
.modal-header{background:#f5efa8!important; border-bottom:2px solid #1b5bab;}
.modal-body label{text-align:right;}
</style>
<div class="content-wrapper">
   
	<div class="page-title">
	    <div class="searchboxmain">
	         <input type="text" name="search" id="country_name" class="form-control" placeholder="Search Client">	 
                 {{ csrf_field() }}
                 <ul id="countryList"></ul>
                  <a class="btn-action btn-view-edit btn-primary" style="background:#367fa9 !important; padding:10px 20px;" href="https://financialservicecenter.net/fscemployee/workstatus">Reset</a>
	        
	   </div>
		<h1 style="margin-top:8px;">Work Status</h1>
		<div class="clear"></div>
	</div>

	<div class="row"> 
<div class="col-md-12">
			<div class="box box-success">
		
             @if(!empty($common->filename))
      <section class="content-header">
      <div class="new-page-title" style="padding:0px;margin: -7px 0 7px 0;">
         <div class="new-page-title-tab">
            <div class="col-md-2 col-sm-2 col-xs-12">
               <div class="new_client_id">
                  <p>{{$common->filename}} </p>
               </div>
            </div>
            @if($common->business_id=='6')
            <div class="col-md-5 col-sm-5 col-xs-12">
            
        <div class="new_company_name" style="margin-top:25px !important;">
            <label> Name :</label> {{ucwords($common->nametype)}}   {{$common->first_name}} {{$common->middle_name}} {{$common->last_name}}  </div></div>
          @else
         
            <div class="col-md-5 col-sm-5 col-xs-12">
               <div class="new_company_name">
                  <label>Company Name :</label>
                  <p>{{$common->company_name}}</p>
               </div>
               <div class="new_company_name">
                  <label>Business Name :</label>
                  <p>{{$common->business_name}}</p>
               </div>
            </div>
            @endif
            <div class="col-md-5 col-sm-5 col-xs-12">
               <div class="new_images_sec">
                  @if(empty($common->business_cat_id)) 
                  <div class="col-md-12 col-sm-12 col-xs-12">
                     @else 
                     @if(empty($common->business_brand_category_id))
                     <div class="col-md-4 col-sm-12 col-xs-12">
                        @else 
                        <div class="col-md-4 col-sm-12 col-xs-12"> 
                           @endif
                           @endif
                           @foreach($business as $busi) 
                           @if($busi->id==$common->business_id)
                           <img src="{{url('public/frontcss/images/')}}/{{$busi->newimage}}" class="img-responsive" style="height:53px!important;">
                          <!--  <div class="arrow"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></div>-->
                        </div>
                        @endif
                        @endforeach
                        @foreach($category as $cate)                                        
                        @if($common->business_cat_id==$cate->id)
                        @if(empty($common->business_brand_category_id))
                       
                        <div class="col-md-4 col-sm-12 col-xs-12">
                           @else 
                         <!--  <div class="arrow">
                              <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                           </div>-->
                           <div class="col-md-4 col-sm-12 col-xs-12"> 
                              @endif
                              <img src="{{url('public/category')}}/{{$cate->business_cat_image}}" alt="" class="img-responsive" />
                           </div>
                           @endif
                           @endforeach
                           @foreach($cb as $bb1)                                     
                           @if($common->business_brand_category_id == $bb1->id)
                           
                           <div class="col-md-4 col-sm-12 col-xs-12 lastimgbox">
                               <div class="arrow">
                              <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                           </div>
                              <img src="{{url('public/businessbrandcategory')}}/{{$bb1->business_brand_category_image}}" alt="" class="img-responsive" />
                           </div>
                           @endif
                           @endforeach
                        </div>
                     </div>
                  </div>
                  </div>
   </section>
    @else

	@endif
				<div class="col-md-12">
				   
				
					@if(!empty($common->filename))
				 <div class="panel with-nav-tabs panel-primary">
   <div class="panel-heading">
   <ul class="nav nav-tabs" id="myTab" style="padding:3px;">
        <li><a href="#tab10primary" data-toggle="tab">Accounting</a></li>
        @if($common->business_id !='6')
        <li class="active"><a href="#tab1primary" data-toggle="tab">Corporation</a></li>
        <li><a href="#tab2primary" data-toggle="tab">License</a></li>
        @endif
        <li><a href="#tab3primary" data-toggle="tab">Taxation</a></li>
        <li><a href="#tab4primary" data-toggle="tab">Banking</a></li>
        <li><a href="#tab5primary" data-toggle="tab">Income</a></li>
        <li><a href="#tab6primary" data-toggle="tab">Expense </a></li>
        <li><a href="#tab60primary" data-toggle="tab">Asset </a></li>
        <li <?php if($common->business_id =='6') { ?>class="active" <?php } ?>><a href="#tab7primary" data-toggle="tab">Others </a></li>
   </ul>
   </div>
    <div class="tab-content">
           <div class="tab-pane <?php if($common->business_id =='6') { ?>fade in active<?php } ?>" id="tab7primary">
                
                <div class="form-group col-md-12  card" style="background: #e0f1fd !important; border: 1px solid #3598dc !important; margin-right:5px; margin-left:0px;margin-bottom:0px;">
                    <div class="panel-heading">
                        <ul class="nav nav-tabs" id="myTab2" style="padding:5px !important;">
                              <li style=" width: 13.2% !important;" class="active"><a href="#subtab05primaryi" class="" data-toggle="tab" aria-expanded="true">Message</a></li>
                            
                              <li style=" width: 13.2% !important;"><a href="#subtab06primaryi" class="" data-toggle="tab" aria-expanded="true">Conversation</a></li>
                               <li style=" width:15% !important;"><a href="#subtab08primaryi" class="" data-toggle="tab" aria-expanded="true">Task</a></li>
                                                     
                              <li style=" width:13% !important;"><a href="#subtab07primaryi" class="" data-toggle="tab" aria-expanded="true">Notes</a></li>
                              <li style=" width:15% !important;"><a href="#subtab09primaryi" class="" data-toggle="tab" aria-expanded="true">tab5</a></li>
                             
                        </ul>
                    </div>
                </div>
                
                <div class="tab-content">
                    <div class="tab-pane fade in active  newcheckbox" id="subtab05primaryi">
                    <div class="card-body" >
    	                <div class="row"> 
    	                <div class="col-md-12"> 
    	                    <table class="table table-hover table-bordered dataTable no-footer">
					           <thead>
					               <tr>
					                   <th>Filing Type</th>
					                   <th>Filing Year</th>
					                    <th>Filing Method</th>
					                   <th>Date</th>
					                     <th >Filing Software</th>
					                   <th >State</th>
					                   <th>Date Of Filing</th>
					                   <th>Action</th>
					                  </tr>
					           </thead>
					           <tbody>
					               <?php
					               if(!empty($clientropersonaltax))
					               {
					               foreach($clientropersonaltax as $personaltax)
					               {
					                   ?>
					               <tr>
					                   <td class="text-center"> <?php echo $personaltax->filing_type;?></td>
					                   <td class="text-center"> <?php echo $personaltax->filing_year;?></td>
					                   <td class="text-center"> <?php echo $personaltax->filing_method;?></td>
					                   <td class="text-center"> <?php echo $personaltax->filing_date;?></td>
					                   <td class="text-center"> <?php echo $personaltax->filing_software;?></td>
					                   <td class="text-center"> <?php echo $personaltax->filing_state;?></td>
					                   <td class="text-center"> <?php echo $personaltax->date_of_filing;?></td>
					                   <td class="text-center">
					                        <a class="btn-action btn-view-edit"><i class="fa fa-edit"></i></a>
                                            <a class="btn-action btn-delete" href="#"><i class="fa fa-trash"></i></a>
					                   </td>
					               </tr>
					               <?php
					               }
					               }
					               else
					               {
					               ?>
					               <tr><td style="text-align:center;" colspan="8">No records found</td></tr>
					               <?php
					               }
					               ?>
					            </tbody>
					       </table>
                        </div>
                        </div>
        		    </div>
        		 
        		</div>
        		  <div class="tab-pane fade newcheckbox" id="subtab08primaryi">
    	                       <br>
    	                    <div class="col-md-12 col-sm-12 col-xs-12">
    	                        <br>
    							<div class="Branch" style="text-align:left; padding-left:15px;">
    								<h1 class="text-center">Task List</h1>
    							</div>
    						</div>
            				<br>
                                
                            <div class="col-md-12 col-sm-12 col-xs-12 text-right">
    						       
    						                        <div class="clear"></div>
    						                       <div class="table-responsive">
						<table class="table table-hover table-bordered" id="sampleTable2">
							<thead>
								<tr>
									<th style="width:10%">Task Date</th>
									<th>Created By</th>
									<th>Assign To</th>
									<th>Subject</th>
								    <th>Task Status</th>
                                    <th>Action</th>
								</tr>
							</thead>
							<tbody>
							    @foreach($taskall as $com)
							    <?php //echo $com->created_at;
							    ?>
								<tr>
								    
									<td style="width:10%">{{date('M-d-Y',strtotime($com->created_at))}}</td>
									<td style="width:15%">@foreach($admin as $com1) @if($com->admin_id==$com1->id) {{ucwords($com1->name)}} @endif @endforeach</td>
									<td style="width:15%">@foreach($set1 as $com1) @if($com->employeeid==$com1->id) {{ucwords($com1->firstName)}} {{ucwords($com1->middleName)}} {{ucwords($com1->lastName)}} @endif @endforeach</td>
								
									<td style="width:15%">{{$com->title}}</td>
								    <td>@if($com->status==2) In Progress @endif @if($com->status==0) Wait @endif  @if($com->status==1) Start @endif @if($com->status==3) End @endif</td>
                                    <td><a class="btn-action btn-view-edit" href="{{route('tasks.edit',$com->id)}}"><i class="fa fa-edit"></i></a></td>
								</tr>
								@endforeach
							</tbody>
						</table>
            </div>
    						                    </div>
    	                
    	                </div>
    	              
        		    <div class="tab-pane fade newcheckbox" id="subtab06primaryi">
                    </br>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        </br>
    					<div class="Branch" style="text-align:left; padding-left:15px;">
    						<h1 class="text-center">Conversation List</h1>
    					</div>
    				</div>
    				<br>
                    
                        
                    <div class="col-md-12 col-sm-12 col-xs-12 text-right">
    						       
    						                        <div class="clear"></div>
    						                         <table class="table table-hover table-bordered dataTable no-footer">
                    						           <thead>
                    						               <tr>
                    						                    <th>No.</th>
                            								    
                            									<th> Date</br> Day</br> Time</th>
                            								
                            									<th>Related To</th>
                            									<th>Description</th>
                            									<th>Notes</th>
                            									<th>Action</th>
                    						               </tr>
                    						           </thead>
                    						           	<?php 
                    						           	$cnt=count($conversation);
                    						           	if($cnt>0)
                    						           	{
                    						           	    $cnt=0;
                    						           	?>
                    						           @foreach($conversation as $conversation)
                    						           
                    						           
                    								    <tr>
                    								        <td style="text-align:center;">{{$loop->index+1}} </td>
                    								        <td style="text-align:center;">{{$conversation->creattiondate}}<br> {{$conversation->day}} <br> {{$conversation->time}}</td>
                    								        <td style="text-align:center;">{{$conversation->relatednames}} </td>
                    								        <td>{{$conversation->condescription}} </td>
                                                            <td>{{$conversation->connotes}} </td>
                                                            <td style="text-align:center;">
    									                        <a class="btn-action btn-view-edit conversationID" data-id="{{$conversation->id}}"><i class="fa fa-edit"></i></a>			    
    				                                            
    									                   </td>
    									                
    									                
                    						            @endforeach
                    						            
                    						            </tr>
                    						           <?php
                    						           	}
                    						           	else
                    						           	{
                    						           ?>
                    						            <tr><td style="text-align:center;" colspan="7">No records found</td></tr>
                    						            <?php
                    						           	}
                    						            ?>
                    						            </tbody>
                    						           
                    						       </table>
    						                    </div>
                        
                    
                </div>
                
                    <div class="tab-pane fade newcheckbox" id="subtab07primaryi">
                    </br>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        </br>
    					<div class="Branch" style="text-align:left; padding-left:15px;">
    						<h1 class="text-center">Notes List</h1>
    					</div>
    				</div>
    				<br>
    							
    				<div class="col-md-12 col-sm-12 col-xs-12 text-right">
    						       
    						                        <div class="clear"></div>
    						                         <table class="table table-hover table-bordered dataTable no-footer">
                    						           <thead>
                    						               <tr>
                    						                    <th>No.</th>
                            								    
                            									<th> Date</br> Day</br> Time</th>
                            								
                            									<th>Related To</th>
                            									<th>Description</th>
                            									<th>Notes</th>
                            									<th>Action</th>
                    						               </tr>
                    						           </thead>
                    						           <?php 
                    						           	$cnt=count($notesdata);
                    						           	if($cnt>0)
                    						           	{
                    						           	    $cnt=0;
                    						           	?>
                    						           @foreach($notesdata as $conversation)
                    								    <tr>
                    								        <td style="text-align:center;">{{$loop->index+1}} </td>
                    								        <td style="text-align:center;">{{$conversation->creattiondate}}<br> {{$conversation->noteday}} <br> {{$conversation->notetime}}</td>
                    								        <td style="text-align:center;">{{$conversation->notesrelated}} </td>
                    								        <td>{{$conversation->notesrelatedcat}} </td>
                                                            <td>{{$conversation->notes}} </td>
                                                            <td style="text-align:center;">
    									                        
    				                                             <a class="btn-action btn-view-edit notesID" data-id="{{$conversation->id}}"><i class="fa fa-edit"></i></a>
    									                   </td>
                    						            @endforeach
                    						            </tr>
                    						            <?php
                    						           	}
                    						           	else
                    						           	{
                    						           ?>
                    						            <tr><td style="text-align:center;" colspan="7">No records found</td></tr>
                    						            <?php
                    						           	}
                    						            ?>
                    						            </tbody>
                    						           
                    						       </table>
    						                    </div>
    		   </div>
    		    </div>
            </div>
      @if($common->business_id !='6')  
       <div class="tab-pane fade in active disablebox newcheckbox" id="tab1primary">
         <div class="col-md-12" style="padding-left:5px; margin-top:5px;">
               
                                        <div class="form-group row  card" style="background: #e0f1fd !important; border: 1px solid #3598dc !important; margin-right:5px; margin-left:0px;margin-bottom:0px;">
                                            <div class="col-md-4" style="padding-top:10px; padding-right:0px; width:30%;">
                      <label class="col-md-1 text-right padtop7" style="width: 60px;padding-left: 0px; margin-top:10px;">State :</label> <div class="col-md-3" style="width: 55px;padding-left: 0px;padding-right: 0px;"><input type="text" value="GA" style="width:50px;" class="form-control fsc-input" readonly=""></div>
                  <label class="col-md-2 text-right padtop7" style="width: 85px;padding-left: 0px;margin-top:10px;">Control #:</label> <div class="col-md-3" style="width: 100px;padding-left: 0px;padding-right: 0px;"><input type="text" value="K706121" class="form-control fsc-input" style="width:100px;" readonly=""></div>
                  </div>
                                                <div class="col-md-8" style="width: 70%; padding-right:0px;">
                                                    <div class="panel-heading">
                                                        <ul class="nav nav-tabs" id="myTab2" style="padding:5px !important;float:right;">
                                                            <!--<li style=" width: 30.2% !important;" class="active"><a href="#subtab1primary" class="" data-toggle="tab">Corporation Renewal</a></li>-->
                                                            <!--<li style=" width: 19.2% !important;"><a href="#subtab2primary" class="" data-toggle="tab">Share Ledger</a></li>-->
                                                            <!--<li style=" width:17.2% !important;"><a href="#subtab3primary" class="" data-toggle="tab">Amendment</a></li>-->
                                                            <!--<li style=" width:13.8% !important;"><a href="#subtab4primary" class="" data-toggle="tab">Minutes</a></li>-->
                                                            <!--<li style=" width:11.8% !important;"><a href="#subtab5primary" class="" data-toggle="tab">Docs</a></li>-->
                                                            <li style=" width: auto !important;" class="active"><a href="#subtab1primary" class="" style="padding: 10px 8px;" data-toggle="tab">Corporation Renewal</a></li>
                                                            <li style=" width: auto !important;"><a href="#subtab2primary" class="" style="padding: 10px 8px;" data-toggle="tab">Share Ledger</a></li>
                                                            <li style=" width: auto !important;"><a href="#subtabprimary6" class="" style="padding: 10px 8px;" data-toggle="tab">Share Certi.</a></li>
                                                            <li style=" width: auto !important;"><a href="#subtabprimary7" class="" style="padding: 10px 8px;" data-toggle="tab">Share Trf.</a></li>
                                                            <li style=" width:auto !important;"><a href="#subtab3primary" class="" style="padding: 10px 8px;" data-toggle="tab">Amendment</a></li>
                                                            <li style=" width:auto !important;"><a href="#subtab4primary" class="" style="padding: 10px 8px;" data-toggle="tab">Minutes</a></li>
                                                            <li style=" width:auto !important;"><a href="#subtab5primary" class="" style="padding: 10px 8px;" data-toggle="tab">Docs</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                        </div>
              
                                        <div class="tab-content pad20" style="padding-top:5px; ">
                                            <div class="tab-pane fade in active  newcheckbox" id="subtab1primary">
                                                <a class="btn_new btn-renew btnaddrecord" data-toggle="modal" data-target="#addNewFormation" style="margin-bottom:10px">Add Record</a>
                                                <div class="clear"></div>
                           <div class="table-responsive">
                                <table class="table table-bordered tablestriped">
                                    <thead>
                                        <tr>
                                            <th>Renew Year</th>
                                            <th>Renew For</th>
                                            <th>Paid Amount</th>
                                            <th>Payment Method</th>
                                            <th>Corporation Status</th>
                                            <th>SOS Reciept</th>
                                            <th>Annual Officer</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                   
                                    
                                    <tbody>
                                        <tr>
                                                                                </tr></tbody>
                                   
                                </table>
               
               
                               <div id="myModals_" class="modal fade">
                                   <div class="modal-dialog">
                                      <div class="modal-content">
                                         <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title">Annual Receipt  </h4>
                                         </div>
                                         <div class="modal-body">
                                            <p><iframe height="450" width="530" src="https://financialservicecenter.net/public/adminupload/"></iframe></p>
                                         </div>
                                         <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                         </div>
                                      </div>
                                   </div>
                                </div>


                                <div id="myModals1_" class="modal fade">
                                   <div class="modal-dialog">
                                      <div class="modal-content">
                                         <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title">Work Officer  </h4>
                                         </div>
                                         <div class="modal-body">
                                            <p><iframe height="450" width="530" src="https://financialservicecenter.net/public/adminupload/"></iframe></p>
                                         </div>
                                         <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                         </div>
                                      </div>
                                   </div>
                                </div>
                                               
               </div>
           </div>
                                            <div class="clear"></div>
                                            <div class="tab-pane fade  newcheckbox" id="subtab2primary">
                        <div class="table-responsive">
                             <table class="table table-bordered table-striped">
                                 <thead>
                        <tr>
                            <th></th>
                            <th>NAME OF CERTIFICATE HOLDER</th>
                            <th>CERTIFICATES ISSUED  NO. SHARES* </th>
                            <th>FROM WHOM TRANSFERRED (If Original Issue Enter As Such)</th>
                            <th>AMOUNT PAID THEREON</th>
                            <th>DATE OF TRANSFER OF SHARES*</th>
                            <th>CERTIFICATES SURRENDERED CERTIF. NOS.</th>
                            <th>CERTIFICATES SURRENDERED NO. SHARES*</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>A</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>B</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>C</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>D</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        </tbody>
                        
                    </table>
                        </div>
                    </div>
                    <div class="tab-pane fade newcheckbox" id="subtabprimary6">
                        Share Certificate
                    </div>
                    <div class="tab-pane fade newcheckbox" id="subtabprimary7">
                        Share Transfer
                    </div>
                    <div class="tab-pane fade  newcheckbox" id="subtab3primary">
                      Amendment
                    </div>
                                            <div class="tab-pane fade newcheckbox" id="subtab4primary">
                                                Minutes
                                            </div>
                                            <div class="tab-pane fade newcheckbox" id="subtab5primary">
                                               <table class="table table-hover table-bordered dataTable no-footer">
                                                    <thead>
                                                    <tr>
                                                        <th style="width:50px">No.</th>
                                                        <th class="text-left">Document Name</th>
                                                        <th style="width:80px;">View</th>
                                                        <th style="width:80px;">Action</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    
                                                    <tr>
                                                        @foreach($documentupload as $doc1)
                                                        <td class="text-center">{{$loop->index+1}}</td>
                                                        <td class="text-left">{{$doc1->documentsname}}</td>
                                                           <td class="text-center"><a href="{{url('public/clientupload')}}/{{$doc1->clientdocument}}" target="_blank" class="btn-action btn-view-edit btn-primary"><i class="fa fa-eye"></i></a></td>
                                                        <!--<td class="text-center">{{$doc1->clientdocument}}</td>-->
                                                        <td class="text-center">
                                                            
                                                            <button type="button" class="btn-action btn-view-edit passingID" data-id="{{$doc1->id}}"><i class="fa fa-edit"></i></button>
                            							    <a onclick="return confirm('Are you sure to remove this record?')" href="{{route('workstatus.destroydocumentemp',[$doc1->id,$doc1->client_id])}}" class="btn-action btn-delete"><i class="fa fa-trash"></i></a>
                                                            
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                    
                                                    
                                                    </tbody>
                                                </table> 
                                               <div class="clear"></div>
                                            </div>
                                        </div>
               
                                       <div class="clear"></div>
                                                                           </div>
        </div>
        @endif
        <div class="tab-pane fade disablebox newcheckbox" id="tab2primary">
           <div class="col-md-12">
    													<div class="Branch" style="text-align:left;padding-left: 15px;">
    														<h1 class="text-center">Business License History</h1>
    													</div>
    												</div>
												    <div class="col-md-12 text-right">
												       <a href="#myModalbusinesspopup" class="btn_new btn-renew pull-right inlinebutton" data-toggle="modal" data-target="#myModalbusinesspopup" style="width:10% !important;">Add Record</a>
												        <div class="clear"></div>
												       <table class="table table-hover table-bordered dataTable no-footer">
												           <thead>
												               <tr>
												                   <th style="width:70px;">Year</th>
												                  
												                   <th style="width:130px;">License Gross</th>
												                    <th style="width:130px;">License Fee</th>
												                
												                   <th style="width:110px;">License #</th>
												                     <th style="width:130px;">License Issue Date</th>
												                   <th style="width:100px;">License Copy</th>
												                   <th style="width:80px">Status</th>
												                   <th style="width:100px">Action</th>
												                   <th style="width:80px">Form</th>
												               </tr>
												           </thead>
												           <tbody>
												               <tr>
												                    @foreach($buslicense as $bus_lic)
                												   	
												                   <td class="text-center"> {{ $bus_lic->license_year}}</td>
												                   
												                   <td class="text-center">{{ $bus_lic->license_gross}}</td>
												                   <td class="text-center">{{ $bus_lic->license_fee}}</td>
												                 
												                   <td> {{ $bus_lic->license_no}}</td>
												                     <td class="text-center"><?php echo date('m/d/Y',strtotime($bus_lic->license_renew_date))?></td>
												                   <td class="text-center"><a href="{{url('public/adminupload')}}/{{$bus_lic->license_copy}}" target="_blank" ><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></td>
												                   <td class="text-center">{{$bus_lic->license_status}}</td>
												                   <td class="text-center">
												                        <a class="btn-action btn-view-edit"><i class="fa fa-edit"></i></a>
				                                                        <a class="btn-action btn-delete" href="#"><i class="fa fa-trash"></i></a>
												                   </td>
												                   <td class="text-center"><a href="#" class="btn btn-primary btn-sm">Create</a></td>
												                 
												               </tr>
												                 @endforeach												           </tbody>
												           
												       </table>
												    </div>
        </div>
        <div class="tab-pane fade disablebox newcheckbox" id="tab3primary">
                        <div class="form-group row  card" style=" margin-right:5px; margin-left:0px;margin-bottom:0px;">
                                      
                                         
                              
                            <div class="col-md-12" >
                                <div class="panel-heading">
                                    <ul class="nav nav-tabs" id="myTab2" style="padding:5px !important; margin-bottom:10px;">
                                          
                                        @if($common->business_id !='6')
                                          <li class="active" style=" width: auto !important;"><a href="#subtab55primary" class="" data-toggle="tab" aria-expanded="true">Income Tax</a></li>
                                          <li style=" width: auto !important;"><a href="#subtab6primary" class="" data-toggle="tab" aria-expanded="true">Payroll Tax</a></li>
                                          <li style=" width: auto !important;"><a href="#subtab7primary" class="" data-toggle="tab" aria-expanded="true">Personal Property Tax</a></li>
                                        @endif
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane fade newcheckbox active in" id="subtab55primary">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						    <div class="row">
						        <a class="btn_new btn-renew btnaddrecord" style="margin:10px 0px;" data-toggle="modal" data-target="#myModalAddrecordtax">Add Record</a>
								<div class="Branch">
									<div class="col-md-4" style="text-align:left;">
										
									</div>
									<div class="col-md-3">
										<h1> Income Tax </h1>
									</div>
									
									<div class="col-md-4" style="text-align:right;">
										
									</div>
									
								</div>
							</div>
						    <!--<a class="btn_new btn-renew btnaddrecord disabled" style="display:block; cursor:pointer;" href="workrecord?id=<?php //echo $common->id;?>">Add Record</a>-->
						    
							<div class="clear"></div>
							
							<?php
							foreach($Incometaxfederal as $Income)
							{
							    
							?>	
							
							<div class="panel-group" id="accordion" role="tablist"  style="margin-top:15px;">
                                <div class="panel panel-default">
                                 
                                    <div class="panel-heading" role="tab" id="headingtwo">
                                         <h4 class="panel-title" style="margin-bottom:0px;">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion" class="collapsed" href="#collapse<?php echo $Income->id;?>">
                                            <span class="ac_name_first">@if($Income->federalsyear!=''){{$Income->federalsyear}} @endif</span>
                                           
                                            <i class="more-less glyphicon glyphicon-plus"></i>
                                            <i class="more-less glyphicon glyphicon-minus"></i>
                                           
                                            </a>
                                         </h4>
                                    </div>
                              
                                    <div style="clear:both;"></div>
                                        
                                    <div id="collapse<?php echo $Income->id;?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingtwo">
                                        <div class="panel-body accordion-body" style="padding-top:15px;">
                                            <div class="table-responsive">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered tablestriped">
                                                        <thead>
                                                            <tr>
                                                                <th>Year</th>
                                                                <th>Tax Return</th>
                                                                <th>Form No.</th>
                                                                <th>Filing Date</th>
                                                                <th>Filing Method</th>
                                                                <th>Status</th>
                                                                <th>Action</th>
                                                            </tr>
                                                        </thead>
                                                       
                                                        
                                                        <tbody>
                                                            <tr>
                                                                <?php
                                                                foreach($Incometax3 as $Income2)
					                                            {
					                                                if($Income2->federalsyear==$Income->federalsyear)
					                                                {
                                                                ?>
                                                                 
                                                                 
                                                               <td class="text-center">{{$Income2->federalsyear}} </td>
                                                               <td class="text-center">{{$Income2->federalstax}} </td>
                                                               <td class="text-center">{{$Income2->federalsform}} </td>
                                                               <td class="text-center">{{date('M-d Y',strtotime($Income2->federalsdate))}} </td>
                                                               <td class="text-center">{{$Income2->federalsmethod}} </td>
                                                               <td class="text-center">{{$Income2->federalsstatus}} </td>
                                                               <td class="text-center">
                                                                   <!--<button type="button" class="btn-action btn-view-edit federalTaxation" data-id="{{$Income2->id}}"><i class="fa fa-edit"></i></button>-->
                                                                <a href="{{route('workstatustaxations.edit',$Income2->id)}}" class="btn-action btn-view-edit"><i class="fa fa-edit"></i></a>
                    					                        <a onclick="return confirm('Are you sure to remove this record?')" href="{{route('workstatus.destroyfederaltax',$Income2->id)}}" class="btn-action btn-delete"><i class="fa fa-trash"></i></a>
                                                              </td>
                                                            </tr>
                                                            <?php
					                                                }
					                   
					                                            }
                                                            ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                
                                </div>
                            
                                
                            </div>
                            
                            <?php 
							}
							?>
                </div> 
                                        </div>
								        <div class="tab-pane fade newcheckbox " id="subtab6primary">
										    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    										    <div class="row">
        											<div class="Branch">
        												<div class="col-md-3" style="text-align:left;">
        													<h1>Federal / State</h1>
        												</div>
        												<div class="col-md-6">
        													<h1>Payroll Tax </h1>
        												</div>
        											</div>
        										</div>
    										
    										
    										
                                                <a class="btn_new btn-renew btnaddrecord" data-toggle="modal" data-target="#myModalAddrecord">Add Record</a>
    										<div class="clear"></div>
    										
    									<div class="panel-group" id="accordion" role="tablist" style="margin-top:15px;">
                                                 <div class="panel panel-default">
                                             
                                                  <div class="panel-heading" role="tab" id="headingtwo">
                                                     <h4 class="panel-title" style="margin-bottom:0px;">
                                                        <a role="button" data-toggle="collapse" data-parent="#accordion" class="collapsed" href="#collapse2">
                                                        <span class="ac_name_first">2018</span>
                                                       
                                                        <i class="more-less glyphicon glyphicon-plus"></i>
                                                        <i class="more-less glyphicon glyphicon-minus"></i>
                                                       
                                                        </a>
                                                     </h4>
                                                  </div>
                                          
                                                    <div style="clear:both;"></div>
                                                    
                                            <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingtwo">
                                             <div class="panel-body accordion-body" style="padding-top:15px;">
                                                     <div class="table-responsive">
                                                        <div class="table-responsive">
                                <table class="table table-bordered tablestriped">
                                    <thead>
                                        <tr>
                                            <th>Year</th>
                                            <th>Tax Return</th>
                                            <th>Form No.</th>
                                            <th>Filing Date</th>
                                            <th>Filing Method</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                   
                                    
                                    <tbody>
                                        <tr>
                                           <td class="text-center">2018</td>
                                           <td class="text-center">Original</td>
                                           <td class="text-center">Form-1120S</td>
                                           <td class="text-center">Feb-22 2020</td>
                                           <td class="text-center">E-File</td>
                                           <td class="text-center">Accepted </td>
                                           <td class="text-center"><a data-toggle="modal" data-target="#incometaxRecord" class="btn-action btn-view-edit"><i class="fa fa-edit"></i></a>
								            <form action="https://financialservicecenter.net/fscemployee/adminworkstatus/2" method="post" style="display:none" id="delete-id-2">
                                        <input type="hidden" name="_token" value="W1cIGb2W87wuAjApnAq5p9HKWwmqimwElMheSksJ"> <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="id" value="2">
                                        </form>
                                        
										<a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record ?'))
                                                                                  {event.preventDefault();document.getElementById('delete-id-2').submit();} else{event.preventDefault();}" href=""><i class="fa fa-trash"></i></a>
					                        
                                          </td>
                                           
                                        
                                        </tr>
                                                                            </tbody>
                                   
                                </table>
               
                                 </div>
                                                    </div>
                                                 
                                            </div>
                                            </div>
                                            
                                        </div>
                                        
                                            
                                    </div>    </div>
                                        </div>
                                        
                                        <div class="tab-pane  fade newcheckbox" id="subtab7primary">
										    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    										    <div class="row">
        											<div class="Branch">
        												<div class="col-md-3" style="text-align:left;">
        													<h1>Federal / State</h1>
        												</div>
        												<div class="col-md-6">
        													<h1>Personal Property Tax </h1>
        												</div>
        											</div>
        										</div>
    										
    										
    										
                                                <a class="btn_new btn-renew btnaddrecord" data-toggle="modal" data-target="#myModalAddrecord">Add Record</a>
    										<div class="clear"></div>
    										
    										<div class="panel-group" id="accordion" role="tablist" style="margin-top:15px;">
                                                 <div class="panel panel-default">
                                             
                                                  <div class="panel-heading" role="tab" id="headingtwo">
                                                     <h4 class="panel-title" style="margin-bottom:0px;">
                                                        <a role="button" data-toggle="collapse" data-parent="#accordion" class="collapsed" href="#collapsetwoppt">
                                                        <span class="ac_name_first">2012</span>
                                                       
                                                        <i class="more-less glyphicon glyphicon-plus"></i>
                                                        <i class="more-less glyphicon glyphicon-minus"></i>
                                                       
                                                        </a>
                                                     </h4>
                                                  </div>
                                          
                                                    <div style="clear:both;"></div>
                                                    
                                            <div id="collapsetwoppt" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingtwo">
                                             <div class="panel-body accordion-body" style="padding-top:15px;">
                                                
                                                     <div class="table-responsive">
                                                        Year
                                                    </div>
                                                 
                                            </div>
                                            </div>
                                            
                                        </div>
                                        
                                            
                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
												
										
										
						          				
										
										
										
        </div>
        <div class="tab-pane fade disablebox newcheckbox" id="tab4primary">
          <h1>Banking</h1>
        </div>
        <div class="tab-pane fade  disablebox newcheckbox" id="tab5primary">
          <h1>tab5</h1>
        </div>
        <div class="tab-pane fade  disablebox newcheckbox" id="tab6primary">
          <h1>tab6</h1>
        </div>
        <div class="tab-pane fade disablebox newcheckbox" id="tab60primary">
          <h1>Asset</h1>
        </div>
        <div class="tab-pane fade disablebox newcheckbox" id="tab10primary">
          <h1>Accounting</h1>
        </div>
  
   </div>
   </div>
						@endif
					</div>
				</div>
			</div>
		</div>
		</div>
		</div>









<!-- The Modal -->
<div class="modal" id="myModalbusinesspopup">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      
      
      
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>





      <!-- Modal body -->
      <div class="modal-body">
        
           <form method="post"  action="{{route('workrecord.store')}}" class="form-horizontal" enctype="multipart/form-data">
                     {{csrf_field()}}

            <?php
            if(isset($common->id)!='')
            {
                 //@if(!empty($common->filename))

                ?>
                    <input type="hidden" name="client_id" value="{{$common->id}}">
                <?php
            }
            ?>
            
            
            <div class="form-group row">
            <label class="col-md-4 control-label text-right">Year</label>
            <div class="col-md-6">
                <select class="form-control" name="license_year" required>
                    <option disabled>Select</option>
                    <option>2020</option>
                    <option>2021</option>
                    <option>2022</option>
                    <option>2023</option>
                </select>
            </div>
            </div>
        
            <div class="form-group row">
            <label class="col-md-4 control-label text-right">Gross Revenue</label>
            <div class="col-md-6"><input type="text" class="form-control" name="license_gross" id="license_gross" required/></div>
        </div>
        
            <div class="form-group row">
            <label class="col-md-4 control-label text-right">License Fee </label>
            <div class="col-md-6"><input type="text" class="form-control txtinput_1" name="license_fee" id="license_fee" required/></div>
        </div>
        
            <div class="form-group row">
            <label class="col-md-4 control-label text-right">License Status</label>
            <div class="col-md-6">
                <select class="form-control" name="license_status" required>
                    <option>Active</option>
                    <option>Inactive</option>
                    <option>Hold</option>
                </select>
            </div>
        </div>
        
        <div class="form-group row">
            <label class="col-md-4 control-label text-right">License Copy</label>
            <!--<div class="col-md-6">-->
            <!--    <input type="file" name="license_copy" required/>-->
            <!--</div>-->
            
            <div class="col-md-8">
                <label class="file-upload btn btn-primary">
                <input type="file" class="form-control" type="file" name="license_copy" required/>
                Browse for file ... </label>
           </div>
        </div>
        
            <div class="form-group row">
            <label class="col-md-4 control-label text-right">Certificate No</label>
            <div class="col-md-6"><input type="text" class="form-control" name="license_no" required/></div>
        </div>
        
        <!--    <div class="form-group row">-->
        <!--    <label class="col-md-4 control-label text-right">Website Link</label>-->
        <!--    <div class="col-md-6"><input type="text" class="form-control" name="website_link" required/></div>-->
        <!--</div>-->

        <div class="form-group row">
            <label class="col-md-4 control-label text-right">Note</label>
            <div class="col-md-6"><input type="text" class="form-control" name="license_note" required/></div>
        </div>

            
            <div class="form-group row">
                <label class="col-md-4 control-label text-right">License Issue Date </label>
                <div class="col-md-6"><input type="date" class="form-control" name="license_renew_date" required/></div>
            </div>

            <div class="form-group row">
            <label class="col-md-4 control-label text-right"></label>
            <div class="col-md-3">
                <input class="btn_new_save btn-primary1 primary1" type="submit" value="Save">
            </div>
            <div class="col-md-3">
                <a class="btn_new_cancel" href="https://financialservicecenter.net/fscemployee/employee">Cancel</a>
            </div>
        </div>
    
        </form>
        
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>		



                                        <!-- Modal -->
                                        <div class="modal fade" id="myModal" role="dialog">
                                            <div class="modal-dialog">
                                            
                                              <!-- Modal content-->
                                              <div class="modal-content">
                                                <div class="modal-header">
                                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                  <h4 class="modal-title">Update Document Record</h4>
                                                </div>
                                                <div class="modal-body">
                                                <?php
                                                if(isset($doc1->id)!='')
                                                {
                                                
                                                ?>
                                                    <form method="post" class="form-horizontal" enctype="multipart/form-data" action="{{route('workstatus.updatedocumentsemp',$doc1->id)}}">
        									{{csrf_field()}}
        								<?php
                                        }
                                        ?>
                                                    <input type="hidden" class="form-control" name="idaa" id="idkl">
                                                    <input type="hidden" class="form-control" name="filename_idss" id="filename_idss">
                                                    <input type="hidden" class="form-control" name="client_id" id="client_id">
                                                    <!--<input type="text" class="form-control" name="documentsname" id="documentsname">-->
                                                    <!--<input type="text" class="form-control" name="clientdocument" id="clientdocument">-->
                                                  
                                                        </br>
                                                           <div class="row">
                                                                <div class="col-md-4"><label class="control-label text-right">Document Name :</label></div>
                                                                <div class="col-md-8">
                                                                   <select class="form-control"  style="width:85%;" name="documentsname"  id="documentsname" required>
                                                                      @foreach($document as $cur)
                                                                      <option value="{{$cur->documentname}}">{{$cur->documentname}}</option>
                                                                      @endforeach
                                                                   </select>
                                                                   @if ($errors->has('documentsname'))
                                                                   <span class="help-block">
                                                                   <strong>{{ $errors->first('documentsname') }}</strong>
                                                                   </span>
                                                                   @endif
                                                                </div>
                                                            </div>

                                                            </br>
                                                            <div class="row">
                                                               <div class="col-md-4"><label class="control-label text-right">Upload Document : </label>
                                                               </div>
                                                               <div class="col-md-8">
                                                                   
                                                                    <label class="file-upload btn btn-primary">
                                                                    <input type="file" class="form-control" name="clientdocument" />
                                                                    Browse for file ... </label>
                                                                     <input type="hidden"  name="clientdocument_1" id="clientdocument"/><span id="clientdocument22"></span>
                                                               </div>
                                                           </div>
                                       
                                                            </br>
                                                            <div class="row">
                                                               <div class="col-md-4">
                                                               </div>
                                                               <div class="col-md-2" style=" padding-right:3px;">
                                                           
                                                                   <input class="btn_new_save btn-primary1 primary1" type="submit" id="primary1" name="submit" value="Save">
                                                                  </div>
                                                                    <div class="col-md-2" style="padding-left:3px;">
                                                                   <a class="btn_new_cancel" data-dismiss="modal">Cancel</a>
                                                                </div>
                                                            </div>
                                                    
                                                  </form>
                                                </div>
                                                <div class="modal-footer">
                                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                </div>
                                              </div>
                                              
                                            </div>
                                          </div>
                                       
                                    </div>
                                </div>
                                
                                
                                
                                
<!-- Modal -->
<div id="myModalAddrecord" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Record</h4>
      </div>
      <div class="modal-body">
          <div class="recordform">
               <div class="row form-group">
                   <label class="col-md-3 control-label"></label>
                <label class="col-md-4 control-label" style="text-align:center;">Federal</label>
                
                <label class="col-md-4 control-label" style="text-align:center;">State</label>
               
            </div>
            <div class="row form-group">
                <label class="col-md-3 control-label">Filling Year</label>
                <div class="col-md-4">
                     <select class="form-control">
                        <option>Select</option>
                        <option>2017 </option>
                        <option>2018</option>
                        <option>2019</option>
                    </select>
                </div>
                <div class="col-md-4">
                     <select class="form-control">
                        <option>Select</option>
                        <option>2017 </option>
                        <option>2018</option>
                        <option>2019</option>
                    </select>
                </div>
            </div>
             <div class="row form-group">
                <label class="col-md-3 control-label">Tax Return</label>
                <div class="col-md-4">
                    <select class="form-control">
                        <option>Select</option>
                        <option>Extension </option>
                        <option>Regular</option>
                        <option>Amendment</option>
                    </select>
                </div>
                 <div class="col-md-4">
                    <select class="form-control">
                        <option>Select</option>
                        <option>Extension </option>
                        <option>Regular</option>
                        <option>Amendment</option>
                    </select>
                </div>
            </div>
              <div class="row form-group">
                <label class="col-md-3 control-label">Form No.</label>
                <div class="col-md-4">
                    <input type="text" class="form-control"/>
                </div>
                 <div class="col-md-4">
                    <input type="text" class="form-control"/>
                </div>
            </div>
              <div class="row form-group">
                <label class="col-md-3 control-label">Due Date</label>
                <div class="col-md-4">
                    <input type="text" class="form-control" placeholder="Mar-15-2020"/>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" placeholder="Mar-15-2020"/>
                </div>
            </div>
            <div class="row form-group">
                <label class="col-md-3 control-label">Filing Method </label>
                <div class="col-md-4">
                    <select class="form-control">
                        <option>Select</option>
                        <option>E-File </option>
                        <option>Paperfile</option>
                    </select>
                </div>
                 <div class="col-md-4">
                    <select class="form-control">
                        <option>Select</option>
                        <option>E-File </option>
                        <option>Paperfile</option>
                    </select>
                </div>
            </div>
            <div class="row form-group">
                <label class="col-md-3 control-label">Filing Date</label>
                <div class="col-md-4">
                    <input type="text" class="form-control"/>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control"/>
                </div>
            </div>
             <div class="row form-group">
                <label class="col-md-3 control-label">Status of Filling </label>
                <div class="col-md-4">
                    <select class="form-control">
                        <option>Select</option>
                        <option>Accepted </option>
                        <option>Rejected</option>
                        <option>Pending</option>
                    </select>
                </div>
                 <div class="col-md-4">
                    <select class="form-control">
                        <option>Select</option>
                        <option>Accepted </option>
                        <option>Rejected</option>
                        <option>Pending</option>
                    </select>
                </div>
            </div>
             <div class="row form-group">
                <label class="col-md-3 control-label">Add File </label>
                <div class="col-md-4">
                   <input type="file" class="form-control"/>
                </div>
                <div class="col-md-4">
                   <input type="file" class="form-control"/>
                </div>
            </div>
              <div class="row form-group">
                <label class="col-md-3 control-label">Note </label>
                <div class="col-md-8">
                   <textarea class="form-control"></textarea>
                </div>
                
            </div>
            <div class="row form-group">
                <label class="col-md-3 control-label">&nbsp;</label>
                <div class="col-md-2">
                    <input type="submit" class="btn_new_save primary" value="Save" hhhhhhhhhhhhh="">
                </div>
                 <div class="col-md-2">
                    <a href="#" class="btn_new_cancel">Cancel</a>
                </div>
               
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

        <!--Modal Conversation Update Start-->
    <form method="post" class="form-horizontal" enctype="multipart/form-data" action="{{route('customer.updatecoversation')}}">
    {{csrf_field()}}
                                                    
    
        <div id="conversationModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Conversation Sheet</h4>
      </div>
      <div class="modal-body">
          <div style="max-width:700px; margin:0px auto;">
       <div class="row">
           <div class="col-md-4">
               <div class="form-group">
               <div class="row">
                   <label class="control-label col-md-5 text-right"><strong>Date:</strong></label>
                   <div class="col-md-7">
                    <!--<input type="text" class="form-control effective_date2"/>-->
                    <input type="hidden" name="CovID" id="CovID" class="form-control">
                    <input type="text" name="date" id="creattiondate1" class="form-control"  placeholder="Date" readonly>
                   </div>
               </div>
               </div>
           </div>
            <div class="col-md-4">
                <div class="form-group">
                <div class="row">
               <label class="control-label col-md-5 text-right" ><strong>Day:</strong></label>
               <div class="col-md-7">
               <input type="text" name="day" id="day1" class="form-control" placeholder="Day" readonly>
               </div>
               </div>
               </div>
           </div>
            <div class="col-md-4">
                <div class="form-group">
                <div class="row">
                   <label class="control-label col-md-3"><strong>Time:</strong></label>
                   <div class="col-md-8">
                        <input type="text" name="time" id="time1" class="form-control"  placeholder="Time" readonly>
                   </div>
               </div>
               </div>
           </div>
       </div>
       
       <div class="form-group{{ $errors->has('type_user') ? ' has-error' : '' }}">
            <div class="row">
            <label class="control-label col-md-3 text-right" style="width:100px;"><strong>Select:</strong></label>
                <div class="col-md-5">
                    <div class="">
                    <select class="form-control fsc-input type_user" name="type_user" id="type_user1" required>
                        <option>Select</option>
                        <option>Client</option>
                        <option>EE-User</option>
                        <option>Vendor</option>
                        <option>Other</option>
                    </select>
                    </div>
                    @if($errors->has('type_user'))
                        <span class="help-block">
                        <strong>{{ $errors->first('type_user') }}</strong>
                        </span>
                    @endif
                </div>
              
            </div>
       </div>
       
       <div class="form-group clientid2" id="clientid" style="display:none;">
            <div class="row">
            <label class="control-label col-md-3 text-right" style="width:100px;"><strong>Client :</strong></label>
                <div class="col-md-5">
                    <select class="form-control" name="clientid" id="clientid1">
                        <option>Select</option>
                        @foreach($listclient as $client)
                            <option value="{{$client->id}}">{{$client->first_name}} {{$client->middle_name}} {{$client->last_name}}</option>
                        @endforeach      
                    </select>
                </div>
               
            </div>
       </div>
       
       <div class="form-group employeeuserid2" id="employeeuserid" style="display:none;">
            <div class="row">
            <label class="control-label col-md-3 text-right" style="width:100px;"><strong>Employee/User:</strong></label>
                <div class="col-md-5">
                    <select class="form-control" name="employeeuserid" id="employeeuserid1">
                        <option>Select</option>
                        @foreach($listemployeeuser as $employee)
                            <option value="{{$employee->id}}">{{$employee->firstName}} {{$employee->middleName}} {{$employee->lastName}}</option>
                        @endforeach      
                    </select>
                </div>
               
            </div>
       </div>
       
       <div class="form-group vendorid2" id="vendorid" style="display:none;">
            <div class="row">
            <label class="control-label col-md-3 text-right" style="width:100px;"><strong>Vendor:</strong></label>
                <div class="col-md-5">
                    <select class="form-control" name="vendorid" id="vendorid1">
                        <option>Select</option>
                        @foreach($listvendoe as $vendoe)
                            <option value="{{$vendoe->id}}">{{$vendoe->firstName}} {{$vendoe->middleName}} {{$vendoe->lastName}}</option>
                        @endforeach      
                    </select>
                </div>
               
            </div>
       </div>
       
       <div class="form-group otherid2" id="otherid" style="display:none;">
           <div class="row">
                <label class="control-label col-md-3" style="width:100px; text-align:right; padding-left:0px!important;"><strong>Other :</strong></label>
                   <div class="col-md-5">
                        <input type="text" class="form-control" name="otherid" id="otherid1">
                   </div>
                   
           </div>
       </div>
       
       <div class="form-group{{ $errors->has('conrelatedname') ? ' has-error' : '' }}">
           <div class="row">
                <label class="control-label col-md-3" style="width:100px; text-align:right; padding-left:0px!important;"><strong>Related to:</strong></label>
                   <div class="col-md-5">
                       <div class="">
                        <select class="form-control" name="conrelatedname" id="conrelatedname1" required>
                              <option>Select</option>
                              @foreach($relatedNames as $relatedNames)
                              <option value="{{$relatedNames->id}}">{{$relatedNames->relatednames}}</option>
                              @endforeach
                        </select>
                        </div>
                        @if($errors->has('conrelatedname'))
                            <span class="help-block">
                            <strong>{{ $errors->first('conrelatedname') }}</strong>
                            </span>
                        @endif
                   </div>
                   
           </div>
       </div>
        

        
       
       
        <div class="form-group">
            <div class="row">
                <label class="control-label col-md-3" style="width:100px; text-align:right; padding-left:0px!important;"><strong>Conversation:</strong></label>
                <div class="col-md-9">
                    <textarea rows="3" cols="12" class="form-control" name="condescription" id="condescription1"> </textarea>
               </div>
            </div>
       </div>
       
        <div class="form-group">
           <div class="row">
                <label class="control-label col-md-3" style="width:100px; text-align:right; padding-left:0px!important;"><strong>Note</strong></label>
                   <div class="col-md-9">
                  <textarea rows="3" cols="12" class="form-control" name="connotes" id="connotes1"> </textarea>
                   </div>
           </div>
       </div>
       
        <div class="form-group">
            <div class="row">
            <label class="control-label col-md-3" style="width:100px; text-align:right; padding-left:0px!important;"><strong>&nbsp;</strong></label>
                <div class="col-md-2">
                    <input class="btn_new_save btn-primary1" id="recInsert" type="submit" name="submit" value="Save">
			    </div>
				<div class="col-md-2">
					<a class="btn_new_cancel" href="https://financialservicecenter.net/fac-Bhavesh-0554">Cancel</a>
               </div>
            </div>
       </div>
       
       
       </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
            </div>
        </div>                                                 
                                                    
    </form>
    <!--Modal Conversation Update End-->

   
   <!--Modal Notes Update Start-->
    <div id="notesModal" class="modal fade" role="dialog">
        <div class="modal-dialog" style="width:850px;">
            <form method="post" class="form-horizontal" enctype="multipart/form-data" action="{{route('customer.updatenotes')}}">
                {{csrf_field()}}
                                                    
    
                <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> Note</h4>
      </div>
      <div class="modal-body">
          <div style="max-width:700px; margin:0px auto;">
       <div class="row mt-3" style="margin-top:20px;">
           <div class="col-md-4">
               <div class="form-group">
               <div class="row">
                   <label class="control-label col-md-5 text-right"  style="padding-right:28px!important;" ><strong>Date:</strong></label>
                   <div class="col-md-6" style="padding-left:12px!important;">
                    <!--<input type="text" class="form-control effective_date2"/>-->
                    <input type="hidden" name="NoteID" id="NoteID"> 
                    <input type="text" name="notedate" id="date2" class="form-control" value="{{date('m-d-Y')}}" placeholder="Date" readonly>
                   </div>
               </div>
               </div>
           </div>
            <div class="col-md-4">
                <div class="form-group">
                <div class="row">
               <label class="control-label col-md-4 text-right" ><strong>Day:</strong></label>
               <div class="col-md-5">
               <input type="text" name="noteday" id="day2" class="form-control" placeholder="Day" value="{{date('l')}}" readonly>
               </div>
               </div>
               </div>
           </div>
            <div class="col-md-4">
                <div class="form-group">
                <div class="row">
                   <label class="control-label col-md-3"><strong>Time:</strong></label>
                   <div class="col-md-5">
                        <input type="text" name="notetime" id="time2" class="form-control" value="{{date("g:i a")}}" placeholder="Time" readonly>
                   </div>
               </div>
               </div>
           </div>
       </div>
               
       <div class="form-group">
            <div class="row">
            <label class="control-label col-md-3 text-right" style="width:110px;"><strong>Select:</strong></label>
                <div class="col-md-5">
                    <div class="">
                    <select class="form-control fsc-input notetype_user" name="notetype_user" id="notetype_user1" required>
                        <option>Select</option>
                        <option>Client</option>
                        <option>EE-User</option>
                        <option>Vendor</option>
                        <option>Other</option>
                    </select>
                    </div>
                </div>
              
            </div>
       </div>
       
        <div class="form-group noteclientid2" id="noteclientid" style="display:none;">
            <div class="row">
            <label class="control-label col-md-3 text-right" style="width:100px;"><strong>Client :</strong></label>
                <div class="col-md-5">
                    <select class="form-control" name="noteclientid" id="noteclientid1">
                        <option>Select</option>
                        @foreach($listclient as $client)
                            <option value="{{$client->id}}">{{$client->first_name}} {{$client->middle_name}} {{$client->last_name}}</option>
                        @endforeach      
                    </select>
                </div>
               
            </div>
       </div>
       
       <div class="form-group noteemployeeuserid2" id="noteemployeeuserid" style="display:none;">
            <div class="row">
            <label class="control-label col-md-3 text-right" style="width:100px;"><strong>Employee/User:</strong></label>
                <div class="col-md-5">
                    <select class="form-control" name="noteemployeeuserid" id="noteemployeeuserid1">
                        <option>Select</option>
                        @foreach($listemployeeuser as $employee)
                            <option value="{{$employee->id}}">{{$employee->firstName}} {{$employee->middleName}} {{$employee->lastName}}</option>
                        @endforeach      
                    </select>
                </div>
               
            </div>
       </div>
       
       <div class="form-group notevendorid2" id="notevendorid" style="display:none;">
            <div class="row">
            
            <label class="control-label col-md-3 text-right" style="width:110px;"><strong>Vendor :</strong></label>
                <div class="col-md-5">
                    <select class="form-control" name="notevendorid" id="notevendorid1">
                        <option>Select</option>
                        @foreach($listvendoe as $vendoe)
                            <option value="{{$vendoe->id}}">{{$vendoe->firstName}} {{$vendoe->middleName}} {{$vendoe->lastName}}</option>
                        @endforeach      
                    </select>
                </div>
               
            </div>
       </div>
       
       <div class="form-group noteotherid2" id="noteotherid" style="display:none;">
           <div class="row">
                
                <label class="control-label col-md-3 text-right" style="width:110px;"><strong>Other :</strong></label>
                   <div class="col-md-5">
                        <input type="text" class="form-control" name="noteotherid" id="noteotherid1">
                   </div>
                   
           </div>
       </div>
       
       <div class="form-group{{ $errors->has('notesrelated') ? ' has-error' : '' }}">
           <div class="row">
                <label class="control-label col-md-3 text-right" style="width:110px;"><strong>Related to :</strong></label>
                   <div class="col-md-5">
                       <div class="">
                        <select class="form-control" name="notenotesrelatedname" id="noterelated1" required>
                              <option>Select</option>
                              @foreach($NotesNames as $notesRelated)
                              <option value="{{$notesRelated->id}}">{{$notesRelated->notesrelated}}</option>
                              @endforeach
                        </select>
                        </div>
                        @if($errors->has('notesrelated'))
                            <span class="help-block">
                            <strong>{{ $errors->first('notesrelated') }}</strong>
                            </span>
                        @endif
                   </div>
                   <div class="col-md-2">
                       <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#basicExampleModalNotes" class="redius"><i class="fa fa-plus"></i></a>
                   </div>
           </div>
       </div>

       
        <div class="form-group">
            <div class="row">
                <label class="control-label col-md-3" style="width:110px; text-align:right; padding-left:0px!important;"><strong>Types of Note:</strong></label>
                <div class="col-md-5">
                    <select class="form-control" name="notesrelatedcat" id="notesrelatedcat1" required>
                        <option>Permenant Note</option>
                        <option>Temporary  Note</option>
                    </select>
               </div>
            </div>
       </div>
       
        <div class="form-group">
           <div class="row">
                <label class="control-label col-md-3" style="width:110px; text-align:right; padding-left:0px!important;"><strong>Note:</strong></label>
                   <div class="col-md-9" style="padding-right:0px;">
                  <textarea rows="3" cols="12" class="form-control" name="notes" id="notes1"> </textarea>
                   </div>
           </div>
       </div>
       
        <div class="form-group">
            <div class="row">
            <label class="control-label col-md-3" style="width:110px; text-align:right; padding-left:0px!important;"><strong>&nbsp;</strong></label>
                <div class="col-md-2">
                    <input class="btn_new_save btn-primary1" id="recInsert2" type="submit" name="submit" value="Save">
			    </div>
				<div class="col-md-2">
					<a class="btn_new_cancel" href="https://financialservicecenter.net/fac-Bhavesh-0554">Cancel</a>
               </div>
            </div>
       </div>
       
       
       </div>
      </div>
      <div class="modal-footer">
       <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
      </div>
    </div>
                                                    
            </form>
        </div>
    </div>
    <!--Modal Notes Update End-->

    
    <!--Modal Starttttttttttttttttttttttt-->
    <div id="myModalAddrecordtax" class="modal fade" role="dialog">
        <div class="modal-dialog" style="width:1000px;">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Client - Income Tax Filing - (Form-1040) <span style="float:right;">Add Record</span></h4>
          </div>
          <div class="modal-body">
              <form class="form-horizontal" method="post" action="{{route('workrecord.storefederal')}}" enctype="multipart/form-data">
                  {{csrf_field()}} 
                  
                  @if(!empty($common->id))
                  <input type="hidden" name="client_id" value="<?php echo $common->id;?>">
                  @endif
                <div class="recordform">
                  
                    <table style="width:100%; max-width:520px; margin:0px auto;" class="taxtable">
                    <tr>
                        <td>
                            <label class="control-label labels">Filling Year</label>
                            @if($Incometaxfederal2>0)
                            <select class="form-control federalyear federalyear3" name="federalsyear" required>
                            @else
                            <select class="form-control federalyear" name="federalsyear" required>
                            @endif

                            
                            <option value="">Select</option>
                            
                            <?php
                                
                                $now=date('M-d-Y');
                                if('Jul-25-2021'==$now)
                                {
                                    $aa=date('Y')-2;
                                    $bb=date('Y')-1;
                                    ?>
                                    <option value="<?php echo $aa;?>"><?php echo $aa;?></option>
                                    <option value="<?php echo $bb;?>"><?php echo $bb;?></option>
                                    <?php
                                }
                                else
                                {
                                    $aa=date('Y')-1;
                                    ?>
                                    <option value="<?php echo $aa;?>"><?php echo $aa;?></option>
                                    <?php
                                }
                                
                            ?>
    
                            
                        </select>
                            
                        </td>
                        <td>
                             <label class="control-label labels">Tax Return</label>
                             
                             <select class="form-control taxation" name="federalstax" required>
                                <option value="">Select</option>
                                <option value="Extension">Extension </option>
                                <option value="Original">Original</option>
                                <option value="Amendment">Amendment</option>
                            </select>
                        </td>
                        <td style="width:150px;">
                            <label class="control-label labels">Due Date</label>
                             <input type="text" class="form-control duedate" name="federalsduedate" placeholder="Mar-15-2020" readonly/>
                        </td>
                    </tr>
                </table>
                
                    <table class="taxtable table table-bordered" style="margin-top:20px;">
                 <tr>
                     <td>
                         <label class="form-label">Federal</label>
                         
                     </td>
                      <td>
                         <label class="form-label">Form No.</label>
                          <input type="text" readonly class="form-control formno" name="federalsform" style="width:100px"/ required>
                         
                     </td>
                      <td>
                         <label class="form-label">Filling Method</label>
                          <select class="form-control filingmethod" name="federalsmethod" required>
                            <option value="">Select</option>
                            <option value="E-File">E-File</option>
                            <option value="Paperfile">Paperfile</option>
                        </select>
                         
                     </td>
                      <td>
                         <label class="form-label">Filling Date</label>
                         <input type="text" class="form-control fdate" id="fillingdate" name="federalsdate" placeholder="MM/DD/YYYY" required/>
                         
                     </td>
                      <td>
                         <label class="form-label">Status of Filling</label>
                         <select class="form-control" name="federalsstatus" required>
                            <option value="">Select</option>
                            <option value="Accepted">Accepted</option>
                            <option value="EF Processing">EF Processing</option>
                            <option value="Pending">Pending</option>
                            <option value="Rejected">Rejected</option>
                        </select>
                         
                         
                     </td>
                      <td style="width:100px">
                         <label class="form-label">File</label>
                          <input type="file" class="form-control" name="federalsfile" style="display:block!important;margin-top:0px;"/>
                     </td>
                     <td>
                         <label class="form-label">Note</label>
                           <textarea class="form-control" name="federalsnote"></textarea>
                         
                     </td>
                 </tr>
             </table>
             
                    <table class="taxtable table table-bordered" style="margin-top:20px;" id="taxationtable">
                    <tr>
                      <th>  <label class="form-label">Resi. State</label></th>
                      <th><label class="form-label">Form No.</label></th>
                      <th><label class="form-label">Filling Method</label></th>
                      <th><label class="form-label">Filling Date</label></th>
                      <th> <label class="form-label">Status of Filling</label></th>
                      <th> <label class="form-label">File</label></th>
                      <th><label class="form-label">Note</label></th>
                      <th></th>
                  </tr>
                  
                  
                 <tr>
                     <td>
                       @if(isset($_REQUEST['id'])&& $_REQUEST['id']!='')
                         <select class="form-control" name="statetax[]" id="statetax">
                             
                             <option>Select</option>
                             @foreach($datastate3 as $datastates)
                             <?php //print_r($datastates);exit;?>
                             <option value="{{$datastates->state}}">{{$datastates->state}}</option>
                             @endforeach
                         </select>
                         
                          
                          @endif
                         
                     </td>
                      <td>
                         
                          <input type="text" readonly class="form-control" name="stateformno[]" id="statefederalformno" style="width:100px"/>
                         
                     </td>
                      <td>
                        
                        <select class="form-control filingmethod" name="statemethod[]">
                            <option value="">Select</option>
                            <option value="E-File">E-File</option>
                            <option value="Paperfile">Paperfile</option>
                        </select>
                         
                     </td>
                      <td>
                         
                         <input type="text" class="form-control fdate" id="fillingdate" name="statedate[]" placeholder="MM/DD/YYYY"/>
                         
                     </td>
                      <td>
                       
                         <select class="form-control" name="statestatus[]">
                            <option value="">Select</option>
                            <option value="Accepted">Accepted</option>
                            <option value="EF Processing">EF Processing</option>
                            <option value="Pending">Pending</option>
                            <option value="Rejected">Rejected</option>
                        </select>
                         
                         
                     </td>
                      <td style="width:100px">
                        
                          <input type="file" class="form-control" name="statefile[]" style="display:block; margin-top:0px;"/>
                     </td>
                     <td>
                         
                           <textarea class="form-control" name="statenote[]"></textarea>
                         
                     </td>
                     <td><a href="#" class="btn btn-primary add-rowform"><i class="fa fa-plus"></i></a></td>
                 </tr>
             </table>
             
                    <div class="row form-group saves ">
                    <label class="col-md-4 control-label">&nbsp;</label>
                    <div class="col-md-2">
                        <input type="submit" class="btn_new_save primary savebutton" value="Save">
                    </div>
                     <div class="col-md-2">
                        <a href="#" class="btn_new_cancel" data-dismiss="modal">Cancel</a>
                    </div>
                   
                </div>
                
                
             
                <input type="hidden" name="client_taxation_id" value="<?php if(isset($common->id)!=''){echo $common->id;}?>">
                <div class="row form-group">
                    
                    <div class="col-md-4 federals" style="display:none;">
                         
                    </div>
                    
                    <div class="col-md-4 states" style="display:none;">
                         <select class="form-control statesyear" name="statesyear">
                            <option value="">Select</option>
                             <?php
                                $aa=date('Y')-1;
                                
                            ?>
                            <option value="<?php echo $aa;?>"><?php echo $aa;?></option>
                            
                            
                        </select>
                    </div>
                </div>
                 <div class="row form-group">
                    <label class="col-md-3 control-label labels" style="display:none;">Tax Return</label>
                    <div class="col-md-4 federals" style="display:none;">
                        
                    </div>
                    <div class="col-md-4 states" style="display:none;">
                        <select class="form-control taxation2" name="statestax">
                           <option value="">Select</option>
                            <option value="Extension">Extension </option>
                            <option value="Original">Original</option>
                            <option value="Amendment">Amendment</option>
                    </select>
                    </div>
                </div>
                  <div class="row form-group">
                    <label class="col-md-3 control-label labels"  style="display:none;">Form No.</label>
                    <div class="col-md-4 federals" style="display:none;">
                       
                    </div>
                  
                    <div class="col-md-4 states" style="display:none;">
                        <input type="text" class="form-control formno2" name="statesformno" readonly/>
                    </div>
                </div>
                  <div class="row form-group">
                    <label class="col-md-3 control-label labels" style="display:none;">Due Date</label>
                    <div class="col-md-4 federals" style="display:none;">
                       
                    </div>
                 
                    <div class="col-md-4 states" style="display:none;">
                        <input type="text" class="form-control duedate2" name="statesduedate" placeholder="Mar-15-2020" readonly/>
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-md-3 control-label labels" style="display:none;">Filing Method </label>
                    <div class="col-md-4 federals" style="display:none;">
                       
                    </div>
                   
                    <div class="col-md-4 states" style="display:none;" >
                        <select class="form-control filingmethod2" name="statesmethod">
                            <option value="">Select</option>
                            <option value="E-File">E-File</option>
                            <option value="Paperfile">Paperfile</option>
                        </select>
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-md-3 control-label labels" style="display:none;">Filing Date</label>
                    <div class="col-md-4 federals" style="display:none;">
                        
                    </div>
                  
                    <div class="col-md-4 states" style="display:none;">
                        <input type="text" class="form-control fdate" id="fillingdate2" name="statesdate"/>
                    </div>
                </div>
                 <div class="row form-group statusof">
                    <label class="col-md-3 control-label labels" style="display:none;">Status of Filling </label>
                    <div class="col-md-4 federals" style="display:none;" >
                        
                    </div>
                   
                    <div class="col-md-4 states" style="display:none;">
                        <select class="form-control" name="statesstatus">
                            <option value="">Select</option>
                            <option value="Accepted">Accepted</option>
                            <option value="Rejected">Rejected</option>
                            <option value="Pending">Pending</option>
                        </select>
                    </div>
                </div>
                 <div class="row form-group">
                    <label class="col-md-3 control-label labels" style="display:none;">Add File </label>
                    <div class="col-md-4 federals" style="display:none;">
                      
                    </div>
                   
                    <div class="col-md-4 states" style="display:none;">
                       <input type="file" class="form-control" name="statesfile"/>
                    </div>
                </div>
                  <div class="row form-group">
                    <label class="col-md-3 control-label labels" style="display:none;">Note </label>
                    <div class="col-md-4 federals" style="display:none;">
                     
                    </div>
                    
                    <div class="col-md-4 states" style="display:none;">
                       <textarea class="form-control" name="statesnote"></textarea>
                    </div>
                </div>
                
                <div class="row form-group saves savebutton" style="display:none;">
                    <label class="col-md-5 control-label">&nbsp;</label>
                    <div class="col-md-2">
                        <input type="submit" class="btn_new_save primary" value="Save">
                    </div>
                     <div class="col-md-2">
                        <a href="#" class="btn_new_cancel" data-dismiss="modal">Cancel</a>
                    </div>
                   
                </div>
            </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>

        </div>
    </div> 
    <!--Modal Enddddddddddddddddddddddddddd-->
    <div class="modal fade" id="addNewFormation" role="dialog">
        <div class="modal-dialog modal-lg">        
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">
                        <span class="pull-left">GA-219</span>Corporation Renewal Info<span class="pull-right" style="margin-right: 15px;">ZK, Corporation</span>
                    </h4>
                </div>
                <div class="modal-body form-horizontal">
                    <div class="form-group row">
                        <label class="col-md-4 control-label text-right">Year : </label>
                        <div class="col-md-5">
                            <select class="form-control fsc-input selectyear">
                                <option value="">Select</option>
                                <option value="2020" style="display:none;">2020</option>
                                <option value="2021">2021</option>
                                <option value="2022">2022</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <input type="hidden" name="formation_client_id" value="81">
                        <label class="col-md-4 control-label text-right">Renew For : </label>
                        <div class="col-md-5">
                            <select class="form-control fsc-input" name="formation_yearbox" id="formation_yearbox11" onchange="getvalYear11();">
                                <option value="">Select</option>
                                <option value="1">1 Year</option>
                                <option value="2">2 Year</option>
                                <option value="3">3 Year</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-4 control-label text-right">Renew Year : </label>
                        <div class="col-md-5">
                            <input type="text" class="form-control showw" name="formation_yearvalue" id="" required="" readonly="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-4 control-label text-right">Paid By : </label>
                        <div class="col-md-5">
                            <select class="form-control fsc-input paidby2" name="record_paid_by">
                                <option value="">Select</option>
                                <option value="Client">Client</option>
                                <option value="FSC">FSC</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-4 control-label text-right">Status : </label>
                        <div class="col-md-5">
                            <select class="form-control" name="record_status" id="record_status" required="">
                                <option value="">Select</option>
                                <option value="Active Owes Curr. Yr. AR">Active Owes Curr. Yr. AR</option>
                                <option value="Active Compliance">Active Compliance</option>
                                <option value="Active Noncompliance">Active Noncompliance</option>
                                <option value="Admin. Dissolved">Admin. Dissolved</option>
                                <option value="Dis-Cancel-Termin">Dis-Cancel-Termin</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-4 control-label text-right">Annually Receipt : </label>
                        <div class="col-md-6">
                            <label class="file-upload btn btn-primary">
                            <input type="file" class="form-control" name="annualreceipt" style="display:none;">Browse for file ... </label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-4 control-label text-right">Officer : </label>
                        <div class="col-md-6">
                            <label class="file-upload btn btn-primary">
                            <input type="file" class="form-control" name="formation_work_officer" style="display:none;">Browse for file ... </label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-4 control-label text-right">Note : </label>
                        <div class="col-md-7">
                            <textarea class="form-control" name="record_note" id="record_note" style="height:70px;" requiired=""></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4"></div>
                        <div class="col-md-2" style=" padding-right:3px;">
                            <input class="btn_new_save btn-primary1 primary1" type="submit" id="primary1" name="submit" value="Save">
                        </div>
                        <div class="col-md-2" style="padding-left:3px;">
                            <a class="btn_new_cancel" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

   
   <script>
   $( function() {
     $(".fdate").datepicker({
                        autoclose: true
                    });
     
     
     
    } );

   $(document).ready(function()
    {
        
        
        $('.federalyear3').change(function()
        {
            var this1=$(this).val();
            var aa='<?php echo $aa=date('Y')-1;?>';
            if(this1 == aa)
            {
                $(".savebutton").hide();
                alert('This year already exists!');
            }
            else
            {
                $(".savebutton").show();
            }
        });
        
        $('.federalyear').change(function()
        {
             
             var this1=$(this).val();
             var aa='<?php echo $aa=date('Y')-1;?>';
             
             if(this1 == aa)
             {
                 $(".duedate").val('Jul-15-<?php echo $aa+1;?>');
                 
             }
            
            
            
            //  if(this1 == 'Extension')
            //  {
            //      $("select option[value='Extension']").hide();
            //      $(".duedate").val('Mar-15-<?php echo $aa+1;?>');
                 
            //  }
            // else if(this1 == bb)
            //  {
            //      $("select option[value='Extension']").hide();
            //      $(".duedate").val('Mar-15-<?php echo $aa+1;?>');
            //  }
            //  else if(this1 == cc)
            //  {
            //      $("select option[value='Extension']").show();
            //      $(".duedate").val('Mar-15-<?php echo $aa+1;?>');
            //  }
      
        });
      
      
        $('.statesyear').change(function()
        {
             
             var this1=$(this).val();
             //alert(this1);
             var aa='<?php echo $aa=date('Y');?>';
             var bb='<?php echo $bb=date('Y')+1;?>';
             var cc='<?php echo $cc=date('Y')+2;?>';
             if(this1 == aa)
             {
                 $("select option[value='Extension']").hide();
                 $(".duedate2").val('Jul-15-<?php echo $aa+1;?>');
                 
             }
            else if(this1 == bb)
             {
                 $("select option[value='Extension']").hide();
                 $(".duedate2").val('Jul-15-<?php echo $bb+1;?>');
             }
             else if(this1 == cc)
             {
                 $("select option[value='Extension']").show();
                 $(".duedate2").val('Jul-15-<?php echo $cc+1;?>');
             }
      
        });
      
      
        $('.taxation').change(function()
        {
              var businessid='<?php if(isset($_REQUEST['id']) && $_REQUEST['id'] !='') { echo $common->business_id; }?>';
            
          var thiss=$(this).val();
          if(thiss == 'Extension')
          {
              if(businessid =='6')
              {
                  $('.formno').val('Form-4868');
              }
              else
              {
              $('.formno').val('Form-7004');    
              }
              
              $("select option[value='E-File']").show();
          }
          
          else if(thiss == 'Original')
          {
              $('.formno').val('Form-1040');
              $("select option[value='E-File']").show();
          }
          else if(thiss == 'Amendment')
          {
              $('.formno').val('Form-1040X');
              $("select option[value='E-File']").hide();
                     
          }
        });
        
        $('.taxation2').change(function()
        {
            var businessid='<?php if(isset($_REQUEST['id']) && $_REQUEST['id'] !='') { echo $common->business_id; }?>';
            var thiss=$(this).val();
          if(thiss == 'Extension')
          {
               if(businessid =='6')
              {
                  $('.formno').val('Form-4868');
              }
              else
              {
              $('.formno').val('Form-7004');    
              }
              $("select option[value='E-File']").show();
          }
          
          else if(thiss == 'Original')
          {
              $('.formno2').val('Form-1040');
              $("select option[value='E-File']").show();
          }
          else if(thiss == 'Amendment')
          {
              $('.formno2').val('Form-1040X');
              $("select option[value='E-File']").hide();
                     
          }
        });
        
    
        $('.filingmethod').change(function()
        {
          var thiss=$(this).val();
          if(thiss == 'Paperfile')
          {
              $('.statusof').hide();
          }
          
          else
          {
              $('.statusof').show();
          }
        });
        
        $('.filingmethod2').change(function()
        {
          var thiss=$(this).val();
          if(thiss == 'Paperfile')
          {
              $('.statusof2').hide();
          }
          
          else
          {
              $('.statusof2').show();
          }
        });
        
        
        var cnt=1;
            $(".add-rowform").click(function()
            {
                cnt++;
                //$('').show();
                //var aa=$('.statetax2').html();
                var markup = ' <tr><td><select class="form-control statetax'+cnt+'" name="statetax[]" id="statetax'+cnt+'"><option>Select</option> @foreach($datastate2 as $datastate3)<option value="{{$datastate3->state}}">{{$datastate3->state}}</option>@endforeach</select></td><td><input type="text" readonly class="form-control formno" name="stateformno[]" id="statefederalformno'+cnt+'" style="width:100px"/></td><td><select class="form-control filingmethod" name="statemethod[]"><option value="">Select</option><option value="E-File">E-File</option><option value="Paperfile">Paperfile</option></select></td><td><input type="text" class="form-control fdate'+cnt+'" id="fillingdate" name="statedate[]" id="federaldate'+cnt+'" placeholder="MM/DD/YYYY"/></td><td><select class="form-control" name="statestatus[]" id="federalstatus'+cnt+'"><option value="">Select</option> <option value="Accepted">Accepted</option><option value="EF Processing">EF Processing</option><option value="Pending">Pending</option><option value="Rejected">Rejected</option></select></td><td style="width:100px"><input type="file" class="form-control" name="statefile[]" id="federalfile'+cnt+'" style="display:block; margin-top:0px;"/></td> <td><textarea class="form-control" name="statenote[]" id="federalnote'+cnt+'"></textarea></td><td><a href="#" class="btn btn-danger removetrrow"><i class="fa fa-minus"></i></a></td></tr>';
                $("#taxationtable tbody").append(markup);
            
                 $( function()
                 {
                    $('.fdate'+cnt).datepicker({
                        autoclose: true
                    });
                    
                });             
        
                $('.statetax'+cnt).on('change', function()
                {
                        var statetaxval=$('.statetax'+cnt).val();
                        $.get('{!!URL::to('getTaxstatedata')!!}?statetaxval='+statetaxval,function(data)
                        {  
                            if(data=='')
                            {
                               $('#statefederalformno'+cnt).val(); 
                            }
                            else
                            {
                                $('#statefederalformno'+cnt).val(data.taxform);
                            }
                            
                        });
               });
           
            });
      
    });
   
   
    $(document).ready(function()
    {
        $('.type_user').on('change', function()
        {   
             //otherid,clientid,employeeuserid,vendorid
             //Client,EE-User,Vendor,Other
             var thisss=$('.type_user').val();
             if(thisss =='Client')
             {
                 $('#clientid').show();
                 $('#employeeuserid').hide();
                 $('#vendorid').hide();
                 $('#otherid').hide();
             }
             else if(thisss =='EE-User')
             {
                 $('#clientid').hide();
                 $('#employeeuserid').show();
                 $('#vendorid').hide();
                 $('#otherid').hide();
             }
             else if(thisss =='Vendor')
             {
                 $('#clientid').hide();
                 $('#employeeuserid').hide();
                 $('#vendorid').show();
                 $('#otherid').hide();
             }
             else if(thisss =='Other')
             {
                 $('#clientid').hide();
                 $('#employeeuserid').hide();
                 $('#vendorid').hide();
                 $('#otherid').show();
             }
        });
        
        $('.notetype_user').on('change', function()
        {   
             //otherid,clientid,employeeuserid,vendorid
             //Client,EE-User,Vendor,Other
             var thisss=$('.notetype_user').val();
             if(thisss =='Client')
             {
                 $('#noteclientid').show();
                 $('#noteemployeeuserid').hide();
                 $('#notevendorid').hide();
                 $('#noteotherid').hide();
             }
             else if(thisss =='EE-User')
             {
                 $('#noteclientid').hide();
                 $('#noteemployeeuserid').show();
                 $('#notevendorid').hide();
                 $('#noteotherid').hide();
             }
             else if(thisss =='Vendor')
             {
                 $('#noteclientid').hide();
                 $('#noteemployeeuserid').hide();
                 $('#notevendorid').show();
                 $('#noteotherid').hide();
             }
             else if(thisss =='Other')
             {
                 $('#noteclientid').hide();
                 $('#noteemployeeuserid').hide();
                 $('#notevendorid').hide();
                 $('#noteotherid').show();
             }
        });
    });
</script>
<script type="text/javascript">
    $(".notesID").click(function () 
        {
            var ids = $(this).attr('data-id');
            //alert(ids);
            $("#notesid").val(ids);
            $('#notesModal').modal('show');
            //console.log(ids);           
            $.get('{!!URL::to('getNotesemp')!!}?documentid='+ids, function(data)
            {  
                //console.log(data);exit;
                if(data == "")
                {
                    
                }
                else
                {
                    //console.log(data.id);exit;
                    $('#NoteID').val(data.id);
                    $('#creattiondate1').val(data.creattiondate);
                    $('#noteday1').val(data.noteday);
                    $('#notetime1').val(data.notetime);
                    $('#notetype_user1').val(data.notetype_user);
                    $('#noteclientid1').val(data.noteclientid);
                    $('#noteemployeeuserid1').val(data.noteemployeeuserid);
                    $('#notevendorid1').val(data.notevendorid);
                    $('#noteotherid1').val(data.noteotherid);
                    $('#noterelated1').val(data.noterelated);
                    $('#notesrelatedcat1').val(data.notesrelatedcat);
                    $('#notes1').val(data.notes);
                    
                    
                       
                                 var thisss=$('#notetype_user1').val();
                                 if(thisss =='Client')
                                 {
                                     $('.noteclientid2').show();
                                     $('.noteemployeeuserid2').hide();
                                     $('.notevendorid2').hide();
                                     $('.noteotherid2').hide();
                                 }
                                 else if(thisss =='EE-User')
                                 {
                                     $('.noteclientid2').hide();
                                     $('.noteemployeeuserid2').show();
                                     $('.notevendorid2').hide();
                                     $('.noteotherid2').hide();
                                 }
                                 else if(thisss =='Vendor')
                                 {
                                     $('.noteclientid2').hide();
                                     $('.noteemployeeuserid2').hide();
                                     $('.notevendorid2').show();
                                     $('.noteotherid2').hide();
                                 }
                                 else if(thisss =='Other')
                                 {
                                     $('.noteclientid2').hide();
                                     $('.noteemployeeuserid2').hide();
                                     $('.notevendorid2').hide();
                                     $('.noteotherid2').show();
                                 }
                    
                }
                

                
            });
        });

    $(".conversationID").click(function () 
        {
            var ids = $(this).attr('data-id');
            //alert(ids);
            $("#conversationid").val(ids);
            $('#conversationModal').modal('show');
            //console.log(ids);           
            $.get('{!!URL::to('getConversationdata')!!}?documentid='+ids, function(data)
            {  
                //console.log(data);exit;
                if(data == "")
                {
                    
                }
                else
                {
                    //console.log(data.id);exit;
                    $('#CovID').val(data.id);
                    $('#creattiondate1').val(data.creattiondate);
                    $('#day1').val(data.day);
                    $('#time1').val(data.time);
                    $('#type_user1').val(data.type_user);
                    $('#clientid1').val(data.clientid);
                    $('#employeeuserid1').val(data.employeeuserid);
                    $('#vendorid1').val(data.vendorid);
                    $('#otherid1').val(data.otherid);
                    $('#conrelatedname1').val(data.conrelatedname);
                    $('#condescription1').val(data.condescription);
                    $('#connotes1').val(data.connotes);
                    
                    
                       
                                 var thisss=$('#type_user1').val();
                                 if(thisss =='Client')
                                 {
                                     $('.clientid2').show();
                                     $('.employeeuserid2').hide();
                                     $('.vendorid2').hide();
                                     $('.otherid2').hide();
                                 }
                                 else if(thisss =='EE-User')
                                 {
                                     $('.clientid2').hide();
                                     $('.employeeuserid2').show();
                                     $('.vendorid2').hide();
                                     $('.otherid2').hide();
                                 }
                                 else if(thisss =='Vendor')
                                 {
                                     $('.clientid2').hide();
                                     $('.employeeuserid2').hide();
                                     $('.vendorid2').show();
                                     $('.otherid2').hide();
                                 }
                                 else if(thisss =='Other')
                                 {
                                     $('.clientid2').hide();
                                     $('.employeeuserid2').hide();
                                     $('.vendorid2').hide();
                                     $('.otherid2').show();
                                 }
                    
                }
                

                
            });
        });
    
    
    
    

        $(".passingID").click(function ()
        {
            var ids = $(this).attr('data-id');
            //alert(ids);
            $("#idkl").val(ids);
            $('#myModal').modal('show');
            //console.log(ids);           
            $.get('{!!URL::to('getClientDocumentdata')!!}?documentid='+ids, function(data)
            {  
                console.log(data);
                if(data == "")
                {
                    
                }
                else
                {
                    //$('#county').html(data);
                    //console.log(data.id);exit;
                    $('#id').val(data.id);
                    $('#filename_idss').val(data.filename_id);
                    $('#client_id').val(data.client_id);
                    $('#documentsname').val(data.documentsname);
                    $('#clientdocument').val(data.clientdocument);
                    $('#clientdocument22').html(data.clientdocument);
                }
                

                
            });
        });
   </script>




<script type="text/javascript">
$(document).ready(function()
 {
     
     
     $("#license_gross").on("input", function(evt) {
		var self = $(this);
		self.val(self.val().replace(/[^\d].+/, ""));
		if ((evt.which < 48 || evt.which > 57)) 
		{
			evt.preventDefault();
		}});  

    $("#license_fee").on("input", function(evt) {
		var self = $(this);
		self.val(self.val().replace(/[^\d].+/, ""));
		if ((evt.which < 48 || evt.which > 57)) 
		{
			evt.preventDefault();
		}});  

     
     
  (function($) {
    var minNumber = -100;
    var maxNumber = 100;
      $('.spinner .btn:first-of-type').on('click', function() {
        if ($('.spinner input').val() == maxNumber) {
          return false;
        } else {
          $('.spinner input').val(parseInt($('.spinner input').val(), 10) + 5 + '%');
        }
      });

  $('.txtinput_1').on("blur", function() {
    var inputVal = parseFloat($(this).val().replace('%', '')) || 0
    if (minNumber > inputVal) {
      //inputVal = -100;
    } else if (maxNumber < inputVal) {
      //inputVal = 100;
    }
    $(this).val(inputVal + '.00');
  });

      $('.spinner .btn:last-of-type').on('click', function() {
        if ($('.spinner input').val() == minNumber) {
          return false;
        } else {
          $('.spinner input').val(parseInt($('.spinner input').val(), 10) - 5 + '%');
        }
      });
})(jQuery);

    }); 

</script>


<script>
function getvalYear(){
    var yearboxval = document.getElementById("formation_yearbox").value;
    var yearvaluesub = document.getElementById("formation_yearvalue").value;
   // alert(yearboxval);
    if(yearboxval==''){
       $("#formation_yearvalue").val('');
    }
    else if(yearboxval==1)
    {
        $("#formation_yearvalue").val('2020');
        $("#formation_amount").val('$50.00');
    }
    else if(yearboxval==2)
    {
        $("#formation_yearvalue").val('2020,2021');
        $("#formation_amount").val('$100.00');
    }
    else if(yearboxval==3)
    {
        $("#formation_yearvalue").val('2020,2021,2022');
        $("#formation_amount").val('$150.00');
    }
}
</script>


<script>
$(document).ready(function(){

 $('#country_name').keyup(function(){ 
        var query = $(this).val();
        if(query != '')
        { 
         var _token = $('input[name="_token"]').val();
         $.ajax({
          url:"{{ route('workstatus.fetchrec') }}",
          method:"POST",
          data:{query:query, _token:_token},
          success:function(data){
           $('#countryList').fadeIn();  
                    $('#countryList').html(data);
          }
         });
        }
    });

    $(document).on('click', 'tr', function(){  
        $('#country_name').val($(this).text());  
        $('#countryList').fadeOut();  
    });  

});




</script>
@endsection

