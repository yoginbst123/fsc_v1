@extends('employee.layouts.app')
@section('main-content') 
<style>
   @charset "utf-8";
   /* CSS Document */
   /* ------- Standar Styles ------- */
   .lunch-clock-time-head {
   width: 100%;
   float: left;
   background: #286DB5;
   }	
   .btn.focus, .btn:focus, .btn:hover {
   color: #1a1a1a;
   text-decoration: none;
   transition: 0.3s;
   }
   .lunch-clock-time {
   width: 100%;
   float: left;
   background: #4a95e4;
   padding: 10px 10px;
   text-align: center;
   }
   .lunch-clock-time-head h2 {
   font-size: 18px;
   color: #fff;
   box-shadow: -1px 4px 8px rgba(0, 0, 0, 0.1);
   margin: 0;
   padding: 12px 0;
   text-align: center;
   }
   .lunch-clock-time-text {
   width: 100%;
   float: left;
   }
   .lunch-clock-schedule {
   width: 100%;
   float: left;
   background: #FFF;
   padding: 6px 10px;
   margin: 14px 0;
   }
   .lunch-clock-schedule h3 {
   font-size: 16px;
   color: #4b77be;
   line-height: 24px;
   margin: 5px 0 10px 0;
   text-align: center;
   }
   .lunch-clock-schedule-left {
   width: 50%;
   float: left;
   }
   .lunch-clock-time p {
   font-size: 15px;
   line-height: 28px;
   text-align: center;
   margin: 4px 0;
   }
   .lunch-clock-schedule-right {
   width: 50%;
   float: left;
   }
   .lunch-clock-time-buttons {
   width: 100%;
   float: left;
   }
   .lunch-clock-time-buttons {
   width: 100%;
   float: left;
   }
   .lunch-clock-time-buttons button:disabled {
   display: none;
   }
   .lunch-clock-time-buttons button {
   width: 150px;
   margin: 0 10px 0 0;
   padding: 6px 0 !important;
   font-size: 15px;
   color: #fff;
   text-transform: uppercase;
   }
   .lunch-clock-time-buttons button.lun-btn-out {
   background: #e61212;
   }
   .lunch-clock-time-buttons button.note-btn {
   background: #e4914a;
   margin: 10px 0;
   }
   .lunch-clock-time-buttons button {
   width: 150px;
   margin: 0 10px 0 0;
   padding: 6px 0 !important;
   font-size: 15px;
   color: #fff;
   text-transform: uppercase;
   }
   .lunch-clock-time-buttons button {
   width: 132px;
   margin: 0 10px 0 0;
   padding: 6px 0 !important;
   font-size: 15px;
   color: #fff;
   text-transform: uppercase;
   }
   .lunch-clock-time-buttons button.lun-btn-in {
   background: #0ba20b;
   }
</style>
<div class="content-wrapper">
<div class="page-title">
   <h1>Dashboard</h1>
</div>
<section class="content">
   <!-- Info boxes -->
   <div class="row">
      <div class="col-md-3 col-sm-6 col-xs-12">
         <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>
            <div class="info-box-content"> <span class="info-box-text">CPU Traffic</span> <span class="info-box-number">90<small>%</small></span> </div>
            <!-- /.info-box-content --> 
         </div>
         <!-- /.info-box --> 
      </div>
      <!-- /.col -->
      <div class="col-md-3 col-sm-6 col-xs-12">
         <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-google-plus"></i></span>
            <div class="info-box-content"> <span class="info-box-text">Likes</span> <span class="info-box-number">41,410</span> </div>
            <!-- /.info-box-content --> 
         </div>
         <!-- /.info-box --> 
      </div>
      <!-- /.col --> 
      <!-- fix for small devices only -->
      <div class="clearfix visible-sm-block"></div>
      <div class="col-md-6 col-sm-6 col-xs-12">
         <!--<div class="timing">
            <div class="col-md-12 lunch-clock-time">
               <div class="lunch-clock-time-head">
                  <h2>Time Log</h2>
               </div>
               <div class="lunch-clock-time-text">
                  <div class="lunch-clock-schedule">
                     <h3><span>Schedule Time :</span> <span>{{$schedule->schedule_in_time}} To {{$schedule->schedule_out_time}}</span></h3>
                     <div class="lunch-clock-schedule-left">
                        <p><span>Time In : @if(empty($emp_schedule->emp_in)) 00:00 @else{{$emp_schedule->emp_in}} @endif</span></p>
                     </div>
                     <div class="lunch-clock-schedule-right">
 <p><span>Time Out : @if(empty($emp_schedule->emp_in)) 00:00 @else{{$emp_schedule->emp_out}} @endif</span></p>
                     </div>
                  </div>
               </div>
               <div class="lunch-clock-time-buttons">
                  <div class="lunch-clock-time-buttons">
<form method="post" action="{{route('employee.store')}}" class="form-horizontal" id="businessname" name="businessname" enctype="multipart/form-data">{{csrf_field()}}
<input type="hidden" name="emp_in_date" value="{{date('m-d-y')}}">
<input type="hidden" name="emp_in" value="{{date('H:i:m')}}">
<input type="hidden" name="employee_id" value="{{Auth::user()->user_id}}">
<input class="btn btn-success icon-btn" type="submit" name="submit" value="Start">
</form>
                    
                     <a class="btn lun-btn-out btn-danger" href="#">End</a>
                     <a class="btn note-btn btn-primary"  href="#">Note</a>
                     
                  </div>
               </div>
            </div>
          
         </div>-->
         <!-- /.col -->
         
         <!-- /.col --> 
      </div>
      <!-- /.row --> 
      <!-- /.row --> 
      <!-- Main row -->
      
      <!-- /.row --> 
</section>
</div>
@endsection

