
@extends('fscemployee.layouts.app')
@section('main-content')
<style>
    .search-btn {
    position: absolute;
    top: 46px;
    right: 16px;
    background:transparent;
    border:transparent;
}
.new_images_sec .col-md-4{position:relative;}
.new_images_sec .col-md-4 .arrow{position:absolute; top:16px; right:-6px;}
.new_images_sec .col-md-4{position:relative;}
.new_images_sec .col-md-4 img{height:53px!important;}
.new_images_sec .col-md-4 .arrow{position:absolute; top:16px; right:-6px;}
.new_images_sec .col-md-4.lastimgbox .arrow{position:absolute; top:10px; left:-6px;}
.searchboxmain{float: left;  display: FLEX;  margin-top: -11px; justify-content: space-between;}
.searchboxmain .form-control{    margin-right: 10px!important;
    margin-top: 5px;
    height: 34px;}
.clear{clear:both;}
.page-title{position:relative;}
.page-title .btn.btn-success{position:absolute; top:5px; right:15px;}
</style>

<?php 
if(isset($common->filename)!='')
{
?>
<style>
.page-title h1{float:left; margin-left:17%;}

</style>

<?php 
}
?>

<style>
/**************** dropdown list ********************/    
    
#countryList{margin:40px 0px 0px 0px;
    padding:0px 0px;
    border: 0px solid #ccc;
    max-height: 200px;
    overflow: auto; position: absolute;
    width: 360px;
    z-index: 99999;}

#countryList li {
    border-bottom: 1px solid #025b90;
    background: #ef7c30;
}
#countryList li a{padding:0px; display:block; color:#fff!important; height:40px;}
#countryList li a span.clientalign{display: flex;
    width: 100px;
    float: left;
    line-height: 15px;
    padding: 4px 0px;
    flex-direction: row;
    justify-content: flex-start;
    align-items: center;
    align-content: center;
    min-height: 40px; background:#038ee0;
    margin-left: 0;
    padding-left: 5px;}
    
    #countryList li a span.entityname{display: flex;
    width:230px;
    float: left;
    line-height: 15px;
    padding: 4px 0px; font-size:13px;
    flex-direction: row;
    justify-content: flex-start;
    align-items: center;
    align-content: center;
    min-height: 38px;
    margin-left: 10px;}
.nav.nav-tabs li{width:15.8%!important;}
.clear{clear:both;}
.mt25{margin-top:25px;}
.text-center{text-align:center!important;}
.text-right{text-align:right!important;}
.table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th{padding:6px 8px 8px 8px!important;}
.pointernone{pointer-events:none}
.pad20{padding:20px;}
.nav-tabs > li.active > a.yel, .nav-tabs > li.active > a.yel:focus, .nav-tabs > li.active > a.yel:hover {
       cursor:default;
}
.nav-tabs > li{
    margin: 0px 0 0 8px;
}
.nav > li > a.yel:hover, .nav > li > a.yel:active, .nav > li > a.yel:focus {
    border-color:#000 !important;
    color:#000 !important;
    background:#ffff99;
    border-radius: 5px !important;
}
.nav-tabs {
    padding: 12px;
    border: 1px solid #3598dc !important;
}
.btnaddmore{background: #337ab7; display:inline-block; margin-bottom:5px;
    padding: 6px 10px;
    color: #fff;
    border-radius: 4px}
.btnremove{background: #ff0000;
    padding: 6px 10px;
    color: #fff;
    border-radius: 4px}
.padzero{padding:0px!important;}
.officermainbox{border:1px solid #ccc; padding:20px; margin-bottom:30px;}
.table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
    padding: 6px 8px 8px 8px !important;
}
.table-bordered, .table-bordered > tbody > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > td, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > thead > tr > th
{border-color:#ccc!important;}
.custom-file-upload {
  border: 1px solid #ccc;
  display: inline-block;
  padding: 6px 12px;
  cursor: pointer;
}
.card ul li{width:24% !important;}
   .card ul.test li a{background:#ffcc66 !important;}
   .card ul.test li.active a{background:#00a0e3 !important;}
   .card ul li a{display: block;width: 100%;color: #333;text-transform: capitalize;background: linear-gradient(180deg, #fdff9a 30%, #e3e449 70%);border: 1px solid #979800 !important;}
   .card ul li a:hover{color: #333;
   background: linear-gradient(180deg, #fbff37 30%, #d3d40e 70%);border: 1px solid #333 !important;}
   .card ul li.active{color: #333;background: linear-gradient(180deg, #fbff37 30%, #d3d40e 70%);border: 1px solid #333 !important;}
   .card ul li a.active, .card ul li a:hover, .card ul li.active a:hover{color: #fff!important;background: #12186b!important;border: 1px solid #12186b !important;}
   .card .nav-tabs{border:0px!important;}
.feeschargesbox .form-control{text-align:right;}
.hrdivider{border-bottom: 2px solid #ccc!important; width: 100%; height: 1px; margin-top: 33px; }
.hrdivider2{margin-top: 14px;   margin-bottom: 30px; border-bottom: 2px solid #ccc!important; width: 100%; height: 1px;}
.officerchange .form-control{background:#fff!important;}
.add-row{background: #007bff; padding: 3px 5px;  color: #fff;  border-radius: 3px; cursor: pointer;}
.delete-row{background:#dc3545; padding: 3px 5px;  color: #fff;  border-radius: 3px; cursor: pointer;}
.nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover {
    color: #FFF !important;
    border: 1px solid #12186b!important;
    background: #12186b!important;
    cursor: default;
    border-radius: 8px;
}
.btn-default {
    border-color: #286090;
}
.btn-success{
background:#e67300 !important;    
}
.btn-success:hover{
background:#e67300 !important;    
}
.newcmpname {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    height: 77px;
}
.btn-group.btn-toggle{
    margin-top:-5px;
}
.toggleswitch{float:left; margin-right:15px;}
.newprospects label{text-align:right; padding-top:6px; text-align:right!important; width:100%;}
.newprospects{ padding:15px;}
.imgicon{background: #fff; display: block;  width:35px; margin-top: -5px;  margin-left: 10px; float:left; margin-right:10px; border-radius:2px; padding:3px; border:1px solid #12186b;}
.imgicon img{max-width:100%;}
</style>
<div class="content-wrapper">
   
	<div class="page-title">
	    <span class="imgicon">
	        <img src="https://www.financialservicecenter.net/public/images/2.png" alt="img" style="width:50px !important;float:left;">
        </span>
        <a href="" class="btn btn-warning pull-right" style="margin: -5px 5px 0px -4px;">New Client</a>
	     <div class="btn-group btn-toggle toggleswitch">
	               
	     @if(empty($common->filename))
            <button class="btn btn-default" onClick="nopress();" style="margin-right:7px;">New prospect work</button>
         @endif    
            <button class="btn btn-primary active"  onClick="noonepress();">New work</button>
          </div>
  
	    <div class="searchboxmain">
	         <input type="text"  name="search" id="country_name" class="form-control" placeholder="Search Client">	 
                 {{ csrf_field() }}
                 <ul id="countryList"></ul>
                  <a class="btn-action btn-view-edit btn-primary" style="background: #367fa9 !important;padding: 8px 15px;margin-top: 5px; margin-bottom:5px;" href="https://financialservicecenter.net/fscemployee/worknew" style="background: #367fa9 !important;padding: 10px 20px;">Reset</a>
	        
	    </div>
	                    
	<h1 style="text-align:center;font-size:22px !important;margin-right:200px !important;font-weight: 600;">New Work / New Prospect Work</h1>
	
		<div class="clearfix"></div>
	</div>
	@if(session()->has('success'))
        <div class="alert alert-success alert-dismissable">{{session()->get('success') }}</div>
    @endif
    	@if(session()->has('error'))
           <div class="alert alert-danger alert-dismissable">{{session()->get('error') }}</div>
    @endif

	<div  class="box box-success mbtmzero col-md-12">
	    <!--<div class="btn btn-success pull-right" style="margin-top: 10px !important;margin-bottom: 10px;">-->
		   <!-- <a style="color:#fff;" >Add New Client</a></div>-->
		    <div class="clearfix"></div>
	    <div class="newprospects" style="display:none">
	        <form method="post" action="https://financialservicecenter.net/fscemployee/worknew/fetch2" enctype="multipart/form-data">
                <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                <input type="hidden" name="clientid" value="1">
	        
	            <div class="row form-group">
    	            <div class="col-md-3">
    	                <label class="control-label">Name : </label>
    	            </div>
    	            <div class="col-md-1">
    	                <select class="form-control" name="worknew_petname">
    	                    <option>Mr.</option>
    	                    <option>Mrs.</option>
    	                    <option>Miss.</option>
    	               </select>
    	            </div>
    	            
    	            <div class="col-md-2">
    	                <input type="text" name="worknew_fname" class="form-control" placeholder="First Name"/>
    	            </div>
    	            
    	            <div class="col-md-1">
    	                <input type="text" name="worknew_mname" class="form-control" placeholder="M"/>
    	            </div>
    	            
    	             <div class="col-md-2">
    	                <input type="text" name="worknew_lname" class="form-control" placeholder="Last Name"/>
    	            </div>
    	        </div>
	        
	            <div class="row form-group">
    	            <div class="col-md-3">
    	                <label class="control-label">Telephone</label>
    	            </div>
    	             <div class="col-md-3">
    	                <input type="tel" name="worknew_telephone" id="worknew_telephone" class="form-control" placeholder=""/>
    	            </div>
    	        </div>
	        
	            <div class="row form-group">
    	            <div class="col-md-3">
    	                <label class="control-label">Email</label>
    	            </div>
    	             <div class="col-md-3">
    	                <input type="email" name="worknew_email" class="form-control" placeholder=""/>
    	            </div>
    	        </div>
	        
	            <div class="row form-group">
                    <div class="col-md-3">
                        <label class="control-label">Work Category : <span class="star-required">*</span></label>
                    </div>  
                                       
                    <div class="col-md-3">
    					<select name="worknew_category" id=""  onchange="myFunction(this)" class="js-example-tags  form-control fsc-input category1 nametype">
        					<option value=''>---Select Work Category---</option>
        					@foreach($workcategory as $cate)
        					<option value='{{$cate->id}}' >{{$cate->category_name}}</option>
        					@endforeach
    					</select>
    				</div>
    				@if ($errors->has('worknew_category'))
    					<span class="help-block">
    						<strong>{{ $errors->first('worknew_category') }}</strong>
    					</span>
    				@endif
				   <div class="col-md-3"><a href="#" data-toggle="modal" data-target="#basicExampleModal" class="redius"><i class="fa fa-plus"></i></a>&nbsp;&nbsp;&nbsp; <a href="#" data-toggle="modal" data-target="#basicExampleModal3" class="redius"><i class="fa fa-minus"></i></a> </div>

				</div>
									
                <div class="row form-group resi_finance" id="" style="display:none;">
                    <div class="col-md-3">
                        <label class="control-label">Work Type :</label>
                    </div>
                    <div class="col-md-3"> 
                        <select class="form-control fsc-input"  name="worknew_type">
                            <option value="">Select</option>
                            <option value="Purchase">Purchase</option>
                            <option value="Refinance">Refinance</option>
                        </select>
                    </div>
                     
                </div>
                                
                <div class="row form-group com_finance" id="" style="display:none;">
                    <div class="col-md-3">
                        <label class="control-label">Work Type :</label>
                    </div>
                    <div class="col-md-3"> 
                            <select class="form-control fsc-input" name="worknew_type">
                                <option value="">Select</option>
                             <option value="Purchase">Purchase</option>
                           <option value="Refinance">Refinance</option>
                      </select>
                    </div>
                </div>
                                
                <div class="row form-group corp_llc" id="" style="display:none;">
                    <div class="col-md-3">
                                   
                        <label class="control-label">Work Type :</label>
                    </div>
                    <div class="col-md-3"> 
                        <select class="form-control fsc-input" name="worknew_type">
                            <option value="">Select</option>
                            <option value="Create New Corp/ LLC">Create New Corp/ LLC</option>
                            <option value="Update Corp / LLC">Update Corp / LLC</option>
                            <option value="Create Documents">Create Documents</option>
                        </select>
                    </div>
                </div>
                                
                <div class="row form-group license" id="" style="display:none;">
                    <div class="col-md-3">
                        <label class="control-label">Work Type :</label>
                    </div>
                    <div class="col-md-3"> 
                        <select class="form-control fsc-input" name="worknew_type">
                            <option value="">Select</option>
                            <option value="Renewal License">Renewal License</option>
                            <option value="New License">New License</option>
                            <option value="Update License">Update License</option>
                        </select>
                    </div>
                    
                </div>
                                
                <div class="row form-group insurance" id="" style="display:none;">
                    <div class="col-md-3">
                        <label class="control-label">Work Type :</label>
                    </div>
                    <div class="col-md-3"> 
                        <select class="form-control fsc-input" name="worknew_type">
                            <option value="">Select</option>
                            <option value="Life Insurance">Life Insurance</option>
                            <option value="Health Insurance">Health Insurance</option>
                            <option value="Auto Insurance">Auto Insurance</option>
                            <option value="Home Insurance">Home Insurance</option>
                            <option value="Business Insurance">Business Insurance</option>
                            <option value="Other Insurance">Other Insurance</option>
                        </select>
                    </div>
                </div>
                                
                <div class="row form-group">
                    <div class="col-md-3">
                        <label class="control-label">Work Priority :</label>
                    </div>
                    <div class="col-md-3"> 
                    <select class="form-control fsc-input" name="worknew_priority" required>
                        <option value="">Select</option>
                       <option value="Regular"> Regular</option>
                       <option value="Immediately">Immediately</option>
                       <option value="Urgent">Urgent</option>
                    </select>
                    </div>
                 </div>
                                     
                <div class="row form-group">
                                    <div class="col-md-3">
                                        <label class="control-label">Due Date :</label>
                                        </div>
                                        
                                         <div class="col-md-3"> 
                                            <input type="text" class="form-control datepicker"  name="worknew_duedate" readonly/>
                                         </div>
                                     </div>
                                
                <div class="row form-group">
                                    <div class="col-md-3">
                                        <label class="control-label">Responsible Person :</label>
                                        </div>
                                        
                                         <div class="col-md-3"> 
                                           <select class="form-control fsc-input" name="worknew_emp" required>
                                           <option value="">Select</option>
                                           <?php
                                           foreach($empname as $emp)
                                           {
                                           ?>
                                                <option value="<?php echo $emp->employee_id;?>"><?php echo $emp->firstName.' '.$emp->middleName.' '.$emp->lastName;?></option>
                                           <?php
                                           }
                                           ?>
                                        </select>
                                         </div>
                                     </div>
                            
                <div class="row form-group">
                                    <div class="col-md-3">
                                        <label class="control-label">Work To Do :</label>
                                        </div>
                                         <div class="col-md-6"> 
                                           <textarea class="form-control" name="worknew_details" style="height:100px;"></textarea>
                                         </div>
                                     </div>
                            
                <div class="row form-group">
                                    <div class="col-md-3">
                                        <label class="control-label text-right">Work Note :</label>
                                        </div>
                                         <div class="col-md-6"> 
                                           <textarea class="form-control" name="worknew_note" style="height:100px;"></textarea>
                                         </div>
                                     </div>
                                
	            <div class="row form-group">
	            <div class="col-sm-12">
										<div class="col-md-2 col-md-offset-3">											<input class="btn_new_save btn-primary" type="submit" value="Save"></div>
										<div class="col-md-2 row">	<a class="btn_new_cancel" href="https://financialservicecenter.net/fscemployee/employee">Cancel</a> 
										</div>
									</div>
	        </div>
	        
	        </form>
	    </div>
	@if(!empty($common->filename))
	<div class="row">
	    <div class="col-md-12">
	        <div>
	             <section class="content-header" style="margin-bottom:10px;padding-top:10px !important; height:90px;">
                        <div class="new-page-title" style="padding:0px;margin: -7px 0 7px 0;">
             <div class="new-page-title-tab">
                <div class="col-md-2 col-sm-2 col-xs-12">
                   <div class="new_client_id">
                      <p>{{$common->filename}} </p>
                   </div>
                </div>
                @if($common->business_id=='6')
                <div class="col-md-5 col-sm-5 col-xs-12">
                
                    <div class="new_company_name" style="margin-top:25px !important;">
                        <label> Name :</label> {{ucwords($common->nametype)}}   {{$common->first_name}} {{$common->middle_name}} {{$common->last_name}}  </div></div>
                      @else
             
                    <div class="col-md-5 col-sm-5 col-xs-12">
                        <div class="newcmpname">
                           <div class="new_company_name">
                              <label>Company Name :</label>
                              <p>{{$common->company_name}}</p>
                           </div>
                           <div class="new_company_name">
                              <label>Business Name :</label>
                              <p>{{$common->business_name}}</p>
                           </div>
                      </div>
                </div>
                @endif
                <div class="col-md-5 col-sm-5 col-xs-12">
                   <div class="new_images_sec">
                      @if(empty($common->business_cat_id)) 
                      <div class="col-md-12 col-sm-12 col-xs-12">
                         @else 
                         @if(empty($common->business_brand_category_id))
                         <div class="col-md-4 col-sm-12 col-xs-12">
                            @else 
                            <div class="col-md-4 col-sm-12 col-xs-12"> 
                               @endif
                               @endif
                               @foreach($business as $busi) 
                               @if($busi->id==$common->business_id)
                               <img src="{{url('public/frontcss/images/')}}/{{$busi->newimage}}" class="img-responsive"  style="height:53px!important;">
                              
                            </div>
                            @endif
                            @endforeach
                            @foreach($category as $cate)                                        
                            @if($common->business_cat_id==$cate->id)
                            @if(empty($common->business_brand_category_id))
                            <div class="arrow" style="float:left; margin-top:16px;"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></div>
                            <div class="col-md-4 col-sm-12 col-xs-12">
                               @else 
                             <!--  <div class="arrow">
                                  <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                               </div>-->
                               <div class="col-md-4 col-sm-12 col-xs-12"> 
                                  @endif
                                  
                                  <img src="{{url('public/category')}}/{{$cate->business_cat_image}}" alt="" class="img-responsive" style="height:53px!important;"/>
                               </div>
                               @endif
                               @endforeach
                               @foreach($cb as $bb1)                                     
                               @if($common->business_brand_category_id == $bb1->id)
                               
                               <div class="col-md-4 col-sm-12 col-xs-12 lastimgbox">
                                   <div class="arrow">
                                  <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                               </div>
                                  <img src="{{url('public/businessbrandcategory')}}/{{$bb1->business_brand_category_image}}" alt="" class="img-responsive" style="height:53px!important;"/>
                               </div>
                               @endif
                               @endforeach
                            </div>
                         </div>
                      </div>
                      </div>
       </section>
       
        <div style="background:#ffffff; border:1px solid #337ab7; width:97%; margin:0px 15px;">
            <div class="card-body col-md-offset-1" >
            	<div class="row"> 
                    <form method="post" action="https://financialservicecenter.net/fscemployee/worknew/fetch1" enctype="multipart/form-data">
                     <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                      <input type="hidden" name="clientsid" value="<?php echo $common->cid;?>">
               
            	                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('firstName') ? ' has-error' : '' }}{{ $errors->has('middleName') ? ' has-error' : '' }}{{ $errors->has('lastName') ? ' has-error' : '' }}">
                                    <!-- <div class="form-group" style="margin-top:15px;">
                                       <label class="col-lg-2 control-label"></label>
                                          <div class="row">
                                             <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                        <input type="radio" id="worknew_prospect" name="worknew_prospect" value="New Work" @if($common->worknew_prospect =='New Work') checked @endif>
                                        <label for="male">New Work</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="worknew_prospect" name="worknew_prospect" value="New Prospect" @if($common->worknew_prospect =='New Prospect') checked @endif>
                                        <label for="male">New Prospect</label>
                                             </div>
                                          </div>
                                       
                                    </div>
                                   !-->
                                    <div class="form-group" style="margin-top:20px;">
                                         <div class="row">
                                       <label class="col-lg-2 control-label text-right" style=" padding-right: 7px; margin-top: 7px;">Work Category : <span class="star-required">*</span></label>
                                         
                                            <!-- <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                <select class="form-control fsc-input" id="nametype" name="worknew_category" onchange="myFunction(this)" required>
                                                    <option value="">Select</option>
                                                   <option value="AccountingWork">Accounting Work</option>
                                                   <option value="TaxationWork">Taxation Work</option>
                                                   <option value="ResidentialFinance">Residential Finance</option>
                                                   <option value="CommercialFinance">Commercial Finance</option>
                                                   <option value="Corporation/LLC">Corporation / LLC</option>
                                                   <option value="License">License</option>
                                                   <option value="Insurance">Insurance</option>
                                                   <option value="Other Service">Other Service</option>
                                                </select>
                                             </div>!-->
                                             
                                             	<div class="col-md-3 ac" style="width: 34.1%;">
        											<div class="row">
        											  	<div class="col-md-12">
        													<select name="worknew_category" id=""  onchange="myFunction(this)" class="js-example-tags  form-control fsc-input category1 nametype">
        													<option value=''>---Select Work Category---</option>
        													@foreach($workcategory as $cate)
        													<option value='{{$cate->id}}' >{{$cate->category_name}}</option>
        													@endforeach
        													</select>
        												</div>
        											</div>
        											@if ($errors->has('worknew_category'))
                										<span class="help-block">
                											<strong>{{ $errors->first('worknew_category') }}</strong>
                										</span>
                									@endif
        										</div>
        											<div class="col-md-3"><a href="#" data-toggle="modal" data-target="#basicExampleModal" class="redius"><i class="fa fa-plus"></i></a>&nbsp;&nbsp;&nbsp; <a href="#" data-toggle="modal" data-target="#basicExampleModal3" class="redius"><i class="fa fa-minus"></i></a> </div>
                                      
                                          </div>
                                          </div>
                                          <div class="form-group resi_finance" id="" style="display:none;">
                                            <div class="row">
                                                <label class="col-lg-2 control-label text-right">Work Type :</label>
                                                 <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin"> 
                                                    <select class="form-control fsc-input"  name="worknew_type">
                                                        <option value="">Select</option>
                                                   <option value="Purchase">Purchase</option>
                                                   <option value="Refinance">Refinance</option>
                                                </select>
                                                 </div>
                                             </div>
                                        </div>
                                        
                                         <div class="form-group com_finance" id="" style="display:none;">
                                            <div class="row">
                                                <label class="col-lg-2 control-label text-right">Work Type :</label>
                                                 <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin"> 
                                                    <select class="form-control fsc-input" name="worknew_type">
                                                        <option value="">Select</option>
                                                     <option value="Purchase">Purchase</option>
                                                   <option value="Refinance">Refinance</option>
                                              </select>
                                                 </div>
                                             </div>
                                        </div>
                                        
                                         <div class="form-group corp_llc" id="" style="display:none;">
                                            <div class="row">
                                                <label class="col-lg-2 control-label text-right">Work Type :</label>
                                                 <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin"> 
                                                    <select class="form-control fsc-input" name="worknew_type">
                                                        <option value="">Select</option>
                                                   <option value="Create New Corp/ LLC">Create New Corp/ LLC</option>
                                                   <option value="Update Corp / LLC">Update Corp / LLC</option>
                                                   <option value="Create Documents">Create Documents</option>
                                                </select>
                                                 </div>
                                             </div>
                                        </div>
                                        
                                        <div class="form-group license" id="" style="display:none;">
                                            <div class="row">
                                                <label class="col-lg-2 control-label text-right">Work Type :</label>
                                                 <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin"> 
                                                    <select class="form-control fsc-input" name="worknew_type">
                                                        <option value="">Select</option>
                                                   <option value="Renewal License">Renewal License</option>
                                                   <option value="New License">New License</option>
                                                   <option value="Update License">Update License</option>
                                                </select>
                                                 </div>
                                             </div>
                                        </div>
                                        
                                        
                                         <div class="form-group insurance" id="" style="display:none;">
                                            <div class="row">
                                                <label class="col-lg-2 control-label text-right">Work Type :</label>
                                                 <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin"> 
                                                    <select class="form-control fsc-input" name="worknew_type">
                                                        <option value="">Select</option>
                                                   <option value="Life Insurance">Life Insurance</option>
                                                   <option value="Health Insurance">Health Insurance</option>
                                                   <option value="Auto Insurance">Auto Insurance</option>
                                                    <option value="Home Insurance">Home Insurance</option>
                                                     <option value="Business Insurance">Business Insurance</option>
                                                      <option value="Other Insurance">Other Insurance</option>
                                                </select>
                                                 </div>
                                             </div>
                                        </div>
                                        
                                           <div class="form-group">
                                            <div class="row">
                                                <label class="col-lg-2 control-label text-right" style="margin-top: 7px;">Work Priority :</label>
                                                 <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin"> 
                                                    <select class="form-control fsc-input" name="worknew_priority" required>
                                                        <option value="">Select</option>
                                                   <option value="Regular"> Regular</option>
                                                   <option value="Immediately">Immediately</option>
                                                   <option value="Urgent">Urgent</option>
                                                </select>
                                                 </div>
                                             </div>
                                        </div>
                                        
                                         <div class="form-group">
                                            <div class="row">
                                                <label class="col-lg-2 control-label text-right" style="margin-top: 7px;">Due Date :</label>
                                                 <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin"> 
                                                    <input type="text" class="form-control datepicker" name="worknew_duedate" readonly/>
                                                   
                                                 </div>
                                             </div>
                                        </div>
                                         <div class="form-group">
                                            <div class="row">
                                                <label class="col-lg-2 control-label text-right" style="margin-top: 7px;">Responsible Person :</label>
                                                 <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin"> 
                                                   <select class="form-control fsc-input" name="worknew_emp" required>
                                                   <option value="">Select</option>
                                                   <?php
                                                   foreach($empname as $emp)
                                                   {
                                                   ?>
                                                        <option value="<?php echo $emp->employee_id;?>"><?php echo $emp->firstName.' '.$emp->middleName.' '.$emp->lastName;?></option>
                                                   <?php
                                                   }
                                                   ?>
                                                </select>
                                                 </div>
                                             </div>
                                        </div>
                                        
                                           <div class="form-group">
                                            <div class="row">
                                                <label class="col-lg-2 control-label text-right" style="margin-top: 40px;">Work To Do :</label>
                                                 <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin"> 
                                                   <textarea class="form-control" name="worknew_details" style="height:100px;"></textarea>
                                                 </div>
                                             </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-lg-2 control-label text-right" style="margin-top: 40px;">Work Note :</label>
                                                 <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin"> 
                                                   <textarea class="form-control" name="worknew_note" style="height:100px;"></textarea>
                                                 </div>
                                             </div>
                                        </div>
                                        
                                        
                                        
                                    <br>
                                 <!--    <div class="form-group">
                                       <label class="col-lg-2 control-label">New Prospect : <span class="star-required">*</span></label>
                                          <div class="row">
                                             <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                <select class="form-control fsc-input" id="nametype" name="nametype">
                                                   <option value="Residence Mortgage">Residence Mortgage</option>
                                                   <option value="Commercial Mortgage">Commercial Mortgage</option>
                                                   <option value="License">License</option>
                                                   <option value="Corporation">Corporation</option>
                                                
                                                </select>
                                             </div>
                                          </div>
                                       
                                    </div>!-->
                                 </div>
                                 
                                 <div class="card-footer" STYLE="margin-left:10px;">
                                    <div class="col-md-1 col-md-offset-2" style="padding-left:0px; margin-left: 17.3%;">
                                       <input class="btn_new_save" type="submit" value="Create">
                                    </div>
                                    <div class="col-md-1" style="padding-left:0px;">
                                       <a class="btn_new_cancel" href="{{url('fscemployee/worknew')}}">Cancel</a> 
                                    </div>
                                 </div>
                            </form>
            	</div>
        	</div>	
    	</div>
    
       @endif
    			
    		  </div>
    		</div>
    	</div>
   
	  </section>
	  </div>
	  </div>
	  </div>
	  <!--</div>-->
	  
	  <script>
      $.ajaxSetup({
           headers: {
               'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
           }
       });
    
      $(function () {
         $('#addopt').click(function () { //alert();
             var newopt = $('#newopt').val();
             if (newopt == '') {
                 alert('Please enter something!');
                 return;
             }
   
            //check if the option value is already in the select box
             $('#vendor_product option').each(function (index) {
                 if ($(this).val() == newopt) {
                     alert('Duplicate option, Please enter new!');
                 }
             })
             $.ajax({
   type: "post",
   url: "{!!route('work.workcategorys')!!}",
   dataType: "json",
   data: $('#ajax').serialize(),
   success: function(data){
       $('#vendor_product').append('<option value=' + newopt + '>' + newopt + '</option>');
       $("#div").load(" #div > *");
      $("#newopt").val('');
   
      alert('Successfully Added');
      location.reload();
     },
   error: function(data){
      alert("Error")
   }
   });
            
              $('#basicExampleModal').modal('hide');
         });
     });
 
</script>
<div class="modal fade" id="basicExampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header" style="background:#038ee0;">
            <h4 class="modal-title" id="exampleModalLabel" style="text-align:center;color:#fff;">Work Category<button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
               </button>
            </h4>
         </div>
         <div class="modal-body" style="background:#ffff99;padding:0px !important;">
            <div class="curency curency_ref" id="div">
                <?php
                    if(isset($common->cid)!='')
                    {
                ?>
               @foreach($workcategory as $cur)
               <div id="cur_{{$cur->id}}" class="col-md-12" style="border:1px solid;background:#ffff99;">
                  <div class="col-md-12" style="margin-top: 7px;margin-bottom:5px;">
                     <a class="delete" style="color:#000;" onclick="return confirm('Are you sure to remove this record?')" href="{{route('worknew.destroydworkcat',[$cur->id,$common->cid])}}" id="{{$cur->id}}">{{$cur->category_name}}
                     <span class="pull-right"><i class="fa fa-trash btn btn-danger" style="padding:6px!important;"></i></span></a>
                  </div>
               </div>
               @endforeach  
               <?php
                    }
                ?>
            </div>
         </div>
         <div class="modal-footer" style="text-align:center;">
            <button type="button" style="margin-top:10px;border:1px solid;" class="btn btn-secondary" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="basicExampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Workcategory</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <form action="" method="post" id="ajax">
            {{csrf_field()}}
            <div class="modal-body">
               <input type="text" id="newopt" name="newopt"  class="form-control" placeholder="Workcategory Name"/>
            </div>
            <div class="modal-footer">
               <button type="button" id="addopt" class="btn btn-primary">Save</button>
               <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
         </form>
      </div>
   </div>
</div>

<script>

function myFunction(){
      var catval = $('.nametype').val();;
      
     
     if(catval=='4'){
         $('.resi_finance').show();
     } else {
         $('.resi_finance').hide();
     }
     
      if(catval=='5'){
         $('.com_finance').show();
     } else {
         $('.com_finance').hide();
     }
     
     if(catval=='6'){
         $('.corp_llc').show();
     } else {
         $('.corp_llc').hide();
     }
     
      if(catval=='7'){
         $('.license').show();
     } else {
         $('.license').hide();
     }
     
       if(catval=='8'){
         $('.insurance').show();
     } else {
         $('.insurance').hide();
     }
     
     
     
     
    }
$(document).ready(function(){
    
$("#worknew_telephone").mask("(999) 999-9999");

$(function () {
  $(".datepicker").datepicker({ 
        autoclose: true, 
        todayHighlight: true
  }).datepicker();
});


 $('#country_name').keyup(function(){ 
        var query = $(this).val();
        if(query != '')
        { 
         var _token = $('input[name="_token"]').val();
         $.ajax({
          url:"{{ route('worknew.fetchemp') }}",
          method:"POST",
          data:{query:query, _token:_token},
          success:function(data){
           $('#countryList').fadeIn();  
                    $('#countryList').html(data);
          }
         });
        }
    });

    $(document).on('click', 'tr', function(){  
        $('#country_name').val($(this).text());  
        $('#countryList').fadeOut();  
    });  
    
   
    

});

$('.btn-toggle').click(function() {
    $(this).find('.btn').toggleClass('active');  
    
    if ($(this).find('.btn-primary').length>0) {
    	$(this).find('.btn').toggleClass('btn-primary');
    }
    if ($(this).find('.btn-danger').length>0) {
    	$(this).find('.btn').toggleClass('btn-danger');
    }
    if ($(this).find('.btn-success').length>0) {
    	$(this).find('.btn').toggleClass('btn-success');
    }
    if ($(this).find('.btn-info').length>0) {
    	$(this).find('.btn').toggleClass('btn-info');
    }
    
    $(this).find('.btn').toggleClass('btn-default');
       
});


function nopress(){
  $('.searchboxmain').hide();
 $('.newprospects').show();
}
function noonepress(){
   $('.searchboxmain').show();
    $('.newprospects').hide();
}
</script>
@endsection

