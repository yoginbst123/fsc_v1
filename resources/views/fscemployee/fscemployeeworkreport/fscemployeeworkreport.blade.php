@extends('fscemployee.layouts.app')
@section('main-content') 
<style>
.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{padding:5px 8px !important;vertical-align:inherit !important;}
.headerclr{background:#ffff99 !important;}
.table > thead > tr > th{background:none !important;}
.addagemodal .form-control:focus, .addagemodal .form-control:hover{background:#ffff99;}
span.required
{
    color:red;
}
.table > thead > tr > th{padding:8px!important;}
.subtitle{height: 25px;  max-width: 600px;  margin: 0px auto 15px;}
.subtitle span{font-size:16px; color:#000;}
.floatleft{float:left;}
.floatright{float:right;}
.reporttext{
    display:flex; 
    flex-direction:row; 
    justify-content:center; 
    width:100%;
    margin: 15px auto 0px;
    border: 2px solid #a5a5a5;
    width: 100%;
    padding-right: 0!important;
    position: relative;
    max-width: inherit!important;
    left: 0%;
    display: flex;
    flex-direction: row;
    background: #e2e2e2;
    padding: 5px 0px;
    height: 60px;
    margin-bottom: -113px;
    position: unset;
    margin-top: -10px;
}
.dt-buttons {
    margin-bottom: 90px;
}
th.text-center.reporthd.sorting_asc {
    width: 45px !important;
}
th.text-center.reporthd.sorting {
    width: 90px !important;
}
.reporttext ul{margin: 0px; padding: 0px; display: flex; flex-direction: ROW; justify-content: space-between;width:100%;}
.reporttext ul li{list-style-type:none; margin:0px 10px; font-size:16px;}
.reporttable{margin-top:10px;}
.reporttable tr th{padding:5px 10px!important;}
.workreportbox{
    /*display:flex; */
    flex-direction:row; justfy-content:flex-start; align-items:flex-start;
}
.workreportbox ul { display:flex; flex-direction:row; justfy-content:flex-start; align-items:flex-start; list-style: none;
    list-style-type: none;
    margin: 0px;
    padding: 0px;}
.workreportbox ul li{ display:flex; flex-direction:row; justify-content:flex-start; align-items:flex-start; flex-wrap:wrap; margin:0px 5px;}
.workreportbox ul li label{width:100%; text-align:left!important;}
.workreportbox ul li  .btn-primary{height:40px; width:100px;}
.reporthd{background:#ffff99!important;}
.edittable tr td{padding:0px 5px!important;}
.form-control, .form-control-insu{
    padding: 6px 6px!important;
}
.reporttext label{
    margin-bottom:0px;
}
@media only screen and (max-width: 1280px){
    #example_wrapper table#example{
        display: table !important;
        overflow-x: auto;
        white-space: nowrap;
    }
}
@media only screen and (max-width: 490px){
    #example_wrapper table#example{
        display: block !important;
        overflow-x: auto;
        white-space: nowrap;
    }
}
</style>
<style>
    @media screen {
  #printSection {
      display: none;
  }
 
}

@media print {
   .close, .btn{display:none; font-size:0px;}
  body * {
    visibility:hidden;
  }
  #printSection, #printSection * {
    visibility:visible;
  }
  #printSection {
    position:absolute;
    left:0;
    top:0;
  }
}



</style>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>FSC EE / User Work Reports <?php //print_r($_SESSION);?></h1>
     		<?php //$datas=$request->session()->all();
     	//	print_r($datas);
     		?>
    </section>
    <section class="content">
   <div class="row">
      <div class="col-md-12">
        <div class="box box-success">
			      <div class="box-header">
              <div class="box-tools pull-right">
              </div>
            </div>
				<div class="col-md-12">
               <form  class="form-horizontal changepassword" action="{{route('fscemployeeworkreport.index')}}"  method="post">
                  {{csrf_field()}}
                  <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="workreportbox">
                                <div class="col-md-2" style="width:22%; padding-left:0px;">
                                    <label for="focusedinput" class="control-label ">EE / User  </label>
                                    <input type="hidden" name="employee" value="<?php echo $employee->id;?>">
                                    <input type="text" readonly class="form-control fsc-input" value="<?php echo $employee->firstName.' '.$employee->middleName.' '.$employee->lastName;?>">
                                </div>
                                <div class="col-md-2" style="width:21%; padding-left:0px;">
                                     <label for="focusedinput" class="control-label ">Type of Business</label>
                                      <select name="business" id="" class="form-control fsc-input">
                                            <option value="">All</option>
                                             @if(!empty($category))
                                              @foreach($category as $cat)
                                              <option value="{{$cat->id}}" <?php if(((isset($_POST['business']) && $_POST['business'] !='')) &&  $_POST['business'] == $cat->id) { echo 'selected';}?>>{{$cat->business_cat_name}}</option>
                                              @endforeach
                                              @endif
                                     </select>
                                </div>
                                <div class="col-md-2" style="width:22%; padding-left:0px;">
                                     <label for="focusedinput" class="control-label ">Type of Service</label>
                                     <select name="type_of_service" id="type_of_service" class="form-control fsc-input servicetype">
                                        <option value="">Select</option>
                                        <option value="10-2" <?php if(isset($_POST['type_of_service']) && $_POST['type_of_service'] =='10-2') { echo 'selected';}?>>Accounting Service</option>
                                        <option value="10-8" <?php if(isset($_POST['type_of_service']) && $_POST['type_of_service']  =='10-8') { echo 'selected';}?>>Payroll Service</option>
                                        @foreach($taxtitle as $cate)
                                            <option value='{{$cate->id}}' <?php if(isset($_POST['type_of_service']) && $_POST['type_of_service'] ==$cate->id) { echo 'selected';}?>>{{$cate->title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                 <div class="col-md-2" style="width:8%; padding-left:0px;">
                                     <label for="focusedinput" class="control-label ">Month</label>
                                      <select name="month" id="" class="form-control fsc-input">
                                          <option value="">Sel.</option>
                                          <option value="Jan" <?php if(isset($_POST['month']) && $_POST['month']  =='Jan') { echo 'selected'; } ?>>Jan</option>
                                          <option value="Feb" <?php if(isset($_POST['month']) && $_POST['month']  =='Feb') { echo 'selected'; } ?>>Feb</option>
                                          <option value="Mar" <?php if(isset($_POST['month']) && $_POST['month']  =='Mar') { echo 'selected'; } ?>>Mar</option>
                                          <option value="Apr" <?php if(isset($_POST['month']) && $_POST['month']  =='Apr') { echo 'selected'; } ?>>Apr</option>
                                          <option value="May" <?php if(isset($_POST['month']) && $_POST['month']  =='May') { echo 'selected'; } ?>>May</option>
                                          <option value="Jun" <?php if(isset($_POST['month']) && $_POST['month']  =='Jun') { echo 'selected'; } ?>>Jun</option>
                                          <option value="Jul" <?php if(isset($_POST['month']) && $_POST['month']  =='Jul') { echo 'selected'; } ?>>Jul</option>
                                          <option value="Aug" <?php if(isset($_POST['month']) && $_POST['month']  =='Aug') { echo 'selected'; } ?>>Aug</option>
                                          <option value="Sep" <?php if(isset($_POST['month']) && $_POST['month']  =='Sep') { echo 'selected'; } ?>>Sep</option>
                                          <option value="Oct" <?php if(isset($_POST['month']) && $_POST['month']  =='Oct') { echo 'selected'; } ?>>Oct</option>
                                          <option value="Nov" <?php if(isset($_POST['month']) && $_POST['month']  =='Nov') { echo 'selected'; } ?>>Nov</option>
                                          <option value="Dec" <?php if(isset($_POST['month']) && $_POST['month']  =='Dec') { echo 'selected'; } ?>>Dec</option>
                                     </select>
                                </div>
                                 <div class="col-md-2" style="width:8%; padding-left:0px;">
                                     <label for="focusedinput" class="control-label ">Year</label>
                                      <select name="year" id="" class="form-control fsc-input">
                                          <option value="">Sel.</option>
                                          <option value="2020" <?php if(isset($_POST['year']) && $_POST['year']  =='2020') { echo 'selected'; } ?>>2020</option>
                                          <option value="2021" <?php if(isset($_POST['year']) && $_POST['year']  =='2021') { echo 'selected'; } ?>>2021</option>
                                      </select>
                                </div>
                                <div class="col-md-2" style="margin-top: 25px;padding-left:0px;">
                                     <label for="focusedinput" class="control-label">&nbsp; </label>
                                     <button class="btn-success btn" type="submit"  name="search" style="width:70px;"> Ok</button>
                                      <button class="btn-danger btn" type="button" name="search" style="width:60px; margin-left:5px; margin-top:0px;"onClick="Refresh();"> Reset</button>
                                </div>
                        </div>
                        
                        
                        <div class="corporationbox" style="display:none;">
                            
                            
                              
                             <div class="form-group" style="margin-left:140px;">
                                      
                            <div class="col-sm-2"  style="width:18%;">
                              <div class="">
                                
                              </div>
                           </div>
                                
                                 <div class="col-sm-3">
                                    <!--<select name="status" id="status" class="form-control fsc-input" style="background-color:#00ef00 !important;color:#fff !important">-->
                                    <!--     <option value="">Status</option> -->
                                    <!--     <option value="Hold" class="Red">Hold</option> -->
                                    <!--     <option class="Orange" value="Pending">Pending</option>-->
                                    <!--    <option value="Approval" class="Green">Approve</option> -->
                                    <!--    <option value="Active" style="background-color:#00ef00 !important;color:#fff !important" selected="">Active</option> -->
                                    <!--    <option value="Inactive" class="Blue" style="background:blue;color:#fff">Inactive</option> -->
                                    <!--</select>-->
                                    <!--name="record_status"-->
                                    
                                   

                                </div>
                            </div>
                        </div>
                        
                    
                        <div class="form-group ">
                           <label for="focusedinput" class="col-md-5 control-label "></label>
                           <div class="col-sm-7" style="margin:0 !important;padding:0 !important;">
                              <div class="col-sm-3">
                                
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  </form>
                  
                  </div>
                  <?php if(isset($client))
                  {?>
                  <div id="printMe">
                        <style>
				  @media print{
					  table{font-family: Verdana, Geneva, Tahoma, sans-serif;}
					  table tr td{padding:4px; border-bottom:1px solid #ccc;}
					   table tr th{padding:4px; border-bottom:1px solid #ccc;}
				  }
				</style>
                  <div class="col-md-12">
                       <div class="Branch"><h1 style="text-align:center;"> Report</h1></div>
                       <div class="clear"></div>
                       <center>
    <img src="https://financialservicecenter.net/public/dashboard/images/fsc_logo.png" width="80px" class="img-responsive">
</center>
<br>
<?php 
  $clientcount=count($client);
                              ?>
                      <div class="fscreport">
                          <div class="reporttext">
                              <ul>
                                  <li><label>Count: </label><br/> <?php echo $clientcount;?></li>
                                  <?php if(isset($employee1->firstName)){?><li><label>EE / User: </label><br/> <?php echo $employee1->firstName.' '.$employee1->middleName.' '.$employee1->lastName;?> @if($employee1->Team) ({{$employee1->Team}}) @endif</li><?php } ?>
                                  <?php if(isset($business1->business_cat_name)){?><li><label>Type of Business: </label><br/> <?php echo $business1->business_cat_name;?></li><?php } ?>
                                  <li><label>Type of Service: </label><br/> <?php if(isset($_POST['type_of_service']) && $_POST['type_of_service'] =='10-2') { echo 'Accounting Service';}?></li>
                                  <li><label>Period: </label><br/> <?php echo $_POST['month'].' '.$_POST['year'];?></li>
                                  
                                  
                                  
                              </ul>
                           </div>
    <!--                    <button attr="printMe"  type="button" class="btn btn-primary btn-sm pull-right clkbutton" style="margin-right: 15px; position: ABSOLUTE; right:0px;-->
    <!--top: 0px;">Print</button>-->
  
                         <div class="table-responsive">
                             <table class="table table-bordered table-striped reporttable"  id="example" align="center" style="width:100%;">
                                <?php $cnt=0;
                                $clientcount=count($client);
                               // print_r($_POST);EXIT;
                                ?>
                             
                              
                                <thead style="background: #ffff99;">
                                <tr>
                                     <th class="text-center reporthd" width="5%" style="width:40px !important;">No.</th>
                                     <th class="text-center reporthd" style="width:40px;">Client ID</th>
                                     
                                     <th class="text-left reporthd"  style="width:100px;text-align:center;">Client Legal Name</th>
                                     <th class="text-left reporthd"  style="width:100px;text-align:center;">Type of Business</th>
                                     
                                 </tr>
                                 </thead>
                                  @if($clientcount !='0')
                                 <tbody>
                                 @foreach($client as $clients)
                                 <?php //print_r($clients);exit; ?>
                                <?php $cnt++;?>
                                 <tr>
                                     <td style="text-align:center !important;">{{$cnt}}</td>
                                     <td style="text-align:left !important;"><a target="_blank" href="clients/{{$clients->id}}/edit" style="color:#337ab7">{{$clients->filename}}</a></td>
                                     
                                     <td style="text-align:left !important;">{{$clients->company_name}}</td>
                                     <td style="text-align:left !important;">{{$clients->business_cat_name}}</td>
                                     
                                      
                                 </tr>
                                 @endforeach
                                 </tbody>
                                 
                                 @endif
                             </table>
                         </div>
                      </div>
                  </div>
                  </div>
                <?php 
                  }
                  ?>
         </div>
         <br>
      </div>
   </div>
</section>
</div>
<!--</div>-->
   <div id="federalModals" class="modal fade" role="dialog">
  <div class="modal-dialog" style="width:1000px;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><span id="clid" class="companycode"></span>Client - Income Tax Filing - (Form-1040) <span style="float:right; margin-right:5px;">Add Record</span></h4>
      </div>
      <div class="modal-body">
          <form class="form-horizontal" method="post" action="{{route('worktax.store')}}" enctype="multipart/form-data">
              {{csrf_field()}} 
              
              
              <input type="hidden" name="client_id" id="federalid">
              
          <div class="recordform">
              
            
            
                <table style="width:100%; max-width:590px; margin:0px auto;" class="taxtable edittable">
                <tr>
                      <TD style="WIDTH:200PX;">
                        <label class="control-label labels">Client Name:</label>
                        <input type="text" class="form-control clname" readonly>
                    </TD>
                    <td>
                        <label class="control-label labels">Filling Year</label>
                        <select class="form-control federalyear" name="federalsyear" id="federalsyear" required>
                        <option value="">Select</option>
                        <?php
                            
                            $now=date('M-d-Y');
                            if('Jul-25-2021'==$now)
                            {
                                $aa=date('Y')-2;
                                $bb=date('Y')-1;
                                ?>
                                <option value="<?php echo $aa;?>"><?php echo $aa;?></option>
                                <option value="<?php echo $bb;?>"><?php echo $bb;?></option>
                                <?php
                            }
                            else
                            {
                                $aa=date('Y')-1;
                                ?>
                                <option value="<?php echo $aa;?>"><?php echo $aa;?></option>
                                <?php
                            }
                            
                        ?>
                        
                        
                        
                    </select>
                        
                    </td>
                    <td>
                         <label class="control-label labels">Tax Return</label>
                         
                         <select class="form-control taxation" name="federalstax" required>
                            <option value="">Select</option>
                            <option value="Extension">Extension </option>
                            <option value="Original">Original</option>
                            <option value="Amendment">Amendment</option>
                        </select>
                    </td>
                    <td style="width:150px;">
                        <label class="control-label labels">Due Date</label>
                         <input type="text" class="form-control duedate" name="federalsduedate" placeholder="Mar-15-2020" readonly/>
                    </td>
                </tr>
            </table>
            
                <table class="taxtable table table-bordered" style="margin-top:20px;" id="taxationtable">
             <tr>
                
                  <td style="width:215px;" colspan="2">
                     <label class="form-label">Federal Form No.</label>
                      <input type="text" readonly class="form-control formno" name="federalsform"  style="width:100%; max-width:100%; margin:0px; padding:0px;" required>
                     
                 </td>
                  <td style="width:120px;">
                     <label class="form-label">Filling Method</label>
                      <select class="form-control filingmethod" name="federalsmethod" required>
                        <option value="">Select</option>
                        <option value="E-File">E-File</option>
                        <option value="Paperfile">Paperfile</option>
                    </select>
                     
                 </td>
                 <td style="width:120px;">
                     <label class="form-label">Filling Date</label>
                     <input type="text" class="form-control fdate fdatef" id="fillingdate" name="federalsdate" placeholder="MM/DD/YYYY" required/>
                     <input type="hidden" class="form-control" id="federalsfile" name="federalsfile" required/>
                      
                 </td>
                  <td style="width:150px;">
                     <label class="form-label">Status of Filling</label>
                     <select class="form-control" name="federalsstatus" required>
                        <option value="">Select</option>
                        <option class="hidest" value="Accepted">Accepted</option>
                        <option class="hidest" value="EF Processing">EF Processing</option>
                        <option  class="hidest" value="Pending">Pending</option>
                        <option class="hidest" value="Rejected">Rejected</option>
                    </select>
                     
                     
                 </td>
                  
                 <td>
                     <label class="form-label">Note</label>
                       <textarea class="form-control" name="federalsnote"></textarea>
                     
                 </td>
                 <td></td>
             </tr>
             <tr><td colspan="8" style="height:20px;"></td></tr>
              <tr>
                  <th>  <label class="form-label">Resi. State</label></th>
                  <th><label class="form-label">Form No.</label></th>
                  <th><label class="form-label">Filling Method</label></th>
                  <th><label class="form-label">Filling Date</label></th>
                  <th> <label class="form-label">Status of Filling</label></th>
                  <th><label class="form-label">Note</label></th>
                  <th></th>
              </tr>
              
              
             <tr>
                 <td>
                   
                     <select class="form-control" name="statetax[]" id="statetax">
                         
                         <option>Select</option>
                         @foreach($datastate as $datastate)
                         <option value="{{$datastate->code}}">{{$datastate->code}}</option>
                         @endforeach
                     </select>
                     
                      
                      
                     
                 </td>
                  <td>
                     
                      <input type="text" readonly class="form-control" name="stateformno[]" id="statefederalformno" style="width:100px"/>
                     
                 </td>
                  <td>
                    
                    <select class="form-control filingmethodstate" name="statemethod[]">
                        <option value="">Select</option>
                        <option value="E-File">E-File</option>
                        <option value="Paperfile">Paperfile</option>
                    </select>
                     
                 </td>
                  <td>
                     
                     <input type="text" class="form-control fdate fdates fdatess" id="fillingdate" name="statedate[]" placeholder="MM/DD/YYYY" required/>
                     <input type="hidden" class="form-control" id="statefile" name="statefile" required/>
                 </td>
                  <td>
                   
                     <select class="form-control" name="statestatus[]">
                        <option value="">Select</option>
                        <option class="hidests" value="Accepted">Accepted</option>
                        <option class="hidests" value="EF Processing">EF Processing</option>
                        <option class="hidests" value="Pending">Pending</option>
                        <option class="hidests" value="Rejected">Rejected</option>
                    </select>
                     
                     
                 </td>
                  
                 <td>
                     
                       <textarea class="form-control" name="statenote[]"></textarea>
                     
                 </td>
                 <td><a href="#" class="btn btn-primary add-rowform"><i class="fa fa-plus"></i></a></td>
             </tr>
         </table>
         
        
         
                <div class="row form-group saves">
                <label class="col-md-4 control-label">&nbsp;</label>
                <div class="col-md-2">
                    <input type="submit" class="btn_new_save primary" value="Save">
                </div>
                 <div class="col-md-2">
                    <a href="#" class="btn_new_cancel" data-dismiss="modal">Cancel</a>
                </div>
               
            </div>
            
            
         
            <input type="hidden" name="client_taxation_id" value="<?php if(isset($common->id)!=''){echo $common->id;}?>">
            <div class="row form-group">
                
                <div class="col-md-4 federals" style="display:none;">
                     
                </div>
                
                <div class="col-md-4 states" style="display:none;">
                     <select class="form-control statesyear" name="statesyear">
                        <option value="">Select</option>
                         <?php
                            $aa=date('Y')-1;
                            
                        ?>
                        <option value="<?php echo $aa;?>"><?php echo $aa;?></option>
                        
                        
                    </select>
                </div>
            </div>
             <div class="row form-group">
                <label class="col-md-3 control-label labels" style="display:none;">Tax Return</label>
                <div class="col-md-4 federals" style="display:none;">
                    
                </div>
                <div class="col-md-4 states" style="display:none;">
                    <select class="form-control taxation2" name="statestax">
                       <option value="">Select</option>
                        <option value="Extension">Extension </option>
                        <option value="Original">Original</option>
                        <option value="Amendment">Amendment</option>
                </select>
                </div>
            </div>
              <div class="row form-group">
                <label class="col-md-3 control-label labels"  style="display:none;">Form No.</label>
                <div class="col-md-4 federals" style="display:none;">
                   
                </div>
              
                <div class="col-md-4 states" style="display:none;">
                    <input type="text" class="form-control formno2" name="statesformno" readonly/>
                </div>
            </div>
              <div class="row form-group">
                <label class="col-md-3 control-label labels" style="display:none;">Due Date</label>
                <div class="col-md-4 federals" style="display:none;">
                   
                </div>
             
                <div class="col-md-4 states" style="display:none;">
                    <input type="text" class="form-control duedate2" name="statesduedate" placeholder="Mar-15-2020" readonly/>
                </div>
            </div>
            <div class="row form-group">
                <label class="col-md-3 control-label labels" style="display:none;">Filing Method </label>
                <div class="col-md-4 federals" style="display:none;">
                   
                </div>
               
                <div class="col-md-4 states" style="display:none;" >
                    <select class="form-control filingmethod2" name="statesmethod">
                        <option value="">Select</option>
                        <option value="E-File">E-File</option>
                        <option value="Paperfile">Paperfile</option>
                    </select>
                </div>
            </div>
            <div class="row form-group">
                <label class="col-md-3 control-label labels" style="display:none;">Filing Date</label>
                <div class="col-md-4 federals" style="display:none;">
                    
                </div>
              
                <div class="col-md-4 states" style="display:none;">
                    <input type="date" class="form-control" id="fillingdate2" name="statesdate"/>
                </div>
            </div>
             <div class="row form-group statusof">
                <label class="col-md-3 control-label labels" style="display:none;">Status of Filling </label>
                <div class="col-md-4 federals" style="display:none;" >
                    
                </div>
               
                <div class="col-md-4 states" style="display:none;">
                    <select class="form-control" name="statesstatus">
                        <option value="">Select</option>
                        <option class="hidestone" value="Accepted">Accepted</option>
                        <option class="hidestone" value="Rejected">Rejected</option>
                        <option class="hidestone" value="Pending">Pending</option>
                    </select>
                </div>
            </div>
             
              <div class="row form-group">
                <label class="col-md-3 control-label labels" style="display:none;">Note </label>
                <div class="col-md-4 federals" style="display:none;">
                 
                </div>
                
                <div class="col-md-4 states" style="display:none;">
                   <textarea class="form-control" name="statesnote"></textarea>
                </div>
            </div>
            <div class="row form-group saves" style="display:none;">
                <label class="col-md-5 control-label">&nbsp;</label>
                <div class="col-md-2">
                    <input type="submit" class="btn_new_save primary" value="Save">
                </div>
                 <div class="col-md-2">
                    <a href="#" class="btn_new_cancel" data-dismiss="modal">Cancel</a>
                </div>
               
            </div>
        </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<script>
/*$.ajaxSetup({
    headers:
    {'X-CSRF-Token': $('input[name="_token"]').val();}
});*/
</script>
<script>
$(document).ready(function() {
$('#sampleTable1').DataTable( {
        dom: 'Bfrtlip',
    "columnDefs": [ {
            "searchable": true,
            "orderable": true,
            "targets": 0
        } ],
        "order": [[ 0, 'asc' ]],
       buttons: [
                 {
                extend:    'copyHtml5',
                text:      '<i class="fa fa-files-o"></i> &nbsp; Copy',
                //titleAttr: 'Copy',
                title: $('h1').text(),
            },{
                extend:'excelHtml5',
                text:'<i class="fa fa-file-excel-o"></i>&nbsp; Excel',
               // titleAttr: 'Excel',
                title: $('h4').text(),
                 customize: function( xlsx ) {
                var sheet = xlsx.xl.worksheets['sheet1.xml'];
                        $('row c[r^="A"]',sheet).attr('s','51'); 
                       //   $('row c[r^="C"]',sheet).attr('s','51'); 
                          $('row:first c',sheet).attr('s','51');
            },
            exportOptions: {
                columns: [0,1,2,3,4,5,6], // Only name, email and role
                }
            },
            {
                extend:    'csvHtml5',
                text:      '<i class="fa fa-file-text-o"></i> &nbsp; CSV',
               // titleAttr: 'CSV',
                title: $('h1').text(),
            },
            {
           extend: 'print',
          text: '<i class="fa fa-print"></i>&nbsp; Print',
         customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '10pt' )
                        .prepend(
                            '<center><img src="https://financialservicecenter.net/public/business/{{$logo->logo}}"/></center>'
                        );
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );
                },
         exportOptions: {
          columns: [0,1,2,3,4,5,6]
      },
      footer: true,
      autoPrint: true
    },
        ],
    } );
});
</script>
<script>
    $(document).ready(function()
    {
        $(".clkbutton").click(function(){
        //alert();
            var attr=$(this).attr('attr');
            alert(attr);
              var printContents = document.getElementById(attr).innerHTML;
        w=window.open();
        w.document.write(printContents);
        w.print();
        w.close();
        });
        function printDiv(divName) {
           // alert();
            
        var printContents = document.getElementById(divName).innerHTML;
        w=window.open();
        w.document.write(printContents);
        w.print();
        w.close();
    }
          $(".fdates").blur(function(){
          var fdatef=$('.fdatef').val();
             
          var thiss=$(this).val();
         // alert(thiss);
          if(thiss)
          {
              
          }
          else
          {
              $('.fdates').val(fdatef);
          }
             
         });

        $('.filingmethod').on('change', function()
        {
           var thiss=$(this).val();
           if(thiss =='Paperfile')
           {
               $('.hidest').hide();
           }
           else
           {
               $('.hidest').show();
           }
        });
        
        $('.filingmethodstate').on('change', function()
        {
           var thiss=$(this).val();
           if(thiss =='Paperfile')
           {
               $('.hidests').hide();
           }
           else
           {
               $('.hidests').show();
           }
        });
        
          $('#statetax').on('change', function()
        {
            var fdate=$('.fdate').val();
            $('.fdatess').val(fdate);
            
            var statetaxval=$("#statetax").val();
            $.get('{!!URL::to('getTaxstatedata')!!}?statetaxval='+statetaxval,function(data)
            {  
                //console.log(data);exit;
                if(data=='')
                {
                   $("#statefederalformno").val(); 
                }
                else
                {
                    $("#statefederalformno").val(data.taxform);
                }
                
            });
   
   });
      
        
        var cnt=1;
          
            $(".add-rowform").click(function()
            {
                cnt++;
                //$('').show();
                //var aa=$('.statetax2').html();
                var markup = ' <tr><td><select class="form-control statetax'+cnt+'" name="statetax[]" id="statetax'+cnt+'"><option>Select</option> @foreach($datastate2 as $datastate3)<option value="{{$datastate3->code}}">{{$datastate3->code}}</option>@endforeach</select></td><td><input type="text" readonly class="form-control formno" name="stateformno[]" id="statefederalformno'+cnt+'" style="width:100px"/></td><td><select class="form-control filingmethod" name="statemethod[]"><option value="">Select</option><option value="E-File">E-File</option><option value="Paperfile">Paperfile</option></select></td><td><input type="text" class="form-control fdatess'+cnt+'" id="fillingdate" name="statedate[]" id="federaldate'+cnt+'" placeholder="MM/DD/YYYY"/></td><td><select class="form-control" name="statestatus[]" id="federalstatus'+cnt+'"><option value="">Select</option> <option value="Accepted">Accepted</option><option value="EF Processing">EF Processing</option><option value="Pending">Pending</option><option value="Rejected">Rejected</option></select></td><td><textarea class="form-control" name="statenote[]" id="federalnote'+cnt+'"></textarea></td><td><a href="#" class="btn btn-danger removetrrow"><i class="fa fa-minus"></i></a></td></tr>';
                $("#taxationtable tbody").append(markup);
            
                $( function()
                {
                    $('.fdate'+cnt).datepicker({
                        autoclose: true
                    });
                });   
        
                $('.statetax'+cnt).on('change', function()
                {
                    var fdate=$('.fdate').val();
                    $('.fdatess'+cnt).val(fdate);
                        var statetaxval=$('.statetax'+cnt).val();
                       // alert(statetaxval);
                        $.get('{!!URL::to('getTaxstatedata')!!}?statetaxval='+statetaxval,function(data)
                        {  
                            if(data=='')
                            {
                               $('#statefederalformno'+cnt).val(); 
                            }
                            else
                            {
                                $('#statefederalformno'+cnt).val(data.taxform);
                            }
                            
                        });
               });
           
            });
            $("body").on("click",".removetrrow",function(){ 
                $(this).parent().parent().remove();
    });        
        
    });
    
</script>
<script>
 $( function()
    {
        $('.fdate').datepicker({
            autoclose: true
        });
    });
$(document).on('change','#status', function()
   {
          var thiss = $(this).val();
       //  alert(thiss);
          if(thiss =='0')
          {
              $('.showfill').show();
          }
          else
          {
              $('.showfill').hide();
          }
 
   });
   
   
$(document).on('change','#formation_status', function()
   {
          var thiss = $(this).val();
       if(thiss =='Taxation')
       {
           $('.showtax').show();
           $('.222').hide();
           $('.111').show();
           
           
       }
       else
       {$('.111').hide();
           $('.222').show();
           $('.showtax').hide();
       }
       if(thiss =='Corporation')
       {
           /*   $.get('{!!URL::to('getStateReport')!!}', function(data)
            {  
                //console.log(data);exit;
                if(data == "")
                {
                    
                }
                else
                {
                    
                }
                

                
            });
    */
       }
          if(thiss =='Taxation')
          {
              $('.taxationpersonalcategory').show();
          }
          else
          {
              $('.taxationpersonalcategory').hide();
          }
 
   });
   
    $('.111').hide();
   $(document).on('change','#tax_subcategory', function()
   {
          var thiss = $(this).val();
       //  alert(thiss);
      // var aa=$("#status option:selected", this).attr("rel");
    //  alert(aa);
          if(thiss =='Personal')
          {
             $('.111').show();
              $('.222').hide();
              $('.taxpersonalcategory').show();
          }
          else
          {
              $('.111').hide();
              $('.222').show();
              $('.taxpersonalcategory').hide();
          }
 
   });
     $('.taxation').change(function()
        {
              var businessid='<?php if(isset($_REQUEST['id']) && $_REQUEST['id'] !='') { echo $common->business_id; }?>';
            
          var thiss=$(this).val();
          if(thiss == 'Extension')
          {
              if(businessid =='6')
              {
                  $('.formno').val('Form-4868');
              }
              else
              {
              $('.formno').val('Form-7004');    
              }
              
              $("select option[value='E-File']").show();
          }
          
          else if(thiss == 'Original')
          {
              $('.formno').val('Form-1040');
              $("select option[value='E-File']").show();
          }
          else if(thiss == 'Amendment')
          {
              $('.formno').val('Form-1040X');
              $("select option[value='E-File']").hide();
                     
          }
        });
      
 $(".federalTaxation").click(function ()
        {
            var federalid = $(this).attr('data-id');
          //  alert(federalid);
            $("#federalid").val(federalid);
            $('#federalModals').modal('show');
            $.get('{!!URL::to('getClientfederaldata2')!!}?federalid='+federalid, function(data)
            {  
                //console.log(data);exit;
                if(data == "")
                {
                    
                }
                else
                {
                    //alert(data.filename);
                    //$('#id').val(data.id);
                    var fullnames=data.first_name +' '+data.last_name;
                  $('#federalsyear').val(data.taxyears);
                    $('#clid').html(data.filename);
                    $('.clname').val(fullnames);
                    
                    
                   // $('#federalstax').val(data.federalstax);
                    //$('#federalsduedate').val(data.federalsduedate);
                    //$('#federalsduedate').val(date('d-m-Y',strtotime(data.federalsduedate)));
                    // $('#federalsform').val(data.federalsform);
                    // $('#federalsmethod').val(data.federalsmethod);
                    // $('.fillingdate').val(data.federalsdate);
                    // $('#federalsstatus').val(data.federalsstatus);
                    // $('#federalsfile').val(data.federalsfile);
                    // $('#federalsfile_1').html(data.federalsfile);
                    // $('#federalsnote').val(data.federalsnote);
                    
                    
                    var this1=$('#federalsyear').val();
                    var aa='<?php echo $aa=date('Y')-1;?>';
             
                    if(this1 == aa)
                    {
                        $(".duedate").val('Jul-15-<?php echo $aa+1;?>');
                 
                    }
                    
                }
                

                
            });
        });
       
 $(document).on('change','.category', function()
   {
  
    var id = $(this).val();
   $.get('{!!URL::to('getRequest')!!}?id='+id, function(data)
   {  
      
       //alert();
   if(data == "")
   {
   $('#business_catagory_name_2').hide();
   $('#business_catagory_name_4').hide();
   $('#business_catagory_name_3').hide();
   }
   else
   {
   $('#business_catagory_name_2').show();
   }
   $('#business_catagory_name').empty();
   $('#business_catagory_name_4').hide();
   $('#business_catagory_name_3').hide();
   $('#business_catagory_name').append('<option value="">---Select---</option>');
   $('#business_brand_category_name').append('<option value="">---Select---</option>');
   $('#business_brand_name').append('<option value="">---Select---</option>');
   $.each(data, function(index, subcatobj)
   {
   $('#business_catagory_name').append('<option value="'+subcatobj.id+'">'+subcatobj.business_cat_name+'</option>');
   //	$('#user_type').val('subcatobj.business_cat_name');
   })
   });
   });
   </script>
   <script>
    function Refresh() {
        window.parent.location = window.parent.location.href;
    }
    
    $(document).ready(function() {
    var table = $('#example').DataTable( {
        dom: 'Bfrtlip',
    "columnDefs": [ {
            "searchable": true,
            "orderable": true,
            "targets": 0
        } ],
        "order": [[ 0, 'asc' ]],
        buttons: [
                 {
                extend:    'copyHtml5',
                text:      '<i class="fa fa-files-o"></i> &nbsp; Copy',
                titleAttr: 'Copy',
                title: $('h3').text(),
                  exportOptions: {
        columns: [0,1, 2, 3,4,5], // Only name, email and role
    }
            },
            {
                extend:    'excelHtml5',
                text:      '<i class="fa fa-file-excel-o"></i>&nbsp; Excel',
               titleAttr: 'Excel',
                title: $('h3').text(),
                 customize: function( xlsx ) {
                var sheet = xlsx.xl.worksheets['sheet1.xml'];
 $('row c', sheet).attr('s', '51');
            },
                  exportOptions: {
        columns: [0,1, 2, 3,4,5], // Only name, email and role
    }
            },
            {
                extend:    'csvHtml5',
                text:      '<i class="fa fa-file-text-o"></i> &nbsp; CSV',
                titleAttr: 'CSV',
                title: $('h3').text(),
                  exportOptions: {
        columns: [0,1, 2, 3,4,5], // Only name, email and role
    }
            },
            {
                extend:    'pdfHtml5',
                text:      '<i class="fa fa-file-pdf-o"></i>&nbsp;  PDF',
                
              customize: function (doc) {
						//Remove the title created by datatTables
						doc.content.splice(0,1);
						//Create a date string that we use in the footer. Format is dd-mm-yyyy
						var now = new Date();
						var jsDate = now.getDate()+'-'+(now.getMonth()+1)+'-'+now.getFullYear();
						
						doc.pageMargins = [20,60,20,20];
						doc.defaultStyle.fontSize = 10;
						doc.styles.tableHeader.fontSize = 10;
						doc['header']=(function() {
							return {
								columns: [{
									    alignment: 'left',
										image: logo,
										width: 50,margin: [200,5]
									},{
										alignment: 'CENTER',
										text: 'List of Client',
										fontSize: 20,
										margin: [10,35],
									},],
								margin: [20, 0, 0,12],alignment: 'center',
							}
						});
						var objLayout = {};
						objLayout['hLineWidth'] = function(i) { return 2; };
						objLayout['vLineWidth'] = function(i) { return 2; };
						objLayout['hLineColor'] = function(i) { return '#ccc'; };
						objLayout['vLineColor'] = function(i) { return '#ccc'; };
						objLayout['paddingLeft'] = function(i) { return 14; };
						objLayout['paddingRight'] = function(i) { return 14; };
						doc.content[0].layout = objLayout;
				},
				titleAttr: 'PDF',
                  exportOptions: {
        columns: [0,1, 2, 3,4,5], // Only name, email and role
    }
            },
            {
           extend: 'print',
          text: '<i class="fa fa-print"></i>&nbsp; Print',
          titleAttr: 'Print',
        customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '10pt' )
                        .prepend(
                            '<center><img src=""/></center>'
                        );
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );
                },
         exportOptions: {
          columns: [0,1, 2, 3,4,5]
      },
      footer: true,
      autoPrint: true
    },],
    } );
$('input.global_filter').on( 'keyup click', function () {
        filterGlobal();
    } );
 
    $('input.column_filter').on( 'keyup click', function () {
        filterColumn( $(this).parents('tr').attr('data-column') );
    } );
  table.on( 'order.dt search.dt', function () {
       table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
          cell.innerHTML = i + 1;
          table.cell(cell).invalidate('dom'); 
       } );
    } ).draw();
       table.columns(6)
        .search('^(?:(?!Inactive|New|1).)*$\r?\n?', true, false)
        .draw();
    $("#choice").on("change",function(){
 var _val = $(this).val();//alert(_val);
 
 if(_val == 'Inactive'){   
        table.columns(6).search(_val).draw();
  }
 else if(_val == 'New'){   
        table.columns(6).search(_val).draw();
  }
  else if(_val == 'Active'){  //alert();
         table.columns(6).search(_val).draw();
          table.columns(6)
        .search('^(?:(?!Inactive|New|1).)*$\r?\n?', true, false)
        .draw();
  }
  else{
        table
        .columns()
        .search('')
        .draw(); 
  }
  })
} );
</script>
<style>
.table > thead > tr > th {background: #98c4f2;text-align: center;}
td{text-align: center !important;}
.form-group {margin-bottom: 15px;width: 100%;float: left;}
   </style>
@endsection