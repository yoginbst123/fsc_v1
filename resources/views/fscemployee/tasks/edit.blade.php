@extends('fscemployee.layouts.app')
@section('main-content')
<div class="content-wrapper">
      <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>Edit Task</h1>
    </section>
   <!-- Main content -->
    <section class="content">
   <div class="row">
      <div class="col-md-12">
         <div class="box box-success">
			      <div class="box-header">
          
              <div class="box-tools pull-right">
                
              </div>
            </div>
            <div class="card-body col-md-12">
               <form method="post" action="{{route('tasks.update',$task->id)}}" class="form-horizontal" id="content" name="content" enctype="multipart/form-data">
                  {{csrf_field()}} {{method_field('PATCH')}}
                   <div class="form-group">
                   <label class="control-label col-md-3">Date / Day / Time:</label>
                     <div class="col-md-9">
                         <div class="row">
                     <div class="col-md-2">
                        <div class="">
                           <input type="text" name="date" id="date" class="form-control" value="{{date('M-d Y',strtotime($task->created_at))}}" readonly placeholder="Date">
                        </div>
                     </div>
                      
                     <div class="col-md-2" style="width: 115px;">
                        <div class="row">
                           <input type="text" name="day" id="day" class="form-control" placeholder="Day" readonly value="{{date('l',strtotime($task->created_at))}}">
                        </div>
                     </div>
                     <input type="hidden" name="state" readonly id="state" value="employee"  class="form-control">
                     <div class="col-md-2" style="width:18.3% !important;" >
                        <div class="">
                           <input type="text" name="time" id="time" class="form-control" readonly value="{{date('g:i a',strtotime($task->created_at))}}" placeholder="Time">
                        </div>
                     </div>
                     </div></div>
                  </div>
<div class="form-group{{ $errors->has('employee') ? ' has-error' : '' }}">
                     <label class="control-label col-md-3">Employee / User :</label>
                     <div class="col-md-4" style="width:35.5%">
                        <div class="">
                           <select name="employee" id="employee" class="form-control fsc-input">
                              <option value="">Select Employee</option>
                               
                              @foreach($emp1 as $em)
                               <option value='{{$em->id}}' @if($task->employeeid==$em->id) selected @endif>{{$em->firstName.' '.$em->middleName.' '.$em->lastName}}</option>
                                                       @endforeach
                           </select>
                        </div>
                        @if ($errors->has('employee'))
                        <span class="help-block">
                        <strong>{{ $errors->first('employee') }}</strong>
                        </span>
                        @endif	
                     </div>
                  </div>
                     <div class="form-group{{ $errors->has('employee') ? ' has-error' : '' }}">
                     <label class="control-label col-md-3">For Whom :</label>
                     <div class="col-md-4" style="width:35.5%">
                        <div class="">
                           <select name="whone" id="whone" class="form-control fsc-input">
                              <option value="">Select</option>
                              <option value='Client' @if($task->whone=='Client') selected @endif>Client</option>
                              <option value='Other' @if($task->whone=='Other') selected @endif>Other</option>
                           </select>
                        </div>
                        	
                     </div>
                  </div>
                   <div class="form-group{{ $errors->has('client') ? ' has-error' : '' }}" style="display:none;"  id="clients">
                     <label class="control-label col-md-3">Client :</label>
                     <div class="col-md-4" style="width:35.5%">
                        <div class="">
                           <select name="client" id="client" class="form-control fsc-input">
                              <option value="">Select</option>
                             @foreach($customer as $cust)
                              <option value='{{$cust->id}}' <?php if($task->client==$cust->id) { echo 'Selected';}?>><?php if($cust->business_id !='6') { echo $cust->company_name.' ('.$cust->filename.')';} else { echo $cust->first_name.' '.$cust->last_name.' ('.$cust->filename.')';}?></option>
                              @endforeach
                           </select>
                        </div>
                        	
                     </div>
                  </div>
                   <div class="form-group{{ $errors->has('other') ? ' has-error' : '' }}" style="display:none;"  id="client1">
                     <label class="control-label col-md-3">Other :</label>
                     <div class="col-md-4" style="width:35.5%">
                        <div class="">
                            <input type="text" name="othername" id="othername" class="form-control fsc-input">
                        </div>
                        	
                     </div>
                  </div>
                  <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                     <label class="control-label col-md-3">Subject :</label>
                     <div class="col-md-8">
                        <div class="">
                           <input type="text" value="{{$task->title}}" name="title" id="title" class="form-control fsc-input">
                            <input type="hidden" value="{{$task->admin_id}}" name="admin_id" id="admin_id" class="form-control fsc-input">
                        </div>
                     </div>
                  </div>
                   <div class="form-group">
                     <label class="control-label col-md-3">Priority :</label>
                     <div class="col-md-8">
                        <div class="">
                           <select name="priority" id="priority" class="form-control fsc-input">
                              <option value='Regular' @if($task->priority=='Regular') selected @endif>Regular</option>
                             <option value='Urgent' @if($task->priority=='Urgent') selected @endif>Urgent</option>
                             <option value='Very Urgent' @if($task->priority=='Very Urgent') selected @endif>Very Urgent</option>
                             
                           </select>
                        </div>
                        
                     </div>
                  </div>
                  
                  <div class="form-group">
                     <label class="control-label col-md-3">Due Date :</label>
                     <div class="col-md-8">
                        <div class="">
                           <input type="text" name="duedate" id="duedate" value="{{$task->duedate}}" class="form-control fsc-input" readonly>
                        </div>
                     
                     </div>
                  </div>
                  <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                     <label class="control-label col-md-3">Description :</label>
                     <div class="col-md-8">
                        <div class="">
                           <textarea id="editor1" name="description" rows="10" cols="80">{!!$task->content!!}</textarea>
                        </div>
                        @if ($errors->has('description'))
                        <span class="help-block">
                        <strong>{{ $errors->first('description') }}</strong>
                        </span>
                        @endif	
                     </div>
                  </div>
                    
                     <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                     <label class="control-label col-md-3">Status :</label>
                     <div class="col-md-4" style="width:35.5%">
                        <div class="">
                           <select name="status" id="status" class="form-control fsc-input">
                              <option value="">Select</option>
                              <option value='1' @if($task->status=='1') selected @endif>In Progress</option>
                              <option value='2' @if($task->status=='2') selected @endif>Wait</option>
                              <option value='3' @if($task->status=='3') selected @endif>Done</option>
                           </select>
                        </div>
                        	
                     </div>
                  </div>
                    
                   <div class="form-group">
                     <label class="control-label col-md-3"></label>
                     <div class="col-md-8">
                        <div class="row">
                           <div class="card-footer">
                      <div class="col-md-2">
									<input class="btn_new_save btn-primary1" type="submit" name="submit" value="Send">
									</div>
									<div class="col-md-2 row">
									<a class="btn_new_cancel" href="{{url('fscemployee/tasks')}}">Cancel</a> 
									</div>
                  
                  </div>
                        </div>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
       </section>
</div>

<script>
   $(document).ready(function() {
  var dateInput = $('input[name="duedate"]'); 
  var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : 'body';
  dateInput.datepicker({
    format: 'M-dd-yyyy',
    container: container,
    todayHighlight: true,
    autoclose: true,
    startDate: truncateDate(new Date())
  });

  $('#duedate').datepicker('setStartDate', truncateDate(new Date())); 
});

function truncateDate(date) {
  return new Date(date.getFullYear(), date.getMonth(), date.getDate());
}
</script>
<script>
$(document).ready(function()
{
    var tvalue=$('#whone').val();
    //alert(tvalue);
    if(tvalue=='Client')
      {
          $('#clients').show();
          $('#client1').hide();
      }
      else if(tvalue=='Other')
      {
          $('#clients').hide();
          $('#client1').show();
      }
      else
      {
          $('#clients').hide();
          $('#client1').hide();
      }

    $('#whone').on('change', function() {
          if(this.value=='Client')
          {
              $('#clients').show();
              $('#client1').hide();
          }
          else if(this.value=='Other'){
              $('#clients').hide();
              $('#client1').show();
              
          }
          else
          {
              $('#clients').hide();
              $('#client1').hide();
          }
        });
});
</script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6/js/select2.min.js"></script>
<script>
$("#client").select2({
})
</script>
<style>
    .select2 {width:100% !important;}
    .select2-container .select2-selection--single {
   
    border: 2px solid #00468F;
}.select2-container .select2-selection--single{ height:36px;}
</style
@endsection()