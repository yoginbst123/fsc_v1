@extends('fscemployee.layouts.app')
@section('main-content')
<style>
.table > thead > tr > th {background: #ffff99; text-align:center;}
label{float:left;}
.dt-buttons{
    margin-bottom:10px;
}
.page-title{
        padding: 8px 15px;

}
</style>
<style>
    .buttons-pdf {
    font-size: 0px !important;
    border-radius: 3px;
    padding: 8px 10px !important;
    background: #fff !important;
    border-color: #c6c6c6  !important;
    color:red !important;
}
.buttons-print {
    font-size: 0px !important;
    border-radius: 3px;
    padding: 8px 10px !important;
    background: #3c8dbc !important;
            border-color: #367fa9 !important;
}
.buttons-excel{
    font-size:0px !important;
    border-radius: 3px;
        padding: 8px 10px !important;
        
         background: #00a65a !important;
    border-color: #008d4c !important;
        

}
.buttons-excel:hover{
     background: #008d4c !important;

}
.buttons-pdf:hover{
     background: #f6f6f6  !important;
}

.buttons-print:hover{
     background: #367fa9 !important;
}

.search-btn{position:absolute;top:10px;right:16px;background:transparent;border: transparent;}
.fa{
    font-size: 16px !important;
}
.taskoxmian{width: 300px;
    position: absolute;
    z-index: 999;
    display: flex;
    flex-direction: row;
    /*left: 30%;*/
    top: 30px;}
.taskoxmian form{width: 100%;
    display: flex;}
.taskoxmian .firstbox{width: 250px;}
#example_filter{
    display:none;
}
</style>
<div class="content-wrapper">
      <!-- Content Header (Page header) -->
    
    <section class="content-header page-title" >
     		<div class="" style="">
     		    <div class="" style="text-align:center;">
     		        <h1>List of Task <span style="padding-right:10px;float:right;">Add / View / Edit</span></h1>
     		    </div>
     		   
     		</div>
    </section>
      <!-- Main content -->
    <section class="content">		
	<div class="row">
	
		<div class="col-md-12">
		    @if( session()->has('success') )
                        <div class="alert alert-success alert-dismissable">{{session()->get('success') }}</div>
                @endif

			<div class="box box-success">
			      <div class="box-header">
			          <div class="col-md-8" style="padding-right:0px;width:65% !important;position: absolute;">
              <div class="row">
              <div class="col-md-4" style="width: 130px; padding-right:0px;">
                <form  class="form-horizontal" action="{{url('fscemployee/tasks/search')}}"  method="post">
                
                  {{csrf_field()}}
                  
    <div class="firstbox"><select name="choice" style="width: 92%;margin-left: 4px;" id="choice" class="form-control">
         <option value="">All</option>
        <option value="1" <?php if(isset($_POST['choice']) && $_POST['choice'] =='1') { echo 'Selected';}?>>In Progress</option>
        <option value="2" <?php if(isset($_POST['choice']) && $_POST['choice'] =='2') { echo 'Selected';}?>>Wait</option>
        <option value="3" <?php if(isset($_POST['choice']) && $_POST['choice'] =='3') { echo 'Selected';}?>>Done</option>
        
    </select></div>
           
    <!--         <div>-->
    <!--             <input type="submit" class="btn btn-success" name="sub" value="Ok">-->
    <!--</div>-->
    
              </form>
                </div>
                 
     <div class="col-md-3" style="width: 196px;">
     <table style="width: 100%;" cellspacing="0" cellpadding="3" border="0">
        <tbody>
         <tr id="filter_global">
                <td align="center"><input type="text" class="global_filter form-control" id="global_filter" placeholder="All Search"><button class="search-btn"><i class="fa fa-search"></i></button></td>
            </tr>
         
            <tr id="filter_col2" data-column="1" style="display:none">
                <td align="center"><input type="text" class="column_filter form-control" id="col1_filter" placeholder="Type"><button class="search-btn"><i class="fa fa-search"></i></button></td>
            </tr>
            <tr id="filter_col3" data-column="2" style="display:none">
                <td align="center"><input type="text" class="column_filter form-control" id="col2_filter" placeholder="EE / User Id"><button class="search-btn"><i class="fa fa-search"></i></button></td>
            </tr>
            <tr id="filter_col4" data-column="3" style="display:none">
                <td align="center"><input type="text" class="column_filter form-control" id="col3_filter" placeholder="Employee Name"><button class="search-btn"><i class="fa fa-search"></i></button></td>
            </tr>
            <tr id="filter_col5" data-column="4" style="display:none">
                <td align="center"><input type="text" class="column_filter form-control" id="col4_filter" placeholder="Email Id"><button class="search-btn"><i class="fa fa-search"></i></button></td>
            </tr>
            <tr id="filter_col6" data-column="5" style="display:none">
                <td align="center"><input type="text" class="column_filter form-control" id="col5_filter" placeholder="Tel. Number"><button class="search-btn"><i class="fa fa-search"></i></button></td>
            </tr>
        </tbody>
    </table>
    </div>
    <div class="col-md-2" style="padding-left: 0px; padding-right: 0px; width: 95px; margin-top:4px;">
     <div class="box-tools pull-right" data-toggle="tooltip" title="Status">
                
              </div>
              </div>
    </div>
    </div>
			          <!--<div class="taskoxmian">-->
            
             <!-- </div>-->
                 
              <div class="box-tools pull-right" style="position: absolute;margin-top: 27px;margin-right: 150px;z-index:9999;">
                	<div class="table-title">
						<!--<a href="{{url('fscemployee/tasks/create')}}">Add New Task</a>-->
					</div>
              </div>
            
				<div class="card-body col-md-12">
					<div class="table-responsive">
						<table class="table table-hover table-bordered" id="example">
							<thead>
								<tr>
									<th width="5%">No</th>
									<th style="width:10%">Creation Date</th>
									<Th>For Whom</Th>
									<th>Subject</th>
									<th>Priority</th>
									<th>Send By</th>
									<th>Status</th>
									<th width="8%">Action</th>
								</tr>
							</thead>							
							<tbody>
                            @foreach($task as $com)
								<tr>
									<td>{{$loop->index+1}}</td>
									<td>@if($com->created_at !='') {!! date('m-d-Y',strtotime($com->created_at)) !!}@endif</td>
										<td>
										    <?php 
										    if($com->whone =='Client') 
										    {
										    foreach($clients as $com1) 
										    { 
										    if($com->client==$com1->id) 
										    {
										    if($com1->business_id =='6') 
										    {
										    echo $com->whone.' ('.ucwords($com1->first_name.' '.$com1->middle_name.' '.$com1->last_name).')';
										    }
										    else 
										    {
										    echo $com->whone.' ('.ucwords($com1->company_name).')';
										    }
										    }
										    }
										    }
										    else if($com->whone =='Other')
										    {
										    echo $com->othername;
										    }?></td>
								
									
									
									<td>{{$com->title}}</td>
									
									<td style="text-align:center;">{!! $com->priority !!}</td>
									<td>@if($com->admin_id==$admin->id) {{$admin->fname.' '.$admin->lname}} @endif </td>
									<td> @if($com->status==1) In Progress @endif  @if($com->status==2) Wait @endif @if($com->status==3) Done @endif</td>
									<td>

										<a class="btn-action btn-view-edit" href="{{route('tasks.edit',$com->id)}}"><i class="fa fa-edit"></i></a>
										<a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record ?'))
                                                                                  {event.preventDefault();document.getElementById('delete-id-{{$com->id}}').submit();} else{event.preventDefault();}" href="{{route('clientsetup.destroy',$com->id)}}"><i class="fa fa-trash"></i></a>
<form action="{{route('tasks.destroy',$com->id)}}" method="post" style="display:none" id="delete-id-{{$com->id}}">
                                        {{csrf_field()}} {{method_field('DELETE')}}
                                        </form>
									</td>
								</tr>
							@endforeach
							</tbody>
							
						</table>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	    </section>
</div>
<script>
$(document).ready(function() {
    var table = $('#example').DataTable( {
        dom: 'Bfrtlip',
    "columnDefs": [ {
            "searchable": true,
            "orderable": true,
            "targets": 0
        } ],
        "order": [[ 0, 'asc' ]],
        buttons: [
                 {
                extend:    'copyHtml5',
                text:      '<i class="fa fa-files-o"></i> &nbsp; Copy',
                titleAttr: 'Copy',
                title: $('h3').text(),
                  exportOptions: {
        columns: [0,1, 2, 3,4,5], // Only name, email and role
    }
            },
            {
                extend:    'excelHtml5',
                text:      '<i class="fa fa-file-excel-o"></i>&nbsp; Excel',
               titleAttr: 'Excel',
                title: $('h3').text(),
                 customize: function( xlsx ) {
                var sheet = xlsx.xl.worksheets['sheet1.xml'];
 $('row c', sheet).attr('s', '51');
            },
                  exportOptions: {
        columns: [0,1, 2, 3,4,5], // Only name, email and role
    }
            },
            {
                extend:    'csvHtml5',
                text:      '<i class="fa fa-file-text-o"></i> &nbsp; CSV',
                titleAttr: 'CSV',
                title: $('h3').text(),
                  exportOptions: {
        columns: [0,1, 2, 3,4,5], // Only name, email and role
    }
            },
            {
                extend:    'pdfHtml5',
                text:      '<i class="fa fa-file-pdf-o"></i>&nbsp;  PDF',
                
              customize: function (doc) {
						//Remove the title created by datatTables
						doc.content.splice(0,1);
						//Create a date string that we use in the footer. Format is dd-mm-yyyy
						var now = new Date();
						var jsDate = now.getDate()+'-'+(now.getMonth()+1)+'-'+now.getFullYear();
						
						doc.pageMargins = [20,60,20,20];
						doc.defaultStyle.fontSize = 10;
						doc.styles.tableHeader.fontSize = 10;
						doc['header']=(function() {
							return {
								columns: [{
									    alignment: 'left',
										image: logo,
										width: 50,margin: [200,5]
									},{
										alignment: 'CENTER',
										text: 'List of Client',
										fontSize: 20,
										margin: [10,35],
									},],
								margin: [20, 0, 0,12],alignment: 'center',
							}
						});
						var objLayout = {};
						objLayout['hLineWidth'] = function(i) { return 2; };
						objLayout['vLineWidth'] = function(i) { return 2; };
						objLayout['hLineColor'] = function(i) { return '#ccc'; };
						objLayout['vLineColor'] = function(i) { return '#ccc'; };
						objLayout['paddingLeft'] = function(i) { return 14; };
						objLayout['paddingRight'] = function(i) { return 14; };
						doc.content[0].layout = objLayout;
				},
				titleAttr: 'PDF',
                  exportOptions: {
        columns: [0,1, 2, 3,4,5], // Only name, email and role
    }
            },
            {
           extend: 'print',
          text: '<i class="fa fa-print"></i>&nbsp; Print',
          titleAttr: 'Print',
        customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '10pt' )
                        .prepend(
                            '<center><img src=""/></center>'
                        );
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );
                },
         exportOptions: {
          columns: [0,1, 2, 3,4,5]
      },
      footer: true,
      autoPrint: true
    },],
    } );
$('input.global_filter').on( 'keyup click', function () {
        filterGlobal();
    } );
 
    $('input.column_filter').on( 'keyup click', function () {
        filterColumn( $(this).parents('tr').attr('data-column') );
    } );
  table.on( 'order.dt search.dt', function () {
       table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
          cell.innerHTML = i + 1;
          table.cell(cell).invalidate('dom'); 
       } );
    } ).draw();
       table.columns(6)
        .search('^(?:(?!Inactive|New|1).)*$\r?\n?', true, false)
        .draw();
    $("#choice").on("change",function(){
 var _val = $(this).val();//alert(_val);
 
 if(_val == 'Inactive'){   
        table.columns(6).search(_val).draw();
  }
 else if(_val == 'New'){   
        table.columns(6).search(_val).draw();
  }
  else if(_val == 'Active'){  //alert();
         table.columns(6).search(_val).draw();
          table.columns(6)
        .search('^(?:(?!Inactive|New|1).)*$\r?\n?', true, false)
        .draw();
  }
  else{
        table
        .columns()
        .search('')
        .draw(); 
  }
  })
} );
   	</script>
@endsection()