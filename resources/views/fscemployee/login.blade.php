<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="{{URL::asset('public/dashboard/css/main.css')}}">
	<title>FSC - Employee</title>
<style>
.help-block{ color:red}   
.semibold-text a { color: red; }
</style>
</head>

<body>

<div class="employee-logo-bg">

	<section class="employee-login-content">
	
		<div class="employee-logo">
			<img src="{{asset('public/dashboard/images/logo_employee.png')}}" alt="" />
		</div>
		
		<div class="employee-login-box">

			<form class="login-form" method="post" action="{{route('fscemployee.login')}}" id="admin-login">
			{{ csrf_field() }}
				<h3 class="login-head">Employee / User Login</h3>
				
				<div class="form-group">
					<label class="employee-control-label">USERNAME</label>
					<input class="employee-form-control" type="text"  name="email" value="{{ old('email') }}" id="email" placeholder="Email" autofocus>
					@if ($errors->has('email'))
					<span class="help-block">
						<strong>{{ $errors->first('email') }}</strong>
					</span>
					@endif
				</div>
				
				<div class="form-group">
					<label class="employee-control-label">PASSWORD</label>
					<input class="employee-form-control" type="password" name="password" id="newpassword" placeholder="Password">
					 <span toggle="#newpassword" style="font-size: 25px;margin-left:-35px;margin-top: 6px;" class="fa fa-fw fa-eye field-icon toggle-password"></span>
					@if ($errors->has('password'))
					<span class="help-block">
						<strong>{{ $errors->first('password') }}</strong>
					</span>
					@endif
				</div>
				
				<div class="form-group btn-container">
					<button  type="submit" name="submit" class="btn_submit">Login</button>
				</div>
				
				<div class="form-group">
					<div class="utility">
					<!--	<p class="semibold-text mb-0"><a data-toggle="flip-1">Forgot Password ?</a></p>-->
						<p class="semibold-text mb-0"><a href="{{ route('forgotpassword.index') }}">Forgot Password ?</a></p>
					<p class="semibold-text mb-0"><a data-toggle="flip-12" href="{{ route('forgotempusername.index') }}">Forgot Username ?</a></p>
					</div>
				</div>
				
				<center>@if ( session()->has('success') )
				<div class="alert alert-success alert-dismissable">{{ session()->get('success') }}</div>
				@endif
				@if ( session()->has('error') )
				<div class="alert alert-danger alert-dismissable">{{ session()->get('error') }}</div>
				@endif
				<div id="Register" style="margin-top:20px;width: 100%;display: inline-block;"></div></center>
				
			</form>
			
			<form class="password-form" method="POST" action="#" id="forgotpassword">
			{{ csrf_field() }}
				<h3 class="login-head"><i class="fa fa-lg fa-fw fa-lock"></i>Forgot Password ?</h3>
				
				<div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
					<label for="email" class="employee-control-label">EMAIL</label>
					<input class="employee-form-control" id="email1" type="email" name="email1" value="{{ old('email') }}" required placeholder="Email">
					@if ($errors->has('email'))
					<span class="help-block">
					<strong>{{ $errors->first('email') }}</strong>
					</span>
					@endif
				</div>
				
				<div id="emailmsg"></div>
				
				<div class="form-group btn-container">
					<button  type="submit" name="submit" class="btn_reset">RESET </button>
				</div>
				
				<div class="form-group mt-10">
					<p class="semibold-text mb-0"><a data-toggle="flip-1"><i class="fa fa-angle-left fa-fw"></i> Back to Login</a></p>
				</div>
				
				<center><div id="Register1" style="margin-top:20px;width: 100%;display: inline-block;"></div></center>
				
			</form>
		
			@if (session('status'))
			<div class="alert alert-success">
			{{ session('status') }}
			</div>
			@endif
		</div>
		
	</section>
	
</div>

</body>

<script src="{{asset('public/dashboard/js/jquery-2.1.4.min.js')}}"></script>
<script src="{{asset('public/dashboard/js/bootstrap.min.js')}}"></script>
<script src="{{asset('public/dashboard/js/plugins/pace.min.js')}}"></script>
<script src="{{asset('public/dashboard/js/main.js')}}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js"></script>
<!--<script>
$('#admin-login').bootstrapValidator({
    message: 'This value is not valid',
    live: 'disabled',
    feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    submitHandler: function(validator, form, submitButton) {
        $.ajax({
          type: "POST",
          url: "{{ URL::to('fscemployee/login') }}",
          data: $('#admin-login').serialize(),
          success: function(msg){            
			  $('#Register').html('<span class="alert alert-success">You Are SuccessFully Login!!!</span>');
			  setTimeout(function(){ location.href="{{ URL::to('/fscemployee/home') }}"; }, 0);
			 
          },
          error: function(){
            $('#Register').html('<span class="alert alert-danger">Your Username And Password Invalid !!!</span>');
          }
        });//close ajax
    },
    fields: {
        email: {              
            validators: {
                notEmpty: {
                    message: 'Please Enter Your Email !!!'
                },
				emailAddress: {
                        message: 'Please Enter Your Valid Email Address'
					}					
				
            }
        },
        password: {
            
            validators: {
                notEmpty: {
                    message: 'Please Enter Your Password !!!'
                }
            }
        },
    } // end field
});



	</script>-->
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script>
$(".toggle-password").click(function() {

  $(this).toggleClass("fa-eye fa-eye-slash");
  var input = $($(this).attr("toggle"));
  if (input.attr("type") == "password") {
    input.attr("type", "text");
  } else {
    input.attr("type", "password");
  }
});
</script>
</html>