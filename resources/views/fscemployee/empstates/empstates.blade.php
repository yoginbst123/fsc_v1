@extends('fscemployee.layouts.app')

@section('main-content')
<style>
    .page-title{
    padding:8px 15px !important;
</style>
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
    <section class="content-header page-title" style="">
     		<div class="">
     		    <div class="" style="text-align:center;">
     		        <h1>List of Tax Authorities <span style="padding-right:10px;float:right;">Add / View / Edit</span></h1>
     		    </div>
     		    
     		</div>
    </section>
    <!-- Main content -->
    <section class="content">
	<div class="row">
	<!--<div class="branch">Federal</div>-->
		<div class="col-md-12">
			<div class="box box-success">
			      <div class="box-header">
             
              <div class="box-tools pull-right">
                <div class="table-title">
					<!--<a href="{{route('states.create')}}">Add New</a>-->
					</div>
              </div>
            </div>
				<div class="col-md-12">
					
<br>
					<div class="table-responsive">
						<table class="table table-hover table-bordered" id="sampleTable3">
							<thead>
								<tr>
									<th width"6%">No</th>
									<th>Type of Entity</th>
									<th>Type Of Tax</th>
         							<th>Forms Name</th>
         							<th width="11%">Due Date</th>
         							<th>Extension Due Date</th>
									<th width="8%">Action</th>
								</tr>
							</thead>
							<tbody>
                                @foreach($price as $bus)
                                    <tr>
                                        <td style="text-align:center;">{{$loop->index + 1}}</td>
                                        <td style="text-align:center;">{{$bus->authority_name_state}}</td>	
                                        <td style="text-align:center;">{{$bus->typeofform_state}}</td>
                                        <td>{{$bus->formname_state}}</td>	
                                        <td style="text-align:center;"><?php echo date('M-d Y',strtotime($bus->due_date_state))?></td>	
                                        <td style="text-align:center;">{{$bus->extension_due_date_state}}</td>	
                                        <td style="text-align:center;">
                                            <a class="btn-action btn-view-edit" href="{{route('empstates.edit', $bus->id)}}"><i class="fa fa-edit"></i></a>
                                            <form action="{{ route('empstates.destroy',$bus->id) }}" method="post" style="display:none" id="delete-id-{{$bus->id}}">
                                                {{csrf_field()}} {{method_field('DELETE')}}
                                            </form>
                                            <a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record ?'))
                                            {event.preventDefault();document.getElementById('delete-id-{{$bus->id}}').submit();} else{event.preventDefault();}" href=""><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		
	</div>
		 </section>
</div>
	
@endsection()