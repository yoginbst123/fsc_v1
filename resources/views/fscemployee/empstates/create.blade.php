@extends('fscemployee.layouts.app')
@section('main-content')
<style>
    .professional_btn {width: 10%;}
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>Tax Authorities / Form</h1>
    </section>
    <!-- Main content -->
    <section class="content">
   <div class="row">
      <div class="col-md-12">
         <div class="box box-success">
			      <div class="box-header">
             
              <div class="box-tools pull-right">
                
              </div>
            </div>
				<div class="col-md-12">
               <form method="post" action="{{route('empstates.store')}}" class="form-horizontal" id="businessname" name="businessname" enctype="multipart/form-data">
                  {{csrf_field()}}
                    <!-- <div class="form-group {{ $errors->has('short_name') ? ' has-error' : '' }}">
                     <label class="control-label col-md-3">Type :</label>
                     <div class="col-md-4">
                         	<input type="hidden" name="type" id="rend" class="form-control" value="{{rand(55555,99999)}}"  placeholder="" />
                         <select type="text" class="form-control fsc-input" name="short_name" id="short_name" placeholder="Enter Your Company Name" >
                                    <option value="">---Select---</option>
                                   	<option value="Entity">Entity</option>
									<option value="Payroll">Payroll</option>
									<option value="Sales">Sales</option>
									<option value="Tobacco">Tobacco</option>
									<option value="COAM">COAM</option>
									<option value="License">License</option>
									<option value="Income">Income</option>
									<option value="Excise">Excise</option>
									<option value="GST">GST</option>
									<option value="VAT">VAT</option>
                                 </select>
                        @if ($errors->has('short_name'))
                        <span class="help-block">
                        <strong>{{ $errors->first('short_name') }}</strong>
                        </span>
                        @endif
                     </div>
                     <div class="col-md-4">
                         	<input type="radio" name="yearlytype" id="yearlytype" value="15"  placeholder="" /> 15 days
                         	
                         	<input type="radio" name="yearlytype" id="yearlytype" value="30"  placeholder="" /> 30 days
                         	<input type="radio" name="yearlytype" id="yearlytype" value="45"  placeholder="" /> 45 days
                         	<input type="radio" name="yearlytype" id="yearlytype" value="60"  placeholder="" /> 60 days
                         
                        
                     </div>
                  </div>-->
                  <div class="form-group {{ $errors->has('typeofform') ? ' has-error' : '' }}">
                     <label class="control-label col-md-3">Type of Tax :</label>
                     <div class="col-md-4">
<select type="text" class="form-control fsc-input" name="typeofform" id="typeofform" placeholder="Enter Your Company Name" >
                                    <option value="">---Select---</option>
                                    <option value="Income Tax">Income Tax</option>
                                    <option value="Payroll Tax">Payroll Tax</option>
                                    <option value="Sales Tax">Sales Tax</option>
                                 </select>
                        @if ($errors->has('typeofform'))
                        <span class="help-block">
                        <strong>{{ $errors->first('typeofform') }}</strong>
                        </span>
                        @endif
                     </div>
                  </div>
                  <div id="hh">
                 <div class="form-group {{ $errors->has('authority_name') ? ' has-error' : '' }}">
                     <label class="control-label col-md-3">Type of Entity :</label>
                     <div class="col-md-4">
                        <select type="text" class="form-control fsc-input" name="authority_name" id="authority_name" placeholder="Enter Your Company Name" >
                                    <option value="">---Select---</option>
                                    
                                    @foreach($entity as $entity1)
                                    <option value="{{$entity1->typeentity}}">{{$entity1->typeentity}}</option>
                                    @endforeach
                                 </select>
                        @if ($errors->has('authority_name'))
                        <span class="help-block">
                        <strong>{{ $errors->first('authority_name') }}</strong>
                        </span>
                        @endif
                     </div><div class="col-md-4"><button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i></button></div>
                  </div>
                  
               <div class="form-group {{ $errors->has('state') ? ' has-error' : '' }}">
                     <label class="control-label col-md-3">State :</label>
                     <div class="col-md-4">
                        <select type="text" class="form-control fsc-input" name="state" id="state" placeholder="Enter Your Company Name">
                            <option value="">Select</option>
                                   <option value="AL">AL</option><option value="AK">AK</option><option value="AS">AS</option><option value="AZ">AZ</option><option value="AR">AR</option><option value="AF">AF</option><option value="AA">AA</option><option value="AC">AC</option><option value="AE">AE</option><option value="AM">AM</option><option value="AP">AP</option><option value="CA">CA</option><option value="CO">CO</option><option value="CT">CT</option><option value="DE">DE</option><option value="DC">DC</option><option value="FM">FM</option><option value="FL">FL</option><option value="GA">GA</option><option value="GU">GU</option><option value="HI">HI</option><option value="ID">ID</option><option value="IL">IL</option><option value="IN">IN</option><option value="IA">IA</option><option value="KS">KS</option><option value="KY">KY</option><option value="LA">LA</option><option value="ME">ME</option><option value="MH">MH</option><option value="MD">MD</option><option value="MA">MA</option><option value="MI">MI</option><option value="MN">MN</option><option value="MS">MS</option><option value="MO">MO</option><option value="MT">MT</option><option value="NE">NE</option><option value="NV">NV</option><option value="NH">NH</option><option value="NJ">NJ</option><option value="NM">NM</option><option value="NY">NY</option><option value="NC">NC</option><option value="ND">ND</option><option value="MP">MP</option><option value="OH">OH</option><option value="OK">OK</option><option value="OR">OR</option><option value="PW">PW</option><option value="PA">PA</option><option value="PR">PR</option><option value="RI">RI</option><option value="SC">SC</option><option value="SD">SD</option><option value="TN">TN</option><option value="TX">TX</option><option value="UT">UT</option><option value="VT">VT</option><option value="VI">VI</option><option value="VA">VA</option><option value="WA">WA</option><option value="WV">WV</option><option value="WI">WI</option><option value="WY">WY</option>
                                 </select>
                       
                     </div>
                  </div>

                  <div class="form-group">
							<label class="control-label col-md-3">Form Name :</label>
							<div class="col-md-4">
								<input type="text" name="formname" id="formname" class="form-control" placeholder="Form Name" />
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Due Date :</label>
							<div class="col-md-4">
								<input type="text" name="duedate" id="duedate" class="form-control" placeholder="Due Date" />
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3">Extension Due Date  :</label>
							<div class="col-md-4">
								<input type="text" name="extdate" id="extdate" class="form-control" placeholder="Extension Due Date" />
							</div>
						</div>
						
                  	<div class="form-group">
							<label class="control-label col-md-3">Authority Short Name :</label>
							<div class="col-md-4">
								<input type="text" name="authority_level" id="authority_level" class="form-control" placeholder="Authority Level" />
							</div>
						</div>
                  	<div class="form-group">
							<label class="control-label col-md-3">Authority Full Name :</label>
							<div class="col-md-4">
								<input type="text" name="authority_name1" id="authority_name1" class="form-control" placeholder="Authority Name" />
							</div>
						</div>
							<div class="form-group">
							<label class="control-label col-md-3">Website :</label>
							<div class="col-md-4">
								<input type="text" name="website" id="website" class="form-control" placeholder="Website" />
							</div>
						</div>
							<div class="form-group">
							<label class="control-label col-md-3">Telephone # :</label>
							<div class="col-md-4">
								<input type="text" name="telephone" id="telephone" class="form-control" placeholder="Telephone" />
							</div>
						</div>
					<!--	<div class="form-group">
							<label class="control-label col-md-3">Renew Link :</label>
							<div class="col-md-4">
								<input type="text" name="renewlink" id="renewlink" class="form-control" placeholder="Renew Link" />
							</div>
						</div>-->
						</div>
	          <div class="form-group" id="payroll" style="display:none">
							<label class="control-label col-md-3">Payroll :</label>
							<div class="col-md-9">
								<div class="check_tab">
									<input type="radio" id="Federal" class="payroll" name="payroll" value="Federal">
									<label for="Federal">Federal</label>
								</div>
								<div class="check_tab">
									<input type="radio" id="State" name="payroll" class="payroll"  value="State">
									<label for="State">State</label>
								</div>
								<div class="check_tab">
									<input type="radio" id="County" name="payroll" class="payroll"  value="County">
									<label for="County">County</label>
								</div>
								<div class="check_tab" >
									<input type="radio" id="Local" name="payroll" class="payroll"  value="Local">
									<label for="Local">Local</label>
								</div>
							</div>
						</div>
	                    <div class="Branch" id="federal" style="display:none">
							<div class="col-md-3" style="text-align:left;">
								<h1 id="fed">Federal</h1>
							</div>
							<div class="col-md-6">
								<h1 style="font-size:20px;">Payroll Tax</h1>
							</div>
						</div>
						<div class="federal_tabs_main" id="federal_tabs_main"  style="display:none">
							<div class="federal_tabs">
								<div class="federal_tab federal_name">
									<label>Short Name</label>
									<input type="text" class="form-control" name="payroll_name" placeholder="">
								</div>
								<div class="federal_tab federal_depart">
									<label>Department Name</label>
									<input type="text" class="form-control" name="payroll_department_name" placeholder="">
								</div>
								<div class="federal_tab federal_link">
									<label>Link</label>
									<input type="text" class="form-control" name="payroll_link" placeholder="">
								</div>
								<div class="federal_tab federal_tele">
									<label>Telephone #</label>
									<input type="text" class="form-control" id="payroll_telephone" name="payroll_telephone" placeholder="">
								</div>
							</div>
						</div>
						<div class="filing_tabs_main" id="filing_tabs_main"  style="display:none">
							<h3 class="comm_title_one">Filing Frequency</h3>
							<div class="filing_tabs">
								<div class="filing_tab filing_form">
									<label>Form #</label>
									<input type="text" class="form-control" name="filing_frequency_form[]" placeholder="">
								</div>
								<div class="filing_tab filing_name">
									<label>Form Name</label>
									<input type="text" class="form-control" placeholder="Form Name" name="filing_frequency_name[]">
								</div>
								<div class="filing_tab filing_freq">
									<label>Filing Frquency</label>
							<select type="text" class="form-control-insu" id="filing_frequency" name="filing_frequency[]">
					            <option value="Annually">Yearly</option>
				            	<option value="Monthly">Monthly</option>
					            <option value="Quaterly">Quaterly</option>
					        </select>
								</div>
								<div class="filing_tab filing_due_date">
									<label>Due Date</label>
									<input type="text" class="form-control" placeholder="" id="filing_frequency_due_date" name="filing_frequency_due_date[]">
								</div>
								<div class="filing_add">
									<button type="button" id="add_row1" class="filing_addbtn btn-success">ADD</button>
								</div>
								<div class="filing_formula">
									<button type="button" id="" class="btn_formula btn-default">Formula</button>
								</div>
							</div>
						</div>
						<div class="payment_tabs_main" style="display:none">
							<h3 class="comm_title_two">Payment Frequency</h3>
							<div class="payment_tabs">
								<div class="payment_tab payment_form">
									<label>Form #</label>
									<input type="text" class="form-control" placeholder="" name="filing_frequency_form1[]">
									
								</div>
								<div class="payment_tab payment_name">
									<label>Form Name</label>
									<input type="text" class="form-control" placeholder="Form Name" name="filing_frequency_name1[]">
								</div>
								<div class="payment_tab payment_freq">
									<label>Payment Frquency</label>
								<select type="text" class="form-control-insu" id="filing_frequency1" name="filing_frequency1[]">
					<option value="Annually">Yearly</option>
					<option value="Monthly">Monthly</option>
					<option value="Quaterly">Quaterly</option>
					</select>
								</div>
								<div class="payment_tab payment_due_date">
									<label>Due Date</label>
									<input type="text" class="form-control" placeholder="" id='filing_frequency_due_date1' name="filing_frequency_due_date1[]">
								</div>
								<div class="payment_add">
									<button type="button" id="add_row2" class="payment_addbtn btn-success">ADD</button>
								</div>
								<div class="payment_formula">
									<button type="button" id="" class="btn_formula btn-default">Formula</button>
								</div>
							</div>
						</div>
               <div class="card-footer">
                    <div class="col-md-2 col-md-offset-3">
									<input class="btn_new_save btn-primary1" style="margin-left:-5%" type="submit" name="submit" value="Save">
									</div>
									<div class="col-md-2 row">
									<a class="btn_new_cancel" href="{{url('fac-Bhavesh-0554/states')}}">Cancel</a> 
									</div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
    </section>
</div>

<script type="text/javascript">
$(document).ready(function(){

	var maxField = 10; //Input fields increment limitation
	var addButton = $('.add_button'); //Add button selector
	var wrapper = $('.filing_tabs_main'); //Input field wrapper
	var fieldHTML = '<div class="filing_tabs"><div class="filing_tab filing_form"><label>Form #</label><input type="hidden" class="form-control" placeholder="" value="" name="fillid[]" ><input type="text" class="form-control" placeholder="" name="filing_frequency_form[]"></div><div class="filing_tab filing_name"><label>Form Name</label><input type="text" name="filing_frequency_name[]" class="form-control" placeholder=""></div><div class="filing_tab filing_freq"><label>Filing Frquency</label><select type="text" class="form-control-insu" id="filing_frequency" name="filing_frequency[]"><option value="Annually">Yearly</option><option value="Monthly">Monthly</option><option value="Quaterly">Quaterly</option></select></div><div class="filing_tab filing_due_date"><label>Due Date</label><input type="text" class="form-control" placeholder="" name="filing_frequency_due_date[]"></div><div class="filing_formula"><button type="button" id="" class="btn_formula btn-default">Formula</button></div><div class="professional_btn"><a href="javascript:void(0);" id="remove_button_pro" class="btn_professional btn_professional_remove">Remove</a></div></div>';
	var x = 1; //Initial field counter is 1
	$(addButton).click(function(){ //Once add button is clicked
		if(x < maxField){ //Check maximum number of input fields
		x++; //Increment field counter
		$(wrapper).append(fieldHTML); // Add field html
		}
	});
	$(wrapper).on('click', '#add_row1', function(e){ //Once remove button is clicked
		e.preventDefault();
		$(wrapper).append(fieldHTML); //Remove field html
		x++; 
	});  
	$(wrapper).on('click', '#remove_button_pro', function(e){ //Once remove button is clicked
		e.preventDefault();
		$(this).parent().parent('.filing_tabs').remove(); //Remove field html
		x--; //Decrement field counter
	});
});
</script>


<script type="text/javascript">
$(document).ready(function(){

	var maxField = 10; //Input fields increment limitation
	var addButton = $('.add_button'); //Add button selector
	var wrapper = $('.payment_tabs_main'); //Input field wrapper
	var fieldHTML = '<div class="payment_tabs"><div class="payment_tab payment_form"><label>Form #</label><input type="hidden" class="form-control" placeholder="" value="" name="pay_id[]" ><input type="text" class="form-control" placeholder="" name="filing_frequency_form1[]"></div><div class="payment_tab payment_name"><label>Form Name</label><input type="text" class="form-control" placeholder="" name="filing_frequency_name1[]"></div><div class="payment_tab payment_freq"><label>Filing Frquency</label><select type="text" class="form-control-insu" id="filing_frequency1" name="filing_frequency1[]"><option value="Annually">Yearly</option><option value="Monthly">Monthly</option><option value="Quaterly">Quaterly</option></select></div><div class="payment_tab payment_due_date"><label>Due Date</label><input type="text" class="form-control" placeholder="" name="filing_frequency_due_date1[]"></div><div class="payment_formula"><button type="button" id="" class="btn_formula btn-default">Formula</button></div><div class="payment_add"><a href="javascript:void(0);" id="remove_button_pro1" class="btn_professional btn_professional_remove">Remove</a></div></div>'; //New input field html 
	var x = 1; //Initial field counter is 1
	$(addButton).click(function(){ //Once add button is clicked
		if(x < maxField){ //Check maximum number of input fields
		x++; //Increment field counter
		$(wrapper).append(fieldHTML); // Add field 
		
		
		}
	});
	$(wrapper).on('click', '#add_row2', function(e){ //Once remove button is clicked
		e.preventDefault();
		$(wrapper).append(fieldHTML); //Remove field html
		x++; 
	});  
	$(wrapper).on('click', '#remove_button_pro1', function(e){ //Once remove button is clicked
		e.preventDefault();
		$(this).parent().parent('.payment_tabs').remove(); //Remove field html
		x--; //Decrement field counter
	});
});
</script>
<script>
   $(document).ready(function(){
        $('#typeofform').change(function (){
            var selectedText =  $('#typeofform').val();
            if(selectedText=='Payroll Tax')
            {
                $('#filing_tabs_main').show();
                $('#federal').show();
                $('#payroll').show();
                 $('#federal_tabs_main').show();
                 $('.payment_tabs_main').show();  
                  $('#hh').hide();
            }
            else 
            {
              
                $('#filing_tabs_main').hide();
                $('#federal').hide();
                $('#payroll').hide();
                 $('#federal_tabs_main').hide();
                 $('.payment_tabs_main').hide();  $('#hh').show();     
            }
        })
    })
    
    
    $(document).ready(function () {
       $('.payroll').click(function () {
     var va= $(this).val();
    $('#fed').html(va);
       });

   });
</script>
    <script type="text/javascript">
   $(function () {
       $("#filing_frequency1").change(function () {
           var selectedText =  $('#filing_frequency1').val();
           var selectedText1 =  $('#federal_payment_frequency_quaterly').val();
           var selectedValue = $(this).val(); 
   if(selectedValue =='Annually')
   {
   $("#filing_frequency_due_date1").val('<?php echo "Jan".'-'.date("t").'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
   }
   else if(selectedValue =='Monthly')
   {
       if(selectedText=='')
       {
          $("#filing_frequency_due_date1").val('<?php echo date("M",strtotime("0 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');  
       }
       else
       {
   if(selectedText > '<?php echo date('M-d-Y');?>') 
    {
       $("#filing_frequency_due_date1").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');
    }
    else
    {
    $("#filing_frequency_due_date1").val('<?php echo date("M",strtotime("0 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');
   }
       }
   }
   else if(selectedValue =='Quaterly')
   {
       if(selectedText1 > '<?php echo date('M-d-Y');?>') 
    {
    //  $("#filing_frequency_due_date").val('');  
    }
    else
    { 
      $("#filing_frequency_due_date1").val('<?php echo date("M",strtotime("3 month",strtotime(date("M")))).'-'.date("t").'-'.date("Y");?>'); 
   }
   }
   else
   {
  
   }   
       });
   });
   </script>
    <script type="text/javascript">
   $(function () {
       $("#filing_frequency").change(function () {
           var selectedText =  $('#filing_frequency').val();
           var selectedText1 =  $('#federal_payment_frequency_quaterly').val();
           var selectedValue = $(this).val(); 
   if(selectedValue =='Annually')
   {
   $("#filing_frequency_due_date").val('<?php echo "Jan".'-'.date("t").'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
   }
   else if(selectedValue =='Monthly')
   {
       if(selectedText=='')
       {
          $("#filing_frequency_due_date").val('<?php echo date("M",strtotime("0 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');  
       }
       else
       {
   if(selectedText > '<?php echo date('M-d-Y');?>') 
    {
       $("#filing_frequency_due_date").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');
    }
    else
    {
    $("#filing_frequency_due_date").val('<?php echo date("M",strtotime("0 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');
   }
       }
   }
   else if(selectedValue =='Quaterly')
   {
       if(selectedText1 > '<?php echo date('M-d-Y');?>') 
    {
    //  $("#filing_frequency_due_date").val('');  
    }
    else
    { 
      $("#filing_frequency_due_date").val('<?php echo date("M",strtotime("3 month",strtotime(date("M")))).'-'.date("t").'-'.date("Y");?>'); 
   }
   }
   else
   {
  
   }   
       });
   });
 

$("#duedate").datepicker({
		autoclose: true,
format: "M-dd",
});
$("#extdate").datepicker({
		autoclose: true,
format: "M-dd",
});
$("#payroll_telephone").mask("(999) 999-9999");
$("#telephone").mask("(999) 999-9999");
   </script>
  <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Type of Entity</h4>
      </div>
      <div class="modal-body" style="display: inline-table;">
           <form action="" method="post" id="ajax2">
                {{csrf_field()}}
        <div class="form-group">
                   <label class="control-label col-md-3">Type of Entity :</label>
                     <div class="col-md-6">
                        <div class="">
                            <input type="text" name="newopt" id="newopt" class="form-control" placeholder="Type of Entity">
                        </div>
                     </div>
                     <div class="col-md-2">
                        <div class="">
                            <input type="button" id="addopt" class="btn btn-primary" value="Add Type of Entity">
                        </div>
                     </div>
                  </div>
                  </form>
                  <div class="form-group">
                   <label class="control-label col-md-3"></label>
                     
                  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script>
          $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  }
              });
           
             $(function () {
                $('#addopt').click(function () { //alert();
                    var newopt = $('#newopt').val();
                    if (newopt == '') {
                        alert('Please enter something!');
                        return;
                    }

                   //check if the option value is already in the select box
                    $('#purpose1 option').each(function (index) {
                        if ($(this).val() == newopt) {
                            alert('Duplicate option, Please enter new!');
                        }
                    })
                    $.ajax({
        type: "post",
        url: "{!!route('typeof.typeofentity')!!}",
        dataType: "json",
        data: $('#ajax2').serialize(),
        success: function(data){
             alert('Successfully Add');
             $('#authority_name').append('<option value=' + newopt + '>' + newopt + '</option>');
             $("#div").load(" #div > *");
             $("#newopt").val('');
        },
        error: function(data){
             alert("Error")
        }
    });
                   
                     $('#myModal').modal('hide');
                });
            });
            
</script>
@endsection()

