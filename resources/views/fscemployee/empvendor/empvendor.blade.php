@extends('fscemployee.layouts.app')
@section('title', 'Vendor')
@section('main-content')
<style>
label{float:left} th{ text-align:center !important}
.input-sm{width: 110% !important;margin-left: 10px !important;}
.dataTables_filter{display:none;}
.dt-buttons{
    margin-bottom:10px;
    position: absolute;
    margin-top: -48px;
    margin-left:86.8%;
}
.page-title{
        padding: 8px 19px !important;
}
</style>
<style>
    .buttons-pdf {
    font-size: 0px !important;
    border-radius: 3px;
    padding: 8px 10px !important;
    background: #fff !important;
    border-color: #c6c6c6  !important;
    color:red !important;
}
.buttons-print {
    font-size: 0px !important;
    border-radius: 3px;
    padding: 8px 10px !important;
    background: #3c8dbc !important;
            border-color: #367fa9 !important;
}
.buttons-excel{
    font-size:0px !important;
    border-radius: 3px;
        padding: 8px 10px !important;
         background: #00a65a !important;
    border-color: #008d4c !important;
}
.buttons-excel:hover{
     background: #008d4c !important;
}
.buttons-pdf:hover{
     background: #f6f6f6  !important;
}
.buttons-print:hover{
     background: #367fa9 !important;
}
.fa{
    font-size: 16px !important;
}
.imgicon{background: #fff; display: block; margin-top:-6px; width: 35px; float: left; height:35px;  margin-right: 10px; float:left; margin-right:10px; border-radius:2px; padding:3px; border:1px solid #12186b;}
.imgicon img{max-width:100%;}
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header page-title" style="">
     		<div class="">
     		    <div class="" style="text-align:center;">
     		        <span><span class="imgicon"> <img src="https://www.financialservicecenter.net/public/images/Vendor-02.png" alt="img"></span></span>
     		        <h1>List of Vendor <span style="padding-right:10px;float:right;">Add / View / Edit</span></h1>
     		    </div>
     		    
     		</div>
    </section>
    <section class="content">		
	<div class="row">
		<div class="col-md-12">
			<div class="box box-success">
			      <div class="box-header">
                   <div class="col-md-5" style="margin-left:-1.6%;height:40px !important;">
              <div class="row">
    <div class="col-md-2"><label style="margin-left:23%;margin-top: 11px;">Search: </label></div>
    <div class="col-md-4" style="padding:0px;">
    <select name="types" style="width: 92%;margin-left: 4px;" id="types" class="form-control">
        <option value="All">All</option>
        <option value="Type Of Service">Type Of Business</option>
        <option value="Vendor Name">Vendor Name</option>
        <option value="Tele">Tele #</option>
        <option value="Email">Email</option>
    </select>
    </div>
     <div class="col-md-5">
     <table style="width: 100%; margin: 0 auto 2em auto;" cellspacing="0" cellpadding="3" border="0">
        <tbody>
               <tr id="filter_global">
                <td align="center"><input type="text" class="global_filter form-control" id="global_filter" placeholder="All Search"></td>
            </tr>
            <tr id="filter_col2" data-column="1" style="display:none">
                <td align="center"><input type="text" class="column_filter form-control" id="col1_filter" placeholder="Type Of Service"></td>
            </tr>
            <tr id="filter_col3" data-column="2" style="display:none">
                <td align="center"><input type="text" class="column_filter form-control" id="col2_filter" placeholder="Vendor Name"></td>
            </tr>
            <tr id="filter_col4" data-column="3" style="display:none">
                <td align="center"><input type="text" class="column_filter form-control" id="col3_filter" placeholder="Tele"></td>
            </tr>
            <tr id="filter_col5" data-column="4" style="display:none">
                <td align="center"><input type="text" class="column_filter form-control" id="col4_filter" placeholder="Email"></td>
            </tr>
        </tbody>
    </table>
    </div>
    </div>
              </div>
             	<div class="col-md-3" style="width:18%;margin-left: 30%;">
					<div class="table-title">
						<a href="{{route('empvendor.create')}}" style="margin-top:5px;"> Add New Vendor</a>
					</div><br>
					<br>
					</div>
            </div>
				<div class="col-md-12">
					<div class="table-title">
</div>@if ( session()->has('success') )
    <div class="alert alert-success alert-dismissable">{{session()->get('success') }}</div>
                               @endif
					<div class="table-responsive">
						<table class="table table-hover table-bordered" id="example">
							<thead>
								<tr>
								   <th width="5%">No</th>
								   <th width="30%" >Type Of Business / Category</th>
								   <th width="25%">Vendor Name</th>
							       <th width="10%">Telephone</th>
							        <th width="8%">Status</th>
								   <th width="7%">Action</th>
								</tr>
							</thead>							
							<tbody>
                                @foreach($employee1 as $employ)
                                    <?php $str1=$employ->business_catagory_name; $splittedstring1=explode(",",$str1);?>
                                    <tr>
                                        <td style="text-transform: capitalize;text-align:center;">{{$loop->index+1}}</td>
                                        <td style="text-transform:capitalize;">
                                            @if($employ->categorys !='') @if($employ->utilitiesname !='') Utility - {{$employ->uname}}  @elseif($employ->insurance_company !='') Insurance - {{$employ->iname}}  @endif  @endif
                                            @foreach($category as $cate) @foreach($splittedstring1 as $key => $value) @if($value ==$cate->id) {{$cate->business_cat_name}}<br> @endif @endforeach @endforeach 
                                        </td>
                                        <td>{{$employ->business_name}}</td>
                                        <td style="text-align:center">{{$employ->telephoneNo1}}</td>
                                        <td><center>@if($employ->check==1)<a class="btn-action btn-view-edit btn_edit" href="">Active</a> @else <a class="btn-action btn-delete" href="">Inactive</a> @endif</center></td>
                                        <td style="text-align:center;">
                                            @if($employ->newemp==1)<a class="" href="{{route('vendor.edit',$employ->id)}}"><img src="{{asset('public/dashboard/images/newimage.gif')}}" alt="" width="50px"></a>@endif
                                            <a class="btn-action btn-view-edit" href="{{route('empvendor.edit',$employ->id)}}"><i class="fa fa-edit"></i></a>
                                            <form action="{{route('empvendor.destroy',$employ->id)}}" method="post" style="display:none" id="delete-id-{{$employ->id}}">
                                                {{csrf_field()}} {{method_field('DELETE')}}
                                            </form>
                                            <a class="btn-action btn-delete" href="#" onclick="if(confirm('Are you sure, You want to delete this record ?'))
                                            {event.preventDefault();document.getElementById('delete-id-{{$employ->id}}').submit();} else{event.preventDefault();}"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	</section>
</div>
<script>
$(document).ready(function() {
    var table = $('#example').DataTable( {
        dom: 'Bfrtlip',
    "columnDefs": [ {
            "searchable": true,
            "orderable": true,
            "targets": 0
        } ],
        "order": [[ 0, 'asc' ]],
        buttons: [
                 {
                extend:    'copyHtml5',
                text:      '<i class="fa fa-files-o"></i> &nbsp; Copy',
                titleAttr: 'Copy',
                title: $('h3').text(),
                  exportOptions: {
        columns: [0,1, 2, 3,4,5], // Only name, email and role
    }
            },
            {
                extend:    'excelHtml5',
                text:      '<i class="fa fa-file-excel-o"></i>&nbsp; Excel',
               titleAttr: 'Excel',
                title: $('h3').text(),
                 customize: function( xlsx ) {
                var sheet = xlsx.xl.worksheets['sheet1.xml'];
 $('row c', sheet).attr('s', '51');
            },
                  exportOptions: {
        columns: [0,1, 2, 3,4,5], // Only name, email and role
    }
            },
            {
                extend:    'csvHtml5',
                text:      '<i class="fa fa-file-text-o"></i> &nbsp; CSV',
                titleAttr: 'CSV',
                title: $('h3').text(),
                  exportOptions: {
        columns: [0,1, 2, 3,4,5], // Only name, email and role
    }
            },
            {
                extend:    'pdfHtml5',
                text:      '<i class="fa fa-file-pdf-o"></i>&nbsp;  PDF',
                
              customize: function (doc) {
						//Remove the title created by datatTables
						doc.content.splice(0,1);
						//Create a date string that we use in the footer. Format is dd-mm-yyyy
						var now = new Date();
						var jsDate = now.getDate()+'-'+(now.getMonth()+1)+'-'+now.getFullYear();
						
						doc.pageMargins = [20,60,20,20];
						doc.defaultStyle.fontSize = 10;
						doc.styles.tableHeader.fontSize = 10;
						doc['header']=(function() {
							return {
								columns: [{
									    alignment: 'left',
										image: logo,
										width: 50,margin: [200,5]
									},{
										alignment: 'CENTER',
										text: 'List of Client',
										fontSize: 20,
										margin: [10,35],
									},],
								margin: [20, 0, 0,12],alignment: 'center',
							}
						});
						var objLayout = {};
						objLayout['hLineWidth'] = function(i) { return 2; };
						objLayout['vLineWidth'] = function(i) { return 2; };
						objLayout['hLineColor'] = function(i) { return '#ccc'; };
						objLayout['vLineColor'] = function(i) { return '#ccc'; };
						objLayout['paddingLeft'] = function(i) { return 14; };
						objLayout['paddingRight'] = function(i) { return 14; };
						doc.content[0].layout = objLayout;
				},
				titleAttr: 'PDF',
                  exportOptions: {
        columns: [0,1, 2, 3,4,5], // Only name, email and role
    }
            },
            {
           extend: 'print',
          text: '<i class="fa fa-print"></i>&nbsp; Print',
          titleAttr: 'Print',
        customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '10pt' )
                        .prepend(
                            '<center><img src=""/></center>'
                        );
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );
                },
         exportOptions: {
          columns: [0,1, 2, 3,4,5]
      },
      footer: true,
      autoPrint: true
    },],
    } );
$('input.global_filter').on( 'keyup click', function () {
        filterGlobal();
    } );
 
    $('input.column_filter').on( 'keyup click', function () {
        filterColumn( $(this).parents('tr').attr('data-column') );
    } );
  table.on( 'order.dt search.dt', function () {
       table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
          cell.innerHTML = i + 1;
          table.cell(cell).invalidate('dom'); 
       } );
    } ).draw();
       table.columns(6)
        .search('^(?:(?!Inactive|New|1).)*$\r?\n?', true, false)
        .draw();
    $("#choice").on("change",function(){
 var _val = $(this).val();//alert(_val);
 
 if(_val == 'Inactive'){   
        table.columns(6).search(_val).draw();
  }
 else if(_val == 'New'){   
        table.columns(6).search(_val).draw();
  }
  else if(_val == 'Active'){  //alert();
         table.columns(6).search(_val).draw();
          table.columns(6)
        .search('^(?:(?!Inactive|New|1).)*$\r?\n?', true, false)
        .draw();
  }
  else{
        table
        .columns()
        .search('')
        .draw(); 
  }
  })
} );


function filterGlobal () {
    $('#example').DataTable().search(
        $('#global_filter').val(),
        $('#global_regex').prop('checked'),
        $('#global_smart').prop('checked')
    ).draw();
}
 
function filterColumn ( i ) {
    $('#example').DataTable().column( i ).search(
        $('#col'+i+'_filter').val(),
        $('#col'+i+'_regex').prop('checked'),
        $('#col'+i+'_smart').prop('checked')
    ).draw();
}


$( "#types" ).on('change',function() {
  // For unique choice
  var selVal = $( "#Type Of Service option:selected" ).val(); 
   if(selVal=='Type Of Service')  
  {
      $('#filter_global').hide();
      $('#filter_col3').hide();
      $('#filter_col4').hide();
      $('#filter_col5').hide();
      $('#filter_col6').hide();
      $('#filter_col7').hide();
      $('#filter_col2').show();
  }
  else if(selVal=='EE / User ID')  
  {
        $('#filter_col7').hide();
      $('#filter_global').hide();
      $('#filter_col2').hide();
      $('#filter_col4').hide();
      $('#filter_col5').hide();
      $('#filter_col6').hide();
      $('#filter_col3').show();
  }
    else if(selVal=='Employee Name')  
  {
      $('#filter_global').hide();
      $('#filter_col2').hide();
      $('#filter_col3').hide();
      $('#filter_col5').hide();
      $('#filter_col6').hide();
      $('#filter_col4').show();  
      $('#filter_col7').hide();
  }
   else if(selVal=='Email ID')  
  {
      $('#filter_global').hide();
      $('#filter_col2').hide();  
      $('#filter_col7').hide();
      $('#filter_col3').hide();
      $('#filter_col4').hide();
      $('#filter_col6').hide();
      $('#filter_col5').show();
  }
  else if(selVal=='Tel. Number')  
  {
      $('#filter_global').hide();
      $('#filter_col2').hide();
      $('#filter_col3').hide();
      $('#filter_col4').hide();
      $('#filter_col5').hide();  
      $('#filter_col7').hide();
      $('#filter_col6').show();
  }
  else if(selVal=='Tel. Number')  
  {
      $('#filter_global').hide();
      $('#filter_col2').hide();
      $('#filter_col3').hide();
      $('#filter_col4').hide();
      $('#filter_col5').hide();  
      $('#filter_col6').hide();
      $('#filter_col7').show();
  }
  else{
      $('#filter_global').show();
       $('#filter_col3').hide();
      $('#filter_col4').hide();
      $('#filter_col5').hide();
      $('#filter_col6').hide();  
      $('#filter_col7').hide();
      $('#filter_col2').hide();
  }
});


</script>
@endsection()