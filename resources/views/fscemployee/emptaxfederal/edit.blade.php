@extends('fac-Bhavesh-0554.layouts.app')
@section('main-content')
<style>
    .professional_btn {width: 10%;}
</style>
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>Tax Authorities / Form</h1>
    </section>
    <!-- Main content -->
    <section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-success">
			      <div class="box-header">
              <h3 class="box-title"></h3>
              <div class="box-tools pull-right">
                
              </div>
            </div>
				<div class="col-md-12">
					<form method="post" action="{{route('taxfederal.update', $bussiness->id)}}" class="form-horizontal" id="businessname" name="businessname" enctype="multipart/form-data">
						{{csrf_field()}}{{method_field('PATCH')}}
						<div class="form-group {{ $errors->has('typeofform') ? ' has-error' : '' }}">
							<label class="control-label col-md-3">Type of Tax :</label>
							<div class="col-md-4">
								<select type="text" class="form-control fsc-input" name="typeofform" id="typeofform" placeholder="Enter Your Company Name" >
									<option value="">---Select---</option>
									<option value="Income Tax" @if($bussiness->typeofform=='Income Tax') selected @endif>Income Tax</option>
									<option value="Payroll Tax"  @if($bussiness->typeofform=='Payroll Tax') selected @endif>Payroll Tax</option>
													<option value="Tabbaco Tax"  @if($bussiness->typeofform=='Tabbaco Tax') selected @endif>Tabbaco Tax</option>
									<option value="Sales Tax"  @if($bussiness->typeofform=='Sales Tax') selected @endif>Sales Tax</option>
								</select>
								@if ($errors->has('typeofform'))
								<span class="help-block">
								<strong>{{ $errors->first('typeofform') }}</strong>
								</span>
								@endif
							</div>
						</div>
						<!--<div class="form-group">
							<label class="control-label col-md-3">Type :</label>
							<div class="col-md-4">
							    	<input type="hidden" name="type" id="rend" class="form-control" value="{{$bussiness->uniques}}"  placeholder="" />
								    <select type="text" class="form-control fsc-input" name="short_name" id="short_name" placeholder="Enter Your Company Name" >
									<option value="">--Select--</option>
									<option @if($bussiness->short_name=='Entity') selected @endif value="Entity">Entity</option>
									<option @if($bussiness->short_name=='Payroll') selected @endif value="Payroll">Payroll</option>
									<option @if($bussiness->short_name=='Sales') selected @endif value="Sales">Sales</option>
									<option @if($bussiness->short_name=='Tobacco') selected @endif value="Tobacco">Tobacco</option>
									<option @if($bussiness->short_name=='COAM') selected @endif value="COAM">COAM</option>
									<option @if($bussiness->short_name=='License') selected @endif value="License">License</option>
									<option @if($bussiness->short_name=='Income') selected @endif value="Income">Income</option>
									<option @if($bussiness->short_name=='Excise') selected @endif value="Excise">Excise</option>
									<option @if($bussiness->short_name=='GST') selected @endif value="GST">GST</option>
									<option @if($bussiness->short_name=='VAT') selected @endif value="VAT">VAT</option>
								</select>
							</div>
							 <div class="col-md-4">
                         	<input type="radio" name="yearlytype" id="yearlytype" value="15" @if($bussiness->yearlytype=='15') checked @endif placeholder="" /> 15 days
                         	<input type="radio" name="yearlytype" id="yearlytype" value="45" @if($bussiness->yearlytype=='45') checked @endif placeholder="" /> 45 days
                         	<input type="radio" name="yearlytype" id="yearlytype" value="60" @if($bussiness->yearlytype=='60') checked @endif placeholder="" /> 60 days
                         	<input type="radio" name="yearlytype" id="yearlytype" value="90" @if($bussiness->yearlytype=='90') checked @endif placeholder="" /> 90 days
                        
                     </div>
						</div>-->
						 <div id="hh" @if($bussiness->short_name=='Payroll') style="display:none" @endif value="Entity">
						<div class="form-group {{ $errors->has('authority_name') ? ' has-error' : '' }}">
							<label class="control-label col-md-3">Type of Entity :</label>
							<div class="col-md-4">
								<select type="text" class="form-control fsc-input" name="authority_name" id="authority_name" placeholder="Enter Your Company Name" >
									<option value="">---Select---</option>
								@foreach($entity as $entity1)
                                    <option value="{{$entity1->typeentity}}" @if($bussiness->authority_name==$entity1->typeentity) selected @endif>{{$entity1->typeentity}}</option>
                                    @endforeach
								
								
								</select>
								@if ($errors->has('authority_name'))
								<span class="help-block">
									<strong>{{ $errors->first('authority_name') }}</strong>
								</span>
								@endif
							</div>
							 <div class="col-md-4"><button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i></button></div>
						</div>
						
						
						  <div class="form-group">
							<label class="control-label col-md-3">Form Name :</label>
							<div class="col-md-4">
								<input type="text" name="formname" id="formname" class="form-control" value="{{$bussiness->formname}}"  placeholder="" />
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Due Date :</label>
							<div class="col-md-4">
								<input type="text" name="duedate" id="duedate" class="form-control" value="{{$bussiness->due_date}}"  placeholder="" />
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Extension Due Date  :</label>
							<div class="col-md-4">
								<input type="text" name="extdate" id="extdate" class="form-control" placeholder="Extension Due Date" value="{{$bussiness->extension_due_date}}" />
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Authority Short Name :</label>
							<div class="col-md-4">
								<input type="text" name="authority_level" id="authority_level" class="form-control" value="{{$bussiness->authority_level}}"  placeholder="" />
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3">Authority Full Name :</label>
							<div class="col-md-4">
								<input type="text" name="authority_name1" id="authority_name1" class="form-control" value="{{$bussiness->authority_level}}"  placeholder="" />
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3">Website :</label>
							<div class="col-md-4">
								<input type="text" name="website" id="website" class="form-control" value="{{$bussiness->website}}"  placeholder="" />
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3">Telephone # :</label>
							<div class="col-md-4">
								<input type="text" name="telephone" id="telephone" class="form-control" value="{{$bussiness->telephone}}"  placeholder="" />
							</div>
						</div>
						
						<!--<div class="form-group">
							<label class="control-label col-md-3">Renew Link :</label>
							<div class="col-md-4">
								<input type="text" name="renewlink" id="renewlink" class="form-control" value="{{$bussiness->renewlink}}"  placeholder="" />
							</div>
						</div>-->
						</div>
						<div class="sss" @if($bussiness->short_name=='Payroll') @else style="display:none" @endif>
						<div class="form-group"  id="payroll">
							<label class="control-label col-md-3">Payroll :</label>
							<div class="col-md-9">
								<div class="check_tab">
									<input type="radio" id="Federal" @if($bussiness->payroll=='Federal') checked @endif  name="payroll" class="payroll" value="Federal">
									<label for="Federal">Federal</label>
								</div>
								<div class="check_tab">
									<input type="radio" id="State" @if($bussiness->payroll=='State') checked @endif  name="payroll" class="payroll" value="State">
									<label for="State">State</label>
								</div>
								<div class="check_tab">
									<input type="radio" id="County" @if($bussiness->payroll=='County') checked @endif  name="payroll" class="payroll" value="County">
									<label for="County">County</label>
								</div>
								<div class="check_tab">
									<input type="radio" id="Local" @if($bussiness->payroll=='Local') checked @endif  name="payroll" class="payroll" value="Local">
									<label for="Local">Local</label>
								</div>
							</div>
						</div>
						
						<div class="Branch"  id="federal">
							<div class="col-md-3" style="text-align:left;">
								<h1 id="fed">Federal</h1>
							</div>
							<div class="col-md-6">
								<h1 style="font-size:20px;">Payroll Tax</h1>
							</div>
						</div>
						
						<div class="federal_tabs_main" id="federal_tabs_main">
							<div class="federal_tabs">
								<div class="federal_tab federal_name">
									<label>Short Name</label>
									<input type="text" class="form-control" value="{{$bussiness->payroll_name}}" name="payroll_name" placeholder="">
								</div>
								<div class="federal_tab federal_depart">
									<label>Department Name</label>
									<input type="text" class="form-control"  value="{{$bussiness->payroll_department_name}}"  name="payroll_department_name" placeholder="">
								</div>
								<div class="federal_tab federal_link">
									<label>Link</label>
									<input type="text" class="form-control"  value="{{$bussiness->renewlink}}"  name="payroll_link" placeholder="">
								</div>
								<div class="federal_tab federal_tele">
									<label>Telephone #</label>
									<input type="text" class="form-control" placeholder="" value="{{$bussiness->telephone}}"  name="payroll_telephone" >
								</div>
							</div>
						</div>
						
						<div class="filing_tabs_main" id="filing_tabs_main">
							<h3 class="comm_title_one">Filing Frequency</h3>
							@foreach($fill as $fillable)
							@if(!empty($fillable->filing_frequency_form))
							<div class="filing_tabs">
								<div class="filing_tab filing_form">
									<label>Form #</label>
									<input type="hidden" class="form-control" placeholder="" value="{{$fillable->id}}" name="fillid[]" >
									<input type="text" class="form-control" value="{{$fillable->filing_frequency_form}}" name="filing_frequency_form[]" placeholder="">
								</div>
								<div class="filing_tab filing_name">
									<label>Form Name</label>
									
									<input type="text" class="form-control" value="{{$fillable->filing_frequency_name}}" name="filing_frequency_name[]"  placeholder="">
								</div>
								<div class="filing_tab filing_freq">
									<label>Filing Frquency</label>
									<select type="text" class="form-control-insu" id="filing_frequency" name="filing_frequency[]">
					<option value="Annually" @if($fillable->filing_frequency=='Yearly') selected @endif>Yearly</option>
					<option value="Monthly" @if($fillable->filing_frequency=='Monthly') selected @endif>Monthly</option>
					<option value="Quaterly" @if($fillable->filing_frequency=='Quaterly') selected @endif>Quaterly</option>
					</select>
								</div>
								<div class="filing_tab filing_due_date">
									<label>Due Date</label>
									<input type="text" class="form-control" value="{{$fillable->filing_frequency_due_date}}" name="filing_frequency_due_date[]"  placeholder="">
								</div>
								<div class="filing_add">
									<button type="button" id="add_row3" class="filing_addbtn btn-danger">Delete</button>
								</div>
								<div class="filing_formula">
									<button type="button" id="" class="btn_formula btn-default">Formula</button>
								</div>
							</div>
							@endif
							@endforeach
							
							<div class="filing_tabs">
								<div class="filing_tab filing_form">
									<label>Form #</label>
									<input type="hidden" class="form-control" placeholder="" value="" name="fillid[]" >
									<input type="text" class="form-control" placeholder=""  name="filing_frequency_form[]" >
								</div>
								<div class="filing_tab filing_name">
									<label>Form Name</label>
									<input type="text" class="form-control" placeholder="" name="filing_frequency_name[]" >
								</div>
								<div class="filing_tab filing_freq">
									<label>Filing Frquency</label>
									<select type="text" class="form-control-insu" id="filing_frequency" name="filing_frequency[]">
				                    	<option value="Annually" >Yearly</option>
				                    	<option value="Monthly" >Monthly</option>
				                    	<option value="Quaterly" >Quaterly</option>
				                	</select>
								</div>
								<div class="filing_tab filing_due_date">
									<label>Due Date</label>
									<input type="text" class="form-control" placeholder="" name="filing_frequency_due_date[]" >
								</div>
								<div class="filing_add">
									<button type="button" id="add_row1" class="filing_addbtn btn-success">ADD</button>
								</div>
								<div class="filing_formula">
									<button type="button" id="" class="btn_formula btn-default">Formula</button>
								</div>
							</div>
						</div>
						
						<div class="payment_tabs_main">
							<h3 class="comm_title_two">Payment Frequency</h3>
								@foreach($pay as $fillable)
										@if(!empty($fillable->filing_frequency_form1))
							<div class="payment_tabs">
								<div class="payment_tab payment_form">
									<label>Form #</label>
									<input type="text" class="form-control" placeholder="" value="{{$fillable->filing_frequency_form1}}" name="filing_frequency_form1[]" >
									<input type="hidden" class="form-control" placeholder="" value="{{$fillable->id}}" name="pay_id[]" >
								</div>
								<div class="payment_tab payment_name">
									<label>Form Name</label>
									<input type="text" class="form-control" placeholder="" value="{{$fillable->filing_frequency_name1}}" name="filing_frequency_name1[]">
								</div>
								<div class="payment_tab payment_freq">
									<label>Payment Frquency</label>
										<select type="text" class="form-control-insu" id="filing_frequency1" name="filing_frequency1[]">
					<option value="Annually" @if($fillable->filing_frequency1=='Yearly') selected @endif>Yearly</option>
					<option value="Monthly" @if($fillable->filing_frequency1=='Monthly') selected @endif>Monthly</option>
					<option value="Quaterly" @if($fillable->filing_frequency1=='Quaterly') selected @endif>Quaterly</option>
					</select>
								
								</div>
								<div class="payment_tab payment_due_date">
									<label>Due Date</label>
									<input type="text" class="form-control" placeholder=""  value="{{$fillable->filing_frequency_due_date1}}" name="filing_frequency_due_date1[]">
								</div>
								<div class="payment_add">
									<button type="button" id="add_row4" class="payment_addbtn btn-danger">Delete</button>
								</div>
								<div class="payment_formula">
									<button type="button" id="" class="btn_formula btn-default">Formula</button>
								</div>
							</div>
							@endif
								@endforeach
							<div class="payment_tabs">
								<div class="payment_tab payment_form">
									<label>Form #</label>
									<input type="text" class="form-control" placeholder="" name="filing_frequency_form1[]">
									<input type="hidden" class="form-control" placeholder="" value="" name="pay_id[]" >
								</div>
								<div class="payment_tab payment_name">
									<label>Form Name</label>
									<input type="text" class="form-control" placeholder="" name="filing_frequency_name1[]">
								</div>
								<div class="payment_tab payment_freq">
									<label>Payment Frquency</label>
									<select type="text" class="form-control-insu" id="filing_frequency1" name="filing_frequency1[]">
					<option value="Annually">Yearly</option>
					<option value="Monthly">Monthly</option>
					<option value="Quaterly">Quaterly</option>
					</select>
								</div>
								<div class="payment_tab payment_due_date">
									<label>Due Date</label>
									<input type="text" class="form-control" placeholder="" name="filing_frequency_due_date1[]">
								</div>
								<div class="payment_add">
									<button type="button" id="add_row2" class="payment_addbtn btn-success">ADD</button>
								</div>
								<div class="payment_formula">
									<button type="button" id="" class="btn_formula btn-default">Formula</button>
								</div>
							</div>	</div>
						</div>
						
					
						<div class="card-footer">
						<div class="col-md-2 col-md-offset-3">
									<input class="btn_new_save btn-primary1" type="submit" name="submit" value="Save">
									</div>
									<div class="col-md-2 row">
									<a class="btn_new_cancel" href="{{url('fac-Bhavesh-0554/taxfederal')}}">Cancel</a> 
									</div>
						</div>
						
					</form>
				</div>
			</div>
		</div>
	</div>
	 </section>
</div>

<script type="text/javascript">
$(document).ready(function(){
    var x = 1;
$("#duedate").datepicker({
		autoclose: true,
format: "M-dd",
});
	var maxField = 10; //Input fields increment limitation
	var addButton = $('.add_button'); //Add button selector
	var wrapper = $('.filing_tabs_main'); //Input field wrapper
	var fieldHTML = '<div class="filing_tabs"><div class="filing_tab filing_form"><label>Form #</label><input type="hidden" class="form-control" placeholder="" value="" name="fillid[]" ><input type="text" class="form-control" placeholder="Form #" id="filing_frequency_form_'+x+'" name="filing_frequency_form[]"></div><div class="filing_tab filing_name"><label>Form Name</label><input type="text" name="filing_frequency_name[]" class="form-control" placeholder=""></div><div class="filing_tab filing_freq"><label>Filing Frquency</label><select type="text" class="form-control-insu" id="filing_frequency" name="filing_frequency[]"><option value="Annually">Yearly</option><option value="Monthly">Monthly</option><option value="Quaterly">Quaterly</option></select></div><div class="filing_tab filing_due_date"><label>Due Date</label><input type="text" class="form-control" placeholder="" name="filing_frequency_due_date[]"></div><div class="filing_formula"><button type="button" id="" class="btn_formula btn-default">Formula</button></div><div class="professional_btn"><a href="javascript:void(0);" id="remove_button_pro" class="btn_professional btn_professional_remove">Remove</a></div></div>'; //New input field html 
	 //Initial field counter is 1
	$(addButton).click(function(){ //Once add button is clicked
		if(x < maxField){ //Check maximum number of input fields
		x++; //Increment field counter
		$(wrapper).append(fieldHTML); // Add field html
		}
	});
	$(wrapper).on('click', '#add_row1', function(e){ //Once remove button is clicked
		e.preventDefault();
		$(wrapper).append(fieldHTML); //Remove field html
		x++; 
	});  
	$(wrapper).on('click', '#remove_button_pro', function(e){ //Once remove button is clicked
		e.preventDefault();
		$(this).parent().parent('.filing_tabs').remove(); //Remove field html
		x--; //Decrement field counter
	});
});
</script>


<script type="text/javascript">
$(document).ready(function(){

	var maxField = 10; //Input fields increment limitation
	var addButton = $('.add_button'); //Add button selector
	var wrapper = $('.payment_tabs_main'); //Input field wrapper
	var fieldHTML = '<div class="payment_tabs"><div class="payment_tab payment_form"><label>Form #</label><input type="hidden" class="form-control" placeholder="" value="" name="pay_id[]" ><input type="text" class="form-control" placeholder="" name="filing_frequency_form1[]"></div><div class="payment_tab payment_name"><label>Form Name</label><input type="text" class="form-control" placeholder="" name="filing_frequency_name1[]"></div><div class="payment_tab payment_freq"><label>Filing Frquency</label><select type="text" class="form-control-insu" id="filing_frequency1" name="filing_frequency1[]"><option value="Annually">Yearly</option><option value="Monthly">Monthly</option><option value="Quaterly">Quaterly</option></select></div><div class="payment_tab payment_due_date"><label>Due Date</label><input type="text" class="form-control" placeholder="" name="filing_frequency_due_date1[]"></div><div class="payment_formula"><button type="button" id="" class="btn_formula btn-default">Formula</button></div><div class="payment_add"><a href="javascript:void(0);" id="remove_button_pro1" class="btn_professional btn_professional_remove">Remove</a></div></div>'; //New input field html 
	var x = 1; //Initial field counter is 1
	$(addButton).click(function(){ //Once add button is clicked
		if(x < maxField){ //Check maximum number of input fields
		x++; //Increment field counter
		$(wrapper).append(fieldHTML); // Add field html
		}
	});
	$(wrapper).on('click', '#add_row2', function(e){ //Once remove button is clicked
		e.preventDefault();
		$(wrapper).append(fieldHTML); //Remove field html
		x++; 
	});  
	$(wrapper).on('click', '#remove_button_pro1', function(e){ //Once remove button is clicked
		e.preventDefault();
		$(this).parent().parent('.payment_tabs').remove(); //Remove field html
		x--; //Decrement field counter
	});
});
</script>
<script>
    $(document).ready(function(){
        $('#short_name').change(function (){
            var selectedText =  $('#short_name').val();
           
             if(selectedText=='Payroll Tax')
            {
                $('#filing_tabs_main').show();
                $('#federal').show();
                $('#payroll').show();
                 $('#federal_tabs_main').show();
                 $('.payment_tabs_main').show();  
                  $('#hh').hide(); 
                    $('.sss').show(); 
            }
           else
            {
                $('#filing_tabs_main').hide();
                $('#federal').hide();
                $('#payroll').hide();
                 $('#federal_tabs_main').hide();
                 $('.payment_tabs_main').hide();  $('#hh').show(); 
            }
        })
    })
    
    
    $(document).ready(function () {
       $('.payroll').click(function () {
     var va= $(this).val();
    $('#fed').html(va);
       });

   });
</script>
  <script type="text/javascript">
   $(function () {
       $("#filing_frequency1").change(function () {
           var selectedText =  $('#filing_frequency1').val();
           var selectedText1 =  $('#federal_payment_frequency_quaterly').val();
           var selectedValue = $(this).val(); 
   if(selectedValue =='Annually')
   {
   $("#filing_frequency_due_date1").val('<?php echo "Jan".'-'.date("t").'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
   }
   else if(selectedValue =='Monthly')
   {
       if(selectedText=='')
       {
          $("#filing_frequency_due_date1").val('<?php echo date("M",strtotime("0 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');  
       }
       else
       {
   if(selectedText > '<?php echo date('M-d-Y');?>') 
    {
       $("#filing_frequency_due_date1").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');
    }
    else
    {
    $("#filing_frequency_due_date1").val('<?php echo date("M",strtotime("0 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');
   }
       }
   }
   else if(selectedValue =='Quaterly')
   {
       if(selectedText1 > '<?php echo date('M-d-Y');?>') 
    {
    //  $("#filing_frequency_due_date").val('');  
    }
    else
    { 
      $("#filing_frequency_due_date1").val('<?php echo date("M",strtotime("3 month",strtotime(date("M")))).'-'.date("t").'-'.date("Y");?>'); 
   }
   }
   else
   {
  
   }   
       });
   });
   </script>
    <script type="text/javascript">
   $(function () {
       $("#filing_frequency").change(function () {
           var selectedText =  $('#filing_frequency').val();
           var selectedText1 =  $('#federal_payment_frequency_quaterly').val();
           var selectedValue = $(this).val(); 
   if(selectedValue =='Annually')
   {
   $("#filing_frequency_due_date").val('<?php echo "Jan".'-'.date("t").'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
   }
   else if(selectedValue =='Monthly')
   {
       if(selectedText=='')
       {
          $("#filing_frequency_due_date").val('<?php echo date("M",strtotime("0 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');  
       }
       else
       {
   if(selectedText > '<?php echo date('M-d-Y');?>') 
    {
       $("#filing_frequency_due_date").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');
    }
    else
    {
    $("#filing_frequency_due_date").val('<?php echo date("M",strtotime("0 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');
   }
       }
   }
   else if(selectedValue =='Quaterly')
   {
       if(selectedText1 > '<?php echo date('M-d-Y');?>') 
    {
    //  $("#filing_frequency_due_date").val('');  
    }
    else
    { 
      $("#filing_frequency_due_date").val('<?php echo date("M",strtotime("3 month",strtotime(date("M")))).'-'.date("t").'-'.date("Y");?>'); 
   }
   }
   else
   {
  
   }   
       });
   });
   
   $("#payroll_telephone").mask("(999) 999-9999");
$("#telephone").mask("(999) 999-9999");
   
$("#duedate").datepicker({
		autoclose: true,
format: "M-dd",
});
$("#extdate").datepicker({
		autoclose: true,
format: "M-dd",
});
   </script>
   <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Type of Entity</h4>
      </div>
      <div class="modal-body" style="display: inline-table;">
           <form action="" method="post" id="ajax2">
                {{csrf_field()}}
        <div class="form-group">
                   <label class="control-label col-md-3">Type of Entity :</label>
                     <div class="col-md-6">
                        <div class="">
                            <input type="text" name="newopt" id="newopt" class="form-control" placeholder="Type of Entity">
                        </div>
                     </div>
                     <div class="col-md-2">
                        <div class="">
                            <input type="button" id="addopt" class="btn btn-primary" value="Add Type of Entity">
                        </div>
                     </div>
                  </div>
                  </form>
                  <div class="form-group">
                   <label class="control-label col-md-3"></label>
                     
                  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script>
          $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  }
              });
           
             $(function () {
                $('#addopt').click(function () { //alert();
                    var newopt = $('#newopt').val();
                    if (newopt == '') {
                        alert('Please enter something!');
                        return;
                    }

                   //check if the option value is already in the select box
                    $('#purpose1 option').each(function (index) {
                        if ($(this).val() == newopt) {
                            alert('Duplicate option, Please enter new!');
                        }
                    })
                    $.ajax({
        type: "post",
        url: "{!!route('typeof.typeofentity')!!}",
        dataType: "json",
        data: $('#ajax2').serialize(),
        success: function(data){
             alert('Successfully Add');
             $('#authority_name').append('<option value=' + newopt + '>' + newopt + '</option>');
             $("#div").load(" #div > *");
             $("#newopt").val('');
        },
        error: function(data){
             alert("Error")
        }
    });
                   
                     $('#myModal').modal('hide');
                });
            });
            
</script>
@endsection()

