@extends('fscemployee.layouts.app')
@section('main-content')
<style>
    .page-title{
    padding:8px 0px !important;
}
    .howtodopopup .form-group{margin-bottom:10px!important; display:block!important;}
    .howtodopopup .form-group .form-control{width:100%;}
.buttons-pdf {
    font-size: 0px !important;
    border-radius: 3px;
    padding: 8px 10px !important;
    background: #fff !important;
    border-color: #c6c6c6  !important;
    color:red !important;
}
.buttons-print {
    font-size: 0px !important;
    border-radius: 3px;
    padding: 8px 10px !important;
    background: #3c8dbc !important;
    border-color: #367fa9 !important;
}
.buttons-excel{
    font-size:0px !important;
    border-radius: 3px;
    padding: 8px 10px !important;
    background: #00a65a !important;
    border-color: #008d4c !important;
}
.buttons-excel:hover{
    background: #008d4c !important;
}
.buttons-pdf:hover{
    background: #f6f6f6  !important;
}
.buttons-print:hover{
    background: #367fa9 !important;
}
.dt-buttons {
    margin-top: -41px;
    position: absolute;
    margin-bottom: 10px;
    right: 15px;
}
.fa{
    font-size: 16px !important;
}
.Urgent
{ text-align:center!important;
   background:#a4d6f9;  
}
.Immedialtely {  background:red;text-align:center!important; }
.Regular {background:#fff; text-align:center!important;}
.imgicon {
    background: #fff;
    display: block;
    width:35px;
    float: left;
    margin-right: 10px;
    float: left;
    margin-right: 10px;
    border-radius: 2px;
    padding: 3px;
    border: 1px solid #12186b;
    margin-top: -7px;
    height: 35px;
}
.imgicon img{max-width:100%;}
.search-btn {
    position: absolute;
    top: 9px;
    right: 16px;
    background: transparent;
    border: transparent;
}
@media only screen and (max-width: 991px){
section.content-header.page-title.center h1 {
    text-align: center !important;
    width: 100% !important;
}
}
</style>
<div class="content-wrapper">
    
    <section class="content-header page-title center" style="">
     		<div class="" style="padding-right:0px;">
     		    <div class="" style="text-align:center;">
     		        <!--<span><i class="fa fa-users" style="float:left;margin-top:5px;"></i></span>-->
     		        <span class="imgicon"> <img src="https://www.financialservicecenter.net/public/images/Work-to-Do-04.png" alt="img" ></span>
     		        <h1>List of Work To Do <span style="padding-right:10px;float:right;">Add / View / Edit</span></h>
     		    </div>
     		    
     		</div>
    </section>
      <section class="content">
   <div class="row">
      <div class="col-md-12">
         <div class="box box-success">
             <div class="box-header">
                 
                <div class="col-md-8" style="padding-left:0px; padding-right:0px;">
              <div class="row">
              <!--<div class="col-md-1" style="width: 7.333% !important;"><label style="margin-left:28%;margin-top: 11px;"> </label></div>-->
              <!--<div class="col-md-3" style="width: 110px; padding-right:0px;">-->
              <!--  <select name="choice" style="width: 92%;margin-left: 4px;" id="choice" class="form-control">-->
              <!--      <option value="1">All</option>-->
              <!--      <option value="Active" selected>Active</option>-->
              <!--      <option value="Inactive">Inactive</option>-->
              <!--      <option value="New">New</option>       -->
              <!--  </select>-->
              <!--  </div>-->
                 <div class="col-md-1 col-xs-1" style=" padding-left:0px; padding-right:0px;"><label style="margin-left:28%;margin-top: 11px;">Search: </label></div>
                  <div class="col-md-4 col-xs-4" style="width: 160px; padding-right:0px;">
                    <select name="types" style="width: 92%;margin-left: 4px;" id="types" class="form-control">
                        <option value="All" selected>All</option>
                       
                        <option value="Createddate">Created Date</option>
                        <option value="Priority">Priority</option>
                        <option value="Clientid">Client Id</option>
                        <option value="Clientname">Client Name</option>
                        <option value="Category">Category</option>
                    </select>
                    </div>
     <div class="col-md-3 col-xs-3" style="width: 163px;">
     <table style="width: 100%;" cellspacing="0" cellpadding="3" border="0">
        <tbody>
         <tr id="filter_global">
                <td align="center"><input type="text" class="global_filter form-control" id="global_filter" placeholder="All Search"><button class="search-btn"><i class="fa fa-search"></i></button></td>
            </tr>
         
            <tr id="filter_col2" data-column="1" style="display:none">
                <td align="center"><input type="text" class="column_filter form-control" id="col1_filter" placeholder="Created Date"><button class="search-btn"><i class="fa fa-search"></i></button></td>
            </tr>
            <tr id="filter_col3" data-column="2" style="display:none">
                <td align="center"><input type="text" class="column_filter form-control" id="col2_filter" placeholder="Priority"><button class="search-btn"><i class="fa fa-search"></i></button></td>
            </tr>
            <tr id="filter_col4" data-column="3" style="display:none">
                <td align="center"><input type="text" class="column_filter form-control" id="col3_filter" placeholder="Client Id"><button class="search-btn"><i class="fa fa-search"></i></button></td>
            </tr>
            <tr id="filter_col5" data-column="4" style="display:none">
                <td align="center"><input type="text" class="column_filter form-control" id="col4_filter" placeholder="Client Name"><button class="search-btn"><i class="fa fa-search"></i></button></td>
            </tr>
            <tr id="filter_col6" data-column="5" style="display:none">
                <td align="center"><input type="text" class="column_filter form-control" id="col5_filter" placeholder="Category"><button class="search-btn"><i class="fa fa-search"></i></button></td>
            </tr>
        </tbody>
    </table>
    </div>
    <div class="col-md-2" style="padding-left: 0px; padding-right: 0px; width: 95px; margin-top:4px;">
     <div class="box-tools pull-right" data-toggle="tooltip" title="Status">
               <!-- <div class="table-title">-->
               <!--   <a href="https://financialservicecenter.net/fac-Bhavesh-0554/worknew" style="position: absolute;width: 100px;margin-left:20px;">Add New</a>-->
               <!--</div>-->
              </div>
              </div>
    </div>
    </div>
        
            </div>
            <div class="col-md-12" style="margin-top:-7px !important;">
               @if ( session()->has('success') )
               <div class="alert alert-success alert-dismissable">{{session()->get('success') }}</div>
               @endif
               <div class="table-responsive">
                  <table class="table table-hover table-bordered" id="example">
                     <thead>
                        <tr>
                            <th style="text-align:center">No</th>
                           <th style="text-align:center">Create Date</th>
                           <th style="text-align:center">Priority</th>
                           <th style="text-align:center">Client Id </th>
                           <th style="text-align:center">Client Name</th>
                           <th style="text-align:center">Category</th>
                           <th>Due Date</th>
                           <th>Responsible Person</th>
                           <th style="text-align:center">Action</th>
                          
                        </tr>
                     </thead>
                     <tbody>
                        

                   @foreach($worktodo as $worktodo)
                        <tr>
                           <td style="text-transform: capitalize;text-align:center;">{{$loop->index+1}}</td>
                           <td style="text-align:center;"><?php echo date('M-d Y',strtotime($worktodo->created_at))?></td>
                           <td <?php if($worktodo->worknew_priority =='Urgent') { ?> class="Urgent"<?php } else if($worktodo->worknew_priority =='Immediately') { ?> class="Immedialtely" 
                        <?php } else if($worktodo->worknew_priority =='Regular') { ?> class="Regular" <?php } ?>>{{$worktodo->worknew_priority}}</td> 
                           <!--<td>{{$worktodo->filename}}</td>-->
                           <td style="text-align:center;">@if($worktodo->filename!='') {{$worktodo->filename}} @else   @endif</td>
                           
                           <td>{{$worktodo->company_name}}</td>
                           <td>{{$worktodo->category_name}}</td>
                           <td style="text-align:center;">{{date('M-d Y',strtotime($worktodo->worknew_duedate))}}</td>
                           
                           <td>{{$worktodo->firstName}} {{$worktodo->middleName}} {{$worktodo->lastName}}</td>
                           <td style="text-align:center;">
            
                                  <a class="btn-action btn-view-edit" href="{{route('fschowtodo.edit',$worktodo->id)}}" ><i class="fa fa-edit"></i></a>
                                    
                                    <form action="{{ route('fschowtodo.destroy',$worktodo->id) }}" method="post" style="display:none" id="delete-id-{{$worktodo->id}}">
                                        {{csrf_field()}} {{method_field('DELETE')}}
                                        <!--<input type="hidden" name="ids" value="{{$worktodo->id}}">-->
                                        </form>
                                        
										<a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record ?'))
                                                                          {event.preventDefault();document.getElementById('delete-id-{{$worktodo->id}}').submit();} else{event.preventDefault();}" href=""><i class="fa fa-trash"></i></a>

                                 
								</td>

                        </tr>
                @endforeach
                
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
    </section>
</div>

<script>
$(document).ready(function() {
    var table = $('#example').DataTable( {
        dom: 'Bfrtlip',
        "pagingType": "input",
    "columnDefs": [ {
            "searchable": true,
            "orderable": true,
            "targets": 0
        } ],
        "order": [[ 0, 'asc' ]],
        buttons: [
                 {
                extend:    'copyHtml5',
                text:      '<i class="fa fa-files-o"></i> &nbsp; Copy',
                titleAttr: 'Copy',
                title: $('h3').text(),
                  exportOptions: {
        columns: [0,1, 2, 3,4,5], // Only name, email and role
    }
            },
            {
                extend:    'excelHtml5',
                text:      '<i class="fa fa-file-excel-o"></i>&nbsp; Excel',
               titleAttr: 'Excel',
                title: $('h3').text(),
                 customize: function( xlsx ) {
                var sheet = xlsx.xl.worksheets['sheet1.xml'];
 $('row c', sheet).attr('s', '51');
            },
                  exportOptions: {
        columns: [0,1, 2, 3,4,5], // Only name, email and role
    }
            },
            {
                extend:    'csvHtml5',
                text:      '<i class="fa fa-file-text-o"></i> &nbsp; CSV',
                titleAttr: 'CSV',
                title: $('h3').text(),
                  exportOptions: {
        columns: [0,1, 2, 3,4,5], // Only name, email and role
    }
            },
            {
                extend:    'pdfHtml5',
                text:      '<i class="fa fa-file-pdf-o"></i>&nbsp;  PDF',
                
              customize: function (doc) {
						//Remove the title created by datatTables
						doc.content.splice(0,1);
						//Create a date string that we use in the footer. Format is dd-mm-yyyy
						var now = new Date();
						var jsDate = now.getDate()+'-'+(now.getMonth()+1)+'-'+now.getFullYear();
						
						doc.pageMargins = [20,60,20,20];
						doc.defaultStyle.fontSize = 10;
						doc.styles.tableHeader.fontSize = 10;
						doc['header']=(function() {
							return {
								columns: [{
									    alignment: 'left',
										image: logo,
										width: 50,margin: [200,5]
									},{
										alignment: 'CENTER',
										text: 'List of Client',
										fontSize: 20,
										margin: [10,35],
									},],
								margin: [20, 0, 0,12],alignment: 'center',
							}
						});
						var objLayout = {};
						objLayout['hLineWidth'] = function(i) { return 2; };
						objLayout['vLineWidth'] = function(i) { return 2; };
						objLayout['hLineColor'] = function(i) { return '#ccc'; };
						objLayout['vLineColor'] = function(i) { return '#ccc'; };
						objLayout['paddingLeft'] = function(i) { return 14; };
						objLayout['paddingRight'] = function(i) { return 14; };
						doc.content[0].layout = objLayout;
				},
				titleAttr: 'PDF',
                  exportOptions: {
        columns: [0,1, 2, 3,4,5], // Only name, email and role
    }
            },
            {
           extend: 'print',
          text: '<i class="fa fa-print"></i>&nbsp; Print',
          titleAttr: 'Print',
        customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '10pt' )
                        .prepend(
                            '<center><img src=""/></center>'
                        );
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );
                },
         exportOptions: {
          columns: [0,1, 2, 3,4,5]
      },
      footer: true,
      autoPrint: true
    },],
    } );
/*$('input.global_filter').on( 'keyup click', function () {
        filterGlobal();
    } );
 
    $('input.column_filter').on( 'keyup click', function () {
        filterColumn( $(this).parents('tr').attr('data-column') );
    } );
  table.on( 'order.dt search.dt', function () {
       table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
          cell.innerHTML = i + 1;
          table.cell(cell).invalidate('dom'); 
       } );
    } ).draw();
       table.columns(6)
        .search('^(?:(?!Inactive|New|1).)*$\r?\n?', true, false)
        .draw();
    $("#choice").on("change",function(){
 var _val = $(this).val();//alert(_val);
 
 if(_val == 'Inactive'){   
        table.columns(6).search(_val).draw();
  }
 else if(_val == 'New'){   
        table.columns(6).search(_val).draw();
  }
  else if(_val == 'Active'){  //alert();
         table.columns(6).search(_val).draw();
          table.columns(6)
        .search('^(?:(?!Inactive|New|1).)*$\r?\n?', true, false)
        .draw();
  }
  else{
        table
        .columns()
        .search('')
        .draw(); 
  }
  })
*/
    
} );
   	
function filterGlobal () {
    $('#example').DataTable().search(
        $('#global_filter').val(),
        $('#global_regex').prop('checked'),
        $('#global_smart').prop('checked')
    ).draw();
}
 
function filterColumn ( i ) {
    $('#example').DataTable().column( i ).search(
        $('#col'+i+'_filter').val(),
        $('#col'+i+'_regex').prop('checked'),
        $('#col'+i+'_smart').prop('checked')
    ).draw();
}
$( "#types" ).on('change',function() {
  // For unique choice
  var selVal = $( "#types option:selected" ).val(); 
 
  if(selVal=='Createddate')  
  {
      $('#filter_global').hide();
      $('#filter_col3').hide();
      $('#filter_col4').hide();
      $('#filter_col5').hide();
      $('#filter_col6').hide();
      $('#filter_col2').show();
  }
  
  else if(selVal=='Priority')  
  {
      $('#filter_global').hide();
      $('#filter_col2').hide();
      $('#filter_col4').hide();
      $('#filter_col5').hide();
      $('#filter_col6').hide();
      $('#filter_col3').show();
  }
  else if(selVal=='Clientid')  
  {
      $('#filter_global').hide();
      $('#filter_col2').hide();
      $('#filter_col4').show();
      $('#filter_col5').hide();
      $('#filter_col6').hide();
      $('#filter_col3').hide();
  }
    else if(selVal=='Clientname')  
  {
      $('#filter_global').hide();
      $('#filter_col2').hide();
      $('#filter_col3').hide();
      $('#filter_col5').show();
      $('#filter_col6').hide();
      $('#filter_col4').hide();
  }
   else if(selVal=='Category')  
  {
      $('#filter_global').hide();
      $('#filter_col2').hide();
      $('#filter_col3').hide();
      $('#filter_col4').hide();
      $('#filter_col6').show();
      $('#filter_col5').hide();
  }
 
  else{
      $('#filter_global').show();
       $('#filter_col3').hide();
      $('#filter_col4').hide();
      $('#filter_col5').hide();
      $('#filter_col6').hide();
      $('#filter_col2').hide();
  }
}); 
</script>

<style>
    .dataTables_filter{display:none;}
</style>
@endsection()