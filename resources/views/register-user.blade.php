@extends('front-section.app')
@section('main-content')
<div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 fsc-content-head">
			<h4>ARE YOU OWNER OF ?</h4>
		</div>
	</div>
	
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:4%;">
@if ( session()->has('success') )
    <div class="alert alert-success alert-dismissable">{{ session()->get('success') }}</div>
@endif
@if ( session()->has('error') )
    <div class="alert alert-danger alert-dismissable">{{ session()->get('error') }}</div>
@endif 
	@foreach($business as $busi)		
		<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-sub-imgs">
	      <a href="@if($busi->link == 'comman-registration/create') {{url($busi->link,[$busi->id,$busi->bussiness_name])}} @else {{url($busi->link,[$busi->id,$busi->bussiness_name])}} @endif"><img style="cursor:pointer;" class="img-responsive" src="{{url('public/business')}}/{{$busi->bussiness_image_name}}"/></a>
		<div class="services-tab"> <a href="@if($busi->link == 'comman-registration/create') {{url($busi->link,[$busi->id,$busi->bussiness_name])}} @else {{url($busi->link,[$busi->id,$busi->bussiness_name])}} @endif"><span><div class="st_title">{{$busi->bussiness_name}}</div></span></a></div>
		</div>
         @endforeach()		
		<!--@foreach($business as $busi)	
		<form id="logout-form1" action="@if($busi->link == 'comman-registration') {{route('comman-registration.create')}} @else {{url($busi->link,$busi->id)}} @endif" method="get">	
		<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-sub-imgs">
		<button type="submit" name="submit"><img style="cursor:pointer;" class="img-responsive" src="{{url('business')}}/{{$busi->bussiness_image_name}}"/></button>
		<div class="services-tab"> <button type="submit" name="submit" value="submit"><span><div class="st_title">{{$busi->bussiness_name}}</div></span></button></div>
		</div>
		<input type="hidden" value="{{$busi->bussiness_name}}" name="user_type">
	     <input type="hidden" value="{{$busi->id}}" name="business_id">
		 <input type="hidden" value="" name="business_cat_id">
		 <input type="hidden" id="business_brand_id" name="business_brand_id" value="" />
		 <input type="hidden" id="business_brand_category_id" name="business_brand_category_id" value="" />
		 </form>
         @endforeach()		
	<!--<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-sub-imgs">
			<a href="{{URL::to('non-profit-organization-registration')}}"><img style="cursor:pointer;" class="img-responsive" src="{{URL::asset('frontcss/images/registration/Non-Profits-Org.png')}}"/></a>
		</div>
		<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-sub-imgs">
			<a href="{{URL::to('service-industry-registration')}}"><img style="cursor:pointer;" class="img-responsive" src=" {{URL::asset('frontcss/images/registration/ServiceIndustry.png')}}"/></a>
		</div>
	</div>
	
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:4%;">
		<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-sub-imgs">
			<a href="{{URL::to('professions-registration')}}"><img style="cursor:pointer;" class="img-responsive" src="{{URL::asset('frontcss/images/registration/Professions.png')}}"/></a>
		</div>
		<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-sub-imgs">
			<a href="{{URL::to('investor-registration')}}"><img style="cursor:pointer;" class="img-responsive" src="{{URL::asset('frontcss/images/registration/Investor.png')}}"/></a>
		</div>
		<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-sub-imgs">
			<a href="{{URL::to('personal-registration')}}"><img style="cursor:pointer;" class="img-responsive" src="{{URL::asset('frontcss/images/registration/Personal.png')}}"/></a>
		</div>-->
	</div>
	
</div>
@endsection()