<?php

namespace App\Http\Controllers\Front;

use App\Front\ApplyEmployment;
use App\Model\Employment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Input;
class ApplyEmploymentContoller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {     

        $employe = Employment::All(); 
        return view('apply-employment',compact('employe'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
public function checkUsernameAvailability() {
        
            $user = DB::table('apply_employments')->where('email', Input::get('email'))->count();
        
            if($user > 0) {
                $isAvailable = FALSE;
            } else {
                $isAvailable = TRUE;
            }
        
            echo json_encode(
                    array(
                        'valid' => $isAvailable
                    ));
        
        }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {    
    if($request->hasFile('resume'))
        {
         $filname = $request->resume->getClientOriginalName();
         $request->resume->move('public/resumes', $filname);
        }
else
{
$filname = '';
}
        $applyemployment = new ApplyEmployment;        
        $applyemployment->firstName = $request->firstName;
        $applyemployment->middleName = $request->middleName;
        $applyemployment->lastName = $request->lastName;
        $applyemployment->address1 = $request->address1;
        $applyemployment->address2 = $request->address2;
        $applyemployment->city = $request->city;
        $applyemployment->stateId = $request->stateId;
        $applyemployment->zip = $request->zip;
        $applyemployment->countryId = $request->countryId;
        $applyemployment->telephoneNo1 = $request->telephoneNo1;
        $applyemployment->telephoneNo2 = $request->telephoneNo2;
        $applyemployment->telephoneNo1Type = $request->telephoneNo1Type;
        $applyemployment->telephoneNo2Type = $request->telephoneNo2Type;
        $applyemployment->email = $request->email;
        $applyemployment->requiremnetId = $request->requiremnetId;
        $applyemployment->resume = $filname;
        $applyemployment->employment_id = $request->employment_id;
        $applyemployment->nametype = $request->nametype;
        $applyemployment->employee_date = date('M-d-Y');
        $email =$request->email;
        $firstName = $request->firstName;
       $data = array('email' =>$email , 'firstname' =>$firstName, 'from' => $email);       
        \Mail::send( 'emp_activation', $data, function( $message ) use ($data)
        {
        $message->to('vijay@businesssolutionteam.com')->from($data['from'], $data['firstname'])->subject( 'Apply Employment' );
        }); 
$applyemployment->save();
        return redirect('employmentsuccess')->with('success','You Are SuccessFully Registered');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Front\ApplyEmployment  $applyEmployment
     * @return \Illuminate\Http\Response
     */
    public function show(ApplyEmployment $applyEmployment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Front\ApplyEmployment  $applyEmployment
     * @return \Illuminate\Http\Response
     */
    public function edit(ApplyEmployment $applyEmployment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Front\ApplyEmployment  $applyEmployment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ApplyEmployment $applyEmployment)
    {
        //
    }

public function getStateList(Request $request)
    {
        $data = DB::table('citystate')->select('state','id')->where('country',$request->id)->take(1000)->get();
        return response()->json($data);  
    }  


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Front\ApplyEmployment  $applyEmployment
     * @return \Illuminate\Http\Response
     */
    public function destroy(ApplyEmployment $applyEmployment)
    {
        //
    }
}
