<?php
namespace App\Http\Controllers\Front;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use App\User;
class UserForgotUsernameController extends Controller
{
    

/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
      return view('forgotuser/forgotuser');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {  
      $email = $request->email;
      $user = User::where('email', '=', $email)->first();
     if ($user === null) 
    {
   return redirect(route('forgotuser.index'))->with('error', 'This Email not found');
    }
   else
   {
    $activation_link = route('forgotuser.edit', ['id' => $user->id,'email' => $email, 'token' => urlencode($user->remember_token)]);
    $data = array('email' => $email,'token'=>$activation_link); 
  \Mail::send( 'resetusername', $data, function( $message ) use ($data)
       {
           $message->to($data['email'])->from($data['email'],$data['token'])->subject('Forgot Username');
       });      
       return redirect(route('forgotuser.index'))->with('success', 'Please Check Your Email');
   }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $business = User::where('id',$id)->first();    
        return View('forgotuser.index',compact('business')); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {  
        $business = User::where('id',$id)->first(); 
        return view('forgotuser/edit',compact('business'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'email'=>'required',            
                        
        ]);
       $business =User::find($id);
        $business->email = $request->email;
         $business->update();
        return redirect(route('forgotuser.index'))->with('success', 'Your Username Successfully update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
