<?php
namespace App\Http\Controllers\Front;

use App\Model\Employment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EmploymentContoller extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
       $employment = Employment::All(); 
       return view('employment',compact('employment'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/employment/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'position_name' =>'required',
            'country' => 'required',
            'state' => 'required',
            'city' => 'required',
            'description' => 'required',
            'date' => 'required',
            'type' => 'required',
            'link' => 'required',            
        ]);

        $commonregistery1 = Employment::orderBy('employ_id', 'desc')->first();
        if(!empty($commonregistery1)) {
         $commonid = $commonregistery1->employ_id;
         $exp11=explode('-',$commonid);        
          $exp12 = $exp11[1];
           $exp13=$exp11[0];
        $n=$exp12;
        $n2 = str_pad($n + 1, 3, 0, STR_PAD_LEFT);
        $cmppcode=$exp13.'-'.$n2;   
}
else
{
$cmppcode ="GA-001";
}

        $employment = new Employment;
        $employment ->employ_id= $cmppcode;
        $employment->position_name = $request->position_name;
        $employment->country = $request->country;
        $employment->state = $request->state;
        $employment->city = $request->city;
        $employment->description = $request->description;
        $employment->date = $request->date;
        $employment->type = $request->type;
        $employment->link = $request->link;
   
        $employment->save();
        return redirect('admin/employment/employment')->with('success','You Are Success Fully Registered');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Employment  $employment
     * @return \Illuminate\Http\Response
     */
    public function show(Employment $employment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Employment  $employment
     * @return \Illuminate\Http\Response
     */
    public function edit(Employment $employment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Employment  $employment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employment $employment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Employment  $employment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employment $employment)
    {
        //
    }
}
