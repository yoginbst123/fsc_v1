<?php
namespace App\Http\Controllers\Front;
use App\Model\Service;
use App\Front\applyservicess;
use App\Front\Scheduler_table;
use Illuminate\Http\Request;
use App\Model\Servicetype;
use App\Http\Controllers\Controller;
use DB;
use App\Model\Citystate;
use App\Model\Zipcode;
use Illuminate\Support\Facades\Input;
use App\Front\Commonregister;
use App\Model\taxstate;
use App\Model\client_shareholder;
class ApplyservicessController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    public function applycounty1(Request $request)
    {
        $data = taxstate::select('countycode','id')->where('county',$request->id)->take(1000)->get();
        return response()->json($data);  
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getthecode(Request $request)
    {  
        $seed = str_split('abcdefghijklmnopqrstuvwxyz'
                     .'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
                     .'0123456789!@#$%^&*()'); // and any other characters
    shuffle($seed); // probably optional since array_is randomized; this may be redundant
    $rand = '';
    foreach (array_rand($seed, 8) as $k) $rand .= $seed[$k];
 
        $email=Input::get('email');
        
         $user = DB::table('applyservicesses')->where('email', Input::get('email'))->count();
       
       
        if($user > 0) {
                echo 'false';
            } 
            else {
               // echo 'true';
                 echo $rand;
                 
        $data = array('email' => $email, 'code' => $rand,'from' => $email);       
        \Mail::send('/codeemail', $data, function( $message ) use ($data)
        {
             $message->to($data['email'])->from( $data['from'],$data['email'],$data['code'])->subject('Welcome!');
        });
        
   
            }

       
        
        
    } 
    public function create(Request $request,$id)
    {   
        //die;
        $service = Servicetype::where('servicetype','=',$id)->get();
        $ser =Service::All();
        return view('applyservices/create',compact(['service','ser']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {       
        // $this->validate($request,[
        //     'password'=>'required|min:8', 
        //     'cpassword'=>'required|same:password',
        // ]);
        
     if($request->get('linense_type'))
       {
        $bulletin = implode(",", $request->get('linense_type')); 
       }
     else
       {
       $bulletin='';
       }
        $applyservice = new applyservicess;
        $applyservice->serviceid= $request->serviceid;
        $applyservice->uniques= $request->unique; 
        $applyservice->typeofcorp = $request->typeofcorp; 
        $applyservice->typeofservice = $request->typeofservice;
        $applyservice->fname = $request->fname;
        $applyservice->mname = $request->mname;              
        $applyservice->lname = $request->lname;
        $applyservice->address = $request->address;
        $applyservice->city = $request->city;
        $applyservice->state = $request->state;
        $applyservice->zip = $request->zip;
        $applyservice->phonenumber = $request->phonenumber;
        $applyservice->phonetype = $request->phonetype;
        $applyservice->ext = $request->ext;
        $applyservice->website = $request->website;
        $applyservice->nametype = $request->nametype;
        $applyservice->newbusiness = $request->newbusiness;
        $applyservice->linense_type = $request->$bulletin;
        $applyservice->typeofservice1 = $request->typeofservice1;
        $applyservice->newbusiness1 = $request->newbusiness1;
        $applyservice->domestic_business = $request->domestic_business;
        $applyservice->foreign_business = $request->foreign_business;
        $applyservice->company_name= $request->company_name;
        $applyservice->company_name_1= $request->company_name_1;
        $applyservice->company_name_2= $request->company_name_2;
        $applyservice->business_name= $request->business_name;
        $applyservice->type_of_business= $request->type_of_business;
        $applyservice->business_telephone= $request->business_telephone; 
        $applyservice->telephoneNo1Type= $request->telephoneNo1Type;
        $applyservice->ext1= $request->ext1;
        $applyservice->term= $request->terms; 
        $applyservice->email= $request->email;
        $applyservice->username= $request->email;
        $applyservice->physical_county= $request->physical_county;
        $applyservice->physical_county_no= $request->physical_county_no;
        $applyservice->password=bcrypt($request->password);
        $applyservice->newpassword= $request->password; 
        $applyservice->directpay=$request->directpay;
        $applyservice->availabilitynote=$request->availabilitynote;
        
        $applyservice->acno=isset($request->acno) ? $request->acno:""; 
        $applyservice->routing = $request->routing; 
        $applyservice->bankname= $request->bankname; 
        //$applyservice->newpassword= $request->password; 
        $email= $request->email;
        $password= $request->password;
        $name= $request->fname.' '.$request->mname.' '.$request->lname;
        $typeofservice = $request->typeofservice;
        $newbusiness1=$request->newbusiness1;
        $company_name=$request->company_name;
        $business_name=$request->business_name;
        $business_address=$request->business_address;
        $type_of_business=$request->type_of_business;
        
        
        $data = array('email' => $email, 'password' => $password, 'fullname' => $name,'typeofservice'=>$typeofservice,'newbusiness1'=>$newbusiness1,'company_name'=>$company_name,'type_of_business'=>$type_of_business,'business_name'=>$business_name,'business_address'=>$business_address,'from' => $email);       
        \Mail::send('/servicemail', $data, function( $message ) use ($data)
        {
             $message->to('vijay@businesssolutionteam.com')->from( $data['from'],$data['fullname'],$data['password'],$data['typeofservice'],$data['newbusiness1'],$data['type_of_business'],$data['company_name'],$data['business_name'],$data['business_address'])->subject('Welcome!');
        });
        
        $data1 = array('email' => $email, 'password' => $password, 'fullname' => $name,'typeofservice'=>$typeofservice,'newbusiness1'=>$newbusiness1,'company_name'=>$company_name,'type_of_business'=>$type_of_business,'business_name'=>$business_name,'business_address'=>$business_address,'from' => $email);       
        \Mail::send('/servicemail1', $data1, function( $message ) use ($data1)
        {
        $message->to($data1['email'])->from( $data1['from'],$data1['fullname'],$data1['password'],$data1['typeofservice'],$data1['newbusiness1'],$data1['type_of_business'],$data1['company_name'],$data1['business_name'],$data1['business_address'])->subject('Welcome!');
        });
        
        $i = 0;
        $agent_fname1 = $request->agent_fname1;
        $agent_mname1= $request->agent_mname1;
        $agent_lname1= $request->agent_lname1;
        $agent_position= $request->agent_position;
        $agent_position2=$request->agent_position1;
        $agent_per=$request->agent_per;
        $effective_date =$request->effective_date;
        $agentnametype =$request->agentnametype;
        $conid=$request->conid;
        $total = $request->total;
        $applyservice->save();
        $info = applyservicess::orderBy('id', 'desc')->first();

    if($request->agent_fname1 !='')
    {
        foreach($agent_fname1 as $post)
        { 
            $conid1 =$conid[$i];
            $agent_fname11 =$agent_fname1[$i];
            $agent_mname11 =$agent_mname1[$i];
            $agent_lname11 =$agent_lname1[$i];
            $agent_position1 =$agent_position[$i];
            $agent_position11 =$agent_position2;
             $agent_per1 =$agent_per[$i];
             $agentnametype1=$agentnametype[$i];
            $effective_date1 =$effective_date[$i];
            $total1 =$total;  
            $i++; 
            if(empty($conid1))
            {
                $insert2 = DB::insert("insert into client_shareholders(`nametype`,`agent_fname1`,`agent_mname1`,`agent_lname1`,`agent_position`,`total`,`client_id`,`agent_per`,`effective_date`,`usertype`) values('".$agentnametype1."','".$agent_fname11."','".$agent_mname11."','".$agent_lname11."','".$agent_position1."','".$total1."','".$info->id."','".$agent_per1."','".$effective_date1."','Service')");
            }
        }
    }
return redirect('thanks');
}

    /**
     * Display the specified resource.
     *
     * @param  \App\Front\Applyservice  $applyservice
     * @return \Illuminate\Http\Response
     */
    public function show(Applyservice $applyservice)
    {
        //
    }
    
    public function servAvailability() {
        
    //   $silver = DB::table("commonregisters")->select("commonregisters.email");
     //  $user = DB::table("applyservicesses")->select("applyservicesses.email")->where('email', Input::get('email'))->unionAll($silver)->count();  
       $user = DB::table('applyservicesses')->where('email', Input::get('email'))->count();
       
            if($user > 0) {
                echo 'false';
            } 
            else {
                echo 'true';
            }
        
        }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Front\Applyservice  $applyservice
     * @return \Illuminate\Http\Response
     */
    public function edit(Applyservice $applyservice)
    {
        //
    }







    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Front\Applyservice  $applyservice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Applyservice $applyservice)
    {
        //
    }
public function getzipcode(Request $request)
{

$data = Citystate::select('country','state','city','county')->where('zipcode',$request->zip)->take(100)->get();

return response()->json($data);  
}

public function getstate(Request $request)
{

$data = Citystate::select('country','zipcode','city','county','state')->where('state',$request->state)->take(100)->get();

return response()->json($data);  
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Front\Applyservice  $applyservice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Applyservice $applyservice)
    {
        //
    }
}
