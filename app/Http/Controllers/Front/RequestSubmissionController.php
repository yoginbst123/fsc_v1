<?php
namespace App\Http\Controllers\Front;
use App\Model\ServiceImages;
use App\Model\Service;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use App\Submissionlogin\Submissionlogin;
use App\Model\Submission;
use App\Front\Commonregister;
use App\Model\Employee;
class RequestSubmissionController extends Controller
{
    public function __construct()
{
$this->middleware('auth:submission');
}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
     $user_id  = Auth::user()->email;
     $subid = $request->subid;
     $subid1 = $request->radio;
     $submission= Submission::where('id','=',$subid)->first();
     $com = Commonregister::where('email','=',$user_id)->orWhere('id','=',$subid1)->get();
     $com2 = Commonregister::Where('id','=',$subid1)->first();
     $employee = Employee::where('type','=','Vendor')->orderBy('firstName', 'asc')->get(); 
     return view('submission/dashboard',compact(['submission','com','com2','employee']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}