<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\User;
use App\employees\Fscemployee;
use Auth;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'client/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
public function login(Request $request)
    {
      // Attempt to log the user in
      if(empty($request->employeeid)){
      
        $user = User::where('email', $request->email)->where('client_id', $request->clientid)->orderBy('id', 'desc')->first();

        if($user['email'] && $user['client_id'])
        {
            if($request->type1=='1')
            {
                if(Auth::guard()->attempt(['email'=>$request->email,'password'=>$request->password,'type' => 1], $request->remember)) 
                {
                    $request->session()->put('blog','1');
                    return redirect('client/home')->with('success','Welcome Our Dashboard!!!');
                } 
                else
                {
                    return redirect()->back()->withInput($request->only('email', 'remember'))->with('error','Your User Id and password wrong');
                }
            }
            else
            {
                echo '2';
            }
        }
    else
    {
        echo "0"; 
    }
}
else
{
 
 $user = Fscemployee::where('email', $request->emp_email)->where('employee_id', $request->employeeid)->first();

 if($user['email'] && $user['employee_id'])
    {
        if($request->type1=='1')
        {
          if (Auth::guard('employee')->attempt(['email' => $request->emp_email, 'password' => $request->password ,'type' => 1], $request->remember)) {
      //echo '3';
      return redirect()->intended('fscemployee')->with('success','Welcome Our Dashboard!!!');
      } else
      {
         // echo '5';
          return redirect()->back()->withInput($request->only('email', 'remember'))->with('error','Your User Id and password wrong');
      }
        }
        else
        {
       echo '2';
        }
    }
    else
    {

     echo "0"; 
}
}
      // if unsuccessful, then redirect back to the login with the form data
   //   return redirect()->back()->withInput($request->only('email', 'remember'))->with('error','Your Username Password Invalid!!!');

    }
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
	
}
