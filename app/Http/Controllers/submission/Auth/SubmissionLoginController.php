<?php
namespace App\Http\Controllers\submission\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Foundation\Auth\AuthenticateSubmision;
use Auth;
class SubmissionLoginController extends Controller
{	
  /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticateSubmision;
    
        /**
         * Where to redirect users after login.
         *
         * @var string
         */
       protected $redirectTo = 'submissions/';
    
        /**
         * Create a new controller instance.
         *
         * @return void
         */
        
        public function __construct()
        {
            $this->middleware('guest:submission')->except('logout');
        }

}