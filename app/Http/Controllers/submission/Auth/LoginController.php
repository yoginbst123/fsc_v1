<?php
namespace App\Http\Controllers\submission\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Model\Submission;
use App\Submissionlogin\Submissionlogin;
use Auth;
class LoginController extends Controller
{
    public function __construct()
    {
      $this->middleware('guest:submission');
    }
    
    public function showLoginForm(Request $request)
    {
     $subid = $request->subid;
     $submission= Submission::where('id','=',$subid)->first();
      return view('request-login',compact('submission'));
    }
    
    public function requestlogin(Request $request)
    {
      // Validate the form data
      $this->validate($request, [
        'username'   => 'required|email',
        'password' => 'required|min:6'
      ]);
      // Attempt to log the user in
      
     $user = Submissionlogin::where('email', $request->username)->where('customerid', $request->clientid)->where('type', $request->type)->first();
   if($user['email'] && $user['customerid'] && $user['type'])
   {
    
      if (Auth::guard('submission')->attempt(['email' => $request->username, 'password' => $request->password], $request->remember)) {
        // if successful, then redirect to their intended location
        return redirect()->intended(view('submission/dashboard'))->with('success','Welcome Our Dashboard!!!');
      }

}
      // if unsuccessful, then redirect back to the login with the form data
      return redirect()->back()->withInput($request->only('email', 'remember'))->with('error','Your Username Password Invalid!!!');

    }

    protected function credentials(Request $request)
    {
 //$input = $request->only($this->username(), 'password');
   // $input['type'] = 1; //Add extra key,value which need to be checked.
    return ['email'=>$request->username,'password'=>$request->password,'type'=>1];

    }
    public function logout(Request $request)
    {
        Auth::guard('submission')->logout();
        $request->session()->flush();
        $request->session()->regenerate();
        return redirect()->guest(route('submission.login.submit'));
    }
}