<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
use App\Addnavigation;
class AddNav extends Controller
{
/**

     * Show the application ajaxImageUpload.

     *

     * @return \Illuminate\Http\Response

     */
    public function navigation()
    {
        return view('navigation');
        
    }
        /**

     * Show the application ajaxImageUploadPost.

     *

     * @return \Illuminate\Http\Response

     */
    public function AddNavPost(Request $request)

    {

      $validator = Validator::make($request->all(), [

        'main_menu' => 'required',
        'slag' => 'required',
       

      ]);
           if ($validator->passes()) {


        $input = $request->all();

        AddNavigation::create($input);
        return response()->json(['success'=>'done']);

      }
      return response()->json(['error'=>$validator->errors()->all()]);

    }    
    
}
