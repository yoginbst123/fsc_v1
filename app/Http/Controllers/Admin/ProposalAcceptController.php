<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin;
use Auth;
use View;
use Session;
use Validator;
use Redirect;
use DB;

class ProposalAcceptController extends Controller
{
    
    public function index()
    {
        return view('ProposalAcceptation');
    }
    
    public function show($id)
    {
        // echo $id;
        // exit;
        $getProposal = DB::table("proposal_client_detail")->Where('ency_id',$id)->Where('status',0)->first();
        
        if(isset($getProposal)){
            
        
        
        //print_r($getProposal);
        //exit;
        $fsc_fee_v = $getProposal->fsc_fee;
        $discount_v = $getProposal->discount;
        $adv_paym_v = $getProposal->adv_payment;
        
        $fsc_fee_d = substr($fsc_fee_v,0,strlen($fsc_fee_v)-3);
        $discount_d = substr($discount_v,0,strlen($discount_v)-3);
        $adv_paym_d = substr($adv_paym_v,0,strlen($adv_paym_v)-3);
        
        $due_bal = 0.0; //$fsc_fee_d-$discount_d-$adv_paym_d;
        $due_bal = (int)$fsc_fee_d-(int)$discount_d-(int)$adv_paym_d;        
        $generate_url="https://financialservicecenter.net/propspectAccept/accept_proposal?id=".$getProposal->ency_id;
        
        $data = array(
                'HasData'=>1,
                'sales_rep_id' => $getProposal->sales_rep_id,
                'client_name'  => $getProposal->client_name,
                'telephone'  => $getProposal->telephone,
                'email'  => $getProposal->email,
                'client_address'  => $getProposal->client_address,
                'city'  => $getProposal->city,
                'state'  => $getProposal->state,
                'zip'  => $getProposal->zip,
                'type_of_service'  => $getProposal->type_of_service,
                'service_period'  => $getProposal->service_period,
                'priority'  => $getProposal->priority,
                'duedate'  => date('M/d/Y', strtotime($getProposal->duedate)),
                'scope_of_work'  => $getProposal->scope_of_work,
                'fsc_fee'  => $getProposal->fsc_fee,
                'discount'  => $getProposal->discount,
                'adv_payment'  => $getProposal->adv_payment,
                'due_bal'=>$due_bal,
                'description'=>$getProposal->descriptions,
                'link'=>$generate_url,
                'pid'=>$getProposal->id
            );
            //echo '<pre>';
        //print_r($getProposal->fsc_fee);
        //exit();
            return view('ProposalAcceptation',compact('data'));
        }else{
            $data = array('HasData'=>0);
            return view('ProposalAcceptation',compact('data'));
        }
        exit;
    }
    
    
    public function accept_proposal()
    {
        $id = $_GET['id'];
        $getProposal = DB::table("proposal_client_detail")->Where('ency_id',$id)->Where('status',0)->first();
        $counts = $getProposal->id;
        
        $query = DB::table('proposal_client_detail')
                ->where('id', $counts) 
                ->update(array(
                    'status' => 1,
                    'accepted_by'=>$getProposal->email,
                    'accepted_date'=>date('Y-m-d')
                    )); 
        
        echo $query;
        exit();
        
    }
    
    public function getproposalDetailsForAccept($id)
    {
        $getProposal = DB::table("proposal_client_detail")->Where('ency_id',$id)->Where('status',0)->first();
        return view('prospect/',compact(['id','json']));
    }
}