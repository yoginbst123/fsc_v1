<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Front\ApplyEmployment;
use App\Model\Employment;
use DB;
use App\Model\Rules;
use Illuminate\Support\Facades\Input;

class EmployeApplicationController extends Controller
{
    public function __construct()
    {
    $this->middleware('auth:admin');
    }  
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
    $application = DB::select("select apply_employments.id as cid,apply_employments.newapp as newapp,apply_employments.firstName as firstName,apply_employments.employee_date as employee_date,apply_employments.middleName as middleName,apply_employments.status as status,apply_employments.lastName as lastName,apply_employments.address1 as address1,apply_employments.address2 as address2,apply_employments.city as city,apply_employments.stateId as stateId,apply_employments.zip as zip,apply_employments.countryId as countryId,apply_employments.telephoneNo1 as telephoneNo1,apply_employments.telephoneNo2 as telephoneNo2,apply_employments.telephoneNo1Type as telephoneNo1Type,apply_employments.telephoneNo2Type as telephoneNo2Type,apply_employments.email as email,apply_employments.requiremnetId as requiremnetId,apply_employments.resume as resume,employments.position_name as position_name,employments.id as cd,employments.country as country,employments.state as state,employments.city as city1,employments.description as description,employments.date as date,employments.type as type from apply_employments left join employments on employments.id=apply_employments.employment_id");
        $employment = Employment::All();
        return view('fac-Bhavesh-0554/employeapplication/employeapplication', compact(['application','employment']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
//exit('111');
$newclient = ApplyEmployment::where('id',$id)->update(['newapp' => 2]);
$employment= DB::table('apply_employments')->select('apply_employments.id as cid','apply_employments.nametype as nametype','apply_employments.newapp as newapp','apply_employments.employee_date as employee_date','apply_employments.employment_id as employment_id','apply_employments.firstName as firstName','apply_employments.middleName as middleName','apply_employments.lastName as lastName','apply_employments.address1 as address1','apply_employments.address2 as address2','apply_employments.city as city','apply_employments.stateId as stateId','apply_employments.zip as zip','apply_employments.countryId as countryId','apply_employments.telephoneNo1 as telephoneNo1','apply_employments.telephoneNo2 as telephoneNo2','apply_employments.telephoneNo1Type as telephoneNo1Type','apply_employments.telephoneNo2Type as telephoneNo2Type','apply_employments.email as email','apply_employments.requiremnetId as requiremnetId','apply_employments.resume as resume','employments.position_name as position_name','employments.id as cd','employments.country as country','employments.state as state','employments.city as city1','employments.description as description','employments.date as date','employments.created_at','employments.type as type')
->leftJoin('employments', function($join){ $join->on('apply_employments.employment_id', '=', 'employments.id');})
->where('apply_employments.id', '=', $id)
->get()
->first();
$employment1 = Employment::All(); 
return view('fac-Bhavesh-0554.employeapplication.edit',compact(['employment','employment1']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->hasFile('resume'))
        {
         $filname = $request->resume->getClientOriginalName();
         $request->resume->move('public/resumes', $filname);
        }
        else
        {
        $filname = $request->resume1;
        }
        $applyemployment = ApplyEmployment::find($id);        
        $applyemployment->firstName = $request->firstName;
        $applyemployment->middleName = $request->middleName;
        $applyemployment->lastName = $request->lastName;
        $applyemployment->address1 = $request->address1;
        $applyemployment->address2 = $request->address2;
        $applyemployment->city = $request->city;
        $applyemployment->stateId = $request->stateId;
        $applyemployment->zip = $request->zip;
        $applyemployment->countryId = $request->countryId;
        $applyemployment->telephoneNo1 = $request->telephoneNo1;
        $applyemployment->telephoneNo2 = $request->telephoneNo2;
        $applyemployment->telephoneNo1Type = $request->telephoneNo1Type;
        $applyemployment->telephoneNo2Type = $request->telephoneNo2Type;
        $applyemployment->email = $request->email;
        $applyemployment->requiremnetId = $request->requiremnetId;
        $applyemployment->resume = $filname;
        $applyemployment->employment_id = $request->employment_id;
        $applyemployment->nametype = $request->nametype;
        $applyemployment->update();        
        return redirect('fac-Bhavesh-0554/employeapplication')->with('success','You Are Update Successfully');
    }

    public function update1($id)
    { 
       $logoStatus = ApplyEmployment::findOrFail($id);
       $logoStatus->update(['status'=>'1','candidate_date'=>date('M-d-Y')]);
       return redirect('fac-Bhavesh-0554/employeapplication')->with('success','Application Successfully Move to Candidate Data');
    }

public function sendmail($name,$email)
    { 
   
    $fname=$name;
    $emails=$email;


$file = 'public/images/Job-Q.xls';
//$file1->attach(Swift_Attachment::fromPath($file));
$data = array('email' => $emails, 'firstName' => $fname,'excel'=>$file, 'from' => 'vijay@businesssolutionteam.com');       
\Mail::send( 'fac-Bhavesh-0554/excelmail', $data, function( $message ) use ($data)
{
$message->to($data['email'])->from( $data['from'], $data['firstName'], $data['excel'])->subject( 'Welcome!' );
});


       return redirect('fac-Bhavesh-0554/employeapplication')->with('success','Please Check Your Mail');
    }


    public function deleteto($id)
    { 
       $logoStatus = ApplyEmployment::findOrFail($id);
       ApplyEmployment::where('id',$id)->delete();
       return redirect('fac-Bhavesh-0554/employeapplication')->with('error','You Are Reject The Application');
    }
  

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ApplyEmployment::where('id',$id)->delete();
        return redirect(route('employeapplication.index'));
    }
}
