<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin;
use App\Model\taxstate;
use App\Model\City;
use App\Model\admin_shareholder;
use App\Model\Control;
use App\Model\taxfederal;
use App\Model\admin_professional;
use App\Model\Adminupload;
use App\Model\FilingFrequency;
use App\Model\PaymentFrequency;
use App\Model\taxstatesses;
use DB;
use Auth;
class InvoiceController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('fac-Bhavesh-0554/invoice/invoice');
     }
}