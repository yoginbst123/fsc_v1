<?php
namespace App\Http\Controllers\Admin;
use App\Model\Appointment;
use App\employees\Fscemployee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Front\Commonregister;
use App\Model\Schedule;
use App\Model\Branch;

use App\Model\Employee;
use App\Model\Appointment_worktype;

use App\employee\Empschedule;
use App\Model\Position;
use App\Model\Regards;
use DB;
class AppointmentController extends Controller
{
public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employee = Fscemployee::All(); 
        $appoinment = Appointment::All();
        $branch = Branch::All(); 
        $schedule = Schedule::All();
        $emp = Employee::All();  
        return view('fac-Bhavesh-0554/appointment/appointment', compact(['employee','appoinment','branch','schedule','emp']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $branch = Branch::All(); 
        $schedule = Schedule::All();
        //$employee = Employee::where('type','!=','clientemployee')->where('check','=',1)->where('type','!=','Vendor')->orderBy('firstName', 'asc')->get();
        
        $employee = DB::table('employees')
            ->select('employees.*', 'teams.team as teamname')
            ->leftjoin('teams', 'teams.id', '=', 'employees.team')
            ->where('employees.type','!=','clientemployee')
            ->where('employees.check','=',1)
            ->where('employees.type','!=','Vendor')
            ->orderBy('employees.firstName', 'asc')
            ->get();
        // echo '<pre>';
        // print_r($employee);
        // exit();
        //Fscemployee::where('active','1')->orderBy('name', 'asc')->get();
        
        $emp1 = Employee::where('check','=',1)->where('type','=','employee')->orderBy('firstName', 'asc')->get();
        $start =date('Y-m-d',strtotime($request->startdate)); 
        $end = date('Y-m-d',strtotime($request->enddate));
        $employee1 = DB::Table('empschedules')->selectRaw('TIMEDIFF(emp_out,emp_in) as total,TIMEDIFF(launch_out,launch_in) as breaktime,TIMEDIFF(launch_out_second,launch_in_second) as breaktime1,emp_out,launch_in,launch_out,emp_in,ip_address,note,emp_in_date,id,employee_id,launch_out_second,launch_in_second')->whereBetween('emp_in_date',array($start,$end))->get();
        $sum = 0; 
        foreach($employee1 as $emp)
        {
            $in = date('H:i',strtotime($emp->emp_in));
            $out = date('H:i',strtotime($emp->emp_out));
            $lunch_in = date('H:i',strtotime($emp->launch_in));
            $lunch_out = date('H:i',strtotime($emp->launch_out));
            $total = date("H:i:s",strtotime($emp->total));
            
            if($emp->breaktime==NULL)
            {
                $breaktime ="00:00:00";
            }
            else
            {
                $breaktime = date("H:i:s",strtotime($emp->breaktime));
            }
            if($emp->breaktime==NULL)
            {
                $breaktime ="00:00:00";
            }
            else
            {
                $breaktime = date("H:i:s",strtotime($emp->breaktime));
            }
            if($emp->breaktime1==NULL)
            {
                $breaktime1 ="00:00:00";
            }
            else
            {
                $breaktime1 = date("H:i:s",strtotime($emp->breaktime1));
            }
            $total1 = floor($total)-(floor($breaktime) + floor ($breaktime1));
            $sum +=$total1;
        }
        
        $appoinment = Appointment::All();
        
        $appodata = array();
        
        foreach($appoinment as $appo)
        {
            
            //echo $this->getsingleTeamName($appo->appo_with) .'-'. $appo->appo_with."<br/>";
            
            $enddates='';
            if($appo->enddate == '0000-00-00'){
                $enddates = date('m/d/Y',strtotime($appo->startdate));
            }else{
                $enddates = date('m/d/Y',strtotime($appo->enddate));
            }
            if(!empty($appo->participant)){
                $parti = $this->getTeamName($appo->participant);
            }else{
                $parti='';
            }
            $appo_with = $this->getsingleTeamName($appo->appo_with);
            
            if(empty($appo->client_id)){
                $tempRep = "";
            }else{
                $tempRep = $this->getTeamRep2($appo->client_id);//$appo->client_id;
            }
            $appodata[] = array(
                'id'=>$appo->id,
                'regarding'=>$appo->regard_short,
                'startdate'=>date('m/d/Y',strtotime($appo->startdate)),
                'enddate'=>$enddates,
                'starttime'=>$appo->starttime,
                'endtime'=>$appo->endtime,
                'clientId'=>$appo->client_id,
                'priority'=>$appo->priority,
                'teamRep'=>$tempRep,
                'type'=>$appo->work_type_id,
                'work_type'=>$appo->work_type,
                'appo_with'=>$appo_with
            );
        }
        //  echo '<pre>';
        //  print_r($appodata);
        // exit();
        $regards = Regards::All();
        $work_type = Appointment_worktype::All();
        return view('fac-Bhavesh-0554/appointment/create', compact(['employee','appoinment','appodata','branch','employee1','emp1','sum','regards','work_type']));
    }
    
    function getsingleTeamName($empid)
    {
        $employee = DB::table('employees')
            ->select('teams.team as teamname')
            ->leftjoin('teams', 'teams.id', '=', 'employees.team')
            ->where('employees.employee_id','=',$empid)->first();
        return  $employee->teamname;
    }
    
    public function getTeamRep($id)
    {
        $returnData = '';
        $commonRe = DB::table('commonregisters')->select('id')->where('filename','=',$id)->first();
        $client_id = $commonRe->id;
        $clientser = DB::table('clientservices')->select('clientservices.employee_id')->where('clientid','=',$client_id)->orderBy('id', 'asc')->first();
        if(!empty($clientser)){
            $empid = $clientser->employee_id;
            $employee = DB::table('employees')
                ->select('teams.team as teamname')
                ->leftjoin('teams', 'teams.id', '=', 'employees.team')
                ->where('employees.id','=',$empid)->first();
            if(!empty($employee)){
                $returnData = $employee->teamname;    
            }else{
                $returnData = '' ;
            }
        }else{
            $returnData = '' ;
        }
        
        return $returnData;
    }
    public function getTeamRep2($id)
    {
        $returnData = '';
        $commonRe = DB::table('commonregisters')->select('id')->where('filename','=',$id)->first();
        $client_id = $commonRe->id;
        $clientser = DB::table('clientservices')->select('clientservices.employee_id')->where('clientid','=',$client_id)->orderBy('id', 'asc')->first();
        if(!empty($clientser)){
            $empid = $clientser->employee_id;
            $employee = DB::table('employees')
                ->select('teams.team as teamname')
                ->leftjoin('teams', 'teams.id', '=', 'employees.team')
                ->where('employees.id','=',$empid)->first();
            if(!empty($employee)){
                $returnData = $employee->teamname;    
            }else{
                $returnData = '' ;
            }
        }else{
            $returnData = '' ;
        }
        
        return $returnData;
    }
    
    function getTeamName($data)
    {
        $datas = explode(',',$data);
        $participant = "";
        for($i=0;$i<count($datas);$i++)
        {
            $employee = DB::table('employees')
            ->select('employees.*', 'teams.team as teamname')
            ->leftjoin('teams', 'teams.id', '=', 'employees.team')
            ->where('employees.employee_id','=',$datas[$i])->first();
            
            if($i == count($datas)-1){
                $participant .=  $employee->teamname;    
            }else{
                $participant .=  $employee->teamname .',';
            }
            
        }
        return $participant;
    }
    
    function getAppTypeColor($shortname)
    {
        $colors = DB::table('appoinment_wotktype')->where('short_name',$shortname)->first();
        return $colors->color;
    }
    
    public function getAppointment()
    {
        $appoinment = Appointment::All();
        $data = array();
        foreach($appoinment as $appo)
        {
            $enddates='';
            if($appo->enddate == null){
                $enddates = $appo->startdate;
            }else{
                $enddates = $appo->enddate;
            }
            if(!empty($appo->participant)){
                $parti = $this->getTeamName($appo->participant);
            }else{
                $parti='';
            }
            
            $rsttime = substr($appo->starttime,0,5);
            $rendtime = substr($appo->endtime,0,5);
            
            $color = $this->getAppTypeColor($appo->work_type_id);
            $data[] = array(
                'id'=>$appo->id,
                'title'=>$appo->starttime.'  '.$appo->endtime.'  |  '.$appo->work_type_id.' | '.$appo->regard_short.'  '. $appo->client_id,
                'start'=>$appo->startdate.' '.$appo->starttime,
                'description'=>'<p>'.$appo->starttime.'  '.$appo->endtime.'</p> <p>'.$appo->work_type_id.'</p> <p>'.$appo->regard_short.'</p> <p>'. $appo->client_id.'</p>',
                'details'=>$appo->details,
                'end'=>$enddates.' '.$appo->endtime,
                'color'=>$color
            );
        }
        echo json_encode($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $position = new Appointment;
        $position->appo_with= $request->employeeid;
        $position->regard_short= $request->regarding;
        $position->regarding= $request->regarding_shortname;
        if($request->withwhome == "Other"){
            $position->client_id= 'Other';
        }else{
            $position->client_id= $request->clientno;
        }
        
        //$position->participant = $participants;
        $position->work_type_id = $request->work_type_id;
        $position->work_type = $request->work_type;
        $position->priority = $request->priority;
        $position->details = $request->details;
        $position->endtime= $request->endtime;
        $position->teamRep=$request->teamRep;
        $position->starttime= $request->starttime;
        $position->enddate= (!empty($request->enddate1)) ? date('Y-m-d',strtotime($request->enddate1)) : '0000-00-00';
        $position->startdate= date('Y-m-d',strtotime($request->startdate));
        
        $sttime = $request->starttime;
        $endtime = $request->endtime;
        
        
        //DB::enableQueryLog();
        $appoinment = Appointment::Where('startdate','=',date('Y-m-d',strtotime($request->startdate)))
                    ->where(function($query) use($sttime,$endtime)
                        {
                            $query->where('starttime','=',$sttime)
                            ->orWhere('endtime','=',$endtime);
                        })->get();
        
        //echo "<pre>";
        //print_r($appoinment);
        //dd(DB::getQueryLog());
        $is_saveed = count($appoinment);
        //echo $is_saveed;
        //exit();
        if($is_saveed == 0){
            $position->save();
            $id = $position->id;
            if(!empty($request->participants)){
                $particount = count($request->participants);
                for($i=0;$i<$particount;$i++){
                    $insert =DB::insert('insert into appointment_participant (appt_id,participant_id) values (?, ?)', [$id, $request->participants[$i]]);
                }
                //$participants = implode(",",$request->participants);
            }else{
                $participants='';
            }
            $insert =DB::insert('insert into appointment_participant (appt_id,participant_id) values (?, ?)', [$id, $request->employeeid]);
            return redirect('fac-Bhavesh-0554/appointment/create?tab=list')->with('success','Appointment Added Successfully');
        }else{
            return redirect()->back()->with('msg', 'Appointment is booked of someone at this selectedtime.');
        }
        
    }
    
    
    
    public function storeRegard(Request $request)
    {
        $regading = new Regards();
        $regading->name = $request->name;
        $regading->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\State  $state
     * @return \Illuminate\Http\Response
     */
    public function show(State $state)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\State  $state
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {  
        //$employee = Fscemployee::orderBy('name', 'asc')->get(); 
        $employee = DB::table('employees')
            ->select('employees.*', 'teams.team as teamname')
            ->leftjoin('teams', 'teams.id', '=', 'employees.team')
            ->where('employees.type','!=','clientemployee')
            ->where('employees.check','=',1)
            ->where('employees.type','!=','Vendor')
            ->orderBy('employees.firstName', 'asc')
            ->get();
        
        
        $appoinment = Appointment::where('id', $id)->first();
        $client = Commonregister::orderBy('first_name', 'asc')->get(); 
        $emp1 = Employee::where('check','=',1)->where('type','=','employee')->orderBy('firstName', 'asc')->get();
        $start =date('Y-m-d',strtotime($request->startdate)); 
        $end = date('Y-m-d',strtotime($request->enddate));
        $employee1 = DB::Table('empschedules')->selectRaw('TIMEDIFF(emp_out,emp_in) as total,TIMEDIFF(launch_out,launch_in) as breaktime,TIMEDIFF(launch_out_second,launch_in_second) as breaktime1,emp_out,launch_in,launch_out,emp_in,ip_address,note,emp_in_date,id,employee_id,launch_out_second,launch_in_second')->whereBetween('emp_in_date',array($start,$end))->get();
        $sum = 0; 
        foreach($employee1 as $emp)
        {
            $in = date('H:i',strtotime($emp->emp_in));
            $out = date('H:i',strtotime($emp->emp_out));
            $lunch_in = date('H:i',strtotime($emp->launch_in));
            $lunch_out = date('H:i',strtotime($emp->launch_out));
            $total = date("H:i:s",strtotime($emp->total));
            
            if($emp->breaktime==NULL)
            {
                $breaktime ="00:00:00";
            }
            else
            {
                $breaktime = date("H:i:s",strtotime($emp->breaktime));
            }
            if($emp->breaktime==NULL)
            {
                $breaktime ="00:00:00";
            }
            else
            {
                $breaktime = date("H:i:s",strtotime($emp->breaktime));
            }
            if($emp->breaktime1==NULL)
            {
                $breaktime1 ="00:00:00";
            }
            else
            {
                $breaktime1 = date("H:i:s",strtotime($emp->breaktime1));
            }
            $total1 = floor($total)-(floor($breaktime) + floor ($breaktime1));
            $sum +=$total1;
        }
        $regards = Regards::All();
        $work_type = Appointment_worktype::All();
        
        
        $commonregiser  = DB::table('commonregisters')
                ->select(
                    'commonregisters.type as Type',
                    'commonregisters.id as cid',
                    'commonregisters.filename as entityid',
                    'commonregisters.first_name as fname',
                    'commonregisters.middle_name as mname',
                    'commonregisters.last_name as lname',
                    'commonregisters.company_name as cname',
                    'commonregisters.business_name',
                    'commonregisters.business_id',
                    'commonregisters.business_no',
                    'commonregisters.status')
                ->where('status','Active')
                ->orWhere('id', $request->id)->get();
            $clientdata = array();
            foreach ($commonregiser as $row) {
                if ($row->status == '1'  || $row->status == 'Active') {
                    if ($row->business_id == '6') {
                        $clientdata[] = array(
                            'filename'=>$row->entityid,
                            'company_name'=>$row->fname . ' ' . $row->mname . ' ' . $row->lname,
                            'business_no'=>$row->business_no
                        );
                    } else {
                        $clientdata[] = array(
                            'filename'=>$row->entityid,
                            'company_name'=>$row->cname,
                            'business_no'=>$row->business_no
                        );
                    }
                }
            }
        
        
        // echo "<pre>";
        // echo "asdfsadf";
        // print_r($appoinment);
        // exit();
        
        return View('fac-Bhavesh-0554.appointment.edit',compact(['employee','appoinment','client','clientdata','employee1','emp1','sum','regards','work_type']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\State  $state
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $position = new Appointment;
        $position = Appointment::find($id);
        $position->appo_with= $request->employeeid;
        $position->regard_short= $request->regarding;
        $position->regarding= $request->regarding_shortname;
        if($request->withwhome == "Other"){
            $position->client_id= 'Other';
        }else{
            $position->client_id= $request->clientno;
        }
        if(!empty($request->participants)){
            $participants = implode(",",$request->participants);    
        }else{
            $participants='';
        }
        
        $position->participant = $participants;
        $position->work_type_id = $request->work_type_id;
        $position->work_type = $request->work_type;
        $position->priority = $request->priority;
        $position->details = $request->details;
        $position->endtime= $request->endtime;
        $position->starttime= $request->starttime;
        $position->enddate= (!empty($request->enddate1)) ? date('Y-m-d',strtotime($request->enddate1)) : '0000-00-00';
        $position->startdate= date('Y-m-d',strtotime($request->startdate));
        $appoinment = Appointment::Where('startdate','=',date('Y-m-d',strtotime($request->startdate)))
                        ->Where('starttime','=',$request->starttime)
                        ->Where('id','!=',$id)
                        ->orWhere('endtime','=',$request->endtime)->get();
        
        $is_saveed = count($appoinment);
        //echo $is_saveed;
        //exit();
        if($is_saveed == 0){
            $position->save();
            return redirect('fac-Bhavesh-0554/appointment/create?tab=list')->with('success','Appointment Added Successfully');
        }else{
            return redirect()->back()->with('msg', 'Appointment is booked of someone at this selectedtime.');
        }
    }


public function getcounty(Request $request)
    {
        $data = taxstate::select('county','id','countycode')->where('state',$request->id)->take(1000)->get();
        return response()->json($data);  
    }
public function getcountycode(Request $request)
    {
        $data = taxstate::select('countycode','id')->where('county',$request->id)->take(1000)->get();
        return response()->json($data);  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\State  $state
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Appointment::where('id',$id)->delete();
       
        return redirect('fac-Bhavesh-0554/appointment/create?tab=list')->with('success','Appointment Added Successfully');
    }
}
