<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Model\Rules;
use App\Model\Employee;
use App\employees\Fscemployee;
use DB;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use App\Model\Typeofser;
class RulesController extends Controller
{
    public function __construct()
    {
    $this->middleware('auth:admin');
    }  
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $employee = Employee::where('type','=','employee')->get(); 
        $homecontent = Rules::orderBy('created_at', 'DESC')->get();
        return view('fac-Bhavesh-0554/rules/rules', compact(['homecontent','employee']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {  
        $typeofser = Typeofser::All();
       // $employee = Employee::where('check','=','1')->where('status','=','1')->get(); 
            $howtodos = DB::table('howtodos')->get();
    //echo "<pre>"; print_r($user);
          $employee = DB::table('employees as t1')->select('t1.*','t2.id as ids','t2.team as teams')
           ->Join('teams as t2', function($join){ $join->on('t2.id', '=','t1.team');})
           ->where('t1.check','=','1')->where('t1.status','=','1')
           ->get();
      
        return view('fac-Bhavesh-0554/rules/create', compact(['howtodos','employee','typeofser']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $this->validate($request,[
            'rules' =>'required',
            'type' =>'required',
            'title' =>'required',
        ]);
        $position = new Rules;
        $position->rules= $request->rules;
        $position->type= $request->type;
        $position->day = $request->day;
        $position->date = $request->date;
        $position->time = $request->time;
        $position->typeofservice = $request->typeofservice;
        $position->respon_type= $request->respon_type;
         $position->usertype= $request->usertype;
        $position->choose_date= $request->choose_date;
        $position->choose_day1= $request->choose_day1;
        $position->choose_day2= $request->choose_day2;
        $position->choose_day3= $request->choose_day3;
        $position->choose_day4= $request->choose_day4;
        $position->choose_day5= $request->choose_day5;
        $position->choose_day6= $request->choose_day6;
        $position->choose_day7= $request->choose_day7;
        $position->title= $request->title;
        $position->howtodo_id= $request->howtodo_id;
        
        $position->employee_id= $request->employee_id;
        
        $position->save();
       if($request->type=='Rules')
        {
$v = "1";
$v2 = "";
$v1 = DB::update('update fscemployees set `rule_status` = ?,`rulesdate` = ?',[$v,$v2]);
        }
        return redirect('fac-Bhavesh-0554/rules')->with('success','Success fully add Rules');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $typeofser = Typeofser::All();
     $homecontent = Rules::where('id',$id)->first(); 
    //  $employee = Employee::where('check','=','1')->get();
        $employee = DB::table('employees as t1')->select('t1.*','t2.id as ids','t2.team as teams')
           ->leftJoin('teams as t2', function($join){ $join->on('t2.id', '=','t1.team');})
           ->where('t1.status','=','1')->where('t1.check','=','1')
           ->orderby('firstName','asc')->take(1000)->get();
                     $howtodos = DB::table('howtodos')->get();
  
      
     return view('fac-Bhavesh-0554.rules.edit',compact(['howtodos','homecontent','employee','typeofser']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
       $this->validate($request,[
            'rules' =>'required',
             'type' =>'required',
                     
        ]);
        $position = Rules::find($id);
        $position->rules= $request->rules;
        $position->type= $request->type;
        $position->title= $request->title;
         $position->day = $request->day;
        $position->date = $request->date;
        $position->time = $request->time;
             $position->usertype= $request->usertype;
        $position->typeofservice = $request->typeofservice;
         $position->respon_type= $request->respon_type;
        $position->choose_date= $request->choose_date;
        $position->employee_id= $request->employee_id;
        $position->choose_day1= $request->choose_day1;
        $position->choose_day2= $request->choose_day2;
        $position->choose_day3= $request->choose_day3;
        $position->choose_day4= $request->choose_day4;
        $position->choose_day5= $request->choose_day5;
        $position->choose_day6= $request->choose_day6;
        $position->choose_day7= $request->choose_day7;
         $position->howtodo_id= $request->howtodo_id;
       
        $position->update();
        return redirect('fac-Bhavesh-0554/rules')->with('success','Success fully update Rues'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Rules::where('id',$id)->delete();
        return redirect(route('rules.index'));
    }
    
    public function getusertype(Request $request)
    {
        if($request->id == 'FSC Employee')
        {
            $emp='employee';
        }
        else if($request->id == 'FSC User')
        {
            $emp='user';
        }
        else
        {
            $emp='';
        }
        
          $data = DB::table('employees as t1')->select('t1.*','t2.id as ids','t2.team as teams')
           ->leftJoin('teams as t2', function($join){ $join->on('t2.id', '=','t1.team');})
           ->where('t1.status','=','1')->where('t1.check','=','1')->where('type',$emp)
           ->orderby('firstName','asc')->take(1000)->get();
      
      //  $data = Employee::select('type','firstName','lastName','id','status')->where('type',$emp)->where('status','=','1')->orderby('firstName','asc')->take(1000)->get();
return response()->json($data);  

    }
}
