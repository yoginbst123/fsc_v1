<?php

namespace App\Http\Controllers\Admin;

use App\Model\State;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StateController extends Controller
{
public function __construct()
    {
    $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
   $price = State::All();
       return view('fac-Bhavesh-0554/state/state', compact(['price']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $position = State::All();
       return view('fac-Bhavesh-0554/state/create', compact(['position']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $this->validate($request,[
            'authority_name' =>'required',
            'short_name' =>'required',
            'type_of_tax' =>'required',
            'telephone' =>'required',
            'address' =>'required', 
'city' =>'required', 
'state' =>'required', 
'zip' =>'required', 
'website_link_name' =>'required', 
'website_link' =>'required',                     
'website' =>'required',                     
        ]);
        $position = new State;
        $position->authority_name= $request->authority_name;
        $position->short_name= $request->short_name;
        $position->type_of_tax= $request->type_of_tax;
        $position->telephone= $request->telephone;
        $position->address= $request->address;
        $position->city= $request->city;
        $position->state= $request->state;
        $position->zip= $request->zip;
        $position->website= $request->website;
        $position->website_link= $request->website_link;
        $position->website_link_name= $request->website_link_name;
        $position->save();
        return redirect('fac-Bhavesh-0554/state')->with('success','Success fully add state');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\State  $state
     * @return \Illuminate\Http\Response
     */
    public function show(State $state)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\State  $state
     * @return \Illuminate\Http\Response
     */
    public function edit(State $state)
    {
      //  exit('1111');
        return View('fac-Bhavesh-0554.state.edit',compact('state'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\State  $state
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, State $state)
    {
       $this->validate($request,[
            'authority_name' =>'required',
            'short_name' =>'required',
            'type_of_tax' =>'required',
            'telephone' =>'required',
            'address' =>'required', 
'city' =>'required', 
'state' =>'required', 
'zip' =>'required', 
'website_link_name' =>'required', 
'website_link' =>'required',                     
'website' =>'required',                     
        ]);
        $position = $state;
        $position->authority_name= $request->authority_name;
        $position->short_name= $request->short_name;
        $position->type_of_tax= $request->type_of_tax;
        $position->telephone= $request->telephone;
        $position->address= $request->address;
        $position->city= $request->city;
        $position->state= $request->state;
        $position->zip= $request->zip;
        $position->website= $request->website;
        $position->website_link= $request->website_link;
        $position->website_link_name= $request->website_link_name;
        $position->update();
        return redirect('fac-Bhavesh-0554/state')->with('success','Success fully update state');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\State  $state
     * @return \Illuminate\Http\Response
     */
    public function destroy(State $state)
    {
         $state->delete();
        return redirect(route('state.index'));
    }
}
