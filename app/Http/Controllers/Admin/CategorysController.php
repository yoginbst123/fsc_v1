<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Categorys;
use DB;
class CategorysController extends Controller
{ 
    public function __construct()
    {
      $this->middleware('auth:admin');
    }
  
  
    public function index(Request $request)
    {  
       $categorys = Categorys::paginate(10); 
       return view('fac-Bhavesh-0554.categorys.categorys',compact('categorys'));
    }
    
    public function create()
    {
        return view('fac-Bhavesh-0554.categorys.create');
    }
    
    
    public function store(Request $request)
    {
        $this->validate($request,[
            'categoryname'=>'required',
        ]);
        $business = new Categorys;
        $business->categoryname = $request->categoryname;
        $business->save();
        return redirect(route('categorys.index'));
    }

    public function edit($id)
    {   
        $cat= Categorys::where('id',$id)->first();    
        return View('fac-Bhavesh-0554.categorys.edit',compact(['cat']));
    }
    
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'categoryname'=>'required'
        ]);
        $business = Categorys::find($id);
        $business->categoryname = $request->categoryname;
        $business->update();
        return redirect(route('categorys.index'));
    }
    
  
    public function destroy($id)
    {
        Categorys::where('id',$id)->delete();
        return redirect(route('categorys.index'));
    }
   
}
