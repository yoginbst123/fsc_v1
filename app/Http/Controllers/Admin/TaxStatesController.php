<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\taxstatesses;
use App\Model\FilingFrequency;
use App\Model\PaymentFrequency;
use Illuminate\Support\Facades\Input;
use DB;
use App\Model\formationsetup;
use App\Model\taxstate;
use App\Model\Category;
use App\Model\Business;
use App\Model\License;

class TaxStatesController extends Controller
{
public function __construct()
    {
    $this->middleware('auth:admin');
    } 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $price1 = taxstatesses::All();
        // $price2 = formationsetup::where('type1','=','Taxation')->orwhere('type1','=','License')->get();
        // $price = DB::table('users')
        //         ->union($price1)
        //         ->union($price2)
        //         ->get();
                
        // $pages  = DB::table('taxstatesses')
        //             ->select('taxstatesses.id','taxstatesses.authority_name_state','taxstatesses.authority_name_state','taxstatesses.short_name_state','taxstatesses.typeofform1_state','taxstatesses.due_date_state','taxstatesses.extensionallow_state','taxstatesses.extension_due_date_state','taxstatesses.federal_fax_authotiry_state','taxstatesses.website_link_name_state','taxstatesses.telephone_state','taxstatesses.reminder_state')->get();
       
         
        // $blogitems  =  DB::table('formationsetups')
        //             ->select('formationsetups.id','formationsetups.question','formationsetups.status','formationsetups.type','formationsetups.type1','formationsetups.website','formationsetups.telephone','formationsetups.fax','formationsetups.authorityname','formationsetups.business_catagory_name','formationsetups.bussiness_name','formationsetups.taxationtypes')->get(); 
         
        $pages  = DB::table('taxstatesses')
                    ->select('taxstatesses.id','taxstatesses.taxationtypes','taxstatesses.typeofform_state as question1','taxstatesses.authority_name1_state as au_name','taxstatesses.telephone_state as telephones','taxstatesses.typeofform1_state as formtype');
        
        $blogitems  =  DB::table('formationsetups')
                    ->select('formationsetups.id','formationsetups.taxationtypes','formationsetups.question as question1','formationsetups.authorityname as au_name','formationsetups.telephone as telephones','formationsetups.type as formtype');
        //print_r($blogitems);die;
        
        $price = $pages->union($blogitems)->get();
       // print_r($price);die;  
        
       // $price = taxstatesses::All();
        return view('fac-Bhavesh-0554/states/states', compact(['price']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $business = Business::all();
        $position = formationsetup::All();
        $category = Category::orderby('business_cat_name','asc')->get(); 
        $currency = License::All();
        
        $entity = DB::table('typeofentity')->get();
        $position = taxstatesses::All();
        return view('fac-Bhavesh-0554/states/create', compact(['position','entity','position','category','business','currency']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[]);
        $position = new taxstatesses;
        
        $position->taxationtypes ='statetaxation';
        $position->state = $request->state;
        $position->short_name_state = $request->short_name;
        $position->yearlytype_state = $request->yearlytype;
        $position->authority_name_state = $request->authority_name;  
        $position->typeofform_state = $request->typeofform;
        $position->filling_frequency = $request->filling_frequency;
        
        $position->formname_state = $request->formname;
        $position->due_date_state = $request->duedate;
        $position->extension_due_date_state = $request->extdate;
         $position->start_date = $request->start_date;
        $position->authority_level_state = $request->authority_level;
        $position->authority_name1_state = $request->authority_name1;
        $position->website_state = $request->website;
        $position->telephone_state = $request->telephone;
        $position->payroll_name_state = $request->payroll_name;
        $position->renewlink_state = $request->renewlink;
        $position->uniques_state = $request->type;
        $position->payroll_department_name_state = $request->payroll_department_name;
        $position->payroll_link_state = $request->payroll_link;
        $position->payroll_telephone_state = $request->payroll_telephone;
        $position->payroll_state  = $request->payroll;

       // $position->state_tax_month = $request->state_tax_month;
       // $position->state_tax_year = $request->state_tax_year;
       // $position->state_tax_personal_rate = $request->state_tax_personal_rate;
        $position->lost = $request->lost;
        $position->educational = $request->educational;
        $position->splost = $request->splost;
        $position->tsplost  = $request->tsplost;
        
        $filing_frequency_form = $request->filing_frequency_form;
        $filing_frequency_name = $request->filing_frequency_name;
        $filing_frequency = $request->filing_frequency;
        $filing_frequency_due_date = $request->filing_frequency_due_date;
        $filing_frequency_form1 = $request->filing_frequency_form1;
        $filing_frequency_name1 = $request->filing_frequency_name1;
        $filing_frequency1 = $request->filing_frequency1;
        $filing_frequency_due_date1 = $request->filing_frequency_due_date1;
        
       
        
     for($i=0; $i < count($filing_frequency_form1); $i++){
        // $check = PaymentFrequency::where('id', '=',$conid1)->delete();
     $models = new PaymentFrequency;
     $models->filing_frequency_form1 = $filing_frequency_form1[$i];
     $models->filing_frequency_name1 = $filing_frequency_name1[$i];
     $models->filing_frequency1 = $filing_frequency1[$i];
     $models->filing_frequency_due_date1 = $filing_frequency_due_date1[$i];
     $models->type = $request->type;
     $models->save();
        }
       
     for($i=0; $i < count($filing_frequency_form); $i++){
        // $check = FilingFrequency::where('id', '=',$conid1)->delete();
            $models = new FilingFrequency;
            $models->filing_frequency_form = $filing_frequency_form[$i];
            $models->filing_frequency_name = $filing_frequency_name[$i];
            $models->filing_frequency = $filing_frequency[$i];
            $models->filing_frequency_due_date = $filing_frequency_due_date[$i];
            $models->type = $request->type;
            $models->save();
        }
        
        $position->save();
        $id=$position->id;
        $mnt=0;
        foreach($request->state_tax_month as $state_tax_month)
        {
            $state_tax_months = $state_tax_month;
            $state_tax_years =$request->state_tax_year[$mnt];
            $state_tax_personal_rates =$request->state_tax_personal_rate[$mnt];
            $usid1 =$id;
            $mnt++;
            $insert2 = DB::insert("insert into statetosalestax(`statesalesid`,`state_tax_month`,`state_tax_year`,`state_tax_personal_rate`) values('".$usid1."','".$state_tax_months."','".$state_tax_years."','".$state_tax_personal_rates."')");
        }
        return redirect('fac-Bhavesh-0554/states')->with('success','Success fully add State');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Federal  $federal
     * @return \Illuminate\Http\Response
     */
    public function show(Federal $federal)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Federal  $federal
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       // exit('111');
       // echo $id;
        $statetax = DB::table('statetosalestax')->where('statesalesid','=',$id)->get();
        //echo "<pre>";
        //print_r($statetax);die;
        $entity = DB::table('typeofentity')->get();
      // $bussiness = taxstatesses::where('id', $id)->first();
     //  print_r($bussiness);
      // echo $bussiness;exit;
       $pages  = DB::table('taxstatesses')
                    ->select('taxstatesses.id','taxstatesses.taxationtypes','taxstatesses.typeofform_state as question1','taxstatesses.authority_name1_state as au_name','taxstatesses.telephone_state as telephones','taxstatesses.typeofform1_state as formtype');
        
        $blogitems  =  DB::table('formationsetups')
                    ->select('formationsetups.id','formationsetups.taxationtypes','formationsetups.question as question1','formationsetups.authorityname as au_name','formationsetups.telephone as telephones','formationsetups.type as formtype');
        //print_r($blogitems);die;
        
        $bussiness = $pages->union($blogitems)->where('id',$id)->first();
 //   echo "<pre>";
 //print_r($bussiness);exit;
    //    $type = $bussiness->uniques;
      //  $fill = FilingFrequency::where('type', $type)->get();
       //$pay = PaymentFrequency::where('type', $type)->get();
     //   return View('fac-Bhavesh-0554.states.edit',compact(['bussiness','fill','pay','entity','statetax']));
     return View('fac-Bhavesh-0554.states.edit',compact(['bussiness','entity','statetax']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Federal  $federal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        // echo "<pre>";
        // print_r($_POST);
        // die;
        $position = taxstatesses::find($id);
        $position->short_name_state = $request->short_name;
        $position->state = $request->state;
        $position->yearlytype_state = $request->yearlytype;
        $position->authority_name_state = $request->authority_name;  
        $position->typeofform_state = $request->typeofform;
        $position->filling_frequency = $request->filling_frequency;
       
        $position->formname_state = $request->formname;
        $position->due_date_state = $request->duedate;
        $position->extension_due_date_state = $request->extdate;
            $position->start_date = $request->start_date;
        $position->authority_level_state = $request->authority_level;
        $position->authority_name1_state = $request->authority_name1;
        $position->website_state = $request->website;
        $position->telephone_state = $request->telephone;
        $position->payroll_name_state = $request->payroll_name;
        $position->renewlink_state = $request->renewlink;
        $position->uniques_state = $request->type;
        $position->payroll_department_name_state = $request->payroll_department_name;
        $position->payroll_link_state = $request->payroll_link;
        $position->payroll_telephone_state = $request->payroll_telephone;
        
        //$position->state_tax_month = $request->state_tax_month;
      //  $position->state_tax_year = $request->state_tax_year;
       // $position->state_tax_personal_rate = $request->state_tax_personal_rate;
        $position->lost = $request->lost;
        $position->educational = $request->educational;
        $position->splost = $request->splost;
        $position->tsplost  = $request->tsplost;
        
        
        $filing_frequency_form = $request->filing_frequency_form;
        $filing_frequency_name = $request->filing_frequency_name;
        $filing_frequency = $request->filing_frequency;
        $filing_frequency_due_date = $request->filing_frequency_due_date;
        $filing_frequency_form1 = $request->filing_frequency_form1;
        $filing_frequency_name1 = $request->filing_frequency_name1;
        $filing_frequency1 = $request->filing_frequency1;
        $filing_frequency_due_date1 = $request->filing_frequency_due_date1;
        $pay_id = $request->pay_id;
        $fill = $request->fillid;
        
        
       
     
        // for($i=0; $i < count($filing_frequency_form1); $i++)
        // {
        //     $id1 = $pay_id[$i];   
        //     $models = new PaymentFrequency;
        //     $models->filing_frequency_form1 = $filing_frequency_form1[$i];
        //     $models->filing_frequency_name1 = $filing_frequency_name1[$i];
        //     $models->filing_frequency1 = $filing_frequency1[$i];
        //     $models->filing_frequency_due_date1 = $filing_frequency_due_date1[$i];
        //     $models->type = $request->type;
        //     $models->save();
        //     if(empty($pay_id))
        //     {
        //     }
        //     else
        //     {
        //         $affectedRows1 = PaymentFrequency::where('id', '=',$id1)->delete();
        //         $affectedRows1 = PaymentFrequency::where('filing_frequency_form1', '=', NULL)->delete();
        //     }     
        // }
       
            // for($j=0; $j < count($filing_frequency_form); $j++)
            // {
            //     // $check = FilingFrequency::where('id', '=',$conid1)->delete();
            //     $fill1 = $fill[$j];
            //     $models = new FilingFrequency;
            //     $models->filing_frequency_form = $filing_frequency_form[$j];
            //     $models->filing_frequency_name = $filing_frequency_name[$j];
            //     $models->filing_frequency = $filing_frequency[$j];
            //     $models->filing_frequency_due_date = $filing_frequency_due_date[$j];
            //     $models->type = $request->type;
            //     $models->save();
            //     if(empty($fill))
            //     {
            //     }
            //     else
            //     {
            //     $affectedRows1 = FilingFrequency::where('id', '=',$fill1)->delete();
            //     $affectedRows1 = FilingFrequency::where('filing_frequency_form', '=', NULL)->delete();
            //     }     
            // }       
        $position->update();
        $id=$position->id;
        $mnt=0;
        foreach($request->state_tax_month as $state_tax_month)
        {
            $sid =$request->sid[$mnt];
            $state_tax_months = $state_tax_month;
            $state_tax_years =$request->state_tax_year[$mnt];
            $state_tax_personal_rates =$request->state_tax_personal_rate[$mnt];
            $usid1 =$id;
            $mnt++;
            DB::table('statetosalestax')->where('id','=',$sid)->delete();
            $insert2 = DB::insert("insert into statetosalestax(`statesalesid`,`state_tax_month`,`state_tax_year`,`state_tax_personal_rate`) values('".$usid1."','".$state_tax_months."','".$state_tax_years."','".$state_tax_personal_rates."')");
        }
        
        return redirect('fac-Bhavesh-0554/states')->with('success','Success fully update State');
    }

   
    public function destroy($id)
    {
 
           taxstatesses::where('id',$id)->delete();
            return redirect('fac-Bhavesh-0554/states')->with('success','Success fully deleted State');
    }
    
    // public function statetaxdelete($id)
    // {
    //     DB::table('statetosalestax')->where('id','=',$id)->delete();
    //     return redirect('fac-Bhavesh-0554/states')->with('success','Success fully deleted county');
    // }
    
    
    public function destroylicense($id)
    {
        formationsetup::where('id',$id)->delete();
        return redirect('fac-Bhavesh-0554/states')->with('success','Success fully deleted State');
    }
    
}
