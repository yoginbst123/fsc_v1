<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Citystate;
use DB;
use Input;

use Session;
use Excel;
class ZipcodeController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
     return view('fac-Bhavesh-0554.zipcode.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'import_file'=>'required',
              
        ]);
$file = $request->import_file;
 $handle = fopen($file, "r");
 $c = 0;
 while(($filesop = fgetcsv($handle, 1000, ",")) !== false)
 {
 $name = $filesop[0];
 $email = $filesop[1];
 $email1 = $filesop[2];
 $email2 = $filesop[3];
 $email3 = $filesop[4];
DB::insert('insert into citystates(`zipcode`,`city`,`state`,`country`,`county`) values(?,?,?,?,?)',[$name,$email,$email1,$email2,$email3]);
 }
 return redirect(route('citystate.index'));


    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
   
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $city= Zipcode::where('id',$id)->first();    
        return View('admin.zipcode.edit',compact('city'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    $this->validate($request,[
            'zipcode'=>'required',
        ]);
        $business =Zipcode::find($id);
        $business->zipcode = $request->zipcode;
        $business->update();
        return redirect(route('zipcode.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Zipcode::where('id',$id)->delete();
        return redirect(route('zipcode.index'));
    }
}
