<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Front\Commonregister;
use App\Model\BusinessBrand;

use App\Model\Category;
use App\Model\Business;
use App\Model\Employee;
use App\Model\client_professional;
use App\Model\client_shareholder;
use App\User;
use App\Model\Categorybusiness;
use App\Model\Contact_userinfo;
use App\Mail\Activationmail;
use App\Model\taxstate;
use DB;
use Hash;
use Carbon;
use App\Submissionlogin\Submissionlogin;
use App\Model\Admin;
use Auth;
use App\Model\Price;
use App\Model\Currency;
use App\Model\Typeofser;
use App\Model\Period;
use App\Model\Taxtitle;
use App\Model\Language;
use App\Model\Ethnic;
use App\Model\Document;
use App\Model\Adminupload;

use App\employees\Fscemployee;
use Illuminate\Support\Facades\Input;
use App\Model\Logo;
use App\Model\admin_professional;


class AdminworkstatusController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
   
   
    
    public function index(Request $request)
    {
        $id=Auth::user()->id;
        $formations = DB::table('admin_formation')->where('admin_id','=',$id)->get();
        $common = DB::table('admins')->select('*')->where('id', '=',$id)->get()->first(); 
        $Incometax = DB::table('admin_taxation_it')->select('*')->get(); 
        
        $buslicense = DB::table('admin_license')->select('*')->where('admin_id', '=',$id)->get(); 
        $admin_buslicense = DB::table('admin_license')->select('*')->where('admin_id', '=',$id)->orderBy('id', 'DESC')->first(); 
        $admin_bank = DB::table('admin_bank')->select('id','bank_name')->orderBy('bank_name', 'ASC')->get(); 
        $upload1 = Adminupload::All();
        $admin_professional1 = DB::table('admin_professionals as t1')->select('t1.*','t2.id as ids','t2.admin_id','t2.admin_lic_id','t2.pro_license_year','t2.pro_license_fee','t2.pro_license_copy','t2.status','t2.pro_license_note')
                                    ->leftJoin('admin_pro_lic as t2', function($join){ $join->on('t1.id', '=', 't2.admin_lic_id');})
                                    ->orderBy('t1.id', 'ASC')->get();
        $up = DB::table('adminuploads')->first();
        $ak1 = DB::table('admin_professionals')->first();
        $document = Document::get();
        $admindoc = DB::table('admintodocument')->get();
        $sharledger = DB::table('admin_shareledger')->get();
        $newbanking = DB::table('new_banking as t1')->select('t1.*','t2.id as ids','t2.bank_name')
                        ->leftJoin('admin_bank as t2', function($join){ $join->on('t1.bankname_banking', '=', 't2.id');})
                        ->where('t1.admin_id', '=',$id)->get();
                        
        $newbankinglast = DB::table('new_banking')->select('*')->where('admin_id', '=',$id)->orderBy('id', 'DESC')->first();                         
        // echo "<pre>";
         //print_r($newbankinglast);die;
        return view('fac-Bhavesh-0554/adminworkstatus/adminworkstatus',compact(['newbankinglast','newbanking','sharledger','admin_bank','Incometax','up','upload1','ak1','admin_professional1','admin_buslicense','admindoc','document','common','formations','buslicense']));
    }
   
   
   


    public function storefederal(Request $request)
    {
       // print_r($_POST);EXIT;
        $federal_year=$request->federalyear;
        $federal_tax=$request->federaltax;
        $federal_formno=$request->federalformno;
        $federal_duedate=date('Y-m-d',strtotime($request->federalduedate));
        $federal_method=$request->federalmethod;
        $federal_date=date('Y-m-d',strtotime($request->federaldate));
        
        $federal_status=$request->federalstatus;
        $federal_note=$request->federalnote;
        
        $path1= public_path().'/adminupload/'.$_FILES['federalfile']['name'];
        
        
        if(isset($_FILES['federalfile']['name'])!='')
        {
             if(move_uploaded_file($_FILES['federalfile']['tmp_name'], $path1)) 
            {
               $federal_copy= $_FILES['federalfile']['name']; 
            } 
        }
        $federal_file=$federal_copy;
        
       
        DB::insert("insert into admin_taxation_it(`federalyear`,`federaltax`,`federalformno`,`federalduedate`,`federalmethod`,`federaldate`,`federalstatus`,`federalnote`,`federalfile`) 
         values('$federal_year','$federal_tax','$federal_formno','$federal_duedate','$federal_method','$federal_date','$federal_status','$federal_note','$federal_file')");
        return redirect('fac-Bhavesh-0554/adminworkstatus')->with('success','Taxation Added Successfully');

    } 

    public function store(Request $request)
    {
      
       // echo "<pre>"; 
        //print_r($_REQUEST);die;
        $this->validate($request,[
            // 'license_year' =>'required',
            // 'license_gross' =>'required',
            // 'license_fee' =>'required',
            // 'license_status' =>'required',
            // 'license_copy	' =>'required', 
            // 'license_no' =>'required', 
            // 'website_link' =>'required', 
            // 'license_renew_date' =>'required',                   
        ]);
        
        $admin_id=$request->admin_id;
        $license_year=$request->license_year;
        $license_gross=$request->license_gross;
        $license_fee=$request->license_fee;
        $license_status=$request->license_status;
        $license_website=$request->website;
        
       // $license_copy=$request->license_copy;
        
        $path1= public_path().'/adminupload/'.$_FILES['license_copy']['name'];
        
        
        if(isset($_FILES['license_copy']['name'])!='')
        {
             if(move_uploaded_file($_FILES['license_copy']['tmp_name'], $path1)) 
            {
               $license_copy= $_FILES['license_copy']['name']; 
            } 
        }
        
        //$license_copy=$request->license_copy;
        $license_no=$request->license_no;
      //  $website_link=$request->website_link;
        $license_note=$request->license_note;
      
        $license_renew_date=$request->license_renew_date;
       
        DB::insert("insert into admin_license(`admin_id`,`license_year`,`license_gross`,`license_fee`,`license_status`,`license_copy`,`license_no`,`website`,`license_note`,`license_renew_date`) 
         values('$admin_id','$license_year','$license_gross','$license_fee','$license_status','$license_copy','$license_no','$license_website','$license_note','$license_renew_date')");
        return redirect('fac-Bhavesh-0554/adminworkstatus')->with('success','License Added Successfully');
    }
    
    
    public function addshareledger(Request $request)
    {
        // echo "<pre>"; 
        // print_r($_REQUEST);die;
        $this->validate($request,[
            // 'holder_name' =>'required',
            // 'issue_certificate' =>'required',
            // 'origine_issue_from' =>'required',
            // 'paid_amount' =>'required',
            // 'transfer_date	' =>'required', 
            // 'surrender_certificate' =>'required', 
            // 'surrender_certificate_no' =>'required', 
        ]);
        
        
        $admin_id=$request->admin_id;
        $holder_name=$request->holder_name;
        $issue_certificate=$request->issue_certificate;
        $origine_issue_from=$request->origine_issue_from;
        $paid_amount=$request->paid_amount;
        $transfer_date=$request->transfer_date;
        $surrender_certificate=$request->surrender_certificate;
        $surrender_certificate_no=$request->surrender_certificate_no;
        
        $returnValue = DB::table('admin_shareledger')
        ->insert([ 
                   'admin_id' => $admin_id,
                   'holder_name' => $holder_name,
                   'issue_certificate' =>$issue_certificate,
                   'origine_issue_from' =>$origine_issue_from,
                   'paid_amount' =>$paid_amount,
                   'transfer_date' =>$transfer_date,
                   'surrender_certificate' =>$surrender_certificate,
                   'surrender_certificate_no' =>$surrender_certificate_no,
                 
            ]);     
       
        return redirect('fac-Bhavesh-0554/adminworkstatus')->with('success','Share Ledger Added Successfully');
    }
    
    
    public function updateshareledger(Request $request)
    {
        
        $this->validate($request,[
            // 'holder_name' =>'required',
            // 'issue_certificate' =>'required',
            // 'origine_issue_from' =>'required',
            // 'paid_amount' =>'required',
            // 'transfer_date	' =>'required', 
            // 'surrender_certificate' =>'required', 
            // 'surrender_certificate_no' =>'required', 
        ]);
       
        
        $id=$_REQUEST['ledger_id'];
        $admin_id=$_REQUEST['admin_ledid'];
        $holder_name=$_REQUEST['holder_name'];
        $issue_certificate=$_REQUEST['issue_certificate'];
        $origine_issue_from=$_REQUEST['origine_issue_from'];
        $paid_amount=$_REQUEST['paid_amount'];
        $transfer_date=$_REQUEST['transfer_date'];
        $surrender_certificate=$_REQUEST['surrender_certificate'];
        $surrender_certificate_no=$_REQUEST['surrender_certificate_no'];
       // echo "<pre>"; 
        // print_r($_REQUEST);die;
         
         DB::table('admin_shareledger')->where('id', '=', $id)
        ->update([
                   'admin_id' => $admin_id,
                   'holder_name' => $holder_name,
                   'issue_certificate' =>$issue_certificate,
                   'origine_issue_from' =>$origine_issue_from,
                   'paid_amount' =>$paid_amount,
                   'transfer_date' =>$transfer_date,
                   'surrender_certificate' =>$surrender_certificate,
                   'surrender_certificate_no' =>$surrender_certificate_no,
                 
            ]);     
       
        return redirect('fac-Bhavesh-0554/adminworkstatus')->with('success','Share Ledger Updated Successfully');
    }
    
    
    
    public function addnewbanking(Request $request)
    {
        // echo "<pre>"; 
        // print_r($_REQUEST);die;
        $this->validate($request,[
            // 'holder_name' =>'required',
            // 'issue_certificate' =>'required',
            // 'origine_issue_from' =>'required',
            // 'paid_amount' =>'required',
            // 'transfer_date	' =>'required', 
            // 'surrender_certificate' =>'required', 
            // 'surrender_certificate_no' =>'required', 
        ]);
        
        
        $admin_id=$request->admin_id;
        $bankname_banking=$request->bankname_banking;
        $bankfourdigit=$request->bankfourdigit;
        $banknick_name=$request->banknick_name;
        $bankstartdate=$request->bankstartdate;
        $bank_beginnigbalance=$request->bank_beginnigbalance;
        $bankenddate=$request->bankenddate;
        $bank_notes=$request->bank_notes;
        $bank_accountdoneby=$request->bank_accountdoneby;
        $bank_endingbalance=$request->bank_endingbalance;
        $bank_accountdonedate=$request->bank_accountdonedate;
        
        $returnValue = DB::table('new_banking')
        ->insert([ 
                   'admin_id' => $admin_id,
                   'bankname_banking' => $bankname_banking,
                   'bankfourdigit' =>$bankfourdigit,
                   'banknick_name' =>$banknick_name,
                   'bankstartdate' =>$bankstartdate,
                   'bank_beginnigbalance' =>$bank_beginnigbalance,
                   'bankenddate' =>$bankenddate,
                   'bank_notes' =>$bank_notes,
                   'bank_accountdoneby' =>$bank_accountdoneby,                 
                   'bank_endingbalance' =>$bank_endingbalance,
                   'bank_accountdonedate' =>$bank_accountdonedate,
            ]);     
       
        return redirect('fac-Bhavesh-0554/adminworkstatus')->with('success','New Banking Added Successfully');
    }
    
    
    public function updatenewbanking(Request $request)
    {
        // echo "<pre>"; 
        // print_r($_REQUEST);die;
        $this->validate($request,[
            // 'holder_name' =>'required',
            // 'issue_certificate' =>'required',
            // 'origine_issue_from' =>'required',
            // 'paid_amount' =>'required',
            // 'transfer_date	' =>'required', 
            // 'surrender_certificate' =>'required', 
            // 'surrender_certificate_no' =>'required', 
        ]);
        
        $admin_id=$request->adminnewnk_id;
        $id=$request->newbank_id;
        $bankname_banking=$request->bankname_banking;
        $bankfourdigit=$request->bankfourdigit;
        $banknick_name=$request->banknick_name;
        $bankstartdate=$request->bankstartdate;
        $bank_beginnigbalance=$request->bank_beginnigbalance;
        $bankenddate=$request->bankenddate;
        $bank_notes=$request->bank_notes;
        $bank_accountdoneby=$request->bank_accountdoneby;
        $bank_endingbalance=$request->bank_endingbalance;
        $bank_accountdonedate=$request->bank_accountdonedate;
        
        $returnValue = DB::table('new_banking')->where('id', '=', $id)
        ->update([ 
                   'admin_id' => $admin_id,
                   'bankname_banking' => $bankname_banking,
                   'bankfourdigit' =>$bankfourdigit,
                   'banknick_name' =>$banknick_name,
                   'bankstartdate' =>$bankstartdate,
                   'bank_beginnigbalance' =>$bank_beginnigbalance,
                   'bankenddate' =>$bankenddate,
                   'bank_notes' =>$bank_notes,
                   'bank_accountdoneby' =>$bank_accountdoneby,                 
                   'bank_endingbalance' =>$bank_endingbalance,
                   'bank_accountdonedate' =>$bank_accountdonedate,
            ]);     
       
        return redirect('fac-Bhavesh-0554/adminworkstatus')->with('success','New Banking Updated Successfully');
    }
    
    public function getbankcodes(Request $request)
    {
        $data = DB::table('admin_bank')->where('id',$request->id)->take(1000)->get();
        return response()->json($data);  
    }
    
    public function getbankcodes2(Request $request)
    {
        $data = DB::table('admin_bank')->where('id',$request->id)->take(1000)->get();
        return response()->json($data);  
    }

    public function updatebuslicense(Request $request)
    {
      
        // echo "<pre>"; 
        // print_r($_REQUEST);die;
        $this->validate($request,[
            // 'license_year' =>'required',
            // 'license_gross' =>'required',
            // 'license_fee' =>'required',
            // 'license_status' =>'required',
            // 'license_copy	' =>'required', 
            // 'license_no' =>'required', 
            // 'website_link' =>'required', 
            // 'license_renew_date' =>'required',                   
        ]);
        
        $id=$request->idlic;
        $admin_id=$request->admin_id;
        $license_year=$request->license_year;
        $license_gross=$request->license_gross;
        $license_fee=$request->license_fee;
        $license_status=$request->license_status;
    
        
        $path1= public_path().'/adminupload/'.$_FILES['license_copy']['name'];
        if(isset($_FILES['license_copy']['name'])!='')
        {
            if(move_uploaded_file($_FILES['license_copy']['tmp_name'], $path1)) 
            {
               $license_copy= $_FILES['license_copy']['name']; 
            } 
            else
            {
                $license_copy=$request->license_copy_2;
            }
        }
        else
        {
            $license_copy=$request->license_copy_2;
        }
        
        $license_no=$request->license_no;
        $license_note=$request->license_note;
        $license_renew_date=$request->license_renew_date;
        
        $returnValue = DB::table('admin_license')->where('id', '=', $id)
        ->update([ 
                   'admin_id' => $admin_id,
                   'license_year' => $license_year,
                   'license_gross' =>$license_gross,
                   'license_fee' =>$license_fee,
                   'license_status' =>$license_status,
                   'license_copy' =>$license_copy,
                   'license_no' =>$license_no,
                   'license_note' =>$license_note,
                   'license_renew_date' =>$license_renew_date
            ]);     
       
        return redirect('fac-Bhavesh-0554/adminworkstatus')->with('success','License Updated Successfully ');
    }
    

    
   
    public function update(Request $request)
    {
        //echo "<pre>";print_r($_POST);die;
        $this->validate($request,[                  
            'formation_yearbox'=>'required',   
            'formation_yearvalue'=>'required',   
            'formation_amount'=>'required',   
            'formation_payment'=>'required',   
            'record_status'=>'required',
           // 'formation_work_officer'=>'required',
            //'annualreceipt'=>'required',

        ]); 
        
        $formationid=$request->formationid;
        $admin_id=$request->admin_id_formation;
        if($request->formation_yearbox!='')
        {
            $formation_yearbox=$request->formation_yearbox;
        }
       
        if($request->formation_yearbox2!='')
        {
           $formation_yearbox=$request->formation_yearbox2;
        }
       
        if($request->formation_yearbox3!='')
        {
            $formation_yearbox=$request->formation_yearbox3; 
        }
       
        $formation_yearvalue =$request->formation_yearvalue;
        $formation_amount = $request->formation_amount;
        $formation_payment = $request->formation_payment;
        
        $record_status = $request->record_status;
        $path1= public_path().'/adminupload/'.$_FILES['annualreceipt']['name'];
        $path2= public_path().'/adminupload/'.$_FILES['formation_work_officer']['name'];
        
        if(isset($_FILES['annualreceipt']['name'])!='')
        {
            if(move_uploaded_file($_FILES['annualreceipt']['tmp_name'], $path1)) 
            {
               $filesname1= $_FILES['annualreceipt']['name']; 
            } 
            else
            {
               $filesname1= $_POST['annualreceipt_1']; 
            }
        }
        else
        {
           $filesname1= $_POST['annualreceipt_1']; 
        }
            
        if(isset($_FILES['formation_work_officer']['name'])!='')
        {
            if(move_uploaded_file($_FILES['formation_work_officer']['tmp_name'], $path2)) 
            {
               $filesname2= $_FILES['formation_work_officer']['name']; 
            } 
            else
            {
               $filesname2= $_POST['officers']; 
            }
        }
        else
        {
           $filesname2= $_POST['officers']; 
        }
            
            $returnValue = DB::table('admin_formation')->where('id', '=', $formationid)
            ->update([ 
                        
                       'admin_id' => $admin_id,
                       'formation_yearbox' =>$formation_yearbox,
                       'formation_yearvalue' =>$formation_yearvalue,
                       'formation_amount' =>$formation_amount,
                       'formation_payment' =>$formation_payment,
                       'record_status' =>$record_status,
                       'annualreceipt' =>$filesname1,
                       'formation_work_officer' =>$filesname2,
                       
                ]);     

        return redirect(route('adminworkstatus.index'))->with('success','Your Profile Updated Successfully ');
    }



    public function updatedocument()
    {
        //print_r($_POST);die;
       
        $admin_id=$_POST['admin_id'];
        $documentsname=$_POST['documentsname']; 
        
        
        if(isset($_FILES['admindocument']['name'])!='')
        {
            $path1= public_path().'/adminupload/'.$_FILES['admindocument']['name'];    
            if(move_uploaded_file($_FILES['admindocument']['tmp_name'], $path1)) 
            {
               $filesname1= $_FILES['admindocument']['name']; 
            } 
        }
        
        
        $insert2 = DB::insert("insert into admintodocument(admin_id,documentsname,admindocument)
                                                        values('$admin_id','$documentsname','$filesname1')");
         return redirect(route('adminworkstatus.index'))->with('success','Your Profile Updated Successfully ');
    }
    
    
    public function updateprolicense(Request $request)
    {
      
        // echo "<pre>"; 
        // print_r($_REQUEST);die;
        $this->validate($request,[
            // 'license_year' =>'required',
            // 'license_gross' =>'required',
            // 'license_fee' =>'required',
            // 'license_status' =>'required',
            // 'license_copy	' =>'required', 
            // 'license_no' =>'required', 
            // 'website_link' =>'required', 
            // 'license_renew_date' =>'required',                   
        ]);
        
       
        $admin_lic_id=$request->admin_lic_id;
        $admin_id=$request->admin_id;
        $pro_license_year=$request->pro_license_year;
        
        $pro_license_fee=$request->pro_license_fee;
        $status=$request->status;
    
        
        $path1= public_path().'/adminupload/'.$_FILES['pro_license_copy']['name'];
        if(isset($_FILES['pro_license_copy']['name'])!='')
        {
            if(move_uploaded_file($_FILES['pro_license_copy']['tmp_name'], $path1)) 
            {
               $pro_license_copy= $_FILES['pro_license_copy']['name']; 
            } 
            
        }
        
        
        $pro_license_website=$request->pro_license_website;
        $pro_license_note=$request->pro_license_note;
        
        $returnValue = DB::table('admin_pro_lic')
        ->insert([ 
                    
                   'admin_lic_id' => $admin_lic_id,
                   'admin_id' => $admin_id,
                   'pro_license_year' => $pro_license_year,
                   'pro_license_fee' =>$pro_license_fee,
                   'status' =>$status,
                   'pro_license_copy' =>$pro_license_copy,
                   'pro_license_website' =>$pro_license_website,
                   'pro_license_note' =>$pro_license_note,
                  
            ]);     
            
        $returnValue = DB::table('admin_professionals')->where('id', '=', $admin_lic_id)
            ->update([ 
                    
                    'admin_id' => $admin_id,
                    'profession_license_copy' =>$pro_license_copy,
                    'profession_license_website'=>$pro_license_website,
            ]);    
       
        return redirect('fac-Bhavesh-0554/adminworkstatus')->with('success','License Updated Successfully ');
    }
    
    
    
    public function updatingeprolicense(Request $request)
    {
      
        // echo "<pre>"; 
        // print_r($_REQUEST);die;
        $this->validate($request,[
            // 'license_year' =>'required',
            // 'license_gross' =>'required',
            // 'license_fee' =>'required',
            // 'license_status' =>'required',
            // 'license_copy	' =>'required', 
            // 'license_no' =>'required', 
            // 'website_link' =>'required', 
            // 'license_renew_date' =>'required',                   
        ]);
        
        
        $id=$request->id;
        $admin_lic_id=$request->admin_lic_id;
        $admin_id=$request->admin_id;
        $pro_license_year=$request->pro_license_year;
        
        $pro_license_fee=$request->pro_license_fee;
        $status=$request->status;
    
        
        $path1= public_path().'/adminupload/'.$_FILES['pro_license_copy']['name'];
        if(isset($_FILES['pro_license_copy']['name'])!='')
        {
            if(move_uploaded_file($_FILES['pro_license_copy']['tmp_name'], $path1)) 
            {
               $pro_license_copy= $_FILES['pro_license_copy']['name']; 
            }
            else
            {
               $pro_license_copy= $_POST['pro_license_copy_2']; 
            }
            
            
        }
        else
        {
           $pro_license_copy= $_POST['pro_license_copy_2']; 
        }
        
        $pro_license_website=$request->pro_license_website;
        $pro_license_note=$request->pro_license_note;
        
        
        $returnValue = DB::table('admin_pro_lic')->where('id', '=', $id)
            ->update([ 
                    
                   'admin_lic_id' => $admin_lic_id,
                   'admin_id' => $admin_id,
                   'pro_license_year' => $pro_license_year,
                   'pro_license_fee' =>$pro_license_fee,
                   'status' =>$status,
                   'pro_license_copy' =>$pro_license_copy,
                   'pro_license_website'=>$pro_license_website,
                   'pro_license_note' =>$pro_license_note,
                  
            ]);
            
        $returnValue = DB::table('admin_professionals')->where('id', '=', $admin_lic_id)
            ->update([ 
                    
                    'admin_id' => $admin_id,
                    'profession_license_copy' =>$pro_license_copy,
                    'profession_license_website'=>$pro_license_website,
            ]);    
       
        return redirect('fac-Bhavesh-0554/adminworkstatus')->with('success','License Updated Successfully ');
    }

    public function getdocument(Request $request)
    {
        $documentRow = DB::table('admintodocument')->where('id',$request->documentid)->first();
        return response()->json($documentRow);
    }


    public function getbuslicense(Request $request)
    {
        $licenseRow = DB::table('admin_license')->where('id',$request->licenseid)->first();
        return response()->json($licenseRow);
    }
    
    public function getformation(Request $request)
    {
        $formationRow = DB::table('admin_formation')->where('id',$request->formationid)->first();
        return response()->json($formationRow);
    }
    
     public function getprolicense(Request $request)
    {
        $prolicenseRow = DB::table('admin_pro_lic')->where('id',$request->prolicenseid)->first();
        return response()->json($prolicenseRow);
    }
        
    public function updatedocuments(Request $request,$id)
    {
        //print_r($_REQUEST);
        //print_r($_POST);die;
       
        $admin_id=$request->admin_id;
        $documentsname=$request->documentsname; 
        
        $path1= public_path().'/adminupload/'.$_FILES['admindocument']['name'];    
        if(isset($_FILES['admindocument']['name'])!='')
        {
            if(move_uploaded_file($_FILES['admindocument']['tmp_name'], $path1)) 
            {
               $filesname1= $_FILES['admindocument']['name']; 
            }
            else
        {
            $filesname1=$_POST['admindocument_1']; 
        }
         
        }
        else
        {
            $filesname1=$_POST['admindocument_1']; 
        }
        
        
        $returnValue = DB::table('admintodocument')->where('id', '=', $id)
        ->update([ 
                   'admin_id' => $admin_id,
                   'documentsname' =>$documentsname,
                   'admindocument' =>$filesname1
            ]);     

         return redirect(route('adminworkstatus.index'))->with('success','Your Document Updated Successfully ');
    }

    public function getcounty(Request $request)
    {
        $data = taxstate::select('county','id','countycode')->where('state',$request->id)->take(1000)->get();
        return response()->json($data);  
    }
    
    public function getcountycode(Request $request)
    {
        $data = taxstate::select('countycode','id')->where('county',$request->id)->take(1000)->get();
        return response()->json($data);  
    }

    public function destroy(Request $request,$id)
    {
       $formation = DB::table('admin_formation')->where('id','=',$id)->delete();
       
                   $returnValue = DB::table('admins')->where('id', '=', 1)
            ->update([ 
                       'agent_expiredate' =>'Apr-01-2020',
                ]);     

        return redirect(route('adminworkstatus.index'))->with('success','Your Profile Deleted Successfully ');
    }
    
    public function newbankingdelete(Request $request,$id)
    {
        DB::table('new_banking')->where('id','=',$id)->delete();
        return redirect(route('adminworkstatus.index'))->with('success','New Bank Deleted Successfully ');
    }
    
    public function destroylicense(Request $request,$id)
    {
        DB::table('admin_license')->where('id','=',$id)->delete();
        return redirect(route('adminworkstatus.index'))->with('success','Your License Deleted Successfully ');
    }
    
    public function destroyprolic(Request $request,$id)
    {
       // echo "<pre>"; print_r($_REQUEST);die;
        DB::table('admin_pro_lic')->where('id','=',$id)->delete();
        return redirect(route('adminworkstatus.index'))->with('success','Your License Deleted Successfully ');
    }
    
    public function destroydocument(Request $request,$id)
    {
        DB::table('admintodocument')->where('id','=',$id)->delete();
        return redirect(route('adminworkstatus.index'))->with('success','Your Document Deleted Successfully ');
    }
    
    public function destroydocumenttitle(Request $request,$id)
    {
        DB::table('documents')->where('id','=',$id)->delete();
        return redirect(route('adminworkstatus.index'))->with('success','Your Document Title Deleted Successfully ');
    }
    
    public function destroyledger(Request $request,$id)
    {
        DB::table('admin_shareledger')->where('id','=',$id)->delete();
        return redirect(route('adminworkstatus.index'))->with('success','Share Ledger Deleted Successfully ');
    }
    
    
    public function getshareledager(Request $request)
    {
        $ledgerRow = DB::table('admin_shareledger')->where('id',$request->ledgerid)->first();
        return response()->json($ledgerRow);
    }
    
    public function getnewbank(Request $request)
    {
        $newbankRow = DB::table('new_banking')->where('id',$request->newbankid)->first();
        return response()->json($newbankRow);
    }
    
    
}
