<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\QuestionSection;
use DB;
class QuestionSectionController extends Controller
{ 
    public function __construct()
    {
      $this->middleware('auth:admin');
    }
  
  
    public function index(Request $request)
    {  
        $questionsection = QuestionSection::All();   
        return view('fac-Bhavesh-0554.questionsection.questionsection',compact(['questionsection']));
    }
    
    public function create()
    {
        return view('fac-Bhavesh-0554.questionsection.create');
    }
    
    
    public function store(Request $request)
    {
        $this->validate($request,[
            'question_type'=>'required',
            'status'=>'required'
        ]);
        $questions = new QuestionSection;
        $questions->question_type = $request->question_type;
          $questions->status = $request->status;
        $questions->save();
        return redirect(route('questionsection.index'));
    }

    public function edit($id)
    {   
        $questionsection= QuestionSection::where('id',$id)->first();    
        return View('fac-Bhavesh-0554.questionsection.edit',compact(['questionsection']));
    }
    
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'question_type'=>'required',
            'status'=>'required',
        ]);
        $questions = QuestionSection::find($id);
        $questions->question_type = $request->question_type;
        $questions->status = $request->status;
        $questions->update();
        return redirect(route('questionsection.index'));
    }
    
  
    public function destroy($id)
    {
        QuestionSection::where('id',$id)->delete();
        return redirect(route('questionsection.index'));
    }
   
}
