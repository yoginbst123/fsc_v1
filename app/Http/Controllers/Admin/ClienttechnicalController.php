<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\employee\Leave;
use App\Model\Employee;
use App\Model\Rules;
use App\Model\Clienttechnical;
use Auth;
use DB;
use App\User;
use Illuminate\Support\Facades\Input;
use App\employees\Fscemployee;
class ClienttechnicalController extends Controller
{
    public function __construct()
    {
    $this->middleware('auth:admin');
    }  
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $leave = Leave::All();
        $user_id  = Auth::user()->user_id;
        $employee = User::where('id',$user_id)->first();
        $rules = Clienttechnical::get();
        return view('fac-Bhavesh-0554/clienttechnical/clienttechnical',compact(['rules','employee']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { 
       $user_id  = Auth::user()->user_id;
       $employee = Employee::where('id',$user_id)->first();
       return view('fscemployee/clienttechnical/create',compact(['employee']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request,[
        'to' =>'required',
        'subject' =>'required',
                                
        ]);
        $user_id  = Auth::user()->user_id;
        if($request->hasFile('attachment'))
        {
         $filname = $request->attachment->getClientOriginalName();
         $request->attachment->move('public/attachment', $filname);
        }
        else
        {
            $filname ='';
        }
        $branch  = new Clienttechnical;
        $branch->to_supporter = $request->to;
        $branch->subject = $request->subject;
        $branch->details = $request->details;
        $branch->attachment =  $filname;
        $branch->emp_id = $user_id;
        $branch->date = $request->date;
        $branch->time = $request->time;
        $branch->day = $request->day;
        $branch->save();
        return redirect('fscemployee/clienttechnical')->with('success','Success fully add Technicalsupport');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
     
     $user_id  = Auth::user()->user_id;
     $employee = User::where('id',$user_id)->first(); 
     $homecontent = Clienttechnical::where('id',$id)->first(); 
     $tech = Employee::get(); 
     return view('fac-Bhavesh-0554.clienttechnical.edit',compact(['homecontent','employee','tech']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request,[]);
      $user_id  = Auth::user()->user_id;
      if($request->hasFile('attachment'))
        {
         $filname = $request->attachment->getClientOriginalName();
         $request->attachment->move('public/attachment', $filname);
        }
        else
        {
            $filname ='';
        }
      $branch = Clienttechnical::find($id);
      $branch->to_supporter = $request->to;
      $branch->subject = $request->subject;
      $branch->details = $request->details;
      $branch->answer = $request->answer;
      $branch->attachment =  $filname;
      //$branch->emp_id = $user_id; $branch->date = $request->date;
      $branch->time = $request->time;
      $branch->day = $request->day;
      $branch->update();
      return redirect('fac-Bhavesh-0554/clienttechnical/')->with('success','Success fully update Technicalsupport');
        
    }
  

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Technical::where('id',$id)->delete();
        return redirect(route('technicalsupport.index'));
        
    }
}
