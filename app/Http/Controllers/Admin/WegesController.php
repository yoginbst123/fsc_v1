<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Front\Commonregister;
use App\Model\BusinessBrand;

use App\Model\Category;
use App\Model\Business;
use App\Model\Employee;
use App\Model\client_professional;
use App\Model\client_shareholder;
use App\User;
use App\Model\Categorybusiness;
use App\Model\Contact_userinfo;
use App\Mail\Activationmail;
use App\Model\taxstate;
use App\Model\formationsetup;

use DB;
use Hash;
use Carbon;
use App\Submissionlogin\Submissionlogin;
use App\Model\Admin;
use Auth;
use App\Model\Price;
use App\Model\Currency;
use App\Model\Typeofser;
use App\Model\Period;
use App\Model\Taxtitle;
use App\Model\Language;
use App\Model\Ethnic;

use App\employees\Fscemployee;
use Illuminate\Support\Facades\Input;
use App\Model\Logo;
class WegesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    
    public function index(Request $request)
    {
       // print_r($_REQUEST);die;
        $state = DB::table('state')->where('countrycode','USA')->get();
        $employeesRow = DB::table('employees')->where('client_id',490)->get();
        //return view('fac-Bhavesh-0554.weges.weges');
        return view('fac-Bhavesh-0554.weges.weges',compact(['state','employeesRow']));
    }
     
     
    public function store(Request $request)
    {
        //print_r($_REQUEST);die;
        
        $l = 0;
        if(empty($request->wagesamounts))
        {
        }
        else
        {
            foreach($request->wagesamounts as $wagesamounts1)
            { 
                $client_id =$request->client_id;
                $employer_id = $request->employer_id[$l];
                //$wagesamounts = $request->wagesamounts[$l];
                $firstName = $request->firstName[$l];
                $lastName = $request->lastName[$l];
                $statename = $request->statename[$l];
                $wagestotalamounts = $request->wagestotalamounts;
                    
                $l++;
                DB::insert("insert into weges(`client_id`,`employer_id`,`firstName`,`lastName`,`wagesamounts`,`statename`,`wagestotalamounts`) 
                values('".$client_id."','".$employer_id."','".$firstName."','".$lastName."','".$wagesamounts1."','".$statename."','".$wagestotalamounts."')");
            }
        }
        
        return redirect()->back();
    }
    
    
    function edit($id)
    {
        $state = DB::table('state')->where('countrycode','USA')->get();
        $employeesRow = DB::table('employees')->where('client_id',$id)->get();
        //return view('fac-Bhavesh-0554.weges.weges');
        return view('fac-Bhavesh-0554.weges.edit',compact(['state','employeesRow']));

    }    
    
    public function getClientdata(Request $request)
    {
        $state = DB::table('state')->get();
        $documentRow = DB::table('employees')->where('client_id',$request->incomeid)->take(100)->get();
        //$documentRow = DB::table('employees')->where('client_id',$request->incomeid)->get();
        $output='';
        return view('fac-Bhavesh-0554.workrecord.edit',compact(['state','documentRow']));
    }
        
    public function destroyincomes(Request $request,$id)
    {   
        DB::table('incomes')->where('id','=',$id)->delete();
        return redirect()->back()->with('success','Your Record Deleted Successfully ');
    }    
        
    public function getTaxstatedata(Request $request)
    {
        $documentRow = DB::table('statetax')->where('statename',$request->statetaxval)->first();
        return response()->json($documentRow);  
    }    
        
    function addincome(Request $request,$id)
    {    
        // echo $id;
        // echo "<pre>";
        // print_r($_REQUEST);die;
      
        $wrkyear=$request->wrkyear;
        $wagetotal=$request->wagetotal;
        $taxintrest=$request->taxintrest;
        $qulifieddividends=$request->qulifieddividends;
        $pension=$request->pension;
        $socialsecurity=$request->socialsecurity;
        $capital=$request->capital;
        $otherincome=$request->otherincome;
        $totalamount=$request->totalamount;
    
        if(isset($request->wrkyear)!='')
        {
            $ids = DB::table('incomes')
                ->insert([
                    'client_id' =>$id,
                    'wrkyear' =>$wrkyear,
                    'wagetotal' =>$wagetotal,
                    'taxintrest' =>$taxintrest,
                    'qulifieddividends' =>$qulifieddividends,
                    'pension' =>$pension,
                    'socialsecurity' =>$socialsecurity,
                    'capital' =>$capital,
                    'otherincome' =>$otherincome,
                    'totalamount' =>$totalamount
                ]);
        }
    
        return redirect()->back()->with('success','Income Added Successfully');
       
    } 
    
    function updateincome(Request $request)
    {    
        // echo $id;
        // echo "<pre>";
        // print_r($_REQUEST);die;
        $incomeid=$request->incomeid;
        $wrkyear=$request->wrkyear;
        $wagetotal=$request->wagetotal;
        $taxintrest=$request->taxintrest;
        $qulifieddividends=$request->qulifieddividends;
        $pension=$request->pension;
        $socialsecurity=$request->socialsecurity;
        $capital=$request->capital;
        $otherincome=$request->otherincome;
        $totalamount=$request->totalamount;
    
        $ids = DB::table('incomes')->where('id',$incomeid)
            ->update([
                'wrkyear' =>$wrkyear,
                'wagetotal' =>$wagetotal,
                'taxintrest' =>$taxintrest,
                'qulifieddividends' =>$qulifieddividends,
                'pension' =>$pension,
                'socialsecurity' =>$socialsecurity,
                'capital' =>$capital,
                'otherincome' =>$otherincome,
                'totalamount' =>$totalamount
            ]);
    
        return redirect()->back()->with('success','Income Updated Successfully');
       
    } 
    
     
    public function update(Request $request, $id)
    {
        $update = ['annualfees' => $request->annualfees, 'processingfees' => $request->processingfees];
        Commonregister::where('id',$id)->update($update);
        return Redirect::to('workrecord')->with('success','Workrecord updated successfully');
    }
    
  
  
    
}
