<?php

namespace App\Http\Controllers\Admin;

use App\Model\taxstate;
use App\Model\countytosalestax;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
class TaxStateController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
   
    public function index()
    {
       $price = taxstate::All();
       return view('fac-Bhavesh-0554/taxstate/taxstate', compact(['price']));
    }

   
    public function create()
    {
        $county = DB::table('county_master')->get();
        //print_r($county);die;
        $position = taxstate::All();
        return view('fac-Bhavesh-0554/taxstate/create', compact(['position','county']));
    }
    
    public function getcountytax(Request $request)
    {
        $countyRow = DB::table('county_master')->where('state_code',$request->filecounty)->get();
        $aa='';
        foreach($countyRow as $countyname)
        {
            $aa.= "<option value='".$countyname->county_name."'>".$countyname->county_name."<option>";    
        }
        return response()->json($aa);
    }
    
    public function getcountycodes(Request $request)
    {
        $countyRow = DB::table('county_master')->select('county_code')->where('county_name',$request->filename)->first();
        //print_r($countyRow);die;
        //echo $bb=$countyRow->county_code;
        return response()->json($countyRow);
       
    }
    

    
    public function store(Request $request)
    {
        // echo "<pre>";
        // print_r($_POST);
        // die;
        $this->validate($request,[
            'countycode' =>'required', 
            'county' =>'required', 
            'state' =>'required',
            //'tax_note' =>'required',
            // 'month' =>'required',
            // 'year' =>'required',
            //'personal_form_number' =>'required',
            //'personal_form_name' =>'required',
            //'personal_due_date' =>'required',
            //'personal_website' =>'active_url',
            'personal_telephone' =>'required',
            //'personal_contact_name' =>'required',
            'personal_secondary_telephone' =>'required',
           // 'personal_email' =>'required',
            //'property_form_number' =>'required',
            //'property_form_name' =>'required',
            //'property_due_date' =>'required',
            //'property_website' =>'active_url',
            'property_telephone' =>'required',
            //'property_contact_name' =>'required',
            'property_secondary_telephone' =>'required',
            //'property_email' =>'required',
          
        ]);
        $position = new taxstate;
        $position->countycode= $request->countycode;
        $position->county= $request->county;
        $position->state= $request->state;
        $position->personal_filling_frequency= $request->personal_filling_frequency;
        $position->property_filling_frequency= $request->property_filling_frequency;
        
        $position->personal_form_number= $request->personal_form_number;
        $position->personal_form_name= $request->personal_form_name;
        $position->personal_due_date= $request->personal_due_date;
        $position->personal_website= $request->personal_website;
        $position->personal_telephone= $request->personal_telephone; 
        $position->personal_contact_name= $request->personal_contact_name;
        $position->personal_secondary_telephone= $request->personal_secondary_telephone;
        $position->personal_email = $request-> personal_email;
        $position->personal_file= $request->personal_file;
        $position->personal_tax_note= $request->personal_tax_note;
        $position->property_form_number= $request->property_form_number;
        $position->property_form_name= $request->property_form_name; 
        $position->property_due_date= $request->property_due_date;
        $position->property_website= $request->property_website;
        $position->property_telephone= $request->property_telephone;
        $position->property_contact_name= $request->property_contact_name;
        $position->property_secondary_telephone= $request->property_secondary_telephone; 
        $position->property_email= $request->property_email;
        $position->property_file= $request->property_file;
        $position->property_tax_note= $request->property_tax_note;
        
        $position->personal_tax_year= $request->personal_tax_year;
        $position->personal_payment_online= $request->personal_payment_online;
        $position->property_tax_year= $request->property_tax_year;
        $position->property_payment_online= $request->property_payment_online;
        
        $position->county_website= $request->county_website;
        $position->county_address= $request->county_address;
        $position->county_telephone= $request->county_telephone;
        $position->county_city= $request->county_city;
        $position->county_state= $request->county_state;
        $position->county_zip= $request->county_zip;
        $position->county_fax= $request->county_fax;
        
         $position->licence_form_number= $request->licence_form_number;
         $position->licence_form_name= $request->licence_form_name;
         $position->licence_tax_year= $request->licence_tax_year;
         $position->licence_filling_frequency= $request->licence_filling_frequency;
         $position->licence_due_date= $request->licence_due_date;
         $position->licence_telephone= $request->licence_telephone;
         $position->licence_contact_name= $request->licence_contact_name;
         $position->licence_secondary_telephone= $request->licence_secondary_telephone;
         $position->licence_email= $request->licence_email;
         $position->licence_file= $request->licence_file;
         $position->licence_website= $request->licence_website;
         $position->licence_payment_online= $request->licence_payment_online;
         $position->licence_note= $request->licence_note;
         $position->county_authority_name= $request->county_authority_name;
        $position->save();
        $id=$position->id;
        
        $county_tax_month = $request->county_tax_month;
        $county_tax_year = $request->county_tax_year;
        $county_tax_personal_rate = $request->county_tax_personal_rate;
        $county_lost = $request->county_lost;
        $county_educational = $request->county_educational;
        $county_splost = $request->county_splost;
        $county_tsplost = $request->county_tsplost;
        $county_host = $request->county_host;
        $county_marta = $request->county_marta;
        $county_other = $request->county_other;
        $county_tsplost2 = $request->county_tsplost2;
        $county_filtontf = $request->county_filtontf;
        $county_atlantata = $request->county_atlantata;
        
        $countyss = count(array_filter($request->county_tax_month));
        for($i=0; $i < $countyss; $i++)
        {
            if($county_tax_month!='' || $county_tax_month!=null || $county_tax_month!='0')
			{
                $models = new countytosalestax;
                $models->countysalesid =  $id;
                $models->county_tax_month = $county_tax_month[$i] ? $county_tax_month[$i] : '0';
                $models->county_tax_month=$county_tax_month[$i] ? $county_tax_month[$i] : '0';
                $models->county_tax_year=$county_tax_year[$i] ? $county_tax_year[$i] : '0';
                $models->county_tax_personal_rate=$county_tax_personal_rate[$i] ? $county_tax_personal_rate[$i] : '0';
                $models->county_lost=isset($county_lost[$i]) ? $county_lost[$i] : '0';
                $models->county_educational=isset($county_educational[$i]) ? $county_educational[$i] : '0';
                $models->county_splost=isset($county_splost[$i]) ? $county_splost[$i] : '0';
                $models->county_tsplost=isset($county_tsplost[$i]) ? $county_tsplost[$i] : '0';
                $models->county_host=isset($county_host[$i]) ? $county_host[$i] : '0';
                $models->county_marta=isset($county_marta[$i]) ? $county_marta[$i] : '0';
                $models->county_other=isset($county_other[$i]) ? $county_other[$i] : '0';
                $models->county_tsplost2=isset($county_tsplost2[$i]) ? $county_tsplost2[$i] : '0';
                $models->county_filtontf=isset($county_filtontf[$i]) ? $county_filtontf[$i] : '0';
                $models->county_atlantata=isset($county_atlantata[$i]) ? $county_atlantata[$i] : '0';
                $models->save();
                
                // if(isset($county_lost[$i])!='' || isset($county_lost[$i])!=null || isset($county_lost[$i])!='0')
                // {
                //       $models->county_lost=isset($county_lost[$i]);
                // }
                // else
                // {
                //     $models->county_lost= '0';
                // }
                
                // if(isset($county_educational[$i])!='' || isset($county_educational[$i])!=null || isset($county_educational[$i])!='0')
                // {
                //       $models->county_lost=isset($county_educational[$i]);
                // }
                // else
                // {
                //     $models->county_educational= '0';
                // }
                
                // if(isset($county_splost[$i])!='' || isset($county_splost[$i])!=null || isset($county_splost[$i])!='0')
                // {
                //       $models->county_splost=isset($county_splost[$i]);
                // }
                // else
                // {
                //     $models->county_splost= '0';
                // }
                
                // if(isset($county_splost[$i])!='' || isset($county_splost[$i])!=null || isset($county_splost[$i])!='0')
                // {
                //       $models->county_splost=isset($county_splost[$i]);
                // }
                // else
                // {
                //     $models->county_splost= '0';
                // }

                // if(isset($county_host[$i])!='' || isset($county_host[$i])!=null || isset($county_host[$i])!='0')
                // {
                //       $models->county_host=isset($county_host[$i]);
                // }
                // else
                // {
                //     $models->county_host= '0';
                // }
                
                // if(isset($county_marta[$i])!='' || isset($county_marta[$i])!=null || isset($county_marta[$i])!='0')
                // {
                //       $models->county_marta=isset($county_marta[$i]);
                // }
                // else
                // {
                //     $models->county_marta= '0';
                // }
                
                // if(isset($county_other[$i])!='' || isset($county_other[$i])!=null || isset($county_other[$i])!='0')
                // {
                //       $models->county_other=isset($county_other[$i]);
                // }
                // else
                // {
                //     $models->county_other= '0';
                // }
                
                // if(isset($county_tsplost2[$i])!='' || isset($county_tsplost2[$i])!=null || isset($county_tsplost2[$i])!='0')
                // {
                //       $models->county_tsplost2=isset($county_tsplost2[$i]);
                // }
                // else
                // {
                //     $models->county_tsplost2= '0';
                // }

                // if(isset($county_filtontf[$i])!='' || isset($county_filtontf[$i])!=null || isset($county_filtontf[$i])!='0')
                // {
                //       $models->county_filtontf=isset($county_filtontf[$i]);
                // }
                // else
                // {
                //     $models->county_filtontf= '0';
                // }
                
                
                // if(isset($county_atlantata[$i])!='' || isset($county_atlantata[$i])!=null || isset($county_atlantata[$i])!='0')
                // {
                //       $models->county_atlantata=isset($county_atlantata[$i]);
                // }
                // else
                // {
                //     $models->county_atlantata= '0';
                // }
                
               
			}
        }
        
        
        // $mnt=0;
        // foreach($request->county_tax_month as $county_tax_month)
        // {
        //     $county_tax_months = $county_tax_month;
        //     $county_tax_years =$request->county_tax_year[$mnt];
        //     $county_tax_personal_rates =$request->county_tax_personal_rate[$mnt];
            
        //     $county_losts= $request->county_lost[$mnt]; 
        //     $county_educationals= $request->county_educational[$mnt]; 
        //     $county_splosts= $request->county_splost[$mnt];
        //     $county_tsplosts= $request->county_tsplost[$mnt];
        //     $county_hosts= $request->county_host[$mnt];
        //     $county_martas	= $request->county_marta[$mnt];
        //     $county_others	= $request->county_other[$mnt];
        //     $county_tsplost2s= $request->county_tsplost2[$mnt]; 
        //     $county_filtontfs= $request->county_filtontf[$mnt]; 
        //     $county_atlantatas= $request->county_atlantata[$mnt];
            
        //     $usid1 =$id;
        //     $mnt++;
        //      $insert2 = DB::insert("insert into countytosalestax(`countysalesid`,`county_tax_month`,`county_tax_year`,`county_tax_personal_rate`,`county_lost`,`county_educational`,`county_splost`,`county_tsplost`,`county_host`,`county_marta`,`county_other`,`county_tsplost2`,`county_filtontf`,`county_atlantata`) 
        //      values('".$usid1."','".$county_tax_months."','".$county_tax_years."','".$county_tax_personal_rates."','".$county_losts."','".$county_educationals."','".$county_splosts."','".$county_tsplosts."','".$county_hosts."','".$county_martas."','".$county_others."','".$county_tsplost2s."','".$county_filtontfs."','".$county_atlantatas."')");
        // }
        return redirect('fac-Bhavesh-0554/taxstate')->with('success','Success fully add County');
    }

  
 
    public function edit($id)
    {  
        $countys = DB::table('county_master')->get();
        $countyRow = DB::table('countytosalestax')->where('countysalesid','=',$id)->get();
        $wordCount = $countyRow->count();
       // echo "<pre>";
        //print_r($wordCount);die;
        $county = DB::table('countytosalestax')->where('countysalesid','=',$id)->orderBy('id', 'asc')->get();
        $state = taxstate::where('id',$id)->first();
        
        return View('fac-Bhavesh-0554.taxstate.edit',compact('state','county','wordCount','countys'));
    }
    
   
    public function update(Request $request, $id)
    {
      // echo "<pre>";
        // print_r($_POST);
        // die;
      $this->validate($request,[
            'countycode' =>'required', 
            'county' =>'required', 
            'state' =>'required',
                    //'tax_note' =>'required',
                    // 'month' =>'required',
                    // 'year' =>'required',
                    //'personal_form_number' =>'required',
                    //'personal_form_name' =>'required',
                    //'personal_due_date' =>'required',
                    //'personal_website' =>'required',
            //'personal_telephone' =>'required',
                    //'personal_contact_name' =>'required',
            //'personal_secondary_telephone' =>'required',
            //'personal_email' =>'required',
                    //'property_form_number' =>'required',
                    //'property_form_name' =>'required',
                    //'property_due_date' =>'required',
                    //'property_website' =>'required',  
            //'property_telephone' =>'required',
                    //'property_contact_name' =>'required',
            //'property_secondary_telephone' =>'required',
            //'property_email' =>'required',
          
        ]);
        
        $position =taxstate::find($id);
        $position->countycode= $request->countycode;
        $position->county= $request->county;
        $position->state= $request->state;
        $position->personal_filling_frequency= $request->personal_filling_frequency;
        $position->property_filling_frequency= $request->property_filling_frequency;
        
        $position->personal_form_number= $request->personal_form_number;
        $position->personal_form_name= $request->personal_form_name;
        $position->personal_due_date= $request->personal_due_date;
        $position->personal_website= $request->personal_website;
        $position->personal_telephone= $request->personal_telephone; 
        $position->personal_contact_name= $request->personal_contact_name;
        $position->personal_secondary_telephone= $request->personal_secondary_telephone;
        $position->personal_email = $request-> personal_email ;
        $position->personal_file= $request->personal_file;
        $position->personal_tax_note= $request->personal_tax_note;
        $position->property_form_number= $request->property_form_number;
        $position->property_form_name= $request->property_form_name; 
        $position->property_due_date= $request->property_due_date;
        $position->property_website= $request->property_website;
        $position->property_telephone= $request->property_telephone;
        $position->property_contact_name= $request->property_contact_name;
        $position->property_secondary_telephone= $request->property_secondary_telephone; 
        $position->property_email= $request->property_email; 
        $position->property_file= $request->property_file;
        $position->property_tax_note= $request->property_tax_note;
        
        $position->personal_tax_year= $request->personal_tax_year;
        $position->personal_payment_online= $request->personal_payment_online;
        $position->property_tax_year= $request->property_tax_year;
        $position->property_payment_online= $request->property_payment_online;
        
        $position->county_website= $request->county_website;
        $position->county_address= $request->county_address;
        $position->county_telephone= $request->county_telephone;
        $position->county_city= $request->county_city;
        $position->county_state= $request->county_state;
        $position->county_zip= $request->county_zip;
        $position->county_fax= $request->county_fax;
        
         $position->licence_form_number= $request->licence_form_number;
         $position->licence_form_name= $request->licence_form_name;
         $position->licence_tax_year= $request->licence_tax_year;
         $position->licence_filling_frequency= $request->licence_filling_frequency;
         $position->licence_due_date= $request->licence_due_date;
         $position->licence_telephone= $request->licence_telephone;
         $position->licence_contact_name= $request->licence_contact_name;
         $position->licence_secondary_telephone= $request->licence_secondary_telephone;
         $position->licence_email= $request->licence_email;
         $position->licence_file= $request->licence_file;
         $position->licence_website= $request->licence_website;
         $position->licence_payment_online= $request->licence_payment_online;
         $position->licence_note= $request->licence_note;
         $position->county_authority_name= $request->county_authority_name;
        
         $position->update();
         $id=$position->id;
         
        $county_tax_month = $request->county_tax_month;
        $county_tax_year = $request->county_tax_year;
        $county_tax_personal_rate = $request->county_tax_personal_rate;
        $county_lost = $request->county_lost;
        $county_educational = $request->county_educational;
        $county_splost = $request->county_splost;
        $county_tsplost = $request->county_tsplost;
        $county_host = $request->county_host;
        $county_marta = $request->county_marta;
        $county_other = $request->county_other;
        $county_tsplost2 = $request->county_tsplost2;
        $county_filtontf = $request->county_filtontf;
        $county_atlantata = $request->county_atlantata;
        $sid = $request->sid;
            
        $countyss = count(array_filter($request->county_tax_month));
        for($i=0; $i < $countyss; $i++)
        {
            if($county_tax_month!='' || $county_tax_month!=null || $county_tax_month!='0')
			{
			    if(isset($sid[$i])!='' || isset($sid[$i])!=null || isset($sid[$i])!='0')
			    {
			        $id1 = $sid[$i];    
			    }
			    else
			    {
			       
			    }
			    
                $models = new countytosalestax;
                $models->countysalesid =  $id;
                $models->county_tax_month = $county_tax_month[$i] ? $county_tax_month[$i] : '0';
                $models->county_tax_year=$county_tax_year[$i] ? $county_tax_year[$i] : '0';
                $models->county_tax_personal_rate=$county_tax_personal_rate[$i] ? $county_tax_personal_rate[$i] : '0';
                $models->county_tax_personal_rate=$county_tax_personal_rate[$i] ? $county_tax_personal_rate[$i] : '0';
                $models->county_lost=isset($county_lost[$i]) ? $county_lost[$i] : '0';
                $models->county_educational=isset($county_educational[$i]) ? $county_educational[$i] : '0';
                $models->county_splost=isset($county_splost[$i]) ? $county_splost[$i] : '0';
                $models->county_tsplost=isset($county_tsplost[$i]) ? $county_tsplost[$i] : '0';
                $models->county_host=isset($county_host[$i]) ? $county_host[$i] : '0';
                $models->county_marta=isset($county_marta[$i]) ? $county_marta[$i] : '0';
                $models->county_other=isset($county_other[$i]) ? $county_other[$i] : '0';
                $models->county_tsplost2=isset($county_tsplost2[$i]) ? $county_tsplost2[$i] : '0';
                $models->county_filtontf=isset($county_filtontf[$i]) ? $county_filtontf[$i] : '0';
                $models->county_atlantata=isset($county_atlantata[$i]) ? $county_atlantata[$i] : '0';
                
                $models->save();
                if(empty($sid))
                {
                    
                }
                else
                {
                    $affectedRows1 = countytosalestax::where('id', '=',$id1)->delete();
                }     
			}
        }
         
    //     if (is_null($request->county_tax_month))
    //     {
            
    //     }
    //     else
    //     {
    //          $mnt=0;
    //          foreach($request->county_tax_month as $county_tax_month)
    //          {
                
    //                 //$sid =$request->sid[$mnt];
    //                 $countysalesid =$request->countysalesid;
    //                 $county_tax_months = $county_tax_month;
    //                 $county_tax_years =$request->county_tax_year[$mnt];
    //                 $county_tax_personal_rates =$request->county_tax_personal_rate[$mnt];
                    
    //                 $county_losts= $request->county_lost[$mnt]; 
    //                 $county_educationals= $request->county_educational[$mnt]; 
    //                 $county_splosts= $request->county_splost[$mnt];
    //                 $county_tsplosts= $request->county_tsplost[$mnt];
    //                 $county_hosts= $request->county_host[$mnt];
    //                 $county_martas	= $request->county_marta[$mnt];
    //                 $county_others	= $request->county_other[$mnt];
    //                 $county_tsplost2s= $request->county_tsplost2[$mnt]; 
    //                 $county_filtontfs= $request->county_filtontf[$mnt]; 
    //                 $county_atlantatas= $request->county_atlantata[$mnt];
                    
    //                 $usid1 =$id;
    //                 $mnt++;
    //                 // DB::table('countytosalestax')->where('countysalesid','=',$countysalesid)->where('id','=',$sid)->delete();
    //                 // DB::table('countytosalestax')->where('id','=',$sid)->delete();
    //                 $insert2 = DB::insert("insert into countytosalestax(`countysalesid`,`county_tax_month`,`county_tax_year`,`county_tax_personal_rate`,`county_lost`,`county_educational`,`county_splost`,`county_tsplost`,`county_host`,`county_marta`,`county_other`,`county_tsplost2`,`county_filtontf`,`county_atlantata`) 
    //                 values('".$usid1."','".$county_tax_months."','".$county_tax_years."','".$county_tax_personal_rates."','".$county_losts."','".$county_educationals."','".$county_splosts."','".$county_tsplosts."','".$county_hosts."','".$county_martas."','".$county_others."','".$county_tsplost2s."','".$county_filtontfs."','".$county_atlantatas."')");
    // 		}
    //     }
        
        
           // EXIT;
        // foreach($request->county_tax_month as $county_tax_month)
        // {
        //     $sid =$request->sid;
        //     $county_tax_months = $county_tax_month;
        //     $county_tax_years =$request->county_tax_year[$mnt];
        //     $county_tax_personal_rates =$request->county_tax_personal_rate[$mnt];
        //     $usid1 =$id;
        //     $mnt++;
        //     DB::table('countytosalestax')->where('id','=',$sid)->delete();
        //      $insert2 = DB::insert("insert into countytosalestax(`countysalesid`,`county_tax_month`,`county_tax_year`,`county_tax_personal_rate`) values('".$usid1."','".$county_tax_months."','".$county_tax_years."','".$county_tax_personal_rates."')");
        // }
        return redirect('fac-Bhavesh-0554/taxstate')->with('success','Success fully update county');
    }

   
    public function destroy($id)
    {
        taxstate::where('id',$id)->delete();
          return redirect('fac-Bhavesh-0554/taxstate')->with('success','Success fully deleted county');
    }
    
    public function countydelete($id)
    {
        DB::table('countytosalestax')->where('id','=',$id)->delete();
        return redirect('fac-Bhavesh-0554/taxstate')->with('success','Success fully deleted county');
    }
    
    
    // public function taxstatedelete($id,$ids)
    // {
    //     echo $id;
    //     echo $ids;die;
       
    //     DB::table('countytosalestax')->where('id','=',$id)->delete();
    //     return redirect('fac-Bhavesh-0554/taxstate/');
    //     //return View('fac-Bhavesh-0554.taxstate.edit',compact('state','county'));
    //     //return redirect(route('taxstate.index'));
    // }
    
   
}
