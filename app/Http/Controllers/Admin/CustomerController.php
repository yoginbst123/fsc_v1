<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Front\Commonregister;
use App\Model\BusinessBrand;

use App\Model\Category;
use App\Model\Business;
use App\Model\Employee;
use App\Model\client_professional;
use App\Model\client_shareholder;
use App\User;
use App\Model\Categorybusiness;
use App\Model\Contact_userinfo;
use App\Mail\Activationmail;
use App\Model\taxstate;
use DB;
use Hash;
use Carbon;
use App\Submissionlogin\Submissionlogin;
use App\Model\Admin;
use Auth;
use App\Model\Price;
use App\Model\Currency;
use App\Model\Typeofser;
use App\Model\Period;
use App\Model\Taxtitle;
use App\Model\Language;
use App\Model\Ethnic;

use App\employees\Fscemployee;
use Illuminate\Support\Facades\Input;
use App\Model\Logo;

class CustomerController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $name = $request->input('user_type');

        if (isset($name)) {


            $common = DB::table('commonregisters')->select('commonregisters.firstname', 'commonregisters.middlename', 'commonregisters.lastname', 'commonregisters.id as cid', 'commonregisters.paymentnote as paymentnote', 'commonregisters.register_id1 as register_id1', 'commonregisters.register_id as register_id', 'commonregisters.billingtoo as billingtoo', 'commonregisters.paymentmode as paymentmode', 'commonregisters.creationdate as creationdate', 'commonregisters.etelephone1 as etelephone1', 'commonregisters.filename as filename', 'commonregisters.created_at as created_at', 'commonregisters.mailing_address1 as mailing_address1', 'commonregisters.bussiness_zip as bussiness_zip', 'commonregisters.newclient as newclient', 'commonregisters.due_date as due_date', 'commonregisters.department as department', 'commonregisters.type_of_activity as type_of_activity', 'commonregisters.county_no as county_no', 'commonregisters.county_name as county_name', 'commonregisters.level as level', 'commonregisters.setup_state as setup_state', 'commonregisters.business_state as business_state', 'commonregisters.business_city as business_city', 'commonregisters.business_country as business_country', 'commonregisters.business_address as business_address', 'commonregisters.business_store_name as business_store_name', 'commonregisters.mailing_address as mailing_address', 'commonregisters.legalname as legalname', 'commonregisters.dbaname as dbaname', 'commonregisters.mailing_city as mailing_city', 'commonregisters.mailing_state as mailing_state', 'commonregisters.mailing_zip as mailing_zip', 'commonregisters.user_type as user_type', 'commonregisters.user_type as user_type', 'commonregisters.status as status', 'commonregisters.company_name as company_name', 'commonregisters.business_name as business_name', 'commonregisters.first_name as first_name', 'commonregisters.middle_name as middle_name', 'commonregisters.last_name as last_name', 'commonregisters.email as email', 'commonregisters.address as address', 'commonregisters.address1 as address1', 'commonregisters.city as city', 'commonregisters.stateId as stateId', 'commonregisters.zip as zip', 'commonregisters.countryId as countryId', 'commonregisters.mobile_no as mobile_no', 'commonregisters.business_no as business_no', 'commonregisters.business_fax as business_fax', 'commonregisters.website as website', 'commonregisters.user_type as user_type', 'commonregisters.business_id as business_id', 'commonregisters.business_cat_id as business_cat_id', 'commonregisters.business_brand_id as business_brand_id', 'commonregisters.business_brand_category_id as business_brand_category_id', 'businesses.bussiness_name as bussiness_name', 'categories.business_cat_name as business_cat_name', 'business_brands.business_brand_name as business_brand_name', 'categorybusinesses.business_brand_category_name as business_brand_category_name', 'commonregisters.recstatus')
                ->leftJoin('categories', function ($join) {
                    $join->on('commonregisters.business_cat_id', '=', 'categories.id');
                })
                ->leftJoin('businesses', function ($join) {
                    $join->on('commonregisters.business_id', '=', 'businesses.id');
                })
                ->leftJoin('business_brands', function ($join) {
                    $join->on('commonregisters.business_brand_id', '=', 'business_brands.id');
                })
                ->leftJoin('categorybusinesses', function ($join) {
                    $join->on('commonregisters.business_brand_category_id', '=', 'categorybusinesses.id');
                })
                ->where('commonregisters.user_type', '=', $name)->where('commonregisters.filename', '!=', "")->orderBy('filename', 'asc')->get();
        } else {
            $common = DB::table('commonregisters')->select('commonregisters.firstname', 'commonregisters.middlename', 'commonregisters.lastname', 'commonregisters.id as cid', 'commonregisters.billingtoo as billingtoo', 'commonregisters.register_id1 as register_id1', 'commonregisters.paymentnote as paymentnote', 'commonregisters.register_id as register_id', 'commonregisters.paymentmode as paymentmode', 'commonregisters.etelephone1 as etelephone1', 'commonregisters.creationdate as creationdate', 'commonregisters.created_at as created_at', 'commonregisters.filename as filename', 'commonregisters.mailing_address1 as mailing_address1', 'commonregisters.newclient as newclient', 'commonregisters.bussiness_zip as bussiness_zip', 'commonregisters.due_date as due_date', 'commonregisters.department as department', 'commonregisters.type_of_activity as type_of_activity', 'commonregisters.county_no as county_no', 'commonregisters.county_name as county_name', 'commonregisters.level as level', 'commonregisters.setup_state as setup_state', 'commonregisters.business_state as business_state', 'commonregisters.business_city as business_city', 'commonregisters.business_country as business_country', 'commonregisters.business_address as business_address', 'commonregisters.business_store_name as business_store_name', 'commonregisters.mailing_address as mailing_address', 'commonregisters.legalname as legalname', 'commonregisters.dbaname as dbaname', 'commonregisters.mailing_city as mailing_city', 'commonregisters.mailing_state as mailing_state', 'commonregisters.mailing_zip as mailing_zip', 'commonregisters.user_type as user_type', 'commonregisters.user_type as user_type', 'commonregisters.status as status', 'commonregisters.company_name as company_name', 'commonregisters.business_name as business_name', 'commonregisters.first_name as first_name', 'commonregisters.middle_name as middle_name', 'commonregisters.last_name as last_name', 'commonregisters.email as email', 'commonregisters.address as address', 'commonregisters.address1 as address1', 'commonregisters.city as city', 'commonregisters.stateId as stateId', 'commonregisters.zip as zip', 'commonregisters.countryId as countryId', 'commonregisters.mobile_no as mobile_no', 'commonregisters.business_no as business_no', 'commonregisters.business_fax as business_fax', 'commonregisters.website as website', 'commonregisters.user_type as user_type', 'commonregisters.business_id as business_id', 'commonregisters.business_cat_id as business_cat_id', 'commonregisters.business_brand_id as business_brand_id', 'commonregisters.business_brand_category_id as business_brand_category_id', 'businesses.bussiness_name as bussiness_name', 'categories.business_cat_name as business_cat_name', 'business_brands.business_brand_name as business_brand_name', 'categorybusinesses.business_brand_category_name as business_brand_category_name', 'commonregisters.recstatus')
                ->leftJoin('categories', function ($join) {
                    $join->on('commonregisters.business_cat_id', '=', 'categories.id');
                })
                ->leftJoin('businesses', function ($join) {
                    $join->on('commonregisters.business_id', '=', 'businesses.id');
                })
                ->leftJoin('business_brands', function ($join) {
                    $join->on('commonregisters.business_brand_id', '=', 'business_brands.id');
                })
                ->leftJoin('categorybusinesses', function ($join) {
                    $join->on('commonregisters.business_brand_category_id', '=', 'categorybusinesses.id');
                })->where('commonregisters.filename', '!=', "")
                ->orderBy('filename', 'asc')->get();
        }

        //$countrecstatus = Commonregister::where('recstatus','=','1')->count();

        $count = Commonregister::where('status', '=', 'Active')->count();
        $countb = Commonregister::where('business_id', '!=', '6')->where('status', '=', 'Active')->count();
        $countp = Commonregister::where('business_id', '=', '6')->where('status', '=', 'Active')->count();

        $logo = Logo::where('id', '=', 1)->first();
        return view('fac-Bhavesh-0554.customer.customer', compact(['common', 'logo', 'count', 'countb', 'countp']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        $category = Category::orderBy('business_cat_name', 'asc')->get();
        $business = Business::orderBy('bussiness_name', 'asc')->get();
        $businessbrand = BusinessBrand::orderBy('business_brand_name', 'asc')->get();
        $cb = Categorybusiness::orderBy('business_brand_category_name', 'asc')->get();
        $position = Price::All();
        $currency = Currency::All();
        $typeofser = Typeofser::orderBy('typeofservice', 'asc')->get();
        $period = Period::All();
        $taxtitle = Taxtitle::All();
        return view('fac-Bhavesh-0554/customer/create', compact(['business', 'category', 'cb', 'businessbrand', 'currency', 'typeofser', 'period', 'taxtitle']));
    }


    public function getfilename(Request $request)
    {
        $countyRow = Commonregister::select('id', 'filename')->where('filename', $request->filename)->get();
        $wordCount = $countyRow->count();
        return response()->json($wordCount);
    }


    public function getEEResponsibility(Request $request)
    {
        //echo 'ghjhghg';
        exit('uuuu');
        //exit('1111111111111');
        //  $countyRow = Commonregister::select('id','filename')->where('filename',$request->filename)->get();
        //$wordCount = $countyRow->count();
        //return response()->json($wordCount);
    }

    public function getConversationdata(Request $request)
    {
        $documentRow = DB::table('conversation_sheet')->where('id', $request->documentid)->first();
        return response()->json($documentRow);
    }

    public function store(Request $request)
    {
        //echo $resStr = strtoupper($request->filename3);
        //ECHO "<PRE>";print_r($_POST);die;

        $this->validate($request, []);

        if ($request->filename1 != '' && $request->filename != '' && $request->filename3 != '') {
            $resStr = strtoupper($request->filename3);
            $files = $request->filename1 . '-' . $request->filename . '-' . $resStr;
        } else {
            $files = $request->filename1 . '-' . $request->filename;
        }

//   $files = $request->filename1.'-'.$request->filename.'-'.$resStr;
        $myString = $request->filename;
        $myString = str_replace("-", "", $myString);
        $businessbrand = new Commonregister;
        $businessbrand->register_id = $myString;
        $businessbrand->filename = $files;
        $businessbrand->recstatus = '1';

        $businessbrand->status = $request->status;
        $businessbrand->business_id = $request->business_id;
        $businessbrand->business_name = $request->business_name;
        $businessbrand->business_cat_id = $request->business_catagory_name;
        $businessbrand->company_name1 = $request->company_name1;
        $businessbrand->company_name = $request->company_name;
        $businessbrand->service_period = $request->serviceperiod;
        $businessbrand->first_name = $request->firstname;
        $businessbrand->middle_name = $request->middlename;
        $businessbrand->last_name = $request->lastname;
        $businessbrand->legalname = $request->legalname;
        $businessbrand->dbaname = $request->dbaname;
        $businessbrand->address = $request->address;
        $businessbrand->address1 = $request->address1;
        $businessbrand->city = $request->city;
        $businessbrand->countryId = $request->countryId;
        $businessbrand->stateId = $request->stateId;
        $businessbrand->zip = $request->zip;
        $businessbrand->mailing_address = $request->second_address;
        $businessbrand->mailing_address1 = $request->second_address1;
        $businessbrand->mailing_city = $request->second_city;
        $businessbrand->mailing_state = $request->second_state;
        $businessbrand->mailing_zip = $request->second_zip;
        $businessbrand->email = $request->email;
        $businessbrand->mobile_no = $request->mobile_no;
        $businessbrand->business_no = $request->business_no;
        $businessbrand->motelephoneile1 = $request->motelephoneile1;
        $businessbrand->businesstype = $request->businesstype;
        $businessbrand->telephoneNo2Type = $request->telephoneNo2Type;
        $businessbrand->businessext = $request->businessext;
        $businessbrand->business_fax = $request->business_fax;
        $businessbrand->website = $request->website;
        $businessbrand->business_brand_id = $request->business_brand_name;
        $businessbrand->business_brand_category_id = $request->business_brand_category_name;
        $businessbrand->business_store_name = $request->business_store_name;
        $businessbrand->business_address = $request->business_address;
        $businessbrand->business_city = $request->business_city;
        $businessbrand->business_country = $request->business_country;
        $businessbrand->business_state = $request->business_state;
        $businessbrand->bussiness_zip = $request->bussiness_zip;
        $businessbrand->level = $request->level;
        $businessbrand->setup_state = $request->setup_state;
        $businessbrand->county_name = $request->county_name;
        $businessbrand->county_no = $request->county_no;
        $businessbrand->type_of_activity = $request->type_of_activity;
        $businessbrand->department = $request->department;
        $businessbrand->due_date = $request->due_date;
        $businessbrand->user_type = $request->user_type;
        $businessbrand->minss = $request->minss;
        $businessbrand->firstname = $request->firstname;
        $businessbrand->middlename = $request->middlename;
        $businessbrand->lastname = $request->lastname;
        $businessbrand->contact_address1 = $request->contact_address1;
        $businessbrand->contact_address2 = $request->contact_address2;
        $businessbrand->city_1 = $request->city_1;
        $businessbrand->state_1 = $request->state_1;
        $businessbrand->zip_1 = $request->zip_1;
        $businessbrand->etelephone1 = $request->etelephone1;
        //  $businessbrand->mobile_1 = $request->mobile_1;
        $businessbrand->eteletype1 = $request->mobiletype_1;
        $businessbrand->eext1 = $request->ext2_1;
        //   $businessbrand->etelephone2 = $request->mobile_2;
        $businessbrand->mobile_2 = $request->mobile_2;
        $businessbrand->eteletype2 = $request->mobiletype_2;
        $businessbrand->eext2 = $request->ext2_2;
        $businessbrand->email_1 = $request->email_1;
        $businessbrand->contact_fax_1 = $request->contact_fax_1;
        $businessbrand->nametype = $request->nametype;
        $businessbrand->billingtoo = $request->billingtoo;
        $businessbrand->accounting_period = $request->accounting_period;

        $businessbrand->faxbli3 = $request->faxbli3;
        $businessbrand->guardian = isset($request->guardian) ? $request->guardian : "";
        $businessbrand->guardian2 = isset($request->guardian2) ? $request->guardian2 : "";
        $businessbrand->faxbli3_g = $request->faxbli3_g;

        $businessbrand->faxbli1 = $request->faxbli2;
        $businessbrand->emailbli1 = $request->emailbli2;

        $businessbrand->ac_service_month = $request->ac_service_month;
        $businessbrand->ac_service_year = $request->ac_service_year;


        if ($request->business_id == '6') {
            $businessbrand->CL = $request->CL;
            $businessbrand->other_maritial_status1 = $request->other_maritial_status;

            $businessbrand->personalname = $request->personalname;
            $businessbrand->maritial_first_name = $request->maritial_first_name;
            $businessbrand->maritial_middle_name = $request->maritial_middle_name;
            $businessbrand->maritial_last_name = $request->maritial_last_name;
            $businessbrand->maritial_spouse = $request->maritial_spouse;
            $businessbrand->maritial_spouse1 = $request->maritial_spouse1;
            $businessbrand->telephoneNo2Type = $request->telephoneNo2Type;
            $businessbrand->motelephoneile1 = $request->motelephoneile1;
            $businessbrand->ext2 = $request->ext2;
        }
        $businessbrand->newclient = 2;
        $businessbrand->save();
        $em = $request->email;
        $common = Commonregister::where('email', $em)->first();
        $id = $common->id;
        $sign = $request->ckq;


        $t = 0;
        // echo "<pre>";print_r($_POST);exit;
        $taxservices1 = isset($_POST['taxation_service']) ? $_POST['taxation_service'] : "";

        if (empty($taxservices1)) {

        } else {
            $taxcurrency = isset($_POST['currency']) ? $_POST['currency'] : "";
            $taxpricetype = isset($_POST['pricetype']) ? $_POST['pricetype'] : "";
            $taxserviceperiod = isset($_POST['serviceperiod']) ? $_POST['serviceperiod'] : "";

            $taxusers = DB::table('client_to_taxation')->where('clientid', '=', $id)->delete();
            
            //$taxation_service_count=$_POST['taxation_service'];
            
            //echo $taxation_service_count.'<br/>';
            
            
            //for($i=0;$i<$taxation_service_count;$i++){
            //    echo $_POST['taxation_service'][$i].'<br/>';
            //}
            //exit();
            
            //Editing by Ankit patel
            foreach ($_POST['taxation_service'] as $taxservice) {

                $taxservice1 = $taxservice;
                $taxserviceperiod1 = isset($_POST['taxation_service_period'][$t]) ? $_POST['taxation_service_period'][$t] : "";

                $taxemp = isset($_POST['clemployee'][$t]) ? $_POST['clemployee'][$t] : "";
                $taxac = isset($_POST['tax_acc_checkby'][$t]) ? $_POST['tax_acc_checkby'][$t] : "";
                $taxnte = isset($_POST['taxationnote'][$t]) ? $_POST['taxationnote'][$t] : "";
                $taxlocked = isset($_POST['taxlocked2'][$t]) ? $_POST['taxlocked2'][$t] : "";

                $taxyears = isset($_POST['taxyears'][$t]) ? $_POST['taxyears'][$t] : "";

                $taxservicemonth = isset($_POST['tax_service_month'][$t]) ? $_POST['tax_service_month'][$t] : "";
                $taxserviceyear = isset($_POST['tax_service_year'][$t]) ? $_POST['tax_service_year'][$t] : "";

                if ($request->business_id == '6') {
                    $taxxyears = $taxyears;
                } else {
                    $taxxyears = $taxserviceyear;
                }
                if (!empty($taxxyears)) {

                    $taxyearsplus = $taxxyears + 1;
                    if ($taxxyears != '2019') {
                        $expiredates = $taxyearsplus . '-04-15';
                    } else {
                        $expiredates = $taxyearsplus . '-07-15';
                    }
                } else {
                    $expiredates = '0000-00-00';
                }

                //  $clienttaxid=$_POST['id'][$t];

                $t++;
                if ($taxservice1 != '') {
                    $taxinsert = DB::insert("insert into client_to_taxation(`tax_service_month`,`tax_service_year`,`expiredate`,`taxyears`,`locked`,`pricetype`,`currency`,`serviceperiod`,`clientid`,`taxation_service`,`taxation_service_period`,`employee_id`,`tax_acc_checkby`,`note`)
                                 values('" . $taxservicemonth . "','" . $taxserviceyear . "','" . $expiredates . "','" . $taxxyears . "','" . $taxlocked . "','" . $taxpricetype . "','" . $taxcurrency . "','" . $taxserviceperiod . "','" . $id . "','" . $taxservice1 . "','" . $taxserviceperiod1 . "','" . $taxemp . "','" . $taxac . "','" . $taxnte . "')");
                }
            }

        }


        $totalprice1 = $request->totalprice1;
        $pricetype = $request->pricetype;
        $currency = $request->currency;
        $serviceperiod = $request->serviceperiod;
        $typeofservice = isset($request->typeofservice) ? $request->typeofservice : "";


        $regularprice = isset($request->regularprice) ? $request->regularprice : "";
        $comboprice = isset($request->comboprice) ? $request->comboprice : "";
        $price = isset($request->price) ? $request->price : "";
        $discountprice = $request->discountprice;
        $pricenote = $request->pricenote;
        $titles = $request->titles;
        $regularprice11 = $request->regularprice1;
        $comboprice11 = $request->comboprice1;
        $serviceid = isset($request->serviceid) ? $request->serviceid : "";
        $taxid = $request->taxid;
        $serviceincludes = isset($request->serviceincludes) ? $request->serviceincludes : "";
        $sign = $request->ckq;
        $taxnote = $request->taxnote;
        if (empty($request->taxprice)) {
            $taxprice = 0;
        } else {
            $taxprice = $request->taxprice;
        }


        $l = 0;
        $countservice = DB::table('clientservices')->where('clientid', '=', $id)->count();
        //  echo "<pre>";print_r($_POST);EXIT;
        if (empty($serviceincludes)) {
        } else {
            $usersdel = DB::table('clientservices')->where('clientid', '=', $id)->delete();
            //  print_r($typeofservice);exit;
            //  echo "<pre>"; print_r($serviceincludes);exit;
            foreach ($regularprice as $typeofservice11) {
                $typeofservice1 = $request->typeofservice[$l];


                // echo "<pre>";print_r($typeofservice2);
                $regularprice1 = $regularprice[$l];
                $comboprice1 = $comboprice[$l];
                $price1 = $price[$l];
                $serviceid1 = $serviceid[$l];
                $serviceincludes1 = $serviceincludes[$l];

                $empnotes = isset($request->monthlynote[$l]) ? $request->monthlynote[$l] : "";
                $empids1 = isset($request->employee_r[$l]) ? $request->employee_r[$l] : "";
                $acccheckby = isset($request->acc_check_by[$l]) ? $request->acc_check_by[$l] : "";
                $locked1 = isset($request->taxlocked1[$l]) ? $request->taxlocked1[$l] : "";
                $acperiods1 = isset($request->acperiods1[$l]) ? $request->acperiods1[$l] : "";


                $l++;

                if (!empty($acperiods1)) {
                    // exit('1111');
                    $clientservices = DB::insert("insert into clientservices
        (`locked`,`employeenote`,`employee_id`,`acc_check_by`,`sign`,`total`,`serviceincludes`,`note`,`clientid`,`pricetype`,`currency`,`typeofservice`,
        `serviceperiod`,`regularprice`,`comboprice`,`price`,`discountprice`,`periods`) 
        values
        ('" . $locked1 . "','" . $empnotes . "','" . $empids1 . "','" . $acccheckby . "','" . $sign . "','" . $request->totalprice1 . "','" . $serviceincludes1 . "',
        '" . $request->pricenote . "',
        '" . $id . "','" . $pricetype . "','" . $currency . "','" . $typeofservice1 . "','" . $serviceperiod . "','" . $regularprice1 . "','" . $comboprice1 . "',
        '" . $price1 . "','" . $discountprice . "','" . $acperiods1 . "')");
                    //  $lastid = DB::getPdo()->lastInsertId();

                    //  print_r($clientservices);exit;
                    //$users = DB::table('clientservices')->where('regularprice','=','')->delete();
                }
                //  else
                //{
                //  exit('2222222');
                // }

                /* else
                 {
                 $returnValue = DB::table('clientservices')->where('id','=',$serviceid1)
                 ->update([
                            //'note' => $pricenote,
                            'pricetype' =>$pricetype,
                            'currency' =>$currency,
                            'serviceperiod'=>$serviceperiod,
                            'typeofservice'=>$typeofservice1,
                            'regularprice'=>$regularprice1,
                            'comboprice'=>$comboprice1,
                            'price'=>$price1,
                            'discountprice'=>$discountprice,
                            'serviceincludes'=> $serviceincludes1,
                            'total' =>$totalprice1,
                            'employee_id' =>$empids,
                            'employeenote' =>$empnotes,
                            'sign' =>$sign]);
                 }*/
            }
            //  ECHO "<PRE>"; print_r($typeofservice11);exit;
        }


        $taxpricetype = $request->pricetype;
        $taxcurrency = $request->currency;
        $titletax = $request->titletax;

        $taxserviceperiod = $request->serviceperiod;
        $taxtypeofservice = isset($request->taxtypeofservice) ? $request->taxtypeofservice : "";
        $taxregularprice = isset($request->taxregularprice) ? $request->taxregularprice : "";
        $taxcomboprice = isset($request->taxcomboprice) ? $request->taxcomboprice : "";
        $taxserviceid = isset($request->taxserviceid) ? $request->taxserviceid : "";
        //$taxid = $request->taxid;
        $taxserviceincludes = isset($request->taxserviceincludes) ? $request->taxserviceincludes : "";
        $taxsign = $request->taxckq;
        //$taxnote = $request->taxnote;

        $taxprice2 = isset($request->taxprice1) ? $request->taxprice1 : "";


        $taxl = 0;
        $countservice = DB::table('taxclientservices')->where('clientid', '=', $id)->count();

        if (empty($taxserviceincludes)) {
        } else {
            $usersdel = DB::table('taxclientservices')->where('clientid', '=', $id)->delete();
            //  print_r($typeofservice);exit;
            foreach ($taxregularprice as $taxregularprice11) {
                $taxtypeofservice1 = isset($request->taxtypeofservice[$taxl]) ? $request->taxtypeofservice[$taxl] : "";

                // echo "<pre>";print_r($typeofservice2);
                $taxregularprice1 = $taxregularprice11;
                $taxcomboprice1 = $taxcomboprice[$taxl];
                $taxprice1 = $taxprice2[$taxl];
                $titletax1 = $titletax[$taxl];
                //$taxserviceid1 = $taxserviceid[$taxl];
                $taxserviceincludes1 = $taxserviceincludes[$taxl];
                $taxempids = isset($request->clemployee[$taxl]) ? $request->clemployee[$taxl] : "";
                $taxacccheckby = isset($request->taxacc_check_by[$taxl]) ? $request->taxacc_check_by[$taxl] : "";
                $taxlocked1 = isset($request->taxlocks[$l]) ? $request->taxlocks[$l] : "";
                $taxempnotes = isset($request->taxmonthlynote[$taxl]) ? $request->taxmonthlynote[$taxl] : "";


                $taxl++;

                if (!empty($titletax1)) {

                    $clientservices = DB::insert("insert into taxclientservices(`taxacc_check_by`,`titletax`,`taxemployee_id`,`taxemployeenote`,`taxsign`,`taxserviceincludes`,`clientid`,`pricetype`,`currency`,`taxtypeofservice`,`serviceperiod`,`taxregularprice`,`taxcomboprice`,`taxprice1`) 
        values('" . $taxacccheckby . "','" . $titletax1 . "','" . $taxempids . "','" . $taxempnotes . "','" . $taxsign . "','" . $taxserviceincludes1 . "','" . $id . "','" . $taxpricetype . "','" . $taxcurrency . "','" . $taxtypeofservice1 . "','" . $taxserviceperiod . "','" . $taxregularprice1 . "','" . $taxcomboprice1 . "','" . $taxprice1 . "')");
                    $lastid1 = DB::getPdo()->lastInsertId();
                    $returnValue = DB::table('taxclientservices')->where('id', '=', $lastid1)
                        ->update([
                            //'note' => $pricenote,
                            'taxemployeenote' => $taxempnotes,
                            'taxemployee_id' => $taxempids,
                            'taxacc_check_by' => $taxacccheckby,
                            'tax_locked' => $taxlocked1
                        ]);

                }
            }
            // print_r($typeofservice2);exit;
        }


        /* $l = 0;
               if(empty($request->pricetype))
               {
               }
               else
               {
                   //exit('222222222');
               foreach($request->regularprice as $typeofservice11)
               {
               $typeofservice1 = isset($request->typeofservice[$l]) ? $request->typeofservice[$l]:"";

               $regularprice1 = isset($request->regularprice[$l]) ? $request->regularprice[$l]:"";
               $comboprice1 = isset($request->comboprice[$l]) ? $request->comboprice[$l]:"";
               $price1 = isset($request->price[$l]) ? $request->price[$l]:"";
               $serviceincludes1 = isset($request->serviceincludes[$l]) ? $request->serviceincludes[$l]:"";
               $empids = isset($request->employee_r[$l]) ? $request->employee_r[$l]:"";
               $acccheckby = isset($request->acc_check_by[$l]) ? $request->acc_check_by[$l]:"";

               $empnotes = isset($request->monthlynote[$l]) ? $request->monthlynote[$l]:"";

               $l++;
               $clientservices = DB::insert("insert into clientservices(`acc_check_by`,`employee_id`,`employeenote`,`sign`,`total`,`serviceincludes`,`note`,`clientid`,
               `pricetype`,`currency`,`typeofservice`,`serviceperiod`,`regularprice`,`comboprice`,`price`,`discountprice`)
               values('".$acccheckby."',".$empids."','".$empnotes."','".$sign."','".$request->totalprice1."','".$serviceincludes1."','".$request->pricenote."','".$id."','".$request->pricetype."','".$request->currency."','".$typeofservice1."',
               '".$request->serviceperiod."','".$regularprice1."','".$comboprice1."','".$price1."','".$request->discountprice."')");
               //$users = DB::table('clientservices')->where('regularprice','=','')->delete();
               }
               }
             */
        /* $ll = 0;
         if(empty($request->titles))
         {

         }
         else{
         foreach($request->titles as $typeofservice11)
         {
         $taxid1=$request->taxid[$ll];
         $taxnote1=$request->taxnote[$ll];
         $titles1=$request->titles[$ll];
         $comboprice12=$request->comboprice1[$ll];
         $regularprice12=$request->regularprice1[$ll];
         if(empty($request->taxprice[$ll]))
         {
             $taxprice1=0;
         }
         else
         {
         $taxprice1=$request->taxprice[$ll];

         }

         $ll++;
         if(empty($taxid1))
         {
             if(!empty($titles1)){
              //   return $id;
         $clientservicetitles = DB::insert("insert into clientservicetitles(`clientid`,`servicetype`,`regularprice1`,`comboprice1`,`titles`,`taxprice`,`taxnote`)
         values('".$id."','3','".$regularprice12."','".$comboprice12."','".$titles1."','".$taxprice1."','".$taxnote1."')");
         $users = DB::table('clientservicetitles')->where('regularprice1', '=', '')->delete();
          }
         }
         }
         }
         */

        if (($request->status == 'Pending') or ($request->status == 'Hold')) {
            $name = $request->first_name . ' ' . $request->middle_name . ' ' . $request->last_name;
            $status = $request->status;
            $email = $request->email;
            $user_type = $request->user_type;
            $name = $request->first_name;
            DB::table('users')->where('user_id', $id)->update(['user_type' => $user_type, 'type' => 2, 'name' => $name]);
            $returnValue = DB::table('users')->where('email', '=', $email)->update(['user_type' => $user_type, 'type' => 2, 'name' => $name]);
            $data = array('email' => $email, 'first_name' => $name, 'from' => 'vijay@businesssolutionteam.com');
            /*\Mail::send('fac-Bhavesh-0554/hold', $data, function( $message ) use ($data)
            {
            $message->to($data['email'] )->from($data['from'], $data['first_name'])->subject('Account Approval');
            });*/
        } else {
            $status = $request->status;
            $email = $request->email;
            $name = $request->first_name;
            $password = mt_rand();
            $user_type = $request->user_type;
            $client_id = $files;
            $ccc = $request->filename;
            $user = User::where('email', '=', $email)->where('client_id', '=', $client_id)->first();
// if ($user === null)
// {
// $insert = DB::insert("insert into users(`name`,`email`,`password`,`newpassword`,`user_id`,`user_type`,`type`,`client_id`) values('".$name."','".$email."','".bcrypt($password)."','".$password."','".$id."','".$user_type."','1','".$client_id."')");
// $data = array('email' => $email, 'first_name' => $name, 'from' => 'bvijay@financialservicecenter.net', 'password' =>$password,'client'=>$client_id);
// /*\Mail::send( 'activation', $data, function( $message ) use ($data)
// {
// $message->to( $data['email'] )->from( $data['from'], $data['first_name'], $data['password'], $data['client'] )->subject('Account Approval');
// });*/
// }
        }
        return redirect('fac-Bhavesh-0554/customer')->with('success', 'Client added successfully');
    }

    public function servicedel2(Request $request)
    {
        $tos = $request->input('tos');
        $clientid = $request->input('clientid');

        if ($tos == '2') {

            $returnValue11 = DB::table('commonregisters')->where('id', '=', $clientid)->update(['accounting_period' => '']);

            //print_r($returnValue11);

        } else if ($tos == '8') {
            $returnValue11 = DB::table('commonregisters')->where('id', '=', $clientid)->update(['payroll_period' => '', 'payroll_service_month' => '', 'payroll_service_year' => '']);


        }


        $users = DB::table('clientservices')->where('id', '=', $request->input('id'))->delete();
        if ($users) {
            echo 'Data Deleted';
        }
    }

    public function taxservicedel2(Request $request)
    {
        $tos = $request->input('ttax');
        $clientid = $request->input('clientid');


        $users1 = DB::table('client_to_taxation')->where('clientid', '=', $clientid)->where('taxation_service', '=', $tos)->delete();


        $users = DB::table('taxclientservices')->where('id', '=', $request->input('id'))->delete();
        if ($users) {
            echo 'Data Deleted';
        }
    }

    public function servicedel3(Request $request)
    {
        $users = DB::table('clientservicetitles')->where('id', '=', $request->input('id'))->delete();
        if ($users) {
            echo 'Data Deleted';
        }
    }

    public function deleteacc(Request $request)
    {
        $users = DB::table('accounting_table')->where('id', '=', $request->input('id'))->delete();
        if ($users) {
            echo 'Data Deleted';
        }
    }

    public function resetServiceInfo1(Request $request, $cid)
    {
        //echo $_GET['id'];
        //echo $cid;die;
        //echo $cid=$request->cid;
        $returnValue11 = DB::table('commonregisters')->where('id', '=', $cid)->update(['accounting_period' => '', 'ac_service_month' => '', 'ac_service_year' => '']);
        $returnValue12 = DB::table('commonregisters')->where('id', '=', $cid)->update(['payroll_period' => '', 'payroll_service_month' => '', 'payroll_service_year' => '']);

        $users = DB::table('client_to_taxation')->where('clientid', '=', $cid)->delete();

        $users = DB::table('clientservices')->where('clientid', '=', $cid)->delete();
        $users1 = DB::table('taxclientservices')->where('clientid', '=', $cid)->delete();
        $users2 = DB::table('accounting_table')->where('clientid', '=', $cid)->delete();

        return redirect()->back()->with('success', 'Your Data Reset Successfully ');
    }


    public function resetServiceInfo(Request $request)
    {
        //echo $_GET['id'];
        //echo "<pre>";    print_r($_SERVER);
        $ss = $_SERVER['argv'][0];
        $ss1 = explode('=', $ss);
        //echo $ss1[1];
        //  echo 'xx';
        $returnValue11 = DB::table('commonregisters')->where('id', '=', $ss1[1])->update(['accounting_period' => '', 'ac_service_month' => '', 'ac_service_year' => '']);
        $returnValue12 = DB::table('commonregisters')->where('id', '=', $ss1[1])->update(['payroll_period' => '', 'payroll_service_month' => '', 'payroll_service_year' => '']);

        $users = DB::table('client_to_taxation')->where('clientid', '=', $ss1[1])->delete();

        $users = DB::table('clientservices')->where('clientid', '=', $ss1[1])->delete();
        $users1 = DB::table('taxclientservices')->where('clientid', '=', $ss1[1])->delete();
        $users2 = DB::table('accounting_table')->where('clientid', '=', $ss1[1])->delete();


        //  $users = DB::table('clientservicetitles')->where('clientid', '=',$request->input('id'))->delete();
        if ($returnValue11) {
            echo 'Service Information Data Reset Successfully';
        } else {
            echo '111';
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        $common = Commonregister::where('id', $id)->first();
        return View::make('fac-Bhavesh-0554.customer.customer', compact(['common']));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return \Illuminate\Http\Response
     */


    public function getlanguage(Request $request)
    {
        $lang = $request->language;
        $data = Employee::whereRaw('FIND_IN_SET("' . $lang . '",languages)')->where('status', '=', '1')->orderby('firstName', 'asc')->get();

        return response()->json($data);

    }


    public function getsecondlanguage(Request $request)
    {
        $lang = $request->language;
        $data = Employee::whereRaw('FIND_IN_SET("' . $lang . '",languages)')->where('status', '=', '1')->orderby('firstName', 'asc')->get();

        return response()->json($data);

    }

    public function edit($id)
    {
        // echo $id;die;
        $categoryname = DB::table('commonregisters')->select('commonregisters.id', 'commonregisters.id as cid', 'commonregisters.business_cat_id', 'categories.sic', 'categories.naics')
            ->leftJoin('categories', function ($join) {
                $join->on('categories.id', '=', 'commonregisters.business_cat_id');
            })
            ->where('commonregisters.id', '=', $id)->first();

        $returnValue11 = DB::table('commonregisters')
            ->where('id', '=', $id)
            ->update([
                'recstatus' => '0'
            ]);
        // $clientbank=DB::table('client_bank')->where('client_id','=',$id)->orderby('bank_name','asc')->get();

        $clientbank = DB::table('client_bank')->select('client_bank.id as ccid', 'client_bank.nick_name', 'client_bank.routingno', 'client_bank.fourdigit', 'client_bank.client_id', 'client_bank.bank_name', 'bankmaster.id', 'bankmaster.id', 'bankmaster.bankname')
            ->leftJoin('bankmaster', function ($join) {
                $join->on('bankmaster.id', '=', 'client_bank.bank_name');
            })->
            where('client_bank.client_id', '=', $id)->get();

        $taxationservice = DB::table('taxtitles')->whereIn('id', array(1, 2, 3, 6, 7))->orderby('title', 'asc')->groupBy('title')->get();
        $payrollservice = DB::table('payroll_service')->get();

        $statecode = DB::table('state')->where('countrycode', '=', 'USA')->orderby('code', 'asc')->get();
        $countuser = DB::table('commonregisters')->select('commonregisters.id', 'contact_userinfos.user_access', 'contact_userinfos.user_id')
            ->leftJoin('contact_userinfos', function ($join) {
                $join->on('contact_userinfos.user_id', '=', 'commonregisters.id');
            })
            ->where('commonregisters.id', '=', $id)->where('contact_userinfos.user_access', '=', 1)->get()->count();

        $taxstate = taxstate::All();
        //$language = language::orderBy('language_name', 'asc')->get();
        $language = DB::table('languages')->orderBy('language_name', 'asc')->get();
        //echo "<pre>";
        //print_r($language);die;
        $ethnic = Ethnic::orderBy('ethnic_name', 'asc')->get();

        $newclient12 = Commonregister::where('id', $id)->get()->first();
        // echo $newclient12->filename;exit;

        $commonuser = DB::table('commonregisters')->select('commonregisters.company_name', 'commonregisters.filename', 'commonregisters.business_id', 'commonregisters.first_name', 'commonregisters.last_name', 'commonregisters.status')->where('status', '=', 'Active')->orderby('filename', 'asc')->get();
        $linkuser = DB::table('commonregisters')->select('commonregisters.productid', 'commonregisters.company_name', 'commonregisters.business_id', 'commonregisters.first_name', 'commonregisters.last_name', 'commonregisters.status')->whereRaw('FIND_IN_SET("' . $newclient12->filename . '",productid)')->get();
        //  echo "<pre>";print_r($linkuser);die;
        $secondlanguagename = DB::table('employees')->select('employee_id', 'firstName', 'lastName', 'middleName', 'languages')->whereRaw('FIND_IN_SET("' . $newclient12->other_second_language . '",languages)')->get();
        $bid = $newclient12->business_id;
        $catid = $newclient12->business_cat_id;
        $brand_id = $newclient12->business_brand_id;
        $category = Category::where('bussiness_name', '=', $bid)->orderby('business_cat_name', 'asc')->get();
        $business = Business::orderBy('bussiness_name', 'asc')->get();
        // echo "<pre>";print_r($business);die;
        $businessbrand = BusinessBrand::where('business_cat_id', '=', $catid)->orderBy('business_brand_name', 'asc')->get();
        $cb = Categorybusiness::where('business_brand_id', '=', $brand_id)->orderBy('business_brand_category_name', 'asc')->get();

        $info = Contact_userinfo::where('user_id', '=', $id)->get();
        $infoCount = $info->count();


        $info1 = Contact_userinfo::where('user_id', '=', $id)->first();
        $user_id = Auth::user()->id;
        $business_cat_id = Auth::user()->business_cat_id;
        $user = User::where('user_id', '=', $id)->first();
        $newclient = Commonregister::where('id', $id)->update(['newclient' => 2]);
        $subcustomer = User::groupBy('client_id')->get();

        $note = DB::table('notes')->get();
        $accountingtable = DB::table('accounting_table')->where('clientid', '=', $id)->get();
        $userinfos = DB::table('contact_userinfos')->where('user_id', '=', $id)->get();


        $newlocations = DB::table('newlocations')->where('clientid', $id)->get();
        $position = Price::All();
        $currency = Currency::All();
        $typeofser = Typeofser::orderBy('typeofservice', 'asc')->get();
        $period = Period::All();
        $other_first_language1 = Employee::where('id', '=', $newclient12->other_first_language1)->where('status', '=', '1')->first();
        $other_second_language1 = Employee::where('employee_id', '=', $newclient12->other_second_language1)->where('status', '=', '1')->first();
        $taxtitle = Taxtitle::orderBy('title', 'asc')->get();
        $taxtitles = Taxtitle::orderBy('id', 'asc')->get();
        // $taxtitles1 = Taxtitle::where('id','=','7')->where('id','=','')->orderBy('id', 'asc')->get();


        $admin_notes = DB::table('notes')->where('type', '=', 'admin')->where('userid', '=', $id)->get();
        $clientser = DB::table('clientservices')->select('clientservices.periods', 'clientservices.typeofservice1', 'clientservices.id', 'clientservices.clientid', 'clientservices.pricetype', 'clientservices.currency', 'clientservices.serviceperiod', 'clientservices.typeofservice', 'clientservices.serviceincludes', 'clientservices.regularprice', 'clientservices.comboprice', 'clientservices.price', 'clientservices.status', 'clientservices.discountprice', 'clientservices.note', 'clientservices.total', 'clientservices.sign', 'clientservices.employeenote', 'clientservices.employee_id', 'clientservices.acc_check_by', 'clientservices.locked', 'clientservices.taxation_title')->where('clientid', '=', $id)->orderBy('id', 'asc')->get();
        $taxservices = DB::table('client_to_taxation')->where('clientid', '=', $id)->where('check_status', '=', 0)->orderBy('id', 'asc')->get();
        $taxservices1 = DB::table('client_to_taxation')->where('clientid', '=', $id)->where('check_status', '=', 0)->orderBy('taxation_service', 'asc')->get();

        $taxclientser = DB::table('taxclientservices')->where('clientid', '=', $id)->orderBy('id', 'asc')->get();
        $taxclientser5 = DB::table('taxclientservices')->where('clientid', '=', $id)->first();

        $clientser5 = DB::table('clientservices')->where('clientid', '=', $id)->get()->first();
        $taxservices5 = DB::table('client_to_taxation')->where('clientid', '=', $id)->where('check_status', '=', 0)->first();

        // print_r($clientser5);
        $clientsertitle = DB::table('clientservicetitles')->where('clientid', '=', $id)->orderBy('id', 'asc')->get();

        $clientsertitle1 = DB::table('clientservicetitles')->select('clientservicetitles.clientid', 'clientservicetitles.servicetype', 'clientservicetitles.locked')->where('clientservicetitles.clientid', '=', $id)->get()->first();


        $client = DB::table('notes')->where('admin_id', '=', $id)->where('type', '=', 'client')->get();
        $fsc = DB::table('notes')->where('admin_id', '=', '')->where('type', '=', 'fsc')->get();
        $every = DB::table('notes')->where('type', '=', 'Everybody')->get();

        $employee1 = DB::table('employees')->select('employees.emp_service1', 'employees.emp_service2', 'employees.acc_service_1', 'employees.acc_service_2', 'employees.id', 'employees.team', 'employees.type', 'employees.firstName', 'employees.middleName', 'employees.lastName', 'teams.id as teamid', 'teams.team as teams')
            ->leftJoin('teams', function ($join) {
                $join->on('teams.id', '=', 'employees.team');
            })
            ->where('employees.check', '=', '1')->where('employees.type', '!=', 'clientemployee')->where('employees.type', '!=', 'Vendor')->where('employees.type', '!=', '')->orderBy('employees.firstName', 'asc')->get();


        // $employee1 = Employee::where('check','=','1')->where('type','!=','clientemployee')->where('type','!=','Vendor')->where('type','!=','')->orderBy('firstName', 'asc')->get();
        $employee = Fscemployee::orderBy('name', 'asc')->get();
        $common = DB::table('commonregisters')->select('commonregisters.mailing_address_check', 'commonregisters.other_first_language1', 'commonregisters.other_first_language', 'commonregisters.ac_service_month', 'commonregisters.ac_service_year', 'commonregisters.payroll_service_month', 'commonregisters.payroll_service_year', 'commonregisters.contactnametype', 'commonregisters.other_dob_month_s', 'commonregisters.other_dob_day_s', 'commonregisters.basic_guardian1', 'commonregisters.basic_guardian2', 'commonregisters.guardian', 'commonregisters.guardian2', 'commonregisters.faxbli3_g', 'commonregisters.productid', 'commonregisters.accounting_period', 'commonregisters.payroll_period', 'commonregisters.other_period', 'commonregisters.adminonly', 'commonregisters.supervisor', 'commonregisters.fscee', 'commonregisters.fscuser', 'commonregisters.client', 'commonregisters.alluser', 'commonregisters.telephoneNo1Type', 'commonregisters.other_second_language1', 'commonregisters.id as cid', 'commonregisters.contact_title as contact_title', 'commonregisters.paymentmode11 as paymentmode11', 'commonregisters.carttype1 as carttype1', 'commonregisters.cartno1 as cartno1', 'commonregisters.acountname1 as acountname1', 'commonregisters.paymentnote1 as paymentnote1', 'commonregisters.acountname1 as acountname1', 'commonregisters.acountno1 as acountno1', 'commonregisters.routingno1 as routingno1', 'commonregisters.bankname1 as bankname1', 'commonregisters.maritial_spouse1 as maritial_spouse1', 'commonregisters.maritial_spouse as maritial_spouse', 'commonregisters.ext2 as ext2', 'commonregisters.telephoneNo2Type as telephoneNo2Type', 'commonregisters.motelephoneile1 as motelephoneile1', 'commonregisters.maritial_last_name as maritial_last_name', 'commonregisters.maritial_middle_name as maritial_middle_name', 'commonregisters.maritial_first_name as maritial_first_name', 'commonregisters.personalname as personalname', 'commonregisters.CL as CL', 'commonregisters.other_maritial_status1 as other_maritial_status1', 'commonregisters.other_maritial_status as other_maritial_status', 'commonregisters.other_dob_month as other_dob_month', 'commonregisters.other_dob_day as other_dob_day', 'commonregisters.other_spouse_month as other_spouse_month', 'commonregisters.other_spouse_day as other_spouse_day', 'commonregisters.other_marriage_month as other_marriage_month', 'commonregisters.other_marriage_day as other_marriage_day', 'commonregisters.other_ethnic as other_ethnic', 'commonregisters.other_ethnic_s as other_ethnic_s', 'commonregisters.other_main_language as other_main_language', 'commonregisters.other_first_language as other_first_language', 'commonregisters.other_second_language as other_second_language', 'commonregisters.emailbli1 as emailbli1', 'commonregisters.faxbli1 as faxbli1', 'commonregisters.faxbli3 as faxbli3', 'commonregisters.cartnote as cartnote', 'commonregisters.billingtoo as billingtoo', 'commonregisters.cartno as cartno', 'commonregisters.carttype as carttype', 'commonregisters.paymentmode1 as paymentmode1', 'commonregisters.acountname as acountname', 'commonregisters.acountno as acountno', 'commonregisters.routingno as routingno', 'commonregisters.bankname as bankname', 'commonregisters.paymentnote as paymentnote', 'commonregisters.paymentmode as paymentmode', 'commonregisters.locations as locations', 'commonregisters.multilocation as multilocation', 'commonregisters.subscription_answer3 as subscription_answer3', 'commonregisters.subscription_answer2 as subscription_answer2', 'commonregisters.subscription_answer1 as subscription_answer1', 'commonregisters.subscription_question3 as subscription_question3', 'commonregisters.subscription_question1 as subscription_question1', 'commonregisters.subscription_question2 as subscription_question2', 'commonregisters.user_answer3 as user_answer3', 'commonregisters.user_answer2 as user_answer2', 'commonregisters.user_answer1 as user_answer1', 'commonregisters.limited_answer3 as limited_answer3', 'commonregisters.limited_answer2 as limited_answer2', 'commonregisters.limited_answer1 as limited_answer1', 'commonregisters.user_question3 as user_question3', 'commonregisters.user_question2 as user_question2', 'commonregisters.user_question1 as user_question1', 'commonregisters.limited_question3 as limited_question3', 'commonregisters.limited_question2 as limited_question2', 'commonregisters.limited_question1 as limited_question1', 'commonregisters.useremail as useremail', 'commonregisters.user_resetdate as user_resetdate', 'commonregisters.useremail as useremail', 'commonregisters.user_resetdays as user_resetdays', 'commonregisters.user_active as user_active', 'commonregisters.limited_resetdate as limited_resetdate', 'commonregisters.limited_resetdays as limited_resetdays', 'commonregisters.limited_active as limited_active', 'commonregisters.limited_user as limited_user', 'commonregisters.subscription_answer1 as subscription_answer1', 'commonregisters.subscription_answer2 as subscription_answer2', 'commonregisters.subscription_answer3 as subscription_answer3', 'commonregisters.subscription_question3 as subscription_question3', 'commonregisters.subscription_question2 as subscription_question2', 'commonregisters.subscription_question1 as subscription_question1', 'commonregisters.subscription_resetdays as subscription_resetdays', 'commonregisters.subscription_resetdate as subscription_resetdate', 'commonregisters.subscription_active as subscription_active', 'commonregisters.subscription_lock as subscription_lock', 'commonregisters.subscription_user as subscription_user', 'commonregisters.user_cell as user_cell', 'commonregisters.user_email as user_email', 'commonregisters.user_name as user_name', 'commonregisters.creationdate as creationdate', 'commonregisters.nametype as nametype', 'commonregisters.etelephone2 as etelephone2', 'commonregisters.eext2 as eext2', 'commonregisters.eteletype2 as eteletype2', 'commonregisters.eext1  as eext1', 'commonregisters.eteletype1 as eteletype1', 'commonregisters.filename as filename', 'commonregisters.etelephone1 as etelephone1', 'commonregisters.businessext as businessext', 'commonregisters.businesstype as businesstype', 'commonregisters.contact_address1 as contact_address1', 'commonregisters.contact_address2 as contact_address2', 'commonregisters.city_1 as city_1', 'commonregisters.state_1 as state_1', 'commonregisters.zip_1 as zip_1', 'commonregisters.mobile_1 as mobile_1', 'commonregisters.mobiletype_1 as mobiletype_1', 'commonregisters.ext2_1 as ext2_1', 'commonregisters.mobile_2 as mobile_2', 'commonregisters.mobiletype_2 as mobiletype_2', 'commonregisters.ext2_2 as ext2_2', 'commonregisters.contact_fax_1 as contact_fax_1', 'commonregisters.email_1 as email_1', 'commonregisters.minss as minss', 'commonregisters.firstname as firstname', 'commonregisters.middlename as middlename', 'commonregisters.lastname as lastname', 'commonregisters.mailing_address1 as mailing_address1', 'commonregisters.bussiness_zip as bussiness_zip', 'commonregisters.due_date as due_date', 'commonregisters.department as department', 'commonregisters.type_of_activity as type_of_activity', 'commonregisters.county_no as county_no', 'commonregisters.county_name as county_name', 'commonregisters.level as level', 'commonregisters.setup_state as setup_state', 'commonregisters.business_state as business_state', 'commonregisters.business_city as business_city', 'commonregisters.business_country as business_country', 'commonregisters.business_address as business_address', 'commonregisters.business_store_name as business_store_name', 'commonregisters.mailing_address as mailing_address', 'commonregisters.legalname as legalname', 'commonregisters.dbaname as dbaname', 'commonregisters.mailing_city as mailing_city', 'commonregisters.mailing_state as mailing_state', 'commonregisters.mailing_zip as mailing_zip', 'commonregisters.user_type as user_type', 'commonregisters.user_type as user_type', 'commonregisters.status as status', 'commonregisters.company_name as company_name', 'commonregisters.business_name as business_name', 'commonregisters.first_name as first_name', 'commonregisters.middle_name as middle_name', 'commonregisters.last_name as last_name', 'commonregisters.email as email', 'commonregisters.address as address', 'commonregisters.address1 as address1', 'commonregisters.city as city', 'commonregisters.stateId as stateId', 'commonregisters.zip as zip', 'commonregisters.countryId as countryId', 'commonregisters.mobile_no as mobile_no', 'commonregisters.business_no as business_no', 'commonregisters.business_fax as business_fax', 'commonregisters.website as website', 'commonregisters.user_type as user_type', 'commonregisters.business_id as business_id', 'commonregisters.business_cat_id as business_cat_id', 'commonregisters.business_brand_id as business_brand_id', 'commonregisters.business_brand_category_id as business_brand_category_id', 'businesses.bussiness_name as bussiness_name', 'categories.business_cat_name as business_cat_name', 'business_brands.business_brand_name as business_brand_name', 'categorybusinesses.business_brand_category_name as business_brand_category_name')
            ->leftJoin('categories', function ($join) {
                $join->on('commonregisters.business_cat_id', '=', 'categories.id');
            })
            ->leftJoin('businesses', function ($join) {
                $join->on('commonregisters.business_id', '=', 'businesses.id');
            })
            ->leftJoin('business_brands', function ($join) {
                $join->on('commonregisters.business_brand_id', '=', 'business_brands.id');
            })
            ->leftJoin('categorybusinesses', function ($join) {
                $join->on('commonregisters.business_brand_category_id', '=', 'categorybusinesses.id');
            })
            ->where('commonregisters.id', '=', "$id")->get()->first();

        $conversation = DB::table('conversation_sheet as t1')->select('t1.*', 't2.id as ids', 't2.relatednames')
            ->Join('relatednames as t2', function ($join) {
                $join->on('t2.id', '=', 't1.conrelatedname');
            })
            ->where('t1.clientid', '=', $id)
            ->get();

        $listclient = DB::table('commonregisters')->where('first_name', '!=', '')->where('last_name', '!=', '')->orderBy('first_name', 'ASC')->get();
        $listvendoe = DB::table('employees')->where('type', 'Vendor')->where('firstName', '!=', '')->where('lastName', '!=', '')->orderBy('firstName', 'ASC')->get();
        $listemployeeuser = DB::table('employees')->where('type', 'employee')->where('firstName', '!=', '')->where('lastName', '!=', '')->orderBy('firstName', 'ASC')->get();
        //print_r($listemployeeuser);die;
        $relatedNames = DB::table('relatednames')->get();

        $notesdata = DB::table('notes_sheet as t1')->select('t1.*', 't2.id as ids', 't2.notesrelated')
            ->Join('notesrelateds as t2', function ($join) {
                $join->on('t2.id', '=', 't1.noterelated');
            })
            ->where('t1.noteclientid', '=', $id)
            ->get();
        //print_r($notesdata);die;
        $NotesNames = DB::table('notesrelateds')->get();

        return view('fac-Bhavesh-0554.customer.customer-edit', compact(['payrollservice', 'categoryname', 'clientbank', 'taxservices1', 'taxtitles', 'taxservices5', 'taxclientser5', 'notesdata', 'NotesNames', 'listclient', 'listvendoe', 'listemployeeuser', 'relatedNames', 'conversation', 'accountingtable', 'taxclientser', 'linkuser', 'commonuser', 'taxationservice', 'taxservices', 'userinfos', 'statecode', 'infoCount', 'countuser', 'secondlanguagename', 'clientsertitle1', 'other_first_language1', 'other_second_language1', 'ethnic', 'language', 'newlocations', 'subcustomer', 'employee1', 'employee', 'clientser5', 'clientser', 'clientsertitle', 'note', 'every', 'fsc', 'taxstate', 'admin_notes', 'client', 'common', 'category', 'business', 'businessbrand', 'cb', 'user', 'info', 'info1', 'position', 'currency', 'period', 'typeofser', 'taxtitle']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */


//mmmmmmmmmmmmmmmmmmmmmm


    public function update(Request $request, $id)
    {
        ob_start();
        /// echo "<pre>"; print_r($_POST['taxation_service_period']);
        //  print_r($_POST['taxation_service']);
        //exit;
// ECHO  "<pre>";print_r($_POST);EXIT;
//    print_r(array_filter($_POST['titletax']));
//echo "<pre>";print_r($_POST['taxation_service']);
//exit;
        //  print_r($accountingsoftware);
        //  echo "<pre>";
        //print_r($accountingtable);
        //print_r($_POST);exit;

        $accountingsoftware = isset($request->accounting_software) ? $request->accounting_software : "";

        $typeofwork = isset($request->typeofwork) ? $request->typeofwork : "";
        $accountinglocation = $request->accounting_location;
        $acc = 0;
        $acctable = DB::table('accounting_table')->where('clientid', '=', $id)->delete();
        if ($accountingsoftware) {
            foreach ($accountingsoftware as $accsoftware) {

                $accsoftware1 = $accsoftware;
                $acclocation1 = $accountinglocation[$acc];
                $acctypeofwork1 = $typeofwork[$acc];


                $acc++;
                if ($accsoftware1 != '') {
                    $accinsert = DB::insert("insert into accounting_table(`clientid`,`accounting_software`,`accounting_location`,`typeofwork`)
                    values('" . $id . "','" . $accsoftware1 . "','" . $acclocation1 . "','" . $acctypeofwork1 . "')");
                }
            }
        }

        if ($request->status == 'Pending' or $request->status == 'Hold') {
            $name = $request->first_name . ' ' . $request->middle_name . ' ' . $request->last_name;
            $status = $request->status;
            $email = $request->email;
            $name = $request->first_name;
            $user_type = $request->user_type;
            $ccc = $request->fileno;
            //DB::table('users')->where('user_id', $id)->update(['user_type' =>$user_type,'type'=>2,'name'=>$name]);
            $returnValue = DB::table('users')->where('email', '=', $email)->update(['user_type' => $user_type, 'type' => 2, 'name' => $name, 'client_id' => $ccc]);
            //$data = array('email' => $email, 'first_name' => $name, 'from' =>'bvijay@financialservicecenter.net');
            /*\Mail::send('fac-Bhavesh-0554/hold', $data, function( $message ) use ($data)
            {
            $message->to( $data['email'] )->from( $data['from'], $data['first_name'])->subject('FSC Employee');
            });*/
        } else {
            $user_type = $request->user_type;
            $name = $request->first_name . ' ' . $request->middle_name . ' ' . $request->last_name;
            DB::table('users')->where('user_id', $id)->update(['user_type' => $user_type, 'type' => 1, 'name' => $name]);
            $status = $request->status;
            $email = $request->email;
            $name = $request->first_name;
            $question1 = $request->question1;
            $question2 = $request->question2;
            $question3 = $request->question3;
            $answer1 = $request->answer1;
            $answer2 = $request->answer2;
            $answer3 = $request->answer3;
            $password = mt_rand();
            $client_id = $request->fileno;
            $user_type = $request->user_type;
            $ccc = $request->fileno;

            //$user = User::where('email', '=', $email)->where('client_id', '=', $client_id)->first();
            // if ($user === null)
            // {
            // $ccc = $request->fileno;
            // $name = $request->firstname .' '. $request->middlename .' '. $request->lastname;
            // $insert = DB::insert("insert into users(`name`,`email`,`password`,`newpassword`,`user_id`,`user_type`,`question1`,`question2`,`question3`,`answer1`,`answer2`,`answer3`,`type`,`client_id`) values('".$name."','".$email."','".bcrypt($password)."','".$password."','".$id."','".$user_type."','".$question1."','".$question2."','".$question3."','".$answer1."','".$answer2."','".$answer3."','1','".$ccc."')");
            // $data = array('email' => $email, 'first_name' => $name, 'from' => 'bvijay@financialservicecenter.net', 'password' =>$password ,'client' =>$client_id);
            //     /*\Mail::send( 'activation', $data, function( $message ) use ($data)
            //     {
            //     $message->to( $data['email'] )->from( $data['from'], $data['first_name'], $data['password'], $data['client'] )->subject('FSC Client');
            //     });*/
            // }
            //else
            // {
            //     if ($user->type === '2' || $user->type === '0' || $user->type === '3')
            //     {
            //         $ccc = $request->fileno;
            //         $name = $request->firstname .' '. $request->middlename .' '. $request->lastname;
            //         $user_type=  $request->user_type;
            //         $returnValue = DB::table('users')->where('email', '=', $email)->update(['user_type' =>$user_type,'type'=>1,'name'=>$name,'password' =>bcrypt($password),'newpassword' =>$password,'client_id'=>$ccc]);
            //         $data = array('email' => $email, 'first_name' => $name, 'from' =>'bvijay@financialservicecenter.net', 'password' =>$password,'client'=>$client_id);
            //         /*\Mail::send( 'activation', $data, function( $message ) use ($data)
            //         {
            //             $message->to( $data['email'] )->from( $data['from'], $data['first_name'], $data['password'] , $data['client'] )->subject('FSC Client');
            //         });*/
            //     }
            //     else
            //     {
            //         $user_type=  $request->user_type;
            //         $returnValue = DB::table('users')->where('email', '=', $email)->update(['user_type' =>$user_type,'type'=>1,'name'=>$name]);
            //     }
            // }
        }

        $subscription_active = $request->subscription_active;
        if ($subscription_active == '1') {
            $name = $request->firstname . ' ' . $request->middlename . ' ' . $request->lastname;
            $email = $request->email;
            $first_name = $request->firstname;
            $middle_name = $request->middlename;
            $last_name = $request->lastname;
            $business_catagory = $request->business_catagory_name;
            $ccc = $request->fileno;
            $user_type = $request->user_type;
            $user = Submissionlogin::where('email', '!=', $email)->first();
            if ($user === null) {
                $name = $request->firstname . ' ' . $request->middlename . ' ' . $request->lastname;
                $insert = DB::insert("insert into submissionlogins(`firstname`,`middlename`,`lastname`,`email`,`password`,`newpassword`,`catagory_type`,`user_type`,`type`,`customerid`) values('" . $first_name . "','" . $middle_name . "','" . $last_name . "','" . $email . "','" . bcrypt($password) . "','" . $password . "','" . $business_catagory . "','" . $id . "','Submission','" . $ccc . "')");
                $data = array('email' => $email, 'first_name' => $name, 'from' => 'bvijay@financialservicecenter.net', 'password' => $password, 'client' => $ccc);
                Mail::send('activation', $data, function ($message) use ($data) {
                    $message->to($data['email'])->from($data['from'], $data['first_name'], $data['password'], $data['client'])->subject('FSC Client');
                });
            }
        }

        $limited_active = $request->limited_active;
        if ($limited_active == '1') {
            //die;
            $name = $request->firstname . ' ' . $request->middlename . ' ' . $request->lastname;
            $email = $request->email;
            $first_name = $request->firstname;
            $middle_name = $request->middlename;
            $last_name = $request->lastname;
            $business_catagory = $request->business_catagory_name;
            $ccc = $request->fileno;
            $password = mt_rand();
            $user_type = $request->user_type;
            $client_id = $request->fileno;

            $user = User::where('email', '=', $email)->count();
            //$user = User::where('client_id', '=', $client_id)->count();
            // echo $user;exit;
            //echo "<pre>";print_r($user);
            //exit('11111');
            if ($user == '0') {

                $name = $request->firstname . ' ' . $request->middlename . ' ' . $request->lastname;
                $insert = DB::insert("insert into users(`name`,`email`,`password`,`newpassword`,`user_id`,`user_type`,`type`,`client_id`) values('" . $name . "','" . $email . "','" . bcrypt($password) . "','" . $password . "','" . $id . "','" . $user_type . "','1','" . $client_id . "')");
                $data = array('email' => $email, 'first_name' => $name, 'from' => 'bvijay@financialservicecenter.net', 'password' => $password, 'client' => $client_id);

                $to = $email;
                $subject = "Client Account Approval";
                $message = "<!DOCTYPE html><html lang='en-US'><head><meta charset='utf-8'><title>FSC</title></head><body><img src='https://financialservicecenter.net/public/frontcss/images/fsc_logo.png' alt='img'><br><br><table border ='1' style='color: #333;font-family: Helvetica, Arial, sans-serif;width:100%;border-collapse:collapse; border-spacing: 0;' ><tr style='background:#535da6;'><td colspan='2' style='text-align:center;font-size:18px;color:#FFF;padding:10px 0;'> Congratulations, <br>Your account is approved by FSC</td></tr><tr><td style='width:40%;text-align:left;font-weight:bold;border:1px solid #CCC;height:30px;padding:0 10px;'><p>Client ID :</p></td><td style='width:60%;text-align:left;border: 1px solid #CCC; padding:0 10px; height: 30px;'><p> $client_id </p></td></tr><tr><td style='width:40%;text-align:left;font-weight:bold;border:1px solid #CCC;height:30px;padding:0 10px;'><p>Fullname :</p></td><td style='width:60%;text-align:left;border: 1px solid #CCC; padding:0 10px; height: 30px;'><p> $name </p></td></tr><tr><td style='width:40%;text-align:left;font-weight:bold;border:1px solid #CCC;height:30px;padding:0 10px;'><p>Email :</p></td><td style='width:60%;text-align:left;border: 1px solid #CCC;padding:0 10px; height: 30px;'><p> $email </p></td></tr><tr><td style='width:40%;text-align:left;font-weight:bold;border:1px solid #CCC;height:30px;padding:0 10px;'><p>Username :</p></td><td style='width:60%;text-align:left;border: 1px solid #CCC;padding:0 10px; height: 30px;'><p> $email </p></td></tr><tr><td style='width:40%;text-align:left;font-weight:bold;border:1px solid #CCC;height:30px;padding:0 10px;'><p>Password :</p></td><td style='width:60%;text-align:left;border: 1px solid #CCC;padding:0 10px; height: 30px;'><p> $password </p></td></tr><tr style='background:#535da6;'><td colspan='2' style='text-align:center;font-size:18px;color:#FFF;padding:10px 0;'> Please click this link to update your profile and create your password<br>Login Link: <a href='http://financialservicecenter.net/login' target='_blank'>Click Here</a></td></tr></table></body></html>";
                //$message = "Testingsssss";

                $headers = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $headers .= 'From: <bvijay@financialservicecenter.net>' . "\r\n";
                $headers .= 'Cc: bvijay@financialservicecenter.net' . "\r\n";
                mail($to, $subject, $message, $headers);

                // \Mail::send( 'activation', $data, function( $message ) use ($data)
                // {
                //     $message->to( $data['email'] )->from( $data['from'], $data['first_name'], $data['password'], $data['client'] )->subject('Account Approval');
                // });
            }
        }

        $user_type = $request->user_type;
        if ($request->locations == 'yes') {
            $multilocation = $request->multilocation;
        } else {
            $multilocation = '';
            DB::table('newlocations')->where('clientid', '=', $id)->delete();
        }

        $returnValue = DB::table('users')->where('email', '=', $email)->update(['user_type' => $user_type]);
        if ($request->paymentmode == 'ACH') {
            $bankname = $request->bankname;
            $routingno = $request->routingno;
            $acountno = $request->acountno;
            $acountname = $request->acountname;
            $cartno = '';
            $carttype = '';
        } elseif ($request->paymentmode == 'CreditCard') {
            $carttype = $request->carttype;
            $cartno = $request->cartno;
            $bankname = '';
            $routingno = '';
            $acountno = '';
            $acountname = '';
        } else {
            $carttype = '';
            $cartno = '';
            $bankname = '';
            $routingno = '';
            $acountno = '';
            $acountname = '';
        }


        if ($request->paymentmode11 == 'ACH') {
            $bankname1 = $request->bankname1;
            $routingno1 = $request->routingno1;
            $acountno1 = $request->acountno1;
            $acountname1 = $request->acountname1;
            $cartno1 = '';
            $carttype1 = '';
        } elseif ($request->paymentmode11 == 'CreditCard') {
            $carttype1 = $request->carttype1;
            $cartno1 = $request->cartno1;
            $bankname1 = '';
            $routingno1 = '';
            $acountno1 = '';
            $acountname1 = '';
        } else {
            $carttype1 = '';
            $cartno1 = '';
            $bankname1 = '';
            $routingno1 = '';
            $acountno1 = '';
            $acountname1 = '';
        }


        if ($request->business_id == '6') {
            $CL = $request->CL;
            $other_maritial_status = $request->other_maritial_status1;
            $personalname = $request->personalname;
            $maritial_first_name = $request->maritial_first_name;
            $maritial_middle_name = $request->maritial_middle_name;
            $maritial_last_name = $request->maritial_last_name;
            $maritial_spouse = $request->maritial_spouse;
            $maritial_spouse1 = $request->maritial_spouse1;
            $telephoneNo2Type = $request->telephoneNo2Type;
            $motelephoneile1 = $request->motelephoneile1;
            $ext2 = $request->ext2;
        } else {
            $CL = '';
            $other_maritial_status = '';
            $personalname = '';
            $maritial_first_name = '';
            $maritial_middle_name = '';
            $maritial_last_name = '';
            $maritial_spouse = '';
            $maritial_spouse1 = '';
            $telephoneNo2Type = '';
            $motelephoneile1 = '';
            $ext2 = '';
        }


        //   if(isset($request->accessby)!='')
        //   {
        //       $accessby_string=implode("-", $request->accessby);
        //   }
        //   else
        //   {
        //       $accessby_string='';
        //   }

        if (!empty($request->emailbli1)) {
            $contactnos = $request->email;
        } else {
            $contactnos = $request->email_1;
        }
        if (!empty($request->productid)) {
            $productsid = implode(',', $request->productid);
        } else {
            $productsid = '';
        }

        $returnValue11 = DB::table('commonregisters')
            ->where('id', '=', $id)
            ->update([
                'filename' => $request->fileno,
                'status' => $status,
                'multilocation' => $multilocation,
                'CL' => $CL,
                'service_period' => $request->serviceperiod,
                'other_maritial_status1' => $other_maritial_status,
                'personalname' => $personalname,
                'maritial_first_name' => $maritial_first_name,
                'maritial_middle_name' => $maritial_middle_name,
                'maritial_last_name' => $maritial_last_name,
                'maritial_spouse' => $maritial_spouse,
                'maritial_spouse1' => $maritial_spouse1,
                'guardian' => isset($request->guardian) ? $request->guardian : "",
                'guardian2' => isset($request->guardian2) ? $request->guardian2 : "",
                'basic_guardian1' => isset($request->basic_guardian1) ? $request->basic_guardian1 : "",
                'basic_guardian2' => isset($request->basic_guardian2) ? $request->basic_guardian2 : "",

                'faxbli3_g' => $request->faxbli3_g,

                'telephoneNo1Type' => $request->telephoneNo1Type,
                'telephoneNo2Type' => $request->telephoneNo2Type,
                'motelephoneile1' => $request->motelephoneile1,

                'ac_service_month' => $request->ac_service_month,
                'ac_service_year' => $request->ac_service_year,
                'payroll_service_month' => $request->payroll_service_month,
                'payroll_service_year' => $request->payroll_service_year,


                'nametype' => $request->nametypess,
                'ext2' => $ext2,
                'locations' => $request->locations,
                'company_name' => $request->company_name,
                'business_name' => $request->business_name,
                'first_name' => $request->fname,
                'middle_name' => $request->mname,
                'last_name' => $request->lname,
                'firstname' => $request->firstname,
                'middlename' => $request->middlename,
                'lastname' => $request->lastname,
                'other_maritial_status' => $request->other_maritial_status,
                'other_dob_month' => $request->other_dob_month,
                'other_dob_month_s' => $request->other_dob_month_s,

                'other_dob_day' => $request->other_dob_day,
                'other_dob_day_s' => $request->other_dob_day_s,
                'other_spouse_month' => $request->other_spouse_month,
                'other_spouse_day' => $request->other_spouse_day,
                'other_marriage_month' => $request->other_marriage_month,
                'other_marriage_day' => $request->other_marriage_day,
                'other_ethnic' => $request->other_ethnic,
                'other_ethnic_s' => $request->other_ethnic_s,

                'other_main_language' => $request->other_main_language,
                'other_first_language' => $request->other_first_language,
                'other_second_language' => $request->other_second_language,
                'other_first_language1' => $request->other_first_language1,
                'other_second_language1' => $request->other_second_language1,
                'paymentnote' => $request->paymentnote,
                'paymentmode' => $request->paymentmode,
                'bankname' => $bankname,
                'routingno' => $routingno,
                'acountno' => $acountno,
                'acountname' => $acountname,
                'carttype' => $carttype,
                'cartno' => $cartno,

                'paymentnote1' => $request->paymentnote1,
                'paymentmode11' => $request->paymentmode11,
                'bankname1' => $bankname1,
                'routingno1' => $routingno1,
                'acountno1' => $acountno1,
                'acountname1' => $acountname1,
                'carttype1' => $carttype1,
                'cartno1' => $cartno1,

                'business_id' => $request->business_id,
                'contactnametype' => $request->contactnametype,
                'business_cat_id' => $request->business_catagory_name,
                'business_brand_id' => $request->business_brand_name,
                'business_brand_category_id' => $request->business_brand_category_name,
                'address' => $request->address,
                'address1' => $request->address1,
                'countryId' => $request->countryId,
                'city' => $request->city,
                'stateId' => $request->stateId,
                'zip' => $request->zip,
                'mailing_address' => $request->mailing_address,
                'mailing_address_check' => $request->mailing_address_check,

                'billingtoo' => $request->billingtoo,
                'faxbli3' => $request->faxbli3,
                'faxbli1' => $request->faxbli1,
                'emailbli1' => $request->emailbli1,
                'mailing_address1' => $request->mailing_address1,
                'mailing_city' => $request->mailing_city,
                'mailing_state' => $request->mailing_state,
                'mailing_zip' => $request->mailing_zip,
                'email' => $request->email,
                'business_no' => $request->business_no,
                'businesstype' => $request->businesstype,
                'businessext' => $request->businessext,
                'business_fax' => $request->business_fax,
                'mobile_no' => $request->mobile_no,
                'website' => $request->website,
                'minss' => $request->minss,
                'contact_address1' => $request->contact_address1,
                'contact_address2' => $request->contact_address2,
                'contact_title' => $request->contact_title,
                'city_1' => $request->city_1,
                'state_1' => $request->state_1,
                'zip_1' => $request->zip_1,
                'etelephone1' => $request->etelephone1,
                //'etelephone1' => $request->mobile_1,
                'eteletype1' => $request->mobiletype_1,
                'eext1' => $request->ext2_1,
                'etelephone2' => $request->etelephone2,
                'eteletype2' => $request->mobiletype_2,
                'eext2' => $request->ext2_2,
                'email_1' => $contactnos,
                'contact_fax_1' => $request->contact_fax_1,
                'legalname' => $request->legalname,
                'dbaname' => $request->dbaname,
                'business_store_name' => $request->business_store_name,
                'business_address' => $request->business_address,
                'business_country' => $request->business_country,
                'business_city' => $request->business_city,
                'business_state' => $request->business_state,
                'bussiness_zip' => $request->bussiness_zip,
                //'nametype' => $request->nametype,
                'creationdate' => $request->creationdate,
                'user_type' => $request->user_type,
                'billingtoo' => $request->businessaddress,
                'subscription_user' => $request->subscription_user,
                'subscription_question1' => $request->subscription_question1,
                'subscription_question2' => $request->subscription_question2,
                'subscription_question3' => $request->subscription_question3,
                'subscription_answer1' => $request->subscription_answer1,
                'subscription_answer2' => $request->subscription_answer2,
                'subscription_answer3' => $request->subscription_answer3,
                'subscription_lock' => $request->subscription_lock,
                'subscription_active' => $request->subscription_active,
                'subscription_resetdate' => $request->subscription_resetdate,
                'subscription_resetdays' => $request->subscription_resetdays,
                'limited_active' => $request->limited_active,
                'limited_user' => $request->limited_user,
                'limited_question1' => $request->limited_question1,
                'limited_question2' => $request->limited_question2,
                'limited_question3' => $request->limited_question3,
                'limited_answer1' => $request->limited_answer1,
                'limited_answer2' => $request->limited_answer2,
                'limited_answer3' => $request->limited_answer3,
                'limited_resetdate' => $request->limited_resetdate,
                //'subscription_resetdate' => $request->subscription_resetdate,
                'limited_resetdays' => $request->limited_resetdays,
                'user_active' => $request->user_active,
                'user_name' => $request->user_name,
                'user_question1' => $request->user_question1,
                'user_question2' => $request->user_question2,
                'user_question3' => $request->user_question3,
                'user_answer1' => $request->user_answer1,
                'user_answer2' => $request->user_answer2,
                'user_answer3' => $request->user_answer3,
                'user_resetdate' => $request->user_resetdate,
                'user_cell' => $request->user_cell,
                'user_email' => $request->user_email,
                'user_resetdays' => $request->user_resetdays,
                'accounting_period' => $request->accounting_period,
                'productid' => $productsid,

                'payroll_period' => $request->payroll_period,
                'other_period' => $request->other_period,

                //'accessby'=>$accessby_string,
                'adminonly' => $request->adminonly,
                'supervisor' => $request->supervisor,
                'fscee' => $request->fscee,
                'fscuser' => $request->fscuser,
                'client' => $request->client,
                'alluser' => $request->alluser,

                'mobile_1' => $request->mobile_1,
                'mobile_2' => $request->mobile_2

            ]);


        $t = 0;
        // echo "<pre>";print_r($_POST);exit;
        $taxservices1 = $_POST['taxation_service'];

        if (empty($taxservices1)) {

        } else {
            
            $taxcurrency = isset($_POST['currency']) ? $_POST['currency'] : "";
            $taxpricetype = isset($_POST['pricetype']) ? $_POST['pricetype'] : "";
            $taxserviceperiod = isset($_POST['serviceperiod']) ? $_POST['serviceperiod'] : "";
            
            $taxusers = DB::table('client_to_taxation')->where('clientid', '=', $id)->delete();
            $taxation_service_count = count($_POST['taxation_service']);
            
            
            
            foreach ($_POST['taxation_service'] as $taxservice) {

                $taxservice1 = $taxservice;
                $taxserviceperiod1 = isset($_POST['taxation_service_period'][$t]) ? $_POST['taxation_service_period'][$t] : "";

                $taxemp = isset($_POST['clemployee'][$t]) ? $_POST['clemployee'][$t] : "";
                $taxac = isset($_POST['tax_acc_checkby'][$t]) ? $_POST['tax_acc_checkby'][$t] : "";
                $taxnte = isset($_POST['taxationnote'][$t]) ? $_POST['taxationnote'][$t] : "";
                $taxlocked = isset($_POST['taxlocked2'][$t]) ? $_POST['taxlocked2'][$t] : "";

                $taxyears = isset($_POST['taxyears'][$t]) ? $_POST['taxyears'][$t] : "";

                $taxservicemonth = isset($_POST['tax_service_month'][$t]) ? $_POST['tax_service_month'][$t] : "";
                $taxserviceyear = isset($_POST['tax_service_year'][$t]) ? $_POST['tax_service_year'][$t] : "";

                if ($request->business_id == '6') {
                    $taxxyears = $taxyears;
                } else {
                    $taxxyears = $taxserviceyear;
                }
                if (!empty($taxxyears)) {

                    $taxyearsplus = $taxxyears + 1;
                    if ($taxxyears != '2019') {
                        $expiredates = $taxyearsplus . '-04-15';
                    } else {
                        $expiredates = $taxyearsplus . '-07-15';
                    }
                } else {
                    $expiredates = '0000-00-00';
                }

                //  $clienttaxid=$_POST['id'][$t];

                $t++;
                if ($taxservice1 != '') {
                    $taxinsert = DB::insert("insert into client_to_taxation(`tax_service_month`,`tax_service_year`,`expiredate`,`taxyears`,`locked`,`pricetype`,`currency`,`serviceperiod`,`clientid`,`taxation_service`,`taxation_service_period`,`employee_id`,`tax_acc_checkby`,`note`)
                                values('" . $taxservicemonth . "','" . $taxserviceyear . "','" . $expiredates . "','" . $taxxyears . "','" . $taxlocked . "','" . $taxpricetype . "','" . $taxcurrency . "','" . $taxserviceperiod . "','" . $id . "','" . $taxservice1 . "','" . $taxserviceperiod1 . "','" . $taxemp . "','" . $taxac . "','" . $taxnte . "')");
                }
                /* $lastids = DB::getPdo()->lastInsertId();

                 $returnValue = DB::table('client_to_taxation')->where('id','=',$lastids)
              ->update([
                        'locked'=>$taxlocked
                        ]);
                   */

            }


        }


        $noteid = $request->noteid;
        $adminnotes = $request->adminnotes;
        $notetype = $request->notetype;
        $usid = $request->usid;
        $subject = $request->adminsubject;
        $k = 0;
        $users = DB::table('notes')->where('admin_id', '=', $usid)->first();
        foreach ($adminnotes as $notess) {
            $noteid1 = $noteid[$k];
            $note1 = $adminnotes[$k];
            $notetype1 = $notetype[$k];
            $sub1 = $subject[$k];
            $usid1 = $usid[$k];
            $k++;
            if (empty($noteid1)) {
                $insert2 = DB::insert("insert into notes(`notes`,`admin_id`,`type`,`subject`,`userid`) values('" . $note1 . "','" . $usid1 . "','" . $notetype1 . "','" . $sub1 . "','" . $id . "')");

                $users = DB::table('notes')->where('notes', '=', '')->delete();
            } else {
                $returnValue = DB::table('notes')->where('id', '=', $noteid1)
                    ->update(['notes' => $note1,
                        'admin_id' => $usid1,
                        'type' => $notetype1
                    ]);
                $users = DB::table('notes')->where('notes', '=', '')->delete();
            }
        }
        $location_telephone = $request->location_telephone;
        $location_address = $request->location_address;
        $location_zip = $request->location_zip;
        $location_state = $request->location_state;
        $location_city = $request->location_city;
        $location_name = $request->location_name;
        $location_id = $request->location_id;
        $location = $request->location;
        $location_idd = $request->location_idd;
        $ll = 0;
        if (empty($location)) {
        } else {
            foreach ($location as $notess12) {
                $location_telephone1 = $location_telephone[$ll];
                $location_address1 = $location_address[$ll];
                $location_zip1 = $location_zip[$ll];
                $location_state1 = $location_state[$ll];
                $location_city1 = $location_city[$ll];
                $location_name1 = $location_name[$ll];
                $location_id1 = $location_id[$ll];
                $location1 = $location[$ll];
                $location_idd1 = $location_idd[$ll];
                $ll++;
                if (empty($location_idd1)) {
                    $insert2 = DB::insert("insert into newlocations(`location`,`location_id`,`location_name`,`location_city`,`location_state`,`location_zip`,`location_address`,`location_telephone`,`clientid`) values('" . $location1 . "','" . $location_id1 . "','" . $location_name1 . "','" . $location_city1 . "','" . $location_state1 . "','" . $location_zip1 . "','" . $location_address1 . "','" . $location_telephone1 . "','" . $id . "')");
                    $users = DB::table('newlocations')->where('location_id', '=', '')->delete();
                } else {
                    $returnValue = DB::table('newlocations')->where('id', '=', $location_idd1)
                        ->update(['location' => $location1,
                            'location_id' => $location_id1,
                            'location_name' => $location_name1,
                            'location_city' => $location_city1,
                            'location_state' => $location_state1,
                            'location_zip' => $location_zip1,
                            'location_address' => $location_address1,
                            'location_telephone' => $location_telephone1,
                        ]);
                    $users = DB::table('newlocations')->where('location_id', '=', '')->delete();
                }
            }
        }

        $totalprice1 = $request->totalprice1;
        $pricetype = $request->pricetype;
        $currency = $request->currency;
        $serviceperiod = $request->serviceperiod;
        $typeofservice = isset($request->typeofservice) ? $request->typeofservice : "";


        $regularprice = isset($request->regularprice) ? $request->regularprice : "";
        $comboprice = isset($request->comboprice) ? $request->comboprice : "";
        $price = isset($request->price) ? $request->price : "";
        $discountprice = $request->discountprice;
        $pricenote = $request->pricenote;
        $titles = $request->titles;
        $regularprice11 = $request->regularprice1;
        $comboprice11 = $request->comboprice1;
        $serviceid = isset($request->serviceid) ? $request->serviceid : "";
        $taxid = $request->taxid;
        $serviceincludes = isset($request->serviceincludes) ? $request->serviceincludes : "";
        $sign = $request->ckq;
        $taxnote = $request->taxnote;
        if (empty($request->taxprice)) {
            $taxprice = 0;
        } else {
            $taxprice = $request->taxprice;
        }


        $l = 0;
        $countservice = DB::table('clientservices')->where('clientid', '=', $id)->count();
        //  echo "<pre>";print_r($_POST);EXIT;
        if (empty($serviceincludes)) {
        } else {
            $usersdel = DB::table('clientservices')->where('clientid', '=', $id)->delete();
            //  print_r($typeofservice);exit;
            //  echo "<pre>"; print_r($serviceincludes);exit;
            foreach ($regularprice as $typeofservice11) {
                $typeofservice1 = $request->typeofservice[$l];


                // echo "<pre>";print_r($typeofservice2);
                $regularprice1 = $regularprice[$l];
                $comboprice1 = $comboprice[$l];
                $price1 = $price[$l];
                $serviceid1 = $serviceid[$l];
                $serviceincludes1 = $serviceincludes[$l];

                $empnotes1 = isset($request->monthlynote[$l]) ? $request->monthlynote[$l] : "";
                $empids11 = isset($request->employee_r[$l]) ? $request->employee_r[$l] : "";
                $acccheckby1 = isset($request->acc_check_by[$l]) ? $request->acc_check_by[$l] : "";
                $acperiods1 = isset($request->acperiods1[$l]) ? $request->acperiods1[$l] : "";


                $l++;

                if (!empty($acperiods1)) {
                    //print_r($request->acperiods1);
                    // print_r($empnotes);
                    // echo 'aaaa'.$empids1 = $request->employee_r[$l];
                    //   echo $empids1;
                    //exit('1111');
                    $clientservices = DB::insert("insert into clientservices
        (`employeenote`,`employee_id`,`acc_check_by`,`sign`,`total`,`serviceincludes`,`note`,`clientid`,`pricetype`,`currency`,`typeofservice`,
        `serviceperiod`,`regularprice`,`comboprice`,`price`,`discountprice`,`periods`) 
        values
        ('" . $empnotes1 . "','" . $empids11 . "','" . $acccheckby1 . "','" . $sign . "','" . $request->totalprice1 . "','" . $serviceincludes1 . "',
        '" . $request->pricenote . "',
        '" . $id . "','" . $pricetype . "','" . $currency . "','" . $typeofservice1 . "','" . $serviceperiod . "','" . $regularprice1 . "','" . $comboprice1 . "',
        '" . $price1 . "','" . $discountprice . "','" . $acperiods1 . "')");
                    //  $lastid = DB::getPdo()->lastInsertId();


                    //$users = DB::table('clientservices')->where('regularprice','=','')->delete();
                }
                //  else
                //{
                //  exit('2222222');
                // }

                /* else
                 {
                 $returnValue = DB::table('clientservices')->where('id','=',$serviceid1)
                 ->update([
                            //'note' => $pricenote,
                            'pricetype' =>$pricetype,
                            'currency' =>$currency,
                            'serviceperiod'=>$serviceperiod,
                            'typeofservice'=>$typeofservice1,
                            'regularprice'=>$regularprice1,
                            'comboprice'=>$comboprice1,
                            'price'=>$price1,
                            'discountprice'=>$discountprice,
                            'serviceincludes'=> $serviceincludes1,
                            'total' =>$totalprice1,
                            'employee_id' =>$empids,
                            'employeenote' =>$empnotes,
                            'sign' =>$sign]);
                 }*/
            }
            //exit;
            //  ECHO "<PRE>"; print_r($typeofservice11);exit;
        }
        // exit;


        $taxpricetype = $request->pricetype;
        $taxcurrency = $request->currency;
        $titletax = $request->titletax;

        $taxserviceperiod = $request->serviceperiod;
        $taxtypeofservice = isset($request->taxtypeofservice) ? $request->taxtypeofservice : "";
        $taxregularprice = isset($request->taxregularprice) ? $request->taxregularprice : "";
        $taxcomboprice = isset($request->taxcomboprice) ? $request->taxcomboprice : "";
        $taxserviceid = isset($request->taxserviceid) ? $request->taxserviceid : "";
        //$taxid = $request->taxid;
        $taxserviceincludes = isset($request->taxserviceincludes) ? $request->taxserviceincludes : "";
        $taxsign = $request->taxckq;
        //$taxnote = $request->taxnote;

        $taxprice2 = isset($request->taxprice1) ? $request->taxprice1 : "";


        $taxl = 0;
        $countservice = DB::table('taxclientservices')->where('clientid', '=', $id)->count();

        if (empty($taxserviceincludes)) {
        } else {
            $usersdel = DB::table('taxclientservices')->where('clientid', '=', $id)->delete();
            //  print_r($typeofservice);exit;
            foreach ($taxregularprice as $taxregularprice11) {
                $taxtypeofservice1 = isset($request->taxtypeofservice[$taxl]) ? $request->taxtypeofservice[$taxl] : "";

                // echo "<pre>";print_r($typeofservice2);
                $taxregularprice1 = $taxregularprice11;
                $taxcomboprice1 = $taxcomboprice[$taxl];
                $taxprice1 = $taxprice2[$taxl];
                $titletax1 = $titletax[$taxl];
                //$taxserviceid1 = $taxserviceid[$taxl];
                $taxserviceincludes1 = $taxserviceincludes[$taxl];
                $taxempids = isset($request->clemployee[$taxl]) ? $request->clemployee[$taxl] : "";
                $taxacccheckby = isset($request->taxacc_check_by[$taxl]) ? $request->taxacc_check_by[$taxl] : "";
                $taxlocked1 = isset($request->taxlocks[$l]) ? $request->taxlocks[$l] : "";
                $taxempnotes = isset($request->taxmonthlynote[$taxl]) ? $request->taxmonthlynote[$taxl] : "";


                $taxl++;

                if (!empty($titletax1)) {

                    $clientservices = DB::insert("insert into taxclientservices(`taxacc_check_by`,`titletax`,`taxemployee_id`,`taxemployeenote`,`taxsign`,`taxserviceincludes`,`clientid`,`pricetype`,`currency`,`taxtypeofservice`,`serviceperiod`,`taxregularprice`,`taxcomboprice`,`taxprice1`) 
        values('" . $taxacccheckby . "','" . $titletax1 . "','" . $taxempids . "','" . $taxempnotes . "','" . $taxsign . "','" . $taxserviceincludes1 . "','" . $id . "','" . $taxpricetype . "','" . $taxcurrency . "','" . $taxtypeofservice1 . "','" . $taxserviceperiod . "','" . $taxregularprice1 . "','" . $taxcomboprice1 . "','" . $taxprice1 . "')");
                    $lastid1 = DB::getPdo()->lastInsertId();
                    $returnValue = DB::table('taxclientservices')->where('id', '=', $lastid1)
                        ->update([
                            //'note' => $pricenote,
                            'taxemployeenote' => $taxempnotes,
                            'taxemployee_id' => $taxempids,
                            'taxacc_check_by' => $taxacccheckby,
                            'tax_locked' => $taxlocked1
                        ]);

                }
            }
            // print_r($typeofservice2);exit;
        }


        if (isset($request->firstname_sec) != '') {
            $secid = $request->secid;
            $persinfo = count(array_filter($request->firstname_sec));
            for ($i = 0; $i < $persinfo; $i++) {
                if ($request->firstname_sec != '' || $request->firstname_sec != null || $request->firstname_sec != '0') {
                    if (isset($secid[$i]) != '' || isset($secid[$i]) != null || isset($secid[$i]) != '0') {
                        $id1 = $secid[$i];
                    } else {

                    }

                    $userid = isset($request->user_id) ? $request->user_id : "";
                    $nametype_sec = isset($request->nametype_sec[$i]) ? $request->nametype_sec[$i] : "";
                    $firstname_sec = isset($request->firstname_sec[$i]) ? $request->firstname_sec[$i] : "";
                    $middlename_sec = isset($request->middlename_sec[$i]) ? $request->middlename_sec[$i] : "";
                    $lastname_sec = isset($request->lastname_sec[$i]) ? $request->lastname_sec[$i] : "";
                    $contact_address_sec_1 = isset($request->contact_address_sec_1[$i]) ? $request->contact_address_sec_1[$i] : "";
                    $contact_address_sec_2 = isset($request->contact_address_sec_2[$i]) ? $request->contact_address_sec_2[$i] : "";
                    $city_sec = isset($request->city_sec[$i]) ? $request->city_sec[$i] : "";
                    $state_sec = isset($request->state_sec[$i]) ? $request->state_sec[$i] : "";
                    $zip_sec = isset($request->zip_sec[$i]) ? $request->zip_sec[$i] : "";
                    $mobile_sec_1 = isset($request->mobile_sec_1[$i]) ? $request->mobile_sec_1[$i] : "";
                    $mobiletype_sec_1 = isset($request->mobiletype_sec_1[$i]) ? $request->mobiletype_sec_1[$i] : "";
                    $ext_sec_1 = isset($request->ext_sec_1[$i]) ? $request->ext_sec_1[$i] : "";
                    $mobile_sec_2 = isset($request->mobile_sec_2[$i]) ? $request->mobile_sec_2[$i] : "";
                    $mobiletype_sec_2 = isset($request->mobiletype_sec_2[$i]) ? $request->mobiletype_sec_2[$i] : "";
                    $ext_sec_2 = isset($request->ext_sec_2[$i]) ? $request->ext_sec_2[$i] : "";
                    $contact_fax_sec = isset($request->contact_fax_sec[$i]) ? $request->contact_fax_sec[$i] : "";
                    $email_sec = isset($request->email_sec[$i]) ? $request->email_sec[$i] : "";
                    $sec_title = isset($request->sec_title[$i]) ? $request->sec_title[$i] : "";
                    $user_access = isset($request->user_access[$i]) ? $request->user_access[$i] : "";


                    $returnValue11 = DB::insert("insert into contact_userinfos(`user_id`,`nametype_sec`,`firstname_sec`,`middlename_sec`,`lastname_sec`,`sec_title`,`contact_address_sec_1`,`contact_address_sec_2`,`city_sec`,`state_sec`,`zip_sec`,`mobile_sec_1`,`mobiletype_sec_1`,`ext_sec_1`,`mobile_sec_2`,`mobiletype_sec_2`,`ext_sec_2`,`contact_fax_sec`,`email_sec`,`user_access`) 
                         values('" . $userid . "','" . $nametype_sec . "','" . $firstname_sec . "','" . $middlename_sec . "','" . $lastname_sec . "','" . $sec_title . "','" . $contact_address_sec_1 . "','" . $contact_address_sec_2 . "','" . $city_sec . "','" . $state_sec . "','" . $zip_sec . "','" . $mobile_sec_1 . "','" . $mobiletype_sec_1 . "','" . $ext_sec_1 . "','" . $mobile_sec_2 . "','" . $mobiletype_sec_2 . "','" . $ext_sec_2 . "','" . $contact_fax_sec . "','" . $email_sec . "','" . $user_access . "')");
                    if (empty($secid)) {

                    } else {
                        $returnValue11111 = DB::table('contact_userinfos')->where('id', '=', $id1)->delete();
                    }
                }
            }
        }


        if ($returnValue11 > 0) {
            //  return response()->json([
            //    'status'     => 'success',
            //   'newopt1'     => $request->fileno]);

            //return redirect('fac-Bhavesh-0554/customer');
            return redirect('fac-Bhavesh-0554/customer/' . $id . '/edit')->with('success', 'Your Profile Successfully Update');

            //return redirect(route('customer.index'))->with('success','Your Profile Successfully Update');

        } else {
            return redirect('fac-Bhavesh-0554/customer/' . $id . '/edit')->with('success', 'Your profile updated successfully!');

        }


    }


//mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm


    public function getcategory(Request $request)
    {
        $data = Category::select('business_cat_name', 'id')->where('bussiness_name', $request->id)->orderby('business_cat_name', 'asc')->take(1000)->get();
        return response()->json($data);
    }

    public function getbankdata(Request $request)
    {
        // echo $request->id;
//$data = DB::table('client_bank')->where('id',$request->ccid)->orderby('bank_name','asc')->get();

        $data = DB::table('client_bank')->select('client_bank.id as ccid', 'client_bank.nick_name', 'client_bank.routingno', 'client_bank.fourdigit', 'client_bank.client_id', 'client_bank.bank_name', 'bankmaster.id', 'bankmaster.id', 'bankmaster.bankname')
            ->leftJoin('bankmaster', function ($join) {
                $join->on('bankmaster.id', '=', 'client_bank.bank_name');
            })->
            where('client_bank.fourdigit', '=', $request->id)->first();


        return response()->json($data);
    }

    public function locked2(Request $request)
    {
        // print_r($request);EXIT;
        $id = $request->id;
        $serviceid = $request->serviceid;
        $servalue = $request->lock;
        $cid = $request->clientid;

        $data = DB::table('clientservices')->where('id', $id)->update(['locked' => $servalue]);

        return response()->json($data);
//return 'success';
    }


    public function updatecoversation(Request $request)
    {
        //print_r($_REQUEST);EXIT;

        $this->validate($request, [
            'type_user' => 'required',
            'conrelatedname' => 'required',
        ]);


        $CovID = $request->CovID;
        $date = $request->date;
        $day = $request->day;
        $time = $request->time;
        $type_user = $request->type_user;
        if ($request->clientid != '') {
            $clientid = $request->clientid;
        } else {
            $clientid = '';
        }

        if ($request->employeeuserid != '') {
            $employeeuserid = $request->employeeuserid;
        } else {
            $employeeuserid = '';
        }

        if ($request->vendorid != '') {
            $vendorid = $request->vendorid;
        } else {
            $vendorid = '';
        }

        if ($request->otherid != '') {
            $otherid = $request->otherid;
        } else {
            $otherid = '';
        }

        $conrelatedname = $request->conrelatedname;
        $condescription = $request->condescription;
        $connotes = $request->connotes;

        $returnValue = DB::table('conversation_sheet')->where('id', $CovID)
            ->update([
                'creattiondate' => $date,
                'day' => $day,
                'time' => $time,
                'type_user' => $type_user,
                'clientid' => $clientid,
                'employeeuserid' => $employeeuserid,
                'vendorid' => $vendorid,
                'otherid' => $otherid,
                'conrelatedname' => $conrelatedname,
                'condescription' => $condescription,
                'connotes' => $connotes,

            ]);


        return redirect()->back()->with('success', 'Record Updated Successfully');
    }

    public function updatenotes(Request $request)
    {
        // echo "<pre>";
        // print_r($_REQUEST);die;

        $this->validate($request, [
            'notetype_user' => 'required',
            'notesrelatedcat' => 'required',
        ]);


        $NoteID = $request->NoteID;
        $notedate = $request->notedate;
        $noteday = $request->noteday;
        $notetime = $request->notetime;
        $notetype_user = $request->notetype_user;
        if ($request->noteclientid != '') {
            $noteclientid = $request->noteclientid;
        } else {
            $noteclientid = '';
        }

        if ($request->noteemployeeuserid != '') {
            $noteemployeeuserid = $request->noteemployeeuserid;
        } else {
            $noteemployeeuserid = '';
        }

        if ($request->notevendorid != '') {
            $notevendorid = $request->notevendorid;
        } else {
            $notevendorid = '';
        }

        if ($request->noteotherid != '') {
            $noteotherid = $request->noteotherid;
        } else {
            $noteotherid = '';
        }

        //$notesrelatedname=$request->notesrelatedname;
        $notesrelatedcat = $request->notesrelatedcat;
        $notes = $request->notes;

        $returnValue = DB::table('notes_sheet')->where('id', $NoteID)
            ->update([
                'creattiondate' => $notedate,
                'noteday' => $noteday,
                'notetime' => $notetime,
                'notetype_user' => $notetype_user,
                'noteclientid' => $noteclientid,
                'noteemployeeuserid' => $noteemployeeuserid,
                'notevendorid' => $notevendorid,
                'noteotherid' => $noteotherid,
                //'notesrelatedname' =>$notesrelatedname,
                'notesrelatedcat' => $notesrelatedcat,
                'notes' => $notes,

            ]);


        return redirect()->back()->with('success', 'Record Updated Successfully');
    }


    public function updateemployee(Request $request)
    {
        // print_r($request);EXIT;
        $id = $request->id;
        $serviceid = $request->empid;


        $data = DB::table('clientservices')->where('id', $id)->update(['employee_id' => $serviceid]);

        return response()->json($data);
//return 'success';
    }

    public function updateacccheckby(Request $request)
    {
        // print_r($request);EXIT;
        $id = $request->id;
        $serviceid = $request->empid;


        $data = DB::table('clientservices')->where('id', $id)->update(['acc_check_by' => $serviceid]);

        return response()->json($data);
//return 'success';
    }


    public function taxupdateemployee(Request $request)
    {
        // print_r($request);EXIT;
        $id = $request->id;
        $serviceid = $request->empid;


        $data = DB::table('client_to_taxation')->where('id', $id)->update(['employee_id' => $serviceid]);

        return response()->json($data);
//return 'success';
    }

    public function taxupdateacccheckby(Request $request)
    {
        // print_r($request);EXIT;
        $id = $request->id;
        $serviceid = $request->empid;


        $data = DB::table('client_to_taxation')->where('id', $id)->update(['tax_acc_checkby' => $serviceid]);

        return response()->json($data);
//return 'success';
    }

    public function taxlocked2(Request $request)
    {
        // print_r($request);EXIT;
        $id = $request->id;
        $serviceid = $request->serviceid;
        $servalue = $request->lock;
        $cid = $request->clientid;

        $data = DB::table('client_to_taxation')->where('id', $id)->update(['locked' => $servalue]);

        return response()->json($data);
//return 'success';
    }


    public function getcategory_1(Request $request)
    {
        $data = Category::select('business_cat_name', 'id', 'business_cat_image')->where('id', $request->id)->orderby('business_cat_name', 'asc')->take(1000)->get();
        return response()->json($data);
    }

    public function getnaics(Request $request)
    {
        $data = Category::select('business_cat_name', 'id', 'business_cat_image', 'naics', 'sic')->where('id', $request->naics)->first();
        return response()->json($data);
    }

    public function getcategory1(Request $request)
    {
        $data = Business::select('bussiness_name', 'id', 'bussiness_image_name', 'newimage')->where('id', $request->id)->orderby('bussiness_name', 'asc')->take(1000)->get();
        return response()->json($data);
    }

    public function getcategory1m(Request $request)
    {
        // echo $request->id;
        $data = BusinessBrand::select('business_brand_name', 'id')->where('business_cat_id', $request->id)->orderby('business_brand_name', 'asc')->take(1000)->get();
        return response()->json($data);
    }

    public function getcategoryimage(Request $request)
    {
        $data = BusinessBrand::select('business_brand_name', 'id', 'business_brand_image')->where('id', $request->id)->orderby('business_brand_name', 'asc')->take(1000)->get();
        return response()->json($data);
    }

    public function getcategory1mm(Request $request)
    {
        $data = Categorybusiness::select('business_brand_category_name', 'id')->where('business_brand_id', $request->id)->orderby('business_brand_category_name', 'asc')->take(100)->get();
        return response()->json($data);
    }

    public function business_brand_category_name(Request $request)
    {
        $data = Categorybusiness::select('business_brand_category_name', 'id', 'business_brand_category_image')->where('id', $request->id)->orderby('business_brand_category_name', 'asc')->take(100)->get();
        return response()->json($data);
    }

    public function getsign3(Request $request)
    {
        $data = DB::table('prices')->select('currency', 'id', 'typeofservice', 'period')->where('serviceperiod', $request->serviceperiod)->where('currency', $request->currency)->take(1000)->get();
        return response()->json($data);
    }


    public function salestax1(Request $request)
    {
        if ($request->typeofservice == '3') {
            $data = DB::table('taxationservice')->select('regularprice1', 'id', 'comboprice1', 'serviceincludes1')->where('currency', $request->currency)->where('typeofservice', $request->typeofservice)->where('period', $request->serviceperiod)->where('title', $request->id)->take(100)->get();
        } else if ($request->typeofservice == '10') {
            $data = DB::table('prices')->select('regularprice', 'id', 'comboprice', 'serviceincludes')->where('currency', $request->currency)->where('typeofservice', $request->id)->take(100)->get();
        } else {
            $data = DB::table('prices')->select('regularprice', 'id', 'comboprice', 'serviceincludes')->where('currency', $request->currency)->where('typeofservice', $request->id)->where('period', $request->serviceperiod)->take(100)->get();
        }
        return response()->json($data);
    }

    public function salestaxacc(Request $request)
    {
        if ($request->typeofservice == '3') {
            $data = DB::table('taxationservice')->select('regularprice1', 'id', 'comboprice1', 'serviceincludes1')->where('currency', $request->currency)->where('typeofservice', $request->typeofservice)->where('period', $request->id)->where('title', $request->id)->take(100)->get();
        } else if ($request->typeofservice == '10') {
            $data = DB::table('prices')->select('regularprice', 'id', 'comboprice', 'serviceincludes')->where('currency', $request->currency)->where('typeofservice', $request->id)->take(100)->get();
        } else {

            $data = DB::table('prices')->select('regularprice', 'id', 'comboprice', 'serviceincludes', 'typeofservice', 'currency', 'period')->where('currency', $request->currency)->where('typeofservice', $request->typeofservice)->where('period', $request->id)->take(100)->first();
        }
//print_r($data);
        return response()->json($data);
    }


    public function salestaxpay(Request $request)
    {

        $data = DB::table('prices')->select('regularprice', 'id', 'comboprice', 'payrollservice', 'serviceincludes', 'typeofservice', 'currency', 'period')->where('currency', $request->currency)->where('payrollservice', $request->typeofservice)->take(100)->first();
        return response()->json($data);
    }

    public function salestaxations(Request $request)
    {

        $data = DB::table('taxationservice')->select('taxationservice.currency', 'taxationservice.period', 'taxationservice.typeofservice', 'taxationservice.regularprice1', 'taxationservice.title', 'taxationservice.id', 'taxationservice.comboprice1', 'taxationservice.serviceincludes1', 'taxtitles.id as taxxid', 'taxtitles.title as taxtitle')->
        leftJoin('taxtitles', function ($join) {
            $join->on('taxtitles.id', '=', 'taxationservice.title');
        })->
        where('taxationservice.currency', $request->currency)->where('taxationservice.typeofservice', 3)->where('taxationservice.period', $request->serviceperiod)->where('taxationservice.title', $request->title)->take(100)->first();
        return response()->json($data);


    }

    public function deletecon(Request $request)
    {
        $users = DB::table('contact_userinfos')->where('id', '=', $request->input('id'))->delete();
        if ($users) {
            echo 'Data Deleted';
        }
    }

    public function notedelete($id)
    {
        $uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $uri_segments = explode('/', $uri_path);
        $urisegment = $uri_segments[3];
        $getdata = DB::table('notes')->where('id', '=', $urisegment)->first();

        // Contact_userinfo::where('id',$id)->delete();
        DB::table('notes')->where('id', '=', $id)->delete();
        // return redirect('fac-Bhavesh-0554/customer/');
        return redirect('fac-Bhavesh-0554/customer/' . $getdata->userid . '/edit')->with('success', 'Your Record Deleted Successfully');

    }

    public function locksdelete($id)
    {
        $uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $uri_segments = explode('/', $uri_path);
        $urisegment = $uri_segments[3];

        DB::table('clientservices')->where('id', '=', $id)->delete();

        return redirect('fac-Bhavesh-0554/customer/' . $urisegment . '/edit')->with('success', 'Your Record Deleted Successfully');

        // return redirect('fac-Bhavesh-0554/customer/');
    }


    public function destroy($id)
    {
        Commonregister::where('id', $id)->delete();
        $affectedRows = Contact_userinfo::where('user_id', '=', $id)->delete();
        $user = User::where('user_id', '=', $id)->delete();
        return redirect(route('customer.index'))->with('error', 'Your Record Deleted Successfully');
    }


    public function destroyinfo($id)
    {

        $uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $uri_segments = explode('/', $uri_path);
        $urisegment = $uri_segments[3];

        $affectedRows = Contact_userinfo::where('id', '=', $id)->delete();
        // return redirect('fac-Bhavesh-0554/customer/'.$urisegment.'/edit')->with('success','Your Record Deleted Successfully');

        return redirect()->back()->with('success', 'Your Record Deleted Successfully');

    }

    public function destroytax($id)
    {


        $uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $uri_segments = explode('/', $uri_path);
        $urisegment = $uri_segments[3];
        $getdata = DB::table('client_to_taxation')->where('id', '=', $id)->first();
        DB::table('client_to_taxation')->where('id', '=', $id)->delete();
        DB::table('taxclientservices')->where('clientid', '=', $getdata->clientid)->where('titletax', '=', $getdata->taxation_service)->delete();


        return redirect()->back()->with('success', 'Your Record Deleted Successfully');

    }


}