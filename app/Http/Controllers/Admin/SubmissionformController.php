<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Front\Commonregister;
use App\Model\BusinessBrand;

use App\Model\Category;
use App\Model\Business;
use App\Model\Employee;
use App\Model\Task;

use App\Model\client_professional;
use App\Model\client_shareholder;
use App\User;
use App\Model\Categorybusiness;
use App\Model\Contact_userinfo;
use App\Mail\Activationmail;
use App\Model\taxstate;
use DB;
use Hash;
use Carbon;
use App\Submissionlogin\Submissionlogin;
use App\Model\Admin;
use Auth;
use App\Model\Price;
use App\Model\Currency;
use App\Model\Typeofser;
use App\Model\Period;
use App\Model\Taxtitle;
use App\Model\Language;
use App\Model\Ethnic;

use App\employees\Fscemployee;
use Illuminate\Support\Facades\Input;
use App\Model\Logo;

class SubmissionformController extends Controller
{
public function __construct()
    {
    $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   
    
    public function index(Request $request)
    {

        $common = DB::table('submitform')->select('submitform.id as sid','submitform.submit_id','submitform.name','submitform.telephone','submitform.email',
        'submitform.requestform','submitform.note','submissions.id','submissions.submission_name')->leftJoin('submissions', function($join){ $join->on('submissions.id', '=', 'submitform.submit_id');})
    ->where('submitform.submit_id','=',$_REQUEST['id'])->orderBy('submitform.id', 'desc')->get();
    
         

        return view('fac-Bhavesh-0554/submissionform/submissionform',compact(['common']));
    }
    
    
    
    public function edit(Request $request)
    {
        exit('1111111111');
              return view('fac-Bhavesh-0554/submissionform/edit',compact([]));
   
    } 
   
    

    public function update(Request $request)
    {

        $this->validate($request,[                  
            'formation_yearbox'=>'required',   
            'formation_yearvalue'=>'required',   
            'record_status'=>'required',
        ]); 
        
        
        $id=$request->formationid;
        $client_id=$request->client_id;
        $formation_yearbox = $request->formation_yearbox;
        $formation_yearvalue =$request->formation_yearvalue;
        $formation_amount = $request->formation_amount;
        $formation_payment = $request->formation_payment;
        $record_status = $request->record_status;

        $path1= public_path().'/adminupload/'.$_FILES['annualreceipt']['name'];
        $path2= public_path().'/adminupload/'.$_FILES['formation_work_officer']['name'];
        if(move_uploaded_file($_FILES['annualreceipt']['tmp_name'], $path1)) 
        {
           $filesname1= $_FILES['annualreceipt']['name']; 
        } 
        else
        {
           $filesname1= $_POST['annualreceipt_1']; 
        }
        $annualreceipt=$filesname1;
        
        if(move_uploaded_file($_FILES['formation_work_officer']['tmp_name'], $path2)) 
        {
           $filesname2= $_FILES['formation_work_officer']['name']; 
        } 
        else
        {
           $filesname2= $_POST['officers']; 
        }
        $formation_work_officer=$filesname2;
        
        $record_paid_by = $request->record_paid_by;
        $formation_penalty = $request->formation_penalty;
        $work_processingfees = $request->work_processingfees;
        $record_totalamt = $request->record_totalamt;
        $paiddate = $request->paiddate;
        $record_note = $request->record_note;
         
        $returnValue = DB::table('client_formation')->where('id', '=', $id)
        ->update([ 
                   'client_id' => $client_id,
                   'formation_yearbox' => $formation_yearbox,
                   'formation_yearvalue' =>$formation_yearvalue,
                   'formation_amount' =>str_replace("$","",$formation_amount),
                   'formation_payment' =>$formation_payment,
                   'record_status' =>$record_status,
                   'annualreceipt' =>$annualreceipt,
                   'formation_work_officer' =>$formation_work_officer,
                   
                   'record_paid_by' =>$record_paid_by,
                   'formation_penalty' =>str_replace("$","",$formation_penalty),
                   'work_processingfees' =>str_replace("$","",$work_processingfees),
                   'record_totalamt' =>str_replace("$","",$record_totalamt),
                   'paiddate' =>$paiddate,
                   'record_note' =>$record_note,
            ]);
        return redirect()->back()->with('success','Formation Successfully Added');
    }
    public function update1(Request $request)
    {
 exit('11111111');
        $id=$request->id;
       
        return redirect()->back()->with('success','Formation Successfully Added');
    }
    
    
    public function addformation(Request $request)
    {
        // echo "<pre>";
        // print_r($_POST);die;
        $this->validate($request,[                  
            'formation_yearbox'=>'required',   
            'formation_yearvalue'=>'required',   
            //'formation_amount'=>'required',   
            //'formation_payment'=>'required',   
            'record_status'=>'required',
        ]); 
        
        $client_id=$request->formation_client_id;
        
        if(isset($request->formation_yearbox)!='')
        {
            $formation_yearbox = $request->formation_yearbox;
        }
        else
        {
            $formation_yearbox = $request->formation_yearbox_2;
        }
        
        
        if(isset($request->formation_yearvalue)!='')
        {
            $formation_yearvalue =$request->formation_yearvalue;    
        }
        else
        {
            $formation_yearvalue =$request->formation_yearvalue_2;    
        }
        
        
        if(isset($request->formation_amount)!='')
        {
            $formation_amount = $request->formation_amount;
        }
        else
        {
            $formation_amount = $request->formation_amount_2;
        }
        
        
        if(isset($request->formation_payment)!='')
        {
            $formation_payment = $request->formation_payment;
        }
        else
        {
            $formation_payment = '';
        }
        $record_status = $request->record_status;
        
        
        
        $record_paid_by = $request->record_paid_by;
        
        if(isset($request->formation_penalty)!='')
        {
            $formation_penalty = $request->formation_penalty;
        }
        else
        {
            $formation_penalty = '';
        }
        
        if(isset($request->work_processingfees)!='')
        {
            $work_processingfees = $request->work_processingfees;
        }
        else
        {
            $work_processingfees = '';
        }
        
        if(isset($request->record_totalamt)!='')
        {
            $record_totalamt = $request->record_totalamt;
        }
        else
        {
            $record_totalamt = '';
        }
        
        if(isset($request->paiddate)!='')
        {
            $paiddate = $request->paiddate;
        }
        else
        {
            $paiddate = '';
        }
        
        
        
        $path1= public_path().'/adminupload/'.$_FILES['annualreceipt']['name'];
        $path2= public_path().'/adminupload/'.$_FILES['formation_work_officer']['name'];
        if(move_uploaded_file($_FILES['annualreceipt']['tmp_name'], $path1)) 
        {
           $filesname1= $_FILES['annualreceipt']['name']; 
        } 
        if(isset($filesname1))
        {
        $annualreceipt=$filesname1;
        }
        else
        {
            $annualreceipt='';
        }
        
        if(move_uploaded_file($_FILES['formation_work_officer']['tmp_name'], $path2)) 
        {
           $filesname2= $_FILES['formation_work_officer']['name']; 
        }
        
        if(isset($filesname2))
        {
        $formation_work_officer=$filesname1;
        }
        else
        {
            $formation_work_officer='';
        }
        
       // $formation_work_officer=$filesname2;
        $record_note = $request->record_note;

        $returnValue = DB::table('client_formation')
        ->insert([ 
                   'client_id' => $client_id,
                   'formation_yearbox' => $formation_yearbox,
                   'formation_yearvalue' =>$formation_yearvalue,
                   'formation_amount' =>str_replace("$","",$formation_amount),
                   'formation_payment' =>$formation_payment,
                   'record_status' =>$record_status,
                   'annualreceipt' =>$annualreceipt,
                   'formation_work_officer' =>$formation_work_officer,
                   
                   'record_paid_by' =>$record_paid_by,
                   'formation_penalty' =>str_replace("$","",$formation_penalty),
                   'work_processingfees' =>str_replace("$","",$work_processingfees),
                   'record_totalamt' =>str_replace("$","",$record_totalamt),
                   'paiddate' =>$paiddate,
                   'record_note' =>$record_note,
            ]);
        return redirect()->back()->with('success','Formation Successfully Added');
    }
    
    public function destroy(Request $request,$id)
    {
        //echo $id;die;
        DB::table('client_formation')->where('id','=',$id)->delete();
        return redirect()->back()->with('success','Your Formation Successfully Deleted');
    }
    
    public function updatedocuments(Request $request)
    {
        //print_r($_REQUEST);die;
        // print_r($_POST);die;
       
        $idss=$request->idaa;       
        $filename_id=$request->filename_idss;
        $client_id=$request->client_id;
        $documentsname=$request->documentsname; 
        $path1= public_path().'/clientupload/'.$_FILES['clientdocument']['name'];
        if(isset($_FILES['clientdocument']['name'])!='')
        {
            if(move_uploaded_file($_FILES['clientdocument']['tmp_name'], $path1)) 
            {
               $filesname1= $_FILES['clientdocument']['name']; 
            }
            else
            {
                $filesname1=$_POST['clientdocument_1'];
            }
         
        }
        else
        {
            $filesname1=$_POST['clientdocument_1'];
        }
        
        $returnValue = DB::table('clienttodocument')->where('id', '=', $idss)
        ->update([ 
                   'filename_id' => $filename_id,
                   'client_id' => $client_id,
                   'documentsname' =>$documentsname,
                   'clientdocument' =>$filesname1
            ]);     

         //return redirect(route('workstatus.index'))->with('success','Your Document Successfully Updated');
        // return redirect('https://')->with('success','Your Document Successfully Updated');
         
         return redirect('fac-Bhavesh-0554/workstatus?id='.$client_id)->with('success','Your Document Successfully Updated');;
         
    }

   
  
   
   
}
