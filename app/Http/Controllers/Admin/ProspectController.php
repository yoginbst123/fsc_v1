<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Model\Branch;
use App\Model\Prospect;
use App\Model\ProspectSub;
use App\Model\Proposal_client_details;
use App\Model\Service;
use App\Model\Position;
use DB;
use Mail;


class ProspectController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $getProposal = DB::table('proposal_client_detail')
            ->select(
                'proposal_client_detail.id as pid',
                'proposal_client_detail.created_at',
                'proposal_client_detail.client_name',
                'proposal_client_detail.telephone',
                'proposal_client_detail.status',
                'typeofser.typeofservice as service_name',
                'typeofser.id as sid'
                )
            ->join('typeofser', function ($join) {
                $join->on('proposal_client_detail.type_of_service', '=', 'typeofser.id');
            })->get();
           

        return view('fac-Bhavesh-0554/prospect/prospect', compact('getProposal'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$position = Position::All();
        $service = Service::All();
        return view('fac-Bhavesh-0554/prospect/create', compact(['service']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }
    
    public function storeProposalClient(Request $request)
    {

        // $this->validate($request, [
        //     'client' => 'required',
        //     'praposal_client_name' => 'required',
        //     'praposal_client_telephone' => 'required',
        //     'praposal_client_email' => 'required',
        //     'prospect_service' => 'required',

        //     'priority' => 'required',
        //     'duedate' => 'required',
        //     'scope_of_work' => 'required',
        //     'fsc_fee' => 'required',
        // ]);

        $proposal_client = new Proposal_client_details;
        $user = $request->session()->get('key');
        $pid = $proposal_client->id;

        $proposal_client->type_of_client  = $request->client_type;
        
        if($request->client_type == 2){
            $proposal_client->client_id  = $request->proposal_client_id;
        }else{
            $proposal_client->proposal_id  = $request->proposal_client_id;
        }
        $ency_id = password_hash($request->proposal_client_id, PASSWORD_DEFAULT);
        $proposal_client->client_name  = $request->praposal_client_name;
        $proposal_client->telephone  = $request->praposal_client_telephone;
        $proposal_client->email  = $request->praposal_client_email;
        
        $proposal_client->client_address  = $request->praposal_client_address;
        $proposal_client->city  = $request->praposal_client_city;
        $proposal_client->state  = $request->praposal_client_state;
        $proposal_client->zip  = $request->praposal_client_zip;
    
        $proposal_client->type_of_service  = $request->prospect_service;
        $proposal_client->service_period  = $request->period_service;
        $proposal_client->priority  = $request->priority;
        $proposal_client->duedate  = date('Y-m-d', strtotime($request->duedate));
        $proposal_client->scope_of_work  = $request->scope_of_work;
        
        $proposal_client->fsc_fee  = $request->fsc_fee;
        $proposal_client->discount  = $request->discount;
        $proposal_client->adv_payment  = $request->adv_payment;
        $proposal_client->descriptions  = $request->description;
        $proposal_client->send_by= Auth::user()->minss.' '. Auth::user()->fname .' '.Auth::user()->mname.' '. Auth::user()->lname;
        $proposal_client->status=0;
        $proposal_client->ency_id = str_replace('/','',$ency_id);
        $proposal_client->save();
        
        $fsc_fee_v = $request->fsc_fee;
        $discount_v = $request->discount;
        $adv_paym_v = $request->adv_payment;
        
        $fsc_fee_d = substr($fsc_fee_v,0,strlen($fsc_fee_v)-3);
        $discount_d = substr($discount_v,0,strlen($discount_v)-3);
        $adv_paym_d = substr($adv_paym_v,0,strlen($adv_paym_v)-3);
        
        $due_bal = 0.0; //$fsc_fee_d-$discount_d-$adv_paym_d;
        $due_bal = (int)$fsc_fee_d-(int)$discount_d-(int)$adv_paym_d;
        $generate_url ="https://financialservicecenter.net/propspectAccept/".str_replace('/','',$ency_id);
        
        $pid = $proposal_client->id;
        
        $data = array(
                'email' => $request->praposal_client_email,
                'client_name'  => $request->praposal_client_name,
                'telephone'  => $request->praposal_client_telephone,
                'email'  => $request->praposal_client_email,
                'client_address'  => $request->praposal_client_address,
                'city'  => $request->praposal_client_city,
                'state'  => $request->praposal_client_state,
                'zip'  => $request->praposal_client_zip,
                'type_of_service'  => $request->prospect_service,
                'service_period'  => $request->period_service,
                'priority'  => $request->priority,
                'duedate'  => date('M/d/Y', strtotime($request->duedate)),
                'scope_of_work'  => $request->scope_of_work,
                'fsc_fee'  => $request->fsc_fee,
                'discount'  => $request->discount,
                'adv_payment'  => $request->adv_payment,
                'due_bal'=>$due_bal,
                'description'=>$request->description,
                'link'=>$generate_url,
                'pid'=>$pid
            );
        
        Mail::send('proposalmail', $data, function($message) use ($data) {
            $message->to($data['email'])
                    ->from(
                        'info@financialservicecenter.net',
                        $data['client_name'],
                        $data['telephone'],
                        $data['email'],
                        $data['client_address'],
                        $data['city'],
                        $data['state'],
                        $data['zip'],
                        $data['type_of_service'],
                        $data['service_period'],
                        $data['priority'],
                        $data['duedate'],
                        $data['scope_of_work'],
                        $data['fsc_fee'],
                        $data['discount'],
                        $data['adv_payment'],
                        $data['due_bal'],
                        $data['description'],
                        $data['link'],
                        $data['pid']
                )->subject('FSC Proposal');
        });

        // $subservices = $request->selective_services;
        // $countof = explode(',', $subservices);

        // for ($i = 0; $i < count($countof); $i++) {
        //     $proposal_client_sub = new Proposal_client_detail_services;
        //     $proposal_client_sub->proposal_client_id = $pid;
        //     $proposal_client_sub->service_id = $request->prospect_service;
        //     $proposal_client_sub->sub_service_id = $countof[$i];
        //     $proposal_client_sub->save();
        // }

        return redirect('fac-Bhavesh-0554/prospect/')->with('success', 'Success fully Update Branch');
    }
    
    public function accept_proposal(Request $request)
    {
        $id = $request->id;
        $getProposal = DB::table("proposal_client_detail")->Where('ency_id',$id)->Where('status',0)->first();
        $counts = $getProposal->id;
        
        $query = DB::table('proposal_client_detail')
                ->where('id', $counts) 
                ->update(array('status' => 1)); 
        
        echo $query;
        exit();
        
    }
    
    public function getproposalDetailsForAccept($id)
    {
        $getProposal = DB::table("proposal_client_detail")->Where('ency_id',$id)->Where('status',0)->first();
    }
    
    public function getGeneratePid()
    {
        //$proposal_client = new Proposal_client_details;
        $proposal = DB::table('proposal_client_detail')
                    ->select('proposal_client_detail.proposal_id')->whereNotNull('proposal_id')->get();
        $wordCount = $proposal->count() + 1;
        $genId="";
        $ldigit = strlen($wordCount);
        if($ldigit == 1){
            $genId ="000".$wordCount;
        }else if($ldigit == 2){
            $genId = "00".$wordCount;
        }else if($ldigit == 3){
            $genId = "0".$wordCount;
        }else if($ldigit ==4){
            $genId = $wordCount;
        }
        echo "Pro-".date('y')."-".$genId;
    }
    
    
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $getProposal = DB::table('proposal_client_detail')
            ->select(
                    'proposal_client_detail.id as pid',
                    'proposal_client_detail.created_at',
                    'proposal_client_detail.client_name',
                    'proposal_client_detail.telephone',
                    'proposal_client_detail.email',
                    'proposal_client_detail.priority',
                    'proposal_client_detail.fsc_fee',
                    'proposal_client_detail.duedate',
                    'proposal_client_detail.status',
                    'typeofser.typeofservice as service_name',
                    'typeofser.id as sid'
                )
            ->join('typeofser', function ($join) {
                $join->on('proposal_client_detail.type_of_service', '=', 'typeofser.id');
            })->Where('proposal_client_detail.id',$id)->first();
            
        $result = array();
        $subResult = array();
        array_push($result,array(
            'pid'=>$getProposal->pid,
            'proposal_created'=>date('M/d/Y',strtotime($getProposal->created_at)),
            'proposal_day'=>date('l',strtotime($getProposal->created_at)),
            'client_name'=>$getProposal->client_name,
            'telephone'=>$getProposal->telephone,
            'email'=>$getProposal->email,
            'fsc_fee'=>$getProposal->fsc_fee,
            'priority'=>$getProposal->priority,
            'duedate'=>$getProposal->duedate,
            'status'=>$getProposal->status,
            
            'service_name'=>$getProposal->service_name,
            'sid'=>$getProposal->sid,
        ));
        
        // $getsubServiceList = DB::table('proposal_client_detail_services')
        //     ->Where('service_id',$getProposal->sid)
        //     ->Where('proposal_client_id',$getProposal->pid)->get();
            
        // foreach($getsubServiceList as $subservice):
            
        //     $firstService = substr($subservice->sub_service_id,0,1);
        //     $secondServiceId = substr($subservice->sub_service_id,1,1);
        //     if($firstService == 'm')
        //     {
        //         $getsubProposal = DB::table('prospect_services')->Where('id',$secondServiceId)->get();
        //         foreach($getsubProposal as $subProposal):
        //             array_push($subResult,array(
        //                 'name'=>$subProposal->name
        //             ));
        //         endforeach;
                
        //     }
        //     else if($firstService == 's')
        //     {
        //         $getsubProposal = DB::table('prospect_services_sub')->Where('id',$secondServiceId)->get();
        //         foreach($getsubProposal as $subProposal):
        //             $getsubProposals = DB::table('prospect_services')->Where('id',$subProposal->service_sub_id)->first();
        //             array_push($subResult,array(
        //                 'name'=>$subProposal->name,
        //                 'Parent_name'=>$getsubProposals->name
        //             ));
        //         endforeach;
        //     }
            
        // endforeach;
        array_push($result,array("Services"=>$subResult));
        $json = json_encode(array("result"=>$result));
        return view('fac-Bhavesh-0554/prospect/info',compact(['id','json']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $branch = Branch::where('id', $id)->first();
        $position = Position::All();
        return view('fac-Bhavesh-0554.branch.edit', compact(['branch', 'position']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'country' => 'required',
            'city' => 'required',
            'branch' => 'required',
            'branchtype' => 'required',
        ]);
        $branch  = Branch::find($id);
        $branch->positionid = $request->branchtype;
        $branch->branchname = $request->branch;
        $branch->city = $request->city;
        $branch->country = $request->country;
        $branch->update();
        return redirect('fac-Bhavesh-0554/branch')->with('success', 'Success fully Update Branch');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Branch::where('id', $id)->delete();
        return redirect(route('branch.index'));
    }
}
