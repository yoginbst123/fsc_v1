<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Question;
use App\Model\Answer;
use DB;
use Illuminate\Support\Facades\Input;

class AnswerController extends Controller
{
    public function __construct()
    {
    $this->middleware('auth:admin');
    }  
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
   
        $position = Answer::All();
        return view('fac-Bhavesh-0554/answer/answer', compact(['position']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {  $position = Answer::All();
       return view('fac-Bhavesh-0554/answer/create', compact(['position']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $this->validate($request,[
            'answer' =>'required',
                     
        ]);
        $position = new Answer;
        $position->answer= $request->answer;
        $position->q_id= $request->q_id;
        $position->save();
        return redirect('fac-Bhavesh-0554/question')->with('success',' added Answer Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
     $position = Answer::where('id',$id)->first(); 
     return view('fac-Bhavesh-0554.answer.edit',compact(['position']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
       $this->validate($request,[
            'answer' =>'required',
                     
        ]);
        $position = Answer::find($id);
        $position->answer= $request->answer;
        $position->update();
        return redirect('fac-Bhavesh-0554/answer')->with('success','update question Successfully'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Answer::where('id',$id)->delete();
        return redirect(route('answer.index'));
    }
}
