<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Model\Employee;
use App\employees\Fscemployee;
use Illuminate\Http\Request;
use App\Model\Position;
use App\Model\Branch;
use App\Model\Rules;
use App\User;
use View;
use Validator;
use Redirect;
use Session;
use DB;
use Auth;
use App\Model\Language;
use Hash;
use App\Model\Super;
use App\Model\Schedule;
use App\Model\Msg;
use App\Model\Task;
use Illuminate\Support\Facades\Input;
use App\Front\ApplyEmployment;
use Illuminate\Support\Facades\File;
use App\Front\Commonregister;
use App\Model\Holiday;

class ClientemployeeController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
       $position = Position::All(); 
       $branch = Branch::All(); 
       $employee = Employee::where('type','=','clientemployee')->orderBy('employee_id', 'asc')->get(); 
       $customer = Commonregister::All(); 
       return view('fac-Bhavesh-0554/clientemployee/index',compact(['employee','position','branch','customer']));
    }

public function checkemailAvailability(Request $request) {
        
            $user = DB::table('employees')->where('email', Input::get('email'))->count();
        
            if($user > 0) {
               return "<span style=\"color:red;font-size:16px;\">Email Id already exists</span>";
            } else {
               return "<span style=\"color:green;font-size:16px;\">Email Id Available</span>";
            }
 
        }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {    $position = Position::All(); 
           $branch = Branch::All(); 
        return view('fac-Bhavesh-0554/employee/create',compact(['position','branch']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $this->validate($request,[
           // 'firstName' => 'required',
          //  'middleName' => 'required',
           // 'lastName' => 'required',
           // 'address1' => 'required',
           // 'city' => 'required',
            //'zip' => 'required',
           // 'stateId' => 'required',
           // 'countryId' => 'required',
           // 'telephoneNo1' => 'required',
             
           // 'telephoneNo1Type' => 'required',          
            
          //  'email' => 'required|unique:employees',
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048', 
                
        ]); 
         if($request->hasFile('photo'))
        {
         $filname = $request->photo->getClientOriginalName();
         $request->photo->move('public/employeeimage', $filname);
        }  
else
{
$filname ='';
}      
        $employee= new Employee;       
        $employee->employee_id = $request->employee_id;
        $employee->type = $request->type;
        $employee->firstName= $request->firstName;
        $employee->middleName= $request->middleName;
        $employee->lastName= $request->lastName;
        $employee->address1= $request->address1;
        $employee->address2= $request->address2;
        $employee->city= $request->city;
        $employee->stateId= $request->stateId;
        $employee->zip= $request->zip;
        $employee->countryId= $request->countryId;
        $employee->telephoneNo1= $request->telephoneNo1;
        $employee->telephoneNo2= $request->telephoneNo2;
        $employee->ext1= $request->ext1;
        $employee->ext2= $request->ext2;
        $employee->telephoneNo1Type= $request->telephoneNo1Type;
        $employee->telephoneNo2Type= $request->telephoneNo2Type;
        $employee->email= $request->email;
        $employee->nametype= $request->nametype;
        $employee->photo= $filname;
        $employee->save();
        return redirect('fac-Bhavesh-0554/employee')->with('success','Employee Added Seccessfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Employment  $employment
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Employment  $employment
     * @return \Illuminate\Http\Response
     */
    public function edit($id,Request $request )
    {  
$super = Super::All();
$newclient = Employee::where('id',$id)->update(['newemp' => 2]);
$emp = Employee::where('id',$id)->first();
$super1 = Employee::where('super','=','1')->get();
$empfsc = Fscemployee::where('user_id',$id)->first();
$holiday = Holiday::orderBy('holiday_name', 'asc')->get(); 
          $position = Position::All(); 
           $rules = Rules::All(); 
           //$branch = Branch::All(); 
           $language = Language::orderBy('language_name', 'asc')->get();
           $branch  = DB::table('branches')->select('city')->groupBy('city')->get();
           $info1 = DB::table('employee_pay_info')->where('employee_id', $id)->first();
           $employes = DB::table('employee_rules')->where('emp_id', $id)->get();
           $employes2 = DB::table('employee_rules')->where('emp_id', $id)->first();
           $info = DB::table('employee_pay_info')->where('employee_id', $id)->get(); 
           $info3 = DB::table('employee_other_info')->where('employee_id', $id)->get();
           $info2 = DB::table('employee_other_info')->where('employee_id', $id)->first();
           $review1= DB::table('employee_review')->where('employee_id', $id)->get();
         $review = DB::table('employee_review')->where('employee_id', $id)->first();
          $admin_notes = DB::table('notes')->where('admin_id','=',$id)->where('type','=',$emp->type)->get();
        return View('fac-Bhavesh-0554.employee.edit',compact(['holiday','language','super','super1','admin_notes','rules','emp','info1','info','empfsc','info2','info3','review1','review','position','branch','employes','employes2']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Employment  $employment
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request,[
            //'employee_id' =>'required',
            //'firstName' => 'required',
            //'middleName' => 'required',
            //'lastName' => 'required',
            //'address1' => 'required',
            //'city' => 'required',
           // 'zip' => 'required',
            //'stateId' => 'required',
            //'countryId' => 'required',
            //'telephoneNo1' => 'required',
           // 'telephoneNo2' => 'required',  
           // 'telephoneNo1Type' => 'required',          
            //'telephoneNo2Type' => 'required',
           // 'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048', 
//'pfid1' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048', 
//'pfid2' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048', 
                
        ]); 
       /*  if($request->hasFile('photo'))
        {
         $filname = $request->photo->getClientOriginalName();
         $request->photo->move('public/employeeimage', $filname);
        } 
else
{
$filname = $request->photo1;
}

if($request->hasFile('pfid1'))
        {
         $filname1 = $request->pfid1->getClientOriginalName();
         $request->pfid1->move('public/employeeProof1', $filname1);
        } 
else
{
$filname1 = $request->pfid12;
}
if($request->hasFile('pfid2'))
        {
         $filname2 = $request->pfid2->getClientOriginalName();
         $request->pfid2->move('public/employeeProof2', $filname2);
        } 
else
{
$filname2 = $request->pfid22;
}

if($request->hasFile('additional_attach'))
        {
         $additional_att = $request->additional_attach->getClientOriginalName();
         $request->additional_attach->move('public/additional_attach', $additional_att);
        } 
else
{
$additional_att = $request->additional_attach_11;
}
if($request->hasFile('additional_attach_1'))
        {
         $additional_att_1 = $request->additional_attach_1->getClientOriginalName();
         $request->additional_attach_1->move('public/additional_attach1', $additional_att_1);
        } 
else
{
$additional_att_1 = $request->additional_attach_12;
}

if($request->hasFile('additional_attach_2'))
        {
         $additional_att_2 = $request->additional_attach_2->getClientOriginalName();
         $request->additional_attach_2->move('public/additional_attach2', $additional_att_2);
        } 
else
{
$additional_att_2 = $request->additional_attach3;
}
if($request->hasFile('agreement'))
{
         $agreement1 = $request->agreement->getClientOriginalName();
         $request->agreement->move('public/agreement', $agreement1);
} 
else
{
$agreement1 = $request->agreement_1;
}
if($request->hasFile('resume'))
{
$resume1 = $request->resume->getClientOriginalName();
$request->resume->move('public/resumes', $resume1);
} 
else
{
$resume1= $request->resume_1;
}  */     
if($request->check=='0')
{
$status = 0;
$password1 =  '';
$name = $request->firstName .' '. $request->middleName .' '. $request->lastName;   
DB::table('fscemployees')->where('user_id', $id)->update(['type'=>0,'name'=>$name]);
DB::table('employees')->where('id', $id)->update(['check'=>0]);
}
else
{
 $status = '1';
$email = $request->email;
$name = $request->firstName .' '. $request->middleName .' '. $request->lastName;
DB::table('fscemployees')->where('user_id', $id)->update(['type'=>1,'name'=>$name]);
if($request->status=='0')
{
$password =  mt_rand();
$password1 =  bcrypt($password);
$user = Fscemployee::where('email', '=', $email)->first();
if ($user === null){
$insert = DB::insert("insert into fscemployees(`name`,`email`,`password`,`newpassword`,`user_id`,`type`,`type1`) values('".$name."','".$email."','".bcrypt($password)."','".$password."','".$id."','1','clientemployee')");
$data = array('email' => $email, 'firstName' => $name, 'from' => $email, 'password' =>$password);       
\Mail::send( 'fac-Bhavesh-0554/employe', $data, function( $message ) use ($data)
{
$message->to( $data['email'] )->from( $data['from'], $data['firstName'], $data['password'] )->subject( 'Welcome!' );
}); 
} 
}
else
{
$password1= $request->password;
}
}    

        $employee= Employee::find($id);       
        $employee->employee_id = $request->employee_id;
        $employee->super = $request->super;
        $employee->firstName= $request->firstName;
        $employee->type= $request->type;
        $employee->read= $request->terms;
        $employee->middleName= $request->middleName;
        $employee->lastName= $request->lastName;
        $employee->address1= $request->address1;
        $employee->address2= $request->address2;
        $employee->city= $request->city;
        $employee->password = $password1;
        $employee->stateId= $request->stateId;
        $employee->zip= $request->zip;
        $employee->countryId= $request->countryId;
        $employee->telephoneNo1= $request->telephoneNo1;
        $employee->telephoneNo2= $request->telephoneNo2;
        $employee->ext1= $request->ext1;
        $employee->ext2= $request->ext2;
        $employee->telephoneNo1Type= $request->telephoneNo1Type;
        $employee->telephoneNo2Type= $request->telephoneNo2Type;
        $employee->email= $request->email;
       // $employee->photo= $filname;
        $employee->hiremonth= $request->hiremonth;
        $employee->hireday= $request->hireday;
        $employee->hireyear= $request->hireyear;
        $employee->termimonth= $request->termimonth;
        $employee->termiday= $request->termiday;
        $employee->termiyear= $request->termiyear;
        $employee->tnote= $request->tnote;
        $employee->rehiremonth= $request->rehiremonth;
        $employee->rehireday= $request->rehireday;
        $employee->rehireyear= $request->rehireyear;
        $employee->branch_city= $request->branch_city;
        $employee->branch_name= $request->branch_name;
        $employee->position= $request->position;
        $employee->note= $request->note;
      //  $employee->additional_attach=$additional_att;
       // $employee->additional_attach_1=$additional_att_1;    
       //$employee->additional_attach_2=$additional_att_2;
        $employee->pay_method= $request->pay_method;
        $employee->pay_frequency= $request->pay_frequency;
        $employee->gender= $request->gender;
        $employee->marital= $request->marital;
        $employee->month= $request->month;
        $employee->day= $request->day;
        $employee->year= $request->year;
        $employee->pf1= $request->pf1;
        //$employee->pfid1= $filname1;
        $employee->pf2= $request->pf2;
        //$employee->pfid2= $filname2;
        $employee->epname= $request->epname;
        $employee->relation= $request->relation;
        $employee->eaddress1= $request->eaddress1;
        $employee->ecity= $request->ecity;
        $employee->estate= $request->estate;
        $employee->ezipcode= $request->ezipcode;
        $employee->etelephone1= $request->etelephone1;
        $employee->eteletype1= $request->eteletype1;
        $employee->eext1= $request->eext1;
        $employee->etelephone2= $request->etelephone2;
        $employee->eteletype2= $request->eteletype2;
        $employee->eext2= $request->eext2;
        $employee->comments1= $request->comments1;
        $employee->uname= $request->uname;
        $employee->question1= $request->question1;
        $employee->answer1= $request->answer1;
        $employee->question2= $request->question2;
        $employee->answer2= $request->answer2;
        $employee->question3= $request->question3;
        $employee->answer3= $request->answer3;
        $employee->other_info= $request->other_info;
        $employee->computer_name= $request->computer_name;
        $employee->computer_ip= $request->computer_ip;
        $employee->comments= $request->comments;
        $employee->check= $request->check;
        $employee->filling_status= $request->filling_status;
        $employee->fedral_claim= $request->fedral_claim;
        $employee->additional_withholding= $request->additional_withholding;
        $employee->state_claim= $request->state_claim;
        $employee->additional_withholding_1= $request->additional_withholding_1;
        $employee->local_claim= $request->local_claim;
        $employee->additional_withholding_2= $request->additional_withholding_2;
       // $employee->resume= $resume1;
        $employee->type_agreement= $request->type_agreement;
        $employee->firstName_1= $request->firstName_1;
        //$employee->agreement= $agreement1;
        $employee->middleName_1= $request->middleName_1;
        $employee->lastName_1= $request->lastName_1;
        $employee->address11= $request->address11;
        $employee->efax= $request->efax;
        $employee->fax= $request->fax;
        $employee->reset= $request->reset;
         $employee->resetdate= $request->reset_date;
         $employee->nametype= $request->nametype;
        $employee->eemail= $request->eemail;
        $employee->status=$status;
        $employee->update();
        
        if($employee->update()) {
        return response()->json([
            'status'     => 'success',
             'newopt'     => $request->employee_id]);
    } else {
        return response()->json([
            'status' => 'error']);
    }
        $pay_method = $request->pay_method;
        $pay_frequency = $request->pay_frequency;
        $pay_scale = $request->pay_scale;
        $effective_date = $request->effective_date;
        $fields = $request->fields;
        $employee= $request->employee;
        $work= $request->work;
        $work_responsibility1= $request->work_responsibility;

        $i = 0;
        $j = 0; 
        $k = 0;
DB::table('employee_pay_info')->where('employee_id', $id)->first();
foreach($pay_scale as $post)
{ 
$pay_method1 = $pay_method;
$pay_frequency1 = $pay_frequency;
$pay_scale1 = $pay_scale[$i];
$effective_date1 = $effective_date[$i];
$fields1 = $fields[$i];
$employee1 = $employee[$i];
$i++; 
DB::table('employee_pay_info')->where('pay_scale','=', '')->delete();
if(empty($employee1))
{
$insert2 = DB::insert("insert into employee_pay_info(`employee_id`,`pay_method`,`pay_frequency`,`pay_scale`,`effective_date`,`fields`) values('".$id."','".$pay_method."','".$pay_frequency."','".$post."','".$effective_date1."','".$fields1."')");    
DB::table('employee_pay_info')->where('pay_scale','=', '')->delete();
  
}
else
{
//DB::table('employee_pay_info')->where('id', $employee1)->delete();
$returnValue = DB::table('employee_pay_info')->where('id', '=', $employee1)
->update([ 'employee_id' => $id,
           'pay_method' =>$pay_method,
           'pay_frequency' =>$pay_frequency,
           'pay_scale' =>$post,
           'effective_date' =>$effective_date1,
           'fields' =>$fields1
    ]);


DB::table('employee_pay_info')->where('pay_scale','=', '')->delete();
//$affectedRows = employee_pay_info::where('employee_id', '=', $id)->delete();
}
}
$first_rev_day = $request->first_rev_day;
$reviewmonth= $request->reviewmonth;
$hiring_comments= $request->hiring_comments;
$ree= $request->ree;

DB::table('employee_review')->where('employee_id', $id)->first();
foreach($first_rev_day as $post)
{ 
$first_rev_day1 = $first_rev_day[$k];
$reviewmonth1 = $reviewmonth[$k];
$hiring_comments1 = $hiring_comments[$k];
$ree1 = $ree[$k];
$k++; 
DB::table('employee_review')->where('first_rev_day', '=', '')->delete();
if(empty($ree1))
{
   
    $insert2 = DB::insert("insert into employee_review(`employee_id`,`first_rev_day`,`reviewmonth`,`hiring_comments`) values('".$id."','".$post."','".$reviewmonth1."','".$hiring_comments1."')");
     DB::table('employee_review')->where('first_rev_day', '=', '')->delete();
}
else
{
//DB::table('employee_review')->where('id', $ree1)->delete();
$returnValue = DB::table('employee_review')->where('id', '=', $ree1)
->update([ 'employee_id' => $id,
           'first_rev_day' =>$post,
           'reviewmonth' =>$reviewmonth1,
           'hiring_comments' =>$hiring_comments1
    ]);
DB::table('employee_review')->where('first_rev_day', '=', '')->delete();
//$affectedRows = employee_pay_info::where('first_rev_day', '=', '')->delete();
}
}

$noteid = $request->noteid;
$adminnotes= $request->adminnotes;
$k=0;
$type=$request->type;
$users = DB::table('notes')->where('admin_id', '=', $id)->first();
foreach($adminnotes as $notess)
{ 
   $noteid1 = $noteid[$k];
   $note1 =$adminnotes[$k];
   $k++;
   if(empty($noteid1))
{
    $insert2 = DB::insert("insert into notes(`notes`,`admin_id`,`type`) values('".$note1."','".$id."','".$type."')");
}
else
{
$returnValue = DB::table('notes')->where('id', '=', $noteid1)
->update([ 'notes' => $note1,
           'admin_id' =>$id,
           'type' =>$type
]);    
$users = DB::table('notes')->where('notes', '=', '')->delete();
}
}

//return redirect()->route('employee.edit', $id)->with('success','Successfully update Employee');
    }

public function getbranch1(Request $request)
{
$data = Branch::select('branchname','city')->where('city',$request->id)->take(100)->get();
return response()->json($data);  
}

 public function reviewdelete1($id)
    { 
        $user = DB::table('employee_review')->where('id','=',$id)->first();
        
       $logoStatus = DB::table('employee_review')->where('id','=',$id)->delete();
        return redirect()->route('employee.edit', $user->employee_id)->with('error','Your Record Deleted Successfully');
    }
    public function paydelete1($id)
    { 
         $user = DB::table('employee_pay_info')->where('id','=',$id)->first();
       $logoStatus = DB::table('employee_pay_info')->where('id','=',$id)->delete();
       return redirect()->route('employee.edit', $user->employee_id)->with('error','Your Record Deleted Successfully');
       
       //return redirect(route('employee.index'))->with('error','Your Record Deleted Successfully');
    }
    
    
     public function empnotedelete($id)
    { 
       $users = DB::table('notes')->where('id',$id)->delete();
       return redirect(route('employee.index'))->with('error','Your Record Deleted Successfully');
    }
    
    
     public function ajaxImage(Request $request,$id)
    {
        if ($request->isMethod('get'))
            return view('ajax_image_upload');
        else {
            $validator = Validator::make($request->all(),
                [
                    'file' => 'image',
                ],
                [
                    'file.image' => 'The file must be an image (jpeg, png, bmp, gif, or svg)'
                ]);
            if ($validator->fails())
                return array(
                    'fail' => true,
                    'errors' => $validator->errors()
                );
            $extension = $request->file('file')->getClientOriginalExtension();
            $dir = 'public/uploads/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('file')->move($dir, $filename);
             DB::table('employees')->where('id', '=',$id)->update(['additional_attach_2' => $filename]);
            return $filename;
        }
    }

    public function deleteImage($filename)
    {
        Employee::delete('public/uploads/' . $filename);
    }
    
    
    
         public function ajaxImage1(Request $request,$id)
    {
        if ($request->isMethod('get'))
            return view('ajax_image_upload1');
        else {
            $validator = Validator::make($request->all(),
                [
                    'file_1' => 'image',
                ],
                [
                    'file_1.image' => 'The file must be an image (jpeg, png, bmp, gif, or svg)'
                ]);
            if ($validator->fails())
                return array(
                    'fail' => true,
                    'errors' => $validator->errors()
                );
            $extension = $request->file('file_1')->getClientOriginalExtension();
            $dir = 'public/uploads/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('file_1')->move($dir, $filename);
             DB::table('employees')->where('id', '=',$id)->update(['additional_attach_1' => $filename]);
            return $filename;
        }
    }

    public function deleteImage1($filename)
    {
        Employee::delete('public/uploads/' . $filename);
    }
    
    
     public function ajaxImage2(Request $request,$id)
    {
        if ($request->isMethod('get'))
            return view('ajax_image_upload2');
        else {
            $validator = Validator::make($request->all(),
                [
                    'file_2' => 'image',
                ],
                [
                    'file_2.image' => 'The file must be an image (jpeg, png, bmp, gif, or svg)'
                ]);
            if ($validator->fails())
                return array(
                    'fail' => true,
                    'errors' => $validator->errors()
                );
            $extension = $request->file('file_2')->getClientOriginalExtension();
            $dir = 'public/uploads/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('file_2')->move($dir, $filename);
             DB::table('employees')->where('id', '=',$id)->update(['additional_attach' => $filename]);
            return $filename;
        }
    }

    public function deleteImage2($filename)
    {
        Employee::delete('public/uploads/' . $filename);
    }
    
     public function pfid_upload1(Request $request,$id)
    {
        if ($request->isMethod('get'))
            return view('pfid_upload');
        else {
            $validator = Validator::make($request->all(),
                [
                    'pfid1' => 'image',
                ],
                [
                    'pfid1.image' => 'The file must be an image (jpeg, png, bmp, gif, or svg)'
                ]);
            if ($validator->fails())
                return array(
                    'fail' => true,
                    'errors' => $validator->errors()
                );
            $extension = $request->file('pfid1')->getClientOriginalExtension();
            $dir = 'public/employeeProof1/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('pfid1')->move($dir, $filename);
             DB::table('employees')->where('id', '=',$id)->update(['pfid1' => $filename]);
            return $filename;
        }
    }
    
    
     public function pfid_upload2(Request $request,$id)
    {
        if ($request->isMethod('get'))
            return view('pfid_upload2');
        else {
            $validator = Validator::make($request->all(),
                [
                    'pfid2' => 'image',
                ],
                [
                    'pfid2.image' => 'The file must be an image (jpeg, png, bmp, gif, or svg)'
                ]);
            if ($validator->fails())
                return array(
                    'fail' => true,
                    'errors' => $validator->errors()
                );
            $extension = $request->file('pfid2')->getClientOriginalExtension();
            $dir = 'public/employeeProof2/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('pfid2')->move($dir, $filename);
             DB::table('employees')->where('id', '=',$id)->update(['pfid2' => $filename]);
            return $filename;
        }
    }
    
     public function photo_upload2(Request $request,$id)
    {
        if ($request->isMethod('get'))
            return view('photo_upload2');
        else {
            $validator = Validator::make($request->all(),
                [
                    'photo' => 'image',
                ],
                [
                    'photo.image' => 'The file must be an image (jpeg, png, bmp, gif, or svg)'
                ]);
            if ($validator->fails())
                return array(
                    'fail' => true,
                    'errors' => $validator->errors()
                );
            $extension = $request->file('photo')->getClientOriginalExtension();
            $dir = 'public/employeeimage/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('photo')->move($dir, $filename);
             DB::table('employees')->where('id', '=',$id)->update(['photo' => $filename]);
            return $filename;
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Employment  $employment
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    $ss = Employee::where('id',$id)->first();$k=$ss->email;
    ApplyEmployment::where('email',$k)->delete();
    Employee::where('id',$id)->delete();
    Schedule::where('emp_name',$id)->delete();
    Schedule::where('emp_name',$id)->delete();
    DB::table('empschedules')->where('employee_id', $id);
    Msg::where('employeeid',$id)->delete();
    Msg::where('admin_id',$id)->delete();
    Task::where('employeeid',$id)->delete();
    
    Fscemployee::where('user_id',$id)->delete();
    DB::table('employee_review')->where('employee_id','=',$id)->first();
    return redirect(route('employee.index'))->with('success','Record Delete SuccessFully');
    }
}
