<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Marque;

use DB;
use Illuminate\Support\Facades\Input;

class MarqueController extends Controller
{
    public function __construct()
    {
    $this->middleware('auth:admin');
    }  
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        
     $branch= Marque::All();

   
        return view('fac-Bhavesh-0554/marque/marque', compact(['branch']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {  
      
       return view('fac-Bhavesh-0554/marque/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request,[
            'marque' =>'required',
                                 
        ]);
        $branch  = new Marque;
        $branch->marque= $request->marque;
   
        $branch->save();
        return redirect('fac-Bhavesh-0554/marque')->with('success','Success fully add Marque');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
     $branch= Marque::where('id',$id)->first(); 

     return view('fac-Bhavesh-0554.marque.edit',compact(['branch']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
      $this->validate($request,[
            'marque' =>'required',
                             
        ]);
        $branch  = Marque::find($id);
        $branch->marque= $request->marque;
   
        $branch->update();
        return redirect('fac-Bhavesh-0554/marque')->with('success','Success fully Update marque');
    }
  

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Marque::where('id',$id)->delete();
        return redirect(route('marque.index'));
        
    }
}
