<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\formationsetup;
use DB;
use App\Model\taxstate;
use Illuminate\Support\Facades\Input;
use App\Model\Category;
use App\Model\Business;
use App\Model\License;
class TaxationSetupController extends Controller
{
    public function __construct()
    {
    $this->middleware('auth:admin');
    }  
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
   
         $position = formationsetup::where('type1','=','Taxation')->orwhere('type1','=','License')->get();
         $category = Category::orderby('business_cat_name','asc')->get(); 
         return view('fac-Bhavesh-0554/taxation/taxation', compact(['position','category']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { 
        $business = Business::all();
        $position = formationsetup::All();
        $category = Category::orderby('business_cat_name','asc')->get(); 
        $license=DB::table('license')->orderBy('licensename')->get();
       // echo "<pre>";print_r($license);exit;
        $currency = License::All();
        return view('fac-Bhavesh-0554/taxation/create', compact(['license','position','category','business','currency']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
       $this->validate($request,[
            //'question' =>'required',
                     
        ]);
        
        $c1 = $request->bussiness_name;
        if(empty($c1))
        {
        $List1='';
        }
        else
        {
        $List1 = $c1; 
        }
        
        $type = isset($request->tab) ? $request->tab:"";
        $position = new formationsetup;
        if($type=='Taxation')
        {
        $position->question= isset($request->question) ? $request->question:"";
        $position->type= isset($request->typeofservice) ? $request->typeofservice:"";
        $position->date= isset($request->expiredate1) ? $request->expiredate1:"";
        $position->business_catagory_name= isset($request->business_catagory_name) ? $request->business_catagory_name:"";
        }
        if($type=='License')
        {
        $position->question= isset($request->typeoflicense) ? $request->typeoflicense:"";
        $position->business_catagory_name= $List1;
        $position->bussiness_name=$List1;
        $position->type= isset($request->typeofservice1) ? $request->typeofservice1:"";
        $position->state= isset($request->state) ? $request->state:"";
        $position->cityname= isset($request->cityname) ? $request->cityname:"";
        $position->countyname= isset($request->county) ? $request->county:"";
        $position->countysnumber= isset($request->countynumber) ? $request->countynumber:"";
        $position->website= isset($request->website) ? $request->website:"";
        $position->telephone= isset($request->telephone) ? $request->telephone:"";
        $position->fax= isset($request->fax) ? $request->fax:"";
        $position->renew= isset($request->renew) ? $request->renew:"";
        $position->renewal= isset($request->renewal) ? $request->renewal:"";
        $position->annualfees= isset($request->annualfees) ? $request->annualfees:"";
        $position->processingfees= isset($request->processingfees) ? $request->processingfees:"";
        if($request->renewal=='Individual')
        {
        $position->duedate1= '';
        $position->expiredate2='';
        $position->duedate='';
        $position->date= '';  
        }
        else
        {
        $position->duedate1= isset($request->duedate1) ? $request->duedate1:"";
        $position->expiredate2= isset($request->expiredate2) ? $request->expiredate2:"";
        $position->duedate= isset($request->duedate) ? $request->duedate:"";
        $position->date= isset($request->expiredate) ? $request->expiredate:"";
        }
        $position->renewalwebsite= isset($request->renewalwebsite) ? $request->renewalwebsite:"";
        $position->authorityname= isset($request->authorityname) ? $request->authorityname:"";
        }
        $position->taxationtypes ='licensetaxation';
        $position->type1=  isset($request->tab) ? $request->tab:"";
        $position->save();
        
        return redirect('fac-Bhavesh-0554/states')->with('success','Success fully add Question');
        
        ///return redirect('fac-Bhavesh-0554/taxation')->with('success','Success fully add Question');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        //echo "dfbdfdh";die;
        $currency = License::All();
        $business = Business::all();
        $taxstate = taxstate::All();
        $position = formationsetup::where('id',$id)->first(); 
        $category = Category::orderby('business_cat_name','asc')->get();
        return view('fac-Bhavesh-0554.taxation.edit',compact(['position','taxstate','category','business','currency']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
       $this->validate($request,[
            'typeoflicense' =>'required',
        ]);
        $c1 = $request->bussiness_name;
        if(empty($c1))
        {
            $List1='';
        }
        else
        {
            $List1 = $c1;
        }
        $position = formationsetup::find($id);
        $type = $request->tab;
        if($type=='Taxation')
        {
        $position->question= $request->question;
        $position->type= $request->typeofservice;
        $position->date= $request->expiredate1;
        $position->business_catagory_name= $request->business_catagory_name;
        }
        if($type=='License')
        {
        $position->business_catagory_name= $List1;
        $position->bussiness_name= $List1;
        $position->question= $request->typeoflicense;
        $position->type= $request->typeofservice1;
        $position->state= $request->state;
        $position->cityname= $request->cityname;
        $position->countyname= $request->county;
        $position->countysnumber= $request->countynumber;
        $position->website= $request->website;
        $position->telephone= $request->telephone;
        $position->fax= $request->fax;
        $position->renew= $request->renew;
        $position->annualfees= $request->annualfees;
        $position->processingfees= $request->processingfees;
        
        if($request->renewal=='Individual')
        {
        $position->duedate1= '';
        $position->expiredate2='';
        $position->duedate='';
        $position->date= '';  
        }
        else
        {
        $position->duedate1= $request->duedate1;
        $position->expiredate2= $request->expiredate2;
        $position->duedate= $request->duedate;
        $position->expiredate= $request->expiredate;
        }
        $position->renewal= $request->renewal;
        $position->authorityname= $request->authorityname;
        $position->renewalwebsite= $request->renewalwebsite;
        }
        $position->type1=  $request->tab;
        $position->update();
        
         return redirect('fac-Bhavesh-0554/states')->with('success','Success fully add Question');
        //return redirect('fac-Bhavesh-0554/taxation')->with('success','Success fully update question'); 
    }

 public function removelicense2(Request $request)
    { 
       $student = License::find($request->input('id'));
        if($student->delete())
        {
            echo 'Data Deleted';
        }
    }
 public function typelicense()
    {   
        $newopt = Input::get('newopt');
        if (DB::table('licenses')->where('licensename', '=', $newopt)->exists())   {
                //return response()->json(['status' => 'error']);
                }   
                else 
                { 
        $position = new License;
        $position->licensename = $newopt;
        $position->save();
         if ($position) {
        return response()->json([
            'status'     => 'success',
             'newopt'     => $newopt]);
    } else {
        return response()->json([
            'status' => 'error']);
    }
    }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        formationsetup::where('id',$id)->delete();
        return redirect(route('states.index'));
    }
}
