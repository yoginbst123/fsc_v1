<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Front\Commonregister;
use App\Model\BusinessBrand;
use App\Model\Category;
use App\Model\Business;
use App\User;
use App\Model\Categorybusiness;
use App\Model\Contact_userinfo;
use App\Mail\Activationmail;
use DB;
use Hash;
use Carbon;
use Validator;
use Response;
use Illuminate\Support\Facades\Input;
class ClientconvertController extends Controller
{
public function __construct()
{
$this->middleware('auth:admin');
}
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
public function index(Request $request)
{
$name = $request->input('user_type');
if(isset($name))
{
$common = DB::table('commonregisters')->select('commonregisters.id as cid','commonregisters.filename as filename','commonregisters.created_at as created_at','commonregisters.mailing_address1 as mailing_address1','commonregisters.bussiness_zip as bussiness_zip','commonregisters.newclient as newclient','commonregisters.due_date as due_date','commonregisters.department as department','commonregisters.type_of_activity as type_of_activity','commonregisters.county_no as county_no','commonregisters.county_name as county_name','commonregisters.level as level','commonregisters.setup_state as setup_state','commonregisters.business_state as business_state','commonregisters.business_city as business_city','commonregisters.business_country as business_country','commonregisters.business_address as business_address','commonregisters.business_store_name as business_store_name','commonregisters.mailing_address as mailing_address','commonregisters.legalname as legalname','commonregisters.dbaname as dbaname','commonregisters.mailing_city as mailing_city','commonregisters.mailing_state as mailing_state','commonregisters.mailing_zip as mailing_zip','commonregisters.user_type as user_type','commonregisters.user_type as user_type','commonregisters.status as status','commonregisters.company_name as company_name','commonregisters.business_name as business_name','commonregisters.first_name as first_name','commonregisters.middle_name as middle_name','commonregisters.last_name as last_name','commonregisters.email as email','commonregisters.address as address','commonregisters.address1 as address1','commonregisters.city as city','commonregisters.stateId as stateId','commonregisters.zip as zip','commonregisters.countryId as countryId','commonregisters.mobile_no as mobile_no','commonregisters.business_no as business_no','commonregisters.business_fax as business_fax','commonregisters.website as website','commonregisters.user_type as user_type','commonregisters.business_id as business_id','commonregisters.business_cat_id as business_cat_id','commonregisters.business_brand_id as business_brand_id','commonregisters.business_brand_category_id as business_brand_category_id','businesses.bussiness_name as bussiness_name' ,'categories.business_cat_name as business_cat_name','business_brands.business_brand_name as business_brand_name','categorybusinesses.business_brand_category_name as business_brand_category_name')
->leftJoin('categories', function($join){ $join->on('commonregisters.business_cat_id', '=', 'categories.id');})
->leftJoin('businesses', function($join){ $join->on('commonregisters.business_id', '=', 'businesses.id');})
->leftJoin('business_brands', function($join){ $join->on('commonregisters.business_brand_id', '=', 'business_brands.id');})
->leftJoin('categorybusinesses', function($join){ $join->on('commonregisters.business_brand_category_id', '=', 'categorybusinesses.id');})
->where('commonregisters.user_type', '=', $name)->where('commonregisters.filename', '=', NULL)->get();
}
else{
$common = DB::table('commonregisters')->select('commonregisters.id as cid','commonregisters.created_at as created_at','commonregisters.filename as filename','commonregisters.mailing_address1 as mailing_address1','commonregisters.newclient as newclient','commonregisters.bussiness_zip as bussiness_zip','commonregisters.due_date as due_date','commonregisters.department as department','commonregisters.type_of_activity as type_of_activity','commonregisters.county_no as county_no','commonregisters.county_name as county_name','commonregisters.level as level','commonregisters.setup_state as setup_state','commonregisters.business_state as business_state','commonregisters.business_city as business_city','commonregisters.business_country as business_country','commonregisters.business_address as business_address','commonregisters.business_store_name as business_store_name','commonregisters.mailing_address as mailing_address','commonregisters.legalname as legalname','commonregisters.dbaname as dbaname','commonregisters.mailing_city as mailing_city','commonregisters.mailing_state as mailing_state','commonregisters.mailing_zip as mailing_zip','commonregisters.user_type as user_type','commonregisters.user_type as user_type','commonregisters.status as status','commonregisters.company_name as company_name','commonregisters.business_name as business_name','commonregisters.first_name as first_name','commonregisters.middle_name as middle_name','commonregisters.last_name as last_name','commonregisters.email as email','commonregisters.address as address','commonregisters.address1 as address1','commonregisters.city as city','commonregisters.stateId as stateId','commonregisters.zip as zip','commonregisters.countryId as countryId','commonregisters.mobile_no as mobile_no','commonregisters.business_no as business_no','commonregisters.business_fax as business_fax','commonregisters.website as website','commonregisters.user_type as user_type','commonregisters.business_id as business_id','commonregisters.business_cat_id as business_cat_id','commonregisters.business_brand_id as business_brand_id','commonregisters.business_brand_category_id as business_brand_category_id','businesses.bussiness_name as bussiness_name' ,'categories.business_cat_name as business_cat_name','business_brands.business_brand_name as business_brand_name','categorybusinesses.business_brand_category_name as business_brand_category_name')
->leftJoin('categories', function($join){ $join->on('commonregisters.business_cat_id', '=', 'categories.id');})
->leftJoin('businesses', function($join){ $join->on('commonregisters.business_id', '=', 'businesses.id');})
->leftJoin('business_brands', function($join){ $join->on('commonregisters.business_brand_id', '=', 'business_brands.id');})
->leftJoin('categorybusinesses', function($join){ $join->on('commonregisters.business_brand_category_id', '=', 'categorybusinesses.id');})->where('commonregisters.filename', '=', NULL)->get();  
}
return view('fac-Bhavesh-0554.customerconvert.customerconvert',compact('common'));
}
/**
* Show the form for creating a new resource.
*
* @return \Illuminate\Http\Response
*/
public function create()
{   
//
}
/**
* Store a newly created resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @return \Illuminate\Http\Response
*/
public function store(Request $request)
{
//
}
/**
* Display the specified resource.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function show($id)
{

}
/**
* Show the form for editing the specified resource.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function edit($id)
{  

$common  = Commonregister::where('id',$id)->first();
$idd = $common->business_id;
$business = Business::where('id',$idd)->first();
return view('fac-Bhavesh-0554.customerconvert.edit',compact(['common','business']));
}
/**
* Update the specified resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function update(Request $request, $id)
{
$this->validate($request,[
'company_name'=>'required',
'business_name'=>'required',
'first_name'=>'required',
'middle_name'=>'required',
'last_name'=>'required',
'email'=>'required',
'address'=>'required',
'city'=>'required',
'stateId'=>'required',
'zip'=>'required',
'countryId'=>'required',
'business_no'=>'required',
'telephone1'=>'required',
'telephone1type'=>'required',
'client_id'=>'required',
'status'=>'required',          
]);              
if($request->status=='Approval')
{
$name = $request->first_name .' '. $request->middle_name .' '. $request->last_name;   
//DB::table('users')->where('user_id', $id)->update(['type'=>1,'name'=>$name]);
$client_id = $request->client_id;
$email = $request->email;
$password =  mt_rand();
$user_type=  $request->user_type;
$user = User::where('email', '=', $email)->first();
if ($user === null)
{
    $name = $request->first_name .' '. $request->middle_name .' '. $request->last_name;  
$insert = DB::insert("insert into users(`name`,`email`,`password`,`user_id`,`user_type`,`type`) values('".$name."','".$email."','".bcrypt($password)."','".$id."','".$user_type."','1')");
$data = array('email' => $email, 'first_name' => $name, 'from' => 'vijay@businesssolutionteam.com', 'password' =>$password,'client'=>$client_id);       
\Mail::send( 'activation', $data, function( $message ) use ($data)
{
$message->to( $data['email'] )->from( $data['from'], $data['first_name'], $data['password'] , $data['client'] )->subject( 'Welcome!' );
});      
}
else
{
//return $user->type;
if ($user->type === '2')
{
    $name = $request->first_name .' '. $request->middle_name .' '. $request->last_name;  
$user_type=  $request->user_type;
$returnValue = DB::table('users')->where('email', '=', $email)->update(['user_type' =>$user_type,'type'=>1,'name'=>$name,'password' =>bcrypt($password)]);  
$data = array('email' => $email, 'first_name' => $name, 'from' => 'vijay@businesssolutionteam.com', 'password' =>$password,'client'=>$client_id);       
\Mail::send( 'activation', $data, function( $message ) use ($data)
{
$message->to( $data['email'] )->from( $data['from'], $data['first_name'], $data['password'] , $data['client'] )->subject( 'Welcome!' );
});
}
else
{
$user_type=  $request->user_type;
$returnValue = DB::table('users')->where('email', '=', $email)->update(['user_type' =>$user_type,'type'=>1,'name'=>$name]);  
}
} 
}
else
{
$name = $request->first_name .' '. $request->middle_name .' '. $request->last_name;   
$status = $request->status;
$email = $request->email;
$name = $request->first_name;
$user_type=  $request->user_type;
$returnValue = DB::table('users')->where('email', '=', $email)->update(['user_type' =>$user_type,'type'=>2,'name'=>$name]);
$data = array('email' => $email, 'first_name' => $name, 'from' => 'vijay@businesssolutionteam.com');       
\Mail::send('fac-Bhavesh-0554/hold', $data, function( $message ) use ($data)
{
$message->to( $data['email'] )->from( $data['from'], $data['first_name'])->subject( 'Welcome!' );
});            
}
        $bulletin = implode(",", $request->get('bulletin'));
        $due_date = implode(',', $request->get('due_date'));
        $employee = implode(',', $request->get('employee'));
        $businessbrand = Commonregister::find($id);
        $businessbrand->nametype= $request->nametype;
        $businessbrand->company_name= $request->company_name;
        $businessbrand->business_name = $request->business_name;
        $businessbrand->first_name = $request->first_name;      
        $businessbrand->middle_name = $request->middle_name;
        $businessbrand->last_name= $request->last_name;
        $businessbrand->email = $request->email;
        $businessbrand->address = $request->address;      
        $businessbrand->address1 = $request->address1; 
        $businessbrand->city= $request->city;
        $businessbrand->stateId = $request->stateId;
        $businessbrand->zip = $request->zip;      
        $businessbrand->countryId = $request->countryId; 
        $businessbrand->business_fax = $request->business_fax;      
        $businessbrand->website = $request->website; 
        $businessbrand->business_no = $request->business_no;
        $businessbrand->businesstype = $request->businesstype;
        $businessbrand->businessext = $request->businessext;
        $businessbrand->etelephone1 = $request->telephone1;     
        $businessbrand->eteletype1 = $request->telephone1type;     
        $businessbrand->eext1 = $request->telephone1ext;
        $businessbrand->etelephone2 = $request->telephone2;     
        $businessbrand->eteletype2 = $request->telephone2type; 
        $businessbrand->eteletype2 = $request->telephone2type; 
        $businessbrand->status= $request->status;      
        $businessbrand->filename = $request->client_id;  
        $businessbrand->monthly_fee = $request->monthly_fee; 
        $businessbrand->yearly_fee = $request->yearly_fee; 
        $businessbrand->amount = $request->amount; 
        $businessbrand->credit_card = $request->credit_card; 
        $businessbrand->expire = $request->expire; 
        $businessbrand->security_code = $request->security_code; 
        $businessbrand->credit_card_holder_name = $request->credit_card_holder_name; 
        $businessbrand->credit_address = $request->credit_address; 
        $businessbrand->credit_city = $request->credit_city; 
        $businessbrand->credit_state = $request->credit_state; 
        $businessbrand->credit_zip = $request->credit_zip; 
        $businessbrand->bulletin = $bulletin; 
        $businessbrand->employee = $employee; 
        $businessbrand->taxation =  $request->taxation; 
        $businessbrand->due_date1 = $due_date; 
        $businessbrand->terms = $request->terms; 
        $businessbrand->update();      
        return redirect(route('customerconvert.index'))->with('success','Your Profile SuccessFully Update');
}

public function checkclient(Request $request){
$user = DB::table('commonregisters')->where('filename', Input::get('filename'))->count();        
         if($user > 0) {
                $isAvailable = FALSE;
            } else {
                $isAvailable = TRUE;
            }        
            echo json_encode(
                    array(
                        'valid' => $isAvailable
                    ));
            
    
}        
    public function checkclientemail(Request $request){
$user = DB::table('commonregisters')->where('email', Input::get('email'))->count();
         if($user > 0) {
               $isAvailable = FALSE;
            } else {
                $isAvailable = TRUE;
            }  
            echo json_encode(
                    array(
                        'valid' => $isAvailable
                    ));
            
    
}    
        
/**
* Remove the specified resource from storage.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/


public function destroy($id)
{
Commonregister::where('id',$id)->delete();
$user = User::where('user_id', '=', $id)->delete();
return redirect(route('customerconvert.index'));
}
}