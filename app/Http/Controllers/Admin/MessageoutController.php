<?php
namespace App\Http\Controllers\Admin;
use App\employees\Fscemployee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Msg;
use App\Model\Employee;
use App\Front\Commonregister;
use Illuminate\Support\Facades\Input;

use DB;
use App\Model\Logo;

class MessageoutController extends Controller
{
public function __construct()
    {
    $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $logo = Logo::where('id','=',1)->first();
   
        $task = Msg::where('recieve','=','1')->orderBy('id', 'desc')->get();
      // $emp1 = Employee::where('type','=','employee')->get();
    $silver = DB::table("commonregisters")
    ->select("commonregisters.business_no","commonregisters.first_name","commonregisters.middle_name","commonregisters.last_name","commonregisters.address","commonregisters.city","commonregisters.stateId","commonregisters.countryId"
    ,"commonregisters.business_no","commonregisters.email","commonregisters.status","commonregisters.id")->where('commonregisters.status', '=',"Active");
 $emp1 = DB::table("employees")->select("employees.telephoneNo1","employees.firstName","employees.middleName","employees.lastName","employees.address1","employees.city","employees.stateId","employees.countryId"
,"employees.telephoneNo1","employees.email","employees.type","employees.id")->where('employees.check', '=', "1")->unionAll($silver)->get();  
       $emp = Fscemployee::All();
       return view('fac-Bhavesh-0554/msgout/msgout', compact(['logo','task','emp','emp1']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $purpose= DB::table('purposes')->get();
      $emp = Fscemployee::All();
      $emp1 = Employee::where('type','=','employee')->get();
      $employee1 = Employee::All();
       return view('fac-Bhavesh-0554/msgout/create',compact(['emp','emp1','purpose','employee1']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $employee =  $request->get('employee');
        $position = new Msg;
        $position->date= $request->date;
          $position->time= $request->time;
          $position->day= $request->day;
          $position->title= $request->type;
          $position->content= $request->othermsg;
          $position->admin_id= $request->employees;
          $position->employeeid= $request->employee;
          $position->type= $request->type;
          $position->rlt_msg= $request->other;
          $position->call_back= $request->purpose1;
         $position->busname= $request->busname;
         $position->clientfile= $request->clientfile;
         $position->clientno= $request->clientno;
         $position->clientname= $request->clientname;
         $position->purpose= $request->purpose;
        $position->recieve= 1;
         $position->status= 1;
         $position->status3= 'All';
         
        $position->save();
        return redirect('fac-Bhavesh-0554/msgout')->with('success','Message Sent Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\State  $state
     * @return \Illuminate\Http\Response
     */
    public function show(State $state)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\State  $state
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {  $purpose= DB::table('purposes')->get();
        $task = Msg::where('id',$id)->first(); 
        $emp = Fscemployee::All();
        $emp1 = Employee::where('type','=','employee')->where('id','=',$task->employeeid)->first();
        return View('fac-Bhavesh-0554.msgout.edit',compact(['task','emp','emp1','purpose']));
    }
  public function purposes()
    {   
        $newopt = Input::get('newopt');
        //$sign = Input::get('sign');
        if (DB::table('purposes')->where('purposename', '=', $newopt)->exists())   {
                //return response()->json(['status' => 'error']);
                }   
                else 
                { 
                    
        $position = DB::insert('insert into purposes (purposename) values (?)', array($newopt));
       
         if ($position) {
        return response()->json([
            'status'     => 'success',
             'newopt'     => $newopt]);
    }
    else 
    {
        return response()->json([
            'status' => 'error']);
    }
    }
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\State  $state
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $this->validate($request,[
            'title' =>'required',
            'description' =>'required',
            'employee' =>'required',
                               
        ]);
        $position =Msg::find($id);
        $position->title= $request->title;
        $position->content= $request->description;
        $position->admin_id= $request->admin_id;
       // $position->employeeid= $employee;
        $position->type= $request->type;
          $position->day= $request->day;
        $position->rlt_msg= $request->rlt_msg;
        $position->call_back= $request->call_back;
        $position->date= $request->date;
        $position->time= $request->time;
        $position->busname= $request->busname;
        $position->clientfile= $request->clientfile;
        $position->clientno= $request->clientno;
        $position->clientname= $request->clientname;
        $position->purpose= $request->purpose;
         $position->time1= date('h:i');
        $position->send= 1;
        $position->update();
        return redirect('fac-Bhavesh-0554/msgout')->with('success','Message Sent Successfully');
    }
    
    
public function clientname(Request $request)
    {   
        if(($request->id=='employee') or ($request->state=='employee'))
        {
        $data = Employee::select('firstName','id','middleName','lastName','type','employee_id','telephoneNo1')->where('type',$request->id)->orWhere('id', $request->id)->take(1000)->get();
        }
        
        else
        {
           $data = Commonregister::select('first_name','id','middle_name','last_name','business_no','status','filename','business_name')->where('status',$request->id)->orWhere('id', $request->id)->take(1000)->get(); 
        }
        return response()->json($data);  
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\State  $state
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        Msg::where('id',$id)->delete();
        return redirect(route('msgout.index'));
    }
}
