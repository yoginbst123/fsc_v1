<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Business;
class BusinessController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {  
       $business = Business::All(); 
       return view('fac-Bhavesh-0554.business.business',compact('business'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
     return view('fac-Bhavesh-0554.business.business-add-new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'bussiness_name'=>'required|unique:businesses,bussiness_name',
            'bussiness_image_name'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',            
            'link'=>'required|unique:businesses,link',            
        ]);
        if($request->hasFile('bussiness_image_name'))
        {
         $filname = $request->bussiness_image_name->getClientOriginalName();
         $request->bussiness_image_name->move('public/business', $filname);
        }
        
        $business = new Business;
        $business->bussiness_name = $request->bussiness_name;
        $business->link = $request->link;
        $business->bussiness_image_name = $filname;
        $business->save();
        return redirect(route('business.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    $bussiness = Business::where('id', $id)->first();
    return View::make('fac-Bhavesh-0554.business.business', compact('business'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $business = Business::where('id',$id)->first();    
        return View('fac-Bhavesh-0554.business.business-edit',compact('business'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    $this->validate($request,[
            'bussiness_name'=>'required',
            'link'=>'required', 
                        
        ]);
        if($request->hasFile('bussiness_image_name'))
        {
         $filname = $request->bussiness_image_name->getClientOriginalName();
         $request->bussiness_image_name->move('public/business', $filname);
        }
        else
        {
            $filname = $request->bussiness_image_name1;   
        }
        
        $business =Business::find($id);
        $business->bussiness_name = $request->bussiness_name;
        $business->link = $request->link;
        $business->bussiness_image_name = $filname;
        
        $business->update();
        return redirect(route('business.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Business::where('id',$id)->delete();
        return redirect(route('business.index'));
    }
}
