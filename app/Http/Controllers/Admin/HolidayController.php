<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Model\Holiday;
use DB;
use App\Http\Controllers\Controller;
class HolidayController extends Controller
{
public function __construct()
    {
    $this->middleware('auth:admin');
    } 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
         $holiday = Holiday::orderBy('holiday_date', 'asc')->get(); 
       
        
        
        
        return view('fac-Bhavesh-0554.holiday.holiday',compact(['holiday']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('fac-Bhavesh-0554/holiday/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $this->validate($request,[
            'holiday_name' =>'required',
            'holiday_date' =>'required',                     
        ]);
        $holiday = new Holiday;
        $holiday->holiday_name= $request->holiday_name;
        $holiday->holiday_date= date('Y-m-d',strtotime($request->holiday_date));
         $holiday->save();
       return redirect('fac-Bhavesh-0554/holiday')->with('success','Holiday Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function show(Holiday $holiday)
    {
        //
    }





    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    { 
     
      
      $holiday = Holiday::where('id',$id)->orderBy('id', 'asc')->first();
      return View('fac-Bhavesh-0554.holiday.edit',compact('holiday'));
      
      
      
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Holiday $holiday)
    {
       // echo "<pre>";return $request;
       
     // ECHO "<PRE>"; print_r($_POST);EXIT;
       $this->validate($request,[
             'holiday_name' =>'required',
            'holiday_date' =>'required'                   
        ]);   
        $holiday1 = $holiday;                  
        $holiday1->holiday_name= $request->holiday_name;
        $holiday1->holiday_date= date('Y-m-d',strtotime($request->holiday_date));
       // $holiday1->update();
     
$na1 = $holiday->id;
//$empschedule = empschedule::where('emp_sch_id',$na1)->where('date_1',$date_to4)->get();



$returnValue = DB::table('holidays')->where('id', '=', $na1)
->update([ 'holiday_name' => $request->holiday_name,
           'holiday_date' => date('Y-m-d',strtotime($request->holiday_date))
    ]);

    //return $schedule_date1;

return redirect('fac-Bhavesh-0554/holiday')->with('success','Holiday Update Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function destroy(Holiday $holiday)
    {
      $holiday->delete();
    return redirect(route('holiday.index'))->with('success','Holiday Delete Successfully');
    }
    
    
  
}