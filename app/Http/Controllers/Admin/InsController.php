<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Cestatus;
use App\Model\Employee;
use App\employees\Fscemployee;
use App\Model\admin_professional;
use App\Model\Email;
use DB;
use App\Model\Admin;
use App\Model\Insurance;
use App\User;
use Illuminate\Support\Facades\Input;

class InsController extends Controller
{
    public function __construct()
    {
    $this->middleware('auth:admin');
    }  
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {  
       return view('fac-Bhavesh-0554/insurance/create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $branch  = new Insurance;
        $branch->date_attend = $request->date_attend;
        $branch->course_name = $request->course_name;
        $branch->school_name = $request->school_name;
        $branch->field_of_study = $request->field_of_study;
        $branch->ethics_hrs = $request->ethics_hrs;
        $branch->general_hrs = $request->general_hrs;
        $branch->total_ce_hrs = $request->total_ce_hrs;
        $branch->save();
        return redirect('fac-Bhavesh-0554/cestatus')->with('success','Success fully add CE Status');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
     //$branch = Commonregister::where('id',$id)->first(); 
     $position = Insurance::where('id',$id)->first(); 
     return view('fac-Bhavesh-0554/insurance.edit',compact(['position']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $branch = Insurance::find($id);
        $branch->date_attend = $request->date_attend;
        $branch->course_name = $request->course_name;
        $branch->school_name = $request->school_name;
        $branch->field_of_study = $request->field_of_study;
        $branch->ethics_hrs = $request->ethics_hrs;
        $branch->general_hrs = $request->general_hrs;
        $branch->total_ce_hrs = $request->total_ce_hrs;
        $branch->update();
        return redirect('fac-Bhavesh-0554/cestatus')->with('success','Success fully Update Email');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Insurance::where('id',$id)->delete();
        return redirect(route('fac-Bhavesh-0554/cestatus'));
    }
}
