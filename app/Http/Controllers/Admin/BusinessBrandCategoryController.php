<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\BusinessBrand;
use App\Model\Category;
use App\Model\Business;
use App\Model\Categorybusiness;
use DB;
class BusinessBrandCategoryController extends Controller
{
   
    public function __construct()
    {
      $this->middleware('auth:admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   
    public function index()
    {  $category = Category::All();   
        $business = Business::all();
        $businessbrand = BusinessBrand::All(); 
       // $categorybusiness = Categorybusiness::All(); 

       $categorybusiness = DB::select("select categorybusinesses.id as cid,categorybusinesses.link as link,categorybusinesses.business_brand_category_image as business_brand_category_image,categorybusinesses.business_brand_category_name as business_brand_category_name,categories.business_cat_name,categories.id as bid,
        businesses.bussiness_name as bussiness_name,businesses.id,
        business_brands.business_brand_name as business_brand_name,business_brands.id
          from categorybusinesses left join categories on categories.id=categorybusinesses.business_cat_id
          left join businesses on businesses.id=categorybusinesses.business_id 
          left join business_brands on business_brands.id=categorybusinesses.business_brand_id order by categorybusinesses.business_brand_category_name asc");
        return view('fac-Bhavesh-0554.business-brand-category.business-brand-category',compact(['business','category','businessbrand','categorybusiness']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {    
        $category = Category::All();   
        $business = Business::All(); 
        $businessbrand = BusinessBrand::All();
        $categorybusiness = Categorybusiness::All(); 
        return View('fac-Bhavesh-0554.business-brand-category.business-brand-category-add-new', compact(['business','category','businessbrand','categorybusiness']));
    }
    public function getcategory(Request $request)
    {
        $data = Category::select('business_cat_name','id')->where('bussiness_name',$request->id)->orderby('business_cat_name','asc')->take(100)->get();
        return response()->json($data);  
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'business_id'=>'required',
            'business_cat_id'=>'required',
            'business_brand_id'=>'required',
            'business_brand_category_name'=>'required',
            'business_brand_category_image'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'link'=>'required',           
        ]);
        if($request->hasFile('business_brand_category_image'))
        {
         $filname = $request->business_brand_category_image->getClientOriginalName();
         $request->business_brand_category_image->move('public/businessbrandcategory', $filname);
        }        
        $businessbrand = new Categorybusiness;
        $businessbrand->business_id = $request->business_id;
        $businessbrand->business_cat_id = $request->business_cat_id;
        $businessbrand->business_brand_id = $request->business_brand_id;
        $businessbrand->business_brand_category_name = $request->business_brand_category_name;
        $businessbrand->business_brand_category_image = $filname;
        $businessbrand->link = $request->link;
        $businessbrand->save();
        return redirect(route('business-brand-category.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::All();   
        $business = Business::All();         
        $businessbrand= BusinessBrand::All();
        $categorybusiness = Categorybusiness::where('id', $id)->first(); 
        return View::make('admin.business-brand-category.business-brand-category-edit', compact(['business','category','businessbrand','categorybusiness']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::All();   
        $business = Business::all(); 
        $businessbrand = BusinessBrand::All();
        $categorybusiness =DB::table('categorybusinesses')->select('categorybusinesses.id as cid','categorybusinesses.link as link','categorybusinesses.business_id as business_id','categorybusinesses.business_cat_id as business_cat_id','categorybusinesses.business_brand_id as business_brand_id','categorybusinesses.business_brand_category_image as business_brand_category_image','categorybusinesses.business_brand_category_name as business_brand_category_name','business_brands.business_brand_name as business_brand_name','businesses.bussiness_name as bussiness_name' ,'categories.business_cat_name as business_cat_name')
        ->leftJoin('categories', function($join){ $join->on('categorybusinesses.business_cat_id', '=', 'categories.id');})
        ->leftJoin('businesses', function($join){ $join->on('categorybusinesses.business_id', '=', 'businesses.id');})
        ->leftJoin('business_brands', function($join){ $join->on('categorybusinesses.business_brand_id', '=', 'business_brands.id');})
        ->where('categorybusinesses.id', '=', "$id")->get()->first();
         return View('fac-Bhavesh-0554.business-brand-category.business-brand-category-edit', compact(['business','category','businessbrand','categorybusiness']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $this->validate($request,[
            'business_id'=>'required',
            'business_cat_id'=>'required',
            'business_brand_id'=>'required',
            'business_brand_category_name'=>'required',           
            'link'=>'required',           
        ]);
        if($request->hasFile('business_brand_category_image'))
        {
         $filname = $request->business_brand_category_image->getClientOriginalName();
         $request->business_brand_category_image->move('public/businessbrandcategory', $filname);
        }    
        else
        {
        $filname = $request->business_brand_category_image1;   
        }       
        $categorybusiness = Categorybusiness::find($id);
        $categorybusiness->business_id = $request->business_id;
        $categorybusiness->business_cat_id = $request->business_cat_id;
        $categorybusiness->business_brand_id = $request->business_brand_id;
        $categorybusiness->business_brand_category_name = $request->business_brand_category_name;
        $categorybusiness->business_brand_category_image = $filname;
        $categorybusiness->link = $request->link;
        $categorybusiness->update();
        return redirect(route('business-brand-category.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Categorybusiness::where('id',$id)->delete();
        return redirect(route('business-brand-category.index'));
    }
}
