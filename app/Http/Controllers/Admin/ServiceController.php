<?php

namespace App\Http\Controllers\Admin;

use App\Model\Service;
use App\Model\Prospect;
use App\Model\ProspectSub;
use App\Model\ServiceImages;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class ServiceController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $service = Service::All();
        return view('fac-Bhavesh-0554/services/services',compact('service'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('fac-Bhavesh-0554.services.create');
    }
    
    public function createmaster()
    {
        $service = Service::All();
        return view('fac-Bhavesh-0554/services/createmaster',compact('service'));
    }
    
    
    public function getSelectedServiceData($id)
    {
        //$id = $request->service_id;
        $result = array();
        $sub_result = array();
        $sub_service = DB::table('prospect_services')->where('service_id',$id)->get();
        foreach($sub_service as $servicess):
            
            array_push($result,array(
                'id'=>'m'.$servicess->id,
                'parent'=>'#',
                'text'=>$servicess->name
            ));
            
            $sub_sub_service = DB::table('prospect_services_sub')->where('service_sub_id',$servicess->id)->get();
            foreach($sub_sub_service as $sub_service):
                array_push($result,array(
                    'id'=>'s'.$sub_service->id,
                    'parent'=>'m'.$servicess->id,
                    'text'=>$sub_service->name
                ));
            endforeach;
            if(count($sub_sub_service)>0){
                //array_push($result,array($servicess->name=>$sub_result));
            }
        endforeach;
        //print_r($result);
        return json_encode($result);
    }
    
    public function masterStore(Request $request)
    {
        //echo "ajsdfkjldsaf";
        //print_r($request->service_names);
        
        $this->validate($request,[
            'service_names' =>'required',  
            'service_value.*'=>'required'
        ]);
        
        $size = count($_POST['service_value']);
        //echo '<b>sub Service Main Name Size : '.$size.' of Sub Main Service Size </b><br/><br/>';
        for($i=0;$i<$size;$i++){
            
            $service  = new Prospect;
            $service->name = $_POST['service_value'][$i];
            $service->service_id=$request->service_names;
            $service->price=$request->$_POST['service_price'][$i];
            //echo 'Value : '.$_POST['service_value'][$i].' </b><br/><br/>';
            $service->save();
        }
        
        $subExists = $request->subservice;
       
        if($subExists == 'on'){
            
            $sub_service_size = count($_POST['subservice_name']);
            
            //echo '<b>sub Service Name Size : '.$sub_service_size. ' of Service Size</b> <br/><br/><br/>';
            for($j=0;$j<$sub_service_size;$j++){
            
                $sub_service_val_size = count($_POST['subservice_val']);
                
                $service_sub  = new Prospect;
                $service_sub->name=$_POST['subservice_name'][$j];
                $service_sub->service_id=$request->service_names;
                $service_sub->save();
                $service_sub_id = $service_sub->id;
                
                //echo 'Value : '.$_POST['subservice_name'][$j].'<br/>';
                
                //echo '<b>sub Service Value Size : '.$sub_service_val_size.' of Sub Service Size </b><br/><br/>';
                
                for($c=0;$c<$sub_service_val_size;$c++){
                    $service_sub_sub = new ProspectSub;
                    $service_sub_sub->service_sub_id = $service_sub_id;
                    $service_sub_sub->name = $_POST['subservice_val'][$c];
                    $service_sub_sub->price=$_POST['subservice_price'][$c];
                    $service_sub_sub->save();
                    //echo 'Value : '.$_POST['subservice_val'][$c].' <br/>';
                }
            }
        }
        
       // exit();
        
        //$service = Service::All();
        return redirect('fac-Bhavesh-0554/services/createmaster')->with('success','Success fully added');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'service_name'=>'required',
        ]);            
        $businessbrand = new Service;
        $businessbrand->service_name = $request->service_name;
        $businessbrand->service_tag = $request->service_tag;
        $businessbrand->description= $request->description;
        $businessbrand->description1= $request->description1;
        $businessbrand->save();
        return redirect(route('services.index'))->with('success','Service SuccessFully Add');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Admin\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Admin\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
       return View('fac-Bhavesh-0554.services.edit',compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Admin\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Service $service)
    {
        $this->validate($request,[
            'service_name'=>'required',
            
                              
        ]);            
        $businessbrand = $service;
        $businessbrand->service_name = $request->service_name;
        $businessbrand->description = $request->description;
        $businessbrand->service_tag = $request->service_tag;
        $businessbrand->description1= $request->description1;
        $businessbrand->update();
        return redirect(route('services.index'))->with('success','Service SuccessFully Update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Admin\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {
        $service->delete();
        return redirect(route('services.index'));
    }
}
