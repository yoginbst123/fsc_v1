<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\BusinessBrand;
use App\Model\Category;
use App\Model\Business;
use DB;
class BusinessBrandController extends Controller
{ public function __construct()
    {
      $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $category = Category::All();   
        $business = Business::all();
        //$businessbrand = BusinessBrand::All(); 
        $businessbrand = DB::select("select business_brands.id as cid,business_brands.link as link,business_brands.business_brand_image as business_brand_image,business_brands.business_brand_name as business_brand_name,categories.business_cat_name,categories.id as bid,businesses.bussiness_name as bussiness_name,businesses.id
        from business_brands left join categories on categories.id=business_brands.business_cat_id
        left join businesses on businesses.id=business_brands.business_id");  
        return view('fac-Bhavesh-0554.business-brand.business-brand', compact(['business','category','businessbrand']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $category = Category::All();   
        $business = Business::all();
        $businessbrand = BusinessBrand::All();   
        return view('fac-Bhavesh-0554.business-brand.business-brand-add-new',compact(['business','category','businessbrand']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'business_id'=>'required',
            'business_cat_id'=>'required',
            'business_brand_name'=>'required',
            'business_brand_image'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'link'=>'required',           
        ]);
        if($request->hasFile('business_brand_image'))
        {
         $filname = $request->business_brand_image->getClientOriginalName();
         $request->business_brand_image->move('public/businessbrand', $filname);
        }        
        $businessbrand = new BusinessBrand;
        $businessbrand->business_id = $request->business_id;
        $businessbrand->business_cat_id = $request->business_cat_id;
        $businessbrand->business_brand_name = $request->business_brand_name;
        $businessbrand->business_brand_image = $filname;
        $businessbrand->link = $request->link;
        $businessbrand->save();
        return redirect(route('business-brand.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        $category = Category::All();   
        $business = Business::All(); 
        $businessbrand= BusinessBrand::where('id', $id)->first();
        return View::make('fac-Bhavesh-0554.business-brand.business-brand', compact(['business','category','businessbrand']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getcategory(Request $request)
    {
        $data = Category::select('business_cat_name','id')->where('bussiness_name',$request->id)->take(100)->get();
        return response()->json($data);  
    }
    public function getbrand(Request $request)
    {
        $data = BusinessBrand::select('business_brand_name','id')->where('business_cat_id',$request->id)->take(100)->get();
        return response()->json($data);  
    } 
     public function edit($id)
    {     
        $category = Category::All();   
        $business = Business::all(); 
        $businessbrand = DB::table('business_brands')->select('business_brands.id as cid','business_brands.link as link','business_brands.business_brand_image as business_brand_image','business_brands.business_brand_name as business_brand_name','business_brands.business_cat_id as business_cat_id','business_brands.business_id as business_id','businesses.bussiness_name as bussiness_name' ,'categories.business_cat_name as business_cat_name')
        ->leftJoin('categories', function($join){ $join->on('business_brands.business_cat_id', '=', 'categories.id');})
        ->leftJoin('businesses', function($join){ $join->on('business_brands.business_id', '=', 'businesses.id');})
        ->where('business_brands.id', '=', "$id")->get()->first();
         return View('fac-Bhavesh-0554.business-brand.business-brand-edit',compact(['business','category','businessbrand']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'business_id'=>'required',
            'business_cat_id'=>'required',
            'business_brand_name'=>'required',            
            'link'=>'required',           
        ]);
        if($request->hasFile('business_brand_image'))
        {
         $filname = $request->business_brand_image->getClientOriginalName();
         $request->business_brand_image->move('public/businessbrand', $filname);
        }     
        else
        {
            $filname = $request->business_brand_image1;   
        }   
        $businessbrand = BusinessBrand::find($id);
        $businessbrand->business_id = $request->business_id;
        $businessbrand->business_cat_id = $request->business_cat_id;
        $businessbrand->business_brand_name = $request->business_brand_name;
        $businessbrand->business_brand_image = $filname;
        $businessbrand->link = $request->link;
        $businessbrand->update();
        return redirect(route('business-brand.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        BusinessBrand::where('id',$id)->delete();
        return redirect(route('business-brand.index'));
    }
}
