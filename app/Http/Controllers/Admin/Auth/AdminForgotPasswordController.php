<?php
namespace App\Http\Controllers\Admin\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Password;
use App\Model\Admin;
use Illuminate\Http\Request;
class AdminForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

  //  use SendsPasswordResetEmails;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:admin');
    }
    
     /**
     * Display the form to request a password reset link.
     *
     * @return \Illuminate\Http\Response
     */
  
    public function index()
    { 
      return view('fac-Bhavesh-0554/adminpassword/adminpassword');
    }
    
     public function store(Request $request)
    {  
      $email = $request->email;
      $user = Admin::where('email', '=', $email)->first();
     if ($user === null) 
    {
   return redirect(route('adminpassword.index'))->with('error', 'This Email not found');
    }
   else
   {
    $activation_link = route('adminpassword.edit', ['id' => $user->id,'email' => $email, 'token' => urlencode($user->remember_token)]);
    
    $msg = 'Forgot Password';
    $data = array('email' => $email,'token'=>$activation_link,'messages'=>$msg); 
  \Mail::send( 'fac-Bhavesh-0554/forgotpasswordadmin', $data, function( $message ) use ($data)
       {
           $message->to($data['email'])->from($data['email'],$data['messages'],$data['token'])->subject('Forgot Password');
       });      
       return redirect(route('adminpassword.index'))->with('success', 'Please Check Your Email');
   }
    }
    
    
    
     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $business = Admin::where('id',$id)->first();    
        return View('adminpassword.index',compact('business')); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {  
        $business = Admin::where('id',$id)->first(); 
        return view('fac-Bhavesh-0554.adminpassword.edit',compact('business'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'newpassword'=>'required',            
                        
        ]);
       $business =Admin::find($id);
        $business->password = bcrypt($request->newpassword);
         $business->update();
        return redirect(route('adminpassword.index'))->with('success', 'Your Password Successfully update');
    }
public function broker()
    {
        return Password::broker('admins');
    }
}
