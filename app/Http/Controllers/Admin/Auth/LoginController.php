<?php
namespace App\Http\Controllers\admin\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\AdminResetPasswordNotification;
use Auth;
class LoginController extends Controller
{
    public function __construct()
    {
      $this->middleware('guest:admin');
    }
    public function showLoginForm()
    {
      return view('fac-Bhavesh-0554.login');
    }
    public function login(Request $request)
    {
      //  ECHO '22222';EXIT;
      // Validate the form data
      $this->validate($request, [
        'email'   => 'required|email',
        'password' => 'required|min:6'
      ]);
      // Attempt to log the user in
      //if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
        // if successful, then redirect to their intended location
       // return redirect()->back()->with('message', 'Registered successfully, please login...!');
     //  return redirect()->back()->withInput($request->only('email', 'remember'))->with('success','Your Username Or Password Is Invalid...');
       // return redirect()->intended(route('fac-Bhavesh-0554.dashboard'))->with('success','You are success fully Login !!!');
     // }
if(Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
     
    } 
    else{
         echo "save1";
    }
       
   
//return redirect()->back()->withInput($request->only('email', 'remember'))->with('error','Your Username Or Password Is Invalid...');
    }
    public function logout(Request $request)
    {
        Auth::guard('admin')->logout();
        $request->session()->flush();
        $request->session()->regenerate();
        return redirect()->guest(route( 'fac-Bhavesh-0554.login'));
    }
public function sendPasswordResetNotification($token)
    {
        $this->notify(new AdminResetPasswordNotification($token));
    }



}