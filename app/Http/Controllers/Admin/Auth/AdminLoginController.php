<?php
namespace App\Http\Controllers\Admin\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\AdminResetPasswordNotification;
use Illuminate\Foundation\Auth\AuthenticatesAdmin;
use Auth;
class AdminLoginController extends Controller
{
  /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesAdmin;
    
        /**
         * Where to redirect users after login.
         *
         * @var string
         */
        protected $redirectTo = 'fac-Bhavesh-0554/dashboard';
    
        /**
         * Create a new controller instance.
         *
         * @return void
         */
        
        public function __construct()
        {
            $this->middleware('guest:admin')->except('logout');
        }
public function sendPasswordResetNotification($token)
    {
        $this->notify(new AdminResetPasswordNotification($token));
    }
}