<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Employee;
use App\employees\Fscemployee;
use Illuminate\Http\Request;
use App\Model\Position;
use App\Model\Branch;
use App\Model\Rules;
use App\User;
use View;
use Validator;
use Redirect;
use Session;
use DB;
use Auth;
use App\Model\Admin;
use Hash;
use App\Model\Super;
use App\Model\Schedule;
use App\Model\Msg;
use App\Model\Task;
use Illuminate\Support\Facades\Input;
use App\Front\ApplyEmployment;
use Illuminate\Support\Facades\File;
use App\Model\Category;
use App\Model\Product;
use App\Model\Utilities;
use App\Model\Other_businesstype;
use App\Model\InsuranceCompany;

use App\Model\Relatednames;
use App\Model\Accountcode;
use App\Model\Notesrelated;
use App\Model\admin_profession;

use App\Model\Teams;

class VendorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $position = Position::All();
        $category = Category::orderby('business_cat_name', 'asc')->get();
        $employee = DB::table('employees')->select('employees.*', 'utilities.id as uid', 'utilities.utilitiesname as uname', 'insurance_companies.id as iid', 'insurance_companies.insurance_company as iname')
            ->leftJoin('utilities', function ($join) {
                $join->on('utilities.id', '=', 'employees.utilitiesname');
            })
            ->leftJoin('insurance_companies', function ($join) {
                $join->on('insurance_companies.id', '=', 'employees.insurance_company');
            })
            ->where('employees.type', '=', 'Vendor')->orderBy('employees.vendordate', 'desc')->get();


        // $employee = Employee::where('type','=','Vendor')->orderBy('vendordate', 'desc')->get(); 
        return view('fac-Bhavesh-0554/vendor/vendor', compact(['employee', 'position', 'category']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function relatednames()
    {
        $newopt = Input::get('newopt');
        $sign = Input::get('sign');
        if (DB::table('relatednames')->where('relatednames', '=', $newopt)->exists()) {
            //return response()->json(['status' => 'error']);
        } else {
            $relatednames = new Relatednames;
            $relatednames->relatednames = $newopt;
            // $relatednames->sign = $sign;
            $relatednames->save();
            $id = $relatednames->id;
            if ($relatednames) {
                return response()->json([
                    'status' => 'success',
                    'id'=>$id,
                    'newopt' => $newopt]);
            } else {
                return response()->json([
                    'status' => 'error']);
            }
        }
    }
    
    public function profession(){
        
        $profession = Input::get('profession_name');
        if(DB::table('admin_profession')->where('name', '=', $profession)->exists()){
            return response()->json([
                    'status' => 'Message',
                    'Msg'=>'Name is exists, Please try again']);
        }else{
            $profession_ = new admin_profession;
            $profession_->name = $profession;
            $profession_->created_at = now();
            $profession_->save();
            $lastId = $profession_->id;
            if($profession_){
                return response()->json([
                    'status' => 'success',
                    'id'=>$lastId,
                    'name' => $profession    
                ]);
            }else{
                return response()->json([
                    'status' => 'error']);
            }
        }
    }
    
    public function professionDelete(Request $request){
        
        $id=$request->input('id');
        $student = DB::table('admin_profession')->where('id', $id)->delete();
        if($student){
           $data = array();
            $remove = DB::table('admin_profession')->get();
            foreach($remove as $rm){
               $data[] =[
                    'status' => 'success',
                    'id'=>$rm->id,
                    'name' => $rm->name
                ];
            }
        }
        return json_encode($data);
    }
    
   


    public function notesrelateds()
    {
        $newopt2 = Input::get('newopt2');
        //$sign = Input::get('sign');
        if (DB::table('notesrelateds')->where('notesrelated', '=', $newopt2)->exists()) {
            //return response()->json(['status' => 'error']);
        } else {
            $notesrelated = new Notesrelated;
            $notesrelated->notesrelated = $newopt2;
            // $notesrelated->sign = $sign;
            $notesrelated->save();
            $id = $notesrelated->id;
            if ($notesrelated) {
                return response()->json([
                    'status' => 'success',
                    'id'=>$id,
                    'newopt2' => $newopt2]);
            } else {
                return response()->json([
                    'status' => 'error']);
            }
        }
    }


    public function create()
    {
        $category = Category::orderby('business_cat_name', 'asc')->get();
        $products1 = DB::table('products')->orderby('productname', 'asc')->get();
        $positions1 = DB::table('positions')->orderby('position', 'asc')->get();

        $accountcode = Accountcode::orderby('accountcode', 'asc')->get();
        $utilities = DB::table('utilities')->orderby('utilitiesname', 'asc')->get();
        $insurance = DB::table('insurance_companies')->orderby('insurance_company', 'asc')->get();
        $businesstypes = DB::table('other_businesstypes')->orderby('businesstype', 'asc')->get();
        return view('fac-Bhavesh-0554/vendor/create', compact(['businesstypes', 'insurance', 'utilities', 'positions1', 'category', 'products1', 'accountcode']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'business_name' => 'required|unique:employees',
            'productid' => 'required'
        ]);

        $employee = new Employee;
        $c = $request->productid;
        if (empty($c)) {
            $List = '';
        } else {
            $List = implode(', ', $c);
        }

        $posc = $request->positions;
        if (empty($posc)) {
            $poscList = '';
        } else {
            $poscList = implode(', ', $posc);
        }

        // $c1 = $request->business_catagory_name;
        // if(empty($c1))
        // {
        //     $List1='';
        // }
        // else
        // {
        //     $List1 = implode(', ', $c1); 
        // }

        // if($request->categorys =='Utility')
        // {

        //     $c2 = $request->utilitiesname;
        //     if(empty($c2))
        //     {
        //         $List2='';
        //     }
        //     else
        //     {
        //         $List2 = $request->utilitiesname; 
        //     }
        //         $employee->utilitiesname= isset($List2) ? $List2:"";
        // }  

        // if($request->categorys =='Insurance')
        // {

        //     $c3 = $request->insurance_company;
        //     if(empty($c3))
        //     {
        //         $List2i='';
        //     }
        //     else
        //     {
        //         $List2i = $request->insurance_company; 
        //     }

        //     $employee->insurance_company= isset($List2i) ? $List2i:"";
        // }

        //$employee->entity = isset($request->entity) ? $request->entity:"";

        if ($request->entity == 'Business') {
            $c1 = $request->business_catagory_name;
            if (empty($c1)) {
                $List1 = '';
            } else {
                $List1 = implode(', ', $c1);
            }
            $employee->business_catagory_name = isset($List1) ? $List1 : "";
        } else {
            if ($request->categorys == 'Utility') {

                $c2 = $request->utilitiesname;
                if (empty($c2)) {
                    $List2 = '';
                } else {
                    $List2 = $request->utilitiesname;
                }

                $employee->utilitiesname = isset($List2) ? $List2 : "";
            } else {
                $c3 = $request->insurance_company;
                if (empty($c3)) {
                    $List2i = '';
                } else {
                    $List2i = $request->insurance_company;
                }

                $employee->insurance_company = isset($List2i) ? $List2i : "";

            }
            $employee->categorys = $request->categorys;
        }


        $employee->employee_id = $request->employee_id;
        $employee->product = isset($List) ? $List : "";

        $employee->vendorid = Auth::user()->fname . ' ' . Auth::user()->mname . ' ' . Auth::user()->lname;
        $employee->type = 'Vendor';
        $employee->vendortype = 'Admin';

        $employee->billingtoo123 = isset($request->billingtoo123) ? $request->billingtoo123 : "";
        $employee->billingtoo = isset($request->billingtoo) ? $request->billingtoo : "";
        $employee->emailbli2 = isset($request->emailbli2) ? $request->emailbli2 : "";
        //$employee->categorys= $request->categorys;
        //$employee->categorys= $List1;
        $employee->entity = isset($request->entity) ? $request->entity : "";
        $employee->firstName = isset($request->firstname) ? $request->firstname : "";
        $employee->middleName = isset($request->middlename) ? $request->middlename : "";
        $employee->lastName = isset($request->lastname) ? $request->lastname : "";
        $employee->address1 = isset($request->address) ? $request->address : "";
        $employee->address2 = isset($request->address1) ? $request->address1 : "";
        $employee->city = isset($request->city) ? $request->city : "";
        $employee->stateId = isset($request->stateId) ? $request->stateId : "";
        $employee->zip = isset($request->zip) ? $request->zip : "";
        $employee->countryId = isset($request->countryId) ? $request->countryId : "";
        $employee->telephoneNo1 = isset($request->telephone) ? $request->telephone : "";
        $employee->telephoneNo2 = isset($request->motelephoneile1) ? $request->motelephoneile1 : "";
        $employee->ext1 = isset($request->ext1) ? $request->ext1 : "";
        $employee->ext2 = isset($request->ext2) ? $request->ext2 : "";
        $employee->telephoneNo1Type = isset($request->telephoneNo1Type) ? $request->telephoneNo1Type : "";
        $employee->telephoneNo2Type = isset($request->telephoneNo2Type) ? $request->telephoneNo2Type : "";
        $employee->email = isset($request->email) ? $request->email : "";
        $employee->nametype = isset($request->minss) ? $request->minss : "";
        $employee->fax = isset($request->business_fax) ? $request->business_fax : "";
        $employee->website = isset($request->website) ? $request->website : "";
        $employee->etelephone1 = isset($request->mobile_1) ? $request->mobile_1 : "";
        $employee->eteletype1 = isset($request->mobiletype_1) ? $request->mobiletype_1 : "";
        $employee->eext1 = isset($request->ext2_1) ? $request->ext2_1 : "";
        $employee->etelephone2 = isset($request->mobile_2) ? $request->mobile_2 : "";
        $employee->eteletype2 = isset($request->mobiletype_2) ? $request->mobiletype_2 : "";
        $employee->eext2 = isset($request->ext2_2) ? $request->ext2_2 : "";
        $employee->efax = isset($request->contact_fax) ? $request->contact_fax : "";
        $employee->business_name = isset($request->business_name) ? $request->business_name : "";
        $employee->accountcode = isset($request->accountcode) ? $request->accountcode : "";
        $employee->account_name = isset($request->account_name) ? $request->account_name : "";
        $employee->check = isset($request->status) ? $request->status : "";
        $employee->vendordate = date('Y-m-d');
        $employee->efax = isset($request->contact_fax_1) ? $request->contact_fax_1 : "";
        $employee->account_belongs_to = isset($request->account_belongs_to) ? $request->account_belongs_to : "";


        $employee->positions = isset($poscList) ? $poscList : "";
        $employee->howtoshow = isset($request->howtoshow) ? $request->howtoshow : "";
        $employee->accno = isset($request->accno) ? $request->accno : "";

        $employee->managermr1 = isset($request->managermr1) ? $request->managermr1 : "";
        $employee->managerfname1 = isset($request->managerfname1) ? $request->managerfname1 : "";
        $employee->managermname1 = isset($request->managermname1) ? $request->managermname1 : "";
        $employee->managerlname1 = isset($request->managerlname1) ? $request->managerlname1 : "";

        $employee->managermr2 = isset($request->managermr2) ? $request->managermr2 : "";
        $employee->managerfname2 = isset($request->managerfname2) ? $request->managerfname2 : "";
        $employee->managermname2 = isset($request->managermname2) ? $request->managermname2 : "";
        $employee->managerlname2 = isset($request->managerlname2) ? $request->managerlname2 : "";

        $employee->contactmanagermr1 = isset($request->contactmanagermr1) ? $request->contactmanagermr1 : "";
        $employee->contactmanagerfname1 = isset($request->contactmanagerfname1) ? $request->contactmanagerfname1 : "";
        $employee->contactmanagermname1 = isset($request->contactmanagermname1) ? $request->contactmanagermname1 : "";
        $employee->contactmanagerlname1 = isset($request->contactmanagerlname1) ? $request->contactmanagerlname1 : "";

        $employee->contactmanagermr2 = isset($request->contactmanagermr2) ? $request->contactmanagermr2 : "";
        $employee->contactmanagerfname2 = isset($request->contactmanagerfname2) ? $request->contactmanagerfname2 : "";
        $employee->contactmanagermname2 = isset($request->contactmanagermname2) ? $request->contactmanagermname2 : "";
        $employee->contactmanagerlname2 = isset($request->contactmanagerlname2) ? $request->contactmanagerlname2 : "";

        $employee->vendordate = date('Y-m-d H:i:s');
        $employee->business_catagory_name = isset($List1) ? $List1 : "";
        $employee->notes = isset($request->note) ? $request->note : "";
        $employee->save();
        return redirect('fac-Bhavesh-0554/vendor')->with('success', 'You Are Registered Successfully');
    }

    public function servicedel2(Request $request)
    {
        $users = DB::table('clientservices')->where('id', '=', $request->input('id'))->delete();
        if ($users) {
            echo 'Data Deleted';
        }
        //return redirect(route('customer.index'))->with('error','Your Record Deleted Successfully');
    }

    public function servicedel3(Request $request)
    {
        $users = DB::table('clientservicetitles')->where('id', '=', $request->input('id'))->delete();

        if ($users) {
            echo 'Data Deleted';
        }
        //return redirect(route('customer.index'))->with('error','Your Record Deleted Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $common = Commonregister::where('id', $id)->first();
        return View::make('fac-Bhavesh-0554.vendor.vendor', compact(['common']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $emp = Employee::where('id', $id)->first();
        $category = Category::All();
        $positions1 = DB::table('positions')->orderby('position', 'asc')->get();

        $products1 = DB::table('products')->orderby('productname', 'asc')->get();
        $newclient = Employee::where('id', $id)->update(['newemp' => 2]);
        $accountcode = Accountcode::orderby('accountcode', 'asc')->get();
        $insurance = DB::table('insurance_companies')->orderby('insurance_company', 'asc')->get();


        $businesstypes = DB::table('other_businesstypes')->orderby('businesstype', 'asc')->get();


        $conversation = DB::table('conversation_sheet as t1')->select('t1.*', 't2.id as ids', 't2.relatednames')
            ->Join('relatednames as t2', function ($join) {
                $join->on('t2.id', '=', 't1.conrelatedname');
            })
            ->where('t1.vendorid', '=', $id)
            ->get();

        $listclient = DB::table('commonregisters')->where('first_name', '!=', '')->where('last_name', '!=', '')->orderBy('first_name', 'ASC')->get();
        $listvendoe = DB::table('employees')->where('type', 'Vendor')->where('firstName', '!=', '')->where('lastName', '!=', '')->orderBy('firstName', 'ASC')->get();
        $listemployeeuser = DB::table('employees')->where('type', 'employee')->where('firstName', '!=', '')->where('lastName', '!=', '')->orderBy('firstName', 'ASC')->get();
        //print_r($listemployeeuser);die;
        $relatedNames = DB::table('relatednames')->get();
        $utilities = DB::table('utilities')->orderby('utilitiesname', 'asc')->get();

        $notesdata = DB::table('notes_sheet as t1')->select('t1.*', 't2.id as ids', 't2.notesrelated')
            ->Join('notesrelateds as t2', function ($join) {
                $join->on('t2.id', '=', 't1.noterelated');
            })
            ->where('t1.notevendorid', '=', $id)
            ->get();
        //print_r($notesdata);die;
        $NotesNames = DB::table('notesrelateds')->get();

        return view('fac-Bhavesh-0554.vendor.edit', compact(['businesstypes', 'insurance', 'utilities', 'notesdata', 'NotesNames', 'listclient', 'listvendoe', 'listemployeeuser', 'relatedNames', 'conversation', 'positions1', 'emp', 'category', 'products1', 'accountcode']));
    }

    public function updatecoversation(Request $request)
    {
        //print_r($_REQUEST);EXIT;

        $this->validate($request, [
            'type_user' => 'required',
            'conrelatedname' => 'required',
        ]);


        $CovID = $request->CovID;
        $date = $request->date;
        $day = $request->day;
        $time = $request->time;
        $type_user = $request->type_user;
        if ($request->clientid != '') {
            $clientid = $request->clientid;
        } else {
            $clientid = '';
        }

        if ($request->employeeuserid != '') {
            $employeeuserid = $request->employeeuserid;
        } else {
            $employeeuserid = '';
        }

        if ($request->vendorid != '') {
            $vendorid = $request->vendorid;
        } else {
            $vendorid = '';
        }

        if ($request->otherid != '') {
            $otherid = $request->otherid;
        } else {
            $otherid = '';
        }

        $conrelatedname = $request->conrelatedname;
        $condescription = $request->condescription;
        $connotes = $request->connotes;

        $returnValue = DB::table('conversation_sheet')->where('id', $CovID)
            ->update([
                'creattiondate' => $date,
                'day' => $day,
                'time' => $time,
                'type_user' => $type_user,
                'clientid' => $clientid,
                'employeeuserid' => $employeeuserid,
                'vendorid' => $vendorid,
                'otherid' => $otherid,
                'conrelatedname' => $conrelatedname,
                'condescription' => $condescription,
                'connotes' => $connotes,

            ]);


        return redirect()->back()->with('success', 'Record Updated Successfully');
    }

    public function updatenotes(Request $request)
    {
        // echo "<pre>";
        // print_r($_REQUEST);die;

        $this->validate($request, [
            'notetype_user' => 'required',
            'notesrelatedcat' => 'required',
        ]);


        $NoteID = $request->NoteID;
        $notedate = $request->notedate;
        $noteday = $request->noteday;
        $notetime = $request->notetime;
        $notetype_user = $request->notetype_user;
        if ($request->noteclientid != '') {
            $noteclientid = $request->noteclientid;
        } else {
            $noteclientid = '';
        }

        if ($request->noteemployeeuserid != '') {
            $noteemployeeuserid = $request->noteemployeeuserid;
        } else {
            $noteemployeeuserid = '';
        }

        if ($request->notevendorid != '') {
            $notevendorid = $request->notevendorid;
        } else {
            $notevendorid = '';
        }

        if ($request->noteotherid != '') {
            $noteotherid = $request->noteotherid;
        } else {
            $noteotherid = '';
        }

        //$notesrelatedname=$request->notesrelatedname;
        $notesrelatedcat = $request->notesrelatedcat;
        $notes = $request->notes;

        $returnValue = DB::table('notes_sheet')->where('id', $NoteID)
            ->update([
                'creattiondate' => $notedate,
                'noteday' => $noteday,
                'notetime' => $notetime,
                'notetype_user' => $notetype_user,
                'noteclientid' => $noteclientid,
                'noteemployeeuserid' => $noteemployeeuserid,
                'notevendorid' => $notevendorid,
                'noteotherid' => $noteotherid,
                //'notesrelatedname' =>$notesrelatedname,
                'notesrelatedcat' => $notesrelatedcat,
                'notes' => $notes,

            ]);


        return redirect()->back()->with('success', 'Record Updated Successfully');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        /*$this->validate($request,[
         'business_catagory_name' =>'required',
         'vendor_name' =>'required',
         'productid' =>'required'
         ]);*/
        // exit('1112');
        $employee = Employee::find($id);

        $c = $request->productid;
        if (empty($c)) {
            $List = '';
        } else {
            $List = implode(', ', $c);
        }

        $posc = $request->positions;
        if (empty($posc)) {
            $poscList = '';
        } else {
            $poscList = implode(', ', $posc);
        }

        $c1 = $request->business_catagory_name;
        if (empty($c1)) {
            $List1 = '';
        } else {
            $List1 = implode(', ', $c1);
        }
        if ($request->categorys == 'Utility') {

            $c2 = $request->utilitiesname;
            if (empty($c2)) {
                $List2 = '';
            } else {
                $List2 = $request->utilitiesname;
            }
            $employee->utilitiesname = isset($List2) ? $List2 : "";
        }
        if ($request->categorys == 'Insurance') {

            $c3 = $request->insurance_company;
            if (empty($c3)) {
                $List2i = '';
            } else {
                $List2i = $request->insurance_company;
            }
            $employee->insurance_company = isset($List2i) ? $List2i : "";
        }
        $employee->billingtoo123 = isset($request->billingtoo123) ? $request->billingtoo123 : "";
        $employee->billingtoo = isset($request->billingtoo) ? $request->billingtoo : "";
        $employee->emailbli2 = isset($request->emailbli2) ? $request->emailbli2 : "";
        $employee->employee_id = isset($request->employee_id) ? $request->employee_id : "";
        $employee->vendordate = date('Y-m-d');
        $employee->firstName = isset($request->firstname) ? $request->firstname : "";
        $employee->middleName = isset($request->middlename) ? $request->middlename : "";
        $employee->lastName = isset($request->lastname) ? $request->lastname : "";
        $employee->address1 = isset($request->address) ? $request->address : "";
        $employee->address2 = isset($request->address1) ? $request->address1 : "";
        $employee->city = isset($request->city) ? $request->city : "";
        $employee->stateId = isset($request->stateId) ? $request->stateId : "";
        $employee->zip = isset($request->zip) ? $request->zip : "";
        $employee->countryId = isset($request->countryId) ? $request->countryId : "";
        $employee->telephoneNo1 = isset($request->telephone) ? $request->telephone : "";
        $employee->telephoneNo2 = isset($request->motelephoneile1) ? $request->motelephoneile1 : "";
        $employee->ext1 = isset($request->ext1) ? $request->ext1 : "";
        $employee->ext2 = isset($request->ext2) ? $request->ext2 : "";
        $employee->telephoneNo1Type = isset($request->telephoneNo1Type) ? $request->telephoneNo1Type : "";
        $employee->telephoneNo2Type = isset($request->telephoneNo2Type) ? $request->telephoneNo2Type : "";
        $employee->email = isset($request->email) ? $request->email : "";
        $employee->nametype = isset($request->minss) ? $request->minss : "";
        $employee->fax = isset($request->business_fax) ? $request->business_fax : "";
        $employee->website = isset($request->website) ? $request->website : "";
        $employee->etelephone1 = isset($request->mobile_1) ? $request->mobile_1 : "";
        $employee->eteletype1 = isset($request->mobiletype_1) ? $request->mobiletype_1 : "";
        $employee->categorys = isset($request->categorys) ? $request->categorys : "";

        $employee->eext1 = isset($request->ext2_1) ? $request->ext2_1 : "";

        $employee->etelephone2 = isset($request->mobile_2) ? $request->mobile_2 : "";
        $employee->eteletype2 = isset($request->mobiletype_2) ? $request->mobiletype_2 : "";
        $employee->eext2 = isset($request->ext2_2) ? $request->ext2_2 : "";

        $employee->efax = isset($request->contact_fax) ? $request->contact_fax : "";
        $employee->efax = isset($request->contact_fax_1) ? $request->contact_fax_1 : "";
        $employee->business_name = isset($request->business_name) ? $request->business_name : "";
        $employee->business_catagory_name = isset($List1) ? $List1 : "";
        $employee->accountcode = isset($request->accountcode) ? $request->accountcode : "";
        $employee->account_name = isset($request->account_name) ? $request->account_name : "";
        $employee->account_belongs_to = isset($request->account_belongs_to) ? $request->account_belongs_to : "";
        $employee->product = isset($List) ? $List : "";
        $employee->positions = isset($poscList) ? $poscList : "";
        $employee->howtoshow = isset($request->howtoshow) ? $request->howtoshow : "";
        $employee->accno = isset($request->accno) ? $request->accno : "";

        $employee->notes = isset($request->note) ? $request->note : "";
        $employee->check = isset($request->status) ? $request->status : "";
        $employee->status = isset($request->status) ? $request->status : "";


        $employee->managermr1 = isset($request->managermr1) ? $request->managermr1 : "";
        $employee->managerfname1 = isset($request->managerfname1) ? $request->managerfname1 : "";
        $employee->managermname1 = isset($request->managermname1) ? $request->managermname1 : "";
        $employee->managerlname1 = isset($request->managerlname1) ? $request->managerlname1 : "";

        $employee->managermr2 = isset($request->managermr2) ? $request->managermr2 : "";
        $employee->managerfname2 = isset($request->managerfname2) ? $request->managerfname2 : "";
        $employee->managermname2 = isset($request->managermname2) ? $request->managermname2 : "";
        $employee->managerlname2 = isset($request->managerlname2) ? $request->managerlname2 : "";

        $employee->contactmanagermr1 = isset($request->contactmanagermr1) ? $request->contactmanagermr1 : "";
        $employee->contactmanagerfname1 = isset($request->contactmanagerfname1) ? $request->contactmanagerfname1 : "";
        $employee->contactmanagermname1 = isset($request->contactmanagermname1) ? $request->contactmanagermname1 : "";
        $employee->contactmanagerlname1 = isset($request->contactmanagerlname1) ? $request->contactmanagerlname1 : "";

        $employee->contactmanagermr2 = isset($request->contactmanagermr2) ? $request->contactmanagermr2 : "";
        $employee->contactmanagerfname2 = isset($request->contactmanagerfname2) ? $request->contactmanagerfname2 : "";
        $employee->contactmanagermname2 = isset($request->contactmanagermname2) ? $request->contactmanagermname2 : "";
        $employee->contactmanagerlname2 = isset($request->contactmanagerlname2) ? $request->contactmanagerlname2 : "";

        $employee->update();
        return redirect(route('vendor.index'))->with('success', 'Your Profile Update Successfully');
    }

    public function getcategory(Request $request)
    {
        $data = Category::select('business_cat_name', 'id')->where('bussiness_name', $request->id)->take(1000)->get();
        return response()->json($data);
    }

    public function locked2(Request $request)
    {
        $lock = $request->id;
        $cid = $request->clientid;
        $data = DB::table('clientservices')->where('id', $cid)->update(['locked' => $lock]);
        return response()->json($data);
//return 'success';
    }


    public function getcategory_1(Request $request)
    {
        $data = Category::select('business_cat_name', 'id', 'business_cat_image')->where('id', $request->id)->take(1000)->get();
        return response()->json($data);
    }

    public function getcategory1(Request $request)
    {
        $data = Business::select('bussiness_name', 'id', 'bussiness_image_name', 'newimage')->where('id', $request->id)->take(1000)->get();
        return response()->json($data);
    }


    public function getcategory1m(Request $request)
    {
        $data = BusinessBrand::select('business_brand_name', 'id')->where('business_cat_id', $request->id)->take(1000)->get();
        return response()->json($data);
    }

    public function getcategoryimage(Request $request)
    {
        $data = BusinessBrand::select('business_brand_name', 'id', 'business_brand_image')->where('id', $request->id)->take(1000)->get();
        return response()->json($data);
    }

    public function getcategory1mm(Request $request)
    {
        $data = Categorybusiness::select('business_brand_category_name', 'id')->where('business_brand_id', $request->id)->take(100)->get();
        return response()->json($data);
    }

    public function business_brand_category_name(Request $request)
    {
        $data = Categorybusiness::select('business_brand_category_name', 'id', 'business_brand_category_image')->where('id', $request->id)->take(100)->get();
        return response()->json($data);
    }

    public function Accountcode1(Request $request)
    {
        $data = Accountcode::select('accountcode', 'id', 'account_name', 'account_belongs_to')->where('accountcode', $request->id)->take(100)->get();
        return response()->json($data);
    }

    public function salestax1(Request $request)
    {
        if ($request->typeofservice == '3') {
            $data = DB::table('taxationservice')->select('regularprice1', 'id', 'comboprice1')->where('currency', $request->currency)->where('typeofservice', $request->typeofservice)->where('period', $request->serviceperiod)->where('title', $request->id)->take(100)->get();
        } else {
            $data = DB::table('prices')->select('regularprice', 'id', 'comboprice', 'serviceincludes')->where('currency', $request->currency)->where('typeofservice', $request->id)->where('period', $request->serviceperiod)->take(100)->get();
        }
        return response()->json($data);
    }


    public function deletecon(Request $request)
    {
        $users = DB::table('contact_userinfos')->where('id', '=', $request->input('id'))->delete();

        if ($users) {
            echo 'Data Deleted';
        }
        //return redirect(route('customer.index'))->with('error','Your Record Deleted Successfully');
    }

    public function notedelete($id)
    {
        // Contact_userinfo::where('id',$id)->delete(); 
        DB::table('notes')->where('id', '=', $id)->delete();
        return redirect('fac-Bhavesh-0554/vendor/');
    }

    public function locksdelete($id)
    {
        // Contact_userinfo::where('id',$id)->delete(); 
        DB::table('clientservices')->where('id', '=', $id)->delete();
        return redirect('fac-Bhavesh-0554/vendor/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */


    public function destroy($id)
    {
        Employee::where('id', $id)->delete();
        return redirect(route('vendor.index'))->with('error', 'Your Record Deleted Successfully');
    }

    public function productss()
    {
        $newopt = Input::get('newoptp');
        //      $sign = Input::get('sign');
        if (DB::table('products')->where('productname', '=', $newopt)->exists()) {
            //return response()->json(['status' => 'error']);
        } else {
            $position = new Product;
            $position->productname = $newopt;
            // $position->sign = $sign;
            $position->save();
            if ($position) {
                return response()->json([
                    'status' => 'success',
                    'newopt' => $newopt]);
            } else {
                return response()->json([
                    'status' => 'error']);
            }
        }
    }

    public function utilitiess()
    {
        $newopt = Input::get('newopt');
        if (DB::table('utilities')->where('utilitiesname', '=', $newopt)->exists()) {
            //return response()->json(['status' => 'error']);
        } else {
            $position = new Utilities;
            $position->utilitiesname = $newopt;
            $position->save();
            if ($position) {
                return response()->json([
                    'utilitiesname' => 'success',
                    'newopt' => $newopt]);
            } else {
                return response()->json([
                    'utilitiesname' => 'error']);
            }
        }
    }


    public function otherbusinesss()
    {
        $newopt = Input::get('newopt');
        if (DB::table('other_businesstypes')->where('businesstype', '=', $newopt)->exists()) {
            //return response()->json(['status' => 'error']);
        } else {
            $position = new Other_businesstype;
            $position->businesstype = $newopt;
            $position->save();
            if ($position) {
                return response()->json([
                    'businesstype' => 'success',
                    'newopt' => $newopt]);
            } else {
                return response()->json([
                    'businesstype' => 'error']);
            }
        }
    }

    public function insurances()
    {
        $newopt = Input::get('newopti');
        if (DB::table('insurance_companies')->where('insurance_company', '=', $newopt)->exists()) {
            //return response()->json(['status' => 'error']);
        } else {
            $position = new InsuranceCompany;
            $position->insurance_company = $newopt;
            $position->save();
            if ($position) {
                return response()->json([
                    'insurance_name' => 'success',
                    'newopt' => $newopt]);
            } else {
                return response()->json([
                    'insurance_name' => 'error']);
            }
        }
    }

    public function teams()
    {
        $newopt = Input::get('newopt');
        if (DB::table('teams')->where('team', '=', $newopt)->exists()) {
            //return response()->json(['status' => 'error']);
        } else {
            $position = new Teams;
            $position->team = $newopt;
            $position->save();
            if ($position) {
                return response()->json([
                    'status' => 'success',
                    'newopt' => $newopt]);
            } else {
                return response()->json([
                    'status' => 'error']);
            }
        }
    }


    public function positions()
    {
        $newopt = Input::get('newopt');
        $sign = Input::get('sign');
        if (DB::table('positions')->where('position', '=', $newopt)->exists()) {
            //return response()->json(['status' => 'error']);
        } else {
            $position = new Position;
            $position->position = $newopt;
            // $position->sign = $sign;
            $position->save();
            if ($position) {
                return response()->json([
                    'status' => 'success',
                    'newopt' => $newopt]);
            } else {
                return response()->json([
                    'status' => 'error']);
            }
        }
    }

    public function removeutilitiess(Request $request)
    {
        $student = Utilities::find($request->input('id'));
        if ($student->delete()) {
            echo 'Data Deleted';
        }
    }

    public function removebusinesss(Request $request)
    {
        $student = Other_businesstype::find($request->input('id'));
        if ($student->delete()) {
            echo 'Data Deleted';
        }
    }

    public function removeinsurances(Request $request)
    {
        $student = InsuranceCompany::find($request->input('id'));
        if ($student->delete()) {
            echo 'Data Deleted';
        }
    }


    public function removeproducts(Request $request)
    {
        $student = Product::find($request->input('id'));
        if ($student->delete()) {
            echo 'Data Deleted';
        }
    }

    public function removeteams(Request $request)
    {
        $student = Teams::find($request->input('id'));
        if ($student->delete()) {
            echo 'Data Deleted';
        }
    }

    public function removepositions(Request $request)
    {
        $student = Position::find($request->input('id'));
        if ($student->delete()) {
            echo 'Data Deleted';
        }
    }

}