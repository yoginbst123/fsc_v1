<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Subcategorys;
use App\Model\Categorys;
use DB;
class SubcategorysController extends Controller
{ 
    public function __construct()
    {
      $this->middleware('auth:admin');
    }
  
 
    public function index(Request $request)
    { 
        
        $categorys = DB::table('subcategorys as t1')->select('t1.*','t2.categoryname')
        ->leftJoin('categorys as t2', function($join){ $join->on('t2.subcategoryid', '=', 't1.id');})
        ->orderBy('t1.subcategoryname', 'asc')->get();  
      //$categorys = Subcategorys::paginate(10); 
      return view('fac-Bhavesh-0554.subcategorys.subcategorys',compact(['categorys']));
    }
    
    
    
    // public function create()
    // {
    //     return view('fac-Bhavesh-0554.subcategorys.create');
    // }
    
    
    // public function store(Request $request)
    // {
    //     $this->validate($request,[
    //         'categoryname'=>'required',
    //     ]);
    //     $business = new Categorys;
    //     $business->categoryname = $request->categoryname;
    //     $business->save();
    //     return redirect(route('subcategorys.index'));
    // }

    // public function edit($id)
    // {   
    //     $cat= Categorys::where('id',$id)->first();    
    //     return View('fac-Bhavesh-0554.subcategorys.edit',compact(['cat']));
    // }
    
    // public function update(Request $request, $id)
    // {
    //     $this->validate($request,[
    //         'categoryname'=>'required'
    //     ]);
    //     $business = Categorys::find($id);
    //     $business->categoryname = $request->categoryname;
    //     $business->update();
    //     return redirect(route('subcategorys.index'));
    // }
    
  
    // public function destroy($id)
    // {
    //     Categorys::where('id',$id)->delete();
    //     return redirect(route('subcategorys.index'));
    // }
   
}
