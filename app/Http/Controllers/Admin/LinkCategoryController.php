<?php
namespace App\Http\Controllers\Admin;
use App\Model\Link;
use App\Model\Linkcategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LinkCategoryController extends Controller
{
/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
         $this->middleware('auth:admin');
    } 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 

     $link = Linkcategory::All(); 
      return view('fac-Bhavesh-0554/linkcategory/linkcategory',compact('link'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    return view('fac-Bhavesh-0554/linkcategory/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'category'=>'required|unique:linkcategories,category',
            'type'=>'required',
            'linkimage'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',            
                       
        ]);
        if($request->hasFile('linkimage'))
        {
         $filname = $request->linkimage->getClientOriginalName();
         $request->linkimage->move('public/linkcategory', $filname);
        }
        
        $business = new Linkcategory;
        $business->category= $request->category;
        $business->type = $request->type;  
         $business->link = $request->link;  
        
        $business->linkimage = $filname;
        $business->save();
        return redirect(route('linkcategory.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Link  $link
     * @return \Illuminate\Http\Response
     */
    public function show(Link $link)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Link  $link
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {    $link= Linkcategory::where('id',$id)->first(); 
        return View('fac-Bhavesh-0554.linkcategory.edit',compact('link'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Link  $link
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'category'=>'required',           
             
        ]);
        if($request->hasFile('linkimage'))
        {
         $filname = $request->linkimage->getClientOriginalName();
         $request->linkimage->move('public/linkcategory', $filname);
        }
     else
         {
         $filname = $request->linkimage1;
            }        
        $business = Linkcategory::find($id);
        $business->type = $request->type;
        $business->link = $request->link;  
        $business->linkimage = $filname;
        $business->category = $request->category;
        $business->update();
        return redirect(route('linkcategory.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Link  $link
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         Linkcategory::where('id',$id)->delete();
        return redirect(route('linkcategory.index'));
    }
}
