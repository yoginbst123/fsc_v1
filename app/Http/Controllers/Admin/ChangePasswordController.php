<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Model\Admin;
use Illuminate\Support\Facades\Input;
use View;
use Validator;
use Redirect;
use Session;
use DB;
use Auth;
class ChangePasswordController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
         
        $start = Auth::user()->lock;
        $start1 = strtotime($start."+30 minutes");
        $end1 = strtotime($start);
        $ck =  $start1-$end1;
        $currentdate = strtotime(date('Y-m-d H:i:s'));
        if($currentdate >= $start1)
        {
        $returnValue = DB::table('admins')->where('flag', '=', '3')->update(['flag' => '0']);
        }
       return view('fac-Bhavesh-0554.changepassword');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $admin = Admin::where('id',$id)->first();    
        return View('fac-Bhavesh-0554.changepassword',compact('admin'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'oldpassword' => 'required|password',
            'password'=>'required|min:8|different:oldpassword', 
            'cpassword'=>'required|same:password',
          ];
          Admin::find(auth()->user()->password);                  
          // custom rule for 'password'
          Validator::extend('password', function( $attribute, $value, $parameters) {
            $user = Admin::find(auth()->user()->id);
            $pass = $user->password;
            // compare the entered password with what the database has, e.g. validates the current password
            return Hash::check($value,$pass);
          });
          
          // custom message if validation for password fails
          $messages = [ 'password' => 'Your current password does not match our records.'];
          
          // validate input with rules, adding in custom messages
          $validation = Validator::make(Input::all(), $rules, $messages);
          
          // if validation fails, redirect back to previous page
          if ( $validation->fails() ) {
             $business =Admin::find($id);
             $business->flag = $request->flag;
             $business->lock = date('Y-m-d H:i:s');
             $business->update();    
             return Redirect::back()->withInput()->withErrors( $validation->messages() );
          }
         else{       
            $resetdays = $request->resetdays;
            $startdate = date('Y-m-d');
            $enddate = date('Y-m-d', strtotime("+$resetdays days"));
            $business =Admin::find($id);
            $business->flag = '0';
            $business->password = bcrypt($request->password);
            $business->reset_day = $request->resetdays;
            $business->remaining_day = $request->resetdays;
            $business->reset_date = $request->reset_date;
            $business->start_date =  $startdate;
            $business->end_date =  $request->reset_date;   
            $business->update();
            $name = $request->password;
            $reset = $request->reset_date;
            $email = $request->email;
            $data = array('email' => $email, 'password' => $name, 'reset' =>  $reset, 'from' => 'vijay@businesssolutionteam.com');       
            return redirect(route('changepassword.index'))->with('success', 'Your Password Changed Successfully');
        }
}

public function getadminpassword(Request $request)
    {    
     
     $data = $request->all();        
     $user = Admin::find(auth()->user()->id);
     if(!Hash::check($data['oldpassword'], $user->password))
     {
         $isAvailable = false;
     }
     else
     {
         $isAvailable = true;   
     }
     echo json_encode(
                 array('valid' => $isAvailable));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
