<?php
namespace App\Http\Controllers\Admin;
use App\Model\Homecontent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
class HomecontentController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $homecontent = Homecontent::All();
        return view('fac-Bhavesh-0554/homecontent/homecontent',compact('homecontent'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('fac-Bhavesh-0554/homecontent/create',compact('homecontent'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title'=>'required|unique:homecontents,title',
            'content'=>'required',   
        ]);              
        $category = new Homecontent;
        $category->title = $request->title;      
        $category->content = $request->content;
        $category->save();
        return redirect(route('homecontent.index'))->with('success', 'Content Successfully Add');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Homecontent  $homecontent
     * @return \Illuminate\Http\Response
     */
    public function show(Homecontent $homecontent)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Homecontent  $homecontent
     * @return \Illuminate\Http\Response
     */
    public function edit(Homecontent $homecontent)
    {
        return View('fac-Bhavesh-0554.homecontent.edit',compact('homecontent'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Homecontent  $homecontent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Homecontent $homecontent)
    {
        $this->validate($request,[
            'title'=>'required',
            'content'=>'required',   
        ]);    
        $category = $homecontent;
        $category->title = $request->title;      
        $category->content = $request->content;
        $category->update();
        return redirect(route('homecontent.index'))->with('success', 'Content Successfully Update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Homecontent  $homecontent
     * @return \Illuminate\Http\Response
     */
    public function destroy(Homecontent $homecontent)
    {
        $homecontent->delete();
        return redirect(route('homecontent.index'));
    }
}
