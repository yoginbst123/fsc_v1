<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
class BankingsetupController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth:admin');
    }
    
    
    public function index()
    {  
        $bankdata = DB::table('bankmaster')->get();
        
       return view('fac-Bhavesh-0554.bankingsetup.bankingsetup',compact(['bankdata']));
    }

    
    public function create(Request $request)
    {
           $statedata = DB::table('state')->where('countrycode','=','USA')->get();
     
        return view('fac-Bhavesh-0554.bankingsetup.create',compact(['statedata']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //print_r($_REQUEST);die;    
        $this->validate($request,[
            'bankname'=>'required',
            'bankaddress'=>'required',            
            'routingnumber'=>'required',            
            'contactnumber'=>'required',            
            'bankwebsite'=>'required',            
            
        ]);
        
        $bankname = $request->bankname;
        $bankaddress = $request->bankaddress;
        $routingnumber = $request->routingnumber;
        $contactnumber = $request->contactnumber;
        $bankwebsite = $request->bankwebsite;
        $bankphonetype = $request->bankphonetype;
        
        
        $branchname=isset($request->branchname) ? $request->branchname:"";
        $branchaddress=isset($request->branchaddress) ? $request->branchaddress:"";
        $branchcity=isset($request->branchcity) ? $request->branchcity:"";
        $branchstate=isset($request->branchstate) ? $request->branchstate:"";
        $branchzip=isset($request->branchzip) ? $request->branchzip:"";
        
        $branchcontactname=isset($request->branchcontactname) ? $request->branchcontactname:"";
        $position=isset($request->position) ? $request->position:"";
        $telephone1=isset($request->telephone1) ? $request->telephone1:"";
        $telephone1type=isset($request->telephone1type) ? $request->telephone1type:"";
        $telephone2=isset($request->telephone2) ? $request->telephone2:"";
        $telephone2type=isset($request->telephone2type) ? $request->telephone2type:"";
        
        $email=isset($request->email) ? $request->email:"";
        $notes=isset($request->notes) ? $request->notes:"";
        
        
        
        
        
        
    
        $returnValue = DB::table('bankmaster')
            ->insert([ 
                   'bankname' =>$bankname,
                   'bankaddress' =>$bankaddress,
                   'routingnumber' =>$routingnumber,
                   'contactnumber' =>$contactnumber,
                   'bankwebsite' =>$bankwebsite,
                   'bankphonetype'=>$bankphonetype,
                   
                   'branchname' =>$branchname,
                   'branchaddress' =>$branchaddress,
                   'branchcity' =>$branchcity,
                   'branchstate' =>$branchstate,
                   'branchzip' =>$branchzip,
                   
                   'branchcontactname' =>$branchcontactname,
                   'position' =>$position,
                   'telephone1' =>$telephone1,
                   'telephone1type' =>$telephone1type,
                   'telephone2' =>$telephone2,
                   'telephone2type' =>$telephone2type,
                   'email' =>$email,
                   'notes' =>$notes
                   
                   
                ]);

        return redirect(route('bankingsetup.index'))->with('success','Record Added Seccessfully');
    }

    
    
    public function edit($id)
    {     
         $statedata = DB::table('state')->where('countrycode','=','USA')->get();
     
        $bankdata= DB::table('bankmaster')->where('id','=',$id)->first();
        return View('fac-Bhavesh-0554.bankingsetup.edit',compact(['bankdata','statedata']));
    }

    
    public function update(Request $request, $id)
    {
           $this->validate($request,[
            'bankname'=>'required',
            'bankaddress'=>'required',            
            'routingnumber'=>'required',            
            'contactnumber'=>'required', 
            'bankwebsite' =>'required',
        ]);
        
        $bankname =$request->bankname;
        $bankaddress = $request->bankaddress;
        $routingnumber = $request->routingnumber;
        $contactnumber = $request->contactnumber;
        $bankwebsite = $request->bankwebsite;
        
        $bankphonetype = $request->bankphonetype;
        
        
        $branchname=isset($request->branchname) ? $request->branchname:"";
        $branchaddress=isset($request->branchaddress) ? $request->branchaddress:"";
        $branchcity=isset($request->branchcity) ? $request->branchcity:"";
        $branchstate=isset($request->branchstate) ? $request->branchstate:"";
        $branchzip=isset($request->branchzip) ? $request->branchzip:"";
        
        $branchcontactname=isset($request->branchcontactname) ? $request->branchcontactname:"";
        $position=isset($request->position) ? $request->position:"";
        $telephone1=isset($request->telephone1) ? $request->telephone1:"";
        $telephone1type=isset($request->telephone1type) ? $request->telephone1type:"";
        $telephone2=isset($request->telephone2) ? $request->telephone2:"";
        $telephone2type=isset($request->telephone2type) ? $request->telephone2type:"";
        
        $email=isset($request->email) ? $request->email:"";
        $notes=isset($request->notes) ? $request->notes:"";
        
        
        
        $returnValue = DB::table('bankmaster')->where('id', '=', $id)
            ->update([ 
                   'bankname' =>$bankname,
                   'bankaddress' =>$bankaddress,
                   'routingnumber' =>$routingnumber,
                   'contactnumber' =>$contactnumber,
                   'bankwebsite' =>$bankwebsite,
                   
                   'bankphonetype'=>$bankphonetype,
                   
                   'branchname' =>$branchname,
                   'branchaddress' =>$branchaddress,
                   'branchcity' =>$branchcity,
                   'branchstate' =>$branchstate,
                   'branchzip' =>$branchzip,
                   
                   'branchcontactname' =>$branchcontactname,
                   'position' =>$position,
                   'telephone1' =>$telephone1,
                   'telephone1type' =>$telephone1type,
                   'telephone2' =>$telephone2,
                   'telephone2type' =>$telephone2type,
                   'email' =>$email,
                   'notes' =>$notes
                   
                ]);
        return redirect(route('bankingsetup.index'))->with('success','Record Updated Seccessfully ');
    }


    public function destroy($id)
    {
        DB::table('bankmaster')->where('id',$id)->delete();
        return redirect(route('bankingsetup.index'))->with('success','Record Deleted Seccessfully ');
    }
}
