<?php
namespace App\Http\Controllers\Admin;
use App\Model\Clientnupload;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\client_professional;
use App\Front\Commonregister;
use App\Model\BusinessBrand;
use App\Model\Category;
use App\Model\Business;
use App\User;
use DB;
use Hash;
use Carbon;
class ClientUploadController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth:admin');
    }
/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $upload = Clientnupload::All();
       return view('fac-Bhavesh-0554/clientupload/clientupload',compact('upload'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    $common = DB::table('commonregisters')->select('commonregisters.id as cid','commonregisters.created_at as created_at','commonregisters.filename as filename','commonregisters.mailing_address1 as mailing_address1','commonregisters.newclient as newclient','commonregisters.bussiness_zip as bussiness_zip','commonregisters.due_date as due_date','commonregisters.department as department','commonregisters.type_of_activity as type_of_activity','commonregisters.county_no as county_no','commonregisters.county_name as county_name','commonregisters.level as level','commonregisters.setup_state as setup_state','commonregisters.business_state as business_state','commonregisters.business_city as business_city','commonregisters.business_country as business_country','commonregisters.business_address as business_address','commonregisters.business_store_name as business_store_name','commonregisters.mailing_address as mailing_address','commonregisters.legalname as legalname','commonregisters.dbaname as dbaname','commonregisters.mailing_city as mailing_city','commonregisters.mailing_state as mailing_state','commonregisters.mailing_zip as mailing_zip','commonregisters.user_type as user_type','commonregisters.user_type as user_type','commonregisters.status as status','commonregisters.company_name as company_name','commonregisters.business_name as business_name','commonregisters.first_name as first_name','commonregisters.middle_name as middle_name','commonregisters.last_name as last_name','commonregisters.email as email','commonregisters.address as address','commonregisters.address1 as address1','commonregisters.city as city','commonregisters.stateId as stateId','commonregisters.zip as zip','commonregisters.countryId as countryId','commonregisters.mobile_no as mobile_no','commonregisters.business_no as business_no','commonregisters.business_fax as business_fax','commonregisters.website as website','commonregisters.user_type as user_type','commonregisters.business_id as business_id','commonregisters.business_cat_id as business_cat_id','commonregisters.business_brand_id as business_brand_id','commonregisters.business_brand_category_id as business_brand_category_id','businesses.bussiness_name as bussiness_name' ,'categories.business_cat_name as business_cat_name','business_brands.business_brand_name as business_brand_name','categorybusinesses.business_brand_category_name as business_brand_category_name')
->leftJoin('categories', function($join){ $join->on('commonregisters.business_cat_id', '=', 'categories.id');})
->leftJoin('businesses', function($join){ $join->on('commonregisters.business_id', '=', 'businesses.id');})
->leftJoin('business_brands', function($join){ $join->on('commonregisters.business_brand_id', '=', 'business_brands.id');})
->leftJoin('categorybusinesses', function($join){ $join->on('commonregisters.business_brand_category_id', '=', 'categorybusinesses.id');})->where('commonregisters.status', '=', "Approval")
->get();
    $professional = client_professional::All();
    return view('fac-Bhavesh-0554.clientupload.create',compact(['professional','common']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
        'upload_name'=>'required',
        'upload'=>'required',                        
        ]);
        if($request->hasFile('upload'))
        {
        $filname = $request->upload->getClientOriginalName();
        $request->upload->move('public/clientupload', $filname);
        }
        $business = new Clientnupload;
        $business->upload_name = $request->upload_name;
        $business->client_number= $request->client_number;
        $business->upload = $filname;
        $business->year = date('Y');
        $business->license_period = $request->license_period;
        $business->expired_date = $request->expired_date;
        $business->website_link = $request->website_link;
        $business->client_id = $request->client_number;
        $business->reminder = $request->reminder;
        $business->notification = $request->notification;
        $business->warning = $request->warning;
        $business->name = $request->name;
        $business->main_website = $request->main_website;
        $business->telephone = $request->telephone;
        $business->save();
        return redirect(route('clientupload.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Clientnupload  $clientnupload
     * @return \Illuminate\Http\Response
     */
    public function show(Clientnupload $clientnupload)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Clientnupload  $clientnupload
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
             $common = DB::table('commonregisters')->select('commonregisters.id as cid','commonregisters.created_at as created_at','commonregisters.filename as filename','commonregisters.mailing_address1 as mailing_address1','commonregisters.newclient as newclient','commonregisters.bussiness_zip as bussiness_zip','commonregisters.due_date as due_date','commonregisters.department as department','commonregisters.type_of_activity as type_of_activity','commonregisters.county_no as county_no','commonregisters.county_name as county_name','commonregisters.level as level','commonregisters.setup_state as setup_state','commonregisters.business_state as business_state','commonregisters.business_city as business_city','commonregisters.business_country as business_country','commonregisters.business_address as business_address','commonregisters.business_store_name as business_store_name','commonregisters.mailing_address as mailing_address','commonregisters.legalname as legalname','commonregisters.dbaname as dbaname','commonregisters.mailing_city as mailing_city','commonregisters.mailing_state as mailing_state','commonregisters.mailing_zip as mailing_zip','commonregisters.user_type as user_type','commonregisters.user_type as user_type','commonregisters.status as status','commonregisters.company_name as company_name','commonregisters.business_name as business_name','commonregisters.first_name as first_name','commonregisters.middle_name as middle_name','commonregisters.last_name as last_name','commonregisters.email as email','commonregisters.address as address','commonregisters.address1 as address1','commonregisters.city as city','commonregisters.stateId as stateId','commonregisters.zip as zip','commonregisters.countryId as countryId','commonregisters.mobile_no as mobile_no','commonregisters.business_no as business_no','commonregisters.business_fax as business_fax','commonregisters.website as website','commonregisters.user_type as user_type','commonregisters.business_id as business_id','commonregisters.business_cat_id as business_cat_id','commonregisters.business_brand_id as business_brand_id','commonregisters.business_brand_category_id as business_brand_category_id','businesses.bussiness_name as bussiness_name' ,'categories.business_cat_name as business_cat_name','business_brands.business_brand_name as business_brand_name','categorybusinesses.business_brand_category_name as business_brand_category_name')
->leftJoin('categories', function($join){ $join->on('commonregisters.business_cat_id', '=', 'categories.id');})
->leftJoin('businesses', function($join){ $join->on('commonregisters.business_id', '=', 'businesses.id');})
->leftJoin('business_brands', function($join){ $join->on('commonregisters.business_brand_id', '=', 'business_brands.id');})
->leftJoin('categorybusinesses', function($join){ $join->on('commonregisters.business_brand_category_id', '=', 'categorybusinesses.id');})->where('commonregisters.status', '=', "Approval")
->get();
             $professional = client_professional::All();
             $clientnupload = Clientnupload::where('id', $id)->first();
             return View('fac-Bhavesh-0554.clientupload.edit',compact(['clientnupload','professional','common']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Clientnupload  $clientnupload
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
        'upload_name'=>'required',
        ]);
        
        if($request->hasFile('upload'))
        {
        $filname = $request->upload->getClientOriginalName();
        $request->upload->move('public/clientupload', $filname);
        }
        else
        {
        $filname = $request->upload1;   
        }
        $business = Clientnupload::find($id);
        $business->upload_name = $request->upload_name;
        $business->client_number= $request->client_number;
        $business->upload = $filname;
        $business->year = date('Y');
        $business->license_period = $request->license_period;
        $business->expired_date = $request->expired_date;
        $business->website_link = $request->website_link;
        $business->client_id = $request->client_number;
        $business->reminder = $request->reminder;
        $business->notification = $request->notification;
        $business->warning = $request->warning;
        $business->name = $request->name;
        $business->main_website = $request->main_website;
        $business->telephone = $request->telephone;
        $business->update();
        return redirect(route('clientupload.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Clientnupload  $clientnupload
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Clientnupload::where('id',$id)->delete();
        return redirect(route('clientupload.index'));
    }
}
