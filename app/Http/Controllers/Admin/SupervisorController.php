<?php
namespace App\Http\Controllers\Admin;
use App\Model\Task;
use App\employees\Fscemployee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Employee;
use App\Model\Super;
class SupervisorController extends Controller
{
public function __construct()
    {
    $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $task = Super::All();
       $emp = Employee::where('super','=','1')->orderby('firstName','asc')->get();
       return view('fac-Bhavesh-0554/supervisor/supervisor', compact(['task','emp']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $emp = Employee::where('super','=','1')->orderby('firstName','asc')->get();
       return view('fac-Bhavesh-0554/supervisor/create',compact(['emp']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $this->validate($request,[
            'supervisorcode' =>'required',
            'supervisorname' =>'required',
        ]);
         $position = new Super;
         $position->username= $request->supervisorname;
         $position->code= $request->supervisorcode;
         $position->save();
         return redirect('fac-Bhavesh-0554/supervisor')->with('success','Add Supervisor Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\State  $state
     * @return \Illuminate\Http\Response
     */
    public function show(State $state)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\State  $state
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $task = Super::where('id',$id)->first(); 
        $emp = Employee::where('super','=','1')->orderby('firstName','asc')->get();
        return View('fac-Bhavesh-0554.supervisor.edit',compact(['task','emp']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\State  $state
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $position = Super::find($id);
        $position->username= $request->supervisorname;
        $position->code= $request->supervisorcode;
        $position->update();
        return redirect('fac-Bhavesh-0554/supervisor')->with('success','update Supervisor Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\State  $state
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        Super::where('id',$id)->delete();
        return redirect(route('supervisor.index'));
    }
}
