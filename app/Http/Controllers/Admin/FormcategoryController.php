<?php
namespace App\Http\Controllers\Admin;
use App\Model\Link;
use App\Model\Formcategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FormCategoryController extends Controller
{
/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
         $this->middleware('auth:admin');
    } 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 

     $link = Formcategory::All(); 
      return view('fac-Bhavesh-0554/formcategory/formcategory',compact('link'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    return view('fac-Bhavesh-0554/formcategory/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'category'=>'required',
            'type'=>'required',
                    
                       
        ]);
        
        
        $business = new Formcategory;
        $business->category= $request->category;
        $business->type = $request->type;  
        $business->save();
        return redirect(route('formcategory.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Link  $link
     * @return \Illuminate\Http\Response
     */
    public function show(Link $link)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Link  $link
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {    $link= Formcategory::where('id',$id)->first(); 
        return View('fac-Bhavesh-0554.formcategory.edit',compact('link'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Link  $link
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'category'=>'required',           
             
        ]);
               
        $business = Formcategory::find($id);

        $business->type = $request->type;
        $business->category = $request->category;
        $business->update();
        return redirect(route('formcategory.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Link  $link
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         Formcategory::where('id',$id)->delete();
        return redirect(route('formcategory.index'));
    }
}
