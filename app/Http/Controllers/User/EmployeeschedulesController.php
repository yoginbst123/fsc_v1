<?php
namespace App\Http\Controllers\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\employee\Empschedule;
use App\Model\Employee;
use App\Model\Schedule;
use App\employees\Fscemployee;
use App\Model\Position;
use App\Model\Branch;
use DB;
use Auth;
use App\User;
use Illuminate\Support\Facades\Input;
use Response;
class EmployeeschedulesController extends Controller
{
 public function __construct()
    {
      $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $branch = Branch::All(); 
        $schedule = Schedule::All();
        $employee = Fscemployee::All();
        $start =strtotime($request->startdate); 
        $end = strtotime($request->enddate);
        $start1 =date('Y-m-d',strtotime($request->startdate)); 
        $end1 = date('Y-m-d',strtotime($request->enddate));
        if(empty($request->emp_name))
        {
            $emp1 = Employee::where('check','=',1)->where('type','=','clientemployee')->where('pay_frequency','=',$request->duration)->get();
            $ss = Schedule::get();
            $employee1 = DB::Table('schedule_emp_dates')->selectRaw('emp_sch_id,date_1,clockin,clockout,id')->whereBetween('date_1',array($start,$end))->get();
        }
        else
        {
            $emp1 = Employee::where('check','=',1)->where('type','=','clientemployee')->where('pay_frequency','=',$request->duration)->where('id','=',$request->emp_name)->get();
            $ss1 = Schedule::where('emp_name','=',$request->emp_name)->first();
            $ss = Schedule::get();
           $employee1 = DB::Table('schedule_emp_dates')->selectRaw('emp_sch_id,date_1,clockin,clockout,id')->whereBetween('date_1',array($start,$end))->where('emp_sch_id','=',$ss1->id)->get();
        }
        
        $employee2 = DB::Table('empschedules')->selectRaw('TIMEDIFF(emp_out,emp_in) as total,TIMEDIFF(launch_out,launch_in) as breaktime,TIMEDIFF(launch_out_second,launch_in_second) as breaktime1,emp_out,launch_in,launch_out,emp_in,ip_address,note,emp_in_date,id,employee_id,launch_out_second,launch_in_second')->whereBetween('emp_in_date',array($start1,$end1))->get();
        $sum = 0; 
        foreach($employee2 as $emp)
        {
            $in = date('H:i',strtotime($emp->emp_in));
            $out = date('H:i',strtotime($emp->emp_out));
            $lunch_in = date('H:i',strtotime($emp->launch_in));
            $lunch_out = date('H:i',strtotime($emp->launch_out));
            $total = date("H:i:s",strtotime($emp->total));
        
            if($emp->breaktime==NULL)
            {
            $breaktime ="00:00:00";
            }
            else
            {
            $breaktime = date("H:i:s",strtotime($emp->breaktime));
            }
            
            if($emp->breaktime==NULL)
            {
            $breaktime ="00:00:00";
            }
            else
            {
            $breaktime = date("H:i:s",strtotime($emp->breaktime));
            }
            
            if($emp->breaktime1==NULL)
            {
                $breaktime1 ="00:00:00";
            }
            else
            {
                $breaktime1 = date("H:i:s",strtotime($emp->breaktime1));
            }
                $total1 = floor($total)-(floor($breaktime) + floor ($breaktime1));
                $sum +=$total1;
            }
            
        return view('client/employeeschedule/employeeschedule',compact(['employee','branch','employee2','employee1','emp1','ss','sum']));
    }
    
    
    public function getdurationemp(Request $request)
    {
        $data = Employee::select('pay_frequency','id','firstName','middleName','lastName')->where('pay_frequency',$request->id)->where('check','1')->where('type',$request->type1)->take(100)->get();
        return response()->json($data);  
    }   
    public function report()
    {
        return Response::json($employee);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        
      
    }
    
 
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
     
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
