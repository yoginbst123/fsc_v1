<?php
namespace App\Http\Controllers\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Model\Schedule;
use App\Model\Branch;
use App\Model\Employee;
use App\Model\Schedulesetup;
use DB;
use Hash;
use Auth;
use App\Http\Controllers\Controller;
use App\Model\empschedule;

class ClientschedulesetupController extends Controller
{
public function __construct()
    {
    $this->middleware('auth');
    } 
    
    public function index()
    {   
        
       $branch = Branch::All(); 
       $schedule = Schedulesetup::All(); 
       
        $sch = DB::table('schedules')->select('schedules.id as cid','schedules.entity_id','schedules.type','schedules.emp_city','schedules.emp_name','schedules.duration','schedules.sch_start_date',
        'schedules.sch_end_date','schedules.schedule_in_time','schedules.schedule_out_time',
        'employees.id as eid','employees.firstName','employees.lastName','employees.middleName')
            ->leftJoin('employees', function($join){ $join->on('employees.id', '=', 'schedules.emp_name');})
            ->where('employees.type', '=', 'clientemployee')->orderBy('employees.firstName', 'asc')->get();

         //echo "<pre>"; print_r($sch);die;
        $emp = Employee::where('status','=','1')->orderBy('firstName', 'asc')->get(); 
        $empschedule = empschedule::All();
        $schedulesetup = Schedulesetup::All();
       
        //return view('client.clientschedulesetup.clientschedulesetup',compact(['schedule']));
        return view('client.clientschedulesetup.clientschedulesetup',compact(['sch','branch','schedule','emp','empschedule','schedulesetup']));
    }
    
    public function create()
    {
        $user_id = Auth::user()->user_id;
        $branch = DB::table('employees')->select('branch_city','type','clienttype')->where('clienttype','=',$user_id)->where('branch_city','!=', '')->where('type','=','clientemployee')->orderBy('branch_city', 'asc')->groupBy('branch_city')->get(); 
         // echo "<pre>"; print_r($branch);die;
        $schedule = Schedulesetup::All(); 
       
        $sch = DB::table('schedules')->select('schedules.id as cid','schedules.entity_id','schedules.type','schedules.emp_city','schedules.emp_name','schedules.duration','schedules.sch_start_date',
        'schedules.sch_end_date','schedules.schedule_in_time','schedules.schedule_out_time',
        'employees.id as eid','employees.firstName','employees.lastName','employees.middleName')
            ->leftJoin('employees', function($join){ $join->on('employees.id', '=', 'schedules.emp_name');})
            ->where('employees.type', '!=', 'clientemployee')->where('employees.status','=','1')->orderBy('employees.firstName', 'asc')->get();

     
        $emp = Employee::where('status','=','1')->orderBy('firstName', 'asc')->get(); 
        $empschedule = empschedule::All();
        $schedulesetup = Schedulesetup::All();
       
       return view('client/clientschedulesetup/create',compact('sch','branch','schedule','emp','empschedule','schedulesetup','user_id'));
    }


    public function store(Request $request)
    {
       $this->validate($request,[
            'emp_city' =>'required',
            'emp_name' =>'required',
            'duration' =>'required',
            'sch_start_date' =>'required',                     
        ]);
        $position = new Schedule;
        $position->emp_city= $request->emp_city;
        $position->type= 'clientemployee';
        $position->emp_name= $request->emp_name;
        $position->duration= $request->duration;
        $position->sch_start_date= $request->sch_start_date;
        $position->sch_end_date= $request->sch_end_date;
        $position->schedule_in_time= $request->schedule_in_time;
        $position->schedule_out_time= $request->schedule_out_time;
        $position->sch_start_day= $request->sch_start_day;
        $position->sch_end_day= $request->sch_end_day;
        $position->save();
        $lastId = $position->id;
        $date_from = $request->sch_start_date;  
        $date_from1 = $request->sch_start_date;  
        $date_from2 = $request->schedule_in_time;
        $date_from3 = $request->schedule_out_time;
        $date_from = strtotime($date_from);
        $date_to = $request->sch_end_date; 
        $date_to1 = $request->sch_end_date; 
        $date_to = strtotime($date_to);
       
        for($i=$date_from; $i<=$date_to; $i+=86400) 
        {
            $day = date("D", $i);
            if($day=='Sun')
            {
                $insert2 = DB::insert("insert into schedule_emp_dates(`emp_sch_id`,`date_1`,`clockin`,`clockout`,`status`) values('".$lastId."','".$i."','','','1')");  
            }
            else if($day=='Sat')
            {
                $insert2 = DB::insert("insert into schedule_emp_dates(`emp_sch_id`,`date_1`,`clockin`,`clockout`,`status`) values('".$lastId."','".$i."','','','1')");     
            }
            else
            {
                $insert2 = DB::insert("insert into schedule_emp_dates(`emp_sch_id`,`date_1`,`clockin`,`clockout`) values('".$lastId."','".$i."','".$date_from2."','".$date_from3."')");  
            }
        }
        return redirect('client/clientschedulesetup')->with('success','Successfully added Schedule');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function show(Schedule $schedule)
    {
        //
    }


    public function edit($cid)
    { 

        $user_id = Auth::user()->user_id;
      
      $branch = Employee::select('branch_city','type')->where('branch_city','!=', '')->where('type','=','clientemployee')->orderBy('branch_city', 'asc')->groupBy('branch_city')->get(); 
      $schedulesetup = DB::table('schedules')->select('schedules.id as cid','schedules.entity_id','schedules.type','schedules.emp_city','schedules.emp_name','schedules.duration','schedules.sch_start_date',
        'schedules.sch_start_day','schedules.sch_end_day','schedules.sch_end_date','schedules.schedule_in_time','schedules.schedule_out_time','employees.id','employees.firstName','employees.lastName','employees.middleName','employees.type')
            ->leftJoin('employees', function($join){ $join->on('employees.id', '=', 'schedules.emp_name');})
            ->where('employees.type', '=', 'clientemployee')->where('schedules.id', '=', $cid)->orderBy('employees.firstName', 'asc')->first();

      $emp = Employee::select('firstName','lastName','clienttype')->where('type','=','clientemployee')->where('clienttype','=',$user_id)->orderBy('firstName', 'asc')->get(); 
      $empschedule = DB::table('schedule_emp_dates')->where('emp_sch_id',$cid)->orderBy('id', 'asc')->get();
      return View('client.clientschedulesetup.edit',compact(['schedulesetup','branch','emp','empschedule','user_id']));
    }

    public function update(Request $request,Schedule $schedule)
    {
        //echo "<pre>";print_r($_REQUEST);EXIT;
       
     // ECHO "<PRE>"; print_r($_POST);EXIT;
       $this->validate($request,[
            'emp_city' =>'required',
           // 'emp_name' =>'required',
            'duration' =>'required',
            'sch_start_date' =>'required',                     
        ]);   
        $position = $schedule;                  
        $position->emp_city= $request->emp_city;
        $position->emp_name= $request->emp_name;
        $position->duration= $request->duration;
        $position->sch_start_date= $request->sch_start_date;
        $position->sch_end_date= $request->sch_end_date;
        $position->schedule_in_time= $request->schedule_in_time;
        $position->schedule_out_time= $request->schedule_out_time;
        $position->sch_start_day= $request->sch_start_day;
        $position->sch_end_day= $request->sch_end_day;
        $position->type= 'clientemployee';
        $position->update();
        $lastId = $position->id;
        
        $date_from = $request->sch_start_date;
        $date_from1 = $request->sch_start_date;
        $date_from2 = $request->schedule_in_time;
        $date_from3 = $request->schedule_out_time;
        $date_from = strtotime($date_from);
        $date_to = $request->sch_end_date;  
        $date_to1 = $request->sch_end_date;  
        $date_to = strtotime($date_to);
        //$date_to4 = $request->schedule_date; 
        //$date_to4 = strtotime($date_to4);
        $na1 = $schedule->id;
        //$empschedule = empschedule::where('emp_sch_id',$na1)->where('date_1',$date_to4)->get();
        for($i=$date_from; $i<=$date_to; $i+=86400) 
        {
        //$insert2 = DB::insert("insert into schedule_emp_dates(`emp_sch_id`,`date_1`,`clockin`,`clockout`) values('".$lastId."','".$i."','".$date_from2."','".$date_from3."')");  
        //DB::table('schedule_emp_dates')->where('emp_sch_id', $lastId)->where('date_1','!=', $i)->delete();
        }
        $schedule_date = $request->schedule_date;  
        $schedule_clockin = $request->schedule_in_time;  
        $schedule_id = $request->schedule_id;  
        $schedule_clockout = $request->schedule_out_time;  
        $j= 0;
        foreach($schedule_date as $post)
        {
        $schedule_date1 =$schedule_date[$j]; 
        $schedule_date1 = strtotime($schedule_date1);
        $day = date("D", $schedule_date1);
        $schedule_clockin1 = $schedule_clockin;  
        $schedule_id1 = $schedule_id[$j];  
        $schedule_clockout1 = $schedule_clockout; 
        
        if($day=='Sun'){
         $returnValue = DB::table('schedule_emp_dates')->where('id', '=', $schedule_id1)
        ->update([ 'date_1' => $schedule_date1,
                   'clockin' =>'',
                   'clockout' =>'',
            ]);   
        }
        else if($day=='Sat')
        {
           $returnValue = DB::table('schedule_emp_dates')->where('id', '=', $schedule_id1)
        ->update([ 'date_1' => $schedule_date1,
                   'clockin' =>'',
                   'clockout' =>'',
            ]);  
        }
        
        else{
        $returnValue = DB::table('schedule_emp_dates')->where('id', '=', $schedule_id1)
        ->update([ 'date_1' => $schedule_date1,
                   'clockin' => $request->schedule_clockin[$j],
                   'clockout' => $request->schedule_clockout[$j],
            ]);
        }
            //return $schedule_date1;
        $j++;
        }
        return redirect('client/clientschedulesetup')->with('success','Successfully Updated Schedule');
    }

    
    public function destroy($id)
    {
       Schedulesetup::where('id',$id)->delete();
    return redirect(route('clientschedulesetup.index'))->with('success','Success Fully Delete Record');
    }
    
}
