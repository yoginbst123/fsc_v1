<?php
namespace App\Http\Controllers\User;
use Illuminate\Http\Request;
use App\Model\Rules;
use App\Model\Employee;
use App\employees\Fscemployee;
use DB;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
class WrkresponsibiltyController extends Controller
{
    public function __construct()
    {
    $this->middleware('auth');
    }  
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $employee = Employee::where('type','=','employee')->get(); 
        $homecontent = Rules::All();
        return view('client/workresponsibilty/workresponsibilty', compact(['homecontent','employee']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {  
      $employee = Employee::where('type','=','employee')->get(); 
       return view('client/workresponsibilty/create', compact(['employee']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $this->validate($request,[
            'rules' =>'required','type' =>'required','title' =>'required',
                     
        ]);
         
        
        $position = new Rules;
        $position->rules= $request->rules;
        $position->type= $request->type;
        $position->title= $request->title;
        $position->employee_id= $request->employee_id;
        
        $position->save();
       if($request->type=='Rules')
        {
$v = "1";
$v2 = "";
$v1 = DB::update('update fscemployees set `rule_status` = ?,`rulesdate` = ?',[$v,$v2]);
        }
        return redirect('client/workresponsibilty')->with('success','Success fully add Rules');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
     $homecontent = Rules::where('id',$id)->first(); 
      $employee = Employee::where('type','=','employee')->get(); 
     return view('client.workresponsibilty.edit',compact(['homecontent','employee']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
       $this->validate($request,[
            'rules' =>'required',
             'type' =>'required',
                     
        ]);
        $position = Rules::find($id);
        $position->rules= $request->rules;
        $position->type= $request->type;
        $position->title= $request->title;
        $position->employee_id= $request->employee_id;
        $position->update();
        return redirect('client/workresponsibilty')->with('success','Success fully update Rues'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Rules::where('id',$id)->delete();
        return redirect(route('workresponsibilty.index'));
    }
}
