<?php
namespace App\Http\Controllers\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User\UserProfile;
use App\Front\Commonregister;
use App\Model\BusinessBrand;
use App\Model\Category;
use App\Model\Business;
use App\Model\Question;
use App\Model\Answer;
use App\Model\taxstate;
use App\User;
use App\Model\Categorybusiness;
use DB;
use Hash;
use Auth;
use App\Model\Contact_userinfo;
use App\Model\client_professional;
use App\Model\Clientnupload;
use App\Model\client_shareholder;
use App\Model\taxfederal;

use App\Model\Currency;
use App\Model\Period;
use App\Model\Typeofser;
use App\Model\Employee;
class UserProfileController extends Controller
{
   
/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(isset($request->filename)!='')
        {
            //    echo $request->filename;
             $newclientdata = DB::table('commonregisters')->select('id','filename')->where('filename',$request->filename)->first();
             $user_id = $newclientdata->id;
             //print_r($newclientdata);
            //die;
        }
        else
        {
            //echo "bbbb";die;
            $user_id = Auth::user()->user_id;
        }
            
        
        $category = Category::All();   
        $business = Business::all();
        $businessbrand = BusinessBrand::All(); 
        $cd = Categorybusiness::All();
        $cb= Categorybusiness::All();
        $user = User::All();
        $answer= Answer::All();
        $question= Question::All();
        $taxstate = taxstate::All();
        $upload = Clientnupload::All();
        $common1 = Commonregister::where('id', '=', $user_id)->first();
        $client = DB::table('notes')->where('admin_id','=', $user_id)->where('type','=','client')->get();
        $fsc = DB::table('notes')->where('admin_id','=','')->where('type','=','fsc')->get();
        $every = DB::table('notes')->where('type','=','Everybody')->get(); 
        
        $admin_notes = DB::table('notes')->where('type','=','admin')->get();
        $note = DB::table('notes')->where('admin_id', '=', $user_id)->get();
        $info = Contact_userinfo::where('user_id', '=', $user_id)->get();
        $info1 = Contact_userinfo::where('user_id', '=', $user_id)->first();
        $shareholder=client_shareholder::where('client_id', '=', $user_id)->get();
        $professional=client_professional::where('client_id', '=', $user_id)->get();
        $typeofentity = DB::table('typeofentity')->orderBy('typeentity','=','asc')->get(); 
         
         $common = DB::table('commonregisters')->select('commonregisters.active_tabs','commonregisters.code','commonregisters.ext2_2','commonregisters.mobiletype_2','commonregisters.ext2_1','commonregisters.ext2_1','commonregisters.mobiletype_1','commonregisters.mobile_1','commonregisters.formation_start_date','commonregisters.formation_type_of_entity','commonregisters.adminonly','commonregisters.supervisor',
         'commonregisters.fscee','commonregisters.fscuser','commonregisters.client','commonregisters.alluser','commonregisters.work_paidby','commonregisters.type_of_entity_answer',
         'commonregisters.work_status','commonregisters.billingtoo','commonregisters.id','commonregisters.soscertificate','commonregisters.sosaoi','commonregisters.address_type',
         'commonregisters.formation_city','commonregisters.formation_state','commonregisters.formation_zip','commonregisters.formation_address','commonregisters.email_1 as email_1',
         'commonregisters.businessext as businessext','commonregisters.creationdate as creationdate','commonregisters.user_answer3 as user_answer3',
         'commonregisters.user_answer2 as user_answer2','commonregisters.user_answer1 as user_answer1','commonregisters.user_cell as user_cell',
         'commonregisters.user_resetdays as user_resetdays','commonregisters.user_question3 as user_question3','commonregisters.user_question2 as user_question2',
         'commonregisters.user_question1 as user_question1','commonregisters.user_email as user_email','commonregisters.user_name as user_name','commonregisters.user_active as user_active',
         'commonregisters.limited_answer3 as limited_answer3','commonregisters.limited_answer2 as limited_answer2','commonregisters.limited_answer1 as limited_answer1',
         'commonregisters.limited_resetdate as limited_resetdate','commonregisters.limited_resetdays as limited_resetdays','commonregisters.limited_question3 as limited_question3',
         'commonregisters.limited_question2 as limited_question2','commonregisters.limited_question1 as limited_question1','commonregisters.limited_user as limited_user',
         'commonregisters.limited_active as limited_active','commonregisters.subscription_answer3 as subscription_answer3','commonregisters.subscription_answer2 as subscription_answer2',
         'commonregisters.subscription_answer1 as subscription_answer1','commonregisters.subscription_resetdate as subscription_resetdate',
         'commonregisters.subscription_resetdays as subscription_resetdays','commonregisters.subscription_user as subscription_user','commonregisters.subscription_lock as subscription_lock',
         'commonregisters.subscription_active as subscription_active','commonregisters.locations as locations','commonregisters.multilocation as multilocation',
         'commonregisters.businesstype as businesstype','commonregisters.emailbli1 as emailbli1','commonregisters.faxbli1 as faxbli1','commonregisters.billingtoo11 as billingtoo11',
         'commonregisters.business_address_2 as business_address_2','commonregisters.business_address_3 as business_address_3','commonregisters.business_city_1 as business_city_1',
         'commonregisters.business_state_1 as business_state_1','commonregisters.bussiness_zip_1 as bussiness_zip_1','commonregisters.business_country_1 as business_country_1',
         'commonregisters.location_county as location_county','commonregisters.physical_county_no as physical_county_no','commonregisters.purchagename as purchagename',
         'commonregisters.sallername as sallername','commonregisters.businessloan as businessloan','commonregisters.loannote as loannote',
         'commonregisters.business_credit_card as business_credit_card','commonregisters.bank_accounts as bank_accounts','commonregisters.banktype1 as banktype1',
         'commonregisters.banktype as banktype','commonregisters.due_date07 as due_date07','commonregisters.due_date06 as due_date06','commonregisters.buyer_note as buyer_note',
         'commonregisters.personname_buyer as personname_buyer','commonregisters.buyername as buyername','commonregisters.due_date04 as due_date04',
         'commonregisters.purchagenote as purchagenote','commonregisters.personname_saller as personname_saller','commonregisters.sellername as sellername',
         'commonregisters.due_date03 as due_date03','commonregisters.closedate as closedate','commonregisters.saleofbusiness as saleofbusiness','commonregisters.newbusiness as newbusiness',
         'commonregisters.due_date02 as due_date02','commonregisters.contact_fax_1 as contact_fax_1','commonregisters.eext2 as eext2','commonregisters.eteletype2 as eteletype2',
         'commonregisters.etelephone2 as etelephone2','commonregisters.eext1 as eext1','commonregisters.eteletype1 as eteletype1','commonregisters.etelephone1 as etelephone1',
         'commonregisters.zip_1 as zip_1','commonregisters.state_1 as state_1','commonregisters.city_1 as city_1','commonregisters.contact_address2 as contact_address2',
         'commonregisters.contact_address1 as contact_address1','commonregisters.lastname as lastname','commonregisters.middlename as middlename','commonregisters.firstname as firstname',
         'commonregisters.state_id as state_id','commonregisters.nametype as nametype','commonregisters.federal_payment_frequency_date as federal_payment_frequency_date','commonregisters.number_1 as number_1','commonregisters.notes as notes','commonregisters.number_4 as number_4','commonregisters.payment_frequency_date as payment_frequency_date','commonregisters.fedral_state_2 as fedral_state_2','commonregisters.payment_frequency_1 as payment_frequency_1','commonregisters.number_3 as number_3','commonregisters.fedral_state_1 as fedral_state_1','commonregisters.number_2 as number_2','commonregisters.payment_frequency as payment_frequency','commonregisters.federal_payment_frequency as federal_payment_frequency','commonregisters.federal_frequency as federal_frequency','commonregisters.payment_quaterly_monthly as payment_quaterly_monthly','commonregisters.payment_quaterly_quaterly as payment_quaterly_quaterly','commonregisters.quaterly_quaterly as quaterly_quaterly','commonregisters.quaterly_monthly as quaterly_monthly','commonregisters.frequency_due_date_monthly_2 as frequency_due_date_monthly_2','commonregisters.frequency_due_date_quaterly_2 as frequency_due_date_quaterly_2','commonregisters.frequency_due_date_quaterly_1 as frequency_due_date_quaterly_1','commonregisters.frequency_due_date_monthly_1 as frequency_due_date_monthly_1','commonregisters.frequency_due_date_year_1 as frequency_due_date_year_1','commonregisters.frequency_due_date_quaterly as frequency_due_date_quaterly','commonregisters.frequency_due_date_monthly as frequency_due_date_monthly','commonregisters.frequency_due_date_year as frequency_due_date_year','commonregisters.payment_frequency_quaterly as payment_frequency_quaterly','commonregisters.payment_frequency_monthly as payment_frequency_monthly','commonregisters.payment_frequency_year as payment_frequency_year','commonregisters.federal_payment_frequency_quaterly as federal_payment_frequency_quaterly','commonregisters.federal_payment_frequency_month as federal_payment_frequency_month','commonregisters.federal_payment_frequency_year as federal_payment_frequency_year','commonregisters.federal_frequency_due_quaterly as federal_frequency_due_quaterly','commonregisters.federal_frequency_due_monthly as federal_frequency_due_monthly','commonregisters.federal_frequency_due_year as federal_frequency_due_year','commonregisters.state_of_formation as state_of_formation','commonregisters.typeofcorp_effect_2 as typeofcorp_effect_2','commonregisters.type_of_entity as type_of_entity','commonregisters.typeofcorp_effect as typeofcorp_effect','commonregisters.type_of_entity_answer as type_of_entity_answer','commonregisters.type_of_entity_answer1 as type_of_entity_answer1','commonregisters.type_of_entity_answer3 as type_of_entity_answer3','commonregisters.type_of_entity_answer2 as type_of_entity_answer2','commonregisters.form_authority as form_authority','commonregisters.formauthority  as formauthority','commonregisters.unemployent as unemployent','commonregisters.form_authority_1 as form_authority_1','commonregisters.withholding_authority  as withholding_authority','commonregisters.profession_not as profession_not','commonregisters.business_license2 as business_license2','commonregisters.business_license3 as business_license3','commonregisters.business_license1 as business_license1','commonregisters.business_license as business_license','commonregisters.business_license_jurisdiction as business_license_jurisdiction','commonregisters.due_date_1 as due_date_1','commonregisters.due_date2 as due_date2','commonregisters.due_date_2 as due_date_2','commonregisters.type_form1 as type_form1','commonregisters.authority as authority','commonregisters.fedral_state as fedral_state','commonregisters.type_form as type_form','commonregisters.federal_id as federal_id','commonregisters.typeofcorp1 as typeofcorp1','commonregisters.agent_mname as agent_mname','commonregisters.agent_lname as agent_lname','commonregisters.agent_fname as agent_fname','commonregisters.contact_number as contact_number','commonregisters.legal_name1 as legal_name1','commonregisters.typeofservice as typeofservice','commonregisters.id as cid','commonregisters.filename as filename','commonregisters.mailing_address1 as mailing_address1','commonregisters.bussiness_zip as bussiness_zip','commonregisters.due_date as due_date','commonregisters.department as department','commonregisters.type_of_activity as type_of_activity','commonregisters.county_no as county_no','commonregisters.county_name as county_name','commonregisters.level as level','commonregisters.setup_state as setup_state','commonregisters.business_state as business_state','commonregisters.business_city as business_city','commonregisters.business_country as business_country','commonregisters.business_address as business_address','commonregisters.business_store_name as business_store_name','commonregisters.mailing_address as mailing_address','commonregisters.legalname as legalname','commonregisters.dbaname as dbaname','commonregisters.mailing_city as mailing_city','commonregisters.mailing_state as mailing_state','commonregisters.mailing_zip as mailing_zip','commonregisters.user_type as user_type','commonregisters.user_type as user_type','commonregisters.status as status','commonregisters.company_name as company_name','commonregisters.business_name as business_name','commonregisters.first_name as first_name','commonregisters.middle_name as middle_name','commonregisters.last_name as last_name','commonregisters.email as email','commonregisters.address as address','commonregisters.address1 as address1','commonregisters.city as city','commonregisters.stateId as stateId','commonregisters.zip as zip','commonregisters.countryId as countryId','commonregisters.mobile_no as mobile_no','commonregisters.business_no as business_no','commonregisters.business_fax as business_fax','commonregisters.website as website','commonregisters.user_type as user_type','commonregisters.business_id as business_id','commonregisters.business_cat_id as business_cat_id','commonregisters.business_brand_id as business_brand_id','commonregisters.business_brand_category_id as business_brand_category_id','businesses.bussiness_name as bussiness_name','categories.business_cat_name as business_cat_name','business_brands.business_brand_name as business_brand_name','categorybusinesses.business_brand_category_name as business_brand_category_name','commonregisters.extension_due_date','commonregisters.type_form_file2','commonregisters.extension_due_date2','commonregisters.quarter_type','commonregisters.quarter_date','commonregisters.eptpspin','commonregisters.federal_frequency_due_date','commonregisters.typeofcorp','commonregisters.typeofservices','commonregisters.client_payroll','commonregisters.client_intangible','commonregisters.client_tobacco','commonregisters.client_pay','commonregisters.client_coam','commonregisters.quarter_type_holding1','commonregisters.quarter_date_holding','commonregisters.frequency_type_holding1','commonregisters.federal_frequency_due_date_holding','commonregisters.number_3','commonregisters.pin','commonregisters.payment_frequency_type_holding2','commonregisters.payment_frequency_due_date_holding','commonregisters.ga_dept_labour','commonregisters.form_number_4','commonregisters.quarter_type_unemploy1','commonregisters.quarter_date_unemploy','commonregisters.frequency_type_unemploy1','commonregisters.federal_frequency_due_date_unemploy','commonregisters.payment_frequency_type_unemploy2','commonregisters.payment_frequency_due_date_unemploy','commonregisters.form_number_1','commonregisters.pay_pw','commonregisters.federal_note','commonregisters.state_holding_no','commonregisters.holding_uname','commonregisters.holding_password','commonregisters.state_unemploy_no','commonregisters.unemploy_uname','commonregisters.unemploy_password','commonregisters.saltax_state_name','commonregisters.saltax_ga_dept','commonregisters.saltax_county','commonregisters.saltax_county_no','commonregisters.saltax_period','commonregisters.saltax_due_date','commonregisters.saltax_tax_no','commonregisters.saltax_uname','commonregisters.saltax_password','commonregisters.saltax_state_rt','commonregisters.saltax_county_rt','commonregisters.saltax_rt','commonregisters.excise_state_name','commonregisters.excise_ga_dept','commonregisters.excise_period','commonregisters.excise_due_date','commonregisters.excise_tax_no','commonregisters.excise_uname','commonregisters.excise_password','commonregisters.personal_state_name','commonregisters.personal_county','commonregisters.personal_county_no','commonregisters.personal_ga_dep','commonregisters.personal_period','commonregisters.personal_due_date','commonregisters.personal_ac_no','commonregisters.personal_filing','commonregisters.personal_uname','commonregisters.personal_password','commonregisters.coam_state_name','commonregisters.coam_ga_dept','commonregisters.coam_period','commonregisters.coam_due_date','commonregisters.coam_no','commonregisters.coam_unmae','commonregisters.coam_password','commonregisters.federal_note_2','commonregisters.online_payment','commonregisters.interview_type_of_entity','commonregisters.interview_business_sdate','commonregisters.interview_business_detail','commonregisters.formation_register','commonregisters.formation_register_entity','commonregisters.formation_date','commonregisters.license_yesno','commonregisters.license_entity','commonregisters.license_corporation','commonregisters.license_date','commonregisters.formation_yearbox','commonregisters.formation_yearvalue','commonregisters.formation_amount','commonregisters.formation_payment','commonregisters.record_status','commonregisters.annualreceipt','commonregisters.formation_work_officer','commonregisters.telephoneNo1Type','commonregisters.motelephoneile1','commonregisters.telephoneNo2Type','commonregisters.mobile_2','commonregisters.contact_title')
                    ->leftJoin('categories', function($join){ $join->on('commonregisters.business_cat_id', '=', 'categories.id');})         
                    ->leftJoin('businesses', function($join){ $join->on('commonregisters.business_id', '=', 'businesses.id');})
                    ->leftJoin('business_brands', function($join){ $join->on('commonregisters.business_brand_id', '=', 'business_brands.id');})
                    ->leftJoin('categorybusinesses', function($join){ $join->on('commonregisters.business_brand_category_id', '=', 'categorybusinesses.id');})
                    ->where('commonregisters.id', '=', $user_id)->get()->first();
         
         $admin_professional =client_professional::where('client_id', '=', $user_id)->get();
         $bankdata = DB::table('client_bank')->select('*')->where('client_id', '=',$user_id)->get();     
         $carddata = DB::table('client_credit_information')->select('*')->where('client_id', '=',$user_id)->get();     
         $bankmaster = DB::table('bankmaster')->get();
         $carddatalist = DB::table('cardmaster')->select('*')->orderBy('cardname', 'ASC')->get(); 
         $bankdatalist = DB::table('bankmaster')->select('*')->orderBy('bankname', 'ASC')->get(); 
         $currency = Currency::All();
         $period = Period::All();
         $clientser = DB::table('clientservices')->where('clientid','=',$user_id)->get();
         $clientser5 = DB::table('clientservices')->where('clientid','=',$user_id)->get()->first();
         $typeofser = Typeofser::orderBy('typeofservice', 'asc')->get();
         $empname = Employee::orderBy('firstName', 'asc')->get();   
         $clientsertitle = DB::table('clientservicetitles')->where('clientid','=',$user_id)->get();
         
         return view('client/profile',compact(['user_id','clientsertitle','empname','typeofser','clientser','period','currency','carddatalist','bankdatalist','bankdata','carddata','bankmaster','admin_professional','upload','taxstate','typeofentity','admin_notes','client','fsc','every','note','common','category','business','businessbrand','cd','cb','info','info1','professional','shareholder','question','answer']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
  /*      $category = Category::All();   
        $business = Business::all();
        $businessbrand = BusinessBrand::All(); 
        $cb = Categorybusiness::All();
$cd = Categorybusiness::All();
        $user_id = Auth::user()->user_id;
         $info = Contact_userinfo::where('user_id', '=', $user_id)->get();
        $info1 = Contact_userinfo::where('user_id', '=', $user_id)->first();
        $common = DB::table('commonregisters')->select('commonregisters.id as cid','commonregisters.bussiness_zip as bussiness_zip','commonregisters.business_state as business_state','commonregisters.business_city as business_city','commonregisters.business_country as business_country','commonregisters.business_address as business_address','commonregisters.business_store_name as business_store_name','commonregisters.mailing_address as mailing_address','commonregisters.legalname as legalname','commonregisters.dbaname as dbaname','commonregisters.mailing_city as mailing_city','commonregisters.mailing_state as mailing_state','commonregisters.mailing_zip as mailing_zip','commonregisters.user_type as user_type','commonregisters.user_type as user_type','commonregisters.status as status','commonregisters.company_name as company_name','commonregisters.business_name as business_name','commonregisters.first_name as first_name','commonregisters.middle_name as middle_name','commonregisters.last_name as last_name','commonregisters.email as email','commonregisters.address as address','commonregisters.address1 as address1','commonregisters.city as city','commonregisters.stateId as stateId','commonregisters.zip as zip','commonregisters.countryId as countryId','commonregisters.mobile_no as mobile_no','commonregisters.business_no as business_no','commonregisters.business_fax as business_fax','commonregisters.website as website','commonregisters.user_type as user_type','commonregisters.business_id as business_id','commonregisters.business_cat_id as business_cat_id','commonregisters.business_brand_id as business_brand_id','commonregisters.business_brand_category_id as business_brand_category_id','businesses.bussiness_name as bussiness_name' ,'categories.business_cat_name as business_cat_name','business_brands.business_brand_name as business_brand_name','categorybusinesses.business_brand_category_name as business_brand_category_name')
        ->leftJoin('categories', function($join){ $join->on('commonregisters.business_cat_id', '=', 'categories.id');})
        ->leftJoin('businesses', function($join){ $join->on('commonregisters.business_id', '=', 'businesses.id');})
        ->leftJoin('business_brands', function($join){ $join->on('commonregisters.business_brand_id', '=', 'business_brands.id');})
        ->leftJoin('categorybusinesses', function($join){ $join->on('commonregisters.business_brand_category_id', '=', 'categorybusinesses.id');})
        ->where('commonregisters.id', '=', $user_id)->get()->first();
        return view('client/profile',compact(['common','category','business','businessbrand','cb','cd','$info1','$info']));*/
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   
    public function update(Request $request, $id)
    {
           
            // $user_id1 = Auth::user()->user_id;
            $user_id1 = $id;
            //echo "<pre>";
            //print_r($_REQUEST);die;
            
            
            // $agent_fname1 = $request->agent_fname1;
            // $agent_mname1= $request->agent_mname1;
            // $agent_lname1= $request->agent_lname1;
            // $agent_position= $request->agent_position;
            // $agent_position2=$request->agent_position1;
            // $agent_per=$request->agent_per;
            // $effective_date =$request->effective_date;
            // $conid=$request->conid;
            // $total = $request->total;
            // $i = 0;

            // $data = $request->all();        
            // $user = User::find(auth()->user()->id);                      
            
            // $company_name = $request->company_name;
            // $business_name = $request->business_name;
            // $address = $request->address;
            // $address1 = $request->address1;
            // $countryId = $request->countryId;
            // $city = $request->city;
            // $stateId = $request->stateId;
            // $zip = $request->zip;
            // $mailing_address = $request->mailing_address;
            // $mailing_address1 = $request->mailing_address1;
            // $mailing_city = $request->mailing_city;
            // $mailing_state = $request->mailing_state;
            
            // $mailing_zip = $request->mailing_zip;
            // $email = $request->email;
            // $business_no = $request->business_no;
            // $business_fax = $request->business_fax;      
            // $website = $request->website;
            // $nametype = $request->nametype;
            // $firstname = $request->firstname;
            // $middlename = $request->middlename;
            // $lastname = $request->lastname;
            // $contact_address1 = $request->contact_address1;
            // $contact_address2 = $request->contact_address2;
            // $city_1 = $request->city_1;
            // $state = $request->state;
            // $zip_1 = $request->zip_1;
            // $mobile_1 = $request->mobile_1;
            // $mobiletype_1 = $request->mobiletype_1;
            // $mobile_2 = $request->mobile_2;
            // $mobiletype_2 = $request->mobiletype_2;
            // $ext2_2 = $request->ext2_2;
            // $contact_fax_1 = $request->contact_fax_1;
            
            
            
            // $mailing_city = $request->mailing_city;           
            // $mailing_state = $request->mailing_state;           
            // $mailing_zip = $request->mailing_zip;           
            // $mailing_address = $request->mailing_address; 
            // $mailing_address1 = $request->mailing_address1; 
            // $business_store_name = $request->business_store_name;  
            // $business_address = $request->business_address;
            // $business_country = $request->business_country;  
            // $business_city = $request->business_city;  
            // $business_state = $request->business_state;  
            // $bussiness_zip = $request->bussiness_zip;  
            // $business =User::find($id);
            // $user_id = Auth::user()->user_id;  
            // $business->name = $request->name;             
            // $business->question1 = $request->question1;            
            // $business->question2 = $request->question2;            
            // $business->question3 = $request->question3;            
            // $business->answer1 = $request->answer1;            
            // $business->answer2 = $request->answer2;            
            // $business->answer3 = $request->answer3; 
            // $info = client_shareholder::where('client_id', '=',$user_id)->first();
                // foreach($agent_fname1 as $post)
                // { 
                // $conid1 =$conid[$i];
                // $agent_fname11 =$agent_fname1[$i];
                // $agent_mname11 =$agent_mname1[$i];
                // $agent_lname11 =$agent_lname1[$i];
                // $agent_position1 =$agent_position[$i];
                // $agent_position11 =$agent_position2;
                // $agent_per1 =$agent_per[$i];
                // $effective_date1 =$effective_date[$i];
                // $total1 =$total;  
                // $i++; 
                // $insert2 = DB::insert("insert into client_shareholders(`agent_fname1`,`agent_mname1`,`agent_lname1`,`agent_position`,`agent_position1`,`total`,`client_id`,`agent_per`,`effective_date`) values('".$agent_fname11."','".$agent_mname11."','".$agent_lname11."','".$agent_position1."','".$agent_position11."','".$total1."','".$user_id."','".$agent_per1."','".$effective_date1."')");
                // if(empty($conid))
                // {
                // }
                // else
                // {
                // $affectedRows = client_shareholder::where('id', '=', $conid1)->delete();
                // $affectedRows = client_shareholder::where('agent_fname1', '=', '')->delete();
                // }
                // }
                //$type_of_entity = implode(",", $request->get('type_of_entity'));
                //$type_of_entity_answer = implode(",", $request->get('type_of_entity_answer'));
                
                $returnValue = DB::table('commonregisters')->where('id', '=', $user_id1)->update([
                    
                        'code' => $request->code,
                        'company_name' => $request->company_name,
                        'business_name' => $request->business_name,
                        'address' => $request->address,
                        'address1' => $request->address1,
                        'countryId' => $request->countrId,
                        'city' => $request->city,
                        'stateId' => $request->stateId,
                        'zip' => $request->zip,
                        'mailing_address' => $request->mailing_address,
                        'mailing_address1' => $request->mailing_address1,
                        'mailing_city' => $request->mailing_city,
                        'mailing_state' => $request->mailing_stae,
                        
                        'mailing_zip' => $request->mailing_zip,
                        'email' => $request->email,
                        'business_no' => $request->business_no,
                        'business_fax' => $request->business_fax,
                        'website' => $request->website,
                        'nametype' => $request->nametype,
                        'firstname' => $request->firstname,
                        'middlename' => $request->middlename,
                        'lastname' => $request->lastname,
                        'contact_address1' => $request->contact_address1,
                        'contact_address2' => $request->contact_address2,
                        'city_1' => $request->city_1,
                        'state' => $request->state,
                        'zip_1' => $request->zip_1,
                        'mobile_1' => $request->mobile_1,
                        'mobiletype_1' => $request->mobiletype_1,
                        'mobile_2' => $request->mobile_2,
                        'mobiletype_2' => $request->mobiletype_2,
                        'ext2_2' => $request->ext2_2,
                        'contact_fax_1' => $request->contact_fax_1,
                        'limited_active' => 1,
                        
                    ]);
                    
                // $noteid = $request->noteid;
                // $adminnotes = $request->adminnotes;
                // $notetype = $request->notetype;
                // $usid = $request->usid;
                // $k=0;
                // $users = DB::table('notes')->where('admin_id', '=', $usid)->first();
                // foreach($adminnotes as $notess)
                // { 
                //     $noteid1 = $noteid[$k];
                //     $note1 =$adminnotes[$k];
                //     $notetype1 =$notetype[$k];
                //     $usid1 =$usid[$k];
                //     $k++;
                //     if(empty($noteid1))
                //     {
                //         $insert2 = DB::insert("insert into notes(`notes`,`admin_id`,`type`) values('".$note1."','".$usid1."','".$notetype1."')");
                //     }
                //     else
                //     {
                //         $returnValue = DB::table('notes')->where('id', '=', $noteid1)
                //         ->update([ 'notes' => $note1,
                //               'admin_id' =>$usid1,
                //               'type' =>$notetype1 
                //     ]);    
                //         $users = DB::table('notes')->where('notes', '=', '')->delete();
                //     }
                // }            
            //$business->update();            
            return redirect()->back()->with('success', 'Your Profile Successfully Updated');
       
    }
   
   
   
   
    public function getcategory2(Request $request)
    {
      
        $data = Category::select('business_cat_name','id')->where('bussiness_name',$request->id)->take(100)->get();
        return response()->json($data);
    }
    public function getcategory111(Request $request)
    {
        $data = Business::select('bussiness_name','id')->where('id',$request->id)->take(100)->get();
        return response()->json($data);  
    }
    public function delete1($id,$cid)
    {        
    Contact_userinfo::where('id',$id)->delete(); 
    return redirect('client/profile/');     
    }
   
     public function getimagesssss111(Request $request)
     {
       $data = Clientnupload::select('upload_name','upload')->where('upload_name',$request->id)->take(10000)->get();
       if(! $data)
      {
     $data = "File not Uploaded";
return response()->json($data); 
}
else
{
return response()->json($data);  
}
}
public function getfaderal1(Request $request)
{
$data = taxfederal::select('authority_name','short_name','telephone','address','city','zip','website_link_name')->where('authority_name',$request->id)->take(10000)->get();
return response()->json($data);  
}
 public function clientdelete2_1($id)
    { 
       $logoStatus = client_shareholder::findOrFail($id);
       client_shareholder::where('id',$id)->delete();
       return redirect('client/profile')->with('error','Record is delete');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
