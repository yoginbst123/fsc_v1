<?php
namespace App\Http\Controllers\User;
use App\Model\Msg;
use App\Model\Admin;
use App\employees\Fscemployee;
use Illuminate\Http\Request;
use App\Front\Commonregister;
use App\Model\Employee;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use App\User;
class SubmissionsController extends Controller
{
public function __construct()
    {
    $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
      
       return view('client/submissions/submissions');
    }

    
}
