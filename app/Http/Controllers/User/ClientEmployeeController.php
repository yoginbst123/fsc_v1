<?php
namespace App\Http\Controllers\User;
use App\Http\Controllers\Controller;
use App\Model\Employee;
use App\employees\Fscemployee;
use Illuminate\Http\Request;
use App\Model\Position;
use App\Model\Branch;
use App\Model\Rules;
use App\User;
use View;
use Validator;
use Redirect;
use Session;
use DB;
use Auth;
use Hash;
use App\Model\Super;
use App\Model\Schedule;
use App\Model\Msg;
use App\Model\Task;
use Illuminate\Support\Facades\Input;
use App\Front\ApplyEmployment;
use Illuminate\Support\Facades\File;
class ClientEmployeeController extends Controller
{
public function __construct()
    {
    $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
       $start = Auth::user()->user_id;
       $position = Position::All(); 
       $branch = Branch::All(); 
       $employee = Employee::orderBy('employee_id', 'asc')->where('type','=','clientemployee')->where('clienttype','=',$start)->get(); 
       return view('client/cli-employee/cli-employee',compact(['employee','position','branch']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clienttype_id = Auth::user()->user_id;
       return view('client/cli-employee/create',compact(['clienttype_id']));
    }

    public function store(Request $request)
    {
      // echo "<pre>";print_r($_POST);exit;
        if($request->hasFile('photo'))
        {
            $filname = $request->photo->getClientOriginalName();
            $request->photo->move('public/employeeimage', $filname);
        }  
        else
        {
            $filname ='';
        }
        
          
        
        
        
        $id = Auth::user()->user_id;
        $employee= new Employee;       
        $employee->clienttype = $id;
        $employee->employee_id = $request->employee_id;
        $employee->check = $request->check;
        //  $employee->type = 'employee';
        $employee->firstName= $request->firstName;
        $employee->middleName= $request->middleName;
        $employee->lastName= $request->lastName;
        $employee->address1= $request->address1;
        $employee->address2= $request->address2;
        $employee->city= $request->city;
        $employee->stateId= $request->stateId;
        
        if($request->city!='' && $request->stateId!='')
        {
            $aa=$request->city;
            $bb=$request->stateId;
            $cc=$aa.', '.$bb;
           
            
        }
        else
        {
             $cc='';
        }
        
        $employee->branch_city=$cc;
        $employee->zip= $request->zip;
        $employee->countryId= $request->countryId;
        $employee->telephoneNo1= $request->telephoneNo1;
        $employee->telephoneNo2= $request->telephoneNo2;
        $employee->ext1= $request->ext1;
        $employee->ext2= $request->ext2;
        $employee->telephoneNo1Type= $request->telephoneNo1Type;
        $employee->telephoneNo2Type= $request->telephoneNo2Type;
        $employee->email= $request->email;
        $employee->nametype= $request->nametype;
        $employee->photo= $filname;
        $employee->type= 'clientemployee';
        // $password = mt_rand();
        // $password1 = bcrypt($password);
        // $employee->password1= $password;
        // $employee->password= $password1;
        $employee->save();
        $email = $request->email;
        $name = $request->firstName .' '. $request->middleName .' '. $request->lastName;
        
        $lastInsertedId= $employee->id;   
        $employee_id=$request->employee_id;
        
        if($request->check=='1')
        {
            if($request->city!='' && $request->stateId!='')
            {
                $aa=$request->city;
                $bb=$request->stateId;
                $cc=$aa.', '.$bb;
               
                
            }
            else
            {
                 $cc='';
            } 
            $password = mt_rand();
            $password1 = bcrypt($password);
            $user = Fscemployee::where('email', '=', $email)->where('type1', '=', 'clientemployee')->first();
            if ($user === null)
            {
                //print_r($user);exit;
                
                $insert = DB::insert("insert into fscemployees(`clienttype`,`name`,`branch_city`,`email`,`password`,`newpassword`,`user_id`,`type`,`type1`,`employee_id`) values('".$id."','".$name."','".$cc."','".$email."','".bcrypt($password)."','".$password."','".$lastInsertedId."','1','clientemployee','".$employee_id."')");
            
                $data = array('email' => $email, 'firstName' => $name, 'from' => $email, 'password' =>$password);       
                \Mail::send( 'client/employemsg', $data, function( $message ) use ($data)
                {
                    $message->to( $data['email'] )->from('info@financialservicecenter.net', $data['firstName'], $data['password'] )->subject( 'Account Approval' );
                }); 
            }    
        }

        return redirect('client/cli-employee')->with('success','Successfully added Employee');
    }
    
    
    // public function store(Request $request)
    // {
       
    //     if($request->hasFile('photo'))
    //     {
    //      $filname = $request->photo->getClientOriginalName();
    //      $request->photo->move('public/employeeimage', $filname);
    //     }  
    //     else
    //     {
    //     $filname ='';
    //     }
        
    //     $id = Auth::user()->user_id;
    //     $employee= new Employee;       
    //     $employee->clienttype = $id;
    //     $employee->employee_id = $request->employee_id;
    //     $employee->check = $request->check;
    //   //  $employee->type = 'employee';
    //     $employee->firstName= $request->firstName;
    //     $employee->middleName= $request->middleName;
    //     $employee->lastName= $request->lastName;
    //     $employee->address1= $request->address1;
    //     $employee->address2= $request->address2;
    //     $employee->city= $request->city;
    //     $employee->stateId= $request->stateId;
    //     $employee->zip= $request->zip;
    //     $employee->countryId= $request->countryId;
    //     $employee->telephoneNo1= $request->telephoneNo1;
    //     $employee->telephoneNo2= $request->telephoneNo2;
    //     $employee->ext1= $request->ext1;
    //     $employee->ext2= $request->ext2;
    //     $employee->telephoneNo1Type= $request->telephoneNo1Type;
    //     $employee->telephoneNo2Type= $request->telephoneNo2Type;
    //     $employee->email= $request->email;
    //     $employee->nametype= $request->nametype;
    //     $employee->photo= $filname;
    //     $employee->type= 'clientemployee';
    //     $employee->save();
    //     $email = $request->email;
    //     $name = $request->firstName .' '. $request->middleName .' '. $request->lastName;
    //     $lastInsertedId= $employee->id;   
    //     $employee_id=$request->employee_id;
    //     if($request->check=='1')
    //     {
             
    //     $password = mt_rand();
    //     $password1 = bcrypt($password);
    //     $user = Fscemployee::where('email', '=', $email)->where('type1', '=', 'clientemployee')->first();
        
    //     if ($user === null){
            
    //     $insert = DB::insert("insert into fscemployees(`name`,`email`,`password`,`newpassword`,`user_id`,`type`,`type1`,`employee_id`) values('".$name."','".$email."','".bcrypt($password)."','".$password."','".$lastInsertedId."','1','clientemployee','".$employee_id."')");
        
    //     $data = array('email' => $email, 'firstName' => $name, 'from' => $email, 'password' =>$password);       
    //     \Mail::send( 'client/employemsg', $data, function( $message ) use ($data)
    //     {
    //     $message->to( $data['email'] )->from('info@financialservicecenter.net', $data['firstName'], $data['password'] )->subject( 'Account Approval' );
    //     }); 
    //     }    
    //     }
    //     return redirect('client/cli-employee')->with('success','Success fully add Message');
    // }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\State  $state
     * @return \Illuminate\Http\Response
     */
    public function show(State $state)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\State  $state
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {  
           $super = Super::All();
           $newclient = Employee::where('id',$id)->update(['newemp' => 2]);
           $emp = Employee::where('id',$id)->first();
           $super1 = Employee::where('super','=','1')->get();
           $empfsc = Fscemployee::where('user_id',$id)->first();
           $position = Position::All(); 
           $rules = Rules::All(); 
           //$branch = Branch::All(); 
           $branch  = DB::table('branches')->select('city')->groupBy('city')->get();
           $info1 = DB::table('employee_pay_info')->where('employee_id', $id)->first();
           $employes = DB::table('employee_rules')->where('emp_id', $id)->get();
           $employes2 = DB::table('employee_rules')->where('emp_id', $id)->first();
           $info = DB::table('employee_pay_info')->where('employee_id', $id)->get(); 
           $info3 = DB::table('employee_other_info')->where('employee_id', $id)->get();
           $info2 = DB::table('employee_other_info')->where('employee_id', $id)->first();
           $review1= DB::table('employee_review')->where('employee_id', $id)->get();
           $review = DB::table('employee_review')->where('employee_id', $id)->first();
           $admin_notes = DB::table('cli_emp_notes')->where('client_emp_id','=',$id)->get();
           //cli_emp_notes
           return View('client.cli-employee.edit',compact(['super','super1','admin_notes','rules','emp','info1','info','empfsc','info2','info3','review1','review','position','branch','employes','employes2']));
    }

    
// public function update(Request $request, $id)
// {
//   // return $request;exit;
// if($request->check=='0')
// {
// $status = 0;
// $password1 =  '';
// $name = $request->firstName .' '. $request->middleName .' '. $request->lastName;   
// DB::table('fscemployees')->where('user_id', $id)->update(['type'=>0,'name'=>$name]);
// DB::table('employees')->where('id', $id)->update(['check'=>0]);
// }
// else
// {
//     $name = $request->firstName .' '. $request->middleName .' '. $request->lastName;   

//  $status = '1';
// $email = $request->email;
// DB::table('fscemployees')->where('user_id', $id)->update(['type'=>1,'name'=>$name]);
// if($request->status=='0')
// {
// $password =  mt_rand();
// $password1 =  bcrypt($password);
// $name = $request->firstName .' '. $request->middleName .' '. $request->lastName;
// $user = Fscemployee::where('email', '=', $email)->where('type1', '=', 'clientemployee')->first();
// if ($user === null){
// $insert = DB::insert("insert into fscemployees(`name`,`email`,`password`,`newpassword`,`user_id`,`type`,`type1`) values('".$name."','".$email."','".bcrypt($password)."','".$password."','".$id."','1','clientemployee')");
// $data = array('email' => $email, 'firstName' => $name, 'from' => $email, 'password' =>$password);       
// \Mail::send( 'fac-Bhavesh-0554/employe', $data, function( $message ) use ($data)
// {
// $message->to( $data['email'] )->from('info@financialservicecenter.net', $data['firstName'], $data['password'] )->subject( 'Account Approval' );
// }); 
// } 
// }
// else
// {
// $password1= $request->password;
// }
// }    
//         $employee= Employee::find($id);       
//         $employee->employee_id = $request->employee_id;
//         $employee->super = $request->super;
//         $employee->firstName= $request->firstName;
//       // $employee->type= $request->type;
//         $employee->read= $request->terms;
//         $employee->middleName= $request->middleName;
//         $employee->lastName= $request->lastName;
//         $employee->address1= $request->address1;
//         $employee->address2= $request->address2;
//         $employee->city= $request->city;
//         $employee->password = $password1;
//         $employee->stateId= $request->stateId;
//         $employee->zip= $request->zip;
//         $employee->countryId= $request->countryId;
//         $employee->telephoneNo1= $request->telephoneNo1;
//         $employee->telephoneNo2= $request->telephoneNo2;
//         $employee->ext1= $request->ext1;
//         $employee->ext2= $request->ext2;
//         $employee->telephoneNo1Type= $request->telephoneNo1Type;
//         $employee->telephoneNo2Type= $request->telephoneNo2Type;
//         $employee->email= $request->email;
//         $employee->hiremonth= $request->hiremonth;
//         $employee->hireday= $request->hireday;
//         $employee->hireyear= $request->hireyear;
//         $employee->termimonth= $request->termimonth;
//         $employee->termiday= $request->termiday;
//         $employee->termiyear= $request->termiyear;
//         $employee->tnote= $request->tnote;
//         $employee->rehiremonth= $request->rehiremonth;
//         $employee->rehireday= $request->rehireday;
//         $employee->rehireyear= $request->rehireyear;
//         $employee->branch_city= $request->branch_city;
//         $employee->branch_name= $request->branch_name;
//         $employee->position= $request->position;
//         $employee->note= $request->note;
//         $employee->pay_method= $request->pay_method;
//         $employee->pay_frequency= $request->pay_frequency;
//         $employee->gender= $request->gender;
//         $employee->marital= $request->marital;
//         $employee->month= $request->month;
//         $employee->day= $request->day;
//         $employee->year= $request->year;
//         $employee->pf1= $request->pf1;
//         $employee->pf2= $request->pf2;
//         $employee->epname= $request->epname;
//         $employee->relation= $request->relation;
//         $employee->eaddress1= $request->eaddress1;
//         $employee->ecity= $request->ecity;
//         $employee->estate= $request->estate;
//         $employee->ezipcode= $request->ezipcode;
//         $employee->etelephone1= $request->etelephone1;
//         $employee->eteletype1= $request->eteletype1;
//         $employee->eext1= $request->eext1;
//         $employee->etelephone2= $request->etelephone2;
//         $employee->eteletype2= $request->eteletype2;
//         $employee->eext2= $request->eext2;
//         $employee->comments1= $request->comments1;
//         $employee->uname= $request->uname;
//         $employee->question1= $request->question1;
//         $employee->answer1= $request->answer1;
//         $employee->question2= $request->question2;
//         $employee->answer2= $request->answer2;
//         $employee->question3= $request->question3;
//         $employee->answer3= $request->answer3;
//         $employee->other_info= $request->other_info;
//         $employee->computer_name= $request->computer_name;
//         $employee->computer_ip= $request->computer_ip;
//         $employee->comments= $request->comments;
//         $employee->check= $request->check;
//         $employee->filling_status= $request->filling_status;
//         $employee->fedral_claim= $request->fedral_claim;
//         $employee->additional_withholding= $request->additional_withholding;
//         $employee->state_claim= $request->state_claim;
//         $employee->additional_withholding_1= $request->additional_withholding_1;
//         $employee->local_claim= $request->local_claim;
//         $employee->additional_withholding_2= $request->additional_withholding_2;
//         $employee->type_agreement= $request->type_agreement;
//         $employee->firstName_1= $request->firstName_1;
//         $employee->middleName_1= $request->middleName_1;
//         $employee->lastName_1= $request->lastName_1;
//         $employee->address11= $request->address11;
//         $employee->efax= $request->efax;
//         $employee->fax= $request->fax;
//         $employee->reset= $request->reset;
//          $employee->resetdate= $request->reset_date;
//          $employee->nametype= $request->nametype;
//         $employee->eemail= $request->eemail;
//         $employee->status=$status;
//         $employee->update();
        
//         if($employee->update()) {
//         return response()->json([
//             'status'     => 'success',
//              'employee_id'     => $request->employee_id]);
//     } else {
//         return response()->json([
//             'status' => 'error']);
//     }
//         $pay_method = $request->pay_method;
//         $pay_frequency = $request->pay_frequency;
//         $pay_scale = $request->pay_scale;
//         $effective_date = $request->effective_date;
//         $fields = $request->fields;
//         $employee= $request->employee;
//         $work= $request->work;
//         $work_responsibility1= $request->work_responsibility;

//         $i = 0;
//         $j = 0; 
//         $k = 0;
// DB::table('employee_pay_info')->where('employee_id', $id)->first();
// foreach($pay_scale as $post)
// { 
// $pay_method1 = $pay_method;
// $pay_frequency1 = $pay_frequency;
// $pay_scale1 = $pay_scale[$i];
// $effective_date1 = $effective_date[$i];
// $fields1 = $fields[$i];
// $employee1 = $employee[$i];
// $i++; 
// DB::table('employee_pay_info')->where('pay_scale','=', '')->delete();
// if(empty($employee1))
// {
// $insert2 = DB::insert("insert into employee_pay_info(`employee_id`,`pay_method`,`pay_frequency`,`pay_scale`,`effective_date`,`fields`) values('".$id."','".$pay_method."','".$pay_frequency."','".$post."','".$effective_date1."','".$fields1."')");    
// DB::table('employee_pay_info')->where('pay_scale','=', '')->delete();
  
// }
// else
// {
// $returnValue = DB::table('employee_pay_info')->where('id', '=', $employee1)->update([ 'employee_id' => $id,'pay_method' =>$pay_method,'pay_frequency' =>$pay_frequency,'pay_scale' =>$post,'effective_date' =>$effective_date1,'fields' =>$fields1]);
// DB::table('employee_pay_info')->where('pay_scale','=', '')->delete();
// }
// }
// $first_rev_day = $request->first_rev_day;
// $reviewmonth= $request->reviewmonth;
// $hiring_comments= $request->hiring_comments;
// $ree= $request->ree;

// DB::table('employee_review')->where('employee_id', $id)->first();
// foreach($first_rev_day as $post)
// { 
// $first_rev_day1 = $first_rev_day[$k];
// $reviewmonth1 = $reviewmonth[$k];
// $hiring_comments1 = $hiring_comments[$k];
// $ree1 = $ree[$k];
// $k++; 
// DB::table('employee_review')->where('first_rev_day', '=', '')->delete();
// if(empty($ree1))
// {
//     $insert2 = DB::insert("insert into employee_review(`employee_id`,`first_rev_day`,`reviewmonth`,`hiring_comments`) values('".$id."','".$post."','".$reviewmonth1."','".$hiring_comments1."')");
//      DB::table('employee_review')->where('first_rev_day', '=', '')->delete();
// }
// else
// {
// $returnValue = DB::table('employee_review')->where('id', '=', $ree1)
// ->update([ 'employee_id' => $id,
//           'first_rev_day' =>$post,
//           'reviewmonth' =>$reviewmonth1,
//           'hiring_comments' =>$hiring_comments1
//     ]);
// DB::table('employee_review')->where('first_rev_day', '=', '')->delete();
// }
// }

// $noteid = $request->noteid;
// $adminnotes= $request->adminnotes;
// $k=0;
// $type=$request->type;
// $users = DB::table('notes')->where('admin_id', '=', $id)->first();
// foreach($adminnotes as $notess)
// { 
//   $noteid1 = $noteid[$k];
//   $note1 =$adminnotes[$k];
//   $k++;
//   if(empty($noteid1))
// {
//     $insert2 = DB::insert("insert into notes(`notes`,`admin_id`,`type`) values('".$note1."','".$id."','".$type."')");
// }
// else
// {
// $returnValue = DB::table('notes')->where('id', '=', $noteid1)->update([ 'notes' => $note1,'admin_id' =>$id,'type' =>$type]);    
// $users = DB::table('notes')->where('notes', '=', '')->delete();
// }
// }
// }

    public function update(Request $request, $id)
    {
        // echo "<pre>";
        // print_r($_REQUEST);die;
          $user_id = Auth::user()->user_id;
        if($request->check=='0')
        {
            $status = 0;
            $password1 =  '';
            $name = $request->firstName .' '. $request->middleName .' '. $request->lastName;   
            DB::table('fscemployees')->where('user_id', $id)->update(['type'=>0,'name'=>$name]);
            DB::table('employees')->where('id', $id)->update(['check'=>0]);
        }
        else
        {
            if($request->city!='' && $request->stateId!='')
            {
                $aa=$request->city;
                $bb=$request->stateId;
                $cc=$aa.', '.$bb;
            }
            else
            {
                $cc='';
            }
            
            $name = $request->firstName .' '. $request->middleName .' '. $request->lastName;   
            $status = '1';
            $email = $request->email;
            DB::table('fscemployees')->where('user_id', $id)->update(['type'=>1,'name'=>$name]);
            if($request->status=='0')
            {
                
                $password =  mt_rand();
                $password1 =  bcrypt($password);
                $name = $request->firstName .' '. $request->middleName .' '. $request->lastName;
                $user = Fscemployee::where('email', '=', $email)->where('type1', '=', 'clientemployee')->first();
                if ($user === null)
                {
                    if($request->city!='' && $request->stateId!='')
                    {
                        $aa=$request->city;
                        $bb=$request->stateId;
                        $cc=$aa.', '.$bb;
                    }
                    else
                    {
                        $cc='';
                    }
                     $insert = DB::insert("insert into fscemployees(`clienttype`,`name`,`branch_city`,`email`,`password`,`newpassword`,`user_id`,`type`,`type1`) values('".$user_id."','".$name."','".$cc."','".$email."','".bcrypt($password)."','".$password."','".$id."','1','clientemployee')");
                     $data = array('email' => $email, 'firstName' => $name, 'from' => $email, 'password' =>$password);       
                    \Mail::send( 'VARR-Admin/employe', $data, function( $message ) use ($data)
                    {
                        $message->to( $data['email'] )->from('info@financialservicecenter.net', $data['firstName'], $data['password'] )->subject( 'Account Approval' );
                    }); 
                } 
            }
            else
            {
                $password =  mt_rand();
                $password1 =  bcrypt($password);
                //$password1= $request->password;
            }
        }    
        
            $noteid = $request->noteid;
            $adminnotes= $request->adminnotes;
            $k=0;
            //$users = DB::table('cli_emp_notes')->where('admin_id', '=', $id)->first();
            if(!empty($adminnotes))
            {
                foreach($adminnotes as $notess)
                { 
                   $noteid1 = $noteid[$k];
                //   $note1 =$adminnotes[$k];
                    $note1 =$notess;
                   $k++;
                   $insert2 = DB::insert("insert into cli_emp_notes(`notes`,`client_emp_id`) values('".$note1."','".$id."')");
                   if(empty($noteid1))
                    {
                        
                    }
                    else
                    {
                        $returnValue = DB::table('cli_emp_notes')->where('id','=',$noteid1)->delete();
                    }
                }
            }
        
                
                
                
                
        
                $employee= Employee::find($id);       
                $employee->employee_id = $request->employee_id;
                $employee->super = $request->super;
                $employee->firstName= $request->firstName;
                $employee->type= $request->type;
                $employee->read= $request->terms;
                $employee->middleName= $request->middleName;
                $employee->lastName= $request->lastName;
                $employee->address1= $request->address1;
                $employee->address2= $request->address2;
                $employee->city= $request->city;
                $employee->password = $password1;
                //$employee->password1 = $password;
                
                $employee->stateId= $request->stateId;
                
                if($request->city!='' && $request->stateId!='')
                {
                    $aa=$request->city;
                    $bb=$request->stateId;
                    $cc=$aa.', '.$bb;
                }
                else
                {
                    $cc='';
                }
                $employee->branch_city=$cc;
                $employee->zip= $request->zip;
                $employee->countryId= $request->countryId;
                $employee->telephoneNo1= $request->telephoneNo1;
                $employee->telephoneNo2= $request->telephoneNo2;
                $employee->ext1= $request->ext1;
                $employee->ext2= $request->ext2;
                $employee->telephoneNo1Type= $request->telephoneNo1Type;
                $employee->telephoneNo2Type= $request->telephoneNo2Type;
                $employee->email= $request->email;
                $employee->hiremonth= $request->hiremonth;
                $employee->hireday= $request->hireday;
                $employee->hireyear= $request->hireyear;
                $employee->termimonth= $request->termimonth;
                $employee->termiday= $request->termiday;
                $employee->termiyear= $request->termiyear;
                $employee->tnote= $request->tnote;
                $employee->rehiremonth= $request->rehiremonth;
                $employee->rehireday= $request->rehireday;
                $employee->rehireyear= $request->rehireyear;
                $employee->branch_name= $request->branch_name;
                $employee->position= $request->position;
                $employee->note= $request->note;
                $employee->pay_method= $request->pay_method;
                $employee->pay_frequency= $request->pay_frequency;
                $employee->gender= $request->gender;
                $employee->marital= $request->marital;
                $employee->month= $request->month;
                $employee->day= $request->day;
                $employee->year= $request->year;
                $employee->pf1= $request->pf1;
                $employee->pf2= $request->pf2;
                
                // $path1= public_path().'/adminupload/'.$_FILES['resume']['name'];
                // if(isset($_FILES['resume']['name'])!='' && $_FILES['resume']['name']!=null)
                // {
                //     if(move_uploaded_file($_FILES['resume']['tmp_name'], $path1)) 
                //     {
                //       $filename=$_FILES['resume']['name']; 
                //     } 
                //     else
                //     {
                //         $filename=$request->resume_1;
                //     }
                // }
                // else
                // {
                //     $filename=$request->resume_1;
                // }
                // $employee->resume= $filename;
                
                
                // $path1= public_path().'/adminupload/'.$_FILES['resume']['name'];
                // if(isset($_FILES['resume']['name'])!='' && $_FILES['resume']['name']!=null)
                // {
                //     if(move_uploaded_file($_FILES['resume']['tmp_name'], $path1)) 
                //     {
                //       $filename=$_FILES['resume']['name']; 
                //     } 
                //     else
                //     {
                //         $filename=$request->resume_1;
                //     }
                // }
                // else
                // {
                //     $filename=$request->resume_1;
                // }
                // $employee->resume= $filename;
                
                $employee->epname= $request->epname;
                $employee->relation= $request->relation;
                $employee->eaddress1= $request->eaddress1;
                $employee->ecity= $request->ecity;
                $employee->estate= $request->estate;
                $employee->ezipcode= $request->ezipcode;
                $employee->etelephone1= $request->etelephone1;
                $employee->eteletype1= $request->eteletype1;
                $employee->eext1= $request->eext1;
                $employee->etelephone2= $request->etelephone2;
                $employee->eteletype2= $request->eteletype2;
                $employee->eext2= $request->eext2;
                $employee->comments1= $request->comments1;
                $employee->uname= $request->uname;
                $employee->question1= $request->question1;
                $employee->answer1= $request->answer1;
                $employee->question2= $request->question2;
                $employee->answer2= $request->answer2;
                $employee->question3= $request->question3;
                $employee->answer3= $request->answer3;
                $employee->other_info= $request->other_info;
                $employee->computer_name= $request->computer_name;
                $employee->computer_ip= $request->computer_ip;
                $employee->comments= $request->comments;
                $employee->check= $request->check;
                $employee->filling_status= $request->filling_status;
                $employee->fedral_claim= $request->fedral_claim;
                $employee->additional_withholding= $request->additional_withholding;
                $employee->state_claim= $request->state_claim;
                $employee->additional_withholding_1= $request->additional_withholding_1;
                $employee->local_claim= $request->local_claim;
                $employee->additional_withholding_2= $request->additional_withholding_2;
                $employee->type_agreement= $request->type_agreement;
                $employee->firstName_1= $request->firstName_1;
                $employee->middleName_1= $request->middleName_1;
                $employee->lastName_1= $request->lastName_1;
                $employee->address11= $request->address11;
                $employee->efax= $request->efax;
                $employee->fax= $request->fax;
                $employee->reset= $request->reset;
                 $employee->resetdate= $request->reset_date;
                 $employee->nametype= $request->nametype;
                $employee->eemail= $request->eemail;
                $employee->status=$status;
                $employee->update();
                
                if($employee->update()) {
                return response()->json([
                    'status'     => 'success',
                     'employee_id'     => $request->employee_id]);
                } else {
                    return response()->json([
                        'status' => 'error']);
                }
                
                
                $pay_method = $request->pay_method;
                $pay_frequency = $request->pay_frequency;
                $pay_scale = $request->pay_scale;
                $effective_date = $request->effective_date;
                $fields = $request->fields;
                $employee= $request->employee;
                $work= $request->work;
                $work_responsibility1= $request->work_responsibility;
        
                $i = 0;
                $j = 0; 
                $k = 0;
        DB::table('employee_pay_info')->where('employee_id', $id)->first();
        foreach($pay_scale as $post)
        { 
        $pay_method1 = $pay_method;
        $pay_frequency1 = $pay_frequency;
        $pay_scale1 = $pay_scale[$i];
        $effective_date1 = $effective_date[$i];
        $fields1 = $fields[$i];
        $employee1 = $employee[$i];
        $i++; 
        DB::table('employee_pay_info')->where('pay_scale','=', '')->delete();
        if(empty($employee1))
        {
        $insert2 = DB::insert("insert into employee_pay_info(`employee_id`,`pay_method`,`pay_frequency`,`pay_scale`,`effective_date`,`fields`) values('".$id."','".$pay_method."','".$pay_frequency."','".$post."','".$effective_date1."','".$fields1."')");    
        DB::table('employee_pay_info')->where('pay_scale','=', '')->delete();
          
        }
        else
        {
        $returnValue = DB::table('employee_pay_info')->where('id', '=', $employee1)->update([ 'employee_id' => $id,'pay_method' =>$pay_method,'pay_frequency' =>$pay_frequency,'pay_scale' =>$post,'effective_date' =>$effective_date1,'fields' =>$fields1]);
        DB::table('employee_pay_info')->where('pay_scale','=', '')->delete();
        }
        }
        $first_rev_day = $request->first_rev_day;
        $reviewmonth= $request->reviewmonth;
        $hiring_comments= $request->hiring_comments;
        $ree= $request->ree;
        
        DB::table('employee_review')->where('employee_id', $id)->first();
        foreach($first_rev_day as $post)
        { 
        $first_rev_day1 = $first_rev_day[$k];
        $reviewmonth1 = $reviewmonth[$k];
        $hiring_comments1 = $hiring_comments[$k];
        $ree1 = $ree[$k];
        $k++; 
            DB::table('employee_review')->where('first_rev_day', '=', '')->delete();
            if(empty($ree1))
            {
                $insert2 = DB::insert("insert into employee_review(`employee_id`,`first_rev_day`,`reviewmonth`,`hiring_comments`) values('".$id."','".$post."','".$reviewmonth1."','".$hiring_comments1."')");
                 DB::table('employee_review')->where('first_rev_day', '=', '')->delete();
            }
            else
            {
            $returnValue = DB::table('employee_review')->where('id', '=', $ree1)
            ->update([ 'employee_id' => $id,
                       'first_rev_day' =>$post,
                       'reviewmonth' =>$reviewmonth1,
                       'hiring_comments' =>$hiring_comments1
                ]);
            DB::table('employee_review')->where('first_rev_day', '=', '')->delete();
            }
        }
        
    }


    public function clientempnotedelete($id)
    { 
       $users = DB::table('cli_emp_notes')->where('id',$id)->delete();
       return redirect()->back()->with('success','Your Record Deleted Successfully');
    }

    public function getbranch12(Request $request)
    {
    $data = Branch::select('branchname','city')->where('city',$request->id)->take(100)->get();
    return response()->json($data);  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\State  $state
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
    $ss = Employee::where('id',$id)->first();
    $k=$ss->email;
    ApplyEmployment::where('email',$k)->delete();
    Employee::where('id',$id)->delete();
    Schedule::where('emp_name',$id)->delete();
    Schedule::where('emp_name',$id)->delete();
    DB::table('empschedules')->where('employee_id', $id);
    Msg::where('employeeid',$id)->delete();
    Msg::where('admin_id',$id)->delete();
    Task::where('employeeid',$id)->delete();
    Fscemployee::where('user_id',$id)->delete();
    DB::table('employee_review')->where('employee_id','=',$id)->first();
    return redirect(route('cli-employee.index'));
    }
}
