<?php
namespace App\Http\Controllers\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Front\Commonregister;
use App\Model\BusinessBrand;
use App\Model\Employee;
use App\Model\Category;
use App\Model\Business;
use App\Model\client_professional;
use App\Model\client_shareholder;
use App\User;
use App\Model\Categorybusiness;
use App\Model\Contact_userinfo;
use App\Mail\Activationmail;
use App\Model\taxstate;
use DB;
use Hash;
use Carbon;
use App\Model\Admin;
use Auth;
use App\Model\Price;
use App\Model\Currency;
use App\Model\Typeofser;
use App\Model\Period;
use App\Model\Taxtitle;
use App\employees\Fscemployee;
use App\Model\Product;
use Illuminate\Support\Facades\Input;
use App\Model\Accountcode;
class VendorssController extends Controller
{
public function __construct()
{
$this->middleware('auth');
}
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
public function index(Request $request)
{
$user_id  = Auth::user()->user_id;
$uid  = Auth::user()->id;
$employee = Employee::where('id',$user_id)->first();
$name = $request->input('user_type');
$category = Category::All(); 
$employee = Employee::where('type','=','Vendor')->where('userid','=',$user_id)->orderBy('firstName', 'asc')->get(); 
return view('client.clientvendor.clientvendor',compact(['common','employee','category']));
}
/**
* Show the form for creating a new resource.
*
* @return \Illuminate\Http\Response
*/
public function create()
{   
    $category = Category::All();   
    $user_id  = Auth::user()->user_id;
    $uid  = Auth::user()->id;
     $employee = Employee::where('id',$user_id)->first();
      $products1 = DB::table('products')->get();
    return view('client/clientvendor/create',compact(['products1','business','category','cb','businessbrand','currency','typeofser','period','taxtitle','employee']));
}
/**
* Store a newly created resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @return \Illuminate\Http\Response
*/
public function store(Request $request)
{
     $this->validate($request,[
    'business_catagory_name' =>'required',
    'business_name' =>'required|unique:employees',
    'productid' =>'required'
    ]);
      $c1 = $request->business_catagory_name;
        if(empty($c1))
        {
            $List1='';
        }
        else
        {
            $List1 = implode(', ', $c1); 
        }
        $c = $request->productid;
        $List = implode(', ', $c); 
        $user_id  = Auth::user()->user_id;
        $uid  = Auth::user()->id;
        $employee1 = Commonregister::where('id',$user_id)->first();
        $employee= new Employee;   
        $employee->vendortype = 'Client';
        $employee->employee_id = $request->employee_id;
        $employee->type = 'Vendor';
        $employee->userid = $user_id;
        $employee->types = $request->types;
        $employee->firstName= $request->firstname;
        $employee->middleName= $request->middlename;
        $employee->lastName= $request->lastname;
        $employee->address1= $request->address;
        $employee->address2= $request->address1;
        $employee->city= $request->city;
        $employee->stateId= $request->stateId;
        $employee->zip= $request->zip;
        $employee->countryId= $request->countryId;
        $employee->telephoneNo1= $request->telephone;
        $employee->telephoneNo2= $request->motelephoneile1;
        $employee->ext1= $request->ext1;
        $employee->ext2= $request->ext2;
        $employee->telephoneNo1Type= $request->telephoneNo1Type;
        $employee->telephoneNo2Type= $request->telephoneNo2Type;
        $employee->email= $request->email;
        $employee->nametype= $request->minss;
        $employee->fax= $request->business_fax;
        $employee->website= $request->website;
        $employee->etelephone1= $request->mobile_1;
        $employee->eteletype1= $request->mobiletype_1;
        $employee->eext1= $request->ext2_1;
        $employee->efax= $request->contact_fax;
        $employee->business_name= $request->business_name;
        $employee->product= $List;
        $employee->vendorid = $employee1->first_name.' '.$employee1->middle_name.' '.$employee1->last_name;
        $employee->vendortime= date('H:i');
        $employee->business_catagory_name= $List1;
        $employee->vendordate= date('Y-m-d H:i:s');
        $employee->save();
        return redirect('client/clientvendor')->with('success','You Are Registered Successfully');
    }

/**
* Display the specified resource.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function show($id)
{
$common = Commonregister::where('id',$id)->first();
return View::make('client.vendor.vendor',compact(['common']));
}
/**
* Show the form for editing the specified resource.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function edit($id)
{  
     $user_id  = Auth::user()->user_id;
     $uid  = Auth::user()->id;
     $employee = Employee::where('id',$user_id)->first();
     $emp = Employee::where('id',$id)->first();
     $category = Category::All();  
     $products1 = DB::table('products')->get();
     $accountcode = Accountcode::All(); 
     return view('client.clientvendor.edit',compact(['accountcode','products1','employee1','employee','emp','clientser','clientsertitle','note','every','fsc','taxstate','admin_notes','client','common','category','business','businessbrand','cb','user','info','info1','position','currency','period','typeofser','taxtitle']));
}
/**
* Update the specified resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function update(Request $request, $id)
{
 $this->validate($request,[
     'business_catagory_name' =>'required',
    'business_name' =>'required',
    'productid' =>'required'
    ]);
      $c1 = $request->business_catagory_name;
        if(empty($c1))
        {
            $List1='';
        }
        else
        {
            $List1 = implode(', ', $c1); 
        }
  $c = $request->productid;
     $List = implode(', ', $c); 
       $employee= Employee::find($id);  
       $employee->employee_id = $request->employee_id;
       // $employee->type = 'Vendor';
        $employee->firstName= $request->firstname;
        $employee->middleName= $request->middlename;
        $employee->lastName= $request->lastname;
        $employee->address1= $request->address;
        $employee->address2= $request->address1;
        $employee->city= $request->city;
        $employee->stateId= $request->stateId;
        $employee->zip= $request->zip;
        $employee->countryId= $request->countryId;
        $employee->telephoneNo1= $request->telephone;
        $employee->telephoneNo2= $request->motelephoneile1;
        $employee->ext1= $request->ext1;
        $employee->ext2= $request->ext2;
        $employee->telephoneNo1Type= $request->telephoneNo1Type;
        $employee->telephoneNo2Type= $request->telephoneNo2Type;
        $employee->email= $request->email;
        $employee->nametype= $request->minss;
        $employee->fax= $request->business_fax;
        $employee->website= $request->website;
        $employee->etelephone1= $request->mobile_1;
        $employee->eteletype1= $request->mobiletype_1;
        $employee->eext1= $request->ext2_1;
        $employee->efax= $request->contact_fax;
        $employee->business_name= $request->business_name;
        $employee->business_catagory_name= $List1;
        $employee->product= $List;
        $employee->update();
        return redirect('client/clientvendor')->with('success','Your Profile Update Successfully');
}
public function getcategory(Request $request)
{
$data = Category::select('business_cat_name','id')->where('bussiness_name',$request->id)->take(1000)->get();
return response()->json($data);  
}

public function locked2(Request $request)
{
    $lock=$request->id;
    $cid = $request->clientid;
    $data = DB::table('clientservices')->where('id',$cid)->update(['locked' => $lock]);
    return response()->json($data); 
//return 'success';
}


public function getcategory_1(Request $request)
{
$data = Category::select('business_cat_name','id','business_cat_image')->where('id',$request->id)->take(1000)->get();
return response()->json($data);  
}


public function getcategory1(Request $request)
{
$data = Business::select('bussiness_name','id','bussiness_image_name','newimage')->where('id',$request->id)->take(1000)->get();
return response()->json($data);  
}

public function getcategory1m(Request $request)
{
$data = BusinessBrand::select('business_brand_name','id')->where('business_cat_id',$request->id)->take(1000)->get();
return response()->json($data);  
}

public function getcategoryimage(Request $request)
{
$data = BusinessBrand::select('business_brand_name','id','business_brand_image')->where('id',$request->id)->take(1000)->get();
return response()->json($data);  
}

public function getcategory1mm(Request $request)
{
$data = Categorybusiness::select('business_brand_category_name','id')->where('business_brand_id',$request->id)->take(100)->get();
return response()->json($data);  
}

public function business_brand_category_name(Request $request)
{
$data = Categorybusiness::select('business_brand_category_name','id','business_brand_category_image')->where('id',$request->id)->take(100)->get();
return response()->json($data);  
}

public function salestax1(Request $request)
{
if($request->typeofservice=='3')
{
$data = DB::table('taxationservice')->select('regularprice1','id','comboprice1')->where('currency',$request->currency)->where('typeofservice',$request->typeofservice)->where('period',$request->serviceperiod)->where('title',$request->id)->take(100)->get();
}
else
{
$data = DB::table('prices')->select('regularprice','id','comboprice','serviceincludes')->where('currency',$request->currency)->where('typeofservice',$request->id)->where('period',$request->serviceperiod)->take(100)->get();
}
return response()->json($data);  
}


    public function deletecon( Request $request)
    { 
    $users =  DB::table('contact_userinfos')->where('id', '=',$request->input('id'))->delete();
      
       if($users)
        {
            echo 'Data Deleted';
        }
       //return redirect(route('customer.index'))->with('error','Your Record Deleted Successfully');
    }
   
  public function notedelete($id)
  {        
  // Contact_userinfo::where('id',$id)->delete(); 
   DB::table('notes')->where('id', '=',$id)->delete();
   return redirect('fac-Bhavesh-0554/vendor/');     
   }
   
    public function locksdelete($id)
  {        
  // Contact_userinfo::where('id',$id)->delete(); 
   DB::table('clientservices')->where('id', '=',$id)->delete();
   return redirect('fac-Bhavesh-0554/vendor/');     
   }
/**
* Remove the specified resource from storage.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function destroy($id)
{
Commonregister::where('id',$id)->delete();
$affectedRows = Contact_userinfo::where('user_id', '=', $id)->delete();
$user = User::where('user_id', '=', $id)->delete();
return redirect(route('clientvendor.index'))->with('error','Your Record Deleted Successfully');
}
}