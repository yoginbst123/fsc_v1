<?php
namespace App\Http\Controllers\Clients\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Foundation\Auth\AuthenticatesClient;
use App\Model\ClientEmployee;
use Auth;
class EmployeeClientLoginController extends Controller
{	
  /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesClient;
    
        /**
         * Where to redirect users after login.
         *
         * @var string
         */
       protected $redirectTo = 'clientemployee';
       
     public function __construct()
        {
            $this->middleware('guest:client')->except('logout');
        }
        /**
         * Create a new controller instance.
         *
         * @return void
         */
         public function showLoginForm1()
          {
          return view('login');
         }
    
     public function login1(Request $request)
     {
      $this->validate($request, [
        'email'   => 'required|email',
        'password' => 'required|min:6'
      ]);
      
    $user = ClientEmployee::where('email', $request->email)->first();
    if($user==Null)
    {
        return redirect()->back()->withInput($request->only('email', 'remember'))->with('error','Your Username Password  Not Registered!!!');
    }
    else
    {
    if($user->type==1)
    { 
      if(Auth::guard('client')->attempt(['email' => $request->email, 'password' => $request->password, 'type' =>1], $request->remember)) 
      {
          return redirect('clientemployee/')->with('success','Welcome Our Dashboard!!!');
       // return redirect()->back('clientemployee/')->with('success','Welcome Our Dashboard!!!');
      }
    }
    else if($user->type==0)
    {
        return redirect()->back()->withInput($request->only('email', 'remember'))->with('error','Your Account is Inactive pelase contact your admin!!!');
    }
      }
      return redirect()->back()->withInput($request->only('email', 'remember'))->with('error','Your Username Password Invalid!!!');
    }
    
    public function logout1(Request $request)
        {
        $this->guard('client')->logout();
        $request->session()->invalidate();
        return redirect('/login');
       }
}