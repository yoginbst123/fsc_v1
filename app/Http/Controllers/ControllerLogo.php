<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
use DB;
use App\AddLogo;

class ControllerLogo extends Controller
{
/**

     * Show the application ajaxImageUpload.

     *

     * @return \Illuminate\Http\Response

     */
    public function logo()
    { 
        return view('logo');
        
        
    }
        /**

     * Show the application ajaxImageUploadPost.

     *

     * @return \Illuminate\Http\Response

     */
    public function AddLogoPost(Request $request)

    {

      $validator = Validator::make($request->all(), [

        'logo_text' => 'required',
         'logo_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
       

      ]);
           if ($validator->passes()) {


        $input = $request->all();
        $input['logo_image'] = time().'.'.$request->logo_image->getClientOriginalExtension();

        $request->logo_image->move(public_path('logo1'), $input['logo_image']);
        AddLogo::create($input);
        return response()->json(['success'=>'done']);

      }
      return response()->json(['error'=>$validator->errors()->all()]);

    }
    
    
     public function editlogo($id)
    { 
     return view('editlogo', compact('id'));
    }
    
     public function update($id)
    {
        $input = request(['name']);

        $level->fill($input)->save();

        return redirect('logo');
    }
public function destroy($id) {
      DB::delete('delete from add_logos where id = ?',[$id]);
      echo "Record deleted successfully.<br/>";
      echo '<a href="http://localhost/mahendra/public/logo">Click Here</a> to go back.';
   }
}
