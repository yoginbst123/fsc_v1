<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Model\Home;
use App\Model\Business;
use App\Model\BusinessBrand;
use App\Model\Category;
use App\Front\Commonregister;
use App\User;
use App\Model\Admin;
use App\Model\Task;
use App\Model\Msg;
use App\Model\Service;
use App\Model\Categorybusiness;
use App\employee\Empschedule;
use App\Model\Employee;
use App\Model\Schedule;
use App\Model\Schedulesetup;
use App\employees\Fscemployee;
use App\Model\Position;
use App\Model\Branch;
use App\Model\Proposal_client_details;
use App\Model\Proposal_client_detail_services;
use App\Model\Servicetype;

use App\Model\Typeofser;
use App\Model\Period;
use App\Model\Taxtitle;

use DB;
use Carbon\Carbon;
use App\Model\Document;
use Illuminate\Support\Facades\Input;

use App\Model\Adminupload;
use App\Model\Clientnupload;
use App\Front\StoreContact;
use App\Model\Submission;
use App\Model\Relatednames;
use App\Model\Notesrelated;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    public function index()
    {
        //$listclient = DB::table('commonregisters')->where('first_name', '!=', '')->where('last_name', '!=', '')->orderBy('first_name', 'ASC')->get();


        //$data = Commonregister::select('firstname','id','middlename','lastname','business_no','status','filename','business_name','company_name')->where('status','Active')->orWhere('id', $request->id)->get();
        $listclient = DB::table('commonregisters')
            ->select(
                'commonregisters.type as Type',
                'commonregisters.id as cid',
                'commonregisters.filename as entityid',
                'commonregisters.first_name as fname',
                'commonregisters.middle_name as mname',
                'commonregisters.last_name as lname',
                'commonregisters.company_name as cname',
                'commonregisters.business_name',
                'commonregisters.business_id',
                'commonregisters.business_no',
                'commonregisters.status')
            ->where('status', 'Active')->get();
        //$listclient = array();
        // foreach ($commonregiser as $row) {
        //     if ($row->status == '1'  || $row->status == 'Active') {
        //         if ($row->business_id == '6') {
        //             $listclient[] = array(
        //                 'id'=>$row->cid,
        //                 'filename'=>$row->entityid,
        //                 'company_name'=>$row->fname . ' ' . $row->mname . ' ' . $row->lname,
        //                 'business_no'=>$row->business_no
        //             );
        //         } else {
        //             $listclient[] = array(
        //                 'id'=>$row->cid,
        //                 'filename'=>$row->entityid,
        //                 'company_name'=>$row->cname,
        //                 'business_no'=>$row->business_no
        //             );
        //         }
        //     }
        // }


        //$listvendoe = DB::table('employees')->where('type', 'Vendor')->where('firstName', '!=', '')->where('lastName', '!=', '')->orderBy('firstName', 'ASC')->get();
        $listvendoe = DB::table('employees')->where('type', 'Vendor')->orderBy('business_name', 'ASC')->get();
        $listemployeeuser = DB::table('employees')->where('type','!=' ,'vendor')->where('check','1')->orderBy('firstName', 'ASC')->get();
        $relatedNames = DB::table('relatednames')->get();
        $relatedNames1 = DB::table('relatednames')->get();
        $notesRelated = DB::table('notesrelateds')->get();
        $notesRelated1 = DB::table('notesrelateds')->get();

        $schedulesetup = DB::table('schedulesetups')->where('type', 'employee')->where('duration', '=', 'Bi-weekly')->first();


        $submission = Submission::All();
        $scheduledate = Schedule::get();
        foreach ($scheduledate as $a) {
            $sch = DB::table('schedule_emp_dates')->where('emp_sch_id', $a->id)->orderBy('id', 'DESC')->get();
            //return $sch11 = DB::table('schedule_emp_dates')->where('emp_sch_id', $a->id)->where('schedule_status11', 2)->get();
            $duration = $a->duration;
            $date_from = $a->sch_end_date;
            $currdate = date('m/d/Y');
            $date_from1 = strtotime($date_from);
            $cdate = strtotime($currdate);
            if ($cdate > $date_from1) {
                if ($duration == 'Bi-Weekly') {
                    //DB::table('schedule_emp_dates')->where('emp_sch_id', $a->id)->delete();
                    $date = date("m/d/Y");
                    $date2 = date("l");
                    $date1 = date("m/d/Y", strtotime("+13 day", strtotime($date)));
                    $date3 = date("l", strtotime("+13 day", strtotime($date)));
                    $date4 = date("m/d/Y", strtotime("+17 day", strtotime($date)));
                    $date5 = date("l", strtotime("+17 day", strtotime($date)));
                    $up = DB::table('schedulesetups')->where('duration', '=', 'Bi-Weekly')
                        ->update([
                            'sch_start_date' => $date,
                            'sch_end_date' => $date1,
                            'sch_end_day' => $date3,
                            'sch_start_day' => $date2,
                            'sch_pay_date' => $date4,
                            'sch_pay_day' => $date5,
                        ]);
                    $up = DB::table('schedules')->where('id', '=', $a->id)
                        ->update([
                            'sch_start_date' => $date,
                            'sch_end_date' => $date1,
                            'sch_start_day' => $date2,
                            'sch_end_day' => $date3,
                        ]);
                    $date = strtotime($date);
                    $date1 = strtotime($date1);
                    for ($i = $date; $i <= $date1; $i += 86400) {
                        $day = date("D", $i);
                        if ($day == 'Sun') {
                            $insert2 = DB::insert("insert into schedule_emp_dates(`emp_sch_id`,`date_1`,`clockin`,`clockout`) values('" . $a->id . "','" . $i . "','','')");
                        } else if ($day == 'Sat') {
                            $insert2 = DB::insert("insert into schedule_emp_dates(`emp_sch_id`,`date_1`,`clockin`,`clockout`) values('" . $a->id . "','" . $i . "','','')");
                        } else {
                            $insert2 = DB::insert("insert into schedule_emp_dates(`emp_sch_id`,`date_1`,`clockin`,`clockout`) values('" . $a->id . "','" . $i . "','" . $a->schedule_in_time . "','" . $a->schedule_out_time . "')");
                        }
                    }
                }
                if ($a->duration == 'Weekly') {
                    $date = date("m/d/Y");
                    $date2 = date("l");
                    $date1 = date("m/d/Y", strtotime("+6 day", strtotime($date)));
                    $date3 = date("l", strtotime("+6 day", strtotime($date)));
                    $date4 = date("m/d/Y", strtotime("+10 day", strtotime($date)));
                    $date5 = date("l", strtotime("+10 day", strtotime($date)));

                    $up = DB::table('schedulesetups')->where('duration', '=', 'Weekly')
                        ->update([
                            'sch_start_date' => $date,
                            'sch_end_date' => $date1,
                            'sch_end_day' => $date3,
                            'sch_start_day' => $date2,
                            'sch_pay_date' => $date4,
                            'sch_pay_day' => $date5,
                        ]);
                    $up = DB::table('schedules')->where('id', '=', $a->id)
                        ->update([
                            'sch_start_date' => $date,
                            'sch_end_date' => $date1,
                            'sch_start_day' => $date2,
                            'sch_end_day' => $date3,
                        ]);
                    $date = strtotime($date);
                    $date1 = strtotime($date1);
                    for ($i = $date; $i <= $date1; $i += 86400) {
                        $day = date("D", $i); //print_r($i);exit;

                        if ($day == 'Sun') {
                            $insert2 = DB::insert("insert into schedule_emp_dates(`emp_sch_id`,`date_1`,`clockin`,`clockout`) values('" . $a->id . "','" . $i . "','','')");
                        } else if ($day == 'Sat') {

                            $insert2 = DB::insert("insert into schedule_emp_dates(`emp_sch_id`,`date_1`,`clockin`,`clockout`) values('" . $a->id . "','" . $i . "','','')");
                        } else {
                            $insert2 = DB::insert("insert into schedule_emp_dates(`emp_sch_id`,`date_1`,`clockin`,`clockout`) values('" . $a->id . "','" . $i . "','" . $a->schedule_in_time . "','" . $a->schedule_out_time . "')");
                        }
                    }
                } else if ($a->duration == 'Monthly') {
                    $month = date("M");
                    if ($month == 'Jan' || $month == 'Mar' || $month == 'May' || $month == 'Jul' || $month == 'Aug' || $month == 'Oct' || $month == 'Dec') {
                        $totaldays = 30;
                        $date = date("m/d/Y");
                        $date2 = date("l");
                        $date1 = date("m/d/Y", strtotime("+30 day", strtotime($date)));
                        $date3 = date("l", strtotime("+30 day", strtotime($date)));
                        $date4 = date("m/d/Y", strtotime("+35 day", strtotime($date)));
                        $date5 = date("l", strtotime("+35 day", strtotime($date)));
                    } else if ($month == 'Feb') {
                        $totaldays = 27;
                        $date = date("m/d/Y");
                        $date2 = date("l");
                        $date1 = date("m/d/Y", strtotime("+27 day", strtotime($date)));
                        $date3 = date("l", strtotime("+27 day", strtotime($date)));
                        $date4 = date("m/d/Y", strtotime("+27 day", strtotime($date)));
                        $date5 = date("l", strtotime("+27 day", strtotime($date)));
                    } else if ($month == 'Apr' || $month == 'Jun' || $month == 'Sep' || $month == 'Nov') {
                        $totaldays = 29;
                        $date = date("m/d/Y");
                        $date2 = date("l");
                        $date1 = date("m/d/Y", strtotime("+29 day", strtotime($date)));
                        $date3 = date("l", strtotime("+29 day", strtotime($date)));
                        $date4 = date("m/d/Y", strtotime("+29 day", strtotime($date)));
                        $date5 = date("l", strtotime("+29 day", strtotime($date)));
                    }
                    $up = DB::table('schedulesetups')->where('duration', '=', 'Monthly')
                        ->update([
                            'sch_start_date' => $date,
                            'sch_end_date' => $date1,
                            'sch_end_day' => $date3,
                            'sch_start_day' => $date2,
                            'sch_pay_date' => $date4,
                            'sch_pay_day' => $date5,
                        ]);
                    $up = DB::table('schedules')->where('id', '=', $a->id)
                        ->update([
                            'sch_start_date' => $date,
                            'sch_end_date' => $date1,
                            'sch_start_day' => $date2,
                            'sch_end_day' => $date3,
                        ]);
                    $date = strtotime($date);
                    $date1 = strtotime($date1);
                    for ($i = $date; $i <= $date1; $i += 86400) {
                        $day = date("D", $i);
                        if ($day == 'Sun') {
                            $insert2 = DB::insert("insert into schedule_emp_dates(`emp_sch_id`,`date_1`,`clockin`,`clockout`) values('" . $a->id . "','" . $i . "','','')");
                        } else if ($day == 'Sat') {
                            $insert2 = DB::insert("insert into schedule_emp_dates(`emp_sch_id`,`date_1`,`clockin`,`clockout`) values('" . $a->id . "','" . $i . "','','')");
                        } else {
                            $insert2 = DB::insert("insert into schedule_emp_dates(`emp_sch_id`,`date_1`,`clockin`,`clockout`) values('" . $a->id . "','" . $i . "','" . $a->schedule_in_time . "','" . $a->schedule_out_time . "')");
                        }
                    }
                } else if ($a->duration == 'Semi-Monthly') {
                    $date = date("m/d/Y");
                    $date2 = date("l");
                    $date1 = date("m/d/Y", strtotime("+14 day", strtotime($date)));
                    $date3 = date("l", strtotime("+14 day", strtotime($date)));
                    $date4 = date("m/d/Y", strtotime("+18 day", strtotime($date)));
                    $date5 = date("l", strtotime("+18 day", strtotime($date)));
                    $up = DB::table('schedulesetups')->where('duration', '=', 'Semi-Monthly')
                        ->update([
                            'sch_start_date' => $date,
                            'sch_end_date' => $date1,
                            'sch_end_day' => $date3,
                            'sch_start_day' => $date2,
                            'sch_pay_date' => $date4,
                            'sch_pay_day' => $date5,
                        ]);
                    $up = DB::table('schedules')->where('id', '=', $a->id)
                        ->update([
                            'sch_start_date' => $date,
                            'sch_end_date' => $date1,
                            'sch_start_day' => $date2,
                            'sch_end_day' => $date3,
                        ]);
                    $date = strtotime($date);
                    $date1 = strtotime($date1);
                    for ($i = $date; $i <= $date1; $i += 86400) {
                        $day = date("D", $i);
                        if ($day == 'Sun') {
                            $insert2 = DB::insert("insert into schedule_emp_dates(`emp_sch_id`,`date_1`,`clockin`,`clockout`) values('" . $a->id . "','" . $i . "','','')");
                        } else if ($day == 'Sat') {
                            $insert2 = DB::insert("insert into schedule_emp_dates(`emp_sch_id`,`date_1`,`clockin`,`clockout`) values('" . $a->id . "','" . $i . "','','')");
                        } else {
                            $insert2 = DB::insert("insert into schedule_emp_dates(`emp_sch_id`,`date_1`,`clockin`,`clockout`) values('" . $a->id . "','" . $i . "','" . $a->schedule_in_time . "','" . $a->schedule_out_time . "')");
                        }
                    }
                }
            }
        }
        $business = Business::All();
        $admin = Admin::All();
        $category = Category::All();
        $businessbrand = BusinessBrand::All();
        $commonregister = Commonregister::All();
        //$commonregister1 = Commonregister::where('status','=','Approval')->get();

        $commonregister1 = Commonregister::where('filename', '!=', NULL)->where('status', '=', 'Active')->get();

        $cetegorybusiness = Categorybusiness::All();
        $empfsc = Fscemployee::All();
        $msg1 = Msg::where('send', '=', 1)->get();
        $silver = DB::table("commonregisters")
            ->select(
                "commonregisters.first_name",
                "commonregisters.middle_name",
                "commonregisters.last_name",
                "commonregisters.address",
                "commonregisters.city",
                "commonregisters.stateId",
                "commonregisters.countryId",
                "commonregisters.business_no",
                "commonregisters.email",
                "commonregisters.status",
                "commonregisters.id"
            )->where('commonregisters.status', '=', "Approval");
        $akk = DB::table("employees")->select(
            "employees.firstName",
            "employees.middleName",
            "employees.lastName",
            "employees.address1",
            "employees.city",
            "employees.stateId",
            "employees.countryId",
            "employees.telephoneNo1",
            "employees.email",
            "employees.type",
            "employees.id"
        )->where('employees.check', '=', "1")->unionAll($silver)->get();


        $monday = date('Y-m-d', strtotime('Monday this week'));
        $friday = date('Y-m-d', strtotime('Friday this week'));
        $current_time = Carbon::now()->toDateTimeString();
        $employee = DB::Table('empschedules')->whereBetween('emp_in_date', array($monday, $friday))->where('emp_in_date', '=', date('Y-m-d'))->selectRaw('emp_out,launch_in,launch_out,emp_in,ip_address,note,emp_in_date,employee_id,fsccity,launch_out_second,launch_in_second')->get();
        $schedule = Schedule::groupBy('emp_city')->selectRaw('emp_city,GROUP_CONCAT(emp_name) as emp_name')->get();
        $set1 = DB::Table('empschedules')->where('emp_in_date', '=', date('Y-m-d'))->get();
        $set = Employee::groupBy('branch_city')->selectRaw('branch_city')->get();
        $set1 = Employee::All();
        $user = User::All();
        $task = Task::All();
        // $taskall = Task::where('status', '!=',0)->where('checked', '=',0)->orderBy('created_at','desc')->get();
        $taskall = Task::where('status', '!=', 0)->where('status', '!=', 3)->orderBy('created_at', 'desc')->get();


        $scheduleemployee = DB::table('employees')->select(
            'employees.id',
            'employees.employee_id',
            'employees.firstName',
            'employees.middleName',
            'employees.lastName',
            'employees.status',
            'schedules.type',
            'schedules.emp_name',
            'schedules.schedule_in_time',
            'schedules.schedule_out_time'
        )
            ->Join('schedules', function ($join) {
                $join->on('schedules.emp_name', '=', 'employees.id');
            })
            ->where('employees.status', '=', '1')->where('employees.type', '=', 'employee')->groupBy('employees.id')->orderBy('employees.firstName', 'asc')->get();

        //$msg = DB::table('msgs')->select('status', DB::raw('count(*) as msgtotal'))->where('status','=',2)->groupBy('status')->get();

        $msg = DB::table('msgs')->select('checkstatus', DB::raw('count(*) as msgtotal'))->where('send', '=', '1')->where('checkstatus', '=', '1')->groupBy('checkstatus')->get();
        //print_r($msg);die;

        $mondaystr = date('Y-m-d', strtotime('Monday this week'));
        $sundaystr = date('Y-m-d', strtotime('Friday this week'));
        $mondaystr1 = strtotime($mondaystr);
        $sundaystr1 = strtotime($sundaystr);

        $scheduledates = DB::table('schedules')->select(
            'schedules.id',
            'schedule_emp_dates.emp_sch_id',
            'schedule_emp_dates.date_1',
            'schedule_emp_dates.clockin',
            'schedule_emp_dates.clockout',
            'employees.id as empid',
            'employees.status as statuss',
            'employees.employee_id'
        )
            ->Join('schedule_emp_dates', function ($join) {
                $join->on('schedules.id', '=', 'schedule_emp_dates.emp_sch_id');
            })
            ->Join('employees', function ($join) {
                $join->on('employees.id', '=', 'schedules.emp_name');
            })
            ->where('employees.status', '=', '1')->whereBetween('schedule_emp_dates.date_1', array($mondaystr1, $sundaystr1))
            ->get();


        $ss1 = DB::table('msgs')->select('send', DB::raw('count(*) as msgtotal'))->where('send', '=', 1)->groupBy('send')->count();
        $msgc = DB::table('msgs')->select('status', DB::raw('count(*) as msgtotal'))->where('status', '=', 1)->where('status1', '=', 'Done')->groupBy('status')->count();
        $msgc1 = DB::table('msgs')->select('status', DB::raw('count(*) as msgtotal'))->where('status', '=', 1)->where('status1', '=', 'Forword')->groupBy('status')->count();
        $msgdone = Msg::where('status1', '=', 'Done')->where('send', '=', 1)->count();
        $msgforwork = Msg::where('status1', '=', 'Forword')->where('send', '=', 1)->count();
        $taxtitles = DB::table('taxtitles')->orderBy('title', '=', 'asc')->get();
        $clientttaxation = DB::table('client_to_taxation')->groupBy('taxation_service')->get();


        $msg2 = Msg::where('send', '=', 1)->where('status', '=', 2)->where('status1', '=', '')->count();
        $newclient = DB::table('commonregisters')->select('newclient', DB::raw('count(*) as total'))->where('newclient', '=', '0')->groupBy('newclient')->count();
        $newclientall = DB::table('commonregisters')->select('newclient', DB::raw('count(*) as total'))->where('newclient', '=', '0')->groupBy('newclient')->count();
        $common = DB::table('commonregisters')->select('newclient', DB::raw('count(*) as total'))->where('status', '=', 'Active')->groupBy('newclient')->get()->first();
        $employeeusers = DB::table('employees')->select('count', DB::raw('count(*) as total'))->where('check', '=', '1')->where('type', '=', 'user')->groupBy('count')->get()->first();
        $employees = DB::table('employees')->select('count', DB::raw('count(*) as total'))->where('check', '=', '1')->where('type', '=', 'employee')->groupBy('count')->get()->first();
        $employee1 = DB::table('employees')->select('count', DB::raw('count(*) as total'))->where('type', '=', 'employee')->groupBy('count')->get()->first();

        $clientemployee = DB::table('employees')->select('count', DB::raw('count(*) as total'))->where('type', '=', 'clientemployee')->where('count', '=', '1')->groupBy('count')->get()->first();
        $newemp1 = DB::table('employees')->select('newemp', DB::raw('count(*) as total'))->where('newemp', '=', '1')->where('type', '=', 'clientemployee')->groupBy('newemp')->get()->first();

        $newemp = DB::table('employees')->select('newemp', DB::raw('count(*) as total'))->where('newemp', '=', '1')->where('type', '=', 'employee')->groupBy('newemp')->get()->first();
        $common1 = DB::table('commonregisters')->select('user_type', DB::raw('count(*) as total'))->where('user_type', '=', 'Business')->groupBy('user_type')->get()->first();
        $common2 = DB::table('commonregisters')->select('user_type', DB::raw('count(*) as total'))->where('user_type', '=', 'Non-Profit Organization')->groupBy('user_type')->get()->first();
        $common3 = DB::table('commonregisters')->select('user_type', DB::raw('count(*) as total'))->where('user_type', '=', 'Service Industry')->groupBy('user_type')->get()->first();
        $common4 = DB::table('commonregisters')->select('user_type', DB::raw('count(*) as total'))->where('user_type', '=', 'Profession')->groupBy('user_type')->get()->first();
        $common5 = DB::table('commonregisters')->select('user_type', DB::raw('count(*) as total'))->where('user_type', '=', 'Investor')->groupBy('user_type')->get()->first();
        $common6 = DB::table('commonregisters')->select('user_type', DB::raw('count(*) as total'))->where('user_type', '=', 'Personal')->groupBy('user_type')->get()->first();
        $employments = DB::table('employments')->select('count', DB::raw('count(*) as total'))->where('count', '=', '1')->groupBy('count')->get()->first();
        $application = DB::table('apply_employments')->select('status', DB::raw('count(*) as total'))->where('status', '=', '3')->groupBy('status')->get()->first();
        $candidate = DB::table('apply_employments')->select('status', DB::raw('count(*) as total'))->where('status', '=', '1')->groupBy('status')->get()->first();
        $emp = DB::table('apply_employments')->select('status', DB::raw('count(*) as total'))->where('status', '=', '2')->groupBy('status')->get()->first();

        //11111
        $prolic = DB::table("admin_professionals as t1")->select("t1.id", "t2.id as ids", "t1.profession_exp_date2 as expiredate", "t1.profession_license_copy as documentcopy", "t1.profession_check as status")
            ->Join('admins as t2', function ($join) {
                $join->on('t2.id', '=', 't1.admin_id');
            });

        $silver = DB::table("admin_license as t1")->select("t1.id", "t2.id as ids", "t1.bus_license_exp_date as expiredate", "t1.license_copy as documentcopy", "t1.license_check as status")
            ->Join('admins as t2', function ($join) {
                $join->on('t2.id', '=', 't1.admin_id');
            });

        $adminupload = DB::table("admin_formation as t1")->select("t1.id", "t2.id as ids", "t2.agent_expiredate as expiredate", "t1.annualreceipt as documentcopy", "t1.formation_check as status")
            ->Join('admins as t2', function ($join) {
                $join->on('t2.id', '=', 't1.admin_id');
            })
            ->groupBy('t1.id')
            ->union($silver)->union($prolic)->get();
        // echo "<pre>";
        // print_r($adminupload);die;


        //22222
        // $prolic1 = DB::table("admin_professionals as t1")->select("t2.id","t1.profession_exp_date2 as expiredate","t1.profession_license_copy as documentcopy","t1.profession_check as status")
        //         ->Join('admins as t2', function($join){ $join->on('t2.id', '=', 't1.admin_id');});

        // $silver1 = DB::table("admin_license as t1")->select("t2.id","t1.bus_license_exp_date as expiredate","t1.license_copy as documentcopy","t1.license_check as status")
        //         ->Join('admins as t2', function($join){ $join->on('t2.id', '=', 't1.admin_id');})
        //         ->where('t1.bus_license_exp_date','=','Mar-22-2020');

        // $adminupload1 = DB::table("admin_formation as t1")->select("t2.id","t2.agent_expiredate as expiredate","t1.annualreceipt as documentcopy","t1.formation_check as status")
        //                 ->Join('admins as t2', function($join){ $join->on('t2.id', '=', 't1.admin_id');})
        //                 ->union($silver)->union($prolic1)->where('t2.agent_expiredate','=','Mar-23-2020')->get();

        $prolic1 = DB::table("admin_professionals as t1")->select("t1.id", "t2.id as ids", "t1.profession_exp_date2 as expiredate", "t1.profession_license_copy as documentcopy", "t1.profession_check as status")
            ->Join('admins as t2', function ($join) {
                $join->on('t2.id', '=', 't1.admin_id');
            });

        $silver1 = DB::table("admin_license as t1")->select("t1.id", "t2.id as ids", "t1.bus_license_exp_date as expiredate", "t1.license_copy as documentcopy", "t1.license_check as status")
            ->Join('admins as t2', function ($join) {
                $join->on('t2.id', '=', 't1.admin_id');
            });

        $adminupload1 = DB::table("admin_formation as t1")->select("t1.id", "t2.id as ids", "t2.agent_expiredate as expiredate", "t1.annualreceipt as documentcopy", "t1.formation_check as status")
            ->Join('admins as t2', function ($join) {
                $join->on('t2.id', '=', 't1.admin_id');
            })
            ->groupBy('t1.id')
            ->union($silver)->union($prolic1)->get();

        //echo "<pre>";
        //print_r($adminupload1);die;

        //333333
        // $prolic2 = DB::table("admin_professionals as t1")->select("t2.id","t1.profession_exp_date2 as expiredate","t1.profession_license_copy as documentcopy","t1.profession_check as status")
        //         ->Join('admins as t2', function($join){ $join->on('t2.id', '=', 't1.admin_id');});

        // $silver2 = DB::table("admin_license as t1")->select("t2.id","t1.bus_license_exp_date as expiredate","t1.license_copy as documentcopy","t1.license_check as status")
        //         ->Join('admins as t2', function($join){ $join->on('t2.id', '=', 't1.admin_id');})
        //         ->where('t1.bus_license_exp_date','=','Mar-31-2020');

        // $adminupload2 = DB::table("admin_formation as t1")->select("t2.id","t2.agent_expiredate as expiredate","t1.annualreceipt as documentcopy","t1.formation_check as status")
        //                 ->Join('admins as t2', function($join){ $join->on('t2.id', '=', 't1.admin_id');})
        //                 ->union($silver2)->union($prolic2)->where('t2.agent_expiredate','=','Apr-01-2020')->get();

        $prolic2 = DB::table("admin_professionals as t1")->select(
            "t1.id",
            "t2.id as ids",
            "t1.profession_exp_date2 as expiredate",
            "t1.profession_license_copy as documentcopy",
            "t1.profession_check as status",
            "t1.filename"
        )
            ->Join('admins as t2', function ($join) {
                $join->on('t2.id', '=', 't1.admin_id');
            });

        $silver2 = DB::table("admin_license as t1")->select(
            "t1.id",
            "t2.id as ids",
            "t1.bus_license_exp_date as expiredate",
            "t1.license_copy as documentcopy",
            "t1.license_check as status",
            "t1.filename"
        )
            ->Join('admins as t2', function ($join) {
                $join->on('t2.id', '=', 't1.admin_id');
            });

        $commonone = DB::table("client_to_taxation as t1")->select("t1.id", "t2.id as ids", "t1.expiredate as expiredate", "documentcopy", "t1.taxation_service as status", "t2.filename as filename")
            ->Join('commonregisters as t2', function ($join) {
                $join->on('t2.id', '=', 't1.clientid');
            });


        $adminupload2 = DB::table("admin_formation as t1")->select("t1.id", "t2.id as ids", "t2.agent_expiredate as expiredate", "t1.annualreceipt as documentcopy", "t1.formation_check as status", "t1.filename as filename")
            ->Join('admins as t2', function ($join) {
                $join->on('t2.id', '=', 't1.admin_id');
            })
            ->groupBy('t1.id')
            ->union($silver2)->union($prolic2)->union($commonone)->get();

        $commonone1 = DB::table("client_to_taxation as t1")->select("t4.federalstax", "t4.flag as flags", "t1.flag", "t2.due_date_2", "t2.filename as filename", "t2.type_form1", "t2.type_form", "t1.taxyears", "t1.clientid as ccid", "t2.personalstatus", "t4.federalsduedate", "t4.federalsduedate", "t4.id as tids", "t4.client_id as tcid", "t3.firstName", "t3.middleName", "t3.lastName", "t3.employee_id", "t2.company_name", "t2.federalstax", "t2.first_name", "t2.middle_name", "t2.last_name", "t2.status as status1", "t2.business_id", "t1.id", "t2.id as ids", "t1.expiredate as expiredate", "documentcopy", "t1.taxation_service as status")
            ->leftJoin('commonregisters as t2', function ($join) {
                $join->on('t2.id', '=', 't1.clientid');
            })
            ->leftJoin('employees as t3', function ($join) {
                $join->on('t3.id', '=', 't1.employee_id');
            })
            ->leftJoin('client_taxfederal as t4', function ($join) {
                $join->on('t4.client_id', '=', 't1.clientid');
            })
            ->where('t1.flag', '=', '0')->where('t2.status', '=', 'Active')->where('t2.type_form', '!=', '')->orwhere('t2.type_form1', '!=', '')->groupBy('t1.id')->orderBy('t4.federalsduedate', 'asc')->orderBy('t1.expiredate', 'asc')->orderBy('t2.filename', 'asc')->get();
//echo 's'.count($commonone1);
        $Federaltaxations = DB::table("client_taxfederal")->where('federalstax', '=', 'Original')->get();
        // echo count($Federaltaxations);
        $arrfed = array();
        foreach ($Federaltaxations as $fed) {
            $arrfed[] = $fed->client_id;

        }
        $c2 = DB::table("client_to_taxation as t1")->select("t1.clientid as ccid", "t2.personalstatus", "t4.id as tids", "t4.client_id as tcid", "t3.firstName", "t3.middleName", "t3.lastName", "t3.employee_id", "t2.company_name", "t2.federalstax", "t2.first_name", "t2.middle_name", "t2.last_name", "t2.status as status1", "t2.business_id", "t1.id", "t2.id as ids", "t1.expiredate as expiredate", "documentcopy", "t1.taxation_service as status", "t2.filename as filename")
            ->leftJoin('commonregisters as t2', function ($join) {
                $join->on('t2.id', '=', 't1.clientid');
            })
            ->leftJoin('employees as t3', function ($join) {
                $join->on('t3.id', '=', 't1.employee_id');
            })
            ->leftJoin('client_taxfederal as t4', function ($join) {
                $join->on('t4.client_id', '=', 't1.clientid');
            })
            ->where('t4.federalstax', '!=', 'Original')->orWhereNull('t4.federalstax')->where('t2.status', '=', 'Active')->where('t4.federalstax', '!=', 'Original')->groupBy('t2.id')->orderBy('t1.expiredate', 'asc')->orderBy('t2.filename', 'asc')->get();

        $clientoriginal = DB::table("client_to_taxation")->select("client_to_taxation.*", "client_taxfederal.client_id", "client_taxfederal.federalstax", "client_taxfederal.federalsyear")->
        Join('client_taxfederal', function ($join) {
            $join->on('client_to_taxation.clientid', '=', 'client_taxfederal.client_id');
        })->where('client_to_taxation.taxation_service', '=', '6')->where('client_to_taxation.flag', '=', '0')->get();
//echo 'zz'.count($clientoriginal);
        /* $commonone33 = DB::table("client_to_taxation as t1")->select("select temp1.clientid,temp1.federalstax from

    (SELECT count(*),clientid,federalstax from

     (select clientid,federalstax from client_to_taxation c

      INNER JOIN client_taxfederal c1 on c1.client_id=c.clientid")->
      whereIn('federalstax', array('Original', 'Extension'))
      ->groupBy("clientid")
    ->havingRaw("COUNT(*) = 1") as temp1 where('federalstax','!=','Original')
             */

        $servicetype = DB::table('applyservicesses')->select('typeofcorp', DB::raw('count(*) as total'))->where('typeofcorp', '=', 'Accounting Bookkeeping Taxation Service')->where('ser_check', '!=', 0)->groupBy('typeofcorp')->get()->first();
        if (empty($servicetype->typeofcorp)) {
        } else {
            $k1 = $servicetype->typeofcorp;
            $s1 = DB::Table('applyservicesses')->where('typeofcorp', '=', $k1)->where('sign', '=', 1)->get()->first();
        }
        $servicetype2 = DB::table('applyservicesses')->select('typeofcorp', DB::raw('count(*) as total'))->where('typeofcorp', '=', 'Residential Mortgage Services')->where('ser_check', '!=', 0)->groupBy('typeofcorp')->get()->first();
        if (empty($servicetype2->typeofcorp)) {
        } else {
            $k11 = $servicetype2->typeofcorp;
            $s11 = DB::Table('applyservicesses')->where('typeofcorp', '=', $k11)->where('sign', '=', 1)->get()->first();
        }
        $servicetype3 = DB::table('applyservicesses')->select('typeofcorp', DB::raw('count(*) as total'))->where('typeofcorp', '=', 'Commercial Mortgage Services')->where('ser_check', '!=', 0)->groupBy('typeofcorp')->get()->first();
        if (empty($servicetype3->typeofcorp)) {
        } else {
            $k12 = $servicetype3->typeofcorp;
            $s12 = DB::Table('applyservicesses')->where('typeofcorp', '=', $k12)->where('sign', '=', 1)->get()->first();
        }

        $servicetype4 = DB::table('applyservicesses')->select('typeofcorp', DB::raw('count(*) as total'))->where('typeofcorp', '=', 'Financial Services')->groupBy('typeofcorp')->get()->first();
        if (empty($servicetype4->typeofcorp)) {
        } else {
            $k14 = $servicetype4->typeofcorp;
            $s13 = DB::Table('applyservicesses')->where('typeofcorp', '=', $k14)->where('sign', '=', 1)->get()->first();
        }
        $servicetype5 = DB::table('applyservicesses')->select('typeofcorp', DB::raw('count(*) as total'))->where('typeofcorp', '=', 'Insurance Service')->where('ser_check', '!=', 0)->groupBy('typeofcorp')->get()->first();
        if (empty($servicetype5->typeofcorp)) {
        } else {
            $k2 = $servicetype5->typeofcorp;
            $s2 = DB::Table('applyservicesses')->where('typeofcorp', '=', $k2)->where('sign', '=', 1)->get()->first();
        }
        $silver = DB::table("commonregisters")
            ->select(
                "commonregisters.business_no",
                "commonregisters.first_name",
                "commonregisters.middle_name",
                "commonregisters.last_name",
                "commonregisters.address",
                "commonregisters.city",
                "commonregisters.stateId",
                "commonregisters.countryId",
                "commonregisters.business_no",
                "commonregisters.email",
                "commonregisters.status",
                "commonregisters.id"
            )->where('commonregisters.status', '=', "Approval");
        $emp1 = DB::table("employees")->select(
            "employees.telephoneNo1",
            "employees.firstName",
            "employees.middleName",
            "employees.lastName",
            "employees.address1",
            "employees.city",
            "employees.stateId",
            "employees.countryId",
            "employees.telephoneNo1",
            "employees.email",
            "employees.type",
            "employees.id"
        )->where('employees.check', '=', "1")->unionAll($silver)->get();
        $emp = Fscemployee::All();
        $contact = StoreContact::where('verifycheck', '=', '1')->count();
        $contact1 = StoreContact::where('status', '=', '1')->where('verifycheck', '=', '1')->first();

        $vendor = Employee::where('type', '=', 'Vendor')->get();

        $vendor1 = Employee::where('type', '=', 'Vendor')->where('newemp', '=', '1')->first();

        $countrecstatus = Commonregister::where('recstatus', '=', '1')->count();
        $datastate = DB::table('state')->where('countrycode', 'USA')->orderBy('code', 'asc')->get();
        $datastate2 = DB::table('state')->where('countrycode', 'USA')->orderBy('code', 'asc')->get();

        $services = Typeofser::All();

        $servicearr = array();
        foreach ($services as $serv):

            if ($serv->id == 2 || $serv->id == 3 || $serv->id == 4 || $serv->id == 11 || $serv->id == 15) {
                if ($serv->id == 2) {
                    array_push($servicearr, array(
                        'id' => $serv->id,
                        'typeofservice' => 'Bookeeping Service'
                    ));
                } else {
                    array_push($servicearr, array(
                        'id' => $serv->id,
                        'typeofservice' => $serv->typeofservice
                    ));
                }
            }

        endforeach;


        // $position = Price::All();
        // $currency = Currency::All();
        // $typeofser = Typeofser::All();
        // $period = Period::All();
        $taxtitle = Taxtitle::All();

        return view('fac-Bhavesh-0554.home', compact(['taxtitle', 'arrfed', 'c2', 'clientttaxation', 'taxtitles', 'clientoriginal', 'datastate2', 'datastate', 'commonone1', 'taskall', 'notesRelated1', 'relatedNames1', 'notesRelated', 'listclient', 'listemployeeuser', 'listvendoe', 'relatedNames', 'scheduledates', 'schedulesetup', 'countrecstatus', 'scheduleemployee', 'vendor1', 'ss1', 'commonregister1', 'vendor', 'commonregister', 'contact1', 'contact', 'employee1', 'employeeusers', 'emp1', 'msgc', 'adminupload', 'newemp1', 'clientemployee', 'msg', 'msgc1', 'msg1', 'empfsc', 'task', 'akk', 'common', 'common1', 'adminupload1', 'adminupload2', 'common2', 'common3', 'common4', 'common5', 'common6', 'admin', 'employees', 'employments', 'application', 'candidate', 'emp', 'newclient', 'newemp', 'employee', 'set', 'schedule', 'set1', 'servicetype', 'servicetype2', 'servicetype3', 'servicetype4', 'servicetype5', 'submission', 'msgforwork', 'msgdone', 'msg2', 'servicearr']));
    }


    public function addData()
    {
        $clientoriginal111 = DB::table("client_to_taxation")->select("client_to_taxation.*", "client_taxfederal.client_id", "client_taxfederal.federalstax", "client_taxfederal.federalsyear")->
        Join('client_taxfederal', function ($join) {
            $join->on('client_taxfederal.client_id', '=', 'client_to_taxation.clientid');
        })->where('client_to_taxation.taxation_service', '=', '7')->
        where('client_taxfederal.flag', '=', '1')->groupBY('client_taxfederal.client_id')->get();

        foreach ($clientoriginal111 as $feedback) {
            $up = DB::table('client_to_taxation')->where('clientid', '=', $feedback->client_id)->where('taxyears', '=', '2019')
                ->update([
                    'flag' => '1'
                ]);


            //$feedarr[]=$feedback->client_id;
        }

        //exit;
        $commonp = DB::table("client_to_taxation as t1")->select("t1.taxation_service_period", "t1.check_status", "t1.tax_service_year", "t1.employee_id as emp", "t1.tax_acc_checkby", "t1.taxation_service_period", "t1.pricetype", "t1.currency", "t1.serviceperiod", "t2.due_date_2", "t2.filename as filename", "t2.type_form1", "t2.type_form", "t1.taxyears", "t1.clientid as ccid", "t2.personalstatus", "t4.federalsduedate", "t4.federalsduedate", "t4.id as tids", "t4.client_id as tcid", "t3.firstName", "t3.middleName", "t3.lastName", "t3.employee_id", "t2.company_name", "t2.federalstax", "t2.first_name", "t2.middle_name", "t2.last_name", "t2.status as status1", "t2.business_id", "t1.id", "t2.id as ids", "t1.expiredate as expiredate", "documentcopy", "t1.taxation_service as status")
            ->leftJoin('commonregisters as t2', function ($join) {
                $join->on('t2.id', '=', 't1.clientid');
            })
            ->leftJoin('employees as t3', function ($join) {
                $join->on('t3.id', '=', 't1.employee_id');
            })
            ->leftJoin('client_taxfederal as t4', function ($join) {
                $join->on('t4.client_id', '=', 't1.clientid');
            })
            ->where('t1.taxyears', '=', '2019')->where('t1.taxation_service', '=', '7')->where('t2.business_id', '=', '6')->where('t2.status', '=', 'Active')->groupBy('t1.id')->orderBy('t4.federalsduedate', 'asc')->orderBy('t1.expiredate', 'asc')->orderBy('t2.filename', 'asc')->get();
        foreach ($commonp as $common) {


            //echo "<pre>";   print_r($common);
            if ($common->check_status == '0') {
                $taxinsert = DB::insert("insert into client_to_taxation(`tax_service_year`,`expiredate`,`taxyears`,`pricetype`,`currency`,
  `serviceperiod`,`clientid`,`taxation_service`,`taxation_service_period`,`employee_id`,`tax_acc_checkby`,`check_status`)
            values('2020','2021-05-17','2020','" . $common->pricetype . "','" . $common->currency . "','" . $common->taxation_service_period . "',
            '" . $common->ccid . "','" . $common->status . "','" . $common->taxation_service_period . "','" . $common->emp . "','" . $common->tax_acc_checkby . "',
            '')");
            }
        }
//exit;

///echo count($commonp);
///echo "<pre>";print_r($commonp);
//exit;
        $clientoriginal11 = DB::table("client_to_taxation")->select("client_to_taxation.*", "client_taxfederal.client_id", "client_taxfederal.federalstax", "client_taxfederal.federalsyear")->
        Join('client_taxfederal', function ($join) {
            $join->on('client_taxfederal.client_id', '=', 'client_to_taxation.clientid');
        })->where('client_to_taxation.taxation_service', '=', '6')->
        where('client_taxfederal.flag', '=', '1')->groupBY('client_taxfederal.client_id')->get();

        $clientoriginals = DB::table("client_taxfederal")->where('flag', '=', '1')->where('federalstax', '=', 'Original')->where('federalsyear', '=', '2019')->get();

        foreach ($clientoriginal11 as $feedback) {
            $up = DB::table('client_to_taxation')->where('client_id', '=', $feedback->client_id)->where('taxyears', '=', '2019')
                ->update([
                    'flag' => '1'
                ]);


            //$feedarr[]=$feedback->client_id;
        }
//exit;
        $clientoriginal1 = DB::table('client_taxfederal')
            ->where('federalstax', '=', 'Original')
            ->groupBy('client_id')
            ->having(DB::raw('count(client_id)'), '=', 1)
            ->get();
        foreach ($clientoriginal1 as $feedback) {

            $up1 = DB::table('client_taxfederal')->where('client_id', '=', $feedback->client_id)->where('federalstax', '=', 'Original')
                ->update([
                    'flag' => '1'
                ]);


            //$feedarr[]=$feedback->client_id;
        }

        //exit;

        $commonone1 = DB::table("client_to_taxation as t1")->select("t1.taxation_service_period", "t1.check_status", "t1.tax_service_year", "t1.employee_id as emp", "t1.tax_acc_checkby", "t1.taxation_service_period", "t1.pricetype", "t1.currency", "t1.serviceperiod", "t2.due_date_2", "t2.filename as filename", "t2.type_form1", "t2.type_form", "t1.taxyears", "t1.clientid as ccid", "t2.personalstatus", "t4.federalsduedate", "t4.federalsduedate", "t4.id as tids", "t4.client_id as tcid", "t3.firstName", "t3.middleName", "t3.lastName", "t3.employee_id", "t2.company_name", "t2.federalstax", "t2.first_name", "t2.middle_name", "t2.last_name", "t2.status as status1", "t2.business_id", "t1.id", "t2.id as ids", "t1.expiredate as expiredate", "documentcopy", "t1.taxation_service as status")
            ->leftJoin('commonregisters as t2', function ($join) {
                $join->on('t2.id', '=', 't1.clientid');
            })
            ->leftJoin('employees as t3', function ($join) {
                $join->on('t3.id', '=', 't1.employee_id');
            })
            ->leftJoin('client_taxfederal as t4', function ($join) {
                $join->on('t4.client_id', '=', 't1.clientid');
            })
            ->where('t1.taxyears', '=', '2019')->where('t1.taxation_service', '=', '6')->where('t2.business_id', '!=', '6')->where('t2.status', '=', 'Active')->groupBy('t1.id')->orderBy('t4.federalsduedate', 'asc')->orderBy('t1.expiredate', 'asc')->orderBy('t2.filename', 'asc')->get();
//echo 'xxc'.count($commonone1);

//echo "<pre>";print_r($commonone1);
//echo count($commonone1);

        foreach ($commonone1 as $common) {
            //echo "<pre>";   print_r($common);
            if ($common->check_status == '0') {
                $taxinsert = DB::insert("insert into client_to_taxation(`tax_service_year`,`expiredate`,`taxyears`,`pricetype`,`currency`,
  `serviceperiod`,`clientid`,`taxation_service`,`taxation_service_period`,`employee_id`,`tax_acc_checkby`,`check_status`)
            values('2020','2021-03-15','2020','" . $common->pricetype . "','" . $common->currency . "','" . $common->taxation_service_period . "',
            '" . $common->ccid . "','" . $common->status . "','" . $common->taxation_service_period . "','" . $common->emp . "','" . $common->tax_acc_checkby . "',
            '')");
            }
        }

    }

    public function addcoversationsheet(Request $request)
    {
        // echo "<pre>";
        // print_r($_REQUEST);die;

        $this->validate($request, [
            'type_user' => 'required',
            'conrelatedname' => 'required',
        ]);


        $date = $request->date;
        $day = $request->day;
        $time = $request->time;
        $type_user = $request->type_user;

        $redirection = '';

        if ($request->clientid != 'Select') {
            $clientid = $request->clientid;
            $redirection='fac-Bhavesh-0554/clientsetup/'.$request->clientid.'/edit?tab=other';
        } else {
            $clientid = '';
        }

        if ($request->employeeuserid != 'Select') {
            $employeeuserid = $request->employeeuserid;
            $redirection='fac-Bhavesh-0554/employee/'.$request->employeeuserid.'/edit?tab=other';
        } else {
            $employeeuserid = '';
        }

        if ($request->vendorid != 'Select') {
            $vendorid = $request->vendorid;
            $redirection='fac-Bhavesh-0554/vendor/'.$request->vendorid.'/edit';
        } else {
            $vendorid = '';
        }

        if ($request->otherid != '') {
            $otherid = $request->otherid;
            $redirection='fac-Bhavesh-0554/';
        } else {
            $otherid = '';
        }

        $conrelatedname = $request->conrelatedname;
        $condescription = $request->condescription;
        $connotes = $request->connotes;

        $returnValue = DB::table('conversation_sheet')
            ->insert([
                'creattiondate' => $date,
                'day' => $day,
                'time' => $time,
                'type_user' => $type_user,
                'clientid' => $clientid,
                'employeeuserid' => $employeeuserid,
                'vendorid' => $vendorid,
                'otherid' => $otherid,
                'conrelatedname' => $conrelatedname,
                'condescription' => $condescription,
                'connotes' => $connotes,

            ]);


        return redirect($redirection)->with('success', 'Record Added Successfully');
    }

    public function addnotesrelated(Request $request)
    {
        // echo "<pre>";
        // print_r($_REQUEST);die;

        $this->validate($request, [
            'notetype_user' => 'required',
            'notesrelatedcat' => 'required',
        ]);


        $notedate = $request->notedate;
        $noteday = $request->noteday;
        $notetime = $request->notetime;
        $notetype_user = $request->notetype_user;
        
        $redirection='';
        
        if ($request->noteclientid != 'Select') {
            $noteclientid = $request->noteclientid;
            $redirection='fac-Bhavesh-0554/clientsetup/'.$request->noteclientid.'/edit?tab=other';
        } else {
            $noteclientid = '';
        }

        if ($request->noteemployeeuserid != 'Select') {
            $noteemployeeuserid = $request->noteemployeeuserid;
            $redirection='fac-Bhavesh-0554/employee/'.$request->noteemployeeuserid.'/edit?tab=other';
        } else {
            $noteemployeeuserid = '';
        }

        if ($request->notevendorid != 'Select') {
            $notevendorid = $request->notevendorid;
            $redirection='fac-Bhavesh-0554/vendor/'.$request->notevendorid.'/edit?tab=other';
        } else {
            $notevendorid = '';
        }

        if ($request->noteotherid != '') {
            $noteotherid = $request->noteotherid;
        } else {
            $noteotherid = '';
        }

        $noterelated = $request->noterelated;
        $notesrelatedcat = $request->notesrelatedcat;
        $notes = $request->notes;

        $returnValue = DB::table('notes_sheet')
            ->insert([
                'creattiondate' => $notedate,
                'noteday' => $noteday,
                'notetime' => $notetime,
                'notetype_user' => $notetype_user,
                'noteclientid' => $noteclientid,
                'noteemployeeuserid' => $noteemployeeuserid,
                'notevendorid' => $notevendorid,
                'noteotherid' => $noteotherid,
                'noterelated' => $noterelated,
                'notesrelatedcat' => $notesrelatedcat,
                'notes' => $notes,

            ]);


        return redirect($redirection)->with('success', 'Record Added Successfully');
    }


    public function documents()
    {

        $newopt = Input::get('newopt');
        //      $sign = Input::get('sign');
        if (DB::table('documents')->where('documentname', '=', $newopt)->exists()) {
            //return response()->json(['status' => 'error']);
        } else {
            $position = new Document;
            $position->documentname = $newopt;
            // $position->sign = $sign;
            $position->save();
            if ($position) {
                return response()->json([
                    'status' => 'success',
                    'newopt' => $newopt
                ]);
            } else {
                return response()->json([
                    'status' => 'error'
                ]);
            }
        }
    }

    public function fetch(Request $request)
    {
        if ($request->get('query')) {
            $query = $request->get('query');


            // $data = DB::table('commonregisters')->where('filename', 'LIKE', "%{$query}%")->orwhere('company_name', 'LIKE', "%{$query}%")->
            // orwhere('business_no', 'LIKE', "%{$query}%")->orwhere('business_name', 'LIKE', "%{$query}%")->get();

            $commonregiser = DB::table('commonregisters')
                ->select(
                    'commonregisters.type as Type',
                    'commonregisters.id as cid',
                    'commonregisters.filename as entityid',
                    'commonregisters.first_name as fname',
                    'commonregisters.middle_name as mname',
                    'commonregisters.last_name as lname',
                    'commonregisters.company_name as cname',
                    'commonregisters.business_name',
                    'commonregisters.business_id',
                    'commonregisters.status'
                )
                ->selectRaw("REPLACE(REPLACE(REPLACE(REPLACE(business_no, '(', ''),')',''),' ',''),'-','') as business_no")->where('filename', 'LIKE', "%{$query}%")->orwhere('business_no', 'LIKE', "%{$query}%")->orwhere('business_name', 'LIKE', "%{$query}%")->orwhere('company_name', 'LIKE', "%{$query}%")->orwhere('first_name', 'LIKE', "%{$query}%");


            $employ1 = DB::table('employees')
                ->select(
                    'employees.type as Type',
                    'employees.id as cid',
                    'employees.employee_id as entityid',
                    'employees.firstName as fname',
                    'employees.middleName as mname',
                    'employees.lastName as lname',
                    'employees.company_name as cname',
                    'employees.business_name',
                    'employees.business_id',
                    'employees.status'
                )
                ->selectRaw("REPLACE(REPLACE(REPLACE(REPLACE(business_no, '(', ''),')',''),' ',''),'-','')  as business_no")->where('business_name', 'LIKE', "%{$query}%")->orwhere('business_no', 'LIKE', "%{$query}%")->orwhere('employee_id', 'LIKE', "%{$query}%")->orwhere('firstName', 'LIKE', "%{$query}%")->orwhere('lastName', 'LIKE', "%{$query}%");

            $bank1 = DB::table('bankmaster')
                ->select(
                    'bankmaster.type as Type',
                    'bankmaster.id as cid',
                    'bankmaster.branchcity as entityid',
                    'bankmaster.branchcontactname as fname',
                    'bankmaster.branchzip as mname',
                    'bankmaster.telephone2 as lname',
                    'bankmaster.bankname as cname',
                    'bankmaster.branchname as business_name',
                    'bankmaster.routingnumber as business_id',
                    'bankmaster.status'
                )
                ->selectRaw("REPLACE(REPLACE(REPLACE(REPLACE(telephone1, '(', ''),')',''),' ',''),'-','')  as business_no")
                ->where('bankname', 'LIKE', "%{$query}%")->orwhere('telephone1', 'LIKE', "%{$query}%")->orwhere('routingnumber', 'LIKE', "%{$query}%")->orwhere('branchname', 'LIKE', "%{$query}%")->orwhere('branchcontactname', 'LIKE', "%{$query}%");

            $datas = $bank1->union($employ1)->union($commonregiser)->get();
            // echo "<pre>";print_r($datas);

            $output = '';
            $i;
            foreach ($datas as $row) {
                // print_r($row);
                // echo $row->filename;
                // echo $query;
                if ($row->status == '1' || $row->status == 'Active') {

                    if ($row->Type == 'Vendor' || $row->Type == 'employee' || $row->Type == 'user' || $row->Type == 'clientemployee' || $row->Type == 'bank') {
                        $id = $row->cid;
                        if ($row->Type == 'Vendor') {
                            $names = $row->business_name;
                        } else if ($row->Type == 'bank') {
                            $names = $row->cname;
                        } else {
                            $names = $row->fname . ' ' . $row->mname . ' ' . $row->lname;
                        }
                        if ($row->Type == 'employee') {
                            $types1 = 'EE';
                        } else if ($row->Type == 'user') {
                            $types1 = 'U';
                        } else if ($row->Type == 'Vendor') {
                            $types1 = 'V';
                        } else if ($row->Type == 'clientemployee') {
                            $types1 = 'C-EE';
                        } else if ($row->Type == 'bank') {
                            $types1 = 'B';
                        }

                        //$types1=ucfirst($row->Type);
                        // $id=$row->cid;

                    } else {
                        //echo $row->Type;
                        if ($row->business_id == '6') {
                            $names = $row->fname . ' ' . $row->mname . ' ' . $row->lname;
                            $types1 = "C";
                            $id = $row->cid;
                        } else {
                            $names = $row->cname;
                            $types1 = "C";
                            $id = $row->cid;
                        }
                    }
                    ?>
                    <li style="text-align:left;"><a onclick="myfun('<?php echo $id; ?>','<?php echo $types1; ?>')"><span
                                    class="bgcolors"><?php echo $types1; ?></span><span
                                    class="clientalign"><?php echo $row->entityid . ' '; ?></span><span
                                    class="entityname"><?php echo $names; ?></span></a></li>
                    <?php
                }
            }
            $output .= '';
            echo $output;
        }
    }

    public function praposal_fetch(Request $request)
    {
        if ($request->get('query')) {
            $query = $request->get('query');
            $commonregiser = DB::table('commonregisters')
                ->select(
                    'commonregisters.type as Type',
                    'commonregisters.id as cid',
                    'commonregisters.filename as entityid',
                    'commonregisters.first_name as fname',
                    'commonregisters.middle_name as mname',
                    'commonregisters.last_name as lname',
                    'commonregisters.company_name as cname',
                    'commonregisters.business_name',
                    'commonregisters.business_id',
                    'commonregisters.status'
                )
                ->selectRaw("REPLACE(REPLACE(REPLACE(REPLACE(business_no, '(', ''),')',''),' ',''),'-','') as business_no")->where('filename', 'LIKE', "%{$query}%")->orwhere('business_no', 'LIKE', "%{$query}%")->orwhere('business_name', 'LIKE', "%{$query}%")->orwhere('company_name', 'LIKE', "%{$query}%")->orwhere('first_name', 'LIKE', "%{$query}%")->get();
            foreach ($commonregiser as $row) {
                if ($row->status == '1' || $row->status == 'Active') {
                    if ($row->business_id == '6') {
                        $names = $row->fname . ' ' . $row->mname . ' ' . $row->lname;
                        $types1 = "C";
                        $id = $row->cid;
                    } else {
                        $names = $row->cname;
                        $types1 = "C";
                        $id = $row->cid;
                    }
                    ?>
                    <li style="text-align:left;"><a onclick="getClientdetailForPraposal('<?php echo $id; ?>')"><span
                                    class="bgcolors"><?php echo $types1; ?></span><span
                                    class="clientalign"><?php echo $row->entityid . ' '; ?></span><span
                                    class="entityname"><?php echo $names; ?></span></a></li>
                    <?php
                }
            }
        }
    }

    public function getDetailForPraposal(Request $request)
    {
        if ($request->get('query')) {
            $result = array();
            $query = $request->get('query');
            $commonregiser = DB::table('commonregisters')
                ->select(
                    'commonregisters.type as Type',
                    'commonregisters.email',
                    'commonregisters.register_id as cid',
                    'commonregisters.filename as entityid',
                    'commonregisters.first_name as fname',
                    'commonregisters.middle_name as mname',
                    'commonregisters.last_name as lname',
                    'commonregisters.company_name as cname',
                    'commonregisters.business_name',
                    'commonregisters.business_id',
                    'commonregisters.address',
                    'commonregisters.business_fax',
                    'commonregisters.city',
                    'commonregisters.stateId',
                    'commonregisters.zip',
                    'commonregisters.status',
                    'commonregisters.business_no'

                )->where('commonregisters.id', $query)->get();
            foreach ($commonregiser as $row) {
                if ($row->status == '1' || $row->status == 'Active') {
                    if ($row->business_id == '6') {
                        $names = $row->fname . ' ' . $row->mname . ' ' . $row->lname;
                        $types1 = "C";
                        $business = $row->business_no;
                        $email = $row->email;
                        $id = $row->cid;
                    } else {
                        $names = $row->cname;
                        $types1 = "C";
                        $business = $row->business_no;
                        $id = $row->cid;
                        $email = $row->email;
                    }
                    array_push($result, array(
                        'name' => $names,
                        'telephone' => $business,
                        'email' => $email,
                        'address' => $row->address,
                        'city' => $row->city,
                        'state' => $row->stateId,
                        'zip' => $row->zip,
                        'cid' => $id,
                        'filename' => $row->entityid,
                        'fax' => $row->business_fax
                    ));
                }
            }
        }
        return json_encode($result);
    }

    public function storeProposalClient(Request $request)
    {

        // $this->validate($request, [
        //     'client' => 'required',
        //     'praposal_client_name' => 'required',
        //     'praposal_client_telephone' => 'required',
        //     'praposal_client_email' => 'required',
        //     'prospect_service' => 'required',

        //     'priority' => 'required',
        //     'duedate' => 'required',
        //     'scope_of_work' => 'required',
        //     'fsc_fee' => 'required',
        // ]);

        $proposal_client = new Proposal_client_details;

        $proposal_client->type_of_client = $request->client;
        $proposal_client->client_name = $request->praposal_client_name;
        $proposal_client->telephone = $request->praposal_client_telephone;
        $proposal_client->email = $request->praposal_client_email;
        $proposal_client->type_of_service = $request->prospect_service;
        $proposal_client->priority = $request->priority;
        $proposal_client->duedate = date('Y-m-d', strtotime($request->duedate));
        $proposal_client->scope_of_work = $request->scope_of_work;
        $proposal_client->fsc_fee = $request->fsc_fee;
        $proposal_client->save();

        $pid = $proposal_client->id;

        $subservices = $request->selective_services;
        $countof = explode(',', $subservices);

        for ($i = 0; $i < count($countof); $i++) {
            $proposal_client_sub = new Proposal_client_detail_services;
            $proposal_client_sub->proposal_client_id = $pid;
            $proposal_client_sub->service_id = $request->prospect_service;
            $proposal_client_sub->sub_service_id = $countof[$i];
            $proposal_client_sub->save();
        }

        return redirect('fac-Bhavesh-0554/prospect/')->with('success', 'Success fully Update Branch');
    }

    public function fetch1(Request $request)
    {
        $id = $request->get('id');
        $type = $request->get('type');


        if ($type == 'C') {
            $commonregiser = DB::table('commonregisters')
                ->select(
                    'commonregisters.email',
                    'commonregisters.business_no as telephone',
                    'commonregisters.mobile_1 as telephone2',
                    'commonregisters.businesstype as telephonetype',
                    'commonregisters.telephoneNo2Type as telephonetype2',
                    'commonregisters.businessext as ext1',
                    'commonregisters.type as Type',
                    'commonregisters.id as cid',
                    'commonregisters.filename as entityid',
                    'commonregisters.first_name as fname',
                    'commonregisters.middle_name as mname',
                    'commonregisters.last_name as lname',
                    'commonregisters.company_name as cname',
                    'commonregisters.business_id',
                    'commonregisters.company_name',
                    'commonregisters.firstname',
                    'commonregisters.lastname',
                    'commonregisters.motelephoneile1 as telephoneNo2',
                    'commonregisters.telephoneNo2Type as telephoneNo2Type',
                    'commonregisters.eext1 as ext2',
                    'commonregisters.personalemail as fscemail',
                    'commonregisters.business_name as bsname'
                )->where('id', '=', $id)->where('status', '=', 'Active')->first();
        } else if ($type == 'EE' || $type == 'U' || $type == 'V' || $type == 'C-EE') {
            $commonregiser = DB::table('employees')
                ->select(
                    'employees.email',
                    'employees.telephoneNo1 as telephone',
                    'employees.etelephone1 as telephone2',
                    'employees.telephoneNo1Type as telephonetype',
                    'employees.eteletype1 as telephonetype2',
                    'employees.ext1',
                    'employees.type as Type',
                    'employees.id as cid',
                    'employees.employee_id as entityid',
                    'employees.firstName as fname',
                    'employees.middleName as mname',
                    'employees.lastName as lname',
                    'employees.business_id',
                    'employees.business_name as company_name',
                    'employees.firstname',
                    'employees.lastname',
                    'employees.telephoneNo2',
                    'employees.telephoneNo2Type',
                    'employees.ext2',
                    'employees.fscemail',
                    'employees.fscemail as bsname'
                )
                ->where('id', '=', $id)->where('status', '=', '1')->first();

            //echo "<pre>";print_r($commonregiser);
        } else if ($type == 'B') {
            $commonregiser = DB::table('bankmaster')
                ->select('bankmaster.type as Type', 'bankmaster.contactnumber as telephone', 'bankmaster.bankname as cname', 'bankmaster.status', 'bankmaster.id')
                ->where('id', '=', $id)->where('status', '=', '1')->first();

            //  echo "<pre>";print_r($commonregiser);
        }


        echo json_encode($commonregiser);
        exit;
    }

    public function fetch111(Request $request)
    {
        $id = $request->get('id');
        $type = $request->get('type');


        if ($type == 'C') {
            $commonregiser = DB::table('commonregisters')
                ->select(
                    'commonregisters.email',
                    'commonregisters.business_no as telephone',
                    'commonregisters.mobile_1 as telephone2',
                    'commonregisters.businesstype as telephonetype',
                    'commonregisters.telephoneNo2Type as telephonetype2',
                    'commonregisters.businessext as ext1',
                    'commonregisters.type as Type',
                    'commonregisters.id as cid',
                    'commonregisters.filename as entityid',
                    'commonregisters.first_name as fname',
                    'commonregisters.middle_name as mname',
                    'commonregisters.last_name as lname',
                    'commonregisters.company_name as cname',
                    'commonregisters.business_id',
                    'commonregisters.company_name',
                    'commonregisters.firstname',
                    'commonregisters.lastname',
                    'commonregisters.motelephoneile1 as telephoneNo2',
                    'commonregisters.eteletype1 as telephoneNo2Type',
                    'commonregisters.eext1 as ext2',
                    'commonregisters.personalemail as fscemail'
                )->where('id', '=', $id)->where('status', '=', 'Active')->first();
        } else if ($type == 'EE' || $type == 'U' || $type == 'V' || $type == 'C-EE') {

            $commonregiser = DB::table('employees')
                ->select(
                    'employees.email',
                    'employees.telephoneNo1 as telephone',
                    'employees.etelephone1 as telephone2',
                    'employees.telephoneNo1Type as telephonetype',
                    'employees.eteletype1 as telephonetype2',
                    'employees.ext1',
                    'employees.type as Type',
                    'employees.id as cid',
                    'employees.employee_id as entityid',
                    'employees.firstName as fname',
                    'employees.middleName as mname',
                    'employees.lastName as lname',
                    'employees.business_id',
                    'employees.business_name as company_name',
                    'employees.firstname',
                    'employees.lastname',
                    'employees.telephoneNo2',
                    'employees.telephoneNo2Type',
                    'employees.ext2',
                    'employees.fscemail'
                )
                ->where('id', '=', $id)->where('status', '=', '1')->first();

            //echo "<pre>";print_r($commonregiser);
        }
        echo json_encode($commonregiser);
        exit;
    }

    public function fetch_period()
    {
        $id = $_GET['id'];
        //echo $id;
        //exit();
        $price = DB::table('prices')->leftJoin('payroll_service', 'payroll_service.id', '=', 'prices.payrollservice')
            ->leftJoin('currencies', 'prices.currency', '=', 'currencies.id')
            ->leftJoin('period', 'prices.period', '=', 'period.id')
            ->leftJoin('typeofser', 'prices.typeofservice', '=', 'typeofser.id')
            ->select('prices.*', 'payroll_service.id as pid', 'payroll_service.servicename', 'currencies.id', 'currencies.currency', 'currencies.sign', 'period.id as pid', 'typeofser.id', 'period.period', 'typeofser.typeofservice', 'prices.id')
            ->orderby('typeofser.typeofservice', 'asc')
            ->where('prices.typeofservice', $id)
            ->get();
        //print_r($price);
        echo json_encode($price);
        //exit;
    }

    public function fetch_period_price()
    {
        $id = $_GET['id'];
        $period = $_GET['pid'];
        //echo $id;
        //exit();
        $price = DB::table('prices')->leftJoin('payroll_service', 'payroll_service.id', '=', 'prices.payrollservice')
            ->leftJoin('currencies', 'prices.currency', '=', 'currencies.id')
            ->leftJoin('period', 'prices.period', '=', 'period.id')
            ->leftJoin('typeofser', 'prices.typeofservice', '=', 'typeofser.id')
            ->select('prices.*', 'payroll_service.id as pid', 'payroll_service.servicename', 'currencies.id', 'currencies.currency', 'currencies.sign', 'period.id', 'typeofser.id', 'period.period', 'typeofser.typeofservice', 'prices.id')
            ->where('prices.typeofservice', $id)
            ->where('prices.period', $period)
            ->orderby('typeofser.typeofservice', 'asc')
            ->first();
        //print_r($price);
        echo json_encode($price);
        //exit;
    }

    public function fetch_taxtion_price()
    {
        $id = $_GET['id'];
        $period = $_GET['pid'];
        $tid = $_GET['tid'];
        //echo $id;
        //exit();
        $price = DB::table('taxationservice')
            ->where('typeofservice', $id)
            ->where('period', $period)
            ->where('title', $tid)
            ->first();
        //print_r($price);
        echo json_encode($price);
        //exit;
    }

}
