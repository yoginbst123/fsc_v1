<?php
namespace App\Http\Controllers\employee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Employee;
use App\employees\Fscemployee;
use App\Model\Business;
use App\Model\Category;
use Auth;
use DB;
use App\Front\Commonregister;
use Illuminate\Support\Facades\Input;
use Response;
use App\Model\Logo;
use App\Model\Period;
use App\Model\Taxtitle;


class FscemployeeworkreportController extends Controller
{
 public function __construct()
    {
      $this->middleware('auth:employee');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {    
               $user_id  = Auth::user()->user_id;
         $id  = Auth::user()->id;
     
           $states=DB::table('commonregisters')->select('stateid')->where('stateid','!=',null)->distinct()->orderBy('stateid', 'ASC')->get();
        //echo "<pre>";
        //print_r($states);die;
         $logo = Logo::where('id','=',1)->first();
         $business = Business::orderBy('bussiness_name', 'asc')->get();
         $taxtitle = Taxtitle::orderBy('title', 'asc')->get();
        // $employee = Employee::where('type','!=','clientemployee')->where('type','!=','Vendor')->where('status','=','1')->orderBy('firstName', 'asc')->get(); 
       $employees = DB::table("employees as t1")->select("t1.timesheet","t1.type","t1.status","t1.id","t1.firstName","t1.middleName","t1.lastName","t1.team","t2.id as teamid","t2.team as Team")
        ->leftJoin('teams as t2', function($join){ $join->on('t2.id', '=', 't1.team');})
        ->where('t1.type','!=','clientemployee')->where('t1.type','!=','Vendor')->where('t1.status','=','1')->where('t1.check','=','1')->orderBy('t1.firstName', 'asc')->get();
      // echo "<pre>"; print_r($employee);
      
       $employee = Employee::where('id','=',$user_id)->first();
      // print_r($employee);
       
         $category = Category::orderBy('business_cat_name', 'asc')->get();   
         $datastate =  DB::table('state')->where('countrycode','USA')->orderBy('code', 'asc')->get(); 
         $datastate2 = DB::table('state')->where('countrycode','USA')->orderBy('code', 'asc')->get(); 
   
    
         $period = Period::All();
     
         return view('fscemployee.fscemployeeworkreport/fscemployeeworkreport',compact(['employees','employee','taxtitle','datastate2','datastate','states','business','category','logo','period']));
    }
    
    
    
     public function store(Request $request)
    {
       // exit('1111');
               $user_id  = Auth::user()->user_id;
         $id  = Auth::user()->id;
     
       
       
         $states=DB::table('commonregisters')->select('stateid')->where('stateid','!=',null)->distinct()->orderBy('stateid', 'ASC')->get();
         $taxtitle = Taxtitle::orderBy('title', 'asc')->get();
        
         $logo = Logo::where('id','=',1)->first();
         $business = Business::orderBy('bussiness_name', 'asc')->get();
         
         $category = Category::orderBy('business_cat_name', 'asc')->get();  
          $business1 = Category::where('id','=',$request->business)->first();
        
          $datastate = DB::table('state')->where('countrycode','USA')->orderBy('code', 'asc')->get(); 
           $datastate2 = DB::table('state')->where('countrycode','USA')->orderBy('code', 'asc')->get(); 
          // $employee1 = Employee::where('id','=',$request->employee)->first();   
         $employee1 = DB::table("employees as t1")->select("t1.type","t1.status","t1.id","t1.firstName","t1.middleName","t1.lastName","t1.team","t2.id as teamid","t2.team as Team")
        ->leftJoin('teams as t2', function($join){ $join->on('t2.id', '=', 't1.team');})
       ->where('t1.id','=',$request->employee)->first();
       $employees = DB::table("employees as t1")->select("t1.type","t1.status","t1.id","t1.firstName","t1.middleName","t1.lastName","t1.team","t2.id as teamid","t2.team as Team")
        ->Join('teams as t2', function($join){ $join->on('t2.id', '=', 't1.team');})
        ->where('t1.type','!=','clientemployee')->where('t1.type','!=','Vendor')->where('t1.status','=','1')->orderBy('t1.firstName', 'asc')->get();
            $employee = Employee::where('id','=',$user_id)->first();
     
             if($request->employee !=''  && $request->business =='' && $request->type_of_service =='' && $request->month =='' &&  $request->year =='')
            {
         $client = DB::table("commonregisters as t1")->select("t6.id as businessid","t6.business_cat_name","t1.ac_service_month","t1.ac_service_year","t1.business_cat_id","t1.filename","t1.status","t1.company_name","t1.id","t2.clientid","t2.employee_id","t3.id as eid","t4.id as taxeid","t5.taxemployee_id","t5.clientid as clienttaxid")
        ->leftJoin('clientservices as t2', function($join){ $join->on('t2.clientid', '=', 't1.id');})
        ->leftJoin('taxclientservices as t5', function($join){ $join->on('t5.clientid', '=', 't1.id');})
        
        ->leftJoin('employees as t3', function($join){ $join->on('t3.id', '=', 't2.employee_id');})
        ->leftJoin('employees as t4', function($join){ $join->on('t4.id', '=', 't5.taxemployee_id');})
         ->leftJoin('categories as t6', function($join){ $join->on('t6.id', '=', 't1.business_cat_id');})
       
        ->where('t1.status','=','Active')->where('t3.id','=',$request->employee)->orWhere('t4.id','=',$request->employee)
        ->groupBy('t1.id')
        ->orderBy('t1.filename','asc')->get();
   
          }
          
             else if($request->employee !=''  && $request->business !='' && $request->type_of_service =='' && $request->month =='' &&  $request->year =='')
            {
                
         $client = DB::table("commonregisters as t1")->select("t6.id as businessid","t6.business_cat_name","t1.ac_service_month","t1.ac_service_year","t1.business_cat_id","t1.filename","t1.status","t1.company_name","t1.id","t2.clientid","t2.employee_id","t3.id as eid","t4.id as taxeid","t5.taxemployee_id","t5.clientid as clienttaxid")
        ->leftJoin('clientservices as t2', function($join){ $join->on('t2.clientid', '=', 't1.id');})
        ->leftJoin('taxclientservices as t5', function($join){ $join->on('t5.clientid', '=', 't1.id');})
        
        ->leftJoin('employees as t3', function($join){ $join->on('t3.id', '=', 't2.employee_id');})
        ->leftJoin('employees as t4', function($join){ $join->on('t4.id', '=', 't5.taxemployee_id');})
        ->leftJoin('categories as t6', function($join){ $join->on('t6.id', '=', 't1.business_cat_id');})
        
        ->where('t1.status','=','Active')->where('t3.id','=',$request->employee)->where('t1.business_cat_id','=',$request->business)->orWhere('t4.id','=',$request->employee)
        ->groupBy('t1.id')
        ->orderBy('t1.filename','asc')->get();
   
          }
          
          else if($request->employee !=''  && $request->business !='' && $request->type_of_service =='10-2' && $request->month =='' &&  $request->year =='')
            {
                
         $client = DB::table("commonregisters as t1")->select("t6.id as businessid","t6.business_cat_name","t1.ac_service_month","t1.ac_service_year","t1.business_cat_id","t1.filename","t1.status","t1.company_name","t1.id","t2.clientid","t2.employee_id","t2.typeofservice","t3.id as eid")
        ->leftJoin('clientservices as t2', function($join){ $join->on('t2.clientid', '=', 't1.id');})
        
        ->leftJoin('employees as t3', function($join){ $join->on('t3.id', '=', 't2.employee_id');})
        ->leftJoin('categories as t6', function($join){ $join->on('t6.id', '=', 't1.business_cat_id');})
        
        ->where('t1.status','=','Active')->where('t3.id','=',$request->employee)->where('t1.business_cat_id','=',$request->business)->where('t2.typeofservice','=','2')
        ->groupBy('t1.id')
        ->orderBy('t1.filename','asc')->get();
   
          }
          
          else if($request->employee !=''  && $request->business =='' && $request->type_of_service =='10-2' && $request->month =='' &&  $request->year =='')
            {
                
         $client = DB::table("commonregisters as t1")->select("t6.id as businessid","t6.business_cat_name","t1.ac_service_month","t1.ac_service_year","t1.business_cat_id","t1.filename","t1.status","t1.company_name","t1.id","t2.clientid","t2.employee_id","t2.typeofservice","t3.id as eid")
        ->leftJoin('clientservices as t2', function($join){ $join->on('t2.clientid', '=', 't1.id');})
        
        ->leftJoin('employees as t3', function($join){ $join->on('t3.id', '=', 't2.employee_id');})
        ->leftJoin('categories as t6', function($join){ $join->on('t6.id', '=', 't1.business_cat_id');})
        
        ->where('t1.status','=','Active')->where('t3.id','=',$request->employee)->where('t2.typeofservice','=','2')
        ->groupBy('t1.id')
        ->orderBy('t1.filename','asc')->get();
   
          }
          
          else if($request->employee !=''  && $request->business =='' && $request->type_of_service =='10-2' && $request->month !='' &&  $request->year !='')
            {
                
         $client = DB::table("commonregisters as t1")->select("t6.id as businessid","t6.business_cat_name","t1.ac_service_month","t1.ac_service_year","t1.business_cat_id","t1.filename","t1.status","t1.company_name","t1.id","t2.clientid","t2.employee_id","t2.typeofservice","t3.id as eid")
        ->leftJoin('clientservices as t2', function($join){ $join->on('t2.clientid', '=', 't1.id');})
        
        ->leftJoin('employees as t3', function($join){ $join->on('t3.id', '=', 't2.employee_id');})
        ->leftJoin('categories as t6', function($join){ $join->on('t6.id', '=', 't1.business_cat_id');})
        
        ->where('t1.ac_service_month','=',$request->month)->where('t1.ac_service_year','=',$request->year)->where('t1.status','=','Active')->where('t3.id','=',$request->employee)->where('t2.typeofservice','=','2')
        ->groupBy('t1.id')
        ->orderBy('t1.filename','asc')->get();
   
          }
          else if($request->employee !=''  && $request->business !='' && $request->type_of_service =='10-2' && $request->month !='' &&  $request->year !='')
            {
                
         $client = DB::table("commonregisters as t1")->select("t6.id as businessid","t6.business_cat_name","t1.ac_service_month","t1.ac_service_year","t1.business_cat_id","t1.filename","t1.status","t1.company_name","t1.id","t2.clientid","t2.employee_id","t2.typeofservice","t3.id as eid")
        ->leftJoin('clientservices as t2', function($join){ $join->on('t2.clientid', '=', 't1.id');})
        
        ->leftJoin('employees as t3', function($join){ $join->on('t3.id', '=', 't2.employee_id');})
        ->leftJoin('categories as t6', function($join){ $join->on('t6.id', '=', 't1.business_cat_id');})
        
        ->where('t1.ac_service_month','=',$request->month)->where('t1.business_cat_id','=',$request->business)->where('t1.ac_service_year','=',$request->year)->where('t1.status','=','Active')->where('t3.id','=',$request->employee)->where('t2.typeofservice','=','2')
        ->groupBy('t1.id')
        ->orderBy('t1.filename','asc')->get();
   
          }
          
          else if($request->employee ==''  && $request->business =='' && $request->type_of_service =='10-2' && $request->month =='' &&  $request->year =='')
            {
                
         $client = DB::table("commonregisters as t1")->select("t6.id as businessid","t6.business_cat_name","t1.ac_service_month","t1.ac_service_year","t1.business_cat_id","t1.filename","t1.status","t1.company_name","t1.id","t2.clientid","t2.employee_id","t2.typeofservice","t3.id as eid")
        ->leftJoin('clientservices as t2', function($join){ $join->on('t2.clientid', '=', 't1.id');})
        
        ->leftJoin('employees as t3', function($join){ $join->on('t3.id', '=', 't2.employee_id');})
        ->leftJoin('categories as t6', function($join){ $join->on('t6.id', '=', 't1.business_cat_id');})
        
        ->where('t1.status','=','Active')->where('t2.typeofservice','=','2')
        ->groupBy('t1.id')
        ->orderBy('t1.filename','asc')->get();
   
          }
          
          
          else if($request->employee ==''  && $request->business =='' && $request->type_of_service =='10-2' && $request->month !='' &&  $request->year !='')
            {
                
         $client = DB::table("commonregisters as t1")->select("t6.id as businessid","t6.business_cat_name","t1.ac_service_month","t1.ac_service_year","t1.business_cat_id","t1.filename","t1.status","t1.company_name","t1.id","t2.clientid","t2.employee_id","t2.typeofservice","t3.id as eid")
        ->leftJoin('clientservices as t2', function($join){ $join->on('t2.clientid', '=', 't1.id');})
        
        ->leftJoin('employees as t3', function($join){ $join->on('t3.id', '=', 't2.employee_id');})
        ->leftJoin('categories as t6', function($join){ $join->on('t6.id', '=', 't1.business_cat_id');})
        
        ->where('t1.status','=','Active')->where('t2.typeofservice','=','2')->where('t1.ac_service_month','=',$request->ac_service_month)->where('t1.ac_service_year','=',$request->ac_service_year)
        ->groupBy('t1.id')
        ->orderBy('t1.filename','asc')->get();
   
          }
          
         return view('fscemployee.fscemployeeworkreport/fscemployeeworkreport',compact(['employees','business1','employee1','employee','client','taxtitle','datastate2','datastate','states','logo','category','business']));
    
    }
   
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      // 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
     //  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}