<?php
namespace App\Http\Controllers\employee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\employee\Leave;
use App\Model\Employee;
use Auth;
use DB;
use App\User;
use Illuminate\Support\Facades\Input;

class LeaveController extends Controller
{
    public function __construct()
    {
    $this->middleware('auth:employee');
    }  
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        //$leave = Leave::All();
        
        $user_id  = Auth::user()->user_id;
        $employee = Employee::where('id',$user_id)->first();
        $leave = Leave::where('employee_id',$user_id)->get();
        return view('fscemployee/leave/leave',compact(['leave','employee']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { 
        $user_id  = Auth::user()->user_id;
        $employee = Employee::where('id',$user_id)->first();
       return view('fscemployee/leave/create',compact(['employee']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request,[
            'start_date' =>'required',
            'total_days' =>'required',
        ]);
        $branch  = new Leave;
        $branch->start_date = $request->start_date;
        $branch->total_days = $request->total_days;
        $branch->end_date = $request->end_date;
        $branch->leave_reason = $request->leave_reason;
        $branch->employee_id = $request->employee_id;
        $branch->creation_date = date('Y-m-d');
        $branch->save();
        return redirect('fscemployee/leave')->with('success','Success fully add Leave');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
     //$branch = Commonregister::where('id',$id)->first(); 
     //$position = Employee::All(); 
      $user_id  = Auth::user()->user_id;
        $employee = Employee::where('id',$user_id)->first();
     $leave = Leave::where('id',$id)->first();
     return view('fscemployee.leave.edit',compact(['leave','employee']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
      $this->validate($request,[]);
       $branch = Leave::find($id);
         $branch->start_date = $request->start_date;
        $branch->total_days = $request->total_days;
        $branch->end_date = $request->end_date;
        $branch->leave_reason = $request->leave_reason;
        $branch->employee_id = $request->employee_id;
        $branch->creation_date = date('Y-m-d');
        $branch->update();
        return redirect('fscemployee/leave')->with('success','Success fully Update Leave');
    }
  

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Branch::where('id',$id)->delete();
        return redirect(route('addressbook.index'));
        
    }
}
