<?php
namespace App\Http\Controllers\employee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Front\Commonregister;
use App\Model\BusinessBrand;

use App\Model\Category;
use App\Model\Business;
use App\Model\Employee;
use App\Model\client_professional;
use App\Model\client_shareholder;
use App\User;
use App\Model\Categorybusiness;
use App\Model\Contact_userinfo;
use App\Mail\Activationmail;
use App\Model\taxstate;
use DB;
use Hash;
use Carbon;
use App\Submissionlogin\Submissionlogin;
use App\Model\Admin;
use Auth;
use App\Model\Price;
use App\Model\Currency;
use App\Model\Typeofser;
use App\Model\Period;
use App\Model\Taxtitle;
use App\Model\Language;
use App\Model\Ethnic;
use App\Model\Workcategory;

use App\employees\Fscemployee;
use Illuminate\Support\Facades\Input;
use App\Model\Logo;

class WorknewController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:employee');
    }
    
    public function index(Request $request)
    {
        $workcategory = Workcategory::orderBy('category_name', 'asc')->get();
        $id = $request->id;
        $taxstate = taxstate::All();
        $language = language::orderBy('language_name', 'asc')->get();
        $ethnic = Ethnic::orderBy('ethnic_name', 'asc')->get();
        $newclient12 = Commonregister::where('id',$id)->first();
        if(empty($newclient12)){
           $other_first_language1 ='';
        $other_second_language1 =''; 
        $category='';
        $cb='';
        $bid='';
        $businessbrand='';
        }
        else{
        $bid= $newclient12->business_id;
        $catid= $newclient12->business_cat_id;
        $brand_id= $newclient12->business_brand_id;
            $businessbrand = BusinessBrand::where('business_cat_id','=',$catid)->orderBy('business_brand_name', 'asc')->get(); 
        $cb= Categorybusiness::where('business_brand_id','=',$brand_id)->orderBy('business_brand_category_name', 'asc')->get(); 
        $category = Category::where('bussiness_name','=',$bid)->orderby('business_cat_name', 'asc')->get(); 
        $other_first_language1 = Employee::where('id','=',$newclient12->other_first_language1)->first();
        $other_second_language1 = Employee::where('id','=',$newclient12->other_second_language1)->first();
        }
         
        $business = Business::orderBy('bussiness_name', 'asc')->get();
         
        $info = Contact_userinfo::where('user_id', '=', $id)->get();
        $info1 = Contact_userinfo::where('user_id', '=', $id)->first();
        $user_id  = Auth::user()->id;
        $business_cat_id  = Auth::user()->business_cat_id;
        $user = User::where('user_id', '=', $id)->first();
        $newclient = Commonregister::where('id',$id)->update(['newclient' => 2]);
        $subcustomer = User::get();
        $note = DB::table('notes')->get();
        $newlocations = DB::table('newlocations')->where('clientid',$id)->get();
        $position = Price::All();
        $currency = Currency::All();
        $typeofser = Typeofser::orderBy('typeofservice', 'asc')->get();
        $period = Period::All();
        
        $taxtitle = Taxtitle::All();
        $admin_notes = DB::table('notes')->where('type','=','admin')->where('userid','=',$id)->get();
        $clientser = DB::table('clientservices')->where('clientid','=',$id)->get();
        $clientser5 = DB::table('clientservices')->where('clientid','=',$id)->get()->first();
        $clientsertitle = DB::table('clientservicetitles')->where('clientid','=',$id)->where('clientid','=',$id)->get();
        $client = DB::table('notes')->where('admin_id','=',$id)->where('type','=','client')->get();
        $fsc = DB::table('notes')->where('admin_id','=','')->where('type','=','fsc')->get();
        $every = DB::table('notes')->where('type','=','Everybody')->get();
        $employee1 = Employee::where('check','=','1')->where('type','!=','clientemployee')->where('type','!=','Vendor')->where('type','!=','')->orderBy('firstName', 'asc')->get();
        $employee2 = Fscemployee::orderBy('name', 'asc')->get();
         $empname = Employee::where('check','=','1')->where('type','!=','clientemployee')->where('type','!=','Vendor')->where('type','!=','')->orderBy('firstName', 'asc')->get();   
        // echo "<pre>";
        //print_r($empname);die;        
        $common = DB::table('commonregisters')->select('commonregisters.worknew_emp','commonregisters.worknew_prospect','commonregisters.worknew_category','commonregisters.worknew_type','commonregisters.worknew_priority','commonregisters.worknew_duedate','commonregisters.worknew_details','commonregisters.worknew_note','commonregisters.worknew_prospect','commonregisters.type_of_entity_answer','commonregisters.contact_number as contact_number','commonregisters.work_year','commonregisters.work_status','commonregisters.work_annualfees','commonregisters.work_processingfees','commonregisters.work_totalamt','commonregisters.work_paidby','commonregisters.work_paymentmethod','commonregisters.work_note','commonregisters.work_officer','commonregisters.work_annualreceipt','commonregisters.work_renewyear','commonregisters.work_zip','commonregisters.work_renewperiod','commonregisters.work_address','commonregisters.work_city','commonregisters.work_state','commonregisters.work_address as work_address','commonregisters.work_changes as work_changes','commonregisters.id as cid','commonregisters.contact_title as contact_title','commonregisters.paymentmode11 as paymentmode11','commonregisters.carttype1 as carttype1','commonregisters.cartno1 as cartno1','commonregisters.acountname1 as acountname1','commonregisters.paymentnote1 as paymentnote1','commonregisters.acountname1 as acountname1','commonregisters.acountno1 as acountno1','commonregisters.routingno1 as routingno1','commonregisters.bankname1 as bankname1','commonregisters.maritial_spouse1 as maritial_spouse1','commonregisters.maritial_spouse as maritial_spouse','commonregisters.ext2 as ext2','commonregisters.telephoneNo2Type as telephoneNo2Type','commonregisters.motelephoneile1 as motelephoneile1','commonregisters.maritial_last_name as maritial_last_name','commonregisters.maritial_middle_name as maritial_middle_name','commonregisters.maritial_first_name as maritial_first_name','commonregisters.personalname as personalname','commonregisters.CL as CL','commonregisters.other_maritial_status1 as other_maritial_status1','commonregisters.other_maritial_status as other_maritial_status','commonregisters.other_dob_month as other_dob_month','commonregisters.other_dob_day as other_dob_day','commonregisters.other_spouse_month as other_spouse_month','commonregisters.other_spouse_day as other_spouse_day','commonregisters.other_marriage_month as other_marriage_month','commonregisters.other_marriage_day as other_marriage_day','commonregisters.other_ethnic as other_ethnic','commonregisters.other_main_language as other_main_language','commonregisters.other_first_language as other_first_language','commonregisters.other_second_language as other_second_language','commonregisters.emailbli1 as emailbli1','commonregisters.faxbli1 as faxbli1','commonregisters.faxbli3 as faxbli3','commonregisters.cartnote as cartnote','commonregisters.billingtoo as billingtoo','commonregisters.cartno as cartno','commonregisters.carttype as carttype','commonregisters.paymentmode1 as paymentmode1','commonregisters.acountname as acountname','commonregisters.acountno as acountno','commonregisters.routingno as routingno','commonregisters.bankname as bankname','commonregisters.paymentnote as paymentnote','commonregisters.paymentmode as paymentmode','commonregisters.locations as locations','commonregisters.multilocation as multilocation','commonregisters.subscription_answer3 as subscription_answer3','commonregisters.subscription_answer2 as subscription_answer2','commonregisters.subscription_answer1 as subscription_answer1','commonregisters.subscription_question3 as subscription_question3','commonregisters.subscription_question1 as subscription_question1','commonregisters.subscription_question2 as subscription_question2','commonregisters.user_answer3 as user_answer3','commonregisters.user_answer2 as user_answer2','commonregisters.user_answer1 as user_answer1','commonregisters.limited_answer3 as limited_answer3','commonregisters.limited_answer2 as limited_answer2','commonregisters.limited_answer1 as limited_answer1','commonregisters.user_question3 as user_question3','commonregisters.user_question2 as user_question2','commonregisters.user_question1 as user_question1','commonregisters.limited_question3 as limited_question3','commonregisters.limited_question2 as limited_question2','commonregisters.limited_question1 as limited_question1','commonregisters.useremail as useremail','commonregisters.user_resetdate as user_resetdate','commonregisters.useremail as useremail','commonregisters.user_resetdays as user_resetdays','commonregisters.user_active as user_active','commonregisters.limited_resetdate as limited_resetdate','commonregisters.limited_resetdays as limited_resetdays','commonregisters.limited_active as limited_active','commonregisters.limited_user as limited_user','commonregisters.subscription_answer1 as subscription_answer1','commonregisters.subscription_answer2 as subscription_answer2','commonregisters.subscription_answer3 as subscription_answer3','commonregisters.subscription_question3 as subscription_question3','commonregisters.subscription_question2 as subscription_question2','commonregisters.subscription_question1 as subscription_question1','commonregisters.subscription_resetdays as subscription_resetdays','commonregisters.subscription_resetdate as subscription_resetdate','commonregisters.subscription_active as subscription_active','commonregisters.subscription_lock as subscription_lock','commonregisters.subscription_user as subscription_user','commonregisters.user_cell as user_cell','commonregisters.user_email as user_email','commonregisters.user_name as user_name','commonregisters.creationdate as creationdate','commonregisters.nametype as nametype','commonregisters.etelephone2 as etelephone2','commonregisters.eext2 as eext2','commonregisters.eteletype2 as eteletype2','commonregisters.eext1  as eext1','commonregisters.eteletype1 as eteletype1','commonregisters.filename as filename','commonregisters.etelephone1 as etelephone1','commonregisters.businessext as businessext','commonregisters.businesstype as businesstype','commonregisters.contact_address1 as contact_address1','commonregisters.contact_address2 as contact_address2','commonregisters.city_1 as city_1','commonregisters.state_1 as state_1','commonregisters.zip_1 as zip_1','commonregisters.mobile_1 as mobile_1','commonregisters.mobiletype_1 as mobiletype_1','commonregisters.ext2_1 as ext2_1','commonregisters.mobile_2 as mobile_2','commonregisters.mobiletype_2 as mobiletype_2','commonregisters.ext2_2 as ext2_2','commonregisters.contact_fax_1 as contact_fax_1','commonregisters.email_1 as email_1','commonregisters.minss as minss','commonregisters.firstname as firstname','commonregisters.middlename as middlename','commonregisters.lastname as lastname','commonregisters.mailing_address1 as mailing_address1','commonregisters.bussiness_zip as bussiness_zip','commonregisters.due_date as due_date','commonregisters.department as department','commonregisters.type_of_activity as type_of_activity','commonregisters.county_no as county_no','commonregisters.county_name as county_name','commonregisters.level as level','commonregisters.setup_state as setup_state','commonregisters.business_state as business_state','commonregisters.business_city as business_city','commonregisters.business_country as business_country','commonregisters.business_address as business_address','commonregisters.business_store_name as business_store_name','commonregisters.mailing_address as mailing_address','commonregisters.legalname as legalname','commonregisters.dbaname as dbaname','commonregisters.mailing_city as mailing_city','commonregisters.mailing_state as mailing_state','commonregisters.mailing_zip as mailing_zip','commonregisters.user_type as user_type','commonregisters.user_type as user_type','commonregisters.status as status','commonregisters.company_name as company_name','commonregisters.business_name as business_name','commonregisters.first_name as first_name','commonregisters.middle_name as middle_name','commonregisters.last_name as last_name','commonregisters.email as email','commonregisters.address as address','commonregisters.address1 as address1','commonregisters.city as city','commonregisters.stateId as stateId','commonregisters.zip as zip','commonregisters.countryId as countryId','commonregisters.mobile_no as mobile_no','commonregisters.business_no as business_no','commonregisters.business_fax as business_fax','commonregisters.website as website','commonregisters.user_type as user_type','commonregisters.business_id as business_id','commonregisters.business_cat_id as business_cat_id','commonregisters.business_brand_id as business_brand_id','commonregisters.business_brand_category_id as business_brand_category_id','businesses.bussiness_name as bussiness_name','categories.business_cat_name as business_cat_name','business_brands.business_brand_name as business_brand_name','categorybusinesses.business_brand_category_name as business_brand_category_name')
        ->leftJoin('categories', function($join){ $join->on('commonregisters.business_cat_id', '=', 'categories.id');})
        ->leftJoin('businesses', function($join){ $join->on('commonregisters.business_id', '=', 'businesses.id');})
        ->leftJoin('business_brands', function($join){ $join->on('commonregisters.business_brand_id', '=', 'business_brands.id');})
        ->leftJoin('categorybusinesses', function($join){ $join->on('commonregisters.business_brand_category_id', '=', 'categorybusinesses.id');})
        ->where('commonregisters.id', '=', "$id")->get()->first();  
        
        $logo = Logo::where('id','=',1)->first();
        $user_id  = Auth::user()->user_id;
        $employee = Employee::where('id',$user_id)->first();
        
        return view('fscemployee/worknew/worknew',compact(['logo','employee2','workcategory','empname','other_first_language1','other_second_language1','ethnic','language','newlocations','subcustomer','employee1','employee','clientser5','clientser','clientsertitle','note','every','fsc','taxstate','admin_notes','client','common','category','business','businessbrand','cb','user','info','info1','position','currency','period','typeofser','taxtitle']));
    }
    
    
    // function worktodo()
    // {
    //       $worktodo = DB::table('commonregisters')->select('employees.employee_id','employees.firstName','employees.middleName','employees.lastName','commonregisters.id','client_worknew.id','client_worknew.clientid','client_worknew.worknew_prospect','client_worknew.worknew_category','client_worknew.worknew_type','client_worknew.worknew_priority','client_worknew.worknew_duedate','client_worknew.worknew_emp','client_worknew.worknew_details','client_worknew.worknew_note')
    //     ->leftJoin('employees', function($join){ $join->on('commonregisters.worknew_emp', '=', 'employees.employee_id');})
    //     ->leftJoin('client_worknew', function($join){ $join->on('commonregisters.id', '=', 'client_worknew.clientid');})
    //     ->where('commonregisters.id','=',$id)->where('commonregisters.worknew_prospect','!=','')->get();
    //     // echo "<pre>";
    //     // print_r($worktodo);die;
    //     return view('fscemployee/worktodo',compact(['empname']));
    // }
    
    function fetchemp(Request $request)
    {
     if($request->get('query'))
     {
      $query = $request->get('query');
     // $data = DB::table('commonregisters')->where('filename', 'LIKE', "%{$query}%")->get();
        
      $data = DB::table('commonregisters')->where('filename', 'LIKE', "%{$query}%")->orwhere('company_name', 'LIKE', "%{$query}%")->orwhere('business_no', 'LIKE', "%{$query}%")->orwhere('business_name', 'LIKE', "%{$query}%")->get();
     
      $output = '';
      $i;
      foreach($data as $row)
      {
          if($row->business_id =='6')
       {
           $names=$row->first_name.' '.$row->middle_name.' '.$row->last_name;
       }
       else
       {
           $names=$row->company_name;
       }
       $output .= '<li><a href="'.url('fscemployee/worknew?id=').''.$row->id.'"><span class="clientalign">'.$row->filename.'</span> <span class="entityname">'.$names.'</span></a></li>';
      }
      $output .= '';
      echo $output;
     }
    }
    
    function fetch1()
    {
       //echo "<pre>"; print_r($_POST);exit;
       
        
        $id = DB::table('client_worknew')
            ->insert([
                'clientid' => $_POST['clientsid'],
                'worknew_category' =>$_POST['worknew_category'],
                'worknew_type' =>$_POST['worknew_type'],
                'worknew_priority' =>$_POST['worknew_priority'],
                'worknew_duedate' =>date('Y-m-d',strtotime($_POST['worknew_duedate'])),
                'worknew_emp' =>$_POST['worknew_emp'],
                'worknew_details' =>$_POST['worknew_details'],
                'worknew_note' =>$_POST['worknew_note'],
                'worknew_prospect'=>'Client',
            ]);
    
       return redirect('fscemployee/worktodo');
        //return redirect('fscemployee/worktodo?id='.$_POST['clientsid']);
           
    }
    
    function fetch2()
    {
      // echo "<pre>"; print_r($_POST);exit;
       
        
        $id = DB::table('client_worknew')
            ->insert([
                'adminid' => 1,
                
                'worknew_petname' =>$_POST['worknew_petname'],
                'worknew_fname' =>$_POST['worknew_fname'],
                'worknew_mname' =>$_POST['worknew_mname'],
                'worknew_lname' =>$_POST['worknew_lname'],
                'worknew_telephone' =>$_POST['worknew_telephone'],
                'worknew_email' =>$_POST['worknew_email'],
                
                
                'worknew_category' =>$_POST['worknew_category'],
                'worknew_type' =>$_POST['worknew_type'],
                'worknew_priority' =>$_POST['worknew_priority'],
                'worknew_duedate' =>date('Y-m-d',strtotime($_POST['worknew_duedate'])),
                'worknew_emp' =>$_POST['worknew_emp'],
                'worknew_details' =>$_POST['worknew_details'],
                'worknew_note' =>$_POST['worknew_note'],
                'worknew_prospect'=>'Admin',
            ]);
    
       return redirect('fscemployee/worktodo');
        //return redirect('fscemployee/worktodo?id='.$_POST['clientsid']);
           
    }

    
    public function workcategorys()
    {   
        $newopt = Input::get('newopt');
  
        if (DB::table('workcategories')->where('category_name', '=', $newopt)->exists())   {
                //return response()->json(['status' => 'error']);
                }   
                else 
                { 
        $position = new Workcategory;
        $position->category_name = $newopt;
       // $position->sign = $sign;
        $position->save();
         if ($position) {
        return response()->json([
            'status'     => 'success',
             'newopt'     => $newopt]);
            } else {
                return response()->json([
                    'status' => 'error']);
            }
        }
    }
    
    
    public function create()
    {
 
       return view('fscemployee/worknew/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $this->validate($request,[
            'states' =>'required',
            'tax_authority' =>'required',
            'country_code' =>'required',
            'telephone' =>'required',
            'address' =>'required', 
'city' =>'required', 
'state' =>'required', 
'zip' =>'required',                   
'website' =>'required',                     
        ]);
        $position = new City;
        $position->authority_name= $request->states;
        $position->short_name= $request->tax_authority;
        $position->type_of_tax= $request->country_code;
        $position->telephone= $request->telephone;
        $position->address= $request->address;
        $position->city= $request->city;
        $position->state= $request->state;
        $position->zip= $request->zip;
        $position->website= $request->website;

        $position->save();
        return redirect('fscemployee/worknew')->with('success','State Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\State  $state
     * @return \Illuminate\Http\Response
     */
    public function show(State $state)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\State  $state
     * @return \Illuminate\Http\Response
     */
    public function edit(City $city)
    {
        return View('fac-Bhavesh-0554.worknew.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\State  $state
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, City $city)
    {
        $this->validate($request,[
            'states' =>'required',
            'tax_authority' =>'required',
            'country_code' =>'required',
            'telephone' =>'required',
            'address' =>'required', 
'city' =>'required', 
'state' =>'required', 
'zip' =>'required',                   
'website' =>'required',                     
        ]);
        $position = $city;
        
     
        $position->authority_name= $request->states;
        $position->short_name= $request->tax_authority;
        $position->type_of_tax= $request->country_code;
        $position->telephone= $request->telephone;
        $position->address= $request->address;
        $position->city= $request->city;
        $position->state= $request->state;
        $position->zip= $request->zip;
        $position->website= $request->website;
        $position->update();
        return redirect('fscemployee/work-to-do')->with('success','State Update Successfully');
    }


    public function getcounty(Request $request)
    {
        $data = taxstate::select('county','id','countycode')->where('state',$request->id)->take(1000)->get();
        return response()->json($data);  
    }
    
    public function getcountycode(Request $request)
    {
        $data = taxstate::select('countycode','id')->where('county',$request->id)->take(1000)->get();
        return response()->json($data);  
    }

    public function destroydworkcat(Request $request,$id,$cid)
    {
        DB::table('workcategories')->where('id','=',$id)->delete();
        return redirect('fscemployee/worknew?id='.$cid)->with('success','Your Worknew Successfully Deleted');;
    }

    
    // public function destroydworkcat(Request $request,$id)
    // {
    //     DB::table('workcategories')->where('id','=',$id)->delete();
    //     return redirect(route('fscemployee/workrecord'));
    //     return redirect(route('fscemployee/workrecord'))->with('success','Client Document Successfully Deleted');
    // }
}
