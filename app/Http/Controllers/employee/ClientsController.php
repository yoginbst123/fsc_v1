<?php
namespace App\Http\Controllers\employee;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Front\Commonregister;
use App\Model\BusinessBrand;
use App\Model\Category;
use App\Model\Business;
use App\User;
use App\Model\Categorybusiness;
use App\Model\Contact_userinfo;
use App\Mail\Activationmail;
use DB;
use App\Model\Currency;
use Hash;
use Auth;
use App\Model\Period;
use App\Model\Taxtitle;
use App\Model\taxstate;
use Carbon;
use App\Model\taxfederal;
use App\Model\Question;
use App\Model\Answer;
use App\Model\Typeofser;
use App\Model\client_shareholder;
use App\Model\clientprimaryquestion;
use App\Model\client_professional;
use App\Model\Clientnupload;
use App\Model\FilingFrequency;
use App\Model\PaymentFrequency;
use App\Model\formationsetup;
use App\Model\Employee;
use App\Model\Logo;
use App\Model\QuestionSection;
class ClientsController extends Controller
{
public function __construct()
{
    $this->middleware('auth:employee');
}
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
public function index(Request $request)
{
$name = $request->input('user_type');
 if(isset($name))
     {
        $common = DB::table('commonregisters')->select('commonregisters.lastname as lastname','commonregisters.middlename as middlename','commonregisters.firstname as firstname','commonregisters.id as cid','commonregisters.nametype as nametype','commonregisters.filename as filename','commonregisters.created_at as created_at','commonregisters.mailing_address1 as mailing_address1','commonregisters.bussiness_zip as bussiness_zip','commonregisters.newclient as newclient','commonregisters.due_date as due_date','commonregisters.department as department','commonregisters.type_of_activity as type_of_activity','commonregisters.county_no as county_no','commonregisters.county_name as county_name','commonregisters.level as level','commonregisters.setup_state as setup_state','commonregisters.business_state as business_state','commonregisters.business_city as business_city','commonregisters.business_country as business_country','commonregisters.business_address as business_address','commonregisters.business_store_name as business_store_name','commonregisters.mailing_address as mailing_address','commonregisters.legalname as legalname','commonregisters.dbaname as dbaname','commonregisters.mailing_city as mailing_city','commonregisters.mailing_state as mailing_state','commonregisters.mailing_zip as mailing_zip','commonregisters.user_type as user_type','commonregisters.user_type as user_type','commonregisters.status as status','commonregisters.company_name as company_name','commonregisters.business_name as business_name1','commonregisters.first_name as first_name','commonregisters.middle_name as middle_name','commonregisters.last_name as last_name','commonregisters.email as email','commonregisters.address as address','commonregisters.address1 as address1','commonregisters.city as city','commonregisters.stateId as stateId','commonregisters.zip as zip','commonregisters.countryId as countryId','commonregisters.mobile_no as mobile_no','commonregisters.business_no as business_no','commonregisters.business_fax as business_fax','commonregisters.website as website','commonregisters.user_type as user_type','commonregisters.business_id as business_id','commonregisters.business_cat_id as business_cat_id','commonregisters.business_brand_id as business_brand_id','commonregisters.business_brand_category_id as business_brand_category_id','businesses.bussiness_name as bussiness_name' ,'categories.business_cat_name as business_cat_name','business_brands.business_brand_name as business_brand_name','categorybusinesses.business_brand_category_name as business_brand_category_name')
->leftJoin('categories', function($join){ $join->on('commonregisters.business_cat_id', '=', 'categories.id');})
->leftJoin('businesses', function($join){ $join->on('commonregisters.business_id', '=', 'businesses.id');})
->leftJoin('business_brands', function($join){ $join->on('commonregisters.business_brand_id', '=', 'business_brands.id');})
->leftJoin('categorybusinesses', function($join){ $join->on('commonregisters.business_brand_category_id', '=', 'categorybusinesses.id');})
->where('commonregisters.user_type', '=', $name)->orderBy('filename', 'asc')->where('commonregisters.filename', '!=', "")->get();
        }
     else
     {
        $common = DB::table('commonregisters')->select('commonregisters.lastname as lastname','commonregisters.middlename as middlename','commonregisters.firstname as firstname','commonregisters.id as cid','commonregisters.nametype as nametype','commonregisters.created_at as created_at','commonregisters.filename as filename','commonregisters.mailing_address1 as mailing_address1','commonregisters.newclient as newclient','commonregisters.bussiness_zip as bussiness_zip','commonregisters.due_date as due_date','commonregisters.department as department','commonregisters.type_of_activity as type_of_activity','commonregisters.county_no as county_no','commonregisters.county_name as county_name','commonregisters.level as level','commonregisters.setup_state as setup_state','commonregisters.business_state as business_state','commonregisters.business_city as business_city','commonregisters.business_country as business_country','commonregisters.business_address as business_address','commonregisters.business_store_name as business_store_name','commonregisters.mailing_address as mailing_address','commonregisters.legalname as legalname','commonregisters.dbaname as dbaname','commonregisters.mailing_city as mailing_city','commonregisters.mailing_state as mailing_state','commonregisters.mailing_zip as mailing_zip','commonregisters.user_type as user_type','commonregisters.user_type as user_type','commonregisters.status as status','commonregisters.company_name as company_name','commonregisters.business_name as business_name1','commonregisters.first_name as first_name','commonregisters.middle_name as middle_name','commonregisters.last_name as last_name','commonregisters.email as email','commonregisters.address as address','commonregisters.address1 as address1','commonregisters.city as city','commonregisters.stateId as stateId','commonregisters.zip as zip','commonregisters.countryId as countryId','commonregisters.mobile_no as mobile_no','commonregisters.business_no as business_no','commonregisters.business_fax as business_fax','commonregisters.website as website','commonregisters.user_type as user_type','commonregisters.business_id as business_id','commonregisters.business_cat_id as business_cat_id','commonregisters.business_brand_id as business_brand_id','commonregisters.business_brand_category_id as business_brand_category_id','businesses.bussiness_name as bussiness_name' ,'categories.business_cat_name as business_cat_name','business_brands.business_brand_name as business_brand_name','categorybusinesses.business_brand_category_name as business_brand_category_name')
->leftJoin('categories', function($join){ $join->on('commonregisters.business_cat_id', '=', 'categories.id');})
->leftJoin('businesses', function($join){ $join->on('commonregisters.business_id', '=', 'businesses.id');})
->leftJoin('business_brands', function($join){ $join->on('commonregisters.business_brand_id', '=', 'business_brands.id');})
->leftJoin('categorybusinesses', function($join){ $join->on('commonregisters.business_brand_category_id', '=', 'categorybusinesses.id');})->where('commonregisters.filename', '!=', "")->orderBy('filename', 'asc')
->get();  
     }
 $logo = Logo::where('id','=',1)->first();
  $user_id  = Auth::user()->user_id;
$employee = Employee::where('id',$user_id)->first();
return view('fscemployee.clients.clients',compact(['common','employee','logo']));
}
/**
* Show the form for creating a new resource.
*
* @return \Illuminate\Http\Response
*/
public function create()
{   $category = Category::All();   
    $business = Business::all();
    $businessbrand = BusinessBrand::All(); 
    $cb= Categorybusiness::All();  
    return view('fac-Bhavesh-0554/clientsetup/create',compact(['business','category','cb','businessbrand']));
}
/**
* Store a newly created resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @return \Illuminate\Http\Response
*/
public function store(Request $request)
{
$this->validate($request,[
    'filename'=>'required',
    'status'=>'required',
    'business_name'=>'required',
    'business_catagory_name'=>'required',
    'company_name'=>'required',
    'first_name'=>'required',
    'middle_name'=>'required',
    'last_name'=>'required',
    'legalname'=>'required',
    'dbaname'=>'required',
    'address'=>'required',
    'address1'=>'required',
    'city'=>'required',
    'countryId'=>'required',
    'stateId'=>'required',
    'zip'=>'required', 
    'second_address'=>'required',
    'second_address1'=>'required',
    'second_city'=>'required',
    'second_state'=>'required',
    'second_zip'=>'required',
    'email'=>'required|unique:commonregisters,email',
    'mobile_no'=>'required',
    'business_no'=>'required',
    'business_fax'=>'required',
    'website'=>'required',
    'business_brand_name'=>'required',
    'business_brand_category_name'=>'required',
    'business_store_name'=>'required',
    'business_address'=>'required',
    'business_city'=>'required',
    'business_country'=>'required',  
    'business_state'=>'required',
    'bussiness_zip'=>'required',
    'level'=>'required',
    'county_no'=>'required',
    'county_name'=>'required',
    'type_of_activity'=>'required',
    'department'=>'required',           
    ]);

  $businessbrand = new Commonregister;
  $businessbrand->filename = $request->filename;
  $businessbrand->status = $request->status;
  $businessbrand->business_id = $request->business_id;  
  $businessbrand->business_name = $request->business_name;      
  $businessbrand->business_cat_id = $request->business_catagory_name;
  $businessbrand->company_name= $request->company_name;
  $businessbrand->first_name = $request->first_name;
  $businessbrand->middle_name = $request->middle_name;      
  $businessbrand->last_name = $request->last_name; 
  $businessbrand->legalname= $request->legalname;
  $businessbrand->dbaname = $request->dbaname;
  $businessbrand->address = $request->address;      
  $businessbrand->address1 = $request->address1;
  $businessbrand->city= $request->city;        
  $businessbrand->countryId = $request->countryId;      
  $businessbrand->stateId = $request->stateId; 
  $businessbrand->zip = $request->zip;
  $businessbrand->mailing_address= $request->second_address;
  $businessbrand->mailing_address1 = $request->second_address1;
  $businessbrand->mailing_city = $request->second_city;      
  $businessbrand->mailing_state = $request->second_state;
  $businessbrand->mailing_zip = $request->second_zip;
  $businessbrand->email = $request->email;
  $businessbrand->mobile_no = $request->mobile_no;  
  $businessbrand->business_no= $request->business_no;
  $businessbrand->business_fax = $request->business_fax;
  $businessbrand->website = $request->website;      
  $businessbrand->business_brand_id = $request->business_brand_name;
  $businessbrand->business_brand_category_id= $request->business_brand_category_name;
  $businessbrand->business_store_name = $request->business_store_name;
  $businessbrand->business_address = $request->business_address;      
  $businessbrand->business_city = $request->business_city; 
  $businessbrand->business_country= $request->business_country;
  $businessbrand->business_state = $request->business_state;
  $businessbrand->bussiness_zip = $request->bussiness_zip;      
  $businessbrand->level = $request->level;
  $businessbrand->setup_state= $request->setup_state;        
  $businessbrand->county_name = $request->county_name;      
  $businessbrand->county_no = $request->county_no; 
  $businessbrand->type_of_activity = $request->type_of_activity;
  $businessbrand->department= $request->department;
  $businessbrand->due_date = $request->due_date; 
  $businessbrand->user_type = $request->user_type;        
  $businessbrand->save();
if($request->status=='Pending' or $request->status=='Hold')
{
$name = $request->first_name .' '. $request->middle_name .' '. $request->last_name;   
$status = $request->status;
$email = $request->email;
$name = $request->first_name;  
DB::table('users')->where('user_id', $id)->update(['user_type' =>$user_type,'type'=>2,'name'=>$name]);
$returnValue = DB::table('users')->where('email', '=', $email)->update(['user_type' =>$user_type,'type'=>2,'name'=>$name]);
$data = array('email' => $email, 'first_name' => $name, 'from' => 'vijay@businesssolutionteam.com');       
\Mail::send('admin/hold', $data, function( $message ) use ($data)
{
$message->to( $data['email'] )->from( $data['from'], $data['first_name'])->subject( 'Welcome!' );
});
}
else
{
$status = $request->status;
$email = $request->email;
$name = $request->first_name;
$password =  mt_rand();
$user_type=  $request->user_type;
$client_id = $request->client_id;
$user = User::where('email', '=', $email)->first();
if ($user === null){
$insert = DB::insert("insert into users(`name`,`email`,`password`,`user_id`,`user_type`,`type`) values('".$name."','".$email."','".bcrypt($password)."','".$cmppcode."','".$user_type."','1')");
$data = array('email' => $email, 'first_name' => $name, 'from' => $email, 'password' =>$password,'client'=>$client_id);       
\Mail::send( 'activation', $data, function( $message ) use ($data)
{
$message->to( $data['email'] )->from( $data['from'], $data['first_name'], $data['password'], $data['client'] )->subject( 'Welcome!' );
});      
}
}
$contact_person_name = $request->contact_person_name;
$business= $request->business;
$business_fax1= $request->business_fax1;
$residence= $request->residence;
$cell= $request->cell;
$residence_fax = $request->residence_fax;
$contemail = $request->contemail;
$telephone = $request->telephone;
$conid = $request->conid;
$i = 0;
$info = Contact_userinfo::where('user_id', '=',$cmppcode)->first();
foreach($contact_person_name as $post)
{ 
$conid1 =$conid[$i];
$business1 =$business[$i];
$business_fax11 =$business_fax1[$i];
$residence1 =$residence[$i];
$cell1 =$cell[$i];
$residence_fax1 =$residence_fax[$i];
$contemail1 =$contemail[$i]; 
$telephone1 =$telephone[$i]; 
$i++; 
$insert2 = DB::insert("insert into contact_userinfos(`contact_person_name`,`user_id`,`business`,`business_fax`,`residence`,`cell`,`residence_fax`,`email`,`telephone`) values('".$post."','".$cmppcode."','".$business1."','".$business_fax11."','".$residence1."','".$cell1."','".$residence_fax1."','".$contemail1."','".$telephone1."')");
if(empty($conid))
{
}
else
{
$affectedRows = Contact_userinfo::where('id', '=',$conid1)->delete();
}
}
 return redirect('fac-Bhavesh-0554/clientsetup')->with('success','You Are SuccessFully Registered');
}
/**
* Display the specified resource.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function show($id)
{
$common = Commonregister::where('id',$id)->first();
return View::make('fac-Bhavesh-0554.clientsetup.clientsetup',compact(['common']));
}



public function edit($id)
    {
        $categoriesname = DB::table('categories')->select('categories.id','categories.licenses','commonregisters.business_cat_id','commonregisters.id as cid')
            ->leftJoin('commonregisters', function($join){ $join->on('commonregisters.business_cat_id', '=', 'categories.id');})
            ->where('commonregisters.id', '=',$id)->first();
    //  echo $categoriesname->licenses;
     // print_r($categoriesname);  exit;  
     if(isset($categoriesname))
     {
   $strexp=explode(",",$categoriesname->licenses);
    $datalicense = DB::table('license')->whereIn('id', $strexp)->orderby('id', 'asc')->get();
// echo "<pre>";print_r($datalicense);exit;
     }
     else
     {
        $datalicense=''; 
     }
   
         
          $clienttolicense = DB::table('client_to_license')->select('client_to_license.client_id','client_to_license.licenseid','client_to_license.licensestatus',
          'license.id','license.licensename')
            ->leftJoin('license', function($join){ $join->on('client_to_license.licenseid', '=', 'license.id');})
            ->where('client_to_license.client_id', '=',$id)->get();
       
   
   $accountingtable=DB::table('accounting_table')->where('clientid','=',$id)->get();
      
      
        $categoryname = DB::table('commonregisters')->select('commonregisters.id','commonregisters.id as cid','commonregisters.business_cat_id','categories.sic','categories.naics')
            ->leftJoin('categories', function($join){ $join->on('categories.id', '=', 'commonregisters.business_cat_id');})
            ->where('commonregisters.id', '=',$id)->first();
        
        
        $employee1 = DB::table('employees')->select('employees.emp_service1','employees.emp_service2','employees.acc_service_1','employees.acc_service_2','employees.id','employees.team','employees.type','employees.firstName','employees.middleName','employees.lastName','teams.id as teamid','teams.team as teams')
            ->leftJoin('teams', function($join){ $join->on('teams.id', '=', 'employees.team');})
            ->where('employees.check','=','1')->where('employees.type','!=','clientemployee')->where('employees.type','!=','Vendor')->where('employees.type','!=','')->orderBy('employees.firstName', 'asc')->get();
        
        $employeeassign = DB::table('clientservices')->select('teams.team as Team','teams.id as tids','typeofser.shortcode','clientservices.clientid','typeofser.id as tid','typeofser.typeofservice as servicename','clientservices.typeofservice','clientservices.employee_id','employees.id','employees.firstName','employees.middleName','employees.lastName')
            ->leftJoin('typeofser', function($join){ $join->on('typeofser.id', '=', 'clientservices.typeofservice');})
            ->leftJoin('employees', function($join){ $join->on('employees.id', '=', 'clientservices.employee_id');})
             ->leftJoin('teams', function($join){ $join->on('teams.id', '=', 'employees.team');})
           
            ->where('clientservices.clientid', '=',$id)->orderBy('typeofser.typeofservice', 'asc')->get();
        
            
        $employeetaxassign = DB::table('client_to_taxation')->select('taxtitles.shortcode','teams.team as Team','teams.id as tids','client_to_taxation.taxation_service_period','client_to_taxation.clientid','taxtitles.id as tid','taxtitles.title as servicename','client_to_taxation.taxation_service','client_to_taxation.employee_id','employees.id','employees.firstName','employees.middleName','employees.lastName')
            ->leftJoin('taxtitles', function($join){ $join->on('taxtitles.id', '=', 'client_to_taxation.taxation_service');})
            ->leftJoin('employees', function($join){ $join->on('employees.id', '=', 'client_to_taxation.employee_id');})
            ->leftJoin('teams', function($join){ $join->on('teams.id', '=', 'employees.team');})
           
            ->where('client_to_taxation.clientid', '=',$id)->orderBy('taxtitles.title', 'asc')->get();
      
        $user_id  = Auth::user()->user_id;
         $employee = Employee::where('id',$user_id)->first();
        $commoncounty = DB::table('commonregisters')->select('commonregisters.location_county','commonregisters.physical_county_no','commonregisters.id')->where('commonregisters.id', '=', "$id")->get()->first();
      $taxstatecounty = DB::table('taxstates')->select('countycode','county','county_authority_name','personal_due_date')->where('countycode','=',$commoncounty->physical_county_no)->where('county','=',$commoncounty->location_county)->first();
      // echo $taxstate11->county;
      $commonfederal = DB::table('commonregisters')->select('commonregisters.typeofcorp1','commonregisters.id')->where('commonregisters.id', '=', "$id")->first();
      $pricefederal = taxfederal::where('formname','=',$commonfederal->typeofcorp1)->first();
      //echo "<pre>"; print_r($pricefederal);exit;
       //  $taxstatecounty = taxstate::where('countycode','=',"'".$commoncounty->physical_county_no."'")->where('county','=',"'".$commoncounty->location_county."'")->first();
       // print_r($taxstatecounty);
         $lastformation = DB::table('client_formation')->whereRaw('FIND_IN_SET("'.date('Y').'",formation_yearvalue)')->where('client_id', '=',$id)->orderby('id', 'desc')->first();
        //print_r($lastformation);exit;
         $lastformation1 = DB::table('client_formation')->where('client_id', '=',$id)->orderby('id', 'desc')->first();
        //print_r($lastformation);exit;
        
         $carddatalist = DB::table('cardmaster')->select('*')->orderBy('cardname', 'ASC')->get(); 
         $bankdatalist = DB::table('bankmaster')->select('*')->orderBy('bankname', 'ASC')->get(); 
         $buslicense = DB::table('client_license')->select('*')->where('client_id', '=',$id)->orderBy('id', 'DESC')->first(); 
         
         $statelist = DB::table('state')->select('*')->where('countrycode', '=','USA')->orderBy('code', 'ASC')->get(); 
         
         $questionsection = QuestionSection::All(); 
         $position = formationsetup::where('type1','=','License')->where('question','=','Tobacco License')->get();    
         $taxstate = taxstate::All();
         $category = Category::orderBy('business_cat_name', 'asc')->get();   
         $business = Business::all();
         $empname = Employee::orderBy('firstName', 'asc')->get();   
         $period = Period::All();
         $businessbrand = BusinessBrand::All(); 
         $clientsertitle = DB::table('clientservicetitles')->where('clientid','=',$id)->where('clientid','=',$id)->get();
         $cb = Categorybusiness::All();
         $primaryquestion = clientprimaryquestion::All();
         $answer= Answer::orderBy('answer', 'asc')->get();
         $question= Question::All();
         $payroll = taxfederal::orderBy('authority_name', 'asc')->get(); 
         $info = Contact_userinfo::where('user_id', '=', $id)->get();
         $shareholder=client_shareholder::where('client_id', '=', $id)->orderBy('id', 'asc')->get();
         $total = client_shareholder::where('client_id', '=', $id)->sum('agent_per');
         $admin_shareholder=client_shareholder::orderBy('agent_position', 'ASC')->get();
         $agent=client_shareholder::where('agent_position','=','Agent')->get()->first();
         $ceo=client_shareholder::where('agent_position','=','CEO')->get()->first();
         $cfo=client_shareholder::where('agent_position','=','CFO')->get()->first();
         $sece=client_shareholder::where('agent_position','=','Secretary')->get()->first();
         $sec=client_shareholder::where('agent_position','=','Sec')->get()->first();
         $admin_professional =client_professional::where('client_id', '=', $id)->get();
         $info1 = Contact_userinfo::where('user_id', '=', $id)->first();
         $user = User::where('user_id', '=', $id)->first();
         $upload = Clientnupload::All();
         $fsc = DB::table('notes')->where('admin_id','=','')->where('type','=','fsc')->get();
         $banking  = DB::table('banking_informations')->where('bankname','!=','')->where('client_id','=',$id)->get();
         $banking1  = DB::table('credit_card_informations')->where('business_credit_card_number','!=','')->where('client_id','=',$id)->get();
         //$payroll->uniques;
         $pay = PaymentFrequency::get()->first();
         $fill = FilingFrequency::get()->first(); 
         $newclient = Commonregister::where('id',$id)->update(['newclient' => 2]);           
         $admin_notes = DB::table('notes')->where('admin_id','=',$id)->where('type','=','client')->get(); 
         $typeofentity = DB::table('typeofentity')->orderBy('typeentity','=','asc')->get();
         
         $employeeteam = DB::table('clientservices')->select('clientservices.clientid','clientservices.employee_id','clientservices.typeofservice',
         'employees.id','employees.team','teams.id as teamid','teams.team as teams')
                    ->leftJoin('employees', function($join){ $join->on('clientservices.employee_id', '=', 'employees.id');})
                    ->leftJoin('teams', function($join){ $join->on('teams.id', '=', 'employees.team');})
                    
                    ->where('clientservices.clientid', '=', "$id")->where('clientservices.typeofservice', '=', "2")->get()->first();
        
          $taxservices = DB::table('client_to_taxation')->where('clientid','=',$id)->get();
         $taxtitle = Taxtitle::orderBy('title', 'asc')->get();
        $taxclientser = DB::table('taxclientservices')->where('clientid','=',$id)->get();
        $taxservices5 = DB::table('client_to_taxation')->where('clientid','=',$id)->first();
       $taxservices1 = DB::table('client_to_taxation')->where('clientid','=',$id)->orderBy('taxation_service', 'asc')->get();
        
          $common = DB::table('commonregisters')->select('commonregisters.mailing_address_check','commonregisters.corp_llc','commonregisters.contactnametype','commonregisters.taxation_register_entity','commonregisters.basic_guardian1','commonregisters.basic_guardian2','commonregisters.guardian','commonregisters.guardian2','commonregisters.faxbli3_g','commonregisters.productid','commonregisters.accounting_period','commonregisters.payroll_period','commonregisters.maritial_last_name','commonregisters.maritial_middle_name','commonregisters.maritial_first_name','commonregisters.personalname','commonregisters.other_maritial_status1','commonregisters.CL','commonregisters.tank_state','commonregisters.tank_renew','commonregisters.tank_annual','commonregisters.tank_date','commonregisters.tank_note','commonregisters.food_state','commonregisters.food_renew','commonregisters.food_annual','commonregisters.food_date','commonregisters.food_note','commonregisters.common_status','commonregisters.formation_start_date','commonregisters.formation_type_of_entity','commonregisters.adminonly','commonregisters.supervisor',
         'commonregisters.fscee','commonregisters.fscuser','commonregisters.client','commonregisters.alluser','commonregisters.work_paidby','commonregisters.type_of_entity_answer',
         'commonregisters.work_status','commonregisters.billingtoo','commonregisters.id','commonregisters.soscertificate','commonregisters.sosaoi','commonregisters.address_type',
         'commonregisters.formation_city','commonregisters.formation_state','commonregisters.formation_zip','commonregisters.formation_address','commonregisters.email_1 as email_1',
         'commonregisters.businessext as businessext','commonregisters.creationdate as creationdate','commonregisters.user_answer3 as user_answer3',
         'commonregisters.user_answer2 as user_answer2','commonregisters.user_answer1 as user_answer1','commonregisters.user_cell as user_cell',
         'commonregisters.user_resetdays as user_resetdays','commonregisters.user_question3 as user_question3','commonregisters.user_question2 as user_question2',
         'commonregisters.user_question1 as user_question1','commonregisters.user_email as user_email','commonregisters.user_name as user_name','commonregisters.user_active as user_active',
         'commonregisters.limited_answer3 as limited_answer3','commonregisters.limited_answer2 as limited_answer2','commonregisters.limited_answer1 as limited_answer1',
         'commonregisters.limited_resetdate as limited_resetdate','commonregisters.limited_resetdays as limited_resetdays','commonregisters.limited_question3 as limited_question3',
         'commonregisters.limited_question2 as limited_question2','commonregisters.limited_question1 as limited_question1','commonregisters.limited_user as limited_user',
         'commonregisters.limited_active as limited_active','commonregisters.subscription_answer3 as subscription_answer3','commonregisters.subscription_answer2 as subscription_answer2',
         'commonregisters.subscription_answer1 as subscription_answer1','commonregisters.subscription_resetdate as subscription_resetdate',
         'commonregisters.subscription_resetdays as subscription_resetdays','commonregisters.subscription_user as subscription_user','commonregisters.subscription_lock as subscription_lock',
         'commonregisters.subscription_active as subscription_active','commonregisters.locations as locations','commonregisters.multilocation as multilocation',
         'commonregisters.businesstype as businesstype','commonregisters.emailbli1 as emailbli1','commonregisters.faxbli1 as faxbli1','commonregisters.billingtoo11 as billingtoo11',
         'commonregisters.business_address_2 as business_address_2','commonregisters.business_address_3 as business_address_3','commonregisters.business_city_1 as business_city_1',
         'commonregisters.business_state_1 as business_state_1','commonregisters.bussiness_zip_1 as bussiness_zip_1','commonregisters.business_country_1 as business_country_1',
         'commonregisters.location_county as location_county','commonregisters.physical_county_no as physical_county_no','commonregisters.purchagename as purchagename',
         'commonregisters.sallername as sallername','commonregisters.businessloan as businessloan','commonregisters.loannote as loannote',
         'commonregisters.business_credit_card as business_credit_card','commonregisters.bank_accounts as bank_accounts','commonregisters.banktype1 as banktype1',
         'commonregisters.banktype as banktype','commonregisters.due_date07 as due_date07','commonregisters.due_date06 as due_date06','commonregisters.buyer_note as buyer_note',
         'commonregisters.personname_buyer as personname_buyer','commonregisters.buyername as buyername','commonregisters.due_date04 as due_date04',
         'commonregisters.purchagenote as purchagenote','commonregisters.personname_saller as personname_saller','commonregisters.sellername as sellername',
         'commonregisters.due_date03 as due_date03','commonregisters.closedate as closedate','commonregisters.saleofbusiness as saleofbusiness','commonregisters.newbusiness as newbusiness',
         'commonregisters.due_date02 as due_date02','commonregisters.contact_fax_1 as contact_fax_1','commonregisters.eext2 as eext2','commonregisters.eteletype2 as eteletype2',
         'commonregisters.etelephone2 as etelephone2','commonregisters.eext1 as eext1','commonregisters.eteletype1 as eteletype1','commonregisters.etelephone1 as etelephone1',
         'commonregisters.zip_1 as zip_1','commonregisters.state_1 as state_1','commonregisters.city_1 as city_1','commonregisters.contact_address2 as contact_address2',
         'commonregisters.contact_address1 as contact_address1','commonregisters.lastname as lastname','commonregisters.middlename as middlename','commonregisters.firstname as firstname',
         'commonregisters.state_id as state_id','commonregisters.nametype as nametype','commonregisters.federal_payment_frequency_date as federal_payment_frequency_date','commonregisters.number_1 as number_1','commonregisters.notes as notes','commonregisters.number_4 as number_4','commonregisters.payment_frequency_date as payment_frequency_date','commonregisters.fedral_state_2 as fedral_state_2','commonregisters.payment_frequency_1 as payment_frequency_1','commonregisters.number_3 as number_3','commonregisters.fedral_state_1 as fedral_state_1','commonregisters.number_2 as number_2','commonregisters.payment_frequency as payment_frequency','commonregisters.federal_payment_frequency as federal_payment_frequency','commonregisters.federal_frequency as federal_frequency','commonregisters.payment_quaterly_monthly as payment_quaterly_monthly','commonregisters.payment_quaterly_quaterly as payment_quaterly_quaterly','commonregisters.quaterly_quaterly as quaterly_quaterly','commonregisters.quaterly_monthly as quaterly_monthly','commonregisters.frequency_due_date_monthly_2 as frequency_due_date_monthly_2','commonregisters.frequency_due_date_quaterly_2 as frequency_due_date_quaterly_2','commonregisters.frequency_due_date_quaterly_1 as frequency_due_date_quaterly_1','commonregisters.frequency_due_date_monthly_1 as frequency_due_date_monthly_1','commonregisters.frequency_due_date_year_1 as frequency_due_date_year_1','commonregisters.frequency_due_date_quaterly as frequency_due_date_quaterly','commonregisters.frequency_due_date_monthly as frequency_due_date_monthly','commonregisters.frequency_due_date_year as frequency_due_date_year','commonregisters.payment_frequency_quaterly as payment_frequency_quaterly','commonregisters.payment_frequency_monthly as payment_frequency_monthly','commonregisters.payment_frequency_year as payment_frequency_year','commonregisters.federal_payment_frequency_quaterly as federal_payment_frequency_quaterly','commonregisters.federal_payment_frequency_month as federal_payment_frequency_month','commonregisters.federal_payment_frequency_year as federal_payment_frequency_year','commonregisters.federal_frequency_due_quaterly as federal_frequency_due_quaterly','commonregisters.federal_frequency_due_monthly as federal_frequency_due_monthly','commonregisters.federal_frequency_due_year as federal_frequency_due_year','commonregisters.state_of_formation as state_of_formation','commonregisters.typeofcorp_effect_2 as typeofcorp_effect_2','commonregisters.type_of_entity as type_of_entity','commonregisters.typeofcorp_effect as typeofcorp_effect','commonregisters.type_of_entity_answer as type_of_entity_answer','commonregisters.type_of_entity_answer1 as type_of_entity_answer1','commonregisters.type_of_entity_answer3 as type_of_entity_answer3','commonregisters.type_of_entity_answer2 as type_of_entity_answer2','commonregisters.form_authority as form_authority','commonregisters.formauthority  as formauthority','commonregisters.unemployent as unemployent','commonregisters.form_authority_1 as form_authority_1','commonregisters.withholding_authority  as withholding_authority','commonregisters.profession_not as profession_not','commonregisters.business_license2 as business_license2','commonregisters.business_license3 as business_license3','commonregisters.business_license1 as business_license1','commonregisters.business_license as business_license','commonregisters.business_license_jurisdiction as business_license_jurisdiction','commonregisters.due_date_1 as due_date_1','commonregisters.due_date2 as due_date2','commonregisters.due_date_2 as due_date_2','commonregisters.type_form1 as type_form1','commonregisters.authority as authority','commonregisters.fedral_state as fedral_state','commonregisters.type_form as type_form','commonregisters.federal_id as federal_id','commonregisters.typeofcorp1 as typeofcorp1','commonregisters.agent_mname as agent_mname','commonregisters.agent_lname as agent_lname','commonregisters.agent_fname as agent_fname','commonregisters.contact_number as contact_number','commonregisters.legal_name1 as legal_name1','commonregisters.typeofservice as typeofservice','commonregisters.id as cid','commonregisters.filename as filename','commonregisters.mailing_address1 as mailing_address1','commonregisters.bussiness_zip as bussiness_zip','commonregisters.due_date as due_date','commonregisters.department as department','commonregisters.type_of_activity as type_of_activity','commonregisters.county_no as county_no','commonregisters.county_name as county_name','commonregisters.level as level','commonregisters.setup_state as setup_state','commonregisters.business_state as business_state','commonregisters.business_city as business_city','commonregisters.business_country as business_country','commonregisters.business_address as business_address','commonregisters.business_store_name as business_store_name','commonregisters.mailing_address as mailing_address','commonregisters.legalname as legalname','commonregisters.dbaname as dbaname','commonregisters.mailing_city as mailing_city','commonregisters.mailing_state as mailing_state','commonregisters.mailing_zip as mailing_zip','commonregisters.user_type as user_type','commonregisters.user_type as user_type','commonregisters.status as status','commonregisters.company_name as company_name','commonregisters.business_name as business_name','commonregisters.first_name as first_name','commonregisters.middle_name as middle_name','commonregisters.last_name as last_name','commonregisters.email as email','commonregisters.address as address','commonregisters.address1 as address1','commonregisters.city as city','commonregisters.stateId as stateId','commonregisters.zip as zip','commonregisters.countryId as countryId','commonregisters.mobile_no as mobile_no','commonregisters.business_no as business_no','commonregisters.business_fax as business_fax','commonregisters.website as website','commonregisters.user_type as user_type','commonregisters.business_id as business_id','commonregisters.business_cat_id as business_cat_id','commonregisters.business_brand_id as business_brand_id','commonregisters.business_brand_category_id as business_brand_category_id','businesses.bussiness_name as bussiness_name','categories.business_cat_name as business_cat_name','business_brands.business_brand_name as business_brand_name','categorybusinesses.business_brand_category_name as business_brand_category_name','commonregisters.extension_due_date','commonregisters.type_form_file2','commonregisters.extension_due_date2','commonregisters.quarter_type','commonregisters.quarter_date','commonregisters.eptpspin','commonregisters.federal_frequency_due_date','commonregisters.typeofcorp','commonregisters.typeofservices','commonregisters.client_payroll','commonregisters.client_intangible','commonregisters.client_tobacco','commonregisters.client_pay','commonregisters.client_coam','commonregisters.quarter_type_holding1','commonregisters.quarter_date_holding','commonregisters.frequency_type_holding1','commonregisters.federal_frequency_due_date_holding','commonregisters.number_3','commonregisters.pin','commonregisters.payment_frequency_type_holding2','commonregisters.payment_frequency_due_date_holding','commonregisters.ga_dept_labour','commonregisters.form_number_4','commonregisters.quarter_type_unemploy1','commonregisters.quarter_date_unemploy','commonregisters.frequency_type_unemploy1','commonregisters.federal_frequency_due_date_unemploy','commonregisters.payment_frequency_type_unemploy2','commonregisters.payment_frequency_due_date_unemploy','commonregisters.form_number_1','commonregisters.pay_pw','commonregisters.federal_note','commonregisters.state_holding_no','commonregisters.holding_uname','commonregisters.holding_password','commonregisters.state_unemploy_no','commonregisters.unemploy_uname','commonregisters.unemploy_password','commonregisters.saltax_state_name','commonregisters.saltax_ga_dept','commonregisters.saltax_county','commonregisters.saltax_county_no','commonregisters.saltax_period','commonregisters.saltax_due_date','commonregisters.saltax_tax_no','commonregisters.saltax_uname','commonregisters.saltax_password','commonregisters.saltax_state_rt','commonregisters.saltax_county_rt','commonregisters.saltax_rt','commonregisters.excise_state_name','commonregisters.excise_ga_dept','commonregisters.excise_period','commonregisters.excise_due_date','commonregisters.excise_tax_no','commonregisters.excise_uname','commonregisters.excise_password','commonregisters.personal_state_name','commonregisters.personal_county','commonregisters.personal_county_no','commonregisters.personal_ga_dep','commonregisters.personal_period','commonregisters.personal_due_date','commonregisters.personal_ac_no','commonregisters.personal_filing','commonregisters.personal_uname','commonregisters.personal_password','commonregisters.coam_state_name','commonregisters.coam_ga_dept','commonregisters.coam_period','commonregisters.coam_due_date','commonregisters.coam_no','commonregisters.coam_unmae','commonregisters.coam_password','commonregisters.federal_note_2','commonregisters.online_payment','commonregisters.interview_type_of_entity','commonregisters.interview_business_sdate','commonregisters.interview_business_detail','commonregisters.formation_register','commonregisters.formation_register_entity','commonregisters.formation_date','commonregisters.license_yesno','commonregisters.license_entity','commonregisters.license_corporation','commonregisters.license_date','commonregisters.formation_yearbox','commonregisters.formation_yearvalue','commonregisters.formation_amount','commonregisters.formation_payment','commonregisters.record_status','commonregisters.annualreceipt','commonregisters.formation_work_officer','commonregisters.telephoneNo1Type','commonregisters.motelephoneile1','commonregisters.telephoneNo2Type','commonregisters.mobile_2','commonregisters.contact_title')
                    ->leftJoin('categories', function($join){ $join->on('commonregisters.business_cat_id', '=', 'categories.id');})         
                    ->leftJoin('businesses', function($join){ $join->on('commonregisters.business_id', '=', 'businesses.id');})
                    ->leftJoin('business_brands', function($join){ $join->on('commonregisters.business_brand_id', '=', 'business_brands.id');})
                    ->leftJoin('categorybusinesses', function($join){ $join->on('commonregisters.business_brand_category_id', '=', 'categorybusinesses.id');})
                    ->where('commonregisters.id', '=', "$id")->get()->first();
       // echo $common->stateId;exit;
        //echo "<pre>";
        //print_r($common);die;
      
        if(isset($common->saltax_due_date)!='')
        {
            $bb=explode('-',$common->saltax_due_date);
       
        
            $bb=explode('-',$common->saltax_due_date);
            if(isset($common->interview_type_of_entity)!='' && isset($bb[2])!=null)
            {
                $commontax = DB::table('taxstatesses')->select('taxstatesses.id','taxstatesses.state','statetosalestax.statesalesid','statetosalestax.state_tax_personal_rate',
                'statetosalestax.state_tax_year')
                    ->leftJoin('statetosalestax', function($join){ $join->on('taxstatesses.id', '=', 'statetosalestax.statesalesid');})         
                    ->where('taxstatesses.state', '=',$common->formation_register_entity)->where('statetosalestax.state_tax_year','=',$bb[2])->get()->first();    
                   // echo "<pre>";
                 // print_r($commontax);die;
            }
        
        }
        
        
        
        if(isset($common->location_county)!='')
        {
          $countytax = DB::table('taxstates')->select('taxstates.id','taxstates.county','taxstates.countycode','countytosalestax.countysalesid',
          'countytosalestax.county_tax_personal_rate','countytosalestax.county_tax_year')
            ->leftJoin('countytosalestax', function($join){ $join->on('taxstates.id', '=', 'countytosalestax.countysalesid');})         
            ->where('taxstates.county', '=', $common->location_county)->where('taxstates.countycode','=',$common->physical_county_no)->get()->first();   
             //  echo "<pre>";
              //print_r($countytax);die;
        }
        
         $clientser = DB::table('clientservices')->where('clientid','=',$id)->get();
         $clientser5 = DB::table('clientservices')->where('clientid','=',$id)->get()->first();
         $admin_notes = DB::table('notes')->where('type','=','admin')->get();
         $currency = Currency::All();
         $client = DB::table('notes')->where('admin_id','=',$id)->where('type','=','client')->get();
         $every = DB::table('notes')->where('type','=','Everybody')->get();
         $typeofser = Typeofser::orderBy('typeofservice', 'asc')->get();
         
         $bankdata = DB::table('client_bank')->select('*')->where('client_id', '=',$id)->get();     
         $carddata = DB::table('client_credit_information')->select('*')->where('client_id', '=',$id)->get();     
         $bankmaster = DB::table('bankmaster')->get();
         $statedata = DB::table('citystate')->where('country', '=','USA')->orderBy('state', 'asc')->get();     
         $positions1 = DB::table('positions')->orderby('position','asc')->get();
         
         $personlaltaxation = DB::table('personaltaxation')->where('client_id', '=',$id)->orderby('year', 'asc')->get();
         $personlalincome = DB::table('personalincome')->where('client_id', '=',$id)->orderby('incomeyear', 'asc')->get();
         
         $personlaldependent2 = DB::table('personaldependent')->where('client_id', '=',$id)->orderby('dependyear', 'desc')->orderby('dependentrelation', 'asc')->get();
         $personlaldependent = DB::table('personaldependent')->where('client_id', '=',$id)->groupBy('dependyear')->orderby('dependyear', 'desc')->get();
        
         $Incometaxfederal = DB::table('personalincome')->select('personalincome.id','personalincome.client_id','personalincome.incomesource','employees.id as empid','employees.nametype','employees.firstName','employees.middleName','employees.lastName','employees.address1','employees.city','employees.stateId','employees.zip','employees.positions','otherincome.id as id2','otherincome.income_id as income_id2','otherincome.client_id as client_id2','otherincome.city as city2','otherincome.stateId as stateId2','otherincome.zip as zip2','otherincome.business_no as business_no2','otherincome.incometype','otherincome.type')
                ->leftJoin('employees', function($join){ $join->on('personalincome.id', '=', 'employees.income_id');})
                ->leftJoin('otherincome', function($join){ $join->on('personalincome.id', '=', 'otherincome.income_id');})
                ->where('personalincome.client_id',$id)->orderBy('personalincome.id','DESC')->groupBy('personalincome.incomesource')->get(); 
         $Incometax2 = DB::table('personalincome')->select('personalincome.id','personalincome.client_id','personalincome.incomesource','employees.id as empid','employees.nametype','employees.firstName','employees.middleName','employees.lastName','employees.address1','employees.city','employees.stateId','employees.zip','employees.positions','otherincome.id as id2','otherincome.income_id as income_id2','otherincome.client_id as client_id2','otherincome.city as city2','otherincome.stateId as stateId2','otherincome.zip as zip2','otherincome.business_no as business_no2','otherincome.incometype','otherincome.type')
                ->leftJoin('employees', function($join){ $join->on('personalincome.id', '=', 'employees.income_id');})
                ->leftJoin('otherincome', function($join){ $join->on('personalincome.id', '=', 'otherincome.income_id');})
                ->where('personalincome.client_id',$id)->orderBy('personalincome.id','DESC')->groupBy('personalincome.incomesource')->get();
        
         $expense = DB::table('expense')->where('client_id', '=',$id)->groupBy('expense')->orderby('expense', 'asc')->get();
         $expense2 = DB::table('expense')->where('client_id', '=',$id)->orderby('expense', 'asc')->get(); 
         
         $healthinsurance = DB::table('employees')->where('type', '=','Vendor')->where('categorys', '=','Insurance')->where('insurance_company', '=','8')->orderby('business_name', 'asc')->get();
       // print_r($healthinsurance);exit;
         $healthagent = DB::table('employees')->where('type', '=','Vendor')->where('categorys', '=','Insurance Agent')->orderby('business_name', 'asc')->get();
         
        //   echo "<pre>";
        //  print_r($Incometax2);die;
         
        if(isset($common->interview_type_of_entity)!='' && isset($common->saltax_due_date)!='')
        { 
            return view('fscemployee.clients.edit',compact(['accountingtable','clienttolicense','datalicense','categoryname','expense','expense2','employee1','employeeassign','employeetaxassign','taxservices1','taxservices5','personlaldependent2','Incometaxfederal','Incometax2','personlaldependent','positions1','statedata','personlalincome','personlaltaxation','taxclientser','taxtitle','taxservices','employee','statelist','commonfederal','pricefederal','commoncounty','taxstatecounty','employeeteam','lastformation1','lastformation','carddatalist','bankdatalist','bankmaster','carddata','bankdata','buslicense','typeofentity','commontax','countytax','questionsection','banking1','empname','banking','position','every','client','clientsertitle','fsc','typeofser','period','currency','admin_notes','clientser5','clientser','pay','fill','upload','admin_notes','total','payroll','admin_shareholder','ceo','cfo','sece','sec','taxstate','common','category','business','businessbrand','cb','user','info','info1','question','answer','shareholder','admin_professional','primaryquestion']));
        }
        else
        {
            return view('fscemployee.clients.edit',compact(['accountingtable','healthagent','healthinsurance','clienttolicense','datalicense','categoryname','expense','expense2','employee1','employeeassign','employeetaxassign','taxservices1','taxservices5','personlaldependent2','Incometaxfederal','Incometax2','personlaldependent','positions1','statedata','personlalincome','personlaltaxation','taxclientser','taxtitle','taxservices','employee','statelist','commonfederal','pricefederal','commoncounty','taxstatecounty','employeeteam','lastformation1','lastformation','carddatalist','bankdatalist','bankmaster','carddata','bankdata','buslicense','typeofentity','questionsection','banking1','empname','banking','position','every','client','clientsertitle','fsc','typeofser','period','currency','admin_notes','clientser5','clientser','pay','fill','upload','admin_notes','total','payroll','admin_shareholder','ceo','cfo','sece','sec','taxstate','common','category','business','businessbrand','cb','user','info','info1','question','answer','shareholder','admin_professional','primaryquestion']));
        }
    }
    
    

    public function destroyfilling(Request $request,$id)
    {   
        DB::table('personaltaxation')->where('id','=',$id)->delete();
        return redirect()->back()->with('success','Your Record Deleted Successfully ');
    }
    
    public function destroydependent(Request $request,$id)
    {   
        DB::table('personaldependent')->where('id','=',$id)->delete();
        return redirect()->back()->with('success','Your Record Deleted Successfully ');
    }

    public function update(Request $request, $id)
    {
       // echo $request->billingtoo11;exit;
        // echo "<pre>";
        // print_r($_REQUEST);die;
       // echo $request->record_status;exit;
        $infoformation = DB::table('client_formation')->where('client_id', '=', $id)->count();
       if($infoformation =='0')
       {
           
           $insertformation = DB::insert("insert into client_formation(`client_id`,`record_status`) values('".$id."','".$request->record_status."')");
       }
       else
       {
           
            $returnValue = DB::table('client_formation')->where('client_id', '=', $id)->update([ 'record_status' => $request->record_status]);  
                   
       }
       
        if($request->hasFile('soscertificate'))
        {
            $filname = $_FILES['soscertificate']['name'];
            $request->soscertificate->move('public/adminupload', $filname);
        }
        else
        {
            $filname = $request->soscertificate1;   
        }
            
        if($request->hasFile('sosaoi'))
        {
            $filname1 = $_FILES['sosaoi']['name'];
            $request->sosaoi->move('public/adminupload', $filname1);
        }
        else
        {
            $filname1 = $request->sosaoi1;   
        }
        
        if($request->status=='Pending' or $request->status=='Hold')
        {
            $name = $request->first_name .' '. $request->middle_name .' '. $request->last_name;   
            $status = $request->status;
            $email = $request->email;
            $name = $request->first_name;  
            //DB::table('users')->where('user_id', $id)->update(['user_type' =>$user_type,'type'=>2,'name'=>$name]);
            $returnValue = DB::table('users')->where('email', '=', $email)->update(['user_type' =>$user_type,'type'=>2,'name'=>$name]);
            $data = array('email' => $email, 'first_name' => $name, 'from' => 'vijay@businesssolutionteam.com');       
            \Mail::send('fac-Bhavesh-0554/hold', $data, function( $message ) use ($data)
            {
                $message->to( $data['email'] )->from( $data['from'], $data['first_name'])->subject( 'Welcome!' );
            });
        }
        else
        {
            $name = $request->first_name .' '. $request->middle_name .' '. $request->last_name;   
            $status = $request->status;
            $email = $request->email;
            $name = $request->first_name;
            $question1 = $request->question1;
            $question2 = $request->question2;
            $question3 = $request->question3;
            $answer1 =   $request->answer1;
            $answer2 =   $request->answer2;
            $answer3 =   $request->answer3;
            $password =  mt_rand();
            $client_id = $request->client_id;
            $user_type=  $request->user_type;
            $user = User::where('email', '=', $email)->first();
            if ($user === null)
            {
                $insert = DB::insert("insert into users(`name`,`email`,`password`,`user_id`,`user_type`,`question1`,`question2`,`question3`,`answer1`,`answer2`,`answer3`,`type`) values('".$name."','".$email."','".bcrypt($password)."','".$id."','".$user_type."','".$question1."','".$question2."','".$question3."','".$answer1."','".$answer2."','".$answer3."','1')");
                $data = array('email' => $email, 'first_name' => $name, 'from' => 'vijay@businesssolutionteam.com', 'password' =>$password ,'client' =>$client_id);       
                \Mail::send( 'activation', $data, function( $message ) use ($data)
                {
                $message->to( $data['email'] )->from( $data['from'], $data['first_name'], $data['password'], $data['client'] )->subject( 'Welcome!' );
                });      
            }
            else
            {
                $name = $request->first_name .' '. $request->middle_name .' '. $request->last_name;   
                $returnValue = DB::table('users')->where('email', '=', $email)->update(['user_type' =>$user_type,'type'=>1,'name'=>$name]);   
            }      
        }
        
        $contact_person_name = $request->contact_person_name;
        $business= $request->business;
        $business_fax1= $request->business_fax1;
        $residence= $request->residence;
        $cell= $request->cell;
        $residence_fax = $request->residence_fax;
        $contemail = $request->contemail;
        $telephone = $request->telephone;
        $conid = $request->conid;
        $i = 0;
        $info = Contact_userinfo::where('user_id', '=', $id)->first();
        if($request->get('type_of_entity'))
        {
            $type_of_entity = implode(",", $request->get('type_of_entity'));
        }
        else
        {
            $type_of_entity='';
        }
        
        
        
         if($request->hasFile('annualreceipt'))
            {
             $filesname1 = $request->annualreceipt->getClientOriginalName();
             $request->annualreceipt->move('public/adminupload', $filesname1);
            }
            else
            {
            $filesname1 = $request->annualreceipt_1;   
            }
           
         if($request->hasFile('formation_work_officer'))
            {
             $filesname2 = $request->formation_work_officer->getClientOriginalName();
             $request->formation_work_officer->move('public/adminupload', $filesname2);
            }
            else
            {
            $filesname2 = $request->officers;   
            }
        
        if($request->bank_accounts!='')
        {
            $bank_accounts=$request->bank_accounts;        
        }
        else
        {
            $bank_accounts='';
        }
            
        
        $returnValue = DB::table('commonregisters')->where('id', '=', $id)->update([
        'filename' => isset($request->fileno) ? $request->fileno:"",
        'company_name' => isset($request->company_name) ? $request->company_name:"",
        'business_name' => isset($request->business_name) ? $request->business_name:"",
        'email' => isset($request->email) ? $request->email:"",
        'address' => isset($request->address) ? $request->address:"",
        'address1' => isset($request->address1) ? $request->address1:"",
        'city' => isset($request->city) ? $request->city:"",
        'stateId' => isset($request->stateId) ? $request->stateId:"",
        'zip' => isset($request->zip) ? $request->zip:"",      
        'countryId' => $request->countryId,
        'mobile_no' => $request->mobile_no,
        'business_no' => $request->business_no,
        'business_fax' => $request->business_fax,
        'website' => $request->website,
        'status' => $status,
        'type_of_entity_answer1' => $request->type_of_entity_answer1,
        'type_of_entity_answer2' => $request->type_of_entity_answer2,
        'type_of_entity_answer3' => $request->type_of_entity_answer3,
        'legalname' => $request->legalname,           
        'dbaname' => $request->dbaname,
        'typeofcorp_effect_2' => $request->typeofcorp_effect_2,
        'state_id' => $request->state_id,
        'mailing_city' => $request->mailing_city,           
        'mailing_state' => $request->mailing_state,
        'mailing_zip' => $request->mailing_zip,           
        'mailing_address' => $request->mailing_address,
        'mailing_address_check' => $request->mailing_address_check,
        
        'mailing_address1' => $request->mailing_address1,
        'business_store_name' => $request->business_store_name,  
        'business_address' => $request->business_address,
        'business_country' => $request->business_country,  
        'business_city' => $request->business_city,
        'business_state' => $request->business_state,  
        'bussiness_zip' => $request->bussiness_zip,
        'level' => $request->level,
        'setup_state' => $request->setup_state,
        'county_name' => $request->county_name,
        'business_license2' => $request->business_license2,
        'county_no' => $request->county_no,
        'type_of_activity' => $request->type_of_activity,
        'profession_not'=> $request->profession_not,
        'department' => $request->department,
        'due_date' => $request->due_date,
        // 'typeofservice' => $request->typeofservice,
        'state_of_formation' => $request->state_of_formation,
        'soscertificate'=>$filname,
        'sosaoi'=>$filname1,
        
        'legal_name1' => $request->legal_name1,
        'contact_number' => $request->contact_number,
        'agent_fname' => $request->agent_fname,
        'agent_mname' => $request->agent_mname,
        'agent_lname' => $request->agent_lname,
        'typeofcorp_effect' => $request->typeofcorp_effect,
        'state_id'=> $request->state_id,
        'number_1'=> $request->number_1,
        'number_2'=> $request->number_2,
        'number_3'=> $request->number_3,
        'number_4'=> $request->number_4,
        'federal_id' => $request->federal_id,
        'type_form' => $request->type_form,
        'due_date_1' => $request->due_date_1,
        'fedral_state' => $request->fedral_state,
        'type_form1' => $request->type_form1,
        'due_date_2' => $request->due_date_2,
        'business_license_jurisdiction' => $request->business_license_jurisdiction,
        'business_license' => $request->business_license,
        'business_license1' => $request->business_license1,
        'business_license3' => $request->business_license3,
        'typeofcorp1' => $request->typeofcorp1,
        'authority' => $request->authority,
        //'type_of_entity' => $type_of_entity,
        //'type_of_entity_answer' => $type_of_entity_answer,
        'frequency_due_date_1' => $request->frequency_due_date_1,
        'frequency_due_date' => $request->frequency_due_date,
        'quaterly_due_date' => $request->quaterly_due_date,
        'frequency_type_form_1' => $request->frequency_type_form_1,
        'frequency_due_date_1' => $request->frequency_due_date_1,
        'payment_frequency_date_1'=> $request->payment_frequency_date_1,
        'payment_frequency_date'=> $request->payment_frequency_date,
        'payment_frequency'=> $request->payment_frequency,
        'payment_frequency_1'=> $request->payment_frequency_1,
        'federal_frequency'=> $request->federal_frequency,
        'federal_payment_frequency'=> $request->federal_payment_frequency,
        'federal_payment_frequency_date'=> $request->federal_payment_frequency_date,
        'due_date02'=> $request->due_date02,
        'newbusiness'=> $request->newbusiness,
        'saleofbusiness'=> $request->saleofbusiness,
        'closedate'=> $request->closedate,
        'due_date03'=> $request->due_date03,
        'sellername'=> $request->sellername,
        'purchagenote'=> $request->purchagenote,
        'due_date04'=> $request->due_date04,
        'buyername'=> $request->buyername,
        'personname_saller'=> $request->personname_saller,
        'personname_buyer'=> $request->personname_buyer,
        'buyer_note'=> $request->buyer_note,
        'due_date06'=> $request->due_date06,
        'due_date07'=> $request->due_date07,
        'purchagename'=> $request->purchagename,
        'sallername'=> $request->sallername,
        'banktype'=> $request->banktype,
        'banktype1'=> $request->banktype1,
        //'bank_accounts'=> $request->bank_accounts,
        'bank_accounts'=>$bank_accounts,
        'business_credit_card'=> $request->business_credit_card,
        'loannote'=> $request->loannote,
        'businessloan'=> $request->businessloan,
        'billingtoo11'=> $request->billingtoo11,
        'billingtoo'=> $request->billingtoo,
        'common_status'=>$request->common_status,
        
        'business_address_2'=> $request->business_address_2,
        'business_address_3'=> $request->business_address_3,
        'business_city_1'=> $request->business_city_1,
        'business_state_1'=> $request->business_state_1,
        'bussiness_zip_1'=> $request->bussiness_zip_1,
        'business_country_1'=> $request->business_country_1,
        'location_county'=> $request->location_county,
        'physical_county_no'=> $request-> physical_county_no,
        'address_type'=> $request->address_type,
        'formation_city'=> $request->formation_city,
        'formation_state'=> $request->formation_state,
        'formation_address'=> $request->formation_address,
        'formation_zip'=> $request->formation_zip,
        
        'due_date2' => $request->due_date2,
        'form_number_1'=> $request->form_number_1,
        'federal_ga_dept'=> $request->federal_ga_dept,
        'federal_name'=> $request->federal_name,
        'extension_due_date'=> $request->extension_due_date,
        'type_form_file2'=> $request->type_form_file2,
        'extension_due_date2'=> $request->extension_due_date2,
        'federal_frequency_due_date'=> $request->federal_frequency_due_date,
        'due_date'=> $request->due_date,
        'state'=> $request->state,
        'pay_pw'=> $request->pay_pw,
        'payroll_name'=> $request->payroll_name,
        'ga_dept'=> $request->ga_dept,
        'form_number_2'=> $request->form_number_2,
        'frequency'=> $request->frequency,
        'fedral_state_1'=> $request->fedral_state_1,
        'ga_dept_1'=> $request->ga_dept_1,
        'form_number_3'=> $request->form_number_3,
        'quarter_type_holding1'=> $request->quarter_type_holding1,
        'quarter_date_holding'=> $request->quarter_date_holding,
        'frequency_type_holding1'=> $request->frequency_type_holding1,
        'federal_frequency_due_date_holding'=> $request->federal_frequency_due_date_holding,
        'number_3'=> $request->number_3,
        'pin'=> $request->pin,
        'payment_frequency_type_holding2'=> $request->payment_frequency_type_holding2,
        'payment_frequency_due_date_holding'=> $request->payment_frequency_due_date_holding,
        'fedral_state_2'=> $request->payment_frequency_due_date_holding,
        'ga_dept_labour'=> $request->ga_dept_labour,
        'form_number_4'=> $request->form_number_4,
        'quarter_type_unemploy1'=> $request->quarter_type_unemploy1,
        'quarter_date_unemploy'=> $request->quarter_date_unemploy,
        'frequency_type_unemploy1'=> $request->frequency_type_unemploy1,
        'federal_frequency_due_date_unemploy'=> $request->federal_frequency_due_date_unemploy,
        'payment_frequency_type_unemploy2'=> $request->payment_frequency_type_unemploy2,
        'payment_frequency_due_date_unemploy'=> $request->payment_frequency_due_date_unemploy,
        'number_note'=> $request->number_note,
        'federal_note'=> $request->federal_note,
        
        'state_holding_no'=> $request->state_holding_no,
        'holding_uname'=> $request->holding_uname,
        'holding_password'=> $request->holding_password,
        'state_unemploy_no'=> $request->state_unemploy_no,
        'unemploy_uname'=> $request->unemploy_uname,
        'unemploy_password'=> $request->unemploy_password,
        
        'quarter_type'=> $request->quarter_type,
        'quarter_date'=> $request->quarter_date,
        'eptpspin'=> $request->eptpspin,
        'federal_form_1'=> $request->federal_form_1,
        'typeofservices'=> $request->typeofservices,
        
        
        'state_unemploy_no'=> $request->state_unemploy_no,
        'unemploy_uname'=> $request->unemploy_uname,
        'unemploy_password'=> $request->unemploy_password,
        'saltax_state_name'=> $request->saltax_state_name,
        'saltax_ga_dept'=> $request->saltax_ga_dept,
        'saltax_county'=> $request->saltax_county,
        'saltax_county_no'=> $request->saltax_county_no,
        'saltax_period'=> $request->saltax_period,
        'saltax_due_date'=> $request->saltax_due_date,
        'saltax_tax_no'=> $request->saltax_tax_no,
        'saltax_uname'=> $request->saltax_uname,
        'saltax_password'=> $request->saltax_password,
        'saltax_rt'=> $request->saltax_rt,
        'excise_state_name'=> $request->excise_state_name,
        'excise_ga_dept'=> $request->excise_ga_dept,
        'excise_period'=> $request->excise_period,
        'excise_due_date'=> $request->excise_due_date,
        'excise_tax_no'=> $request->excise_tax_no,
        'excise_uname'=> $request->excise_uname,
        'excise_password'=> $request->excise_password,
        'personal_state_name'=> $request->personal_state_name,
        'personal_county'=> $request->personal_county,
        'personal_county_no'=> $request->personal_county_no,
        'personal_ga_dep'=> $request->personal_ga_dep,
        'personal_period'=> $request->personal_period,
        'personal_due_date'=> $request->personal_due_date,
        'personal_ac_no'=> $request->personal_ac_no,
        'personal_filing'=> $request->personal_filing,
        'personal_uname'=> $request->personal_uname,
        'personal_password'=> $request->personal_password,
        'coam_state_name'=> $request->coam_state_name,
        'coam_ga_dept'=> $request->coam_ga_dept,
        'coam_period'=> $request->coam_period,
        'coam_due_date'=> $request->coam_due_date,
        'coam_no'=> $request->coam_no,
        'coam_unmae'=> $request->coam_unmae,
        'coam_password'=> $request->coam_password,
        'saltax_state_rt'=> $request->saltax_state_rt,
        'saltax_county_rt'=> $request->saltax_county_rt,
        'federal_note_2'=> $request->federal_note_2,
        'online_payment'=> $request->online_payment,
        
        'client_payroll'=> $request->client_payroll,
        'client_intangible'=> $request->client_intangible,
        'client_tobacco'=> $request->client_tobacco,
        'client_pay'=> $request->client_pay,
        'client_coam'=> $request->client_coam,
    
        'interview_type_of_entity'=> $request->interview_type_of_entity,
        'interview_business_sdate'=> $request->interview_business_sdate,
        'interview_business_detail'=> $request->interview_business_detail,
        'formation_register'=> $request->formation_register,
        'formation_register_entity'=> $request->formation_register_entity,
        'formation_type_of_entity'=> $request->formation_type_of_entity,
        'formation_start_date'=> $request->formation_start_date,
        'formation_date'=> $request->formation_date,
        'license_yesno'=> $request->license_yesno,
        'license_entity'=> $request->license_entity,
        'license_corporation'=> $request->license_corporation,
        'license_date'=> $request->license_date,
        
        'tank_state'=>$request->tank_state,
        'tank_renew'=>$request->tank_renew,
        'tank_annual'=>$request->tank_annual,
        'tank_date'=>date('Y-m-d',strtotime($request->tank_date)),
        'tank_note'=>$request->tank_note,
        
        'food_state'=>$request->food_state,
        'food_renew'=>$request->food_renew,
        'food_annual'=>$request->food_annual,
        'food_date'=>date('Y-m-d',strtotime($request->food_date)),
        'food_note'=>$request->food_note
        
        ]); 
        
        $agent_fname1 = $request->agent_fname1;
        $agent_mname1= $request->agent_mname1;
        $agent_lname1= $request->agent_lname1;
        $agent_position= $request->agent_position;
        $agent_position2= $request->agent_position1;
        $agent_per= $request->agent_per;
        $effective_date = $request->effective_date;
        $agentstatus = $request->agentstatus;
        $conid= $request->conid;
        $total = $request->total;
        $i = 0;
        
        $info = client_shareholder::where('client_id', '=', $id)->first();
        foreach($agent_fname1 as $post)
        { 
            $conid1 =$conid[$i];
            $agentstatus1= $agentstatus[$i] ;
            $agent_fname11 = $agent_fname1[$i];
            $agent_mname11 = $agent_mname1[$i];
            $agent_lname11 = $agent_lname1[$i];
            $agent_position1 = $agent_position[$i];
            $agent_position11 = $agent_position2;
            $agent_per1 = $agent_per[$i];
            $effective_date1 = $effective_date[$i];
            $total1 = $total;  
            $i++; 
            if(empty($conid1))
            {
                $insert2 = DB::insert("insert into client_shareholders(`agent_fname1`,`agent_mname1`,`agent_lname1`,`agent_position`,`agent_position1`,`total`,`client_id`,`agent_per`,`effective_date`,`agentstatus`) values('".$agent_fname11."','".$agent_mname11."','".$agent_lname11."','".$agent_position1."','".$agent_position11."','".$total1."','".$id."','".$agent_per1."','".$effective_date1."','".$agentstatus1."')");
            }
            else
            {
                 $returnValue = DB::table('client_shareholders')->where('id', '=', $conid1)
                ->update([ 'agent_fname1' => $agent_fname11,
                           'agent_mname1' =>$agent_mname11,
                           'agent_lname1' =>$agent_lname11,
                           'agent_position' =>$agent_position1,
                           'agent_position1' =>$agent_position11,
                           'total' =>$total1,
                           'client_id' =>$id,
                           'agent_per' =>$agent_per1,
                           'effective_date' =>$effective_date1,
                            'agentstatus' =>$agentstatus1
                    ]);     
                $affectedRows = client_shareholder::where('agent_fname1', '=', '')->delete();
            }
        }
        
        $profession = $request->profession;
        $profession_state= $request->profession_state;
        $profession_epr_date= $request->profession_epr_date;
        $profession_license= $request->profession_license;
        $profession_note=$request->profession_note;
        $profession_id=$request->profession_id;
        $fileno=$request->fileno;
        $profession_epr_dates= $request->profession_effective_date;
        $j = 0;
        if(empty($profession))
        {
        }
        else
        {
            $info = client_professional::where('client_id', '=', $id)->first();
            foreach($profession as $post)
            { 
                $fileno1 = $fileno;
                $profession_epr_date11=$profession_epr_dates[$j];
                $profession_id1 =$profession_id[$j];
                $profession1 =$profession[$j];
                $profession_state1 =$profession_state[$j];
                $profession_epr_date1 =$profession_epr_date[$j];
                $profession_license1 =$profession_license[$j];
                $profession_note1 =$profession_note[$j];
                $proid = $profession1.'-'.$profession_state1;
                $j++; 
                if(empty($profession_id1))
                {
                $insert2 = DB::insert("insert into client_professionals(`profession_epr_date1`,`fileno1`,`profession`,`profession_state`,`profession_epr_date`,`profession_license`,`profession_note`,`client_id`,`pro_id`) values('".$profession_epr_date11."','".$fileno1."','".$profession1."','".$profession_state1."','".$profession_epr_date1."','".$profession_license1."','".$profession_note1."','".$id."','".$proid."')");
                }
                else
                {
                    $returnValue = DB::table('client_professionals')->where('id', '=', $profession_id1)->update([ 'profession' => $profession1,'profession_state' =>$profession_state1,'profession_epr_date' =>$profession_epr_date1,'profession_license' =>$profession_license1,'profession_note' =>$profession_note1,'client_id' =>$id,'pro_id' =>$proid,'profession_epr_date1' =>$profession_epr_date11]);  
                    $affectedRows1 = client_professional::where('profession', '=', '')->delete();
                }
            }
        }
        
        $licenseid = isset($request->licenseid) ? $request->licenseid:"";
        $licensestatus= isset($request->licenseid) ? $request->licensestatus:"";
        $lj = 0;
        if(empty($licenseid))
        {
        }
        else
        {
           // $info = DB::delete::('client_id', '=', $id)->first();
               DB::table('client_to_license')->where('client_id', '=', $id)->delete();
                            
            foreach($licenseid as $post)
            { 
                $licenseid1 = $post;
                $licensestatus1=$licensestatus[$lj];
                $lj++; 
                $insert2 = DB::insert("insert into client_to_license(`client_id`,`licenseid`,`licensestatus`) values('".$id."','".$licenseid1."','".$licensestatus1."')");
             
            }
        }
        
        
        // Banking Start

            
            
            if(isset($request->bank_name)!='')
            {
                $bank_id = $request->bank_id;
                $persinfo = count(array_filter($request->bank_name));
                for($i=0; $i < $persinfo; $i++)
                {
                    if($request->bank_name!='' || $request->bank_name!=null || $request->bank_name!='0')
        			{
        			    if(isset($bank_id[$i])!='' || isset($bank_id[$i])!=null || isset($bank_id[$i])!='0')
        			    {
        			        $bank_id1 = $bank_id[$i];    
        			    }
        			    else
        			    {
        			       
        			    }
        			    
                            $client_id = $id;
                            $bank_name1 = isset($request->bank_name[$i]) ? $request->bank_name[$i]:"" ;
                            $nick_name1 = isset($request->nick_name[$i]) ? $request->nick_name[$i]:"";
                            $fourdigit1 = isset($request->fourdigit[$i]) ? $request->fourdigit[$i]:"";
                            $statement1 = isset($request->statement[$i]) ? $request->statement[$i]:"";
                            $stubs1 = isset($request->stubs[$i]) ? $request->stubs[$i]:"";
                            $opendate1 = isset($request->opendate[$i]) ? $request->opendate[$i]:"";
                            $bankstatus1 = isset($request->bankstatus[$i]) ? $request->bankstatus[$i]:"";
                            $closedate1 = isset($request->bankclosedate[$i]) ? $request->bankclosedate[$i]:"";
                            $routingno = isset($request->routingno[$i]) ? $request->routingno[$i]:"";
                            
                            
        			    
        			    
                            DB::insert("insert into client_bank(`routingno`,`client_id`,`bank_name`,`nick_name`,`fourdigit`,`statement`,`stubs`,`opendate`,`bankstatus`,`bankclosedate`)
                            values('".$routingno."','".$client_id."','".$bank_name1."','".$nick_name1."','".$fourdigit1."','".$statement1."','".$stubs1."','".$opendate1."','".$bankstatus1."','".$closedate1."')");    
                            if(empty($bank_id))
                            {
                                
                            }
                            else
                            {
                                DB::table('client_bank')->where('id', '=', $bank_id1)->delete();
                            }     
        			}
                }
            }
            
            
        // Banking End
        
        
        // Card Start
            if(isset($request->card_bank_name)!='')
            {
                $card_id = $request->card_id;
                $persinfo = count(array_filter($request->card_bank_name));
                for($i=0; $i < $persinfo; $i++)
                {
                    if($request->card_bank_name!='' || $request->card_bank_name!=null || $request->card_bank_name!='0')
        			{
        			    if(isset($card_id[$i])!='' || isset($card_id[$i])!=null || isset($card_id[$i])!='0')
        			    {
        			        $card_id1 = $card_id[$i];    
        			    }
        			    else
        			    {
        			       
        			    }
        			    
                            $client_id = $id;
                            $card_bank_name1 = isset($request->card_bank_name[$i]) ? $request->card_bank_name[$i]:"" ;
                            $card_nick_name1 = isset($request->card_nick_name[$i]) ? $request->card_nick_name[$i]:"";
                            $card_fourdigit1 = isset($request->card_fourdigit[$i]) ? $request->card_fourdigit[$i]:"";
                            $card_statement1 = isset($request->card_statement[$i]) ? $request->card_statement[$i]:"";
                            $card_notes1 = isset($request->card_notes[$i]) ? $request->card_notes[$i]:"";
                            $card_opendate1 = isset($request->card_opendate[$i]) ? $request->card_opendate[$i]:"";
                            $card_bankstatus1 = isset($request->card_bankstatus[$i]) ? $request->card_bankstatus[$i]:"";
                            $card_closedate1 = isset($request->card_closedate[$i]) ? $request->card_closedate[$i]:"";
                            
                            DB::insert("insert into client_credit_information(`client_id`,`card_bank_name`,`card_nick_name`,`card_fourdigit`,`card_statement`,`card_notes`,`card_opendate`,`card_bankstatus`,`card_closedate`)
                            values('".$client_id."','".$card_bank_name1."','".$card_nick_name1."','".$card_fourdigit1."','".$card_statement1."','".$card_notes1."','".$card_opendate1."','".$card_bankstatus1."','".$card_closedate1."')");        			    
        			    
                            if(empty($card_id))
                            {
                                
                            }
                            else
                            {
                                DB::table('client_credit_information')->where('id', '=', $card_id1)->delete();
                            }     
        			}
                }
            }
        // Card End
            
        
        $noteid = $request->noteid;
        $adminnotes= $request->adminnotes;
        $k=0;
        $users = DB::table('notes')->where('admin_id', '=', $id)->first();
        foreach($adminnotes as $notess)
        { 
           $noteid1 = $noteid[$k];
           $note1 =$adminnotes[$k];
           $k++;
            if(empty($noteid1))
            {
                $insert2 = DB::insert("insert into notes(`notes`,`admin_id`,`type`) values('".$note1."','".$id."','client')");
            }
            else
            {
                $returnValue = DB::table('notes')->where('id', '=', $noteid1)->update([ 'notes' => $note1,'admin_id' =>$id,'type' =>'client']);    
                $users = DB::table('notes')->where('notes', '=', '')->delete();
            }
        }
        return redirect(route('clients.edit',$id))->with('success','Your Profile Update Successfully');
    }


public function getcategory(Request $request)
{
    $data = Category::select('business_cat_name','id')->where('bussiness_name',$request->id)->take(100)->get();
    return response()->json($data); 
    
    
}
public function getrouting1(Request $request)
{
    $data = DB::table('bankmaster')->where('id', '=', $request->id)->first();
    return response()->json($data);  
}

public function getAgentTelephone(Request $request)
{
    $data = DB::table('employees')->where('id', '=', $request->vendorid)->first();
    return response()->json($data);  
}

public function getcategory1(Request $request)
{
$data = Business::select('bussiness_name','id')->where('id',$request->id)->take(100)->get();
return response()->json($data);  
}

    public function deletecon($id,$cid)
    {        
        Contact_userinfo::where('id',$id)->delete(); 
        return redirect('fscemployee/clients'.$cid.'/edit');     
    }


    public function destroyemployee(Request $request,$empid)
    {   
        //echo $empid;die;
        DB::table('employees')->where('id','=',$empid)->delete();
        return redirect()->back()->with('success','Your Record Deleted Successfully ');
    }
    
    public function destryotherincome(Request $request,$id2)
    {   
       // echo $id2;die;
        DB::table('otherincome')->where('id','=',$id2)->delete();
        return redirect()->back()->with('success','Your Record Deleted Successfully ');
    }



public function getimagesssss(Request $request)
{
$data = Clientnupload::select('upload_name','upload')->where('upload_name',$request->id)->take(10000)->get();
if(! $data)
{
$data = "File not Uploaded";
return response()->json($data); 
}
else
{
return response()->json($data);  
}
}
    public function clientdelete($id)
    { 
       $logoStatus = client_professional::findOrFail($id);
       client_professional::where('id',$id)->delete();
       return redirect('fscemployee/clients')->with('error','Record is delete');
    }

    // public function clientdelete21($id)
    // { 
    //   $logoStatus = client_shareholder::findOrFail($id);
    //   client_shareholder::where('id',$id)->delete();
    //   return redirect('fscemployee/clients')->with('error','Record is delete');
    // }
    
    //public function clientdelete4($id,$ccid)
    public function admindelete4($id,$ccid)
    { 
        //echo $id;
        //echo "<br>";
        //echo $ccid;
        //die;
       $logoStatus = client_shareholder::findOrFail($id);
       client_shareholder::where('id',$id)->delete();
       return redirect('fscemployee/clients/'.$ccid.'/edit')->with('success','Shareholder Successfully Deleted');
    }
    
    public function client_number1(Request $request)
    {
        $data = client_professional::select('profession','profession_state','client_id')->where('fileno1',$request->id)->take(100)->get();
        if(! $data)
        {
            $data = "not find";
            return response()->json($data); 
        }
        else
        {
            return response()->json($data);  
        }
    }

public function destroy($id)
{
Commonregister::where('id',$id)->delete();
$affectedRows = Contact_userinfo::where('user_id', '=', $id)->delete();
$user = User::where('user_id', '=', $id)->delete();
return redirect(route('clients.index'));
}
}