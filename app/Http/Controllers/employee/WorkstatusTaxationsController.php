<?php
namespace App\Http\Controllers\employee;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Front\Commonregister;
use App\Model\BusinessBrand;

use App\Model\Category;
use App\Model\Business;
use App\Model\Employee;
use App\Model\client_professional;
use App\Model\client_shareholder;
use App\User;
use App\Model\Categorybusiness;
use App\Model\Contact_userinfo;
use App\Mail\Activationmail;
use App\Model\taxstate;
use App\Model\formationsetup;

use DB;
use Hash;
use Carbon;
use App\Submissionlogin\Submissionlogin;
use App\Model\Admin;
use Auth;
use App\Model\Price;
use App\Model\Currency;
use App\Model\Typeofser;
use App\Model\Period;
use App\Model\Taxtitle;
use App\Model\Language;
use App\Model\Ethnic;

use App\employees\Fscemployee;
use Illuminate\Support\Facades\Input;
use App\Model\Logo;
class WorkstatusTaxationsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:employee');
    }

    function edit($id)
    {
        //echo $id;die;
        $logo = Logo::where('id','=',1)->first();
        $user_id  = Auth::user()->user_id;
        $employee = Employee::where('id',$user_id)->first();
        
        $datastate = DB::table('citystate')->where('country','USA')->orderBy('state', 'asc')->get(); 
        $datastate2 = DB::table('citystate')->where('country','USA')->orderBy('state', 'asc')->get(); 
        $datastate3 = DB::table('citystate')->where('country','USA')->orderBy('state', 'asc')->get(); 
        
        $Incometaxfederal = DB::table('client_taxfederal')->where('client_id',$id)->orderBy('federalsyear', 'DESC')->groupBy('federalsyear')->get(); 
        
        $federaldata = DB::table('client_taxfederal')->where('id',$id)->first();
        $statedatatax = DB::table('client_taxstate')->where('federal_id',$id)->get();
        
        //echo "<pre>";
        //print_r($datastate3);die;
        
        return view('fscemployee.workstatustaxations.edit',compact(['employee','datastate','datastate2','datastate3','Incometaxfederal','federaldata','statedatatax']));
    }
    
    
    function update(Request $request)
    {
       // echo $id;die;
        //echo "<pre>";
        //print_r($_REQUEST);die;
        
        $federalid=$request->federalid;
        $client_id=$request->client_id;
        $federalsyear=$request->federalsyear;
        $federalstax=$request->federalstax;
        $federalsduedate=date('Y-m-d',strtotime($request->federalsduedate));
        $federalsform=$request->federalsform;
        $federalsmethod=$request->federalsmethod;
        $federalsdate=date('Y-m-d',strtotime($request->federalsdate));
        $federalsstatus=$request->federalsstatus;
        $federalsnote=$request->federalsnote;
        
        // $path1= public_path().'/adminupload/'.$_FILES['federalsfile']['name'];
        // if(isset($_FILES['federalsfile']['name'])!='')
        // {
        //     if(move_uploaded_file($_FILES['federalsfile']['tmp_name'], $path1)) 
        //     {
        //       $federal_copy= $_FILES['federalsfile']['name']; 
        //     }
        //     else
        //     {
        //         $federal_copy= ''; 
        //     }
        // }
        // $federalsfile=$federal_copy;
        
        DB::table('client_taxfederal')->where('id',$federalid)
            ->update([
                'client_id' =>$client_id,
                'federalsyear' =>$federalsyear,
                'federalstax' =>$federalstax,
                'federalsduedate' =>$federalsduedate,
                'federalsform' =>$federalsform,
                'federalsmethod' =>$federalsmethod,
                'federalsdate' =>$federalsdate,
                'federalsstatus' =>$federalsstatus,
                //'federalsfile' =>$federalsfile,
                'federalsnote' =>$federalsnote,
            ]);
        //$ids=$data->id;
        //echo $ids;die;
        
        
        $taxid= $request->taxid;
        //$client_id = $request->client_id;
        //$federal_id= $ids;
        //$stateyear= $request->stateyear;
        $statetax= $request->statetax;
        $stateformno=$request->stateformno;
        $statemethod=$request->statemethod;
        $statedate =$request->statedate;
        $statestatus=$request->statestatus;
        //$statefile=$request->statefile;
        $statenote = $request->statenote;
        $i = 0;
        
        foreach($statetax as $statetax1)
        { 
            $taxid1 =$taxid[$i];
            $client_id =$client_id;
            $federal_id1 =$federalid;
            $stateyear1=$federalsyear;
            $statetax1 =$statetax[$i];
            $stateformno1=$stateformno[$i];
            $statemethod1 =$statemethod[$i];
            $statedate1 =$statedate[$i];
            $statestatus1 =$statestatus[$i];
           // $statefile1 =$statefile[$i];
            $statenote1 =$statenote[$i];
            $i++; 
            if(empty($taxid1))
            {
                $insert2 = DB::insert("insert into client_taxstate(`client_id`,`federal_id`,`stateyear`,`statetax`,`stateformno`,`statemethod`,`statedate`,`statestatus`,`statefile`,`statenote`) values('".$client_id1."','".$federal_id1."','".$stateyear1."','".$statetax1."','".$stateformno1."','".$statemethod1."','".$statedate."','".$statestatus1."','".$statefile1."','".$statenote1."')");
            }
            else
            {
                $returnValue = DB::table('client_taxstate')->where('id', '=', $taxid1)
                ->update([
                           'client_id' => $client_id,
                           'federal_id' =>$federal_id1,
                           'stateyear' => $stateyear1,
                           'statetax' =>$statetax1,
                           'stateformno' =>$stateformno1,
                           'statemethod' =>$statemethod1,
                           'statedate' =>$statedate1,
                           'statestatus' =>$statestatus1,
                           //'statefile' =>$statefile1,
                           'statenote' =>$statenote1,
                    ]);     
                //$affectedRows = admin_shareholder::where('id', '=',$conid1)->delete();
                $affectedRows = DB::table('client_taxstate')->where('id', '=', '')->delete();
            }
        }
        
        return redirect()->back();
    }
    
    
}