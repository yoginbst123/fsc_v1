<?php
namespace App\Http\Controllers\employee;
use Illuminate\Http\Request;
use App\Model\Howtodo;
use App\Model\Employee;
use App\Model\Workcategory;

use App\employees\Fscemployee;
use DB;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
class HowtodoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:employee');
    }  
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
         $user_id  = Auth::user()->user_id;
        $employee = Employee::where('id',$user_id)->first();
        $howtodo=DB::table('howtodos_steps')->where('noteid','=',null)->get(); 
        $howtodo1 =DB::table('howtodos_steps')->get(); 
        $homecontent = Howtodo::All();
        return view('fscemployee/howtodo/howtodo', compact(['employee','howtodo','homecontent','howtodo1']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {  
        
       return view('fscemployee/howtodo/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $this->validate($request,[
        
                     
        ]);
        $position = new Howtodo;
        $position->subject= $request->subject;
        $position->website= $request->website;
        $position->telephone= $request->telephone;
        $position->software= $request->software;
        $noteid = $request->stepid;
        $adminnotes= $request->step;
        $k=0;
        $position->save();
        $iddd = $position->id;
        $users = DB::table('howtodos_steps')->where('id', '=', $noteid)->first();
foreach($adminnotes as $notess)
{ 
   $noteid1 = $noteid[$k];
   $note1 =$adminnotes[$k];
   $k++;
if(empty($noteid1))
{
$insert2 = DB::insert("insert into howtodos_steps(`steps`,`howtodo_id`) values('".$note1."','".$iddd."')");
}
else
{
$returnValue = DB::table('howtodos_steps')->where('id', '=', $noteid1)
->update([ 'steps' => $note1,
           'howtodo_id' =>$id,
           'noteid' =>$noteid3,
]);  
}
}
//return redirect('fac-Bhavesh-0554/howtodo/edit' , $iddd)->with('success','New How To Do Added Successfully');   
//return back()->with('success','New How To Do Added Successfully');
//return redirect()->route('howtodo.index');
return redirect('fac-Bhavesh-0554/howtodo')->with('success','New How To Do Added Successfully');   

    //return back()->with('success','Success fully Add How To Do'); 

}

public function howtodo11delete($id)
  {        
  // Contact_userinfo::where('id',$id)->delete(); 
   DB::table('howtodos_steps')->where('id', '=',$id)->delete();
   return back();     
   } 
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
     $homecontent = Howtodo::where('id',$id)->first(); 
     $howtodo=DB::table('howtodos_steps')->where('howtodo_id','=',$id)->get();
     $howtodo1=DB::table('howtodos_steps')->where('howtodo_id','=',$id)->get();
     return view('fscemployee.howtodo.edit',compact(['homecontent','howtodo','howtodo1']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $position = Howtodo::find($id);
        $position->subject= $request->subject;
        $position->website= $request->website;
        $position->telephone= $request->telephone;
        $position->software= $request->software;
        $noteid = $request->stepid;
         $noteid2 = $request->noteid;
$adminnotes= $request->step;
$k=0;
         $iddd = $position->id;
        $users = DB::table('howtodos_steps')->where('id', '=', $noteid)->first();
foreach($adminnotes as $notess)
{ 
   $noteid1 = $noteid[$k];
   $note1 =$adminnotes[$k];
      $noteid3 =$noteid2[$k];
   $k++;
   if(empty($noteid1))
    {
    $insert2 = DB::insert("insert into howtodos_steps(`steps`,`howtodo_id`,`noteid`) values('".$note1."','".$iddd."','".$noteid3."')");
    }
    else
    {
   $returnValue = DB::table('howtodos_steps')->where('id', '=', $noteid1)
->update([ 'steps' => $note1,
           'howtodo_id' =>$id,
           'noteid' =>$noteid3,
]);    
//$users = DB::table('howtodos_steps')->where('steps','')->delete();
}
}
        $position->update();
        return back()->with('success','Success fully update How To Do'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Howtodo::where('id',$id)->delete();
        return back();
    }
}
