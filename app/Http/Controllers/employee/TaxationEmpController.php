<?php
namespace App\Http\Controllers\employee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\formationsetup;
use App\Model\taxstate;
use Auth;
use DB;
use App\User;
use Illuminate\Support\Facades\Input;
use App\Model\Category;
use App\Model\Business;
use App\Model\Employee;
class TaxationEmpController extends Controller
{
    public function __construct()
    {
    $this->middleware('auth:employee');
    }  
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $position = formationsetup::where('type1','=','Taxation')->orwhere('type1','=','License')->get();
        $user_id  = Auth::user()->user_id;
        $uid  = Auth::user()->id;
        $employee = Employee::where('id',$user_id)->first();
        return view('fscemployee/emptaxation/emptaxation', compact(['position','employee']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { 
        $business = Business::all();
        $position = formationsetup::All();
        $category = Category::orderby('business_cat_name','asc')->get(); 
        $user_id  = Auth::user()->user_id;
        $uid  = Auth::user()->id;
        $employee = Employee::where('id',$user_id)->first();
        return view('fscemployee/emptaxation/create', compact(['position','category','business','employee ']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $this->validate($request,[
            //'question' =>'required',
        ]);
        $type = $request->tab;
        $position = new formationsetup;
        if($type=='Taxation'){
        $position->question= $request->question;
        $position->type= $request->typeofservice;
        $position->date= $request->expiredate1;
        $position->business_catagory_name= $request->business_catagory_name;
        }
        if($type=='License'){
        $position->question= $request->typeoflicense;
        $position->business_catagory_name= $request->business_catagory_name;
        $position->bussiness_name= $request->bussiness_name;
        $position->type= $request->typeofservice1;
        $position->state= $request->state;
        $position->cityname= $request->cityname;
        $position->countyname= $request->county;
        $position->countysnumber= $request->countynumber;
        $position->website= $request->website;
        $position->telephone= $request->telephone;
        $position->fax= $request->fax;
        $position->renew= $request->renew;
        $position->renewal= $request->renewal;
        if($request->renewal=='Individual')
        {
        $position->duedate1= '';
        $position->expiredate2='';
        $position->duedate='';
        $position->date= '';  
        }
        else{
        $position->duedate1= $request->duedate1;
        $position->expiredate2= $request->expiredate2;
        $position->duedate= $request->duedate;
        $position->date= $request->expiredate;
        }
        $position->renewalwebsite= $request->renewalwebsite;
        $position->authorityname= $request->authorityname;
        }
        $position->type1=  $request->tab;
        $position->save();
        return redirect('fscemployee/emptaxation')->with('success','Success fully add Question');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $business = Business::all();
        $taxstate = taxstate::All();
        $position = formationsetup::where('id',$id)->first(); 
        $category = Category::orderby('business_cat_name','asc')->get();
        $user_id  = Auth::user()->user_id;
        $uid  = Auth::user()->id;
        $employee = Employee::where('id',$user_id)->first();
        return view('fscemployee.emptaxation.edit',compact(['position','taxstate','category','business','employee']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $this->validate($request,[
            'typeoflicense' =>'required',
        ]);
        $position = formationsetup::find($id);
        $type = $request->tab;
        if($type=='Taxation')
        {
         $position->question= $request->question;
         $position->type= $request->typeofservice;
         $position->date= $request->expiredate1;
         $position->business_catagory_name= $request->business_catagory_name;
        }
        if($type=='License'){
        $position->business_catagory_name= $request->business_catagory_name;
        $position->bussiness_name= $request->bussiness_name;
        $position->question= $request->typeoflicense;
        $position->type= $request->typeofservice1;
        $position->state= $request->state;
        $position->cityname= $request->cityname;
        $position->countyname= $request->county;
        $position->countysnumber= $request->countynumber;
        $position->website= $request->website;
        $position->telephone= $request->telephone;
        $position->fax= $request->fax;
        $position->renew= $request->renew;
        if($request->renewal=='Individual')
        {
        $position->duedate1= '';
        $position->expiredate2='';
        $position->duedate='';
        $position->date= '';  
        }
        else
        {
        $position->duedate1= $request->duedate1;
        $position->expiredate2= $request->expiredate2;
        $position->duedate= $request->duedate;
        $position->date= $request->expiredate;
        }
        $position->renewal= $request->renewal;
        $position->authorityname= $request->authorityname;
        $position->renewalwebsite= $request->renewalwebsite;
        }
        $position->type1=  $request->tab;
        $position->update();
        return redirect('fscemployee/emptaxation')->with('success','Success fully update question'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        formationsetup::where('id',$id)->delete();
        return redirect(route('emptaxation.index'));
    }
}
