<?php
namespace App\Http\Controllers\employee;
use App\Model\Appointment;
use App\employees\Fscemployee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Employee;
use DB;
use Auth;
class EmpAppointmentController extends Controller
{
public function __construct()
    {
    $this->middleware('auth:employee');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id  = Auth::user()->user_id;
        $employee = Employee::where('id',$user_id)->first();
        
        return view('fscemployee/appointment/create',compact(['employee']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\State  $state
     * @return \Illuminate\Http\Response
     */
    public function show(State $state)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\State  $state
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {  
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\State  $state
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      
    }


public function getcounty(Request $request)
    {
      
    }
public function getcountycode(Request $request)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\State  $state
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }
}
