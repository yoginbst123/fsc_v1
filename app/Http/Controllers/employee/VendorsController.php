<?php
namespace App\Http\Controllers\employee;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Front\Commonregister;
use App\Model\BusinessBrand;
use App\Model\Employee;
use App\Model\Category;
use App\Model\Business;
use App\Model\client_professional;
use App\Model\client_shareholder;
use App\User;
use App\Model\Categorybusiness;
use App\Model\Contact_userinfo;
use App\Mail\Activationmail;
use App\Model\taxstate;
use DB;
use Hash;
use Carbon;
use App\Model\Accountcode;
use App\Model\Admin;
use Auth;
use App\Model\Price;
use App\Model\Currency;
use App\Model\Typeofser;
use App\Model\Period;
use App\Model\Taxtitle;
use App\employees\Fscemployee;
use Illuminate\Support\Facades\Input;
use App\Model\Product;

class VendorsController extends Controller
{
public function __construct()
{
$this->middleware('auth:employee');
}
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
public function index(Request $request)
{
    $user_id  = Auth::user()->user_id;
        $uid  = Auth::user()->id;
$employee = Employee::where('id',$user_id)->first();
//$name = $request->input('user_type');
$category = Category::All(); 
 $employee1 = DB::table('employees')->select('employees.*','utilities.id as uid','utilities.utilitiesname as uname','insurance_companies.id as iid','insurance_companies.insurance_company as iname')
            ->leftJoin('utilities', function($join){ $join->on('utilities.id', '=', 'employees.utilitiesname');})
            ->leftJoin('insurance_companies', function($join){ $join->on('insurance_companies.id', '=', 'employees.insurance_company');})
            
            ->where('employees.type','=','Vendor')->orderBy('employees.vendordate', 'desc')->get();
   
      return view('fscemployee.empvendor.empvendor',compact(['employee','employee1','category']));
}
/**
* Show the form for creating a new resource.
*
* @return \Illuminate\Http\Response
*/
public function create()
{     
     $user_id  = Auth::user()->user_id;
     $uid  = Auth::user()->id;
     $employee = Employee::where('id',$user_id)->first();
     $category = Category::orderby('business_cat_name','asc')->get(); 
    $products1 = DB::table('products')->get();
    $accountcode = Accountcode::All(); 
      $utilities = DB::table('utilities')->orderby('utilitiesname','asc')->get(); 
    $insurance = DB::table('insurance_companies')->orderby('insurance_company','asc')->get(); 
    $businesstypes = DB::table('other_businesstypes')->orderby('businesstype','asc')->get(); 
     $positions1 = DB::table('positions')->orderby('position','asc')->get();
   
  
    return view('fscemployee/empvendor/create',compact(['positions1','utilities','insurance','businesstypes','accountcode','products1','category','employee']));
}
/**
* Store a newly created resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @return \Illuminate\Http\Response
*/
public function store(Request $request)
{
 $this->validate($request,[
        'business_name' =>'required|unique:employees',
        'productid' =>'required'
    ]);
    
        $employee= new Employee;   
        $c = $request->productid;
        if(empty($c))
        {
            $List='';
        }
        else
        {
            $List = implode(', ', $c); 
        }
        
        $posc = $request->positions;
        if(empty($posc))
        {
            $poscList='';
        }
        else
        {
            $poscList = implode(', ', $posc); 
        }
        
        // $c1 = $request->business_catagory_name;
        // if(empty($c1))
        // {
        //     $List1='';
        // }
        // else
        // {
        //     $List1 = implode(', ', $c1); 
        // }
        
        // if($request->categorys =='Utility')
        // {
      
        //     $c2 = $request->utilitiesname;
        //     if(empty($c2))
        //     {
        //         $List2='';
        //     }
        //     else
        //     {
        //         $List2 = $request->utilitiesname; 
        //     }
        //         $employee->utilitiesname= isset($List2) ? $List2:"";
        // }  
        
        // if($request->categorys =='Insurance')
        // {
      
        //     $c3 = $request->insurance_company;
        //     if(empty($c3))
        //     {
        //         $List2i='';
        //     }
        //     else
        //     {
        //         $List2i = $request->insurance_company; 
        //     }
                
        //     $employee->insurance_company= isset($List2i) ? $List2i:"";
        // }
            
        //$employee->entity = isset($request->entity) ? $request->entity:"";
        
        if($request->entity =='Business')
        {
            $c1 = $request->business_catagory_name;
            if(empty($c1))
            {
                $List1='';
            }
            else
            {
                $List1 = implode(', ', $c1); 
            }
            $employee->business_catagory_name= isset($List1) ? $List1:"";
        }
        else
        {
            if($request->categorys =='Utility')
            {
      
                $c2 = $request->utilitiesname;
                if(empty($c2))
                {
                    $List2='';
                }
                else
                {
                    $List2 = $request->utilitiesname; 
                }
                
                $employee->utilitiesname= isset($List2) ? $List2:"";
            } 
            else
            {
                    $c3 = $request->insurance_company;
                    if(empty($c3))
                    {
                        $List2i='';
                    }
                    else
                    {
                        $List2i = $request->insurance_company; 
                    }
                        
                    $employee->insurance_company= isset($List2i) ? $List2i:"";
            
            }
            $employee->categorys= $request->categorys;  
        }
        
        
        $employee->employee_id = $request->employee_id;
          $employee->product= isset($List) ? $List:"";
      
        $employee->vendorid = Auth::user()->fname.' '.Auth::user()->mname.' '.Auth::user()->lname;
        $employee->type = 'Vendor';
        $employee->vendortype = 'Admin';
        
        $employee->billingtoo123= isset($request->billingtoo123) ? $request->billingtoo123:"";
        $employee->billingtoo= isset($request->billingtoo) ? $request->billingtoo:"";
        $employee->emailbli2= isset($request->emailbli2) ? $request->emailbli2:"";
        //$employee->categorys= $request->categorys;
        //$employee->categorys= $List1;
        $employee->entity=isset($request->entity) ? $request->entity:"";
        $employee->firstName= isset($request->firstname) ? $request->firstname:"";
        $employee->middleName= isset($request->middlename) ? $request->middlename:"";
        $employee->lastName= isset($request->lastname) ? $request->lastname:"";
        $employee->address1= isset($request->address) ? $request->address:"";
        $employee->address2= isset($request->address1) ? $request->address1:"";
        $employee->city= isset($request->city) ? $request->city:"";
        $employee->stateId= isset($request->stateId) ? $request->stateId:"";
        $employee->zip= isset($request->zip) ? $request->zip:"";
        $employee->countryId= isset($request->countryId) ? $request->countryId:"";
        $employee->telephoneNo1= isset($request->telephone) ? $request->telephone:"";
        $employee->telephoneNo2= isset($request->motelephoneile1) ? $request->motelephoneile1:"";
        $employee->ext1= isset($request->ext1) ? $request->ext1:"";
        $employee->ext2= isset($request->ext2) ? $request->ext2:"";
        $employee->telephoneNo1Type= isset($request->telephoneNo1Type) ? $request->telephoneNo1Type:"";
        $employee->telephoneNo2Type= isset($request->telephoneNo2Type) ? $request->telephoneNo2Type:"";
        $employee->email= isset($request->email) ? $request->email:"";
        $employee->nametype= isset($request->minss) ? $request->minss:"";
        $employee->fax= isset($request->business_fax) ? $request->business_fax:"";
        $employee->website= isset($request->website) ? $request->website:"";
        $employee->etelephone1= isset($request->mobile_1) ? $request->mobile_1:"";
        $employee->eteletype1= isset($request->mobiletype_1) ? $request->mobiletype_1:"";
        $employee->eext1= isset($request->ext2_1) ? $request->ext2_1:"";
        $employee->etelephone2= isset($request->mobile_2) ? $request->mobile_2:"";
        $employee->eteletype2= isset($request->mobiletype_2) ? $request->mobiletype_2:"";
        $employee->eext2= isset($request->ext2_2) ? $request->ext2_2:"";
        $employee->efax= isset($request->contact_fax) ? $request->contact_fax:"";
        $employee->business_name= isset($request->business_name) ? $request->business_name:"";
        $employee->accountcode= isset($request->accountcode) ? $request->accountcode:"";
        $employee->account_name= isset($request->account_name) ? $request->account_name:"";
        $employee->check= isset($request->status) ? $request->status:"";
        $employee->vendordate= date('Y-m-d');
        $employee->efax= isset($request->contact_fax_1) ? $request->contact_fax_1:"";
        $employee->account_belongs_to= isset($request->account_belongs_to) ? $request->account_belongs_to:"";
        
        
        $employee->positions= isset($poscList) ? $poscList:"";
        $employee->howtoshow= isset($request->howtoshow) ? $request->howtoshow:"";
        $employee->accno= isset($request->accno) ? $request->accno:"";
        
        $employee->managermr1= isset($request->managermr1) ? $request->managermr1:"";
        $employee->managerfname1= isset($request->managerfname1) ? $request->managerfname1:"";
        $employee->managermname1= isset($request->managermname1) ? $request->managermname1:"";
        $employee->managerlname1= isset($request->managerlname1) ? $request->managerlname1:"";
        
        $employee->managermr2= isset($request->managermr2) ? $request->managermr2:"";
        $employee->managerfname2= isset($request->managerfname2) ? $request->managerfname2:"";
        $employee->managermname2= isset($request->managermname2) ? $request->managermname2:"";
        $employee->managerlname2= isset($request->managerlname2) ? $request->managerlname2:"";
        
        $employee->contactmanagermr1= isset($request->contactmanagermr1) ? $request->contactmanagermr1:"";
        $employee->contactmanagerfname1= isset($request->contactmanagerfname1) ? $request->contactmanagerfname1:"";
        $employee->contactmanagermname1= isset($request->contactmanagermname1) ? $request->contactmanagermname1:"";
        $employee->contactmanagerlname1= isset($request->contactmanagerlname1) ? $request->contactmanagerlname1:"";
        
        $employee->contactmanagermr2= isset($request->contactmanagermr2) ? $request->contactmanagermr2:"";
        $employee->contactmanagerfname2= isset($request->contactmanagerfname2) ? $request->contactmanagerfname2:"";
        $employee->contactmanagermname2= isset($request->contactmanagermname2) ? $request->contactmanagermname2:"";
        $employee->contactmanagerlname2= isset($request->contactmanagerlname2) ? $request->contactmanagerlname2:"";
        
        $employee->vendordate= date('Y-m-d H:i:s');
        $employee->business_catagory_name=isset($List1) ? $List1:"";
        $employee->notes= isset($request->note) ? $request->note:"";
        $employee->save();
        return redirect('fscemployee/empvendor')->with('success','You Are Registered Successfully');
    }
/**
* Display the specified resource.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function show($id)
{
$common = Commonregister::where('id',$id)->first();
return View::make('fscemployee.empvendor.empvendor',compact(['common']));
}
/**
* Show the form for editing the specified resource.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function edit($id)
{ 
 $user_id  = Auth::user()->user_id;
        $uid  = Auth::user()->id;
     $employee = Employee::where('id',$user_id)->first();
$emp = Employee::where('id',$id)->first();
$category = Category::All();   
$products1 = DB::table('products')->get();
$accountcode = Accountcode::All(); 
 $insurance = DB::table('insurance_companies')->orderby('insurance_company','asc')->get(); 
 $businesstypes = DB::table('other_businesstypes')->orderby('businesstype','asc')->get(); 
   $utilities = DB::table('utilities')->orderby('utilitiesname','asc')->get(); 
     $positions1 = DB::table('positions')->orderby('position','asc')->get();
        $conversation = DB::table('conversation_sheet as t1')->select('t1.*','t2.id as ids','t2.relatednames')
           ->Join('relatednames as t2', function($join){ $join->on('t2.id', '=','t1.conrelatedname');})
           ->where('t1.vendorid','=',$id)
           ->get();

         $notesdata = DB::table('notes_sheet as t1')->select('t1.*','t2.id as ids','t2.notesrelated')
           ->Join('notesrelateds as t2', function($join){ $join->on('t2.id', '=','t1.noterelated');})
           ->where('t1.notevendorid','=',$id)
           ->get();
  
    $listclient = DB::table('commonregisters')->where('first_name','!=','')->where('last_name','!=','')->orderBy('first_name', 'ASC')->get();
    $listemployeeuser = DB::table('employees')->where('type','employee')->where('firstName','!=','')->where('lastName','!=','')->orderBy('firstName', 'ASC')->get();
    $listvendoe = DB::table('employees')->where('type','Vendor')->where('firstName','!=','')->where('lastName','!=','')->orderBy('firstName', 'ASC')->get();
    $NotesNames = DB::table('notesrelateds')->get();
 
return view('fscemployee.empvendor.edit',compact(['NotesNames','listvendoe','listemployeeuser','listclient','notesdata','conversation','positions1','utilities','insurance','businesstypes','accountcode','products1','employee','emp','category',]));
}
/**
* Update the specified resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @param  int  $id
* @return \Illuminate\Http\Response
*/

 

public function update(Request $request, $id)
{
   $employee= Employee::find($id);  
      
     $c = $request->productid;
     if(empty($c))
        {
            $List='';
        }
        else{
            $List = implode(', ', $c); 
        }
        
        $posc = $request->positions;
     if(empty($posc))
        {
            $poscList='';
        }
        else{
            $poscList = implode(', ', $posc); 
        }
     
        $c1 = $request->business_catagory_name;
        if(empty($c1))
        {
            $List1='';
        }
        else
        {
            $List1 = implode(', ', $c1); 
        }
         if($request->categorys =='Utility')
         {
      
        $c2 = $request->utilitiesname;
        if(empty($c2))
        {
            $List2='';
        }
        else
        {
            $List2 = $request->utilitiesname; 
        }
        $employee->utilitiesname= isset($List2) ? $List2:"";
         }  
            if($request->categorys =='Insurance')
         {
      
         $c3 = $request->insurance_company;
        if(empty($c3))
        {
            $List2i='';
        }
        else
        {
            $List2i = $request->insurance_company; 
        }
        $employee->insurance_company= isset($List2i) ? $List2i:"";
         } 
        $employee->billingtoo123= isset($request->billingtoo123) ? $request->billingtoo123:"";
        $employee->billingtoo= isset($request->billingtoo) ? $request->billingtoo:"";
        $employee->emailbli2= isset($request->emailbli2) ? $request->emailbli2:"";
       $employee->employee_id = isset($request->employee_id) ? $request->employee_id:"";
       $employee->vendordate= date('Y-m-d');
        $employee->firstName= isset($request->firstname) ? $request->firstname:"";
        $employee->middleName= isset($request->middlename) ? $request->middlename:"";
        $employee->lastName= isset($request->lastname) ? $request->lastname:""; 
        $employee->address1= isset($request->address) ? $request->address:"";
        $employee->address2= isset($request->address1) ? $request->address1:"";
        $employee->city= isset($request->city) ? $request->city:"";
        $employee->stateId= isset($request->stateId) ? $request->stateId:"";
        $employee->zip= isset($request->zip) ? $request->zip:"";
        $employee->countryId= isset($request->countryId) ? $request->countryId:"";
        $employee->telephoneNo1= isset($request->telephone) ? $request->telephone:"";
        $employee->telephoneNo2= isset($request->motelephoneile1) ? $request->motelephoneile1:"";
        $employee->ext1= isset($request->ext1) ? $request->ext1:"";
        $employee->ext2= isset($request->ext2) ? $request->ext2:"";
        $employee->telephoneNo1Type= isset($request->telephoneNo1Type) ? $request->telephoneNo1Type:"";
        $employee->telephoneNo2Type= isset($request->telephoneNo2Type) ? $request->telephoneNo2Type:"";
        $employee->email= isset($request->email) ? $request->email:"";
        $employee->nametype= isset($request->minss) ? $request->minss:"";
        $employee->fax= isset($request->business_fax) ? $request->business_fax:"";
        $employee->website= isset($request->website) ? $request->website:"";
        $employee->etelephone1= isset($request->mobile_1) ? $request->mobile_1:"";
        $employee->eteletype1= isset($request->mobiletype_1) ? $request->mobiletype_1:"";
         $employee->categorys= isset($request->categorys) ? $request->categorys:"";
       
        $employee->eext1= isset($request->ext2_1) ? $request->ext2_1:"";
        
        $employee->etelephone2= isset($request->mobile_2) ? $request->mobile_2:"";
        $employee->eteletype2= isset($request->mobiletype_2) ? $request->mobiletype_2:"";
        $employee->eext2= isset($request->ext2_2) ? $request->ext2_2:"";
        
        $employee->efax= isset($request->contact_fax) ? $request->contact_fax:"";
        $employee->efax= isset($request->contact_fax_1) ? $request->contact_fax_1:"";
        $employee->business_name= isset($request->business_name) ? $request->business_name:"";
        $employee->business_catagory_name= isset($List1) ? $List1:"";
        $employee->accountcode= isset($request->accountcode) ? $request->accountcode:"";
        $employee->account_name= isset($request->account_name) ? $request->account_name:"";
        $employee->account_belongs_to= isset($request->account_belongs_to) ? $request->account_belongs_to:"";
        $employee->product= isset($List) ? $List:"";
         $employee->positions= isset($poscList) ? $poscList:"";
        $employee->howtoshow= isset($request->howtoshow) ? $request->howtoshow:"";
        $employee->accno= isset($request->accno) ? $request->accno:"";
        
        $employee->notes= isset($request->note) ? $request->note:"";
        $employee->check= isset($request->status) ? $request->status:""; 
        
         $employee->managermr1= isset($request->managermr1) ? $request->managermr1:"";
        $employee->managerfname1= isset($request->managerfname1) ? $request->managerfname1:"";
        $employee->managermname1= isset($request->managermname1) ? $request->managermname1:"";
        $employee->managerlname1= isset($request->managerlname1) ? $request->managerlname1:"";
        
        $employee->managermr2= isset($request->managermr2) ? $request->managermr2:"";
        $employee->managerfname2= isset($request->managerfname2) ? $request->managerfname2:"";
        $employee->managermname2= isset($request->managermname2) ? $request->managermname2:"";
        $employee->managerlname2= isset($request->managerlname2) ? $request->managerlname2:"";
        
        $employee->contactmanagermr1= isset($request->contactmanagermr1) ? $request->contactmanagermr1:"";
        $employee->contactmanagerfname1= isset($request->contactmanagerfname1) ? $request->contactmanagerfname1:"";
        $employee->contactmanagermname1= isset($request->contactmanagermname1) ? $request->contactmanagermname1:"";
        $employee->contactmanagerlname1= isset($request->contactmanagerlname1) ? $request->contactmanagerlname1:"";
        
        $employee->contactmanagermr2= isset($request->contactmanagermr2) ? $request->contactmanagermr2:"";
        $employee->contactmanagerfname2= isset($request->contactmanagerfname2) ? $request->contactmanagerfname2:"";
        $employee->contactmanagermname2= isset($request->contactmanagermname2) ? $request->contactmanagermname2:"";
        $employee->contactmanagerlname2= isset($request->contactmanagerlname2) ? $request->contactmanagerlname2:"";
       
        $employee->update();
        return redirect(route('empvendor.index'))->with('success','Your Profile Update Successfully');
}

public function getcategory(Request $request)
{
$data = Category::select('business_cat_name','id')->where('bussiness_name',$request->id)->take(1000)->get();
return response()->json($data);  
}

public function locked2(Request $request)
{
    $lock=$request->id;
    $cid = $request->clientid;
  $data = DB::table('clientservices')->where('id',$cid)->update(['locked' => $lock]);
return response()->json($data); 
//return 'success';
}
public function getcategory_1(Request $request)
{
$data = Category::select('business_cat_name','id','business_cat_image')->where('id',$request->id)->take(1000)->get();
return response()->json($data);  
}
public function getcategory1(Request $request)
{
$data = Business::select('bussiness_name','id','bussiness_image_name','newimage')->where('id',$request->id)->take(1000)->get();
return response()->json($data);  
}
public function getcategory1m(Request $request)
{
$data = BusinessBrand::select('business_brand_name','id')->where('business_cat_id',$request->id)->take(1000)->get();
return response()->json($data);  
}
public function getcategoryimage(Request $request)
{
$data = BusinessBrand::select('business_brand_name','id','business_brand_image')->where('id',$request->id)->take(1000)->get();
return response()->json($data);  
}
public function getcategory1mm(Request $request)
{
$data = Categorybusiness::select('business_brand_category_name','id')->where('business_brand_id',$request->id)->take(100)->get();
return response()->json($data);  
}
public function business_brand_category_name(Request $request)
{
$data = Categorybusiness::select('business_brand_category_name','id','business_brand_category_image')->where('id',$request->id)->take(100)->get();
return response()->json($data);  
}

public function salestax1(Request $request)
{
if($request->typeofservice=='3')
{
$data = DB::table('taxationservice')->select('regularprice1','id','comboprice1')->where('currency',$request->currency)->where('typeofservice',$request->typeofservice)->where('period',$request->serviceperiod)->where('title',$request->id)->take(100)->get();
}
else
{
$data = DB::table('prices')->select('regularprice','id','comboprice','serviceincludes')->where('currency',$request->currency)->where('typeofservice',$request->id)->where('period',$request->serviceperiod)->take(100)->get();
}
return response()->json($data);  
}

public function deletecon( Request $request)
    { 
    $users =  DB::table('contact_userinfos')->where('id', '=',$request->input('id'))->delete();
      
       if($users)
        {
            echo 'Data Deleted';
        }
    }
   
  public function notedelete($id)
  {        
   DB::table('notes')->where('id', '=',$id)->delete();
   return redirect('fac-Bhavesh-0554/vendor/');     
   }
   
    public function locksdelete($id)
  {        
   DB::table('clientservices')->where('id', '=',$id)->delete();
   return redirect('fac-Bhavesh-0554/vendor/');     
   }
/*
* 
* Remove the specified resource from storage.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/

public function destroy($id)
{
Employee::where('id',$id)->delete();
$user = User::where('user_id', '=', $id)->delete();
return redirect(route('empvendor.index'))->with('error','Your Record Deleted Successfully');
}
}