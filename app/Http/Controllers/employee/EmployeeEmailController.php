<?php
namespace App\Http\Controllers\employee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\employee\Leave;
use App\Model\Employee;
use App\employees\Fscemployee;
use App\Model\Email;
use DB;use Auth;
use App\Model\Logo;
use App\User;
use Illuminate\Support\Facades\Input;

class EmployeeEmailController extends Controller
{
    public function __construct()
    {
    $this->middleware('auth:employee');
    }  
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   $user_id  = Auth::user()->user_id;
      //$email = Email::where('for_whom','=',$user_id)->get();
     //   $email = Email::get();
           $email = DB::table('email_emp')->select('employees.email_access','email_emp.id as ids','email_emp.for_whom','email_emp.email','email_emp.access_address','email_emp.password','email_emp.ext','email_emp.telephone','email_emp.location','employees.id as eid','employees.status','employees.type','employees.firstName','employees.middleName','employees.lastName','employees.check')
        ->leftJoin('employees', function($join){ $join->on('employees.id', '=', 'email_emp.for_whom');})
        ->where('employees.status', '=', "1")->where('employees.check', '=', "1")
        ->where('email_emp.for_whom', '=', $user_id)->first();  
   
        $emp =   Employee::get();
        $employee = Employee::where('id',$user_id)->first();
        $logo = Logo::where('id','=',1)->first();
        
        $maiEmail = DB::table('mainemail')->first();
        
        // print_r($maiEmail->mainemail);
        // exit();
         $emp_emai_rights = DB::table('emp_email_rights as remp')->select('eemp.*','remp.access','remp.id as rempid')
                            ->leftjoin('email_emp as eemp',function($join){$join->on('remp.emp_email_id','=','eemp.id');})->where('remp.emp_id',$user_id)
                            ->get();
        //$email_emp = DB::table();
        
        return view('fscemployee/eemail/eemail',compact(['emp','email','employee','logo','maiEmail','emp_emai_rights']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {  
     $emp1 = Employee::where('check','=',1)->get();
     
     return view('fscemployee/eemail/create',compact(['emp1']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request,[
            'for_whom' =>'required',
            'email' =>'required',
            'access_address' =>'required',
            'password' =>'required',                                
        ]);
        $branch  = new Email;
        $branch->for_whom = $request->for_whom;
        $branch->email = $request->email;
        $branch->access_address = $request->access_address;
        $branch->password = $request->password;
        $branch->save();
        return redirect('fscemployee/eemail')->with('success','Success fully add Email');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
     //$branch = Commonregister::where('id',$id)->first(); 
     //$position = Employee::All(); 
     $user_id  = Auth::user()->user_id;
      
     
   $emp =   Employee::where('check','=',1)->where('type','!=','clientemployee')->where('type','!=','Vendor')->where('type','!=','')->orderBy('firstName', 'asc')->get();;
     $leave = Email::where('id',$id)->first();
       $email = Email::All(); 
           $employee = Employee::where('id',$user_id)->first();
     return view('fscemployee/eemail.edit',compact(['leave','emp','email','employee']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $branch = Email::find($id);
        $branch->password = $request->password;
        $branch->update();
        return redirect('fscemployee/eemail')->with('success','Success fully Update Email');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Email::where('id',$id)->delete();
        return redirect('fscemployee/email');
    }
}
