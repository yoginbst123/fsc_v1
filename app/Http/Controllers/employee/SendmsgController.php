<?php
namespace App\Http\Controllers\employee;
use App\Model\Msg;
use App\Model\Admin;
use App\employees\Fscemployee;
use App\Model\Employee;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Front\Commonregister;
use DB;
class SendmsgController extends Controller
{
public function __construct()
    {
    $this->middleware('auth:employee');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    { 
        $start = Auth::user()->user_id;
        $silver = DB::table("commonregisters")
                ->select("commonregisters.first_name","commonregisters.middle_name","commonregisters.last_name","commonregisters.address","commonregisters.city","commonregisters.stateId","commonregisters.countryId"
                ,"commonregisters.business_no","commonregisters.email","commonregisters.status","commonregisters.id")->where('commonregisters.status', '=',"Approval");
        $emp = DB::table("employees")->select("employees.firstName","employees.middleName","employees.lastName","employees.address1","employees.city","employees.stateId","employees.countryId"
                ,"employees.telephoneNo1","employees.email","employees.type","employees.id")->where('employees.check', '=', "1")->unionAll($silver)->get();  
        $admin1 = Admin::where('id','=',1)->first();
        $admin = Employee::All();
         $user_id  = Auth::user()->user_id;
        $employee = Employee::where('id','=',$user_id)->first();
        $task = Msg::where('employeeid','=',$start)->orwhere('admin_id','=',$start)->orwhere('forword_id','=',$start)->where('recieve','=','1')->orderBy('id', 'DESC')->get();
        return view('fscemployee/sendmessage/sendmessage', compact(['task','emp','admin','employee','admin1']))->with('i', ($request->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         
       $purpose= DB::table('purposes')->get();
       $id  = Auth::user()->id;
       $user_id  = Auth::user()->user_id;
       $employee = Employee::where('id','=',$user_id)->where('check','=','1')->first();
       $employee1 = Fscemployee::where('user_id','!=',$user_id)->where('type','=','1')->orderBy('name', 'asc')->get();
       $employee3 = Employee::where('id','!=',$user_id)->where('status','=','1')->where('type','!=','clientemployee')->orderBy('firstName', 'asc')->get();
       
      //echo "<pre>";  print_r($employee3);
       
       $client = Commonregister::where('status','=','Active')->orderBy('first_name', 'asc')->get();
       return view('fscemployee/sendmessage/create',compact(['employee3','employee','purpose','employee1','client']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //echo "<pre>";print_r($_POST);EXIT;
          $start = Auth::user()->id;
          $start1 = Auth::user()->user_id;
          if(empty($request->busname2))
          {
              $names_str='';
          }
          else{
          $names_arr = $request->busname2;
          $names_str = implode(" , ",$names_arr);
          }
          $employee = Employee::where('id','=',$start1)->first();
          $type = $employee->type;
          $position = new Msg;
          $position->busname2= $names_str;
          $position->date= $request->date;
          $position->time= $request->time;
          $position->day= $request->day;
          $position->title= $request->type;
          $position->content= $request->othermsg;
          if(empty($request->clientname))
          { 
              $position->admin_id= $request->employee;
          }
          else
          {
             $position->admin_id= $request->clientname; 
          }
          $position->employee_id= $start1;
          $position->employeeid= $request->employees;
          $position->type= $request->type;
          $position->rlt_msg= $request->other;
          $position->call_back= $request->purpose1;
          $position->busname= $request->busname;
          $position->clientfile= $request->clientfile;
          $position->clientno= $request->clientno;
          $position->clientname= $request->clientname;
          $position->purpose= $request->purpose;
          $position->send= 1;
          $position->send2= 1;
          $position->recieve= 1;
          $position->status= 2;
          $position->status3= 'New';
          
          $position->save();
          return redirect('fscemployee/sendmessage')->with('success','Success fully add Message');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\State  $state
     * @return \Illuminate\Http\Response
     */
    public function show(State $state)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\State  $state
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {       
       $user_id  = Auth::user()->user_id;
       $purpose= DB::table('purposes')->get();
       $task = Msg::where('id',$id)->where('employeeid',$user_id)->first(); 
       //print_r($task);die;
       $employee = Employee::where('id','=',$user_id)->where('check','=','1')->first();
       $employee1 = Employee::where('id','=',$task->employeeid)->first();
       $userclient1 = Commonregister::where('id','=',$task->admin_id)->get();
       $countries = Msg::where('employeeid',$user_id)->update(['recieve' => '2']);
       $emp = Fscemployee::All();
       $admin = Admin::All();
       
        return View('fscemployee.sendmessage.edit',compact(['task','emp','admin','employee','employee1','purpose']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\State  $state
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $this->validate($request,[
            'title' =>'required',
            'description' =>'required',
            'employee' =>'required',
        ]);
         $position =Msg::find($id);
         $position->title= $request->type;
         $position->content= $request->othermsg;
         if(empty($request->clientname))
          { 
              $position->admin_id= $request->employee;
          }
          else
          {
             $position->admin_id= $request->clientname; 
          }
         $position->employeeid= $start;
         $position->type= $type;
         $position->rlt_msg= $request->other;
         $position->call_back= $request->purpose1;
         $position->date= $request->date;
         $position->time= $request->time;
         $position->day= $request->day;
         $position->busname= $request->busname;
         $position->clientfile= $request->clientfile;
         $position->clientno= $request->clientno;
         $position->clientname= $request->clientname;
         $position->purpose= $request->purpose;
         $position->status1= $request->status1;
         $position->returndate= $request->returndate;
         $position->notes= $request->notes;
         $position->update();
         return redirect('fac-Bhavesh-0554/sendmessage')->with('success','Success fully update Message');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\State  $state
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        Msg::where('id',$id)->delete();
        return redirect(route('sendmessage.index'));
    }
}
