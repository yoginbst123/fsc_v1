<?php
namespace App\Http\Controllers\employee\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use App\employees\Fscemployee;
class FscemployeeForgotPasswordController extends Controller
{
    
public function __construct()
    {
        $this->middleware('guest:employee');
    }
/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
      return view('fscemployee/forgotpassword/forgotpassword');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {  
      $email = $request->email;
      $user = Fscemployee::where('email', '=', $email)->first();
     if ($user === null) 
    {
   return redirect(route('forgotpassword.index'))->with('error', 'This Email not found');
    }
   else
   {
    $activation_link = route('forgotpassword.edit', ['id' => $user->id,'email' => $email, 'token' => urlencode($user->remember_token)]);
    
    $msg = 'Forgot Password';
    $data = array('email' => $email,'token'=>$activation_link,'messages'=>$msg); 
  \Mail::send( 'fscemployee/forgotpasswordemployee', $data, function( $message ) use ($data)
       {
           $message->to($data['email'])->from($data['email'],$data['messages'],$data['token'])->subject('Forgot Password');
       });      
       return redirect(route('forgotpassword.index'))->with('success', 'Please Check Your Email');
   }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $business = Fscemployee::where('id',$id)->first();    
        return View('forgotpassword.index',compact('business')); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {  
        $business = Fscemployee::where('id',$id)->first(); 
        return view('fscemployee/forgotpassword/edit',compact('business'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'newpassword'=>'required',            
                        
        ]);
       $business =Fscemployee::find($id);
        $business->password = bcrypt($request->newpassword);
         $business->update();
        return redirect(route('forgotpassword.index'))->with('success', 'Your Password Successfully update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
