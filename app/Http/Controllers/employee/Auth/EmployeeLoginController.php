<?php
namespace App\Http\Controllers\employee\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Foundation\Auth\AuthenticatesEmployee;
use Auth;
class EmployeeLoginController extends Controller
{	
  /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesEmployee;
    
        /**
         * Where to redirect users after login.
         *
         * @var string
         */
       protected $redirectTo = 'fscemployee/';
    
        /**
         * Create a new controller instance.
         *
         * @return void
         */
        
        public function __construct()
        {
            $this->middleware('guest:employee')->except('logout');
        }
        
        public function logout1(Request $request)
        {
            $this->guard('employee')->logout1();
            $request->session()->invalidate();
            return redirect('/login');
        }

}