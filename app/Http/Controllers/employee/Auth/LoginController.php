<?php
namespace App\Http\Controllers\employee\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\employees\Fscemployee;
use Auth;
class LoginController extends Controller
{
    public function __construct()
    {
      $this->middleware('guest:employee');
    }
    public function showLoginForm()
    {
      return view('fscemployee.login');
    }
    public function login(Request $request)
    {
        
      // Validate the form data
      $this->validate($request, [
        'email'   => 'required|email',
        'password' => 'required|min:6'
      ]);
      // Attempt to log the user in
     $user = Fscemployee::where('email', $request->email)->first();
    if($user==Null)
    {
        return redirect()->back()->withInput($request->only('email', 'remember'))->with('error','Your Username Password  Not Registered!!!');
    }
    else
    {
     if($user->type==1)
    {
      // echo $request->email;exit;
        //echo $request->password;exit;
      if (Auth::guard('employee')->attempt(['email' => $request->email, 'password' => $request->password, 'type' => 1], $request->remember)) {
        // if successful, then redirect to their intended location
        //exit('1111');
        //return redirect()->intended(route('fscemployee.dashboard'))->with('success','Welcome Our Dashboard!!!');
        return redirect(route('fscemployee.dashboard'))->with('success','Welcome Our Dashboard!!!');
      }
    }
    else if($user->type==0)
    {
        return redirect()->back()->withInput($request->only('email', 'remember'))->with('error','Your Account is Inactive pelase contact your admin!!!');
    }
      }
      // if unsuccessful, then redirect back to the login with the form data
      return redirect()->back()->withInput($request->only('email', 'remember'))->with('error','Your Username Password Invalid!!!');
    }

    protected function credentials(Request $request)
    {
 //$input = $request->only($this->username(), 'password');
   // $input['type'] = 1; //Add extra key,value which need to be checked.
    return ['email'=>$request->email,'password'=>$request->password,'type'=>1];
    }
    public function logout(Request $request)
    {
        Auth::guard('employee')->logout();
        $request->session()->flush();
        $request->session()->regenerate();
       return redirect()->guest(route( 'fscemployee.login.submit' ));
    }
}