<?php
namespace App\Http\Controllers\employee;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Model\Schedule;
use App\Model\Branch;
use App\Model\Employee;
use App\Model\empschedule;
use App\Model\Logo;
use DB;
use Hash;
use Auth;
use App\Model\Utilities;
use App\Http\Controllers\Controller;
use App\Model\Schedulesetup;
use Illuminate\Support\Facades\Input;
use App\Model\Other_businesstype;
use App\Model\InsuranceCompany;
class SchedulesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:employee');
    } 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $user_id  = Auth::user()->user_id;
        $employee = Employee::where('id',$user_id)->first();
        $logo = Logo::where('id','=',1)->first();
        $branch = Branch::All(); 
        $schedule = Schedule::where('emp_name','=',$user_id)->get();
        $emp = Employee::orderBy('firstName', 'asc')->get(); 
        $empschedule = empschedule::All();
        $schedulesetup = Schedulesetup::All();
        
        return view('fscemployee.schedules.schedule',compact(['logo','branch','schedule','emp','empschedule','schedulesetup','employee']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $branch = Branch::All(); 
       $user_id  = Auth::user()->user_id;
       $employee = Employee::where('id',$user_id)->first();
       return view('fscemployee/schedules/create',compact(['branch','employee']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $this->validate($request,[
            'emp_city' =>'required',
            'emp_name' =>'required',
            'duration' =>'required',
            'sch_start_date' =>'required',                     
        ]);
        $position = new Schedule;
        $position->emp_city= $request->emp_city;
        $position->type= 'employee';
        $position->emp_name= $request->emp_name;
        $position->duration= $request->duration;
        $position->sch_start_date= $request->sch_start_date;
        $position->sch_end_date= $request->sch_end_date;
        $position->schedule_in_time= $request->schedule_in_time;
        $position->schedule_out_time= $request->schedule_out_time;
        $position->sch_start_day= $request->sch_start_day;
        $position->sch_end_day= $request->sch_end_day;
        $position->save();
        $lastId = $position->id;
        $date_from = $request->sch_start_date;  
        $date_from1 = $request->sch_start_date;  
        $date_from2 = $request->schedule_in_time;
        $date_from3 = $request->schedule_out_time;
        $date_from = strtotime($date_from);
        $date_to = $request->sch_end_date; 
        $date_to1 = $request->sch_end_date; 
        $date_to = strtotime($date_to);
       
        for($i=$date_from; $i<=$date_to; $i+=86400) 
       {
       $day = date("D", $i);
       if($day=='Sun'){
           $insert2 = DB::insert("insert into schedule_emp_dates(`emp_sch_id`,`date_1`,`clockin`,`clockout`,`status`) values('".$lastId."','".$i."','','','1')");  
       }
       else if($day=='Sat')
       {
        $insert2 = DB::insert("insert into schedule_emp_dates(`emp_sch_id`,`date_1`,`clockin`,`clockout`,`status`) values('".$lastId."','".$i."','','','1')");     
       }
       else{
       $insert2 = DB::insert("insert into schedule_emp_dates(`emp_sch_id`,`date_1`,`clockin`,`clockout`) values('".$lastId."','".$i."','".$date_from2."','".$date_from3."')");  
       }
       }
       return redirect('fscemployee/schedules')->with('success','Success fully add Schedule');
    }

    public function updateintaxation(Request $request)
    {
    
        $taxation_id= $request->taxation_id;
        $taxfillingyear2= $request->taxfillingyear2;
        $taxfillingstatus2= $request->taxfillingstatus2;
        
        
        
        $returnValue = DB::table('personaltaxation')->where('id',$taxation_id)
                ->update([ 
                            'year' =>$taxfillingyear2,
                            'status' =>$taxfillingstatus2
                    ]);
        
        return redirect()->back()->with('success','Your record Updated Successfully');
    }
    
    public function utilitiess()
    {   
        $newopt = Input::get('newopt');
        if (DB::table('utilities')->where('utilitiesname', '=', $newopt)->exists()) 
        {
                //return response()->json(['status' => 'error']);
        }   
        else 
        { 
            $position = new Utilities;
            $position->utilitiesname = $newopt;
            $position->save();
             if ($position) 
             {
                return response()->json([
                'utilitiesname'     => 'success',
                 'newopt'     => $newopt]);
            } 
            else
            {
                return response()->json([
                    'utilitiesname' => 'error']);
            }
        }
    }
    
    public function removeutilitiess(Request $request)
    { 
       $student = Utilities::find($request->input('id'));
        if($student->delete())
        {
            echo 'Data Deleted';
        }
    }
    
    public function otherbusinesss()
    {   
        $newopt = Input::get('newopt');
        if (DB::table('other_businesstypes')->where('businesstype', '=', $newopt)->exists()) 
        {
                //return response()->json(['status' => 'error']);
        }   
        else 
        { 
            $position = new Other_businesstype;
            $position->businesstype = $newopt;
            $position->save();
             if ($position) 
             {
                return response()->json([
                'businesstype'     => 'success',
                 'newopt'     => $newopt]);
            } 
            else
            {
                return response()->json([
                    'businesstype' => 'error']);
            }
        }
    }
    
    public function removebusinesss(Request $request)
    { 
       $student = Other_businesstype::find($request->input('id'));
        if($student->delete())
        {
            echo 'Data Deleted';
        }
    }
    
    public function insurances()
    {   
        $newopt = Input::get('newopti');
        if (DB::table('insurance_companies')->where('insurance_company', '=', $newopt)->exists()) 
        {
                //return response()->json(['status' => 'error']);
        }   
        else 
        { 
            $position = new InsuranceCompany;
            $position->insurance_company = $newopt;
            $position->save();
             if ($position) 
             {
                return response()->json([
                'insurance_name'     => 'success',
                 'newopt'     => $newopt]);
            } 
            else
            {
                return response()->json([
                    'insurance_name' => 'error']);
            }
        }
    }
    
    public function removeinsurances(Request $request)
    { 
       $student = InsuranceCompany::find($request->input('id'));
        if($student->delete())
        {
            echo 'Data Deleted';
        }
    }
    
    public function getWeges(Request $request)
    {
        $formationRow = DB::table('employees')->where('id',$request->wegesid)->first();
        return response()->json($formationRow);
    }
    
    public function getTaxations(Request $request)
    {
        $formationRow = DB::table('personaltaxation')->where('id',$request->taxationid)->first();
        return response()->json($formationRow);
    }
    
    public function getExpense(Request $request)
    {
        $formationRow = DB::table('expense')->where('id',$request->expenseid)->first();
        return response()->json($formationRow);
    }
    
    public function otherWeges(Request $request)
    {
        $formationRow = DB::table('otherincome')->where('id',$request->otherid)->first();
        return response()->json($formationRow);
    }
    
    
    public function updateexpense(Request $request)
    {
        $expense_id= $request->expense_id;
        $client_expense_id= $request->client_expense_id;
        $expense= $request->expense2;
        $typeinsurance= $request->typeinsurance2;
        $typeutilities=$request->typeutilities2;
        
        $years=$request->years;
        $policystartdate=$request->policystartdate;
        $policyenddate=$request->policyenddate;
        $placetype=$request->placetype;
        $insurancepro=$request->insurancepro;
        $agentname=$request->agentname;
        $agenttel=$request->agenttel;
        $policy=$request->policy;
        $incomeself=$request->incomeself;
        $incomespouse=$request->incomespouse;
    
    
        DB::table('expense')->where('id',$expense_id)
                ->update([ 
                            'expense' => $expense,
                            //'typeinsurance' =>$typeinsurance,
                            'typeutilities' =>$typeutilities
                    ]);
                    
        // DB::table('expense_health')
        //         ->insert([
        
        $health_id = DB::table('expense_health')
                    -> insertGetId(array(
                            'expense_id' => $expense_id,
                            'client_id' =>$client_expense_id,
                            'years' =>$years,
                            'policystartdate' =>$policystartdate,
                            'policyenddate' =>$policyenddate,
                            'placetype' =>$placetype,
                            'insurancepro' =>$insurancepro,
                            'agentname' =>$agentname,
                            'agenttel' =>$agenttel,
                            'policy' =>$policy,
                            'incomeself' =>$incomeself,
                            'incomespouse' =>$incomespouse,
                    ));
                            
                    // ]);       
                    
        $personalnames = $request->personalnames;
        $startdatepolicy= $request->startdatepolicy;
        $enddatepolicy= $request->enddatepolicy;
        $i = 0;
        
        foreach($personalnames as $post)
        { 
            $health_ids=$health_id;
            $expense_id= $request->expense_id;
            $client_expense_id= $request->client_expense_id;
            $personalnames1 = $post;
            $startdatepolicy1 = $startdatepolicy[$i];
            $enddatepolicy1 = $enddatepolicy[$i];
            $i++; 
            if(!empty($personalnames1))
            {
                DB::insert("insert into expense_health_policy(`health_id`,`expense_id`,`client_id`,`personalnames`,`startdatepolicy`,`enddatepolicy`)
                values('".$health_ids."','".$expense_id."','".$client_expense_id."','".$personalnames1."','".$startdatepolicy1."','".$enddatepolicy1."')");        
            }
            else
            {
                
            }
        }            
        
        return redirect()->back()->with('success','Your record Updated Successfully');
    }
    
    public function updateotherweges(Request $request)
    {
        $this->validate($request,[
                //'business_catagory_name' =>'required',
                //'business_name' =>'required|unique:employees',
                //'productid' =>'required'
            ]);
            
        
        //$wegesyear = $request->employeryear;
        $otherid= $request->other_id;
        $bankname=$request->employerbankname;
        $address=$request->employeraddress;
        $city=$request->employercity;
        $stateId=$request->employerstate;
        $zip= $request->employerzip;
        $business_no= $request->employertelephone;
        //$incometype= $request->incometype;
        
        
        $returnValue = DB::table('otherincome')->where('id',$otherid)
                ->update([ 
                            //'income_id' => $income_id,
                            //'client_id' =>$client_id,
                            //'wegesyear' =>$wegesyear,
                            'bankname' =>$bankname,
                            'address' =>$address,
                            'city' =>$city,
                            'stateId' =>$stateId,
                            'zip' =>$zip,
                            'business_no' =>$business_no,
                            //'incometype' =>$incometype,
                    ]);
        
        return redirect()->back()->with('success','Your record Updated Successfully');
    }
    
    public function updateincomeweges(Request $request)
    {
    
        $wegesid= $request->income_id;
        $nametype= $request->nametype;
        $firstName=$request->firstName;
        $middleName=$request->middleName;
        $lastName=$request->lastName;
        $address1=$request->address1;
        $city= $request->city;
        $stateId= $request->stateId;
        $zip=$request->zip;
        $positions=$request->positions;
        $employerstdate=$request->employerstdate;
        $employereddate= $request->employereddate;
        $vendordate= date('Y-m-d H:i:s');
        
        $returnValue = DB::table('employees')->where('id',$wegesid)
                ->update([ 
                            'nametype' => $nametype,
                            'firstName' =>$firstName,
                            'middleName' =>$middleName,
                            'lastName' =>$lastName,
                            'address1' =>$address1,
                            'city' =>$city,
                            'stateId' =>$stateId,
                            'zip' =>$zip,
                            'positions' =>$positions,
                            'employerstdate' =>$employerstdate,
                            'employereddate' =>$employereddate,
                            'vendordate'=>$vendordate
                    ]);
        
        return redirect()->back()->with('success','Your record Updated Successfully');
    }
    
    public function updatedependent(Request $request)
    {
        $this->validate($request,[
                //'business_catagory_name' =>'required',
                //'business_name' =>'required|unique:employees',
                //'productid' =>'required'
            ]);
            
        $dependent_id= $request->dependent_id;
        $dependyear= $request->dependyear;
        $dependentrelation=$request->dependentrelation;
        $dependentfname=$request->dependentfname;
        $dependentlname=$request->dependentlname;
        $dependentmonth=$request->dependentmonth;
        $dependentyear= $request->dependentyear;
        $dependentcitizen= $request->dependentcitizen;
        
        $returnValue = DB::table('personaldependent')->where('id',$dependent_id)
                ->update([ 
                            'dependyear' => $dependyear,
                            'dependentrelation' =>$dependentrelation,
                            'dependentfname' =>$dependentfname,
                            'dependentlname' =>$dependentlname,
                            
                            'dependentmonth' =>$dependentmonth,
                            'dependentyear' =>$dependentyear,
                            'dependentcitizen' =>$dependentcitizen,
                    ]);
        
        return redirect()->back()->with('success','Your record Updated Successfully');
    }
    
    
   public function incomeadd()
    {   
        //$incomeyear=$_POST['incomeyear'];
        $incomesource=$_POST['incomesource'];
        $id=$_POST['id'];
        
        $insert = DB::insert("insert into personalincome(`client_id`,`incomesource`) values('".$id."','".$incomesource."')");
        
         if ($insert) {
        return response()->json([
            'incomesource'     => 'success']);
            } else {
                return response()->json([
                    'incomesource' => 'error']);
            }

    }
    
    
    public function emptaxadd()
    {   
        $year=$_POST['year'];
        $status=$_POST['status'];
        $id=$_POST['id'];
        
        $insert = DB::insert("insert into personaltaxation(`year`,`status`,`client_id`) values('".$year."','".$status."','".$id."')");
        
         if ($insert) {
        return response()->json([
            'status'     => 'success',
             'year'     => $year]);
            } else {
                return response()->json([
                    'status' => 'error']);
            }

    }
    
    
    
    public function getDependent(Request $request)
    {
        $formationRow = DB::table('personaldependent')->where('id',$request->dependentid)->first();
        return response()->json($formationRow);
    }
    
    public function dependentadd()
    {   
        
        $dependyear=$_POST['dependyear'];
        $relation=$_POST['dependentrelation'];
        $fname=$_POST['dependentfname'];
        $lname=$_POST['dependentlname'];
        //$mname=$_POST['dependentmname'];
        $dependentmonth=$_POST['dependentmonth'];
        $dependentyear=$_POST['dependentyear'];
        $dependentcitizen=$_POST['dependentcitizen'];
        
        
        
        $id=$_POST['id'];
        
        // $insert = DB::insert("insert into personaldependent(`dependyear`,`dependentrelation`,`dependentfname`,`dependentlname`,`dependentmname`,`dependentmonth`,`dependentyear`,`dependentcitizen`,`client_id`)
        // values('".$dependyear."','".$relation."','".$fname."','".$lname."','".$mname."','".$dependentmonth."','".$dependentyear."','".$dependentcitizen."','".$id."')");
        $insert = DB::insert("insert into personaldependent(`dependyear`,`dependentrelation`,`dependentfname`,`dependentlname`,`dependentmonth`,`dependentyear`,`dependentcitizen`,`client_id`)
        values('".$dependyear."','".$relation."','".$fname."','".$lname."','".$dependentmonth."','".$dependentyear."','".$dependentcitizen."','".$id."')");
        
         if ($insert) {
        return response()->json([
            'status'     => 'success',
             'relation'     => $relation]);
            } else {
                return response()->json([
                    'status' => 'error']);
            }

    }
    
    
    public function expenseadd()
    {   
       // $incomeyear=$_POST['incomeyear'];
        $expense=$_POST['expense'];
        $id=$_POST['id'];
        $typeinsurance=$_POST['typeinsurance'];
        $typeutilities=$_POST['typeutilities'];
        
        $insert = DB::insert("insert into expense(`client_id`,`expense`,`typeinsurance`,`typeutilities`) values('".$id."','".$expense."','".$typeinsurance."','".$typeutilities."')");
        
         if ($insert) {
        return response()->json([
            'expense'     => 'success']);
            } else {
                return response()->json([
                    'expense' => 'error']);
            }

    }
    
    public function deleteexp(Request $request,$id)
    {   
        DB::table('expense')->where('id','=',$id)->delete();
        return redirect()->back()->with('success','Your Record Deleted Successfully ');
    }
    
    public function destroyfilling(Request $request,$id)
    {   
        DB::table('personaltaxation')->where('id','=',$id)->delete();
        return redirect()->back()->with('success','Your Record Deleted Successfully ');
    }
    
    public function destroydependent(Request $request,$id)
    {   
        DB::table('personaldependent')->where('id','=',$id)->delete();
        return redirect()->back()->with('success','Your Record Deleted Successfully ');
    }
    
public function fscemployee1(Request $request)
{
$data = Employee::select('firstName','lastName','id')->where('branch_city',$request->id)->where('type','employee')->where('check','1')->take(100)->get();
return response()->json($data);  
}
public function fscgetduration1(Request $request)
{
$data = Employee::select('pay_frequency','id')->where('id',$request->id)->first();
$data1 = $data->pay_frequency;
$data2 = Schedulesetup::select('duration','id','sch_start_date','sch_start_day','sch_end_date','sch_end_day')->where('duration',$data1)->take(100)->get();
return response()->json($data2);  
}

public function schedules(Request $request)
{
    if($request->companyId1=='1' && ! empty($request->companyId2))
    {
     $returnValue = DB::table('schedule_emp_dates')->where('id', '=', $request->companyId)->update(['schedule_status11' => 2,'status' => 0,'clockin' => $request->companyId2, 'clockout' =>$request->companyId3]);   
    }
    else
    {
   $returnValue = DB::table('schedule_emp_dates')->where('id', '=', $request->companyId)->update(['schedule_status11' => 1,'status' => 1,'clockin' => '', 'clockout' =>'']);
    }
 return response()->json($returnValue);  
}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function edit(Schedule $schedule)
    { 
      $branch = Branch::All();
      $na = $schedule->emp_name;
      $na1 = $schedule->id;
      $duration= $schedule->duration;
      $user_id  = Auth::user()->user_id;
      $employee = Employee::where('id',$user_id)->first();
      $emp = Employee::where('id',$na)->first();
      $empschedule = empschedule::where('emp_sch_id',$na1)->orderBy('id', 'asc')->get();
      $schedulesetup =Schedulesetup::where('duration','=',$duration)->first();
      return View('fscemployee.schedules.edit',compact(['branch','schedule','emp','empschedule','schedulesetup','employee']));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Schedule $schedule)
    {
       $this->validate($request,[
            'emp_city' =>'required',
            'emp_name' =>'required',
            'duration' =>'required',
            'sch_start_date' =>'required',                     
        ]);   
        $position = $schedule;                  
        $position->emp_city= $request->emp_city;
        $position->emp_name= $request->emp_name;
        $position->duration= $request->duration;
        $position->sch_start_date= $request->sch_start_date;
        $position->sch_end_date= $request->sch_end_date;
        $position->schedule_in_time= $request->schedule_in_time;
        $position->schedule_out_time= $request->schedule_out_time;
        $position->sch_start_day= $request->sch_start_day;
        $position->sch_end_day= $request->sch_end_day;
        $position->type= 'employee';
        $position->update();
        $lastId = $position->id;
        $date_from = $request->sch_start_date;
        $date_from1 = $request->sch_start_date;
        $date_from2 = $request->schedule_in_time;
        $date_from3 = $request->schedule_out_time;
        $date_from = strtotime($date_from);
        $date_to = $request->sch_end_date;  
        $date_to1 = $request->sch_end_date;  
        $date_to = strtotime($date_to);
       $na1 = $schedule->id;
//$empschedule = empschedule::where('emp_sch_id',$na1)->where('date_1',$date_to4)->get();
for($i=$date_from; $i<=$date_to; $i+=86400) 
{
//$insert2 = DB::insert("insert into schedule_emp_dates(`emp_sch_id`,`date_1`,`clockin`,`clockout`) values('".$lastId."','".$i."','".$date_from2."','".$date_from3."')");  
//DB::table('schedule_emp_dates')->where('emp_sch_id', $lastId)->where('date_1','!=', $i)->delete();
}
$schedule_date = $request->schedule_date;  
$schedule_clockin = $request->schedule_in_time;  
$schedule_id = $request->schedule_id;  
$schedule_clockout = $request->schedule_out_time;  
$j= 0;
foreach($schedule_date as $post)
{
$schedule_date1 =$schedule_date[$j]; 
$schedule_date1 = strtotime($schedule_date1);
$day = date("D", $schedule_date1);
$schedule_clockin1 = $schedule_clockin;  
$schedule_id1 = $schedule_id[$j];  
$schedule_clockout1 = $schedule_clockout; 
$j++;
if($day=='Sun'){
 $returnValue = DB::table('schedule_emp_dates')->where('id', '=', $schedule_id1)
->update([ 'date_1' => $schedule_date1,
           'clockin' =>'',
           'clockout' =>'',
    ]);   
}
else if($day=='Sat')
{
   $returnValue = DB::table('schedule_emp_dates')->where('id', '=', $schedule_id1)->update([ 'date_1' => $schedule_date1,
           'clockin' =>'',
           'clockout' =>'',
    ]);  
}
else
{
$returnValue = DB::table('schedule_emp_dates')->where('id', '=', $schedule_id1)
->update([ 'date_1' => $schedule_date1,
           'clockin' => $request->schedule_in_time,
           'clockout' => $request->schedule_out_time,
    ]);
}
    //return $schedule_date1;

}
return redirect('fscemployee/schedules')->with('success','Success fully Update Schedule');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
     
    public function destroy(Schedule $schedule)
    {
            $schedule->delete();
            return redirect(route('schedules.index'))->with('success','Success Fully Delete Record');
    }
    
    public function Clockin(Request $request)
    { 
       $student = empschedule::find($request->input('id'));
        if($student->delete())
        {
            echo 'Data Deleted';
        }
    }
}