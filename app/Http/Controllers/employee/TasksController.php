<?php

namespace App\Http\Controllers\employee;
use App\Model\Task;
use App\employees\Fscemployee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Employee;
use App\Model\Logo;
use App\Front\Commonregister;
use DB;
use Hash;
use Auth;
class TasksController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:employee');
    }

    public function index()
    {
      //  exit('222');
      $logo = Logo::where('id','=',1)->first();
       $user_id  = Auth::user()->user_id;
      $employee = Employee::where('id','=',$user_id)->first();  
      $task = DB::table('tasks')->where('employeeid','=',$user_id)->orderBy('id', 'desc')->get();
      $emp = DB::table('employees')->get();
      $admin = DB::table('admins')->first();
      //$emp = Fscemployee::All();
      $emp1 = Employee::get();
        $clients =DB::table('commonregisters')->get();
    
      
      return view('fscemployee/tasks/task', compact(['clients','employee','task','emp','emp1','logo','admin']));
    }
    
     public function search()
    {
        
         $logo = Logo::where('id','=',1)->first();
       $user_id  = Auth::user()->user_id;
      $employee = Employee::where('id','=',$user_id)->first();  
     
      $emp = DB::table('employees')->get();
      $admin = DB::table('admins')->first();
      //$emp = Fscemployee::All();
      $emp1 = Employee::get();
        $clients =DB::table('commonregisters')->get();
    
         $emp = DB::table('employees')->get();
       $clients =DB::table('commonregisters')->get();
      $emp1 = Employee::get();
      $logo = Logo::where('id','=',1)->first();
      $search1=$_POST['choice'];
        if(isset($_POST['sub']))
        {
            if(isset($_POST['choice']) &&  $_POST['choice'] !='')
            {
             $task = DB::table('tasks')->where('employeeid','=',$user_id)->where('status','=',$search1)->orderBy('id', 'desc')->get();
            }
            else
            {
              $task = DB::table('tasks')->where('employeeid','=',$user_id)->orderBy('id', 'desc')->get();
        }
        }
      
     
    //   echo "<pre>";
    //     print_r($emp);die;
      return view('fscemployee/tasks/task', compact(['clients','employee','task','emp','emp1','logo','admin']));
    }
    
    
    
    
    // public function index()
    // {
    //     //$emp1 = Employee::get();
    //     //$task = Task::All();
    //     $task = DB::table('Task as t1')
    //         ->select('t1.id,t1.title,t1.content,t1.admin_id,t1.employeeid,t1.priority,t1.duedate,t1.status,t2.firstName,t2.middleName')
    //         ->join('Employee as t2', 't2.id', '=', 't1.employeeid')
    //         ->get();
    //     $emp = Fscemployee::All();
    //     $logo = Logo::where('id','=',1)->first();
    //     return view('fscemployee/tsks/task', compact(['task','emp','logo']));
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $emp = Fscemployee::All();
        $emp1 = Employee::where('check','=',1)->where('status','=',1)->orderBy('firstName', 'asc')->get();
        // print_r($emp1);die;
        $customer = Commonregister::where('status','=','Active')->get();
        $logo = Logo::where('id','=',1)->first();
      $user_id  = Auth::user()->user_id;
      $employee = Employee::where('id',$user_id)->first();
        return view('fscemployee/tasks/create',compact(['employee','emp','emp1','customer']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $this->validate($request,[
            'title' =>'required',
            'description' =>'required',
            'employee' =>'required',
        ]);
        $position = new Task;
        $position->title= $request->title;
        $position->content= $request->description;
        $position->admin_id= $request->admin_id;
        $position->employeeid= $request->employee;
        $position->whone= $request->whone;
        $position->client= $request->client;
        $position->othername= $request->othername;
        $position->priority= $request->priority;
        $position->duedate= $request->duedate;
        $position->creattiondate= $request->date;
        $position->day= $request->day;
        $position->time = $request->time;
        $position->save();
        return redirect('fscemployee/tasks')->with('success','Task Added Successfully');
    }

    
    public function edit($id)
    {   
        
      //  $position = Task::find($id);
        //$position->checked=0;
        //$position->update();
          DB::table('tasks')->where('id', $id)->update(['checked' =>'1']);
    
        //  $task = Task::where('id',$id)->first(); 
          $emp = Fscemployee::All();
        //  $emp1 = Employee::where('check','=','1')->orderBy('firstName', 'asc')->get();
        //  $customer = Commonregister::All();
        
        $task = DB::table('tasks')->where('id','=',$id)->first();
        $emp1 = DB::table('employees')->where('check','=',1)->where('status','=',1)->orderBy('firstName', 'asc')->get();
         $customer = Commonregister::where('status','=','Active')->get();
        
        $logo = Logo::where('id','=',1)->first();
        $user_id  = Auth::user()->user_id;
        $employee = Employee::where('id',$user_id)->first();
        return View('fscemployee.tasks.edit',compact(['employee','logo','task','emp','emp1','customer']));
    }
    
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\State  $state
     * @return \Illuminate\Http\Response
     */
      public function timer($id)
    { 
        echo '111111111111';exit;
        return View('fscemployee.tasks.timer',compact([]));
    }
    public function update(Request $request, $id)
    {
       $this->validate($request,[
            'title' =>'required',
            'description' =>'required',
            'employee' =>'required',
        ]);
        $position = Task::find($id);
        $position->title= $request->title;
        $position->content= $request->description;
        $position->admin_id= $request->admin_id;
        $position->employeeid= $request->employee;
        $position->whone= $request->whone;
        $position->client= $request->client;
        $position->othername= $request->othername;
        $position->priority= $request->priority;
        $position->duedate= $request->duedate;
        $position->creattiondate= $request->date;
        $position->day= $request->day;
        $position->time = $request->time;
        $position->status= $request->status;
        $position->update();
        return redirect('fscemployee/tasks')->with('success','Task Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\State  $state
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        Task::where('id',$id)->delete();
        return redirect('fscemployee/tasks')->with('success','Task Deleted Successfully');
    }
}
