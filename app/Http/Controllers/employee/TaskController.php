<?php
namespace App\Http\Controllers\employee;
use App\Model\Task;
use App\employees\Fscemployee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Employee;
use App\Model\Logo;
use App\Front\Commonregister;
use DB;
use Hash;
use Auth;
class TaskController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:employee');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $task = Task::All();
      $emp = Fscemployee::All();
         $clients =DB::table('commonregisters')->get();
    
      $emp1 = Employee::get();
      $logo = Logo::where('id','=',1)->first();
      $user_id  = Auth::user()->user_id;
      $employee = Employee::where('id',$user_id)->first();
      return view('fscemployee/task/task', compact(['clients','employee','task','emp','emp1','logo']));
    }
    
    
    // public function index()
    // {
    //     //$emp1 = Employee::get();
    //     //$task = Task::All();
    //     $task = DB::table('Task as t1')
    //         ->select('t1.id,t1.title,t1.content,t1.admin_id,t1.employeeid,t1.priority,t1.duedate,t1.status,t2.firstName,t2.middleName')
    //         ->join('Employee as t2', 't2.id', '=', 't1.employeeid')
    //         ->get();
    //     $emp = Fscemployee::All();
    //     $logo = Logo::where('id','=',1)->first();
    //     return view('fscemployee/task/task', compact(['task','emp','logo']));
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $emp = Fscemployee::All();
        $emp1 = Employee::where('check','=',1)->where('status','=',1)->orderBy('firstName', 'asc')->get();
        // print_r($emp1);die;
        $customer = Commonregister::where('status','=','Active')->get();
        
        return view('fscemployee/task/create',compact(['emp','emp1','customer']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $this->validate($request,[
            'title' =>'required',
            'description' =>'required',
            'employee' =>'required',
        ]);
        $position = new Task;
        $position->title= $request->title;
        $position->content= $request->description;
        $position->admin_id= $request->admin_id;
        $position->employeeid= $request->employee;
        $position->priority= $request->priority;
        $position->duedate= $request->duedate;
        $position->creattiondate= $request->date;
        $position->day= $request->day;
        $position->time = $request->time;
        $position->save();
        return redirect('fscemployee/task')->with('success','Task Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\State  $state
     * @return \Illuminate\Http\Response
     */
    public function show(State $state)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\State  $state
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
         $task = Task::where('id',$id)->first(); 
         $emp = Fscemployee::All();
         $emp1 = Employee::where('check','=','1')->orderBy('firstName', 'asc')->get();
         $customer = Commonregister::All();
          $logo = Logo::where('id','=',1)->first();
      $user_id  = Auth::user()->user_id;
      $employee = Employee::where('id',$user_id)->first();
        return View('fscemployee.task.edit',compact(['logo','employee','task','emp','emp1','customer']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\State  $state
     * @return \Illuminate\Http\Response
     */
    public function updated(Request $request)
    {
      //  print_r($_REQUEST);die;
       $this->validate($request,[
            'title' =>'required',
            'description' =>'required',
            'employee' =>'required',
        ]);
        $position = Task::find($id);
        $position->title= $request->title;
        $position->content= $request->description;
        $position->admin_id= $request->admin_id;
        $position->employeeid= $request->employee;
        $position->priority= $request->priority;
        $position->duedate= $request->duedate;
        $position->creattiondate= $request->date;
        $position->day= $request->day;
        $position->time = $request->time;
        $position->update();
        return redirect('fscemployee/task')->with('success','Task Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\State  $state
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        echo $id;
        die;
        Task::where('id',$id)->delete();
        return redirect(route('task.index'));
    }
}
