<?php
namespace App\Http\Controllers\employee;
use App\Model\Appointment;
use App\employees\Fscemployee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
class EmpAppointmentController extends Controller
{
public function __construct()
    {
    $this->middleware('auth:employee');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    
    
    
        $employee = DB::table('employees')
            ->select('employees.*', 'teams.team as teamname')
            ->leftjoin('teams', 'teams.id', '=', 'employees.team')
            ->where('employees.type','!=','clientemployee')
            ->where('employees.check','=',1)
            ->where('employees.type','!=','Vendor')
            ->orderBy('employees.firstName', 'asc')
            ->get();
            
        $appemp = DB::table('employees')->select('employee_id')->where('id',Auth::user()->user_id)->first();
        echo "output <br/> ";
        print_r($appemp);
        exit();
        
        $appoinment_with = $appemp->employee_id;
        $emp1 = Employee::where('check','=',1)->where('type','=','employee')->orderBy('firstName', 'asc')->get();
        $appoinment = Appointment::All();
        
        $appodata = array();
        
        foreach($appoinment as $appo)
        {
            
            //echo $this->getsingleTeamName($appo->appo_with) .'-'. $appo->appo_with."<br/>";
            
            $enddates='';
            if($appo->enddate == '0000-00-00'){
                $enddates = date('m/d/Y',strtotime($appo->startdate));
            }else{
                $enddates = date('m/d/Y',strtotime($appo->enddate));
            }
            if(!empty($appo->participant)){
                $parti = $this->getTeamName($appo->participant);
            }else{
                $parti='';
            }
            $appo_with = $this->getsingleTeamName($appo->appo_with);
            $appodata[] = array(
                'id'=>$appo->id,
                'regarding'=>$appo->regard_short,
                'startdate'=>date('m/d/Y',strtotime($appo->startdate)),
                'enddate'=>$enddates,
                'starttime'=>$appo->starttime,
                'endtime'=>$appo->endtime,
                'clientId'=>$appo->client_id,
                'priority'=>$appo->priority,
                'teamRep'=>$parti,
                'type'=>$appo->work_type_id,
                'work_type'=>$appo->work_type,
                'appo_with'=>$appo_with
            );
        }
        // echo '<pre>';
        // print_r($appodata);
        //exit();
        $regards = Regards::All();
        $work_type = Appointment_worktype::All();
        return view('fscemployee/appointment/created',compact(['employee','appoinment','appodata','regards','work_type','appoinment_with']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\State  $state
     * @return \Illuminate\Http\Response
     */
    public function show(State $state)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\State  $state
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {  
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\State  $state
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      
    }


public function getcounty(Request $request)
    {
      
    }
public function getcountycode(Request $request)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\State  $state
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }
}
