<?php
namespace App\Http\Controllers\employee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Accountcode;
use DB;use Auth;
use App\User;
use App\Model\Employee;
class EmpaccountcodeController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth:employee');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {  
         $user_id  = Auth::user()->user_id;
                $employee = Employee::where('id',$user_id)->first();
       $accountcode = Accountcode::orderBy('accountcode', 'asc')->get(); 
       return view('fscemployee.empaccountcode.empaccountcode',compact(['accountcode','employee']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $accountcode = Accountcode::orderBy('accountcode', 'asc')->get(); 
     return view('fac-Bhavesh-0554.accountcode.create',compact('accountcode'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'accountcode'=>'required',
            'account_name'=>'required',            
            'account_belongs_to'=>'required',            
        ]);
       
        
        $business = new Accountcode;
        $business->accountcode = $request->accountcode;
        $business->account_name = $request->account_name;
        $business->account_belongs_to = $request->account_belongs_to;
        $business->save();
    //    return redirect(route('accountcode.index'));
        return redirect(route('accountcode.index'))->with('success','Account Code Seccessfully Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    $accountcode = Accountcode::where('id', $id)->first();
    return View::make('fac-Bhavesh-0554.accountcode.accountcode', compact('accountcode'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {     
        $user_id  = Auth::user()->user_id;
                $employee = Employee::where('id',$user_id)->first();
        $accountcode1 = Accountcode::orderBy('accountcode', 'asc')->get(); 
        $accountcode = Accountcode::where('id',$id)->first();    
        return View('fscemployee.empaccountcode.edit',compact(['accountcode','accountcode1','employee']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
           $this->validate($request,[
            'accountcode'=>'required',
            'account_name'=>'required',            
            'account_belongs_to'=>'required',            
        ]);
        
        $business =Accountcode::find($id);
        $business->accountcode = $request->accountcode;
        $business->account_name = $request->account_name;
        $business->account_belongs_to = $request->account_belongs_to;
        $business->update();
        return redirect(route('accountcode.index'))->with('success','Account Code Seccessfully Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Accountcode::where('id',$id)->delete();
        return redirect(route('accountcode.index'))->with('success','Account Code Seccessfully Deleted');
    }
}
