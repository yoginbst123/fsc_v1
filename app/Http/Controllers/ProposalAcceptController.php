<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin;
use Auth;
use View;
use Session;
use Validator;
use Redirect;
use DB;

class ProposalAcceptController extends Controller
{
    
    public function index()
    {
        
    }
    
     public function accept_proposal(Request $request)
    {
        $id = $request->id;
        $getProposal = DB::table("proposal_client_detail")->Where('ency_id',$id)->Where('status',0)->first();
        $counts = $getProposal->id;
        
        $query = DB::table('proposal_client_detail')
                ->where('id', $counts) 
                ->update(array('status' => 1)); 
        
        echo $query;
        exit();
        
    }
    
    public function getproposalDetailsForAccept($id)
    {
        $getProposal = DB::table("proposal_client_detail")->Where('ency_id',$id)->Where('status',0)->first();
        return view('prospect/',compact(['id','json']));
    }
}