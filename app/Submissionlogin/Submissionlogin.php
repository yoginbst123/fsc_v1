<?php
namespace App\Submissionlogin;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Submissionlogin extends Authenticatable
{
use Notifiable;
protected $guard = 'submission';
 /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
   

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password','fax','fname','lname','	city','state','zip','job_title','mobile','address','address1','question1','question2','question3','answer1','answer2','answer3','company_name','contact_person_name','company_email','username','dba_name','user_id','telephone','website','reset_day','remaining_day','login_day','type'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
}

