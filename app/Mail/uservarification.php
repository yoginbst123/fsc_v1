<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Front\Commonregister;

class uservarification extends Mailable
{
    use Queueable, SerializesModels;
    public $businessbrand;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($businessbrand)
    {
        $this->first_name = $businessbrand['first_name'];
        $this->middle_name = $businessbrand['middle_name'];
        $this->email = $businessbrand['email'];
        $this->last_name = $businessbrand['last_name'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('welcome')->with([
            'first_name' => $this->first_name,
            'middle_name' => $this->middle_name,
            'last_name' => $this->last_name,
            'email' => $this->email,
        ]);;
    }
}
