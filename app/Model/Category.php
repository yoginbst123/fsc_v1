<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Eloquent;
class Category extends Model
{
   protected $fillable =['bussiness_name','business_cat_name','business_cat_image','link'];     
}
