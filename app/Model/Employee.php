<?php
namespace App\Model;
use Illuminate\Database\Eloquent\Model;
class Employee extends Model
{
    protected $fillable = ['employee_id','firstName','middleName','lastName','address1','address2','city','stateId','zip','countryId','telephoneNo1','telephoneNo1Type','telephoneNo2','telephoneNo2Type','email','photo','ext1','ext2','hiremonth','hireday','hireyear','termimonth','agreement','termiyear','tnote','rehiremonth','rehireday','rehireyear','branch_city','branch_name','position','note','first_rev_day','reviewmonth','reviewday','reviewyear','hiring_comments','pay_method','pay_scale','fields','pay_frequency','gender','marital','month','type_agreement','resume','pf1','pfid1','pf2','pfid2','epname','relation','eaddress1','ecity','estate','ezipcode','etelephone1','eteletype1','eext1','etelephone2','eteletype2','eext2','comments1','uname','password','question1','answer1','question2','answer2','question3','answer3','work_responsibility','employee_rules','reset','status','created_at','updated_at','check','firstName_1','middleName_1','lastName_1','address11','efax','eemail','effective_date'];
}
