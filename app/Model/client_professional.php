<?php
namespace App\Model;
use Illuminate\Database\Eloquent\Model;
class client_professional extends Model
{
protected $fillable=['profession','profession_state','profession_epr_date','profession_license','profession_note','client_id'];
}
