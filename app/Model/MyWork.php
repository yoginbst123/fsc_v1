<?php
    namespace App\Model;
    
    use Illuminate\Database\Eloquent\Model;
    use Eloquent;
    class MyWork extends Eloquent
    {
        public $table = "mywork";
        protected $fillable = ['priority','type_of_work'];
    }