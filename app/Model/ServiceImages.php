<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ServiceImages extends Model
{
   protected $fillable = ['service_id','serviceimage'];
}
