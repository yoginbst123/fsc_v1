<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Submission extends Model
{
protected $fillable = ['submission_name','submission_image'];
}