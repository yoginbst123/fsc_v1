<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $fillable =['authority_name','short_name','type_of_tax','telephone','address','city','state','zip','website
','website_link_name','website_link'];
}
