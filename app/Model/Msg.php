<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Msg extends Model
{
    protected $fillable =['admin_id','title','content','employeeid'];
}
