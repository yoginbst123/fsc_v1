<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Price;
class Currency extends Model
{

    protected  $fillable =['currency'];
    
public function price()
{
    return $this->belongsToMany(Price::class);
}

}
