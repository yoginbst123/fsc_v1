<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class clientprimaryquestion extends Model
{
protected $fillable = ['type_of_entity','type_of_entity_answer','client_id','type'];
}
