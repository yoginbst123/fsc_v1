<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Clientnupload extends Model
{
   protected $fillable =['upload_name','upload'];
}
