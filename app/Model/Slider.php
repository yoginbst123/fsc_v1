<?php
namespace App\Model;
use Illuminate\Database\Eloquent\Model;
class Slider extends Model
{
protected $fillable=['slider_name','slider_image'];
}
