<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Currency;
class Price extends Model
{
    protected $fillable =['currency','typeofservice','serviceperiod','comboprice','regularprice','serviceincludes'];
    
public function currencies()
{
    return $this->belongsToMany(Currency::class, 'currency_price', 'price_id', 'currency_id')->withPivot('id');
}
}
