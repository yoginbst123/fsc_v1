<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
   protected $fillable = ['name','content','post_date','post_id','linkimage','type'];
}
