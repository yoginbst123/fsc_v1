<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Eloquent;
class Answer extends Eloquent
{
    protected $fillable = ['answer','q_id'];
}
