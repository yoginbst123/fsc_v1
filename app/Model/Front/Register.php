<?php

namespace App\Model\Front;

use Illuminate\Database\Eloquent\Model;

class Register extends Model
{
 
    public function Register()
    {
        return $this->belongsToMany('App\Model\Business','businesses');
    }
    public function getRouteName()
    {
        return 'Personal';
    }

}
