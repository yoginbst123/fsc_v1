<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Linkcategory extends Model
{
   protected $fillable = ['category','picture','type','status'];
}
