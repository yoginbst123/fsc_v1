<?php
namespace App\Model;
use Illuminate\Database\Eloquent\Model;
class admin_professional extends Model
{
protected $fillable=['profession','profession_state','profession_epr_date','profession_license','profession_note','admin_id'];
}
