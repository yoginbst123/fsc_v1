<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Eloquent;
class proposal_client_detail_services extends Eloquent
{
    public $table="proposal_client_detail_services";
    protected $fillable = ['proposal_client_id','service_id','sub_service_id',];
}