<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Eloquent;
class Ethnic extends Eloquent
{
    protected $fillable = ['ethnic_name'];
    
}
