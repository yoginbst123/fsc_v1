<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CsService extends Model
{

    protected $fillable = ['service','billing'];
}
