<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FilingFrequency extends Model
{
    protected $table ='filingfrequencies';
    
    
    public function federal()
    {
        return $this->belongsTo('App\Model\taxfederal');
    }
}
