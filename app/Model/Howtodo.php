<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Howtodo extends Model
{
    protected $fillable =['title'];
}
