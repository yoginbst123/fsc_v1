<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Eloquent;
class Marque extends Eloquent
{
    protected $fillable = ['marque'];
}
