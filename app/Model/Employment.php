<?php
namespace App\Model;
use Illuminate\Database\Eloquent\Model;
class Employment extends Model
{
    protected $fillable = ['position_name','country','state','city','description','date','type','link','status'];
}
