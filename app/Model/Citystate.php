<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Citystate extends Model
{
    protected $fillable =['zipcode','city','state','country'];
}
