<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Control extends Model
{
    protected $fillable =['statename','controlname','admin_id'];
}
