<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Eloquent;
class Question extends Eloquent
{
    protected $fillable = ['question'];
}
