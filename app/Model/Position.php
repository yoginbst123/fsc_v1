<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Eloquent;
class Position extends Eloquent
{
    protected $fillable = ['position'];
}
