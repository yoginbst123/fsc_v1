<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Eloquent;
class Proposal_client_details extends Eloquent
{
    public $table="proposal_client_detail";
    protected $fillable = ['type_of_client','client_name','telephone','email','type_of_service','priority','fsc_fee'];
}