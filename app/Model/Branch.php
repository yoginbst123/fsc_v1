<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Eloquent;
class Branch extends Eloquent
{
    protected $fillable = ['positionid','country','city','branchname'];
}
