<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Categorybusiness extends Model
{
    protected $fillable =['bussiness_name','business_cat_name','business_cat_image','link'];
}
