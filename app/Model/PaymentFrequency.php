<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PaymentFrequency extends Model
{
    protected $table ='paymentfrequency';
    
    
    public function federal()
    {
        return $this->belongsTo('App\Model\taxfederal');
    }
}
