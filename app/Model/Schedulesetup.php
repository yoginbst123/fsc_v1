<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Schedulesetup extends Model
{
   protected $fillable = ['sch_start_date','duration','sch_start_day','sch_end_date','sch_end_day','sch_pay_date','sch_pay_day','status'];
}
