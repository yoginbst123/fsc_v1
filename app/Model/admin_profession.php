<?php
namespace App\Model;
use Illuminate\Database\Eloquent\Model;
class admin_profession extends Model
{
    public $table="admin_profession";
    protected $fillable=['name','created_at'];
}
