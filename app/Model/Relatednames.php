<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Eloquent;
class Relatednames extends Eloquent
{
    protected $fillable = ['relatednames'];
}
