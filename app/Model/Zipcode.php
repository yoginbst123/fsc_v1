<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Eloquent;
class Zipcode extends Eloquent
{
    protected $fillable = ['zip'];
   
}
