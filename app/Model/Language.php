<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Eloquent;
class Language extends Eloquent
{
    protected $fillable = ['language_name'];
    
}
