<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Price;
class License extends Model
{

    protected  $fillable =['licensename'];
    
public function price()
{
    return $this->belongsToMany(Price::class);
}

}
