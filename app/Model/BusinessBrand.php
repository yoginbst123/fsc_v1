<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BusinessBrand extends Model
{
    protected $fillable = ['business_id','business_cat_id','business_brand_name','business-brand-image','status','link'];
}
