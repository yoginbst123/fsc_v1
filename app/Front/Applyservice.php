<?php

namespace App\Front;

use Illuminate\Database\Eloquent\Model;

class Applyservice extends Model
{
    protected $fillable = ['id','firstName','middleName','lastName','address','city','stateId','zip','countryId','telephoneNo1','telephoneNo1Type','telephoneNo2','telephoneNo2Type','fax','email','serviceTypeId','note','status','serviceid'];
}
