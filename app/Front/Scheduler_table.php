<?php

namespace App\Front;

use Illuminate\Database\Eloquent\Model;

class Scheduler_table extends Model
{
    protected $fillable = ['scheduler_fname','scheduler_mname','scheduler_lname','scheduler_ssn','scheduler_position'];
}
