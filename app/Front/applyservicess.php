<?php

namespace App\Front;

use Illuminate\Database\Eloquent\Model;

class applyservicess extends Model
{
    protected $fillable = ['id','typeofservice','newbusiness','domestic_business','foreign_business','choice','reservation','s_st_choice','t_st_choice','stateId','address1','address2','city','state','zip','county','address3','address4','business_city','business_state','business_zip','county2','agent_fname','agent_mname','agent_lname','typeofbusiness','shares','common','scheduler_fname','scheduler_mname','scheduler_lname','scheduler_ssn','scheduler_position','typeofcorp','date'];
}
