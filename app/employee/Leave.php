<?php

namespace App\employee;

use Illuminate\Database\Eloquent\Model;

class Leave extends Model
{
    protected $fillable = ['employee_id','start_date','total_days','end_date','leave_reason','status','creation_date'];
}
