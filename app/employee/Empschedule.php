<?php

namespace App\employee;

use Illuminate\Database\Eloquent\Model;

class Empschedule extends Model
{
    protected $fillable = ['employee_id','emp_in_date','emp_in','emp_out','launch_in','launch_out','note','work_note','notes','edit_notes','adminnote','ip_address','flagip','flag_edit','status'];
}
