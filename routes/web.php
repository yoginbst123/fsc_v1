<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!getJsonOfWork
|
*/
Route::get('/request-log', function()
{
    return view('/request-log');
});
Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    return "Cache is cleared";
});

// Route::get('/makeController', function() {
//     Artisan::call('make:controller Test --resources');
//     return "Controller Created";
// });

Auth::routes();
 
Route::get('/request-login', 'submission\Auth\LoginController@showLoginForm')->name('requestlogin');

Route::post('submission/logout', 'submission\Auth\SubmissionLoginController@logout')->name('submission.logout');

Route::post('/request-login', 'submission\Auth\LoginController@requestlogin');
Route::resource('fscemployee/empclientreport','employee\EmpclientreportController');
Route::resource('fscemployee/empworkreport','employee\EmpworkreportController');

Route::resource('fscemployee/eereport','employee\EmpreportController');
Route::resource('fscemployee/eescheduleworkreport','employee\EmpschedulereportController');

Route::resource('fscemployee/employees','employee\EmployeesController');
Route::resource('fscemployee/workstatus','employee\WorkstatusController');

Route::resource('client/home', 'HomeController');
Route::resource('fscemployee/empsstates','employee\TaxStateEmpsController');
Route::resource('fscemployee/empstates','employee\TaxStateEmpController');
Route::resource('fscemployee/emptaxation','employee\TaxationEmpController');

Route::resource('fscemployee/appointment','employee\EmpAppointmentController');
Route::resource('fscemployee/eemail','employee\EmployeeEmailController');

Route::post('fscemployee/clients/addweges/{id}',['as'=>'clients.addweges','uses'=>'employee\EmploController@addweges']);
Route::post('fscemployee/clients/addwegesbanks/{id}',['as'=>'clients.addwegesbanks','uses'=>'employee\EmploController@addwegesbanks']);
Route::post('fscemployee/addnotesrelated1',['as'=>'addnotesrelated1','uses'=>'employee\EmploController@addnotesrelated1']);
Route::post('fscemployee/notesrelateds1',['as'=>'notesrelated1.notesrelateds1','uses'=>'employee\EmploController@notesrelateds1']);
Route::post('/utilitiess','employee\SchedulesController@utilitiess');
Route::get('/removeutilitiess','employee\SchedulesController@removeutilitiess');
Route::post('/otherbusinesss','employee\SchedulesController@otherbusinesss');  
Route::get('/removebusinesss','employee\SchedulesController@removebusinesss');
Route::post('/insurances','employee\SchedulesController@insurances');  
Route::get('/removeinsurances','employee\SchedulesController@removeinsurances');

Route::post('fscemployee/addcoversationsheet1',['as'=>'addcoversationsheet1','uses'=>'employee\EmploController@addcoversationsheet1']);
Route::post('fscemployee/tasks/search', 'employee\TasksController@search')->name('tasks.search');
Route::post('fac-Bhavesh-0554/purpose',['as'=>'purpose.purposes','uses'=>'Admin\MessageController@purposes']);
Route::get('client/getRequest2','HomeController@getcategory2');
Route::post('client/checkpassword','HomeController@getpassword');

Route::post('fac-Bhavesh-0554/addcoversationsheet',['as'=>'addcoversationsheet','uses'=>'AdminController@addcoversationsheet']);
Route::post('fac-Bhavesh-0554/addnotesrelated',['as'=>'addnotesrelated','uses'=>'AdminController@addnotesrelated']);
Route::post('fac-Bhavesh-0554/workrecord',['as'=>'workrecord.creates','uses'=>'Admin\WorkrecordController@creates']);
Route::post('fac-Bhavesh-0554/workrecord',['as'=>'workrecord.store','uses'=>'Admin\WorkrecordController@store']);

Route::get('client/getcat1','HomeController@getcategory111');
Route::get('client/getusers/{filename}','HomeController@getusers');

Route::get('applycounty','Front\ApplyservicessController@applycounty1');
Route::get('/getstateList','Front\ApplyEmploymentContoller@getStateList');
Route::resource('schedule1','Admin\ScheduleupController');

Route::get('propspectAccept/accept_proposal/','Admin\ProposalAcceptController@accept_proposal');
Route::resource('propspectAccept','Admin\ProposalAcceptController');

Route::get('/fetch-company', 'Admin\ScheduleController@schedules');
Route::resource('submission/dashboard','Front\RequestSubmissionController');
Route::resource('client/clientvendor','User\VendorssController');
Route::get('/fscgetschedule','employee\SchedulesController@fscemployee1');
Route::get('/fscgetduration','employee\SchedulesController@fscgetduration1');
Route::get('/getadminlic','Admin\AdminworkstatusController@getadminlic');
Route::get('/getadminformation','Admin\AdminworkstatusController@getadminformation');
Route::get('/getadminformation2','Admin\AdminworkstatusController@getadminformation2');

Route::post('/income','employee\SchedulesController@incomeadd');
Route::post('/emptax','employee\SchedulesController@emptaxadd');
Route::get('/getDependent','employee\SchedulesController@getDependent');
Route::post('/depend','employee\SchedulesController@dependentadd');
Route::get('/deletedepend/{id}','employee\SchedulesController@destroydependent');
Route::get('/deletefile/{id}','employee\SchedulesController@destroyfilling');
Route::get('/deleteexp/{id}','employee\SchedulesController@deleteexp');
Route::post('/expense','employee\SchedulesController@expenseadd');

Route::post('/updatedependent','employee\SchedulesController@updatedependent');
Route::get('/otherWeges','employee\SchedulesController@otherWeges');
Route::get('/getWeges','employee\SchedulesController@getWeges');
Route::get('/getExpense','employee\SchedulesController@getExpense');
Route::post('/updateincomeweges','employee\SchedulesController@updateincomeweges');
Route::post('/updateotherweges','employee\SchedulesController@updateotherweges');
Route::post('/updateexpense','employee\SchedulesController@updateexpense');
Route::get('/getTaxations','employee\SchedulesController@getTaxations');
Route::post('/updateintaxation','employee\SchedulesController@updateintaxation');

Route::get('removenotenote/removenotesnote1', ['as' => 'removenotenote.removenotesnote1', 'uses' => 'employee\EmploController@removenotenotes']);
Route::get('getComplete/getComplete1', ['as' => 'getComplete.getComplete1', 'uses' => 'employee\EmploController@getComplete']);

Route::post('/login1', 'Clients\Auth\EmployeeClientLoginController@login1'); 
Route::get('/login1', 'Clients\Auth\EmployeeClientLoginController@showLoginForm1')->name('login1');

Route::prefix('clientemployee')->group(function()
{
Route::post('/logout', 'Clients\Auth\EmployeeClientLoginController@logout')->name('clientemployee.logout'); 

Route::resource('home', 'Clients\ClientsController');
Route::resource('', 'Clients\ClientsController');

Route::resource('clientprofile','Clients\ClientprofileController');
Route::resource('clientchangepassword','Clients\ClientchangepasswordController');
Route::post('clientchangepass','Clients\ClientchangepasswordController@getpassword12');
Route::post('/super', 'Clients\ClientsController@supervisor_suepr');
Route::post('/super1', 'Clients\ClientsController@supervisor_suepr1');

Route::post('/superstart2', 'Clients\ClientsController@supervisor_sueprstart2');
Route::post('/superend2', 'Clients\ClientsController@supervisor_sueprend2');

Route::match(['get', 'post'], '/profilephoto1/{id}', 'Clients\ClientprofileController@profilephoto');  
Route::match(['get', 'post'], '/additional/{id}', 'Clients\ClientprofileController@additional1');
Route::match(['get', 'post'], '/additional2/{id}', 'Clients\ClientprofileController@additional2');
Route::match(['get', 'post'], '/additional3/{id}', 'Clients\ClientprofileController@additional3');
Route::match(['get', 'post'], '/pfidd/{id}', 'Clients\ClientprofileController@pfidd');
Route::match(['get', 'post'], '/pfidd1/{id}', 'Clients\ClientprofileController@pfidd1');
});

Route::group(['namespace'=>'Front'],function()
{ 
Route::resource('/', 'FrontController');    
Route::get('register-user','RegisterController@index');
Route::get('getFormation','FormationController@getFormation');

Route::resource('user/client','UsercronController'); 
Route::get('business-registration/{busid}/{type}','CategoryController@index');
Route::get('brand-nonbrand/{busid}/{bustype}/{catid}','BrandNonbrandController@index');
Route::get('non-profit-organization-registration/{id}','NonprofitController@index');
Route::get('business-brand-category-list/{brandid}/{catid}/{busid}/{type}','BusinessbrandcategoryController@index');

Route::resource('comman-registration','CommonRegistrationController');
Route::post('/checkclient','CommonRegistrationController@checkclientAvailability');
Route::post('/checkclient11','CommonRegistrationController@checkclientAvailability11');
Route::post('/checkclientids','CommonRegistrationController@checkclientcheckclientids');
Route::post('/clientname1','CommonRegistrationController@clientname');
Route::get('/getfilename','CommonRegistrationController@getfilename');
Route::get('comman-registration/create/{catid}/{bustype}/{busid}/{brandid}/{brandcatid}','CommonRegistrationController@create');
Route::get('comman-registration/create/{catid}/{bustype}/{busid}/{brandid}','CommonRegistrationController@create');
Route::get('comman-registration/create/{busid}/{bustype}/{catid}','CommonRegistrationController@create');
Route::get('comman-registration/create/{busid}/{bustype}','CommonRegistrationController@create');

Route::get('service-industry-registration/{busid}/{type}','ServiceindustryregistrationController@index');
Route::get('professions-registration/{busid}/{type}','ProfessionsController@index');
Route::get('investor-registration/{busid}/{type}','investorController@index');
Route::get('personal-registration/{busid}/{type}','PersonalController@index');
Route::get('contacts/store2','ContactShowController@store2'); 
Route::get('DailyCronResponsibility','ContactShowController@DailyCronResponsibility'); 

Route::resource('employment','EmploymentContoller');
Route::resource('links','FrontlinkController');
Route::get('linktype/{type}','FrontlinkController@type');
Route::get('apply-employment/{id}/{name}','ApplyEmploymentContoller@index');
Route::resource('apply-employment','ApplyEmploymentContoller');
Route::post('/check_unique','ApplyEmploymentContoller@checkUsernameAvailability');
Route::resource('forgotuser','UserForgotUsernameController');
Route::post('contacts','ContactShowController@store');  

Route::post('/submissions/store/','SubmissionsController@store');

Route::get('contacts/create/{id}/{name}','ContactShowController@create');  
Route::resource('contacts','ContactShowController');
Route::resource('submissions','SubmissionsController');

Route::resource('service','ServicesContoller');
Route::get('apply-service/create/{id}/{service_name}','ApplyserviceController@create');
Route::get('/getzip','ApplyserviceController@getzipcode');
Route::get('/getstate1','ApplyserviceController@getstate');
Route::post('apply-service/create/{id}',['as'=>'apply-service.store','uses'=>'ApplyserviceController@store']);
Route::post('emp_id','ApplyserviceController@getempid');
Route::post('emp_id1','ApplyserviceController@getempid1');
Route::post('check_email','ApplyserviceController@checkemailAvailability');
Route::resource('forms','FormfrontController');  
Route::get('applyservices/create/{id}/{service_name}','ApplyservicessController@create');    
Route::post('applyservices',['as'=>'applyservices.store','uses'=>'ApplyservicessController@store']); 
Route::post('/akAvailability','ApplyservicessController@servAvailability'); 
Route::post('/getthecode','ApplyservicessController@getthecode'); 
Route::post('/service.getthecode',['as'=>'service.getthecode','uses'=>'ApplyservicessController@getthecode']);


});

Route::get('/getbranchs','employee\EmployeeprofileController@getBranches');
Route::match(['get', 'post'], '/profilephoto1/{id}', 'employee\ProController@profilephoto');  
Route::match(['get', 'post'], '/additional/{id}', 'employee\ProController@additional1');
Route::match(['get', 'post'], '/additional2/{id}', 'employee\ProController@additional2');
Route::match(['get', 'post'], '/additional3/{id}', 'employee\ProController@additional3');
Route::match(['get', 'post'], '/pfidd/{id}', 'employee\ProController@pfidd');
Route::match(['get', 'post'], '/pfidd1/{id}', 'employee\ProController@pfidd1');
Route::get('admind3/admindelete3/{id}', ['as' => 'admind3.admindelete3', 'uses' => 'employee\ClientsController@clientdelete21']);

Route::prefix('fscemployee')->group(function() {
Route::get('/login', 'employee\Auth\EmployeeLoginController@showLoginForm')->name('fscemployee.login');
Route::post('/logout', 'employee\Auth\EmployeeLoginController@logout')->name('fscemployee.logout');
Route::resource('forgotpassword','employee\Auth\FscemployeeForgotPasswordController');
Route::resource('forgotempusername','employee\Auth\EmployeeForgotUsernameController');
Route::post('/super', 'employee\EmploController@supervisor_suepr');
Route::post('/fetch2', 'employee\EmploController@fetch2');

Route::post('/super1', 'employee\EmploController@supervisor_suepr1');
Route::resource('/clients', 'employee\ClientsController');

Route::resource('/empvendor','employee\VendorsController');
Route::patch('/super/{id}', 'employee\EmploController@supervisor');
Route::get('', 'employee\EmploController@index')->name('fscemployee.dashboard');
Route::resource('/home', 'employee\EmploController');
Route::resource('empprofile','employee\EmployeeprofileController');
Route::get('review1/reviewdelete1/{id}', ['as' => 'review1.reviewdelete1', 'uses' => 'employee\ProController@reviewdelete11']);
Route::get('responsibility/{id}/res', ['as' => 'responsibility.res', 'uses' => 'employee\ProController@responsibility']);
Route::post('responsibility/respon/{id}', ['as' => 'responsibility.respon', 'uses' => 'employee\ProController@responupdate']);
Route::get('pay1/paydelete1/{id}', ['as' => 'pay1.paydelete1', 'uses' => 'employee\ProController@paydelete11']);
Route::resource('employeeprofile','employee\ProController');
Route::resource('report','employee\EmployeereportController');
Route::resource('leave','employee\LeaveController');
Route::resource('resposibilty','employee\ResposibiltyController');
Route::resource('technicalsupport','employee\TechnicalsupportController');
Route::post('/login', 'employee\Auth\LoginController@login'); 
});

Route::get('fscemployee/employeedetail', 'employee\EmployeereportController@employeedetail122');

Route::post('fscemployee/checkemppassword','employee\EmployeeprofileController@getemployeepassword');
Route::resource('fscemployee/newtask','employee\EmployeenewtaskController');
Route::get('fscemployee/getmessage','employee\EmployeemsgController@getmessages');
Route::get('fscemployee/getmessage1','employee\EmployeemsgController@getmessage12');
Route::get('/getrouting1','employee\ClientsController@getrouting1');
Route::get('/getAgentTelephone','employee\ClientsController@getAgentTelephone');
Route::post('fscemployee/workrecord/fetch1',['as'=>'workrecord.fetch1','uses'=>'employee\WorkrecordController@fetch1']);


Route::resource('fscemployee/sendmessage','employee\SendmsgController');

Route::resource('fscemployee/worknew','employee\WorknewController');
Route::resource('fscemployee/workregular','employee\WorkregularController');
Route::resource('fscemployee/mywork','employee\MyworkController');


Route::resource('fscemployee/workrecord','employee\WorkrecordController');

Route::resource('fscemployee/workstatustaxations','employee\WorkstatusTaxationsController');
Route::resource('workstatustaxations','employee\WorkstatusTaxationsController');

Route::get('fscemployee/workstatustaxations/{id}/edit',['as'=>'workstatustaxations.edit','uses'=>'employee\WorkstatusTaxationsController@edit']);
Route::post('workstatustaxations/update', 'employee\WorkstatusTaxationsController@update')->name('workstatustaxations.update');

Route::resource('fscemployee/getmsg','employee\EmployeemsgController');
Route::get('fscemployee/addressbook-fscemployee','employee\FscaddressbookController@index');
Route::get('fscemployee/addressbook-fscemployee/{id}/{type}/edit',['as'=>'addressbook-fscemployee.edit','uses'=>'employee\FscaddressbookController@edit']);
Route::get('fscemployee/allemployeesheet','employee\AllemployeesheetController@index');
Route::resource('cronjobs','Admin\CronjobsController');

Route::get('thankyou', function () {return view('thankyou');});
Route::get('thanks', function () {return view('thanks');});
Route::get('servicethanks', function () {return view('servicethanks');});
Route::get('tools', function () 
{
return view('tools');
});
Route::get('map', function () 
{
return view('map');
});
Route::get('contactinfo', function () 
{
return view('contactinfo');
});
Route::get('consuccess', function () 
{
return view('consuccess');
});
Route::get('employmentsuccess', function () 
{
return view('employmentsuccess');
});

//Route::resource('/state', 'Front\StateController');
Route::resource('/submissions', 'Front\SubmissionsController');
Route::get('login-user', function () {
return view('login-user');
});
Route::get('/fac-Bhavesh-0554/hold', function () {
return view('fac-Bhavesh-0554/hold');
});
Route::get('/fac-Bhavesh-0554/adminpassword', function () {
return view('fac-Bhavesh-0554/adminpassword');
});

Route::get('/client/clientpassword', function () {
return view('client/clientpassword');
});
Route::get('/fac-Bhavesh-0554/approve', function () {
return view('fac-Bhavesh-0554/approve');
});
Route::get('registrationthank', function () {
return view('registrationthank');
});
Route::get('tax-issue-reporting', function () {
return view('tax-issue-reporting');
});
Route::get('sale-tax-reporting', function () {
return view('sale-tax-reporting');
});
Route::get('payroll-reporting', function () {
return view('payroll-reporting');
});
Route::get('employee-time-reporting', function () {
return view('employee-time-reporting');
});
Route::get('what-we-do', function () {
return view('what-we-do');
});
Route::get('fac-Bhavesh-0554/company-register', function () {
return view('fac-Bhavesh-0554.company-register');
});
Route::get('fac-Bhavesh-0554/employe', function () {
return view('fac-Bhavesh-0554.employe');
});
Route::get('fac-Bhavesh-0554/excelmail', function () {
return view('fac-Bhavesh-0554.excelmail');
});

Route::get('client/clientdelete2/{id}', ['as' => 'client.clientdelete2', 'uses' => 'User\UserProfileController@clientdelete2_1']);
Route::get('/faderal1','User\UserProfileController@getfaderal1');
Route::get('/getimage12','User\UserProfileController@getimagesssss111');

Route::resource('client/profile','User\UserProfileController');

Route::resource('client/cli-employee','User\ClientEmployeeController');
Route::get('client/clientempnotedelete/{id}', ['as' => 'client.clientempnotedelete', 'uses' => 'User\ClientEmployeeController@clientempnotedelete']);

Route::resource('client/clientschedulesetup','User\ClientschedulesetupController');
Route::resource('client/clientschedule','User\ClientscheduleController');
Route::resource('clientemployee/employeereport','Clients\EmployeereportsController');


Route::resource('client/employeereport','User\EmployeereportsController');
Route::get('client/employeereport','User\EmployeereportsController@index');
Route::resource('client/employeeschedule','User\EmployeeschedulesController');


Route::get('/getschedule2','User\ClientscheduleController@employee2');


Route::get('/getclientempschedule','User\ClientscheduleController@clientemployee2');

Route::get('/getEmpcity2','User\ClientscheduleController@getEmpcity2');
Route::get('/getduration3','User\ClientscheduleController@getduration3');
Route::get('/getdurationempclient2','User\ClientscheduleController@getdurationempclient2');

Route::get('/getduration2','User\ClientscheduleController@getduration12');
Route::get('/fetch-company1', 'User\ClientscheduleController@schedules1');
Route::resource('client/workresponsibilty','User\WrkresponsibiltyController');
Route::get('/getBranch2','User\ClientscheduleController@getbranch12');
Route::get('co/delete1/{id}/{user_id}', ['as' => 'co.delete1', 'uses' => 'User\UserProfileController@delete1']);
Route::resource('fscemployee/empaccountcode','employee\EmpaccountcodeController');
Route::resource('fscemployee/schedules','employee\SchedulesController');
Route::resource('fscemployee/empchangepassword','employee\EmpchangepasswordController');
Route::resource('fscemployee/userchangepassword1','employee\FscemployeechangeController');
Route::resource('fscemployee/clientsupports','employee\Clienttechnicalsupport');
Route::post('fscemployee/checkpassword12','employee\FscemployeechangeController@getpassword12');
Route::resource('fscemployee/fschowtodo','employee\FSCHowtodoController');
Route::resource('fscemployee/howtodo','employee\HowtodoController');

Route::resource('fscemployee/eereport','employee\FsceereportController');

Route::resource('fscemployee/fscemployeeworkreport','employee\FscemployeeworkreportController');

Route::resource('fscemployee/submissionrequest','employee\SubmissionRequestController');
Route::resource('fscemployee/tasks','employee\TasksController');
Route::get('fscemployee/task/{id}/edit',['as'=>'task.edit','uses'=>'employee\TaskController@edit']);
Route::get('fscemployee/timer','employee\TasksController@timer');
Route::post('fscemployee/task/updated',['as'=>'task.updated','uses'=>'employee\TaskController@updated']);

Route::resource('client/userchangepassword','User\UserChangePasswordController');
Route::get('/getmessagesclient','User\ClientmsgController@getmessagesclient12');
Route::get('/getmessageclients','User\ClientmsgController@getmessageclients1');
Route::resource('client/clientmsg','User\ClientmsgController');
Route::resource('client/submissions','User\SubmissionsController');

Route::resource('client/clientsupport','User\ClientsupportController');
Route::resource('client/technicalsupports','User\TechnicalsupportsController');
Route::post('client/checkpassword1','User\UserChangePasswordController@getpassword1');

Route::prefix('fac-Bhavesh-0554')->group(function() {
    
//Route::post('/workrecord/fetch1/','WorkrecordController@fetch1');
Route::post('/worknew/fetch1/','WorknewController@fetch1');


Route::post('msg/search', 'Admin\MessageController@search')->name('msg.search');
Route::post('task/search', 'Admin\TaskController@search')->name('task.search');


Route::get('/submissionrequest/{id}/edit1','Admin\SubmissionRequestController@edit1');
Route::post('submissionrequest/update', 'Admin\SubmissionRequestController@update')->name('submissionrequest.update');

Route::get('/login', 'Admin\Auth\AdminLoginController@showLoginForm')->name('fac-Bhavesh-0554.login');
Route::post('/logout', 'Admin\Auth\AdminLoginController@logout')->name('fac-Bhavesh-0554.logout');
Route::get('/logout', 'Admin\Auth\AdminLoginController@logout')->name('fac-Bhavesh-0554.logout');

Route::get('/', 'AdminController@index')->name('fac-Bhavesh-0554.dashboard');
Route::resource('forgotusername','Admin\Auth\AdminForgotUsernameController');
Route::post('/password/email','Admin\Auth\AdminForgotPasswordController@sendResetLinkEmail')->name('fac-Bhavesh-0554.password.email');
Route::get('/password/reset','Admin\Auth\AdminForgotPasswordController@showLinkRequestForm')->name('fac-Bhavesh-0554.password.request');
Route::post('/password/reset','Admin\Auth\AdminResetPasswordController@reset');

Route::get('/password/reset/{token}','Admin\Auth\AdminResetPasswordController@showResetForm')->name('fac-Bhavesh-0554.password.reset');
Route::resource('adminpassword','Admin\Auth\AdminForgotPasswordController');

Route::resource('workstatus','Admin\WorkstatusController');
Route::get('complaint','Admin\WorkstatusController@complaint');
Route::get('reviews','Admin\WorkstatusController@reviews');

Route::resource('adminworkstatus','Admin\AdminworkstatusController');
Route::resource('adminworkstatus','Admin\AdminworkstatusController');
Route::resource('workstatustaxation','Admin\WorkstatusTaxationController');
Route::resource('worktodo','Admin\WorktodoController');
Route::resource('worktax','Admin\WorktodotaxController');

//Route::get('mywork/{id}/edit',['as'=>'mywork.edit','uses'=>'Admin\MyworkController@edit']);
Route::get('mywork/changeOrder/{id}/{orderid}','Admin\MyworkController@changeOrder');
Route::get('mywork/changeStatus/{id}/{orderid}','Admin\MyworkController@changeStatus');
Route::get('mywork/getJsonOfWork/{typeofwork}','Admin\MyworkController@getJsonOfWork');
Route::post('mywork/store','Admin\MyworkController@store')->name('mywork.store');
Route::resource('mywork','Admin\MyworkController');


Route::resource('workrecord','employee\WorkrecordController');
Route::resource('workstatus','employee\WorkstatusController');

Route::resource('worknew','Admin\WorknewController');
Route::get('workrecord', 'Admin\WorkrecordController@index');
Route::get('submissionform', 'Admin\SubmissionformController@index');

Route::get('workstatus', 'Admin\WorkstatusController@index');
Route::get('invoice', 'Admin\InvoiceController@index');

Route::get('fac-Bhavesh-0554/addressbook/{id}/{type}/edit',['as'=>'addressbook.edit','uses'=>'AddressbookController@edit']); 
Route::get('workstatustaxation/{id}/edit',['as'=>'workstatustaxation.edit','uses'=>'WorkstatusTaxationController@edit']);
Route::get('weges/{id}/edit',['as'=>'weges.edit','uses'=>'WegesController@edit']);

Route::post('workstatustaxation/update', 'Admin\WorkstatusTaxationController@update')->name('workstatustaxation.update');

Route::post('workrecord/fetch', 'Admin\WorkrecordController@fetch')->name('workrecord.fetch');
Route::post('workrecord/addincome/{id}', 'Admin\WorkrecordController@addincome')->name('workrecord.addincome');
Route::post('workrecord/updateincome', 'Admin\WorkrecordController@updateincome')->name('workrecord.updateincome');
Route::get('workrecord/destroyincomes/{id}', 'Admin\WorkrecordController@destroyincomes')->name('workrecord.destroyincomes');


Route::post('admin/fetch', 'AdminController@fetch')->name('admin.fetch');
Route::get('admin/fetch_period','AdminController@fetch_period');
Route::get('admin/fetch_period_price','AdminController@fetch_period_price');
Route::get('admin/fetch_taxtion_price','AdminController@fetch_taxtion_price');
Route::get('admin/addData','AdminController@addData');


Route::post('admin/praposal_fetch', 'AdminController@praposal_fetch')->name('admin.praposal_fetch');
Route::post('admin/getDetailForPraposal','AdminController@getDetailForPraposal')->name('admin.getDetailPraposal');
Route::post('admin/storeProposalClient','AdminController@storeProposalClient')->name('admin.storeProposalClient');
Route::post('admin/fetch1', 'AdminController@fetch1')->name('admin.fetch1');

Route::post('employee/fetch11', 'employee\EmploController@fetch11')->name('employee.fetch11');
Route::post('employee/fetch111', 'employee\EmploController@fetch111')->name('employee.fetch111');

Route::post('fetch33', 'HomeController@fetch33')->name('fetch33');
Route::post('fetch333', 'HomeController@fetch333')->name('fetch333');

Route::post('admin/documents', 'AdminController@documents')->name('admin.documents');

Route::post('workrecord/stateupdate', 'Admin\WorkrecordController@stateupdate')->name('workrecord.stateupdate');
Route::post('workrecord/storefederalupdate', 'Admin\WorkrecordController@storefederalupdate')->name('workrecord.storefederalupdate');
Route::post('workrecord/storefederaladmin', 'Admin\WorkrecordController@storefederal')->name('workrecord.storefederaladmin');
Route::post('workrecord/storefederal1', 'Admin\WorkrecordController@storefederal')->name('workrecord.storefederal1');

Route::post('workstatus/storefederalss', 'Admin\WorkstatusController@storefederalss')->name('workstatus.storefederalss');
Route::post('workstatus/updatetaxation', 'Admin\WorkstatusController@updatetaxation')->name('workstatus.updatetaxation');

Route::post('customer/updatecoversation', 'Admin\CustomerController@updatecoversation')->name('customer.updatecoversation');
Route::post('employee/updatecoversation', 'Admin\CustomerController@updatecoversation')->name('employee.updatecoversation');
Route::post('employee/updatecoversation', 'Admin\CustomerController@updatecoversation')->name('employee.updatecoversation');
Route::post('vendor/updatecoversation', 'Admin\CustomerController@updatecoversation')->name('vendor.updatecoversation');
Route::post('employee/updatenotes', 'Admin\EmployeeController@updatenotes')->name('employee.updatenotes');
Route::post('customer/updatenotes', 'Admin\CustomerController@updatenotes')->name('customer.updatenotes');
Route::post('vendor/updatenotes', 'Admin\VendorController@updatenotes')->name('vendor.updatenotes');

Route::post('workrecord/documents', 'Admin\WorkrecordController@documents')->name('workrecord.documents');
Route::post('workrecord/documents', 'employee\WorkrecordController@documents')->name('workrecord.documents');

Route::post('workstatus/fetchrec', 'employee\WorkstatusController@fetchrec')->name('workstatus.fetchrec');

Route::post('workstatus/destroy/{id}', 'Admin\WorkstatusController@destroy')->name('workstatus.destroy');

Route::post('workstatus/updatedocuments', 'Admin\WorkstatusController@updatedocuments')->name('workstatus.updatedocuments');
Route::post('workstatus/update', 'Admin\WorkstatusController@update')->name('workstatus.update');
Route::post('workstatus/addformation', 'Admin\WorkstatusController@addformation')->name('workstatus.addformation');
Route::post('workstatus/fetch', 'Admin\WorkstatusController@fetch')->name('workstatus.fetch');
Route::post('worktodo/fetch', 'Admin\WorktodoController@fetch')->name('worktodo.fetch');
Route::post('worknew/fetch', 'Admin\WorknewController@fetch')->name('worknew.fetch');

Route::post('workrecord/storefederal', 'employee\WorkrecordController@storefederal')->name('workrecord.storefederal');
Route::post('workrecord/fetchrec', 'employee\WorkrecordController@fetchrec')->name('workrecord.fetchrec');
Route::post('worknew/fetchemp', 'employee\WorknewController@fetchemp')->name('worknew.fetchemp');
Route::post('fscemployee/work',['as'=>'work.workcategorys','uses'=>'WorknewController@workcategorys']);
Route::get('worknew/destroydworkcat/{id}/{cid}', 'employee\WorknewController@destroydworkcat')->name('worknew.destroydworkcat');
Route::get('workstatus/destroyfederaltax/{id}', 'employee\WorkstatusController@destroyfederaltax')->name('workstatus.destroyfederaltax');
Route::get('employee/destroyteam/{id}', 'Admin\EmployeeController@destroyteam')->name('employee.destroyteam');



Route::get('clientsetup/destroybank/{id}', 'Admin\ClientsetupController@destroybank')->name('clientsetup.destroybank');
Route::get('clientsetup/destroycard/{id}', 'Admin\ClientsetupController@destroycard')->name('clientsetup.destroycard');

Route::post('workstatus/updatedocumentsemp', 'employee\WorkstatusController@updatedocumentsemp')->name('workstatus.updatedocumentsemp');

Route::post('adminworkstatus/storefederal', 'Admin\AdminworkstatusController@storefederal')->name('adminworkstatus.storefederal');
Route::post('adminworkstatus/updatetaxation', 'Admin\AdminworkstatusController@updatetaxation')->name('adminworkstatus.updatetaxation');
Route::post('adminworkstatus/addshareledger', 'Admin\AdminworkstatusController@addshareledger')->name('adminworkstatus.addshareledger');
Route::post('adminworkstatus/updateshareledger', 'Admin\AdminworkstatusController@updateshareledger')->name('adminworkstatus.updateshareledger');
Route::get('adminworkstatus/destroyledger/{id}', 'Admin\AdminworkstatusController@destroyledger')->name('adminworkstatus.destroyledger');
Route::get('admind4/admindelete4/{id}/{ccid}', ['as' => 'admind4.admindelete4', 'uses' => 'employee\ClientsController@admindelete4']);
Route::post('adminworkstatus/addnewbanking', 'Admin\AdminworkstatusController@addnewbanking')->name('adminworkstatus.addnewbanking');
Route::post('adminworkstatus/updatenewbanking', 'Admin\AdminworkstatusController@updatenewbanking')->name('adminworkstatus.updatenewbanking');
Route::get('workstatus/destroydocument/{id}/{clientids}', 'Admin\WorkstatusController@destroydocument')->name('workstatus.destroydocument');
Route::get('workstatus/destroydocumentemp/{id}/{clientids}', 'employee\WorkstatusController@destroydocumentemp')->name('workstatus.destroydocumentemp');
Route::get('worknew/destroydworkcat/{id}/{cid}', 'Admin\WorknewController@destroydworkcat')->name('worknew.destroydworkcat');

});




Route::post('/checkclientfile','CustomerController@checkclientfile');

Route::group(['namespace'=>'Admin'],function()
{
Route::resource('fac-Bhavesh-0554/language','LanguageController');
Route::resource('fac-Bhavesh-0554/holiday','HolidayController');

Route::resource('fac-Bhavesh-0554/ethnic','EthnicController');

Route::resource('fac-Bhavesh-0554/submissionreq','SubmissionController');

Route::match(['get', 'post'], 'ajax-image-upload/{id}', 'EmployeeController@ajaxImage');
Route::delete('ajax-remove-image/{filename}', 'EmployeeController@deleteImage');
Route::match(['get', 'post'], 'ajax-image-upload1/{id}', 'EmployeeController@ajaxImage1');
Route::delete('ajax-remove-image1/{filename}', 'EmployeeController@deleteImage1');
Route::match(['get', 'post'], 'ajax-image-upload2/{id}', 'EmployeeController@ajaxImage2');
Route::delete('ajax-remove-image2/{filename}', 'EmployeeController@deleteImage2');
Route::match(['get', 'post'], 'pfid_upload/{id}', 'EmployeeController@pfid_upload1');
Route::match(['get', 'post'], 'pfid_upload2/{id}', 'EmployeeController@pfid_upload2');
Route::match(['get', 'post'], 'photo_upload/{id}', 'EmployeeController@photo_upload2');
Route::delete('pfid_upload/{filename}', 'EmployeeController@pfid_upload1');

Route::resource('fac-Bhavesh-0554/employee','EmployeeController');
Route::resource('fac-Bhavesh-0554/clientemployee','ClientemployeeController');
Route::post('fac-Bhavesh-0554/login', 'Auth\LoginController@login');         
Route::resource('fac-Bhavesh-0554/business','BusinessController');
Route::resource('fac-Bhavesh-0554/language','LanguageController');

Route::resource('fac-Bhavesh-0554/bankingsetup','BankingsetupController');

Route::resource('fac-Bhavesh-0554/statetax','StatetaxController');
Route::resource('fac-Bhavesh-0554/businesscard','BusinesscardController');
Route::resource('fac-Bhavesh-0554/letterhead','LetterheadController');

Route::resource('fac-Bhavesh-0554/accountcode','AccountcodeController');
Route::resource('fac-Bhavesh-0554/emailsetup','EmailSetupController');
Route::resource('fac-Bhavesh-0554/howtodo','HowtodoController');
Route::get('/faderal','AdminProfileController@getfaderal');

Route::get('/gemails','EmailController@getemails');
Route::get('fac-Bhavesh-0554/getemailList/{id}','EmailController@getEmailList');

Route::get('/getfaderalsss','AdminProfileController@getfaderalss');
Route::resource('fac-Bhavesh-0554/adminprofile','AdminProfileController');
Route::get('getlegalname','AdminProfileController@getlegalname');
Route::get('/getforms','AdminProfileController@getformss');
Route::get('/getimage','AdminProfileController@getimages');

Route::get('/history1','AdminProfileController@history');

Route::get('/clientid','MessageController@clientname12');
Route::resource('fac-Bhavesh-0554/msg','MessageController');
Route::resource('fac-Bhavesh-0554/msgout','MessageoutController');
Route::resource('fac-Bhavesh-0554/schedulesetup','SchedulesetupController');
Route::resource('fac-Bhavesh-0554/changepassword','ChangePasswordController');
Route::resource('fac-Bhavesh-0554/business-add-new','BusinessController');
Route::resource('fac-Bhavesh-0554/language-add-new','LanguageController');
Route::resource('fac-Bhavesh-0554/ethnic-add-new','EthnicController');
Route::resource('fscemployee/msgoutemp','MessageoutempController');

Route::resource('fac-Bhavesh-0554/businesscategory','CategoryController');    
Route::resource('fac-Bhavesh-0554/business-category-add-new','CategoryController');
Route::resource('fac-Bhavesh-0554/business-brand','BusinessBrandController');
Route::get('/getRequest','BusinessBrandController@getcategory');


Route::get('/getRequestbrand','BusinessBrandController@getbrand');
Route::resource('fac-Bhavesh-0554/employment','EmploymentContoller');
Route::get('deletecon/deletecon1', ['as' => 'delete1.delete', 'uses' => 'CustomerController@deletecon']);
Route::resource('fac-Bhavesh-0554/ee-schedule','EmployeescheduleController');
Route::post('fac-Bhavesh-0554/typeof',['as'=>'typeof.typeofentity','uses'=>'TaxFederalController@entityies']);
Route::get('/salestax','CustomerController@salestax1');
Route::get('/salestaxacc','CustomerController@salestaxacc');

Route::get('/getbankdata','CustomerController@getbankdata');
Route::get('/getrouting','ClientsetupController@getrouting');



Route::get('/salestaxations','CustomerController@salestaxations');

Route::get('/salestaxpay','CustomerController@salestaxpay');


Route::get('/getlanguage','CustomerController@getlanguage');
Route::get('/getsecondlanguage','CustomerController@getsecondlanguage');

Route::get('/lock','CustomerController@locked2');
Route::get('/taxlock','CustomerController@taxlocked2');
Route::get('/updateemployee','CustomerController@updateemployee');
Route::get('/updateacccheckby','CustomerController@updateacccheckby');
Route::get('/taxupdateacccheckby','CustomerController@taxupdateacccheckby');


Route::get('/taxupdateemployee','CustomerController@taxupdateemployee');


Route::get('bank/banks/{id}', ['as' => 'bank.banks', 'uses' => 'ClientsetupController@banks']);
Route::get('bank1/banks1/{id}', ['as' => 'bank1.banks1', 'uses' => 'ClientsetupController@banks1']);




Route::get('notes/notedelete/{id}', ['as' => 'notes.notedelete', 'uses' => 'CustomerController@notedelete']);
Route::get('locked/lockeddelete/{id}', ['as' => 'locked.lockdelete', 'uses' => 'CustomerController@locksdelete']);
Route::get('howtodo1/howtododelete/{id}', ['as' => 'howtodo1.howtododelete', 'uses' => 'HowtodoController@howtodo11delete']);
Route::resource('fac-Bhavesh-0554/business-brand-category','BusinessBrandCategoryController');
Route::resource('fac-Bhavesh-0554/business-brand-category-add-new','BusinessBrandCategoryController');

Route::get('/getEEResponsibility','CustomerController@getEEResponsibility');

Route::resource('fac-Bhavesh-0554/customer','CustomerController');

Route::resource('fac-Bhavesh-0554/vendor','VendorController');
Route::post('fac-Bhavesh-0554/product',['as'=>'product.productss','uses'=>'VendorController@productss']);

Route::post('fac-Bhavesh-0554/positions',['as'=>'position.positions','uses'=>'VendorController@positions']);
Route::post('fac-Bhavesh-0554/team',['as'=>'team.teams','uses'=>'VendorController@teams']);
Route::post('fac-Bhavesh-0554/utilities',['as'=>'utilities.utilitiess','uses'=>'VendorController@utilitiess']);
Route::post('fac-Bhavesh-0554/insurances',['as'=>'insurance.insurances','uses'=>'VendorController@insurances']);
Route::post('fac-Bhavesh-0554/otherbusiness',['as'=>'otherbusiness.otherbusinesss','uses'=>'VendorController@otherbusinesss']);

Route::post('fac-Bhavesh-0554/relatednames',['as'=>'relatedname.relatednames','uses'=>'VendorController@relatednames']);
Route::post('fac-Bhavesh-0554/notesrelateds',['as'=>'notesrelated.notesrelateds','uses'=>'VendorController@notesrelateds']);
Route::post('fac-Bhavesh-0554/profession',['as'=>'profile.profession','uses'=>'VendorController@profession']);

Route::get('fac-Bhavesh-0554/professionDelete', ['as' => 'profile.professionDelete', 'uses' => 'VendorController@professionDelete']);

Route::post('fac-Bhavesh-0554/work',['as'=>'work.workcategorys','uses'=>'WorktodoController@workcategorys']);
Route::post('fac-Bhavesh-0554/teamadd',['as'=>'work.teamadd','uses'=>'WorktodoController@teamadd']);

Route::post('fac-Bhavesh-0554/bankadd',['as'=>'bank.bankadd','uses'=>'ClientsetupController@bankadd']);
Route::post('fac-Bhavesh-0554/taxadd',['as'=>'tax.taxadd','uses'=>'ClientsetupController@taxadd']);
Route::post('fac-Bhavesh-0554/dependentadd',['as'=>'dependent.dependentadd','uses'=>'ClientsetupController@dependentadd']);

Route::post('fac-Bhavesh-0554/incomeadd',['as'=>'income.incomeadd','uses'=>'ClientsetupController@incomeadd']);
Route::post('fac-Bhavesh-0554/cardadd',['as'=>'card.cardadd','uses'=>'ClientsetupController@cardadd']);
Route::post('fac-Bhavesh-0554/expenseadd',['as'=>'expense.expenseadd','uses'=>'ClientsetupController@expenseadd']);

Route::get('products/removeproduct', ['as' => 'removeproduct.removeproducts', 'uses' => 'VendorController@removeproducts']);
Route::get('position/removeposition', ['as' => 'removeposition.removepositions', 'uses' => 'VendorController@removepositions']);
Route::get('teams/removeteam', ['as' => 'removeteam.removeteams', 'uses' => 'VendorController@removeteams']);
Route::get('utilities/removeutilities', ['as' => 'removeutilities.removeutilitiess', 'uses' => 'VendorController@removeutilitiess']);
Route::get('insurance_company/removeinsurance', ['as' => 'removeinsurance.removeinsurances', 'uses' => 'VendorController@removeinsurances']);
Route::get('utilities/removebusiness', ['as' => 'removebusiness.removebusinesss', 'uses' => 'VendorController@removebusinesss']);


Route::get('/getAccountcode','VendorController@Accountcode1');
Route::resource('fac-Bhavesh-0554/customerconvert','ClientconvertController');
Route::resource('fac-Bhavesh-0554/logo','LogoController');
Route::post('getclick','ClientconvertController@checkclient');
Route::post('fetch1','WorkrecordController@fetch1');
Route::post('fetch1','WorknewController@fetch1');


Route::post('checkclientemail1','ClientconvertController@checkclientemail');
Route::get('/getRequest','CustomerController@getcategory');
Route::get('/getRequests','CustomerController@getcategory');

Route::get('/getClientfilenameid','CustomerController@getfilenameid');


Route::get('/getnaics','CustomerController@getnaics');

Route::get('/getClientfilename','CustomerController@getfilename');
Route::get('/getAdminlic','AdminProfileController@getfilename');

Route::get('/getAdminlicprofession','AdminProfileController@getfilenameprofession');

Route::get('/getCountytaxstate','TaxStateController@getcountytax');
Route::get('/getCountycodetax','TaxStateController@getcountycodes');
Route::get('/getDocumentdata','AdminworkstatusController@getdocument');
Route::get('/getAdminlicensedata','AdminworkstatusController@getbuslicense');
Route::get('/getAdminprolicensedata','AdminworkstatusController@getprolicense');
Route::get('/getClientDocumentdata','WorkrecordController@getclientdocument');
Route::get('/getAdminformationdata','AdminworkstatusController@getformation');
Route::get('/getDependent','ClientsetupController@getDependent');
Route::get('/getWeges','ClientsetupController@getWeges');
Route::get('/getExpense','ClientsetupController@getExpense');
Route::get('/getTaxations','ClientsetupController@getTaxations');
Route::get('/getUtilitiesvendor','ClientsetupController@getUtilitiesvendors');

Route::get('/otherWeges','ClientsetupController@otherWeges');
Route::get('/getFormationdata','WorkstatusController@getFormationdata');
Route::get('/getConversationdata','CustomerController@getConversationdata');
Route::get('/getConversationdataemp','EmployeeController@getConversationdataemp');
Route::get('/getNotesemp','EmployeeController@getNotesemp');
Route::get('/getClientfederaldata','WorkrecordController@getClientfederaldata');

Route::get('/getClientfederalyear','WorkrecordController@getClientfederalyear');

Route::get('/getIncomedata','WorkrecordController@getIncomedata');
Route::get('/getClientstatedata','WorkrecordController@getClientstatedata');
Route::get('/getClientdata','WorkrecordController@getClientdata');
Route::get('/getClientfederaldata2','WorktodotaxController@getClientfederaldata2');

Route::get('/getFormationexistdata','WorkstatusController@getFormationexistdata');
Route::get('/getTaxstatedata','WorkrecordController@getTaxstatedata');

Route::get('/getAdminshareledger','AdminworkstatusController@getshareledager');
Route::get('/getAdminnewbank','AdminworkstatusController@getnewbank');

Route::get('/getRequest_1','CustomerController@getcategory_1');
Route::get('/getcat','CustomerController@getcategory1');
Route::get('/getcatmm','CustomerController@getcategory1mm');
Route::get('/getcatm','CustomerController@getcategory1m');
Route::get('/getusertype','RulesController@getusertype');
Route::get('/getAdmintaxtiondata','AdminworkstatusController@gettaxation');
Route::get('/getClienttaxtiondata','AdminworkstatusController@getclienttaxation');
Route::get('/getcategoryimages','CustomerController@getcategoryimage');
Route::get('/getcategoryimages','CustomerController@getcategoryimage');
Route::get('/business_brand_category_name_1','CustomerController@business_brand_category_name');
//Route::get('/getRequest','BusinessBrandCategoryController@getcategory');
Route::resource('fac-Bhavesh-0554/slider','SliderController');
Route::resource('fac-Bhavesh-0554/homecontent','HomecontentController');

Route::resource('fac-Bhavesh-0554/employeapplication','EmployeApplicationController');
Route::resource('fac-Bhavesh-0554/candidate','CandidateController');
Route::get('co/update1/{id}', ['as' => 'co.update1', 'uses' => 'EmployeApplicationController@update1']);
Route::get('send/sendmail/{name}/{email}', ['as' => 'send.sendmail', 'uses' => 'EmployeApplicationController@sendmail']);
Route::get('co/deleteto/{id}', ['as' => 'co.deleteto', 'uses' => 'EmployeApplicationController@deleteto']);

Route::get('fac-Bhavesh-0554/services/createmaster', 'ServiceController@createmaster');
Route::post('fac-Bhavesh-0554/services/masterStore', 'ServiceController@masterStore');
Route::get('fac-Bhavesh-0554/services/getServiceMaster/{id}', 'ServiceController@getSelectedServiceData');
Route::resource('fac-Bhavesh-0554/services','ServiceController');



Route::resource('fac-Bhavesh-0554/contact','ContactController');
Route::get('fac-Bhavesh-0554/serviceimages/{id}','ServiceImagesController@index');
Route::get('fac-Bhavesh-0554/serviceimages/create/{id}','ServiceImagesController@create');
Route::post('fac-Bhavesh-0554/serviceimages/create/{id}',['as'=>'serviceimages.store','uses'=>'ServiceImagesController@store']);
Route::post('fac-Bhavesh-0554/workrecord/fetch1',['as'=>'workrecord.fetch1','uses'=>'WorkrecordController@fetch1']);

Route::resource('fac-Bhavesh-0554/workrecord','WorkrecordController');
Route::resource('fac-Bhavesh-0554/weges','WegesController');

Route::post('fac-Bhavesh-0554/workrecord/updatetaxation',['as'=>'workrecord.updatetaxation','uses'=>'WorkrecordController@updatetaxation']);
Route::post('fac-Bhavesh-0554/worknew/fetch1',['as'=>'worknew.fetch1','uses'=>'WorknewController@fetch1']);
Route::post('fac-Bhavesh-0554/worknew/fetch2',['as'=>'worknew.fetch2','uses'=>'WorknewController@fetch2']);


Route::post('worknew/fetch1', 'employee\WorknewController@fetch1')->name('worknew.fetch1');
Route::post('worknew/fetch2', 'employee\WorknewController@fetch2')->name('worknew.fetch2');

Route::post('fac-Bhavesh-0554/worktodo/fetch3',['as'=>'worktodo.fetch3','uses'=>'WorktodoController@fetch3']);

Route::post('fac-Bhavesh-0554/adminworkstatus/updatedocument',['as'=>'adminworkstatus.updatedocument','uses'=>'AdminworkstatusController@updatedocument']);
Route::post('fac-Bhavesh-0554/adminworkstatus/updatedocuments/{id}',['as'=>'adminworkstatus.updatedocuments','uses'=>'AdminworkstatusController@updatedocuments']);

Route::post('fac-Bhavesh-0554/adminworkstatus/updatebuslicense/{id}',['as'=>'adminworkstatus.updatebuslicense','uses'=>'AdminworkstatusController@updatebuslicense']);
Route::post('fac-Bhavesh-0554/adminworkstatus/updateprolicense',['as'=>'adminworkstatus.updateprolicense','uses'=>'AdminworkstatusController@updateprolicense']);

Route::post('fac-Bhavesh-0554/adminworkstatus/updatingeprolicense',['as'=>'adminworkstatus.updatingeprolicense','uses'=>'AdminworkstatusController@updatingeprolicense']);
Route::post('fac-Bhavesh-0554/adminworkstatus/update',['as'=>'adminworkstatus.update','uses'=>'AdminworkstatusController@update']);

Route::post('fac-Bhavesh-0554/workrecord/updatedocument',['as'=>'workrecord.updatedocument','uses'=>'WorkrecordController@updatedocument']);
Route::post('fac-Bhavesh-0554/workrecord/updatedocuments/{id}',['as'=>'workrecord.updatedocuments','uses'=>'WorkrecordController@updatedocuments']);

Route::post('worknew/updatedocument', 'employee\WorkrecordController@updatedocument')->name('worknew.updatedocument');



Route::resource('fac-Bhavesh-0554/serviceimages','ServiceImagesController');
Route::resource('fac-Bhavesh-0554/servicetype','ServicetypeController');
Route::resource('fac-Bhavesh-0554/applyservice','ApplyserController');  
Route::resource('fac-Bhavesh-0554/forms','FormController');
Route::resource('fac-Bhavesh-0554/position','PositionController'); 
Route::resource('fac-Bhavesh-0554/branch','BranchController');

//Routes of Prospect...

Route::get('fac-Bhavesh-0554/prospect/show/{id}','ProspectController@show');
Route::get('fac-Bhavesh-0554/prospect/getpid','ProspectController@getGeneratePid');
Route::post('fac-Bhavesh-0554/prospect/storeProposalClient','ProspectController@storeProposalClient')->name('prospect.storeProposalClient');
Route::resource('fac-Bhavesh-0554/prospect','ProspectController');


Route::resource('fac-Bhavesh-0554/otherpage','OtherpageController'); 
Route::get('/getBranch','CandidateController@getbranch');
Route::get('/getBranch1','EmployeeController@getbranch1');
Route::get('/getemail','EmployeeController@checkemailAvailability');
Route::get('/getemail1','EmployeeController@getemp1');
Route::post('/admincheck','ChangePasswordController@getadminpassword');
Route::resource('fac-Bhavesh-0554/schedule','ScheduleController');
Route::resource('fac-Bhavesh-0554/link','LinkController');
Route::get('/getlink','LinkController@getlink1');
Route::get('/getform','FormController@getform1');
Route::get('/clientno1','LinkController@getclient1');

Route::get('fac-Bhavesh-0554/link/deleteimage/{id}/{image}', ['as' => 'link.deleteimage', 'uses' => 'LinkController@dropzoneDelete']);

Route::resource('fac-Bhavesh-0554/linkcategory','LinkCategoryController');
Route::resource('fac-Bhavesh-0554/formcategory','FormcategoryController');
Route::get('fac-Bhavesh-0554/certificate/{id}','CertificateController@index');
Route::get('fac-Bhavesh-0554/fscemployeereport','FscemployeereportController@index');
//Route::get('/createmaster','ServiceController@createmaster');

Route::resource('fac-Bhavesh-0554/fscclientreport','FscclientreportController');
Route::resource('fac-Bhavesh-0554/fscworkreport','FscworkreportController');

Route::resource('fac-Bhavesh-0554/fsceeworkreport','FsceeworkreportController');

Route::get('fac-Bhavesh-0554/clientreport','ClientreportController@index');
Route::get('fac-Bhavesh-0554/clientschedules','ClientschedulesController@index');
Route::post('fac-Bhavesh-0554/fscemployeereport',['as'=>'fscemployeereport.store','uses'=>'FscemployeereportController@store']);
Route::post('fac-Bhavesh-0554/email/mainstore/',['as'=>'email.mainstore','uses'=>'EmailController@mainstore']);


Route::get('fscemployeereport/reportedit', ['as' => 'fscemployeereport.reportedit', 'uses' => 'FscemployeereportController@reportedit1']);
Route::resource('fac-Bhavesh-0554/fscemployeereport','FscemployeereportController');

Route::get('fac-Bhavesh-0554/fscemployeereport1','FscemployeereportController@report');
Route::get('report/reportremove', ['as' => 'report.reportremove', 'uses' => 'FscemployeereportController@reportremove1']);
Route::get('/getschedule','ScheduleController@employee');
Route::get('/getduration','ScheduleController@getduration1');
Route::get('/getcontrol','AdminProfileController@getcontrol1');
Route::get('/getdurationemp1','EmployeescheduleController@getdurationemp');
Route::get('/getdurationempclient','EmployeescheduleController@getdurationempclient');

Route::post('fac-Bhavesh-0554/setup/storeRegard', 'SetupController@storeRegard');
Route::post('fac-Bhavesh-0554/setup/updateRegard', 'SetupController@updateRegard');
Route::get('fac-Bhavesh-0554/setup/delRegards/{id}', 'SetupController@delRegards');

Route::get('fac-Bhavesh-0554/appointment/getAppointment', 'AppointmentController@getAppointment');
Route::get('fac-Bhavesh-0554/appointment/getTeamRep/{id}', 'AppointmentController@getTeamRep');

Route::post('fac-Bhavesh-0554/setup/storeWorkType', 'SetupController@storeWorkType');
Route::post('fac-Bhavesh-0554/setup/updateWorkType', 'SetupController@updateWorkType');
Route::get('fac-Bhavesh-0554/setup/delWorkType/{id}', 'SetupController@delWorkType');

Route::get('fac-Bhavesh-0554/setup/appointment','SetupController@appointment');
Route::resource('fac-Bhavesh-0554/setup','SetupController');

Route::get('/ckcurrency','PriceController@ckcurrency1');
Route::resource('fac-Bhavesh-0554/price','PriceController');
Route::get('/checkperiod','PriceController@checkperiod1');
Route::get('review1/removecurrency', ['as' => 'removecurrency.removecurrency1', 'uses' => 'PriceController@removecurrency2']);
Route::get('removepriceemp1/removepriceemp', ['as' => 'removepriceemp.removepriceemp1', 'uses' => 'PriceController@removepriceemp2']);

Route::get('review1/removetypeofservice', ['as' => 'removetypeofservice.removetypeofservice1', 'uses' => 'PriceController@removetypeofservice2']);
Route::get('review1/removetaxtService', ['as' => 'removetaxtService.removetaxtService', 'uses' => 'PriceController@removetaxtService']);

Route::get('review1/removeperiod', ['as' => 'removeperiod.removeperiod1', 'uses' => 'PriceController@removeperiod2']);
Route::post('fac-Bhavesh-0554/currency',['as'=>'currency.currencies','uses'=>'PriceController@currencies']);

Route::post('fac-Bhavesh-0554/license',['as'=>'license.licenses','uses'=>'TaxationSetupController@typelicense']);
Route::get('review1/removelicense', ['as' => 'removelicense.removelicense1', 'uses' => 'TaxationSetupController@removelicense2']);

Route::post('fac-Bhavesh-0554/title',['as'=>'title.taxtitle','uses'=>'PriceController@taxtitle']);
Route::post('fac-Bhavesh-0554/typeofser',['as'=>'typeofser.typeofsers','uses'=>'PriceController@typeofser']);
Route::post('fac-Bhavesh-0554/period',['as'=>'period.periods','uses'=>'PriceController@period1']);
Route::get('getsign','PriceController@getsign1');
Route::get('getsign2','CustomerController@getsign3');Route::get('getsign2','CustomerController@getsign3');

Route::get('removepurpose/removepurpose1', ['as' => 'removepurpose.removepurpose1', 'uses' => 'MessageController@removepurpose2']);
Route::get('removemsg/removemsg1', ['as' => 'removemsg.removemsg1', 'uses' => 'MessageController@removemsgs']);
Route::get('removerelated/removerelated1', ['as' => 'removerelated.removerelated1', 'uses' => 'MessageController@removerelateds']);
Route::get('removenote/removenotes1', ['as' => 'removenote.removenotes1', 'uses' => 'MessageController@removenotes']);

//Route::get('getComplete/getComplete1', ['as' => 'getComplete.getComplete1', 'uses' => 'MessageController@getComplete']);

Route::get('servicedeletes/servicedel1', ['as' => 'servicedeletes.servicedel1', 'uses' => 'CustomerController@servicedel2']);
Route::get('servicedeletes/deleteacc', ['as' => 'servicedeletes.deleteacc', 'uses' => 'CustomerController@deleteacc']);

Route::get('servicedeletes/taxservicedel1', ['as' => 'servicedeletes.taxservicedel1', 'uses' => 'CustomerController@taxservicedel2']);
Route::get('servicedeletes/servicedels', ['as' => 'servicedeletes.servicedels', 'uses' => 'CustomerController@servicedel3']);
Route::get('resetServiceInfo/resetServiceInfo', ['as' => 'resetServiceInfo.resetServiceInfo', 'uses' => 'CustomerController@resetServiceInfo']);

Route::get('removeclockin/removeclockin1', ['as' => 'removeclockin1.removeclockin', 'uses' => 'ScheduleController@Clockin']);
Route::resource('fac-Bhavesh-0554/task','TaskController');
Route::resource('fac-Bhavesh-0554/federal','FederalController');
Route::resource('fac-Bhavesh-0554/state','StateController');
Route::resource('fac-Bhavesh-0554/city','CityController');
Route::get('getcounty1','CityController@getcounty');
Route::get('getcountycode1','CityController@getcountycode');

Route::get('getcountcount','AdminProfileController@getcountycounty');

Route::get('getcount','AdminProfileController@getcounty2');
Route::get('getcountycod','AdminProfileController@getcountycode2');
Route::get('getbankcode','AdminworkstatusController@getbankcodes');
Route::get('getbankcode2','AdminworkstatusController@getbankcodes2');

Route::resource('fac-Bhavesh-0554/taxfederal','TaxFederalController');
Route::get('taxfederal/taxfederaldelete/{id}', ['as' => 'taxfederal.taxfederaldelete', 'uses' => 'TaxFederalController@taxfederaldelete']);
Route::get('taxfederal/taxfederaldeletepay/{id}', ['as' => 'taxfederal.taxfederaldeletepay', 'uses' => 'TaxFederalController@taxfederaldeletepay']);

Route::resource('fac-Bhavesh-0554/states','TaxStatesController');
Route::resource('fac-Bhavesh-0554/taxstate','TaxStateController');
Route::get('taxstate/countydelete/{id}', ['as' => 'taxstate.countydelete', 'uses' => 'TaxStateController@countydelete']);
Route::get('states/statetaxdelete/{id}', ['as' => 'states.statetaxdelete', 'uses' => 'TaxStatesController@statetaxdelete']);

Route::get('customer/destroyinfo/{id}', ['as' => 'customer.destroyinfo', 'uses' => 'CustomerController@destroyinfo']);
Route::get('customer/destroytax/{id}', ['as' => 'customer.destroytax', 'uses' => 'CustomerController@destroytax']);
Route::get('customer/resetServiceInfo1/{cid}', ['as' => 'customer.resetServiceInfo1', 'uses' => 'CustomerController@resetServiceInfo1']);

Route::get('adminworkstatus/destroylicense/{id}', ['as' => 'adminworkstatus.destroylicense', 'uses' => 'AdminworkstatusController@destroylicense']);

Route::get('adminworkstatus/newbankingdelete/{id}', ['as' => 'adminworkstatus.newbankingdelete', 'uses' => 'AdminworkstatusController@newbankingdelete']);
Route::get('workrecord/destroyfederal/{id}',['as'=>'workrecord.destroyfederal','uses'=>'WorkrecordController@destroyfederal']);
Route::get('workrecord/destroystate/{id}',['as'=>'workrecord.destroystate','uses'=>'WorkrecordController@destroystate']);
Route::get('clientsetup/destroyfilling/{id}',['as'=>'clientsetup.destroyfilling','uses'=>'ClientsetupController@destroyfilling']);
Route::get('clientsetup/destroydependent/{id}',['as'=>'clientsetup.destroydependent','uses'=>'ClientsetupController@destroydependent']);
Route::get('clientsetup/destroyincome/{id}',['as'=>'clientsetup.destroyincome','uses'=>'ClientsetupController@destroyincome']);
Route::get('clientsetup/destroyemployee/{empid}',['as'=>'clientsetup.destroyemployee','uses'=>'ClientsetupController@destroyemployee']);
Route::get('clientsetup/clients/destryotherincome/{id2}',['as'=>'clientsetup.destryotherincome','uses'=>'ClientsetupController@destryotherincome']);
Route::post('clientsetup/addweges/{id}',['as'=>'clientsetup.addweges','uses'=>'ClientsetupController@addweges']);
Route::post('clientsetup/addwegesbank/{id}',['as'=>'clientsetup.addwegesbank','uses'=>'ClientsetupController@addwegesbank']);
Route::post('clientsetup/updatedependent',['as'=>'clientsetup.updatedependent','uses'=>'ClientsetupController@updatedependent']);
Route::post('clientsetup/updateincomeweges',['as'=>'clientsetup.updateincomeweges','uses'=>'ClientsetupController@updateincomeweges']);
Route::post('clientsetup/updateotherweges',['as'=>'clientsetup.updateotherweges','uses'=>'ClientsetupController@updateotherweges']);
Route::post('clientsetup/updateexpense',['as'=>'clientsetup.updateexpense','uses'=>'ClientsetupController@updateexpense']);
Route::post('clientsetup/updateintaxation',['as'=>'clientsetup.updateintaxation','uses'=>'ClientsetupController@updateintaxation']);
Route::post('clientsetup/addutilitiesdetail',['as'=>'clientsetup.addutilitiesdetail','uses'=>'ClientsetupController@addutilitiesdetail']);

Route::get('clientsetup/destroyexp/{id}',['as'=>'clientsetup.destroyexp','uses'=>'ClientsetupController@destroyexp']);
Route::get('fscemployee/clients/destroyemployee/{empid}',['as'=>'clients.destroyemployee','uses'=>'employee\ClientsController@destroyemployee']);
Route::get('fscemployee/clients/destryotherincome/{id2}',['as'=>'clients.destryotherincome','uses'=>'employee\ClientsController@destryotherincome']);


Route::get('adminworkstatus/destroyprolic/{id}', ['as' => 'adminworkstatus.destroyprolic', 'uses' => 'AdminworkstatusController@destroyprolic']);
Route::get('adminworkstatus/destroydocument/{id}', ['as' => 'adminworkstatus.destroydocument', 'uses' => 'AdminworkstatusController@destroydocument']);
Route::get('adminworkstatus/destroydocumenttitle/{id}', ['as' => 'adminworkstatus.destroydocumenttitle', 'uses' => 'AdminworkstatusController@destroydocumenttitle']);


Route::get('fac-Bhavesh-0554/workrecord/destroydocumenttitle/{id}',['as'=>'workrecord.destroydocumenttitle','uses'=>'WorkrecordController@destroydocumenttitle']);
Route::get('fac-Bhavesh-0554/workrecord/destroydocument/{id}', ['as' => 'workrecord.destroydocument', 'uses' => 'WorkrecordController@destroydocument']);

Route::get('fscemployee/workrecord/destroydocumenttitle/{id}',['as'=>'workrecord.destroydocumenttitle','uses'=>'WorkrecordController@destroydocumenttitle']);

Route::get('clientsetup/getincomeweges/{id}/{cid}/{cids}', 'Admin\ClientsetupController@getincomeweges')->name('worknew.getincomeweges');


Route::resource('fac-Bhavesh-0554/taxcity','TaxCityController');
Route::resource('fac-Bhavesh-0554/empleave','EmpLeaveController');
Route::resource('fac-Bhavesh-0554/clientsetup','ClientsetupController');
Route::get('/getimagess','ClientsetupController@getimagesssss');
Route::get('/getcertificate','ClientsetupController@getcertificate');
Route::get('/getAdmincertificate','AdminProfileController@getAdmincertificate');

Route::get('/getAdminprolic','AdminProfileController@getAdminprolic');

Route::get('/getaoi','ClientsetupController@getaoi');
Route::get('/getAdminaoi','AdminProfileController@getAdminaoi');
Route::get('/getWorkannual','ClientsetupController@getWorkannual');
Route::get('/getWorkofficer','ClientsetupController@getWorkofficer');



Route::post('adminworkstatus','AdminworkstatusController@store'); 


Route::get('/client_number','ClientsetupController@client_number1');
Route::resource('fac-Bhavesh-0554/cservice','CSServiceController');

Route::resource('fac-Bhavesh-0554/adminupload','AdminUploadController');
Route::resource('fac-Bhavesh-0554/clientupload','ClientUploadController');
Route::resource('fac-Bhavesh-0554/contactinquery','ContactinqueryController');
Route::resource('fac-Bhavesh-0554/linkinquery','LinkinqueryController');
Route::resource('fac-Bhavesh-0554/control','ControlController');
Route::resource('fac-Bhavesh-0554/marque','MarqueController');
Route::resource('fac-Bhavesh-0554/zipcode','ZipcodeController');
Route::resource('fac-Bhavesh-0554/citystate','CitystateController');

Route::resource('fac-Bhavesh-0554/categorys','CategorysController');
Route::resource('fac-Bhavesh-0554/subcategorys','SubcategorysController');
Route::resource('fac-Bhavesh-0554/questionsection','QuestionSectionController');

Route::get('fac-Bhavesh-0554/addressbook','AddressbookController@index');
Route::get('fac-Bhavesh-0554/addressbook/{id}/{type}/edit',['as'=>'addressbook.edit','uses'=>'AddressbookController@edit']); 
Route::resource('fac-Bhavesh-0554/question','QuestionController');
Route::resource('fac-Bhavesh-0554/formationsetup','FormationSetupController');

Route::resource('fac-Bhavesh-0554/license','LicenseController');

Route::resource('fac-Bhavesh-0554/taxation','TaxationSetupController');
Route::get('states/destroylicense/{id}', ['as' => 'states.destroylicense', 'uses' => 'TaxStatesController@destroylicense']);

Route::get('fac-Bhavesh-0554/answer/create/{id}','AnswerController@create');

Route::resource('fac-Bhavesh-0554/answer', 'AnswerController', ['only' => ['store']]);

Route::resource('fac-Bhavesh-0554/rules','RulesController');
Route::get('clientd/clientdelete/{id}', ['as' => 'clientd.clientdelete', 'uses' => 'ClientsetupController@clientdelete']);

Route::get('admind2/admindelete2/{id}/{ccid}', ['as' => 'admind2.admindelete2', 'uses' => 'ClientsetupController@clientdelete1']);


Route::get('review/reviewdelete/{id}', ['as' => 'review.reviewdelete', 'uses' => 'EmployeeController@reviewdelete1']);
Route::get('employee/removeimage/{id}', ['as' => 'employee.removeimage', 'uses' => 'EmployeeController@removeimage']);
Route::get('employee/removeemailrights/{id}/{empid}', ['as' => 'employee.removeemailrights', 'uses' => 'EmployeeController@removeEmailRights']);
Route::get('employee/removeimage1/{id}', ['as' => 'employee.removeimage1', 'uses' => 'EmployeeController@removeimage1']);
Route::get('employee/removeimage2/{id}', ['as' => 'employee.removeimage2', 'uses' => 'EmployeeController@removeimage2']);
Route::get('employee/removeimage3/{id}', ['as' => 'employee.removeimage3', 'uses' => 'EmployeeController@removeimage3']);



Route::get('pay/paydelete/{id}', ['as' => 'pay.paydelete', 'uses' => 'EmployeeController@paydelete1']);
Route::get('admind1/admindelete1/{id}', ['as' => 'admind1.admindelete1', 'uses' => 'AdminProfileController@admindelete1']);
Route::get('admin/adminbankdelete/{id}', ['as' => 'admin.adminbankdelete', 'uses' => 'AdminProfileController@adminbankdelete']);
Route::get('admin/admincarddelete/{id}', ['as' => 'admin.admincarddelete', 'uses' => 'AdminProfileController@admincarddelete']);

Route::get('admin/clientbankdatadelete/{id}', ['as' => 'admin.clientbankdatadelete', 'uses' => 'ClientsetupController@clientbankdatadelete']);
Route::get('admin/clientcarddatadelete/{id}', ['as' => 'admin.clientcarddatadelete', 'uses' => 'ClientsetupController@clientcarddatadelete']);
Route::resource('fac-Bhavesh-0554/emptechnical','EmptechnicalController');

Route::get('admind/admindelete/{id}', ['as' => 'admind.admindelete', 'uses' => 'AdminProfileController@admindelete']);
Route::get('note/notedelete/{id}', ['as' => 'note.notedelete', 'uses' => 'AdminProfileController@notedelete']);

Route::post('fac-Bhavesh-0554/adminprofile/updateformation',['as'=>'adminprofile.updateformation','uses'=>'AdminProfileController@updateformation']);

Route::get('empnote/empnotedelete/{id}', ['as' => 'empnote.empnotedelete', 'uses' => 'EmployeeController@empnotedelete']);
Route::get('taxa/taxadelete/{id}', ['as' => 'taxa.taxadelete', 'uses' => 'PriceController@taxadelete1']);
Route::get('emppay/emppaydelete/{id}', ['as' => 'emppay.emppaydelete', 'uses' => 'EmployeeController@emppaydelete']);
Route::get('empwh/empwhdelete/{id}', ['as' => 'empwh.empwhdelete', 'uses' => 'EmployeeController@empwhdelete']);

Route::get('empreview/empreviewdelete/{id}', ['as' => 'empreview.empreviewdelete', 'uses' => 'EmployeeController@empreviewdelete']);


Route::get('contactinfo/contactdelete/{id}', ['as' => 'contactinfo.contactdelete', 'uses' => 'EmployeeController@contactdelete']);

Route::resource('fac-Bhavesh-0554/servicesprocess','ServiceinquiryController');

Route::resource('fac-Bhavesh-0554/appointment','AppointmentController');
Route::post('/events','CalenderController@event');
Route::resource('fac-Bhavesh-0554/clienttechnical','ClienttechnicalController');
Route::resource('fac-Bhavesh-0554/calender','CalenderController');
Route::resource('fac-Bhavesh-0554/email','EmailController');
Route::resource('fac-Bhavesh-0554/getemail','GetemailController');
Route::get('/cestatus1','CestatusController@cestatus');
Route::get('/getdata','CestatusController@getdata1');
Route::resource('fac-Bhavesh-0554/cestatus','CestatusController');
Route::resource('fac-Bhavesh-0554/insurance','InsController');
Route::resource('fac-Bhavesh-0554/supervisor','SupervisorController');
Route::resource('fac-Bhavesh-0554/submissionrequest','SubmissionRequestController');

});