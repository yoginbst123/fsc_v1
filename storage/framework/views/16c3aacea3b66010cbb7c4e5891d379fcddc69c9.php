
<?php $__env->startSection('main-content'); ?>
    <style>
        #clientid-error {
            position: absolute;
        }
        .nex1:hover, .nex1:focus {
            background-color: transparent;
        }
        .ag {
            position: relative;
            top: -25px;
            font-size: 13px;
            left: -100px;
        }
        .green-border {
            background-color: #003b6d !important;
        }
        .breadcrumb {
            background: #fff2b3;
            border: 2px solid #103b68;
        }
        .breadcrumb li a {
            color: #103b68;
        }
        .breadcrumb > li.ddd.active1 a {
            background: green !important;
        }
        .title-form-group p {
            font-size: 16px;
            line-height: 30px;
        }
        .tess {
            text-align: left;
            float: right;
            font-size: 11px;
            margin-right: 20px;
        }
        .star-required1 {
            color: transparent;
        }
        .fsc-reg-sub-header {
            padding-left: 0;
        }
        .pager li > a, .pager li > span {
            width: 93px;
        }
        .Branch h1 {
            line-height: 35px;
        }
        .services2, .services3, .services4 .border .col-md-12 {
            border: 1px solid #cccccc;
            padding: 15px 0px !important;
            margin-bottom: 15px !important;
        }
        .nav-pills > li.active > a, .nav-pills > li.active > a:focus, .nav-pills > li.active > a:hover {
            color: #fff;
            background-color: green;
        }
        .bread
        .title-form-group p {
            font-size: 12px
        }

        li.next {
            width: 93px;
            font-size: 16px;
            padding: 0 0 10px 0;
        }

        .nav-pills > li > a {
            border-radius: 0 !important;
        }

        li.previous {
            width: 93px;
            font-size: 16px;
            padding: 0 0 10px 0;
        }

        .fsc-reg-sub-header-div {
            border-radius: 0;
            padding: 6px 4%;
            border-top: 2px solid #fff;
            border-bottom: 2px solid #fff;
        }

        .breadcrumb {
            padding: 0px;
            list-style: none;
            overflow: hidden;
            margin-top: -2px;
        }

        .breadcrumb > li + li:before {
            padding: 0;
        }

        .breadcrumb li {
            float: left;
            width: 24.6%;
            text-align: center;
            font-size: 15px;
            color: #103b68;
        }

        }
        .breadcrumb li.active a {
            background: brown;
            background: green;
        }

        .breadcrumb li.completed a {
            background: brown;
            background: hsla(153, 57%, 51%, 1);
        }

        .breadcrumb li.active a:after {
            border-left: 30px solid green;
        }

        .breadcrumb li.completed a:after {
            border-left: 30px solid hsla(153, 57%, 51%, 1);
        }

        .breadcrumb li a {
            width: 95%;
            text-decoration: none;
            padding: 10px 0 10px 45px;
            position: relative;
            display: block;
            float: left;
        }

        .breadcrumb li a:after {
            content: " ";
            display: block;
            width: 0;
            height: 0;
            border-top: 50px solid transparent;
            border-bottom: 50px solid transparent;
            border-left: 30px solid #fff2b3;
            position: absolute;
            top: 50%;
            margin-top: -50px;
            left: 100%;
            z-index: 2;
        }

        .breadcrumb li.selected-bg a:after {
            content: " ";
            display: block;
            width: 0;
            height: 0;
            border-top: 50px solid transparent;
            border-bottom: 50px solid transparent;
            border-left: 30px solid green;
            position: absolute;
            top: 50%;
            margin-top: -50px;
            left: 100%;
            z-index: 2;
        }

        .form-group.bg-color.has-success label {
            color: #fff;
        }

        .breadcrumb li a:before {
            content: " ";
            display: block;
            width: 0;
            height: 0;
            border-top: 50px solid transparent;
            /* Go big on the size, and let overflow hide */
            border-bottom: 50px solid transparent;
            border-left: 30px solid #103b68;
            position: absolute;
            top: 50%;
            margin-top: -50px;
            margin-left: 1px;
            left: 100%;
            z-index: 1;
        }

        .breadcrumb li a {
            border-top: 0 !important;
        }

        .breadcrumb li:first-child a {
            background: green;
            padding-left: 15px;
            width: 100%;
            color: #fff;
        }

        .breadcrumb li:first-child a:after {
            border-left: 30px solid green;
        }

        .breadcrumb li.selected-bg a {
            background: green;
            color: #fff;
        }

        li.previous.previous1 a {
            background: linear-gradient(to top, #ffcc99 0%, #ff6600 100%);
            margin-right: 20px;
        }

        .content-wrapper {
            height: 100%;
        }

        .breadcrumb li:nth-o a {
            padding-left: 15px;
            width: 100%;
            border-radius: 0;
            background: green;
        }

        .pager {
            top: auto;
            bottom: 4px;
            right: 25px;
            position: relative !important;
            display: table !important;
            float: right !important;
        }

        .pager .disabled > a, .pager .disabled > a:focus, .pager .disabled > a:hover, .pager .disabled > span {
            color: #fff;
            cursor: not-allowed;
            background-color: #428bca;
            font-size: 14px;
        }

        .pager li > a, .pager li > span {
            display: inline-block;
            padding: 5px 14px;
            background-color: #0472d0;
            border: 1px solid #ddd;
            border-radius: 15px;
            font-size: 14px;
            color: #fff;
        }

        .error {
            font-size: 16px;
            color: #ff0000;
            font-weight: normal;
        }

        .breadcrumb li a:hover {
            background: green !important;
            color: #fff;
        }

        .breadcrumb li a:hover:after {
            border-left-color: green !important;
        }

        .nav-tabs > li {
            width: 25%;
        }

        .nav-tabs > li > a {
            height: 40px;
        }

        .tess {
            text-align: right;
            position: absolute;
            font-size: 11px;
            margin: 22px 40px 0 0;
            right: 0;
            width: 100%;
        }

        .star-required {
            color: red;
            position: absolute;
        }

        .fieldGroup {
            width: 100%;
            display: inline-block;
            border-bottom: 2px solid #512e90;
            padding-bottom: 20px;
        }

        .fieldGroup:last-child {
            border-bottom: transparent;
        }

        .Red {
            background-color: red !important;
            color: #fff !important
        }

        .Blue {
            background-color: rgb(124, 124, 255) !important;
            color: #fff !important
        }

        .Green {
            background-color: #00ef00 !important;
            color: #fff !important
        }

        .Yellow {
            background-color: Yellow !important;
            color: #555 !important
        }

        .Orange {
            background-color: Orange !important;
            color: #fff !important
        }

        .btn3d.btn-info {
            background: linear-gradient(#e7f3ff, #74b7fd);
            font-size: 11px;
            padding: 9px 10px;
            color: #000000;
            font-weight: 600;
            border: 1px solid #000;
            width: 100%;
            transition: 0.2s;
            text-align: center;
            box-shadow: 0 4px 5px 0 rgba(0, 0, 0, 0.14), 0 1px 10px 0 rgba(0, 0, 0, 0.12), 0 2px 4px -1px rgba(0, 0, 0, 0.3);
            -webkit-box-shadow: 0 4px 5px 0 rgba(0, 0, 0, 0.14), 0 1px 10px 0 rgba(0, 0, 0, 0.12), 0 2px 4px -1px rgba(0, 0, 0, 0.3);
        }

        .btn3d.btn-info:hover, .btn3d.btn-info.active {
            background: linear-gradient(#74b7fd, #e7f3ff);
            transition: 0.2s;
        }

        .fsc-form-label {
            display: block;
            text-align: right;
        }

        .tab-content > .tab-pane {
            padding: 20px 0;
        }

        .btnNext {
            float: right;
        }

        .btnPrevious {
            float: left;
        }

        .ps_main {
            float: left;
        }

        .ps_mr {
            float: left;
            width: 14%;
            margin-right: 1%;
        }

        .ps_firstname {
            float: left;
            width: 33%;
            margin: 0 1%;
        }

        .ps_middlename {
            float: left;
            width: 10%;
            margin: 0 1%;
        }

        .ps_lastname {
            float: left;
            width: 33%;
            margin-left: 1%;
        }

        .form-control {
            font-size: 14px !important;
        }

        .form-control1 {
            width: 100%;
            line-height: 1.44;
            color: #555;
            border: 2px solid #286db5;
            border-radius: 3px;
            transition: border-color ease-in-out .15s;
            padding: 8px 3px 8px 8px !important;
        }

        .clientid2-error {
            color: red !important;
            font-size: 20px;
        }

        .center_main {
            width: 100%;
            text-align: center;
        }

        .center_tab {
            display: inline-block;
            width: 170px;
            margin: 0 10px;
        }

        .ser:nth-of-type(odd) {
            padding: 15px;
            width: 100%;
            background-color: #e8f7fd;
            margin: auto;
            border: solid 1px #d2c292;
        }

        .ser:nth-of-type(even) {
            background-color: #fff5dd;
            padding: 15px 0;
            width: 100%;
            margin: auto;
            border: solid 1px #d2c292;
        }

        .bg-color {
            background: #286db5;
            width: 100%;
            margin: 0 auto 16px auto !important;
            padding: 5px 0;
            color: #fff;
            border: 0;
        }

        .bg-color .form-control {
            background: #fff !important;
        }

        .breadcrumb li.selected-bg a:before {
            border-left: 30px solid #fff;
        }

        .pager.wizard {
            display: block !important;
            width: 100%;
            right: 0px;
        }

        .pager.wizard li.next.previous2 {
            float: right;
            margin-right: 0px;
        }

        .pager.wizard li.previous.previous1 a {
            margin-left: 0px !important;
        }

        .clientidbox {
            position: relative;
        }

        .clientidbox .error.help-block {
            position: absolute !important;
            top: 28px;
        }

        .tab-content > .tab-pane {
            padding: 10px 0 !important;
        }

        .breadcrumb > li + li:before {
            display: none;
        }

        textarea.form-control {

        }

        @media (max-width: 1032px) {
            .breadcrumb li a {
                height: 65px;
            }

            .tab_1 {
                line-height: 3;
            }

            .tab2 {
                line-height: 3;
            }

            .tab3 {
                line-height: 3;
            }
        }

        @media (max-width: 1023px) {
            .tab2 {
                line-height: 1.5;
            }
        }

        @media (max-width: 856px) {
            .tab_1 {
                line-height: 1.5;
            }
        }

        @media (max-width: 767px) {
            .tab_1 {
                line-height: 3;
            }
        }

        @media (max-width: 618px) {
            .tab_1 {
                line-height: 1.5;
            }
        }

        @media (max-width: 580px) {
            .breadcrumb li {
                width: 50%;
                margin: 0px !important;
                border: 1px solid;
            }

            .breadcrumb li a:before {
                display: none;
            }

            .breadcrumb li a:after {
                display: none;
            }

            .breadcrumb li a {
                padding: 5px 5px 10px 10px;
                height: 43px;
            }

            .tab_1, .tab1, .tab2, .tab3 {
                line-height: 2;
            }
        }
    </style>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="page-title content-header">
            <h2>Add New Client </h2>
        </section>
        <?php
        //$value = "This is a test string.";
        //$length = preg_match_all ('/[^ ]/' , $value, $matches);
        //echo $length; //18
        ?>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="">
                            <h3 class="box-title" style="margin-top:13px !important;"></h3>
                            <div class="box-tools pull-right">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="panel with-nav-tabs panel-primary">
                                <div class="panel-heading">
                                </div>
                                <form method="post" id="registrationForm" action="<?php echo e(route('customer.store')); ?>"
                                      class="form-horizontal" name="clientname" enctype="multipart/form-data">
                                    <?php echo e(csrf_field()); ?>

                                    <div id="smartwizard">
                                        <ul class="breadcrumb nav nav-pills"
                                            style="pointer-events: none;margin-bottom:0px !important;">
                                            <li class="active actab">
                                                <a href="#tab1primary" data-toggle="tab" class="tab_1">
                                                    Basic Information
                                                    <!--<i class="fa fa-check-square text-right con3" style="display:none" aria-hidden="true"></i>-->
                                                </a>
                                            </li>
                                            <li class="actab1">
                                                <a href="#tab2primary" class="tab1" data-toggle="tab">
                                                    Contact Information
                                                    <!--<i class="fa fa-check-square text-right con" style="display:none" aria-hidden="true"></i>-->
                                                </a>
                                            </li>
                                            <li class="actab2">
                                                <a href="#tab3primary" class="tab2" data-toggle="tab">
                                                    Service Information
                                                    <!--<i class="fa fa-check-square text-right con1" style="display:none" aria-hidden="true"></i>-->
                                                </a>
                                            </li>
                                            <li class="actab3">
                                                <a href="#tab4primary" class="tab3" data-toggle="tab">
                                                    Notes
                                                    <!--<i class="fa fa-check-square text-right con2" style="display:none"  aria-hidden="true"></i>-->
                                                </a>
                                            </li>
                                        </ul>
                                        <div class="panel-body">
                                            <div class="tab-content">
                                                <div class="tab-pane fade in active" data-step="0" id="tab1primary">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="Branch" style=" margin-left: 5%;width: 85%;">
                                                            <h1>Basic Information</h1>
                                                        </div>
                                                        <br/>
                                                        <div class="form-group clientidbox <?php echo e($errors->has('filename') ? ' has-error' : ''); ?>">
                                                            <label class="control-label col-md-3">Client ID (File No) :
                                                                <span class="star-required">*</span></label>
                                                            <div class="col-lg-8 col-md-9 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <div class="row">
                                                                    <div class="col-md-2 col-xs-6">
                                                                        <input type="text" placeholder="AB"
                                                                               onkeypress="autotab(this, document.clientname.filename)"
                                                                               maxlength="2"
                                                                               class="form-control fsc-input inputs"
                                                                               id="clientid" name="filename1">
                                                                    </div>
                                                                    <div class="col-md-2 col-xs-6">
                                                                        <input type="text" placeholder="123"
                                                                               onkeypress="autotab1(this, document.clientname.filename3)"
                                                                               class="form-control fsc-input"
                                                                               maxlength="2" id="clientid1"
                                                                               name="filename">
                                                                    </div>
                                                                    <div class="col-md-2 col-xs-6">
                                                                        <input type="text" placeholder="A"
                                                                               class="form-control fsc-input inputs"
                                                                               maxlength="12" id="clientid2"
                                                                               name="filename3"
                                                                               style="text-transform:uppercase;">

                                                                    </div>
                                                                    <input type="hidden" placeholder=""
                                                                           class="form-control fsc-input" value="Active"
                                                                           id="status" name="status">
                                                                    <em style="display:none;" id="clientid2-error"
                                                                        class="clientid2-error existmessage">ID already
                                                                        in use!</em>

                                                                    <div class="col-md-3 col-xs-6">

                                                                        <select name="status" id="status5"
                                                                                class="form-control fsc-input <?php if($errors->status == 'Approval'): ?> Yellow  <?php endif; ?>
                                                                                <?php if($errors->status == 'Pending'): ?> Orange <?php endif; ?>
                                                                                <?php if($errors->status == 'Hold'): ?> Blue  <?php endif; ?>
                                                                                <?php if($errors->status == 'Inactive'): ?> Red <?php endif; ?>
                                                                                <?php if($errors->status == 'Active'): ?> Green  <?php endif; ?>">
                                                                            <option value="">Status</option>
                                                                            <option value="Hold"
                                                                                    <?php if($errors->status == 'Hold'): ?> selected
                                                                                    <?php endif; ?> class="Blue">Hold
                                                                            </option>
                                                                            <option class="Orange" value="Pending"
                                                                                    <?php if($errors->status == 'Pending'): ?> selected <?php endif; ?>>
                                                                                Pending
                                                                            </option>
                                                                            <option value="Approval"
                                                                                    <?php if($errors->status == 'Approval'): ?> selected
                                                                                    <?php endif; ?> class="Yellow">Approve
                                                                            </option>
                                                                            <option value="Active" class="Green"
                                                                                    <?php if($errors->status == 'Active'): ?> selected <?php endif; ?>>
                                                                                Active
                                                                            </option>
                                                                            <option value="Inactive" class="Red"
                                                                                    <?php if($errors->status == 'Inactive'): ?> selected
                                                                                    <?php endif; ?> style="background:red;color:#fff">
                                                                                Inactive
                                                                            </option>
                                                                        </select>

                                                                        <!--<select name="status" id="status" class="form-control fsc-input">-->
                                                                        <!--   <option value="">Status</option>-->
                                                                        <!--   <option value="Hold" class="Blue">Hold</option>-->
                                                                        <!--   <option value="Pending" class="Orange">Pending</option>-->
                                                                        <!--   <option value="Approval" class="Yellow">Approve</option>-->
                                                                        <!--   <option value="Active" class="Green">Active</option>-->
                                                                        <!--   <option value="Inactive" class="Red">In Active</option>-->
                                                                        <!--</select>-->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group <?php echo e($errors->has('business_name') ? ' has-error' : ''); ?>">
                                                            <label class="control-label col-md-3">Business Type : <span
                                                                        class="star-required">*</span></label>
                                                            <div class="col-lg-8 col-md-9 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <select name="business_id" id="business_id"
                                                                                class="form-control fsc-input category">
                                                                            <option value=''>---Select---</option>
                                                                            <?php $__currentLoopData = $business; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $busi): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                                <option value='<?php echo e($busi->id); ?>'><?php echo e($busi->bussiness_name); ?></option>
                                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                        </select>
                                                                        <input class=" form-control fsc-input"
                                                                               type="hidden" value='' name="user_type"
                                                                               id="user_type">
                                                                    </div>
                                                                    <div class="col-md-3 pull-right">
                                                                        <div id="image"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="form-group personal" style="display:none">
                                                            <label class="control-label col-md-3">Name : <span
                                                                        class="star-required">*</span></label>
                                                            <div class="col-lg-8 col-md-9 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <div class="row">
                                                                    <div class="col-lg-2 col-md-2 col-xs-4 fsc-form-col fsc-element-margin">
                                                                        <select class="form-control fsc-input"
                                                                                id="nametype" name="nametype">
                                                                            <option value="mr">Mr.</option>
                                                                            <option value="mrs">Mrs.</option>
                                                                            <option value="miss">Miss.</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-lg-3 col-md-3 col-xs-8 fsc-element-margin">
                                                                        <input type="text"
                                                                               class="form-control  fsc-input textonly"
                                                                               id="first_name" name="first_name"
                                                                               placeholder="First Name">
                                                                    </div>
                                                                    <div class="col-lg-1 col-md-1 col-xs-4 fsc-element-margin"
                                                                         style="padding:0px;">
                                                                        <input type="text"
                                                                               class="form-control fsc-input textonly"
                                                                               id="middle_name" maxlength="1"
                                                                               name="middle_name" placeholder="M">
                                                                    </div>
                                                                    <div class="col-lg-3 col-md-3 col-xs-8 fsc-element-margin">
                                                                        <input type="text"
                                                                               class="form-control fsc-input textonly"
                                                                               id="last_name" name="last_name"
                                                                               placeholder="Last name">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group <?php echo e($errors->has('business_catagory_name') ? ' has-error' : ''); ?>"
                                                             style="display:none" id="business_catagory_name_2">
                                                            <label class="control-label col-md-3">Business Category Name
                                                                : <span class="star-required">*</span></label>
                                                            <div class="col-lg-8 col-md-9 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <select name="business_catagory_name"
                                                                                id="business_catagory_name"
                                                                                class="form-control fsc-input category1">
                                                                            <option value=''>---Select Business Category
                                                                                Name---
                                                                            </option>
                                                                            <?php $__currentLoopData = $category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                                <option value='<?php echo e($cate->id); ?>'><?php echo e($cate->business_cat_name); ?></option>
                                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-md-3 pull-right">
                                                                        <div id="image1"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group" id="business_catagory_name_3"
                                                             style="display:none">
                                                            <label class="control-label col-md-3">Business Brand : <span
                                                                        class="star-required">*</span></label>
                                                            <div class="col-md-6">
                                                                <div class="row">
                                                                    <div class="col-md-7">
                                                                        <select name="business_brand_name"
                                                                                id="business_brand_name"
                                                                                class="form-control fsc-input">
                                                                            <option value="">---Brand---</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-md-3 pull-right">
                                                                        <div id="image2"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group" id="business_catagory_name_4"
                                                             style="display:none">
                                                            <label class="control-label col-md-3">Business Brand
                                                                Category Name : <span
                                                                        class="star-required">*</span></label>
                                                            <div class="col-md-6">
                                                                <div class="row">
                                                                    <div class="col-md-7">
                                                                        <select name="business_brand_category_name"
                                                                                id="business_brand_category_name"
                                                                                class="form-control fsc-input">
                                                                            <option value="">---Select Business Brand
                                                                                Category Name---
                                                                            </option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-md-3 pull-right">
                                                                        <div id="image3"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div>

                                                            <div class="naicss col-lg-12 col-md-12 col-sm-12 col-xs-12 <?php echo e($errors->has('zip') ? 'has-error' : ''); ?> <?php echo e($errors->has('stateId') ? 'has-error' : ''); ?> <?php echo e($errors->has('city') ? 'has-error' : ''); ?> "
                                                                 style="padding:0px;">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">NAICS / SIC
                                                                        Code: </label>
                                                                    <div class="col-lg-8 col-md-9 col-sm-12 col-xs-12 fsc-element-margin">
                                                                        <div class="row">
                                                                            <div class="col-md-4 col-sm-5 col-xs-6">
                                                                                <input type="text"
                                                                                       class="form-control naics fsc-input"
                                                                                       id="" name="" placeholder="NAICS"
                                                                                       value="" readonly>
                                                                                <?php if($errors->has('city')): ?>
                                                                                    <span class="help-block">
               <strong><?php echo e($errors->first('city')); ?></strong>
               </span>
                                                                                <?php endif; ?>
                                                                            </div>

                                                                            <div class="col-md-2 col-sm-5 col-xs-6">
                                                                                <div class="dropdown"
                                                                                     style="margin-top: 1%;">
                                                                                    <input type="text"
                                                                                           class="form-control fsc-input sic"
                                                                                           placeholder="SIC" id=""
                                                                                           name="" value="" readonly>

                                                                                    <?php if($errors->has('stateId')): ?>
                                                                                        <span class="help-block">
               <strong><?php echo e($errors->first('stateId')); ?></strong>
               </span>
                                                                                    <?php endif; ?>
                                                                                </div>
                                                                            </div>

                                                                            <!-- <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
               <input type="text" class="form-control fsc-input" placeholder="Search" id="" name="" value="">
           </div>
           !-->
                                                                            <div class="col-md-3 col-sm-2 col-xs-12 fsc-element-margin mt_resp_go">
                                                                                <a href="https://www.naics.com/code-search/"
                                                                                   target="_blank"
                                                                                   class="add-row btn btn-primary"
                                                                                   style="padding: 10px 12px;">Go to
                                                                                    Link</a>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>

                                                        <div class="form-group <?php echo e($errors->has('company_name') ? ' has-error' : ''); ?>"
                                                             style="display:none">
                                                            <label class="control-label col-md-3">Management Co.</label>
                                                            <div class="col-lg-8 col-md-9 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <div class="row">
                                                                    <div class="col-md-9">
                                                                        <input type="text"
                                                                               class="form-control fsc-input"
                                                                               placeholder="Please Enter Your Company Name"
                                                                               name="company_name1" id="company_name1"
                                                                               value="">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="removecom">

                                                            <div class="form-group <?php echo e($errors->has('company_name') ? ' has-error' : ''); ?>">
                                                                <label class="control-label col-md-3">Company Name :
                                                                    <span class="star-required">*</span> <span
                                                                            class="tess">(Legal Name)</span></label>
                                                                <div class="col-lg-8 col-md-9 col-sm-12 col-xs-12 fsc-element-margin">
                                                                    <div class="row">
                                                                        <div class="col-md-9">
                                                                            <input type="text"
                                                                                   class="form-control fsc-input"
                                                                                   placeholder="Please Enter Your Company Name"
                                                                                   name="company_name" id="company_name"
                                                                                   value="">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group <?php echo e($errors->has('business_name') ? ' has-error' : ''); ?>">
                                                                <label class="control-label col-md-3">Business Name :
                                                                    <span class="tess">(DBA Name)</span></label>
                                                                <div class="col-lg-8 col-md-9 col-sm-12 col-xs-12 fsc-element-margin">
                                                                    <div class="row">
                                                                        <div class="col-md-9">
                                                                            <input type="text"
                                                                                   class="form-control fsc-input"
                                                                                   name="business_name"
                                                                                   placeholder="Please Enter Your Business Name"
                                                                                   id="business_name" value="">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="addcom" style="display:none;">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Associate Client
                                                                    ID : </label>
                                                                <div class="col-lg-8 col-md-9 col-sm-12 col-xs-12 fsc-element-margin">
                                                                    <div class="row">
                                                                        <div class="col-md-4">
                                                                            <input type="text"
                                                                                   class="form-control fsc-input"
                                                                                   placeholder="Associate Client ID"
                                                                                   name="CL" id="CL">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Maritial Status
                                                                    :</label>
                                                                <div class="col-lg-8 col-md-9 col-sm-12 col-xs-12 fsc-element-margin">
                                                                    <div class="row">
                                                                        <div class="col-md-9">
                                                                            <select class="form-control maritial"
                                                                                    name="other_maritial_status">
                                                                                <option value="">Select</option>
                                                                                <option value="Married">Married</option>
                                                                                <option value="Single">Single</option>
                                                                                <option value="Widowed">Widowed</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="form-group showmaritial" style="display:none">

                                                                <label class="control-label col-md-3">Spouse Name
                                                                    : </label>

                                                                <div class="col-lg-8 col-md-9 col-sm-12 col-xs-12 fsc-element-margin">
                                                                    <div class="col-lg-2 col-md-1 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin"
                                                                         style="width:10% !important;">
                                                                        <select class="form-control fsc-input"
                                                                                id="personalname" name="personalname">
                                                                            <option value="mr">Mr.</option>
                                                                            <option value="mrs">Mrs.</option>
                                                                            <option value="miss">Miss.</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin"
                                                                         style="width:15% !important;">
                                                                        <input type="text"
                                                                               class="form-control  fsc-input textonly"
                                                                               id="maritial_first_name"
                                                                               name="maritial_first_name"
                                                                               placeholder="First Name">
                                                                    </div>
                                                                    <div class="col-lg-1 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                        <input type="text"
                                                                               class="form-control fsc-input textonly"
                                                                               id="maritial_middle_name" maxlength="1"
                                                                               name="maritial_middle_name"
                                                                               placeholder="M">
                                                                    </div>
                                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin"
                                                                         style="width:16.6% !important;">
                                                                        <input type="text"
                                                                               class="form-control fsc-input textonly"
                                                                               id="maritial_last_name"
                                                                               name="maritial_last_name"
                                                                               placeholder="Last name">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group <?php echo e($errors->has('address') ? 'has-error' : ''); ?>">
                                                            <label class="control-label col-md-3"><span class="bus">Business</span>
                                                                Address 1: <span class="star-required">*</span></label>
                                                            <div class="col-lg-8 col-md-9 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <div class="row">
                                                                    <div class="col-md-9">
                                                                        <input type="text"
                                                                               class="form-control fsc-input"
                                                                               placeholder="Please Enter Your  Address 1"
                                                                               name="address" id="address">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group <?php echo e($errors->has('address1') ? 'has-error' : ''); ?>">
                                                            <label class="control-label col-md-3"><span class="bus">Business</span>
                                                                Address 2: </label>
                                                            <div class="col-lg-8 col-md-9 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <div class="row">
                                                                    <div class="col-md-9">
                                                                        <input type="text"
                                                                               class="form-control fsc-input"
                                                                               placeholder="Please Enter Your  Address 2"
                                                                               name="address1" id="address1">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group <?php echo e($errors->has('countryId') ? 'has-error' : ''); ?>">
                                                            <label class="control-label col-md-3">Country : <span
                                                                        class="star-required">*</span></label>
                                                            <div class="col-lg-8 col-md-9 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <div class="row">
                                                                    <div class="col-md-3">
                                                                        <div class="dropdown">
                                                                            <select name="countryId"
                                                                                    id="countries_states1"
                                                                                    data-country="USA"
                                                                                    class="form-control bfh-countries fsc-input">
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group <?php echo e($errors->has('zip') ? 'has-error' : ''); ?> <?php echo e($errors->has('stateId') ? 'has-error' : ''); ?> <?php echo e($errors->has('city') ? 'has-error' : ''); ?>">
                                                            <label class="control-label col-md-3">City / State / Zip :
                                                                <span class="star-required">*</span></label>
                                                            <div class="col-lg-8 col-md-9 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <div class="row">
                                                                    <div class="col-md-3">
                                                                        <input type="text"
                                                                               class="form-control fsc-input" id="city"
                                                                               name="city" placeholder="City" value="">
                                                                    </div>
                                                                    <div class="col-md-3 col-xs-6">
                                                                        <div class="dropdown" style="margin-top: 1%;">
                                                                            <select name="stateId" id="stateId"
                                                                                    class="form-control fsc-input bfh-states"
                                                                                    data-country="countries_states1">
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-xs-6">
                                                                        <input type="text"
                                                                               class="form-control fsc-input zip"
                                                                               id="zip" name="zip" maxlength="6"
                                                                               placeholder="Zip">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"></label>
                                                            <div class="col-md-6">
                                                                <input type="checkbox" value="1" name="billingtoo"
                                                                       id="billingtoo" onclick="FillBilling(this.form)">
                                                                <label for="billingtoo" class="">Mailing address Same as
                                                                    above <span class="bus">Business</SPAN></label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group <?php echo e($errors->has('second_address') ? 'has-error' : ''); ?>">
                                                            <label class="control-label col-md-3">Mailing Address 1 :
                                                                <span class="star-required">*</span></label>
                                                            <div class="col-lg-8 col-md-9 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <div class="row">
                                                                    <div class="col-md-9">
                                                                        <input type="text"
                                                                               class="form-control fsc-input"
                                                                               id="second_address" name="second_address"
                                                                               placeholder="Mailing Address 1">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group <?php echo e($errors->has('second_address1') ? 'has-error' : ''); ?>">
                                                            <label class="control-label col-md-3">Mailing Address 2
                                                                : </label>
                                                            <div class="col-lg-8 col-md-9 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <div class="row">
                                                                    <div class="col-md-9">
                                                                        <input type="text"
                                                                               class="form-control fsc-input"
                                                                               id="second_address1"
                                                                               name="second_address1"
                                                                               placeholder="Mailing Address 2">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group <?php echo e($errors->has('second_city') ? 'has-error' : ''); ?> <?php echo e($errors->has('second_state') ? 'has-error' : ''); ?><?php echo e($errors->has('second_zip') ? 'has-error' : ''); ?>">
                                                            <label class="control-label col-md-3">City / State / Zip :
                                                                <span class="star-required">*</span></label>
                                                            <div class="col-lg-8 col-md-9 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <div class="row">
                                                                    <div class="col-md-3">
                                                                        <input type="text"
                                                                               class="form-control fsc-input"
                                                                               id="second_city" name="second_city"
                                                                               placeholder="City">
                                                                    </div>
                                                                    <div class="col-md-3 col-xs-6">
                                                                        <div class="dropdown" style="margin-top: 1%;">
                                                                            <select name="second_state"
                                                                                    id="second_state"
                                                                                    class="form-control fsc-input bfh-states"
                                                                                    data-country="countries_states1">
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-xs-6">
                                                                        <input type="text"
                                                                               class="form-control fsc-input zip"
                                                                               id="second_zip" name="second_zip"
                                                                               maxlength="6" placeholder="Zip">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group <?php echo e($errors->has('email') ? 'has-error' : ''); ?>">
                                                            <label class="control-label col-md-3"><span class="bus">Business</span>
                                                                Email : <span class="star-required">*</span></label>
                                                            <div class="col-lg-8 col-md-9 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <input type="email"
                                                                               class="form-control fsc-input" id="email"
                                                                               name='email' placeholder="Email ID"
                                                                               value="">
                                                                    </div>
                                                                    <span style="margin-top:8px; float: left;">(Primary Email)</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Telephone No 1. :
                                                                <span class="star-required">*</span></label>
                                                            <div class="col-lg-8 col-md-9 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <div class="row">
                                                                    <div class="col-md-3">
                                                                        <input name="business_no" value="" type="tel"
                                                                               id="motelephoneile"
                                                                               class="form-control bfh-phone"
                                                                               data-country="countries_states1"
                                                                               data-format="  (999) 999-9999"
                                                                               placeholder="(999) 999-9999">
                                                                    </div>
                                                                    <div class="col-md-3 col-xs-6">
                                                                        <select name="businesstype"
                                                                                id="telephoneNo1Type"
                                                                                class="form-control fsc-input">
                                                                            <option value="">Select</option>
                                                                            <option value="Business">Business</option>
                                                                            <option value="Office">Office</option>
                                                                            <option value="Resid">Res.</option>
                                                                            <option value="Mobile">Mobile</option>
                                                                        </select>
                                                                    </div>

                                                                    <div class="col-md-3 hideyourself col-xs-6">
                                                                        <input class="form-control fsc-input" id="ext1"
                                                                               maxlength="5" readonly name="businessext"
                                                                               value="" placeholder="Ext" type="text">
                                                                    </div>

                                                                    <div class="col-md-3 yourself"
                                                                         style="display:none;">
                                                                        <select class="form-control telephone1guardian"
                                                                                name="telephone1guardian">
                                                                            <option value="">Select</option>
                                                                            <option value="Himself">Himself</option>
                                                                            <option value="Herself">Herself</option>
                                                                            <option value="Spouse">Spouse</option>
                                                                            <option value="Father">Father</option>
                                                                            <option value="Mother">Mother</option>
                                                                            <option value="Daughter">Daughter</option>
                                                                            <option value="Son">Son</option>
                                                                            <option value="Uncle">Uncle</option>
                                                                            <option value="Aunty">Aunty</option>
                                                                            <option value="Other">Other</option>
                                                                        </select>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Telephone No 2.
                                                                :</label>
                                                            <div class="col-lg-8 col-md-9 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <div class="row">
                                                                    <div class="col-md-3">
                                                                        <input name="motelephoneile1" type="tel"
                                                                               id="motelephoneile1"
                                                                               class="form-control bfh-phone"
                                                                               data-country="countries_states1"
                                                                               data-format="  (999) 999-9999"
                                                                               placeholder="(999) 999-9999">
                                                                    </div>
                                                                    <div class="col-md-3 col-xs-6">
                                                                        <select name="telephoneNo2Type"
                                                                                id="telephoneNo2Type"
                                                                                class="form-control fsc-input">
                                                                            <option value="">Select</option>
                                                                            <option value="Business">Business</option>
                                                                            <option value="Office">Office</option>
                                                                            <option value="Resid">Res.</option>
                                                                            <option value="Mobile">Mobile</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-md-3 col-xs-6 hideyourself1">
                                                                        <input class="form-control fsc-input" id="ext2"
                                                                               maxlength="5" readonly name="ext2"
                                                                               placeholder="Ext" type="text">
                                                                    </div>

                                                                    <div class="col-md-3 yourself1"
                                                                         style="display:none;">

                                                                        <select class="form-control telephone2guardian"
                                                                                name="telephone2guardian">
                                                                            <option value="">Select</option>
                                                                            <option value="Himself">Himself</option>
                                                                            <option value="Herself">Herself</option>
                                                                            <option value="Spouse">Spouse</option>
                                                                            <option value="Father">Father</option>
                                                                            <option value="Mother">Mother</option>
                                                                            <option value="Daughter">Daughter</option>
                                                                            <option value="Son">Son</option>
                                                                            <option value="Uncle">Uncle</option>
                                                                            <option value="Aunty">Aunty</option>
                                                                            <option value="Other">Other</option>


                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group <?php echo e($errors->has('business_fax') ? 'has-error' : ''); ?>">
                                                            <label class="control-label col-md-3">Fax # : </label>
                                                            <div class="col-lg-8 col-md-9 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <div class="row">
                                                                    <div class="col-md-3">
                                                                        <input type="text"
                                                                               class="form-control fsc-input bfh-phone"
                                                                               data-country="countries_states1"
                                                                               data-format="  (999) 999-9999" value=""
                                                                               id="business_fax" name="business_fax"
                                                                               placeholder="(999) 999-9999">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group <?php echo e($errors->has('website') ? 'has-error' : ''); ?>">
                                                            <label class="control-label col-md-3">Website : </label>
                                                            <div class="col-lg-8 col-md-9 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <div class="row">
                                                                    <div class="col-md-9">
                                                                        <input type="text"
                                                                               class="form-control fsc-input"
                                                                               id="website" value="" name="website"
                                                                               placeholder="Website address">
                                                                        <div class="text-right"><b>Note : </b>[ http://or https:// required ]
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="nextssss">
                                                        <ul class="pager wizard">
                                                            <li class="next last" style="display:none;"><a href="#" class="btn btn-primary">Last</a>
                                                            </li>
                                                            <li class="next previous2" style="display:none"><a href="#">Next</a>
                                                            </li>
                                                        </ul>
                                                    </div>

                                                </div>
                                                <div class="tab-pane fade" id="tab2primary" data-step="2">
                                                    <div class="Branch">
                                                        <h1>Contact Information</h1>
                                                    </div>

                                                    <div class="col-md-12 col-sm-12 col-xs-12">

                                                        <div class="form-group personaldata"
                                                             style="margin-bottom:0px;display:none;">
                                                            <label class="control-label col-md-3"></label>
                                                            <div class="col-md-3">
                                                                <input type="checkbox" value="1" id="faxbli3name"
                                                                       name="faxbli3name"
                                                                       onclick="faxbli233name(this.form)"><label
                                                                        for="faxbli3name"> Same As Personal Name</label>
                                                            </div>
                                                        </div>

                                                        <div class="form-group <?php echo e($errors->has('firstname') ? ' has-error' : ''); ?>">
                                                            <label class="control-label col-md-3">Name :</label>
                                                            <div class="col-lg-6 col-md-9 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <div class="row">
                                                                    <div class="col-md-2 col-xs-4 <?php echo e($errors->has('middlename') ? ' has-error' : ''); ?>">
                                                                        <select type="text" class="form-control txtOnly"
                                                                                id="minss" name="minss">
                                                                            <option Value="Mr.">Mr.</option>
                                                                            <option Value="Mrs.">Mrs.</option>
                                                                            <option Value="Miss.">Miss.</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-md-4 col-xs-8">
                                                                        <input type="text" class="form-control"
                                                                               placeholder="First Name" id="firstname"
                                                                               name="firstname" value="">
                                                                    </div>
                                                                    <div class="col-md-2 col-xs-4 <?php echo e($errors->has('middlename') ? ' has-error' : ''); ?>">
                                                                        <input type="text" class="form-control"
                                                                               id="middlename" placeholder="M"
                                                                               name="middlename" maxlength="1" value="">
                                                                    </div>
                                                                    <div class="col-md-4 col-xs-8 <?php echo e($errors->has('lastname') ? ' has-error' : ''); ?>">
                                                                        <input type="text" class="form-control"
                                                                               id="lastname" name="lastname"
                                                                               placeholder="Last Name" value="">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group" style="margin-bottom:0px;">
                                                            <label class="control-label col-md-3"></label>
                                                            <div class="col-lg-6 col-md-9 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <input type="checkbox" value="1" id="faxbli3"
                                                                       name="faxbli3"
                                                                       onclick="faxbli233(this.form)"><label
                                                                        for="faxbli3"> Same As Business Address</label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group <?php echo e($errors->has('contact_address1') ? ' has-error' : ''); ?>">
                                                            <label class="control-label col-md-3">Address 1 :</label>
                                                            <div class="col-lg-6 col-md-9 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <input type="text" class="form-control"
                                                                       id="contact_address1" name="contact_address1"
                                                                       placeholder="Address" value="">
                                                            </div>
                                                        </div>
                                                        <div class="form-group <?php echo e($errors->has('contact_address2') ? ' has-error' : ''); ?>">
                                                            <label class="control-label col-md-3">Address 2 :</label>
                                                            <div class="col-lg-6 col-md-9 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <input type="text" class="form-control"
                                                                       id="contact_address2" name="contact_address2"
                                                                       value="" placeholder="Address">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">City / State / Zip
                                                                :</label>
                                                            <div class="col-lg-6 col-md-9 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <input name="city_1" value="" placeholder="City"
                                                                               type="text" id="city_1"
                                                                               class="form-control"/>
                                                                    </div>
                                                                    <div class="">
                                                                        <div class="col-md-4 col-xs-6">
                                                                            <select name="state_1" id="state_1"
                                                                                    class="form-control fsc-input bfh-states"
                                                                                    data-country="countries_states1">
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="">
                                                                        <div class="col-md-4 col-xs-6">
                                                                            <input name="zip_1" type="text" id="zip_1"
                                                                                   class="form-control zip"
                                                                                   placeholder="Zip"/>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group" style="margin-bottom:0px;">
                                                            <label class="control-label col-md-3"></label>
                                                            <div class="col-md-9">
                                                                <input type="checkbox" value="1" id="faxbli3_g"
                                                                       name="faxbli3_g"
                                                                       onclick="faxbli233_g(this.form)"><label
                                                                        for="faxbli3_g"> Same As Basic Telephone</label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Telephone No. 1
                                                                :</label>
                                                            <div class="col-lg-6 col-md-9 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <input class="form-control" name="etelephone1" id="etelephone152"
                                                                               placeholder="(999) 999-9999" type="text"/>
                                                                    </div>
                                                                    <div class="col-md-4 col-xs-6">
                                                                        <select name="mobiletype_1" id="mobiletype_1"
                                                                                class="form-control fsc-input">
                                                                            <option value="">Select</option>
                                                                            <option value="Business">Business</option>
                                                                            <option value="Office">Office</option>
                                                                            <option value="Mobile">Mobile</option>
                                                                            <option value="Other">Other</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-md-4 col-xs-6">
                                                                        <input class="form-control fsc-input"
                                                                               id="ext2_1" readonly
                                                                               name="eext1" placeholder="Ext"
                                                                               type="text">

                                                                        <div class="showguardian" style="display:none;">
                                                                            <select name="guardian" id="guardian"
                                                                                    class="form-control fsc-input">
                                                                                <option value="">Select</option>
                                                                                <option value="Himself">Himself</option>
                                                                                <option value="Herself">Herself</option>
                                                                                <option value="Spouse">Spouse</option>
                                                                                <option value="Father">Father</option>
                                                                                <option value="Mother">Mother</option>
                                                                                <option value="Daughter">Daughter</option>
                                                                                <option value="Son">Son</option>
                                                                                <option value="Uncle">Uncle</option>
                                                                                <option value="Aunty">Aunty</option>
                                                                                <option value="Other">Other</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="form-group <?php echo e($errors->has('mobile1') ? ' has-error' : ''); ?>">
                                                            <label class="control-label col-md-3">Telephone No. 2
                                                                :</label>
                                                            <div class="col-lg-6 col-md-9 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <input name="mobile_2"
                                                                               placeholder="(999) 999-9999" type="tel"
                                                                               id="mobile_2"
                                                                               class="form-control bfh-phone"
                                                                               data-country="countries_states1"
                                                                               data-format="  (999) 999-9999"/>
                                                                    </div>
                                                                    <div class="col-md-4 col-xs-6">
                                                                        <select name="mobiletype_2" id="mobiletype_2" class="form-control fsc-input">
                                                                            <option value="">Select</option>
                                                                            <option value="Business">Business</option>
                                                                            <option value="Office">Office</option>
                                                                            <option value="Resid">Res.</option>
                                                                            <option value="Mobile">Mobile</option>
                                                                        </select>
                                                                        <?php if($errors->has('mobiletype1')): ?>
                                                                            <span class="help-block"><strong><?php echo e($errors->first('mobiletype1')); ?></strong></span>
                                                                        <?php endif; ?>
                                                                    </div>
                                                                    <div class="col-md-4 col-xs-6">
                                                                        <input class="form-control fsc-input" id="ext2_2" maxlength="5" name="ext2_2" readonly placeholder="Ext" type="text">
                                                                        <div class="showguardian2" style="display:none;">
                                                                            <select name="guardian2" id="guardian2" class="form-control fsc-input">
                                                                                <option value="">Select</option>
                                                                                <option value="Himself">Himself</option>
                                                                                <option value="Herself">Herself</option>
                                                                                <option value="Spouse">Spouse</option>
                                                                                <option value="Father">Father</option>
                                                                                <option value="Mother">Mother</option>
                                                                                <option value="Daughter">Daughter</option>
                                                                                <option value="Son">Son</option>
                                                                                <option value="Uncle">Uncle</option>
                                                                                <option value="Aunty">Aunty</option>
                                                                                <option value="Other">Other</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="form-group" style="margin-bottom:0px;">
                                                            <label class="control-label col-md-3"></label>
                                                            <div class="col-md-9">
                                                                <input type="checkbox" value="1" id="faxbli2"
                                                                       name="faxbli2"
                                                                       onclick="faxbli12(this.form)"><label
                                                                        for="faxbli2"> Same As Business Fax</label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group <?php echo e($errors->has('contact_fax') ? ' has-error' : ''); ?>">
                                                            <label class="control-label col-md-3">Fax No. :</label>
                                                            <div class="col-lg-6 col-md-9 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <div class="row">
                                                                    <div class="col-md-4 col-xs-6">
                                                                        <input name="contact_fax_1"
                                                                               placeholder="(999) 999-9999" type="tel"
                                                                               id="contact_fax"
                                                                               class="form-control bfh-phone"
                                                                               data-country="countries_states1"
                                                                               data-format="  (999) 999-9999"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group" style="margin-bottom:0px;">
                                                            <label class="control-label col-md-3"></label>
                                                            <div class="col-lg-6 col-md-9 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <input type="checkbox" value="1" id="emailbli2"
                                                                       name="emailbli2" onclick="emailbl12(this.form)">
                                                                <label style="margin-left: 0px;" for="emailbli2">Same As
                                                                    Business Email</label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  <?php echo e($errors->has('email_1') ? ' has-error' : ''); ?>">
                                                            <label class="control-label col-md-3">Email :</label>
                                                            <div class="col-md-6">
                                                                <input type="text" class="form-control" id="email_1"
                                                                       name="email_1" placeholder="Email ID">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <ul class="pager wizard">
                                                        <li class="previous first" style="display:none;"><a href="#"
                                                                                                            class="btn btn-primary">First</a>
                                                        </li>
                                                        <li class="previous previous1"><a href="#"
                                                                                          class="nex1">Previous</a></li>
                                                        <li class="next last" style="display:none;"><a href="#"
                                                                                                       class="btn btn-primary">Last</a>
                                                        </li>
                                                        <li class="next previous2"><a href="#" class="nex11">Next</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="tab-pane fade" id="tab3primary">
                                                    <!--<div class="">-->
                                                    <!--   <div class="form-group bg-color">-->
                                                    <!--      <label class="control-label col-md-3" style="width: 20%;">Price Selection :</label>-->
                                                    <!--      <div class="col-md-2">-->
                                                    <!--         <select name="pricetype" type="text" id="pricetype" class="form-control">-->
                                                    <!--            <option value="">Select</option>-->
                                                    <!--            <option value="Regular">Regular</option>-->
                                                    <!--            <option value="Combo">Combo</option>-->
                                                    <!--         </select>-->
                                                    <!--      </div>-->
                                                    <!--      <label class="control-label col-md-1" style="width: 106px;">Currency :</label>-->
                                                    <!--      <div class="col-md-2">-->
                                                    <!--         <select name="currency" type="text" id="currency" class="form-control">-->
                                                    <!--            <option value="">Select</option>-->
                                                <!--            <?php $__currentLoopData = $currency; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cur): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>-->
                                                <!--            <option value="<?php echo e($cur->id); ?>"><?php echo e($cur->currency.' '.$cur->sign); ?></option>-->
                                                    <!--            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>-->
                                                    <!--         </select>-->
                                                    <!--      </div>-->
                                                    <!--      <label class="control-label col-md-1" style="width: 142px;">Service Period :</label>-->
                                                    <!--      <div class="col-md-2">-->
                                                    <!--         <select name="serviceperiod" type="text" id="serviceperiod" class="form-control">-->
                                                    <!--            <option value="">Select</option>-->
                                                <!--            <?php $__currentLoopData = $period; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $period1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>-->
                                                <!--            <option value="<?php echo e($period1->id); ?>"><?php echo e($period1->period); ?></option>-->
                                                    <!--            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>-->
                                                    <!--         </select>-->
                                                    <!--      </div>-->
                                                    <!--   </div>-->
                                                    <!--   <div class="form-group service" style="background-color:#fff5dd;padding:15px;width: 100%;margin: auto;display:none">-->
                                                    <!--      <div class="row">-->
                                                    <!--          <div class="col-md-3">-->
                                                    <!--             <label class="control-label">Type of Service :</label>-->
                                                    <!--             <select name="typeofservice[]" type="text" id="typeofservice" class="form-control typeofservice">-->
                                                    <!--                <option value="">Select</option>-->
                                                <!--                <?php $__currentLoopData = $typeofser; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $typeofser1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>-->
                                                <!--                <option value="<?php echo e($typeofser1->id); ?>"><?php echo e($typeofser1->typeofservice); ?></option>-->
                                                    <!--                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>-->
                                                    <!--             </select>-->
                                                    <!--          </div>-->
                                                    <!--          <div id="regular">-->
                                                    <!--             <div class="col-md-4">-->
                                                    <!--                <label class="control-label">Service Includes :</label>-->
                                                    <!---     <input name="serviceincludes[]" type="text" id="serviceincludes" class="form-control"> --->
                                                    <!--                <textarea class="form-control" name="serviceincludes[]" id="serviceincludes" rows="3" id="comment"></textarea>-->
                                                    <!--             </div>-->
                                                    <!--             <div class="col-md-2" style="width:13%;text-align:center;">-->
                                                    <!--                <label class="control-label">Regular Price :</label>-->
                                                    <!--                <input name="regularprice[]" type="text" id="regularprice" placeholder="Regular Price"  readonly class="regularprice form-control">-->
                                                    <!--             </div>-->
                                                    <!--             <div class="col-md-2" style="width:13%;text-align:center;">-->
                                                    <!--                <label class="control-label" style="">Combo Price :</label>-->
                                                    <!--                <input name="comboprice[]" type="text" id="comboprice" placeholder="Combo Price" readonly class="comboprice form-control">-->
                                                    <!--             </div>-->
                                                    <!--             <input name="ckq" type="hidden" id="ckq" class="ckq">-->
                                                    <!--             <div class="col-md-2" style="width:13%;text-align:center;">-->
                                                    <!--                <label class="control-label">Price :</label>-->
                                                    <!--                <input name="price[]" type="text" id="price" placeholder="Price" class="form-control">-->
                                                    <!--             </div>-->
                                                    <!--          </div>-->
                                                    <!--      </div>-->
                                                    <!--      <div class="row">-->
                                                    <!--          <div class="col-md-3">-->
                                                    <!--             <label class="control-label">Service Start Month :</label>-->
                                                    <!--             <select name="]" type="text" id="" class="form-control ">-->
                                                    <!--                <option value="">Select</option>-->
                                                    <!--                <option value="">January</option>-->
                                                    <!--                <option value="">February</option>-->
                                                    <!--                <option value="">March</option>-->
                                                    <!--                <option value="">April</option>-->
                                                    <!--                <option value="">May</option>-->
                                                    <!--                <option value="">June</option>-->
                                                    <!--                <option value="">July</option>-->
                                                    <!--                <option value="">August</option>-->
                                                    <!--                <option value="">September</option>-->
                                                    <!--                <option value="">October</option>-->
                                                    <!--                <option value="">November</option>-->
                                                    <!--                <option value="">December</option>-->
                                                    <!--             </select>-->
                                                    <!--          </div>-->
                                                    <!--          <div class="col-md-3">-->
                                                    <!--             <label class="control-label">Service Start Year :</label>-->
                                                    <!--             <select name="]" type="text" id="" class="form-control ">-->
                                                    <!--                <option value="">Select</option>-->
                                                    <!--                <option value="">2020</option>-->
                                                    <!--                <option value="">2021</option>-->

                                                    <!--             </select>-->
                                                    <!--          </div>-->
                                                    <!--      </div>-->
                                                    <!--   </div>-->
                                                    <!--   <div class="input_fields_wrap_notes" style="display:none">-->
                                                    <!--      <br>-->
                                                    <!--      <br>-->
                                                    <!--      <table class="table" id="table">-->
                                                    <!--         <thead>-->
                                                    <!--            <tr style="background:#00a0e3;color:#fff">-->
                                                    <!--               <th scope="col">#</th>-->
                                                    <!--               <th scope="col">Title</th>-->
                                                    <!--               <th scope="col">Regular Price</th>-->
                                                    <!--               <th scope="col">Combo Price</th>-->
                                                    <!--               <th scope="col">Action</th>-->
                                                    <!--            </tr>-->
                                                    <!--         </thead>-->
                                                    <!--         <tbody>-->
                                                    <!--            <tr>-->
                                                    <!--               <td><input type="hidden" name="taxid[]"></td>-->
                                                    <!--               <td>-->
                                                    <!--                  <select class="form-control pricetype" name="titles[]" id="titles">-->
                                                    <!--                     <option value="">---Select Service---</option>-->
                                                <!--                     <?php $__currentLoopData = $taxtitle; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ti): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>-->
                                                <!--                     <option value="<?php echo e($ti->title); ?>"><?php echo e($ti->title); ?></option>-->
                                                    <!--                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>-->
                                                    <!--                  </select>-->
                                                    <!--               </td>-->
                                                    <!--               <td><input type="text" placeholder="Regular Price" class="regularprice1 form-control" name="regularprice1[]"></td>-->
                                                    <!--               <td><input type="text" class=" form-control comboprice1" placeholder="Combo Price" name="comboprice1[]"></td>-->
                                                    <!--               <td><a class="btn btn-primary" onclick="education_field_note();"> Add</a></td>-->
                                                    <!--            </tr>-->
                                                    <!--         </tbody>-->
                                                    <!--      </table>-->
                                                    <!--   </div>-->
                                                    <!--</div>-->
                                                    <!--<div class="education_service" id="education_service"></div>-->
                                                    <!--<div class="col-md-12 service" style="background-color:#eeeeed;padding: 15px 0px 70px 0px;width: 100%;display:none;">-->
                                                    <!--   <div class="">-->
                                                    <!--      <div class="col-md-3"></div>-->
                                                    <!--      <div class="col-md-4">-->
                                                    <!--         <label>Note :</label> <input type="text" class="form-control" placeholder="Note"name="pricenote" >-->
                                                    <!--      </div>-->
                                                    <!--      <div class="col-md-1">-->
                                                    <!--      </div>-->
                                                    <!--      <div class="col-md-2">-->
                                                    <!--         <label>Discount :</label> <input type="text" style="text-align:right" class="form-control discountprice" placeholder="Discount Price" id="discountprice" name="discountprice">-->
                                                    <!--      </div>-->
                                                    <!--      <div class="col-md-2" style="width: 14%;">-->
                                                    <!--         <label>Total Price :</label> <input type="hidden" class="form-control totalprice1" id="totalpri" placeholder="Total Price" readonly name="totalprice">-->
                                                    <!--         <input type="text" class="form-control totalprice1" style="text-align:right" placeholder="Total Price" readonly name="totalprice1" id="totalprice1">-->
                                                    <!--      </div>-->
                                                    <!--   </div>-->
                                                    <!--   <div class="col-md-12"><br>-->
                                                    <!--      <a class="btn btn-primary pull-right" onclick="education_service();" style="margin-right: 28px;"> Add</a>-->
                                                    <!--   </div>-->
                                                    <!--</div>-->
                                                    <div class="clearfix">
                                                        <div class="form-group bg-color">
                                                            <label class="control-label col-lg-2 col-md-3"
                                                                   style="text-align:right;padding-right:0px;">Price
                                                                Selection :</label>
                                                            <div class="col-lg-2 col-md-3">
                                                                <div class="pricetype">
                                                                    <select name="pricetype" type="text" id="pricetype"
                                                                            class="form-control pricetype"
                                                                            style="background: lightgreen none repeat scroll 0% 0%;">
                                                                        <option value="1">Select</option>
                                                                        <option value="Regular">Regular</option>
                                                                        <option value="Combo">Combo</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <label class="control-label col-lg-2 col-md-3"
                                                                   style="padding-right:0px;">Currency :</label>
                                                            <div class="col-lg-2 col-md-3">
                                                                <div class="pricetype">
                                                                    <select name="currency" type="text" id="currency"
                                                                            class="form-control currency1"
                                                                            style="background: lightgreen none repeat scroll 0% 0%;">
                                                                        <option value="">Select</option>
                                                                        <option value="38">USA $</option>
                                                                        <option value="39">CAD $</option>
                                                                        <option value="40">INR ₹</option>
                                                                        <option value="41">UK $</option>
                                                                    </select>
                                                                </div>
                                                                <input name="ckq" type="hidden" id="ckq" class="ckq"
                                                                       value=" ">
                                                            </div>
                                                            <label class="control-label col-lg-2 col-md-3"
                                                                   style="padding-right:0px;">Service Period :</label>
                                                            <div class="col-lg-2 col-md-3">
                                                                <div class="pricetype">
                                                                    <select name="serviceperiod" type="text"
                                                                            id="serviceperiod"
                                                                            class="form-control serviceperiod"
                                                                            style="background: lightgreen none repeat scroll 0% 0%;">
                                                                        <option value="">Select</option>
                                                                        <option value="10">Monthly</option>
                                                                        <option value="4">Quarterly</option>
                                                                        <option value="5">Half-Year</option>
                                                                        <option value="7">Annually</option>
                                                                        <option value="9">One Time</option>
                                                                        <option value="11">Weekly Payroll Service
                                                                        </option>
                                                                        <option value="12">Bi-Weekly Payroll Service
                                                                        </option>
                                                                        <option value="14">Any payroll period</option>
                                                                        <option value="15">On Request</option>
                                                                        <option value="17">All</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <!--<div class="col-md-1"><a id="305" class="resetServiceInfo btn btn-danger">Reset</a></div>
           !--></div>
                                                        <div class="col-md-12">
                                                            <div class="form-group" style="margin-bottom: 0px;">
                                                                <div class="col-lg-7 cust_left">
                                                                    <div class="row">
                                                                        <div class="col-md-6 col-sm-4">
                                                                            <label class="control-label"
                                                                                   style="padding-left:0px !important;padding-right:0px !important;text-align:right !important;">Accounting
                                                                                Service :</label>
                                                                            <select class="form-control accounting_period">
                                                                                <option value="">Select</option>
                                                                                <option value="10-2">Monthly</option>
                                                                                <option value="4-2">Quarterly</option>
                                                                                <option value="5-2">Half-Yearly</option>
                                                                                <option value="7-2">Yearly</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-md-3 monthhide col-sm-4 p_991"
                                                                             style="padding-left:5px;">
                                                                            <label class="control-label"
                                                                                   style="padding-left:0px !important;padding-right:0px !important;text-align:right !important;">
                                                                                Month</label>
                                                                            <select class="form-control"
                                                                                    name="ac_service_month">
                                                                                <option value=""> Select</option>
                                                                                <option class="qtr" value="Jan">Jan
                                                                                </option>
                                                                                <option class="qtr" value="Feb">Feb
                                                                                </option>
                                                                                <option class="halfyear" value="Mar">
                                                                                    Mar
                                                                                </option>
                                                                                <option class="qtr" value="Apr">Apr
                                                                                </option>
                                                                                <option class="qtr" value="May">May
                                                                                </option>
                                                                                <option value="Jun">Jun</option>
                                                                                <option class="qtr" value="Jul">Jul
                                                                                </option>
                                                                                <option class="qtr" value="Aug">Aug
                                                                                </option>
                                                                                <option class="halfyear" value="Sep">
                                                                                    Sep
                                                                                </option>
                                                                                <option class="qtr" value="Oct">Oct
                                                                                </option>
                                                                                <option class="qtr" value="Nov">Nov
                                                                                </option>
                                                                                <option value="Dec">Dec</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-4 p_991"
                                                                             style="padding-left:0px;">
                                                                            <label class="control-label"
                                                                                   style="padding-left:0px !important;padding-right:0px !important;text-align:right !important;">
                                                                                Year</label>
                                                                            <select class="form-control"
                                                                                    name="ac_service_year">
                                                                                <option value=""> Select</option>
                                                                                <option value="2019">2019</option>
                                                                                <option value="2020">2020</option>
                                                                                <option value="2021">2021</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-5 cust_right">
                                                                    <div class="col-md-3 col-sm-6 p_991"
                                                                         style="padding-left:5px;">
                                                                        <label class="control-label"
                                                                               style="padding-left:0px !important;padding-right:0px !important;text-align:right !important;">End
                                                                            Month</label>
                                                                        <select class="form-control" name="">
                                                                            <option value=""> Select</option>
                                                                            <option value="Jan">Jan</option>
                                                                            <option value="Feb">Feb</option>
                                                                            <option value="Mar">Mar</option>
                                                                            <option value="Apr">Apr</option>
                                                                            <option value="May">May</option>
                                                                            <option value="Jun">Jun</option>
                                                                            <option value="Jul">Jul</option>
                                                                            <option value="Aug">Aug</option>
                                                                            <option value="Sep">Sep</option>
                                                                            <option value="Oct">Oct</option>
                                                                            <option value="Nov">Nov</option>
                                                                            <option value="Dec">Dec</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-6 p_991"
                                                                         style="padding-left:5px;">
                                                                        <label class="control-label"
                                                                               style="padding-left:0px !important;padding-right:0px !important;text-align:right !important;">End
                                                                            Year</label>
                                                                        <select class="form-control" name="">
                                                                            <option value=""> Select</option>
                                                                            <option>2021</option>
                                                                            <option>2022</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-md-6 col-sm-12 p_991"
                                                                         style="padding-left:5px;">
                                                                        <label class="control-label"
                                                                               style="padding-left:0px !important;padding-right:0px !important;text-align:right !important;">Reason</label>
                                                                        <textarea class="form-control"
                                                                                  rows="1"></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <hr style="margin:0px;border-top: 5px solid #868686;">
                                                            <div class="form-group" style="margin-bottom: 0px;">
                                                                <div class="col-lg-7 cust_left">
                                                                    <div id="customFieldsbo">
                                                                        <div class="form-group"
                                                                             style="margin-bottom:0px;">
                                                                            <div class="col-md-5 col-xs-12">
                                                                                <label class="control-label">Taxation
                                                                                    Service:</label>
                                                                                <select class="form-control tax_service_1 tax_servicee"
                                                                                        name="taxation_service[]">
                                                                                    <option value="">Select</option>
                                                                                    <option value="4">941 Reporting
                                                                                        Service
                                                                                    </option>
                                                                                    <option value="6">Business Income
                                                                                        Tax Return
                                                                                    </option>
                                                                                    <option value="3">COAM Reporting
                                                                                        Service
                                                                                    </option>
                                                                                    <option value="8">Estimate Advance
                                                                                        Income Tax
                                                                                    </option>
                                                                                    <option value="7">Individual Income
                                                                                        Tax Return
                                                                                    </option>
                                                                                    <option value="9">Personal Property
                                                                                        Tax
                                                                                    </option>
                                                                                    <option value="1">Sales Tax
                                                                                        Reporting Service
                                                                                    </option>
                                                                                    <option value="2">Tobacco (Excise)
                                                                                        Tax Rep.
                                                                                    </option>
                                                                                    <option value="5">W-2 Reporting
                                                                                        Service
                                                                                    </option>
                                                                                </select>
                                                                            </div>
                                                                            <div class="col-md-2 col-xs-4 p_991"
                                                                                 style="padding-left:0px;padding-right:10px;">
                                                                                <label class="control-label">
                                                                                    Period</label>
                                                                                <select class="form-control tax_period_1"
                                                                                        name="taxation_service_period[]">
                                                                                    <option value="">Select</option>
                                                                                    <option value="Monthly">Monthly
                                                                                    </option>
                                                                                    <option value="Quarterly">
                                                                                        Quarterly
                                                                                    </option>
                                                                                    <option value="Annually">Annually
                                                                                    </option>
                                                                                </select>
                                                                            </div>
                                                                            <div class="col-md-2 col-xs-4 p_991 hidemonths"
                                                                                 style="padding-left:0px;">
                                                                                <label class="control-label">
                                                                                    Month</label>
                                                                                <select class="form-control" name="tax_service_month">
                                                                                    <option value=""> Select</option>
                                                                                    <option class="qtr1" value="Jan">
                                                                                        Jan
                                                                                    </option>
                                                                                    <option class="qtr1" value="Feb">
                                                                                        Feb
                                                                                    </option>
                                                                                    <option value="Mar">Mar</option>
                                                                                    <option class="qtr1" value="Apr">
                                                                                        Apr
                                                                                    </option>
                                                                                    <option class="qtr1" value="May">
                                                                                        May
                                                                                    </option>
                                                                                    <option value="Jun">Jun</option>
                                                                                    <option class="qtr1" value="Jul">
                                                                                        Jul
                                                                                    </option>
                                                                                    <option class="qtr1" value="Aug">
                                                                                        Aug
                                                                                    </option>
                                                                                    <option value="Sep">Sep</option>
                                                                                    <option class="qtr1" value="Oct">
                                                                                        Oct
                                                                                    </option>
                                                                                    <option class="qtr1" value="Nov">
                                                                                        Nov
                                                                                    </option>
                                                                                    <option value="Dec">Dec</option>
                                                                                </select>
                                                                            </div>
                                                                            <div class="col-md-2 col-xs-4 p_991"
                                                                                 style="padding-left:0px;">
                                                                                <label class="control-label">
                                                                                    Year</label>
                                                                                <select class="form-control" name="tax_service_year">
                                                                                    <option value="">Select</option>
                                                                                    <option value="2019">2019</option>
                                                                                    <option value="2020">2020</option>
                                                                                    <option value="2021">2021</option>
                                                                                </select>
                                                                            </div>
                                                                            <div class="col-md-1 col-xs-12 text-right p_991 mt10_991" style="padding-left:0px;margin-top: 30px;">
                                                                                <a class="btn btn-primary addCFnew"><i class="fa fa-plus"></i></a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-5 cust_right">
                                                                    <div class="customyear">
                                                                        <div class="col-md-12">
                                                                        <div class="form-group">
                                                                        <div class="col-md-3 col-sm-12 p_991"style="padding-left:5px;">
                                                                            <label class="control-label" style="padding-left:0px !important;padding-right:0px !important;text-align:right !important;">Final Year</label>
                                                                            <select class="form-control" name="">
                                                                                <option value="">Select</option>
                                                                                <option value="">2019</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-md-9 col-sm-12 p_991" style="padding-left:5px;">
                                                                            <label class="control-label" style="padding-left:0px !important;padding-right:0px !important;text-align:right !important;">Reason</label>
                                                                            <textarea class="form-control" rows="1"></textarea>
                                                                        </div>
                                                                        </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <hr style="margin:0px;border-top: 5px solid #868686;">
                                                            <div class="form-group" style="margin-bottom: 0px;">
                                                                <div class="col-lg-7 cust_left">
                                                                    <div class="row">
                                                                        <div class="col-md-6 col-xs-12">
                                                                            <label class="control-label"
                                                                                   style="padding-left:0px !important;text-align:right !important;padding-right:0px !important;">Payroll
                                                                                Service:</label>
                                                                            <select class="form-control payroll_period"
                                                                                    name="payroll_period">
                                                                                <option value="">Select</option>
                                                                                <option value="">Regular With
                                                                                    Accounting
                                                                                </option>
                                                                                <option value="">Full Payroll Service
                                                                                </option>
                                                                                <option value="">Limited Payroll
                                                                                    Service
                                                                                </option>
                                                                                <option value="">Paystubs Only</option>
                                                                                <option value="">None</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-md-3 p_991 col-xs-6"
                                                                             style="padding-left:5px;">
                                                                            <label class="control-label"
                                                                                   style="padding-left:0px !important;padding-right:0px !important;text-align:right !important;">
                                                                                Month</label>
                                                                            <select class="form-control" name="">
                                                                                <option value=""> Select</option>
                                                                                <option value="Jan">Jan</option>
                                                                                <option value="Feb">Feb</option>
                                                                                <option value="Mar">Mar</option>
                                                                                <option value="Apr">Apr</option>
                                                                                <option value="May">May</option>
                                                                                <option value="Jun">Jun</option>
                                                                                <option value="Jul">Jul</option>
                                                                                <option value="Aug">Aug</option>
                                                                                <option value="Sep">Sep</option>
                                                                                <option value="Oct">Oct</option>
                                                                                <option value="Nov">Nov</option>
                                                                                <option value="Dec">Dec</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-md-3 p_991 col-xs-6"
                                                                             style="padding-left:0px;">
                                                                            <label class="control-label"
                                                                                   style="padding-left:0px !important;padding-right:0px !important;text-align:right !important;">
                                                                                Year</label>
                                                                            <select class="form-control"
                                                                                    name="payroll_service_year">
                                                                                <option value=""> Select</option>
                                                                                <option value="2019">2019</option>
                                                                                <option value="2020">2020</option>
                                                                                <option value="2021">2021</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-5 cust_right">

                                                                </div>
                                                            </div>
                                                            <hr style="margin:0px;border-top: 5px solid #868686;">
                                                            <div class="form-group" style="margin-bottom: 0px;">
                                                                <div class="col-lg-7 cust_left">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <label class="control-label"
                                                                                   style="padding-left:0px !important;text-align:right !important;padding-right:0px !important;">Other
                                                                                Service:</label>
                                                                            <div class="col-md-12" style="padding:0px;">
                                                                                <input type="text" name="other_period"
                                                                                       class="form-control" value="">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group" style="margin-bottom: 0px;">
                                                                <div class="col-lg-7 cust_left">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <label class="control-label"
                                                                                   style="padding-left:0px !important;text-align:right !important;padding-right:0px !important;">Note:</label>
                                                                            <div class="col-md-12" style="padding:0px;">
                                                                                <input type="text" name=""
                                                                                       class="form-control" value="">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="Branch" style="margin-top:20px;">
                                                                <h1>Pricing information</h1>
                                                            </div>
                                                        </div>
                                                        <div class="clear"></div>

                                                        <div class="customer-service-information-bg">


                                                            <div class="services2" style="display:none;"
                                                                 disabled="disabled">
                                                                <div class=" border">
                                                                    <div class="col-md-12" style="padding:0px;">
                                                                        <div class="col-md-3 pi_1">
                                                                            <label class="control-label">Type of Service
                                                                                :</label>
                                                                            <input type="text" class="form-control"
                                                                                   readonly value="Accounting Service">
                                                                            <input type="hidden" name="typeofservice[]"
                                                                                   value="2">
                                                                            <input type="hidden" name="acperiods1[]"
                                                                                   class="acperiods1">
                                                                            <input type="hidden" class="titletaxx1"
                                                                                   name="typeofservice1[]">


                                                                        </div>
                                                                        <div id="regular">
                                                                            <div class="col-md-4 pi_2"
                                                                                 style="width:39% ">
                                                                                <div class="row">
                                                                                    <label class="control-label">Service
                                                                                        Includes :</label>
                                                                                    <textarea readonly cols="50"
                                                                                              name="serviceincludes[]"
                                                                                              id="serviceincludes"
                                                                                              class="form-control serviceincludeacc"></textarea>
                                                                                    <input name="serviceid[]"
                                                                                           type="hidden" id="serviceid"
                                                                                           class="form-control">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-2 pi_3"
                                                                                 style="width:13%">
                                                                                <label class=" readonly control-label">Regular
                                                                                    Price :</label>
                                                                                <input name="regularprice[]" type="text"
                                                                                       id="regularprice"
                                                                                       style="text-align:right;font-size:small !important"
                                                                                       placeholder="Regular Price"
                                                                                       readonly
                                                                                       class="regularpriceacc form-control">
                                                                            </div>
                                                                            <div class="col-md-2 pi_4"
                                                                                 style="width:11%">
                                                                                <div class="row">
                                                                                    <label class="control-label">Combo
                                                                                        Price :</label>
                                                                                    <input name="comboprice[]"
                                                                                           type="text" id="comboprice"
                                                                                           style="text-align:right;font-size:small !important"
                                                                                           placeholder="Combo Price"
                                                                                           readonly
                                                                                           class="combopriceacc form-control">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-2 pi_5"
                                                                                 style="width:11%">
                                                                                <label class="control-label">Your Price
                                                                                    :</label>
                                                                                <input readonly name="price[]"
                                                                                       type="text" id="price"
                                                                                       style="text-align:right;font-size:small !important"
                                                                                       placeholder="Price"
                                                                                       class="form-control priceacc">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div style="clear:both;"></div>
                                                            </div>
                                                            <div style="clear:both;"></div>
                                                            <div class="services4" style="display:none;"
                                                                 disabled="disabled">
                                                                <div class="border">
                                                                    <div class="col-md-12">
                                                                        <div class="col-md-3">
                                                                            <label class="control-label">Type of Service
                                                                                :</label>
                                                                            <input type="text"
                                                                                   class="form-control taxtitletax"
                                                                                   name="taxtypeofservice[]" readonly>
                                                                            <input type="hidden"
                                                                                   class="form-control titletax"
                                                                                   name="titletax[]" id="taxtitle"
                                                                                   readonly>


                                                                        </div>
                                                                        <div id="regular">
                                                                            <div class="col-md-4"
                                                                                 style="width:39% !important">
                                                                                <div class="row">
                                                                                    <label class="control-label">Note
                                                                                        :</label>
                                                                                    <textarea readonly rows="2"
                                                                                              cols="50"
                                                                                              name="taxserviceincludes[]"
                                                                                              id="taxserviceincludes"
                                                                                              class="form-control taxserviceincludenote"></textarea>
                                                                                    <input name="serviceid[]"
                                                                                           type="hidden" id="serviceid"
                                                                                           class="form-control">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-2" style="width:13%">
                                                                                <label class=" readonly control-label">Regular
                                                                                    Price :</label>
                                                                                <input name="taxregularprice[]"
                                                                                       type="text" id="taxregularprice"
                                                                                       style="text-align:right;font-size:small !important"
                                                                                       placeholder="Regular Price"
                                                                                       readonly
                                                                                       class="taxregularpricetax form-control">
                                                                            </div>
                                                                            <div class="col-md-2" style="width:11%">
                                                                                <div class="row">
                                                                                    <label class="control-label">Combo
                                                                                        Price :</label>
                                                                                    <input name="taxcomboprice[]"
                                                                                           type="text"
                                                                                           id="taxcomboprice"
                                                                                           style="text-align:right;font-size:small !important"
                                                                                           placeholder="Combo Price"
                                                                                           readonly
                                                                                           class="taxcombopricetax form-control">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-2" style="width:11%">
                                                                                <label class="control-label">Your Price
                                                                                    :</label>
                                                                                <input readonly name="taxprice1[]"
                                                                                       type="text" id="taxprice1"
                                                                                       style="text-align:right;font-size:small !important"
                                                                                       placeholder="Price"
                                                                                       class="form-control taxpricetax">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="clear"></div>
                                                            </div>

                                                        </div>


                                                        <div>
                                                            <ul class="pager wizard">
                                                                <li class="previous first" style="display:none;"><a
                                                                            href="#" class="btn btn-primary">First</a>
                                                                </li>
                                                                <li class="previous previous1"><a href="#" class="nex1">Previous</a>
                                                                </li>
                                                                <li class="next last" style="display:none;"><a href="#"
                                                                                                               class="btn btn-primary">Last</a>
                                                                </li>
                                                                <li class="next previous2"><a href="#" class="nex11">Next </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="tab4primary" data-step="4">
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <div class="input_fields_wrap">
                                                            <input name="noteid[]" value="" type="hidden"
                                                                   placeholder="Last Name" id="noteid"
                                                                   class="form-control">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Note:</label>
                                                                <div class="col-md-6">
                                                                    <input name="notes[]" value="" type="text"
                                                                           placeholder="Create Note" id="notes"
                                                                           class="form-control">
                                                                </div>
                                                                <div class="col-md-1">
                                                                    <button class="btn btn-success" type="button"
                                                                            onclick="education_fields();"><i
                                                                                class="fa fa-plus"
                                                                                aria-hidden="true"></i></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div id="education_fields"></div>
                                                    </div>

                                                    <div class="card-footer" style="margin-top:20px;">
                                                        <div class="col-xs-3">
                                                            <ul class="pager wizard"
                                                                style="margin:10px 0px 0px!important">
                                                                <li class="previous first" style="display:none;"><a
                                                                            href="#" class="btn btn-primary">First</a>
                                                                </li>
                                                                <li class="previous previous1"><a href="#" class="nex1">Previous</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="col-xs-7">
                                                            <div class="row">
                                                                <div class="col-xs-3" style="width:auto;">
                                                                    <button type="submit" class="btn_new_save"
                                                                            style="padding:8px 20px;">Create
                                                                    </button>
                                                                </div>
                                                                <div class="col-xs-3" style="width:auto;">
                                                                    <a href="<?php echo e(url('/fac-Bhavesh-0554/customer')); ?>"
                                                                       class="btn_new_cancel" style="padding:8px 20px;">Cancel</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--</div>-->

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>

        <script>
            function autotab(current, to) {
                //alert(current.getAttribute("maxlength"));
                if (current.getAttribute &&
                    current.value.length == current.getAttribute("maxlength")) {
                    to.focus()
                }
            }

            function autotab1(current, to) {
                //alert(current.getAttribute("maxlength"));
                if (current.getAttribute &&
                    current.value.length == '2') {
                    to.focus()
                }
            }

        </script>

        <script>

            $('document').ready(function () {
                $('#clientid2').on('blur', function () {
                    var id1 = $("#clientid").val();
                    var id2 = $("#clientid1").val();
                    var id3 = $("#clientid2").val();
                    var id4 = '-';

                    if (id3 == '') {
                        var fullid = id1.concat(id4).concat(id2);
                    } else {
                        var fullid = id1.concat(id4).concat(id2).concat(id4).concat(id3);
                    }

                    // console.log(fullid);           
                    $.get('<?php echo URL::to('getClientfilename'); ?>?filename=' + fullid, function (data) {
                        if (data == 1) {
                            //('#clientid2-error').html('Client id is already Exist!');
                            //$('#clientid1-error').show();
                            $('.existmessage').show();
                            $(".nextssss").hide();
                        } else {
                            //$('#clientid1-error').html();
                            $('.existmessage').hide();
                            $(".nextssss").show();
                        }
                    });

                });

            });
        </script>
        <script>
            var counters = 1;

            $(".addCFnew").click(function () {
                counters++;
                var taxation = '.tax_service_' + counters;
                var taxationperiod = '.tax_period_' + counters;
                var tax_pariodbox = '.tax_periodbox_' + counters;

                var showservice = $('.tax_servicee').html();
                //    $(taxation).on('change', function() {
                //     var this2=$(taxation).val();
                //var taxid=$('.tax_service_1').val();
                var totalcountofrem_cust_year = $('.rem_customyear').length+1;
                $("#customFieldsbo").append('<div class="form-group" style="margin-bottom:0px;"><hr style="margin-top: 10px;margin-bottom: 10px;"><div class="col-md-5"><select class="form-control showtitle tax_service_' + counters + '" name="">' + showservice + '</select> </div> <div class="col-md-2 p_991 col-xs-4" style="padding-left:0px;padding-right:10px;"> <select class="form-control " name=""> <option value="">Select</option> <option value="Monthly">Monthly</option> <option value="Quarterly">Quarterly</option> <option value="Annually" selected="">Annually</option> </select> </div> <div class="col-md-2 p_991 col-xs-4" style="padding-left:0px;"> <select class="form-control"> <option value=""> Select</option> <option value="Jan">Jan</option> <option value="Feb">Feb</option> <option value="Mar">Mar</option> <option value="Apr">Apr</option> <option value="May">May</option> <option value="Jun">Jun</option> <option value="Jul">Jul</option> <option value="Aug">Aug</option> <option value="Sep">Sep</option> <option value="Oct">Oct</option> <option value="Nov">Nov</option> <option value="Dec">Dec</option> </select> </div> <div class="col-md-2 p_991 col-xs-4" style="padding-left:0px;"> <select class="form-control" name=""> <option value="">Select</option> <option value="2019" selected="">2019</option> <option value="2020">2020</option> <option value="2021">2021</option> </select> </div> <div class="col-md-1 col-xs-12 text-right p_991 mt10_991" style="padding-left:0px;"><a id="'+totalcountofrem_cust_year+'" class="btn btn-danger remCFnew"><i class="fa fa-minus"></i></a></div> </div>');
                $(".customyear").append('<div class="clear clearfix"></div><div id="rem_customyearid'+totalcountofrem_cust_year+'" class="col-md-12  rem_customyear"><div class="form-group"><hr style="margin-top: 0px;margin-bottom: 10px;"><div class="col-md-3 col-sm-12 p_991" style="padding-left:5px;"><select class="form-control" name=""><option value="">Select</option><option value="">2019</option></select></div><div class="col-md-9 col-sm-12 p_991" style="padding-left:5px;"><textarea class="form-control" rows="1"></textarea></div></div></div>');
                $(taxation).on('change', function () {
                    var this2 = $(taxation).val();
                    var taxid = $('.tax_service_1').val();
                    // alert(val2);
                    $(".services4").append('<div class="border"><div class="col-md-12"><div class="col-md-3"><label class="control-label">Type of Service :</label><input type="text" class="form-control taxtitletax_' + counters + '" name="taxtypeofservice[]" readonly ><input class="titletax_' + counters + '" type="hidden" name="titletax[]"></div><div id="regular"><div class="col-md-4" style="width:39%;"><div class="row"><label class="control-label">Note :</label><textarea readonly cols="50" name="taxserviceincludes[]" id="taxserviceincludes" class="form-control taxserviceincludenote_' + counters + '" style="height:80px!important;"></textarea><input name="taxserviceid[]" type="hidden" id="taxserviceid" class="form-control"></div></div><div class="col-md-2" style="width:13%"><label class=" readonly control-label">Regular Price :</label><input name="taxregularprice[]" type="text" id="taxregularprice" style="text-align:right;font-size:small !important" placeholder="Regular Price"  readonly class="taxregularpricetax_' + counters + ' form-control"></div><div class="col-md-2" style="width:11%"><div class="row"><label class="control-label" style="width:130px;">Combo Price :</label><input name="taxcomboprice[]" type="text" id="taxcomboprice" style="text-align:right;font-size:small !important" placeholder="Combo Price" readonly class="taxcombopricetax_' + counters + ' form-control"></div></div><div class="col-md-2" style="width:11%"><label class="control-label">Your Price :</label><input readonly name="taxprice1[]" type="text" id="taxprice1" style="text-align:right;font-size:small !important" placeholder="Price" class="form-control taxpricetax_' + counters + '"></div></div></div></div><div class="clear"></div>');
                    //alert(this2);
                    if (this2 == '3' || this2 == '2') {
                        //  alert('2')

                        $('.yearshide_' + counters).hide();
                        $(taxationperiod).html('<option value="Monthly">Monthly</option>');
                        // $('.taxservicemonth_'+counters).css("visibility","visible");
                        $(taxationperiod).hide();


                        $(tax_pariodbox).show();
                        $(taxationperiod).attr("disabled", "disabled");
                        $(tax_pariodbox).prop("disabled", "");
                        $(tax_pariodbox).val('Monthly');

                    }
                    if (this2 == '1') {
                        $(taxationperiod).html('<option value="Monthly">Monthly</option><option value="Quarterly">Quarterly</option><option value="Annually">Annually</option>');
                        //$(taxationperiod).attr("disabled", "" );
                        //   $('.yearshide_'+counters).hide();
                        $(tax_pariodbox).attr("disabled", "disabled");
                        $('.taxservicemonth_' + counters).css("visibility", "visible");
                        $(taxationperiod).show();
                        $(tax_pariodbox).hide();
                        // $(tax_pariodbox).val('Monthly');
                    }
                    if (this2 == '4') {
                        // $(taxationperiod).html('<option value="Quarterly">Quarterly</option>');
                        //      $(taxationperiod).attr("disabled", "disabled");
                        $(tax_pariodbox).prop("disabled", "");
                        $('.taxservicemonth').css("visibility", "visible");
                        $(taxationperiod).hide();
                        $(tax_pariodbox).show();
                        $(tax_pariodbox).val('Quarterly');
                    }
                    if (this2 == '8') {
                        $('.zzz_' + counters).show();
                        $('.taxservicemonth_' + counters).css("visibility", "visible");
                        // $(taxationperiod).html('<option value="Quarterly">Quarterly</option>');
                        $('.taxservicemonth_' + counters).css("visibility", "visible");
                        $(taxationperiod).prop("disabled", "disabled");
                        $(tax_pariodbox).prop("disabled", "");

                        $(taxationperiod).hide();
                        $(tax_pariodbox).show();
                        $(tax_pariodbox).val('Quarterly');
                    }


                    if (this2 == '7') {
                        $('.yearshide_' + counters).show();
                        $('.zzz_' + counters).hide();
                        //  $('.taxservicemonth_'+counters).hide();
                        $('.taxservicemonth_' + counters).css("visibility", "hidden");
                        $(taxationperiod).prop("disabled", "disabled");
                        $(tax_pariodbox).prop("disabled", "");

                        $(taxationperiod).hide();
                        $(tax_pariodbox).show();
                        $(tax_pariodbox).val('Annually');
                    } else {
                        $('.taxservicemonth_' + counters).css("visibility", "visible");
                    }

                    if (this2 == '6' || this2 == '5') {
                        $(taxationperiod).prop("disabled", "disabled");
                        $(tax_pariodbox).prop("disabled", "");

                        // $('.taxservicemonth').hide();
                        $('.taxservicemonth_' + counters).css("visibility", "hidden");

                        $(taxationperiod).hide();
                        $(tax_pariodbox).show();
                        $(tax_pariodbox).val('Annually');
                    } else {
                        $('.taxservicemonth_' + counters).css("visibility", "visible");
                    }


                    var currency = $("#currency option:selected").val();
                    var typeofservice = '3';
                    var titles = this2;

                    var serviceperiod = $("#serviceperiod  option:selected").val();
                    var totalprice1 = $(".totalprice1").val();
                    var pricing = $("#pricetype  option:selected").val();
                    //	alert(pricing);
                    var ckq = $(".ckq").val();//alert(typeofservice);
                    /* 	$.get('<?php echo URL::to('salestaxations'); ?>?typeofservice=' + typeofservice+'&title='+titles+'&serviceperiod=' + serviceperiod+'&currency=' + currency, function(data)
   	{ 
   	   
 //  		$(regularprice).val('');
   //	$(comboprice1).val('');
   	//$(sumprice).val('');
   	//alert(data.title);
   		var counts=Object.keys(data).length;
   	//	alert(counts);
   if(counts > 0)
   {
       
   	if(pricing == 'Regular')
   	{
   	    
   	  //  alert(data.regularprice);
   	   // $('.services4_'+counters).show();
   	    $('.titletax_'+counters).val(data.title);
   	    $('.taxtitletax_'+counters).val(data.taxtitle);
   	    $('.taxserviceincludenote_'+counters).val(data.serviceincludes1);
   	    $('.taxregularpricetax_'+counters).val(data.regularprice1);
   	    $('.taxcombopricetax_'+counters).val(data.comboprice1);
   	    $('.taxpricetax_'+counters).val(data.regularprice1);
   	}
   	if(pricing=='Combo')
   	{
   	     //$('.services4_'+counters).show();
   	     $('.titletax_'+counters).val(data.title);
   	     $('.taxtitletax_'+counters).val(data.taxtitle);
   	    $('.taxserviceincludenote_'+counters).val(data.serviceincludes1);
   	    $('.taxregularpricetax_'+counters).val(data.regularprice1);
   	    $('.taxcombopricetax_'+counters).val(data.comboprice1);
   	    $('.taxpricetax_'+counters).val(data.comboprice1);
   	  
   	}
   }
   
   	});
   	*/
                    $.get('<?php echo URL::to('salestaxations'); ?>?typeofservice=' + typeofservice + '&title=' + titles + '&serviceperiod=' + serviceperiod + '&currency=' + currency, function (data) {

                        /*	$.ajax({
            async: false,
            type: "GET",
            url: '<?php echo URL::to('salestaxations'); ?>?typeofservice=' + typeofservice+'&title='+titles+'&serviceperiod=' + serviceperiod+'&currency=' + currency,
           // dataType: "json",
           
            success: function (data) {*/
                        var counts = Object.keys(data).length;
                        //	alert(counts);
                        if (counts > 0) {

                            if (pricing == 'Regular') {
                                //  $('.services4').removeAttr("disabled");
                                //  alert(data.regularprice);
                                $('.services4').show();

                                //  alert(data.regularprice);
                                // $('.services4_'+counters).show();
                                $('.titletax_' + counters).val(data.title);
                                $('.taxtitletax_' + counters).val(data.taxtitle);
                                $('.taxserviceincludenote_' + counters).val(data.serviceincludes1);
                                $('.taxregularpricetax_' + counters).val(data.regularprice1);
                                $('.taxcombopricetax_' + counters).val(data.comboprice1);
                                $('.taxpricetax_' + counters).val(data.regularprice1);
                            }
                            if (pricing == 'Combo') {
                                //$('.services4').removeAttr("disabled");
                                //  alert(data.regularprice);
                                $('.services4').show();

                                //$('.services4_'+counters).show();
                                $('.titletax_' + counters).val(data.title);
                                //  alert($('.titletax_'+counters).val(data.title));
                                $('.taxtitletax_' + counters).val(data.taxtitle);
                                $('.taxserviceincludenote_' + counters).val(data.serviceincludes1);
                                $('.taxregularpricetax_' + counters).val(data.regularprice1);
                                $('.taxcombopricetax_' + counters).val(data.comboprice1);
                                $('.taxpricetax_' + counters).val(data.comboprice1);

                            }
                        }

                        // }
                        //  });


                    });

                });

                // });
            });
            $("#customFieldsbo").on('click', '.remCFnew', function () {
                //  flag--;
                //alert();
                var id =this.id;
                console.log(this.id);
                $(this).parent().parent().remove();
                $('#rem_customyearid'+id).remove();
            });
            
        //   $('.remCFnew').click(function() {
        //       console.log(this.id);
        //       $(this).parent().parent().remove();
        //   });
        </script>
        <script>
            $('document').ready(function () {

                $('#telephoneNo1Type').on('change', function () {
                    // alert(this.value);
                    if (this.value == 'Mobile') {
                        $('.hideyourself').hide();
                        $('.yourself').show();

                    } else {
                        $('.hideyourself').show();
                        $('.yourself').hide();

                    }

                });

                $('#telephoneNo2Type').on('change', function () {
                    if (this.value == 'Mobile') {
                        $('.hideyourself1').hide();
                        $('.yourself1').show();

                    } else {
                        $('.hideyourself1').show();
                        $('.yourself1').hide();

                    }

                });


                $('#business_id').on('change', function () {
                    var id = $(this).val();
                    if (id == '6') {
                        $('.naicss').hide();
                    } else {
                        $('.naicss').show();
                    }
                    $
                });

                $('.category1').on('change', function () {
                    var thiss = $(this).val();

                    $.get('<?php echo URL::to('getnaics'); ?>?naics=' + thiss, function (data) {
                        //alert(data.naics);

                        //  alert(subcatobj.naics);
                        $('.naics').val(data.naics);
                        $('.sic').val(data.sic);


                    });
                });
                $('#clientid1').on('blur', function () {


                    var id1 = $("#clientid").val();
                    var id2 = $("#clientid1").val();

                    id2.length; // Returns 81
                    $.trim(id2).length; // Returns 68
                    var cc = id2.replace(/ /g, '').length;
                    var id4 = '-';
                    var fullids = id1.concat(id4).concat(id2);
                    // alert(fullid);
                    console.log(fullid);
                    $.get('<?php echo URL::to('getClientfilenameid'); ?>?filenames=' + fullids, function (data) {
                        if (data == 1) {
                            //$('#clientid2-error').html('Client id is already Exist!');
                            //$('#clientid1-error').show();
                        } else {
                            // $('#clientid1-error').html();
                            //$(".nextssss").show();
                        }
                    });

                });

            });


        </script>

        <script>
            $(".ext").mask("99999");
            $("#federal_id").mask("99-9999999");
            $("#contact_number11").mask("a999999");
            $(".ext").mask("99339");
            $("#zip").mask("99999");
            $("#company_zip").mask("99999");
            $("#second_zip").mask("99999");
            $("#etelephone152").mask("(999) 999-9999");


            // $('input[name="filename"]').mask('SS-0000000');
            //  $('input[name="filename"]').focusout(function() {
            //  $('input[name="filename"]').val( this.value.toUpperCase() );

        </script>
        <script>
            $(document).ready(function () {
                //group add limit
                var maxGroup = 3;

                //add more fields group
                $(".addMore").click(function () {
                    if ($('body').find('.fieldGroup').length < maxGroup) {
                        var fieldHTML = '<div class="form-group fieldGroup">' + $(".fieldGroupCopy").html() + '</div>';
                        $('body').find('.fieldGroup:last').after(fieldHTML);
                    } else {
                        alert('Maximum ' + maxGroup + ' Persons are allowed.');
                    }
                });
                //remove fields group
                $("body").on("click", ".remove", function () {
                    $(this).parents(".fieldGroup").remove();
                });
            });
        </script>
        <script>
            function FillBilling(f) {
                if (f.billingtoo.checked == true) {
                    f.second_address.value = f.address.value;
                    $('#second_address').attr('readonly', 'readonly');

                    f.second_address1.value = f.address1.value;
                    $('#second_address1').attr('readonly', 'readonly');

                    f.second_city.value = f.city.value;
                    $('#second_city').attr('readonly', 'readonly');

                    f.second_state.value = f.stateId.value;
                    $('#second_state').attr('disabled', 'disabled');

                    f.second_zip.value = f.zip.value;
                    $('#second_zip').attr('readonly', 'readonly');
                } else {
                    $('#second_address').val('');
                    $('#second_address').removeAttr('readonly');

                    $('#second_address1').val('');
                    $('#second_address1').removeAttr('readonly');

                    $('#second_city').val('');
                    $('#second_city').removeAttr('readonly');

                    $('#second_state').val('');
                    $('#second_state').removeAttr('disable');

                    $('#second_zip').val('');
                    $('#second_zip').removeAttr('readonly');
                }
            }
        </script>
        <script>
            function showDiv(elem) {
                if (elem.value == 'Federal') {
                    document.getElementById('hidden_div').style.display = "none";
                } else {
                    document.getElementById('hidden_div').style.display = "block";
                }
            }
        </script>
        <script>
            $(document).ready(function () {

                $('#ext1').mask('9999');
                $('#ext2').mask('9999');

                $(document).on('change', '.maritial', function () {
                    var thiss = $(this).val();
                    // alert(thiss);
                    if (thiss == 'Married') {
                        $('.showmaritial').show();
                    } else {
                        $('.showmaritial').hide();

                    }
                });
                $(document).on('change', '.category', function () {

                    //console.log('htm');
                    var id = $(this).val();
                    // alert(id);
                    $('#image1').empty('');
                    if (id == '6') {
                        $('.personal').show();
                        $('.removecom').hide();
                        $('.addcom').show();
                        $('.bus').hide();
                        $('.yourself').show();
                        $('.yourself1').show();
                        $('.hideyourself').hide();
                        $('.hideyourself1').hide();
                    } else {
                        $('.hideyourself').show();
                        $('.yourself').hide();
                        $('.hideyourself1').show();
                        $('.yourself1').hide();
                        $('.bus').show();
                        $('.removecom').show();
                        $('.personal').hide();
                        $('.addcom').hide();
                    }
                    // alert(<?php echo URL::to('getRequest'); ?>);

                    $.get('<?php echo URL::to('getRequest'); ?>?id=' + id, function (data) {
                        // alert(data);
                        // alert('1111');
                        if (data == "") {
                            $('#business_catagory_name_2').hide();
                            $('#business_catagory_name_4').hide();
                            $('#business_catagory_name_3').hide();
                        } else {
                            $('#business_catagory_name_2').show();
                        }
                        $('#business_catagory_name').empty();
                        $('#business_catagory_name_4').hide();
                        $('#business_catagory_name_3').hide();
                        $('#business_catagory_name').append('<option value="">---Select---</option>');
                        $('#business_brand_category_name').append('<option value="">---Select---</option>');
                        $('#business_brand_name').append('<option value="">---Select---</option>');
                        $.each(data, function (index, subcatobj) {
                            $('#business_catagory_name').append('<option value="' + subcatobj.id + '">' + subcatobj.business_cat_name + '</option>');
                            //	$('#user_type').val('subcatobj.business_cat_name');
                        })
                    });
                });
            });
        </script>
        <script>
            $(document).ready(function () {
                $(document).on('change', '#business_catagory_name', function () {
                    //console.log('htm');
                    var id = $(this).val();
                    $.get('<?php echo URL::to('getcatm'); ?>?id=' + id, function (data) {
                        // alert('111');
                        if (data == "") {
                            //alert('2222222222222');
                            $('#business_catagory_name_3').hide();
                            $('#business_catagory_name_4').hide();
                        } else {

                            $('#business_catagory_name_3').show();
                            $('#business_catagory_name_4').hide();
                        }
                        $('#business_brand_name').empty();
                        $('#business_brand_category_name').empty();
                        $('#business_brand_name').append('<option value="">---Select---</option>');
                        $.each(data, function (index, subcatobj) {
                            $('#business_brand_name').append('<option value="' + subcatobj.id + '">' + subcatobj.business_brand_name + '</option>');
                        })
                    });
                });
            });
        </script>
        <script>
            $(document).ready(function () {
                $(document).on('change', '#business_brand_category_name', function () {
                    //console.log('htm');
                    var id = $(this).val();
                    $.get('<?php echo URL::to('business_brand_category_name_1'); ?>?id=' + id, function (data) {
                        $('#image3').empty();
                        // $('#business_brand_name').append('<option value="">---Select---</option>');
                        $.each(data, function (index, subcatobj) {
                            $('#image3').append('<img src="/public/businessbrandcategory/' + subcatobj.business_brand_category_image + '" alt="" class="img-responsive">');
                        })
                    });
                });
            });
        </script>
        <script>
            $(document).ready(function () {
                $(document).on('change', '#business_catagory_name', function () {
                    //console.log('htm');
                    var id = $(this).val();
                    $.get('<?php echo URL::to('getRequest_1'); ?>?id=' + id, function (data) {
                        if (data == "") {
                            //	$('#business_catagory_name_4').hide();
                        } else {
                            //	$('#business_catagory_name_4').show();
                        }
                        $('#image1').empty('');
                        $.each(data, function (index, subcatobj) {
                            $('#image1').append('<img src="/public/category/' + subcatobj.business_cat_image + '" alt="" class="img-responsive">');
                        })
                    });
                });
            });
            $(document).ready(function () {

                $(".phone").keypress(function (e) {
                    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                        return false;
                    }
                    var curchr = this.value.length;
                    var curval = $(this).val();
                    if (curchr == 3 && curval.indexOf("(") <= -1) {
                        $(this).val("(" + curval + ")" + " ");
                    } else if (curchr == 4 && curval.indexOf("(") > -1) {
                        $(this).val(curval + ")-");
                    } else if (curchr == 5 && curval.indexOf(")") > -1) {
                        $(this).val(curval + "-");
                    } else if (curchr == 9) {
                        $(this).val(curval + "-");
                        //$(this).attr('maxlength', '14');
                    }
                });
            });
        </script>
        <script>
            $(document).ready(function () {
                $(document).on('change', '#business_brand_name', function () {
                    //console.log('htm');
                    var id = $(this).val();
                    $.get('<?php echo URL::to('getcatmm'); ?>?id=' + id, function (data) {
                        if (data == "") {
                            $('#business_catagory_name_4').hide();
                        } else {
                            $('#business_catagory_name_4').show();
                        }
                        $('#business_brand_category_name').empty();
                        $('#business_brand_category_name').append('<option value="">---Select---</option>');
                        $.each(data, function (index, subcatobj) {
                            $('#business_brand_category_name').append('<option value="' + subcatobj.id + '">' + subcatobj.business_brand_category_name + '</option>');
                        })
                    });
                });
            });
        </script>
        <script>
            $(document).ready(function () {
                $(document).on('change', '#business_brand_name', function () {
                    //console.log('htm');
                    var id = $(this).val();
                    $.get('<?php echo URL::to('/getcategoryimages'); ?>?id=' + id, function (data) {
                        $('#image2').empty();
                        $.each(data, function (index, subcatobj) {
                            $('#image2').append('<img src="/public/businessbrand/' + subcatobj.business_brand_image + '" alt="" class="img-responsive">');
                        })
                    });
                });
            });
        </script>
        <script>
            $(document).ready(function () {
                $(document).on('change', '.category', function () {
                    //console.log('htm');
                    var id = $(this).val();
                    $.get('<?php echo URL::to('getcat'); ?>?id=' + id, function (data) {
                        $('#image').empty();
                        $('#user_type').empty();
                        $.each(data, function (index, subcatobj) {
                            $('#user_type').val(subcatobj.bussiness_name);
                            $('#image').append('<img src="https://financialservicecenter.net/public/frontcss/images/' + subcatobj.newimage + '" alt="" class="img-responsive">');
                        })
                    });

                    $.get('<?php echo URL::to('getRequests'); ?>?id=' + id, function (data) {
                        // alert(id);
                        if (data == "") {
                            $('#business_catagory_name_2').hide();
                            $('#business_catagory_name_4').hide();
                            $('#business_catagory_name_3').hide();
                        } else {
                            $('#business_catagory_name_2').show();
                        }
                        $('#business_catagory_name').empty();
                        $('#business_catagory_name_4').hide();
                        $('#business_catagory_name_3').hide();
                        $('#business_catagory_name').append('<option value="">---Select---</option>');
                        $('#business_brand_category_name').append('<option value="">---Select---</option>');
                        $('#business_brand_name').append('<option value="">---Select---</option>');
                        $.each(data, function (index, subcatobj) {
                            $('#business_catagory_name').append('<option value="' + subcatobj.id + '">' + subcatobj.business_cat_name + '</option>');
                            //	$('#user_type').val('subcatobj.business_cat_name');
                        })
                    });


                });
            });
            $(document).on('change', '.typeofservice', function () {
                $(".typeofservice").not(this).find("option[value=" + $(this).val() + "]").attr('disabled', true);
            });
        </script>

        <script>
            $(document).ready(function () {
                $("select#status").change(function () {
                    var selectedCountry = $(this).children("option:selected").val();
                    var color = $("option:selected", this).attr("class");
                    $("#status").attr("class", color).addClass("form-control1 fsc-input");
                });
            });

        </script>
        <script>
            $(document).on("change", ".numeric1", function () {
                var sum = 0;
                $(".numeric1").each(function () {
                    sum += +$(this).val().replace("%", "");
                    var num = parseFloat($(this).val());
                    $(this).val(num.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "%");
                    //$(".numeric2").val(num.toFixed(2)); 
                });
                if (sum > 100) {
                    $(".total").val(sum.toFixed(2) + "%");
                    $('.btn-primary1').addClass("disabled");
                    $('#t1').show();
                } else if (sum < 100) {
                    $(".total").val(sum.toFixed(2) + "%");
                    $('.btn-primary1').addClass("disabled");
                    $('#t1').show();
                } else if (sum = 100) {
                    $(".total").val(sum.toFixed(2) + "%");
                    $('.btn-primary1').removeClass("disabled");
                    $('#t1').hide();


                } else {
                    $(".total").val(sum.toFixed(2) + "%");
                    $('.btn-primary1').removeClass("disabled");
                }
            });
        </script>
        <script>
            function FillBilling123(f) {
                if (f.billingtoo123.checked == true) {
                    f.agent_fname2.value = f.agent_fname.value;
                    f.agent_mname2.value = f.agent_mname.value;
                    f.agent_lname2.value = f.agent_lname.value;
                }
            }
        </script>
        <script>
            $(document).ready(function () {
                $(document).on('change', '#typeofservice', function () {
                    //console.log('htm');
                    var id = $(this).val();
                    $.get('<?php echo URL::to('/faderal'); ?>?id=' + id, function (data) {
                        $('#typeofcorp').empty();
                        $('#typeofcorp1').empty();
                        $('#due_date').empty();
                        $('#type_form').empty();
                        $('#due_date1').empty();
                        $('#type_form1').empty();
                        $('#fedral_state').empty();
                        //$('#type_form').append('<option value="">Select</option>');
                        //$('#type_form1').append('<option value="">Select</option>');
                        //$('#fedral_state').append('<option value="">Select</option>');
                        $.each(data, function (index, subcatobj) {
                            $('#typeofcorp').val(subcatobj.authority_name);
                            $('#typeofcorp1').append('<option value="' + subcatobj.telephone + '">' + subcatobj.authority_name + ' (' + subcatobj.telephone + ')</option>');
                            $('#type_form').append('<option value="' + subcatobj.telephone + '">' + subcatobj.telephone + '</option>');
                            // $('#type_form').val(subcatobj.telephone);
                            $('#due_date').val(subcatobj.address);
                            $('#due_date1').val(subcatobj.address);
                            $('#fedral_state').append('<option value="' + subcatobj.city + '">' + subcatobj.city + '</option>');
                            $('#type_form1').append('<option value="' + subcatobj.city + ' ' + subcatobj.zip + '">' + subcatobj.city + ' ' + subcatobj.zip + '</option>');
                        })
                    });
                });
            });
        </script>
        <script>
            var date = $('#ext2_1').val();
            $('#mobiletype_1').on('change', function () {
                if (this.value == 'Home') {
                    document.getElementById('ext2_1').removeAttribute('readonly');
                    $('#ext2_1').val();

                    $('.showguardian').hide();
                    $('.hideext').show();


                } else if (this.value == 'Mobile') {
                    $('.hideext').hide();
                    $('.showguardian').show();
                    $('#ext2_1').hide();

                } else {
                    $('.hideext').show();
                    $('#ext2_1').show();
                    document.getElementById('ext2_1').readOnly = true;
                    $('#ext2_1').val('');
                    $('.showguardian').hide();

                }
            })
        </script>
        <script>
            var date = $('#ext1').val();
            $('#telephoneNo1Type').on('change', function () {
                if (this.value == 'Office') {
                    document.getElementById('ext1').removeAttribute('readonly');
                    $('#ext1').val();
                } else if (this.value == 'Business') {
                    document.getElementById('ext1').removeAttribute('readonly');
                    $('#ext1').val();
                } else {
                    document.getElementById('ext1').readOnly = true;
                    $('#ext1').val('');
                }
            })
        </script>
        <script>
            var date = $('#ext2').val();
            $('#telephoneNo2Type').on('change', function () {
                if (this.value == 'Office') {
                    document.getElementById('ext2').removeAttribute('readonly');
                    $('#ext2').val();
                } else if (this.value == 'Business') {
                    document.getElementById('ext2').removeAttribute('readonly');
                    $('#ext2').val();
                } else {
                    document.getElementById('ext2').readOnly = true;
                    $('#ext2').val('');
                }
            })
        </script>
        <script>
            var date = $('#ext2_2').val();
            $('#ext2_2').mask('9999');
            $('#mobiletype_2').on('change', function () {
                if (this.value == 'Home') {
                    document.getElementById('ext2_2').removeAttribute('readonly');
                    $('#ext2_2').val();
                    $('#ext2_2').show();
                    $('.showguardian2').hide();
                    $('.hideext2').show();
                } else if (this.value == 'Mobile') {
                    $('.showguardian2').show();
                    $('.hideext2').hide();
                    $('#ext2_2').hide();
                } else {
                    $('.showguardian2').hide();
                    $('.hideext2').show();
                    document.getElementById('ext2_2').readOnly = true;
                    $('#ext2_2').val('');
                    $('#ext2_2').show();
                }
            })
        </script>
        <script>
            $(document).ready(function () {
                $(".effective_date1").datepicker({
                    autoclose: true,
                    orientation: "top",
                    endDate: "today"
                });
                var maxField = 10; //Input fields increment limitation
                var addButton = $('.add_button'); //Add button selector
                var wrapper = $('.field_wrapper'); //Input field wrapper
                var fieldHTML = '<div class="dd"><div class="form-group"><div class="col-md-12 col-sm-12 col-xs-12"><div class="col-md-4"><label class="control-label">Type of License :</label><select type="text" class="form-control" id="profession" name="profession[]"><option value="">Select</option><option value="ERO">ERO</option><option value="CPA">CPA</option><option value="Mortgage Broker">Mortgage Broker</option><option value="Mortgage Broker">Mortgage Broker</option><option value="PTIN">PTIN</option></select></div><div class="col-md-1"><label class="control-label">State :</label><select name="profession_state[]" id="profession_state" class="form-control fsc-input" style="padding: 2px !important;"><option value="AK">AK</option><option value="AS">AS</option><option value="AZ">AZ</option><option value="AR">AR</option><option value="CA">CA</option><option value="CO">CO</option><option value="CT">CT</option><option value="DE">DE</option><option value="DC">DC</option><option value="FM">FM</option><option value="FL">FL</option><option value="GA">GA</option><option value="GU">GU</option><option value="HI">HI</option><option value="ID">ID</option><option value="IL">IL</option><option value="IN">IN</option><option value="IA">IA</option><option value="KS">KS</option><option value="KY">KY</option><option value="LA">LA</option><option value="ME">ME</option><option value="MH">MH</option><option value="MD">MD</option><option value="MA">MA</option><option value="MI">MI</option><option value="MN">MN</option><option value="MS">MS</option><option value="MO">MO</option><option value="MT">MT</option><option value="NE">NE</option><option value="NV">NV</option><option value="NH">NH</option><option value="NJ">NJ</option><option value="NM">NM</option><option value="NY">NY</option><option value="NC">NC</option><option value="ND">ND</option><option value="MP">MP</option><option value="OH">OH</option><option value="OK">OK</option><option value="OR">OR</option><option value="PW">PW</option><option value="PA">PA</option><option value="PR">PR</option><option value="RI">RI</option><option value="SC">SC</option><option value="SD">SD</option><option value="TN">TN</option><option value="TX">TX</option><option value="UT">UT</option><option value="VT">VT</option><option value="VI">VI</option><option value="VA">VA</option><option value="WA">WA</option><option value="WV">WV</option><option value="WI">WI</option><option value="WY">WY</option></select></div><div class="col-md-2"><label class="control-label">Effective Date :</label><input name="profession_epr_date[]" placeholder="Effective Date" value="" type="text" id="profession_epr_date" class="form-control"></div><div class="col-md-2"><label class="control-label">License # :</label><input name="profession_license[]" placeholder="License" value="" type="text" id="profession_license" class="form-control"></div><div class="col-md-2"><label class="control-label">Expire Date :</label><input name="profession_epr_date[]" placeholder="Expire Date" value="" type="text" id="profession_epr_date" class="form-control"></div><div class="col-md-1"><a href="javascript:void(0);" class="remove_button btn btn-danger" title="Remove field" style="width:100%; margin:26px 0 0 0; padding:6px 0; display:block;"><i class="fa fa-minus" aria-hidden="true"></i></a></div></div></div><div class="form-group"><div class="col-md-12 col-sm-12 col-xs-12"><div class="col-md-4" style="width:39%;"><label class="control-label">Note :</label><input name="profession_note[]" placeholder="Note" value="" type="text" id="profession_note" class="form-control"><input name="profession_id[]" placeholder="Expire Date" value="" type="hidden" id="" class="form-control"></div><div class="col-md-3"></div><div class="col-md-2"><a href="javascript:void(0);" class="btn_new btn-view-license" style="width: 100%;" data-toggle="modal" data-target="#myModal">View License</a></div><div class="col-md-2"><a href="javascript:void(0);" class="btn_new btn-renew" style="width: 100%;">Renew Now</a></div></div><div class="form-group"><div class="col-md-12 col-sm-12 col-xs-12"><div class="col-md-10 col-sm-10 col-xs-12"></div><div class="col-md-2 col-sm-2 col-xs-12"></div></div></div></div>'; //New input field html 
                var x = 1; //Initial field counter is 1
                $(addButton).click(function () { //Once add button is clicked
                    if (x < maxField) { //Check maximum number of input fields
                        x++; //Increment field counter
                        $(wrapper).append(fieldHTML); // Add field html
                    }
                });
                $(wrapper).on('click', '.add_button1', function (e) { //Once remove button is clicked
                    e.preventDefault();
                    $(wrapper).append(fieldHTML); //Remove field html
                    x++;
                    $(".effective_date1").datepicker({
                        autoclose: true,
                        orientation: "top",
                        endDate: "today"
                    });
                });
                $(wrapper).on('click', '.remove_button', function (e) { //Once remove button is clicked
                    e.preventDefault();
                    $(this).parent('.dd').remove(); //Remove field html
                    x--; //Decrement field counter
                });
            });
        </script>
        <script>
            $(document).ready(function () {
                $(document).on('change', '#type_form3', function () {
                    var selectedCountry = $("#fedral_state option:selected").val(); //alert(selectedCountry);
                    var id = $(this).val();
                    $.get('<?php echo URL::to('getcount'); ?>?id=' + id + '&state=' + selectedCountry, function (data) {
                        $('#business_license').empty();

                        $.each(data, function (index, subcatobj) {
                            $('#business_license').append('<option value="' + subcatobj.county + '">' + subcatobj.county + '</option>');
                        })
                    });
                });
            });
        </script>
        <script>
            $(".effective_date").datepicker({
                autoclose: true,
                orientation: "top",
                endDate: "today"
            });
            var room = 1;

            function education_fields() {
                room++;
                var objTo = document.getElementById('education_fields')
                var divtest = document.createElement("div");
                divtest.setAttribute("class", "form-group removeclass" + room);
                divtest.innerHTML = '<label class="control-label col-md-3">Note ' + room + ' :</label><div class="col-md-6"><input name="notes[]" value="" type="text" id="notes" placeholder="Create Note" class=" form-control" /></div></div><div class="col-md-1"> <button class="btn btn-danger" type="button" onclick="remove_education_fields(' + room + ');"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span> </button></div></div></div>';
                var rdiv = 'removeclass' + room;
                var rdiv1 = 'Schoolname' + room;
                objTo.appendChild(divtest)
                $(".effective_date").datepicker({
                    autoclose: true,
                    orientation: "top",
                    endDate: "today"
                });
            }

            function remove_education_fields(rid) {
                $('.removeclass' + rid).remove();
            }
        </script>
        <script type="text/javascript">
            $(function () {

                $("#business_id").change(function () {
                    var ids = $(this).val();

                    if (ids == '6') {
                        $('.personaldata').show();
                    } else {
                        $('.personaldata').hide();
                    }
                });
                $("#type_form3").change(function () {
                    var selectedText = $(this).find("option:selected").text();
                    var selectedValue = $(this).val();
                    if (selectedValue == 'County') {
                        $("#county-change").show();
                        $("#business_license").show();
                        $("#business_license2").hide();
                        $("#county-change1").show();
                        $("#city-change").hide();
                        $("#city-change1").hide();
                    } else if (selectedValue == 'City') {
                        $("#county-change").hide();
                        $("#county-change1").hide();
                        $("#business_license").hide();
                        $("#business_license2").show();
                        $("#city-change").show();
                        $("#city-change1").show();
                    } else {
                        $("#county-change").show();
                        $("#county-change1").show();
                    }
                });
            });
        </script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js"></script>
        <script type="text/javascript"
                src="https://cdn.jsdelivr.net/bootstrap.wizard/1.3.2/jquery.bootstrap.wizard.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
        <script type="text/javascript">
            $.ajaxSetup({headers: {'X-CSRF-Token': $('input[name="_token"]').val()}});
            $(document).ready(function () {
                $.validator.addMethod("email", function (value, element) {
                    return this.optional(element) || /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(value);
                }, "Email Address is invalid: Please enter a valid email address.");
                var $validator = $("#registrationForm").validate({
                    rules: {
                        status: {
                            required: true,
                        },
                        business_id: {
                            required: true,
                        },
                        type_of_business: {
                            required: true,
                        },
                        address: {
                            required: true,
                        },
                        zip: {
                            required: true,
                        },
                        countryId: {
                            required: true,
                        },
                        telephone1: {
                            required: true,
                        },
                        first_name: {
                            required: true,
                        },
                        last_name: {
                            required: true,
                        },
                        first_name_1: {
                            required: true,
                        },
                        last_name_1: {
                            required: true,
                        },
                        'employee[]': {
                            required: true
                        },
                        'bulletin[]': {
                            required: true
                        },
                        taxation: {
                            required: true
                        },
                        'due_date[]': {
                            required: true
                        },
                        website: {
                            required: false,
                            url: true
                        },
                        motelephoneile: {
                            required: true,
                        },
                        credit_card: {
                            required: true,
                            //creditcard: true,
                            minlength: 13,
                            maxlength: 16,
                            digits: true,
                        },
                        email: {
                            required: true,                            
                        },
                        filename: {
                            required: true,
                            remote: {
                                url: "<?php echo e(URL::to('/clientname1')); ?>",
                                type: "post"
                            }
                        },
                        telephone: {
                            required: true,
                        },
                        city: {
                            required: true,
                        }
                    },
                    messages: {
                        email: {
                            required: "Please Enter Your Email Address",
                            email: "Please Enter A Valid Email Address",
                            remote: "Email already in use!"
                        },
                        filename: {
                            required: "Please Enter Your Id",
                            remote: "Id already in use!"
                        },
                        status: {
                            required: "Please Select Your Status",
                            //email: "Please Enter A Valid Email Address",            
                        },
                        business_id: {
                            required: "Please Select Your Business",
                            //email: "Please Enter A Valid Email Address",            
                        },
                        telephone: {
                            required: "Please Enter Your Telephone",
                            //email: "Please Enter A Valid Email Address",            
                        },
                        type_of_business: {
                            required: "Please Select Your Type of Business",
                            //email: "Please Enter A Valid Email Address",            
                        },
                        address: {
                            required: "Please Select Your Business Address",
                            //email: "Please Enter A Valid Email Address",            
                        },
                        zip: {
                            required: "Please Enter Your zip",
                            //email: "Please Enter A Valid Email Address",            
                        },
                        countryId: {
                            required: "Please Select Your State",
                            //email: "Please Enter A Valid Email Address",            
                        },
                        first_name: {
                            required: "Please Select Your First Name",
                            pattern: "The Textbox string format is invalid",
                        },
                        last_name: {
                            required: "Please Select Your Last Name",
                            pattern: "The Textbox string format is invalid",
                        },
                        first_name_1: {
                            required: "Please Select Your First Name",
                            pattern: "The Textbox string format is invalid",
                        },
                        last_name_1: {
                            required: "Please Select Your Last Name",
                            pattern: "The Textbox string format is invalid",
                        },
                        business_no: {
                            required: "Please Select Your Telephone ",
                        },

                        telephone1: {
                            required: "Please Select Your Telephone ",
                        },
                        city: {
                            required: "Please Enter Your City Name",
                        },

                    },
                    errorElement: "em",
                    errorPlacement: function (error, element) {
                        // Add the `help-block` class to the error element
                        error.addClass("help-block");
                        // Add `has-feedback` class to the parent div.form-group
                        // in order to add icons to inputs
                        element.parents(".form-group").addClass("has-feedback");
                        if (element.prop("type") === "checkbox") {
                            error.insertAfter(element.parent("label"));
                        } else {
                            error.insertAfter(element);
                        }
                        // Add the span element, if doesn't exists, and apply the icon classes to it.
                        if (!element.next("span")[0]) {
                            $("<span class='glyphicon glyphicon-remove form-control-feedback'></span>").insertAfter(element);
                        }
                    },
                    success: function (label, element) {
                        // Add the span element, if doesn't exists, and apply the icon classes to it.
                        if (!$(element).next("span")[0]) {
                            $("<span class='glyphicon glyphicon-ok form-control-feedback'></span>").insertAfter($(element));
                        }
                    },
                    highlight: function (element, errorClass, validClass) {
                        $(element).closest('.form-group, .has-feedback').removeClass('has-success').addClass('has-error')
                        $(element).next("span").addClass("glyphicon-remove").removeClass("glyphicon-ok");
                        $('.previous2').css({"display": "none"});
                    },
                    unhighlight: function (element, errorClass, validClass) {
                        $(element).closest('.form-group, .has-feedback').removeClass('has-error').addClass('has-success');
                        $(element).next("span").addClass("glyphicon-ok").removeClass("glyphicon-remove");
                        var clientidnext = $('#clientid').val();
                        var clientid1next = $('#clientid1').val();
                        var statusnext = $('#status').val();

                        var businessidnext = $('#business_id').val();
                        var businesscategorynext = $('#business_category_name').val();
                        var firstnamenext = $('#first_name').val();
                        var lastnamenext = $('#last_name').val();

                        var addressnext = $('#address').val();
                        var countrynext = $('#countries_states1').val();
                        var citynext = $('#city').val();
                        var stateIdnext = $('#stateId').val();
                        var zipnext = $('#zip').val();
                        var emailnext = $('#email').val();

                        if (businessidnext != '6' && businessidnext != '2') {
                            if (clientidnext != '' && clientid1next != '' && statusnext != '' && businesscategorynext != ''
                                && addressnext != '' && countrynext != '' && citynext != '' && stateIdnext != '' && zipnext != '' && emailnext != '') {
                                $('.previous2').css({"display": "block"});
                            }
                        } else if (businessidnext == '6') {

                            if (clientidnext != '' && clientid1next != '' && statusnext != '' && firstnamenext != '' && lastnamenext != ''
                                && addressnext != '' && countrynext != '' && citynext != '' && stateIdnext != '' && zipnext != '' && emailnext != '') {
                                $('.previous2').css({"display": "block"});
                            }

                        }

                    }
                });

                $('#smartwizard').bootstrapWizard({
                    'tabClass': 'nav nav-pills',
                    'onNext': function (tab, navigation, index) {
                        //   alert(index);
                        var $valid = $("#registrationForm").valid();
                        if (index == 1) {
                            $(".con3").css({"display": "block"});
                        }
                        if (index == 0) {

                            $(".con3").css({"display": "block"});
                        } else if (index == 2) {
                            $(".con").css({"display": "block"});
                            $(".actab1").addClass("selected-bg");
                        } else if (index == 3) {
                            $(".con1").css({"display": "block"});
                            $(".con2").css({"display": "block"});
                            $(".actab2").addClass("selected-bg");
                        } else if (index == 4) {
                            $(".con2").css({"display": "block"});
                        }

                        if (!$valid) {
                            $validator.focusInvalid();
                            return false;
                        }
                    },
                    'onPrevious': function (tab, navigation, index) {
                        //alert(index);
                        var $valid = $("#registrationForm").valid();
                        if (index == 0) {

                            $(".con3").css({"display": "none"});
                        } else if (index == 1) {

                            $(".con").css({"display": "none"});
                            $(".actab1").css({"background": "transparent"});
                        } else if (index == 2) {
                            $(".con1").css({"display": "none"});
                            $(".actab2").css({"background": "transparent"});
                            $(".con2").css({"display": "none"});
                        } else if (index == 3) {
                            $(".con2").css({"display": "none"});
                        }

                        if (!$valid) {
                            $validator.focusInvalid();
                            return false;
                        }
                    },
                    'onTabClick': function (activeTab, navigation, currentIndex, nextIndex) {
                        if (nextIndex <= currentIndex) {
                            return;
                        }
                        var $valid = $("#registrationForm").valid();
                        if (!$valid) {
                            $validator.focusInvalid();
                            return false;
                        }
                        if (nextIndex > currentIndex + 1) {
                            return false;

                        }
                    }
                });
            });
        </script>
        <script>
            var room1 = 0;
            var coun = 0;
            var z = room1 + coun;
            $(document).on('change', '.accounting_period', function () {
                var id = $(this).val();
                if (id == '4-2') {
                    $('.qtr').hide();
                    $('.monthhide').show();

                } else if (id == '5-2') {
                    $('.qtr').hide();
                    $('.halfyear').hide();

                    $('.monthhide').show();

                } else if (id == '10-2') {
                    $('.qtr').show();
                    $('.halfyear').show();
                    $('.monthhide').show();


                } else if (id == '7-2') {
                    $('.monthhide').hide();


                } else {
                    $('.qtr').show();
                    $('.halfyear').show();
                    $('.monthhide').show();


                }
                // 	alert(id);
                $('.acperiods1').val(id);
                if (id == '') {
                    $(".services2").attr("disabled", 'disabled');
                    $('.services2').hide();

                } else {
                    $('.services2').show();
                    $(".services2").attr("disabled", false);

                }
                var result = id.split('-');
                var currency = $("#currency option:selected").val();
                var typeofservice = result[1];
                var serviceperiod = $("#serviceperiod  option:selected").val();
                var totalprice1 = $(".totalprice1").val();
                var pricing = $("#pricetype  option:selected").val();
                //	alert(pricing);
                var ckq = $(".ckq").val();//alert(typeofservice);
                $.get('<?php echo URL::to('salestaxacc'); ?>?id=' + result[0] + '&typeofservice=' + typeofservice + '&serviceperiod=' + serviceperiod + '&currency=' + currency, function (data) {
                    $.each(data, function (index, subcatobj) {
                        if (pricing == 'Regular') {
                            //  alert(data.regularprice);
                            $('.services2').removeAttr("disabled");
                            $('.services2').show();

                            $('.serviceincludeacc').val(data.serviceincludes);
                            $('.regularpriceacc').val(data.regularprice);
                            $('.combopriceacc').val(data.comboprice);
                            $('.priceacc').val(data.regularprice);

                        }
                        if (pricing == 'Combo') {
                            $(".services2").prop("disabled", false);

                            $('.services2').show();
                            $('.serviceincludeacc').val(data.serviceincludes);
                            $('.regularpriceacc').val(data.regularprice);
                            $('.combopriceacc').val(data.comboprice);
                            $('.priceacc').val(data.comboprice);

                        }
                    })
                });
            });

            $(document).on('change', '.tax_service_1', function () {


                var id = $(this).val();
                if (id == '3' || id == '2') {
                    $('.hidemonths').show();
                    $('.qtr1').show();
                    $('.tax_period_1').html('<option value="Monthly">Monthly</option>');
                    $('.text_oneoption').removeAttr('disabled');

                    // $('.tax_period_1').hide();
                    // $('.tax_period_1').attr("disabled", "disabled");
                    $('.text_oneoption').show();
                    // $('.text_oneoption').val('Monthly');


                } else if (id == '4' || id == '8') {
                    $('.hidemonths').show();

                    $('.qtr1').hide();
                    $('.text_oneoption').removeAttr('disabled');
                    //alert();
                    $('.tax_period_1').html('<option value="Quarterly">Quarterly</option>');
                    // $('.tax_period_1').hide();
                    //$('.tax_period_1').attr("disabled", "disabled");
                    $('.text_oneoption').show();
                    //  $('.tax_period_1').html('<option value="Quarterly">Quarterly</option>');
                } else if (id == '1') {
                    $('.hidemonths').show();

                    $('.qtr1').show();
                    // $('.tax_period_1').show();
                    $('.tax_period_1').removeAttr('disabled');
                    $('.tax_period_1').html('<option value="Monthly">Monthly</option><option value="Quarterly">Quarterly</option><option value="Annually">Annually</option>');
                    $('.text_oneoption').hide();
                    $('.text_oneoption').attr("disabled", "disabled");

                } else if (id == '6' || id == '5') {
                    $('.hidemonths').hide();

                    $('.qtr1').show();
                    $('.tax_period_1').html('<option value="Annually">Annually</option>');

                    $('.taxservicemonth').css("visibility", "hidden");
                    //    $('.tax_period_1').attr("disabled", "disabled");

                    $('.text_oneoption').show();
                    $('.text_oneoption').removeAttr('disabled');

                    $('.text_oneoption').val('Annually');
                } else if (id == '9') {
                    $('.hidemonths').hide();

                    $('.qtr1').show();
                    $('.tax_period_1').html('<option value="Annually">Annually</option>');

                    $('.taxservicemonth').css("visibility", "hidden");
                    //    $('.tax_period_1').attr("disabled", "disabled");

                    $('.text_oneoption').show();
                    $('.text_oneoption').removeAttr('disabled');

                    $('.text_oneoption').val('Annually');

                } else if (id == '7') {
                    $('.hidemonths').hide();

                    $('.qtr1').show();
                    $('.tax_period_1').html('<option value="Annually">Annually</option>');
                    //$('.yearshide').show();
                    //   $('.tax_period_1').hide();
                    //            $('.tax_period_1').attr("disabled", "disabled");

                    $('.text_oneoption').show();
                    $('.text_oneoption').removeAttr('disabled');

                    $('.text_oneoption').val('Annually');
                }


            });

            $(document).on('change', '.tax_service_1', function () {
                var id = $(this).val();
                //alert(id);
                var result = id.split(' -');
                var currency = $("#currency option:selected").val();
                var typeofservice = '3';
                var titles = id;

                var serviceperiod = $("#serviceperiod  option:selected").val();
                var totalprice1 = $(".totalprice1").val();
                var pricing = $("#pricetype  option:selected").val();
                //	alert(pricing);
                var ckq = $(".ckq").val();//alert(typeofservice);
                $.get('<?php echo URL::to('salestaxations'); ?>?typeofservice=' + typeofservice + '&title=' + titles + '&serviceperiod=' + serviceperiod + '&currency=' + currency, function (data) {

                    //  		$(regularprice).val('');
                    //	$(comboprice1).val('');
                    //$(sumprice).val('');
                    var counts = Object.keys(data).length;

                    //	alert(counts);
                    if (counts > 0) {

                        if (pricing == 'Regular') {

                            //alert(data.regularprice1);
                            $('.services4').removeAttr("disabled");
                            //  alert(data.regularprice);
                            $('.services4').show();

                            $('.taxtitletax').val(data.taxtitle);
                            $('.titletax').val(data.title);


                            $('.taxserviceincludenote').val(data.serviceincludes1);
                            $('.taxregularpricetax').val(data.regularprice1);
                            $('.taxcombopricetax').val(data.comboprice1);
                            $('.taxpricetax').val(data.regularprice1);
                        }
                        if (pricing == 'Combo') {

                            $('.services4').removeAttr("disabled");
                            $('.services4').show();
                            $('.titletax').val(data.title);


                            $('.taxtitletax').val(data.taxtitle);
                            $('.taxserviceincludenote').val(data.serviceincludes1);
                            $('.taxregularpricetax').val(data.regularprice1);
                            $('.taxcombopricetax').val(data.comboprice1);
                            $('.taxpricetax').val(data.comboprice1);

                        }
                    }


                });
            });


            function education_field_note() {
                room1++;
                z++;
                var objTo = document.getElementById('table')
                var divtest = document.createElement("tbody");
                divtest.setAttribute("class", "form-group removeclass" + z);
                divtest.innerHTML = '<tr><td><select class="form-control pricetype_' + z + '" name="titles[]" id="titles"> <option value="">---Select Service---</option> <?php $__currentLoopData = $taxtitle; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ti): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?><option value="<?php echo e($ti->title); ?>"><?php echo e($ti->title); ?></option><?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></select></td><td><input type="taxt" name="taxnote[]" placeholder="Note" class="form-control"></td><td><input type="text" placeholder="Regular Price" class="regularprice_' + z + ' form-control" name="regularprice1[]" style="text-align:right"></td><td><input type="text" style="text-align:right" placeholder="Combo Price" class=" form-control comboprice_' + z + '" name="comboprice1[]"></td><td><input style="text-align:right" type="text" id="taxprice_' + z + '" class=" form-control price taxprice_' + z + '" placeholder="Your Price" name="taxprice"></td><td><button class="btn btn-danger" type="button" onclick="remove_education_fields(' + z + ');"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span> </button></td></tr>';
                var rdiv = 'removeclass' + z;
                var rdiv1 = 'Schoolname' + z;
                var pricetype = '.pricetype_' + z;
                var comboprice1 = '.comboprice_' + z;
                var regularprice = '.regularprice_' + z;
                var sumprice = '#taxprice_' + z;


                $(document).on('change', pricetype, function () {
                    //console.log('htm');
                    var id = $(this).val();
                    var currency = $("#currency option:selected").val();
                    var typeofservice = $("#typeofservice option:selected").val();
                    var serviceperiod = $("#serviceperiod  option:selected").val();
                    var totalprice1 = $(".totalprice1").val();
                    var pricing = $("#pricetype  option:selected").val();
                    var ckq = $(".ckq").val();
                    $.get('<?php echo URL::to('salestax'); ?>?id=' + id + '&typeofservice=' + typeofservice + '&serviceperiod=' + serviceperiod + '&currency=' + currency, function (data) {
                        $(regularprice).val('');
                        $(comboprice1).val('');
                        $(sumprice).val('');
                        $.each(data, function (index, subcatobj) {
                            $(regularprice).val(subcatobj.regularprice1);
                            $(comboprice1).val(subcatobj.comboprice1);

                            //	var comboprice = subcatobj.comboprice1;

                            if (pricing == 'Regular') {
                                $(sumprice).val(subcatobj.regularprice1);
                                var price = subcatobj.regularprice1;
                                var price1 = parseFloat(price.replace(/[^\d\.]*/g, ''));//alert(price1);
                                var price2 = totalprice1;
                                var price3 = parseFloat(price2.replace(/[^\d\.]*/g, ''));
                                var x = price1 + price3;//alert(price3);
                                var x2 = parseFloat(Math.round(x * 100) / 100).toFixed(2);
                                var no = x2.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                var x1 = ckq + ' ' + no;
                                $(".totalprice1").val(x1);
                            }
                            if (pricing == 'Combo') {
                                $(sumprice).val(subcatobj.comboprice1);
                                var price = subcatobj.comboprice1;
                                var price1 = parseFloat(price.replace(/[^\d\.]*/g, ''));
                                var price2 = totalprice1;
                                var price3 = parseFloat(price2.replace(/[^\d\.]*/g, ''));
                                var x = price1 + price3;
                                var x2 = parseFloat(Math.round(x * 100) / 100).toFixed(2);
                                var no = x2.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                var x1 = ckq + ' ' + no;
                                $(".totalprice1").val(x1);
                            }

                        })
                    });
                });
                objTo.appendChild(divtest)

            }

            function remove_education_fields(rid) {
                $('.removeclass' + rid).remove();
                room1--;
                z--;
            }

        </script>

        <script>
            var room1 = 0;
            var coun = 0;
            var z = room1 + coun;

            function education_service() {
                room1++;
                z++;
                var objTo = document.getElementById('education_service')
                var divtest = document.createElement("div");
                divtest.setAttribute("class", "form-group ser removeclass" + z);
                divtest.innerHTML = '<div class="col-md-12"><input name="serviceid[]" type="hidden" id="serviceid" class="form-control"><div class="form-group"><div class="border"><div class="col-md-3"><label class="control-label">Type of Service :</label><select name="typeofservice[]" type="text" id="typeofservice_' + z + '" class="form-control typeofservice"><option value="">Type of Service</option><?php $__currentLoopData = $typeofser; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $typeofser1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?><option value="<?php echo e($typeofser1->id); ?>"><?php echo e($typeofser1->typeofservice); ?></option><?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></select></div><div id="regular_' + z + '"><div class="col-md-4"><label class="control-label">Service Includes :</label>  <textarea class="form-control" rows="3"  name="serviceincludes[]" id="serviceinclude1_' + z + '"></textarea></div><div class="col-md-2" style="width:13%;text-align:center;"><label class="control-label">Regular Price :</label><input name="regularprice[]" style="text-align:right" type="text" id="regularprice2_' + z + '" placeholder="Regular Price"  readonly class="regularprice2_' + z + ' form-control"></div><div class="col-md-2" style="width:13%; text-align:center"><label class="control-label" style="">Combo Price :</label><input name="comboprice[]" type="text" id="comboprice2_' + z + '" style="text-align:center" placeholder="Combo Price" readonly class="comboprice2_' + z + ' form-control"></div><div class="col-md-2" style="width:11%;text-align:center"><label class="control-label">Your Price :</label><input name="price[]" style="text-align:right" type="text" id="price_' + z + '" placeholder="Your Price" class="form-control price"></div></div></div><div class="row"> <div class="col-md-3"> <label class="control-label">Service Start Month :</label> <select name="]" type="text" id="" class="form-control "> <option value="">Select</option> <option value="">January</option> <option value="">February</option> <option value="">March</option> <option value="">April</option> <option value="">May</option> <option value="">June</option> <option value="">July</option> <option value="">August</option> <option value="">September</option> <option value="">October</option> <option value="">November</option> <option value="">December</option> </select> </div> <div class="col-md-3"> <label class="control-label">Service Start Year :</label> <select name="]" type="text" id="" class="form-control "> <option value="">Select</option> <option value="">2020</option> <option value="">2021</option> </select> </div> <span style="color: red;position: relative;right: 28px;text-align: right;float: right;margin-top: 31px; width: 0;" class="fa fa-trash" onclick="remove_service(' + z + ');" aria-hidden="true"></span></div></div></div></div><div class="input_fields_wrap_notes_' + z + '" style="display:none"><br><br><table class="table" id="table_' + z + '" style="width:100%;margin: auto;"><thead><tr style="background:#00a0e3;color:#fff"><th scope="col" style="width: 262px;">Title</th><th scope="col">Note</th><th scope="col" style="width: 120px;">Regular Price</th><th style="width: 120px;" scope="col">Combo Price</th><th style="width: 120px;" scope="col">Your Price</th><th scope="col" style="width: 120px;">Action</th></tr></thead><tbody><tr><td style="width:262px;"><select class="form-control" name="titles[]" id="titles_' + z + '"><option value="">---Select Service---</option><?php $__currentLoopData = $taxtitle; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ti): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?><option value="<?php echo e($ti->title); ?>"><?php echo e($ti->title); ?></option><?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></select></td><td><input type="hidden" name="taxid[]"><input type="text" class="form-control" placeholder="Note" name="taxnote[]" id="note_' + z + '"></td><td style="width: 120px;"><input type="text" placeholder="Regular Price" class="form-control" name="regularprice1[]"  id="regularprice1_' + z + '"></td><td style="width: 120px;"><input type="text" class="form-control" placeholder="Combo Price" name="comboprice1[]" id="comboprice1_' + z + '"></td><td style="width: 120px;"><input type="text" class="form-control price" placeholder="Your Price" name="taxprice[]" id="taxprice1_' + z + '"></td><td style="width: 120px;"><a class="btn btn-primary" onclick="education_field_table(' + z + ');"> Add</a></td></tr></tbody></table></div></div>';
                var rdiv = 'removeclass' + z;
                var rdiv1 = 'Schoolname' + z;
                var pricetype1 = '#titles_' + z;
                var totalprice = '.totalprice';
                var regularprice1 = '#regularprice1_' + z;
                var typeofservice1 = '#typeofservice_' + z;
                var regularprice2 = '#regularprice2_' + z;
                var table = '#table_' + z;//alert(table);
                var input_fields_wrap_notes = '.input_fields_wrap_notes_' + z;
                var regular = '#regular_' + z;
                var currency1 = '#currency';
                var serviceperiod1 = '#serviceperiod';
                var comboprice = '#comboprice1_' + z;
                var comboprice2 = '#comboprice2_' + z;
                var serviceinclude1 = '#serviceinclude1_' + z;
                var price_1 = '#price_' + z;
                var sumprice = '#taxprice1_' + z;
                var ckq = $(".ckq").val();
                $(document).on('blur', '.price', function () {
                    var sum = 0;
                    //var n = parseInt($(this).val().replace(/\D/g,'') || 0);
                    // var n1 = n.toLocaleString();
                    var vk = parseFloat($(this).val().replace(/[^\d\.]*/g, '') || 0);
                    var sign3 = parseFloat(Math.round(vk * 100) / 100).toFixed(2);
                    var n1 = sign3.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    var x2 = ckq + ' ' + n1;
                    $(this).val(x2);

                    $('.price').each(function () {
                        sum += parseFloat($(this).val().replace(/[^\d\.]*/g, '') || 0);
                    });
                    var sum1 = sum;
                    var sign2 = parseFloat(Math.round(sum1 * 100) / 100).toFixed(2);
                    var no = sign2.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    var x1 = ckq + ' ' + no;
                    $('.totalprice1').val(x1);
                });
                $(document).on('change', typeofservice1, function () {
                    var sign1 = $(this).val();
                    if (sign1 == '3') {
                        $(input_fields_wrap_notes).show();
                        $(regular).hide();
                    } else {
                        $(input_fields_wrap_notes).hide();
                        $(regular).show();
                    }
                });
                $(document).on('change', pricetype1, function () { //alert(comboprice);
                    var id = $(this).val();
                    var currency = $(currency1).val();//
                    var typeofservice = $(typeofservice1).val();
                    var serviceperiod = $(serviceperiod1).val();
                    var totalprice1 = $(".totalprice1").val();
                    var pricing = $("#pricetype  option:selected").val();
                    var ckq = $(".ckq").val();
                    $.get('<?php echo URL::to('salestax'); ?>?id=' + id + '&typeofservice=' + typeofservice + '&serviceperiod=' + serviceperiod + '&currency=' + currency, function (data) {
                        $(regularprice1).val('');
                        $(comboprice).val('');
                        $.each(data, function (index, subcatobj) {
                            $(regularprice1).val(subcatobj.regularprice1);
                            $(comboprice).val(subcatobj.comboprice1);
                            if (pricing == 'Regular') {
                                $(sumprice).val(subcatobj.regularprice1);
                                var price = subcatobj.regularprice1;
                                var price1 = parseFloat(price.replace(/[^\d\.]*/g, ''));//alert(price1);
                                var price2 = totalprice1;
                                var price3 = parseFloat(price2.replace(/[^\d\.]*/g, ''));
                                var x = price1 + price3;//alert(price3);
                                var x2 = parseFloat(Math.round(x * 100) / 100).toFixed(2);
                                var no = x2.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                var x1 = ckq + ' ' + no;
                                $(".totalprice1").val(x1);
                            }
                            if (pricing == 'Combo') {
                                $(sumprice).val(subcatobj.comboprice1);
                                var price = subcatobj.comboprice1;
                                var price1 = parseFloat(price.replace(/[^\d\.]*/g, ''));
                                var price2 = totalprice1;
                                var price3 = parseFloat(price2.replace(/[^\d\.]*/g, ''));
                                var x = price1 + price3;
                                var x2 = parseFloat(Math.round(x * 100) / 100).toFixed(2);
                                var no = x2.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                var x1 = ckq + ' ' + no;
                                $(".totalprice1").val(x1);
                            }

                        })
                    });
                });

                $(document).on('change', typeofservice1, function () {
                    var id = $(this).val();
                    var currency = $(currency1).val();
                    var typeofservice = $(typeofservice1).val();
                    var serviceperiod = $(serviceperiod1).val();
                    var totalprice1 = $(".totalprice1").val();
                    var ckq = $(".ckq").val();
                    var pricing = $("#pricetype  option:selected").val();//alert(totalprice1);
                    $.get('<?php echo URL::to('salestax'); ?>?id=' + id + '&typeofservice=' + typeofservice + '&serviceperiod=' + serviceperiod + '&currency=' + currency, function (data) {
                        $(regularprice2).val('');
                        $(comboprice2).val('');
                        $(serviceinclude1).val('');
                        $.each(data, function (index, subcatobj) {
                            $(regularprice2).val(subcatobj.regularprice);
                            var price = subcatobj.regularprice;
                            $(comboprice2).val(subcatobj.comboprice);
                            var comboprice = subcatobj.comboprice;
                            $(serviceinclude1).val(subcatobj.serviceincludes);//alert(pricing);
                            if (pricing == 'Regular') {
                                $(price_1).val(price);

                                var price1 = parseFloat(price.replace(/[^\d\.]*/g, ''));//alert(price1);
                                var price2 = totalprice1;
                                var price3 = parseFloat(price2.replace(/[^\d\.]*/g, ''));
                                var x = price1 + price3;//alert(price3);
                                var x2 = parseFloat(Math.round(x * 100) / 100).toFixed(2);
                                var no = x2.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                var x1 = ckq + ' ' + no;
                                // var x1 = ckq+' '+x;
                                $(".totalprice1").val(x1);
                            }
                            if (pricing == 'Combo') {
                                $(price_1).val(comboprice);

                                var price1 = parseFloat(comboprice.replace(/[^\d\.]*/g, ''));
                                var price2 = totalprice1;
                                var price3 = parseFloat(price2.replace(/[^\d\.]*/g, ''));
                                var x = price1 + price3;

                                var x2 = parseFloat(Math.round(x * 100) / 100).toFixed(2);
                                var no = x2.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                var x1 = ckq + ' ' + no;
                                $(".totalprice1").val(x1);
                            }

                        })
                    });
                });
                objTo.appendChild(divtest)
            }

            function remove_service(rid) {
                $('.removeclass' + rid).remove();
                room1--;
                z--;
            }

            function education_field_table(rid) {
                z++;
                var table = 'table_' + rid;//alert(table);
                var objTo = document.getElementById(table)
                var divtest = document.createElement("tbody");
                divtest.setAttribute("class", "form-group removeclass" + z);
                divtest.innerHTML = '<tr><td><select class="form-control pricetype1_' + z + '" id="pricetype1_' + z + '" name="titles[]"><option value="">---Select Service---</option> <?php $__currentLoopData = $taxtitle; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ti): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?><option value="<?php echo e($ti->title); ?>"><?php echo e($ti->title); ?></option><?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></select></td><td><input type="text" name="taxnote[]" class="form-control"  placeholder="Note"><input type="hidden" name="taxid[]"></td><td><input type="text" placeholder="Regular Price" id="regularprice1_' + z + '" class="regularprice1_' + z + ' form-control" name="regularprice1[]" style="text-align:right"></td><td><input type="text" style="text-align:right" placeholder="Combo Price" class=" form-control comboprice1_' + z + '" id="comboprice1_' + z + '" name="comboprice1[]"></td><td><input type="text" placeholder="Your Price" class=" style="text-align:right" form-control price taxprice1_' + z + '" id="taxprice1_' + z + '" name="taxprice[]"></td><td><button class="btn btn-danger" type="button" onclick="remove_education_fields(' + z + ');"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span> </button></td></tr>';
                var rdiv = 'removeclass' + z;
                var rdiv1 = 'Schoolname' + z;
                var currency1 = '#currency';
                var serviceperiod1 = '#serviceperiod';
                var pricetype = '#pricetype1_' + z;
                var comboprice11 = '#comboprice1_' + z;
                var regularprice11 = '#regularprice1_' + z;
                var typeofservice1 = '#typeofservice_' + rid;
                var sumprice = '#taxprice1_' + z;
                $(document).on('change', pricetype, function () {
                    var id = $(this).val();
                    var currency = $(currency1).val();
                    var typeofservice = $(typeofservice1).val();
                    var serviceperiod = $(serviceperiod1).val();
                    var totalprice1 = $(".totalprice1").val();
                    var pricing = $("#pricetype  option:selected").val();
                    var ckq = $(".ckq").val();
                    $.get('<?php echo URL::to('salestax'); ?>?id=' + id + '&typeofservice=' + typeofservice + '&serviceperiod=' + serviceperiod + '&currency=' + currency, function (data) {
                        $(regularprice11).val('');
                        $(comboprice11).val('');
                        $.each(data, function (index, subcatobj) {
                            $(regularprice11).val(subcatobj.regularprice1);
                            $(comboprice11).val(subcatobj.comboprice1);
                            if (pricing == 'Regular') {
                                $(sumprice).val(subcatobj.regularprice1);
                                var price = subcatobj.regularprice1;
                                var price1 = parseFloat(price.replace(/[^\d\.]*/g, ''));//alert(price1);
                                var price2 = totalprice1;
                                var price3 = parseFloat(price2.replace(/[^\d\.]*/g, ''));
                                var x = price1 + price3;//alert(price3);
                                var x2 = parseFloat(Math.round(x * 100) / 100).toFixed(2);
                                var no = x2.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                var x1 = ckq + ' ' + no;
                                $(".totalprice1").val(x1);
                            }
                            if (pricing == 'Combo') {
                                $(sumprice).val(subcatobj.comboprice1);
                                var price = subcatobj.comboprice1;
                                var price1 = parseFloat(price.replace(/[^\d\.]*/g, ''));
                                var price2 = totalprice1;
                                var price3 = parseFloat(price2.replace(/[^\d\.]*/g, ''));
                                var x = price1 + price3;
                                var x2 = parseFloat(Math.round(x * 100) / 100).toFixed(2);
                                var no = x2.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                var x1 = ckq + ' ' + no;
                                $(".totalprice1").val(x1);
                            }

                        })
                    });
                });
                objTo.appendChild(divtest)
            }

            function remove_education_fields(rid) {
                $('.removeclass' + rid).remove();
                room1--;
                z--;
            }
        </script>
        <script>
            $(document).ready(function () {
                $(document).on('change', '.pricetype', function () {
                    var id = $(this).val();
                    var currency = $("#currency option:selected").val();
                    var typeofservice = $("#typeofservice option:selected").val();
                    var serviceperiod = $("#serviceperiod  option:selected").val();
                    var totalprice1 = $(".totalprice").val();
                    var pricing = $("#pricetype  option:selected").val();

                    $.get('<?php echo URL::to('salestax'); ?>?id=' + id + '&typeofservice=' + typeofservice + '&serviceperiod=' + serviceperiod + '&currency=' + currency, function (data) {
                        $('.regularprice').val('');
                        $('.comboprice').val('');
                        $('#serviceincludes').val('');
                        $.each(data, function (index, subcatobj) {
                            $('.regularprice1').val(subcatobj.regularprice1);
                            $('.comboprice1').val(subcatobj.comboprice1);
                            if (pricing == 'Regular') {
                                $('.sumprice').val(subcatobj.regularprice1);
                                $(".totalprice1").val(subcatobj.regularprice1);
                                $("#taxprice").val(subcatobj.regularprice1);
                            }
                            if (pricing == 'Combo') {
                                $('.sumprice').val(subcatobj.comboprice1);
                                $(".totalprice1").val(subcatobj.comboprice1);
                                $("#taxprice").val(subcatobj.comboprice1);
                            }
                        })
                    });
                });
            });
            $(document).ready(function () {
                $(document).on('change', '#typeofservice', function () {
                    var id = $(this).val();
                    var currency = $("#currency option:selected").val();
                    var typeofservice = $("#typeofservice option:selected").val();
                    var serviceperiod = $("#serviceperiod  option:selected").val();
                    var pricing = $("#pricetype  option:selected").val();
                    //	alert(currency);
                    $.get('<?php echo URL::to('salestax'); ?>?id=' + id + '&typeofservice=' + typeofservice + '&serviceperiod=' + serviceperiod + '&currency=' + currency, function (data) {
                        $('.regularprice').val('');
                        $('.comboprice').val('');
                        $('.serviceincludes').val('');
                        $.each(data, function (index, subcatobj) {
                            $('.regularprice').val(subcatobj.regularprice);
                            $('.comboprice').val(subcatobj.comboprice);
                            if (pricing == 'Regular') {
                                $('#price').val(subcatobj.regularprice);
                                $(".totalprice1").val(subcatobj.regularprice);
                            }
                            if (pricing == 'Combo') {
                                $('#price').val(subcatobj.comboprice);
                                $(".totalprice1").val(subcatobj.comboprice);
                            }
                            $('#serviceincludes').val(subcatobj.serviceincludes);
                        })
                    });
                });

                // discount	
                $('.discountprice').blur(function () {
                    var ckq = $(".ckq").val();
                    var totalprice1 = $("#totalpri").val();
                    var total = parseFloat(totalprice1.replace(/[^\d\.]*/g, '') || 0);
                    //var n = parseInt($(this).val().replace(/\D/g,'') || 0);
                    // var n1 = n.toLocaleString();
                    var vk = parseFloat($(this).val().replace(/[^\d\.]*/g, '') || 0);
                    var sign3 = parseFloat(Math.round(vk * 100) / 100).toFixed(2);
                    var n1 = sign3.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    var x2 = ckq + ' ' + n1;
                    $(this).val(x2);
                    var dis = parseFloat(x2.replace(/[^\d\.]*/g, ''));
                    var ciscount = (total + dis ? total - dis : total);
                    var x21 = parseFloat(Math.round(ciscount * 100) / 100).toFixed(2);
                    var no2 = x21.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    var x11 = ckq + ' ' + no2;
                    $("#totalprice1").val(x11);
                });

                // we used jQuery 'keyup' to trigger the computation as the user type
                $('.price').blur(function () {
                    var ckq = $(".ckq").val();

                    var sum = 0;
                    var vk = parseFloat($(this).val().replace(/[^\d\.]*/g, '') || 0);
                    // var n = parseInt($(this).val().replace(/\D/g,'') || 0);
                    // var n1 = n.toLocaleString();
                    // var n2 = n1..toFixed(2); alert(n2);
                    var sign3 = parseFloat(Math.round(vk * 100) / 100).toFixed(2);
                    var n1 = sign3.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    var x2 = ckq + ' ' + n1;
                    $(this).val(x2);
                    $('.price').each(function () {
                        sum += parseFloat($(this).val().replace(/[^\d\.]*/g, '') || 0);//alert(sum);
                    });
                    var sum1 = sum;
                    var sign2 = parseFloat(Math.round(sum1 * 100) / 100).toFixed(2);
                    var no = sign2.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    var x1 = ckq + ' ' + no;
                    $('.totalprice1').val(x1);
                });

                $(document).on('change', '#currency', function () {
                    var id = $("#currency").val();
                    $.get('<?php echo URL::to('getsign'); ?>?id=' + id, function (data) {
                        $('#ckq').empty();
                        $.each(data, function (index, subcatobj) {

                            $(".ckq").val(subcatobj.sign);
                            //   $('.totalprice1').val(val);
                        })
                    });
                });

            });
        </script>
        <script>
            $(document).ready(function () {

                $(document).on('change', '#pricetype', function () {
                    var id = $(this).val();
                    if (id == 'Regular') {
                        $('.service').show();
                    } else if (id == 'Combo') {
                        $('.service').show();
                    } else {
                        $('.service').hide();
                    }
                })

                $(document).on('change', '.pricetype', function () {
                    //console.log('htm');
                    var id = $(this).val();
                    var currency = $("#currency option:selected").val();
                    var typeofservice = $("#typeofservice option:selected").val();
                    var serviceperiod = $("#serviceperiod  option:selected").val();
                    //	alert(currency);
                    $.get('<?php echo URL::to('salestax'); ?>?id=' + id + '&typeofservice=' + typeofservice + '&serviceperiod=' + serviceperiod + '&currency=' + currency, function (data) {
                        // $('#image').empty();
                        $('.regularprice').val('');
                        $('.comboprice').val('');
                        $.each(data, function (index, subcatobj) { //alert(subcatobj.regularprice1);
                            $('.regularprice1').val(subcatobj.regularprice1);
                            $('.comboprice1').val(subcatobj.comboprice1);
                        })
                    });
                });
            });
            $(document).ready(function () {
                $(document).on('change', '#typeofservice', function () {
                    //console.log('htm');
                    var id = $(this).val();
                    var currency = $("#currency option:selected").val();
                    var typeofservice = $("#typeofservice option:selected").val();
                    var serviceperiod = $("#serviceperiod  option:selected").val();
                    //	alert(currency);
                    $.get('<?php echo URL::to('salestax'); ?>?id=' + id + '&typeofservice=' + typeofservice + '&serviceperiod=' + serviceperiod + '&currency=' + currency, function (data) {
                        // $('#image').empty();
                        $('.regularprice').val('');
                        $('.comboprice').val('');
                        $.each(data, function (index, subcatobj) { //alert(subcatobj.regularprice1);
                            $('.regularprice').val(subcatobj.regularprice1);
                            $('.comboprice').val(subcatobj.comboprice1);
                        })
                    });
                });
            });

            function faxbli233name(f) {
                if (f.faxbli3name.checked == true) {
                    var nametype = $('#nametype').html();
                    var nametype1 = $('#nametype').val();
                    if ('mr' == nametype1) {
                        var mr = 'selected';
                    } else {
                        var mr = '';
                    }

                    if ('mrs' == nametype1) {
                        var mrs = 'selected';
                    } else {
                        var mrs = '';
                    }

                    if ('miss' == nametype1) {
                        var miss = 'selected';
                    } else {
                        var miss = '';
                    }


                    //alert(nametype1);
                    f.firstname.value = f.first_name.value;


                    f.middlename.value = f.middle_name.value;


                    f.lastname.value = f.last_name.value;


                    $("#minss").html('<option value="mr" ' + mr + ' >Mr.</option><option value="mrs" ' + mrs + '>Mrs.</option><option value="miss" ' + miss + '>Miss.</option>');

                } else {
                    f.firstname.value = '';
                    f.middlename.value = '';
                    f.lastname.value = '';
                    $("#minss").html('<option value="mr" >Mr.</option><option value="mrs">Mrs.</option><option value="miss">Miss.</option>');

                }

            }

            function faxbli233(f) {
                if (f.faxbli3.checked == true) {

                    f.contact_address1.value = f.address.value;
                    $('#contact_address1').attr('readonly', 'readonly');

                    f.contact_address2.value = f.address1.value;
                    $('#contact_address2').attr('readonly', 'readonly');

                    f.city_1.value = f.city.value;
                    $('#city_1').attr('readonly', 'readonly');

                    f.state_1.value = f.stateId.value;
                    $('#state_1').attr('disabled', 'disabled');

                    f.zip_1.value = f.zip.value;
                    $('#zip_1').attr('readonly', 'readonly');

                } else {
                    f.contact_address1.value = '';
                    $('#contact_address1').removeAttr('readonly');

                    f.contact_address2.value = '';
                    $('#contact_address2').removeAttr('readonly');

                    f.city_1.value = '';
                    $('#city_1').removeAttr('readonly');

                    f.state_1.value = '';
                    $('#state_1').removeAttr('disabled');

                    f.zip_1.value = '';
                    $('#zip_1').removeAttr('readonly');

                }

            }

            function faxbli233_g(f) {
                if (f.faxbli3_g.checked == true) {
                    //  alert();
                    if (f.businesstype.value == 'Mobile') {
                        $('.showguardian').show();
                        $('.hideext').hide();
                        $('#ext2_1').hide();
                    } else {
                        $('.showguardian').hide();
                        $('.hideext').show();
                        $('#ext2_1').show();

                    }

                    if (f.telephoneNo2Type.value == 'Mobile') {
                        $('.showguardian2').show();
                        $('.hideext2').hide();
                        $('#ext2_2').hide();

                    } else {
                        $('.showguardian2').hide();
                        $('.hideext2').show();
                        $('#ext2_2').show();

                    }
                    // alert(f.telephone2guardian.value);
                    // alert(f.telephone1guardian.value);
                    // alert();
                    //  alert(f.motelephoneile1.value);
                    f.etelephone152.value = f.business_no.value;
                    $('#etelephone152').attr('readonly', 'readonly');
                    $('#etelephone152').removeAttr('maxLength');

                    f.mobiletype_1.value = f.telephoneNo1Type.value;
                    $('#mobiletype_1').attr('disabled', 'disabled');

                    f.eext1.value = f.businessext.value;
                    $('#eext1').attr('readonly', 'readonly');

                    f.guardian2.value = f.telephone2guardian.value;
                    $('#guardian2').attr('disabled', 'disabled');


                    f.guardian.value = f.telephone1guardian.value;
                    $('#guardian').attr('readonly', 'readonly');


                    f.mobile_2.value = f.motelephoneile1.value;
                    $('#mobile_2').attr('readonly', 'readonly');

                    f.mobiletype_2.value = f.telephoneNo2Type.value;
                    $('#mobiletype_2').attr('disabled', 'disabled');

                    f.ext2_2.value = f.ext2.value;
                    $('#ext2_2').attr('readonly', 'readonly');

                } else {
                    f.etelephone152.value = '';
                    $('#etelephone152').removeAttr('readonly');
                    $('#etelephone152').removeAttr('maxLength');

                    f.mobiletype_1.value = '';
                    $('#mobiletype_1').removeAttr('disabled');

                    f.eext1.value = '';
                    $('#eext1').removeAttr('readonly');

                    f.guardian2.value = '';
                    $('#guardian2').removeAttr('disabled');

                    f.guardian.value = '';
                    $('#guardian').removeAttr('readonly');

                    f.mobile_2.value = '';
                    $('#mobile_2').removeAttr('readonly');

                    f.mobiletype_2.value = '';
                    $('#mobiletype_2').removeAttr('disabled');

                    f.ext2_2.value = '';
                    $('#ext2_2').removeAttr('readonly');

                }

            }


            function emailbl12(f) { //alert();
                if (f.emailbli2.checked == true) {//alert();
                    $('#email_1').val(f.email.value);
                    $('#email_1').attr('readonly', 'readonly');
                } else {
                    $('#email_1').val('');
                    $('#email_1').removeAttr('readonly');
                }

            }

            function faxbli12(f) { //alert();
                if (f.faxbli2.checked == true) { //alert(f.business_fax.value);
                    $('#contact_fax').val(f.business_fax.value);
                    $('#contact_fax').attr('readonly', 'readonly');
                } else {
                    $('#contact_fax').val('');
                    $('#contact_fax').removeAttr('readonly');
                }

            }
        </script>
        <script src="https://rawgit.com/RobinHerbots/Inputmask/4.x/dist/jquery.inputmask.bundle.js"></script>
        <script>
            //$('#clientid').inputmask({mask:'AA-'});
        </script>
        <script>
            $('#clientid').inputmask({mask: 'AA'});
            $("#clientid1").mask("999");
        </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>