<?php $__env->startSection('main-content'); ?>
<style>
.modal-dialog {
    width: 898px;
    margin: 30px auto;
}
/*label{float:right}*/
</style>
<div class="content-wrapper">
    <section class="content-header page-title">
        <h2><span class="pull-left"><?php echo e($employment->firstName); ?> <?php echo e($employment->middleName); ?> <?php echo e($employment->lastName); ?></span> Candidate Data</h2>
    </section>
      <section class="content">
   <div class="row">
      <div class="col-md-12">
         <div class="box box-success">
             <div class="box-header">
            </div>
            <div class="card-body">
               <form method="post" action="<?php echo e(route('candidate.update',$employment->cid)); ?>" class="form-horizontal" id="content" name="content" enctype="multipart/form-data">
                  <?php echo e(csrf_field()); ?><?php echo e(method_field('PATCH')); ?>   
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="form-group">
						<label class="col-md-3 control-label">Name : <span class="star-required">*</span></label>
                        <div class="col-lg-6 col-md-8 col-sm-12 col-xs-12">
                            <div class="row">
                                <div class="col-md-2 col-xs-4 fsc-form-col fsc-element-margin" style="">
                                    <div class="">
                                        <select style="padding: 0;" class="form-control fsc-input" id="nametype" name="nametype">
                                            <option value="mr">Mr.</option>
                                            <option value="mrs">Mrs.</option>
                                            <option value="miss">Miss</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-8 fsc-element-margin">						
                                    <input type="hidden" class="form-control fsc-input" name='employment_id' id="employment_id" placeholder="First Name" value="<?php echo e($employment->employment_id); ?>">
                                    <input type="text" class="form-control fsc-input txtOnly" name='firstName' id="firstName " placeholder="First Name" value="<?php echo e($employment->firstName); ?>">
                                </div>
                                <div class="col-md-2 col-xs-4 fsc-element-margin" style="">
                                    <div class="">
                                        <input type="text" class="form-control fsc-input txtOnly" maxlength="1" name='middleName' id="middleName" placeholder="M" value="<?php echo e($employment->middleName); ?>">
                                    </div>		
                                </div>
                                <div class="col-md-4 col-xs-8 fsc-element-margin" style="">
                                    <input type="text" class="form-control fsc-input txtOnly" name="lastName" value="<?php echo e($employment->lastName); ?>" id="lastName" placeholder="Last Name">
                                </div>
                            </div>	
                        </div>
    				</div>
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="form-group">
						<label class="col-md-3 control-label">Address 1 : <span class="star-required">*</span></label>
                        <div class="col-lg-6 col-md-8 col-sm-12 col-xs-12">
                            <div class="row">
                                <div class=" col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                    <input type="text" class="form-control fsc-input" id="address1" value="<?php echo e($employment->address1); ?>" name='address1' placeholder="Address">
                                </div>
                            </div>
                        </div>
                    </div>			
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="form-group">
						<label class="col-md-3 control-label">Address 2 : <span class="star-required">*</span></label>
                        <div class="col-lg-6 col-md-8 col-sm-12 col-xs-12">
                            <div class="row">
                                <div class=" col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                    <input type="text" class="form-control fsc-input" id="address2" name='address2' placeholder="Address" value="<?php echo e($employment->address2); ?>">
                                </div>
                            </div>
                        </div>
                    </div>			
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="form-group">
						<label class="col-md-3 control-label">City / State / Zip : <span class="star-required">*</span></label>
                        <div class="col-lg-6 col-md-8 col-sm-12 col-xs-12">
                            <div class="row">
                                <div class="col-md-4 fsc-element-margin">
                                    <input type="text" class="form-control fsc-input txtOnly" id="city" value="<?php echo e($employment->city); ?>" name='city' placeholder="City">
                                </div>
                                <div class="col-md-4 col-xs-6 fsc-element-margin">
                                    <select name="stateId" id="stateId" class="form-control fsc-input bfh-states" data-country="countries_states1" data-state="<?php echo e($employment->stateId); ?>">
            							<option value="<?php echo e($employment->stateId); ?>"><?php echo e($employment->stateId); ?></option>
            						</select>
                                </div>
                                <div class="col-md-4 col-xs-6 fsc-element-margin">
                                    <input type="text" class="form-control fsc-input" id="zip" value="<?php echo e($employment->zip); ?>" name='zip' placeholder="Zip" maxlength='5'>
                                </div>
                            </div>
                        </div>
                    </div>			
				</div>
				
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="form-group">
						<label class="col-md-3 control-label">Country : <span class="star-required">*</span></label>
						<div class="col-lg-6 col-md-8 col-sm-12 col-xs-12">
                            <div class="row">
            					<div class="col-md-4 col-sm-6 col-xs-6 fsc-form-col fsc-element-margin">
            						<select name="countryId" id="countries_states1" class="form-control fsc-input bfh-countries" data-country="<?php echo e($employment->countryId); ?>">
            						</select>
            					</div>
        					</div>
    					</div>
    				</div>
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="form-group">
						<label class="col-md-3 control-label">Telephone 1 : <span class="star-required">*</span></label>
                        <div class="col-lg-6 col-md-8 col-sm-12 col-xs-12">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 fsc-element-margin">
                                    <input type="phone" class="form-control fsc-input phone" id="telephoneNo1" value='<?php echo e($employment->telephoneNo1); ?>' name='telephoneNo1' placeholder="(999) 999-9999"  data-inputmask="'alias': 'phone'">
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 fsc-form-col fsc-element-margin">
                                    <select name="telephoneNo1Type" id="telephoneNo11Type" class="form-control fsc-input" > 
                                        <option value='Mobile' <?php if($employment->telephoneNo1Type=='Mobile'): ?> selected <?php endif; ?> >Mobile</option>
                                        <option value='Home' <?php if($employment->telephoneNo1Type=='Home'): ?> selected <?php endif; ?>>Home</option>
                                        <option value='Work' <?php if($employment->telephoneNo1Type=='Work'): ?> selected <?php endif; ?>>Work</option>
                                        <option value='Other' <?php if($employment->telephoneNo1Type=='Other'): ?> selected <?php endif; ?>>Other</option>
                                    </select>
                                </div>
                            </div>
                        </div>
    				</div>
				</div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Telephone 2 : </label>
                        <div class="col-lg-6 col-md-8 col-sm-12 col-xs-12">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 fsc-element-margin">
                                    <input type="phone" class="form-control fsc-input phone" value='<?php echo e($employment->telephoneNo2); ?>' name='telephoneNo2' id="telephoneNo2" placeholder="(999) 999-9999"  data-inputmask="'alias': 'phone'">
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 fsc-element-margin">
                                    <select name="telephoneNo2Type" id="telephoneNo22Type" class="form-control fsc-input" >                          
                                        <option value='Mobile'  <?php if($employment->telephoneNo2Type=='Mobile'): ?> selected <?php endif; ?>>Mobile</option>
                                        <option value='Home'  <?php if($employment->telephoneNo2Type=='Home'): ?> selected <?php endif; ?>>Home</option>
                                        <option value='Work'  <?php if($employment->telephoneNo2Type=='Work'): ?> selected <?php endif; ?>>Work</option>
                                        <option value='Other'  <?php if($employment->telephoneNo2Type=='Other'): ?> selected <?php endif; ?>>Other</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Email : </label>
                        <div class="col-lg-6 col-md-8 col-sm-12 col-xs-12">
                            <div class="row">
                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 fsc-element-margin">
                                    <input type="text" class="form-control fsc-input" value='<?php echo e($employment->email); ?>' readonly id="email" name='email' placeholder="Email Address">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Position : <span class="star-required">*</span></label>
                        <div class="col-lg-6 col-md-8 col-sm-12 col-xs-12">
                            <div class="row">
                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 fsc-element-margin">
                                    <div class="dropdown">
                                        <select id='requiremnetId' name='requiremnetId' class='form-control  fsc-input' >                                                 
                                            <?php $__currentLoopData = $employment1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $emp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value='<?php echo e($emp->id); ?>' <?php if($employment->requiremnetId == $emp->id): ?> selected <?php endif; ?>><?php echo e($emp->position_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
				
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="form-group">
					<label class="col-md-3 control-label"></label>
					<div class="col-lg-5 col-md-7 col-sm-7 col-xs-12 fsc-element-margin">
<p>
<?php if(empty($employment->resume)): ?>
<img src="<?php echo e(asset('public/images/file-not-found.png')); ?>" width="60" alt="">
<?php else: ?>
<a href="<?php echo e(asset('public/resumes/')); ?>/<?php echo e($employment->resume); ?>" target="_blank" class="btn btn-success pull-left">

<?php if(pathinfo($employment->resume, PATHINFO_EXTENSION) == 'doc'): ?>
<i class="fa fa-file" aria-hidden="true"></i>
<?php endif; ?>
<?php if(pathinfo($employment->resume, PATHINFO_EXTENSION) == 'pdf'): ?>
<i class="fa fa-file-pdf-o"></i>
<?php endif; ?>
<?php if(pathinfo($employment->resume, PATHINFO_EXTENSION) == 'docx'): ?>
<i class="fa fa-file-pdf-o"></i>
<?php endif; ?> View Resume</a> <?php endif; ?> 
</p>

	</div>
					</div>
				</div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Date / Day / Time : </label>
                        <div class="col-lg-6 col-md-8 col-sm-12 col-xs-12">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 fsc-element-margin">
                                    <input type="text" readonly class="form-control" id="" value="<?php echo e(date('M-d Y',strtotime($employment->candidate_date))); ?>"  placeholder="Date">
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 fsc-form-col fsc-element-margin">
                                    <input type="text" readonly class="form-control" id="" value="<?php echo e(date('D',strtotime($employment->candidate_date))); ?>"  placeholder="Day">
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 fsc-element-margin">
                                    <input type="text" readonly class="form-control" id="" value="<?php echo e(date('g:i A',strtotime($employment->candidate_date))); ?>"  placeholder="Time">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

				
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label class="col-md-3 control-label"></label>
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-2" style="width:auto;">
                                    <a data-toggle="modal" data-target="#myModal" class="btn btn-primary fsc-form-submit btn_new_save" style="padding:8px 15px;">Convert To Employee</a> 
                                </div>
                                <div class="col-md-2" style="width:auto;">	
                                    <a class="btn btn-danger fsc-form-submit btn_new_cancel" style="padding:8px 15px;" href="https://financialservicecenter.net/fac-Bhavesh-0554/candidate">Cancel</a> 
                                </div>
                            </div>
                        </div>
                    </div>
			    </div>

				
				
				
				
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 3%;">
					<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12"></div>
					<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
						
					</div><br><br><br>
					
					
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
<div class="" id="Register"></div>
				</div>
				
		
          
         </div>
      </div>  
      	</form>
      	</div>
      
      </div>  </div>
      </div>
      </section>
   
<!--</div>-->
 <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Convert To Employee</h4>
        </div>
        <div class="modal-body">
          <div class="col-md-12">
      <div class="card" style="display: inline-block;">
        <div class="card-body">
          <div class="panel with-nav-tabs panel-primary">
            <div class="panel-heading">
              <ul class="nav nav-tabs" id="myTab">
                <li class="active"><a href="#tab1primary" data-toggle="tab" aria-expanded="true">General Info</a></li>
                <li><a data-toggle="tab" href="#tab2primary" class="hvr-shutter-in-horizontal">Hiring Info</a></li>
                <li><a href="#tab3primary" data-toggle="tab" class="hvr-shutter-in-horizontal">Pay Info</a></li>
                <li ><a href="#tab4primary" data-toggle="tab" class="hvr-shutter-in-horizontal" aria-expanded="false">Personal Info</a></li>
                <li><a href="#tab5primary" data-toggle="tab" class="hvr-shutter-in-horizontal">Security Info</a></li>
                <li><a href="#tab6primary" data-toggle="tab" class="hvr-shutter-in-horizontal">Rules</a></li>
                <li><a href="#tab7primary" data-toggle="tab" class="hvr-shutter-in-horizontal">Responsibility</a></li>
              </ul>
            </div>
            <form method="post" id="registrationForm" action="<?php echo e(route('candidate.update',$employment->cid)); ?>" class="form-horizontal" enctype="multipart/form-data">        
<?php echo e(csrf_field()); ?><?php echo e(method_field('PATCH')); ?>

<input id='requiremnetId' type="hidden" name='requiremnetId' class='form-control  fsc-input' value='<?php echo e($employment->requiremnetId); ?>' >                                                 
              <div class="panel-body">
                <div class="tab-content">
                  <div class="tab-pane fade  active in" id="tab1primary">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="Branch">
                          <h1>General Information</h1>
                        </div>
<input type="hidden" class="form-control fsc-input" name='employment_id' id="employment_id" placeholder="First Name" value="<?php echo e($employment->employment_id); ?>">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  " style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                         <label class="fsc-form-label">Employee ID : </label>
                        </div>
                        <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                          <div class="row">
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                             <input type="text" class="form-control fsc-input" name="employee_id" id="employee_id" value="">                             </div>
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                             <label class="fsc-form-label">Status : </label>
                            </div>
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                             <select name="check" id="check" class="form-control fsc-input">                          
                            <option value="0">Inactive</option>                         
                            <option value="1">Active</option>                                                                                 
                          </select>
                            </div>
                          </div>
                        </div>
                      </div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  " style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Name : </label>
                        </div>
                        <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                          <div class="row">
                              <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
														<select class="form-control fsc-input" id="nametype" name="nametype">
														    <option value="">Select</option>
														    <option value="mr">Mr.</option>
														    <option value="mrs">Mrs.</option>
														    <option value="miss">Miss</option>
														</select>
														
													</div>
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                              <input type="text" class="form-control fsc-input" id="firstName" name="firstName" placeholder="First" value="<?php echo e($employment->firstName); ?>">                            </div>
                            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                              <input type="text" maxlength="1" class="form-control fsc-input" id="middleName" name="middleName" placeholder="Middle" value="<?php echo e($employment->middleName); ?>">
                            </div>
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                              <input type="text" class="form-control fsc-input" id="lastName" name="lastName" value="<?php echo e($employment->lastName); ?>" placeholder="Last">
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  " style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Address 1 : </label>
                        </div>
                        <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <input type="text" placeholder="Address 1" class="form-control fsc-input" name="address1" id="address1" value="<?php echo e($employment->address1); ?>">                        </div>
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Address 2 : </label>
                        </div>
                        <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <input type="text" placeholder="Address 2" class="form-control fsc-input" name="address2" id="address2" value="<?php echo e($employment->address2); ?>">
                        </div>
                      </div>
 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  " style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">City/State/Zip : </label>
                        </div>
                        <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <div class="row">
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                              <input type="text" class="form-control fsc-input" id="city" name="city" placeholder="City" value="<?php echo e($employment->city); ?>">                            </div>
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <div class="dropdown" style="margin-top: 1%;">
                                <select name="stateId" id="stateId" class="form-control fsc-input">
                            <option value="<?php echo e($employment->stateId); ?>"><?php echo e($employment->stateId); ?></option>
                                 <option value="ID">ID</option>                                 
                                 <option value="ID">ID</option>
                                  <option value="AK">AK</option>
                                  <option value="AS">AS</option>
                                  <option value="AZ">AZ</option>
                                  <option value="AR">AR</option>
                                  <option value="CA">CA</option>
                                  <option value="CO">CO</option>
                                  <option value="CT">CT</option>
                                  <option value="DE">DE</option>
                                  <option value="DC">DC</option>
                                  <option value="FM">FM</option>
                                  <option value="FL">FL</option>
                                  <option value="GA">GA</option>
                                  <option value="GU">GU</option>
                                  <option value="HI">HI</option>
                                  <option value="ID">ID</option>
                                  <option value="IL">IL</option>
                                  <option value="IN">IN</option>
                                  <option value="IA">IA</option>
                                  <option value="KS">KS</option>
                                  <option value="KY">KY</option>
                                  <option value="LA">LA</option>
                                  <option value="ME">ME</option>
                                  <option value="MH">MH</option>
                                  <option value="MD">MD</option>
                                  <option value="MA">MA</option>
                                  <option value="MI">MI</option>
                                  <option value="MN">MN</option>
                                  <option value="MS">MS</option>
                                  <option value="MO">MO</option>
                                  <option value="MT">MT</option>
                                  <option value="NE">NE</option>
                                  <option value="NV">NV</option>
                                  <option value="NH">NH</option>
                                  <option value="NJ">NJ</option>
                                  <option value="NM">NM</option>
                                  <option value="NY">NY</option>
                                  <option value="NC">NC</option>
                                  <option value="ND">ND</option>
                                  <option value="MP">MP</option>
                                  <option value="OH">OH</option>
                                  <option value="OK">OK</option>
                                  <option value="OR">OR</option>
                                  <option value="PW">PW</option>
                                  <option value="PA">PA</option>
                                  <option value="PR">PR</option>
                                  <option value="RI">RI</option>
                                  <option value="SC">SC</option>
                                  <option value="SD">SD</option>
                                  <option value="TN">TN</option>
                                  <option value="TX">TX</option>
                                  <option value="UT">UT</option>
                                  <option value="VT">VT</option>
                                  <option value="VI">VI</option>
                                  <option value="VA">VA</option>
                                  <option value="WA">WA</option>
                                  <option value="WV">WV</option>
                                  <option value="WI">WI</option>
                                  <option value="WY">WY</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                              <input type="text" maxlength="4" class="form-control fsc-input" id="zip1" name="zip" value="<?php echo e($employment->zip); ?>" placeholder="Zip">                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Country :</label>
                        </div>
                        <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                          <div class="row">
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <div class="dropdown">
                                <select name="countryId" id="countryId" class="form-control fsc-input">  
                                    <option value=''>---Select---</option>
                                    <option value='INDIA' <?php if($employment->countryId=='INDIA'): ?> selected <?php endif; ?>>INDIA</option>
                                    <option value='USA' <?php if($employment->countryId=='USA'): ?> selected <?php endif; ?>>USA</option>
                                </select>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                     
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12   " style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Telephone 1 :</label>
                        </div>
                        <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                          <div class="row">
                            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                              <input type="text" class="form-control fsc-input phone" placeholder="(999) 999-9999" id="telephoneNo1" name="telephoneNo1" value="<?php echo e($employment->telephoneNo1); ?>" data-bv-field="telephoneNo1">                             </div>
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <div class="dropdown" style="margin-top: 1%;">
                                <select name="telephoneNo1Type" id="telephoneNo1Type" class="form-control fsc-input">
                                  
                                        <option value="Mobile" <?php if($employment->telephoneNo1Type=='Mobile'): ?> selected <?php endif; ?>>Mobile</option>
                                      <option value="Resident" <?php if($employment->telephoneNo1Type=='Resident'): ?> selected <?php endif; ?>>Resident</option>
                                      <option value="Office" <?php if($employment->telephoneNo1Type=='Office'): ?> selected <?php endif; ?>>Work</option>
                                      <option value="Other" <?php if($employment->telephoneNo1Type=='Other'): ?> selected <?php endif; ?>>Other</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                              <input type="text" class="form-control fsc-input" readonly="" id="ext1" name="ext1" value="" placeholder="Ext">
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12   " style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Telephone 2 :</label>
                        </div>
                        <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                          <div class="row">
                            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                              <input type="text" class="form-control fsc-input phone" id="telephoneNo2" name="telephoneNo2"placeholder="(999) 999-9999" value="<?php echo e($employment->telephoneNo2); ?>" data-bv-field="telephoneNo2">

<input type="hidden" class="form-control fsc-input" id="resume_1" name='resume_1' value="<?php echo e($employment->resume); ?>" placeholder="Select Document">
                            </div>
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <div class="dropdown" style="margin-top: 1%;">
                                <select name="telephoneNo2Type" id="telephoneNo2Type" class="form-control fsc-input">

                                 <option value="Mobile" <?php if($employment->telephoneNo2Type=='Mobile'): ?> selected <?php endif; ?>>Mobile</option>
                                      <option value="Resident" <?php if($employment->telephoneNo2Type=='Resident'): ?> selected <?php endif; ?>>Resident</option>
                                      <option value="Office" <?php if($employment->telephoneNo2Type=='Office'): ?> selected <?php endif; ?>>Work</option>
                                      <option value="Other" <?php if($employment->telephoneNo2Type=='Other'): ?> selected <?php endif; ?>>Other</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                              <input type="text" class="form-control fsc-input" readonly="" id="ext2" name="ext2" value="" placeholder="Ext">
                            </div>
                          </div>
                        </div>
                      </div>
 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Fax:</label>
                        </div>
                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                          <div class="row">
                            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <div class="dropdown">
                               <input type="tel" class="form-control fsc-input phone"placeholder="(999) 999-9999" data-bv-field="fax" id="fax" name="fax">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Email : </label>
                        </div>
                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <input type="text" class="form-control fsc-input" id="email" readonly="" name="email" value="<?php echo e($employment->email); ?>" placeholder="Email">
                        </div>
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Select Picture : </label>
                        </div>
                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <label class="file-upload btn btn-primary">
                Browse for file ... <input name="photo" style="opecity:0" placeholder="Upload Service Image" id="photo" type="file">
            </label>
                            
                           <br> <img id="blah" src="#" alt="your image" style="width:69px;display:none"/>
                        </div>
                      </div>
                 
                    </div>
                  </div>
                  <div class="tab-pane fade" id="tab2primary">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div class="Branch">
                          <h1>Hiring Information</h1>
                        </div>
                      <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Hire Date : </label>
                        </div>
                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <div class="row">
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col"> 
                              <div class="dropdown" style="margin-top: 1%;">
<input name="hiremonth" type="text" id="hiremonth" class="form-control">
                                
                              </div>
                            </div>
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <label class="fsc-form-label">Termination Date : </label>
                            </div>
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <input name="termimonth" type="text" id="termimonth" class="form-control">
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Note :</label>
                        </div>
                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <textarea name="tnote" id="tnote" class="form-control fsc-input"></textarea>
                            </div>
                            
                          </div>
                        </div>
                      </div>
                      
                      <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Re-Hire Date :</label>
                        </div>
                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <div class="row">
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <div class="dropdown" style="margin-top: 1%;">
<input name="rehiremonth" type="text" value="" id="rehiremonth" class="form-control">
                                
                              </div>
                            </div>
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <div class="dropdown" style="margin-top: 1%;">
<label class="fsc-form-label">Termination Date :</label>
                               
                              </div>
                            </div>
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <div class="dropdown" style="margin-top: 1%;">
<input name="rehireyear" type="text" value="" id="rehireyear" class="form-control">
                                
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
<div class="col-lg-10 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Note :</label>
                        </div>
                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <textarea name="tnote" id="tnote" class="form-control fsc-input"></textarea>
                            </div>
                            
                          </div>
                        </div>
                      </div>
                     
                    </div>
                    <div class="">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="Branch">
                          <h1>Branch / Department Information</h1>
                        </div>
                        <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                          <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">Branch City:</label>
                          </div>
                          <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <div class="row">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                <div class="dropdown" style="margin-top: 1%;">
                                  <select name="branch_city" id="branch_city" class="form-control fsc-input category">
                                    <option value="" >---Select City---</option>
                                  <?php $__currentLoopData = $inventory; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pos): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  
                                  <option value="<?php echo e($pos->city); ?>" ><?php echo e($pos->city); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                  </select>
                                </div>
                              </div>
                            </div>
                          </div>
</div>

 <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                          <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Branch Name:</label>
                          </div>
                          <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <div class="row">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                               <input type="text" readonly="" class="form-control fsc-input" id="branch_name" name="branch_name" placeholder="" value="">
                              </div>
                            </div>
                          </div>
                        </div>                        

                        <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12    " style="margin-top:2%;">
                          <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">Position :</label>
                          </div>
                          <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <div class="row">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                <div class="dropdown" style="margin-top: 1%;">
                                  <select name="position" id="position" class="form-control fsc-input">
                                         <option value="" >---Select Position---</option>
                                  <?php $__currentLoopData = $position; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pos): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  
                                  <option value="<?php echo e($pos->id); ?>" ><?php echo e($pos->position); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                  </select>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
<div class="col-lg-10 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                          <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Note :</label>
                          </div>
                          <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <div class="row">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                               <textarea name="note" id="note" class="form-control fsc-input">765765</textarea>
                              </div>
                            </div>
                          </div>
                        </div>  
                        
                      </div>
                    </div>
                    <div class="">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="Branch">
                          <h1>Review Information</h1>
                        </div>

<div class="review" id="education_fields">


                     


<div class="col-lg-10 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">First Review Days :</label>
                        </div>
                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <div class="row">
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <div class="dropdown" style="margin-top: 1%;">
<input type="text" class="form-control fsc-input" name="first_rev_day[]" id="first_rev_day" onkeypress="return isNumberKey(event)" value="" maxlength="3">
                                
                              </div>
                            </div>
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <div class="dropdown" style="margin-top: 1%;">
<label class="fsc-form-label">First Review Date :</label>
                               
                              </div>
                            </div>
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <div class="dropdown" style="margin-top: 1%;">
<input name="reviewmonth[]" type="text" value="" id="reviewmonth" class="form-control  date_Picker" readonly="readonly">
                                
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>  

                      
                        <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                          <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                            <label class="fsc-form-label">Comments :</label>
                          </div>
                          <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                            <div class="row">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                <textarea name="hiring_comments[]" id="hiring_comments" rows="1" class="form-control fsc-input"></textarea>
                              </div>
                            </div></div>
                            </div>

</div>
<div class="col-md-10">
<a href="#"  onclick="education_fields();" class="ad pull-right"><i class="fa fa-plus" aria-hidden="true"></i> Add Review</a>    
  </div>
                        
                    
                      </div>
                    </div>
                  </div>
                  <div class="tab-pane fade" id="tab3primary">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div class="Branch">
                          <h1>Salary Information</h1>
                        </div>
                      <div class="col-md-10 col-sm-8 col-xs-12" style="margin-top:2%;"> 
                        
                        <div class="form-group">
                          <label class="control-label col-md-4">Pay Method :</label>
                          <div class="col-md-8">
                            <select name="pay_method" id="pay_method" class="form-control">
                              <option value="Salary" selected="">Salary</option>
                              <option value="Hourly">Hourly</option>
                            </select>
                          </div>
                        </div>
 <div class="form-group">
                          <label class="control-label col-md-4">Pay Duration :</label>
                          <div class="col-md-8">
                            <select name="pay_frequency" id="pay_frequency" class="form-control">
                              <option value="Weekly">Weekly</option>
                              <option value="Bi-Weekly" selected="">Bi-Weekly</option>
                              <option value="Bi-Monthly">Semi-Monthly</option>
                              <option value="Monthly">Monthly</option>
                            </select>
                          </div>
                        </div>
 <div class="fieldGroup">
  <div id="field43">
			<div class="form-group">                        
                          <label class="control-label col-md-4">Pay Rate  : </label>                       
                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <div class="row">
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col"> 
                              <div class="dropdown" style="margin-top: 1%;">
<input name="pay_scale[]" value="" type="text" id="pay_scale" class="form-control">   
<input name="employee[]" value="" type="hidden" id="employee" class="form-control">                               
                              </div>
                            </div>
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <label class="fsc-form-label">Effective Date : </label>
                            </div>
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <input name="effective_date[]" value=""  type="text" id="effective_date" class="form-control date_Picker">
                            </div>
                          </div>
                        </div>
                      </div>
				

                        <div class="form-group">
                          <label class="control-label col-md-4">Note :</label>
                          <div class="col-md-8">
<textarea id="fields" name="fields[]" class="form-control fsc-input">456546546</textarea>                          
                          </div>
                        </div>

                      </div>					  
					 				  
					  	
</div>
                      </div>

<div class="form-group">
  <div class="col-md-10">
   
<a href="javascript:void(0)" class="addMore pull-right"><i class="fa fa-plus" aria-hidden="true"></i> Add Pay</a>
  </div>
</div>
                      
                    </div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div class="Branch">
                          <h1>Taxes Information</h1>
                        </div>
                      <div class="col-md-10 col-sm-8 col-xs-12" style="margin-top:2%;"> 
                        
                        <div class="form-group">
                          <label class="control-label col-md-4">Filling Status :</label>
                          <div class="col-md-8">
                            <select name="filling_status" id="filling_status" class="form-control">
                              <option value="" selected="">Single</option>
                              <option value="Married File Jointly">Married File Jointly</option>
                              <option value="Married File Separately">Married File Separately</option>
                              <option value="Head of Household">Head of Household</option>
                            </select>
                          </div>
                        </div>
			<div class="form-group">                        
                          <label class="control-label col-md-4">Fedral Claim : </label>                       
                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <div class="row">
                            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col"> 
                              <div class="dropdown" style="margin-top: 1%;">
<input name="fedral_claim" value="" type="text" id="fedral_claim" maxlength="3" onkeypress="return isNumberKey(event)" class="form-control">                                  
                              </div>
                            </div>
                            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <label class="fsc-form-label">Additional Withholding : </label>
                            </div>
                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <input name="additional_withholding" onkeypress="return isNumberKey(event)" value="" type="text" id="additional_withholding" class="form-control">
                            </div>
								<div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
								     <label class="file-upload btn btn-primary">
														<i class="fa fa-upload"></i> <input name="additional_attach" style="opecity:0" placeholder="Upload Service Image" id="additional_attach" type="file">
														</label>
                              
							  
                            </div>
                          </div>
                        </div>
					
                      </div>
<div class="form-group">
                        
                          <label class="control-label col-md-4">State Claim  : </label>
                       
                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <div class="row">
                            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col"> 
                              <div class="dropdown" style="margin-top: 1%;">
<input name="state_claim" value="3" maxlength="3" type="text" id="state_claim" onkeypress="return isNumberKey(event)" class="form-control">  
                                
                              </div>
                            </div>
                            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <label class="fsc-form-label">Additional Withholding : </label>
                            </div>
                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <input name="additional_withholding_1" onkeypress="return isNumberKey(event)" maxlength="3" value="55.00" type="text" id="additional_withholding_1" class="form-control">
                            </div>
							<div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
							    <label class="file-upload btn btn-primary">
														<i class="fa fa-upload"></i>  <input name="additional_attach_1" style="opecity:0" placeholder="Upload Service Image" id="additional_attach_1" type="file">
														</label>

							
                            </div>
                          </div>
                        </div>
                      </div>
			<div class="form-group">
                        
                          <label class="control-label col-md-4">Local Claim  : </label>
                       
                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <div class="row">
                            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col"> 
                              <div class="dropdown" style="margin-top: 1%;">
<input name="local_claim" value="3" onkeypress="return isNumberKey(event)" type="text" id="local_claim" class="form-control">  
                                
                              </div>
                            </div>
                            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <label class="fsc-form-label">Additional Withholding : </label>
                            </div>
                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                            <input name="additional_withholding_2" value="3" onkeypress="return isNumberKey(event)" type="text" id="additional_withholding_2" class="form-control"> 
                            </div>
							<div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <input name="additional_attach_2" value="" type="file" id="additional_attach_2" class="form-control">
                               <label class="file-upload btn btn-primary">
														<i class="fa fa-upload"></i>  <input name="additional_attach_2" style="opecity:0" placeholder="Upload Service Image" id="additional_attach_2" type="file">
														</label>
							
                            </div>
                          </div>
                        </div>
                      </div>		
                      </div>
                    </div>	
                  </div>
                  <div class="tab-pane fade" id="tab4primary">
                    <div class="row">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12    " style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Gender :</label>
                        </div>
                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <div class="row">
                   
 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <div class="dropdown" style="margin-top: 1%;">
                                <select name="gender" id="gender" class="form-control fsc-input">
                                    <option value="">Select </option>
                                  <option value="Male">Male</option>
                                  <option value="Female">Female</option>
                                </select>
                              </div>
 </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Marital Status :</label>
                        </div>
                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <div class="dropdown" style="margin-top: 1%;">
                                <select name="marital" id="marital" class="form-control fsc-input">
                                    <option value="">Select </option>
                                  <option value="Married">Married</option>
                                  <option value="UnMarried" >UnMarried</option>
                                </select>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Date Of Birth : </label>
                        </div>
                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <div class="dropdown" style="margin-top: 1%;">

<input type="text" class="form-control fsc-input" name="month" readonly="" id="month" value="">
                               
                              </div>
                            </div>
                           
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Id Proof 1 :</label>
                        </div>
                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <div class="row">
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <div class="dropdown" style="margin-top: 1%;">
                                <select name="pf1" class="form-control fsc-input" id="pf1">
                                  <option value="">Select </option>
                                  <option value="Voter Id">Voter ID</option>
                                  <option value="Driving Licence">Driving Licence</option>
                                  <option value="State ID">State ID</option>
                                  <option value="Pan Card">Pan Card</option>
                                  <option value="Pass Port">Pass Port</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-form-col ">
                              <div class="dropdown" style="margin-top: 1%;">
                                <input name="pfid1" placeholder="Upload Service Image" class="form-control fsc-input" id="pfid1" type="file"><img id="blah-1" src="#" alt="your image" style="width:69px;margin-top:10px;display:none"/>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12    " style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Id Proof 2 :</label>
                        </div>
                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <div class="row">
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <div class="dropdown" style="margin-top: 1%;">
                                <select name="pf2" class="form-control fsc-input" id="pf2">
                                  <option value="">Select </option>
                                 <option value="Voter Id">Voter ID</option>
                                  <option value="Driving Licence">Driving Licence</option>
                                  <option value="State ID">State ID</option>
                                  <option value="Pan Card">Pan Card</option>
                                  <option value="Pass Port">Pass Port</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-form-col ">
                              <div class="dropdown" style="margin-top: 1%;">
                                <input name="pfid2" placeholder="Upload Service Image" class="form-control fsc-input" id="pfid2" type="file"><img id="blah-2" src="#" alt="your image" style="width:69px;margin-top:10px;display:none"/>
                              </div>
                            </div>
                          </div>

                        </div>
                      </div>


<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Resume :</label>
                        </div>
                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <div class="row">
                            
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-form-col ">
                              <div class="dropdown" style="margin-top: 1%;">
                                  <label class="file-upload btn btn-primary">
                Browse for file ... <input name="resume" style="opecity:0" placeholder="Upload Service Image" id="resume" type="file">
            </label>
                               

 <input name="resume_1" placeholder="Upload Service Image" value="<?php echo e($employment->resume); ?>" class="form-control fsc-input" id="resume_1" type="hidden">
                              </div>
                            </div>
                          </div>

                        </div>
                      </div>
 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Type of Agreement :</label>
                        </div>
                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <div class="row">
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <div class="dropdown" style="margin-top: 1%;">
                                <select name="type_agreement" class="form-control fsc-input" id="type_agreement">
                                  <option value="">Select</option>
                                		<option value="Hiring Letter">Hiring Letter</option>
																<option value="Employment Agreement">Employment Agreement</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-form-col ">
                              <div class="dropdown" style="margin-top: 1%;">
                                <input name="agreement" placeholder="Upload Service Image" class="form-control fsc-input" id="agreement" type="file">
<img id="blah-3" src="#" alt="your image" style="width:69px;margin-top:10px;display:none"/>

                              </div>
                            </div>
                          </div>

                        </div>
                      </div>
                      <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <div class="Branch">
                            <h1>Emergency Contact Info</h1>
                          </div>
                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  " style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Contact Person Name : </label>
                        </div>
                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                          <div class="row">
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                              <input type="text" class="form-control fsc-input" id="firstName_1" name="firstName_1" placeholder="First" value="">                            </div>
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                              <input type="text" maxlength="1" class="form-control fsc-input" id="middleName_1" name="middleName_1" placeholder="Middle" value="">
                            </div>
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                              <input type="text" class="form-control fsc-input" id="lastName_1" name="lastName_1" value="" placeholder="Last">
                            </div>
                          </div>
                        </div>
                      </div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Address 1 : </label>
                        </div>
                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <input type="text" placeholder="Address 1" class="form-control fsc-input" name="address11" id="address11" value="">                        </div>
                      </div>
                          
                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                              <label class="fsc-form-label">Address 2:</label>
                            </div>
                            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                              <input type="text" class="form-control fsc-input" name="eaddress1" id="eaddress1" value="">
                            </div>
                          </div>
                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12    " style="margin-top:2%;">
                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                              <label class="fsc-form-label">City/State/Zip : </label>
                            </div>
                            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                              <div class="row">
                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                  <input type="text" class="form-control fsc-input" id="ecity" name="ecity" placeholder="City" value="">
                                </div>
                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                  <div class="dropdown" style="margin-top: 1%;">
                                    <select name="estate" id="estate" class="form-control fsc-input">
                                      <option value="">Select</option>
                                      <option value="ID">ID</option>
                                      <option value="AK">AK</option>
                                      <option value="AS">AS</option>
                                      <option value="AZ">AZ</option>
                                      <option value="AR">AR</option>
                                      <option value="CA">CA</option>
                                      <option value="CO">CO</option>
                                      <option value="CT">CT</option>
                                      <option value="DE">DE</option>
                                      <option value="DC">DC</option>
                                      <option value="FM">FM</option>
                                      <option value="FL">FL</option>
                                      <option value="GA">GA</option>
                                      <option value="GU">GU</option>
                                      <option value="HI">HI</option>
                                      <option value="ID">ID</option>
                                      <option value="IL">IL</option>
                                      <option value="IN">IN</option>
                                      <option value="IA">IA</option>
                                      <option value="KS">KS</option>
                                      <option value="KY">KY</option>
                                      <option value="LA">LA</option>
                                      <option value="ME">ME</option>
                                      <option value="MH">MH</option>
                                      <option value="MD">MD</option>
                                      <option value="MA">MA</option>
                                      <option value="MI">MI</option>
                                      <option value="MN">MN</option>
                                      <option value="MS">MS</option>
                                      <option value="MO">MO</option>
                                      <option value="MT">MT</option>
                                      <option value="NE">NE</option>
                                      <option value="NV">NV</option>
                                      <option value="NH">NH</option>
                                      <option value="NJ">NJ</option>
                                      <option value="NM">NM</option>
                                      <option value="NY">NY</option>
                                      <option value="NC">NC</option>
                                      <option value="ND">ND</option>
                                      <option value="MP">MP</option>
                                      <option value="OH">OH</option>
                                      <option value="OK">OK</option>
                                      <option value="OR">OR</option>
                                      <option value="PW">PW</option>
                                      <option value="PA">PA</option>
                                      <option value="PR">PR</option>
                                      <option value="RI">RI</option>
                                      <option value="SC">SC</option>
                                      <option value="SD">SD</option>
                                      <option value="TN">TN</option>
                                      <option value="TX">TX</option>
                                      <option value="UT">UT</option>
                                      <option value="VT">VT</option>
                                      <option value="VI">VI</option>
                                      <option value="VA">VA</option>
                                      <option value="WA">WA</option>
                                      <option value="WV">WV</option>
                                      <option value="WI">WI</option>
                                      <option value="WY">WY</option>
                                    </select>
                                  </div>
                                </div>
                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                  <input type="text" class="form-control fsc-input" id="ezipcode" name="ezipcode" value="" placeholder="Zip">
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  " style="margin-top:2%;">
                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                              <label class="fsc-form-label">Telephone 1 :</label>
                            </div>
                            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <div class="row">
                                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                  <input type="text" class="form-control fsc-input" id="etelephone1" name="etelephone1" placeholder="(000) 000-0000" value="">
                                </div>
                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                  <div class="dropdown" style="margin-top: 1%;">
                                    <select name="eteletype1" id="eteletype1" class="form-control fsc-input">
                                         <option value="">Select</option>
                                        <option value="Office">Work</option>
															<option value="Resident">Resident</option>
															<option value="Cell" >Cell</option>
															<option value="Other">Other</option>
                                     
                                    </select>
                                  </div>
                                </div>
                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                  <input type="text" class="form-control fsc-input" value="" readonly="" id="eext1" name="eext1" placeholder="Ext">
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  " style="margin-top:2%;">
                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                              <label class="fsc-form-label">Telephone 2 :</label>
                            </div>
                            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                              <div class="row">
                                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                  <input type="text" class="form-control fsc-input" id="etelephone2" name="etelephone2" placeholder="(000) 000 0000" value="">
                                </div>

                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                  <div class="dropdown" style="margin-top: 1%;">
                                    <select name="eteletype2" id="eteletype2" class="form-control fsc-input">
                                     <option value="">Select</option>
                                                           <option value="Office">Work</option>
															<option value="Resident">Resident</option>
															<option value="Cell">Cell</option>
															<option value="Other">Other</option>
                                    </select>
                                  </div>
                                </div>
                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                  <input type="text" class="form-control fsc-input" value="" readonly="" id="eext2" name="eext2" placeholder="Ext">
                                </div>
                              </div>
                            </div>
                          </div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                              <label class="fsc-form-label">Fax :</label>
                            </div>
                            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                              <input type="text" class="form-control fsc-input" name="efax" id="efax" value="" placeholder="(000) 000 0000">
                            </div>
                          </div>


<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                              <label class="fsc-form-label">E-mail :</label>
                            </div>
                            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                              <input type="text" class="form-control fsc-input" name="eemail" id="eemail" value="">
                            </div>
                          </div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                              <label class="fsc-form-label">Relationship :</label>
                            </div>
                            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                              <input type="text" class="form-control fsc-input" name="relation" id="relation" value="">
                            </div>
                          </div>
                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                              <label class="fsc-form-label">Note For Emergency :</label>
                            </div>
                            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                              <textarea name="comments1" id="comments1" rows="1" class="form-control fsc-input"></textarea>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="tab-pane fade" id="tab5primary">
                    <div class="row">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">User Name :</label>
                        </div>
                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <input id="uname" class="form-control fsc-input" placeholder="User Name" value="" readonly="" name="uname" type="text">
                        </div>
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Password :</label>
                        </div>
                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <input placeholder="Password" class="form-control fsc-input" id="password" name="password" value="" readonly="" type="password">
                          <span toggle="#password" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                        </div>
                      </div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Reset Days : </label>
                        </div>
                        <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <select name="reset" id="reset" class="form-control fsc-input" readonly="">
                            <option value="">Select</option>
                            <option value="30">30</option>
                            <option value="60">60</option>
                            <option value="90">90</option>
                            <option value="120">120</option>                            
                          </select>
                        </div>
                        <div class="col-md-2">  <input name="reset_date" type="text" id="reset_date" class="form-control"></div>
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Question 1 : </label>
                        </div>
                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <select name="question1" id="question1" class="form-control fsc-input" readonly="">
                            <option value="">Select</option>
                            <option value="What was your favorite place to visit as a child?">What was your favorite place to visit as a child?</option>
                            <option value="Who is your favorite actor, musician, or artist?">Who is your favorite actor, musician, or artist?</option>
                            <option value="What is the name of your favorite pet?">What is the name of your favorite pet?</option>
                            <option value="In what city were you born?" selected="selected">In what city were you born?</option>
                            <option value="What is the name of your first school?">What is the name of your first school?</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Answer 1 :</label>
                        </div>
                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <input name="answer1" value="" placeholder="" class="form-control fsc-input" id="answer1" readonly="" type="text">
                        </div>
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Question 2 :</label>
                        </div>
                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <select name="question2" id="question2" class="form-control fsc-input" readonly="">
                            <option value="">Select</option>
                            <option value="What is your favorite movie?">What is your favorite movie?</option>
                            <option value="What was the make of your first car?">What was the make of your first car?</option>
                            <option value="What is your favorite color?" selected="selected">What is your favorite color?</option>
                            <option value="What is your father's middle name?">What is your fathers middle name?</option>
                            <option value="What is the name of your first grade teacher?">What is the name of your first grade teacher?</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Answer 2 :</label>
                        </div>
                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <input name="answer2" value="" placeholder="" class="form-control fsc-input" id="answer2" readonly="" type="text">
                        </div>
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Question 3 :</label>
                        </div>
                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <select name="question3" id="question3" class="form-control fsc-input" readonly="">
                            <option value="">Select</option>
                            <option value="What was your high school mascot?">What was your high school mascot?</option>
                            <option value="Which is your favorite web browser?">Which is your favorite web browser?</option>
                            <option value="In what year was your father born?">In what year was your father born?</option>
                            <option value="What is the name of your favorite childhood friend?" selected="selected">What is the name of your favorite childhood friend?</option>
                            <option value="What was your favorite food as a child?">What was your favorite food as a child?</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;">
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                          <label class="fsc-form-label">Answer 3 :</label>
                        </div>
                        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                          <input name="answer3" value="" placeholder="" class="form-control fsc-input" id="answer3" readonly="" type="text">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="tab-pane fade" id="tab6primary">
                    <div class="row">
	<div class="Branch">
											<h1>Employee Rules</h1>
										</div>
										
										<div class="responsibility-8">
										
											
											<div id="responsibility">
												<ul>
												    <?php $__currentLoopData = $rules; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rule): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
												    <?php if($rule->type=='Rules'): ?>
												    <li><b><?php echo e($rule->title); ?> : </b><br><?php echo $rule->rules; ?></li>
												    <?php endif; ?>
												    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
												   <li>
	<label for="terms" style="float:left">	<input <?php if(empty($empfsc->rule_status)): ?>  <?php else: ?> <?php if($empfsc->rule_status=='2'): ?> checked <?php endif; ?>  <?php endif; ?> type="checkbox" id="terms" name="terms" value="2"  disabled>  
		I read an acknowledge the rules of Financial Service Center and I will follow as per company's rule.</label>
</li>
												</ul>
											</div>
										
										
										</div>
                      
                    </div>
                  </div>
                  <div class="tab-pane fade" id="tab7primary">
                    <div class="row">
	<div class="Branch">
											<h1>Work Responsibility</h1>
										</div>
										<div class="responsibility">
											<div class="form-group">
												<div class="col-md-9">
													
													<table id="example" class="table table-striped table-bordered customers" style="width:100%">
<thead>
<tr>
<th>No.</th>
<th style="width:80%;">Responsibility</th>
<th style="width:20%;">Checked</th>
</tr>
</thead>
<tbody>
     <?php $__currentLoopData = $rules; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rule): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
												    <?php if($rule->type=='Resposibilty' && $rule->employee_id==$emp->id): ?>
											<tr>
<td><?php echo e($loop->index+1); ?></td>
<td><?php echo $rule->rules; ?></td>
<td><?php if($rule->status=='2'): ?> checked <?php else: ?> Uncheck <?php endif; ?></td>
</tr>
												    <?php endif; ?>
												    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</tbody>
</table>
												</div>
											</div>
										
										
										</div>
                      
                    </div>
                  </div>
                  
                  
                </div>
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:3%;">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="btn-center">
                    <button type="submit" class="btn btn-primary  fsc-form-submit class=" hvr-shutter-in-horizontal""="">Save</button>&nbsp;&nbsp;&nbsp;
                    <a class="btn btn-primary  fsc-form-submit class=" hvr-shutter-in-horizontal""="">Cancel</a>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

<!-- copy of input fields group -->
<div class="form-group fieldGroupCopy" style="display: none;">
   <div class="">    
  
<input type="hidden" class="form-control fsc-input" name="employee[]" id="employee" value="">
      <div class="form-group"><label class="control-label col-md-4">Pay Rate  : </label><div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin"><div class="row"><div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col"><div class="dropdown" style="margin-top: 1%;"><input name="pay_scale[]" value="" type="text" id="pay_scale" class="form-control" /></div></div><div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col"><label class="fsc-form-label">Effective Date : </label></div><div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col"><input name="effective_date[]" value="" type="text" id="effective_date" class="form-control date_Picker" /></div></div></div></div><div class="form-group"><label class="control-label col-md-4">Note :</label><div class="col-md-8"><textarea id="fields" name="fields[]" class="form-control fsc-input"></textarea></div></div>
<div class="pull-right" style="margin-top: 10px;"> 
         <a href="javascript:void(0)" class="btn btn-danger remove"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
      </div>
      </div>
<div class="clearfix"></div>
   </div>
</div>



<!-- copy of input fields group -->
<div class="form-group fieldGroupCopy-2" style="display: none;">
   <div class=""> 
<input type="hidden" class="form-control fsc-input" name="work[]" id="work" value="">     
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-top:2%;"><div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row"><label class="fsc-form-label">Work Responsibility : </label></div><div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin"><input type="text" class="form-control fsc-input" name="work_responsibility[]" id="work_responsibility" value=""></div>
<div class="pull-right col-lg-5" style="margin-top: 10px;"> 
         <a href="javascript:void(0)" class="btn btn-danger remove"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
      </div>
      </div>
<div class="clearfix"></div>
   </div>
</div>
<!-- copy of input fields group -->
<div class="form-group fieldGroupCopy-3" style="display: none;">
   <div class="">      

      <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;"><div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row"><label class="fsc-form-label">First Review Day :</label></div><div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin"><div class="row"><div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col"><div class="dropdown" style="margin-top: 1%;"><input type="text" class="form-control fsc-input" name="first_rev_day[]"   maxlength="3" onkeypress="return isNumberKey(event)" id="first_rev_day" value=""></div></div><div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col"><div class="dropdown" style="margin-top: 1%;"><label class="fsc-form-label">First Review Date :</label></div></div><div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col"><div class="dropdown" style="margin-top: 1%;"><input name="reviewmonth[]" readonly type="text" value="" id="reviewmonth" class="form-control" readonly="readonly"></div></div></div></div></div><div class="col-lg-10 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;"><div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row"><label class="fsc-form-label">Comments :</label></div><div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin"><div class="row"><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-col"><textarea name="hiring_comments[]" id="hiring_comments" rows="1" class="form-control fsc-input"></textarea></div></div></div></div>

<div class="pull-right col-lg-4" style="margin-top: 10px;"> 
         <a href="javascript:void(0)" class="btn btn-danger remove"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
      </div>
      </div>
<div class="clearfix"></div>
   </div>
</div>
<script>
//$("#telephoneNo1").mask("(999) 999-9999");
$(".ext").mask("999");
//$("#first_rev_day").mask("999");
//$("#telephoneNo2").mask("(999) 999-9999");
//$("#mobile_no").mask("(999) 999-9999");
$(".usapfax").mask("(999) 999-9999");
$("#zip").mask("9999");

   //$("#telephoneNo1").mask("(999) 999-9999");
   $(".ext").mask("999");
   $("#ext1").mask("999");
   $("#ext2").mask("999");
$("#eext1").mask("999");
   $("#eext2").mask("999");
 //  $("#telephoneNo2").mask("(999) 999-9999");
 //  $("#mobile_no").mask("(999) 999-9999");
   //$("#fax").mask("(999) 999-9999");
  /// $("#etelephone2").mask("(999) 999-9999");
   //$("#etelephone1").mask("(999) 999-9999");
 $("#computer_ip").mask("999.999.999.999");
//$("#efax").mask("(999) 999-9999");
   $("#zip").mask("9999");
 //$("#zip1").mask("9999");

 $("#ezipcode").mask("9999");
</script>
<script>
var date = $('#ext1').val();
$('#telephoneNo1Type').on('change', function() {

if(this.value=='Office')
{
document.getElementById('ext1').removeAttribute('readonly');
$('#ext1').val();
}
else
{
document.getElementById('ext1').readOnly =true;
$('#ext1').val('');
}
})
</script>
<script>
var dat1 = $('#ext2').val();
$('#telephoneNo2Type').on('change', function() {

if(this.value=='Office')
{
document.getElementById('ext2').removeAttribute('readonly');
$('#ext2').val(); 
}
else
{
document.getElementById('ext2').readOnly =true;
$('#ext2').val('');
}
})
</script>

<script>
$('#eteletype2').on('change', function() {

if(this.value=='Office')
{
$('#eext2').val();
document.getElementById('eext2').removeAttribute('readonly');
}
else
{
document.getElementById('eext2').readOnly =true;
$('#eext2').val('');
}
})
</script>
<script>
var dat2 = $('#eext1').val();
$('#eteletype1').on('change', function() {
if(this.value=='Office')
{
document.getElementById('eext1').removeAttribute('readonly');
$('#eext1').val();
}
else
{

document.getElementById('eext1').readOnly =true;
  $('#eext1').val('');
}
})
</script>
<script>
 

function truncateDate(date) {
  return new Date(date.getFullYear(), date.getMonth(), date.getDate());
}
 $(document).ready(function() {
  var dateInput = $('input[name="termimonth"]'); // Our date input has the name "date"
  var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : 'body';
  dateInput.datepicker({
    format: 'M-dd-yyyy',
    container: container,
    todayHighlight: true,
    autoclose: true,
    startDate: truncateDate(new Date()) // <-- THIS WORKS
  });

  $('#termimonth').datepicker('setStartDate', truncateDate(new Date()));
 // <-- SO DOES THIS
});

function truncateDate(date) {
  return new Date(date.getFullYear(), date.getMonth(), date.getDate());
}


 $(document).ready(function() {
  var dateInput = $('input[name="rehiremonth"]'); // Our date input has the name "date"
  var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : 'body';
  dateInput.datepicker({
    format: 'M-dd-yyyy',
    container: container,
    todayHighlight: true,
    autoclose: true,
    startDate: truncateDate(new Date()) // <-- THIS WORKS
  });

  $('#rehiremonth').datepicker('setStartDate', truncateDate(new Date()));
 // <-- SO DOES THIS
});

function truncateDate(date) {
  return new Date(date.getFullYear(), date.getMonth(), date.getDate());
}
 $(document).ready(function() {
  var dateInput = $('input[name="rehireyear"]'); // Our date input has the name "date"
  var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : 'body';
  dateInput.datepicker({
    format: 'M-dd-yyyy',
    container: container,
    todayHighlight: true,
    autoclose: true,
    startDate: truncateDate(new Date()) // <-- THIS WORKS
  });

  $('#rehireyear').datepicker('setStartDate', truncateDate(new Date()));
 // <-- SO DOES THIS
});

function truncateDate(date) {
  return new Date(date.getFullYear(), date.getMonth(), date.getDate());
}
$(document).ready(function() {
  var dateInput = $('input[name="month"]'); // Our date input has the name "date"
  var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : 'body';
  dateInput.datepicker({
    format: 'M-dd-yyyy',
    container: container,
    todayHighlight: true,
    autoclose: true,
    startDate: truncateDate(new Date()) // <-- THIS WORKS
  });

  $('#month').datepicker('setStartDate', truncateDate(new Date()));
 // <-- SO DOES THIS
});


 function truncateDate(date) {
  return new Date(date.getFullYear(), date.getMonth(), date.getDate());
}

</script>
<script type="text/javascript">
$(function() {
             
            $(document).on("click",".date_Picker",function(){        
                 
                    $(this).datepicker({                        
                            changeMonth: true,
                            changeYear: true,
                            format: 'M-dd-yyyy'                       
                        }).datepicker("show");
                });
                 
    });
 
</script>
<script>
   $(document).ready(function(){
       //group add limit
       var maxGroup = 120;
       
       //add more fields group
       $(".addMore").click(function(){
           if($('body').find('.fieldGroup').length < maxGroup){
               var fieldHTML = '<div class="fieldGroup">'+$(".fieldGroupCopy").html()+'</div>';
               $('body').find('.fieldGroup:last').after(fieldHTML);
               
           }else{
               alert('Maximum '+maxGroup+' Persons are allowed.');
           }
       });
       
       //remove fields group
       $("body").on("click",".remove",function(){ 
           $(this).parents(".fieldGroup").remove();
       });
   });
</script>
<script>
   $(document).ready(function(){
       //group add limit
       var maxGroup = 120;
       
       //add more fields group
       $(".add").click(function(){
           if($('body').find('.responsibility').length < maxGroup){
               var fieldHTML = '<div class="responsibility">'+$(".fieldGroupCopy-2").html()+'</div>';
               $('body').find('.responsibility:last').after(fieldHTML);
           }else{
               alert('Maximum '+maxGroup+' Persons are allowed.');
           }
       });
       
       //remove fields group
       $("body").on("click",".remove",function(){ 
           $(this).parents(".responsibility").remove();
       });
   });
</script>
<!--<script>
   $(document).ready(function(){
       //group add limit
       var maxGroup = 120;
       var addrowclass = 0;
       //add more fields group
       $(".ad").click(function(){
            addrowclass++;
           if($('body').find('.review').length < maxGroup){
               var fieldHTML = '<div class="review">'+$(".fieldGroupCopy-3").html()+'</div>';
               $('body').find('.review:last').after(fieldHTML);
                 nextHtml.attr('id', 'reviewmonth' + addrowclass);

               //$('#reviewmonth').addClass('one');
           }else{
               alert('Maximum '+maxGroup+' Persons are allowed.');
           }
       });
       
       //remove fields group
       $("body").on("click",".remove",function(){ 
           $(this).parents(".review").remove();
       });
   });
</script>-->

<script>
var room = 1;
function education_fields() {
 
    room++;
    var objTo = document.getElementById('education_fields')
    var divtest = document.createElement("div");
	divtest.setAttribute("class", "removeclass"+room);
	var rdiv = 'removeclass'+room;
    divtest.innerHTML = '<div class="col-lg-10 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;"><div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row"><label class="fsc-form-label">'+room+' Review Days :</label></div><div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin"><div class="row"><div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col"><div class="dropdown" style="margin-top: 1%;"><input type="text"  maxlength="3" onkeypress="return isNumberKey(event)" class="form-control fsc-input" name="first_rev_day[]" id="first_rev_day'+ room +'" value=""></div></div><div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col"><div class="dropdown" style="margin-top: 1%;"><label class="fsc-form-label">First Review Date :</label></div></div><div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col"><div class="dropdown" style="margin-top: 1%;"><input name="reviewmonth[]" readonly type="text" value="" id="reviewmonth'+ room +'" class="form-control date_Picker" readonly="readonly"></div></div></div></div></div><div class="col-lg-10 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;"><div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row"><label class="fsc-form-label">Comments :</label></div><div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin"><div class="row"><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-col"><textarea name="hiring_comments[]" id="hiring_comments'+ room +'" rows="1" class="form-control fsc-input"></textarea></div></div></div></div><BR><DIV CLASS="col-lg-9 col-md-12"><div class="input-group-btn"> <button class="btn btn-danger pull-right" type="button" onclick="remove_education_fields('+ room +');"> <span class="fa fa-trash" aria-hidden="true"></span> </button></div></div>';
$('#reviewmonth+ room +').datepicker('setStartDate', truncateDate(new Date()));
    
    objTo.appendChild(divtest)
}
   function remove_education_fields(rid) {
	   $('.removeclass'+rid).remove();
   }
</script>


<SCRIPT language=Javascript>
      <!--
      function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
      }
      //-->

jQuery.fn.getNum = function() {
    var val = $.trim($(this).val());
    if(val.indexOf(',') > -1) {
        val = val.replace(',', '.');
    }
    var num = parseFloat(val);
    var num = num.toFixed(2);
    if(isNaN(num)) {
        num = '';
    }
    return num;
}
$(function() {

    $('#additional_withholding,#additional_withholding_1,#additional_withholding_2,#pay_scale').blur(function() {
        $(this).val($(this).getNum());
    });

});
	$('#employee_id').mask('999-999-9999');
$('input[name="employee_id"]').focusout(function() {
   $('input[name="employee_id"]').val( this.value.toUpperCase() );
});
</script>




<script>
$.ajaxSetup({
    headers:
    {
        'X-CSRF-Token': $('input[name="_token"]').val()
    }
});
  $(document).ready(function() {
 
$('#hiremonth').datepicker({
            format: 'M-dd-yyyy',
            assumeNearbyYear: true,
            autoclose: 'true',
            //startView: '',
            todayHighlight: true,
            calendarWeeks: true,
            daysOfWeekHighlighted: '0,6',
            weekStart: '1'
        })
        .on('changeDate', function(e) {
            // Revalidate the date field
            $('#registrationForm').formValidation('revalidateField', 'hiremonth');
        });




    $('#registrationForm').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },

		fields: {

employee_id: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Your Employee Id'
					},				
                   employee_idAddress: {
                        message: 'Please Enter Your Valid Employee Id'
					},
					remote: {
						url: "<?php echo e(URL::to('/emp_id1')); ?>",
						data: function(validator) {
							return {
								email: validator.getFieldElements('email').val()
							}
						},
						message: 'This Email Id Already exit.'
					}
						
                }
            },

            firstName: {
                validators: {
                        stringLength: {
                        min: 1,
                    },
                        notEmpty: {
                        message: 'Please Enter Your First Name'
					},
					regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'The First Name can consist of alphabetical characters and spaces only'
                    }
                }
            },
			middleName: {
                validators: {
                     stringLength: {
                        min: 1,
                    },
                    notEmpty: {
                        message: 'Please Enter Your Middle Name'
                    },
					regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'The Middle name can consist of alphabetical characters and spaces only'
                    }
                }
            },
             lastName: {
                validators: {
                     stringLength: {
                        min: 1,
                    },
                    notEmpty: {
                        message: 'Please Enter Your Last Name'
					},
					
					regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'The Last name can consist of alphabetical characters and spaces only'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Your Email Address'
					},				
                    emailAddress: {
                        message: 'Please Enter Your Valid Email Address'
					},					
				
						
                }
            },
            address1: {
                validators: {
                     stringLength: {
						min: 1,
						message: 'Please Enter Your 8 Charactor'
                    },
                    notEmpty: {
                        message: 'Please Enter Your Address'
                    }
                }
            },
			address1: {
                validators: {
                     stringLength: {
						min: 1,
						message: 'Please Enter Your 8 Charactor'
                    },
                    notEmpty: {
                        message: 'Please Enter Your Address'
                    }
                }
            },
            city: {
                validators: {
                     stringLength: {
						min: 1,
						
                    },
                    notEmpty: {
                        message: 'Please Enter Your City'
                    },
					regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'The City can consist of alphabetical characters and spaces only'
                    }
                }
            },
            stateId: {
                validators: {
                    notEmpty: {
                        message: 'Please Select Your State'
                    }
                }
            },
			countryId: {
                validators: {
                    notEmpty: {
                        message: 'Please Select Your Country'
                    }
                }
            },

            zip: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Your Zip Code'
                    }					
                }
            },
			business_no: {
                validators: {
                    stringLength: {
                        min: 15,
                        message:'Please enter at least 10 characters and no more than 10'
                    },
                    notEmpty: {
                        message: 'Please Enter Your Business Phone Number'
                    },
                    business_no: {
                        country: 'USA',
                        message: 'Please supply a vaild phone number with area code'
                    }
                }
            },
			telephoneNo1: {
                validators: {
                    stringLength: {
                        min: 15,
                        message:'Please enter at least 10 characters and no more than 10'
                    },
                    notEmpty: {
                        message: 'Please Enter Your Mobile Number'
                    }, mobile_no: {
                        country: 'USA',
                        message: 'Please supply a vaild phone number with area code'
                    }
                }
            },


         
            }
        })
        .on('success.form.bv', function(e) {
            $('#success_message').slideDown({ opacity: "show" }, "slow") // Do something ...
                $('#registrationForm').data('bootstrapValidator').resetForm();
            // Prevent form submission
            e.preventDefault();
            // Get the form instance
            var $form = $(e.target);

            // Get the BootstrapValidator instance
            var bv = $form.data('bootstrapValidator');

            // Use Ajax to submit form data
            $.post($form.attr('action'), $form.serialize(), function(result) {
               // console.log(result);
            }, 'json');
        });
});
</script>
<script>
 $(document).ready(function(){
   	$(document).on('change','.category', function()
   	{ 
   		//console.log('htm');
   		var id = $(this).val();
//alert(id);
   		$.get('<?php echo URL::to('getBranch'); ?>?id='+id, function(data)
   		{  
              $('#branch_name').empty();
              $.each(data, function(index, subcatobj)
   		   {
   			   $('#branch_name').val(subcatobj.branchname);
   		   })
   
   		});
   			
   	});
   });

</script>

<script>
$(document).ready( function() {
    	$(document).on('change', '.btn-file :file', function() {
		var input = $(this),
			label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		input.trigger('fileselect', [label]);
		});

		$('.btn-file :file').on('fileselect', function(event, label) {
		    
		    var input = $(this).parents('.input-group').find(':text'),
		        log = label;
		    
		    if( input.length ) {
		        input.val(log);
		    } else {
		        if( log ) alert(log);
		    }
	    
		});
		function readURL(input) {
		    if (input.files && input.files[0]) {
		        var reader = new FileReader();
		        
		        reader.onload = function (e) {
                            $('#blah').show(); 
		            $('#blah').attr('src', e.target.result);

 
		            
		        }
		        
		        reader.readAsDataURL(input.files[0]);
		    }
		}

		$("#photo").change(function(){
		    readURL(this);
		}); 	
	});


function readURL(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
$('#blah-2').show(); 
      $('#blah-2').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}

$("#pfid2").change(function() {
  readURL(this);
});





function readURL1(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
$('#blah-1').show(); 
      $('#blah-1').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}

$("#pfid1").change(function() {
  readURL1(this);
});



function readURL2(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
$('#blah-3').show(); 
      $('#blah-3').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}

$("#agreement").change(function() {
  readURL2(this);
});
</script>
<script>
    $(document).ready(function(){
        $('#first_rev_day').on('keyup',function(){
           var hiremonth = $('#hiremonth').val();
           var reset1 = parseInt($('#first_rev_day').val()); //alert(reset);
            var tt = hiremonth.split("-").reverse().join(" ");
         var t = new Date(tt); 
        // var t = new Date(hiremonth); 
		t.setDate(t.getDate() + reset1);
		var month = "0"+(t.getMonth()+1);
		var date = "0"+t.getDate();
		const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "June","July", "Aug", "Sept", "Oct", "Nov", "Dec"];
		const d = new Date();
		month = month.slice(-2);
		date = date.slice(-2);
		var date = monthNames[t.getMonth()]+"-"+date+"-"+t.getFullYear();
		if(reset1=='')
		{
		    $('#reviewmonth').empty();
		}
		else
		{
           $('#reviewmonth').val(date);
		}
        });
    });
</script>
<script>
    $(document).ready(function(){
        $('#reset').on('change',function(){
           var reset = parseInt($('#reset').val()); 
           var date = new Date();
         var t = new Date(); 
		//var n = $("#resetdays").val(); 
		//alert(offset);
		t.setDate(t.getDate() + reset);
		var month = "0"+(t.getMonth()+1);
		var date = "0"+t.getDate();
		
		const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "June","July", "Aug", "Sept", "Oct", "Nov", "Dec"];
		const d = new Date();
		
		month = month.slice(-2);
		date = date.slice(-2);
		//var date = date +"/"+month+"/"+t.getFullYear();
		var date = monthNames[t.getMonth()]+"-"+date+"-"+t.getFullYear();
           $('#reset_date').val(date);
        });
    });
    
    	$(document).ready(function(){
  /***phone number format***/
  $(".phone").keypress(function (e) {
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
      return false;
    }
    var curchr = this.value.length;
    var curval = $(this).val();
    if (curchr == 3 && curval.indexOf("(") <= -1) {
      $(this).val("(" + curval + ")" + "-");
    } else if (curchr == 4 && curval.indexOf("(") > -1) {
      $(this).val(curval + ")-");
    } else if (curchr == 5 && curval.indexOf(")") > -1) {
      $(this).val(curval + "-");
    } else if (curchr == 9) {
      $(this).val(curval + "-");
      $(this).attr('maxlength', '14');
    }
  });
});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>