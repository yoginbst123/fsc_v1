
<?php $__env->startSection('main-content'); ?>
<style>
    .redius {
        background: #1685cc;
        color: #fff;
        padding: 0;
        border-radius: 50%;
        margin-top: 3px;
        float: left;
        margin-right: 10px;
        width: 30px;
        height: 30px;
        text-align: center;
        line-height: 30px;
    }

    ul.curency {
        width: 100%;
        padding: 0;
        margin: 0;
    }

    ul.curency li {
        background: #286db5;
        padding: 5px 10px;
        margin-bottom: 7px;
        list-style: none;
    }

    ul.curency li a {
        color: #fff;
    }

    ul.curency li a span {
        float: right;
        color: red;
    }

    .table {
        border: 1px solid #616161;
    }

    .table thead tr th {
        border: 1px solid #616161;
    }

    .table thead tr th {
        color: #000;
    }

    .table tbody tr td {
        border: 1px solid #616161;
        padding: 10px 10px !important
    }

    .table>tbody>tr>td,
    .table>tbody>tr>th,
    .table>tfoot>tr>td,
    .table>tfoot>tr>th,
    .table>thead>tr>td,
    .table>thead>tr>th {
        padding: 6px 8px 6px 8px !important;
    }

    .table>tbody>tr>td .comboprice1 {
        width: 60%;
        margin: 0 auto;
    }
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="page-title content-header" style="padding-bottom:35px;">
        <div>
            <div style="text-align:center;">
                <h2>Service Fees Price <span style="text-align:right;padding-right: 20px !important;position: absolute;right: 0;">View / Edit</span></h2>
            </div>
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-success">
                    <div class="box-header">
                        <div class="box-tools pull-right">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <form method="post" action="<?php echo e(route('price.update',$price->id)); ?>" class="form-horizontal" id="businessname" name="businessname" enctype="multipart/form-data">
                            <?php echo e(csrf_field()); ?><?php echo e(method_field('PATCH')); ?>

                            <div class="form-group <?php echo e($errors->has('currency') ? ' has-error' : ''); ?>">
                                <label class="control-label col-md-3">Currency :</label>
                                <div class="col-md-3">
                                    <select name="currency" type="text" id="currency" class="form-control">
                                        <option value="">Currency</option>
                                        <?php $__currentLoopData = $currency; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cur): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($cur->id); ?>" <?php if($cur->id==$price->currency): ?> selected <?php endif; ?>><?php echo e($cur->currency.' '.$cur->sign); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                    <?php if($errors->has('currency')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('currency')); ?></strong>
                                    </span>
                                    <?php endif; ?>
                                </div>
                                <div class="col-md-3"><a href="#" data-toggle="modal" data-target="#basicExampleModal" class="redius"><i class="fa fa-plus"></i></a>&nbsp;&nbsp;&nbsp; <a href="#" data-toggle="modal" data-target="#basicExampleModal3" class="redius"><i class="fa fa-minus"></i></a> </div>
                            </div>
                            <div class="form-group <?php echo e($errors->has('typeofservice') ? ' has-error' : ''); ?>">
                                <label class="control-label col-md-3">Type of Service :</label>
                                <div class="col-md-3">
                                    <select name="typeofservice" type="text" id="typeofservice" class="form-control">
                                        <option value="">Type of Service</option>
                                        <?php $__currentLoopData = $typeofser; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $typeofser1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($typeofser1->id); ?>" <?php if($typeofser1->id==$price->typeofservice): ?> selected <?php endif; ?>><?php echo e($typeofser1->typeofservice); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                    <?php if($errors->has('typeofservice')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('typeofservice')); ?></strong>
                                    </span>
                                    <?php endif; ?>
                                </div>
                                <div class="col-md-3"><a href="#" data-toggle="modal" data-target="#basicExampleModal1" class="redius"><i class="fa fa-plus"></i></a> &nbsp;&nbsp;&nbsp;<a href="#" data-toggle="modal" data-target="#basicExampleModal4" class="redius"><i class="fa fa-minus"></i></a></div>
                            </div>
                            <div class="form-group <?php echo e($errors->has('typeofservice') ? ' has-error' : ''); ?>">
                                <label class="control-label col-md-3">Note:</label>
                                <div class="col-md-3">
                                    <input type="text" id="note" class="form-control" placeholder="Note" value="<?php echo e($price->note); ?>" name="note">

                                </div>
                            </div>
                            <div class="" id="hide_payroll">
                                <div <?php if($price->typeofservice=='11'): ?> style="display:none" <?php endif; ?> class="form-group <?php echo e($errors->has('serviceperiod') ? ' has-error' : ''); ?>">
                                    <label class="control-label col-md-3">Service Period :</label>
                                    <div class="col-md-3">
                                        <select name="serviceperiod" type="text" id="serviceperiod" class="form-control">
                                            <option value="">Service Period</option>
                                            <?php $__currentLoopData = $period; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $period1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($period1->id); ?>" id="dis_<?php echo e($period1->id); ?>" <?php if($period1->id==$price->period): ?> selected <?php endif; ?>><?php echo e($period1->period); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                        <?php if($errors->has('serviceperiod')): ?>
                                        <span class="help-block">
                                            <strong><?php echo e($errors->first('serviceperiod')); ?></strong>
                                        </span>
                                        <?php endif; ?>
                                    </div>
                                    <div class="col-md-3"><a href="#" data-toggle="modal" data-target="#basicExampleModal2" class="redius"><i class="fa fa-plus"></i></a> &nbsp;&nbsp;&nbsp;<a href="#" data-toggle="modal" data-target="#basicExampleModal5" class="redius"><i class="fa fa-minus"></i></a></div>
                                </div>
                            </div>
                            <div class="form-group showperiod" <?php if($price->typeofservice=='3'): ?> style="display:none" <?php endif; ?>>
                                <label class="control-label col-md-3">Payroll Service :</label>
                                <div class="col-md-3">
                                    <select name="payrollservice" type="text" id="payrollservice" class="form-control">
                                        <option value="">Service Payroll Service</option>

                                        <option value=""></option>

                                    </select>

                                </div>
                                <div class="col-md-3"><a href="#" data-toggle="modal" data-target="#basicExampleModal2" class="redius"><i class="fa fa-plus"></i></a> &nbsp;&nbsp;&nbsp;<a href="#" data-toggle="modal" data-target="#basicExampleModal5" class="redius"><i class="fa fa-minus"></i></a></div>
                            </div>
                            <div id="regular" <?php if($price->typeofservice=='3'): ?> style="display:none" <?php endif; ?>>
                                <div class="form-group <?php echo e($errors->has('serviceperiod') ? ' has-error' : ''); ?>">
                                    <label class="control-label col-md-3">Service Includes :</label>
                                    <div class="col-md-6">
                                        <textarea name="serviceincludes" type="text" id="serviceincludes" class="form-control"><?php echo e($price->serviceincludes); ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group <?php echo e($errors->has('regularprice') ? ' has-error' : ''); ?>">
                                    <label class="control-label col-md-3">Regular Price :</label>
                                    <div class="col-md-3">
                                        <input name="regularprice" value="<?php echo e($price->regularprice); ?>" type="text" id="regularprice" class="form-control regularprice">
                                    </div>
                                </div>
                                <div class="form-group <?php echo e($errors->has('comboprice') ? ' has-error' : ''); ?>">
                                    <label class="control-label col-md-3">Combo Price :</label>
                                    <div class="col-md-3">
                                        <input name="comboprice" type="text" id="comboprice" onblur="cal1();" class="form-control comboprice" value="<?php echo e($price->comboprice); ?>">
                                    </div>
                                </div>
                            </div>
                            <?php $i = 1;
                            $count1 = count($priceemp); ?><?php if(!empty($count1)): ?> <style>
                                #regular {
                                    display: none;
                                }
                            </style> <?php endif; ?>
                            <div class="input_fields_wrap_notes1" <?php if(empty($count1)): ?> style="display:none" <?php endif; ?>>
                                <table class="table table-hover table-bordered dataTable no-footer" id="maintable">
                                    <thead>
                                        <tr>
                                            <th>EE Count</th>
                                            <th>Weekly</th>
                                            <th>Bi-Weekly</th>
                                            <th>Semi-Monthly</th>
                                            <th>Monthly</th>
                                            <th>Delete</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $__currentLoopData = $priceemp; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $priceemp1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr id="p_<?php echo e($priceemp1->id); ?>">
                                            <td><input name="eecount[]" type="text" value="<?php echo e($priceemp1->eecount); ?>" placeholder="EE Count" class="form-control">
                                                <input name="priceid[]" type="hidden" value="<?php echo e($priceemp1->id); ?>" placeholder="EE Count" class="form-control">
                                            </td>
                                            <td><input name="weekly[]" type="text" value="<?php echo e($priceemp1->weekly); ?>" placeholder="Weekly" class="form-control"></td>
                                            <td><input name="biweekly[]" type="text" value="<?php echo e($priceemp1->biweekly); ?>" placeholder="Bi-Weekly" class="form-control"></td>
                                            <td><input name="semimonthly[]" type="text" value="<?php echo e($priceemp1->semimonthly); ?>" placeholder="Semi-monthly" class="form-control"></td>
                                            <td><input name="monthly[]" type="text" value="<?php echo e($priceemp1->monthly); ?>" placeholder="Monthly" class="form-control"></td>
                                            <td><a href="javascript:void(0);" class="btn-action btn-delete" id="<?php echo e($priceemp1->id); ?>"><i class="fa fa-trash"></i></a></td>
                                        </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>
                            </div>

                            <?php $i = 1;
                            $count = count($taxa); ?>
                            <div class="input_fields_wrap_notes" <?php if(empty($count)): ?> style="display:none" <?php endif; ?>>
                                <div class="table-title">
                                    <!--<a href="javascript:void(0);" style="font-size:18px;" id="addMore" title="Add More Person"><span class="glyphicon glyphicon-plus"></span></a>-->
                                    <a onclick="education_field_note()" style="font-size:18px;"><span class=" glyphicon glyphicon-plus"></span></a>
                                </div>
                            </div>
                            <div class="input_fields_wrap_notes" <?php if($price->typeofservice=='3'): ?> <?php else: ?> style="display:none" <?php endif; ?>>
                                <br>
                                <br>
                                <table class="table" id="table">
                                    <thead>
                                        <tr style="background:#00a0e3;color:#fff">
                                            <th scope="col">#</th>
                                            <th scope="col" style="width:30%;">Title</th>
                                            <th scope="col">Note</th>
                                            <Th>Only Note</Th>
                                            <th scope="col" style="width:11%">Regular Price</th>
                                            <th scope="col" style="width:11%">Combo Price</th>
                                            <th scope="col" style="width:67px !important;">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i = 1;
                                        $count = count($taxa); ?>
                                        <?php $__currentLoopData = $taxa; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tax): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td><?php echo $i;
                                                $i++; ?> <input type="hidden" name="taxid[]" value="<?php echo e($tax->id); ?>"></td>
                                            <td>
                                                <select class="form-control" name="titles[]" id="titles">
                                                    <?php $__currentLoopData = $taxtitle; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ti): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($ti->id); ?>" <?php if($tax->title==$ti->id): ?> selected <?php endif; ?>><?php echo e($ti->title); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                            </td>
                                            <td><input type="text" class="form-control" value="<?php echo e($tax->serviceincludes1); ?>" name="serviceincludes1[]"></td>
                                            <td><input type="checkbox" id="onlynote_<?php echo $tax->id ?>" value="1" <?php if ($tax->onlynote == '1') {
                                                                                                                            echo 'checked';
                                                                                                                        } ?> onclick="notesonly('<?php echo $tax->id; ?>')" value="" name="onlynote[]" <?php if ($tax->onlynote == '1') {
                                                                                                                                                                                                                                                        echo 'checked';
                                                                                                                                                                                                                                                    } ?>>

                                                <label for="onlynote_<?php echo $tax->id ?>"></label>
                                                <input type="hidden" name="onlynote[]" value="">
                                            </td>

                                            <td><input type="text " style="text-align:right;" id="regularprice_<?php echo $tax->id; ?>" class="regularprice form-control" value="<?php echo e($tax->regularprice1); ?>" name="regularprice1[]"></td>
                                            <td><input type="text" style="text-align:right;" class=" form-control comboprice" id="comboprice_<?php echo $tax->id; ?>" value="<?php echo e($tax->comboprice1); ?>" name="comboprice1[]"></td>
                                            <td><?php if($i == 2): ?>
                                                <a class="btn btn-primary" onclick="education_field_note();"> Add</a>
                                                <?php else: ?>
                                                <a href="#myModal<?php echo e($tax->id); ?>" role="button" class="btn btn-danger" title="Add field" data-toggle="modal"> <span class="glyphicon glyphicon-minus" aria-hidden="true"></span> </a>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                        <script>
                                            function notesonly(id) {
                                                //alert(id);
                                                var ids = document.getElementById("onlynote_" + id);
                                                // alert(ids);
                                                if (ids.checked == true) {
                                                    //alert('1');
                                                    $('#regularprice_' + id).val('Note');
                                                    $('#comboprice_' + id).val('Note');
                                                } else {
                                                    // alert('2');
                                                    $('#regularprice_' + id).val('<?php echo $tax->regularprice1; ?>');
                                                    $('#comboprice_' + id).val('<?php echo $tax->comboprice1; ?>');
                                                }

                                            }
                                        </script>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>
                            </div>
                            <div id="input_fields_wrap_notes"></div>


                            <div class="input_fields_wrap_notes2" style="display:none">
                                <br>
                                <br>
                                <table class="table" id="table2">
                                    <thead>
                                        <tr style="background:#00a0e3;color:#222 !important;border-bottom:1px solid #222 !important;">
                                            <th style="border-bottom:1px solid #222 !important;" scope="col">No</th>
                                            <th style="border-bottom:1px solid #222 !important;" scope="col">Type of License</th>
                                            <th style="border-bottom:1px solid #222 !important;" scope="col">Jurisdiction</th>
                                            <th style="border-bottom:1px solid #222 !important;" scope="col" width="250px">FSC Fees</th>
                                            <th style="border-bottom:1px solid #222 !important;" scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody class="add_license_row">
                                        <tr>
                                            <td style="width:50px !important;"><input type="text" class="form-control" placeholder="1"></td>
                                            <td>
                                                <select class="form-control">
                                                    <option>--- Select Type---</option>
                                                </select>
                                            </td>
                                            <td><input type="text" class="form-control" placeholder="Jurisdiction"></td>
                                            <td><input type="text" class=" form-control" placeholder="FSC Fees"></td>
                                            <td>
                                                <div><a class="btn btn-warning" id="add_license"> Add</a></div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div class="card-footer">
                                <div class="col-md-2 col-md-offset-3">
                                    <input class="btn_new_save btn-primary1" style="margin-left:-5%" type="submit" name="submit" value="Save">
                                </div>
                                <div class="col-md-2 row">
                                    <a class="btn_new_cancel" style="margin-left:-5%" href="<?php echo e(url('fac-Bhavesh-0554/price')); ?>?online=<?php echo e($_REQUEST['online']); ?>">Cancel</a>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </section>
</div>
<!-- Currency--->
<div class="modal fade" id="basicExampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Currency</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" method="post" id="ajax">
                <?php echo e(csrf_field()); ?>

                <div class="modal-body">
                    <input type="text" id="newopt" name="newopt" class="form-control" placeholder="Currency" /><br>
                    <input type="text" id="sign" name="sign" class="form-control" placeholder="Currency Sign" />
                </div>
                <div class="modal-footer">
                    <button type="button" id="addopt" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="basicExampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Currency</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class="curency curency_ref" id="div">
                    <?php $__currentLoopData = $currency; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cur): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li id="cur_<?php echo e($cur->id); ?>"><a class="delete" id="<?php echo e($cur->id); ?>"><?php echo e($cur->currency.' '.$cur->sign); ?> <span><i class="fa fa-trash"></i></span></a></li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- Currency--->
<div class="modal fade" id="basicExampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Type of Service</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" method="post" id="ajax1">
                <?php echo e(csrf_field()); ?>

                <div class="modal-body">
                    <input type="text" id="newopt1" name="newopt1" class="form-control" placeholder="" />
                </div>
                <div class="modal-footer">
                    <button type="button" id="addopt1" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="basicExampleModal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Currency</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class="curency" id="serv">
                    <?php $__currentLoopData = $typeofser; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $typeofser1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li id="ser_<?php echo e($typeofser1->id); ?>"><a class="delete1" id="<?php echo e($typeofser1->id); ?>"><?php echo e($typeofser1->typeofservice); ?> <span><i class="fa fa-trash"></i></span></a></li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="basicExampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Service Period</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" method="post" id="ajax2">
                <?php echo e(csrf_field()); ?>

                <div class="modal-body">
                    <input type="text" id="newopt2" name="newopt2" class="form-control" />
                </div>
                <div class="modal-footer">
                    <button type="button" id="addopt2" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="basicExampleModal5" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Currency</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class="curency" id="serv1">
                    <?php $__currentLoopData = $period; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $period1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li id="per_<?php echo e($period1->id); ?>"><a class="delete2" id="<?php echo e($period1->id); ?>"><?php echo e($period1->period); ?> <span><i class="fa fa-trash"></i></span></a></li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    });

    $(function() {
        $('#addopt').click(function() { //alert();
            var newopt = $('#newopt').val();
            if (newopt == '') {
                alert('Please enter something!');
                return;
            }

            //check if the option value is already in the select box
            $('#currency option').each(function(index) {
                if ($(this).val() == newopt) {
                    alert('Duplicate option, Please enter new!');
                }
            })
            $.ajax({
                type: "post",
                url: "<?php echo route('currency.currencies'); ?>",
                dataType: "json",
                data: $('#ajax').serialize(),
                success: function(data) {
                    alert('Successfully Add');
                    $('#currency').append('<option value=' + newopt + '>' + newopt + '</option>');
                    $("#div").load(" #div > *");
                    $("#newopt").val('');
                },
                error: function(data) {
                    alert("Error")
                }
            });

            $('#basicExampleModal').modal('hide');
        });
    });



    $(function() {
        $('#addopt1').click(function() { //alert();
            var newopt = $('#newopt1').val();
            if (newopt == '') {
                alert('Please enter something!');
                return;
            }

            //check if the option value is already in the select box
            $('#typeofservice option').each(function(index) {
                if ($(this).val() == newopt) {
                    alert('Duplicate option, Please enter new!');
                }
            })

            //add the new option to the select box
            $('#typeofservice').append('<option value=' + newopt + '>' + newopt + '</option>');

            //select the new option (particular value)
            // $('#typeofservice option[value="' + newopt + '"]').prop('selected', true);

            $.ajax({
                type: "post",
                url: "<?php echo route('typeofser.typeofsers'); ?>",
                dataType: "json",
                data: $('#ajax1').serialize(),
                success: function(data) {
                    // alert("Data Save: " + data);
                    alert('Successfully Add');
                    $("#serv").load(" #serv > *");
                    $("#newopt1").val('');
                },
                error: function(data) {
                    alert("Error")
                }
            });

            $('#basicExampleModal1').modal('hide');
        });
    });


    $(function() {
        $('#addopt2').click(function() {
            var newopt = $('#newopt2').val();
            if (newopt == '') {
                alert('Please enter something!');
                return;
            }

            //check if the option value is already in the select box
            $('#serviceperiod option').each(function(index) {
                if ($(this).val() == newopt) {
                    alert('Duplicate option, Please enter new!');
                }
            })

            //add the new option to the select box
            $('#serviceperiod').append('<option value=' + newopt + '>' + newopt + '</option>');

            //select the new option (particular value)
            // $('#serviceperiod option[value="' + newopt + '"]').prop('selected', true);
            $.ajax({
                type: "post",
                url: "<?php echo route('period.periods'); ?>",
                dataType: "json",
                data: $('#ajax2').serialize(),
                success: function(data) {
                    // alert("Data Save: " + data);
                    alert('Successfully Add');
                    $("#serv1").load(" #serv1 > *");
                    $("#newopt2").val('');
                },
                error: function(data) {
                    alert("Error")
                }
            });
            //  $("#newopt2").val('');
            $('#basicExampleModal2').modal('hide');
        });
    });
</script>
<script>
    $(document).ready(function() {
        $(document).on('change', '#typeofservice', function() {
            var sign1 = $(this).val();
            //   alert(sign1);
            if (sign1 == '3') {
                $('.input_fields_wrap_notes').show();
                $('.input_fields_wrap_notes1').hide();
                $('#regular').hide();
                $('#showperiod').hide();
                $('.sss').show();
            } else if (sign1 == '11') {
                $('.input_fields_wrap_notes1').show();
                $('.input_fields_wrap_notes').hide();
                $('#regular').hide();
                $('.sss').hide();
            } else {
                $('.input_fields_wrap_notes').hide();
                $('.input_fields_wrap_notes1').hide();
                $('#regular').show();
                $('.sss').show();
            }
        });
        $(document).on('click', '.remove', function() {
            var id = $(this).attr('id');
            if (confirm("Are you sure you want to Delete this data?")) {
                $.ajax({
                    url: "<?php echo e(route('removepriceemp.removepriceemp1')); ?>",
                    mehtod: "get",
                    data: {
                        id: id
                    },
                    success: function(data) {
                        alert(data);
                        $('#p_' + id).remove();
                        $("#maintable").load(" #maintable > *");
                    }
                })
            } else {
                return false;
            }
        });



        $(document).on('click', '.delete', function() {
            var id = $(this).attr('id');
            if (confirm("Are you sure you want to Delete this data?")) {
                $.ajax({
                    url: "<?php echo e(route('removecurrency.removecurrency1')); ?>",
                    mehtod: "get",
                    data: {
                        id: id
                    },
                    success: function(data) {
                        alert(data);
                        $('#cur_' + id).remove();
                        $("#currency").load(" #currency > *");
                    }
                })
            } else {
                return false;
            }
        });


        $(document).on('click', '.delete1', function() {
            var id = $(this).attr('id');
            if (confirm("Are you sure you want to Delete this data?")) {
                $.ajax({
                    url: "<?php echo e(route('removetypeofservice.removetypeofservice1')); ?>",
                    mehtod: "get",
                    data: {
                        id: id
                    },
                    success: function(data) {
                        alert(data);
                        $('#ser_' + id).remove();
                        $("#typeofservice").load(" #typeofservice > *");
                    }
                })
            } else {
                return false;
            }
        });

        $(document).on('click', '.delete2', function() {
            var id = $(this).attr('id');
            if (confirm("Are you sure you want to Delete this data?")) {
                $.ajax({
                    url: "<?php echo e(route('removeperiod.removeperiod1')); ?>",
                    mehtod: "get",
                    data: {
                        id: id
                    },
                    success: function(data) {
                        alert(data);
                        $('#per_' + id).remove();
                        $("#serviceperiod").load(" #serviceperiod > *");
                    }
                })
            } else {
                return false;
            }
        });
    });
</script>
<script>
    $(document).ready(function() {
        $(document).on('change', '.regularprice', function() { // alert();
            var id = $('#currency').val();
            var sign1 = $(this).val();
            var sign2 = parseFloat(Math.round(sign1 * 100) / 100).toFixed(2);
            var no = sign2.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            $.get('<?php echo URL::to('
                getsign '); ?>?id=' + id,
                function(data) {
                    $('.regularprice').empty();
                    $.each(data, function(index, subcatobj) {
                        var val = subcatobj.sign + ' ' + no;
                        $('.regularprice').val(val);
                        //$('.regularprice').val(val);
                    })

                });

        });
    });


    $(document).ready(function() {
        $(document).on('change', '.comboprice', function() { // alert();
            var id = $('#currency').val();
            var sign1 = $(this).val();
            var sign2 = parseFloat(Math.round(sign1 * 100) / 100).toFixed(2);
            var no = sign2.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            $.get('<?php echo URL::to('
                getsign '); ?>?id=' + id,
                function(data) {
                    $('.comboprice').empty();
                    $.each(data, function(index, subcatobj) {
                        var val = subcatobj.sign + ' ' + no;
                        $('.comboprice').val(val);
                    })

                });

        });

        $(document).on('change', '#typeofservice', function() {
            var sign1 = $(this).val();
            //alert(sign1);
            if (sign1 == '3') {
                $('.input_fields_wrap_notes').show();
                $('#regular').hide();
                $('#showperiod').hide();
            } else {
                $('.input_fields_wrap_notes').hide();
                $('#regular').show();
            }
        });
    });
</script>
<script>
    var room1 = 0;
    var coun = <?php echo $count; ?>;
    var z = room1 + coun;
    function education_field_note() {
        room1++;
        z++;
   var objTo = document.getElementById('table')
   var divtest = document.createElement("tbody");
   divtest.setAttribute("class", "form-group removeclass"+z);
        divtest.innerHTML = '<tr><td>' + z + '  <input type="hidden" name="taxid[]"></td><td><select class="form-control" name="titles[]" id="titles"> <?php $__currentLoopData = $taxtitle; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ti): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?><option value="<?php echo e($ti->id); ?>"><?php echo e($ti->title); ?></option><?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></select></td><td><input type="text" class="form-control" value="" name="serviceincludes1[]"></td><td><input type="checkbox" id="fordemo" name="fordemo"><label for="fordemo"></label></td><td><input type="text" style="text-align: right;" placeholder="Regular Price" class="regularprice_' + z + ' form-control" name="regularprice1[]"></td><td><input type="text" style="text-align: right;" placeholder="Combo Price" class=" form-control comboprice_' + z + '" name="comboprice1[]"></td><td><button class="btn btn-danger" type="button" onclick="remove_education_fields(' + z + ');"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span> </button></td></tr>';
   var rdiv = 'removeclass'+z;
   var rdiv1 = 'Schoolname'+z;
   var comboprice = '.comboprice_'+z;
   var regularprice = '.regularprice_'+z;
   $(document).on('change',comboprice, function()
   	{ //alert();
   		 var id = $('#currency').val();
   		var sign1 = $(this).val();
   		var sign2 =parseFloat(Math.round(sign1 * 100) / 100).toFixed(2);
   		var no = sign2.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
   		$.get('<?php echo URL::to('getsign'); ?>?id='+id, function(data)
   		{  
             $(comboprice).empty();
              $.each(data, function(index, subcatobj)
   		   {
   		  var val = subcatobj.sign+' '+no;
             $(comboprice).val(val);
             //alert(val);
   		   })
   		});
   	});
   $(document).on('change',regularprice, function()
   	{
   		 var id = $('#currency').val();
   		var sign1 = $(this).val();
   		var sign2 =parseFloat(Math.round(sign1 * 100) / 100).toFixed(2);
   		var no = sign2.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
   		$.get('<?php echo URL::to('getsign'); ?>?id='+id, function(data)
   		{  
               $(regularprice).empty();
              $.each(data, function(index, subcatobj)
   		   {
   		  var val = subcatobj.sign+' '+no;
             $(regularprice).val(val);
   		   })
   		});
   	});
   objTo.appendChild(divtest)
   }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    function remove_education_fields(rid) {
        $('.removeclass' + rid).remove();
        room1--;
        z--;
    }

    function cal1() {
        var a = $(".regularprice").val();
        var sign2 = parseFloat(a.replace(/[^\d\.]*/g, ''));
        var b = $(".comboprice").val(); //alert(a); 
        var sign = parseFloat(b.replace(/[^\d\.]*/g, '')); //alert(sign2);
        // alert(b);
        //c2 = a > b;
        if (sign2 >= sign) {
            $(".com").show();
            $(".com").html('Please Enter Combo Price > Regular Price');
        } else if (sign >= sign2) {
            $(".com").hide();
        } else {
            // $(".com").hide(); 
        }
    }
</script>
<?php $__currentLoopData = $taxa; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $re): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<div id="myModal<?php echo e($re->id); ?>" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                <p>Do you want to delete this record ?</p>
            </div>
            <div class="modal-footer">
                <a href="<?php echo e(route('taxa.taxadelete',$re->id)); ?>" class="btn btn-danger">Delete</a>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<script>
    $(function() {
        $('#addMore').on('click', function() {
            $("#table > tbody tr:last").after('<tr><td <input type="text" name="taxid[]" value=""></td><td><select class="form-control"><option>Select Title</select></td><td><input type="text" class="form-control"></td><td><input type="checkbox" id="onlynote_demo" name="onlynote_demo" checked><label for="onlynote_demo"></label></td><td><input type="text " style="text-align:right;" class="regularprice form-control" value="Note" ></td><td><input type="text " style="text-align:right;" class="regularprice form-control" value="Note" ></td><td><a class="btn btn-danger remove_field"><i class="fa fa-minus"></i></a></td></tr>');
            //   alert('22');
            //  var data = $("#maintable tr:eq(1)").clone(true).appendTo("#maintable");
            //  data.find("input").val('');
        });
        $(document).on('click', '.remove', function() {
            var trIndex = $(this).closest("tr").index();
            if (trIndex > 0) {
                $(this).closest("tr").remove();
            } else {
                alert("Sorry!! Can't remove first row!");
            }
        });
    });
</script>
<script>
    $(document).ready(function() {
        $(document).on('change', '.regularprice', function() { // alert();
            var id = $('#currency').val();
            var sign1 = $(this).val();
            var sign2 = parseFloat(Math.round(sign1 * 100) / 100).toFixed(2);
            var no = sign2.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            $.get('<?php echo URL::to('
                getsign '); ?>?id=' + id,
                function(data) {
                    $('.regularprice').empty();
                    $.each(data, function(index, subcatobj) {

                        var val = subcatobj.sign + ' ' + no;
                        $('.regularprice').val(val);
                        //$('.regularprice').val(val);
                    })

                });

        });


        $(document).on('change', '#currency', function() {
            var typeofservice = $('#typeofservice').val();
            var id = $('#currency').val();
            //	 alert(id);
            var sign1 = $(this).val();
            //var sign2 =parseFloat(Math.round(sign1 * 100) / 100).toFixed(2);
            //	var no = sign2.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            $.get('<?php echo URL::to(' / ckcurrency '); ?>?id=' + sign1 + '&state=' + typeofservice, function(data) {
                // $('.regularprice').empty();
                $("#serviceperiod option[value='13']").prop("disabled", false);
                $("#serviceperiod option[value='2']").prop("disabled", false);
                $("#serviceperiod option[value='3']").prop("disabled", false);
                $("#serviceperiod option[value='4']").prop("disabled", false);
                $("#serviceperiod option[value='15']").prop("disabled", false);
                $("#serviceperiod option[value='14']").prop("disabled", false);
                $("#serviceperiod option[value='12']").prop("disabled", false);
                $("#serviceperiod option[value='13']").prop("disabled", false);
                $("#serviceperiod option[value='7']").prop("disabled", false);
                $("#serviceperiod option[value='8']").prop("disabled", false);
                $("#serviceperiod option[value='9']").prop("disabled", false);
                $("#serviceperiod option[value='10']").prop("disabled", false);
                $("#serviceperiod option[value='12']").prop("disabled", false);
                $("#serviceperiod option[value='11']").prop("disabled", false);
                $("#serviceperiod option[value='5']").prop("disabled", false);
                $.each(data, function(index, subcatobj) {
                    $("#serviceperiod option[value='" + subcatobj.period + "']").prop("disabled", true);
                })

            });

        });
        $(document).on('change', '#serviceperiod', function() {
            var payroll = $(this).val();
            if (payroll == '9') {
                $('#maintable').show();
                $('#addMore').show();
            } else {
                $('#maintable').hide();
                $('#addMore').hide();
            }
        });
        $('.showperiod').hide();
        $(document).on('change', '#typeofservice', function() {
            var payroll = $(this).val(); //alert(payroll);
            if (payroll == '8') {
                $('#dis_11').show();
                $('#dis_12').show();
                $('#dis_14').show();
                $('#dis_10').show();
                $('#dis_5').show();
                $('#dis_7').show();
                $('#dis_4').show();
                $('#hide_payroll').hide();
                $('.showperiod').show();
            } else if (payroll == '11') {
                $('#dis_11').show();
                $('#dis_12').show();
                $('#dis_14').show();
                $('#hide_payroll').show();
                $('.showperiod').hide();
                $('#dis_10').show();
                $('#dis_5').show();
                $('#dis_7').show();
                $('#dis_4').show();
            } else if (payroll == '9') {
                $('#dis_11').hide();
                $('#dis_12').hide();
                $('#dis_14').hide();
                $('#hide_payroll').show();
                $('.showperiod').hide();
                $('#dis_10').hide();
                $('#dis_5').hide();
                $('#dis_7').hide();
                $('#dis_4').hide();
            } else if (payroll == '10') {
                $('#dis_11').hide();
                $('#dis_12').hide();
                $('#dis_14').hide();
                $('#hide_payroll').show();
                $('.showperiod').hide();
                $('#dis_10').hide();
                $('#dis_5').hide();
                $('#dis_7').hide();
                $('#dis_4').hide();
            } else {
                $('#dis_11').hide();
                $('#dis_12').hide();
                $('#dis_14').hide();
                $('.showperiod').hide();
                $('#hide_payroll').show();
                $('#dis_10').show();
                $('#dis_5').show();
                $('#dis_7').show();
                $('#dis_4').show();
            }
        });
    });


    $(document).ready(function() {
        $(document).on('change', '.comboprice', function() { // alert();
            var id = $('#currency').val();
            var sign1 = $(this).val();
            var sign2 = parseFloat(Math.round(sign1 * 100) / 100).toFixed(2);
            var no = sign2.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            $.get('<?php echo URL::to('
                getsign '); ?>?id=' + id,
                function(data) {
                    $('.comboprice').empty();
                    $.each(data, function(index, subcatobj) {
                        var val = subcatobj.sign + ' ' + no;
                        $('.comboprice').val(val);
                    })

                });

        });

        $(document).on('change', '#typeofservice', function() {
            var currency = $('#currency').val();
            var id = $('#typeofservice').val(); //alert(currency);
            $("#serviceperiod option[value='13']").prop("disabled", false);
            $("#serviceperiod option[value='2']").prop("disabled", false);
            $("#serviceperiod option[value='3']").prop("disabled", false);
            $("#serviceperiod option[value='4']").prop("disabled", false);
            $("#serviceperiod option[value='15']").prop("disabled", false);
            $("#serviceperiod option[value='14']").prop("disabled", false);
            $("#serviceperiod option[value='12']").prop("disabled", false);
            $("#serviceperiod option[value='13']").prop("disabled", false);
            $("#serviceperiod option[value='7']").prop("disabled", false);
            $("#serviceperiod option[value='8']").prop("disabled", false);
            $("#serviceperiod option[value='9']").prop("disabled", false);
            $("#serviceperiod option[value='10']").prop("disabled", false);
            $("#serviceperiod option[value='12']").prop("disabled", false);
            $("#serviceperiod option[value='11']").prop("disabled", false);
            $("#serviceperiod option[value='5']").prop("disabled", false);
            $.get('<?php echo URL::to(' / ckcurrency '); ?>?id=' + currency + '&state=' + id, function(data) {
                $.each(data, function(index, subcatobj) {
                    $("#serviceperiod option[value='" + subcatobj.period + "']").prop("disabled", true);
                })
            });
        });

        $(document).on('change', '#typeofservice', function() {
            var sign1 = $(this).val();
            //alert(sign1);
            if (sign1 == '3') {
                $('.input_fields_wrap_notes').show();
                $('.input_fields_wrap_notes1').hide();
                $('.input_fields_wrap_notes2').hide();
                $('#regular').hide();
                $('.sss').show();
            } else if (sign1 == '4') {
                $('.input_fields_wrap_notes2').show();
                $('.input_fields_wrap_notes').hide();
                $('.input_fields_wrap_notes1').hide();
                $('#regular').hide();
                $('.sss').hide();
            } else if (sign1 == '11') {
                $('.input_fields_wrap_notes1').show();
                $('.input_fields_wrap_notes').hide();
                $('.input_fields_wrap_notes2').hide();
                $('#regular').hide();
                $('.sss').hide();
            } else {
                $('.input_fields_wrap_notes').hide();
                $('.input_fields_wrap_notes1').hide();
                $('.input_fields_wrap_notes2').hide();
                $('#regular').show();
                $('.sss').show();
            }
        });
    });
    $('#add_license').on('click', function() {
        $(".add_license_row tr:last-child").after('<tr><td><input type="text" class="form-control"></td><td><select class="form-control"> <option> --- Select Type --- </option> </select></td><td><input type="text" class="form-control"></td><td><input type="text" class="form-control"></td><td><a class="btn btn-danger" id="remove_license"><i class="fa fa-minus"></i></a></td></tr>');
    });
    $("body").on('click', '#remove_license', function() {
        $(this).parent().parent().remove();
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>