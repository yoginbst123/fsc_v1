<?php $__env->startSection('main-content'); ?>
<div class="content-wrapper">
    <section class="page-title content-header">
     		<h1>Edit State Tax</h1>
    </section>
    <section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-success">
			      <div class="box-header">
              <div class="box-tools pull-right">
              </div>
            </div>
				<div class="col-md-12">
             <form method="post" action="<?php echo e(route('statetax.update',$bankdata->id)); ?>" class="form-horizontal" enctype="multipart/form-data">
					<?php echo e(csrf_field()); ?> <?php echo e(method_field('PATCH')); ?>

						
						
						<div class="form-group <?php echo e($errors->has('statename') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">State Name : <span class="star-required">*</span></label>
							<div class="col-lg-5 col-md-8">
								<!--<input name="statename" type="text" id="statename" class="form-control p-l-10" />                                                            -->
								<select name="statename" type="text" id="statename" class="form-control p-l-10" />                                                            
								    <option>Select</option>
								    <?php $__currentLoopData = $datastate; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $datastate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								    
								        <option value="<?php echo e($datastate->state); ?>" <?php if($datastate->state==$bankdata->statename): ?> selected <?php endif; ?>><?php echo e($datastate->state); ?></option>
								    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</select>
								<?php if($errors->has('statename')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('statename')); ?></strong>
										</span>
								<?php endif; ?>
							</div>
						</div>
						
					    <div class="form-group <?php echo e($errors->has('taxform') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3"> Form : <span class="star-required">*</span></label>
							<div class="col-lg-5 col-md-8">
								<input name="taxform" type="text" id="taxform" value="<?php echo e($bankdata->taxform); ?>" class="form-control p-l-10" />                                                            
								<?php if($errors->has('taxform')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('taxform')); ?></strong>
										</span>
								<?php endif; ?>
							</div>
						</div>
					        
					        
						<div class="card-footer">
						    <div class="form-group">
    							<label class="control-label col-md-3 hide_991"></label>
    							<div class="col-xs-2" style="width:155px;">
                                    <input class="btn_new_save" type="submit" name="submit" value="Save">
    							</div>
    							<div class="col-xs-2" style="width:155px;">
                                    <a class="btn_new_cancel" href="<?php echo e(url('fac-Bhavesh-0554/statetax')); ?>">Cancel</a> 
    							</div>
    						</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	</section>
<!--</div>-->
<style>
.select2-container--default .select2-selection--single .select2-selection__rendered{
    font-size:16px !important;
    padding:0;
     color:#000;
}
.p-l-10{ padding-left:10px;}
.select2-container--default .select2-selection--single .select2-selection__arrow b{ border-color:#000 transparent transparent transparent; }
.select2-container--default .select2-selection--single .select2-selection__arrow{ top:6px; right:4px; }
.select2-container {
    box-sizing: border-box;
    display: inline-block;
    margin: 0;
    position: relative;
    vertical-align: middle;
    width: 100% !important;
}.select2-container--default .select2-selection--single {
    background-color: #fff;
    /* border: 1px solid #aaa; */
    border-radius: 4px;
    border: 2px solid #2fa6f2;
    height:40px;
    padding:8px;
}</style>
<script>
    $('.js-example-tags').select2({
    tags: true,
    tokenSeparators: [",", " "]
});

</script>

<script>
$("#contactnumber").mask("(999) 999-9999");


</script> 

<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>