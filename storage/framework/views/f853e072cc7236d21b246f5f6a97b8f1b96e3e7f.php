<?php $__env->startSection('main-content'); ?>
<?php
$month_num = 7;
$month_name = date("F-15-Y", mktime(0, 0, 0, $month_num, 10));
?>
<style>
.nav-tabs{
    padding:3px;
}
.questionbox {display: none;}
.share_persentage.form-control,.share_date.form-control{padding: 6px 1px !important;}
.greenText{ background-color:green !important; width:100%;height: 40px;border-radius: 3px;border: 2px solid #2fa6f2;font-size: 16px;outline: 0;color: white !important;}
.blueText{ background-color:blue; width:100%; }
.redText{ background-color:red !important; width:100%;height: 40px;border-radius: 3px;border: 2px solid #2fa6f2;font-size: 16px !important;outline: 0;color: white !important; }

input[type=checkbox]{opacity:0; position: absolute;}  
input[type=checkbox] + label {position:relative;} 
input[type=checkbox] + label{ background:url(https://financialservicecenter.net/public/adminlte/icon_checkbox.png) no-repeat left top; padding-left:30px; height:25px;}
input[type=checkbox]:checked + label {background:url(https://financialservicecenter.net/public/adminlte/icon_checkbox.png) no-repeat left -24px;font-style: normal;  } 
.pr-0{padding-right:0px!important;}
.pl-0{padding-left:0px!important;}
#renewRecord .form-control{margin-bottom:15px;}
#renewRecord  label.text-right{text-align:right!important; width:100%;}

a.disabled {
  pointer-events: none;
  cursor: default;
  opacity: 0.5;
  
  /*color: currentColor;*/
  /*cursor: not-allowed;*/
  /*opacity: 0.5;*/
  /*text-decoration: none;*/
}

.second{
    background:#41B314;color:#ffffff;
    width:100%;
    padding: 0 2px;
    border: 2px solid #29abe2;
    border-radius:0px !important;
}   
.first{
    background:#ff0000;color:#ffffff;
    width:100%;
    padding: 0 2px;
    border: 2px solid #29abe2;
    border-radius:0px !important;
} 
.default_val{
    background:#ffffff;color:#000000;
    width:100%;
    padding: 0 2px;
    border: 2px solid #29abe2;
    border-radius:0px !important;
}
#renewRecord .form-control{margin-bottom:15px;}
#renewRecord  label.text-right{text-align:right!important; width:100%;}

#_corporation_lbl:before{
    content: '';
    position: absolute;
    left: -4px;
    top: 0;
    width: 16px;
    height: 16px;
    background: #d5dbe0;
    -webkit-border-radius: 16px;
    border-radius: 16px;
}

#_corporation_lbl_selected:before{
    content: '';
    position: absolute;
    left: -4px;
    top: 0;
    width: 16px;
    height: 16px;
    background: #86cc17;
    -webkit-border-radius: 16px;
    border-radius: 16px;
}


.newcmpname{display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    height: 77px;}
.Branch .titleleft{width: 45%;
    float: left;
    text-align: left;
    padding-left: 15px;
    padding-top: 5px;}
.togglebox{float:right; margin-right:10px;}
.togglebox label{padding-right:10px;}
.customfieldsbox{border:1px solid #ccc; padding:15px; margin-bottom:15px;}
.mt30{margin-top:30px;}
.btn-toggle a{margin:0px 5px; border-radius:4px!important;}
/*.nav-tabs > li{width:19.6%!important;}*/
.btnyes.active{
    background-color: #00a65a!important;
    border-color: #008d4c!important; color:#fff!important;
}
.btnno.active{
    background-color: #ac2925!important;
    border-color: #ac2925!important; color:#fff!important;
}
.howmany_box{
    margin-top:0px;
}
.taxation_1{
    width:39%;
}
.taxation_2{
    width:27%;
}
.taxation_3{
    
}
@media  screen {
  #printSection {
      display: none;
  }
}

@media  print {
  body * {
   display:block;
  }
  #printSection, #printSection * {
   display:table;
  }
  #printSection {
    position:absolute;
    left:0;
    top:0;
  }
}
.fed_wh2{
    width:22%;       
}
.fed_wh3{
    width:12.5%;       
}
.fed_wh4{
    width:12%;       
}
.fed_wh5{
    width:13%;       
}
.fed_wh6{
    width:16.5%;       
}
.fed_wh7{
    width:14.5%;       
}
.fed_wh8{
    width:34.5%;       
}
.fed_wh13{
    width:14.5%;       
}
.fed_un3{
    width:26%;
}
.fed_un5,.fed_un8{
    width:14.5%;
}
.fed_un6{
    width:67.5%;
}
.st_wh1{
    width:18%;
}
.st_wh4{
    width:16%;
}
.st_wh5{
    width:17%;
}
.st_wh6{
    width:14.5%;
}
.do_que{
    position: absolute;
    right: 0px;
}
.history_btn{
    width:13.3% !important;
}
.professional_profession {
    width: 31%;
}
.professional_note {
    width: 39%;
}

@media  only screen and (max-width: 1380px) {
    .fed_wh1{
        width:10%; 
    }
    .fed_wh2{
        width:26%;       
    }
    .fed_wh3{
        width:14.5%;       
    }
    .fed_wh4{
        width:15%;       
    }
    .fed_wh5{
        width:16%;       
    }
    .fed_wh6{
        width:16.5%;       
    }
    .fed_wh7{
        width:15.5%;       
    }
    .fed_wh7 .form-control{
        margin-bottom:0px !important;
    }
    .fed_wh8{
        width:50%;       
    }
    .fed_wh11{
        width:18.5%;
    }
    .fed_wh13{
        width:17.5%;       
    }
    .fed_un1{
        width:10%;
    }
    .fed_un2{
        width:26%;
    }
    .fed_un3{
        width:26%;
    }
    .fed_un4,.fed_un5,.fed_un7,.fed_un8{
        width:18%;
    }
    .fed_un6{
        width:62%;
    }
    .st_wh1{
        width:28%;
    }
    .st_wh2{
        width:17%;
    }
    .st_wh3{
        width:19%;
    }
    .st_wh4{
        width:18%;
    }
    .st_wh5{
        width:21%;
    }
    .st_wh6{
        width:22%;
    }
}
@media  only screen and (max-width: 1190px) {
    .fed_wh1{
        width:12%; 
    }
    .fed_wh2{
        width:32%;       
    }
    .fed_wh3{
        width:18%;       
    }
    .fed_wh4{
        width:18%;       
    }
    .fed_wh5{
        width:19%;       
    }
    .fed_wh6{
        width:18.5%;       
    }
    .fed_wh7{
        width:18%;       
    }
    .fed_wh7 .form-control{
        margin-bottom:0px !important;
    }
    .fed_wh8{
        width:43.5%;       
    }
    .fed_wh9{
        width:19%;
    }
    .fed_wh10{
        width: 18.5%;
    }
    .fed_wh11{
        width:25.5%;
    }
    .fed_wh13{
        width:22%;       
    }
    .taxation_1,.taxation_2,.taxation_3 {
        width: 50%;
    }
    .taxation_3 .col-lg-6{
        width:66%;
    }   
    .fed_un1{
        width:12%;
    }
    .fed_un2{
        width:32%;
    }
    .fed_un3{
        width:19%;
    }
    .fed_un4{
        width:18.5%;
    }
    .fed_un5{
        width:18%;
    }
    .fed_un7,.fed_un8{
        width:25%;
    }
    .fed_un6{
        width:49%;
    }
    .st_wh1{
        width:26%;
    }
    
}
@media  only screen and (max-width: 1280px) {
{
.greenText{ font-size: 14px !important; }
.form-control{font-size: 14px !important;}
}    
}    

@media  only screen and (max-width: 1050px) {
    .fed_wh1{
        width:19%; 
    }
    .fed_wh2{
        width:36.5%;       
    }
    .fed_wh3{
        width:21%;       
    }
    .fed_wh4{
        width:23%;       
    }
    .fed_wh5{
        width:23.5%;       
    }
    .fed_wh6{
        width:21.5%;       
    }
    .fed_wh7{
        width:20%;       
    }
    .fed_wh7 .form-control{
        margin-bottom:0px !important;
    }
    .fed_wh8{
        width:34.5%;       
    }
    .fed_wh9{
        width:24%;
    }
    .fed_wh10{
        width: 21%;
    }
    .fed_wh11{
        width:27.5%;
    }
    .fed_wh13{
        width:27%;       
    }
    .fed_un1{
        width:19%;
    }
    .fed_un2{
        width:37%;
    }
    .fed_un3{
        width:19%;
    }
    .fed_un4{
        width:25%;
    }
    .fed_un5{
        width:24%;
    }
    .fed_un7,.fed_un8{
        width:25%;
    }
    .fed_un6{
        width:51%;
    }
    .fed_un5 .form-control{
        margin-bottom:0px !important;
    }
    .st_wh1 {
        width: 35%;
    }
    .st_wh5 {
        width: 24%;
    }
    .do_que {
        position: relative;
        display: block;
        text-align: right;
    }
    .do_title{
        text-align: right !important;
        margin-bottom: 12px !important;
        margin-right: 10px !important;
        border-bottom: 1px solid;
        padding-bottom: 5px !important;
    }
    .professional_profession {
        width: 35%;
    }
    .professional_profession.professional_note {
        width: 58%;
    }
    .professional_state {
        width: 13%;
    }
    .professional_effective {
        width: 21%;
    }
    .professional_license {
        width: 20%;
    }
    .professional_expire {
        width: 20%;
    }
    .history_btn{
        width:auto !important;
    }
    .history_btn a{
        padding: 5px 10px !important;
    }
}
@media  only screen and (max-width : 991px){
    .fed_wh1,.fed_wh2,.fed_wh3,.fed_wh4,.fed_wh5,.fed_wh6,.fed_wh7,.fed_wh8,.fed_wh9,.fed_wh10,.fed_wh11,.fed_wh12,.fed_wh13{
        width:50%;
        float:left;
    }
    .fed_un1,.fed_un2,.fed_un3,.fed_un4,.fed_un5,.fed_un6,.fed_un7,.fed_un8,.st_wh1,.st_wh2,.st_wh3,.st_wh4,.st_wh5,.st_wh6{
        width:50%;
        float:left;
    }
    .taxation_3 .col-lg-6{
        width:100%;
    }
    .col-sm-6.cust_label{
        width:auto;
        line-height: 2.5;
    }
    .mt30 {
        margin-top: 0px;
    }
    .w_incorp_date{
        width:58% !important;
        float:left;
    }
    .w_incorp{
        width:auto !important;
        float:left;
        line-height: 2.5;
    }
    .hide-991{
        display:none;
    }
    .Branch .titleleft {
        width: 100%;
        text-align: center;
        border-bottom: 1px solid #6d6d6d;
    }
    .btn-group.btn-toggle{
        align-items: center;
        display:inline-flex !important;
        float:none;
        margin:0px;
    }
    .Branch h1{
        text-align:center;
    }
    .full_991{
        width:100% !important;
    }
}
@media  only screen and (max-width : 962px){
    .nav-tabs > li {
        width: 23% !important;
    }
    .nav-tabs>li>a{
        padding:10px 7px;
    }
}

@media  only screen and (max-width : 767px){
.nav-tabs > li {
    width: 24% !important;
    margin: 5px 0 0 3px !important;
}
.Branch h1{
    padding:3px;
}

}
@media  only screen and (max-width: 600px){
    .fed_wh1,.fed_wh2,.fed_wh3,.fed_wh4,.fed_wh5,.fed_wh6,.fed_wh7,.fed_wh8,.fed_wh9,.fed_wh10,.fed_wh11,.fed_wh12,.fed_wh13{
        width:100%;
    }
    .fed_un1,.fed_un2,.fed_un3,.fed_un4,.fed_un5,.fed_un6,.fed_un7,.fed_un8,.st_wh1,.st_wh2,.st_wh3,.st_wh4,.st_wh5,.st_wh6{
        width:100%;
    }
    .professional_profession,.professional_state,.professional_effective,.professional_license,.professional_expire,.professional_profession.professional_note{
        width:98%;
    }
}
@media  only screen and (max-width: 500px){
    .nav-tabs > li {
        width: 32% !important;
    }
    .nav-tabs>li>a {
        padding: 10px 5px;
    }
    .nav-tabs>li>a{
        font-size:15px !important;
    }
    .questionbox select.form-control{
        white-space: normal;
        height: 65px;
    }
    .w_incorp_date{
        width:100% !important;
        float:left;
    }
    .col-sm-6.cust_label,.w_incorp{
        line-height: 1;
    }
}
.form-horizontal .form-group small.ln{position: relative;  width: 100%; top:0;  right: 9px;}
.form-horizontal .form-group small.ln1{position: relative;  width: 100%; top:0;  right: 9px;}
/*.form-horizontal .form-group label.control-label{  width: 100%; }*/
/*.form-horizontal .form-group .form-control{ margin-bottom:10px; }*/



}
#_corporation:before{
    content: '';
    position: absolute;
    left: 0;
    top: 0;
    width: 16px;
    height: 16px;
    background: #d5dbe0;
    -webkit-border-radius: 16px;
    border-radius: 16px;
}
</style>

<div class="content-wrapper">
    <?php echo $__env->make('fac-Bhavesh-0554.layouts.Headermenu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <section class="content-header page-title"><h1>Admin Profile</h1></section>
	 <section class="content">
	    <div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-body">
					<?php if(session()->has('success')): ?>
                           <div class="alert alert-success alert-dismissable"><?php echo e(session()->get('success')); ?></div>
					<?php endif; ?>
					<?php if(session()->has('error')): ?>
					<div class="alert alert-danger alert-dismissable"><?php echo e(session()->get('error')); ?></div>
					<?php endif; ?>
				
				   
					<div class="row">
						<div class="col-md-12">
							<div class="panel with-nav-tabs panel-primary">
								<div class="panel-heading">
									<ul class="nav nav-tabs" id="myTab">
									    
										<li <?php if(isset($_REQUEST['id'])=='tab3primary'){}else {?>class="active" <?php }?> style="margin-top:0px ;"><a href="#tabprimary" data-toggle="tab" class="alery">Company Info</a></li>
										<li style="margin-top:0px ;"><a href="#tab1primary" data-toggle="tab" class="alery">Contact Info</a></li>
										<li style="margin-top:0px ;"><a href="#tab2primary" data-toggle="tab" class="alery">Security </a></li>
										<li <?php if(isset($_REQUEST['id']) && $_REQUEST['id'] =='tab3primary'){?>class="active" <?php }?> style="margin-top:0px ;"><a href="#tab3primary" data-toggle="tab" class="alery">Formation </a></li>
										<li <?php if(isset($_REQUEST['id'])=='tab5primary'){?>class="active" <?php }else {}?>><a href="#tab5primary" data-toggle="tab" class="alery">License </a></li>
										<li><a href="#tab4primary" data-toggle="tab" class="alery">Taxation </a></li>
										<li><a href="#tab6primary" data-toggle="tab" class="alery">Banking </a></li>
										<li><a href="#tab7primary" data-toggle="tab" class="alery">Other</a></li>
									</ul>
								</div>
							
								<form method="post" class="form-horizontal" id="adminform" enctype="multipart/form-data" id="myform" action="<?php echo e(route('adminprofile.update',Auth::user()->id)); ?>">
									<?php echo e(csrf_field()); ?> <?php echo e(method_field('PATCH')); ?>

									
									    <div class="panel-body">
										<div class="tab-content">
										<?php
										if(isset($_REQUEST['id'])=='tab3primary')
										{
										    ?>
										        <div class="tab-pane fade" id="tabprimary">
										    <?php
										}
										else
										{
										    ?>
										        <div class="tab-pane fade in active" id="tabprimary">
										    <?php
										}
										
										?>
											
												<div class="">
													<div class="Branch">
														<h1>Company Information</h1>
													</div>
												</div>
												<br/>
												<div class="col-md-12 col-sm-12 col-xs-12">
												    
												    <!-- Ankit Patel -->
												    
												   <!-- <input type="text" name="dateFrom" id="dateFrom" readonly="true" value="" class="textbox" />
<input type="text" name="dateTo" id="dateTo" readonly="true" value="" class="textbox" />-->


												<div class="form-group <?php echo e($errors->has('company_name') ? ' has-error' : ''); ?>">
												    <!--<small class="ln">(Legal Name)</small>-->
												<label class="control-label col-md-3">Company Name <small>(Legal Name)</small> : <span class="star-required">*</span> </label>
												<div class="col-lg-6 col-md-7">
												<input type="text" class="form-control" id="" value="<?php echo e(Auth::user()->company_name); ?>" name="company_name">								   
												<?php if($errors->has('company_name')): ?>
												<span class="help-block">
												<strong><?php echo e($errors->first('company_name')); ?></strong>
												</span>
												<?php endif; ?>
												</div>
												</div>
												<div class="form-group<?php echo e($errors->has('dba_name') ? ' has-error' : ''); ?>">
												    <!--<small class="ln1" style="margin-left: -18px;">(DBA)</small>-->
												<label class="control-label col-md-3">Business Name <small>(DBA)</small> : <span class="star-required">*</span></label>
												<div class="col-lg-6 col-md-7">
												<input type="text" class="form-control" id="dba_name" name="dba_name" value="<?php echo e(Auth::user()->dba_name); ?>">				
												<input type="hidden" class="form-control" id="text1" name="text1" value="">
												<?php if($errors->has('dba_name')): ?>
												<span class="help-block">
												<strong><?php echo e($errors->first('dba_name')); ?></strong>
												</span>
												<?php endif; ?>
												</div>
												</div>
												<div class="form-group  <?php echo e($errors->has('address1') ? ' has-error' : ''); ?>">
												<label class="control-label col-md-3">Reg. Address 1  : <span class="star-required">*</span></label>
												<div class="col-lg-6 col-md-7">
												<input type="text" class="form-control" id="address1" name="address1" value="<?php echo e(Auth::user()->address); ?>">								
												<?php if($errors->has('address1')): ?>
												<span class="help-block">
												<strong><?php echo e($errors->first('address1')); ?></strong>
												</span>
												<?php endif; ?>
												</div>
												</div>
												<div class="form-group">
												<label class="control-label col-md-3 p_23 p_cmn_991"> Reg. Address 2 : </label>
												<div class="col-lg-6 col-md-7">
												<input type="text" class="form-control" id="address2" name="address2" value="<?php echo e(Auth::user()->address1); ?>">								
												</div>
												</div>
												<div class="form-group <?php echo e($errors->has('company_city') ? ' has-error' : ''); ?>">
												<label class="control-label col-md-3">City / State / Zip  : <span class="star-required">*</span></label>
												<div class="col-lg-2 col-md-3">
												<input name="company_city" value="<?php echo e(Auth::user()->company_city); ?>" type="text" id="company_city" class="textonly form-control">
												<?php if($errors->has('company_city')): ?>
												<span class="help-block">
												<strong><?php echo e($errors->first('company_city')); ?></strong>
												</span>
												<?php endif; ?>
												</div>
												<div class="<?php echo e($errors->has('company_state') ? ' has-error' : ''); ?>" >
												<div class="col-md-2 col-xs-6">
												<select name="company_state" id="company_state" class="form-control fsc-input">
												<?php if(empty(Auth::user()->company_state)): ?>
												<option value="">State</option>
												<?php else: ?>
												<option value="<?php echo e(Auth::user()->company_state); ?>"><?php echo e(Auth::user()->company_state); ?></option>
												<?php endif; ?>
												<option value="AK">AK</option>
												<option value="AS">AS</option>
												<option value="AZ">AZ</option>
												<option value="AR">AR</option>
												<option value="CA">CA</option>
												<option value="CO">CO</option>
												<option value="CT">CT</option>
												<option value="DE">DE</option>
												<option value="DC">DC</option>
												<option value="FM">FM</option>
												<option value="FL">FL</option>
												<option value="GA">GA</option>
												<option value="GU">GU</option>
												<option value="HI">HI</option>
												<option value="ID">ID</option>
												<option value="IL">IL</option>
												<option value="IN">IN</option>
												<option value="IA">IA</option>
												<option value="KS">KS</option>
												<option value="KY">KY</option>
												<option value="LA">LA</option>
												<option value="ME">ME</option>
												<option value="MH">MH</option>
												<option value="MD">MD</option>
												<option value="MA">MA</option>
												<option value="MI">MI</option>
												<option value="MN">MN</option>
												<option value="MS">MS</option>
												<option value="MO">MO</option>
												<option value="MT">MT</option>
												<option value="NE">NE</option>
												<option value="NV">NV</option>
												<option value="NH">NH</option>
												<option value="NJ">NJ</option>
												<option value="NM">NM</option>
												<option value="NY">NY</option>
												<option value="NC">NC</option>
												<option value="ND">ND</option>
												<option value="MP">MP</option>
												<option value="OH">OH</option>
												<option value="OK">OK</option>
												<option value="OR">OR</option>
												<option value="PW">PW</option>
												<option value="PA">PA</option>
												<option value="PR">PR</option>
												<option value="RI">RI</option>
												<option value="SC">SC</option>
												<option value="SD">SD</option>
												<option value="TN">TN</option>
												<option value="TX">TX</option>
												<option value="UT">UT</option>
												<option value="VT">VT</option>
												<option value="VI">VI</option>
												<option value="VA">VA</option>
												<option value="WA">WA</option>
												<option value="WV">WV</option>
												<option value="WI">WI</option>
												<option value="WY">WY</option>
												</select>
												<?php if($errors->has('company_state')): ?>
												<span class="help-block">
												<strong><?php echo e($errors->first('company_state')); ?></strong>
												</span>
												<?php endif; ?>
												</div>
												</div>
												<div class="">
												<div class="col-md-2 col-xs-6 <?php echo e($errors->has('company_zip') ? ' has-error' : ''); ?>">
												<input name="company_zip" value="<?php echo e(Auth::user()->company_zip); ?>" type="text" id="company_zip" class="zip form-control">
												<?php if($errors->has('company_zip')): ?>
												<span class="help-block">
												<strong><?php echo e($errors->first('company_zip')); ?></strong>
												</span>
												<?php endif; ?>
												</div>
												</div>
												</div>
												<div class="form-group <?php echo e($errors->has('telephone') ? ' has-error' : ''); ?>">
												<label class="control-label col-md-3">Telephone No.  : <span class="star-required">*</span></label>
												<div class="col-lg-2 col-md-3">
												<input name="telephone" value="<?php echo e(Auth::user()->telephone); ?>" type="tel" id="motelephoneile" class="form-control" placeholder="(999) 999-9999"/>
												<?php if($errors->has('telephone')): ?>
												<span class="help-block">
												<strong><?php echo e($errors->first('telephone')); ?></strong>
												</span>
												<?php endif; ?>
												</div>
												<div class="col-md-2 col-xs-6 fsc-form-col fsc-element-margin <?php echo e($errors->has('telephoneNo1Type') ? ' has-error' : ''); ?>">
												<select name="telephoneNo1Type" id="telephoneNo1Type" class="form-control fsc-input" style="height:-1% !important">
												<option value="Office" <?php if(Auth::user()->telephoneNo1Type=='Office'): ?> selected <?php endif; ?>>Office</option><option value="Mobile" <?php if(Auth::user()->telephoneNo1Type=='Mobile'): ?> selected <?php endif; ?>>Mobile</option>
												</select>
												<?php if($errors->has('telephoneNo1Type')): ?>
												<span class="help-block">
												<strong><?php echo e($errors->first('telephoneNo1Type')); ?></strong>
												</span>
												<?php endif; ?>
												</div>
												<div class="col-md-2 col-xs-6 fsc-element-margin">
												<input class="form-control fsc-input" <?php if(Auth::user()->telephoneNo1Type=='Office'): ?> <?php else: ?> readonly="" <?php endif; ?> id="ext1" maxlength="5" name="ext1" value="<?php echo e(Auth::user()->ext1); ?>" placeholder="Ext" type="text">
												</div>
												</div>
												<div class="form-group <?php echo e($errors->has('fax') ? ' has-error' : ''); ?>">
												<label class="control-label col-md-3 p_23 p_cmn_991">Fax No. : </label>
												<div class="col-lg-2 col-md-3">
												<input name="fax" value="<?php echo e(Auth::user()->fax); ?>" type="tel" placeholder="(999) 999-9999" id="fax" class="form-control" />
												<?php if($errors->has('fax')): ?>
												<span class="help-block">
												<strong><?php echo e($errors->first('fax')); ?></strong>
												</span>
												<?php endif; ?>
												</div>
												</div>
												</div>
												<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="form-group <?php echo e($errors->has('website') ? ' has-error' : ''); ?>">
												<label class="control-label col-md-3 p_23 p_cmn_991">Website : </label>
												<div class="col-lg-6 col-md-7">
												<input name="website" value="<?php echo e(Auth::user()->website); ?>" type="text" id="website" class="form-control" />
												<?php if($errors->has('website')): ?>
												<span class="help-block">
												<strong><?php echo e($errors->first('website')); ?></strong>
												</span>
												<?php endif; ?>
												</div>
												</div>
												</div>
												<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="form-group <?php echo e($errors->has('company_email') ? ' has-error' : ''); ?>">
												<label class="control-label col-md-3">Email  : <span class="star-required">*</span></label>
												<div class="col-lg-6 col-md-7">
												<input type="text" class="form-control" id="company_email" name="company_email" value="<?php echo e(Auth::user()->company_email); ?>">				
												<?php if($errors->has('company_email')): ?>
												<span class="help-block">
												<strong><?php echo e($errors->first('company_email')); ?></strong>
												</span>
												<?php endif; ?>
												</div>
												</div>
												</div>
												<div class="Branch">
												<h1>Physical Address</h1>
												</div>
												<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="form-group">
												<label class="control-label col-md-3"></label>
												<div class="col-md-6">
												    <input type="checkbox" id="Samadd1" name="Samadd1" <?php if(!empty(Auth::user()->physical_1)): ?> <?php echo e('checked'); ?> <?php endif; ?>  onclick="FillBilling1(this.form)">
												    <label class="fsc-form-label" for="Samadd1">&nbsp; Same As Company Address</label>
												</div>
												</div>
												<div class="form-group  <?php echo e($errors->has('physical_1') ? ' has-error' : ''); ?>">
												<label class="control-label col-md-3">Physical Address 1  : <span class="star-required">*</span></label>
												<div class="col-lg-6 col-md-7">
												<input type="text" class="form-control" id="physical_1" name="physical_1" value="<?php echo e(Auth::user()->physical_1); ?>" <?php if(!empty(Auth::user()->physical_1)): ?> readonly <?php endif; ?>>
												<?php if($errors->has('physical_1')): ?>
												<span class="help-block">
												<strong><?php echo e($errors->first('physical_1')); ?></strong>
												</span>
												<?php endif; ?>
												</div>
												</div>
												<div class="form-group">
												<label class="control-label col-md-3 p_23 p_cmn_991">Physical Address 2 : </label>
												<div class=" col-lg-6 col-md-7">
												    <input type="text" class="form-control" id="physical_2" name="physical_2" value="<?php echo e(Auth::user()->physical_2); ?>" <?php if(!empty(Auth::user()->physical_1)): ?> readonly <?php endif; ?>>								
												</div>
												</div>
												<div class="form-group <?php echo e($errors->has('physical_city') ? ' has-error' : ''); ?><?php echo e($errors->has('physical_state') ? ' has-error' : ''); ?><?php echo e($errors->has('physical_zip') ? ' has-error' : ''); ?>">
												<label class="control-label col-md-3">City / State / Zip  : <span class="star-required">*</span></label>
												<div class="col-lg-2 col-md-3">
												<input name="physical_city" value="<?php echo e(Auth::user()->physical_city); ?>" type="text" id="physical_city" class="textonly form-control" <?php if(!empty(Auth::user()->physical_1)): ?> readonly <?php endif; ?>>
												<?php if($errors->has('physical_city')): ?>
												<span class="help-block">
												<strong><?php echo e($errors->first('physical_city')); ?></strong>
												</span>
												<?php endif; ?>
												</div>
												<div class="">
												<div class="col-md-2 col-xs-6">
												<select name="physical_state" id="physical_state" class="form-control fsc-input" <?php if(!empty(Auth::user()->physical_1)): ?> disabled <?php endif; ?>>
												    <option value="">State</option>
    												<?php $__currentLoopData = $states; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $st): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    												    <option value="<?php echo e($st->state); ?>" <?php if(Auth::user()->physical_state == $st->state): ?> <?php echo e('selected'); ?> <?php endif; ?>><?php echo e($st->state); ?></option>
    												<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
												</select>
												<input name="physical_state" id="hiddenstate" value="" type="hidden"/>
												<?php if($errors->has('physical_state')): ?>
												<span class="help-block">
												<strong><?php echo e($errors->first('physical_state')); ?></strong>
												</span>
												<?php endif; ?>
												</div>
												</div>
												<div class="">
												<div class="col-md-2 col-xs-6">
												<input name="physical_zip" value="<?php echo e(Auth::user()->physical_zip); ?>" type="text" id="physical_zip" class="zip form-control" <?php if(!empty(Auth::user()->physical_1)): ?> readonly <?php endif; ?>>
												<?php if($errors->has('physical_zip')): ?>
												<span class="help-block">
												<strong><?php echo e($errors->first('physical_zip')); ?></strong>
												</span>
												<?php endif; ?>
												</div>
												</div>
												</div>
												<div class="form-group ">
												<label class="control-label col-md-3">County / Code : <span class="star-required">*</span></label>

												<div class="">
    												<div class="col-lg-2 col-md-3 col-xs-6">
        												<select name="physical_county" id="physical_county" class="form-control fsc-input">
        												    <?php $__currentLoopData = $taxstate; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        												        <option value="<?php echo e($v->county); ?>" <?php if($v->county==Auth::user()->physical_county): ?> selected <?php endif; ?>><?php echo e($v->county); ?></option>
        												    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        												</select>
        												<?php if($errors->has('physical_county')): ?>
        												<span class="help-block">
        												    <strong><?php echo e($errors->first('physical_county')); ?></strong>
        												</span>
        												<?php endif; ?>
    												</div>
												</div>
												
												<div class="">
    												<div class="col-md-2 col-xs-6">
    												    <input name="physical_county_no" value="<?php echo e(Auth::user()->physical_county_no); ?>" type="text" id="physical_county_no" readonly class="form-control">
    												</div>
												</div>
												</div> 
												</div>
											</div>
											
											<div class="tab-pane fade" id="tab1primary">
												<div class="">
													<div class="Branch">
														<h1>Contact Information</h1>
													</div>
												</div>
												<br/>
												<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="form-group <?php echo e($errors->has('firstname') ? ' has-error' : ''); ?>">
												<label class="control-label col-md-3">Name :</label>
												<div class="col-md-7">
												    <div class="row">
												<div class="col-md-2 col-xs-4">
												<select type="text" class="form-control txtOnly" id="minss" name="minss"><option Value="Mr." <?php if(Auth::user()->minss=='Mr.'): ?> selected <?php endif; ?>>Mr.</option><option Value="Mrs."  <?php if(Auth::user()->minss=='Mrs.'): ?> selected <?php endif; ?>>Mrs.</option><option Value="Miss." <?php if(Auth::user()->minss=='Miss.'): ?> selected <?php endif; ?>>Miss.</option></select>			
											  </div>
												<div class="col-md-4 col-xs-8">
												<input type="text" class="form-control textonly" id="firstname" name="firstname" value="<?php echo e(Auth::user()->fname); ?>">				
												<?php if($errors->has('firstname')): ?>
												<span class="help-block">
												<strong><?php echo e($errors->first('firstname')); ?></strong>
												</span>
												<?php endif; ?>
												</div>
												<div class="col-md-2 col-xs-4 <?php echo e($errors->has('middlename') ? ' has-error' : ''); ?>">
												<div class="">
												<input type="text" class="form-control textonly" id="middlename" name="middlename" maxlength="1" value="<?php echo e(Auth::user()->mname); ?>">				
												<?php if($errors->has('middlename')): ?>
												<span class="help-block">
												<strong><?php echo e($errors->first('middlename')); ?></strong>
												</span>
												<?php endif; ?>
												</div>
												</div>
												<div class="col-md-4 col-xs-8 <?php echo e($errors->has('lastname') ? ' has-error' : ''); ?>">
												<input type="text" class="form-control textonly" id="lastname" name="lastname" value="<?php echo e(Auth::user()->lname); ?>">				
												<?php if($errors->has('lastname')): ?>
												<span class="help-block">
												<strong><?php echo e($errors->first('lastname')); ?></strong>
												</span>
												<?php endif; ?>
												</div>
												</div>
												</div>
												</div>
												<div class="form-group <?php echo e($errors->has('contact_address1') ? ' has-error' : ''); ?>">
												<label class="control-label col-md-3">Address 1 :</label>
												<div class="col-md-7">
												<input type="text" class="form-control" id="contact_address1" name="contact_address1" value="<?php echo e(Auth::user()->contact_address1); ?>">				
												<?php if($errors->has('contact_address1')): ?>
												<span class="help-block">
												<strong><?php echo e($errors->first('contact_address1')); ?></strong>
												</span>
												<?php endif; ?>
												</div>
												</div>
												<div class="form-group <?php echo e($errors->has('contact_address2') ? ' has-error' : ''); ?>">
												<label class="control-label col-md-3">Address 2 :</label>
												<div class="col-md-7">
												<input type="text" class="form-control" id="contact_address2" name="contact_address2" value="<?php echo e(Auth::user()->contact_address2); ?>">				
												<?php if($errors->has('contact_address2')): ?>
												<span class="help-block">
												<strong><?php echo e($errors->first('contact_address2')); ?></strong>
												</span>
												<?php endif; ?>
												</div>
												</div>
												<div class="form-group <?php echo e($errors->has('city') ? ' has-error' : ''); ?>">
												<label class="control-label col-md-3">City / State / Zip :</label>
												<div class="col-md-3">
												<input name="city" value="<?php echo e(Auth::user()->city); ?>" type="text" id="city" class="textonly form-control" />
												<?php if($errors->has('city')): ?>
												<span class="help-block">
												<strong><?php echo e($errors->first('city')); ?></strong>
												</span>
												<?php endif; ?>
												</div>
												<div class="<?php echo e($errors->has('state') ? ' has-error' : ''); ?>">
												<div class="col-md-2 col-xs-6">
												<select name="state" id="stateId" class="form-control fsc-input">
												<?php if(empty(Auth::user()->state)): ?>
												<option value="">---Select State---</option>
												<?php else: ?>
												<option value="<?php echo e(Auth::user()->state); ?>"><?php echo e(Auth::user()->state); ?></option>
												<?php endif; ?>
												<option value="AK">AK</option>
												<option value="AS">AS</option>
												<option value="AZ">AZ</option>
												<option value="AR">AR</option>
												<option value="CA">CA</option>
												<option value="CO">CO</option>
												<option value="CT">CT</option>
												<option value="DE">DE</option>
												<option value="DC">DC</option>
												<option value="FM">FM</option>
												<option value="FL">FL</option>
												<option value="GA">GA</option>
												<option value="GU">GU</option>
												<option value="HI">HI</option>
												<option value="ID">ID</option>
												<option value="IL">IL</option>
												<option value="IN">IN</option>
												<option value="IA">IA</option>
												<option value="KS">KS</option>
												<option value="KY">KY</option>
												<option value="LA">LA</option>
												<option value="ME">ME</option>
												<option value="MH">MH</option>
												<option value="MD">MD</option>
												<option value="MA">MA</option>
												<option value="MI">MI</option>
												<option value="MN">MN</option>
												<option value="MS">MS</option>
												<option value="MO">MO</option>
												<option value="MT">MT</option>
												<option value="NE">NE</option>
												<option value="NV">NV</option>
												<option value="NH">NH</option>
												<option value="NJ">NJ</option>
												<option value="NM">NM</option>
												<option value="NY">NY</option>
												<option value="NC">NC</option>
												<option value="ND">ND</option>
												<option value="MP">MP</option>
												<option value="OH">OH</option>
												<option value="OK">OK</option>
												<option value="OR">OR</option>
												<option value="PW">PW</option>
												<option value="PA">PA</option>
												<option value="PR">PR</option>
												<option value="RI">RI</option>
												<option value="SC">SC</option>
												<option value="SD">SD</option>
												<option value="TN">TN</option>
												<option value="TX">TX</option>
												<option value="UT">UT</option>
												<option value="VT">VT</option>
												<option value="VI">VI</option>
												<option value="VA">VA</option>
												<option value="WA">WA</option>
												<option value="WV">WV</option>
												<option value="WI">WI</option>
												<option value="WY">WY</option>
												</select>
												<?php if($errors->has('state')): ?>
												<span class="help-block">
												<strong><?php echo e($errors->first('state')); ?></strong>
												</span>
												<?php endif; ?>
												</div>
												</div>
												<div class="<?php echo e($errors->has('zip') ? ' has-error' : ''); ?>">
												<div class="col-md-2 col-xs-6">
												<input name="zip" value="<?php echo e(Auth::user()->zip); ?>" type="text" id="zip" class="form-control zip" />
												<?php if($errors->has('zip')): ?>
												<span class="help-block">
												<strong><?php echo e($errors->first('zip')); ?></strong>
												</span>
												<?php endif; ?>
												</div>
												</div>
												</div>
												<div class="form-group <?php echo e($errors->has('mobile') ? ' has-error' : ''); ?>">
												<label class="control-label col-md-3">Telephone No. 1:</label>
												<div class="col-md-3">
												<input name="mobile" placeholder="(999) 999-9999" value="<?php echo e(Auth::user()->mobile); ?>" type="tel" id="mobile" class="form-control" />
												<?php if($errors->has('mobile')): ?>
												<span class="help-block">
												<strong><?php echo e($errors->first('mobile')); ?></strong>
												</span>
												<?php endif; ?>
												</div>
												<div class="col-md-2 col-xs-6 fsc-form-col fsc-element-margin <?php echo e($errors->has('mobiletype') ? ' has-error' : ''); ?>">
												<select name="mobiletype" id="mobiletype" class="form-control fsc-input" style="height:-1% imporatnt">
												<option value="Home" <?php if(Auth::user()->mobiletype==='Home'): ?> selected <?php endif; ?>>Office</option>
												<option value="Mobile" <?php if(Auth::user()->mobiletype==='Mobile'): ?> selected <?php endif; ?>>Mobile</option>
												<option value="Other" <?php if(Auth::user()->mobiletype==='Other'): ?> selected <?php endif; ?>>Other</option>
												</select>
												<?php if($errors->has('mobiletype')): ?>
												<span class="help-block">
												<strong><?php echo e($errors->first('mobiletype')); ?></strong>
												</span>
												<?php endif; ?>
												</div>
												<div class="col-md-2 col-xs-6 fsc-element-margin">
												<input class="form-control fsc-input" <?php if(Auth::user()->mobiletype=='Home'): ?> <?php else: ?> readonly="" <?php endif; ?>  id="ext2" maxlength="5"  name="ext2" value="<?php echo e(Auth::user()->ext2); ?>" placeholder="Ext" type="text">
												</div>
												</div>
												<div class="form-group <?php echo e($errors->has('mobile1') ? ' has-error' : ''); ?>">
												<label class="control-label col-md-3">Telephone No. 2:</label>
												<div class="col-md-3">
												<input name="mobile1" placeholder="(999) 999-9999" value="<?php echo e(Auth::user()->mobile1); ?>" type="tel" id="mobile1" class="form-control" />
												<?php if($errors->has('mobile1')): ?>
												<span class="help-block">
												<strong><?php echo e($errors->first('mobile1')); ?></strong>
												</span>
												<?php endif; ?>
												</div>
												<div class="col-md-2 col-xs-6 fsc-form-col fsc-element-margin <?php echo e($errors->has('mobiletype1') ? ' has-error' : ''); ?>">
												<select name="mobiletype1" id="mobiletype1" class="form-control fsc-input" style="height:-1% important">
												<option value="Home" <?php if(Auth::user()->mobiletype1==='Home'): ?> selected <?php endif; ?>>Office</option>
												<option value="Mobile" <?php if(Auth::user()->mobiletype1==='Mobile'): ?> selected <?php endif; ?>>Mobile</option>
												<option value="Other" <?php if(Auth::user()->mobiletype1==='Other'): ?> selected <?php endif; ?>>Other</option>
												</select>
												<?php if($errors->has('mobiletype1')): ?>
												<span class="help-block">
												<strong><?php echo e($errors->first('mobiletype1')); ?></strong>
												</span>
												<?php endif; ?>
												</div>
												<div class="col-md-2 col-xs-6 fsc-element-margin">
												<input class="form-control fsc-input" <?php if(Auth::user()->mobiletype1=='Home'): ?> <?php else: ?> readonly="" <?php endif; ?> id="ext21" maxlength="5"  name="ext21" value="<?php echo e(Auth::user()->ext21); ?>" placeholder="Ext" type="text">
												</div>
												</div>
												<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
												</div>
												<div class="form-group ">
												<label class="control-label col-md-3"></label>
												<div class="col-md-8">
												<input type="checkbox" id="billingtoo2" <?php if(!empty(Auth::user()->contact_fax)): ?> checked <?php endif; ?> name="billingtoo2" onclick="FillBilling2(this.form)"><label class="fsc-form-label" for="billingtoo2">&nbsp; Same As Company Fax</label>
												</div>
												</div>
												<div class="form-group <?php echo e($errors->has('contact_fax') ? ' has-error' : ''); ?>">
												<label class="control-label col-md-3">Fax No. :</label>
												<div class="col-md-3">
												<input name="contact_fax" placeholder="(000)000-0000" value="<?php echo e(Auth::user()->contact_fax); ?>" type="tel" id="contact_fax" class="form-control" />
												<?php if($errors->has('contact_fax')): ?>
												<span class="help-block">
												<strong><?php echo e($errors->first('contact_fax')); ?></strong>
												</span>
												<?php endif; ?>
												</div>
												</div>
												<div class="form-group  <?php echo e($errors->has('email_1') ? ' has-error' : ''); ?>">
												<label class="control-label col-md-3">Email :</label>
												<div class="col-md-7">
												<input type="text" class="form-control" id="email_1"  name="email_1" value="<?php echo e(Auth::user()->email_1); ?>">								
												<?php if($errors->has('email_1')): ?>
												<span class="help-block">
												<strong><?php echo e($errors->first('email_1')); ?></strong>
												</span>
												<?php endif; ?>
												</div>
												</div>
												</div>
											</div>
											
											<div class="tab-pane fade" id="tab2primary">
												<div class="">
													<div class="Branch">
														<h1>Security Information</h1>
													</div>
												</div>
												<br/>
												<div class="col-md-12 col-sm-12 col-xs-12">
													<div class="form-group  <?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
														<label class="control-label col-md-3">Username :</label>
														<div class="col-md-6">
														<input type="text" class="form-control" id="email"  name="email" value="<?php echo e(Auth::user()->email); ?>">								
														<?php if($errors->has('email')): ?>
														<span class="help-block">
														<strong><?php echo e($errors->first('email')); ?></strong>
														</span>
														<?php endif; ?>
														</div>
														<div class="col-md-3" style="margin-top: 10px;">
                                                               <input type="checkbox" id="myCheck" onclick="myFunction()" name="myCheck"> <label class="fsc-form-label" for="myCheck"> Security Question </label>
                                                        </div>
													</div>
												
                                                        <div class="questionbox" id="questionbox">
													<div class="form-group <?php echo e($errors->has('question1') ? ' has-error' : ''); ?>">
														<label class="control-label col-md-3">Security Q. 1 :</label>
														<div class="col-md-6">
															<select name="question1" id="question1" class="form-control">
																<option value="">---Select---</option>
																<option value="What was your favorite place to visit as a child?" <?php if(Auth::user()->question1=='What was your favorite place to visit as a child?'): ?> selected <?php endif; ?> >What was your favorite place to visit as a child?</option>
																<option value="Who is your favorite actor, musician, or artist?" <?php if(Auth::user()->question1=='Who is your favorite actor, musician, or artist?'): ?> selected <?php endif; ?>>Who is your favorite actor, musician, or artist?</option>
																<option value="What is the name of your favorite pet?" <?php if(Auth::user()->question1=='What is the name of your favorite pet?'): ?> selected <?php endif; ?>>What is the name of your favorite pet?</option>
																<option <?php if(Auth::user()->question1=='In what city were you born?'): ?> selected <?php endif; ?> value="In what city were you born?">In what city were you born?</option>
																<option <?php if(Auth::user()->question1=='What is the name of your first school?'): ?> selected <?php endif; ?> value="What is the name of your first school?">What is the name of your first school?</option>
															</select>
															<?php if($errors->has('question1')): ?>
															<span class="help-block">
															<strong><?php echo e($errors->first('question1')); ?></strong>
															</span>
															<?php endif; ?>
														</div>
													</div>
													<div class="form-group <?php echo e($errors->has('answer1') ? ' has-error' : ''); ?>">
														<label class="control-label col-md-3">Answer 1:</label>
														<div class="col-md-6">
															<input name="answer1" type="password" value="<?php echo e(Auth::user()->answer1); ?>" type="text" id="answer1" class="form-control" />
															<?php if($errors->has('answer1')): ?>
															<span class="help-block">
															<strong><?php echo e($errors->first('answer1')); ?></strong>
															</span>
															<?php endif; ?>
														</div>
														<div class="col-md-3" style="margin-top: 10px;">
														    <input type="checkbox" id="ans1" onclick="myFunctionone1()"> <label class="fsc-form-label" for="ans1">Show Answer </label>
														</div>
													</div>
													
													<div class="form-group  <?php echo e($errors->has('question2') ? ' has-error' : ''); ?>">
														<label class="control-label col-md-3">Security Q. 2 :</label>
														<div class="col-md-6">
															<select name="question2" id="question2" class="form-control">
															<option value="">---Select---</option>
															<option value="What is your favorite movie?"  <?php if(Auth::user()->question2=='What is your favorite movie?'): ?> selected <?php endif; ?>>What is your favorite movie?</option>
															<option value="What was the make of your first car?" <?php if(Auth::user()->question2=='What was the make of your first car?'): ?> selected <?php endif; ?>>What was the make of your first car?</option>
															<option value="What is your favorite color?" <?php if(Auth::user()->question2=='What is your favorite color?'): ?> selected <?php endif; ?>>What is your favorite color?</option>
															<option value="What is your father middle name?" <?php if(Auth::user()->question2=='What is your father middle name?'): ?> selected <?php endif; ?>>What is your father's middle name?</option>
															<option value="What is the name of your first grade teacher?" <?php if(Auth::user()->question2=='What is the name of your first grade teacher?'): ?> selected <?php endif; ?>>What is the name of your first grade teacher?</option>
															</select>
															<?php if($errors->has('question2')): ?>
															<span class="help-block">
															<strong><?php echo e($errors->first('question2')); ?></strong>
															</span>
															<?php endif; ?>
														</div>
													</div>
													
													<div class="form-group <?php echo e($errors->has('answer2') ? ' has-error' : ''); ?>">
														<label class="control-label col-md-3">Answer 2:</label>
														<div class="col-md-6">
															<input name="answer2" type="password" value="<?php echo e(Auth::user()->answer2); ?>" type="text" id="answer2" class="form-control" />
															<?php if($errors->has('answer2')): ?>
															<span class="help-block">
															<strong><?php echo e($errors->first('answer2')); ?></strong>
															</span>
															<?php endif; ?>
														</div>
														<div class="col-md-3" style="margin-top: 10px;">
														    <input type="checkbox" id="anstwo" onclick="myFunctionone2()"> <label class="fsc-form-label" for="anstwo">Show Answer </label>
														</div>
													</div>
													
													<div class="form-group <?php echo e($errors->has('question3') ? ' has-error' : ''); ?>">
														<label class="control-label col-md-3">Security Q. 3:</label>
														<div class="col-md-6">
															<select name="question3" id="question3" class="form-control">
															<option value="">---Select---</option>
															<option value="What was your high school mascot?" <?php if(Auth::user()->question3=='What was your high school mascot?'): ?> selected <?php endif; ?>>What was your high school mascot?</option>
															<option value="Which is your favorite web browser?" <?php if(Auth::user()->question3=='Which is your favorite web browser?'): ?> selected <?php endif; ?>>Which is your favorite web browser?</option>
															<option value="In what year was your father born?" <?php if(Auth::user()->question3=='In what year was your father born?'): ?> selected <?php endif; ?>>In what year was your father born?</option>
															<option value="What is the name of your favorite childhood friend?" <?php if(Auth::user()->question3=='What is the name of your favorite childhood friend?'): ?> selected <?php endif; ?>>What is the name of your favorite childhood friend?</option>
															<option value="What was your favorite food as a child?" <?php if(Auth::user()->question3=='What was your favorite food as a child?'): ?> selected <?php endif; ?>>What was your favorite food as a child?</option>
															</select>
															<?php if($errors->has('question3')): ?>
															<span class="help-block">
															<strong><?php echo e($errors->first('question3')); ?></strong>
															</span>
															<?php endif; ?>
														</div>
													</div>
													
													<div class="form-group <?php echo e($errors->has('answer3') ? ' has-error' : ''); ?>">
														<label class="control-label col-md-3">Answer 3:</label>
														<div class="col-md-6">
															<input name="answer3" type="password" value="<?php echo e(Auth::user()->answer3); ?>" type="text" id="answer3" class="form-control" />
															
															<?php if($errors->has('answer3')): ?>
															<span class="help-block">
															<strong><?php echo e($errors->first('answer3')); ?></strong>
															</span>
															<?php endif; ?>
														</div>
														<div class="col-md-3" style="margin-top: 10px;">
														    <input type="checkbox" id="ansthree" onclick="myFunctionone3()"> <label class="fsc-form-label" for="ansthree">Show Answer </label>
														</div>
													</div>
													</div>
												</div>
												
											
												
											</div>
											
											<div class="tab-pane fade <?php if(isset($_REQUEST['id']) && $_REQUEST['id'] == 'tab3primary'){?>in active <?php }?>" id="tab3primary">
										
												
												<div class="">
													<div class="Branch">
														<h1>Company Formation Information</h1>
													</div>
												</div>
												
												<br/>
												
												<div class="col-md-12 col-sm-12 col-xs-12">
													<div class="form-group <?php echo e($errors->has('state_of_formation') ? ' has-error' : ''); ?>">
													<label class="control-label col-md-3 col-sm-6 cust_label">State of Formation :</label>
													<div class="col-md-2 col-sm-7">
													<select name="state_of_formation" id="state_of_formation" class="form-control fsc-input state_of_formation">
													<?php if(empty(Auth::user()->state_of_formation)): ?>
													<option value="">---Select State---</option>
													<?php else: ?>
													<option value="<?php echo e(Auth::user()->state_of_formation); ?>"><?php echo e(Auth::user()->state_of_formation); ?></option>
													<?php endif; ?>
													<option value="AK">AK</option>
													<option value="AS">AS</option>
													<option value="AZ">AZ</option>
													<option value="AR">AR</option>
													<option value="CA">CA</option>
													<option value="CO">CO</option>
													<option value="CT">CT</option>
													<option value="DE">DE</option>
													<option value="DC">DC</option>
													<option value="FM">FM</option>
													<option value="FL">FL</option>
													<option value="GA">GA</option>
													<option value="GU">GU</option>
													<option value="HI">HI</option>
													<option value="ID">ID</option>
													<option value="IL">IL</option>
													<option value="IN">IN</option>
													<option value="IA">IA</option>
													<option value="KS">KS</option>
													<option value="KY">KY</option>
													<option value="LA">LA</option>
													<option value="ME">ME</option>
													<option value="MH">MH</option>
													<option value="MD">MD</option>
													<option value="MA">MA</option>
													<option value="MI">MI</option>
													<option value="MN">MN</option>
													<option value="MS">MS</option>
													<option value="MO">MO</option>
													<option value="MT">MT</option>
													<option value="NE">NE</option>
													<option value="NV">NV</option>
													<option value="NH">NH</option>
													<option value="NJ">NJ</option>
													<option value="NM">NM</option>
													<option value="NY">NY</option>
													<option value="NC">NC</option>
													<option value="ND">ND</option>
													<option value="MP">MP</option>
													<option value="OH">OH</option>
													<option value="OK">OK</option>
													<option value="OR">OR</option>
													<option value="PW">PW</option>
													<option value="PA">PA</option>
													<option value="PR">PR</option>
													<option value="RI">RI</option>
													<option value="SC">SC</option>
													<option value="SD">SD</option>
													<option value="TN">TN</option>
													<option value="TX">TX</option>
													<option value="UT">UT</option>
													<option value="VT">VT</option>
													<option value="VI">VI</option>
													<option value="VA">VA</option>
													<option value="WA">WA</option>
													<option value="WV">WV</option>
													<option value="WI">WI</option>
													<option value="WY">WY</option>
													</select>			
													<?php if($errors->has('state_of_formation')): ?>
													<span class="help-block">
													<strong><?php echo e($errors->first('state_of_formation')); ?></strong>
													</span>
													<?php endif; ?>
													</div>
													<label class="control-label col-lg-2 col-md-4 w_incorp" style="text-align:left !important;">Date of Incorporation :</label>
                                                        <div class="col-lg-2 col-md-3 w_incorp_date" >
                                                            <!--<input type="text" id="due_date4" name="due_date4" value="<?php echo e(Auth::user()->due_date4); ?>" class="form-control"/>-->
                                                            <input type="text" id="due_date4" name="due_date4" value="<?php echo date('m-d-Y',strtotime(Auth::user()->due_date4))?>" class="form-control effective_date1"/>
                                                        </div>
													</div>
													<div class="form-group <?php echo e($errors->has('legal_name') ? ' has-error' : ''); ?>">
													<label class="control-label col-md-3">Legal Name :</label>
													<div class="col-lg-6 col-md-9">
													<input type="text" class="form-control" id="legal_name1" readonly name="legal_name1" value="<?php echo e(Auth::user()->legal_name); ?>"><input type="text" style="display:none" class="form-control" id="legal_name" readonly name="legal_name" value="<?php echo e(Auth::user()->company_name); ?>">				
													<?php if($errors->has('legal_name')): ?>
													<span class="help-block">
													<strong><?php echo e($errors->first('legal_name')); ?></strong>
													</span>
													<?php endif; ?>
													</div>
													</div>
													
													<div class="form-group <?php echo e($errors->has('contact_number') ? ' has-error' : ''); ?>">
													<label class="control-label col-md-3">Control Number :</label>
													<div class="col-lg-2 col-md-3">
													<input type="text" class="form-control" maxlength="7" id="contact_number1" name="contact_number" value="<?php echo e(Auth::user()->contact_number); ?>" style="text-transform: capitalize;">			
													<?php if($errors->has('contact_number')): ?>
													<span class="help-block">
													<strong><?php echo e($errors->first('contact_number')); ?></strong>
													</span>
													<?php endif; ?>
													</div>
													<div class="col-lg-4 col-md-6">
													<?php
													    if(isset($formation->record_status)!='')
													    {
													        ?>
													           <input type="text" class="form-control"  value="<?php echo e($formation->record_status); ?>" placeholder="Corporation Status"  style="text-transform: capitalize;" readonly>			    
													        <?php
													    }
													    else
													    {
													        ?>
													        <input type="text" class="form-control"  value="<?php echo e(Auth::user()->record_status); ?>" placeholder="Corporation Status"  style="text-transform: capitalize;" readonly>			
													        <?php
													    }
													?>
                                                        
                                                    </div>
													<!--
													<div class="col-md-2"><a class="btn btn-primary" onclick="ToggleReadOnlyState ();">Edit</a>	 </div> -->
													</div>
                                                    <div class="form-group">
                                                    <label for="pass1" class="col-md-3 control-label hide-991"></label>
                                                        <?php if(Auth::user()->soscertificate != '' && Auth::user()->sosaoi !=''): ?>
                                                            <div class="col-lg-2 col-md-3 col-xs-6" style="margin-left: 1%;">
                                                                <label id="<?php if(Auth::user()->formation=='Corporation'): ?><?php echo e('_corporation_lbl_selected'); ?><?php else: ?><?php echo e('_corporation_lbl'); ?><?php endif; ?>"> Corporation</label>
                                                            </div>
                                                            <div class="col-lg-2 col-md-3 col-xs-5">
                                                               <label id="<?php if(Auth::user()->formation=='LLC'): ?><?php echo e('_corporation_lbl_selected'); ?><?php else: ?><?php echo e('_corporation_lbl'); ?><?php endif; ?>"> LLC</label>
                                                            </div>
                                                            <input type='hidden' name="formation" value="<?php echo e(Auth::user()->formation); ?>" />
                                                        <?php else: ?>
                                                            <div class="col-lg-2 col-md-3 col-xs-6">
                                                                <input type="radio"  class="corporation" id="vcorporation" style="height: 17px;width: 17px;vertical-align: middle;margin: -2px 0 0 text-transform: capitalize;"
                                                                value="Corporation">
                                                                Corporation
                                                            </div>
                                                            <div class="col-lg-2 col-md-3 col-xs-5">
                                                                <input type="radio" value="LLC"  class="corporation1" id="vcorporation1" style="height: 17px;width: 17px;vertical-align: middle;margin: -2px 0 0;" name="corporation">
                                                                LLC
                                                            </div>
                                                        <?php endif; ?>
                                                        <input type="hidden" value="<?php echo Auth::user()->formation;?>" id="corpid">
                                                    
                                                    </div>
													<div class="form-group">
    													<label class="control-label col-md-3"> Certificate of <span class="hide3">Corporation :</span>
    													<span class="show3" style="display:none;">Organization </span></label>
        													<?php if(Auth::user()->soscertificate !='')
                                                        {?>
                                                        <div class="col-md-4">
                                                             <a data-toggle="modal" num="SOS Certificate" 
                                                             class="btn btn-info btn3d Certificate-btn btn3d btn-info
                                                             Certificate-btn openBtncertificate Pro-btn">Certificate of <span  class="hidee1" style="width:230px;">Corporation</span>
                                                             <span class="show1" style="display:none;">Organization</span></a>
                                                        </div>
                                                        <?php
                                                        }
                                                       ?> 
                                                        <div class="col-md-3">
                                                        <label class="file-upload btn btn-primary">								
                                                        
        							                        <input type="file" name="soscertificate" id="soscertificate" class="form-control"/>Browse for file ... </label>
        						                            <input type="hidden" name="soscertificate1" value="<?php echo e(Auth::user()->soscertificate); ?>">&nbsp;
        						                            <a class="btn-action btn-delete" href=""><i class="fa fa-trash"></i></a>
                                                            <!--<p id="file-name"></p>-->
                                                       </div>
    												</div>
													
												    <div class="form-group">
													<label class="control-label col-md-3">Articles Of <span class="hide4" style="width:230px;">Incorporation</span>
													<span class="show4" style="display:none;">Organization </span>:</label>
    												<?php if(Auth::user()->sosaoi !='')
                                                    {?>
                                                   <div class="col-md-4">
                                                       
                                                       <a data-toggle="modal" num="SOS-AOI" class="btn btn-info btn3d Certificate-btn btn3d btn-info Certificate-btn openBtnaoi Pro-btn">Articles Of <span class="hide4" style="width:230px;">Incorporation</span>
                                                       <span class="show4" style="display:none;">Organization </span></a>
                                                    </div>
                                                    <?php
                                                    }
                                                    ?>
                                                    <div class="col-md-3">
                                                   
    							                    <label class="file-upload btn btn-primary">								
                                                        <input type="file" name="sosaoi" id="sosaoi" class="form-control"/>Browse for file ... </label>
    						                            <input type="hidden" name="sosaoi1" value="<?php echo e(Auth::user()->sosaoi); ?>">&nbsp;
    						                            <a class="btn-action btn-delete" href=""><i class="fa fa-trash"></i></a>
                                                        <!--<p id="file-name1"></p>-->
                                                   </div>
                                                
                                                
                                             	</div>
												<div class="form-group <?php echo e($errors->has('agent_fname') ? ' has-error' : ''); ?>">
													<label class="control-label col-md-3">Agent Name:</label>
													<div class="col-lg-2 col-md-3">
													<input name="agent_fname" value="<?php echo e(Auth::user()->agent_fname); ?>" type="text" id="agent_fname" placeholder="First name" class="textonly form-control" />
													<?php if($errors->has('agent_fname')): ?>
													<span class="help-block">
													<strong><?php echo e($errors->first('agent_fname')); ?></strong>
													</span>
													<?php endif; ?>
													</div>
													
													<div class="<?php echo e($errors->has('agent_mname') ? ' has-error' : ''); ?>">
													<div class="col-lg-2 col-md-3 col-xs-4">
													<input name="agent_mname" value="<?php echo e(Auth::user()->agent_mname); ?>" maxlength="1" type="text" placeholder="Middle" id="agent_mname" class="textonly form-control" />
													<?php if($errors->has('agent_mname')): ?>
													<span class="help-block">
													<strong><?php echo e($errors->first('agent_mname')); ?></strong>
													</span>
													<?php endif; ?>
													</div>
													</div>
													
													<div class="<?php echo e($errors->has('agent_lname') ? ' has-error' : ''); ?>">
													<div class="col-lg-2 col-md-3 col-xs-8">
													<input name="agent_lname"  value="<?php echo e(Auth::user()->agent_lname); ?>" type="text" placeholder="Last Name" id="agent_lname" class="textonly form-control" />
													<?php if($errors->has('agent_lname')): ?>
													<span class="help-block">
													<strong><?php echo e($errors->first('agent_lname')); ?></strong>
													</span>
													<?php endif; ?>
													</div>
													</div>
												</div>
													
												<div class="form-group">
													<label class="control-label col-md-3">Renewal Date : </label>
													<div class="col-lg-2 col-md-3">
													<?php 
													if(isset($formation->formation_yearbox)!='')
													{
    													 if($formation->formation_yearbox==1)
    													 {
    													   ?>
    													   <input name="agent_expiredate" readonly value="Apr-01-2021" type="text" id="agent_expiredate" placeholder="Renew Date" class="textonly form-control"/>
    													   <?php  
    													 }
    													 else if($formation->formation_yearbox==2)
    													 {
    													   ?>
    													   <input name="agent_expiredate" readonly value="Apr-01-2022" type="text" id="agent_expiredate" placeholder="Renew Date" class="textonly form-control"/>
    													   <?php
    												     }
    													 else if($formation->formation_yearbox==3)
    													 {
    													   ?>
    													    <input name="agent_expiredate" readonly value="Apr-01-2023" type="text" id="agent_expiredate" placeholder="Renew Date" class="textonly form-control"/>
    													   <?php
    												     }
    												 }
												     else
												     {
												            ?>
			                                                <input name="agent_expiredate" readonly value="Apr-01-<?php echo date('Y');?>" 
			                                                type="text" id="agent_expiredate" placeholder="Renew Date" class="textonly form-control"/>									         
			                                             <?php    
												     }
													 ?>
													</div>
													<div class="">
													<div class="col-lg-2 col-md-3">
													   <!--<a style="display:block; cursor:pointer;"  data-toggle="modal" data-target="#renewRecord" class="btn_new btn-renew disabled">Renew Record</a>-->
													   <a style="display:block; cursor:pointer; margin-top:5px;"  data-toggle="modal" class="btn_new btn-renew" href="https://financialservicecenter.net/fac-Bhavesh-0554/adminworkstatus">Renew Record</a>
													</div>
													<div class="col-lg-2 col-md-3">
													<a style="display:block; margin-top:5px;" href="https://ecorp.sos.ga.gov/Account" target="_blank" class="btn_new btn-renew">Renew Here</a>
													</div>
													</div>
													</div>
												</div>
												
												<div class="">
													<div class="Branch">
														<h1>Shareholder / Officer Information</h1>
													</div>
												</div>
												<br/>
												<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
													<input type="checkbox" class="" id="billingtoo" name="billingtoo" onclick="FillBilling(this.form)" <?php if(empty($agent)): ?> <?php else: ?> checked <?php endif; ?>>
												</div> 
												<div class="">
													<input class="form-control" name="agent1"  type="hidden" id="agent1" placeholder="First name" class="textonly form-control" />
													<input class="form-control" name="agent2"  type="hidden" id="agent2" placeholder="First name" class="textonly form-control" />
													<input class="form-control" name="agent3"  type="hidden" id="agent3" placeholder="First name" class="textonly form-control" />
													<input class="form-control" name="agent4"  type="hidden" id="agent4" placeholder="First name" class="textonly form-control" />
													<input class="form-control" name="agent5"  type="hidden" id="agent5" placeholder="First name" class="textonly form-control" />
													<input class="form-control" name="agent6"  type="hidden" id="agent6" placeholder="First name" class="textonly form-control" />
													<div class="">
														<div class="share_tabs_main">
															<div class="share_tabs">
																<div class="share_tab share_firstn" style="margin: 10px 1% -10px 1%;">
																    <label style="font-size: 14px;color:#404040">First Name</label>
																</div>
																<div class="share_tab share_m" style="margin: 10px 1% -10px 1%;">
																    <label style="font-size: 14px;color:#404040">M</label>
																
																</div>
																<div class="share_tab share_lastn" style="margin: 10px 1% -10px 1%;">
																    <label style="font-size: 14px;color:#404040">Last Name</label>
																
																</div>
																<div class="share_tab share_position" style="margin: 10px 1% -10px 1%;">
																    <label style="font-size: 14px;color:#404040">Position</label>
																
																</div>
																<div class="share_tab share_persentage" style="width: 9%; margin: 10px 1% -10px 1%;">
																    <label style="font-size: 14px;color:#404040">Percentage</label>
																
																</div>
																<div class="share_tab share_date" style="margin: 10px 1% -10px 1%;">
																    <label style="font-size: 14px;color:#404040">Effective Date</label>
																    
																</div>
																<div class="share_tab share_date" style="width: 10.5%;margin: 10px 1% -10px 1%;">
																    <label style="font-size: 14px;color:#404040">Status</label>
																 
																</div>
															
															</div>
														</div>
													</div>
												<?php $admin1 = count($admin_shareholder);?>
												
													<?php $__currentLoopData = $admin_shareholder; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ak): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
													<?php if($ak->agent_fname1==NULL): ?>
													<?php else: ?> 
														<style>
														 .input_fields_wrap_shareholder{ display:none}   
														</style>													
														<div class="">
														<div class="share_tabs_main" id="input_fields_wrap_2">
															<div class="share_tabs <?php echo e($errors->has('agent_fname1') ? ' has-error' : ''); ?>">
																<div class="share_tab share_firstn">
																   
																<input name="conid[]" value="<?php echo e($ak->id); ?>" type="hidden" placeholder="Last Name" id="conid" class="textonly form-control" />
																<input class="form-control" name="agent_fname1[]" value="<?php echo e($ak->agent_fname1); ?>" type="text" id="agent_fname1" placeholder="First names" class="textonly form-control" />
																</div>
																<div class="share_tab share_m">
																   
																<input class="form-control" name="agent_mname1[]"  value="<?php echo e($ak->agent_mname1); ?>" type="text" placeholder="M" id="agent_mname1" class="textonly form-control" />
																</div>
																<div class="share_tab share_lastn">
																  
																<input class="form-control" name="agent_lname1[]" value="<?php echo e($ak->agent_lname1); ?>" type="text" placeholder="Last Name" id="agent_lname1" class="textonly form-control" />
																</div>
																<div class="share_tab share_position">
																
																<select class="form-control agentsposition" name="agent_position[]" id="agent_position11"  >
																<option value="">Position</option>
																<option <?php if($ak->agent_position=='Agent'): ?>  Selected hidden  <?php else: ?>  <?php if(empty($agent->id)): ?> <?php else: ?>  hidden <?php endif; ?> <?php endif; ?> value="Agent">Agent</option>
																<option <?php if($ak->agent_position=='Agent'): ?>  disabled  <?php else: ?> <?php if($ak->agent_position=='Sec'): ?> disabled @ednif  <?php endif; ?> <?php endif; ?> <?php if($ak->agent_position=='CEO'): ?>  selected hidden  <?php else: ?> <?php if(empty($ceo->id)): ?>   <?php else: ?>  hidden <?php endif; ?>  <?php endif; ?>  value="CEO">CEO</option>
																<option <?php if($ak->agent_position=='Agent'): ?>  disabled  <?php else: ?> <?php if($ak->agent_position=='Sec'): ?> disabled @ednif  <?php endif; ?> <?php endif; ?> <?php if($ak->agent_position=='CFO'): ?>  Selected hidden  <?php else: ?> <?php if(empty($cfo->id)): ?>  <?php else: ?>  hidden  <?php endif; ?> <?php endif; ?> value="CFO">CFO</option>
																<option  <?php if($ak->agent_position=='Agent'): ?>  disabled  <?php else: ?> <?php if($ak->agent_position=='Sec'): ?> disabled @ednif  <?php endif; ?> <?php endif; ?> <?php if($ak->agent_position=='Secretary'): ?> Selected hidden <?php else: ?>  <?php if(empty($sece->id)): ?>  <?php else: ?>  hidden  <?php endif; ?>  <?php endif; ?>  value="Secretary" >Secretary</option>
																<option <?php if(($ak->agent_position=='Secretary' && $ak->agent_position=='CEO' && $ak->agent_position=='CFO')): ?> hidden  <?php endif; ?>  <?php if($ak->agent_position=='Sec'): ?> Selected  hidden  <?php else: ?> <?php if(empty($sec->id)): ?>  <?php else: ?>  hidden  <?php endif; ?> <?php endif; ?> value="Sec">CEO / CFO / Sec.</option>
																</select>
																</div>
																<div class="share_tab share_persentage" style="width: 9%;">
																   
																<input name="agent_per[]" value="<?php if($ak->agent_per >0): ?>  <?php echo e($ak->agent_per); ?> <?php endif; ?>" style="padding: 0px;text-align:right;" type="text" placeholder="" id="agent_per11" class="txtOnly form-control num numeric1" style="text-align:right;" /><div class="cc"></div>
																</div>
																<div class="share_tab share_date">
																   
																    <?php $date=date_create($ak->effective_date);?>
																<input name="effective_date[]" value="<?php echo e(date_format($date,"M-d-Y")); ?>" placeholder="Effective Date" style="padding: 6px 5px;"  id="effective_date" class="form-control effective_date2"  style="text-align:right"/>
																</div>
																<div class="share_tab share_date" style="width: 10.5%;">
																<select class="form-control greenText" onchange="this.className=this.options[this.selectedIndex].className"
    class="greenText" name="agentstatus[]" id="agentstatus">
																<option value="">Status</option>
																<option value="Active" <?php if($ak->agentstatus=='Active'): ?>  Selected <?php endif; ?> class="greenText">Active</option>
															    <option value="In-Active" <?php if($ak->agentstatus=='In-Active'): ?>  Selected  <?php endif; ?> class="redText">In-Active</option>
																</select>
																</div>
																<div class="share_add aaa" style="width:auto">
																   
																<a href="#myModalk_<?php echo e($ak->id); ?>"  role="button" style="width: 39px;" class="btn btn-danger removebtn" title="Add field" data-toggle="modal"><i class="fa fa-minus"></i></a>
																</div>
															</div>
														</div>
													</div>
													
													<?php endif; ?>
													<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
													
													<div class="">
													    <div class="mainfile">
														<div class="input_fields_wrap input_fields_wrap_shareholder">
															<div class="input_fields_wrap_1">
																<input name="conid[]" value="" type="hidden" placeholder="Last Name" id="conid" class="textonly form-control" />
																<div class="share_tabs_main">
																	<div class="share_tabs ">
																		<div class="share_tab share_firstn">
																		<input name="agent_fname1[]" value="" type="text" id="agent_fname2" placeholder="First names" class="textonly form-control" />
																		</div>
																		<div class="share_tab share_m">
																		<input name="agent_mname1[]"  value="" type="text" placeholder="M" id="agent_mname2" class="textonly form-control" />
																		</div>
																		<div class="share_tab share_lastn">
																		<input name="agent_lname1[]" value="" type="text" placeholder="Last Name" id="agent_lname2" class="textonly form-control" />
																		</div>
																		<div class="share_tab share_position">
																		<select name="agent_position[]" id="agent_position" class="form-control agent_position agentsposition" >
																		<option value="">Position</option>
																		<option value="Agent" <?php if(empty($agent->agent_position)): ?> <?php else: ?> disabled <?php endif; ?>>Agent</option>
																		<option value="CEO" <?php if(empty($sec->agent_position)): ?>  <?php else: ?> disabled <?php endif; ?> <?php if(empty($ceo->agent_position)): ?> <?php else: ?> disabled <?php endif; ?>>CEO</option>
																		<option value="CFO" <?php if(empty($sec->agent_position)): ?>  <?php else: ?> disabled <?php endif; ?> <?php if(empty($cfo->agent_position)): ?>  <?php else: ?> disabled <?php endif; ?>>CFO</option>
																		<option value="Secretary" <?php if(empty($sec->agent_position)): ?>  <?php else: ?> disabled <?php endif; ?> <?php if(empty($sece->agent_position)): ?>  <?php else: ?> disabled <?php endif; ?>>Secretary</option>
																		<option  value="Sec" <?php if((empty($sece->agent_position) && empty($cfo->agent_position) && empty($ceo->agent_position))): ?> <?php else: ?> disabled <?php endif; ?> <?php if(empty($sec->agent_position)): ?>  <?php else: ?> disabled <?php endif; ?>   >CEO / CFO / Sec.</option>
																		</select>
																		</div>
																		<div class="share_tab share_persentage" style="width: 9%;">
																		<input name="agent_per[]" value="" type="text" placeholder="" id="agent_per" style="padding: 6px 5px;"  placeholder="Feb-07-1997" class="txtOnly form-control numeric1  num"  style="text-align:right"/><div class="cc"></div>
																		</div>
																		<div class="share_tab share_date">
																		<input name="effective_date[]" value="" maxlength="10" type="text" style="padding: 6px 5px;"  placeholder="Effective Date" id="effective_date" class="txtOnly form-control effective_date2"  style="text-align:right"/>
																		</div>
																		<div class="share_tab share_date" style="width: 10.5%;">
																<select class="form-control" name="agentstatus[]" id="agentstatus"  >
																<option value="">Status</option>
																<option value="Active">Active</option>
															    <option value="In-Active">In-Active</option>
																</select>
																</div>
																		<div class="share_add" style="width:auto;">
																		<a href="javascript:void(0)"  style="width: 39px;" class="btn btn-danger remove" title="Add field" ><i class="fa fa-minus"></i></a>
																		</div>
																	</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="col-md-12">
													<div class="share_tabs_main">
														<div class="share_tabs_other">
															<div class="share_tab share_firstn">&nbsp;</div>
															<div class="share_tab share_m">&nbsp;</div>
															<div class="share_tab share_lastn">&nbsp;</div>
															
															<div class="share_tab share_position">
																<label class="share_total">Total :</label>
															</div>
															
															<div class="share_tab share_persentage">
																<input style="text-align:right;margin-left: 2px;    width: 93%;padding: 6px 5px;" name="total" value="<?php echo e(number_format($total,2)); ?>%" type="text" placeholder="" id="total" class="txtOnly form-control total" readonly />
																<p style="display:none;color:red;float: left;" id="t1">This should be not more then 100.00%</p>
															</div>
															
															<div class="share_tab share_remove">
															    <button type="button" id="add_row1" class="btn btn-success addbtn">ADD</button>
															</div>
														</div>
													</div>
												</div>
											</div>

											
											<div class="tab-pane fade" id="tab4primary">
													<div class="col-md-12 col-sm-12 col-xs-12">
												</div>
												<div class="">
													<div class="Branch">
														<h1>For Income-Tax Purpose - Select Type of Entity :</h1>
													</div>
													
													<div class="form-group">
														<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										
    													<!--	<div class="col-lg-5 col-md-4 col-sm-4 col-xs-4 fsc-form-row">
        														<label class="col-md-4 fsc-form-label pr-0" for="" style="padding-top:8px; width:30%;">Type of Corp : </label>
        														<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin pr-0 pl-0">
        													        <input name="typeofcorp"  type="text" placeholder="Type of Corp" id="typeofcorp" value="<?php echo e(Auth::user()->typeofservice); ?>" class="form-control" readonly/>
        														</div>
    														</div>!-->
    														
															<div class="col-lg-5 col-md-5 col-sm-4 col-xs-12 fsc-form-row taxation_1" style="padding-left:0px;">
        														<label class="col-md-4 fsc-form-label pl-0 text-right" for=""  style="padding-top:8px;">Type of Entity :</label>
        														<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin pl-0 pr-0">
        															<select class="form-control fsc-input" name="typeofservice" id="typeofservice" placeholder="Enter Your Company Name">
            														<option value=""> ---Select--- </option>
            													
                                                                        <?php $__currentLoopData = $entity; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $entity1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                        <option value="<?php echo e($entity1->typeentity); ?>" <?php if(Auth::user()->typeofservice==$entity1->typeentity): ?> selected <?php endif; ?>><?php echo e($entity1->typeentity); ?></option>
                                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            													
            														</select>				
            													</div>
    														</div>
														
														
    														<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 fsc-form-row taxation_2" style="padding-left:0px;">
        														<label class="col-md-6 fsc-form-label pl-0 text-right" for=""  style="padding-top:8px;">Type of Form :</label>
        														<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin pl-0 pr-0">
        															<input type="text" name="typeofcorp1" class="form-control" id="typeofcorps" placeholder="Enter Form Name" value="<?php echo e(Auth::user()->typeofcorp1); ?>" readonly>
        														</div>
    														</div>
														
        													<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 fsc-form-row taxation_3" style="padding-left:0px;">
        														<label class="col-md-4 fsc-form-label pl-0 text-right" for="" style="padding-top:8px;">Effective Date : </label>
        														<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin pl-0 pr-0" >
        														    <input name="typeofcorp_effect" value="<?php echo e(Auth::user()->typeofcorp_effect); ?>" maxlength="12" type="text" placeholder="" id="typeofcorp_effect" class="form-control  effective_date1"  />
    													    	</div> 
    														</div>
													
														</div>
													</div>
												</div>
												
												<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
												    <div class="row">
													<div class="Branch">
														<div class="col-md-3" style="text-align:left;">
															<h1>Federal / State</h1>
														</div>
														<div class="col-md-6">
															<h1>Income Tax </h1>
														</div>
													</div>
												</div>
													
												
												<div class="federal_tabs_main">
												<div class="col-md-12 col-sm-12 col-xs-12">
														<div class="form-group <?php echo e($errors->has('federal_id') ? ' has-error' : ''); ?>">
														    <div class="col-lg-4 col-md-5 col-sm-6">
															<label class="control-label">Federal ID :</label>
															<input type="text" class="form-control txtOnly federal_id" id="federal_id" maxlength="9" name="federal_id" value="<?php echo e(Auth::user()->federal_id); ?>">	
															<?php if($errors->has('federal_id')): ?>
															<span class="help-block">
															<strong><?php echo e($errors->first('federal_id')); ?></strong>
															</span>
															<?php endif; ?>
															</div>
															<div class="col-lg-3 col-md-4 col-sm-6">
															<label class="control-label">Form To File (Federal):</label>
															<input type="text" class="form-control" id="type_form_file" name="type_form" value="<?php echo e(Auth::user()->typeofcorp1); ?>" readonly>			
															<?php if($errors->has('type_form')): ?>
															<span class="help-block">
															<strong><?php echo e($errors->first('type_form')); ?></strong>
															</span>
															<?php endif; ?>
															</div>
															<div class="col-lg-2 col-md-3 col-sm-6">
															<label class="control-label">Due Date :</label>
															<input type="text" class="form-control" id="due_date_1" name="due_date" value="<?php echo e(Auth::user()->due_date); ?>" readonly>
															</div>
															<div class="col-lg-3 col-md-4 col-sm-6">
															<label class="control-label">Extension Due Date :</label>
															<input type="text" class="form-control"  id="extension_due_date_1" name="extension_due_date" value="<?php echo e(Auth::user()->extension_due_date); ?>" readonly>
															</div>
														<!--</div>-->
														
														<!--<div class="form-group">-->
														    <div class="col-md-2 col-sm-6">
																<label class="control-label">State :</label>
																<input type="text" readonly class="form-control" class="form-control-insu" id="state_id_2" name="country_id" value="<?php echo e(Auth::user()->state_of_formation); ?>">
															</div>
															
															<div class="col-md-2 col-sm-6">
															<label class="control-label">State ID :</label>
															<!--<input type="text" class="form-control txtOnly" id="state_id" maxlength="9" name="state_id" value="<?php echo e(Auth::user()->state_id); ?>">-->
															<input type="text" class="form-control" class="form-control-insu" id="state_id" name="state_id" value="<?php echo e(Auth::user()->state_id); ?>">
															<?php if($errors->has('state_id')): ?>
															<span class="help-block">
															<strong><?php echo e($errors->first('state_id')); ?></strong>
															</span>
															<?php endif; ?>
															</div>
															
															<div class="col-lg-3 col-md-4 col-sm-6">
															<label class="control-label"> Form To File (State) :</label>
															<input type="text" class="form-control" id="type_form_file2" name="type_form_file2" value="<?php echo e(Auth::user()->type_form_file2); ?>" readonly>
															<?php if($errors->has('type_form')): ?>
															<span class="help-block">
															<strong><?php echo e($errors->first('type_form')); ?></strong>
															</span>
															<?php endif; ?>
															</div>
															
															<div class="col-lg-2 col-md-3 col-sm-6">
															<label class="control-label">Due Date :</label>
															<input type="text" class="form-control" id="due_date_2" name="due_date_2" value="<?php echo e(Auth::user()->due_date_2); ?>" readonly>
															</div>
															
															<div class="col-lg-3 col-md-4 col-sm-6">
															<label class="control-label">Extension Due Date :</label>
															<input type="text" class="form-control"  id="extension_due_date_2" name="extension_due_date2" value="<?php echo e(Auth::user()->extension_due_date2); ?>" readonly>
															</div>
															
														</div>
													</div>
												</div>
												</div>
												
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
													<div class="row">
													    <div class="Branch  " style="background: #b3fff8;display: grid;padding:7px 10px !important;margin-top:px;">
                											<h1 class="pull-left" style="position:absolute;">Federal / State</h1>
                											<h1 class="text-center do_title">Payroll Tax</h1>
                										    <div class="togglebox do_que">
                                                                <label class="control-label" style="padding-top: 2px;">Do You have Payroll ? : </label>
                                                                <select class="form-control" name="payroll_required" id="payroll_que" style="float: right;width: 100px;vertical-align: middle;margin-top: -4px;height: 33px;">
                                                                    <!--<option class="default_val"  selected="">Select</option>-->
                                                                    <option class="bg-green first" value="1" >Yes</option>
                                                                    <option class="bg-red second" value="0">No</option>
                                                                </select>
                                                            </div>
                										</div>
													</div>
													<div class="form-group <?php echo e($errors->has('type_form') ? ' has-error' : ''); ?>">
													
														<div class="form-group <?php echo e($errors->has('fedral_state') ? ' has-error' : ''); ?>">
														
															<div class="">
															
																<div class="fe_state_tab_main">
																	<div class="fe_state_tab">
																		<div class="col-md-12">
																			<h3>Federal Withholding & FICA Tax</h3>
																		</div>
																		
																		<div class="col-md-1 fed_wh1">
																			<label class="control-label-insu"> Name :</label>
																			<input type="text" class="form-control-insu" id="federal_name" name="federal_name" value="IRS" readonly>
																			<input type="hidden" class="form-control-insu" id="federal_frequency_due_year" name="federal_frequency_due_year" value="<?php echo e(Auth::user()->federal_frequency_due_year); ?>" readonly=""> 
																			<input type="hidden" class="form-control-insu" id="federal_frequency_due_monthly" name="federal_frequency_due_monthly" value="<?php echo e(Auth::user()->federal_frequency_due_monthly); ?>" readonly=""> 
																			<input type="hidden" class="form-control-insu" id="federal_frequency_due_quaterly" name="federal_frequency_due_quaterly" value="<?php echo e(Auth::user()->federal_frequency_due_quaterly); ?>" readonly=""> 
																			<input type="hidden" class="form-control-insu" id="federal_payment_frequency_year" name="federal_payment_frequency_year" value="<?php echo e(Auth::user()->federal_payment_frequency_year); ?>" readonly=""> 
																			<input type="hidden" class="form-control-insu" id="federal_payment_frequency_month" name="federal_payment_frequency_month" value="<?php echo e(Auth::user()->federal_payment_frequency_month); ?>" readonly=""> 
																			<input type="hidden" class="form-control-insu" id="federal_payment_frequency_quaterly" name="federal_payment_frequency_quaterly" value="<?php echo e(Auth::user()->federal_payment_frequency_quaterly); ?>" readonly=""> 
																			<input type="hidden" class="form-control-insu" id="payment_frequency_year" name="payment_frequency_year" value="<?php echo e(Auth::user()->payment_frequency_year); ?>" readonly=""> 
																			<input type="hidden" class="form-control-insu" id="payment_frequency_monthly" name="payment_frequency_monthly" value="<?php echo e(Auth::user()->payment_frequency_monthly); ?>" readonly=""> 
																			<input type="hidden" class="form-control-insu" id="payment_frequency_quaterly" name="payment_frequency_quaterly" value="<?php echo e(Auth::user()->payment_frequency_quaterly); ?>" readonly=""> 
																			<input type="hidden" class="form-control-insu" id="frequency_due_date_year" name="frequency_due_date_year" value="<?php echo e(Auth::user()->frequency_due_date_year); ?>" readonly=""> 
																			<input type="hidden" class="form-control-insu" id="frequency_due_date_monthly" name="frequency_due_date_monthly" value="<?php echo e(Auth::user()->frequency_due_date_monthly); ?>" readonly=""> 
																			<input type="hidden" class="form-control-insu" id="frequency_due_date_quaterly" name="frequency_due_date_quaterly" value="<?php echo e(Auth::user()->frequency_due_date_quaterly); ?>" readonly=""> 
																			<input type="hidden" class="form-control-insu" id="frequency_due_date_year_1" name="frequency_due_date_year_1" value="<?php echo e(Auth::user()->frequency_due_date_year_1); ?>" readonly=""> 
																			<input type="hidden" class="form-control-insu" id="frequency_due_date_monthly_1" name="frequency_due_date_monthly_1" value="<?php echo e(Auth::user()->frequency_due_date_monthly_1); ?>" readonly=""> 
																			<input type="hidden" class="form-control-insu" id="frequency_due_date_quaterly_1" name="frequency_due_date_quaterly_1" value="<?php echo e(Auth::user()->frequency_due_date_quaterly_1); ?>" readonly=""> 
																			<input type="hidden" class="form-control-insu" id="frequency_due_date_quaterly_2" name="frequency_due_date_quaterly_2" value="<?php echo e(Auth::user()->frequency_due_date_quaterly_2); ?>" readonly=""> 
																			<input type="hidden" class="form-control-insu" id="frequency_due_date_monthly_2" name="frequency_due_date_monthly_2" value="<?php echo e(Auth::user()->frequency_due_date_monthly_2); ?>" readonly=""> 
																			<input type="hidden" class="form-control-insu" id="frequency_due_date_quaterly_2" name="frequency_due_date_quaterly_2" value="<?php echo e(Auth::user()->frequency_due_date_quaterly_2); ?>" readonly=""> 
																			<input type="hidden" class="form-control-insu" id="federal_frequency_due_year" name="federal_frequency_due_year" value="<?php echo e(Auth::user()->federal_frequency_due_year); ?>" readonly=""> 
																			<input type="hidden" class="form-control-insu" id="federal_frequency_due_monthly" name="federal_frequency_due_monthly" value="<?php echo e(Auth::user()->federal_frequency_due_monthly); ?>" readonly=""> 
																			<input type="hidden" class="form-control-insu" id="quaterly_monthly" name="quaterly_monthly" value="<?php echo e(Auth::user()->quaterly_monthly); ?>" readonly=""> 
																			<input type="hidden" class="form-control-insu" id="quaterly_quaterly" name="quaterly_quaterly" value="<?php echo e(Auth::user()->quaterly_quaterly); ?>" readonly=""> 
																			<input type="hidden" class="form-control-insu" id="payment_quaterly_quaterly" name="payment_quaterly_quaterly" value="<?php echo e(Auth::user()->payment_quaterly_quaterly); ?>" readonly=""> 
																			<input type="hidden" class="form-control-insu" id="payment_quaterly_monthly" name="payment_quaterly_monthly" value="<?php echo e(Auth::user()->payment_quaterly_monthly); ?>" readonly=""> 
																		</div>
																		
																		<div class="col-md-3 fed_wh2">
																			<label class="control-label-insu">Department Authority :</label>
																			<input type="text" class="form-control-insu" id="federal_ga_dept" name="federal_ga_dept" value="Internal Revenue Service" readonly> 
																		</div>
																		
																		<div class="col-md-2 fed_wh3">
																		<label class="control-label-insu">Form No.  :</label>
																		    <select type="text" class="form-control-insu" id="form_number_1" name="form_number_1">
																			    <option value="">Select</option>
    																			<option value="Form-941" <?php if(Auth::user()->federal_frequency=='Quarterly'): ?> selected <?php endif; ?>>Form-941</option>
    																			<option value="Form-944" <?php if(Auth::user()->federal_frequency=='Annually'): ?> selected <?php endif; ?>>Form-944</option>
																			</select>
																		</div>
																		
																		<div class="col-md-2 fed_wh4 business2" <?php if(Auth::user()->federal_frequency =='Annually'): ?> style="display:none;" <?php endif; ?>>
																			<label class="control-label-insu">Quarter Period:</label>
																			<select type="text" class="form-control-insu" id="quarter_type" name="quarter_type">
																			    <option value="">Select</option>
																			    <option value="1st Qtr" <?php if(Auth::user()->quarter_type=='1st Qtr'): ?> selected <?php endif; ?>>1st Qtr.</option>
    																			<option value="2nd Qtr" <?php if(Auth::user()->quarter_type=='2nd Qtr'): ?> selected <?php endif; ?>>2nd Qtr.</option>
    																			<option value="3rd Qtr" <?php if(Auth::user()->quarter_type=='3rd Qtr'): ?> selected <?php endif; ?>>3rd Qtr.</option>
    																			<option value="4th Qtr" <?php if(Auth::user()->quarter_type=='4th Qtr'): ?> selected <?php endif; ?>>4th Qtr.</option>
																			</select>
																		</div>
																		
																		<div class="col-md-2 fed_wh5">
																			<label class="control-label-insu">Period End Date</label>
																			<input type="text" class="form-control-insu" id="quarter_date" name="quarter_date" value="<?php echo e(Auth::user()->quarter_date); ?>" readonly> 
																		</div>
																		
																		<div class="col-md-2 fed_wh6">
																			<label class="control-label-insu">Filing Frequency</label>
																			<input type="text" class="form-control-insu" id="federal_frequency11" name="federal_frequency"  value="<?php echo e(Auth::user()->federal_frequency); ?>" readonly>
    																			<?php if($errors->has('federal_frequency')): ?>
    																			<span class="help-block">
    																			<strong><?php echo e($errors->first('federal_frequency')); ?></strong>
    																			</span>
    																			<?php endif; ?>
																		</div>
																		
																		<div class="col-md-2 fed_wh7">
																			<label class="control-label-insu">Due Date :</label>
																			<input type="text" class="form-control-insu form-control" id="federal_frequency_due_date" name="federal_frequency_due_date" value="<?php echo e(Auth::user()->federal_frequency_due_date); ?>" readonly>
																		</div>
																	<!--</div>-->
																	
																	<!--<div class="fe_state_tab">-->
																		<div class="col-md-4 fed_wh8">
																			<label class="control-label-insu">Federal Note  :</label>
																			<input type="text" class="form-control-insu" name="number_1" value="<?php echo e(Auth::user()->number_1); ?>">
																		</div>
																		
																		<div class="col-md-2 fed_wh9">
																		    <label class="control-label-insu">EFTPS PIN  :</label>
																			<input type="text" class="form-control-insu" id="eptpspin" name="eptpspin" value="<?php echo e(Auth::user()->eptpspin); ?>">
																		</div>
																		
																		<div class="col-md-2 fed_wh10">
																		    <label class="control-label-insu">PW </label>
																			<input type="text" class="form-control-insu" id="pay_pw" name="pay_pw" value="<?php echo e(Auth::user()->pay_pw); ?>">
																		</div>
																		
																		<div class="col-md-2 fed_wh11 federal_payment_frequency_941" <?php if(Auth::user()->federal_payment_frequency=='Annually'): ?> style="display:none;" <?php endif; ?>>
																			<label class="control-label-insu">Payment Frequency </label>
																			<select type="text" class="form-control-insu" id="federal_payment_frequency" name="federal_payment_frequency">
																			    <option value="">-Select-</option>
																			    <option value="Monthly"   <?php if(Auth::user()->federal_payment_frequency =='Monthly'): ?> selected <?php endif; ?>>Monthly</option>
        																	    <option value="Quarterly" <?php if(Auth::user()->federal_payment_frequency =='Quarterly'): ?> selected <?php endif; ?>>Quarterly</option>
    																	    </select>
																		</div>
																		
																		<div class="col-md-2 fed_wh12 federal_payment_frequency_944"  <?php if(Auth::user()->federal_payment_frequency=='Annually'): ?> style="display:block;"  <?php else: ?>   style="display:none;" <?php endif; ?>>
																			<label class="control-label-insu">Payment Frequency </label>
																			<select type="text" class="form-control-insu " id="federal_payment_frequency_annually" name="federal_payment_frequency">
																			    <option value="">-Select-</option>
																			    <option value="Quarterly" <?php if(Auth::user()->federal_payment_frequency =='Quarterly'): ?> selected <?php endif; ?>>Quarterly</option>
        																	    <option value="Annually" <?php if(Auth::user()->federal_payment_frequency =='Annually'): ?> selected <?php endif; ?>>Annually</option>
    																	    </select>
																		</div>
																		
																		<div class="col-md-2 fed_wh13">
																			<label class="control-label-insu">Payment Due Date </label>
																			<input type="text" class="form-control-insu form-control" name="federal_payment_frequency_date" id="federal_payment_frequency_date2" value="<?php echo e(Auth::user()->federal_payment_frequency_date); ?>" readonly>
																		</div>
																		
																	</div>
																</div>
																
																<div class="fe_state_tab_main">
																	<div class="fe_state_tab">
																		<div class="col-md-12">
																			<h3>Federal Unemployment Tax (FUTA)</h3>
																		</div>
																		<div class="col-md-1 fed_un1">
																			<label class="control-label-insu">Name :</label>
																			<input type="text" class="form-control-insu" name="payroll_name" value="IRS" readonly> 
																		</div>
                                                                        
																		<div class="col-md-4 fed_un2">
																			<label class="control-label-insu">Department Authority  :</label>
																			<input type="text" class="form-control-insu" id="ga_dept" name="ga_dept" value="Internal Revenue Service" readonly> 
																		</div>
																	
																		<div class="col-md-3 fed_un3">
																			<label class="control-label-insu">Form No.  :</label>
																			<input type="text" class="form-control-insu" id="form_number_2" readonly name="form_number_2" value="Form-940">
																		</div>
																		
																		<div class="col-md-2 fed_un4">
																			<label class="control-label-insu">Filing Frequency</label>
																			<input type="text" class="form-control-insu" name="frequency" id="frequency" value="Annually" readonly>
																		</div>
																	
																		<div class="col-md-2 fed_un5">
																			<label class="control-label-insu">Due Date :</label>
																			<input type="text" class="form-control-insu form-control" name="frequency_due_date" 
																			value="<?php echo "Jan".'-31-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>" readonly>
    																			<?php if($errors->has('frequency_due_date')): ?>
    																			<span class="help-block">
    																			<strong><?php echo e($errors->first('frequency_due_date')); ?></strong>
    																			</span>
    																			<?php endif; ?>
																		</div>
																	<!--</div>-->
																	
																	<!--<div class="fe_state_tab">-->
																	
																		<div class="col-md-8 fed_un6">
																			<label class="control-label-insu">Note :</label>
																			<input type="text" class="form-control-insu" id="number_2" name="number_2" value="<?php echo e(Auth::user()->number_2); ?>">
																		</div>
																	
																	
																		<div class="col-md-2 fed_un7">
																			<label class="control-label-insu">Payment  Frequency </label>
																			<select type="text" class="form-control-insu"  name="payment_frequency" id="payment_frequency">
    																		    <option value="Quarterly" <?php if(Auth::user()->payment_frequency =='Quarterly'): ?> selected <?php endif; ?>>Quarterly</option>
            																	<option value="Annually" <?php if(Auth::user()->payment_frequency =='Annually'): ?> selected <?php endif; ?>>Annually</option>
																			</select>
																		</div>
																		
																		<div class="col-md-2 fed_un8">
																			<label class="control-label-insu">Payment Due Date </label>
																			<input type="text" class="form-control-insu form-control" name="payment_frequency_date" id="payment_frequency_date" value="<?php echo e(Auth::user()->payment_frequency_date); ?>" readonly>
																		</div>
																	</div>
																</div>
															
																<div class="fe_state_tab_main">
																	<div class="fe_state_tab">
																	    <div class="col-md-12">
																	        <h3>State Withholding</h3>
																	    </div>
																	<div class="col-md-1 fed_wh1">
																	    <label class="control-label-insu">Name :</label>
																	    <input type="text" class="form-control-insu" name="fedral_state_1" value="GA" readonly> 
																	</div>
																	<div class="col-md-3 fed_wh2">
																	    <label class="control-label-insu">Department Authority  :</label>
																	    <input type="text" class="form-control-insu" id="ga_dept_1" name="ga_dept_1" value="GA Dept of Revenue" readonly>
																	</div>
																	<div class="col-md-2 fed_wh3">
    																	<label class="control-label-insu">Form No.  :</label>
    																	<input type="text" class="form-control-insu" id="form_number_3" readonly name="form_number_3" value="Form-G-7">
																	</div>
																	<div class="col-md-2 business2 fed_wh4">
																		<label class="control-label-insu">Quarter Period:</label>
																		<select type="text" class="form-control-insu" id="quarter_type_holding1" name="quarter_type_holding1">
																		    <option value="">Select</option>
																		    <option value="1st Qtr" <?php if(Auth::user()->quarter_type_holding1=='1st Qtr'): ?> selected <?php endif; ?>>1st Qtr.</option>
																			<option value="2nd Qtr" <?php if(Auth::user()->quarter_type_holding1=='2nd Qtr'): ?> selected <?php endif; ?>>2nd Qtr.</option>
																			<option value="3rd Qtr" <?php if(Auth::user()->quarter_type_holding1=='3rd Qtr'): ?> selected <?php endif; ?>>3rd Qtr.</option>
																			<option value="4th Qtr" <?php if(Auth::user()->quarter_type_holding1=='4th Qtr'): ?> selected <?php endif; ?>>4th Qtr.</option>
																		</select>
																	</div>
																	<div class="col-md-2 fed_wh5">
																		<label class="control-label-insu">Period End Date</label>
																		<input type="text" class="form-control-insu" id="quarter_date_holding" name="quarter_date_holding" value="<?php echo e(Auth::user()->quarter_date_holding); ?>" readonly> 
																	</div>
																	<div class="col-md-2 fed_wh6">
																	    <label class="control-label-insu">Filing Frequency</label>
																	    <input type="text" class="form-control-insu" id="frequency_type_holding1" name="frequency_type_holding1" value="Quarterly" readonly>
																	</div>
																	<div class="col-md-2 fed_wh7">
																	    <label class="control-label-insu">Due Date:</label>
																	    <input type="text" class="form-control-insu form-control" id="federal_frequency_due_date_holding" name="federal_frequency_due_date_holding" value="<?php echo e(Auth::user()->federal_frequency_due_date_holding); ?>" readonly>
																	</div>
																<!--</div>-->
																
																<!--<div class="fe_state_tab">-->
																	    <div class="col-md-2 st_wh1">
																	        <label class="control-label-insu">State Withholding No:</label>
																	        <input type="text" class="form-control-insu" id="number_3" name="number_3" value="<?php echo e(Auth::user()->number_3); ?>">
																	    </div>
    																	<div class="col-md-2 st_wh2">
																	        <label class="control-label-insu">PIN :</label>
																	        <input type="text" class="form-control-insu" id="pin" name="pin" value="<?php echo e(Auth::user()->pin); ?>">
																	    </div>
																	    <div class="col-md-2 st_wh3">
																	        <label class="control-label-insu">Username :</label>
																	        <input type="text" class="form-control-insu" id="holding_uname" name="holding_uname" value="<?php echo e(Auth::user()->holding_uname); ?>">
																	    </div>
																	    <div class="col-md-2 st_wh4">
																	        <label class="control-label-insu">Password :</label>
																	        <input type="password" class="form-control-insu" id="holding_password" name="holding_password" value="<?php echo e(Auth::user()->holding_password); ?>">
																	    </div>
    																	<div class="col-md-2 st_wh5">
        																	<label class="control-label-insu">Payment Frequency </label>
        																	<select type="text" class="form-control-insu" id="payment_frequency_type_holding2" name="payment_frequency_type_holding2">
        																	    <option value="">-Select-</option>
            																	<option value="Monthly" <?php if(Auth::user()->payment_frequency_type_holding2 =='Monthly'): ?> selected <?php endif; ?>>Monthly</option>
            																	<option value="Quarterly" <?php if(Auth::user()->payment_frequency_type_holding2 =='Quarterly'): ?> selected <?php endif; ?>>Quarterly</option>
        																	</select>
    																	</div>
    																	<div class="col-md-2 st_wh6">
        																	<label class="control-label-insu">Payment Due Date </label>
            																	<input type="text" class="form-control-insu form-control" id="payment_frequency_due_date_holding" name="payment_frequency_due_date_holding" value="<?php echo e(Auth::user()->payment_frequency_due_date_holding); ?>" readonly>
    																	</div>
																</div>
															</div>
																
															<div class="fe_state_tab_main">
																<div class="fe_state_tab">
																	<div class="col-md-12">
																	    <h3>State Unemployment (SUTA)</h3>
																	</div>
																	
    																<div class="col-md-1 fed_wh1">
    																	<label class="control-label-insu">Name :</label>
    																	<input type="text" class="form-control-insu" name="fedral_state_2" value="GA" readonly> 
    																</div>
    																<div class="col-md-3 fed_wh2">
    																	<label class="control-label-insu">Department Authority  :</label>
    																	<input type="text" class="form-control-insu" id="ga_dept_labour" name="ga_dept_labour" value="GA Dept of Labor" readonly>
    																</div>
    																<div class="col-md-2 fed_wh3">
																	    <label class="control-label-insu">Form No.  :</label>
																	    <input type="text" class="form-control-insu" id="form_number_4" readonly name="form_number_4" value="Form-DOL-4N">
    																</div>
																	<div class="col-md-2 business2 fed_wh4">
																		<label class="control-label-insu">Quarter Period:</label>
																		<select type="text" class="form-control-insu" id="quarter_type_unemploy1" name="quarter_type_unemploy1">
																		    <option value="">Select</option>
																		    <option value="1st Qtr" <?php if(Auth::user()->quarter_type_unemploy1=='1st Qtr'): ?> selected <?php endif; ?>>1st Qtr.</option>
																			<option value="2nd Qtr" <?php if(Auth::user()->quarter_type_unemploy1=='2nd Qtr'): ?> selected <?php endif; ?>>2nd Qtr.</option>
																			<option value="3rd Qtr" <?php if(Auth::user()->quarter_type_unemploy1=='3rd Qtr'): ?> selected <?php endif; ?>>3rd Qtr.</option>
																			<option value="4th Qtr" <?php if(Auth::user()->quarter_type_unemploy1=='4th Qtr'): ?> selected <?php endif; ?>>4th Qtr.</option>
																		</select>
																	</div>
																	<div class="col-md-2 fed_wh5">
																		<label class="control-label-insu">Period End Date</label>
																		<input type="text" class="form-control-insu" id="quarter_date_unemploy" name="quarter_date_unemploy" value="<?php echo e(Auth::user()->quarter_date_unemploy); ?>" readonly> 
																	</div>
    																<div class="col-md-2 fed_wh6">
    																	<label class="control-label-insu">Filing Frequency</label>
    																	<input type="text" class="form-control-insu" id="frequency_type_unemploy1" name="frequency_type_unemploy1" value="Quarterly" readonly>
    																</div>
    																<div class="col-md-2 fed_wh7">
    																	<label class="control-label-insu">Due Date :</label>
    																	<input type="text" class="form-control-insu form-control" id="federal_frequency_due_date_unemploy" name="federal_frequency_due_date_unemploy" value="<?php echo e(Auth::user()->federal_frequency_due_date_unemploy); ?>" readonly>
    																</div>
															    <!--</div>-->
																	
																<!--<div class="fe_state_tab">-->
																    
																	    <div class="col-md-4 fed_wh8">
																            <label class="control-label-insu">State Unemployment No :</label>
																	        <input type="text" class="form-control-insu" id="number_4" name="number_4" value="<?php echo e(Auth::user()->number_4); ?>">
																	    </div>
    																	<div class="col-md-2 fed_wh9">
    																	    <label class="control-label-insu">Username :</label>
    																	    <input type="text" class="form-control-insu" id="unemploy_uname" name="unemploy_uname" value="<?php echo e(Auth::user()->unemploy_uname); ?>">
    																	</div>
    																	<div class="col-md-2 fed_wh10">
    																	    <label class="control-label-insu">Password :</label>
    																	    <input type="password" class="form-control-insu" id="unemploy_password" name="unemploy_password" value="<?php echo e(Auth::user()->unemploy_password); ?>">
    																	</div>
    																	<div class="col-md-2 fed_wh11">
    																	    <label class="control-label-insu">Payment Frequency </label>
															    	        <input type="text" class="form-control-insu" id="payment_frequency_type_unemploy2" name="payment_frequency_type_unemploy2" value="Quarterly" readonly>
    																	</div>
    																	<div class="col-md-2 fed_wh13">
    																	    <label class="control-label-insu">Payment Due Date </label>
    																	    <input type="text" class="form-control-insu form-control" id="payment_frequency_due_date_unemploy" name="payment_frequency_due_date_unemploy" value="<?php echo e(Auth::user()->payment_frequency_due_date_unemploy); ?>" readonly>
    																	</div>
																	
																</div>
															</div>
															
															<div class="showno" id="no_payroll"><h3 style="text-align:center;">No Payroll Require</h3></div>
																
														</div>
													</div>
												</div>
											</div>
											
										</div>
										
											<!--<div class="tab-pane fade" id="tab5primary">-->
											<div class="tab-pane fade <?php if(isset($_REQUEST['id']) && $_REQUEST['id'] == 'tab5primary'){?>in active <?php }?>" id="tab5primary">
												<div class="">
													<div class="Branch">
                                                <h1 class="titleleft">Business License</h1>
                                                <div class="btn-group btn-toggle " style="display: flex;"> 
                                                    <!--<label class="control-label">Do You Require License ? : </label>-->
                                                       <?php  $countnbus= count($buslicense1);?>
                                                        <?php if($countnbus >0){?>
                                                    <!--<input type="text"  class="form-control"  style="width:55px; height:33px;" readonly value="Yes">-->
                                                    <!--<input type="hidden" name="lic_required" readonly value="1">-->
                                                        <?php } else{?>
                                                    <!--<input type="text"  class="form-control"  style="width:55px; height:33px;" readonly value="No">-->
                                                    <!--<input type="hidden" name="lic_required" readonly value="0">-->
                                                         <?php } ?>
                                                  
                                                    <div class="togglebox">
                                                        <label class="control-label">Do You Require License ? :</label>
                                                        <div class="btn-group btn-toggle">  
                                                            <?php if($countnbus == 0): ?>
                                                                <select class="form-control lic_que" name="license_required" id="license_required" style="float: right; width: 100px; vertical-align: middle; margin: 1px 0px 2px 0px; height: 33px;">
                                                                    <!--<option class="default_val">Select</option>-->
                                                                    <option class="bg-green first" value="1">Yes</option>
                                                                    <option class="bg-red second" value="0">No</option>
                                                                </select>
                                                            <?php else: ?> 
                                                                <label class="control-label" disable>Yes</label>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                           </div>
												
												<br/>
												<?php
												$countbusiness=count($buslicense1);
												if($countbusiness > 0) {?>
												<div class="col-md-12 col-sm-12 col-xs-12 show_business_lic federal_tabs_main">
													<div class="form-group <?php echo e($errors->has('business_license_jurisdiction') ? ' has-error' : ''); ?>" >
														<div class="">
															<div class="col-md-3">
																<label class="control-label" style="font-size:15px;"> Jurisdiction :</label>
																<select type="text" class="form-control" id="type_form3" name="business_license_jurisdiction">
																	<option value="">Select</option>
																	<option value="City" <?php if(Auth::user()->business_license_jurisdiction=='City'): ?> selected <?php endif; ?>>City</option>
																	<option value="County" <?php if(Auth::user()->business_license_jurisdiction=='County'): ?> selected <?php endif; ?>>County</option>
																</select>
																<?php if($errors->has('business_license_jurisdiction')): ?>
																<span class="help-block">
																<strong><?php echo e($errors->first('business_license_jurisdiction')); ?></strong>
																</span>
																<?php endif; ?>
															</div>
															<div class="col-md-3">
																<label id="city-change" class="control-label" style="display:none;">City :</label> 
																<label id="county-change" class="control-label">County :</label>
																<?php if(Auth::user()->business_license_jurisdiction=='County'): ?>
																<select type="text" class="form-control" id="business_license2" name="business_license2">
																	<option value="">Select</option>
																	<option value="<?php echo e(Auth::user()->business_license2); ?>" <?php if(Auth::user()->business_license2): ?> selected <?php endif; ?>><?php echo e(Auth::user()->business_license2); ?></option>
																	<?php $__currentLoopData = $taxstate; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																	<option value="<?php echo e($v->county); ?>"  <?php if($v->county==Auth::user()->business_license2): ?> selected <?php endif; ?>><?php echo e($v->county); ?></option>
																	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
																</select>
																<?php elseif(Auth::user()->business_license_jurisdiction=='City'): ?>
																<input type="text" class="form-control" id="business_license3"  name="business_license2" value="<?php echo e(Auth::user()->business_license2); ?>">
																<?php else: ?>
																	<select type="text" class="form-control" id="business_license2" name="business_license2" <?php if(Auth::user()->business_license_jurisdiction=='City'): ?> style="display:none;" <?php endif; ?>>
																	<option value="">Select</option>
																	<?php $__currentLoopData = $taxstate; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																	<option value="<?php echo e($v->county); ?>" <?php if($v->county==Auth::user()->business_license2): ?> selected <?php endif; ?>><?php echo e($v->county); ?></option>
																	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
																</select>
																<?php endif; ?>
																	<div id="business_license4"></div>
																	<!--<div id="business_license5"></div>!-->
															</div>
															<div class="col-md-2">
																<label id="city-change1" class="control-label" style="display:none;">City # :</label> 
																<label id="county-change1" class="control-label">County # :</label>
																<input name="business_license1" placeholder="" value="<?php echo e(Auth::user()->business_license1); ?>" type="text" id="business_license1" class="form-control" />
																<?php if($errors->has('business_license1')): ?>
																<span class="help-block">
																	<strong><?php echo e($errors->first('business_license1')); ?></strong>
																</span>
																<?php endif; ?>
															</div>
															<div class="col-md-2">
																<label id="county-change1" class="control-label">License # :<?php //echo count($buslicense1);?></label>
																
																<?php
																    if(isset($buslicense)!='')
																    {
																        
																        ?>
																        	<input name="business_license3" readonly value="<?php echo e($buslicense->license_no); ?>" type="text" id="" class="form-control" />
																        <?php
																    }
																    else
																    {
																        ?>
																        	<input name="" readonly value="" type="text" class="form-control" />
																        
																        <?php
																    }
																   
																?>
																
															
																<?php if($errors->has('business_license3')): ?>
																<span class="help-block">
																	<strong><?php echo e($errors->first('business_license3')); ?></strong>
																</span>
																<?php endif; ?>
															</div>
															<div class="col-md-2">
																<label class="control-label">Expire Date :</label>
																<?php
																    if(isset($buslicense->id)!='')
																    {
																?>
																<input type="hidden" name="business_id" value="<?php echo e($buslicense->id); ?>">
																<?php
																    }
																?>
																<?php
																  
																  if(isset($buslicense->license_year)!='' && isset($buslicense->license_year)=='2020')
																    {
																        ?>
																         <input type="text" class="form-control" name="bus_license_exp_date" value="Mar-31-2021" readonly>
																        <?php
																    }
																    else  if(isset($buslicense->license_year)!='' && isset($buslicense->license_year)=='2021')
																    {
																        ?>
																            
																         <input type="text" class="form-control" name="bus_license_exp_date" value="Mar-31-2022" readonly>
																        <?php
																    }
																     else  if(isset($buslicense->license_year)!='' && isset($buslicense->license_year)=='2022')
																    {
																        ?>
																         <!--<input type="text" class="form-control" name="due_date2" value="Mar-31-2023" readonly>-->
																         <input type="text" class="form-control" name="bus_license_exp_date" value="Mar-31-2023" readonly>
																        <?php
																    }
																    else
																    {
																        ?>
																            <input type="text" class="form-control effective_date1" id="due_date2" name="bus_license_exp_date" value="<?php echo e(Auth::user()->due_date2); ?>" readonly>        
																        <?php
																    }
																?>
																
															</div>
														</div>
													</div>
													<div class="form-group">
														<div class="">
															<div class="col-md-6">
																<label class="control-label" style="font-size:15px;padding:0;">Note :</label>
																<input name="notes" placeholder="Note" value="<?php echo e(Auth::user()->notes); ?>" type="text" id="" class="form-control">
															</div>	
															
															<?php $id1 = Auth::user()->business_license_jurisdiction;?>
															<!--<div class="col-md-2">-->
															<!--	<label></label>-->
															<!--	<a class="btn_new btn-view-license">License History</a>-->
															<!--</div>-->
															<div class="col-md-2">
																<label></label>
																<?php
																if(isset($buslicense)!='')
																{
																    ?>
																    	<a style="display:block;" data-toggle="modal" class="btn_new openBtn007 btn-view-license">View License</a>
																    
																    <?php
																}
																else
																{
																    ?>
															            		    
																    <?php
																}
																?>
															
															</div>
															<div class="col-md-2">
																<style>.nn{ display:none !important}</style>
																<label></label>
																
																<?php
																if(isset($buslicense) !='')
																{
																    
																    ?>
																    <a style="display:block;" href="https://ecorp.sos.ga.gov/Account" target="_blank" class="btn_new btn-renew">Renew Here</a>
																    
																    <?php
																}
																else
																{
																    ?>
																    <?php
																}
																?>
																
															    
																<label></label>
															    <a href="https://ecorp.sos.ga.gov/Account" class="btn_new btn-renew nn">Renew Here</a>
															</div>
															<div class="col-md-2">
															    <label></label>
															    <a href="#" class="btn_new btn-renew">Renew Record</a>
														    </div>
														</div>
													</div>
												</div>
												<?php 
    										}
    								 		else
    								 		{
    										    ?>
    										    <div class="col-md-12 col-sm-12 col-xs-12 show_business_lic1 federal_tabs_main"><h3 style="text-align:center;">No License Require</h3></div>
    										    <?php
    								 		}
    										
    												$pro = count($admin_professional);
												?>
												<div class="">
													<div class="Branch" style="text-align:left; padding-left:15px;">
														<h1>Professional License Total: <?php echo $pro; ?></h1>
													</div>
												</div>
												<br/>
									
									            
									
												<?php 
    												$i = 1; 
    												$pro = count($admin_professional);
												?>
												<?php if($pro!=NULL): ?>
													<?php $__currentLoopData = $admin_professional; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ak1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
													<?php $i; ?>
									
												<?php if($ak1->profession==NULL): ?>
												<?php else: ?>
												<div class="">
													<div class="field_wrapper">
														<div id="field0">
															<div class="professional_tabs_main">
														       	<div class="professional_tabs">
																	<div class="professional_tab professional_profession">
																		    <label>Profession :</label>
																		    <select type="text" class="form-control-insu profession<?php echo e($ak1->id); ?>" name="profession[]">
    																			<option value="">Select</option>
                																<option value="ERO" <?php if($ak1->profession=='ERO'): ?> selected <?php endif; ?>>ERO</option>
                																<option value="CPA" <?php if($ak1->profession=='CPA'): ?> selected <?php endif; ?>>CPA</option>
                																<option value="Mortgage Broker" <?php if($ak1->profession=='Mortgage Broker'): ?> selected <?php endif; ?>>Mortgage Broker</option>
                																<option value="PTIN" <?php if($ak1->profession=='PTIN'): ?> selected <?php endif; ?>>PTIN</option>
                																<option value="Insurance Agent" <?php if($ak1->profession=='Insurance Agent'): ?> selected <?php endif; ?>>Insurance Agent</option>
                																<option value="Insurance Broker" <?php if($ak1->profession=='Insurance Broker'): ?> selected <?php endif; ?>>Insurance Broker</option>
                																<option value="MLO" <?php if($ak1->profession=='MLO'): ?> selected <?php endif; ?>>MLO</option>
    																		</select>
																	</div>
																	<div class="professional_tab professional_state">
																		<label>State :</label>
																		<select name="profession_state[]" id="profession_state" class="form-control-insu profession_state<?php echo e($ak1->id); ?>">
																		    <?php if(empty($ak1->profession_state)): ?> <?php else: ?> <option value="<?php echo e($ak1->profession_state); ?>"><?php echo e($ak1->profession_state); ?></option> <?php endif; ?>
																		<option value="AK">AK</option>
																		<option value="AS">AS</option>
																		<option value="AZ">AZ</option>
																		<option value="AR">AR</option>
																		<option value="CA">CA</option>
																		<option value="CO">CO</option>
																		<option value="CT">CT</option>
																		<option value="DE">DE</option>
																		<option value="DC">DC</option>
																		<option value="FM">FM</option>
																		<option value="FL">FL</option>
																		<option value="GA">GA</option>
																		<option value="GU">GU</option>
																		<option value="HI">HI</option>
																		<option value="ID">ID</option>
																		<option value="IL">IL</option>
																		<option value="IN">IN</option>
																		<option value="IA">IA</option>
																		<option value="KS">KS</option>
																		<option value="KY">KY</option>
																		<option value="LA">LA</option>
																		<option value="ME">ME</option>
																		<option value="MH">MH</option>
																		<option value="MD">MD</option>
																		<option value="MA">MA</option>
																		<option value="MI">MI</option>
																		<option value="MN">MN</option>
																		<option value="MS">MS</option>
																		<option value="MO">MO</option>
																		<option value="MT">MT</option>
																		<option value="NE">NE</option>
																		<option value="NV">NV</option>
																		<option value="NH">NH</option>
																		<option value="NJ">NJ</option>
																		<option value="NM">NM</option>
																		<option value="NY">NY</option>
																		<option value="NC">NC</option>
																		<option value="ND">ND</option>
																		<option value="MP">MP</option>
																		<option value="OH">OH</option>
																		<option value="OK">OK</option>
																		<option value="OR">OR</option>
																		<option value="PW">PW</option>
																		<option value="PA">PA</option>
																		<option value="PR">PR</option>
																		<option value="RI">RI</option>
																		<option value="SC">SC</option>
																		<option value="SD">SD</option>
																		<option value="TN">TN</option>
																		<option value="TX">TX</option>
																		<option value="UT">UT</option>
																		<option value="VT">VT</option>
																		<option value="VI">VI</option>
																		<option value="VA">VA</option>
																		<option value="WA">WA</option>
																		<option value="WV">WV</option>
																		<option value="WI">WI</option>
																		<option value="WY">WY</option>
																		</select>
																	</div>
																	
																	<div class="professional_tab professional_profession">
																	    <label>Type :</label>
																		<select type="text" class="form-control-insu professiontypefunction<?php echo e($ak1->id); ?> professiontype<?php echo e($ak1->id); ?>" id="professiontype<?php echo e($ak1->id); ?>" name="professiontype[]">
																			<option value="">Select</option>
            																<option value="Firm" <?php if($ak1->professiontype=='Firm'): ?> selected <?php endif; ?>>Firm</option>
            																<option value="Individual" <?php if($ak1->professiontype=='Individual'): ?> selected <?php endif; ?>>Individual</option>
																		</select>
																	</div>
																
																	
																	<div class="professional_tab profession_priority">
																		<label>License Period:</label>
																			
																		<input type="text" class="form-control-insu profession_priority1<?php echo e($ak1->id); ?>" id="profession_priority<?php echo e($ak1->id); ?>" name="profession_priority1[]" value="1" readonly>
                														 
                							    						<input type="text" class="form-control-insu profession_priority2<?php echo e($ak1->id); ?>" id="profession_priority<?php echo e($ak1->id); ?>" name="profession_priority2[]" value="2" readonly>
                														 
                												        <select type="text" class="form-control-insu profession_priority3<?php echo e($ak1->id); ?>" id="profession_priority<?php echo e($ak1->id); ?>" name="profession_priority3[]">
																			<option value="">Select</option>
            																<option value="1" <?php if($ak1->profession_priority=='1'): ?> selected <?php endif; ?>>1</option>
            																<option value="2" <?php if($ak1->profession_priority=='2'): ?> selected <?php endif; ?>>2</option>
            																<option value="3" <?php if($ak1->profession_priority=='3'): ?> selected <?php endif; ?>>3</option>
            																<option value="4" <?php if($ak1->profession_priority=='4'): ?> selected <?php endif; ?>>4</option>
																		</select>
    														
																	</div>
																	
																	<div class="professional_tab professional_license">
																		<label>License # :</label>
																		<input name="profession_license[]" placeholder="License"  value="<?php echo e($ak1->profession_license); ?>" type="text" id="profession_license" class="form-control-insu profession_license<?php echo e($ak1->id); ?>">
																	</div>
																	
																	<div class="professional_tab professional_expire">
																		<label>Expire Date :</label>
                														 <!--<input name="profession_exp_date[]"  class="form-control-insu profession_exp_date<?php echo e($ak1->id); ?>" placeholder="Expire Date" value="<?php //if($ak1->profession_exp_date !='0000-00-00') { echo date('m/d/Y',strtotime($ak1->profession_exp_date));}?>" type="text" id="profession_epr_date" readonly>-->
                														 <input name="profession_exp_date[]"  class="form-control-insu profession_exp_date<?php echo e($ak1->id); ?>" placeholder="Expire Date" value="<?php if($ak1->profession_exp_date !='0000-00-00') { echo date('m/d/Y',strtotime($ak1->profession_exp_date));}?>" type="text" id="profession_epr_date" readonly>
																	</div>
																	
																	<div class="professional_tab professional_effective" style="width:10% !Important">
																		<label>Effective Date :</label>
																		<input name="profession_effective_date[]" placeholder="Effective Date" value="<?php echo e($ak1->profession_epr_date1); ?>" type="text" id="profession_effective_date" class="form-control-insu effective_date1">
																	</div>
																	
																	
																	<div class="professional_add">
																	   <?php if($ak1->id!=''): ?>
																	   <a href="#myModalm99_<?php echo e($ak1->id); ?>" id="remove_button_pro" role="button" class="btn_new btn-remove"  data-toggle="modal" ><i class="fa fa-minus" aria-hidden="true"></i></a>
																	   <?php else: ?>
																	    
																	   <?php endif; ?>
																	</div>
																	
																	<div class="professional_tab professional_profession professional_note">
																		<label>Note :</label>
																		<input name="profession_note[]" placeholder="Note"  value="<?php echo e($ak1->profession_note); ?>" type="text" id="profession_note" class="form-control-insu">
																		<input type="hidden" name="profession_id[]" placeholder="Expire Date" value="<?php echo e($ak1->id); ?>" class="form-control-insu">
																		<input type="hidden" name="check_flag[]" value="<?php echo e($ak1->check_flag); ?>" class="form-control-insu">
																	</div>
	                                                                 
											
																	</script>
																	<!--<div class="professional_tab  professional_effective_1">-->
																	<!--	<label></label>-->
																	<!--    <a data-toggle="modal-history" num="<?php echo e($ak1->profession.'-'.$ak1->profession_state.'-'.$ak1->profession_license); ?>" class="btn_new btn-view-license openBtn1 Pro-btn">License History</a>-->
																	<!--</div>-->
																	
																	<div class="professional_tab professional_license_1">
            									                        <?php if($ak1->profession_license_copy!=''): ?>
            									                        
            									                        <input type="hidden" name="profession_license_copy[]" value="<?php echo e($ak1->profession_license_copy); ?>">
            									                       
            									                        <label></label>
            									                         <a data-toggle="modal" data-id="<?php echo e($ak1->check_type); ?>"  class="btn_new btn-view-license openBtn Pro-btn openBtncertificate_lic">View License</a>
            									                        <!--<a data-toggle="modal" data-id="<?php echo e($ak1->ids); ?>"  class="btn_new btn-view-license openBtn Pro-btn openBtncertificate_lic">View License</a>-->
            									                        <!--<a data-toggle="modal" data-id="<?php echo e($ak1->id); ?>"  class="btn_new btn-view-license openBtn Pro-btn openBtncertificate_lic">View License</a>-->
            														    <?php else: ?>
            														    <label></label>
            														     <!--<a class="btn_new btn-view-license Pro-btn">File Not Upload</a>-->
            														    <?php endif; ?>
            														</div>
            														
            														<div class="professional_tab  professional_expire_1">
                													
                														<label></label>
                														<?php if($ak1->profession_license_website!=NULL): ?>
                														 <input type="hidden" name="profession_license_website[]" value="<?php echo e($ak1->profession_license_website); ?>">
                														<a href="<?php echo e($ak1->profession_license_website); ?>" class="btn_new btn-renew" target='_blank'>Renew Here</a>
                                                                        <?php else: ?>
                                                                        <!--<a href="" class="btn_new btn-renew">Renew Here</a>-->
                                                                        <?php endif; ?>
																	</div>
																	
																	<div class="professional_tab professional_state formss<?php echo e($ak1->id); ?> individual<?php echo e($ak1->id); ?>" id="individual<?php echo e($ak1->id); ?>" style="display:none;">
																		<!--<label style="position:absolute;margin-top:27px;">CE Check:</label>-->
																		<label style="margin-top:28px;">CE Check:</label>
																		
																	   
																	            <input name="ce[]"  type="checkbox" id="ce_<?php echo e($ak1->id); ?>"  value="1" <?php if($ak1->ce_check=='1'): ?> checked <?php endif; ?>/>
																                <label for="ce_<?php echo e($ak1->id); ?>" style="margin-top: 27px;position:absolute;"></label>
																	   
																	</div>
													    
																</div>
															</div>
														</div>
													</div>
											    </div>
											    
											<script>
										    $(document).ready(function()
                                            {
                                                var pro=$('#professiontype<?php echo e($ak1->id); ?>').val();
                                                if(pro == 'Firm')
                                                {
                                                    $(".formss<?php echo e($ak1->id); ?>").hide();
                                                }
                                                else if(pro == 'Individual')
                                                {
                                                    $(".individual<?php echo e($ak1->id); ?>").show();
                                                }
                                                
                                                // profession<?php echo e($ak1->id); ?>

                                                // profession_state<?php echo e($ak1->id); ?>

                                                // professiontype<?php echo e($ak1->id); ?>

                                                // profession_priority<?php echo e($ak1->id); ?>

                                                // profession_exp_date<?php echo e($ak1->id); ?>

                                                
                                                
                                    		    var prof=$(".profession<?php echo e($ak1->id); ?>").val();
                                    	    	var profstate=$(".profession_state<?php echo e($ak1->id); ?>").val();
                                    	    	var proftype=$(".professiontype<?php echo e($ak1->id); ?>").val();
                                    	    	
                                    	    	var id4='-';
                                                var fullids=prof.concat(id4).concat(profstate).concat(id4).concat(proftype);
                                    	    	//alert(fullids);
                                    	   // 	var profprio1=$(".profession_priority1<?php echo e($ak1->id); ?>").val();
                                    	   // 	var profprio2=$(".profession_priority2<?php echo e($ak1->id); ?>").val();
                                    	   // 	var profprio3=$(".profession_priority3<?php echo e($ak1->id); ?>").val();
                                    	    	//var profexpdate=$(".profession_exp_date<?php echo e($ak1->id); ?>").val();
                                    	    	
                                    	    	
                                    	    	if(prof == 'CPA' && profstate == 'GA' && proftype =='Individual')
                                    	    	{
                                    	    	    //$(".profession_exp_date<?php echo e($ak1->id); ?>").val('31-Dec');
                                    	    	    $(".profession_priority2<?php echo e($ak1->id); ?>").show();
                                    	    	    $(".profession_priority1<?php echo e($ak1->id); ?>").hide();
                                    	    	    $(".profession_priority3<?php echo e($ak1->id); ?>").hide();
                                    	    	}
                                    	    	else if(prof == 'CPA' && profstate == 'GA' && proftype =='Firm')
                                    	    	{
                                    	    	    //$(".profession_exp_date<?php echo e($ak1->id); ?>").val('30-Jun');
                                    	    	    $(".profession_priority2<?php echo e($ak1->id); ?>").show();
                                    	    	    $(".profession_priority1<?php echo e($ak1->id); ?>").hide();
                                    	    	    $(".profession_priority3<?php echo e($ak1->id); ?>").hide();
                                    	    	   
                                    	    	}
                                    	    	else if(prof == 'Mortgage Broker' && profstate == 'GA' && proftype =='Firm')
                                    	    	{
                                    	    	    //$(".profession_exp_date<?php echo e($ak1->id); ?>").val('30-Dec');
                                    	    	    $(".profession_priority2<?php echo e($ak1->id); ?>").hide();
                                    	    	    $(".profession_priority1<?php echo e($ak1->id); ?>").show();
                                    	    	    $(".profession_priority3<?php echo e($ak1->id); ?>").hide();
                                    	    	}
                                    	    	else if(prof == 'MLO' && profstate == 'GA' && proftype =='Individual')
                                    	    	{
                                    	    	    //$(".profession_exp_date<?php echo e($ak1->id); ?>").val('31-Dec');
                                    	    	    $(".profession_priority2<?php echo e($ak1->id); ?>").hide();
                                    	    	    $(".profession_priority1<?php echo e($ak1->id); ?>").show();
                                    	    	    $(".profession_priority3<?php echo e($ak1->id); ?>").hide();
                                    	    	}
                                    	    	else if(prof == 'Insurance Broker' && profstate == 'GA' && proftype =='Firm')
                                    	    	{
                                    	    	    //$(".profession_exp_date<?php echo e($ak1->id); ?>").val('31-Dec');
                                    	    	    $(".profession_priority2<?php echo e($ak1->id); ?>").show();
                                    	    	    $(".profession_priority1<?php echo e($ak1->id); ?>").hide();
                                    	    	    $(".profession_priority3<?php echo e($ak1->id); ?>").hide();
                                    	    	}
                                    	    	else if(prof == 'Insurance Agent' && profstate == 'GA' && proftype =='Individual')
                                    	    	{
                                    	    	    //$(".profession_exp_date<?php echo e($ak1->id); ?>").val('31-Dec');
                                    	    	    $(".profession_priority2<?php echo e($ak1->id); ?>").show();
                                    	    	    $(".profession_priority1<?php echo e($ak1->id); ?>").hide();
                                    	    	    $(".profession_priority3<?php echo e($ak1->id); ?>").hide();
                                    	    	}
                                    	    	else
                                    	    	{
                                    	    	    $(".profession_priority2<?php echo e($ak1->id); ?>").hide();
                                    	    	    $(".profession_priority1<?php echo e($ak1->id); ?>").hide();
                                    	    	    $(".profession_priority3<?php echo e($ak1->id); ?>").show();
                                    	    	}
                                        	 
                                        	 
                                        	    // onclick
                                        	    $(".profession<?php echo e($ak1->id); ?>").on("change", function()
                                                {
                                                    var prof=$(".profession<?php echo e($ak1->id); ?>").val();
                                        	    	var profstate=$(".profession_state<?php echo e($ak1->id); ?>").val();
                                        	    	var proftype=$(".professiontype<?php echo e($ak1->id); ?>").val();
                                        	    	//var profprio=$(".profession_priority<?php echo e($ak1->id); ?>").val();
                                    	    	
                                    	    	    var id4='-';
                                                    var fullids=prof.concat(id4).concat(profstate).concat(id4).concat(proftype);
                                    	    	    //alert(fullids);
                                    	    	    
                                    	    	    $.get('<?php echo URL::to('getAdminlic'); ?>?filename='+fullids, function(data)
                                                    {  
                                                        //alert(data);
                                                        //console.log(data);return false;
                                                        if(data>=1)
                                                        {
                                                            //recInsert,recExist
                                                            alert('Record Already Exist!');
                                                            $('#recExist').show();
                                                            $(".recExist").show();
                                                            $("#recInsert").hide();
                                                             
                                                        }
                                                        else
                                                        {
                                                            $('#recExist').hide();
                                                            $(".recExist").hide();
                                                            $("#recInsert").show();
                                                        }
                                                    });
                                    	    	    
                                    	    	    if(prof == 'CPA' && profstate == 'GA' && proftype =='Individual')
                                        	    	{
                                        	    	    $(".profession_exp_date<?php echo e($ak1->id); ?>").val('12/31');
                                        	    	    $(".profession_priority2<?php echo e($ak1->id); ?>").show();
                                        	    	    $(".profession_priority1<?php echo e($ak1->id); ?>").hide();
                                        	    	    $(".profession_priority3<?php echo e($ak1->id); ?>").hide();
                                        	    	}
                                        	    	else if(prof == 'CPA' && profstate == 'GA' && proftype =='Firm')
                                        	    	{
                                        	    	    $(".profession_exp_date<?php echo e($ak1->id); ?>").val('06/30');
                                        	    	    $(".profession_priority2<?php echo e($ak1->id); ?>").show();
                                        	    	    $(".profession_priority1<?php echo e($ak1->id); ?>").hide();
                                        	    	    $(".profession_priority3<?php echo e($ak1->id); ?>").hide();
                                        	    	   
                                        	    	}
                                        	    	else if(prof == 'Mortgage Broker' && profstate == 'GA' && proftype =='Firm')
                                        	    	{
                                        	    	    $(".profession_exp_date<?php echo e($ak1->id); ?>").val('12/30');
                                        	    	    $(".profession_priority2<?php echo e($ak1->id); ?>").hide();
                                        	    	    $(".profession_priority1<?php echo e($ak1->id); ?>").show();
                                        	    	    $(".profession_priority3<?php echo e($ak1->id); ?>").hide();
                                        	    	}
                                        	    	else if(prof == 'MLO' && profstate == 'GA' && proftype =='Individual')
                                        	    	{
                                        	    	    $(".profession_exp_date<?php echo e($ak1->id); ?>").val('12/31');
                                        	    	    $(".profession_priority2<?php echo e($ak1->id); ?>").hide();
                                        	    	    $(".profession_priority1<?php echo e($ak1->id); ?>").show();
                                        	    	    $(".profession_priority3<?php echo e($ak1->id); ?>").hide();
                                        	    	}
                                        	    	else if(prof == 'Insurance Broker' && profstate == 'GA' && proftype =='Firm')
                                        	    	{
                                        	    	    $(".profession_exp_date<?php echo e($ak1->id); ?>").val('12/31');
                                        	    	    $(".profession_priority2<?php echo e($ak1->id); ?>").show();
                                        	    	    $(".profession_priority1<?php echo e($ak1->id); ?>").hide();
                                        	    	    $(".profession_priority3<?php echo e($ak1->id); ?>").hide();
                                        	    	}
                                        	    	else if(prof == 'Insurance Agent' && profstate == 'GA' && proftype =='Individual')
                                        	    	{
                                        	    	    $(".profession_exp_date<?php echo e($ak1->id); ?>").val('12/31');
                                        	    	    $(".profession_priority2<?php echo e($ak1->id); ?>").show();
                                        	    	    $(".profession_priority1<?php echo e($ak1->id); ?>").hide();
                                        	    	    $(".profession_priority3<?php echo e($ak1->id); ?>").hide();
                                        	    	}
                                        	    	else
                                        	    	{
                                        	    	    $(".profession_priority2<?php echo e($ak1->id); ?>").hide();
                                        	    	    $(".profession_priority1<?php echo e($ak1->id); ?>").hide();
                                        	    	    $(".profession_priority3<?php echo e($ak1->id); ?>").show();
                                        	    	}
                                    	    		
                                                });
                                                
                                                $(".profession_state<?php echo e($ak1->id); ?>").on("change", function()
                                                {
                                                    var prof=$(".profession<?php echo e($ak1->id); ?>").val();
                                        	    	var profstate=$(".profession_state<?php echo e($ak1->id); ?>").val();
                                        	    	var proftype=$(".professiontype<?php echo e($ak1->id); ?>").val();
                                        	    	//var profprio=$(".profession_priority<?php echo e($ak1->id); ?>").val();
                                    	    	
                                    	    	    var id4='-';
                                                    var fullids=prof.concat(id4).concat(profstate).concat(id4).concat(proftype);
                                    	    	    //alert(fullids);
                                    	    	    
                                    	    	    $.get('<?php echo URL::to('getAdminlic'); ?>?filename='+fullids, function(data)
                                                    {  
                                                        //alert(data);
                                                        //console.log(data);return false;
                                                        if(data>=1)
                                                        {
                                                            //recInsert,recExist
                                                            alert('Record Already Exist!');
                                                            $('#recExist').show();
                                                            $(".recExist").show();
                                                            $("#recInsert").hide();
                                                             
                                                        }
                                                        else
                                                        {
                                                            $('#recExist').hide();
                                                            $(".recExist").hide();
                                                            $("#recInsert").show();
                                                        }
                                                    });
                                    	    	    
                                    	    	    if(prof == 'CPA' && profstate == 'GA' && proftype =='Individual')
                                        	    	{
                                        	    	    $(".profession_exp_date<?php echo e($ak1->id); ?>").val('12/31');
                                        	    	    $(".profession_priority2<?php echo e($ak1->id); ?>").show();
                                        	    	    $(".profession_priority1<?php echo e($ak1->id); ?>").hide();
                                        	    	    $(".profession_priority3<?php echo e($ak1->id); ?>").hide();
                                        	    	}
                                        	    	else if(prof == 'CPA' && profstate == 'GA' && proftype =='Firm')
                                        	    	{
                                        	    	    $(".profession_exp_date<?php echo e($ak1->id); ?>").val('06/30');
                                        	    	    $(".profession_priority2<?php echo e($ak1->id); ?>").show();
                                        	    	    $(".profession_priority1<?php echo e($ak1->id); ?>").hide();
                                        	    	    $(".profession_priority3<?php echo e($ak1->id); ?>").hide();
                                        	    	   
                                        	    	}
                                        	    	else if(prof == 'Mortgage Broker' && profstate == 'GA' && proftype =='Firm')
                                        	    	{
                                        	    	    $(".profession_exp_date<?php echo e($ak1->id); ?>").val('12/30');
                                        	    	    $(".profession_priority2<?php echo e($ak1->id); ?>").hide();
                                        	    	    $(".profession_priority1<?php echo e($ak1->id); ?>").show();
                                        	    	    $(".profession_priority3<?php echo e($ak1->id); ?>").hide();
                                        	    	}
                                        	    	else if(prof == 'MLO' && profstate == 'GA' && proftype =='Individual')
                                        	    	{
                                        	    	    $(".profession_exp_date<?php echo e($ak1->id); ?>").val('12/31');
                                        	    	    $(".profession_priority2<?php echo e($ak1->id); ?>").hide();
                                        	    	    $(".profession_priority1<?php echo e($ak1->id); ?>").show();
                                        	    	    $(".profession_priority3<?php echo e($ak1->id); ?>").hide();
                                        	    	}
                                        	    	else if(prof == 'Insurance Broker' && profstate == 'GA' && proftype =='Firm')
                                        	    	{
                                        	    	    $(".profession_exp_date<?php echo e($ak1->id); ?>").val('12/31');
                                        	    	    $(".profession_priority2<?php echo e($ak1->id); ?>").show();
                                        	    	    $(".profession_priority1<?php echo e($ak1->id); ?>").hide();
                                        	    	    $(".profession_priority3<?php echo e($ak1->id); ?>").hide();
                                        	    	}
                                        	    	else if(prof == 'Insurance Agent' && profstate == 'GA' && proftype =='Individual')
                                        	    	{
                                        	    	    $(".profession_exp_date<?php echo e($ak1->id); ?>").val('12/31');
                                        	    	    $(".profession_priority2<?php echo e($ak1->id); ?>").show();
                                        	    	    $(".profession_priority1<?php echo e($ak1->id); ?>").hide();
                                        	    	    $(".profession_priority3<?php echo e($ak1->id); ?>").hide();
                                        	    	}
                                        	    	else
                                        	    	{
                                        	    	    $(".profession_priority2<?php echo e($ak1->id); ?>").hide();
                                        	    	    $(".profession_priority1<?php echo e($ak1->id); ?>").hide();
                                        	    	    $(".profession_priority3<?php echo e($ak1->id); ?>").show();
                                        	    	}
                                    	    		
                                                });
                                        	    
                                        	    $(".professiontype<?php echo e($ak1->id); ?>").on("change", function()
                                                {
                                                    var prof=$(".profession<?php echo e($ak1->id); ?>").val();
                                        	    	var profstate=$(".profession_state<?php echo e($ak1->id); ?>").val();
                                        	    	var proftype=$(".professiontype<?php echo e($ak1->id); ?>").val();
                                        	    	//var profprio=$(".profession_priority<?php echo e($ak1->id); ?>").val();
                                    	    	
                                    	    	    var id4='-';
                                                    var fullids=prof.concat(id4).concat(profstate).concat(id4).concat(proftype);
                                    	    	    //alert(fullids);
                                    	    	    
                                    	    	    $.get('<?php echo URL::to('getAdminlic'); ?>?filename='+fullids, function(data)
                                                    {  
                                                        //alert(data);
                                                        //console.log(data);return false;
                                                        if(data>=1)
                                                        {
                                                            //recInsert,recExist
                                                            alert('Record Already Exist!');
                                                            $('#recExist').show();
                                                            $(".recExist").show();
                                                            $("#recInsert").hide();
                                                             
                                                        }
                                                        else
                                                        {
                                                            $('#recExist').hide();
                                                            $(".recExist").hide();
                                                            $("#recInsert").show();
                                                        }
                                                    });
                                    	    	    
                                    	    	    if(prof == 'CPA' && profstate == 'GA' && proftype =='Individual')
                                        	    	{
                                        	    	    $(".profession_exp_date<?php echo e($ak1->id); ?>").val('12/31');
                                        	    	    $(".profession_priority2<?php echo e($ak1->id); ?>").show();
                                        	    	    $(".profession_priority1<?php echo e($ak1->id); ?>").hide();
                                        	    	    $(".profession_priority3<?php echo e($ak1->id); ?>").hide();
                                        	    	}
                                        	    	else if(prof == 'CPA' && profstate == 'GA' && proftype =='Firm')
                                        	    	{
                                        	    	    $(".profession_exp_date<?php echo e($ak1->id); ?>").val('06/30');
                                        	    	    $(".profession_priority2<?php echo e($ak1->id); ?>").show();
                                        	    	    $(".profession_priority1<?php echo e($ak1->id); ?>").hide();
                                        	    	    $(".profession_priority3<?php echo e($ak1->id); ?>").hide();
                                        	    	   
                                        	    	}
                                        	    	else if(prof == 'Mortgage Broker' && profstate == 'GA' && proftype =='Firm')
                                        	    	{
                                        	    	    $(".profession_exp_date<?php echo e($ak1->id); ?>").val('12/30');
                                        	    	    $(".profession_priority2<?php echo e($ak1->id); ?>").hide();
                                        	    	    $(".profession_priority1<?php echo e($ak1->id); ?>").show();
                                        	    	    $(".profession_priority3<?php echo e($ak1->id); ?>").hide();
                                        	    	}
                                        	    	else if(prof == 'MLO' && profstate == 'GA' && proftype =='Individual')
                                        	    	{
                                        	    	    $(".profession_exp_date<?php echo e($ak1->id); ?>").val('12/31');
                                        	    	    $(".profession_priority2<?php echo e($ak1->id); ?>").hide();
                                        	    	    $(".profession_priority1<?php echo e($ak1->id); ?>").show();
                                        	    	    $(".profession_priority3<?php echo e($ak1->id); ?>").hide();
                                        	    	}
                                        	    	else if(prof == 'Insurance Broker' && profstate == 'GA' && proftype =='Firm')
                                        	    	{
                                        	    	    $(".profession_exp_date<?php echo e($ak1->id); ?>").val('12/31');
                                        	    	    $(".profession_priority2<?php echo e($ak1->id); ?>").show();
                                        	    	    $(".profession_priority1<?php echo e($ak1->id); ?>").hide();
                                        	    	    $(".profession_priority3<?php echo e($ak1->id); ?>").hide();
                                        	    	}
                                        	    	else if(prof == 'Insurance Agent' && profstate == 'GA' && proftype =='Individual')
                                        	    	{
                                        	    	    $(".profession_exp_date<?php echo e($ak1->id); ?>").val('12/31');
                                        	    	    $(".profession_priority2<?php echo e($ak1->id); ?>").show();
                                        	    	    $(".profession_priority1<?php echo e($ak1->id); ?>").hide();
                                        	    	    $(".profession_priority3<?php echo e($ak1->id); ?>").hide();
                                        	    	}
                                        	    	else
                                        	    	{
                                        	    	    $(".profession_priority2<?php echo e($ak1->id); ?>").hide();
                                        	    	    $(".profession_priority1<?php echo e($ak1->id); ?>").hide();
                                        	    	    $(".profession_priority3<?php echo e($ak1->id); ?>").show();
                                        	    	}
                                    	    	
                                        	   //     if(prof == 'CPA' && profstate == 'GA' && proftype =='Individual')
                                        	   // 	{
                                        	   // 	    $(".profession_exp_date<?php echo e($ak1->id); ?>").val('31-Dec');
                                        	   // 	    $(".profession_priority<?php echo e($ak1->id); ?>").val('2');
                                        	   // 	}
                                        	   // 	else if(prof == 'CPA' && profstate == 'GA' && proftype =='Firm')
                                        	   // 	{
                                        	   // 	    $(".profession_exp_date<?php echo e($ak1->id); ?>").val('30-Jun');
                                        	   // 	    $(".profession_priority<?php echo e($ak1->id); ?>").val('2');
                                        	    	   
                                        	   // 	}
                                        	   // 	else if(prof == 'Mortgage Broker' && profstate == 'GA' && proftype =='Firm')
                                        	   // 	{
                                        	   // 	    $(".profession_exp_date<?php echo e($ak1->id); ?>").val('30-Dec');
                                        	   // 	    $(".profession_priority<?php echo e($ak1->id); ?>").val('1');
                                        	   // 	}
                                        	   // 	else if(prof == 'MLO' && profstate == 'GA' && proftype =='Individual')
                                        	   // 	{
                                        	   // 	    $(".profession_exp_date<?php echo e($ak1->id); ?>").val('31-Dec');
                                        	   // 	    $(".profession_priority<?php echo e($ak1->id); ?>").val('1');
                                        	   // 	}
                                        	   // 	else if(prof == 'Insurance Broker' && profstate == 'GA' && proftype =='Firm')
                                        	   // 	{
                                        	   // 	    $(".profession_exp_date<?php echo e($ak1->id); ?>").val('31-Dec');
                                        	   // 	    $(".profession_priority<?php echo e($ak1->id); ?>").val('2');
                                        	   // 	}
                                        	   // 	else if(prof == 'Insurance Agent' && profstate == 'GA' && proftype =='Individual')
                                        	   // 	{
                                        	   // 	    $(".profession_exp_date<?php echo e($ak1->id); ?>").val('31-Dec');
                                        	   // 	    $(".profession_priority<?php echo e($ak1->id); ?>").val('2');
                                        	   // 	}
                                        	    	
                                                }); 	
                                        	    	
                                        	   // 	if(thiss == 'Individual' && prof == 'CPA')
                                        	   // 	{
                                        	   // 	    $('#profession_priority2'+x+'').show();
                                        	   // 	    $('#profession_priority'+x+'').hide();
                                        	   // 	}
                                        	   // 	else
                                        	   // 	{
                                        	   // 	    $('#profession_priority2'+x+'').hide();
                                        	   // 	    $('#profession_priority'+x+'').show();
                                        	   // 	}
                                        
                                        		
                                                
                                                
                                                // var ckstatus=$('#check_status<?php echo e($ak1->id); ?>').val();
                                                
                                                // if(ckstatus == 1)
                                                // {
                                                //     $("#profession_epr_date<?php echo e($ak1->id); ?>").hide();
                                                //     $("#profession_priority<?php echo e($ak1->id); ?>").hide();
                                                    
                                                // }
                                                // else if(ckstatus == 0)
                                                // {
                                                //     $("#profession_epr_date_<?php echo e($ak1->id); ?>").show();
                                                //     $("#profession_priority_<?php echo e($ak1->id); ?>").show();
                                                    
                                                // }
                                                
                                            }); 
                                            
											</script>    
											<script>
											$(document).ready(function()
                                            {   
    										    $(".professiontypefunction<?php echo e($ak1->id); ?>").on("change", function()
                                                {  
                                                    //alert();
                                                    var pro=$('#professiontype<?php echo e($ak1->id); ?>').val();
                                                   // alert(pro);
                                                    if(pro == 'Firm')
                                                    {
                                                        $(".formss<?php echo e($ak1->id); ?>").hide();
                                                    }
                                                    else if(pro == 'Individual')
                                                    { 
                                                        //$(".individual<?php echo e($ak1->id); ?>").hide();
                                                        $("#individual<?php echo e($ak1->id); ?>").show();
                                                    }
                                                });
                                                
                                                
                                                
                                                
                                            });

											    
											</script>
											
												<?php endif; ?>
												
												<?php $i++;?>
												<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
												
												<?php else: ?>
												
												<div class="">
													<div class="field_wrapper">
														<div id="field0">
															<div class="professional_tabs_main">
																<div class="professional_tabs">
																    
																	<div class="professional_tab professional_profession">
																		<label>Profession :</label>
																		<span class="d_flex">
    																		<select type="text" class="form-control-insu profession_option" id="" name="profession[]">
    																		    <option value="">Select</option>
    																		    <?php $__currentLoopData = $admin_professionlist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cur): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    																		        <option value="<?php echo e($cur->name); ?>"><?php echo e($cur->name); ?></option>
    																		    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    																			
                																<!--<option value="ERO">ERO</option>-->
                																<!--<option value="CPA">CPA</option>-->
                																<!--<option value="Mortgage Broker">Mortgage Broker</option>-->
                																<!--<option value="PTIN">PTIN</option>-->
                																<!--<option value="Insurance Agent">Insurance Agent</option>-->
                																<!--<option value="Insurance Broker">Insurance Broker</option>-->
                																<!--<option value="MLO">MLO</option>-->
    																		</select>&nbsp;
    																		<a href="#" class="btn btn-primary" data-toggle="modal" data-target="#basicExampleModal"><i class="fa fa-plus v_middle"></i></a>
    																		&nbsp;<a href="#" class="btn btn-primary" data-toggle="modal" data-target="#basicExampleModal3"><i class="fa fa-minus v_middle"></i></a>
																		</span>
																	</div>
																	
																	<div class="professional_tab professional_state">
																		<label>State :</label>
																		<select name="profession_state[]" id="profession_state" class="form-control-insu">
																		<option value="AK">AK</option>
																		<option value="AS">AS</option>
																		<option value="AZ">AZ</option>
																		<option value="AR">AR</option>
																		<option value="CA">CA</option>
																		<option value="CO">CO</option>
																		<option value="CT">CT</option>
																		<option value="DE">DE</option>
																		<option value="DC">DC</option>
																		<option value="FM">FM</option>
																		<option value="FL">FL</option>
																		<option value="GA">GA</option>
																		<option value="GU">GU</option>
																		<option value="HI">HI</option>
																		<option value="ID">ID</option>
																		<option value="IL">IL</option>
																		<option value="IN">IN</option>
																		<option value="IA">IA</option>
																		<option value="KS">KS</option>
																		<option value="KY">KY</option>
																		<option value="LA">LA</option>
																		<option value="ME">ME</option>
																		<option value="MH">MH</option>
																		<option value="MD">MD</option>
																		<option value="MA">MA</option>
																		<option value="MI">MI</option>
																		<option value="MN">MN</option>
																		<option value="MS">MS</option>
																		<option value="MO">MO</option>
																		<option value="MT">MT</option>
																		<option value="NE">NE</option>
																		<option value="NV">NV</option>
																		<option value="NH">NH</option>
																		<option value="NJ">NJ</option>
																		<option value="NM">NM</option>
																		<option value="NY">NY</option>
																		<option value="NC">NC</option>
																		<option value="ND">ND</option>
																		<option value="MP">MP</option>
																		<option value="OH">OH</option>
																		<option value="OK">OK</option>
																		<option value="OR">OR</option>
																		<option value="PW">PW</option>
																		<option value="PA">PA</option>
																		<option value="PR">PR</option>
																		<option value="RI">RI</option>
																		<option value="SC">SC</option>
																		<option value="SD">SD</option>
																		<option value="TN">TN</option>
																		<option value="TX">TX</option>
																		<option value="UT">UT</option>
																		<option value="VT">VT</option>
																		<option value="VI">VI</option>
																		<option value="VA">VA</option>
																		<option value="WA">WA</option>
																		<option value="WV">WV</option>
																		<option value="WI">WI</option>
																		<option value="WY">WY</option>
																		</select>
																	</div>
																	
																	<div class="professional_tab professional_effective">
																		<label>Effective Date :</label>
																		<input name="profession_effective_date[]" placeholder="Effective Date" value="" type="text" id="profession_effective_date" class="form-control-insu effective_date1">
																	</div>
																	
																	<div class="professional_tab professional_license">
																		<label>License # :</label>
																		<input name="profession_license[]" placeholder="License"  value="" type="text" id="profession_license" class="form-control-insu">
																	</div>
																	
																	<div class="professional_tab professional_expire">
																		<label>Expire Date :</label>
														                <input name="profession_epr_date[]" placeholder="Expire Date" value="" type="text" id="profession_epr_date" class="form-control-insu" readonly>
																	</div>
																	
																	<div class="professional_tab professional_profession professional_note">
																		<label>Note :</label>
																		<input name="profession_note[]" placeholder="Note"  value="" type="text" id="profession_note" class="form-control-insu">
																		
																	</div>
																	
																	<div class="professional_tab professional_state">
																		<label>CE :</label>
																		<p style="margin:7px 0px;"><input name="ce[]" type="checkbox" class="form-control"  id="ce[]" value="1" style="width:auto;">
																		<label class="" for="ce[]" style=""> Check</label></p>
																	</div>
																	
																	<div class="professional_tab professional_effective history_btn">
																		<label class="hide_991"></label>
																	    	<a data-toggle="modal-history" class="btn_new btn-view-license openBtn1 Pro-btn">License History</a>
																	</div>
																	<div class="professional_tab professional_license history_btn">
																
															<label class="btn-success hide_991"></label>
															 <a class="btn_new btn-view-license Pro-btn">File Not Upload</a>
														</div>
																	<div class="professional_tab professional_expire history_btn">
																	    
																	    
															<label class="btn-success hide_991"></label>
														<a href="javascript:void(0);" class="btn_new btn-renew btn-success">Renew Now</a> 
																	</div>
																</div>
															</div>
														</div>
													</div>
											
											</div>
												
												
												<?php endif; ?>
											
												<div class="">
													<div class="field_wrapper">
														<div id="field0">
															<div class="professional_tabs_main" id="professional_tabs_main">
																<div class="professional_tabs" style="background:transparent;border: transparent;">
																	<div class="" style="margin-top:0px;float:right;">
																		<a href="javascript:void(0);" style="width: 90px;bottom: auto;right: 0px;left: auto;position: initial;" id="add_button_pro" class="btn_new btn-add"><i class="fa fa-plus" aria-hidden="true"></i> Add </a>
																	</div>
																	
																</div>
															</div>
														</div>
													</div>
												</div>
											
											</div>
										</div>
										<div class="tab-pane fade" id="tab7primary">
											    <div class="">
													<div class="Branch" >
														<h1>Notes</h1>
													</div>
												</div>
												<br>
												<div class="col-md-12 col-sm-12 col-xs-12">
													<div class="input_fields_wrap_notes">
													    <?php $l=1; $notecon = count($admin_notes);?>
													    <?php if($notecon!=NULL): ?>
													     <?php $__currentLoopData = $admin_notes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $notes): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
													        <input name="noteid[]" value="<?php echo e($notes->id); ?>" type="hidden" placeholder="" id="noteid" class="textonly form-control">
													        <div class="form-group">
															    <label class="control-label col-md-3">Note <?php echo $l; $l++;?>:</label>
															    <div class="col-md-6">
														            <input name="adminnotes[]" value="<?php echo e($notes->notes); ?>"  type="text" placeholder="Create Note" id="adminnotes" class="textonly form-control">
														        </div> 
														        
														    
														<?php if($l==2): ?>
														<div class="col-md-2">
														   <a class="btn btn-primary " onclick="education_field_note();">Add </a>
														    
														<?php else: ?>
														<!--<div class="col-md-1">-->
														<!--    <a href="#myModalnote_<?php echo e($notes->id); ?>" id="add_row_note" role="button" class="btn btn-danger remove_note" title="Remove field" data-toggle="modal"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></a>-->
														<!--    </div>-->
														    <?php endif; ?>
													            <a href="#myModalnote_<?php echo e($notes->id); ?>" id="add_row_note" role="button" class="btn btn-danger remove_note" title="Remove field" data-toggle="modal"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></a>
													        </div>
														</div>
														<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
													    <?php else: ?>
														<input name="noteid[]" value="" type="hidden" placeholder="" id="noteid" class="textonly form-control">
													<div class="form-group">
															<label class="control-label col-md-3">Note :</label>
															<div class="col-md-6">
														<input name="adminnotes[]" value=""  type="text" placeholder="Create Note" id="adminnotes" class="textonly form-control">
														</div>   
														<div class="col-md-2">
														   <a class="btn btn-primary" onclick="education_field_note();">Add</a>
														    </div>
														<?php if($l==2): ?>
														<?php else: ?>
														<div class="col-md-1">
														    </div>
														    <?php endif; ?>
														</div>
														<?php endif; ?>
													</div>
												<div id="input_fields_wrap_notes"></div>
											</div>
											<div class="">
												<div class="Branch" >
													<h1>Tech. Company Info</h1>
												</div>
											</div>
											<div class="clear clearfix"></div>
											<br/>
											<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="form-group ">
												    <!--<small class="ln">(Legal Name)</small>-->
    												<label class="control-label col-md-3">Company Name <small>(Legal Name)</small> : </label>
    												<div class="col-lg-6 col-md-7">
    												    <input type="text" class="form-control"name="tec_company_name" value="<?php echo e(Auth::user()->tec_company_name); ?>" readonly>								   
    												</div>
												</div>
												<div class="form-group">
												    <!--<small class="ln1" style="margin-left: -18px;">(DBA)</small>-->
    												<label class="control-label col-md-3">Business Name <small>(DBA)</small> : </label>
    												<div class="col-lg-6 col-md-7">
    												<input type="text" class="form-control" name="tec_dba_name"  value="<?php echo e(Auth::user()->tec_dba_name); ?>" readonly>				
    												</div>
												</div>
												<div class="form-group  ">
												    <label class="control-label col-md-3">Reg. Address 1  : </label>
    												<div class="col-lg-6 col-md-7">
    												    <input type="text" class="form-control" value="<?php echo e(Auth::user()->tec_contact_address1); ?>" name="tec_contact_address1" readonly>								
    												</div>
												</div>
												<div class="form-group">
    												<label class="control-label col-md-3"> Reg. Address 2 : </label>
    												<div class="col-lg-6 col-md-7">
    												    <input type="text" class="form-control" value="<?php echo e(Auth::user()->tec_contact_address2); ?>" name="tec_contact_address2" readonly>								
    												</div>
												</div>
												<div class="form-group ">
    												<label class="control-label col-md-3">City / State / Zip  : </label>
    												<div class="col-lg-2 col-md-3">
    												    <input name="tec_company_city" value="<?php echo e(Auth::user()->tec_company_city); ?>" type="text" class="textonly form-control" readonly>
    												</div>
    												<div class="col-md-2 col-xs-6">
    												    <input type="hidden" name="tec_company_state" value="<?php echo e(Auth::user()->tec_company_state); ?>" class="form-control fsc-input">
    												    <select class="form-control fsc-input" disabled>
    												        <?php if(empty(Auth::user()->tec_company_state)): ?>
            												<option value="">State</option>
            												<?php else: ?>
            												<option value="<?php echo e(Auth::user()->tec_company_state); ?>"><?php echo e(Auth::user()->tec_company_state); ?></option>
            												<?php endif; ?>
            												<option value="GA">GA</option>
            												<option value="AK">AK</option>
            												<option value="AS">AS</option>
            												<option value="AZ">AZ</option>
            												<option value="AR">AR</option>
            												<option value="CA">CA</option>
            												<option value="CO">CO</option>
            												<option value="CT">CT</option>
            												<option value="DE">DE</option>
            												<option value="DC">DC</option>
            												<option value="FM">FM</option>
            												<option value="FL">FL</option>
            												<option value="GA">GA</option>
            												<option value="GU">GU</option>
            												<option value="HI">HI</option>
            												<option value="ID">ID</option>
            												<option value="IL">IL</option>
            												<option value="IN">IN</option>
            												<option value="IA">IA</option>
            												<option value="KS">KS</option>
            												<option value="KY">KY</option>
            												<option value="LA">LA</option>
            												<option value="ME">ME</option>
            												<option value="MH">MH</option>
            												<option value="MD">MD</option>
            												<option value="MA">MA</option>
            												<option value="MI">MI</option>
            												<option value="MN">MN</option>
            												<option value="MS">MS</option>
            												<option value="MO">MO</option>
            												<option value="MT">MT</option>
            												<option value="NE">NE</option>
            												<option value="NV">NV</option>
            												<option value="NH">NH</option>
            												<option value="NJ">NJ</option>
            												<option value="NM">NM</option>
            												<option value="NY">NY</option>
            												<option value="NC">NC</option>
            												<option value="ND">ND</option>
            												<option value="MP">MP</option>
            												<option value="OH">OH</option>
            												<option value="OK">OK</option>
            												<option value="OR">OR</option>
            												<option value="PW">PW</option>
            												<option value="PA">PA</option>
            												<option value="PR">PR</option>
            												<option value="RI">RI</option>
            												<option value="SC">SC</option>
            												<option value="SD">SD</option>
            												<option value="TN">TN</option>
            												<option value="TX">TX</option>
            												<option value="UT">UT</option>
            												<option value="VT">VT</option>
            												<option value="VI">VI</option>
            												<option value="VA">VA</option>
            												<option value="WA">WA</option>
            												<option value="WV">WV</option>
            												<option value="WI">WI</option>
            												<option value="WY">WY</option>
    												    </select>
    												</div>
    												<div class="col-md-2 col-xs-6">
    												    <input name="tec_company_zip" readonly value="<?php echo e(Auth::user()->tec_company_zip); ?>" type="text" id="company_zip2" class="zip form-control">
    												</div>
												</div>
												<div class="form-group ">
    												<label class="control-label col-md-3">Telephone No.  : </label>
    												<div class="col-lg-2 col-md-3">
    												    <input name="tec_company_mobile" readonly value="<?php echo e(Auth::user()->tec_company_mobile); ?>" type="text" id="motelephoneile2" class="form-control" placeholder="(999) 999-9999">
    												</div>
    												<div class="col-md-2 col-xs-6 fsc-form-col fsc-element-margin ">
    												    <input type="hidden" name="tec_telephoneNo1Type" value="<?php echo e(Auth::user()->tec_telephoneNo1Type); ?>" class="form-control fsc-input">
        												<select name="tec_telephoneNo1Type" id="tec_telephoneNo1Type" class="form-control fsc-input" disabled style="height:-1% !important">
        												    <option value="Office" <?php if(Auth::user()->tec_telephoneNo1Type=='Office'): ?> selected <?php endif; ?>>Office</option>
        												    <option value="Mobile" <?php if(Auth::user()->tec_telephoneNo1Type=='Mobile'): ?> selected <?php endif; ?>>Mobile</option>
        												</select>
    												</div>
    												<div class="col-md-2 col-xs-6 fsc-element-margin">
    												    <input class="form-control fsc-input ext2" id="tec_ext" readonly value="<?php echo e(Auth::user()->tec_ext1); ?>" maxlength="5" name="tec_ext1" placeholder="Ext" type="text">
    												</div>
												</div>
												<div class="form-group ">
    												<label class="control-label col-md-3">Fax No. : </label>
    												<div class="col-lg-2 col-md-3">
        												<input type="text" name="tec_fax" readonly value="<?php echo e(Auth::user()->tec_fax); ?>" id="fax2" placeholder="(999) 999-9999" class="form-control">
    												</div>
												</div>
												<div class="form-group ">
    												<label class="control-label col-md-3">Website : </label>
    												<div class="col-lg-6 col-md-7">
    												    <input name="tec_website" readonly value="<?php echo e(Auth::user()->tec_website); ?>" type="text" class="form-control">
    												</div>
												</div>
												<div class="form-group ">
    												<label class="control-label col-md-3">Email  : </label>
    												<div class="col-lg-6 col-md-7">
    												    <input type="text" readonly value="<?php echo e(Auth::user()->tec_company_email); ?>" class="form-control" name="tec_company_email">				
    												</div>
												</div>
											</div>
										</div>
										<div class="tab-pane fade" id="tab6primary">
										<?php
    									    if(isset($bankdata)!='')		
    									    {
    										    $bankcount = count($bankdata);
    									    }
										?>
										<div class="">
										<div class="Branch">
                                           <h1 class="titleleft">Banking Information </h1>
                                          
                                            <div class="btn-group btn-toggle" style="display:flex;">  
                                                <label class="control-label">Do You have Business Bank Account ? : </label>
                                                <?php $countbankdata=count($bankdata);
                                                if($countbankdata >0)
                                                {?>
                                                <input class="form-control" readonly type="text" value="Yes" style="width:55px; height:33px;">
                                                <input class="form-control" type="hidden" name="bank_required" value="1">
                                                <?php
                                                }
                                                else
                                                {
                                                    ?>
                                                
                                                
                                                <select class="form-control bank_que" name="bank_required" id="bank_required" style="float: right; width: 100px; vertical-align: middle; margin: 1px 0px 2px 0px; height: 33px !important;">
                                                    <option class="bg-green first" value="1" selected="">Yes</option>
                                                    <option class="bg-red second" value="0">No</option>
                                                </select>
                                                <?php 
                                                }
                                                ?>
                                            </div>
                                       </div>
                                       <div class="clear clearfix"></div>
                                       <?php
                                        if($bankcount>0)
                                        {
                                            ?>
                                            <div class="row show_bank1">
                                            <?php
                                        }
                                        else
                                        {
                                            ?>
                                            <div class="row show_bank1"  id="banknamediv" style="display: none;">
                                            <?php
                                        }
                                        ?>       
                                       <div class="col-md-12">
                                        <div class="form-group col-md-12">
                                            <label class="control-label">How many bank accounts do you have ? :</label>
                                              <input type="text" class="form-control howmany_box " id="bank_accounts_count" name="bank_accounts_count" value="<?php if($bankcount>0){echo $bankcount;}else{echo '1';}?>" style="width:100px; text-align:center;display:inline;" readonly>
                                                <a class="btn btn-primary addCFnew" style="padding: 9px 15px;margin-left: 10px;vertical-align:top;">Add</a>
                                            <span class="add_btn_banking" style="float:right;">
                                                <a href="<?php echo e(url('/fac-Bhavesh-0554/bankingsetup')); ?>" class="btn btn-warning">Add Bank Name</a>
                                            </span>
                                       </div>
                                       </div>
                                            <?php
                                                $i=1; 
                                                //echo $bankcount;
                                            ?>
                                            <?php if($bankcount>0): ?>
                                            <?php $__currentLoopData = $bankdata; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bankdetail): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											
                                            <div class="col-md-12">
                                                <div id="customFieldsbo">
                                              <div class="customfieldsbox">
                                               <div class="row form-group" style="margin-bottom:0px;">
                                                   <input type="hidden" class="form-control" name="bank_id[]" value="<?php echo e($bankdetail->id); ?>">
                                                   <div class="col-md-3">
                                                       <label class="control-label">Bank Name</label>
                                                        <select class="form-control" name="bank_name[]">
                                                            <option value="">Select</option>
                                                            <?php $__currentLoopData = $bankmaster; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bkmaster): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                <option value="<?php echo e($bkmaster->bankname); ?>" <?php if($bkmaster->bankname==$bankdetail->bank_name): ?> selected <?php endif; ?>><?php echo e($bkmaster->bankname); ?></option>
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        </select>
                                                   </div>
                                                    <div class="col-md-3">
                                                       <label class="control-label">Account Nick Name</label>
                                                       <input type="text" class="form-control" name="nick_name[]" value="<?php echo e($bankdetail->nick_name); ?>">
                                                   </div>
                                                    <div class="col-md-3 full_991" style="width:23%">
                                                       <label class="control-label">Last 4 digit of A/C #</label>
                                                       <input type="text" class="form-control fourdigit<?php echo $i;?>" name="fourdigit[]" value="<?php echo e($bankdetail->fourdigit); ?>">
                                                   </div>
                                                   <div class="col-md-3">
                                                       <label class="control-label">Open Date</label>
                                                       <input type="text" class="form-control opendate" id="opendate<?php echo $i;?>" name="opendate[]" value="<?php echo e($bankdetail->opendate); ?>">
                                                   </div>
                                                    <div class="col-md-1">
                                                    
                                                   </div>
                                               </div>
                                               <div class="row form-group">
                                                   <div class="col-md-3">
                                                       <label class="control-label">Check Stubs</label>
                                                       <select class="form-control" name="stubs[]">
                                                        <option value="">Select</option>
                                                       <option value="St. With Check Image" <?php if($bankdetail->stubs=='St. With Check Image'): ?> selected <?php endif; ?>>St. With Check Image</option>
                                                       <option value="Counter Check" <?php if($bankdetail->stubs=='Counter Check'): ?> selected <?php endif; ?>>Counter Check</option>
                                                   </select>
                                                   </div>
                                                   <div class="col-md-3">
                                                       <label class="control-label">Statement</label>
                                                       <select class="form-control" name="statement[]">
                                                       <option value="">Select</option>
                                                       <option value="E-Statement" <?php if($bankdetail->statement=='E-Statement'): ?> selected <?php endif; ?>>E-Statement</option>
                                                       <option value="Paper-Statement" <?php if($bankdetail->statement=='Paper-Statement'): ?> selected <?php endif; ?>>Paper-Statement</option>
                                                       <option value="FSC get Online" <?php if($bankdetail->statement=='FSC get Online'): ?> selected <?php endif; ?>>FSC get Online</option>
                                                       
                                                   </select>
                                                   </div>
                                                   
                                                    <div class="col-md-3 full_991" style="width:23%">
                                                       <label class="control-label">Status</label>
                                                       <select class="form-control" id="bankstatus<?php echo e($bankdetail->id); ?>" name="bankstatus[]">
                                                           <option value="">Select</option>
                                                           <option value="Active" <?php if($bankdetail->bankstatus=='Active'): ?> selected <?php endif; ?>>Active</option>
                                                           <option value="Close" <?php if($bankdetail->bankstatus=='Close'): ?> selected <?php endif; ?>>Close</option>
                                                           
                                                       </select>
                                                   </div>
                                                    <div class="col-md-3" id="closedate<?php echo e($bankdetail->id); ?>" style="width:20%;">
                                                       <label class="control-label">Close Date</label>
                                                       <input type="text" class="form-control closedate<?php echo $i;?>"  name="closedate[]" value="<?php echo e($bankdetail->closedate); ?>" readonly>
                                                   </div>
                                                  
                                                    <div class="col-md-1">
                                                        <?php if($bankcount>0): ?>
														    <a href="#bankModal_<?php echo e($bankdetail->id); ?>" id="remove_button_pro" role="button"class="btn btn-danger mt30" data-toggle="modal">Remove</a>
														<?php else: ?>
																    
														<?php endif; ?>
                                                      
                                                   </div>
                                               </div>
                                           </div>
                                           </div>
                                            </div>
                                            
                                            
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            
                                            <?php
                                                $i++;
                                            ?>
                                            <?php else: ?>
                                            
                                            <div class="col-md-12">
                                                <div id="customFieldsbo">
                                              <div class="customfieldsbox">
                                               <div class="row form-group" style="margin-bottom:0px;">
                                                   <div class="col-md-3">
                                                       <label class="control-label">Bank Name</label>
                                                       <!--<input type="text" class="form-control" name="bank_name[]">-->
                                                       <select class="form-control" name="bank_name[]">
                                                            <option value="">Select</option>
                                                            <?php $__currentLoopData = $bankmaster; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bkmaster): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                <option value="<?php echo e($bkmaster->bankname); ?>"><?php echo e($bkmaster->bankname); ?></option>
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        </select>
                                                   </div>
                                                    <div class="col-md-3">
                                                       <label class="control-label">Account Nick Name</label>
                                                       <input type="text" class="form-control" name="nick_name[]">
                                                   </div>
                                                    <div class="col-md-3 full_991" style="width:23%">
                                                       <label class="control-label">Last 4 digit of A/C #</label>
                                                       <input type="text" class="form-control fourdigit" name="fourdigit[]">
                                                   </div>
                                                   <div class="col-md-3 full_991" style="width:16%">
                                                       <label class="control-label">Open Date</label>
                                                       <input type="text" class="form-control opendate" name="opendate[]" >
                                                   </div>
                                                    <div class="col-md-1">
                                                    
                                                   </div>
                                               </div>
                                               <div class="row form-group">
                                                   <div class="col-md-3">
                                                       <label class="control-label">Check Stubs</label>
                                                       <select class="form-control" name="stubs[]">
                                                        <option value="">Select</option>
                                                       <option value="St. With Check Image">St. With Check Image</option>
                                                       <option value="Counter Check">Counter Check</option>
                                                   </select>
                                                   </div>
                                                   <div class="col-md-3">
                                                       <label class="control-label">Statement</label>
                                                       <select class="form-control" name="statement[]">
                                                       <option value="">Select</option>
                                                       <option value="E-Statement">E-Statement</option>
                                                       <option value="Paper-Statement">Paper-Statement</option>
                                                       <option value="FSC get Online">FSC get Online</option>
                                                       
                                                   </select>
                                                   </div>
                                                   
                                                    <div class="col-md-3 full_991" style="width:23%">
                                                       <label class="control-label">Status</label>
                                                       <select class="form-control statuss" name="bankstatus[]" >
                                                           <option value="">Select</option>
                                                           <option value="Active">Active</option>
                                                           <option value="Close">Close</option>
                                                           
                                                       </select>
                                                    </div>
                                                    <div class="col-md-3 closes" style="width:16%;display:none">
                                                       <label class="control-label">Close Date</label>
                                                       <input type="text" class="form-control closedate"  name="closedate[]" readonly>
                                                    </div>
                                                  
                                                    
                                               </div>
                                           </div>
                                           </div>
                                            </div>
                                            
                                            <?php endif; ?>
                                       
                                       </div>
                                       
                                     <div class="Branch">
                                     	<?php
									    if(isset($carddata)!='')		
									    {
										    $cardcount = count($carddata);
									    }
										?>   
										<h1 class="titleleft">Credit Card Information</h1>
                                        
                                        <div class="togglebox">
                                            <label class="control-label">Do You have Business Credit Card ? : </label>
                                            <div class="btn-group btn-toggle"> 
                                            <?php $countcarddata=count($carddata);
                                                if($countcarddata >0)
                                                {?>
                                                <input class="form-control" type="text" style="width:55px;height:33px;"  value="Yes" readonly>
                                                <input class="form-control" type="hidden" name="creditcard_required" value="1">
                                                <?php
                                                }
                                                else
                                                {
                                                    ?>
                                                
                                                <select name="creditcard_required" name="creditcard_required" id="creditcard_required" class="form-control card_que" style="float: right; width: 100px; vertical-align: middle; margin: 1px 0px 2px 0px; height: 33px;">
                                                    <option class="bg-green first" value="1">Yes</option>
                                                    <option class="bg-red second" value="0" selected>No</option>
                                                </select>
                                                    <?php 
                                                }
                                                ?>
                                            </div>
                                        </div>
                                                    
                                       </div>
                                       
                                       
                                     
                                        <?php
                                        if($cardcount>0)
                                        {
                                            ?>
                                            <div class="row show_card1">
                                            <?php
                                        }
                                        else
                                        {
                                            ?>
                                            <div class="row show_card1" id="creditcardnamediv" style="display:none">
                                            <?php
                                        }
                                        ?>     
                                        <div class="col-md-12">
                                             <div class="form-group col-md-12">
                                                <label class="control-label">How many Business Credit Card do you have ? : </label>
                                                <input type="text" value="<?php if($cardcount>0){echo $cardcount;}else{echo '1';}?>" class="form-control ccd_count howmany_box" name="business_credit_card" style="width:100px; text-align:center;display:inline;" readonly>
                                                <a class="btn btn-primary addCFnewccd" style="padding: 9px 15px;margin-left: 10px;vertical-align:top;">Add</a>
                                                <!--<span style="float:right;">-->
                                                <!--    <a href="#" class="btn btn-warning">Add Credit Card</a>-->
                                                <!--</span>-->
                                            </div>
                                        </div>
                                            <?php $i=1 ?>
                                            <?php if($cardcount>0): ?>
                                            <?php $__currentLoopData = $carddata; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $cardetail): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											
											<?php $is_disable = 'readonly' ?>
											<?php if($cardetail->card_bank_name == "Visa" || $cardetail->card_bank_name == "Master Card"): ?>
											    <?php $is_disable = '' ?>
											<?php endif; ?>

                                            <div class="col-md-12">
                                                <div id="customFieldsboccard">
                                                  <div class="customfieldsbox">
                                                    <div class="row form-group" style="margin-bottom:0px;">
                                                       <!--<div class="col-md-3">-->
                                                       <!--    <input type="hidden" class="form-control" name="card_id[]" value="<?php echo e($cardetail->id); ?>">-->
                                                       <!--    <label class="control-label">Credit Card Name</label>-->
                                                       <!--    <input type="text" class="form-control" name="card_bank_name[]" value="<?php echo e($cardetail->card_bank_name); ?>">-->
                                                       <!--</div>-->
                                                        <div class="col-md-3">
                                                            <label class="control-label">Type of CC</label>
                                                            <select class="form-control card_bank_name" id="<?php echo e($key+1); ?>" name="card_bank_name[]" onchange="setIssuerName(this);">
                                                                <option>Select</option>
                                                                <option value="Visa" <?php if($cardetail->card_bank_name == "Visa"): ?> selected <?php endif; ?>>Visa</option>
                                                                <option value="Master Card" <?php if($cardetail->card_bank_name == "Master Card"): ?> selected <?php endif; ?>>Master Card</option>
                                                                <option value="Discover" <?php if($cardetail->card_bank_name == "Discover"): ?> selected <?php endif; ?>>Discover</option>
                                                                <option value="American Express" <?php if($cardetail->card_bank_name == "American Express"): ?> selected <?php endif; ?>>American Express</option>
                                                                <option value="Other" <?php if($cardetail->card_bank_name == "Other"): ?> selected <?php endif; ?>>Other</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-3">
                                                           <!--<label class="control-label">Account Nick Name</label>-->
                                                           <label class="control-label">CC Issuer name</label>
                                                           <input type="text" class="form-control" id="card_issuer_name<?php echo e($key+1); ?>" <?php echo e($is_disable); ?> name="card_nick_name[]" value="<?php echo e($cardetail->card_nick_name); ?>" >
                                                        </div>
                                                        <div class="col-md-3 full_991" style="width:23%">
                                                           <label class="control-label">Last 4 digit of A/C #</label>
                                                           <input type="text" class="form-control cardfourdigit<?php echo $i;?>" name="card_fourdigit[]" value="<?php echo e($cardetail->card_fourdigit); ?>">
                                                        </div>
                                                       
                                                       
                                                        <div class="col-md-2 full_991" style="width:20%">
                                                           <label class="control-label">Statement</label>
                                                            <select class="form-control" name="card_statement[]">
                                                               <option value="">Select</option>
                                                               <option value="E-Statement" <?php if($cardetail->card_statement=='E-Statement'): ?> selected <?php endif; ?>>E-Statement</option>
                                                               <option value="Paper-Statement" <?php if($cardetail->card_statement=='Paper-Statement'): ?> selected <?php endif; ?>>Paper-Statement</option>
                                                               <option value="FSC get Online" <?php if($cardetail->card_statement=='FSC get Online'): ?> selected <?php endif; ?>>FSC get Online</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-1">
                                                        
                                                       </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col-md-3">
                                                           <label class="control-label">Note</label>
                                                           <input type="text" class="form-control" name="card_notes[]" value="<?php echo e($cardetail->card_notes); ?>">
                                                        </div>
                                                        <div class="col-md-3">
                                                           <label class="control-label">Open Date</label>
                                                           <input type="text" class="form-control cardopendate<?php echo $i;?>" name="card_opendate[]" value="<?php echo e($cardetail->card_opendate); ?>" readonly>
                                                        </div>
                                                        <div class="col-md-3 full_991" style="width:23%">
                                                           <label class="control-label">Status</label>
                                                           <select class="form-control" name="card_bankstatus[]" id="card_bankstatus<?php echo e($cardetail->id); ?>">
                                                               <option value="">Select</option>
                                                               
                                                               <option value="Active" <?php if($cardetail->card_bankstatus=='Active'): ?> selected <?php endif; ?>>Active</option>
                                                               <option value="Close" <?php if($cardetail->card_bankstatus=='Close'): ?> selected <?php endif; ?>>Close</option>
                                                             
                                                               
                                                           </select>
                                                        </div>
                                                        <div class="col-md-3" style="width:15.5%;display:none;" id="card_closedate<?php echo e($cardetail->id); ?>">
                                                           <label class="control-label">Close Date</label>
                                                           <input type="text" class="form-control cardclosedate<?php echo $i;?>" name="card_closedate[]" value="<?php echo e($cardetail->card_closedate); ?>" readonly>
                                                        </div>
                                                       
                                                        <div class="col-md-1">
                                                            
															    
															
																<a href="#cardModal_<?php echo e($cardetail->id); ?>" id="remove_button_pro" role="button"class="btn btn-danger mt30" data-toggle="modal">Remove</a>	    
															
                                                          
                                                       </div>
                                                      
                                                       <!-- <div class="col-md-1">-->
                                                       <!--   <a class="btn btn-primary mt30 addCFnewccd">Add</a>-->
                                                       <!--</div>-->
                                                    </div>
                                                </div>
                                               </div>
                                             
                                           </div>
                                           
                                            <script>
                                                $(document).ready(function()
                                                {
                                                    $('.opendate').datepicker();
                                                   	$(".cardfourdigit<?php echo $i;?>").mask("9999");
                                                    $(".cardopendate<?php echo $i;?>").datepicker({
                                                   		autoclose: true,
                                                         format: "mm/dd/yyyy",
                                                   	});
                                                   	$(".cardclosedate<?php echo $i;?>").datepicker({
                                                   		autoclose: true,
                                                         format: "mm/dd/yyyy",
                                                   	});
                                                   	
                                                   	var pro=$('#card_bankstatus<?php echo e($cardetail->id); ?>').val();
                                                    //alert(pro);
                                                    if(pro == 'Close')
                                                    {
                                                        $("#card_closedate<?php echo e($cardetail->id); ?>").show();
                                                    }
                                                    else
                                                    {
                                                        $("#card_closedate<?php echo e($cardetail->id); ?>").hide();
                                                    }


                                                });
                                                </script>
                                                
                                            <script>
											$(document).ready(function()
                                            {   
    										    $("#card_bankstatus<?php echo e($cardetail->id); ?>").on("click", function()
                                                {  
                                                    
                                                    var pro=$('#card_bankstatus<?php echo e($cardetail->id); ?>').val();
                                                     //alert(pro);
                                                    if(pro == 'Close')
                                                    {
                                                        $("#card_closedate<?php echo e($cardetail->id); ?>").show();
                                                    }
                                                    else
                                                    {
                                                        $("#card_closedate<?php echo e($cardetail->id); ?>").hide();
                                                    }
                                                });
                                                
                                                
                                                

                                                
                                            });

											    
											</script>
                                           
                                            
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php $i++ ?>
  
                                            <?php else: ?>
                                            
                                            <div class="col-md-12">
                                                <div id="customFieldsboccard">
                                                  <div class="customfieldsbox">
                                                   <div class="row form-group" style="margin-bottom:0px;">
                                                       <!--<div class="col-md-3">-->
                                                       <!--    <input type="hidden" class="form-control" name="card_id[]" value="">-->
                                                       <!--    <label class="control-label">Credit Card Name</label>-->
                                                       <!--    <input type="text" class="form-control" name="card_bank_name[]" value="">-->
                                                       <!--</div>-->
                                                        <div class="col-md-3">
                                                            <label class="control-label">Type of CC</label>
                                                            <select class="form-control card_bank_name" id="1" name="card_bank_name[]" onchange="setIssuerName(this);">
                                                                <option>Select</option>
                                                                <option value="Visa">Visa</option>
                                                                <option value="Master Card">Master Card</option>
                                                                <option value="Discover">Discover</option>
                                                                <option value="American Express">American Express</option>
                                                                <option value="Other">Other</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-3">
                                                           <!--<label class="control-label">Account Nick Name</label>-->
                                                           <label class="control-label">CC Issuer name</label>
                                                           <input type="text" class="form-control" id="card_issuer_name1" name="card_nick_name[]" value="">
                                                       </div>
                                                        <div class="col-md-3 full_991" style="width:23%">
                                                           <label class="control-label">Last 4 digit of A/C #</label>
                                                           <input type="text" class="form-control cardfourdigit" name="card_fourdigit[]" value="">
                                                       </div>
                                                       
                                                       
                                                       <div class="col-md-2 full_991" style="width:20%">
                                                           <label class="control-label">Statement</label>
                                                            <select class="form-control" name="card_statement[]">
                                                           <option value="">Select</option>
                                                           <option value="E-Statement">E-Statement</option>
                                                           <option value="Paper-Statement">Paper-Statement</option>
                                                           <option value="FSC get Online">FSC get Online</option>
                                                           
                                                       </select>
                                                       </div>
                                                        <div class="col-md-1">
                                                        
                                                       </div>
                                                   </div>
                                                   <div class="row form-group">
                                                       <div class="col-md-3">
                                                           <label class="control-label">Note</label>
                                                           <input type="text" class="form-control" name="card_notes[]" value="">
                                                       </div>
                                                       <div class="col-md-3">
                                                           <label class="control-label">Open Date</label>
                                                           <input type="text" class="form-control cardopendate" name="card_opendate[]" value="" readonly>
                                                       </div>
                                                        <div class="col-md-3 full_991" style="width:23%">
                                                           <label class="control-label">Status</label>
                                                           <select class="form-control" name="card_bankstatus[]" id="card_bankstatus">
                                                               <option value="">Select</option>
                                                               
                                                               <option value="Active">Active</option>
                                                               <option value="Close">Close</option>
                                                             
                                                               
                                                           </select>
                                                       </div>
                                                        <div class="col-md-3" style="width:15.5%;display:none;" id="card_closedate">
                                                           <label class="control-label">Close Date</label>
                                                           <input type="text" class="form-control cardclosedate" name="card_closedate[]" value="" readonly>
                                                       </div>
                                                       
                                                        <div class="col-md-1">
                                                            	<a href="#cardModal" id="remove_button_pro" role="button"class="btn btn-danger mt30" data-toggle="modal">Remove</a>
                                                       </div>
                                                      
                                                       <!-- <div class="col-md-1">-->
                                                       <!--   <a class="btn btn-primary mt30 addCFnewccd">Add</a>-->
                                                       <!--</div>-->
                                                   </div>
                                               </div>
                                               </div>
                                             
                                           </div>
                                            
                                            <?php endif; ?>
                                       </div>
                                       
                                      <div class="Branch">
                                             <h1 class="titleleft">Loan Information</h1>
                                             <!--<div class="togglebox">-->
                                             <!--    <label class="control-label">Do You have Business Loan</label>-->
                                             <!--      <div class="btn-group btn-toggle"> -->
                                             <!--           <a class="btn   btn-default btnyes " onclick="showcreditcardnamevid1();">Yes</a>-->
                                             <!--           <a class="btn btnno  active " onclick="showcreditcardnamevid2();">No</a>-->
                                             <!--            <input type="hidden" value="" id="businessloan" name="businessloan">-->
                                             <!--         </div>-->
                                             <!--           <a href="#" id="uparrow3" onclick="showcreditcardnamevid102();" style="display:none;"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>-->
                                             <!--<a href="#" id="dwnarrow3" onclick="hidecreditcardnamevid102();" style="display:none;"><i class="fa fa-chevron-down" aria-hidden="true"></i></a>-->
                                             <!--</div>-->
                                             <div class="togglebox">
                                                 <label class="control-label">Do You have Business Loan ? : </label>
                                                <div class="btn-group btn-toggle">     
                                                    <select name="business_loan" id="business_loan" class="form-control loan_que" style="float: right; width: 100px; vertical-align: middle; margin: 1px 0px 2px 0px; height: 33px;">
                                                        <option class="bg-green first" value="1">Yes</option>
                                                        <option class="bg-red second" value="0" selected="">No</option>
                                                    </select>
                                                </div>
                                                                                                       
                                             </div>
                                         </div> 
                                       
                                       <div class="form-group row" id="creditcardnamediv1" style="display:none">
                                           <label class="control-label col-md-4">Type of Loan / Note :</label>
                                           <div class="col-md-4">
                                             <select class="form-control" name="loannote">
                                                    <option value="">Select</option>
                                                   <option value="Business Loan">Business Loan</option>
                                                   <option value="Property Loan">Property Loan</option>
                                                   <option value="Personal Loan">Personal Loan</option>
                                                   <option value="Seller Note">Seller Note</option>
                                               </select>
                                           </div>
                                       </div>
									</div>
									
									
										</div>
											
											
									</div>
									
									<div class="card-footer">
												<div class="col-md-offset-3 col-md-7">
													<div class="row">
													    <div class="col-md-3 recExist" style="display:none;">
														    	<span style="color:red;" class="">Professional License Record already Exist!</span>
														</div>
														<div class="col-xs-3" style="width:155px;">
															<input class="btn_new_save btn-primary1" id="recInsert" type="submit" name="submit" value="Save">
															
														
															<input class="btn_new_save btn-primary1" id="recExist"  type="submit" name="submit" disabled value="Save2" style="display:none;" >
														</div>
														<div class="col-xs-3" style="width:155px">
															<a class="btn_new_cancel" href="<?php echo e(url('fac-Bhavesh-0554/')); ?>">Cancel</a> 
														</div>
															
													</div>
												</div>
											</div>
							</div>
							
							    </form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> 
	 </section>
</div>

    <!--Formation Renew Modal Start-->
    
    <div class="modal fade" id="basicExampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="exampleModalLabel">Profession </h4>
                </div>
                <form action="" method="post" id="profession_form">
                    <?php echo e(csrf_field()); ?>

                    <div class="modal-body">
                        <input type="text" id="profession_name" name="profession_name" class="form-control" placeholder="Profession Name"/>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="profession_save" class="btn btn-primary">Save</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
    <div class="modal fade" id="basicExampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background:#038ee0;">
                    <h4 class="modal-title" id="exampleModalLabel" style="text-align:center;color:#000000;">Profession
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </h4>
                </div>
                <div class="modal-body" style="background:#ffff99;padding:0px !important;">
                    <div class="curency curency_ref" id="professionForDelete">
                        <?php $__currentLoopData = $admin_professionlist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cur): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div id="cur_<?php echo e($cur->id); ?>" class="col-md-12" style="border:1px solid;background:#def9ff;">
                                <div class="col-md-12" style="margin-top: 7px;margin-bottom:5px;">
                                    <a class="deleteprofession" style="color:#000;" id="<?php echo e($cur->id); ?>"><?php echo e($cur->name); ?>

                                        <span class="pull-right"><i class="fa fa-trash" style="padding: 1px 6px!important;"></i></span>
                                    </a>
                                </div>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
                <div class="modal-footer" style="text-align:center;">
                    <button type="button" style="margin-top:10px;border:1px solid;" class="btn btn-secondary"data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    
	<div id="renewRecord" class="modal fade" role="dialog">
        <div class="modal-dialog">
                                    
           
            <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Renew Record</h4>
                                          </div>
                                                                                                                        
                                          <form method="post" class="form-horizontal" enctype="multipart/form-data" action="https://financialservicecenter.net/fac-Bhavesh-0554/adminprofile/updateformation">
									        <?php echo e(csrf_field()); ?>

									        <div class="modal-body">
                                               <div class="row">
                                                   <div class="col-md-4">
                                                       <label class="control-label text-right">Renew For : </label>
                                                   </div>
                                                   <div class="col-md-5">
                                                       
                                                        <select class="form-control formation_yearbox" id="formation_yearbox" name="formation_yearbox" onChange="getvalYear();">
                                                            <option value="">Select </option>
                                                            <option value="1">1 Year</option>
                                                            <option value="2">2 Year</option>
                                                            <option value="3">3 Year</option>
                                                        </select>
                                                        
                                                        <select class="form-control formation_yearbox2" id="formation_yearbox2" name="formation_yearbox2" onChange="getvalYear();"  style="display:none">
                                                            <option value="">Select </option>
                                                            <option value="1">1 Year</option>
                                                            <option value="2">2 Year</option>
                                                            <option value="3">3 Year</option>
                                                        </select>
                                                        
                                                        <select class="form-control formation_yearbox3" id="formation_yearbox3" name="formation_yearbox3" onChange="getvalYear();" style="display:none">
                                                            <option value="">Select </option>
                                                            <option value="1">1 Year</option>
                                                            <option value="2">2 Year</option>
                                                            <option value="3">3 Year</option>
                                                        </select>
                                                   </div>
                                               </div>
                                               
                                               <div class="row">
                                                   <div class="col-md-4">
                                                       <label class="control-label text-right">Renew Year : </label>
                                                   </div>
                                                   <div class="col-md-5">
                                    
                                                        <input type="text" class="form-control" name="formation_yearvalue" id="formation_yearvalue" required readonly/>
                                                   </div>
                                                   
                                                   
                                               </div>
                                               
                                               <div class="row">
                                                   <div class="col-md-4">
                                                       <label class="control-label text-right">Renew Amount : </label>
                                                   </div>
                                                   <div class="col-md-5">
                                                   <input class="form-control" type="textbox" readonly name="formation_amount" id="formation_amount" required>
                                                    </div>
                                               </div>
                                               
                                               <div class="row">
                                                   <div class="col-md-4">
                                                       <label class="control-label text-right">Paid Amount : </label>
                                                   </div>
                                                   <div class="col-md-5">
                                                   <input class="form-control" type="textbox" name="formation_paid" id="formation_paid" required>
                                                    </div>
                                               </div>
                                               
                                               <div class="row">
                                                   <div class="col-md-4">
                                                       <label class="control-label text-right">Method of Payment : </label>
                                                   </div>
                                                   <div class="col-md-5">
                                                       <input type="text" class="form-control" name="formation_payment" id="formation_payment" required/>
                                                   </div>
                                               </div>
                                               
                                               <div class="row">
                                                   <div class="col-md-4"><label class="control-label text-right">Status : </label>
                                                   </div>
                                                   <div class="col-md-5">
                                                       <select class="form-control" name="record_status" required>
                                                            <option value="0">Select</option>
                                                             <option value="Active Owes Curr. Yr. AR">Active Owes Curr. Yr. AR</option>
                                                             <option value="Active Compliance">Active Compliance</option>
                                                             <option value="Active Noncompliance">Active Noncompliance</option>
                                                             <option value="Admin. Dissolved">Admin. Dissolved</option>
                                                             <option value="Dis-Cancel-Termin">Dis-Cancel-Termin</option>
                                                         
                                                        </select>
                                                   </div>
                                               </div>
                                               
                                               <div class="row" style="margin-bottom:10px;">
                                                   <div class="col-md-4"><label class="control-label text-right">Annually Receipt : </label>
                                                   </div>
                                                   <div class="col-md-8">
                                                     
                                                        <label class="file-upload btn btn-primary">
                                                        <input type="file" class="form-control" name="annualreceipt" id="annualreceipt" required/>
                                                        Browse for file ... </label>
                                               
                                                   </div>
                                               </div>
                                                                   
                                               <div class="row" style="margin-bottom:10px;">
                                                   <div class="col-md-4"><label class="control-label text-right">Officer : </label>
                                                   </div>
                                                    <div class="col-md-8">
                                                     
                                                        <label class="file-upload btn btn-primary">
                                                         <input type="file" class="form-control" name="formation_work_officer"  id="formation_work_officer" required/>
                                                         Browse for file ... </label>
                                                          
                                                   </div>
                                               </div>
                                               
                                               <div class="row">
                                                   <div class="col-md-4">
                                                       <label class="control-label text-right">Website : </label>
                                                   </div>
                                                   <div class="col-md-5">
                                                   <input class="form-control" type="text" name="formation_website" id="formation_website" required>
                                                    </div>
                                               </div>
                                               
                                                <div class="row">
                                                   <div class="col-md-4">
                                                   </div>
                                                   <div class="col-md-2" style=" padding-right:3px;">
                                                       <!--<input class="btn_new_save btn-primary1" type="submit" value="Save" style="disabled:true">!-->
                                                       <input class="btn_new_save btn-primary1" type="submit" name="submit" value="Save">
                                                      </div>
                                                        <div class="col-md-2" style="padding-left:3px;">
                                                       <a class="btn_new_cancel" data-dismiss="modal">Cancel</a>
                                                    </div>
                                                </div>
                                          </div>
                                          </form>
                                          <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                          </div>
                                        </div>
                                    
        </div>
    </div>
    <!--Formation Renew Modal End-->
    
   

<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>



                                            
                                           








<script type="text/javascript">

// $(document).ready(function(){
//     <?php if(Auth::user()->soscertificate != '' && Auth::user()->sosaoi !=''): ?>
        
//         <?php if(Auth::user()->formation="Corporation"): ?>
//             $("#_corporation").attr('checked',true);
//             $("#_corporation1").attr('checked', false);
//         <?php else: ?>
//             $("#_corporation").attr('checked', false);
//             $("#_corporation1").attr('checked', true);
//         <?php endif; ?>
        
//         $('#_corporation').attr('disabled',true);
//         $('#_corporation1').attr('disabled',true);
        
//     <?php else: ?>
//         // $("#_corporation").attr('checked', true);
//         // $("#_corporation1").attr('checked', false);
//         $('#_corporation1').removeAttr('disabled');
//         $('#_corporation').removeAttr('disabled');
//     <?php endif; ?>
// });

(function($) {
    var minNumber = -100;
    var maxNumber = 100;
      $('.spinner .btn:first-of-type').on('click', function() {
        if ($('.spinner input').val() == maxNumber) {
          return false;
        } else {
          $('.spinner input').val(parseInt($('.spinner input').val(), 10) + 5 + '%');
        }
      });

  $('.txtinput_1').on("blur", function() {
    var inputVal = parseFloat($(this).val().replace('%', '')) || 0
    if (minNumber > inputVal) {
      inputVal = -100;
    } else if (maxNumber < inputVal) {
      inputVal = 100;
    }
    $(this).val(inputVal + '.00%');
  });

      $('.spinner .btn:last-of-type').on('click', function() {
        if ($('.spinner input').val() == minNumber) {
          return false;
        } else {
          $('.spinner input').val(parseInt($('.spinner input').val(), 10) - 5 + '%');
        }
      });
})(jQuery);


$(document).ready(function () 
{
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    });

    $('#profession_save').click(function(){
        var profession = $('#profession_name').val();
        $.ajax({
            type: "post",
            url: "<?php echo route('profile.profession'); ?>",
            dataType: "json",
            data: $('#profession_form').serialize(),
            success: function (data) {
                console.log(data['status']);
                if(data['status']=="Message"){
                    alert(data['Msg']);
                    $('#basicExampleModal').modal('hide');
                    $("#profession_name").val('');
                }else{
                    $('.profession_option').append('<option value=' + profession + '>' + profession + '</option>');
                    $('#professionForDelete').append('<div id="cur_' + data["id"] + '" class="col-md-12" style="border:1px solid;background:#def9ff;">' +
                        '<div class="col-md-12" style="margin-top: 7px;margin-bottom:5px;">' +
                        '<a class="deletenotes" style="color:#000;" id="' + data["id"] + '">' + data["name"] +
                        '<span class="pull-right"><i class="fa fa-trash" style="padding: 1px 6px!important;"></i></span>' +
                        '</a></div></div>');
                    $('#basicExampleModal').modal('hide');
                    $("#profession_name").val('');
                }
            },
            error: function (data) {
                alert("Error")
            }
        });
    });
    
    $(document).on('click', '.deleteprofession', function () {
        var id = $(this).attr('id');//alert();
        if (confirm("Are you sure you want to Delete this data?")) {
            $.ajax({
                url: "<?php echo e(route('profile.professionDelete')); ?>",
                mehtod: "get",
                data: {id: id},
                success: function (data) {
                    var json = JSON.parse(data);
                    //console.log(json);
                    
                    $('.profession_option').empty();
                    $('#professionForDelete').empty();
                    $('.profession_option').append('<option value="">Select</option>');
                    for (var i = 0; i < json.length; i++) {
                        console.log("name : " + json[i]['newopt']);
                        $('.profession_option').append('<option value=' + json[i]['name'] + '>' + json[i]['name'] + '</option>');
                        $('#professionForDelete').append('<div id="cur_' + json[i]['id'] + '" class="col-md-12" style="border:1px solid;background:#def9ff;">' +
                            '<div class="col-md-12" style="margin-top: 7px;margin-bottom:5px;">' +
                            '<a class="deleterelated" style="color:#000;" id="' + json[i]['id'] + '">' + json[i]['name'] +
                            '<span class="pull-right"><i class="fa fa-trash" style="padding: 1px 6px!important;"></i></span>' +
                            '</a></div></div>');
                    }
                    $('#basicExampleModal3').modal('hide');
                }
            })
        } else {
            return false;
        }
    });
    
    
    
    $('.lic_que').on('change', function() {
      if ( this.value == '1')
      {
        $(".show_business_lic").show();
        $(".show_business_lic1").hide();
      }
      else
      {
        $(".show_business_lic1").show();
        $(".show_business_lic").hide();
      }
    });
    $("#no_payroll").hide();
    $('#payroll_que').on('change', function() {
      if ( this.value == '1')
      {
        $(".fe_state_tab_main").show();
        $("#no_payroll").hide();
        
      }
      else
      {
        $(".fe_state_tab_main").hide();
        $("#no_payroll").show();
        
      }
    });
    
    $(".statuss").on("click", function()
   	{
   	    var pro=$('.statuss').val();
        //alert(pro);           
        if(pro == 'Close')
        {
            $(".closes").show();
        }
        else
        {
            $(".closes").hide();
        }
   	});
   	
   	
   	$(".statuss2").on("click", function()
   	{
   	    var pro=$('.statuss2').val();
        //alert(pro);           
        if(pro == 'Close')
        {
            $(".closes2").show();
        }
        else
        {
            $(".closes2").hide();
        }
   	});


    $(".cardopendate").datepicker({
   		autoclose: true,
         format: "mm/dd/yyyy",
   	});
   	$(".cardfourdigit").mask("9999");
   
   	
   	$(".cardclosedate").datepicker({
   		autoclose: true,
         format: "mm/dd/yyyy",
   	});

   // formation_yearbox_value
    var aa=$('#formation_yearbox_value').val();
    //alert(aa);
   
    if(aa=='')
    {
      $(".formation_yearbox").show();
      $(".formation_yearbox2").hide();
      $(".formation_yearbox3").hide();
      

    }
    else if(aa=='1')
    {
      $(".formation_yearbox").hide();
      $(".formation_yearbox2").show();
      $(".formation_yearbox3").hide();
      
    }
    else if(aa=='2')
    {
      $(".formation_yearbox").hide();
      $(".formation_yearbox2").hide();
      $(".formation_yearbox3").show();
      
    }
    

  $('#myform').validate({ // initialize the plugin
  	rules: {
				formation_yearbox: {
						required: true,
				},
    		},
	messages:
			{
				formation_yearbox: {
							required: 'Please select type a year ',
						},
			}
  });

});


function getvalYear(){

    var yearbox=$('#formation_yearbox').val();
    var yearbox2=$('#formation_yearbox2').val();
    var yearbox3=$('#formation_yearbox3').val();
    //alert(yearbox2);
    if(yearbox==''){
      //$("#formation_yearvalue").val('');
    }
    else if(yearbox==1)
    {
        $("#formation_yearvalue").val('2020');
        $("#formation_amount").val('$50.00');
    }
    else if(yearbox==2)
    {
        $("#formation_yearvalue").val('2020,2021');
        $("#formation_amount").val('$100.00');
    }
    else if(yearbox==3)
    {
        $("#formation_yearvalue").val('2020,2021,2022');
        $("#formation_amount").val('$150.00');
    }
    
    if(yearbox2==''){
      //$("#formation_yearvalue").val('');
    }
    else if(yearbox2==1)
    {
        $("#formation_yearvalue").val('2020');
        $("#formation_amount").val('$50.00');
    }
    else if(yearbox2==2)
    {
        $("#formation_yearvalue").val('2020,2021');
        $("#formation_amount").val('$100.00');
    }
    else if(yearbox2==3)
    {
        $("#formation_yearvalue").val('2020,2021,2022');
        $("#formation_amount").val('$150.00');
    }
    
    if(yearbox3==''){
      //$("#formation_yearvalue").val('');
    }
    else if(yearbox3==1)
    {
        $("#formation_yearvalue").val('2020');
        $("#formation_amount").val('$50.00');
    }
    else if(yearbox3==2)
    {
        $("#formation_yearvalue").val('2020,2021');
        $("#formation_amount").val('$100.00');
    }
    else if(yearbox3==3)
    {
        $("#formation_yearvalue").val('2020,2021,2022');
        $("#formation_amount").val('$150.00');
    }
}


</script>

<script>
$(document).ready(function()
{
    $('.openBtncertificate_lic').on('click', function()
    {
        var adminprolicid = $(this).attr('data-id');
            //alert(adminprolicid);
          $.get('<?php echo URL::to('getAdminprolic'); ?>?id='+adminprolicid, function(data)
          {
                //console.log(data);exit;
                if(data == "")
                {
                    $('#myModal2').modal({show:true}); 
                }
                else
                { 
                    $.each(data, function(index, subcatobj)
                    {
                        $('#myModalview1111').modal({show:true});
                        $('#viewmodelview111111').html('<embed src="https://financialservicecenter.net/public/adminupload/'+subcatobj.profession_license_copy+'" width="100%" height="300px">');
                    });
                }
          });
    });
    
   $('.openBtncertificate').on('click', function()
   {
       var num = '<?php echo Auth::user()->id;?>';
       $.get('<?php echo URL::to('getAdmincertificate'); ?>?id='+num, function(data)
       {
            if(data == "")
            {
                $('#myModal2').modal({show:true}); 
            }
            else
            { 
                $.each(data, function(index, subcatobj)
                {
          
                    $('#myModalview').modal({show:true});
                        $('#viewmodelview').html('<embed src="https://financialservicecenter.net/public/adminupload/'+subcatobj.soscertificate+'" width="100%" height="300px">');
                    });
           }
       });
   });
   
    $('.openBtnaoi').on('click', function()
   {
   var num = '<?php echo Auth::user()->id;?>';
   $.get('<?php echo URL::to('getAdminaoi'); ?>?id='+num, function(data)
   {
   if(data == "")
   {
   $('#myModal2').modal({show:true}); 
   }
   else
   { 
   $.each(data, function(index, subcatobj)
   {
      // alert(subcatobj.upload_name);
  
   $('#myModalviewaoi').modal({show:true});
   $('#viewmodelviewaoi').html('<embed src="https://financialservicecenter.net/public/adminupload/'+subcatobj.sosaoi+'" width="100%" height="300px">');
   });
   }
   });
   });
   
  
        
     $(".effective_date1").datepicker({
   		autoclose: true,
        format: "mm/dd/yyyy",
   	});
    
      $(".effective_date17").datepicker({
   		autoclose: true,
        format: "M/d/Y",
   	});
   	
    $('#typeofservice').on('change', function() 
    {
     
        var state=$("#state_id_2").val();
        var typeofcorp=$("#typeofservice").val();
   
        $("#typeofcorp").val(typeofcorp);
        if(this.value == 'C Corporation' && state=='GA')
        {
            $("#typeofcorps").val('Form-1120');
            $("#due_date_1").val('<?php echo "Apr-15-".date("Y");?>');
            $("#due_date_2").val('<?php echo "Apr-15-".date("Y");?>');
            $("#extension_due_date_1").val('<?php echo "Oct-15-".date("Y");?>');
            $("#extension_due_date_2").val('<?php echo "Oct-15-".date("Y");?>');
            $("#type_form_file").val('Form-1120');
            $("#type_form_file2").val('Form-600');
        }
        else if(this.value == 'S Corporation' && state=='GA')
        {
            $("#typeofcorps").val('Form-1120S');
             $("#due_date_1").val('<?php echo "Mar-15-".date("Y");?>');
            $("#due_date_2").val('<?php echo "Mar-15-".date("Y");?>');
            $("#extension_due_date_1").val('<?php echo "Sep-15-".date("Y");?>');
            $("#extension_due_date_2").val('<?php echo "Sep-15-".date("Y");?>');
           $("#type_form_file").val('Form-1120S');
            $("#type_form_file2").val('Form-600S');
        
        }
        else if((this.value == 'Single Member LLC' || this.value == 'Individual' ) && state=='GA')
        {
            $("#typeofcorps").val('1040--Sch -C or Sch-E');
            $("#due_date_1").val('<?php echo "Apr-15-".date("Y");?>');
            $("#due_date_2").val('<?php echo "Apr-15-".date("Y");?>');
            $("#extension_due_date_1").val('<?php echo "Oct-15-".date("Y");?>');
            $("#extension_due_date_2").val('<?php echo "Oct-15-".date("Y");?>');
            $("#type_form_file").val('1040--Sch -C or Sch-E');
            $("#type_form_file2").val('Form-500');
        }
        else if(this.value == 'LLC-2 or more member' && state=='GA')
        {
            $("#typeofcorps").val('Form-1065');
              $("#due_date_1").val('<?php echo "Mar-15-".date("Y");?>');
            $("#due_date_2").val('<?php echo "Mar-15-".date("Y");?>');
            $("#extension_due_date_1").val('<?php echo "Sep-15-".date("Y");?>');
            $("#extension_due_date_2").val('<?php echo "Sep-15-".date("Y");?>');
           $("#type_form_file").val('Form-1065');
            $("#type_form_file2").val('Form-700');
        }
        else if(this.value == 'Estate' && state=='GA')
        {
            $("#typeofcorps").val('Form-706');
            $("#due_date_1").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');
            $("#due_date_2").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');
            $("#extension_due_date_1").val('<?php echo date("M",strtotime("6 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');
            $("#extension_due_date_2").val('<?php echo date("M",strtotime("6 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');
            $("#type_form_file").val('Form-706');
            $("#type_form_file2").val('');
        }
        else if(typeofcorp == 'Fiduciary' && state=='GA')
        {
           $("#typeofcorps").val('Form-1041');
            $("#due_date_1").val('');
            $("#due_date_2").val('');
            $("#extension_due_date_1").val('');
            $("#extension_due_date_2").val('');
            $("#type_form_file").val('Form-1041');
            $("#type_form_file2").val('Form-501');
        }
        else if(this.value == 'Tax Exempt' && state=='GA')
        {
            $("#typeofcorps").val('Form-990');
            $("#due_date_1").val('<?php echo date("M",strtotime("2 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');
            $("#due_date_2").val('');
            $("#extension_due_date_1").val('');
            $("#extension_due_date_2").val('');
            $("#type_form_file").val('Form-990');
            $("#type_form_file2").val('');
        }
    });
    
    
    $('#form_number_1').on('change', function() 
    {
         if(this.value == '')
        {
            $("#quarter_date").val('')
            $("#federal_frequency11").val('');
            $(".federal_payment_frequency").show();
        }
        else if(this.value == 'Form-941')
        {
            $("#federal_frequency11").val('Quarterly');
            $(".federal_payment_frequency_941").show();
            $(".federal_payment_frequency_944").hide();
            $(".business2").show();
           
        }
        else if(this.value == 'Form-944')
        {
            $("#quarter_date").val('')
            $("#federal_frequency11").val('Annually');
            $(".federal_payment_frequency_944").show();
            $(".federal_payment_frequency_941").hide();
            $("#federal_frequency_due_date").val('<?php echo "Jan".'-'.date("t").'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
            $("#federal_payment_frequency_date2").val('<?php echo "Jan".'-'.date("t").'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
            $("#quarter_date").val('<?php echo date("M",strtotime("9 month",strtotime(date("M")))).'-'."31".'-'.date("Y");?>');
            $(".business2").hide();
        }
    });
    


    $('#federal_payment_frequency').on('change', function() 
    {
        if(this.value == '')
        {
           $(".federal_payment_frequency").show(); 
        }
        else if(this.value == 'Monthly')
        {
            if(this.value == 'Monthly' && 'Jan'=='<?php echo date("M");?>')
            {
                $("#federal_payment_frequency_date2").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');
            }
            else if(this.value == 'Monthly' && 'Feb'=='<?php echo date("M");?>')
            {
                $("#federal_payment_frequency_date2").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');
            }
            else if(this.value == 'Monthly' && 'Mar'=='<?php echo date("M");?>')
            {
                $("#federal_payment_frequency_date2").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');
            }
            else if(this.value == 'Monthly' && 'Apr'=='<?php echo date("M");?>')
            {
                $("#federal_payment_frequency_date2").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');
            }
            else if(this.value == 'Monthly' && 'May'=='<?php echo date("M");?>')
            {
                $("#federal_payment_frequency_date2").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');
            }
            else if(this.value == 'Monthly' && 'Jun'=='<?php echo date("M");?>')
            {
                $("#federal_payment_frequency_date2").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');
            }
            else if(this.value == 'Monthly' && 'Jul'=='<?php echo date("M");?>')
            {
                $("#federal_payment_frequency_date2").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');
            }
            else if(this.value == 'Monthly' && 'Aug'=='<?php echo date("M");?>')
            {
                $("#federal_payment_frequency_date2").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');
            }
            else if(this.value == 'Monthly' && 'Sep'=='<?php echo date("M");?>')
            {
                $("#federal_payment_frequency_date2").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');
            }
            else if(this.value == 'Monthly' && 'Oct'=='<?php echo date("M");?>')
            {
                $("#federal_payment_frequency_date2").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');
            }
            else if(this.value == 'Monthly' && 'Nov'=='<?php echo date("M");?>')
            {
                $("#federal_payment_frequency_date2").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');
            }
            else if(this.value == 'Monthly' && 'Dec'=='<?php echo date("M");?>')
            {
                $("#federal_payment_frequency_date2").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');
            }
        }
        else if(this.value == 'Quarterly')
        {  
            if(this.value == 'Quarterly' && 'Jan'=='<?php echo date("M");?>')
            {
                $("#federal_payment_frequency_date2").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            }
            else if(this.value == 'Quarterly' && 'Feb'=='<?php echo date("M");?>')
            {
                $("#federal_payment_frequency_date2").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            }
            else if(this.value == 'Quarterly' && 'Mar'=='<?php echo date("M");?>')
            {
                $("#federal_payment_frequency_date2").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            }
            else if(this.value == 'Quarterly' && 'Apr'=='<?php echo date("M");?>')
            {
                $("#federal_payment_frequency_date2").val('<?php echo date("M",strtotime("4 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            }
            else if(this.value == 'Quarterly' && 'May'=='<?php echo date("M");?>')
            {
                $("#federal_payment_frequency_date2").val('<?php echo date("M",strtotime("4 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            }
            else if(this.value == 'Quarterly' && 'Jun'=='<?php echo date("M");?>')
            {
                $("#federal_payment_frequency_date2").val('<?php echo date("M",strtotime("4 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            }
            else if(this.value == 'Quarterly' && 'Jul'=='<?php echo date("M");?>')
            {   
                $("#federal_payment_frequency_date2").val('<?php echo date("M",strtotime("7 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            }
            else if(this.value == 'Quarterly' && 'Aug'=='<?php echo date("M");?>')
            {  
                $("#federal_payment_frequency_date2").val('<?php echo date("M",strtotime("7 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            }
            else if(this.value == 'Quarterly' && 'Sep'=='<?php echo date("M");?>')
            {
                $("#federal_payment_frequency_date2").val('<?php echo date("M",strtotime("7 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            }
            else if(this.value == 'Quarterly' && 'Oct'=='<?php echo date("M");?>')
            {
                 $("#federal_payment_frequency_date2").val('<?php echo "Jan".'-'.date("t").'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
            }
            else if(this.value == 'Quarterly' && 'Nov'=='<?php echo date("M");?>')
            {
                 $("#federal_payment_frequency_date2").val('<?php echo "Jan".'-'.date("t").'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
            }
            else if(this.value == 'Quarterly' && 'Dec'=='<?php echo date("M");?>')
            {
                 $("#federal_payment_frequency_date2").val('<?php echo "Jan".'-'.date("t").'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
            }
        }
    });
    
    
    $('#federal_payment_frequency_annually').on('change', function() 
    {
        if(this.value == 'Quarterly')
        {
            if(this.value == 'Quarterly' && 'Jan'=='<?php echo date("M");?>')
            {
                $("#federal_payment_frequency_date2").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            }
            else if(this.value == 'Quarterly' && 'Feb'=='<?php echo date("M");?>')
            {
                $("#federal_payment_frequency_date2").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            }
            else if(this.value == 'Quarterly' && 'Mar'=='<?php echo date("M");?>')
            {
                $("#federal_payment_frequency_date2").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            }
            else if(this.value == 'Quarterly' && 'Apr'=='<?php echo date("M");?>')
            {
                $("#federal_payment_frequency_date2").val('<?php echo date("M",strtotime("4 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            }
            else if(this.value == 'Quarterly' && 'May'=='<?php echo date("M");?>')
            {
                $("#federal_payment_frequency_date2").val('<?php echo date("M",strtotime("4 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            }
            else if(this.value == 'Quarterly' && 'Jun'=='<?php echo date("M");?>')
            {
                $("#federal_payment_frequency_date2").val('<?php echo date("M",strtotime("4 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            }
            else if(this.value == 'Quarterly' && 'Jul'=='<?php echo date("M");?>')
            {   
                $("#federal_payment_frequency_date2").val('<?php echo date("M",strtotime("7 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            }
            else if(this.value == 'Quarterly' && 'Aug'=='<?php echo date("M");?>')
            {  
                $("#federal_payment_frequency_date2").val('<?php echo date("M",strtotime("7 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            }
            else if(this.value == 'Quarterly' && 'Sep'=='<?php echo date("M");?>')
            {
                $("#federal_payment_frequency_date2").val('<?php echo date("M",strtotime("7 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            }
            else if(this.value == 'Quarterly' && 'Oct'=='<?php echo date("M");?>')
            {
                 $("#federal_payment_frequency_date2").val('<?php echo "Jan".'-'.date("t").'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
            }
            else if(this.value == 'Quarterly' && 'Nov'=='<?php echo date("M");?>')
            {
                 $("#federal_payment_frequency_date2").val('<?php echo "Jan".'-'.date("t").'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
            }
            else if(this.value == 'Quarterly' && 'Dec'=='<?php echo date("M");?>')
            {
                 $("#federal_payment_frequency_date2").val('<?php echo "Jan".'-'.date("t").'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
            }
        }
        if(this.value == 'Annually')
        {
            $("#federal_payment_frequency_date2").val('<?php echo "Jan".'-'.date("t").'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
        }
    });

    
    $('#quarter_type').on('change', function() 
    {
        if(this.value == '')
        {
            $("#quarter_date").val('');
            $("#federal_frequency_due_date").val('');
            $("#federal_payment_frequency_date2").val('');
        }
        else if(this.value == '1st Qtr')
        {
            $("#quarter_date").val('<?php echo date("M",strtotime("0 month",strtotime(date("M")))).'-'."31".'-'.date("Y");?>');
            $("#federal_frequency_due_date").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            $("#federal_payment_frequency_date2").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
        }
        else if(this.value == '2nd Qtr')
        {
            $("#quarter_date").val('<?php echo date("M",strtotime("3 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            $("#federal_frequency_due_date").val('<?php echo date("M",strtotime("4 month",strtotime(date("M")))).'-'."31".'-'.date("Y");?>');
            $("#federal_payment_frequency_date2").val('<?php echo date("M",strtotime("4 month",strtotime(date("M")))).'-'."31".'-'.date("Y");?>');
        
        }
        else if(this.value == '3rd Qtr')
        {
            $("#quarter_date").val('<?php echo date("M",strtotime("6 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            $("#federal_frequency_due_date").val('<?php echo date("M",strtotime("7 month",strtotime(date("M")))).'-'."31".'-'.date("Y");?>');
            $("#federal_payment_frequency_date2").val('<?php echo date("M",strtotime("7 month",strtotime(date("M")))).'-'."31".'-'.date("Y");?>');
        
        }
        else if(this.value == '4th Qtr')
        {
            $("#quarter_date").val('<?php echo date("M",strtotime("9 month",strtotime(date("M")))).'-'."31".'-'.date("Y");?>');
            $("#federal_frequency_due_date").val('<?php echo "Jan".'-'.date("t").'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
            $("#federal_payment_frequency_date2").val('<?php echo "Jan".'-'.date("t").'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
          
        }
    });
    
    //State withholding
    
    $('#quarter_type_holding1').on('change', function() 
    {
        if(this.value == '')
        {
            $("#quarter_date_holding").val('');
            $("#federal_frequency_due_date_holding").val('');
            $("#payment_frequency_due_date_holding").val('');
            
        }
        else if(this.value == '1st Qtr')
        {
            $("#quarter_date_holding").val('<?php echo date("M",strtotime("0 month",strtotime(date("M")))).'-'."31".'-'.date("Y");?>');
            $("#federal_frequency_due_date_holding").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            $("#payment_frequency_due_date_holding").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
        }
        else if(this.value == '2nd Qtr')
        {
            $("#quarter_date_holding").val('<?php echo date("M",strtotime("3 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            $("#federal_frequency_due_date_holding").val('<?php echo date("M",strtotime("4 month",strtotime(date("M")))).'-'."31".'-'.date("Y");?>');
            $("#payment_frequency_due_date_holding").val('<?php echo date("M",strtotime("4 month",strtotime(date("M")))).'-'."31".'-'.date("Y");?>');
        
        }
        else if(this.value == '3rd Qtr')
        {
            $("#quarter_date_holding").val('<?php echo date("M",strtotime("6 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            $("#federal_frequency_due_date_holding").val('<?php echo date("M",strtotime("7 month",strtotime(date("M")))).'-'."31".'-'.date("Y");?>');
            $("#payment_frequency_due_date_holding").val('<?php echo date("M",strtotime("7 month",strtotime(date("M")))).'-'."31".'-'.date("Y");?>');
        
        }
        else if(this.value == '4th Qtr')
        {
            $("#quarter_date_holding").val('<?php echo date("M",strtotime("9 month",strtotime(date("M")))).'-'."31".'-'.date("Y");?>');
            $("#federal_frequency_due_date_holding").val('<?php echo "Jan".'-'.date("t").'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
            $("#payment_frequency_due_date_holding").val('<?php echo "Jan".'-'.date("t").'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
        }
    });
    
    
    	
    	$('#payment_frequency_type_holding2').on('change', function() 
        {
        
        if(this.value == 'Monthly')
        {
            if(this.value == 'Monthly' && 'Jan'=='<?php echo date("M");?>')
            {
                $("#payment_frequency_due_date_holding").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');
            }
            else if(this.value == 'Monthly' && 'Feb'=='<?php echo date("M");?>')
            {
                $("#payment_frequency_due_date_holding").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');
            }
            else if(this.value == 'Monthly' && 'Mar'=='<?php echo date("M");?>')
            {
                $("#payment_frequency_due_date_holding").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');
            }
            else if(this.value == 'Monthly' && 'Apr'=='<?php echo date("M");?>')
            {
                $("#payment_frequency_due_date_holding").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');
            }
            else if(this.value == 'Monthly' && 'May'=='<?php echo date("M");?>')
            {
                $("#payment_frequency_due_date_holding").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');
            }
            else if(this.value == 'Monthly' && 'Jun'=='<?php echo date("M");?>')
            {
                $("#payment_frequency_due_date_holding").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');
            }
            else if(this.value == 'Monthly' && 'Jul'=='<?php echo date("M");?>')
            {
                $("#payment_frequency_due_date_holding").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');
            }
            else if(this.value == 'Monthly' && 'Aug'=='<?php echo date("M");?>')
            {
                $("#payment_frequency_due_date_holding").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');
            }
            else if(this.value == 'Monthly' && 'Sep'=='<?php echo date("M");?>')
            {
                $("#payment_frequency_due_date_holding").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');
            }
            else if(this.value == 'Monthly' && 'Oct'=='<?php echo date("M");?>')
            {
                $("#payment_frequency_due_date_holding").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');
            }
            else if(this.value == 'Monthly' && 'Nov'=='<?php echo date("M");?>')
            {
                $("#payment_frequency_due_date_holding").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');
            }
            else if(this.value == 'Monthly' && 'Dec'=='<?php echo date("M");?>')
            {
                $("#payment_frequency_due_date_holding").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');
            }
        }
        else if(this.value == 'Quarterly')
        {  
            if(this.value == 'Quarterly' && 'Jan'=='<?php echo date("M");?>')
            {
                $("#payment_frequency_due_date_holding").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            }
            else if(this.value == 'Quarterly' && 'Feb'=='<?php echo date("M");?>')
            {
                $("#payment_frequency_due_date_holding").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            }
            else if(this.value == 'Quarterly' && 'Mar'=='<?php echo date("M");?>')
            {
                $("#payment_frequency_due_date_holding").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            }
            else if(this.value == 'Quarterly' && 'Apr'=='<?php echo date("M");?>')
            {
                $("#payment_frequency_due_date_holding").val('<?php echo date("M",strtotime("4 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            }
            else if(this.value == 'Quarterly' && 'May'=='<?php echo date("M");?>')
            {
                $("#payment_frequency_due_date_holding").val('<?php echo date("M",strtotime("4 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            }
            else if(this.value == 'Quarterly' && 'Jun'=='<?php echo date("M");?>')
            {
                $("#payment_frequency_due_date_holding").val('<?php echo date("M",strtotime("4 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            }
            else if(this.value == 'Quarterly' && 'Jul'=='<?php echo date("M");?>')
            {   
                $("#payment_frequency_due_date_holding").val('<?php echo date("M",strtotime("7 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            }
            else if(this.value == 'Quarterly' && 'Aug'=='<?php echo date("M");?>')
            {  
                $("#payment_frequency_due_date_holding").val('<?php echo date("M",strtotime("7 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            }
            else if(this.value == 'Quarterly' && 'Sep'=='<?php echo date("M");?>')
            {
                $("#payment_frequency_due_date_holding").val('<?php echo date("M",strtotime("7 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            }
            else if(this.value == 'Quarterly' && 'Oct'=='<?php echo date("M");?>')
            {
                 $("#payment_frequency_due_date_holding").val('<?php echo "Jan".'-'.date("t").'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
            }
            else if(this.value == 'Quarterly' && 'Nov'=='<?php echo date("M");?>')
            {
                 $("#payment_frequency_due_date_holding").val('<?php echo "Jan".'-'.date("t").'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
            }
            else if(this.value == 'Quarterly' && 'Dec'=='<?php echo date("M");?>')
            {
                 $("#payment_frequency_due_date_holding").val('<?php echo "Jan".'-'.date("t").'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
            }
        }
    });
    
    //State Unemployment (SUTA)
   
    $('#quarter_type_unemploy1').on('change', function() 
    {
        if(this.value == '')
        {
            $("#quarter_date_unemploy").val('');
            $("#federal_frequency_due_date_unemploy").val('');
            $("#payment_frequency_due_date_unemploy").val('');
            
        }
        else if(this.value == '1st Qtr')
        {
            $("#quarter_date_unemploy").val('<?php echo date("M",strtotime("0 month",strtotime(date("M")))).'-'."31".'-'.date("Y");?>');
            $("#federal_frequency_due_date_unemploy").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            $("#payment_frequency_due_date_unemploy").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
        }
        else if(this.value == '2nd Qtr')
        {
            $("#quarter_date_unemploy").val('<?php echo date("M",strtotime("3 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            $("#federal_frequency_due_date_unemploy").val('<?php echo date("M",strtotime("4 month",strtotime(date("M")))).'-'."31".'-'.date("Y");?>');
            $("#payment_frequency_due_date_unemploy").val('<?php echo date("M",strtotime("4 month",strtotime(date("M")))).'-'."31".'-'.date("Y");?>');
        
        }
        else if(this.value == '3rd Qtr')
        {
            $("#quarter_date_unemploy").val('<?php echo date("M",strtotime("6 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            $("#federal_frequency_due_date_unemploy").val('<?php echo date("M",strtotime("7 month",strtotime(date("M")))).'-'."31".'-'.date("Y");?>');
            $("#payment_frequency_due_date_unemploy").val('<?php echo date("M",strtotime("7 month",strtotime(date("M")))).'-'."31".'-'.date("Y");?>');
        
        }
        else if(this.value == '4th Qtr')
        {
            $("#quarter_date_unemploy").val('<?php echo date("M",strtotime("9 month",strtotime(date("M")))).'-'."31".'-'.date("Y");?>');
            $("#federal_frequency_due_date_unemploy").val('<?php echo "Jan".'-'.date("t").'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
            $("#payment_frequency_due_date_unemploy").val('<?php echo "Jan".'-'.date("t").'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
        }
    });
    
    
    	
    	$('#payment_frequency_type_unemploy2').on('change', function() 
        {
        
        if(this.value == 'Monthly')
        {
            if(this.value == 'Monthly' && 'Jan'=='<?php echo date("M");?>')
            {
                $("#payment_frequency_due_date_unemploy").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');
            }
            else if(this.value == 'Monthly' && 'Feb'=='<?php echo date("M");?>')
            {
                $("#payment_frequency_due_date_unemploy").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');
            }
            else if(this.value == 'Monthly' && 'Mar'=='<?php echo date("M");?>')
            {
                $("#payment_frequency_due_date_unemploy").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');
            }
            else if(this.value == 'Monthly' && 'Apr'=='<?php echo date("M");?>')
            {
                $("#payment_frequency_due_date_unemploy").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');
            }
            else if(this.value == 'Monthly' && 'May'=='<?php echo date("M");?>')
            {
                $("#payment_frequency_due_date_unemploy").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');
            }
            else if(this.value == 'Monthly' && 'Jun'=='<?php echo date("M");?>')
            {
                $("#payment_frequency_due_date_unemploy").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');
            }
            else if(this.value == 'Monthly' && 'Jul'=='<?php echo date("M");?>')
            {
                $("#payment_frequency_due_date_unemploy").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');
            }
            else if(this.value == 'Monthly' && 'Aug'=='<?php echo date("M");?>')
            {
                $("#payment_frequency_due_date_unemploy").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');
            }
            else if(this.value == 'Monthly' && 'Sep'=='<?php echo date("M");?>')
            {
                $("#payment_frequency_due_date_unemploy").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');
            }
            else if(this.value == 'Monthly' && 'Oct'=='<?php echo date("M");?>')
            {
                $("#payment_frequency_due_date_unemploy").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');
            }
            else if(this.value == 'Monthly' && 'Nov'=='<?php echo date("M");?>')
            {
                $("#payment_frequency_due_date_unemploy").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');
            }
            else if(this.value == 'Monthly' && 'Dec'=='<?php echo date("M");?>')
            {
                $("#payment_frequency_due_date_unemploy").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."15".'-'.date("Y");?>');
            }
        }
        else if(this.value == 'Quarterly')
        {  
            if(this.value == 'Quarterly' && 'Jan'=='<?php echo date("M");?>')
            {
                $("#payment_frequency_due_date_unemploy").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            }
            else if(this.value == 'Quarterly' && 'Feb'=='<?php echo date("M");?>')
            {
                $("#payment_frequency_due_date_unemploy").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            }
            else if(this.value == 'Quarterly' && 'Mar'=='<?php echo date("M");?>')
            {
                $("#payment_frequency_due_date_unemploy").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            }
            else if(this.value == 'Quarterly' && 'Apr'=='<?php echo date("M");?>')
            {
                $("#payment_frequency_due_date_unemploy").val('<?php echo date("M",strtotime("4 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            }
            else if(this.value == 'Quarterly' && 'May'=='<?php echo date("M");?>')
            {
                $("#payment_frequency_due_date_unemploy").val('<?php echo date("M",strtotime("4 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            }
            else if(this.value == 'Quarterly' && 'Jun'=='<?php echo date("M");?>')
            {
                $("#payment_frequency_due_date_unemploy").val('<?php echo date("M",strtotime("4 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            }
            else if(this.value == 'Quarterly' && 'Jul'=='<?php echo date("M");?>')
            {   
                $("#payment_frequency_due_date_unemploy").val('<?php echo date("M",strtotime("7 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            }
            else if(this.value == 'Quarterly' && 'Aug'=='<?php echo date("M");?>')
            {  
                $("#payment_frequency_due_date_unemploy").val('<?php echo date("M",strtotime("7 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            }
            else if(this.value == 'Quarterly' && 'Sep'=='<?php echo date("M");?>')
            {
                $("#payment_frequency_due_date_unemploy").val('<?php echo date("M",strtotime("7 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            }
            else if(this.value == 'Quarterly' && 'Oct'=='<?php echo date("M");?>')
            {
                 $("#payment_frequency_due_date_unemploy").val('<?php echo "Jan".'-'.date("t").'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
            }
            else if(this.value == 'Quarterly' && 'Nov'=='<?php echo date("M");?>')
            {
                 $("#payment_frequency_due_date_unemploy").val('<?php echo "Jan".'-'.date("t").'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
            }
            else if(this.value == 'Quarterly' && 'Dec'=='<?php echo date("M");?>')
            {
                 $("#payment_frequency_due_date_unemploy").val('<?php echo "Jan".'-'.date("t").'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
            }
        }
    });
    	
    	//Federal Unemployment Tax (FUTA)
    	
    $('#frequency').on('change', function() 
    {
        if(this.value == 'Annually')
        {
            $("#frequency_due_date").val('<?php echo "Jan".'-'.date("t").'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
        }
    });
    	
    		
    $('#payment_frequency').on('change', function() 
    {
        if(this.value == 'Quarterly')
        {  
            if(this.value == 'Quarterly' && 'Jan'=='<?php echo date("M");?>')
            {
                $("#payment_frequency_date").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            }
            else if(this.value == 'Quarterly' && 'Feb'=='<?php echo date("M");?>')
            {
                $("#payment_frequency_date").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            }
            else if(this.value == 'Quarterly' && 'Mar'=='<?php echo date("M");?>')
            {
                $("#payment_frequency_date").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            }
            else if(this.value == 'Quarterly' && 'Apr'=='<?php echo date("M");?>')
            {
                $("#payment_frequency_date").val('<?php echo date("M",strtotime("4 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            }
            else if(this.value == 'Quarterly' && 'May'=='<?php echo date("M");?>')
            {
                $("#payment_frequency_date").val('<?php echo date("M",strtotime("4 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            }
            else if(this.value == 'Quarterly' && 'Jun'=='<?php echo date("M");?>')
            {
                $("#payment_frequency_date").val('<?php echo date("M",strtotime("4 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            }
            else if(this.value == 'Quarterly' && 'Jul'=='<?php echo date("M");?>')
            {   
                $("#payment_frequency_date").val('<?php echo date("M",strtotime("7 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            }
            else if(this.value == 'Quarterly' && 'Aug'=='<?php echo date("M");?>')
            {  
                $("#payment_frequency_date").val('<?php echo date("M",strtotime("7 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            }
            else if(this.value == 'Quarterly' && 'Sep'=='<?php echo date("M");?>')
            {
                $("#payment_frequency_date").val('<?php echo date("M",strtotime("7 month",strtotime(date("M")))).'-'."30".'-'.date("Y");?>');
            }
            else if(this.value == 'Quarterly' && 'Oct'=='<?php echo date("M");?>')
            {
                 $("#payment_frequency_date").val('<?php echo "Jan".'-'.date("t").'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
            }
            else if(this.value == 'Quarterly' && 'Nov'=='<?php echo date("M");?>')
            {
                 $("#payment_frequency_date").val('<?php echo "Jan".'-'.date("t").'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
            }
            else if(this.value == 'Quarterly' && 'Dec'=='<?php echo date("M");?>')
            {
                 $("#payment_frequency_date").val('<?php echo "Jan".'-'.date("t").'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
            }
        }
        else if(this.value == 'Annually')
        { 
            $("#payment_frequency_date").val('<?php echo "Jan".'-'.date("t").'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
        }
    });
    
    
    
   
});
</script> 

<div id="myModalview1111" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">View Licence Copy</h4>
         </div>
         <div class="modal-body">
            <div id="viewmodelview111111"></div>
         </div>
         <div class="modal-footer"></div>
      </div>
   </div>
</div>

<div id="myModalview" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel"><span class="pull-left"><?php echo e(Auth::user()->company_name); ?></span><span style="margin-right:10px;" class="pull-right">View Certificate</span></h4>
         </div>
         <div class="modal-body">
            <div id="viewmodelview"></div>
         </div>
         <div class="modal-footer"></div>
      </div>
   </div>
</div>

<div id="myModalviewaoi" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel"><span class="pull-left"><?php echo e(Auth::user()->company_name); ?></span><span style="margin-right:10px;" class="pull-right">View Articles of Incorporation</span></h4>
         </div>
         <div class="modal-body">
            <div id="viewmodelviewaoi"></div>
         </div>
         <div class="modal-footer"></div>
      </div>
   </div>
</div>


 <!-- Modal Third Start-->
    <div class="modal fade" id="myModals33" role="dialog">
                <div class="modal-dialog">
                
                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Update Professional License</h4>
                    </div>
                    <div class="modal-body">
                        
                    
                        
                    
                    <div class="form-group row">
                        <label class="col-md-4 control-label text-right">License Copy</label>
                
                        <div class="col-md-6">
                            <label class="file-upload btn btn-primary">
                            <input type="file" class="form-control" type="file" name="pro_license_copy"/>
                            Browse for file ... </label>
            
                            <input type="hidden"  name="pro_license_copy_2" id="pro_license_copy"/><span id="pro_license_copy_2"></span>
                       </div>
                    </div>
                    
                 
                        
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                  </div>
              
            </div>
        </div>
    <!-- Modal Third End-->


<script>
$.ajaxSetup({headers:{'X-CSRF-Token': $('input[name="_token"]').val()}});
$("#federal_id").mask("99-9999999");
$("#contact_number11").mask("a999999");
$("#motelephoneile").mask("(999) 999-9999");
$(".ext").mask("99339");
$("#business_fax").mask("(999) 999-9999");
$("#contact_number").mask("(999) 999-9999");
$("#mobile").mask("(999) 999-9999");
$("#mobile1").mask("(999) 999-9999");
$(".usapfax").mask("(999) 999-9999");
$("#fax").mask("(999) 999-9999");
$("#contact_fax").mask("(999) 999-9999");
$("#company_mobile").mask("(999) 999-9999");
//  $(".effective_date").mask("00/00/0000");
$("#zip").mask("99999");
$("#company_zip").mask("99999"); 
$(".effective_date1").mask("99/99/9999"); 

$("#company_zip2").mask("99999"); 
$("#motelephoneile2").mask("(999) 999-9999");
$(".ext2").mask("99999");
$("#fax2").mask("(999) 999-9999");

</script> 
<script>
    $(document).ready(function() {
        $(document).on('change', '.category', function() {
            //console.log('htm');
            var id = $(this).val();
            $.get('<?php echo URL::to('getRequest'); ?>?id=' + id,
                function(data) {
                    $('#business_cat_id').empty();
                    $.each(data, function(index, subcatobj) {
                        $('#business_cat_id').append('<option value="' + subcatobj.id + '">' + subcatobj.business_cat_name + '</option>');
                    })
                });
        });
    });
</script>
<script>
    var date = $('#ext1').val();
	$('#telephoneNo1Type').on('change', function() {
        if (this.value == 'Office') {
            document.getElementById('ext1').removeAttribute('readonly');
            $('#ext1').val();
        } else {
            document.getElementById('ext1').readOnly = true;
            $('#ext1').val('');
        }
    });
    $('#tec_telephoneNo1Type').on('change', function() {
        if (this.value == 'Office') {
            document.getElementById('tec_ext').removeAttribute('readonly');
            $('#tec_ext').val();
        } else {
            document.getElementById('tec_ext').readOnly = true;
            $('#tec_ext').val('');
        }
    })
</script>
<script>
    var dat1 = $('#ext2').val();
	$('#mobiletype').on('change', function() {
		if (this.value == 'Home') {
			document.getElementById('ext2').removeAttribute('readonly');
			$('#ext2').val();
		} else {
			document.getElementById('ext2').readOnly = true;
			$('#ext2').val('');
		}
	})
</script>
<script>
	var dat1 = $('#ext21').val();
	$('#mobiletype1').on('change', function() {
		if (this.value == 'Home') {
			document.getElementById('ext21').removeAttribute('readonly');
			$('#ext21').val();
		} else {
			document.getElementById('ext21').readOnly = true;
			$('#ext2').val('');
		}
	})
</script>
<script>
  var $select = $(".agent_position");
$select.on("change", function() {
    var selected = [];  
    $.each($select, function(index, select) {           
        if (select.value !== "") { selected.push(select.value); }
       // alert(select.value);
    });         
   $("option").prop("hidden", false);         
   for (var index in selected) { $('option[value="'+selected[index]+'"]').prop("hidden", true); 
       
   }
});

$(document).ready(function(){

       //group add limit
       var maxGroup = 120;
       count = 1;
       
       var position=["Agent","Secretery","CEO","CFO","CEO/CFO/SEC"];
 	position.sort();
 	$('.agentsposition option:selected').each(function(){
        var index = position.indexOf($(this).val());
        if (index > -1) {
            position.splice(index, 1);
            //alert();
        }
    });
    
    
       $(".btn-success").click(function(){
           count += 1;// alert( count);
            var selectId = $('.agentsposition').length+1;
       
         var aa = $('body').find('.input_fields_wrap_shareholder').length; 
         
         if(count > aa)
         { //alert();
          //$('.input_fields_wrap_shareholder').css('display', 'block');
           if($('body').find('.input_fields_wrap_shareholder').length < maxGroup){
               var fieldHTML = '<div class="input_fields_wrap_shareholder" style="display:block">'+$(".input_fields_wrap_1").html()+'</div>';
               $('body').find('.input_fields_wrap_shareholder:last').after(fieldHTML);
               
                options = [];
            
            $('#agent_position'+selectId).empty();
	        for(i=0;i<position.length;i++){
 	            $('#agent_position'+selectId).append(new Option(position[i],position[i]));   
 	        }
            $('.agentsposition option:selected').each(function(){
                var index = position.indexOf($(this).val());
                if (index > -1) {
                    position.splice(index, 1);
                    //alert();
                }
            });
	       
           }else{
               alert('Maximum '+maxGroup+' Persons are allowed.');
           }
         }
         else
         {
             $('.input_fields_wrap_shareholder').css('display', 'block');
         
         }
       });
       
       //remove fields group
       $("body").on("click",".remove",function(){ 
           $(this).parents(".input_fields_wrap_shareholder").remove();
           checkPercentage();
       });
   });

//     $(document).ready(function() 
//     {
//         var maxGroup = 120;
//         count = 0;
//         $(".addbtn").click(function() 
//         {
//             count += 1; // alert( count);
//             var aa = $('body').find('.input_fields_wrap_shareholder').length;
//             if (count > aa) { //alert();
//                 if ($('body').find('.input_fields_wrap_shareholder').length < maxGroup) {
//                     var fieldHTML = '<div class="input_fields_wrap_shareholder" style="display:block">' + $(".input_fields_wrap_1").html() + '</div>';
//                     $('body').find('.input_fields_wrap_shareholder:last').after(fieldHTML);
//                 var $select = $(".agent_position");
// $select.on("change", function() {
//     var selected = [];  
//     $.each($select, function(index, select) 
//     {           
//         if (select.value !== "") 
//         { var dd = selected.push(select.value); //alert(dd);
//             if(dd=='2' && dd=='3' && dd=='4')
//             {
   
//             }
//         }
//     });         
//   $("option").prop("hidden", false);         
//   for (var index in selected) { $('option[value="'+selected[index]+'"]').prop("hidden", true); }
// });
//                 } else {
//                     alert('Maximum ' + maxGroup + ' Persons are allowed.');
//                 }
//             } else {
//                 $('.input_fields_wrap_shareholder').css('display', 'block');

//             }
//         });

//         //remove fields group
//         $("body").on("click", ".remove", function() {
//           // alert();
//             $(this).parents(".input_fields_wrap_shareholder").remove();
//      //var dd =  $("#agent_position").val(); alert(dd);
//     // alert($('select option:hidden').val();
         
//         });
//     }); 
</script>
<script type="text/javascript">
$(document).on('focus','.effective_date2', function(){
  //$(this).datepicker();
  $(this).removeClass('hasDatepicker').datepicker();
});
</script>
<script>
    $(document).on('keyup', '.federal_id', function() {
       // $('#number_1').val(this.value);
       // $('#number_2').val(this.value);
    })
</script>

<script>
function checkPercentage()
{
     var sum = 0;
       
    $(".numeric1").each(function() {
            sum += +$(this).val().replace("%", "");
            var num = parseFloat($(this).val());
            $(this).val(num.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "%");

            if ($(this).val() == 'NaN%') {
                // alert();
                // $(this).val(parseFloat($(this).val('0')).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")+"%");
                $(this).val('0.00%');

            }
        });
        if (sum > 100) {
            $(".total").val(sum.toFixed(2) + "%");
            $('.btn-primary1').attr('disabled','disabled');
            $('#t1').show();
        } 
        else if (sum < 100) {
            $(".total").val(sum.toFixed(2) + "%");
            $('.btn-primary1').attr('disabled','disabled');
            $('#t1').show();
        } 
        else if (sum = 100) {
            $(".total").val(sum.toFixed(2) + "%");
            $('.btn-primary1').removeAttr('disabled');
            $('#t1').hide();
        } 
        else {
            $(".total").val(sum.toFixed(2) + "%");
            $('.btn-primary1').removeAttr('disabled');
        }

}
    $(document).on("change", ".numeric1", function() {
        checkPercentage();
        //alert();
            });
</script>
<script>
    function FillBilling2(f) {
        if (f.billingtoo2.checked == true) {
            f.contact_fax.value = f.fax.value;
        } else {
            f.contact_fax.value = '';
        }
    } 
</script>
<div id="newmyModalss" class="modal fade " role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="background:#ffff99;border-bottom:5px solid green !important;">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="headings"></h4>
      </div>
      <div class="modal-body">
          testtt
          </div>
          </div>
          </div>
          </div>

<script>
    function FillBilling1(f) {
        
        if (f.Samadd1.checked == true) { 
            
            f.physical_1.value = f.address1.value;
            $('#physical_1').attr('readonly','readonly');
            
            f.physical_2.value = f.address2.value;
            $('#physical_2').attr('readonly','readonly');
            
            f.physical_city.value = f.company_city.value;
            $('#physical_city').attr('readonly','readonly');
            
            f.physical_state.value = f.company_state.value;
            $('#physical_state').attr('disabled','disabled');
            $('#physical_state').attr('name','st');
            
            f.hiddenstate.value = f.company_state.value;
            $('#hiddenstate').attr('name','physical_state');
            
            f.physical_zip.value = f.company_zip.value;
            $('#physical_zip').attr('readonly','readonly');

        } else {
            f.physical_1.value = '';
            $('#physical_1').removeAttr('readonly');
            
            f.physical_2.value = '';
            $('#physical_2').removeAttr('readonly');
            
            f.physical_city.value = '';
            $('#physical_city').removeAttr('readonly');
            
            f.physical_state.value = '';
            $('#physical_state').removeAttr('disabled');
            $('#physical_state').attr('name','physical_state');
            $('#hiddenstate').attr('name','st');
            
            f.physical_zip.value = '';
            $('#physical_zip').removeAttr('readonly');
        }
    } 
    </script> 
    <script>
    function FillBilling(f) {
        var vv = $("#agent_fname1").val();
        var vv1 = $("#agent_mname1").val();
        var vv2 = $("#agent_lname1").val();
        var vv3 = $("#agent_position11").val();
        var vv4 = $("#agent_per11").val();
        var vv5 = $("#effective_date").val(); //alert(vv4)
        var k = $("#agent1").val();
        var k1 = $("#agent2").val();
        var k2 = $("#agent3").val();
        var k3 = $("#agent4").val();
        var k4 = $("#agent5").val();
        var k5 = $("#agent6").val();
        //alert(vv);
        if (f.billingtoo.checked == true) { //alert();
            f.agent_fname2.value = f.agent_fname.value;
            f.agent_mname2.value = f.agent_mname.value;
            f.agent_lname2.value = f.agent_lname.value;
             $('.btn-primary1').attr('disabled','disabled');
            if (vv3 == 'Agent' || k3 == 'Agent') 
            {
                $("#agent_fname1").val(k);
                $("#agent_mname1").val(k1);
                $("#agent_lname1").val(k2);
                $("#agent_position11").val(k3);
                $("#agent_per11").val(k4);
                $("#effective_date").val(k5);
                $("#agent_position11").append("<option selected value='Agent'>Agent</option>");
                 $('#input_fields_wrap_2').css('background', '#438bd6').css('padding', '10px 0 1px 0');
            }
            else
            {
           // $("#agent_position").empty();
             $("#agent_position").find('option[value="Agent"]:first').prop('selected', true);
              $("#agent_position").find('option[value="Agent"]').prop('hidden', true);
            //$("#agent_position").append("<option  <?php if(empty($agent->agent_position)): ?>  <?php else: ?> disabled  <?php endif; ?> value='Agent'>Agent</option><option value='CEO' <?php if(empty($ceo->agent_position)): ?> <?php else: ?>  disabled <?php endif; ?>  <?php if(empty($sec->agent_position)): ?> <?php else: ?>  disabled <?php endif; ?>>CEO</option><option value='CFO' <?php if(empty($cfo->agent_position)): ?> <?php else: ?>   disabled <?php endif; ?>  <?php if(empty($sec->agent_position)): ?> <?php else: ?>  disabled <?php endif; ?>>CFO</option><option value='Secretary' <?php if(empty($sece->agent_position)): ?> <?php else: ?>  disabled <?php endif; ?>  <?php if(empty($sec->agent_position)): ?> <?php else: ?>  disabled <?php endif; ?>>Secretary</option><option value='Sec' <?php if(empty($sec->agent_position)): ?> <?php else: ?>  disabled <?php endif; ?> <?php if((empty($sece->agent_position) && empty($cfo->agent_position) && empty($ceo->agent_position))): ?> <?php else: ?> disabled <?php endif; ?>>CEO / CFO / Sec.</option>");
            $('.input_fields_wrap_1').css('background', '#438bd6').css('padding', '10px 0 1px 0');
            }
        } 
        else { 
            f.agent_fname2.value = '';
            f.agent_mname2.value = '';
            f.agent_lname2.value = '';
            if (vv3 == 'Agent') 
            {
                $("#agent1").val(vv);
                $("#agent2").val(vv1);
                $("#agent3").val(vv2);
                $("#agent4").val(vv3);
                $("#agent5").val(vv4);
                $("#agent6").val(vv5);
                $("#agent_position11").empty();
                $("#agent_fname1").val('');
                $("#agent_per11").val('');
                $("#agent_mname1").val('');
                $("#agent_lname1").val('');
                $("#effective_date").val('');     
                $('#input_fields_wrap_2').css('background', 'transparent').css('padding', '0');
            }
            $("#agent_position").find('option[value="Agent"]:first').prop('selected', false);
                   $("#agent_position").find('option[value="Agent"]').prop('hidden', false);
            //$("#agent_position").empty();
          //  $("#agent_position").append("<option value=''>Position</option><option value='Agent'  <?php if(empty($agent->agent_position)): ?>  <?php else: ?> disabled   <?php endif; ?>>Agent</option><option value='CEO' <?php if(empty($sec->agent_position)): ?>  <?php else: ?> disabled <?php endif; ?> <?php if(empty($ceo->agent_position)): ?>  <?php else: ?> disabled   <?php endif; ?> >CEO</option><option value='CFO' <?php if(empty($sec->agent_position)): ?>  <?php else: ?> disabled <?php endif; ?> <?php if(empty($cfo->agent_position)): ?>  <?php else: ?> disabled   <?php endif; ?>>CFO</option><option value='Secretary' <?php if(empty($secetary->agent_position)): ?>  <?php else: ?> disabled   <?php endif; ?> <?php if(empty($sec->agent_position)): ?>  <?php else: ?> disabled <?php endif; ?>>Secretary</option><option <?php if(empty($sec->agent_position)): ?>  <?php else: ?> disabled <?php endif; ?> value='Sec'   <?php if((empty($sece->agent_position) && empty($cfo->agent_position) && empty($ceo->agent_position))): ?> <?php else: ?> disabled <?php endif; ?>>CEO / CFO / Sec.</option>");
            $('.input_fields_wrap_1').css('background', 'transparent').css('padding', '0');
        }
    } 
</script>

<script>
    $('#state_of_formation').on('change', function() {
        //  alert( this.value ); // or $(this).val()
        if (this.value == "") {
            $('#legal_name1').show();
            $('#legal_name').hide();
        } else {
            $('#legal_name1').hide();
            $('#legal_name').show();
        }
    });
   
</script>


<script>
    $(document).ready(function() {
        $(document).on('change', '.state_of_formation', function() {
            var id = $(this).val();
            $.get('<?php echo URL::to('getcontrol'); ?>?id=' + id,
                function(data) {
                    $('#contact_number1').empty();
                    $.each(data, function(index, subcatobj) {
                        $('#contact_number1').val(subcatobj.controlname);
                    })

                });

        });
    });
</script>

<style>
    .disabled {
        pointer - events: none;
    }
</style>
<script>
    $(document).ready(function() {
        $(document).on('change', '#typeofservice', function() {
            //console.log('htm');
            var id = $(this).val();  //alert(id);
            $.get('<?php echo URL::to('/faderal'); ?>?id='+id, function(data) { //alert();
            $("#typeofcorp").empty(); 
            $("#typeofcorp1").empty(); 
            $("#type_form").empty(); 
            $("#type_formm").empty(); 
            $("#due_date").empty(); 
            //$("#due_datem").empty(); 
            $("#due_date1").empty(); 
            $("#frequency_due_date_1").empty(); 
            $("#quaterly_due_date").empty(); 
            $("#frequency_due_date").empty(); 
            $("#form_authority").empty(); 
            $("#formauthority").empty(); 
           // $("#form_authority_1").empty(); 
            $("#typeofcorp_effect_2").empty(); 
            $("#form_1").empty(); 
            $("#form_2").empty(); 
            $("#form_3").empty(); 
            $("#fedral_state").empty(); 
            //$('.extension_due_date').empty();
            $("#payroll_name").empty(); 
            $("#fedral_state_1").empty(); 
            $("#fedral_state_2").empty(); 
           // $("#federal_name").empty(); 
            //$("#type_form1").empty(); $("#federal_name").empty(); 
                $.each(data, function(index, subcatobj) 
                {                   
                    $('#typeofcorp').val(subcatobj.authority_name);
                    //$('#typeofcorp1').append('<option value="' + subcatobj.formname + '">' + subcatobj.formname + '</option>');
                    $('#typeofcorp1').val(subcatobj.formname);
                    
                    $('#type_form').val(subcatobj.formname);
                    $('#type_formm').val(subcatobj.formname);
                     //$('#type_form').val(subcatobj.telephone);
                    $('#due_date').val(subcatobj.due_date);
                    $('#due_datem1').val(subcatobj.due_date);
                    $('#extension_due_date').val(subcatobj.extension_due_date);
                    $('#extension_due_date1').val(subcatobj.extension_due_date);
                    $('#due_date1').val(subcatobj.due_date);
                    $('#frequency_due_date_1').val(subcatobj.due_date);
                    $('#quaterly_due_date').val(subcatobj.due_date);
                    $('#frequency_due_date').val(subcatobj.due_date);
                    $('#form_authority').val(subcatobj.zip);
                    $('#formauthority').val(subcatobj.zip);
                   // $('#form_authority_1').val(subcatobj.formname);
                    $('#typeofcorp_effect_2').val(subcatobj.due_date);
                    //$('#federal_name').val(subcatobj.payroll_department_name);
                    $('#payroll_name').val(subcatobj.payroll_department_name);
                    $('#fedral_state_1').val(subcatobj.payroll_department_name);
                    $('#fedral_state_2').val(subcatobj.payroll_department_name);
                    $('#federal_form_1').val(subcatobj.typeofform);
                    $('#form_1').val(subcatobj.typeofform);
                    $('#form_2').val(subcatobj.typeofform);
                    $('#form_3').val(subcatobj.typeofform);
                    $('.extension_due_date').val(subcatobj.extension_due_date);
                     $('#form_authority_1').val(subcatobj.formname);
                    //$('#fedral_state').append('<option value="' + subcatobj.city + '">' + subcatobj.city + '</option>');
                   //$('#type_form1').append('<option value="' + subcatobj.city + ' ' + subcatobj.zip + '">' + subcatobj.city + ' ' + subcatobj.zip + '</option>');
                })

            });

        });
    });
</script>

<script>
    $(document).ready(function() {
        $(document).on('change', '#typeofservice', function() {
            //console.log('htm');
            var id = $(this).val(); // alert(id);
            $.get('<?php echo URL::to('/getfaderalsss'); ?>?id='+id, function(data) { //alert();
           $("#due_datem").empty(); 
            $("#form_authority_1").empty(); 
            $('.extension_due_date1').empty();
                $.each(data, function(index, subcatobj) {                   
                    $('#form_authority_1').val(subcatobj.formname_state);
                    $('.extension_due_date1').val(subcatobj.extension_due_date_state);
                    $('#due_datem').val(subcatobj.due_date_state);
                    $('#country_id').append('<option value="' + subcatobj.state +'</option>');
                })

            });

        });
    });
</script>
<script>
    $(document).ready(function() {
        $("#company_zip").keyup(function() {
            //console.log('htm');
            var id = $(this).val();
            $.get('<?php echo URL::to('/getzip'); ?>?zip='+id, function(data) {
                $('#company_state').empty();
                $('company_city').empty();
                $('#company_country').empty();
                $.each(data, function(index, subcatobj) {
                    $('#city').removeAttr("disabled");
                    $('#stateId').removeAttr("disabled");
                    $('#company_city').val(subcatobj.city);
                    $('#company_state').append('<option value="' + subcatobj.state + '">' + subcatobj.state + '</option>');
                    $('#company_country').append('<option value="' + subcatobj.country + '">' + subcatobj.country + '</option>');
                })
            });
        });
    });
</script>
<script>
    $(document).ready(function() {
        $("#physical_zip").keyup(function() {
            //console.log('htm');
            var id = $(this).val();
            $.get('<?php echo URL::to(' / getzip '); ?>?zip=' + id, function(data) {
                $('#physical_state').empty();
                $('physical_city').empty(); //$('#company_country').empty();
                $.each(data, function(index, subcatobj) {
                    $('#physical_city').removeAttr("disabled");
                    $('#physical_state').removeAttr("disabled");
                    $('#physical_city').val(subcatobj.city);
                    $('#physical_state').append('<option value="' + subcatobj.state + '">' + subcatobj.state + '</option>');
                    //$('#company_country').append('<option value="'+subcatobj.country+'">'+subcatobj.country+'</option>');
                })
            });
        });
    });
</script>




<!--<div id="myModal1" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">-->
<!--	<div class="modal-dialog" role="document">-->
<!--		<div class="modal-content">-->
<!--			<div class="modal-header">-->
<!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
<!--				<h4 class="modal-title" id="myModalLabel">PDF File</h4>-->
<!--			</div>-->
<!--			<div class="modal-body">-->
<!--				<?php $__currentLoopData = $upload; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bus): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>-->
<!--				<?php if($bus->upload_name =='SOS Certificate'): ?>-->
<!--				<embed src="<?php echo e(url('public/adminupload')); ?>/<?php echo e($bus->upload); ?>" frameborder="0" width="100%" height="400px">-->
<!--				<?php else: ?>-->
<!--				<h2>No File Uploaded</h2>-->
<!--				<?php endif; ?>-->
<!--				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>-->
<!--			</div>-->
<!--			<div class="modal-footer"></div>-->
<!--		</div>-->
<!--	</div>-->
<!--</div>-->

<!--<div id="myModal2" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">-->
<!--	<div class="modal-dialog" role="document">-->
<!--		<div class="modal-content">-->
<!--			<div class="modal-header">-->
<!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
<!--				<h4 class="modal-title" id="myModalLabel"><span class="col2"></span></h4>-->
<!--			</div>-->
<!--			<div class="modal-body">-->
<!--				<h2>No File Uploaded</h2>-->
<!--			</div>-->
<!--			<div class="modal-footer"></div>-->
<!--		</div>-->
<!--	</div>-->
<!--</div>-->

<script>
	function showDiv(elem){ 
		if(elem.value == 'S Corporation ( Form-1120S)'){ 
		$('#due_date').val('March 15th <?php echo date('Y');?>');
		}
		else if(elem.value == 'C Corporation ( Form-1120)'){ //alert();
		$('#due_date').val('March 15th <?php echo date('Y');?>');
		}
		else{
		document.getElementById('hidden_div').style.display = "block";
		}
	}
</script>
<script>
    $(document).ready(function () {
    $('#corporation').click(function () {
      //  alert();
        if ($(this).is(':checked')) {
             $('.show4').hide();
    
           $('.hidee1').show();
            $('.hide2').show();
           $('.hide3').show();
           $('.hide4').show();
          
           $('.show1').hide();
           $('.show2').hide();
           $('.show3').hide();
             }
        
    });

    $('#corporation1').click(function () {
       // alert();
        if ($(this).is(':checked')) {
            //alert("Transfer Thai Gayo");
            $('.show4').show();
           $('.show1').show();
           $('.show3').show(); 
           $('.show2').show();
           
            $('.hide2').hide();
            $('.hide4').hide();
           $('.hidee1').hide();
           
           $('.hide3').hide();
           
        }
          });
}); 
</script>

<script type="text/javascript">
$(document).ready(function(){
    var corpid=$('#corpid').val();
    
    if(corpid =='Corporation')
    {
           $('.show4').hide();
    
           $('.hidee1').show();
            $('.hide2').show();
           $('.hide3').show();
           $('.hide4').show();
          
           $('.show1').hide();
           $('.show2').hide();
           $('.show3').hide();
          
    }
    else 
    {
           $('.show4').show();
           $('.show1').show();
           $('.show3').show(); 
           $('.show2').show();
           
            $('.hide2').hide();
            $('.hide4').hide();
           $('.hidee1').hide();
           
           $('.hide3').hide();
         
    }
     $(".datepicker").datepicker({ 
        autoclose: true, 
        todayHighlight: true,
        }).datepicker();
    
	$(".effective_date2").datepicker({
		autoclose: true,
        format: "mm/dd/yyyy",
        
		//endDate: "today"
	});
	
	$(".effective_date1").datepicker({
		autoclose: true,
        format: "mm/dd/yyyy",
        minDate:0,
		//endDate: "today"
	}).change(dateChanged).on('changeDate', dateChanged);
	
	function dateChanged(ev) {
        console.log($('.effective_date1').val());
    }
    
	var maxField = 10; //Input fields increment limitation
	var addButton = $('.add_button'); //Add button selector
	var wrapper = $('#professional_tabs_main'); //Input field wrapper
    //var fieldHTML = '<div class="professional_tabs professional_tabs"><div class="professional_tab professional_profession" style="width:160px;"><label>Profession :</label><select type="text" class=" form-control-insu" id="" name="profession[]"><option value="">Select</option><option value="ERO">ERO</option><option value="CPA">CPA</option><option value="Mortgage Broker">Mortgage Broker</option><option value="Insurance Agency">Insurance Agency</option><option value="PTIN">PTIN</option><option value="Insurance Agent">Insurance Agent</option><option value="Insurance Broker">Insurance Broker</option><option value="MLO">MLO</option></select></div><div class="professional_tab professional_state" style="width:6.5% !Important;"><label>State :</label><select name="profession_state[]" id="profession_state" class="form-control-insu"><option value="AK">AK</option><option value="AS">AS</option><option value="AZ">AZ</option><option value="AR">AR</option><option value="CA">CA</option><option value="CO">CO</option><option value="CT">CT</option><option value="DE">DE</option><option value="DC">DC</option><option value="FM">FM</option><option value="FL">FL</option><option value="GA">GA</option><option value="GU">GU</option><option value="HI">HI</option><option value="ID">ID</option><option value="IL">IL</option><option value="IN">IN</option><option value="IA">IA</option><option value="KS">KS</option><option value="KY">KY</option><option value="LA">LA</option><option value="ME">ME</option><option value="MH">MH</option><option value="MD">MD</option><option value="MA">MA</option><option value="MI">MI</option><option value="MN">MN</option><option value="MS">MS</option><option value="MO">MO</option><option value="MT">MT</option><option value="NE">NE</option><option value="NV">NV</option><option value="NH">NH</option><option value="NJ">NJ</option><option value="NM">NM</option><option value="NY">NY</option><option value="NC">NC</option><option value="ND">ND</option><option value="MP">MP</option><option value="OH">OH</option><option value="OK">OK</option><option value="OR">OR</option><option value="PW">PW</option><option value="PA">PA</option><option value="PR">PR</option><option value="RI">RI</option><option value="SC">SC</option><option value="SD">SD</option><option value="TN">TN</option><option value="TX">TX</option><option value="UT">UT</option><option value="VT">VT</option><option value="VI">VI</option><option value="VA">VA</option><option value="WA">WA</option><option value="WV">WV</option><option value="WI">WI</option><option value="WY">WY</option></select></div><div class="professional_tab professional_profession" style="width:100px;"><label>Type :</label><select class="form-control-insu addce_'+x+'" id="professiontype" name="professiontype[]"><option value="">Select</option><option value="Firm">Firm</option><option value="Individual">Individual</option></select></div><div class="professional_tab professional_effective" style="width:12% !Important;"><label>Effective Date :</label><input name="profession_effective_date[]" placeholder="Effective Date" value="" type="text" id="profession_effective_date" class="form-control-insu effective_date1"></div><div class="professional_tab professional_license" style="width:12% !Important;"><label>License # :</label><input name="profession_license[]" placeholder="License" value="" type="text" id="profession_license" class="form-control-insu"></div><div class="professional_tab professional_expire" style="width:12% !Important;"><label>Expire Date :</label><input name="profession_epr_date[]" placeholder="Expire Date" value="" type="text" id="profession_epr_date" class="form-control-insu datepicker"></div><div class="professional_tab profession_priority" style="width:10.5% !Important;"><label>License Period:</label><select type="text" class="form-control-insu" id="profession_priority" name="profession_priority[]"><option value="">Select</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option></select></div><div class="professional_add"><a href="javascript:void(0);" id="remove_button_pro" class="btn_new btn-remove"><i class="fa fa-minus" aria-hidden="true"></i></a></div><div class="professional_tab professional_note"><label>Note :</label><input name="profession_note[]" placeholder="Note" value="" type="text" id="profession_note" class="form-control-insu"><input name="profession_id[]" placeholder="Expire Date" value="" type="hidden" id="" class="form-control-insu"></div><div class="professional_tab professional_state hidece_'+x+'"><input name="ce[]"  type="checkbox" id="ce" value="1"/><label for="ce">CE Check</label></div><div class="professional_tab professional_effective_1"><label></label><a data-toggle="modal-history" num="" class="btn_new btn-view-license openBtn1 Pro-btn">License History</a></div><div class="professional_tab professional_license_1"><label></label><a data-toggle="modal" num="1" class="btn_new btn-view-license">View License</a></div><div class="professional_tab professional_expire_1"><label></label><a href="javascript:void(0);" class="btn_new btn-renew">Renew Now</a></div></div>'; //New input field html 	
    
// 	var fieldHTML = '<div class="professional_tabs professional_tabs"><div class="professional_tab professional_profession" style="width:160px;"><label>Profession :</label><select type="text" class=" form-control-insu" id="" name="profession[]"><option value="">Select</option><option value="ERO">ERO</option><option value="CPA">CPA</option><option value="Mortgage Broker">Mortgage Broker</option><option value="Insurance Agency">Insurance Agency</option><option value="PTIN">PTIN</option><option value="Insurance Agent">Insurance Agent</option><option value="Insurance Broker">Insurance Broker</option><option value="MLO">MLO</option></select></div><div class="professional_tab professional_state"><label>State :</label><select name="profession_state[]" id="profession_state" class="form-control-insu"><option value="AK">AK</option><option value="AS">AS</option><option value="AZ">AZ</option><option value="AR">AR</option><option value="CA">CA</option><option value="CO">CO</option><option value="CT">CT</option><option value="DE">DE</option><option value="DC">DC</option><option value="FM">FM</option><option value="FL">FL</option><option value="GA">GA</option><option value="GU">GU</option><option value="HI">HI</option><option value="ID">ID</option><option value="IL">IL</option><option value="IN">IN</option><option value="IA">IA</option><option value="KS">KS</option><option value="KY">KY</option><option value="LA">LA</option><option value="ME">ME</option><option value="MH">MH</option><option value="MD">MD</option><option value="MA">MA</option><option value="MI">MI</option><option value="MN">MN</option><option value="MS">MS</option><option value="MO">MO</option><option value="MT">MT</option><option value="NE">NE</option><option value="NV">NV</option><option value="NH">NH</option><option value="NJ">NJ</option><option value="NM">NM</option><option value="NY">NY</option><option value="NC">NC</option><option value="ND">ND</option><option value="MP">MP</option><option value="OH">OH</option><option value="OK">OK</option><option value="OR">OR</option><option value="PW">PW</option><option value="PA">PA</option><option value="PR">PR</option><option value="RI">RI</option><option value="SC">SC</option><option value="SD">SD</option><option value="TN">TN</option><option value="TX">TX</option><option value="UT">UT</option><option value="VT">VT</option><option value="VI">VI</option><option value="VA">VA</option><option value="WA">WA</option><option value="WV">WV</option><option value="WI">WI</option><option value="WY">WY</option></select></div><div class="professional_tab professional_profession" style="width:100px;"><label>Type :</label><select type="text" class="form-control-insu" id="professiontype" name="professiontype[]"><option value="">Select</option><option value="Firm">Firm</option><option value="Individual">Individual</option></select></div><div class="professional_tab professional_effective"><label>Effective Date :</label><input name="profession_effective_date[]" placeholder="Effective Date" value="" type="text" id="profession_effective_date" class="form-control-insu effective_date1"></div><div class="professional_tab professional_license"><label>License # :</label><input name="profession_license[]" placeholder="License" value="" type="text" id="profession_license" class="form-control-insu"></div><div class="professional_tab professional_expire"><label>Expire Date :</label><input name="profession_epr_date[]" placeholder="Expire Date" value="" type="text" id="profession_epr_date" class="form-control-insu"></div><div class="professional_add"><a href="javascript:void(0);" id="remove_button_pro" class="btn_new btn-remove"><i class="fa fa-minus" aria-hidden="true"></i></a></div><div class="professional_tab professional_note"><label>Note :</label><input name="profession_note[]" placeholder="Note" value="" type="text" id="profession_note" class="form-control-insu"><input name="profession_id[]" placeholder="Expire Date" value="" type="hidden" id="" class="form-control-insu"></div><div class="professional_tab professional_state"><label>CE :</label<input name="ce[]"  type="checkbox" id="ce_6" value="1"/><label for="ce_6">Check</label></div><div class="professional_tab professional_effective_1"><label></label><a data-toggle="modal-history" num="" class="btn_new btn-view-license openBtn1 Pro-btn">License History</a></div><div class="professional_tab professional_license_1"><label></label><a data-toggle="modal" num="1" class="btn_new btn-view-license">View License</a></div><div class="professional_tab professional_expire_1"><label></label><a href="javascript:void(0);" class="btn_new btn-renew">Renew Now</a></div></div>'; //New input field html 
	var x = 1; //Initial field counter is 1

    <?php
    if($pro>0)
    {
        ?>
        var x='<?php echo $pro;?>';
        <?php
    }
    else
    {
        ?>
       var x=0;
        <?php
    }
    ?>
	$(addButton).click(function()
	{ //Once add button is clicked
		if(x < maxField){ //Check maximum number of input fields
		x++; //Increment field counter
		$(wrapper).append(fieldHTML); // Add field html
		
		}
	});
	
	$(wrapper).on('click', '#add_button_pro', function(e){ //Once remove button is clicked
		e.preventDefault();
		x++;

// 	 	$(wrapper).append('<div class="professional_tabs professional_tabs"><div class="professional_tab professional_profession" style="width:160px;"><label>Profession :</label><select type="text" class=" form-control-insu profession'+x+'" id="profession'+x+'" name="profession[]"><option value="">Select</option><option value="ERO">ERO</option><option value="CPA">CPA</option><option value="Mortgage Broker">Mortgage Broker</option><option value="Insurance Agency">Insurance Agency</option><option value="PTIN">PTIN</option><option value="Insurance Agent">Insurance Agent</option><option value="Insurance Broker">Insurance Broker</option><option value="MLO">MLO</option></select></div><div class="professional_tab professional_state" style="width:6.5% !Important;"><label>State :</label><select name="profession_state[]" id="profession_state" class="form-control-insu profession_state'+x+'"><option value="AK">AK</option><option value="AS">AS</option><option value="AZ">AZ</option><option value="AR">AR</option><option value="CA">CA</option><option value="CO">CO</option><option value="CT">CT</option><option value="DE">DE</option><option value="DC">DC</option><option value="FM">FM</option><option value="FL">FL</option><option value="GA">GA</option><option value="GU">GU</option><option value="HI">HI</option><option value="ID">ID</option><option value="IL">IL</option><option value="IN">IN</option><option value="IA">IA</option><option value="KS">KS</option><option value="KY">KY</option><option value="LA">LA</option><option value="ME">ME</option><option value="MH">MH</option><option value="MD">MD</option><option value="MA">MA</option><option value="MI">MI</option><option value="MN">MN</option><option value="MS">MS</option><option value="MO">MO</option><option value="MT">MT</option><option value="NE">NE</option><option value="NV">NV</option><option value="NH">NH</option><option value="NJ">NJ</option><option value="NM">NM</option><option value="NY">NY</option><option value="NC">NC</option><option value="ND">ND</option><option value="MP">MP</option><option value="OH">OH</option><option value="OK">OK</option><option value="OR">OR</option><option value="PW">PW</option><option value="PA">PA</option><option value="PR">PR</option><option value="RI">RI</option><option value="SC">SC</option><option value="SD">SD</option><option value="TN">TN</option><option value="TX">TX</option><option value="UT">UT</option><option value="VT">VT</option><option value="VI">VI</option><option value="VA">VA</option><option value="WA">WA</option><option value="WV">WV</option><option value="WI">WI</option><option value="WY">WY</option></select></div><div class="professional_tab professional_profession" style="width:100px;"><label>Type :</label><select class="form-control-insu addce_'+x+' professiontype'+x+'" id="professiontype" name="professiontype[]"><option value="">Select</option><option value="Firm">Firm</option><option value="Individual">Individual</option></select></div><div class="professional_tab professional_effective" style="width:12% !Important;"><label>Effective Date :</label><input name="profession_effective_date[]" placeholder="Effective Date" value="" type="text" id="profession_effective_date" class="form-control-insu effective_date1"></div><div class="professional_tab professional_license" style="width:12% !Important;"><label>License # :</label><input name="profession_license[]" placeholder="License" value="" type="text" id="profession_license" class="form-control-insu"></div><div class="professional_tab professional_expire" style="width:12% !Important;"><label>Expire Date :</label><input name="profession_exp_date[]" placeholder="Expire Date"  type="text" id="profession_epr_date" class="form-control-insu effective_date1 profession_exp_date'+x+'"></div><div class="professional_tab profession_priority" style="width:10.5% !Important;"><label>License Period:</label><input type="text" class="form-control-insu" id="profession_priority2'+x+'" name="profession_priority2[]" value="2" style="display:none;" readonly><select type="text" class="form-control-insu" id="profession_priority'+x+'" name="profession_priority[]"><option value="">Select</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option></select></div><div class="professional_add"><a href="javascript:void(0);" id="remove_button_pro" class="btn_new btn-remove"><i class="fa fa-minus" aria-hidden="true"></i></a></div><div class="professional_tab professional_note"><label>Note :</label><input name="profession_note[]" placeholder="Note" value="" type="text" id="profession_note" class="form-control-insu"><input name="profession_id[]" placeholder="Expire Date" value="" type="hidden" id="" class="form-control-insu"></div><div style="display:none;" class="professional_tab professional_state hidece_'+x+'"><label>CE Check</label><input type="checkbox" name="ce[]" id="ce_'+x+'" value="1"/><label for="ce_'+x+'" style="margin-top: 27px;position:absolute;"></label></div></div>');

        $(wrapper).append('<div class="professional_tabs professional_tabs"><div class="professional_tab professional_profession"><label>Profession :</label><span class="d_flex"> <select type="text" class=" form-control-insu profession_option profession'+x+'" id="profession'+x+'" name="profession[]"> <option value="">Select</option> <?php $__currentLoopData = $admin_professionlist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cur): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?><option value="<?php echo e($cur->name); ?>"><?php echo e($cur->name); ?></option> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </select>&nbsp;<a href="#" class="btn btn-primary" data-toggle="modal" data-target="#basicExampleModal"><i class="fa fa-plus v_middle"></i></a>&nbsp;<a href="#" class="btn btn-primary" data-toggle="modal" data-target="#basicExampleModal3"><i class="fa fa-minus v_middle"></i></a></span></div><div class="professional_tab professional_state" style=""><label>State :</label><select name="profession_state[]" id="profession_state" class="form-control-insu profession_state'+x+'"><option value="AK">AK</option><option value="AS">AS</option><option value="AZ">AZ</option><option value="AR">AR</option><option value="CA">CA</option><option value="CO">CO</option><option value="CT">CT</option><option value="DE">DE</option><option value="DC">DC</option><option value="FM">FM</option><option value="FL">FL</option><option value="GA">GA</option><option value="GU">GU</option><option value="HI">HI</option><option value="ID">ID</option><option value="IL">IL</option><option value="IN">IN</option><option value="IA">IA</option><option value="KS">KS</option><option value="KY">KY</option><option value="LA">LA</option><option value="ME">ME</option><option value="MH">MH</option><option value="MD">MD</option><option value="MA">MA</option><option value="MI">MI</option><option value="MN">MN</option><option value="MS">MS</option><option value="MO">MO</option><option value="MT">MT</option><option value="NE">NE</option><option value="NV">NV</option><option value="NH">NH</option><option value="NJ">NJ</option><option value="NM">NM</option><option value="NY">NY</option><option value="NC">NC</option><option value="ND">ND</option><option value="MP">MP</option><option value="OH">OH</option><option value="OK">OK</option><option value="OR">OR</option><option value="PW">PW</option><option value="PA">PA</option><option value="PR">PR</option><option value="RI">RI</option><option value="SC">SC</option><option value="SD">SD</option><option value="TN">TN</option><option value="TX">TX</option><option value="UT">UT</option><option value="VT">VT</option><option value="VI">VI</option><option value="VA">VA</option><option value="WA">WA</option><option value="WV">WV</option><option value="WI">WI</option><option value="WY">WY</option></select></div><div class="professional_tab professional_effective"><label>Type :</label><select class="form-control-insu addce_'+x+' professiontype'+x+'" id="professiontype" name="professiontype[]"><option value="">Select</option><option value="Firm">Firm</option><option value="Individual">Individual</option></select></div><div class="professional_tab professional_effective" ><label>Effective Date :</label><input name="profession_effective_date[]" placeholder="Effective Date" value="" type="text" id="profession_effective_date" class="form-control-insu effective_date1"></div><div class="professional_tab professional_license"><label>License # :</label><input name="profession_license[]" placeholder="License" value="" type="text" id="profession_license" class="form-control-insu profession_license'+x+'"></div><div class="professional_tab professional_expire"><label>Expire Date :</label><input name="profession_exp_date[]" placeholder="Expire Date"  type="text" id="profession_epr_date" class="form-control-insu effective_date1 profession_exp_date'+x+'"></div><div class="professional_tab professional_expire"><label>License Period:</label><input type="text" class="form-control-insu profession_priority1'+x+'" id="profession_priority'+x+'" name="profession_priority1[]" value="1" readonly style="display:none;"><input type="text" class="form-control-insu profession_priority2'+x+'" id="profession_priority2'+x+'" name="profession_priority2[]" value="2" readonly style="display:none;"><select class="form-control-insu profession_priority3'+x+'" id="profession_priority'+x+'" name="profession_priority3[]"><option value="">Select</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option></select></div><div class="professional_tab professional_profession professional_note"><label>Note :</label><input name="profession_note[]" placeholder="Note" value="" type="text" id="profession_note" class="form-control-insu"></div><div style="display:none;" class="professional_tab professional_state hidece_'+x+'"><label>CE Check</label><input type="checkbox" name="ce[]" id="ce_'+x+'" value="1"/><label for="ce_'+x+'" style="margin-top: 27px;position:absolute;"></label></div><div class="professional_add" style="width:auto;"><a href="javascript:void(0);" id="remove_button_pro" class="btn_new btn-remove" style="padding:6px 12px !important;"><i class="fa fa-minus" aria-hidden="true"></i></a></div></div>');
	 	
	 	$('.profession'+x+'').on("change", function()
        {
            var prof=$('.profession'+x+'').val();
	    	var profstate=$('.profession_state'+x+'').val();
	    	var proftype=$('.professiontype'+x+'').val();
	    	//var proflic=$('.profession_license'+x+'').val();
	    	
	    	var id4='-';
            var fullids=prof.concat(id4).concat(profstate).concat(id4).concat(proftype);
    	    //alert(fullids);
	    	//console.log(fullids);           
            $.get('<?php echo URL::to('getAdminlic'); ?>?filename='+fullids, function(data)
            {  
                //alert(data);
                //console.log(data);exit;
                if(data==1)
                {
                    //recInsert,recExist
                    alert('Record Already Exist!');
                    $('#recExist').show();
                    $(".recExist").show();
                    $("#recInsert").hide();
                     
                }
                else
                {
                    $('#recExist').hide();
                    $(".recExist").hide();
                    $("#recInsert").show();
                }
            });

	    	
	    	
	    	if(prof == 'CPA' && profstate == 'GA' && proftype =='Individual')
	    	{
	    	    $('.profession_exp_date'+x+'').val('12/31');
	    	    $('.profession_priority1'+x+'').hide();
	    	    $('.profession_priority2'+x+'').show();
	    	    $('.profession_priority3'+x+'').hide();
	    	    
	    	}
	    	else if(prof == 'CPA' && profstate == 'GA' && proftype =='Firm')
	    	{
	    	    $('.profession_exp_date'+x+'').val('06/30');
	    	    //$('.profession_priority'+x+'').val('2');
	    	    $('.profession_priority1'+x+'').hide();
	    	    $('.profession_priority2'+x+'').show();
	    	    $('.profession_priority3'+x+'').hide();
	    	   
	    	}
	    	else if(prof == 'Mortgage Broker' && profstate == 'GA' && proftype =='Firm')
	    	{
	    	    $('.profession_exp_date'+x+'').val('12/30');
	    	    //$('.profession_priority'+x+'').val('1');
	    	    $('.profession_priority1'+x+'').show();
	    	    $('.profession_priority2'+x+'').hide();
	    	    $('.profession_priority3'+x+'').hide();
	    	}
	    	else if(prof == 'MLO' && profstate == 'GA' && proftype =='Individual')
	    	{
	    	    $('.profession_exp_date'+x+'').val('12/31');
	    	    //$('.profession_priority'+x+'').val('1');
	    	    $('.profession_priority1'+x+'').show();
	    	    $('.profession_priority2'+x+'').hide();
	    	    $('.profession_priority3'+x+'').hide();
	    	}
	    	else if(prof == 'Insurance Broker' && profstate == 'GA' && proftype =='Firm')
	    	{
	    	    $('.profession_exp_date'+x+'').val('12/31');
	    	    //$('.profession_priority'+x+'').val('2');
	    	    $('.profession_priority1'+x+'').hide();
	    	    $('.profession_priority2'+x+'').show();
	    	    $('.profession_priority3'+x+'').hide();
	    	}
	    	else if(prof == 'Insurance Agent' && profstate == 'GA' && proftype =='Individual')
	    	{
	    	    $('.profession_exp_date'+x+'').val('12/31');
	    	    //$('.profession_priority'+x+'').val('2');
	    	    $('.profession_priority1'+x+'').hide();
	    	    $('.profession_priority2'+x+'').show();
	    	    $('.profession_priority3'+x+'').hide();
	    	}
	    	
	    	
        });
        
        $('.profession_state'+x+'').on("change", function()
        {
            var prof=$('.profession'+x+'').val();
	    	var profstate=$('.profession_state'+x+'').val();
	    	var proftype=$('.professiontype'+x+'').val();
	    	//var proflic=$('.profession_license'+x+'').val();
	    	
	    	var id4='-';
            var fullids=prof.concat(id4).concat(profstate).concat(id4).concat(proftype);
    	    //alert(fullids);
	    	//console.log(fullids);           
            $.get('<?php echo URL::to('getAdminlic'); ?>?filename='+fullids, function(data)
            {  
                //alert(data);
                //console.log(data);exit;
                if(data==1)
                {
                    //recInsert,recExist
                    alert('Record Already Exist!');
                    $('#recExist').show();
                    $(".recExist").show();
                    $("#recInsert").hide();
                     
                }
                else
                {
                    $('#recExist').hide();
                    $(".recExist").hide();
                    $("#recInsert").show();
                }
            });

	    	
	    	
	    	if(prof == 'CPA' && profstate == 'GA' && proftype =='Individual')
	    	{
	    	    $('.profession_exp_date'+x+'').val('12/31');
	    	    $('.profession_priority1'+x+'').hide();
	    	    $('.profession_priority2'+x+'').show();
	    	    $('.profession_priority3'+x+'').hide();
	    	    
	    	}
	    	else if(prof == 'CPA' && profstate == 'GA' && proftype =='Firm')
	    	{
	    	    $('.profession_exp_date'+x+'').val('06/30');
	    	    //$('.profession_priority'+x+'').val('2');
	    	    $('.profession_priority1'+x+'').hide();
	    	    $('.profession_priority2'+x+'').show();
	    	    $('.profession_priority3'+x+'').hide();
	    	   
	    	}
	    	else if(prof == 'Mortgage Broker' && profstate == 'GA' && proftype =='Firm')
	    	{
	    	    $('.profession_exp_date'+x+'').val('12/30');
	    	    //$('.profession_priority'+x+'').val('1');
	    	    $('.profession_priority1'+x+'').show();
	    	    $('.profession_priority2'+x+'').hide();
	    	    $('.profession_priority3'+x+'').hide();
	    	}
	    	else if(prof == 'MLO' && profstate == 'GA' && proftype =='Individual')
	    	{
	    	    $('.profession_exp_date'+x+'').val('12/31');
	    	    //$('.profession_priority'+x+'').val('1');
	    	    $('.profession_priority1'+x+'').show();
	    	    $('.profession_priority2'+x+'').hide();
	    	    $('.profession_priority3'+x+'').hide();
	    	}
	    	else if(prof == 'Insurance Broker' && profstate == 'GA' && proftype =='Firm')
	    	{
	    	    $('.profession_exp_date'+x+'').val('12/31');
	    	    //$('.profession_priority'+x+'').val('2');
	    	    $('.profession_priority1'+x+'').hide();
	    	    $('.profession_priority2'+x+'').show();
	    	    $('.profession_priority3'+x+'').hide();
	    	}
	    	else if(prof == 'Insurance Agent' && profstate == 'GA' && proftype =='Individual')
	    	{
	    	    $('.profession_exp_date'+x+'').val('12/31');
	    	    //$('.profession_priority'+x+'').val('2');
	    	    $('.profession_priority1'+x+'').hide();
	    	    $('.profession_priority2'+x+'').show();
	    	    $('.profession_priority3'+x+'').hide();
	    	}
	    	
	    	
        });
	 	
	 	
 	 	$('.professiontype'+x+'').on("change", function()
        //$('.profession_license'+x+'').on("blur", function()
        {
            var prof=$('.profession'+x+'').val();
	    	var profstate=$('.profession_state'+x+'').val();
	    	var proftype=$('.professiontype'+x+'').val();
	    	//var proflic=$('.profession_license'+x+'').val();
	    	
	    	var id4='-';
            var fullids=prof.concat(id4).concat(profstate).concat(id4).concat(proftype);
    	    //alert(fullids);
	    	//console.log(fullids);           
            $.get('<?php echo URL::to('getAdminlic'); ?>?filename='+fullids, function(data)
            {  
                //alert(data);
                //console.log(data);exit;
                if(data ==1)
                {
                    //recInsert,recExist
                    alert('Record Already Exist!');
                    $('#recExist').show();
                    $(".recExist").show();
                    $("#recInsert").hide();
                     
                }
                else
                {
                    $('#recExist').hide();
                    $(".recExist").hide();
                    $("#recInsert").show();
                }
            });

	    	
	    	
	    	if(prof == 'CPA' && profstate == 'GA' && proftype =='Individual')
	    	{
	    	    $('.profession_exp_date'+x+'').val('12/31');
	    	    $('.profession_priority1'+x+'').hide();
	    	    $('.profession_priority2'+x+'').show();
	    	    $('.profession_priority3'+x+'').hide();
	    	    
	    	}
	    	else if(prof == 'CPA' && profstate == 'GA' && proftype =='Firm')
	    	{
	    	    $('.profession_exp_date'+x+'').val('06/30');
	    	    //$('.profession_priority'+x+'').val('2');
	    	    $('.profession_priority1'+x+'').hide();
	    	    $('.profession_priority2'+x+'').show();
	    	    $('.profession_priority3'+x+'').hide();
	    	   
	    	}
	    	else if(prof == 'Mortgage Broker' && profstate == 'GA' && proftype =='Firm')
	    	{
	    	    $('.profession_exp_date'+x+'').val('12/30');
	    	    //$('.profession_priority'+x+'').val('1');
	    	    $('.profession_priority1'+x+'').show();
	    	    $('.profession_priority2'+x+'').hide();
	    	    $('.profession_priority3'+x+'').hide();
	    	}
	    	else if(prof == 'MLO' && profstate == 'GA' && proftype =='Individual')
	    	{
	    	    $('.profession_exp_date'+x+'').val('12/31');
	    	    //$('.profession_priority'+x+'').val('1');
	    	    $('.profession_priority1'+x+'').show();
	    	    $('.profession_priority2'+x+'').hide();
	    	    $('.profession_priority3'+x+'').hide();
	    	}
	    	else if(prof == 'Insurance Broker' && profstate == 'GA' && proftype =='Firm')
	    	{
	    	    $('.profession_exp_date'+x+'').val('12/31');
	    	    //$('.profession_priority'+x+'').val('2');
	    	    $('.profession_priority1'+x+'').hide();
	    	    $('.profession_priority2'+x+'').show();
	    	    $('.profession_priority3'+x+'').hide();
	    	}
	    	else if(prof == 'Insurance Agent' && profstate == 'GA' && proftype =='Individual')
	    	{
	    	    $('.profession_exp_date'+x+'').val('12/31');
	    	    //$('.profession_priority'+x+'').val('2');
	    	    $('.profession_priority1'+x+'').hide();
	    	    $('.profession_priority2'+x+'').show();
	    	    $('.profession_priority3'+x+'').hide();
	    	}
	    	
	    	
        });
	 	
	 	//  profession
		//  profession_priority2
		//  profession_priority
		  
		$('.addce_'+x+'').change(function () 
		{
		    var prof=$('#profession'+x+'').val();
	    	var thiss = $('.addce_'+x+'').val();
	    //	alert(prof);
	    	if(thiss == 'Individual')
	    	{
	    	    $('.hidece_'+x+'').show();
	    	  //  $('#profession_priority2'+x+'').show();
	    	  //  $('#profession_priority'+x+'').hide();
	    	}
	    	else if(thiss == 'Firm')
	    	{
	    	    $('.hidece_'+x+'').hide();
	    	  //  $('#profession_priority2'+x+'').hide();
	    	  //  $('#profession_priority'+x+'').show();
	    	}
	    	
	    	if(thiss == 'Individual' && prof == 'CPA')
	    	{
	    	    $('#profession_priority2'+x+'').show();
	    	    $('#profession_priority'+x+'').hide();
	    	}
	    	else
	    	{
	    	    $('#profession_priority2'+x+'').hide();
	    	    $('#profession_priority'+x+'').show();
	    	}

		});
		
		$(".effective_date1").datepicker({
		    autoclose: true,
            format: "mm/dd/yyyy",
            endDate: "today"
		});
		
	

$(document).ready(function(){

  
   /* $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        
        var currentTab = $(e.target).text(); // get current tab
        var vf = $('#text1').val();//alert(vf);
        var current_tab = e.target;
        var previousTab = $(e.relatedTarget).text(); 
        var target = $(e.relatedTarget).attr("href");
        $('.no-button').eq(0).attr('href',target);
        var href = $('.no-button').attr('href');
     if(target==href){ 
        if(vf)
        {
             alert('2');
        // $("#alerts").modal({
		//show: true,
	//});
           
        } 
        else
        {
            
        }
     }
     else
     { 
     }
     
    });
*/});

$(function() {

    $('#login-form-link').click(function(e) {
		$("#login-form").delay(100).fadeIn(100);
 		$("#register-form").fadeOut(100);
		$('#register-form-link').removeClass('active');
		$(this).addClass('active');
		e.preventDefault();
	});
	$('#register-form-link').click(function(e) {
		$("#register-form").delay(100).fadeIn(100);
 		$("#login-form").fadeOut(100);
		$('#login-form-link').removeClass('active');
		$(this).addClass('active');
		e.preventDefault();
	});

});


$(document).on('click', '.no-button', function () {
    var href = $(this).attr('href');
     var $link = $('li.active a[data-toggle="tab"]');
    $link.parent().removeClass('active');
    var tabLink = $link.attr('href');//alert(tabLink);
     $('#alerts').modal('hide');
    $('#myTab a[href="' + href + '"]').tab('show');
      
});
 $('#ce_check').click(function() {// alert();
																	       if($('#ce_check').is(':checked'))
																	       {
																	           $('#ce_check1').val('1');
																	       }
																	       else
																	       {
																	            $('#ce_check1').val('0');
																	       }

    
});
	});  
	$(wrapper).on('click', '#remove_button_pro', function(e){ //Once remove button is clicked
		e.preventDefault();
		$(this).parent().parent('.professional_tabs').remove(); //Remove field html
		x--; //Decrement field counter
	});
});

																	   
</script>


<script type="text/javascript">
$(function () {
$("#type_form3").change(function () {
var selectedText =  $('#fedral_state_1').val();
//alert(selectedText);
var selectedValue = $(this).val();

if(selectedValue =='County')
{
var val= $('#physical_county_no').val();
var val1= $('#physical_county').val();
$('#business_license1').val(val);
$('#business_license4').hide();
$('#business_license3').hide();
$('#business_license5').empty();
$('#business_license5').show();
$('#business_license5').append('<select type="text" class="form-control" id="business_license2" name="business_license2" <?php if(Auth::user()->business_license_jurisdiction=='City'): ?> style="display:none;" <?php endif; ?>><option value="">Select</option><?php $__currentLoopData = $taxstate; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?><option value="<?php echo e($v->county); ?>" <?php if($v->county==Auth::user()->business_license2): ?> selected <?php endif; ?>><?php echo e($v->county); ?></option><?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></select>');
$("#county-change").show();
$("#county-change1").show();
$("#city-change").hide();
$("#city-change1").hide();
}
else if(selectedValue =='City')
{
 $('#business_license4').empty();
var val2 = $('#physical_city').val();
$('#business_license1').val('');
$('#business_license2').val(''); 
$('#business_license5').hide();
//$('#business_license2').append('<option value="'+val1+'">'+val1+'</option>');
$('#business_license4').append('<input tpye="text" name="business_license2" value="" class="form-control">'); 
$("#county-change").hide();
$("#county-change1").hide();
$("#city-change").show();
$("#city-change1").show();
}
else
{
$("#county-change").show();
//$("#county-change1").show();
}   
});
});
</script>
<script>
$(document).ready(function(){
	$(document).on('change','#type_form3', function()
	{        
var selectedCountry =  $('#company_state').val();
var physical_county =  $('#physical_county').val();
var physical_city =  $('#physical_city').val();//alert(physical_city);
// alert(selectedCountry);
		var id = $(this).val();
		$.get('<?php echo URL::to('getcount'); ?>?id='+id+'&state=' + selectedCountry, function(data)
		{  
            $('#business_license2').empty();
            $('#business_license2').append('<option value="">Select</option>');
            if(physical_city)
		       {
		            $('#business_license4').val(physical_city);
		            $('#business_license2').hide();
		            $('#business_license4').show();
		       }
		      
           $.each(data, function(index, subcatobj)
		   {
		       if(subcatobj.county==physical_county)
		       {
		            $('#business_license4').hide();
		            $('#business_license2').show();
		            $('#business_license2').append('<option value="'+physical_county+'" selected>'+physical_county+'</option>');
		       }
		       else
		       {
		            $('#business_license2').append('<option value="'+subcatobj.county+'">'+subcatobj.county+'</option>');
		       }
		   })
		});
	});
});
</script>
<script>
$(document).ready(function(){
	$(document).on('change','#physical_county', function()
	{        
        var selectedCountry = $("#fedral_state option:selected").val(); //alert(selectedCountry);
		var id = $(this).val();// alert(id);
		$.get('<?php echo URL::to('getcountycod'); ?>?id='+id, function(data)
		{  
            $('#physical_county_no').empty();

           $.each(data, function(index, subcatobj)
		   {
		       $('#physical_county_no').val(subcatobj.countycode);

		   })

		});
			
	});
});
</script>

<script>
$(document).ready(function(){
	$(document).on('change','#business_license2', function()
	{
		var id = $(this).val();
		$.get('<?php echo URL::to('getcountycod'); ?>?id='+id, function(data)
		{  
        $('#business_license1').empty();
           $.each(data, function(index, subcatobj)
		   {
           $('#business_license1').val(subcatobj.countycode);
		   })
		});
	});
});
</script>

<!--<script>
$(document).ready(function(){
$(document).on('change','#typeofcorp1', function()
{ //alert();
//console.log('htm');
var id = $(this).val(); //alert(id);
$.get('<?php echo URL::to('/getforms'); ?>?id='+id, function(data)
{ 
$('#due_date').empty();
$('#type_form').empty();
$('#due_date1').empty();
$('#type_form1').empty();
$('#fedral_state').empty();
$.each(data, function(index, subcatobj)
{
$('#type_form').append('<option value="'+subcatobj.telephone+'">'+subcatobj.telephone+'</option>');
$('#typeofcorp').val(subcatobj.authority_name); 		 
$('#due_date').val(subcatobj.address);
$('#due_date1').val(subcatobj.address);
$('#fedral_state').append('<option value="'+subcatobj.city+'">'+subcatobj.city+'</option>');
$('#type_form1').append('<option value="'+subcatobj.city+' '+subcatobj.zip+'">'+subcatobj.city+' '+subcatobj.zip+'</option>');
})
});
});
});
</script>-->

<div id="myModal007" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" nnnnnnnnnn>
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel"><span class="pull-left"><?php echo e(Auth::user()->company_name); ?></span><span style="margin-left:20px;" class="pull-left">View License</span></h4>
			</div>
			<div class="modal-body">
				<div id="viewmodel007"></div>
			</div>
			<div class="modal-footer"></div>
		</div>
	</div>
</div>


<div id="myModal8" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel"><span class="col1"></span></h4>
			</div>
			<div class="modal-body">
				<div id="viewmodel"></div>
			</div>
			<div class="modal-footer"></div>
		</div>
	</div>
</div>

<div id="myModal-history" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel"><span class="col1"></span></h4>
			</div>
			<div class="modal-body">
				<div id="viewmodel-1"></div>
			</div>
			<div class="modal-footer"></div>
		</div>
	</div>
</div>

<script>
$(document).ready(function()
{
    $('.openBtn').on('click', function()
    {
            var num = $(this).attr("num"); //alert(num);
            //alert(num);
            $.get('<?php echo URL::to('getimage'); ?>?id='+num, function(data){// alert();
            if(data == "")
            {
                if(num==='SOS Certificate')
                {
                $('.col2').html('Certificate of Corporation :');
                }
                else if(num==='SOS-AOI')
                {
                $('.col2').html('Articles of Incorporation');
                }
                else
                {
                $('.col2').html(num);
                }
                $('#myModal2').modal({show:true}); // alert('File Not Uploaded');  
            }
            else
            { 
                $.each(data, function(index, subcatobj)
                { 
                    //alert();
                    if(num === subcatobj.upload_name) 
                    {
                        if(subcatobj.upload_name==='SOS Certificate')
                        {
                            $('.col1').html('Certificate of Corporation :');
                        }
                        else if(subcatobj.upload_name==='SOS-AOI')
                        {
                            $('.col1').html('Articles of Incorporation');
                        }
                        else
                        {
                            $('.col1').html(num);
                        }
                        $('#myModal8').modal({show:true});
                        $('#viewmodel').html('<embed src="https://financialservicecenter.net/public/adminupload/'+subcatobj.upload+'" width="100%" height="300px">');
                    }
                    else
                    {
                        //alert();
                    }
                });
            }
    });		
    });	
});
</script>


<script>
$(document).ready(function()
{
    $('.openBtn007').on('click', function()
    {
        <?php
        if(isset($buslicense)!='')
        {
            ?>
            $('#myModal007').modal({show:true});
            $('#viewmodel007').html('<embed src="https://financialservicecenter.net/public/adminupload/<?php echo $buslicense->license_copy;?>" width="100%" height="300px">');    
            <?php
        }
        ?>
        
        // $('#myModal007').modal({show:true});
        // $('#viewmodel007').html('<embed src="https://financialservicecenter.net/public/adminupload/'+licensefilename+'" width="100%" height="300px">');
        // $('#viewmodel007').html('<embed src="https://financialservicecenter.net/public/adminupload/'+subcatobj.upload+'" width="100%" height="300px">');
    });		});	

</script>


<script>
$(document).ready(function(){
$('.openBtn1').on('click', function()
{
//console.log('htm');
var num = $(this).attr("num"); //alert(num);
alert(num);
$.get('<?php echo URL::to('history1'); ?>?id='+num, function(data){// alert();
if(data == "")
{
if(num==='SOS Certificate')
{
$('.col2').html('Certificate of Corporation :');
}
else if(num==='SOS-AOI')
{
$('.col2').html('Articles of Incorporation');
}
else
{
$('.col2').html(num);
}
$('#myModal2').modal({show:true}); // alert('File Not Uploaded');  
}
else
{ 
$.each(data, function(index, subcatobj)
{ //alert();
if(num === subcatobj.upload_name) 
{
if(subcatobj.upload_name==='SOS Certificate')
{
$('.col1').html('Certificate of Corporation :');
}
else if(subcatobj.upload_name==='SOS-AOI')
{
$('.col1').html('Articles of Incorporation');
}
else
{
$('.col1').html('License');
}
$('#myModal-history').modal({show:true});
$('#viewmodel-1').html('<embed src="https://financialservicecenter.net/public/adminupload/'+subcatobj.upload+'" width="100%" height="300px">');
}
else
{
//alert();
}
});
}
});		
});	
});
</script>

<script type="text/javascript">
	function ToggleReadOnlyState () {
		var textarea = document.getElementById ("contact_number1");
		textarea.readOnly = !textarea.readOnly;
	}
</script>

<?php $__currentLoopData = $admin_professional; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ak1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<?php if($ak1->profession==NULL): ?>
<?php else: ?>
<div id="myModalm99_<?php echo e($ak1->id); ?>" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Confirmation</h4>
			</div>
			<div class="modal-body">
				<p>Do you want to delete this record ?</p>
			</div>
			<div class="modal-footer">
				<a href="<?php echo e(route('admind1.admindelete1',$ak1->id)); ?>" class="btn btn-danger">Delete</a>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


<?php $__currentLoopData = $bankdata; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bankdetail): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<?php if($bankdetail->id==NULL): ?>
<?php else: ?>
<div id="bankModal_<?php echo e($bankdetail->id); ?>" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Confirmation</h4>
			</div>
			<div class="modal-body">
				<p>Do you want to delete this record ?</p>
			</div>
			<div class="modal-footer">
				<a href="<?php echo e(route('admin.adminbankdelete',$bankdetail->id)); ?>" class="btn btn-danger">Delete</a>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


 <?php $__currentLoopData = $carddata; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cardetail): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
 <?php if($cardetail->id==NULL): ?>
 <?php else: ?>
<div id="cardModal_<?php echo e($cardetail->id); ?>" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Confirmation</h4>
			</div>
			<div class="modal-body">
				<p>Do you want to delete this record ?</p>
			</div>
			<div class="modal-footer">
				<a href="<?php echo e(route('admin.admincarddelete',$cardetail->id)); ?>" class="btn btn-danger">Delete</a>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


<?php $__currentLoopData = $admin_shareholder; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ak): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<?php if($ak->agent_fname1==NULL): ?>
<?php else: ?>
<div id="myModalk_<?php echo e($ak->id); ?>" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Confirmation</h4>
			</div>
			<div class="modal-body">
				<p>Do you want to delete this record ?</p>
			</div>

			<div class="modal-footer">
     <a href="<?php echo e(route('admind.admindelete',$ak->id)); ?>" class="btn btn-danger">Delete</a>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

<?php $__currentLoopData = $admin_notes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $notes): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<?php if($notes->notes==NULL): ?>
<?php else: ?>
<div id="myModalnote_<?php echo e($notes->id); ?>" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Confirmation</h4>
			</div>
			<div class="modal-body">
				<p>Do you want to delete this record ?</p>
			</div>

			<div class="modal-footer">
     <a href="<?php echo e(route('note.notedelete',$notes->id)); ?>" class="btn btn-danger">Delete</a>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

<script>
var room1 = 0;
var coun = <?php echo $notecon;?>;
var z = room1 + coun; 
function education_field_note()
{
    room1++;
    z++;
    var objTo = document.getElementById('input_fields_wrap_notes')
    var divtest = document.createElement("div");
    divtest.setAttribute("class", "form-group removeclass"+z);
    divtest.innerHTML = '<label class="control-label col-md-3">Note '+ z +' :</label><div class="col-md-6"><input name="noteid[]" value="" type="hidden" placeholder="" id="noteid" class=""><input name="adminnotes[]" value="" type="text" id="adminnotes" placeholder="Create Note" class="textonly form-control" /></div></div><div class="col-md-1"> <button class="btn btn-danger" type="button" onclick="remove_education_fields('+ z +');"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span> </button></div></div></div>';
    var rdiv = 'removeclass'+z;
    var rdiv1 = 'Schoolname'+z;
    objTo.appendChild(divtest)
}

function remove_education_fields(rid) 
{
    $('.removeclass'+rid).remove();
    //z--;
    room1--;
}  
</script>
<?php 
$remday = Auth::user()->remainder_date;
$rem16 = date('Y-m-d', strtotime('-2 months', strtotime($remday)));
//$beforethreeday = strtotime($rem3);
//$enddate = strtotime($remday);
$current = date('Y-m-d');
if($current>=$rem16 && $current <= $remday) 
{ ?>
<script>
$(window).load(function(){        
   $('#myModalremainder').modal('show');
}); 
</script>
<div id="myModalremainder" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">License Renew Remainder</h4>
      </div>
      <div class="modal-body">
         <p>Please Renew your License.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<?php 
} else if($current >=$remday) { ?>
<!--<script type="text/javascript">
$(document).ready(function(){
$("#myModalremainder").modal({
		backdrop:'static',
		keyword:'false',
		show: true,
	});
	});	
</script>
<div id="myModalremainder" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <div class="modal-content">
      <div class="modal-header">
 
        <h4 class="modal-title">License Renew Remainder</h4>
      </div>
      <div class="modal-body">
        <p>Please Renew your License. <br> Please contact your Admin.</p>
      </div>
      <div class="modal-footer">
       	<a href="<?php echo e(route('fac-Bhavesh-0554.logout')); ?>" onclick="event.preventDefault();  document.getElementById('logout-form').submit();" class="btn btn-default"><i class="fa fa-sign-out sidebar-icon"></i><span>Log Out</span></a>
      </div>
    </div>

  </div>
</div>-->
<?php }?>
<?php $__currentLoopData = $admin_professional; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ak1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<?php if($ak1->profession==NULL): ?>
<?php else: ?>
<?php $__currentLoopData = $upload; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $up): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<?php if($up->upload_name==$ak1->pro_id): ?>
<?php 
$remday = $up->expired_date;
$arr = explode('/', $remday);
$newDate = $arr[2].'-'.$arr[0].'-'.$arr[1];
$rem16 = date('Y-m-d', strtotime('-2 months', strtotime($newDate)));
//$beforethreeday = strtotime($rem3);
//$enddate = strtotime($remday);
$current = date('Y-m-d');
if($current >= $rem16 && $current <= $newDate) 
{ ?>
<script>
$(window).load(function(){        
   $("#myModalremainder1").modal('show');
}); 
</script>
<div id="myModalremainder1" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">License Renew Remainder</h4>
      </div>
      <div class="modal-body">
         <p>Please Renew your License.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<?php 
}
else if($current >=$newDate) { ?>
<!--<script type="text/javascript">
$(document).ready(function(){
$("#myModalremainder1").modal({
		backdrop:'static',
		keyword:'false',
		show: true,
	});
	});	
</script>
<div id="myModalremainder1" class="modal fade" role="dialog">
  <div class="modal-dialog">
   
    <div class="modal-content">
      <div class="modal-header">
      
        <h4 class="modal-title">License Renew Remainder</h4>
      </div>
      <div class="modal-body">
        <p>Please Renew your License. <br> Please contact your Admin.</p>
      </div>
      <div class="modal-footer">
       	<a href="<?php echo e(route('fac-Bhavesh-0554.logout')); ?>" onclick="event.preventDefault();  document.getElementById('logout-form').submit();" class="btn btn-default"><i class="fa fa-sign-out sidebar-icon"></i><span>Log Out</span></a>
					
      </div>
    </div>

  </div>
</div>-->
<?php }?>
<?php endif; ?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php endif; ?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

<div id="alertss" class="modal fade" role="dialog" >
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content" style="background-color: #ededed;">
      <div class="modal-header">
  <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Save</h4>
      </div>
      <div class="modal-body">
        <p>Admin Profile Successfully Saved.</p>
      </div>
      <div class="modal-footer">
        <button data-dismiss="modal" class="btn btn-default" style="background-color: #bfbfbf;">Close</button>
        
      </div>
    </div> 

  </div>
</div>
<div id="alertss1" class="modal fade" role="dialog" >
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content" style="background-color: #ededed;">
      <div class="modal-header">
  <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Save</h4>
      </div>
      <div class="modal-body">
        <p>Admin Profile not Successfully Saved.</p>
      </div>
      <div class="modal-footer">
        <button data-dismiss="modal" class="btn btn-default" style="background-color: #bfbfbf;">Close</button>
        
      </div>
    </div> 

  </div>
</div>

<div id="alerts" class="modal fade" role="dialog" >
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content model-width" style="background-color: #f3f3f3;">
      <div class="modal-header">
  <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Save changes</h4>
      </div>
      <div class="modal-body">
        <center><p>Do you want to save the changes ?</p></center>
      </div>
      <div class="modal-footer">
         <button type="submit" style="width:69px" class="btn btn-default primary1 primary" style="">Yes</button>
         <button type="button" style="width:69px" id="button" class="btn btn-default primary">No</button>
         <a class="btn btn-default primary no-button">Cancel</a>
         </div>
    </div> 

  </div>


											
<!---------------------->
<!--<div class="wrap">
  <h1>Bootstrap Modal Example</h1>
  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#MyModal">
    Large modal
  </button>
</div>!-->
<div id="printThis">
    <div id="MyModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  
  <div class="modal-dialog modal-lg">
    
    <!-- Modal Content: begins -->
    <div class="modal-content">
      
      <!-- Modal Header -->
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="gridSystemModalLabel">Your Headings</h4>
      </div>
    
      <!-- Modal Body -->  
      <div class="modal-body">
        <div class="body-message">
          <h4>Any Heading</h4>
          <p>And a paragraph with a full sentence or something else...</p>
        </div>
      </div>
    
      <!-- Modal Footer -->
      <div class="modal-footer">
       <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
      <button id="btnPrint" type="button" class="btn btn-default">Print</button>
      </div>
    
    </div>
    <!-- Modal Content: ends -->
    
  </div>
    </div>
</div>






<script>

document.getElementById("btnPrint").onclick = function () {
    printElement(document.getElementById("printThis"));
}

function printElement(elem) {
    var domClone = elem.cloneNode(true);
    
    var $printSection = document.getElementById("printSection");
    
    if (!$printSection) {
        var $printSection = document.createElement("div");
        $printSection.id = "printSection";
        document.body.appendChild($printSection);
    }
    
    $printSection.innerHTML = "";
    $printSection.appendChild(domClone);
    window.print();
}

// function getvalYear(){
//     var yearboxval = document.getElementById("formation_yearbox").value;
//     var yearvaluesub = document.getElementById("formation_yearvalue").value;
//   //  alert(yearboxval);
//   if(yearboxval==''){
//       $("#formation_yearvalue").val('');
//     }
//     else if(yearboxval==1)
//     {
//         $("#formation_yearvalue").val('2020');
//         $("#formation_amount").val('$50.00');
//     }
//     else if(yearboxval==2)
//     {
//         $("#formation_yearvalue").val('2020,2021');
//         $("#formation_amount").val('$100.00');
//     }
//     else if(yearboxval==3)
//     {
//         $("#formation_yearvalue").val('2020,2021,2022');
//         $("#formation_amount").val('$150.00');
//     }
// }




</script>



<script>
$('select').on('change', function() {
    $('#text1').val(this.value);
    
});

   jQuery(function($) {
  var input = $('input,textarea,select');
  input.on('keydown', function() {
    var key = event.keyCode || event.charCode || event.which;
//alert(key);
    if( key == 8 || key == 46 ){
   $('#text1').val(key);
    }
     else{   
         $('#text1').val(key);
         
     }
  });
});
$(document).ready(function()
{
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        var currentTab = $(e.target).text(); // get current tab
        var vf = $('#text1').val();
        //  var test1= $('input[type="radio"]:checked').val();
        // var tt1= $('#text1').val(test1);
 // alert(test1);
        //(vf);
        var current_tab = e.target;
        var previousTab = $(e.relatedTarget).text(); 
        var target = $(e.relatedTarget).attr("href");
        $('.no-button').eq(0).attr('href',target);
        var href = $('.no-button').attr('href');
     //  alert(target);
    //   alert(href);
     if(target==href){ 
        if(vf)
        { //alert('21');
         $("#alerts").modal({
		show: true,
	});
        } 
        else
        {
            
        }
     }
     else
     { 
     }
     
    });
});

$(function() {

    $('#login-form-link').click(function(e) {
		$("#login-form").delay(100).fadeIn(100);
 		$("#register-form").fadeOut(100);
		$('#register-form-link').removeClass('active');
		$(this).addClass('active');
		e.preventDefault();
	});
	$('#register-form-link').click(function(e) {
		$("#register-form").delay(100).fadeIn(100);
 		$("#login-form").fadeOut(100);
		$('#login-form-link').removeClass('active');
		$(this).addClass('active');
		e.preventDefault();
	});

});


$(document).on('click', '.no-button', function () {
    var href = $(this).attr('href');
     var $link = $('li.active a[data-toggle="tab"]');
    $link.parent().removeClass('active');
    var tabLink = $link.attr('href');//alert(tabLink);
     $('#alerts').modal('hide');
    $('#myTab a[href="' + href + '"]').tab('show');
      
});
</script>


<script>
$('.btn-toggle').click(function() {
    $(this).find('.btn').toggleClass('active');  
    /*if ($(this).find('.btn-primary').length>0) {
    	$(this).find('.btn').toggleClass('btn-primary');
    }
    if ($(this).find('.btn-danger').length>0) {
    	$(this).find('.btn').toggleClass('btn-danger');
    }
    if ($(this).find('.btn-success').length>0) {
    	$(this).find('.btn').toggleClass('btn-success');
    }
    if ($(this).find('.btn-info').length>0) {
    	$(this).find('.btn').toggleClass('btn-info');
    }*/
    $(this).find('.btn').toggleClass('btn-default');
});
/*$('form').submit(function(){
  var radioValue = $("input[name='options']:checked").val();
  if(radioValue){
     alert("You selected - " + radioValue);
   };
    return false;
});
*/
$(document).ready(function(){
    var flag = $('#flag').val();
    var flag2 = $('#flag2').val();
   // var flag = $('#flag3').val();
  
    <?php
    if($bankcount>0)
    {
        ?>
        var cnt='<?php echo $bankcount;?>';
        <?php
    }
    else
    {
        ?>
       var cnt=1;
        <?php
    }
    ?>
   	$(".addCFnew").click(function()
   	{
	    cnt++;
	   var rowCount = $("#customFieldsbo .customfieldsbox").length;
	   var countrow=rowCount+1;
        $('#bank_accounts_count').val(countrow);
	    //var cnt11=cnt.count();
//var len=$(cnt11).length();
//alert(cnt11);
		$(".show_bank1").append('<div class="col-md-12 "><div id="customFieldsbo"><div class="customfieldsbox"><div class="row form-group"> <div class="col-md-3"><label class="control-label">Bank Name</label> <select class="form-control" name="bank_name[]"><option value="">Select</option><?php $__currentLoopData = $bankmaster; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bkmaster): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?><option value="<?php echo e($bkmaster->bankname); ?>"><?php echo e($bkmaster->bankname); ?></option><?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></select></div> <div class="col-md-3"><label class="control-label">Account Nick Name</label> <input type="text" class="form-control" name="nick_name[]"/> </div> <div class="col-md-3 full_991" style="width:23%"><label class="control-label">Last 4 digit of A/C #</label> <input type="text" class="form-control fourdigit" name="fourdigit[]"/> </div> <div class="col-md-3 full_991" style="width:16%"> <label class="control-label">Open Date</label><input type="text" class="form-control opendate" name="opendate[]"/> </div>   <div class="col-md-1"></div>  </div> <div class="row form-group"> <div class="col-md-3"> <label class="control-label">Check Stubs</label><select class="form-control" name="stubs[]"><option value="">Select</option><option value="St. With Check Image">St. With Check Image</option><option value="Counter Check">Counter Check</option></select> </div> <div class="col-md-3"> <label class="control-label">Statement</label><select class="form-control" name="statement[]"><option value="">Select</option><option value="E-Statement">E-Statement</option><option value="Paper-Statement">Paper-Statement</option><option value="FSC get Online">FSC get Online</option></select></div> <div class="col-md-3 full_991" style="width:23%;"><label class="control-label">Status</label> <select class="form-control showns'+cnt+'" name="bankstatus[]"><option value="">Select</option><option value="Active">Active</option><option value="Close">Close</option></select> </div> <div class="col-md-3 show'+cnt+'" style="width:16%;display:none;"> <label class="control-label">Close Date</label> <input type="text" class="form-control closedate" name="closedate[]" readonly/> </div> <div class="col-md-1"><a class="btn btn-danger mt30 remCFnew">Remove</a> </div></div></div></div> ');
 
        $('.showns'+cnt).on('change', function() 
        {
             var thisss=$(this).val();
             if(thisss =='Close')
             {
                 $('.show'+cnt).show();
             }
             else
             {
                 $('.show'+cnt).hide();
             }
        });
 
        $(".opendate").datepicker({
       		autoclose: true,
             format: "mm/dd/yyyy",
       	});
       	$(".fourdigit").mask("9999");
       
       	
       	$(".closedate").datepicker({
       		autoclose: true,
             format: "mm/dd/yyyy",
       	});

	});
   
   
    $(".show_bank1").on('click','.remCFnew',function(){
      //  flag--;
      var rowCount = $("#customFieldsbo .customfieldsbox").length;
	   var countrow=rowCount-1;
	     $('#bank_accounts_count').val(countrow);
	  
       //alert(countrow);
      //alert(cnt)
      //alert();
        $(this).parent().parent().parent().parent().parent().remove();
    });
    
    
    <?php
    if($cardcount>0)
    {
        ?>
        var cnt2='<?php echo $cardcount;?>';
        <?php
    }
    else
    {
        ?>
       var cnt2=0;
        <?php
    }
    ?>
    $(".addCFnewccd").click(function()
    {   cnt2++;
     var rowCount = $("#customFieldsboccard .customfieldsbox").length;
	 var countrow=rowCount+1;
	 
	 var clone_div = '<div class="col-md-12">';
    clone_div += '<div id="customFieldsboccard">';
    clone_div += '<div class="customfieldsbox">';
    clone_div += '<div class="row form-group" style="margin-bottom:0px;">';
    clone_div += '<div class="col-md-3">';
    clone_div += '<label class="control-label">Type of CC</label>';
    clone_div += '<select class="form-control card_bank_name" id="'+countrow+'" name="card_bank_name[]" onchange="setIssuerName(this)">';
    clone_div += '<option>Select</option>';
    clone_div += '<option value="Visa">Visa</option>';
    clone_div += '<option value="Master Card">Master Card</option>';
    clone_div += '<option value="Discover">Discover</option>';
    clone_div += '<option value="American Express">American Express</option>';
    clone_div += '<option value="Other">Other</option>';
    clone_div += '</select>';
    clone_div += '</div>';
    clone_div += '<div class="col-md-3">';
    clone_div += '<label class="control-label">CC Issuer name</label>';
    clone_div += '<input type="text" class="form-control" name="card_nick_name[]" id="card_issuer_name'+countrow+'" value="">';
    clone_div += '</div>';
    clone_div += '<div class="col-md-3 full_991" style="width:23%">';
    clone_div += '<label class="control-label">Last 4 digit of A/C #</label>';
    clone_div += '<input type="text" class="form-control cardfourdigit" name="card_fourdigit[]" value="">';
    clone_div += '</div>';
    clone_div += '<div class="col-md-2 full_991" style="width:20%">';
    clone_div += '<label class="control-label">Statement</label>';
    clone_div += '<select class="form-control" name="card_statement[]">';
    clone_div += '<option value="">Select</option>';
    clone_div += '<option value="E-Statement">E-Statement</option>';
    clone_div += ' <option value="Paper-Statement">Paper-Statement</option>';
    clone_div += ' <option value="FSC get Online">FSC get Online</option>';
    clone_div += '</select>';
    clone_div += '</div>';
    clone_div += '<div class="col-md-1">';
    clone_div += '</div>';
    clone_div += '</div>';
    clone_div += '<div class="row form-group">';
    clone_div += '<div class="col-md-3">';
    clone_div += '<label class="control-label">Note</label>';
    clone_div += '<input type="text" class="form-control" name="card_notes[]" value="">';
    clone_div += '</div>';
    clone_div += '<div class="col-md-3">';
    clone_div += '<label class="control-label">Open Date</label>';
    clone_div += '<input type="text" class="form-control cardopendate" name="card_opendate[]" value="" readonly>';
    clone_div += '</div>';
    clone_div += '<div class="col-md-3 full_991" style="width:23%">';
    clone_div += '<label class="control-label">Status</label>';
    clone_div += '<select class="form-control" name="card_bankstatus[]" id="card_bankstatus">';
    clone_div += '<option value="">Select</option>';
    clone_div += '<option value="Active">Active</option>';
    clone_div += '<option value="Close">Close</option>';
    clone_div += '</select>';
    clone_div += '</div>';
    clone_div += '<div class="col-md-3" style="width:15.5%;display:none;" id="card_closedate">';
    clone_div += '<label class="control-label">Close Date</label>';
    clone_div += '<input type="text" class="form-control cardclosedate" name="card_closedate[]" value="" readonly>';
    clone_div += '</div>';
    clone_div += '<div class="col-md-1">';
    clone_div += '<a class="btn btn-danger mt30 remCFccdnew">Remove</a>';
    clone_div += '</div>';
    clone_div += '</div>';
    clone_div += '</div>';
    clone_div += '</div>';
    clone_div += '</div>'; 
	  
	  
      $('.ccd_count').val(countrow);
      
       
	  
// 		$(".show_card1").append('<div class="col-md-12 "><div id="customFieldsboccard"><div class="customfieldsbox"> <div class="row form-group"> <div class="col-md-3"><label class="control-label">Credit Card Name</label> <input type="text" class="form-control" name="card_bank_name[]"/></div> <div class="col-md-3"><label class="control-label">Account Nick Name</label> <input type="text" class="form-control" name="card_nick_name[]"/> </div> <div class="col-md-3 full_991" style="width:23%"><label class="control-label">Last 4 digit of A/C #</label> <input type="text" class="form-control cardfourdigit" name="card_fourdigit[]"/> </div> <div class="col-md-2 full_991" style="width:20%"> <label class="control-label">Statement</label> <select class="form-control" name="card_statement[]"><option value="">Select</option><option value="E-Statement">E-Statement</option><option value="Paper-Statement">Paper-Statement</option><option value="FSC get Online">FSC get Online</option></select><div class="col-md-1"></div> </div> </div> <div class="row form-group"> <div class="col-md-3"> <label class="control-label">Note</label><input type="text" class="form-control" name="card_notes[]"/> </div> <div class="col-md-3"> <label class="control-label">Open Date</label><input type="text" class="form-control cardopendate" name="card_opendate[]" /> </div> <div class="col-md-3 full_991" style="width:23%"><label class="control-label">Status</label> <select class="form-control showns'+cnt2+'" name="card_bankstatus[]"><option value="">Select</option><option value="Active">Active</option><option value="Close">Close</option></select> </div> <div class="col-md-3 show'+cnt2+'" style="width:15.5%;display:none;"> <label class="control-label">Close Date</label> <input type="text" class="form-control cardclosedate" name="card_closedate[]" readonly/> </div> <div class="col-md-1"><a class="btn btn-danger mt30 remCFccdnew">Remove</a> </div></div></div></div> ');
		//$(".show_card1").append('<div class="col-md-12 "><div id="customFieldsboccard"><div class="customfieldsbox"> <div class="row form-group"> <div class="col-md-3"><label class="control-label">Type of CC</label> <select class="form-control" card_bank_name" id="'+countrow+' ><option>Select</option><option>Visa</option><option>Master Card</option><option>Discover</option><option>American Express</option><option>Other</option></select> </div> <div class="col-md-3"><label class="control-label">CC Issuer name</label> <input type="text" class="form-control" name="card_nick_name[]"/> </div> <div class="col-md-3 full_991" style="width:23%"><label class="control-label">Last 4 digit of A/C #</label> <input type="text" class="form-control cardfourdigit" name="card_fourdigit[]"/> </div> <div class="col-md-2 full_991" style="width:20%"> <label class="control-label">Statement</label> <select class="form-control" name="card_statement[]"><option value="">Select</option><option value="E-Statement">E-Statement</option><option value="Paper-Statement">Paper-Statement</option><option value="FSC get Online">FSC get Online</option></select><div class="col-md-1"></div> </div> </div> <div class="row form-group"> <div class="col-md-3"> <label class="control-label">Note</label><input type="text" class="form-control" name="card_notes[]"/> </div> <div class="col-md-3"> <label class="control-label">Open Date</label><input type="text" class="form-control cardopendate" name="card_opendate[]" /> </div> <div class="col-md-3 full_991" style="width:23%"><label class="control-label">Status</label> <select class="form-control showns'+cnt2+'" name="card_bankstatus[]"><option value="">Select</option><option value="Active">Active</option><option value="Close">Close</option></select> </div> <div class="col-md-3 show'+cnt2+'" style="width:15.5%;display:none;"> <label class="control-label">Close Date</label> <input type="text" class="form-control cardclosedate" name="card_closedate[]" readonly/> </div> <div class="col-md-1"><a class="btn btn-danger mt30 remCFccdnew">Remove</a> </div></div></div></div> ');
		$(".show_card1").append(clone_div);
		$('.showns'+cnt2).on('change', function() 
        {
             var thisss=$(this).val();
             if(thisss =='Close')
             {
                 $('.show'+cnt2).show();
             }
             else
             {
                 $('.show'+cnt2).hide();
             }
        });
 
        $(".cardopendate").datepicker({
       		autoclose: true,
             format: "mm/dd/yyyy",
       	});
       	$(".cardfourdigit").mask("9999");
       
       	
       	$(".cardclosedate").datepicker({
       		autoclose: true,
             format: "mm/dd/yyyy",
       	});
		
	});
	
	
   
   
    $(".show_card1").on('click','.remCFccdnew',function(){
      //  flag--;
      //alert();
        var rowCount = $("#customFieldsboccard .customfieldsbox").length;
	  var countrow=rowCount-1;
      $('.ccd_count').val(countrow);

        $(this).parent().parent().parent().parent().parent().remove();
    });
    
   
   
	$(".addCF").click(function(){
	    flag++;
		$("#customFields").append('<tr valign="top"><td>'+flag+'</td><td><input type="hidden" class="form-control" name="flag[]" placeholder="Enter Bank Name"/><input type="text" class="form-control" name="bankname[]" placeholder="Enter Bank Name"/></td><td><input type="text" class="form-control" name="accountname[]" placeholder="Enter Account Nick Name"/></td><td><input type="text" name="lastfourname[]" class="form-control fourdigit" placeholder="Enter Last 4 digit of A/C #"/></td><td><Select class="form-control" name="statement[]"><option value="">Select</option><option value="E-Staement">E-Staement</option><option value="Paper-Statement">Paper-Statement</option><option value="FSC get Online">FSC get Online</option></Select></td> <td><Select class="form-control" name="counter[]"><option>Select</option><option>St. With Check image</option> <option>Counter Check</option> </Select></td><td> <a href="javascript:void(0);" class="remCF btn btn-danger">Remove</a></td></tr>');
	
	    $(".fourdigit").mask("9999");
   
	});
	$(".addCF2").click(function(){
	    flag2++;
		$("#customFields2").append('<tr valign="top"><td>'+flag2+'</td><td><input type="hidden" class="form-control" name="flag2[]" placeholder="Enter Bank Name"/><input type="text" class="form-control" placeholder="Enter Credit Card Name" name="business_credit_card_number[]"/></td><td><input type="text" name="business_credit_card_digit_number[]" class="form-control" placeholder="Enter Last 4 digit Number"/></td><td><input type="text" name="business_credit_enddate[]" class="form-control effective_date1" placeholder="Enter Statement End Date"/></td><td> <a href="javascript:void(0);" class="remCF2 btn btn-danger">Remove</a></td></tr>');
	   	$(".effective_date1").datepicker({
   		autoclose: true,
         format: "mm/dd/yyyy",
   	});
	});
	
    $("#customFields").on('click','.remCF',function(){
        flag--;
        $(this).parent().parent().remove();
    });
      $("#customFields2").on('click','.remCF2',function(){
        flag2--;
        $(this).parent().parent().remove();
    });
});


    function setIssuerName(e){
	    var id = e.id;
	    value = e.value;
	    if(value =='Visa' || value == 'Master Card'){
	        $('#card_issuer_name'+e.id).removeAttr('readonly');
	        $('#card_issuer_name'+e.id).val('');
	    }else{
	        $('#card_issuer_name'+e.id).val(value);
	        $('#card_issuer_name'+e.id).attr('readonly',true); 
	    }
	}

</script>
<script type="text/javascript">
    $(document).ready(function()
    {
        
        $("#subscription_access").click(function () { 
            if ($(this).is(":checked")) {
                $(".subscription_hidden2").show();
               // $("#AddPassport").hide();
            } else {
                $(".subscription_hidden2").hide();
               // $("#AddPassport").show();
            }
        });
        
         $("#limit_access").click(function () { 
            if ($(this).is(":checked")) {
                $(".limit_hidden").show();
               // $("#AddPassport").hide();
            } else {
                $(".limit_hidden").hide();
               // $("#AddPassport").show();
            }
        });
        
        $("#user_access").click(function () { 
            if ($(this).is(":checked")) {
                $(".user_hidden").show();
               // $("#AddPassport").hide();
            } else {
                $(".user_hidden").hide();
               // $("#AddPassport").show();
            }
        });
        
        
   
        
           $('#businessone').on('change', function() {
               
                if($(this).val() =='NewCreated'){
                    // alert('hide');
                    $('.purchage_business ').hide();
                    
                } else  if($(this).val() =='Purchase'){
                   // alert('show');
                    $('.purchage_business ').show();
                } else {
                     $('.purchage_business ').hide();
                }
             });
             
              $('#salesbusiness').on('change', function() {
                 if($(this).val() =='Sale'){
                // alert('hide');
                $('.sale_business ').show();
                $('.transfer_business ').hide();
                
            } else if($(this).val() =='Transfer'){
               // alert('show');
                $('.transfer_business ').show();
                $('.sale_business ').hide();
            } else{
                 $('.transfer_business ').hide();
                $('.sale_business ').hide();
            }
        });


     $("#button").click(function()
     {
       location.reload(true);
      });
    });
    
    function showbanknamevid()
    {
        $('#btnno').removeClass('btn-danger');
        $('#banknamediv').show();
        $('#uparrow').show();
        $('#banktype').val('1');
    }
    
    function showbanknamevid02()
    {
        $('#banknamediv').show();
        $('#uparrow').show();
        $('#dwnarrow').hide();
        $('#banktype').val('1');
    }
   
    function hidebanknamevid()
    {
        $('#btnyes').removeClass('btn-success');
        $('#btnno').addClass('btn-danger');
        $('#banknamediv').hide();
        $('#uparrow').hide();
        $('#dwnarrow').hide();
        $('#banktype').val('2');
    }
    
    function hidebanknamevid02()
    {
        $('#banknamediv').hide();
        $('#uparrow').hide();
        $('#dwnarrow').show();
        $('#banktype').val('2');
    }
    
    function showcreditcardnamevid()
    {
        $('#creditcardnamediv').show();
        $('#uparrow2').show();
        $('#dwnarrow2').hide();
        $('#banktype1').val('1');
    }
    
    function showcreditcardnamevid02()
    {
        $('#creditcardnamediv').hide();
        $('#uparrow2').hide();
        $('#dwnarrow2').show();
    }
    
    function showcreditcardnamevid1()
    {
        $('#creditcardnamediv1').show();
        $('#businessloan').val('1');
        $('#uparrow3').show();
        $('#dwnarrow3').hide();
    }
    
    function showcreditcardnamevid102()
    {
        $('#creditcardnamediv1').hide();
        $('#uparrow3').hide();
        $('#dwnarrow3').show();
        $('#businessloan').val('1');
    }
    
    
     function showcreditcardnamevid2()
    {
     $('#creditcardnamediv1').hide();
      $('#uparrow3').hide();
     $('#dwnarrow3').hide();
      $('#businessloan').val('2');
    }
    
       function hidecreditcardnamevid102()
    {
     $('#creditcardnamediv1').show();
     $('#uparrow3').show();
     $('#dwnarrow3').hide();
      $('#businessloan').val('2');
    }
    
    function hidecreditcardnamevid()
    {
     $('#creditcardnamediv').hide();
      $('#banktype1').val('2');
    }
    
    
     function hidecreditcardnamevid02()
    {
     $('#creditcardnamediv').hide();
      $('#uparrow2').hide();
      $('#dwnarrow2').show();
      //$('#banktype1').val('2');
    }
    
    
            
    $('#businessone').on('change', function() {
        
           //alert();
        if ($(this).value == 'NewCreated') 
        {
             alert('NewCreated');
         $('.purchage_business').hide();
        }
        else if ($(this).value == 'Purchase') 
        {
            alert('purchase');
         $('.purchage_business').show();
        }
    });
    
    $('input[type=radio][name=saleofbusiness]').change(function() 
    {
    if (this.value == 'Transfer of Business') 
    {
     $('.sale_business').hide();
     $('.transfer_business').show();
    }
    else if (this.value == 'Sale of Business') 
    {
     $('.sale_business').show();
     $('.transfer_business').hide();
    }
    
});
var bankinfo1=$('.bank_que').val();
    if(bankinfo1=='0')
    {
        //alert();
        $("#banknamediv").hide();
    }
    else if(bankinfo1=='1')
    {
        $("#banknamediv").show();
    }
    $(".show_bank1").show();
    $('.bank_que').on('change', function() {
      if ( this.value == '1')
      {
        $(".show_bank1").show();
      }
      else
      {
        $(".show_bank1").hide();
      }
    });// $(".card_detail").hide();
    var card1=$('#creditcardnamediv').val();
    if(card1=='0')
    {
        $("#creditcardnamediv").hide();
    }
    else if(card1=='1')
    {
        $("#creditcardnamediv").show();
    }

    $('.card_que').on('change', function() {
      if ( this.value == '1')
      {
        $(".show_card1").show();
      }
      else
      {
       $(".show_card1").hide();

      }
      
    });
      
    $('.loan_que').on('change', function() {
      if ( this.value == '1')
      {
        $("#creditcardnamediv1").show();
      }
      else
      {
       $("#creditcardnamediv1").hide();

      }
      
    });
</script>

<script>

$(function () {
        $('.primary1').click(function () { 
        $.ajax({
        type: "post",
        url: "<?php echo e(route('adminprofile.update',Auth::user()->id)); ?>",
        dataType: "json",
        data: $('#adminform').serialize(),
        success: function(data){
            //alert(data);
           // echo "success";die;
            $('#alerts').modal('hide');
           // $('#alertss').modal('show');
            var vf = $('#text1').val('');
           // return redirect(route('adminprofile'));
           alert('Admin profile Succesfully Updated.');
            location.reload(true);
        
            
        },
        error: function(data){
          //  echo "error";die;
              $('#alerts').modal('hide');
           
              alert('Admin profile Succesfully Updated.');
             location.reload(true);
       // location.reload(true);
           // $('#alertss1').modal('show');
        }
    });
       
//$('#myModal2').modal('hide');
                });
            });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#button").click(function(){
            location.reload(true);
         });
    });
</script>
<script>
        function myFunction() {
  // Get the checkbox
  var checkBox = document.getElementById("myCheck");
  // Get the output text
  var text = document.getElementById("questionbox");

  // If the checkbox is checked, display the output text
  if (checkBox.checked == true){
    text.style.display = "block";
  } else {
    text.style.display = "none";
  }
}
    </script>
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
     <script>
        function myFunctionone1() {
       var x = document.getElementById("answer1");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }}
  
   function myFunctionone2() {
       var x = document.getElementById("answer2");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }}

 function myFunctionone3() {
       var x = document.getElementById("answer3");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }}
// $("#soscertificate").change(function(){
//   $("#file-name").text(this.files[0].name);
// });
// $("#sosaoi").change(function(){
//   $("#file-name1").text(this.files[0].name);
// });

    // Ankit Patel

    // $("#dateFrom").datepicker({
    //     onClose: function () {
    //         $("#dateTo").datepicker(
    //             "change", {
    //             minDate: new Date($('#dateFrom').val())
    //         });
    //     }
    // });

    // $("#dateTo").datepicker({
    //     onClose: function () {
    //         $("#dateFrom").datepicker(
    //             "change", {
    //             maxDate: new Date($('#dateTo').val())
    //         });

    //     }
    // });
var select5 = document.getElementById('license_required');
var select4 = document.getElementById('business_loan');
var select3 = document.getElementById('bank_required');
var select1 = document.getElementById('payroll_que');
var select2 = document.getElementById('creditcard_required');
select5.onchange = function () {
    select5.className = this.options[this.selectedIndex].className;
}
select4.onchange = function () {
    select4.className = this.options[this.selectedIndex].className;
}
select3.onchange = function () {
    select3.className = this.options[this.selectedIndex].className;
}
select1.onchange = function () {
    select1.className = this.options[this.selectedIndex].className;
}
// select2.onchange = function () {
//     select2.className = this.options[this.selectedIndex].className;
// }
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>