<?php $__env->startSection('main-content'); ?>
<style>
    .buttons-pdf {
    font-size: 0px !important;
    border-radius: 3px;
    padding: 8px 10px !important;
    background: #fff !important;
    border-color: #c6c6c6  !important;
    color:red !important;
}
.buttons-print {
    font-size: 0px !important;
    border-radius: 3px;
    padding: 8px 10px !important;
    background: #3c8dbc !important;
            border-color: #367fa9 !important;
}
.buttons-excel{
    font-size:0px !important;
    border-radius: 3px;
        padding: 8px 10px !important;
        
         background: #00a65a !important;
    border-color: #008d4c !important;
}
.box-header{
    padding-top:0px;
}
.buttons-excel:hover{
     background: #008d4c !important;

}
.buttons-pdf:hover{
     background: #f6f6f6  !important;
}

.buttons-print:hover{
     background: #367fa9 !important;
}
.fa{
    font-size: 16px !important;
}
@media  only screen and (max-width: 991px){
    .table-title a {
        margin-top: 0px !important; 
        margin-right: 0px !important;
    }
}
@media  only screen and (max-width: 880px){
    .box-header>.box-tools{
        position: relative !important;
        margin-right: 5px !important;
        margin-top: 0px !important;
    }
    .table-title a {
        margin-right: -10px !important;
    }
}
@media  only screen and (max-width: 490px){
    div.dataTables_wrapper div.dataTables_filter{
        width: 100%;
        display: flex;
    }
    div.dataTables_wrapper div.dataTables_filter label{
        width: 84%;
    }
    .table-title a {
        margin-top: -34px !important;
        margin-right: 10px !important;
    }
    .box-header>.box-tools {
        position: absolute !important;
        margin-top: 51px !important;
        margin-right: 120px !important;
    }
    .box-header {
        padding: 10px !important;
    }
}
</style>
<div class="content-wrapper">
	 <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>Prospect</h1>
    </section>
    <!-- Main content -->
    <section class="content">
	<div class="row">
	
		<div class="col-md-12">
			<div class="box box-success">
			      <div class="box-header">
            
              <div class="box-tools pull-right" style="position: absolute;margin-right: 132px;margin-top:7px;z-index:9999;">
                <div class="table-title">
					
						<a href="#">Add New Prospect</a>
					</div>
              </div>
            </div>
				<div class="col-md-12">
					
					<?php if( session()->has('success') ): ?>
                        <div class="alert alert-success alert-dismissable"><?php echo e(session()->get('success')); ?></div>
                               <?php endif; ?>
					<div class="table-responsive">
						<table class="table table-bordered" id="sampleTable345">
							<thead>
							    <tr>
							        <th width="5%">No</th>
							        <th width="12%">Proposal Date</th>
							        <th width="24%">Client Name</th>
							        <th>Type of Service</th>
							        <th width="11%">Telephone</th>
							        <th>Status</th>
							        <th width="10%">Action</th>
						        </tr>
						    </thead>
							<tbody>
							    <?php $__currentLoopData = $getProposal; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $proposal): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							    <tr>
							        <td style="text-align:center"><?php echo e($key+1); ?></td>
							        <td style="text-align:center"><?php echo e(date('M-d-Y',strtotime($proposal->created_at))); ?></td>
							        <td><?php echo e($proposal->client_name); ?></td>
							        <td><?php echo e($proposal->service_name); ?></td>
							        <td style="text-align:center"><?php echo e($proposal->telephone); ?></td>
							        <td>
							            <?php if($proposal->status == 1): ?>
							                <?php echo e('Accepted'); ?> 
							            <?php endif; ?>
							            
							             <?php if($proposal->status == 0): ?>
							                <?php echo e('Pending'); ?> 
							            <?php endif; ?>
							        </td>
							        <td style="text-align:center">
							            <a class="btn-action btn-view-edit" href="<?php echo e(url('fac-Bhavesh-0554/prospect/show')); ?>/<?php echo e($proposal->pid); ?>"><i class="fa fa-eye"></i></a>
							            <a class="btn-action btn-view-edit" href="">
						                    <i class="fa fa-edit"></i>
					                    </a>
					                    <a class="btn-action btn-delete" href=""><i class="fa fa-trash"></i></a>
							        </td>
						        </tr>
						        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</section>
<!--</div>-->

<?php $__env->stopSection(); ?>






<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>