<?php $__env->startSection('main-content'); ?>
 <style>
 body{
    font-size:13px !important;
}
.subttl{margin-bottom:10px;}
    .subttl h4{margin:0px; padding:0px;}
    .form-control{height:30px; padding:0px 5px;}
    .text-left {text-align:left;}
    .padleft20{padding-left:12px!important;}
    .wdth100{width:140px!important;}
    .wdth160{width:145px!important;}
    .wdth200{width:200px!important;}
    input[type=date]::-webkit-inner-spin-button, 
input[type=date]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}
 
input[type=date] {
    -moz-appearance:textfield;
}
.bold{font-weight:bold;}
.clear{clear:both;}
.padleftzero{padding-left:0px!important;}
</style>  
<div class="content-wrapper">
      <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     	 <h1>County --- Taxation / License</h1>
    </section>
    <!-- Main content -->
    <section class="content">
    <div class="row">
        <div class="col-md-12">
         	<div class="box box-success">
			    <div class="box-header">
                  <div class="box-tools pull-right">
                </div>
            </div>
			<div class="col-md-12">
                <!--<div class="Branch">-->
                <!--    <h1>County - Taxation - License</h1>-->
                <!--</div><br>-->
               <form method="post" action="<?php echo e(route('taxstate.store')); ?>" id="form_valid" class="form-horizontal" enctype="multipart/form-data">
                  <?php echo e(csrf_field()); ?>

                <div class="col-md-12">
                    <div class="Branch subttl" style="background:#b3e3ff!important;">
                         <h4>County Information</h4>
                    </div>
                </div>
        
                
                 <div class="formbox">
                             <div class="col-md-2" style="width:12%;">
                            <div class=" <?php echo e($errors->has('state') ? ' has-error' : ''); ?>">
                            <label class="control-label">State</label>
                             <select name="state" id="state_county_first" class="form-control fsc-input" style="width:100px;"> <br/>
                                           
                                                        <option value="">---Select---</option>
                                                                                  
                                                           <option value="AK">AK</option>
                                                           <option value="AS">AS</option>
                                                           <option value="AZ">AZ</option>
                                                           <option value="AR">AR</option>
                                                           <option value="CA">CA</option>
                                                           <option value="CO">CO</option>
                                                           <option value="CT">CT</option>
                                                           <option value="DE">DE</option>
                                                           <option value="DC">DC</option>
                                                           <option value="FM">FM</option>
                                                           <option value="FL">FL</option>
                                                           <option value="GA">GA</option>
                                                           <option value="GU">GU</option>
                                                           <option value="HI">HI</option>
                                                           <option value="ID">ID</option>
                                                           <option value="IL">IL</option>
                                                           <option value="IN">IN</option>
                                                           <option value="IA">IA</option>
                                                           <option value="KS">KS</option>
                                                           <option value="KY">KY</option>
                                                           <option value="LA">LA</option>
                                                           <option value="ME">ME</option>
                                                           <option value="MH">MH</option>
                                                           <option value="MD">MD</option>
                                                           <option value="MA">MA</option>
                                                           <option value="MI">MI</option>
                                                           <option value="MN">MN</option>
                                                           <option value="MS">MS</option>
                                                           <option value="MO">MO</option>
                                                           <option value="MT">MT</option>
                                                           <option value="NE">NE</option>
                                                           <option value="NV">NV</option>
                                                           <option value="NH">NH</option>
                                                           <option value="NJ">NJ</option>
                                                           <option value="NM">NM</option>
                                                           <option value="NY">NY</option>
                                                           <option value="NC">NC</option>
                                                           <option value="ND">ND</option>
                                                           <option value="MP">MP</option>
                                                           <option value="OH">OH</option>
                                                           <option value="OK">OK</option>
                                                           <option value="OR">OR</option>
                                                           <option value="PW">PW</option>
                                                           <option value="PA">PA</option>
                                                           <option value="PR">PR</option>
                                                           <option value="RI">RI</option>
                                                           <option value="SC">SC</option>
                                                           <option value="SD">SD</option>
                                                           <option value="TN">TN</option>
                                                           <option value="TX">TX</option>
                                                           <option value="UT">UT</option>
                                                           <option value="VT">VT</option>
                                                           <option value="VI">VI</option>
                                                           <option value="VA">VA</option>
                                                           <option value="WA">WA</option>
                                                           <option value="WV">WV</option>
                                                           <option value="WI">WI</option>
                                                           <option value="WY">WY</option>
                                                        </select>
                          </div>
                        
                        </div>
                              <div class="col-md-2 padleftzero" style="width:12%">
                                   <label class="control-label">County Name</label> <br/>
                                    
                                    <select name="county" id="county" class="form-control fsc-input">
                                         <option value="">--Select--</option> 
                                         <!--   <?php $__currentLoopData = $county; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $countys): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>-->
                                         <!--       <option value="<?php echo e($countys->county_name); ?>"><?php echo e($countys->county_name); ?></option> -->
                                         <!--   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>-->
                                    </select>
                              </div>
                      
                        
                        <div class="col-md-2 padleftzero" style="width:10.5%;">
                             <label class="control-label">County Code</label>
                                <input type="text" name="countycode" id="countycode" class="form-control" placeholder="County Code" style="width:90px;"  minlength="2" maxlength="6" readonly/> 
                             
                        </div>
                        
                         <div class="col-md-3 padleftzero" style="width:18%;">
                             <label class="control-label">County Authority Name</label>
                                <input type="text" name="county_authority_name" id="county_authority_name" class="form-control" placeholder="County Authority Name"/> 
                             
                        </div>
                        
                        <div class="col-md-4 padleftzero" style="width:31%">
                             <label class="control-label">County Website</label>
                            <input type="text" class="form-control" placeholder="County Website" id="county_website" name="county_website"/>
                        </div>
                        
                        <div class="col-md-2 padleftzero" style="width:16%">
                         <label class="control-label">County Telephone</label>
                          <input type="text" class="form-control" placeholder="County Telephone" id="county_telephone" name="county_telephone">
                        </div>
                        
                        <div class="clear"></div>
                        </div>
                         <div class="formbox mrbtmzero">
                            <div class="col-md-6" style="width:46%">
                                <label class="control-label">County Address</label>
                              <input type="text" class="form-control" placeholder="County Address" id="county_address" name="county_address"/>
                            </div>
                             <div class="col-md-2 padleftzero" style="width:16%">
                                  <label class="control-label">City</label>
                              <input type="text" class="form-control" placeholder="City" id="county_city" name="county_city"/>
                            </div>
                             <div class="col-md-2 padleftzero" style="width:10%">
                                  <label class="control-label">State</label>
                                <input type="text" class="form-control" placeholder="State" id="county_state" name="county_state" readonly/>
                            </div>
                            <div class="col-md-2 padleftzero" style="width:11%">
                                 <label class="control-label">Zip Code</label>
                                <input type="text" class="form-control" placeholder="ZIP Code" id="county_zip" name="county_zip"  minlength="5" maxlength="5"/>
                            </div>
                            <div class="col-md-2 padleftzero" style="width:15%">
                                 <label class="control-label">FAX</label>
                                <input type="text" class="form-control" placeholder="Fax" id="county_fax" name="county_fax"/> 

                            </div>
                             <div class="clear"></div>
                           
                        </div>
                
                <div style="border-bottom:1px solid #ccc; margin:10px 0px; width:100%; display:inline-block;"></div>
                <div class="clear"></div>
                <div class="col-md-12">
                    <div class="Branch subttl" style="background:#deeef7; border-color:#a3d3ef;">
                         <h4>Sales Tax Info :</h4>
                     </div>
                    <!--<div class="col-md-6">-->
                    <!-- <table class="table table-bordered table-striped" id="mytable">-->
                    <!--     <tr id="row1">-->
                    <!--         <th>Month</th>-->
                    <!--         <th>Year</th>-->
                    <!--         <th>Sales Tax Rate</th>-->
                    <!--         <th></th>-->
                    <!--     </tr>-->
                    <!--     <tr id="row2">-->
                    <!--         <td>-->
                    <!--             <div class="col-md-6">-->
                    <!--                <select name="month[]" id="month" class="form-control fsc-input" style="width:100px;">-->
                    <!--                                           <option value="">---Select month---</option>-->
                    <!--                                           <option value="01">01</option>-->
                    <!--                                           <option value="02">02</option>-->
                    <!--                                           <option value="03">03</option>-->
                    <!--                                           <option value="04">04</option>-->
                    <!--                                           <option value="05">05</option>-->
                    <!--                                           <option value="06">06</option>-->
                    <!--                                           <option value="07">07</option>-->
                    <!--                                           <option value="08">08</option>-->
                    <!--                                           <option value="09">09</option>-->
                    <!--                                           <option value="10">10</option>-->
                    <!--                                           <option value="11">11</option>-->
                    <!--                                           <option value="12">12</option>-->
                    <!--                                        </select>-->
                                    
                    <!--             </div>-->
                    <!--         </td>-->
                    <!--         <td>-->
                    <!--             <div class="col-md-6">-->
                    <!--                <select name="year[]" id="year" class="form-control fsc-input" style="width:100px;">-->
                    <!--                                           <option value="">---Select year---</option>-->
                    <!--                                           <option value="2020">2020</option>-->
                    <!--                                           <option value="2021">2021</option>-->
                    <!--                                           <option value="2022">2022</option>-->
                    <!--                                           <option value="2023">2023</option>-->
                    <!--                                           <option value="2024">2024</option>-->
                    <!--                                           <option value="2025">2025</option>-->
                    <!--                                        </select>-->
                                  
                    <!--             </div>-->
                    <!--         </td>-->
                    <!--         <td><input type="text" name="personal_rate[]" id="personal_rate" class="form-control txtinput_1"  placeholder="Rate"/></td>-->
                    <!--         <td> <div class="col-md-6"><input type="button" class="add-row btn btn-primary" value="+"></div></td>-->
                    <!--     </tr>-->
                    <!-- </table>-->
                    <!--</div>-->

                    <div class="col-md-10">
                                <table class="table table-bordered table-striped" id="mytable">
                                     <tr id="row1">
                                         <th>Month</th>
                                         <th>Year</th>
                                         <th>Sales Tax Rate</th>
                                         <th>Type</th>
                                         <th><div class="col-md-6"><input type="button" class="add-row btn btn-primary" value="+"></th>
                                     </tr>
                                     <tr id="row2">
                                         <td>
                                             <div class="col-md-6">
                                                <select name="county_tax_month[]" id="county_tax_month_1" class="form-control fsc-input" style="width:100px;">
                                                                           <option value="">---Select month---</option>
                                                                           <option value="01">01</option>
                                                                           <option value="02">02</option>
                                                                           <option value="03">03</option>
                                                                           <option value="04">04</option>
                                                                           <option value="05">05</option>
                                                                           <option value="06">06</option>
                                                                           <option value="07">07</option>
                                                                           <option value="08">08</option>
                                                                           <option value="09">09</option>
                                                                           <option value="10">10</option>
                                                                           <option value="11">11</option>
                                                                           <option value="12">12</option>
                                                                        </select>
                                                
                                             </div>
                                         </td>
                                         <td>
                                             <div class="col-md-6">
                                                <select name="county_tax_year[]" id="county_tax_year_1" class="form-control fsc-input" style="width:100px;">
                                                                           <option value="">---Select year---</option>
                                                                            <option value="2020">2020</option>
                                                                           <option value="2021">2021</option>
                                                                           <option value="2022">2022</option>
                                                                           <option value="2023">2023</option>
                                                                           <option value="2024">2024</option>
                                                                           <option value="2025">2025</option>
                                                                        </select>
                                              
                                             </div>
                                         </td>
                                         <td>
                                            <div class="col-md-3" style="width:100px;">
                                                <input type="text" name="county_tax_personal_rate[]" id="county_tax_personal_rate_1" class="form-control txtinput_1"  placeholder="Rate"/>
                                            </div>
                                         </td> 
                                         <td style="padding-top:0px!important; padding-right:0px!important;">
                                                <div class="salestaxckbox">
                                                   <table>
                                                       <tr>
                                                           <td>
                                                               <span>L</span>
                                                                <input type="checkbox" name="county_lost[]" id="county_lost_1" value="1">
                                                                <label for="county_lost_1" data-toggle="tooltip" data-placement="top" title="Lost"> </label> 
                                                            </td>
                                                            <td>
                                                                <span>E</span>
                                                                 <input type="checkbox" name="county_educational[]" id="county_educational_1" value="1">
                                                                <label for="county_educational_1" data-toggle="tooltip" data-placement="top" title="Educational"></label>
                                                            </td>
                                                            <td>
                                                                <span>S</span>
                                                                 <input type="checkbox" name="county_splost[]" id="county_splost_1" value="1">
                                                                <label for="county_splost_1" data-toggle="tooltip" data-placement="top" title="SPLOST"></label>
                                                            </td>
                                                             <td> 
                                                                <span>T</span>
                                                                 <input type="checkbox" name="county_tsplost[]" id="county_tsplost_1" value="1">
                                                                <label for="county_tsplost_1" data-toggle="tooltip" data-placement="top" title="TSPLOST1"></label>
                                                            </td>
                                                            <td>
                                                                <span>H</span>
                                                                 <input type="checkbox" name="county_host[]" id="county_host_1" value="1">
                                                                <label for="county_host_1" data-toggle="tooltip" data-placement="top" title="EHOST or EHOST"></label>
                                                            <td>
                                                            <td>
                                                                 <span>M</span>
                                                                 <input type="checkbox" name="county_marta[]" id="county_marta_1" value="1">
                                                                <label for="county_marta_1" data-toggle="tooltip" data-placement="top" title="MARTA"></label>
                                                            <td>
                                                            <td>
                                                                <span>O</span>
                                                                 <input type="checkbox" name="county_other[]" id="county_other_1" value="1">
                                                                <label for="county_other_1" data-toggle="tooltip" data-placement="top" title="Other"></label>
                                                            <td>
                                                            <td>
                                                                 <span>T2</span>
                                                                 <input type="checkbox" name="county_tsplost2[]" id="county_tsplost2_1" value="1">
                                                                <label for="county_tsplost2_1" data-toggle="tooltip" data-placement="top" title="TSPLOST2">&nbsp;</label>
                                                            <td>
                                                            <td>
                                                               <span>TF</span>
                                                                 <input type="checkbox" name="county_filtontf[]" id="county_filtontf_1" value="1">
                                                                <label for="county_filtontf_1" data-toggle="tooltip" data-placement="top" title="Fulton TSPLOST">&nbsp;</label>
                                                            <td>
                                                            <td>
                                                                <span>TA</span>
                                                                 <input type="checkbox" name="county_atlantata[]" id="county_atlantata_1" value="1">
                                                                <label for="county_atlantata_1" data-toggle="tooltip" data-placement="top" title="Atlanta TSPLOST">&nbsp;</label>
                                                            <td>
                                                        </tr>
                                                   </table>
                                                </div>
                                            </td>
                                         </td>
                                         
                                         <td> <div class="col-md-6"></div></td>
                                     </tr>
                                    
                                </table>
                            </div>
             

                </div>
                    <div class="clear"></div>
                    <div class="col-md-12">
                        <div class="Branch subttl" style="background:#deeef7; border-color:#a3d3ef;">
                             <h4 class="text-left padleft20 bold">Personal Property Tax Info </h4>
                        </div>
                        <div class="row" style="padding-left:15px;">
                                     <div class="col-md-2" style="width:13%">
                                          <label class="control-label">Form No.</label>
                                         <input type="text" class="form-control" placeholder="Form No" id="personal_form_number" name="personal_form_number" minlength="2" maxlength="50"/>
                                     </div>
                                     <div class="col-md-3 padleftzero" style="width:27%;">
                                          <label class="control-label">Form Name</label>
                                         <input type="text" class="form-control" placeholder="Form Name" id="personal_form_name" name="personal_form_name"  minlength="2" maxlength="50"/>
                                     </div>
                                     
                                      <div class="col-md-2 padleftzero" style="width:100px;">
                                           <label class="control-label">Tax Year</label>
                                        <select name="personal_tax_year" id="personal_tax_year" class="form-control fsc-input">
                                            <option value="">---Select year---</option>
                                            <option value="2020">2020</option>
                                            <option value="2021">2021</option>
                                            <option value="2022">2022</option>
                                            <option value="2023">2023</option>
                                            <option value="2024">2024</option>
                                            <option value="2025">2025</option>
                                        </select>
                                      </div>
                                     
                                      <div class="col-md-3 padleftzero" style="width:250px;">
                                           <label class="control-label">Filling Frequency</label>
                                         <input type="text" class="form-control" name="personal_filling_frequency" id="personal_filling_frequency" value="Annually" readonly>
                                         <!--       <option value="">Select</option>-->
                                         <!--       <option value="Annually">Annually</option>-->
                                         <!--       <option value="Quarterly">Quarterly</option>-->
                                         <!--       <option value="Monthly">Monthly</option>-->
                                                
                                         <!--</select>-->
                                     </div>
                                    
                                     <div class="col-md-2 padleftzero" style="width:12%;">
                                         <label class="control-label">Due Date</label>
                                        <input type="text" class="form-control" id="personal_due_date" name="personal_due_date" value="Apr-01-2021" placeholder="mm-dd-yyyy" readonly/>
                                     </div>
                            
                                     <div class="col-md-2 padleftzero" style="width:14%;">
                                         <label class="control-label">Telephone</label>
                                            <input type="text" class="form-control" placeholder="Telephone" id="personal_telephone" name="personal_telephone" />
                                     </div>
                                </div>
                                
                                
                                           
                                <div class="row" style="padding-left:15px; margin-top:15px; margin-bottom:15px;">
                                    <div class="col-md-2" >
                                        <label class="control-label">Person Name</label>
                                      <input type="text" class="form-control" placeholder="Contact Person Name" id="personal_contact_name" name="personal_contact_name"   minlength="2" maxlength="50"/>
                                    </div>
                                    <div class="col-md-2 padleftzero" style="width:14%;">
                                        <label class="control-label">Contact No.</label>
                                        <input type="text" class="form-control" placeholder="Contact Telephone" id="personal_secondary_telephone" name="personal_secondary_telephone" />
                                    </div>
                                    <div class="col-md-4 padleftzero" style="width:25%;">
                                        <label class="control-label">Email</label>
                                        <input type="email" class="form-control" placeholder="Email" name="personal_email"/>
                                    </div>
                                     <div class="col-md-3 padleftzero" style="width:22%;">
                                         <label>Website</label> 
                                    <input type="text" class="form-control" placeholder="Website url" name="property_website" id="property_website"/>
                                </div>
                                    <div class="col-md-2 padleftzero" style="width:10%;">
                                        
                                        <label>File Online</label> 
                                       
                                            <select class="form-control" name="personal_file">
                                                <option value="">---Select---</option>
                                                <option value="Yes">Yes</option>
                                                <option value="No">No</option>
                                            </select>
                                      
                                    </div>
                                   
                                 <div class="col-md-2 padleftzero" style="width:11%;">
                                        <label>Payment Online</label> 
                                            <select class="form-control" name="personal_payment_online">
                                                <option value="">---Select---</option>
                                                <option value="Yes">Yes</option>
                                                <option value="No">No</option>
                                            </select>
                                </div>
                                   
                                </div>
                                <div class="clear"></div>
                                
                                
                               
                             
                              <div class="clear"></div>
                                <label class="control-label col-md-1" style="margin-left:10px;">Note :</label>
                                <div class="col-md-10" style="padding-right:0px; width:89.2%;padding-bottom:15px !important;">
                                    <input type="text" name="personal_tax_note" id="personal_tax_note"  class="form-control"  placeholder="Note" /> 
                                </div>
                                <br>
                            </div>
                                   
                   
                  <div class="clear"></div>
                   <div class="col-md-12">
                    <div class="Branch subttl" style="background:#deeef7; border-color:#a3d3ef;">
                         <h4 class="text-left padleft20 bold">Property Tax Info </h4>
                     </div>
                 <div>
                      <div class="row" style="padding-left:15px;">
                            <div class="col-md-2" style="width:13%;">
                                <label >Form No.</label> 
                                 <input type="text" class="form-control" placeholder="Form No." id="property_form_number" name="property_form_number" />
                            </div>
                            <div class="col-md-3 padleftzero" style="width:27%;">
                                <label >Form Name</label> 
                                 <input type="text" class="form-control" placeholder="Form Name" id="property_form_name" name="property_form_name"   minlength="2" maxlength="50"/>
                            </div>
                            
                           
                            
                            
                             <div class="col-md-3 padleftzero" style="width:140px;">
                                 <label >Filling Frequency</label> 
                                 <select class="form-control" name="property_filling_frequency" id="property_filling_frequency">
                                        <option value="">Select</option>
                                        <option value="Annually">Annually</option>
                                        <option value="Quarterly">Quarterly</option>
                                        <option value="Monthly">Monthly</option>
                                        
                                 </select>
                             </div>
                            
                             <div class="col-md-2 padleftzero"  style="width:12%;">
                                  <label >Due Date</label>
                                 <input type="text" class="form-control" id="property_due_date" name="property_due_date" placeholder="mm-dd-yyyy" readOnly/>
                            </div>
                            
                            <div class="col-md-2 padleftzero"  style="width:14%;">
                                 <label >Telephone</label>
                                 <input type="text" class="form-control" placeholder="Telephone" id="property_telephone" name="property_telephone"/>
                            </div>
                             <div class="col-md-3 padleftzero" style="width:20%;">
                                  <label >Person Name</label>
                                 <input type="text" class="form-control" placeholder="Contact Person Name" id="property_contact_name" name="property_contact_name"  minlength="2" maxlength="50"/>
                             </div>
                        </div>
                        <div class="clear"></div>
                        
                        <div class="row" style="padding-left:15px; margin-top:15px; margin-bottom:15px;">
                            
                             
                             <div class="col-md-2" style="width:20%;">
                                  <label >Contact No.</label>
                                 <input type="text" class="form-control" placeholder="Contact Telephone" id="property_secondary_telephone" name="property_secondary_telephone" />
                             </div>
                             
                             <div class="col-md-3 padleftzero" style="width:31%;">
                              <label >Email</label>
                                 <input type="email" class="form-control" placeholder="Email" name="property_email" />
                           
                        </div>
                              <div class="col-md-3 padleftzero">
                             <label >Website</label>
                            <input type="text" class="form-control" placeholder="Website url" name="property_website" id="property_website" />
                        </div>
                        <div class="col-md-3 padleftzero" style="width:23%;">
                            <label>File Online</label> 
                                <select class="form-control" name="property_file">
                                    <option value="">---Select---</option>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                        </div>
                        
                             
                        <div class="clear"></div>
                       
                    
                         </div>
                         
                        <div class="row" style="padding-left:15px; margin-top:15px; margin-bottom:15px;">
                            <div class=" col-md-12">
                            <label class="control-label col-md-1" style="margin-left:10px;">Note :</label>
                            <div class="col-md-10" style="padding-right:0px; width:89.5%;">
                                <input type="text" name="property_tax_note" id="property_tax_note" class="form-control"  placeholder="Note" /> 
                            </div>
                            </div>
    	                </div>
    	                   
    	               
               

                     <div class="clear"></div>
                        
                          <div class="col-md-12">
                                <div class="Branch subttl" style="background:#f7e4ba; border-color:#e0c588;">
                                    <h4 class="text-left padleft20 bold">Business Licence Information</h4>
                                </div>
                                <div class="row" style="padding-left:15px;">
                                     <div class="col-md-2" style="width:13%">
                                          <label>Form No.</label> 
                                         <input type="text" class="form-control" placeholder="Form No" id="licence_form_number" name="licence_form_number"  minlength="2" maxlength="50"/>
                                     </div>
                                     <div class="col-md-3 padleftzero" style="width:27%;">
                                         <label>Form Name</label> 
                                         <input type="text" class="form-control" placeholder="Form Name" id="licence_form_name" name="licence_form_name"   minlength="2" maxlength="50"/>
                                     </div>
                                     
                                      <div class="col-md-2 padleftzero" style="width:100px;">
                                           <label>Tax Year</label> 
                                        <select name="licence_tax_year" id="licence_tax_year" class="form-control fsc-input">
                                            <option value="">---Select year---</option>
                                            <option value="2020">2020</option>
                                            <option value="2021">2021</option>
                                            <option value="2022">2022</option>
                                            <option value="2023">2023</option>
                                            <option value="2024">2024</option>
                                            <option value="2025">2025</option>
                                        </select>
                                      </div>
                                     
                                      <div class="col-md-3 padleftzero" style="width:240px;">
                                           <label>Filling Frequency</label> 
                                         <select class="form-control" name="licence_filling_frequency" id="licence_filling_frequency">
                                                <option value="">Select</option>
                                                <option value="Annually">Annually</option>
                                                <option value="Quarterly">Quarterly</option>
                                                <option value="Monthly">Monthly</option>
                                                
                                         </select>
                                     </div>
                                   
                                     <div class="col-md-2 padleftzero" style="width:12%;">
                                          <label>Due Date</label> 
                                
                                        <input type="text" class="form-control" id="licence_due_date" name="licence_due_date"  placeholder="mm-dd-yyyy" readOnly/>
                                     </div>
                            
                                     <div class="col-md-2 padleftzero" style="width:14%;">
                                         <label>Telephone</label> 
                                            <input type="text" class="form-control" placeholder="Telephone" id="licence_telephone" name="licence_telephone" />
                                     </div>
                                </div>
                        
                                <div class="clear"></div>
                                <div class="row" style="padding-left:15px; margin-top:15px; margin-bottom:15px;">
                                    <div class="col-md-2" >
                                        <label>Person Name</label> 
                                      <input type="text" class="form-control" placeholder="Contact Person Name" id="licence_contact_name" name="licence_contact_name"   minlength="2" maxlength="50"/>
                                    </div>
                                    
                                    <div class="col-md-2 padleftzero" style="width:14%;">
                                        <label>Contact No.</label> 
                                        <input type="text" class="form-control" placeholder="Contact Telephone" id="licence_secondary_telephone" name="licence_secondary_telephone" />
                                    </div>
                                    
                                    <div class="col-md-4 padleftzero" style="width:22%;">
                                        <label>Email</label> 
                                        <input type="email" class="form-control" placeholder="Email" name="licence_email" />
                                    </div>
                                    
                                    
                                     <div class="col-md-3 padleftzero" style="width:22%;">
                                    <label>Website</label> 
                                    <input type="text" class="form-control" placeholder="Website url" name="licence_website" id="licence_website" />
                                </div>
                                <div class="col-md-2 padleftzero" style="width:12%;">
                                        <label>File Online</label> 
                                        
                                            <select class="form-control" name="licence_file" id="licence_file">
                                                <option value="">---Select---</option>
                                                <option value="Yes">Yes</option>
                                                <option value="No">No</option>
                                            </select>
                                       
                                    </div>
                                     <div class="col-md-2 padleftzero" style="width:13%;">
                                        <label >Payment Online</label> 
                                            <select class="form-control" name="licence_payment_online" id="licence_payment_online">
                                                <option value="">---Select---</option>
                                                <option value="Yes">Yes</option>
                                                <option value="No">No</option>
                                            </select>
                                       
                                </div>
                                   
                                </div>
                                
                               
                                
                               
                             
                              <div class="clear"></div>
                                <label class="control-label col-md-1" style="margin-left:10px;margin-top:5px;text-align:right;">Note :</label>
                                <div class="col-md-11" style="padding-right:0px; width:89.2%;padding-bottom:15px !important;">
                                    <input type="text" name="licence_note" id="licence_note"  class="form-control"  placeholder="Note" /> 
                                </div>
                                <br>
                    
                    
                     </div>	 
                    
                  <div class="card-footer">
                   <div class="col-md-2 col-md-offset-3">
									<input class="btn_new_save btn-primary1" style="margin-left:-5%" type="submit" name="submit" value="Save">
									</div>
									<div class="col-md-2 row">
									<a class="btn_new_cancel" href="<?php echo e(url('fac-Bhavesh-0554/taxstate')); ?>">Cancel</a> 
									</div>
                  </div>
               </form>
            </div>
         </div>
      </div>
      </div>
   </div>
     </section>
</div>
<!--<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>-->

<script>

      $('document').ready(function(){
         $('#state_county_first').on('change', function(){
            var statecounty = $("#state_county_first").val(); 
            var fullcounty=statecounty;
            console.log(fullcounty);           
            $.get('<?php echo URL::to('getCountytaxstate'); ?>?filecounty='+fullcounty, function(data)
            {  
                //console.log(data);
                if(data == "")
                {
                    
                }
                else
                {
                    $('#county').html(data);
                  
                }
                

                
            });
            
        });
 
    });
</script>

<script>

      $('document').ready(function(){
         $('#county').on('change', function(){
            var id1 = $("#county").val(); 
            var fullid=id1;
            console.log(fullid);           
            $.get('<?php echo URL::to('getCountycodetax'); ?>?filename='+fullid, function(data)
            {  
                console.log(data);
                if(data == "")
                {
                    
                }
                else
                {
                    $('#countycode').val(data.county_code);
                }
                
               
            
                
            });
            
        });
 
    });
</script>

<script type="text/javascript">

$(document).ready(function()
 {
    
  (function($) {
    var minNumber = -100;
    var maxNumber = 100;
      $('.spinner .btn:first-of-type').on('click', function() {
        if ($('.spinner input').val() == maxNumber) {
          return false;
        } else {
          $('.spinner input').val(parseInt($('.spinner input').val(), 10) + 5 + '%');
        }
      });

  $('.txtinput_1').on("blur", function() {
    var inputVal = parseFloat($(this).val().replace('%', '')) || 0
    if (minNumber > inputVal) {
      inputVal = -100;
    } else if (maxNumber < inputVal) {
      inputVal = 100;
    }
    $(this).val(inputVal + '.00%');
  });

      $('.spinner .btn:last-of-type').on('click', function() {
        if ($('.spinner input').val() == minNumber) {
          return false;
        } else {
          $('.spinner input').val(parseInt($('.spinner input').val(), 10) - 5 + '%');
        }
      });
})(jQuery);
  
    //county_tax_month,county_tax_year,county_tax_personal_rate
    
        var k=1;
        $(".add-row").click(function(){
            k++;
          //  var markup = "<tr><td><div class='col-md-10'><select name='county_tax_month[]' id='county_tax_month' class='form-control fsc-input' style='width:100px;'><option>---Select month---</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option></select><div></td><td><div class='col-md-6'><select name='county_tax_year[]' id='county_tax_year' class='form-control fsc-input' style='width:100px;'><option>---Select year---</option><option value='2020'>2020</option><option value='2021'>2021</option><option value='2022'>2022</option><option value='2023'>2023</option><option value='2024'>2024</option><option value='2025'>2025</option></select></div></td><td><div class='col-md-3' style='width:100px;'><input type='text' name='county_tax_personal_rate[]' id='county_tax_personal_rate' class='form-control txtinput_"+k+"'  placeholder='Rate'/><td style='padding-top:0px!important; padding-right:0px!important;'><div class='salestaxckbox'><table><tr><td><span>L</span><input type='checkbox' name='county_lost["+k+"]' id='county_lost["+k+"]' value='1'><label for='county_lost["+k+"]' data-toggle='tooltip' data-placement='top' title='Lost'> </label></td><td><span>E</span><input type='checkbox' name='county_educational["+k+"]' id='county_educational["+k+"]' value='1'><label for='county_educational["+k+"]' data-toggle='tooltip' data-placement='top' title='Educational'></label></td><td><span>S</span><input type='checkbox' name='county_splost["+k+"]' id='county_splost["+k+"]' value='1'><label for='county_splost["+k+"]' data-toggle='tooltip' data-placement='top' title='SPLOST'></label></td><td><span>T</span><input type='checkbox' name='county_tsplost["+k+"]' id='county_tsplost["+k+"]' value='1'><label for='county_tsplost["+k+"]' data-toggle='tooltip' data-placement='top' title='TSPLOST1'></label></td><td><span>H</span><input type='checkbox' name='county_host["+k+"]' id='county_host["+k+"]' value='1'><label for='county_host["+k+"]' data-toggle='tooltip' data-placement='top' title='EHOST or EHOST'></label><td><td><span>M</span><input type='checkbox' name='county_marta["+k+"]' id='county_marta["+k+"]' value='1'><label for='county_marta["+k+"]' data-toggle='tooltip' data-placement='top' title='MARTA'></label><td><td><span>O</span><input type='checkbox' name='county_other["+k+"]' id='county_other["+k+"]' value='1'><label for='county_other["+k+"]' data-toggle='tooltip' data-placement='top' title='Other'></label><td><td><span>T2</span><input type='checkbox' name='county_tsplost2["+k+"]' id='county_tsplost2["+k+"]' value='1'><label for='county_tsplost2["+k+"]' data-toggle='tooltip' data-placement='top' title='TSPLOST2'>&nbsp;</label><td><td><span>TF</span><input type='checkbox' name='county_filtontf["+k+"]' id='county_filtontf["+k+"]' value='1'><label for='county_filtontf["+k+"]' data-toggle='tooltip' data-placement='top' title='Fulton TSPLOST'>&nbsp;</label><td><td><span>TA</span><input type='checkbox' name='county_atlantata["+k+"]' id='county_atlantata["+k+"]' value='1'><label for='county_atlantata["+k+"]' data-toggle='tooltip' data-placement='top' title='Atlanta TSPLOST'>&nbsp;</label><td></tr></table></div></div></td><td><div class='col-md-6'> <button type='button' class='delete-row btn btn-danger' onClick='deleterow();'>-</button></div></td></tr>";
           markup = "<tr><td><div class='col-md-10'><select name='county_tax_month[]' id='county_tax_month_"+k+"' class='form-control fsc-input' style='width:100px;'><option>---Select month---</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option></select><div></td><td><div class='col-md-6'><select name='county_tax_year[]' id='county_tax_year_"+k+"' class='form-control fsc-input' style='width:100px;'><option>---Select year---</option><option value='2020'>2020</option><option value='2021'>2021</option><option value='2022'>2022</option><option value='2023'>2023</option><option value='2024'>2024</option><option value='2025'>2025</option></select></div></td><td><div class='col-md-3' style='width:100px;'><input type='text' name='county_tax_personal_rate[]' id='county_tax_personal_rate_"+k+"' class='form-control txtinput_"+k+"'  placeholder='Rate'/><td style='padding-top:0px!important; padding-right:0px!important;'><div class='salestaxckbox'><table><tr><td><span>L</span><input type='checkbox' name='county_lost[]' id='county_lost_"+k+"' value='1'><label for='county_lost_"+k+"' data-toggle='tooltip' data-placement='top' title='Lost'> </label></td><td><span>E</span><input type='checkbox' name='county_educational[]' id='county_educational_"+k+"' value='1'><label for='county_educational_"+k+"' data-toggle='tooltip' data-placement='top' title='Educational'></label></td><td><span>S</span><input type='checkbox' name='county_splost[]' id='county_splost_"+k+"' value='1'><label for='county_splost_"+k+"' data-toggle='tooltip' data-placement='top' title='SPLOST'></label></td><td><span>T</span><input type='checkbox' name='county_tsplost[]' id='county_tsplost_"+k+"' value='1'><label for='county_tsplost_"+k+"' data-toggle='tooltip' data-placement='top' title='TSPLOST1'></label></td><td><span>H</span><input type='checkbox' name='county_host[]' id='county_host_"+k+"' value='1'><label for='county_host_"+k+"' data-toggle='tooltip' data-placement='top' title='EHOST or EHOST'></label><td><td><span>M</span><input type='checkbox' name='county_marta[]' id='county_marta_"+k+"' value='1'><label for='county_marta_"+k+"' data-toggle='tooltip' data-placement='top' title='MARTA'></label><td><td><span>O</span><input type='checkbox' name='county_other[]' id='county_other_"+k+"' value='1'><label for='county_other_"+k+"' data-toggle='tooltip' data-placement='top' title='Other'></label><td><td><span>T2</span><input type='checkbox' name='county_tsplost2[]' id='county_tsplost2_"+k+"' value='1'><label for='county_tsplost2_"+k+"' data-toggle='tooltip' data-placement='top' title='TSPLOST2'>&nbsp;</label><td><td><span>TF</span><input type='checkbox' name='county_filtontf[]' id='county_filtontf_"+k+"' value='1'><label for='county_filtontf_"+k+"' data-toggle='tooltip' data-placement='top' title='Fulton TSPLOST'>&nbsp;</label><td><td><span>TA</span><input type='checkbox' name='county_atlantata[]' id='county_atlantata_"+k+"' value='1'><label for='county_atlantata_"+k+"' data-toggle='tooltip' data-placement='top' title='Atlanta TSPLOST'>&nbsp;</label><td></tr></table></div></div></td><td><div class='col-md-6'> <button type='button' class='delete-row btn btn-danger' onClick='deleterow();'>-</button></div></td></tr>";
            $("#mytable").append(markup);
            var minNumber = -100;
            var maxNumber = 100;
            $('.txtinput_'+k).on("blur", function() {
                var inputVal = parseFloat($(this).val().replace('%', '')) || 0
                if (minNumber > inputVal)
                {
                    inputVal = -100;
                } else if (maxNumber < inputVal) {
                    inputVal = 100;
                }
                $('.txtinput_'+k).val(inputVal + '.00%');
            });
        });
    }); 
    
   
      $("body").on("click",".delete-row",function(){ 
        $(this).parents("tr").remove();
    });
    



</script>



<script type="text/javascript">
    $(document).ready(function()
    {
        
       
        
        $('#state_county_first').on('change', function() 
        {
            var state=$("#state_county_first").val();
            $("#county_state").val(state);
        });
 
        $('#personal_filling_frequency').on('change', function() 
        {
            if(this.value == 'Monthly')
            {
                if(this.value == 'Monthly' && 'Jan'=='<?php echo date("M");?>')
                {
                    $("#personal_due_date").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Feb'=='<?php echo date("M");?>')
                {
                    $("#personal_due_date").val('<?php echo date("M",strtotime("01 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Mar'=='<?php echo date("M");?>')
                {
                    $("#personal_due_date").val('<?php echo date("M",strtotime("01 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Apr'=='<?php echo date("M");?>')
                {
                    $("#personal_due_date").val('<?php echo date("M",strtotime("01 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'May'=='<?php echo date("M");?>')
                {
                    $("#personal_due_date").val('<?php echo date("M",strtotime("01 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Jun'=='<?php echo date("M");?>')
                {
                    $("#personal_due_date").val('<?php echo date("M",strtotime("01 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Jul'=='<?php echo date("M");?>')
                {
                    $("#personal_due_date").val('<?php echo date("M",strtotime("01 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Aug'=='<?php echo date("M");?>')
                {
                    $("#personal_due_date").val('<?php echo date("M",strtotime("01 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Sep'=='<?php echo date("M");?>')
                {
                    $("#personal_due_date").val('<?php echo date("M",strtotime("01 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Oct'=='<?php echo date("M");?>')
                {
                    $("#personal_due_date").val('<?php echo date("M",strtotime("01 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Nov'=='<?php echo date("M");?>')
                {
                    $("#personal_due_date").val('<?php echo date("M",strtotime("01 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Dec'=='<?php echo date("M");?>')
                {
                    $("#personal_due_date").val('<?php echo date("M",strtotime("01 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
            }
            else if(this.value == 'Quarterly')
            {  
                if(this.value == 'Quarterly' && 'Jan'=='<?php echo date("M");?>')
                {
                    $("#personal_due_date").val('<?php echo date("M",strtotime("01 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'Feb'=='<?php echo date("M");?>')
                {
                    $("#personal_due_date").val('<?php echo date("M",strtotime("01 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'Mar'=='<?php echo date("M");?>')
                {
                    $("#personal_due_date").val('<?php echo date("M",strtotime("01 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'Apr'=='<?php echo date("M");?>')
                {
                    $("#personal_due_date").val('<?php echo date("M",strtotime("4 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'May'=='<?php echo date("M");?>')
                {
                    $("#personal_due_date").val('<?php echo date("M",strtotime("4 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'Jun'=='<?php echo date("M");?>')
                {
                    $("#personal_due_date").val('<?php echo date("M",strtotime("4 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'Jul'=='<?php echo date("M");?>')
                {   
                    $("#personal_due_date").val('<?php echo date("M",strtotime("7 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'Aug'=='<?php echo date("M");?>')
                {  
                    $("#personal_due_date").val('<?php echo date("M",strtotime("7 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'Sep'=='<?php echo date("M");?>')
                {
                    $("#personal_due_date").val('<?php echo date("M",strtotime("7 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'Oct'=='<?php echo date("M");?>')
                {
                     $("#personal_due_date").val('<?php echo "Jan".'-'."01".'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
                }
                else if(this.value == 'Quarterly' && 'Nov'=='<?php echo date("M");?>')
                {
                     $("#personal_due_date").val('<?php echo "Jan".'-'."01".'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
                }
                else if(this.value == 'Quarterly' && 'Dec'=='<?php echo date("M");?>')
                {
                     $("#personal_due_date").val('<?php echo "Jan".'-'."01".'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
                }
            }
            else if(this.value == 'Annually')
            { 
                $("#personal_due_date").val('<?php echo "Jan".'-'."01".'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
            }
        });
        
        
        $('#property_filling_frequency').on('change', function() 
        {
        
            if(this.value == 'Monthly')
            {
                if(this.value == 'Monthly' && 'Jan'=='<?php echo date("M");?>')
                {
                    $("#property_due_date").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Feb'=='<?php echo date("M");?>')
                {
                    $("#property_due_date").val('<?php echo date("M",strtotime("01 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Mar'=='<?php echo date("M");?>')
                {
                    $("#property_due_date").val('<?php echo date("M",strtotime("01 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Apr'=='<?php echo date("M");?>')
                {
                    $("#property_due_date").val('<?php echo date("M",strtotime("01 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'May'=='<?php echo date("M");?>')
                {
                    $("#property_due_date").val('<?php echo date("M",strtotime("01 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Jun'=='<?php echo date("M");?>')
                {
                    $("#property_due_date").val('<?php echo date("M",strtotime("01 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Jul'=='<?php echo date("M");?>')
                {
                    $("#property_due_date").val('<?php echo date("M",strtotime("01 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Aug'=='<?php echo date("M");?>')
                {
                    $("#property_due_date").val('<?php echo date("M",strtotime("01 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Sep'=='<?php echo date("M");?>')
                {
                    $("#property_due_date").val('<?php echo date("M",strtotime("01 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Oct'=='<?php echo date("M");?>')
                {
                    $("#property_due_date").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Nov'=='<?php echo date("M");?>')
                {
                    $("#property_due_date").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Dec'=='<?php echo date("M");?>')
                {
                    $("#property_due_date").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
            }
            else if(this.value == 'Quarterly')
            {  
                if(this.value == 'Quarterly' && 'Jan'=='<?php echo date("M");?>')
                {
                    $("#property_due_date").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'Feb'=='<?php echo date("M");?>')
                {
                    $("#property_due_date").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'Mar'=='<?php echo date("M");?>')
                {
                    $("#property_due_date").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'Apr'=='<?php echo date("M");?>')
                {
                    $("#property_due_date").val('<?php echo date("M",strtotime("4 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'May'=='<?php echo date("M");?>')
                {
                    $("#property_due_date").val('<?php echo date("M",strtotime("4 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'Jun'=='<?php echo date("M");?>')
                {
                    $("#property_due_date").val('<?php echo date("M",strtotime("4 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'Jul'=='<?php echo date("M");?>')
                {   
                    $("#property_due_date").val('<?php echo date("M",strtotime("7 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'Aug'=='<?php echo date("M");?>')
                {  
                    $("#property_due_date").val('<?php echo date("M",strtotime("7 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'Sep'=='<?php echo date("M");?>')
                {
                    $("#property_due_date").val('<?php echo date("M",strtotime("7 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'Oct'=='<?php echo date("M");?>')
                {
                     $("#property_due_date").val('<?php echo "Jan".'-'."01".'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
                }
                else if(this.value == 'Quarterly' && 'Nov'=='<?php echo date("M");?>')
                {
                     $("#property_due_date").val('<?php echo "Jan".'-'."01".'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
                }
                else if(this.value == 'Quarterly' && 'Dec'=='<?php echo date("M");?>')
                {
                     $("#property_due_date").val('<?php echo "Jan".'-'."01".'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
                }
            }
            else if(this.value == 'Annually')
            { 
                $("#property_due_date").val('<?php echo "Jan".'-'."01".'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
            }
        });
        
        
         
        $('#licence_filling_frequency').on('change', function() 
        {
        
            if(this.value == 'Monthly')
            {
                if(this.value == 'Monthly' && 'Jan'=='<?php echo date("M");?>')
                {
                    $("#licence_due_date").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Feb'=='<?php echo date("M");?>')
                {
                    $("#licence_due_date").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Mar'=='<?php echo date("M");?>')
                {
                    $("#licence_due_date").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Apr'=='<?php echo date("M");?>')
                {
                    $("#licence_due_date").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'May'=='<?php echo date("M");?>')
                {
                    $("#licence_due_date").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Jun'=='<?php echo date("M");?>')
                {
                    $("#licence_due_date").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Jul'=='<?php echo date("M");?>')
                {
                    $("#licence_due_date").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Aug'=='<?php echo date("M");?>')
                {
                    $("#licence_due_date").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Sep'=='<?php echo date("M");?>')
                {
                    $("#licence_due_date").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Oct'=='<?php echo date("M");?>')
                {
                    $("#licence_due_date").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Nov'=='<?php echo date("M");?>')
                {
                    $("#licence_due_date").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Monthly' && 'Dec'=='<?php echo date("M");?>')
                {
                    $("#licence_due_date").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
            }
            else if(this.value == 'Quarterly')
            {  
                if(this.value == 'Quarterly' && 'Jan'=='<?php echo date("M");?>')
                {
                    $("#licence_due_date").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'Feb'=='<?php echo date("M");?>')
                {
                    $("#licence_due_date").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'Mar'=='<?php echo date("M");?>')
                {
                    $("#licence_due_date").val('<?php echo date("M",strtotime("1 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'Apr'=='<?php echo date("M");?>')
                {
                    $("#licence_due_date").val('<?php echo date("M",strtotime("4 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'May'=='<?php echo date("M");?>')
                {
                    $("#licence_due_date").val('<?php echo date("M",strtotime("4 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'Jun'=='<?php echo date("M");?>')
                {
                    $("#licence_due_date").val('<?php echo date("M",strtotime("4 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'Jul'=='<?php echo date("M");?>')
                {   
                    $("#licence_due_date").val('<?php echo date("M",strtotime("7 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'Aug'=='<?php echo date("M");?>')
                {  
                    $("#licence_due_date").val('<?php echo date("M",strtotime("7 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'Sep'=='<?php echo date("M");?>')
                {
                    $("#licence_due_date").val('<?php echo date("M",strtotime("7 month",strtotime(date("M")))).'-'."01".'-'.date("Y");?>');
                }
                else if(this.value == 'Quarterly' && 'Oct'=='<?php echo date("M");?>')
                {
                     $("#licence_due_date").val('<?php echo "Jan".'-'."01".'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
                }
                else if(this.value == 'Quarterly' && 'Nov'=='<?php echo date("M");?>')
                {
                     $("#licence_due_date").val('<?php echo "Jan".'-'."01".'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
                }
                else if(this.value == 'Quarterly' && 'Dec'=='<?php echo date("M");?>')
                {
                     $("#licence_due_date").val('<?php echo "Jan".'-'."01".'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
                }
            }
            else if(this.value == 'Annually')
            { 
                $("#licence_due_date").val('<?php echo "Jan".'-'."01".'-'.date("Y",strtotime("1 year",strtotime(date("Y"))));?>');
            }
        });
    });   
        

</script>


<script type="text/javascript">
//     $(document).ready(function()
//     {
// 		$("#form_valid").validate(
// 		{
// 				rules:{
// 						state: {
// 							required: true,
// 								},
// 						county: {
// 							required: true,
// 								},		
// 						countycode: {
// 							required: true,
// 								},
// 						personal_website: {
// 							//required: true,
// 							url: true,
// 								},
// 						personal_telephone: {
// 							required: true,
// 								},
// 						personal_secondary_telephone: {
// 							required: true,
// 								},
// 						personal_email: {
// 							required: true,
// 								},
// 						property_website: {
// 							//required: true,
// 								url: true,
// 								},
// 						property_telephone: {
// 							required: true,
// 								},
// 						property_secondary_telephone: {
// 							required: true,
// 								},
// 						property_email: {
// 							required: true,
// 								},
								
// 					},
// 				messages:{
// 						state: {
// 								required: "Please sslect state",
// 								},
// 						county: {
// 								required: "Please enter a county",
// 								},
// 						countycode: {
// 								required: "Please enter a county code",
// 								},
// 					    personal_website: {
// 								required: "Please enter a website",
// 								},
// 						personal_telephone: {
// 								required: "Please enter a telephone",
// 						        },
// 						personal_secondary_telephone: {
// 								required: "Please enter a telephone",
// 								},
// 						personal_email: {
// 								required: "Please enter a email",
// 								},
// 						property_website: {
// 								required: "Please enter a website",
// 								},
// 						property_telephone: {
// 								required: "Please enter a telephone",
// 								},
// 						property_secondary_telephone: {
// 								required: "Please enter a telephone",
// 								},
// 						property_email: {
// 								required: "Please enter a email",
// 								},
// 			            }
// 		});

//   });			

</script>

<script>
//  $(document).ready(function()
//  {
//   (function($) {
//     var minNumber = -100;
//     var maxNumber = 100;
//       $('.spinner .btn:first-of-type').on('click', function() {
//         if ($('.spinner input').val() == maxNumber) {
//           return false;
//         } else {
//           $('.spinner input').val(parseInt($('.spinner input').val(), 10) + 5 + '%');
//         }
//       });

//   $('.txtinput_1').on("blur", function() {
//     var inputVal = parseFloat($(this).val().replace('%', '')) || 0
//     if (minNumber > inputVal) {
//       inputVal = -100;
//     } else if (maxNumber < inputVal) {
//       inputVal = 100;
//     }
//     $(this).val(inputVal + '.00%');
//   });

//       $('.spinner .btn:last-of-type').on('click', function() {
//         if ($('.spinner input').val() == minNumber) {
//           return false;
//         } else {
//           $('.spinner input').val(parseInt($('.spinner input').val(), 10) - 5 + '%');
//         }
//       });
// })(jQuery);
  
   
    
//         var k=1;
//         $(".add-row").click(function(){
//             k++;
//             var markup = "<tr><td><div class='col-md-6'><select name='month[]' id='month' class='form-control fsc-input' style='width:100px;'><option>---Select month---</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option></select><div></td><td><div class='col-md-6'><select name='year[]' id='year' class='form-control fsc-input' style='width:100px;'><option>---Select year---</option><option value='2020'>2020</option><option value='2021'>2021</option><option value='2022'>2022</option><option value='2023'>2023</option><option value='2024'>2024</option><option value='2025'>2025</option></select></div></td><td><input type='text' name='personal_rate[]' id='personal_rate' class='form-control txtinput_"+k+"'  placeholder='Rate'/></td><td><div class='col-md-6'> <button type='button' class='delete-row btn btn-danger' onClick='deleterow();'>-</button></div></td></tr>";
//             $("#mytable").append(markup);
//             var minNumber = -100;
//             var maxNumber = 100;
//             $('.txtinput_'+k).on("blur", function() {
//                 var inputVal = parseFloat($(this).val().replace('%', '')) || 0
//                 if (minNumber > inputVal)
//                 {
//                     inputVal = -100;
//                 } else if (maxNumber < inputVal) {
//                     inputVal = 100;
//                 }
//                 $('.txtinput_'+k).val(inputVal + '.00%');
//             });
//         });
//     }); 
    
   
//       $("body").on("click",".delete-row",function(){ 
//         $(this).parents("tr").remove();
//     });
    
    
   
    


    $(document).ready(function() 
    {
          var dateInput = $('input[name="personal_due_date"]'); 
          var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : 'body';
          dateInput.datepicker({
            format: 'dd-M',
            container: container,
            todayHighlight: true,
            autoclose: true,
            startDate:false,
            });
        $('#personal_due_date').datepicker();
    });
    
    $(document).ready(function() 
    {
          var dateInput = $('input[name="property_due_date"]'); 
          var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : 'body';
          dateInput.datepicker({
            format: 'dd-M',
            container: container,
            todayHighlight: true,
            autoclose: true,
            startDate:false,
            });
        $('#property_due_date').datepicker();
    });
    

    function truncateDate(date) {
      return new Date(date.getFullYear(), date.getMonth(), date.getDate());
    }

    $("#personal_telephone").mask("(999) 999-9999");
    $("#personal_secondary_telephone").mask("(999) 999-9999");
    $("#property_telephone").mask("(999) 999-9999");
    $("#property_secondary_telephone").mask("(999) 999-9999");
     $("#licence_telephone").mask("(999) 999-9999");
    $("#licence_secondary_telephone").mask("(999) 999-9999");
    $("#county_telephone").mask("(999) 999-9999");
    $("#county_fax").mask("(999) 999-9999");
    
</script> 
<?php $__env->stopSection(); ?>



<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>