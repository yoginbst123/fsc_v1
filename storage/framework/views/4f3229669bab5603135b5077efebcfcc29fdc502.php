
<?php $__env->startSection('main-content'); ?>
<style>
   .modal-content,.modal-header{display: inherit;}
   .modal-body{font-size: 17px;}
   .text{position: relative;
   top: -201px;
   text-align: center;
   font-size: 20px;
   color: #FFF;
   text-transform: capitalize;}
</style>
<style>
   .m-t-50{ margin-top:50px; }
   .forgot-c{font-size:1.4em; float:right; width:100%; margin-right: 0px; text-align:right; text-decoration: underline;}
   .forgot-c a{ color:red;}
   .fsc-form-label{ margin-top:0; }
</style>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">
   <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 fsc-content-head">
         <h4>SUBMISSION</h4>
      </div>
   </div>
   <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-sub-imgs" style="margin-top:30px;">
         <img style="cursor:pointer;" class="img-responsive" src="<?php echo e(URL::asset('public/submission/')); ?>/<?php echo e($submission->singleimage); ?>">
      </div>
      <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
         <form class="login-form" method="post" action="<?php echo e(route('requestlogin')); ?>" id="admin-login">
            <?php echo e(csrf_field()); ?>

            <div class="form-group has-feedback">
               <label for="name" class="fsc-form-label">Type :</label>
               <select class="form-control fsc-input" id="type" name="type" style="margin-bottom:15px">
                  <option value="Limited">Limited</option>
                  <option value="Submission">Subscription</option>
               </select>
               <small data-bv-validator="notEmpty" data-bv-validator-for="name" class="help-block" style="display: none;">The first name is required</small>
            </div>
            <div class="form-group has-feedback">
               <label for="clientid" class="fsc-form-label">Client ID :</label>
               <input type="text" class="form-control  fsc-input" id="clientid" name="clientid" placeholder="Client ID " data-inputmask-regex="[A-Za-z]{2}-[\w]{2}-\d{2}"><i class="form-control-feedback" data-bv-icon-for="name" style="display: none;"></i>
               <span class="forgot-c"><a href="#" data-toggle="modal" data-target="#myModal">Forgot Client ID </a></span>
            </div>
            <div class="form-group has-feedback">
               <label for="username" class="fsc-form-label">Username</label>
               <input type="text" class="form-control  fsc-input" id="username" name="username" placeholder="Username"><i class="form-control-feedback" data-bv-icon-for="name" style="display: none;"></i>
               <span class="forgot-c"><a href="#" data-toggle="modal" data-target="#myModal1">Forgot Username </a></span>
               <?php if($errors->has('username')): ?>
               <span class="help-block">
               <strong><?php echo e($errors->first('username')); ?></strong>
               </span>
               <?php endif; ?>
            </div>
            <div class="form-group has-feedback">
               <label for="email" class="fsc-form-label">Password</label>
               <input type="password" class="form-control  fsc-input" id="password" name="password" placeholder="Password" data-bv-field="email"><i class="form-control-feedback" data-bv-icon-for="email" style="display: none;"></i>
               <span class="forgot-c"><a href="#" data-toggle="modal" data-target="#myModal2">Forgot password </a></span>
            </div>
            <div class="form-group has-feedback">
               <div class="g-recaptcha" style="float: left;" data-sitekey="6Lc-8rUUAAAAAKQYn2Wu8Y8VOQHYS_ISuGsT96fP"></div>
            </div>
            <div class="form-group has-feedback" style=" float:right; margin-top:15px; margin-left:40px; ">
               <a href="submissions"><button type="submit" class="btn btn-primary btn-lg fsc-form-submit">Cancel</button></a>
            </div>
            <div class="form-group has-feedback" style=" float:right; margin-top:15px; ">
               <button type="submit" class="btn btn-primary btn-lg fsc-form-submit">Ok</button>
            </div>
         </form>
      </div>
      <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style=" margin-top:30px ">
         <img src="<?php echo e(URL::asset('public/submission/')); ?>/<?php echo e($submission->siteimage); ?>" alt="" style="width:100%; border-radius: 5px; border: 4px solid #428bca;">
         <div class="text"><?php echo e($submission->description); ?></div>
      </div>
   </div>
</div>
<!-- Forgot Password -->
<div id="myModal2" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Forgot Password</h4>
         </div>
         <div class="modal-body">
            <p>Please call financial service center office To get your Password</p>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<!-- Forgot Email ID -->
<div id="myModal1" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Forgot Email ID</h4>
         </div>
         <div class="modal-body">
            <p>Please use your Email ID or please call financial service center get your ID</p>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<!-- Forgot Client ID -->
<div id="myModal" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Forgot Client ID</h4>
         </div>
         <div class="modal-body">
            <p>Please call financial service center office To get your Clinet ID</p>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://rawgit.com/RobinHerbots/Inputmask/4.x/dist/jquery.inputmask.bundle.js"></script>
<script>
   $('#clientid').inputmask({mask: 'AA-999-******'});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front-section.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>