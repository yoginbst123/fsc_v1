<?php $__env->startSection('title', 'Message'); ?>
<?php $__env->startSection('main-content'); ?>
<style>
label{float:left;}
.dt-buttons{margin-bottom:10px;}
.search-btn{position:absolute;top:10px;right:16px;background:transparent;border: transparent;}
.dt-buttons {
    margin-top: -41px;
    position: absolute;
    margin-left:86.8%;
}
.page-title{position: absolute
        padding: 8px 19px !important;
}
 .box-tools{
        position:absolute !important;
        margin-left: 280px !important;
    }
.dataTables_filter{
    display:none;
}
    .buttons-pdf {
    font-size: 0px !important;
    border-radius: 3px;
    padding: 8px 10px !important;
    background: #fff !important;
    border-color: #c6c6c6  !important;
    color:red !important;
}
.buttons-print {
    font-size: 0px !important;
    border-radius: 3px;
    padding: 8px 10px !important;
    background: #3c8dbc !important;
            border-color: #367fa9 !important;
}
.buttons-excel{
    font-size:0px !important;
    border-radius: 3px;
        padding: 8px 10px !important;
        
         background: #00a65a !important;
    border-color: #008d4c !important;
        

}
.buttons-excel:hover{
     background: #008d4c !important;

}
.buttons-pdf:hover{
     background: #f6f6f6  !important;
}

.buttons-print:hover{
     background: #367fa9 !important;
}


.fa{
    font-size: 16px !important;
}
.imgicon {
    background: #fff;
    display: block;
    width: 35px;
    float: left;
    margin-right: 10px;
    float: left;
    margin-right: 10px;
    border-radius: 2px;
    padding: 3px;
    border: 1px solid #12186b;
    height: 35px;
    margin-top: -6px;
    overflow: hidden;
}
.imgicon img{max-width:100%;}
.dt-buttons{
    display:none;
}
</style>
<div class="content-wrapper">
	<section class="content-header page-title" style="height:42px;">
     		<div class="" style="padding-right:0px !important;">
     		    <div class="" style="text-align:center;">
     		        <h1>Message Logsheet (Inbox) <span style="padding-right:10px; float:right">Add / View / Edit</span></h1>
     		    </div>
     		    
     		</div>
    </section>
	 <section class="content" style="background-color: #fff;">
	<div class="row">
	
		<div class="col-md-12">
			<div class="card">
				<div class="card-body">
					<div class="table-title" style="width: 100%;display: inline-block;">
					<a style="margin-top:5px;" href="<?php echo e(url('fscemployee/sendmessage/create')); ?>">Add New Message</a>
					</div>
					<?php if(session()->has('success')): ?>
                     <div class="alert alert-success alert-dismissable"><?php echo e(session()->get('success')); ?></div>
                    <?php endif; ?>
					<div class="table-responsive">
						<table class="table table-hover table-bordered" id="sampleTable3">
							<thead>
								<tr>
								    <th>#</th>
									<th width="7%">Priority</th>
									<th width="10%">Dt.- Day <br> Time</th>
									<th>Message From</th>
									<th width="10%">Telephone</th>
									<th>Call Purpose</th>
									<th width="7%">Status</th>
							       	<th width="7%">Action</th>
								</tr>
							</thead>							
							<tbody>
                            <?php $__currentLoopData = $task; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $com): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($com->status1=='Done'): ?> 
                            <?php else: ?>
								<tr>
								    <td style="text-align:center"><?php echo e($loop->index+1); ?></td>
								    <td> <?php echo e($com->call_back); ?><?php if($com->call_back=='2'): ?> <img src="<?php echo e(URL::asset('public/img/Blinking_warning.gif')); ?>" alt="<?php echo e($com->call_back); ?>" width="30px"> <?php elseif($com->call_back=='3'): ?>  <img src="<?php echo e(URL::asset('public/img/giphy.gif')); ?>" alt="<?php echo e($com->call_back); ?>" width="30px"> <?php endif; ?></td>
									<td style="text-align:center"><?php echo e($com->date); ?><br> <?php echo e($com->day); ?> <br><?php echo e($com->time); ?></td>
									<td><?php if($admin1): ?> <?php if($admin1->id==$com->admin_id): ?> <?php echo e(ucwords($admin1->fname)); ?> <?php echo e(ucwords($admin1->mname)); ?> <?php echo e(ucwords($admin1->lname)); ?> <?php endif; ?> <?php endif; ?> <?php $__currentLoopData = $emp; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $com2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php if($com2->id==$com->admin_id): ?> <?php echo e(ucwords($com2->firstName)); ?> <?php echo e(ucwords($com2->middleName)); ?> <?php echo e(ucwords($com2->lastName)); ?> <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></td>
									<td></td>
									<td><?php echo e($com->purpose); ?></td>
									<td></td>
									<td style="text-align:center"><a class="btn-action btn-view-edit" href="<?php echo e(route('sendmessage.edit',$com->id)); ?>"><i class="fa fa-edit"></i></a>
									<a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record ?'))
                                                                                  {event.preventDefault();document.getElementById('delete-id-<?php echo e($com->id); ?>').submit();} else{event.preventDefault();}" href="<?php echo e(route('sendmessage.destroy',$com->cid)); ?>"><i class="fa fa-trash"></i></a>
<form action="<?php echo e(route('sendmessage.destroy',$com->id)); ?>" method="post" style="display:none" id="delete-id-<?php echo e($com->id); ?>">
                                        <?php echo e(csrf_field()); ?> <?php echo e(method_field('DELETE')); ?>

                                        </form></td>
								</tr>
								<?php endif; ?>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	   </section>
</div>
<style>.table > thead > tr > th {background: #ffff99; text-align:center;}</style>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('fscemployee.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>