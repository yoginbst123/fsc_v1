<?php $__env->startSection('main-content'); ?>
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>Slider</h1>
    </section>
    <!-- Main content -->
    <section class="content">
	<div class="row">
		<div class="col-md-12">
				<div class="box box-success">
			      <div class="box-header">
            
              <div class="box-tools pull-right">
                
              </div>
            </div>
				<div class="col-md-12">
                   
					<form method="post" action="<?php echo e(route('slider.update',$slider->id)); ?>" class="form-horizontal" id="slidername" name="slidername" enctype="multipart/form-data">
					<?php echo e(csrf_field()); ?><?php echo e(method_field('PATCH')); ?>

						<div class="form-group<?php echo e($errors->has('slider_name') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Slider Name :</label>
							<div class="col-md-8">
								<input name="slider_name" type="text" value="<?php echo e($slider->slider_name); ?>" id="slider_name" class="form-control"/>								
								<?php if($errors->has('slider_name')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('slider_name')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>
						
						<div class="form-group<?php echo e($errors->has('slider_image') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Slider Image :</label>
							<div class="col-md-8">
								<input type="file" name="slider_image" id="slider_image" class="form-control" value="<?php echo e($slider->slider_image); ?>"/>




	<label class="file-upload btn btn-primary">
		                Browse for file ... <input name="slider_image" style="opecity:0" placeholder="Upload Service Image" id="slider_image" type="file" value="<?php echo e($slider->slider_image); ?>"/>
		            </label>




<br>								
                                <img src="https://financialservicecenter.net/public/slider/<?php echo e($slider->slider_image); ?>" title="<?php echo e($slider->slider_name); ?>" alt="<?php echo e($slider->slider_name); ?>" width="100px">
								<?php if($errors->has('slider_image')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('slider_image')); ?></strong>
										</span>
									<?php endif; ?>	
							</div>
                            <input type="hidden" name="slider_image1" id="slider_image1" value="<?php echo e($slider->slider_image); ?>">
						</div>
						
						<div class="card-footer">
						    <div class="form-group">
							<label class="control-label col-md-3"></label>
							<div class="col-xs-2" style="width:155px;">
<input class="btn_new_save btn-primary1" type="submit" name="submit" value="Save">
							</div>
							<div class="col-xs-2" style="width:155px;">
<a class="btn_new_cancel" href="<?php echo e(url('fac-Bhavesh-0554/slider')); ?>">Cancel</a> 
							</div>
						</div>
						  </div>
						
					</form>
				</div>
			</div>
		</div>
	</div>
	</section>
<!--</div>-->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>