<?php $__env->startSection('main-content'); ?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>Question</h1>
    </section>
    <!-- Main content -->
    <section class="content">
	<div class="row">
		<div class="col-md-12">
				<div class="box box-success">
			      <div class="box-header">
              
              <div class="box-tools pull-right">
                
              </div>
            </div>
				<div class="col-md-12">
					<form method="post" action="<?php echo e(route('question.update',$position->id)); ?>" class="form-horizontal" id="businessname" name="businessname" enctype="multipart/form-data">
					<?php echo e(csrf_field()); ?><?php echo e(method_field('PATCH')); ?>

						<div class="form-group <?php echo e($errors->has('question') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Question Name :</label>
							<div class="col-lg-5 col-md-8">
 <select name="type" id="type" class="form-control">
                                             <option value="">Select</option> 
                                             <option value="Type of Entity" <?php if($position->type=='Type of Entity'): ?> selected <?php endif; ?>>Type of Entity</option>
                                             <option value="Type of Business" <?php if($position->type=='Type of Business'): ?> selected <?php endif; ?>>Type of Business</option>
                                            
                                          </select>								
<?php if($errors->has('question')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('question')); ?></strong>
										</span>
									<?php endif; ?>
								
							</div>
						</div>
						<div class="form-group <?php echo e($errors->has('question') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Question Name :</label>
							<div class="col-lg-5 col-md-8">
								<input name="question" type="text" id="question" class="form-control" value="<?php echo e($position->question); ?>" />                                                            <?php if($errors->has('question')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('question')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>	
										
						
						<div class="card-footer">
						    <div class="row">
    						    <div class="col-md-3"></div>
    						    <div class="col-xs-2" style="width:155px;">
    								<input class="btn_new_save btn-primary1" type="submit" name="submit" value="Save">
    							</div>
    							<div class="col-xs-2" style="width:155px;">
    								<a class="btn_new_cancel" href="<?php echo e(url('fac-Bhavesh-0554/question')); ?>">Cancel</a> 
    							</div>
							</div>
						</div>
						
					</form>
				</div>
			</div>
		</div>
	</div>
	</section>
<!--</div>-->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>