<?php $__env->startSection('main-content'); ?>
<style>
label{float:left;}
.box-tools{
        position:absolute !important;
        margin-top: 7px !important;
        margin-right: 150px !important;
    }
    .page-title {
    display: -ms-flexbox;
    -ms-flex-align: center;
    align-items: center;
    -ms-flex-pack: justify;
    justify-content: space-between;
    -ms-flex-direction: row;
    flex-direction: row;
    padding: 8px 18px !important;
    box-shadow: 0 1px 2px rgba(0,0,0,.1);
    background-color: #D6EBFA !important;
    text-align: center;
}
.buttons-pdf {
    font-size: 0px !important;
    border-radius: 3px;
    padding: 8px 10px !important;
    background: #fff !important;
    border-color: #c6c6c6  !important;
    color:red !important;
}
.buttons-print {
    font-size: 0px !important;
    border-radius: 3px;
    padding: 8px 10px !important;
    background: #3c8dbc !important;
            border-color: #367fa9 !important;
}
.buttons-excel{
    font-size:0px !important;
    border-radius: 3px;
    padding: 8px 10px !important;
    background: #00a65a !important;
    border-color: #008d4c !important;
}
.buttons-excel:hover{
     background: #008d4c !important;
}
.buttons-pdf:hover{
     background: #f6f6f6  !important;
}
.buttons-print:hover{
     background: #367fa9 !important;
}
.fa{
    font-size: 16px !important;
}
.content-header.page-title h2 {
    margin: 0;
    font-size: 22px!important;
    font-weight: 600!important;
    color: #222!important;
}
@media  only screen and (max-width: 991px){
    .table-title a {
        margin-top: 0px !important; 
        margin-right: -10px !important;
    }
}
@media  only screen and (max-width: 860px){
    .box-header>.box-tools{
        position: relative !important;
        margin-right: 5px !important;
        margin-top: 0px !important;
    }
}
@media  only screen and (max-width: 490px){
    div.dataTables_wrapper div.dataTables_filter{
        width: 100%;
        display: flex;
    }
    div.dataTables_wrapper div.dataTables_filter label{
        width: 84%;
    }
    .table-title a {
        margin-top: -34px !important;
        margin-right: 10px !important;
    }
    .box-header>.box-tools {
        position: absolute !important;
        margin-top: 51px !important;
        margin-right: 120px !important;
    }
    .box-header {
        padding: 10px !important;
    }
}
</style>
<div class="content-wrapper">
	 <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>Branch</h1>
    </section>
    <!-- Main content -->
    <section class="content">
	<div class="row">
	
		<div class="col-md-12">
			<div class="box box-success">
			      <div class="box-header" style="padding:0px;">
            
              <div class="box-tools pull-right" style="position: absolute;margin-right: 132px;margin-top:7px;z-index:9999;">
                <div class="table-title">
					
						<a href="<?php echo e(route('branch.create')); ?>">Add New Branch</a>
					</div>
              </div>
            </div>
				<div class="col-md-12">
					
					<?php if( session()->has('success') ): ?>
    <div class="alert alert-success alert-dismissable"><?php echo e(session()->get('success')); ?></div>
                               <?php endif; ?>
					<div class="table-responsive">
						<table class="table table-hover table-bordered" id="sampleTable345">
							<thead>
								<tr>
                                    <th>No</th>
                                    <th>Branch Name</th>							
                                    <th>country</th>							
                                    <th>City</th>							
                                    <th>Branch Type</th>							
                                    <th>Action</th>
								</tr>
							</thead>
							<tbody>
                                <?php $__currentLoopData = $branch; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bra): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          
								<tr>
									<td class="text-center"><?php echo e($loop->index + 1); ?></td>
									<td><?php echo e($bra->branchname); ?></td>
                                    <td><?php echo e($bra->country); ?></td>
                                    <td><?php echo e($bra->city); ?></td>
                                    <td><?php echo e($bra->positionid); ?></td>
									<td class="text-center">
										<a class="btn-action btn-view-edit" href="<?php echo e(route('branch.edit', $bra->id)); ?>"><i class="fa fa-edit"></i></a>
                                        <form action="<?php echo e(route('branch.destroy',$bra->id)); ?>" method="post" style="display:none" id="delete-id-<?php echo e($bra->id); ?>">
                                        <?php echo e(csrf_field()); ?> <?php echo e(method_field('DELETE')); ?>

                                        </form>
                                        
										<a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record ?'))
                                                                                  {event.preventDefault();document.getElementById('delete-id-<?php echo e($bra->id); ?>').submit();} else{event.preventDefault();}" href=""><i class="fa fa-trash"></i></a>
									</td>
								</tr>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		
	</div>
		</section>
<!--</div>-->
	
<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>