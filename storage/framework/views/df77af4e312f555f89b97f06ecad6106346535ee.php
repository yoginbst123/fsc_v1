
<?php $__env->startSection('main-content'); ?>
    <style>
        #printElement, #printButton {
            margin: 30px;
        }

        .page-title {
            padding: 15px 30px !important;
        }

        .col-sm-4 {
            /*padding: 10px;*/
            /*border: 1px solid grey;*/
        }

        .control-label {
            margin-top: 8px !important;
        }

        .dt-button {
            display: none;
        }

        #exampleModal i.more-less {
            float: right;
            padding: 8px;
            color: #005ed2;
        }

        #exampleModal1 i.more-less {
            float: right;
            padding: 8px;
            color: #d07f00;
        }

        #exampleModal2 i.more-less {
            float: right;
            padding: 8px;
            color: #dd4b39;
        }

        .ac_name_first {
            width: 100% !important;
            padding: 8px 10px;
            color: #000000;
        }

        .panel-title a.collapsed .glyphicon-plus {
            display: block;
        }

        .count_acc {
            float: left;
            padding: 8px;
            background: #fff;
            color: #269ef3;
            border-radius: 5px;
            width: 90px;
            text-align: center;
            font-size: 16px;
        }

        .modal small {
            color: #ffffff;
        }

        .panel-title a .glyphicon-plus {
            display: none;
        }

        .panel-title a .glyphicon-minus {
            display: block;
        }

        .ac_name_first {
            width: 100% !important;
            padding: 8px 10px;
            font-weight: bold;
        }

        .panel-title a.collapsed .glyphicon-minus {
            display: none;
        }

        .panel.panel-default .panel-heading h4 a:hover, .panel.panel-default .panel-heading h4 a {
            color: #103b68 !important;
        }

        #countryList {
            margin: 40px 0px 0px -60px;
            padding: 0px 0px;
            list-style-type: none;
            max-height: 200px;
            overflow: auto;
            position: absolute;
            width: 350px;
            z-index: 99;
        }

        #countryList li {
            border-bottom: 1px solid #025b90;
            background: #ef7c30;
        }

        #countryList li a {
            padding: 0px 10px 0px 0px !important;
            margin: 0px !important;
            display: block;
            color: #fff !important;
        }

        #countryList li:hover {
            background: #dc6d23;
        }


        #clientList {
            margin: 5px 0px 0px 3px;
            padding: 0px 0px;
            list-style-type: none;
            max-height: 200px;
            overflow: auto;
            position: absolute;
            width: 450px;
            z-index: 99;
        }

        #clientList li {
            border-bottom: 1px solid #025b90;
            background: #ef7c30;
        }

        #clientList li a {
            padding: 0px 10px 0px 0px !important;
            margin: 0px !important;
            display: block;
            color: #fff !important;
        }

        #clientList li:hover {
            background: #dc6d23;
        }


        .gif {
            width: 70px;
            position: absolute;
            top: -9px;
            left: 4px;
        }

        .box3 h3 {
            font-size: 20px;
        }

        .no-m-l {
            margin-left: 0 !important;
        }

        .no-p-l {
            padding-left: 0 !important;
        }

        .no-m-b {
            margin-bottom: 0 !important;
        }

        .box3 h3 {
            font-size: 18px;
        }

        .table-responsive {
            overflow-x: inherit;
        }

        .table > tbody > tr > td {
            text-align: left;
        }

        .searchboxmain {
            float: right;
            display: FLEX;
            margin-top: -5px;
            justify-content: space-between;
        }

        h1.dash {
            float: left;
            margin-top: 7px;
            width: 100% !important;
            text-align: center;
            border-top: 1px solid;
            padding-top: 7px;
        }

        .edittable tr td {
            padding: 0px 5px !important;
        }

        #collapseOne3 {
            margin-top: 0px !important;
        }

        .clear {
            clear: both;
        }

        #collapseOne03 {
            margin-top: 0px !important;
        }

        .btn_new_preview {
            display: block;
            width: 100%;
            font-size: 16px;
            font-weight: bold;
            color: #FFF;
            padding: 8px 0;
            margin-bottom: 20px;
            text-align: center;
        }

        @media  only screen and (max-width: 767px) {
            .sidebar2 {
                width: 100%;
            }

            .content-wrapper {
                min-height: auto !important;
                height: auto !important;
            }

            .main-box-bg .main-box .images-img {
                padding: 8px 3px;
            }
        }

        .modal.fade.in {
            display: block !important;
            opacity: 1 !important;
            padding-top: 50px;
        }

        #countryList li a {
            height: 40px;
        }

        #countryList li a span.bgcolors {
            background: linear-gradient(to bottom, #b3dced 0%, #29b8e5 50%, #bce0ee 100%) !important;
            padding: 6px 0px;
            text-align: center;
            display: block;
            font-size: 20px;
            font-weight: bold;
            float: left;
            width: 60px;
        }

        #countryList li a span.clientalign {
            display: flex;
            width: 100px;
            float: left;
            line-height: 15px;
            font-size: 13px;
            padding: 4px 0px;
            flex-direction: row;
            justify-content: flex-start;
            align-items: center;
            align-content: center;
            min-height: 40px;
            background: #038ee0;
            margin-left: 0;
            padding-left: 5px;
        }

        #countryList li a span.entityname {
            display: flex;
            width: 190px
            float: left;
            line-height: 15px;
            padding: 4px 0px;
            font-size: 13px;
            flex-direction: row;
            justify-content: flex-start;
            align-items: center;
            align-content: center;
            min-height: 38px;
            padding-left: 10px;
            margin-left: 10px;
        }

        .table tr td.text-center {
            text-align: center !important;
        }

        .modal-body ul {
            margin: 0px;
            padding: 0px;
        }

        .modal-body ul li {
            text-align: left !important;
            list-style-type: none !important;
            font-size: 12px !important;
        }

        .modal.fade.in {
            padding-top: 10px !important;
        }

        .custom_radio {
            height: 17px;
            width: 17px;
            vertical-align: middle;
            margin-top: -4px !important;
        }

        #clientList li a {
            height: 40px;
        }


        #clientList li a {
            height: 40px;
        }

        #clientList li a span.bgcolors {
            background: linear-gradient(to bottom, #b3dced 0%, #29b8e5 50%, #bce0ee 100%) !important;
            padding: 6px 0px;
            text-align: center;
            display: block;
            font-size: 20px;
            font-weight: bold;
            float: left;
            width: 60px;
        }

        #clientList li a span.clientalign {
            display: flex;
            width: 100px;
            float: left;
            line-height: 15px;
            font-size: 13px;
            padding: 4px 0px;
            flex-direction: row;
            justify-content: flex-start;
            align-items: center;
            align-content: center;
            min-height: 40px;
            background: #038ee0;
            margin-left: 0;
            padding-left: 5px;

        }

        #clientList li a span.entityname {
            display: flex;
            width: 190px
            float: left;
            line-height: 15px;
            padding: 4px 0px;
            font-size: 13px;
            flex-direction: row;
            justify-content: flex-start;
            align-items: center;
            align-content: center;
            min-height: 38px;
            padding-left: 10px;
            margin-left: 10px;

        }

        .table tr td.text-center {
            text-align: center !important;
        }

        .modal-body ul {
            margin: 0px;
            padding: 0px;
        }

        .modal-body ul li {
            text-align: left !important;
            list-style-type: none !important;
            font-size: 12px !important;
        }

        .modal.fade.in {
            padding-top: 10px !important;
        }

        .custom_radio {
            height: 17px;
            width: 17px;
            vertical-align: middle;
            margin-top: -4px !important;
        }

        ul.leaders {
            padding: 0;
            width: 90%;
            margin: auto !important;
            overflow-x: hidden;
            list-style: none
        }

        ul.leaders li:before {
            float: left;
            width: 0;
            white-space: nowrap;
            content: ". . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . . . " ". . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . ." ". . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . ." ". . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . ."
        }

        ul.leaders span:first-child {
            padding-right: 0.33em;
            background: white
        }

        ul.leaders span {
            font-size: 17px;
        }

        ul.leaders span + span {
            float: right;
            padding-left: 0.33em;
            font-weight: bold;
            background: white
        }

        ul.leaders1 {
            overflow-x: hidden;
            margin-top: 10px;
            list-style: none
        }

        ul.leaders1 li:before {
            float: left;
            width: 0;
            white-space: nowrap;
            content:

        }

        ul.leaders1 span:first-child {
            padding-right: 0.33em;
            background: white
        }

        ul.leaders1 li {
            font-size: 17px !important;
        }

        ul.leaders1 span + span {
            float: right;
            padding-left: 0.33em;
            font-weight: bold;
            background: white
        }

        #myModalEmloyeeAll .table tr td {
            font-size: 12px !important;
        }

        .alphabet {
            background: #269ef3;
            color: #fff;
            padding: 2px 7px;
            font-weight: bold;
        }

        @media (min-width: 768px) {
            .modal-dialog {
                width: 900px;
                margin: 30px auto;
            }
        }

        .modal-body h5 {
            margin-top: 0px !important;
            margin-bottom: 2px !important;
        }

        .modal-body {
            position: relative;
            padding: 15px 15px;
        }

        .select2-container--default .select2-selection--single .select2-selection__arrow b {
            margin-top: 3px;
        }

        .select2-container--default .select2-selection--single .select2-selection__rendered {
            color: #000;
            line-height: 28px;
            font-weight: bold;
        }

        .select2-container .select2-selection--single {
            height: 36px;
            padding: 2px 0;
            border: 2px solid #286db5 !important;
        }

        .select2-container .select2-selection--single {
            height: 40px;
            border-radius: 3px;
            border: 2px solid #2fa6f2 !important;
            font-size: 16px !important;
            outline: 0;
            color: #000;
            padding: 6px 6px;
            background-color: transparent !important;
            /*width:536px !important;*/
        }

        .select2-container {
            width: 100% !important;
        }


        @media  print {
            #myModal_FSC-120 {
                background: #000000;
            }

            table tr td {
                font-size: 11px !important;
                white-space: nowrap !important;
            }
        }

        @media  print {
            #printbtn {
                display: none;
            }
        }

        @media  screen {
            #printSection {
                display: none;
            }
        }

        .alphabet {
            align-content;
            background: #269ef3;
            color: #fff;
            padding: 2px 7px;
            font-weight: bold;
        }


        @media  print {
            body * {
                visibility: hidden;
            }

            #printSection, #printSection * {
                visibility: visible;
                height: auto;
            }

            #printSection {
                position: absolute;
                left: 0;
                top: 0;
            }

            .myclass tr th {
                white-space: nowrap !important;
                font-size: 12px;
            }

            @page  {
                margin: 0px !important;
            }
        }

        .logoprintbox {
            display: none;
        }

        .skin-blue, .skin-blue .sidebar-menu > li.active > a, .skin-blue .sidebar-menu > li.menu-open > a {
            background: none !important;
        }

        .whitefont p, .whitefont .table tr td, .whitefont ul li {
            color: #fff !important;
        }

        .modal-header {
            background: #96cbea;
            color: #000;
            border-bottom: 2px solid #062492 !important;
        }

        .modal-header .modal-title {
            text-align: center !important;
        }

        .modal-body .form-horizontal .control-label {
            padding-top: 0px !important;
        }

        .content-header.page-title a img {
            /*background: #fbffbf !important;*/
            border: 2px solid #0e59a5 !important;
            /*border:2px solid #000000!important;*/
            /*border-right:1px solid #000000!important;*/
            /*padding-right:10px;*/
            text-align: center;
        }

        .modal-body label {
            padding-top: 0px !important;
        }

        .badgebox {
            background: #ff0000;
            width: 20px;
            height: 20px;
            display: block;
            position: absolute;
            right: -8px;
            top: -6px;
            border-radius: 20px;
            color: #fff;
            font-size: 12px;
        }

        .bloinkcss {
            animation: blinker 1s linear infinite;
        }

        @keyframes  blinker {
            0% {
                opacity: 1;
            }
            20% {
                opacity: 0.5;
            }
            50% {
                opacity: 0;
            }
        }

        .required {
            color: red
        }
    </style>

    <?php //echo $countrecstatus;die;?>
    <!--data-toggle="modal" data-target="#mytask2"-->
    <div class="content-wrapper">
        <section class="content-header page-title dash_title">
            <a href="#" class="dash_a" data-toggle="modal" data-target="#myModalcustomershseet" style="float:left; margin-top:-7px; cursor:pointer;
    border-color: #50bc3c;"><img src="https://financialservicecenter.net/public/images/btn_conversationsheet.png"/>
            </a>
            <a href="#" class="dash_a" data-toggle="modal" data-target="#myModalnotes" style="float:left; margin-top:-7px; cursor:pointer; margin-left:10px; position:relative;
    border-color: #50bc3c;"><img src="https://financialservicecenter.net/public/images/btnnotes.png"/> </a>
            <a href="#" class="dash_a" data-toggle="modal" data-target="#mytask1" style="float:left; margin-top:-7px; cursor:pointer; margin-left:10px; position:relative;
    border-color: #50bc3c;"><img
                    src="https://financialservicecenter.net/public/images/btntask.png"/><?php $counttask = count($taskall); if($counttask != '0'){?>
                <span class="badgebox"><span class="bloinkcss"><?php echo $counttask;?></span></span><?php } ?></a>
        <!--<a href="<?php echo e(url('/fac-Bhavesh-0554/howtodo')); ?>"  class="btn" style="float:left; line-height: 11px; margin-top: -9px; padding-top: 2px; padding-bottom: 2px; font-size: 11px;-->
            <!--color: #000;cursor:pointer; background: #fff; margin-left:7px; position:relative; border-color: #50bc3c;"><b>H</b>ow<br><b>T</b>o<br><b>D</b>o</a>-->
            <a href="<?php echo e(url('/fac-Bhavesh-0554/howtodo')); ?>" class="dash_a" style="float:left; margin-top:-7px; cursor:pointer; margin-left:10px; position:relative;
    border-color: #50bc3c;"><img src="https://financialservicecenter.net/public/images/howtodo.png"/> </a>
            <a href="#" class="dash_a" data-toggle="modal" data-target="#modalComplaint" style="float:left; margin-top:-7px; cursor:pointer; margin-left:10px; position:relative;
    border-color: #50bc3c;"><img src="https://financialservicecenter.net/public/images/btncomp.jpg"/> </a>
            <!-- <a href="#"  data-toggle="modal" data-target="#mydemo1" style="float:left; margin-top:-8px; cursor:pointer; margin-left:10px;color:#000000;-->
            <!--border-color: #50bc3c;"><i class="fa fa-calendar"></i><p style="margin:0px;">Appointment</p></a>-->
            <a href="<?php echo e(route('appointment.create')); ?>" class="dash_a"
               style="float:left; margin-top:-7px; cursor:pointer; margin-left:10px; position:relative;border-color: #50bc3c;">
                <img src="https://financialservicecenter.net/public/images/btn_appoint.png"/> </a>
            <a href="#" class="dash_a" data-toggle="modal" data-target="#modalProposal" style="float:left; margin-top:-7px; cursor:pointer; margin-left:10px; position:relative;
    border-color: #50bc3c;"><img src="https://financialservicecenter.net/public/images/btn_prop.png"/> </a>
            <a href="<?php echo e(url('/fac-Bhavesh-0554/mywork')); ?>" class="dash_a" style="float:left; margin-top:-7px; cursor:pointer; margin-left:10px; position:relative;
    border-color: #50bc3c;"><img src="https://financialservicecenter.net/public/images/btn_work.png"/> </a>
            <a href="<?php echo e(url('/fac-Bhavesh-0554/email')); ?>" class="dash_a" style="float:left; margin-top:-7px; cursor:pointer; margin-left:10px; position:relative;
    border-color: #50bc3c;"><img src="https://financialservicecenter.net/public/images/extension.png"/> </a>
            <!-- <a href="#"  data-toggle="modal" data-target="#mydemo2" style="float:left; margin-top:-6px; cursor:pointer; margin-left:10px;
    border-color: #50bc3c;">Demo2</a>
    !-->


            <div class="searchboxmain">
                <input type="text" name="search" id="country_name" class="form-control" placeholder="Search">
                <a class="btn-action btn-view-edit btn-primary"
                   style="background:#367fa9 !important;padding: 9px 18px;margin-left: 7px;margin-top: 0px;"
                   href="https://financialservicecenter.net/fac-Bhavesh-0554/">Reset</a>
                <?php echo e(csrf_field()); ?>

                <ul id="countryList"></ul>
            </div>

            <h1 class="dash">Dashboard</h1>
        </section>
        <section class="content">
            <div class="boxes">
                <div class="">
                    <div class="row main-box-bg" style="padding-top:6px">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 pr-8">
                            <div class="main-box">
                                <div class="box1" style="border-top: none;margin-bottom: 2px;">
                                    <?php if(empty($common->newclient)): ?>
                                        <h2 class="pull-left">0</h2>
                                    <?php else: ?>
                                        <?php if($countrecstatus>0): ?>
                                        <!--<div class="gif">-->
                                        <!--<img src="<?php echo e(asset('public/dashboard/images/newimage.gif')); ?>" alt="" class="img-responsive">-->
                                            <!--</div>-->
                                        <?php else: ?>

                                        <?php endif; ?>
                                        <h2><?php echo e(count($commonregister1)); ?></h2>
                                    <?php endif; ?>
                                    <h3><span class="big-font">C</span>lient</h3>
                                    <img src="<?php echo e(url('public/images/Client-06.png')); ?>" alt="img" class="images-img ">
                                    <?php if(empty($common->newclient)): ?>
                                        <button type="button" class="btn btn-info information-btn">More info</button>
                                    <?php else: ?>
                                        <a href="<?php echo e(url('/fac-Bhavesh-0554/customer')); ?>"
                                           class="btn btn-info information-btn">More info</a>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 p-4">
                            <div class="main-box">
                                <div class="box2" style="margin-bottom: 2px;">
                                    <h2>2</h2>
                                    <h3><span class="big-font">W</span>ork</h3>
                                    <img src="<?php echo e(url('public/images/desk_2.png')); ?>" alt="img" class="images-img">
                                    <a href="<?php echo e(url('/fac-Bhavesh-0554/worknew')); ?>">
                                        <button type="button" class="btn btn-info information-btn2">More Info</button>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 pl-8">
                            <div class="main-box">
                                <div class="box3" style="margin-bottom: 2px;">
                                    <?php if(empty($employees->count)): ?>
                                        <h2>0</h2>
                                    <?php else: ?>
                                        <?php if(empty($newemp->newemp)): ?>
                                        <?php else: ?>
                                            <div class="gif">
                                                <img src="<?php echo e(asset('public/dashboard/images/newimage.gif')); ?>" alt=""
                                                     class="img-responsive">
                                            </div>
                                        <?php endif; ?>
                                        <h2><?php echo e($employees->total+$employeeusers->total); ?></h2>
                                    <?php endif; ?>

                                    <h3><span class="big-font">E</span>mployee / User</h3>
                                    <img src="<?php echo e(url('public/images/desk_man.png')); ?>" alt="img" class="images-img">
                                    <?php if(empty($employees->count)): ?>
                                        <button type="button" class="btn btn-info information-btn3">More info</button>
                                    <?php else: ?>
                                        <a href="<?php echo e(url('/fac-Bhavesh-0554/employee')); ?>"
                                           class="btn btn-info information-btn3">More info</a>
                                    <?php endif; ?>

                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 pr-8">
                            <div class="main-box">
                                <div class="box1 aaa" style="margin-bottom: 2px;">
                                    <?php if(empty($clientemployee->count)): ?>
                                        <h2 class="pull-left">0</h2>
                                    <?php else: ?>
                                        <?php if(empty($newemp1->newemp)): ?>
                                        <?php else: ?>
                                            <div class="gif">
                                                <img src="<?php echo e(asset('public/dashboard/images/newimage.gif')); ?>" alt=""
                                                     class="img-responsive">
                                            </div>
                                        <?php endif; ?>
                                        <h2 class="pull-left"><?php echo e($clientemployee->total); ?></h2>
                                    <?php endif; ?>
                                    <h3 class="pull-left"><span class="big-font">C</span>lient's</h3>
                                    <div class="sidebar2" style="width:60%">
                                        <h3 class="pull-left"><span class="big-font">E</span>mployee</h3>
                                        <!--<h3 class="no-m-l"><span class="big-font">E</span>mployee</h3>-->
                                    <!--<img src="<?php echo e(url('public/images/1.png')); ?>" alt="img" class="images-img">-->
                                        <img src="<?php echo e(url('public/images/desk_audience.png')); ?>" alt="img"
                                             class="images-img">
                                    </div>
                                    <?php if(empty($clientemployee->count)): ?>
                                        <button type="button" class="btn btn-info information-btn3">More info</button>
                                    <?php else: ?>
                                        <a href="<?php echo e(url('/fac-Bhavesh-0554/clientemployee')); ?>"
                                           class="btn btn-info information-btn3">More info</a>
                                    <?php endif; ?>

                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 p-4">
                            <div class="main-box">
                                <div class="box1" style="border-top: none;margin-bottom: 2px;">

                                    <h2><?php echo e($contact); ?></h2>
                                    <h3><span class="big-font">W</span>ork To Do</h3></h3>
                                    <img src="<?php echo e(url('public/images/Work to Do-04.png')); ?>" alt="img" class="images-img">
                                    <a href="<?php echo e(url('/fac-Bhavesh-0554/worktodo')); ?>"
                                       class="btn btn-info information-btn">More info</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 pl-8">
                            <div class="main-box">
                                <div class="box1" style="border-top: none;margin-bottom: 2px;">
                                    <?php if($msg2 >0): ?>
                                        <div class="gif">
                                            <img src="<?php echo e(asset('public/dashboard/images/newimage.gif')); ?>" alt=""
                                                 class="img-responsive">
                                        </div>
                                    <?php endif; ?>
                                    <h2><?php echo e($msg2); ?></h2>
                                    <h3><span class="big-font">M</span>essage</h3>
                                    <img src="<?php echo e(url('public/images/desk_Message-03.png')); ?>" alt="img"
                                         class="images-img">
                                    <a href="<?php echo e(url('/fac-Bhavesh-0554/msg')); ?>" class="btn btn-info information-btn">More
                                        info</a>

                                    <style>.ms {
                                            display: none !important
                                        }</style>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 pr-8">
                            <div class="main-box">
                                <div class="box3" style="border-top: none;margin-bottom: 2px;">
                                    <?php if($vendor1): ?>
                                        <div class="gif">
                                            <img src="<?php echo e(asset('public/dashboard/images/newimage.gif')); ?>" alt=""
                                                 class="img-responsive">
                                        </div>
                                    <?php else: ?>
                                    <?php endif; ?>
                                    <h2><?php echo e(count($vendor)); ?></h2>
                                    <h3><span class="big-font">V</span>endor</h3>
                                    <img src="<?php echo e(url('public/images/desk_Vendor-02.png')); ?>" alt="img"
                                         class="images-img ">
                                    <a href="<?php echo e(url('/fac-Bhavesh-0554/vendor')); ?>" class="btn btn-info information-btn">More
                                        info</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 p-4">
                            <div class="main-box">
                                <div class="box3" style="border-top: none;margin-bottom: 2px;">

                                    <h2><?php echo e($contact); ?></h2>
                                    <h3><span class="big-font">S</span>ubmission</h3>
                                    <img src="<?php echo e(url('public/images/desk_Submission-05-05.png')); ?>" alt="img"
                                         class="images-img ">
                                    <a href="<?php echo e(url('/fac-Bhavesh-0554/submissionrequest')); ?>"
                                       class="btn btn-info information-btn">More info</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 pl-8">
                            <div class="main-box">
                                <div class="box3" style="border-top: none;margin-bottom: 2px;">
                                    <?php if($contact1): ?>
                                        <div class="gif">
                                            <img src="<?php echo e(asset('public/dashboard/images/newimage.gif')); ?>" alt=""
                                                 class="img-responsive">
                                        </div>
                                    <?php else: ?>
                                    <?php endif; ?>
                                    <h2><?php echo e($contact); ?></h2>
                                    <h3><span class="big-font">C</span>ontact Us</h3>
                                    <img src="<?php echo e(url('public/images/desk_telephone.png')); ?>" alt="img"
                                         class="images-img ">
                                    <a href="<?php echo e(url('/fac-Bhavesh-0554/contactinquery')); ?>"
                                       class="btn btn-info information-btn">More info</a>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
            <div class="boxes">
                <div class="">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 pr-8">
                            <?php
                            $m3 = 0;
                            ?>

                            <?php $__currentLoopData = $adminupload; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $adminlic): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php
                                if(isset($adminlic->expiredate) != '' && isset($adminlic->expiredate) != null)
                                {

                                $exp_date = date('M-d-Y', strtotime("-30 days", strtotime($adminlic->expiredate)));
                                $exp_dates = strtotime($exp_date);

                                $today = date('M-d-Y');
                                $match_today = strtotime($today);

                                $exp_date2 = date('M-d-Y', strtotime("-11 days", strtotime($adminlic->expiredate)));
                                $exp_dates2 = strtotime($exp_date2);
                                if($match_today >= $exp_dates && $match_today <= $exp_dates2)
                                {

                                ?>
                                <div class="gif">
                                    <img src="<?php echo e(asset('public/dashboard/images/newimage.gif')); ?>" alt=""
                                         class="img-responsive">
                                </div>

                                <?php
                                }
                                }

                                ?>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php
                            $m3++;
                            ?>
                            <div class="main-box">
                                <div class="box2" style="margin-bottom: 2px;">
                                    <a data-toggle="modal" data-target="#exampleModal"
                                       class="btn btn-info information-btn3"
                                       style="background-color: #00c0ef !important;padding: 0 20px; width:100%"><h4
                                            style="font-size: 24px;line-height: 24px;float: left;"><?php echo 0; //echo $m;?></h4>
                                        <h3 style="font-size: 15px;float: right;margin: 0;line-height: 41px;"><span
                                                class="big-font" style="font-size: 24px;">R</span>eminder <span
                                                style="margin-left:8px;">( 30 Days )</span></h3></a>
                                </div>
                            </div>


                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 p-4">
                            <?php
                            $m2 = 0;
                            ?>

                            <?php $__currentLoopData = $adminupload1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $adminlic1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php
                                if(isset($adminlic1->expiredate) != '' && isset($adminlic1->expiredate) != null)
                                {
                                $exp_date = date('M-d-Y', strtotime("-10 days", strtotime($adminlic1->expiredate)));
                                $exp_dates = strtotime($exp_date);

                                $today = date('M-d-Y');
                                $match_today = strtotime($today);

                                $exp_date2 = date('M-d-Y', strtotime("-3 days", strtotime($adminlic1->expiredate)));
                                $exp_dates2 = strtotime($exp_date2);
                                if($match_today >= $exp_dates && $match_today <= $exp_dates2)
                                {
                                ?>
                                <div class="gif">
                                    <img src="<?php echo e(asset('public/dashboard/images/newimage.gif')); ?>" alt=""
                                         class="img-responsive">
                                </div>

                                <?php
                                }
                                }

                                ?>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php
                            $m2++;
                            ?>
                            <div class="main-box">
                                <div class="box3" style="margin-bottom: 2px;background:#dd4b39">
                                    <a data-toggle="modal" data-target="#exampleModal1"
                                       class="btn btn-info information-btn3"
                                       style="background-color: #f39c12 !important;padding: 0 20px; width:100%"><h4
                                            style="font-size: 24px;line-height: 24px;float: left;"><?php echo 0; //echo $m2;?></h4>
                                        <h3 style="font-size: 15px;float: right;margin: 0;line-height: 41px;"><span
                                                class="big-font" style="font-size: 24px;">N</span>otification <span
                                                style="margin-left:8px;">( 10 Days )</span></h3></a>
                                </div>
                            </div>


                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 pl-8">
                            <?php
                            $m1 = 0;
                            ?>
                            <?php $__currentLoopData = $adminupload2; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $adminlic1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <?php
                                if(isset($adminlic1->expiredate) != '' && isset($adminlic1->expiredate) != null)
                                {
                                $exp_date = date('M-d-Y', strtotime("-2 days", strtotime($adminlic1->expiredate)));
                                $exp_dates = strtotime($exp_date);

                                $today = date('M-d-Y');
                                $match_today = strtotime($today);
                                if($match_today >= $exp_dates)
                                {
                                //echo $adminlic1->expiredate;
                                ?>

                                <div class="gif">
                                    <img src="<?php echo e(asset('public/dashboard/images/newimage.gif')); ?>" alt=""
                                         class="img-responsive">
                                </div>

                                <?php
                                }
                                }

                                ?>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php
                            $m1++;
                            ?>
                            <?php
                            $arrc11 = array();
                            foreach ($clientoriginal as $corg) {
                                $arrc11[] = $corg->client_id;

                            }
                            //  print_r($arrc11);
                            // echo '----------------'.count($arrc11);
                            //echo "<br>";
                            $cntssw = count($commonone1);
                            foreach ($commonone1 as $taxfederal2) {
                                if (!in_array($taxfederal2->ids, $arrc11)) {
                                    // $cntss112++;
                                }
                            }

                            ?>
                            <div class="main-box">
                                <div class="box"
                                     style="border-top: none;margin-bottom: 2px;background-color: #f39c12 !important;">
                                    <a data-toggle="modal" data-target="#exampleModal2"
                                       class="btn btn-info information-btn"
                                       style="background-color: #dd4b39 !important;padding: 0 20px; width:100%"><h4
                                            class="notify_count"
                                            style="font-size: 24px;line-height: 24px;float: left;"><?php //echo $cntssw; //echo $m1;?></h4>
                                        <h3 style="font-size: 15px;float: right;margin: 0;line-height: 41px;"><span
                                                class="big-font" style="font-size: 24px;">W</span>arning <span
                                                style="margin-left:8px;">( 2 Days )</span></h3></a>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
            <div class="box box-success">
                <div class="">
                    <div id="accordion" class="accordion">
                        <div class="card mb-0">
                            <div class="card-header collapse" data-toggle="collapse" href="#collapseOne">
                                <a class="card-title">
                                    Client
                                </a>
                            </div>
                            <div id="collapseOne" class="card-body collapse" data-parent="#accordion">
                                <div class="row">
                                    <?php if(empty($common1->user_type)): ?>
                                    <?php else: ?>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="info-box bg-aqua">
                                                <span class="info-box-icon"><img style="height:75px;width:75px;"
                                                                                 src="https://financialservicecenter.net/public/business/Business.png"></span>
                                                <div class="info-box-content">
                                                    <span class="info-box-text">Business</span>
                                                    <span class="info-box-number"><?php echo e($common1->total); ?></span>
                                                    <div class="progress">
                                                        <div class="progress-bar" style="width: 70%"></div>
                                                    </div>
                                                    <span class="progress-description">
                   <a href="fac-Bhavesh-0554/customer?user_type=Business" style="color:#fff">More info</a>
                  </span>
                                                </div>
                                                <!-- /.info-box-content -->
                                            </div>
                                            <!-- /.info-box -->
                                        </div>
                                    <?php endif; ?>
                                    <?php if(empty($common2->user_type)): ?>    <?php else: ?>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="info-box bg-green">
                                                <span class="info-box-icon"><i class="fa fa-thumbs-o-up"></i></span>
                                                <div class="info-box-content">
                                                    <span class="info-box-text">Non-Profit Organization</span>
                                                    <span class="info-box-number"><?php echo e($common2->total); ?></span>
                                                    <div class="progress">
                                                        <div class="progress-bar" style="width: 70%"></div>
                                                    </div>
                                                    <span class="progress-description">
                    <a href="fac-Bhavesh-0554/customer?user_type=Non-Profit Organization"
                       style="color:#fff">More info</a>
                  </span>
                                                </div>
                                                <!-- /.info-box-content -->
                                            </div>
                                            <!-- /.info-box -->
                                        </div>
                                    <?php endif; ?>
                                    <?php if(empty($common3->user_type)): ?> <?php else: ?>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="info-box bg-yellow">
                                                <span class="info-box-icon"><i class="fa fa-calendar"></i></span>
                                                <div class="info-box-content">
                                                    <span class="info-box-text">Service Industry</span>
                                                    <span class="info-box-number"><?php echo e($common3->total); ?></span>
                                                    <div class="progress">
                                                        <div class="progress-bar" style="width: 70%"></div>
                                                    </div>
                                                    <span class="progress-description">
                    <a href="fac-Bhavesh-0554/customer?user_type=Service Industry" style="color:#fff">More info</a>
                  </span>
                                                </div>
                                                <!-- /.info-box-content -->
                                            </div>
                                            <!-- /.info-box -->
                                        </div>
                                    <?php endif; ?>
                                    <?php if(empty($common4->user_type)): ?>    <?php else: ?>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="info-box bg-red">
                                                <span class="info-box-icon"><img style="height:75px;width:75px;"
                                                                                 src="https://financialservicecenter.net/public/business/Professions.png"></span>
                                                <div class="info-box-content">
                                                    <span class="info-box-text">Profession</span>
                                                    <span class="info-box-number"><?php echo e($common4->total); ?></span>
                                                    <div class="progress">
                                                        <div class="progress-bar" style="width: 70%"></div>
                                                    </div>
                                                    <span class="progress-description">
                    <a href="fac-Bhavesh-0554/customer?user_type=Profession" style="color:#fff">More info</a>
                  </span>
                                                </div>
                                                <!-- /.info-box-content -->
                                            </div>
                                            <!-- /.info-box -->
                                        </div>
                                    <?php endif; ?>
                                    <?php if(empty($common5->user_type)): ?>    <?php else: ?>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="info-box bg-red">
                                                <span class="info-box-icon"><i class="fa fa-comments-o"></i></span>
                                                <div class="info-box-content">
                                                    <span class="info-box-text">Investor</span>
                                                    <span class="info-box-number"><?php echo e($common5->total); ?></span>
                                                    <div class="progress">
                                                        <div class="progress-bar" style="width: 70%"></div>
                                                    </div>
                                                    <span class="progress-description">
                    <a href="fac-Bhavesh-0554/customer?user_type=Investor" style="color:#fff">More info</a>
                  </span>
                                                </div>
                                                <!-- /.info-box-content -->
                                            </div>
                                            <!-- /.info-box -->
                                        </div>
                                    <?php endif; ?>
                                    <?php if(empty($common6->user_type)): ?>

                                    <?php else: ?>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="info-box bg-yellow">
                                                <span class="info-box-icon"><i class="fa fa-calendar"></i></span>

                                                <div class="info-box-content">
                                                    <span class="info-box-text">Personal</span>
                                                    <span class="info-box-number"><?php echo e($common6->total); ?></span>

                                                    <div class="progress">
                                                        <div class="progress-bar" style="width: 70%"></div>
                                                    </div>
                                                    <span class="progress-description">
                    <a href="fac-Bhavesh-0554/customer?user_type=Personal" style="color:#fff">More info</a>
                  </span>
                                                </div>
                                                <!-- /.info-box-content -->
                                            </div>
                                            <!-- /.info-box -->
                                        </div>

                                    <?php endif; ?>
                                </div>

                            </div>
                            <div class="card-header accordion__question">
                                <a class="card-title">
                                    Apply Services
                                </a>
                            </div>
                            <div class="collapse accordion__answer">
                                <div class="row">
                                    <?php if(empty($servicetype->typeofcorp)): ?>
                                    <?php else: ?>
                                        <div class="col-md-3 col-sm-6 col-xs-12">

                                            <?php if(empty($s1->sign)): ?>

                                            <?php else: ?>
                                                <div class="gif">
                                                    <img src="<?php echo e(asset('public/dashboard/images/newimage.gif')); ?>" alt=""
                                                         class="img-responsive">
                                                </div>
                                            <?php endif; ?>
                                            <div class="info-box bg-red">
                                                <span class="info-box-icon"><i class="fa fa-comments-o"></i></span>

                                                <div class="info-box-content">
                                                    <span class="info-box-text">Accounting & Taxation</span>
                                                    <span class="info-box-number"><?php echo e($servicetype->total); ?></span>

                                                    <div class="progress">
                                                        <div class="progress-bar" style="width: 70%"></div>
                                                    </div>
                                                    <span class="progress-description">
                     <a href="fac-Bhavesh-0554/servicesprocess?typeofcorp=Accounting Bookkeeping Taxation Service"
                        style="color:#fff">More info</a>
                  </span>
                                                </div>
                                                <!-- /.info-box-content -->
                                            </div>
                                        </div>

                                    <?php endif; ?>
                                    <?php if(empty($servicetype2->typeofcorp)): ?>
                                    <?php else: ?>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <?php if(empty($s11->sign)): ?>

                                            <?php else: ?>
                                                <div class="gif">
                                                    <img src="<?php echo e(asset('public/dashboard/images/newimage.gif')); ?>" alt=""
                                                         class="img-responsive">
                                                </div>
                                            <?php endif; ?>
                                            <div class="info-box bg-yellow">
                                                <span class="info-box-icon"><i class="fa fa-calendar"></i></span>

                                                <div class="info-box-content">
                                                    <span class="info-box-text">Residential Mortgage</span>
                                                    <span class="info-box-number"><?php echo e($servicetype2->total); ?></span>

                                                    <div class="progress">
                                                        <div class="progress-bar" style="width: 70%"></div>
                                                    </div>
                                                    <span class="progress-description">
                     <a href="fac-Bhavesh-0554/servicesprocess?typeofcorp=Residential Mortgage Services"
                        style="color:#fff">More info</a>
                  </span>
                                                </div>
                                                <!-- /.info-box-content -->
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    <?php if(empty($servicetype3->typeofcorp)): ?>
                                    <?php else: ?>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <?php if(empty($s12->sign)): ?>

                                            <?php else: ?>
                                                <div class="gif">
                                                    <img src="<?php echo e(asset('public/dashboard/images/newimage.gif')); ?>" alt=""
                                                         class="img-responsive">
                                                </div><?php endif; ?>
                                            <div class="info-box bg-green">
                                                <span class="info-box-icon"><img style="height:75px;width:75px;"
                                                                                 src="<?php echo e(URL::asset('public/serviceimage/009.png')); ?>"></span>

                                                <div class="info-box-content">
                                                    <span class="info-box-text">Commercial Mortgage</span>
                                                    <span class="info-box-number"><?php echo e($servicetype3->total); ?></span>

                                                    <div class="progress">
                                                        <div class="progress-bar" style="width: 70%"></div>
                                                    </div>
                                                    <span class="progress-description">
                    <a href="fac-Bhavesh-0554/servicesprocess?typeofcorp=Commercial Mortgage Services"
                       style="color:#fff">More info</a>
                  </span>
                                                </div>
                                                <!-- /.info-box-content -->
                                            </div>
                                        </div><?php endif; ?>
                                    <?php if(empty($servicetype4->typeofcorp)): ?>
                                    <?php else: ?>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <?php if(empty($s13->sign)): ?>

                                            <?php else: ?>
                                                <div class="gif">
                                                    <img src="<?php echo e(asset('public/dashboard/images/newimage.gif')); ?>" alt=""
                                                         class="img-responsive">
                                                </div>
                                            <?php endif; ?>
                                            <div class="info-box bg-aqua">
                                                <span class="info-box-icon"><img width="75px" height="75px"
                                                                                 src="<?php echo e(URL::asset('public/serviceimage/005.png')); ?>"></i></span>

                                                <div class="info-box-content">
                                                    <span class="info-box-text">Financial</span>
                                                    <span class="info-box-number"><?php echo e($servicetype4->total); ?></span>

                                                    <div class="progress">
                                                        <div class="progress-bar" style="width: 70%"></div>
                                                    </div>
                                                    <span class="progress-description">
                    <a href="fac-Bhavesh-0554/servicesprocess?typeofcorp=Financial Services" style="color:#fff">More info</a>
                  </span>
                                                </div>
                                                <!-- /.info-box-content -->
                                            </div>


                                        </div>
                                    <?php endif; ?>
                                    <?php if(empty($servicetype5->typeofcorp)): ?>
                                    <?php else: ?>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <?php if(empty($s2->sign)): ?>

                                            <?php else: ?>
                                                <div class="gif">
                                                    <img src="<?php echo e(asset('public/dashboard/images/newimage.gif')); ?>" alt=""
                                                         class="img-responsive">
                                                </div><?php endif; ?>
                                            <div class="info-box bg-aqua">
                                                <span class="info-box-icon"><img style="width:75px;height:75px;"
                                                                                 src="<?php echo e(URL::asset('public/serviceimage/i-1.png')); ?>"></span>

                                                <div class="info-box-content">
                                                    <span class="info-box-text">Insurance</span>
                                                    <span class="info-box-number"><?php echo e($servicetype5->total); ?></span>

                                                    <div class="progress">
                                                        <div class="progress-bar" style="width: 70%"></div>
                                                    </div>
                                                    <span class="progress-description">
               <a href="fac-Bhavesh-0554/servicesprocess?typeofcorp=Insurance Service" style="color:#fff">More info</a>
                  </span>
                                                </div>
                                                <!-- /.info-box-content -->
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                </div>

                            </div>
                            <div class="card-header accordion__question">
                                <a class="card-title">
                                    Employment
                                </a>
                            </div>
                            <div class="collapse accordion__answer">
                                <div class="row">
                                    <?php if(empty($employments->count)): ?>
                                    <?php else: ?>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="info-box bg-aqua">
                                                <span class="info-box-icon"><i class="fa fa-bookmark-o"></i></span>

                                                <div class="info-box-content">
                                                    <span class="info-box-text">Advertise</span>
                                                    <span class="info-box-number"><?php echo e($employments->total); ?></span>

                                                    <div class="progress">
                                                        <div class="progress-bar" style="width: 70%"></div>
                                                    </div>
                                                    <span class="progress-description">
                    <a href="<?php echo e(url('/fac-Bhavesh-0554/employment')); ?>" style="color:#fff">More info</a>
                  </span>
                                                </div>
                                                <!-- /.info-box-content -->
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    <?php if(empty($application->status)): ?>
                                    <?php else: ?>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="info-box bg-green">
                                                <span class="info-box-icon"><i class="fa fa-newspaper-o"></i></span>

                                                <div class="info-box-content">
                                                    <span class="info-box-text">Application Recieve</span>
                                                    <span class="info-box-number"><?php echo e($application->total); ?></span>

                                                    <div class="progress">
                                                        <div class="progress-bar" style="width: 70%"></div>
                                                    </div>
                                                    <span class="progress-description">
                 <a href="<?php echo e(url('/fac-Bhavesh-0554/employeapplication')); ?>" style="color:#fff">More info</a>
                  </span>
                                                </div>
                                                <!-- /.info-box-content -->
                                            </div>


                                        </div>
                                    <?php endif; ?>
                                    <?php if(empty($candidate->status)): ?>
                                    <?php else: ?>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="info-box bg-yellow">
                                                <span class="info-box-icon"><i class="fa fa-folder"></i></span>

                                                <div class="info-box-content">
                                                    <span class="info-box-text">Candidata Data</span>
                                                    <span class="info-box-number"><?php echo e($candidate->total); ?></span>

                                                    <div class="progress">
                                                        <div class="progress-bar" style="width: 70%"></div>
                                                    </div>
                                                    <span class="progress-description">
                    <a href="<?php echo e(url('/fac-Bhavesh-0554/candidate')); ?>" style="color:#fff">More info</a>
                  </span>
                                                </div>
                                                <!-- /.info-box-content -->
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    <?php if(empty($emp->status)): ?>
                                    <?php else: ?>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="info-box bg-red">
                                                <span class="info-box-icon"><i class="fa fa-comments-o"></i></span>

                                                <div class="info-box-content">
                                                    <span class="info-box-text">Employee</span>
                                                    <span class="info-box-number"><?php echo e($emp->total); ?></span>

                                                    <div class="progress">
                                                        <div class="progress-bar" style="width: 70%"></div>
                                                    </div>
                                                    <span class="progress-description">
                   <a href="<?php echo e(url('/fac-Bhavesh-0554/employee')); ?>" style="color:#fff">More info</a>
                  </span>
                                                </div>
                                                <!-- /.info-box-content -->
                                            </div>

                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="card-header accordion__question">
                                <a class="card-title">
                                    Task
                                </a>
                            </div>
                            <div class="collapse accordion__answer">
                                <div class="table-responsive">
                                    <table class="table table-hover table-bordered" id="sampleTable2">
                                        <thead>
                                        <tr>
                                            <th style="width:9%">Date Of Task</th>
                                            <th>Assign By</th>
                                            <th>Assign To</th>
                                            <th width="8%">Title</th>
                                            <th style="width:25%">Description</th>
                                            <th>Read Status</th>
                                            <th>Task Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $__currentLoopData = $task; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $com): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php //echo $com->created_at;
                                            ?>
                                            <tr>

                                                <td style="width:10%"><?php echo e(date('M-d-Y',strtotime($com->created_at))); ?></td>
                                                <td style="width:15%"><?php if($com->admin_id==Auth::user()->id): ?> <?php echo e(Auth::user()->fname.' '.Auth::user()->lname); ?> <?php endif; ?> <?php $__currentLoopData = $empfsc; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $com1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php if($com->admin_id==$com1->id): ?> <?php echo e(ucwords($com1->name)); ?> <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></td>
                                                <td style="width:15%"><?php $__currentLoopData = $empfsc; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $com1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php if($com->employeeid==$com1->id): ?> <?php echo e(ucwords($com1->name)); ?> <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></td>
                                                <td style="width:15%"><?php echo e($com->title); ?></td>
                                                <td><span class="truncate"><?php echo $com->content; ?></span></td>
                                                <td><?php if($com->checked==1): ?> Read <?php endif; ?> <?php if($com->checked==0): ?> Not
                                                    Read <?php endif; ?></td>
                                                <td><?php if($com->status==2): ?> In Progress <?php endif; ?> <?php if($com->status==0): ?>
                                                        Wait <?php endif; ?>  <?php if($com->status==1): ?>
                                                        Start <?php endif; ?> <?php if($com->status==3): ?> End <?php endif; ?></td>
                                                <td><a class="btn-action btn-view-edit"
                                                       href="<?php echo e(route('task.edit',$com->id)); ?>"><i class="fa fa-edit"></i></a>
                                                </td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        <!--   <div class="card-header accordion__question">
                <a class="card-title">
                 Message <span><i class="fa fa-bell"></i> (
                <?php echo e($msg2-($msgdone- $msgforwork)); ?> )
              </span>
                </a>
            </div>
            <div class="collapse accordion__answer" style="margin-top:10px !important;">
               
               <div class="table-responsive">
					<table class="table table-hover table-bordered" id="ccc sampleTable2">
							<thead >
								<tr >
									<th style="width:17%">Date Of Messages / Time</th>
									<th>Message From</th>
									<th>Message For Whom</th>
									<th>Client No.</th>
									<th>Subject</th>
								    
                                    <th>Action</th>
								</tr>
							</thead>
							<tbody>
							    <?php $__currentLoopData = $msg1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $com): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($com->status1=='Done' or $com->status1=='Forword'): ?>
                            <?php else: ?>
                                <tr>
									<td><?php echo e($com->date); ?><br><?php echo e($com->time); ?></td>
									<td><?php $__currentLoopData = $emp1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $com1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php if($com->admin_id==$com1->id): ?> <?php echo e(ucwords($com1->firstName.' '.$com1->middleName.' '.$com1->lastName)); ?> <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> <?php if(empty($com->admin_id) && ($com->type=='Other Person')): ?> <?php echo e($com->clientname); ?> <?php endif; ?></td>
									<td><?php $__currentLoopData = $emp1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $com1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php if($com->employeeid==$com1->id): ?> <?php echo e(ucwords($com1->firstName.' '.$com1->middleName.' '.$com1->lastName)); ?> <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></td>
									<td><?php echo e($com->clientno); ?></td>
									<td><?php echo e($com->title); ?></td>
								 
                                    <td><a class="btn-action btn-view-edit" href="<?php echo e(route('msg.edit',$com->id)); ?>"><i class="fa fa-edit"></i></a> 
                                    <a class="delete btn-action btn-delete" id="<?php echo e($com->id); ?>"><span><i class="fa fa-trash"></i></span></a></td>
								</tr>
								<?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
						</table>
            </div>
        </div>!-->
                            <div class="card-header accordion__question">
                                <a class="card-title">
                                    FSC Employee Login Status
                                </a>
                            </div>
                            <div class="collapse accordion__answer">
                                <?php
                                function time_to_decimal($time)
                                {
                                    $timeArr = explode(':', $time);
                                    $decTime = (($timeArr[0] * 60) + ($timeArr[1]) + ($timeArr[2] / 60)) / 60;
                                    return $decTime;
                                }
                                $sum = 0;
                                $k = 1;
                                ?>
                                <?php $__currentLoopData = $schedule; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $schedule1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                    <?php $__currentLoopData = $set; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $s): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                        <?php $k++;?>
                                        <?php if($s->branch_city==$schedule1->emp_city): ?>
                                            <div class="panel-group" id="accordion<?php echo e($s->branch_city); ?>">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading"
                                                         style="border-color: #ddd;color: #fff;padding: 20px;padding-left:0px;">
                                                        <h4 class="panel-title"
                                                            style="padding:7px;margin-top: -20px;margin-right: 19px;position: absolute;border:1px solid black;text-align:center;background: #f39c12!important;color:#ffffff;">
                                                            <a style="text-transform:uppercase; color:#ffffff !important;"
                                                               data-toggle="collapse" data-parent="#accordion<?php echo e($k); ?>"
                                                               href="#collapseOne<?php echo e($k); ?>">
                                                                FSC-<?php echo e($s->branch_city); ?>

                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <script type="text/javascript">
                                                        $('#sampleTable<?php echo e($s->branch_city); ?>').DataTable();
                                                    </script>
                                                    <div id="collapseOne<?php echo e($k); ?>" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                            <div class="table-responsive">
                                                                <table class="table table-hover table-bordered"
                                                                       id="sampleTable<?php echo e($s->branch_city); ?>">
                                                                    <thead>
                                                                    <tr>
                                                                        <th style="width:10% !important;">Employee ID
                                                                        </th>
                                                                        <th>Employee Name</th>
                                                                        <th style="width:10%">Clock In</th>
                                                                        <th style="width:15%">Lunch In / Out</th>
                                                                        <th style="width:10%">Clock Out</th>
                                                                        <th>Status</th>
                                                                        <th style="width:11%">Total Hours(D)</th>
                                                                        <th style="width:10%">Note</th>
                                                                    </tr>
                                                                    </thead>

                                                                    <tbody>
                                                                    <?php $__currentLoopData = $employee; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $emp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                        <?php if($emp->fsccity == $s->branch_city): ?>
                                                                            <?php $__currentLoopData = $set1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $emp1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                                <?php if($emp->employee_id== $emp1->id): ?>
                                                                                    <?php
                                                                                    $in = date('H:i', strtotime($emp->emp_in));
                                                                                    $out = date('H:i', strtotime($emp->emp_out));
                                                                                    $lunch_in = date('H:i', strtotime($emp->launch_in));
                                                                                    $lunch_out = date('H:i', strtotime($emp->launch_out));
                                                                                    $launch_out_second = date('H:i', strtotime($emp->launch_out_second));
                                                                                    $launch_in_second = date('H:i', strtotime($emp->launch_in_second));
                                                                                    $total = date("H:i:s", strtotime($emp->emp_in));
                                                                                    if (empty($emp->emp_out)) {
                                                                                        $total1 = date("H:i:s");
                                                                                    } else {
                                                                                        $total1 = date("H:i:s", strtotime($emp->emp_out));
                                                                                    }
                                                                                    if (empty($emp->lunch_in)) {
                                                                                        $launch_out1 = "00:00:00";
                                                                                        $lunch_in1 = "00:00:00";
                                                                                    } else {
                                                                                        $lunch_in1 = date("H:i:s", strtotime($emp->launch_in));
                                                                                        if (empty($emp->lunch_out)) {
                                                                                            $launch_out1 = date("H:i:s");
                                                                                        } else {
                                                                                            $launch_out1 = date("H:i:s", strtotime($emp->launch_out));
                                                                                        }
                                                                                    }
                                                                                    if (empty($emp->launch_in_second)) {
                                                                                        $launch_in_second = "00:00:00";
                                                                                        $launch_out_second = "00:00:00";
                                                                                    } else {
                                                                                        $launch_in_second = date("H:i:s", strtotime($emp->launch_in_second));
                                                                                        if (empty($emp->launch_out_second)) {
                                                                                            $launch_out_second = date("H:i:s");
                                                                                        } else {
                                                                                            $launch_out_second = date("H:i:s", strtotime($emp->launch_out_second));
                                                                                        }
                                                                                    }
                                                                                    $total2 = (time_to_decimal($total1) - time_to_decimal($total)) - ((time_to_decimal($launch_out1) - time_to_decimal($lunch_in1)) + (time_to_decimal($launch_out1) - time_to_decimal($lunch_in1)));
                                                                                    ?>
                                                                                    <tr>
                                                                                        <td><?php echo e($emp1->employee_id); ?></td>
                                                                                        <td><?php echo e($emp1->firstName.' '.$emp1->middleName.' '.$emp1->lastName); ?></td>
                                                                                        <td style="text-align:center"><?php echo e(date("g:i a", strtotime($in))); ?></td>
                                                                                        <td style="text-align:center"><?php if($emp->launch_in== null): ?>
                                                                                                --/-- <?php else: ?> <?php echo e(date("g:i a", strtotime($lunch_in)).'-'.date("g:i a", strtotime($lunch_out))); ?><?php endif; ?></td>
                                                                                        <td style="text-align:center"><?php if($emp->emp_out== null): ?>
                                                                                                --/-- <?php else: ?><?php echo e(date("g:i a", strtotime($out))); ?><?php endif; ?></td>
                                                                                        <td style="text-align:center"><?php if($emp->emp_out== null): ?>
                                                                                                <p style="color:green">
                                                                                                    In</p> <?php else: ?> <p
                                                                                                    style="color:red">
                                                                                                    Out</p> <?php endif; ?></td>
                                                                                        <td style="text-align:center"><?php echo e(number_format($total2, 2)); ?></td>
                                                                                        <td style="text-align:center"><a
                                                                                                href="">View</a></td>
                                                                                    </tr>
                                                                                <?php endif; ?>
                                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                        <?php endif; ?>

                                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>


                            <div class="card-header accordion__question">
                                <a class="card-title">
                                    FSC Employee Schedule
                                </a>
                            </div>
                            <div class="collapse accordion__answer">
                                <div class="panel-group" id="accordionNorcross2, GA">
                                    <div class="panel panel-default">
                                        <div class="panel-heading"
                                             style="border-color: #ddd;color: #fff;padding: 20px;padding-left:0px;">
                                            <h4 class="panel-title"
                                                style="padding:7px;margin-top: -20px;margin-right: 19px;position: absolute;border:1px solid black;text-align:center;background: #f39c12!important;color:#ffffff;">
                                                <a style="text-transform:uppercase; color:#ffffff !important;"
                                                   data-toggle="collapse" data-parent="#accordion03"
                                                   href="#collapseOne03" aria-expanded="true">
                                                    FSC-Norcross, GA
                                                    <?PHP //ECHO "<PRE>";print_r($scheduleemployee);?>
                                                </a>
                                            </h4>
                                        </div>
                                        <script type="text/javascript">
                                            $('#sampleTableNorcross2, GA').DataTable();
                                        </script>
                                        <div id="collapseOne03" class="panel-collapse collapse" aria-expanded="true"
                                             style="">
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-hover table-bordered"
                                                           id="sampleTableNorcross, GA">
                                                        <thead>
                                                        <tr>
                                                            <th style="width:150px;">Employee ID</th>
                                                            <th style="text-align:center!important;">Employee Name</th>
                                                            <th style="text-align:center!important;">Period</th>

                                                            <th style="width:15%">Action</th>

                                                        </tr>
                                                        </thead>

                                                        <tbody>
                                                        <?php
                                                        foreach($scheduleemployee as $sch)
                                                        {?>
                                                        <?PHP
                                                        //  echo "<pre>"; print_r($scheduledates);
                                                        $monday = date('Y-m-d', strtotime('Monday this week'));
                                                        $sunday = date('Y-m-d', strtotime('Sunday this week'));
                                                        $m = strtotime($monday);
                                                        $f = strtotime($sunday);

                                                        ?>
                                                        <tr>
                                                            <td><?php echo $sch->employee_id;?></td>
                                                            <td><?php echo $sch->firstName . ' ' . $sch->middleName . ' ' . $sch->lastName;?></td>
                                                            <td> <?php echo date('M-d Y', strtotime($monday)) . ' To ' . date('M-d Y', strtotime($sunday));?></td>
                                                            <td style="text-align:center"><a
                                                                    href="#myModal_<?php echo $sch->employee_id;?>"
                                                                    data-toggle="modal">View</a>
                                                                <div id="printThis_<?php echo $sch->employee_id;?>">
                                                                <!-- <div id="printElements_<?php echo $sch->employee_id;?>">!-->

                                                                    <div id="myModal_<?php echo $sch->employee_id;?>"
                                                                         class="modal fade" role="dialog">
                                                                        <div class="modal-dialog">

                                                                            <!-- Modal content-->
                                                                            <div class="modal-content modal-lg">
                                                                                <div class="modal-header" style="">
                                                                                    <h4 class="modal-title"
                                                                                        style="position: relative; width: 98%; float: left;">
                                                                                        <?php //echo 'AA'.$sch->employee_id;?>
                                                                                        <img
                                                                                            src="https://financialservicecenter.net/public/dashboard/images/fsc_logo.png"
                                                                                            alt=""
                                                                                            class="img-responsive"
                                                                                            style="width:70px; margin:0px auto;">

                                                                                        <button
                                                                                            onclick="printDiv('<?php echo $sch->employee_id;?>')"
                                                                                            type="button"
                                                                                            class="btn btn-primary btn-sm pull-right"
                                                                                            style="margin-right: 15px; position: ABSOLUTE; right:0px;
    top: 0px;">Print
                                                                                        </button>

                                                                                    <!-- <button onclick="printElement('<?php echo $sch->employee_id;?>');" id="printbtn" type="button" class="btn btn-primary btn-sm pull-right" style="margin-right: 15px; position: ABSOLUTE; right:0px;
    top: 0px;">
    Print
  </button>!-->


                                                                                    </h4>
                                                                                    <button type="button" class="close"
                                                                                            data-dismiss="modal">&times;
                                                                                    </button>
                                                                                </div>

                                                                                <div class="modal-body">
                                                                                <!--<div class="row">
                                  <div class="col-md-5 text-left">
                                     <strong>EE ID : </strong> <?php echo $sch->employee_id;?>
                                                                                    </div>
                                  <div class="col-md-6 text-left">
                                      <strong>Name:</strong><?php echo $sch->firstName . ' ' . $sch->middleName . ' ' . $sch->lastName;?><br/>
                                      <strong>Schedule  Status: </strong> <br/>
                                      <strong>Period : </strong>
                                  </div>
                              </div>!-->
                                                                                    <style>
                                                                                        .printlogo {
                                                                                            display: none !important;
                                                                                        }
                                                                                    </style>
                                                                                    <div
                                                                                        id="<?php echo $sch->employee_id;?>">
                                                                                        <style>
                                                                                            @media  print {
                                                                                                .printlogo {
                                                                                                    display: block !important;
                                                                                                    padding-bottom: 10px;
                                                                                                    margin-bottom: 10px;
                                                                                                    border-bottom: 1px solid #ccc;
                                                                                                }
                                                                                            }
                                                                                        </style>

                                                                                        <div class="printlogo">
                                                                                            <img
                                                                                                src="https://financialservicecenter.net/public/dashboard/images/fsc_logo.png"
                                                                                                alt=""
                                                                                                class="img-responsive"
                                                                                                style="width:70px; display:block; margin:0px auto;">
                                                                                        </div>
                                                                                        <table class="table myclass"
                                                                                               border="0">
                                                                                            <thead>

                                                                                            <tr>
                                                                                                <th colspan="4"
                                                                                                    class="border-none"
                                                                                                    style="border: 0 !important; background:none!important;">
                                                                                                    <h3 style="margin-top:0px !important;">
                                                                                                        <b>
                                                                                                            Schedule</b>
                                                                                                    </h3>
                                                                                                </th>
                                                                                            </tr>

                                                                                            <tr>

                                                                                            </tr>

                                                                                            <tr>
                                                                                                <th class="border-none"
                                                                                                    style="border: 0 !important;background:none!important;float:left;">
                                                                                                    <b>Employee ID:</b>
                                                                                                    <span
                                                                                                        style="padding:10px ;border: 1px solid;background:none !important;color:#000 !important;"><?php echo $sch->employee_id;?></span>
                                                                                                </th>
                                                                                                <th colspan="2"
                                                                                                    class="border-none"
                                                                                                    style="border: 0 !important; background:none!important;text-align:right !important;vertical-align: middle;">
                                                                                                    Period:
                                                                                                    <?php echo date('M-d Y', strtotime($monday));?>

                                                                                                    To
                                                                                                    <?php echo date('M-d Y', strtotime($sunday));?>
                                                                                                </th>
                                                                                                <th class="border-none"
                                                                                                    style="border: 0 !important;background:none!important;">
                                                                                                    <b>Employee Name
                                                                                                        :</b>
                                                                                                    <span
                                                                                                        style="padding:10px ;border: 1px solid;background:none !important;color:#000 !important;"><?php echo $sch->firstName . ' ' . $sch->middleName . ' ' . $sch->lastName;?></span>
                                                                                                </th>
                                                                                            </tr>


                                                                                            </thead>
                                                                                        </table>


                                                                                        <table
                                                                                            class="table table-bordered table-striped">
                                                                                            <thead>

                                                                                            <tr>
                                                                                                <?php
                                                                                                for ( $i = $m; $i <= $f; $i = $i + 86400 )
                                                                                                {
                                                                                                // echo "<pre>";print_r($sch);
                                                                                                ?>
                                                                                                <th><?php echo date('M-d-Y', $i);?>
                                                                                                    <br/> <?php echo date('l', $i);?>
                                                                                                </th>
                                                                                                <?PHP
                                                                                                }
                                                                                                ?>
                                                                                            </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                            <tr>
                                                                                                <?php

                                                                                                foreach($scheduledates as $schdates)
                                                                                                {
                                                                                                if($sch->employee_id == $schdates->employee_id)
                                                                                                {

                                                                                                ?>
                                                                                                <td style="text-align:center !important;"><?php echo $schdates->clockin;?>
                                                                                                    - <br><span
                                                                                                        style="margin-left:-6% !important;"><?php echo $schdates->clockout;?></span>
                                                                                                </td>
                                                                                                <?php
                                                                                                }

                                                                                                }
                                                                                                /*  
                                    for ( $i = $m; $i <= $f; $i = $i + 86400 ) 
                                    {
                                    ?>
                                    <td><?php echo $sch->schedule_in_time;?> To <?php echo $sch->schedule_out_time;?> </td>
                                    <?php
                                    }*/
                                                                                                ?>
                                                                                                <td style="width:0px !important;">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                                <td style="width:0px !important;">
                                                                                                    &nbsp;
                                                                                                </td>

                                                                                            </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="modal-footer">
                                                                                    <button type="button"
                                                                                            class="btn btn-default btnpopclose"
                                                                                            data-dismiss="modal"
                                                                                            onclick='closemodalpopup()'>
                                                                                        Close
                                                                                    </button>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>

                                                        </tr>

                                                        <?php
                                                        }
                                                        ?>
                                                        <tr>

                                                            <td>FSC</td>
                                                            <td>All</td>
                                                            <td><?php echo date('M-d Y', strtotime($schedulesetup->sch_start_date));?>
                                                                to <?php echo date('M-d Y', strtotime($schedulesetup->sch_end_date));?></td>

                                                            <td style="text-align:center"><a href="#myModalEmloyeeAll"
                                                                                             data-toggle="modal"
                                                                                             data-target="#myModalEmloyeeAll">View</a>
                                                                <!-- Modal -->
                                                                <div>
                                                                    <div id="myModalEmloyeeAll" class="modal fade"
                                                                         role="dialog">
                                                                        <div class="modal-dialog" style="width:65%;">

                                                                            <!-- Modal content-->
                                                                            <div class="modal-content">
                                                                                <div class="modal-header">
                                                                                    <button type="button" class="close"
                                                                                            data-dismiss="modal">&times;
                                                                                    </button>
                                                                                    <h4 class="modal-title"><img
                                                                                            src="https://financialservicecenter.net/public/dashboard/images/fsc_logo.png"
                                                                                            alt=""
                                                                                            class="img-responsive"
                                                                                            style="width:70px; margin:0px auto;"/>
                                                                                        <!-- <button onclick="printdivsAll();" type="button" class="btn btn-primary btn-sm pull-right" style="margin-right: 15px; position: ABSOLUTE; right:0px;
    top: 0px;">Print</button>-->
                                                                                        <button
                                                                                            onclick="printDiv('printMe')"
                                                                                            type="button"
                                                                                            class="btn btn-primary btn-sm pull-right"
                                                                                            style="margin-right: 15px; position: ABSOLUTE; right:0px;
    top: 0px;">Print
                                                                                        </button>


                                                                                        <!--<button onclick="print2();" type="button" class="btn btn-primary btn-sm pull-right" style="margin-right: 15px; position: ABSOLUTE; right:0px;
    top: 0px;">
    Print
  </button>!-->

                                                                                    </h4>
                                                                                </div>
                                                                                <div class="modal-body"
                                                                                     style="height:400px; overflow:auto">

                                                                                    <div id='printMe'>
                                                                                        <style>
                                                                                            @media  print {
                                                                                                .logoprintbox {
                                                                                                    display: block;
                                                                                                }
                                                                                            }
                                                                                        </style>

                                                                                        <div class="logoprintbox">
                                                                                            <img
                                                                                                src="https://financialservicecenter.net/public/dashboard/images/fsc_logo.png"
                                                                                                alt=""
                                                                                                class="img-responsive"
                                                                                                style="width:70px; margin:0px auto;"/>
                                                                                        </div>
                                                                                        <h5 style="font-size:18px; text-align:center">
                                                                                            <strong> Schedule Dates
                                                                                                : <?php echo $schedulesetup->sch_start_date;?>
                                                                                                To <?php echo $schedulesetup->sch_end_date;?> </strong>
                                                                                        </h5>
                                                                                        <?php //echo "<pre>";print_r($scheduleemployee);?>

                                                                                        <table
                                                                                            class="table table-bordered table-striped"
                                                                                            style="font-size:11px;">
                                                                                            <thead>
                                                                                            <?php
                                                                                            $stdate = strtotime($schedulesetup->sch_start_date);
                                                                                            $eddate = strtotime($schedulesetup->sch_end_date);

                                                                                            ?>
                                                                                            <tr>
                                                                                                <th>Date</th>
                                                                                                <th>>>>></th>
                                                                                                <?php
                                                                                                for ( $i = $stdate; $i <= $eddate; $i = $i + 86400 )
                                                                                                {
                                                                                                $iday = date('D', $i);
                                                                                                if($iday != 'Sun')
                                                                                                {
                                                                                                ?>
                                                                                                <th><?php echo date('m/d', $i);?></th>
                                                                                                <?PHP
                                                                                                }
                                                                                                }
                                                                                                ?>
                                                                                            </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                            <tr>
                                                                                                <td style="width:70px;">
                                                                                                    <b>Emp.#</b></td>
                                                                                                <td><b>Emp.Name</b></td>
                                                                                                <?php
                                                                                                for ( $i = $stdate; $i <= $eddate; $i = $i + 86400 )
                                                                                                {
                                                                                                $iday = date('D', $i);
                                                                                                if($iday != 'Sun')
                                                                                                {
                                                                                                ?>
                                                                                                <td style="width:76px; "
                                                                                                    class="text-center">
                                                                                                    <b><?php echo date('D', $i);?></b>
                                                                                                </td>
                                                                                                <?PHP
                                                                                                }
                                                                                                }
                                                                                                ?>
                                                                                            </tr>
                                                                                            <?php
                                                                                            foreach($scheduleemployee as $schemployee)
                                                                                            {?>
                                                                                            <tr>
                                                                                                <td><?php echo $schemployee->employee_id;?></td>
                                                                                                <td style="white-space:nowrap;"><?php echo $schemployee->firstName . ' ' . $schemployee->middleName . ' ' . $schemployee->lastName;?></td>
                                                                                                <?php



                                                                                                foreach($scheduledates as $schdates)
                                                                                                {
                                                                                                if($schemployee->employee_id == $schdates->employee_id)
                                                                                                {
                                                                                                ?>
                                                                                                <td><?php echo $schdates->clockin;?>
                                                                                                    - <?php echo $schdates->clockout;?> </td>

                                                                                                <?php
                                                                                                }

                                                                                                }
                                                                                                /*  for ( $i = $m; $i <= $f; $i = $i + 86400 ) 
                                    {
                                    ?>
                                    <td><?php echo $sch->schedule_in_time;?> To <?php echo $sch->schedule_out_time;?> </td>
                                    <?php
                                    }*/
                                                                                                ?>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>&nbsp;</td>
                                                                                                <?php
                                                                                                foreach($scheduledates as $schdates)
                                                                                                {
                                                                                                if($schemployee->employee_id == $schdates->employee_id)
                                                                                                {
                                                                                                ?>
                                                                                                <td><?php echo $schdates->clockin;?>
                                                                                                    - <?php echo $schdates->clockout;?> </td>

                                                                                                <?php
                                                                                                }

                                                                                                }
                                                                                                ?>

                                                                                            </tr>
                                                                                            <?php
                                                                                            }
                                                                                            ?>
                                                                                            </tbody>
                                                                                        </table>

                                                                                        <table
                                                                                            class="table table-bordered table-striped">
                                                                                            <thead>

                                                                                            <tr>
                                                                                                <th>&nbsp;</th>
                                                                                                <th> Date >>>></th>
                                                                                                <th>&nbsp;</th>

                                                                                                <?php
                                                                                                for ( $i = $stdate; $i <= $eddate; $i = $i + 86400 )
                                                                                                {
                                                                                                $iday = date('D', $i);
                                                                                                if($iday != 'Sun')
                                                                                                {
                                                                                                ?>
                                                                                                <th><?php echo date('m/d', $i);?></th>
                                                                                                <?PHP
                                                                                                }
                                                                                                }
                                                                                                ?>
                                                                                            </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                            <tr>
                                                                                                <td style="width:50px;">
                                                                                                    <b>Emp.#</b></td>
                                                                                                <td style="width:280px;">
                                                                                                    <b>Emp.Name</b></td>
                                                                                                <td style="width:30px;"></td>
                                                                                                <?php
                                                                                                for ( $i = $stdate; $i <= $eddate; $i = $i + 86400 )
                                                                                                {
                                                                                                $iday = date('D', $i);
                                                                                                if($iday != 'Sun')
                                                                                                {
                                                                                                ?>
                                                                                                <td style="width:76px; "
                                                                                                    class="text-center">
                                                                                                    <b><?php echo date('D', $i);?></b>
                                                                                                </td>
                                                                                                <?PHP
                                                                                                }
                                                                                                }
                                                                                                ?>
                                                                                            </tr>
                                                                                            <?php
                                                                                            foreach($scheduleemployee as $schemployee)
                                                                                            {?>
                                                                                            <tr>
                                                                                                <td style="width:60px; white-space:nowrap;"><?php echo $schemployee->employee_id;?></td>
                                                                                                <td><?php echo $schemployee->firstName . ' ' . $schemployee->middleName . ' ' . $schemployee->lastName;?></td>
                                                                                                <td style="width:30px;">
                                                                                                    In
                                                                                                </td>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>&nbsp;</td>


                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td></td>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>Out</td>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>&nbsp;</td>

                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td></td>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>In</td>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>&nbsp;</td>

                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td></td>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>Out</td>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>&nbsp;</td>
                                                                                                <td>&nbsp;</td>

                                                                                            </tr>


                                                                                            <?php
                                                                                            }
                                                                                            ?>


                                                                                            </tbody>
                                                                                        </table>

                                                                                        <p style="text-align:left">
                                                                                            <strong>Note:</strong>
                                                                                            Employee Responsibility:</p>
                                                                                        <ul>
                                                                                            <li>
                                                                                                1. Record clock in and
                                                                                                out immediately when
                                                                                                they come in/out
                                                                                            </li>
                                                                                            <li> 2. All the employee
                                                                                                make sure that time
                                                                                                recorded paid for that
                                                                                                in their payroll
                                                                                                If not please notify
                                                                                                immediately to payroll
                                                                                                department.
                                                                                            </li>
                                                                                            <li> 3. Leave request get
                                                                                                approved before 7 days
                                                                                                in advance
                                                                                            </li>
                                                                                            <li> 4. All the employee
                                                                                                must take lunch brake as
                                                                                                per company rules
                                                                                            </li>
                                                                                            <li> 5. If you have any
                                                                                                question contact office
                                                                                                manager immediately
                                                                                            </li>
                                                                                            <li> 6. Office Lunch Hrs.
                                                                                                1.30 p.m. to 2.00 p.m.
                                                                                            </li>
                                                                                            <li> 7. Work as per schdule
                                                                                                time - After schdule hrs
                                                                                                will not be consider
                                                                                            </li>
                                                                                            <li> 8. Overtime will not be
                                                                                                granted without prior
                                                                                                approval or sign by
                                                                                                supervisor
                                                                                            </li>
                                                                                        </ul>

                                                                                    </div>
                                                                                </div>
                                                                                <div class="modal-footer">
                                                                                    <button type="button"
                                                                                            class="btn btn-default btnpopclose"
                                                                                            data-dismiss="modal"
                                                                                            onclick='closemodalpopup()'>
                                                                                        Close
                                                                                    </button>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>


                                            </div>


                                        </div>
                                    </div>

                                </div>

                                <script type="text/javascript">
                                    function closemodalpopup() {

                                        // alert();
                                        var sactive = document.querySelector(".modal-backdrop");
                                        sactive.classList.remove("in");
                                        sactive.remove();

                                        var bmodal = document.querySelector(".modal-open");
                                        bmodal.classList.remove("modal-open");


                                        document.getElementById("myModalEmloyeeAll").classList.remove("in");

                                        var mpop = document.getElementById("myModalEmloyeeAll")

                                        mpop.removeAttribute("style");


                                        location.reload();


                                        //  $('#myModalEmloyeeAll').modal('hide');
                                    }

                                    function printDiv(divName) {
                                        var printContents = document.getElementById(divName).innerHTML;
                                        w = window.open();
                                        w.document.write(printContents);
                                        w.print();
                                        w.close();
                                    }

                                </script>
                            </div>


                        </div>
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
        </section>

        <script>
            function print1(val) {

                printJS({
                    printable: 'printElements_' + val,
                    type: 'html',
                    targetStyles: ['*']
                })
            }

            function print2() {
                printJS({
                    printable: 'printAllThis1',
                    type: 'html',
                    targetStyles: ['*']
                })
            }

            //document.getElementById('printButton').addEventListener ("click", print1)

            // Print.js
            !function (e, t) {
                "object" == typeof exports && "object" == typeof module ? module.exports = t() : "function" == typeof define && define.amd ? define("print-js", [], t) : "object" == typeof exports ? exports["print-js"] = t() : e["print-js"] = t()
            }(this, function () {
                return function (e) {
                    function t(i) {
                        if (n[i]) return n[i].exports;
                        var o = n[i] = {i: i, l: !1, exports: {}};
                        return e[i].call(o.exports, o, o.exports, t), o.l = !0, o.exports
                    }

                    var n = {};
                    return t.m = e, t.c = n, t.i = function (e) {
                        return e
                    }, t.d = function (e, n, i) {
                        t.o(e, n) || Object.defineProperty(e, n, {configurable: !1, enumerable: !0, get: i})
                    }, t.n = function (e) {
                        var n = e && e.__esModule ? function () {
                            return e.default
                        } : function () {
                            return e
                        };
                        return t.d(n, "a", n), n
                    }, t.o = function (e, t) {
                        return Object.prototype.hasOwnProperty.call(e, t)
                    }, t.p = "./", t(t.s = 10)
                }([function (e, t, n) {
                    "use strict";

                    function i(e, t) {
                        if (e.focus(), r.a.isEdge() || r.a.isIE()) try {
                            e.contentWindow.document.execCommand("print", !1, null)
                        } catch (t) {
                            e.contentWindow.print()
                        }
                        r.a.isIE() || r.a.isEdge() || e.contentWindow.print(), r.a.isIE() && "pdf" === t.type && setTimeout(function () {
                            e.parentNode.removeChild(e)
                        }, 2e3), t.showModal && a.a.close(), t.onLoadingEnd && t.onLoadingEnd()
                    }

                    function o(e, t, n) {
                        void 0 === e.naturalWidth || 0 === e.naturalWidth ? setTimeout(function () {
                            o(e, t, n)
                        }, 500) : i(t, n)
                    }

                    var r = n(1), a = n(3), d = {
                        send: function (e, t) {
                            document.getElementsByTagName("body")[0].appendChild(t);
                            var n = document.getElementById(e.frameId);
                            "pdf" === e.type && (r.a.isIE() || r.a.isEdge()) ? n.setAttribute("onload", i(n, e)) : t.onload = function () {
                                if ("pdf" === e.type) i(n, e); else {
                                    var t = n.contentWindow || n.contentDocument;
                                    t.document && (t = t.document), t.body.innerHTML = e.htmlData, "image" === e.type ? o(t.getElementById("printableImage"), n, e) : i(n, e)
                                }
                            }
                        }
                    };
                    t.a = d
                }, function (e, t, n) {
                    "use strict";
                    var i = {
                        isFirefox: function () {
                            return "undefined" != typeof InstallTrigger
                        }, isIE: function () {
                            return -1 !== navigator.userAgent.indexOf("MSIE") || !!document.documentMode
                        }, isEdge: function () {
                            return !i.isIE() && !!window.StyleMedia
                        }, isChrome: function () {
                            return !!window.chrome && !!window.chrome.webstore
                        }, isSafari: function () {
                            return Object.prototype.toString.call(window.HTMLElement).indexOf("Constructor") > 0 || -1 !== navigator.userAgent.toLowerCase().indexOf("safari")
                        }
                    };
                    t.a = i
                }, function (e, t, n) {
                    "use strict";

                    function i(e, t) {
                        return '<div style="font-family:' + t.font + " !important; font-size: " + t.font_size + ' !important; width:100%;">' + e + "</div>"
                    }

                    function o(e) {
                        return e.charAt(0).toUpperCase() + e.slice(1)
                    }

                    function r(e, t) {
                        var n = document.defaultView || window, i = [], o = "";
                        if (n.getComputedStyle) {
                            i = n.getComputedStyle(e, "");
                            var r = t.targetStyles || ["border", "box", "break", "text-decoration"],
                                a = t.targetStyle || ["clear", "display", "width", "min-width", "height", "min-height", "max-height"];
                            t.honorMarginPadding && r.push("margin", "padding"), t.honorColor && r.push("color");
                            for (var d = 0; d < i.length; d++) for (var l = 0; l < r.length; l++) "*" !== r[l] && -1 === i[d].indexOf(r[l]) && -1 === a.indexOf(i[d]) || (o += i[d] + ":" + i.getPropertyValue(i[d]) + ";")
                        } else if (e.currentStyle) {
                            i = e.currentStyle;
                            for (var s in i) -1 !== i.indexOf("border") && -1 !== i.indexOf("color") && (o += s + ":" + i[s] + ";")
                        }
                        return o += "max-width: " + t.maxWidth + "px !important;" + t.font_size + " !important;"
                    }

                    function a(e, t) {
                        for (var n = 0; n < e.length; n++) {
                            var i = e[n], o = i.tagName;
                            if ("INPUT" === o || "TEXTAREA" === o || "SELECT" === o) {
                                var d = r(i, t), l = i.parentNode,
                                    s = "SELECT" === o ? document.createTextNode(i.options[i.selectedIndex].text) : document.createTextNode(i.value),
                                    c = document.createElement("div");
                                c.appendChild(s), c.setAttribute("style", d), l.appendChild(c), l.removeChild(i)
                            } else i.setAttribute("style", r(i, t));
                            var p = i.children;
                            p && p.length && a(p, t)
                        }
                    }

                    function d(e, t, n) {
                        var i = document.createElement("h1"), o = document.createTextNode(t);
                        i.appendChild(o), i.setAttribute("style", n), e.insertBefore(i, e.childNodes[0])
                    }

                    t.a = i, t.b = o, t.c = r, t.d = a, t.e = d
                }, function (e, t, n) {
                    "use strict";
                    var i = {
                        show: function (e) {
                            var t = document.createElement("div");
                            t.setAttribute("style", "font-family:sans-serif; display:table; text-align:center; font-weight:300; font-size:30px; left:0; top:0;position:fixed; z-index: 9990;color: #0460B5; width: 100%; height: 100%; background-color:rgba(255,255,255,.9);transition: opacity .3s ease;"), t.setAttribute("id", "printJS-Modal");
                            var n = document.createElement("div");
                            n.setAttribute("style", "display:table-cell; vertical-align:middle; padding-bottom:100px;");
                            var o = document.createElement("div");
                            o.setAttribute("class", "printClose"), o.setAttribute("id", "printClose"), n.appendChild(o);
                            var r = document.createElement("span");
                            r.setAttribute("class", "printSpinner"), n.appendChild(r);
                            var a = document.createTextNode(e.modalMessage);
                            n.appendChild(a), t.appendChild(n), document.getElementsByTagName("body")[0].appendChild(t), document.getElementById("printClose").addEventListener("click", function () {
                                i.close()
                            })
                        }, close: function () {
                            var e = document.getElementById("printJS-Modal");
                            e.parentNode.removeChild(e)
                        }
                    };
                    t.a = i
                }, function (e, t, n) {
                    "use strict";
                    Object.defineProperty(t, "__esModule", {value: !0});
                    var i = n(7), o = i.a.init;
                    "undefined" != typeof window && (window.printJS = o), t.default = o
                }, function (e, t, n) {
                    "use strict";
                    var i = n(2), o = n(0);
                    t.a = {
                        print: function (e, t) {
                            var r = document.getElementById(e.printable);
                            if (!r) return window.console.error("Invalid HTML element id: " + e.printable), !1;
                            var a = document.createElement("div");
                            a.appendChild(r.cloneNode(!0)), a.setAttribute("style", "display:none;"), a.setAttribute("id", "printJS-html"), r.parentNode.appendChild(a), a = document.getElementById("printJS-html"), a.setAttribute("style", n.i(i.c)(a, e) + "margin:0 !important;");
                            var d = a.children;
                            n.i(i.d)(d, e), e.header && n.i(i.e)(a, e.header, e.headerStyle), a.parentNode.removeChild(a), e.htmlData = n.i(i.a)(a.innerHTML, e), o.a.send(e, t)
                        }
                    }
                }, function (e, t, n) {
                    "use strict";
                    var i = n(2), o = n(0);
                    t.a = {
                        print: function (e, t) {
                            var r = document.createElement("img");
                            r.src = e.printable, r.onload = function () {
                                r.setAttribute("style", "width:100%;"), r.setAttribute("id", "printableImage");
                                var a = document.createElement("div");
                                a.setAttribute("style", "width:100%"), a.appendChild(r), e.header && n.i(i.e)(a, e.header, e.headerStyle), e.htmlData = a.outerHTML, o.a.send(e, t)
                            }
                        }
                    }
                }, function (e, t, n) {
                    "use strict";
                    var i = n(1), o = n(3), r = n(9), a = n(5), d = n(6), l = n(8),
                        s = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
                            return typeof e
                        } : function (e) {
                            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
                        }, c = ["pdf", "html", "image", "json"];
                    t.a = {
                        init: function () {
                            var e = {
                                printable: null,
                                fallbackPrintable: null,
                                type: "pdf",
                                header: null,
                                headerStyle: "font-weight: 300;",
                                maxWidth: 800,
                                font: "TimesNewRoman",
                                font_size: "12pt",
                                honorMarginPadding: !0,
                                honorColor: !1,
                                properties: null,
                                gridHeaderStyle: "font-weight: bold;",
                                gridStyle: "border: 1px solid lightgray; margin-bottom: -1px;",
                                showModal: !1,
                                onLoadingStart: null,
                                onLoadingEnd: null,
                                modalMessage: "Retrieving Document...",
                                frameId: "printJS",
                                htmlData: "",
                                documentTitle: "Document",
                                targetStyle: null,
                                targetStyles: null
                            }, t = arguments[0];
                            if (void 0 === t) throw new Error("printJS expects at least 1 attribute.");
                            switch (void 0 === t ? "undefined" : s(t)) {
                                case"string":
                                    e.printable = encodeURI(t), e.fallbackPrintable = e.printable, e.type = arguments[1] || e.type;
                                    break;
                                case"object":
                                    e.printable = t.printable, e.fallbackPrintable = void 0 !== t.fallbackPrintable ? t.fallbackPrintable : e.printable, e.type = void 0 !== t.type ? t.type : e.type, e.frameId = void 0 !== t.frameId ? t.frameId : e.frameId, e.header = void 0 !== t.header ? t.header : e.header, e.headerStyle = void 0 !== t.headerStyle ? t.headerStyle : e.headerStyle, e.maxWidth = void 0 !== t.maxWidth ? t.maxWidth : e.maxWidth, e.font = void 0 !== t.font ? t.font : e.font, e.font_size = void 0 !== t.font_size ? t.font_size : e.font_size, e.honorMarginPadding = void 0 !== t.honorMarginPadding ? t.honorMarginPadding : e.honorMarginPadding, e.properties = void 0 !== t.properties ? t.properties : e.properties, e.gridHeaderStyle = void 0 !== t.gridHeaderStyle ? t.gridHeaderStyle : e.gridHeaderStyle, e.gridStyle = void 0 !== t.gridStyle ? t.gridStyle : e.gridStyle, e.showModal = void 0 !== t.showModal ? t.showModal : e.showModal, e.onLoadingStart = void 0 !== t.onLoadingStart ? t.onLoadingStart : e.onLoadingStart, e.onLoadingEnd = void 0 !== t.onLoadingEnd ? t.onLoadingEnd : e.onLoadingEnd, e.modalMessage = void 0 !== t.modalMessage ? t.modalMessage : e.modalMessage, e.documentTitle = void 0 !== t.documentTitle ? t.documentTitle : e.documentTitle, e.targetStyle = void 0 !== t.targetStyle ? t.targetStyle : e.targetStyle, e.targetStyles = void 0 !== t.targetStyles ? t.targetStyles : e.targetStyles;
                                    break;
                                default:
                                    throw new Error('Unexpected argument type! Expected "string" or "object", got ' + (void 0 === t ? "undefined" : s(t)))
                            }
                            if (!e.printable) throw new Error("Missing printable information.");
                            if (!e.type || "string" != typeof e.type || -1 === c.indexOf(e.type.toLowerCase())) throw new Error("Invalid print type. Available types are: pdf, html, image and json.");
                            e.showModal && o.a.show(e), e.onLoadingStart && e.onLoadingStart();
                            var n = document.getElementById(e.frameId);
                            n && n.parentNode.removeChild(n);
                            var p = void 0;
                            switch (p = document.createElement("iframe"), p.setAttribute("style", "display:none;"), p.setAttribute("id", e.frameId), "pdf" !== e.type && (p.srcdoc = "<html><head><title>" + e.documentTitle + "</title></head><body></body></html>"), e.type) {
                                case"pdf":
                                    if (i.a.isFirefox() || i.a.isEdge() || i.a.isIE()) {
                                        window.open(e.fallbackPrintable, "_blank").focus(), e.showModal && o.a.close(), e.onLoadingEnd && e.onLoadingEnd()
                                    } else r.a.print(e, p);
                                    break;
                                case"image":
                                    d.a.print(e, p);
                                    break;
                                case"html":
                                    a.a.print(e, p);
                                    break;
                                case"json":
                                    l.a.print(e, p);
                                    break;
                                default:
                                    throw new Error("Invalid print type. Available types are: pdf, html, image and json.")
                            }
                        }
                    }
                }, function (e, t, n) {
                    "use strict";

                    function i(e) {
                        var t = e.printable, i = e.properties,
                            r = '<div style="display:flex; flex-direction: column;">';
                        r += '<div style="flex:1 1 auto; display:flex;">';
                        for (var a = 0; a < i.length; a++) r += '<div style="flex:1; padding:5px;' + e.gridHeaderStyle + '">' + n.i(o.b)(i[a]) + "</div>";
                        r += "</div>";
                        for (var d = 0; d < t.length; d++) {
                            r += '<div style="flex:1 1 auto; display:flex;">';
                            for (var l = 0; l < i.length; l++) {
                                var s = t[d], c = i[l].split(".");
                                if (c.length > 1) for (var p = 0; p < c.length; p++) s = s[c[p]]; else s = s[i[l]];
                                r += '<div style="flex:1; padding:5px;' + e.gridStyle + '">' + s + "</div>"
                            }
                            r += "</div>"
                        }
                        return r += "</div>"
                    }

                    var o = n(2), r = n(0),
                        a = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
                            return typeof e
                        } : function (e) {
                            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
                        };
                    t.a = {
                        print: function (e, t) {
                            if ("object" !== a(e.printable)) throw new Error("Invalid javascript data object (JSON).");
                            if (!e.properties || "object" !== a(e.properties)) throw new Error("Invalid properties array for your JSON data.");
                            var d = "";
                            e.header && (d += '<h1 style="' + e.headerStyle + '">' + e.header + "</h1>"), d += i(e), e.htmlData = n.i(o.a)(d, e), r.a.send(e, t)
                        }
                    }
                }, function (e, t, n) {
                    "use strict";

                    function i(e, t) {
                        t.setAttribute("src", e.printable), r.a.send(e, t)
                    }

                    var o = n(1), r = n(0);
                    t.a = {
                        print: function (e, t) {
                            if (e.showModal || e.onLoadingStart || o.a.isIE()) {
                                var n = new window.XMLHttpRequest;
                                n.addEventListener("load", i(e, t)), n.open("GET", window.location.origin + "/" + e.printable, !0), n.send()
                            } else i(e, t)
                        }
                    }
                }, function (e, t, n) {
                    e.exports = n(4)
                }])
            });
        </script>
        <div>
        </div>


        <!-- Modal -->


        <script type="text/javascript">
            $('#sampleTable').DataTable();
            $('#sampleTable2').DataTable({
                'paging': true,
                'lengthChange': true,
                'searching': false,
                'ordering': false,
                'info': true,
                'autoWidth': false,
                "dom": '<"top"f>rt<"bottom"lip><"clear">'
            });

            $(document).ready(function () {
                (function () {
                    var showChar = 100;
                    var ellipsestext = "...";

                    $(".truncate").each(function () {
                        var content = $(this).html();
                        if (content.length > showChar) {
                            var c = content.substr(0, showChar);
                            var h = content;
                            var html =
                                '<div class="truncate-text" style="display:block">' +
                                c +
                                '<span class="moreellipses">' +
                                ellipsestext +
                                '&nbsp;&nbsp;<a href="" class="moreless more">more >></a></span></span></div><div class="truncate-text" style="display:none">' +
                                h +
                                '<a href="" class="moreless less"><< Less</a></span></div>';

                            $(this).html(html);
                        }
                    });

                    $(".moreless").click(function () {
                        var thisEl = $(this);
                        var cT = thisEl.closest(".truncate-text");
                        var tX = ".truncate-text";

                        if (thisEl.hasClass("less")) {
                            cT.prev(tX).toggle();
                            cT.slideToggle();
                        } else {
                            cT.toggle();
                            cT.next(tX).fadeToggle();
                        }
                        return false;
                    });
                    /* end iffe */
                })();

                /* end ready */
            });
        </script>

        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document" style="width:70%">
                <div class="modal-content">
                    <div class="modal-header" style="background:#00c0ef !important;color:white;text-align:center;">
                        <h3 class="modal-title" id="exampleModalLabel" style="color:#ffffff!important;"><span
                                class="btn btn-primary countbox" style="border:none !important;padding-right: 6px;">Count :</span>
                            <span class="btn btn-primary countbox">0</span>Reminder <small>( 30 Days )</small>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </h3>

                    </div>
                    <?php
                    $ctax1 = array();
                    foreach ($clientttaxation as $ctax) {
                        $ctax1[] = $ctax->taxation_service;
                    }
                    ?>
                    <div class="modal-body">
                        <div class="panel-group" id="accordion" role="tablist" style="margin-top:15px;">
                            <?php $__currentLoopData = $taxtitles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $taxx): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if(in_array($taxx->id,$ctax1)): ?>

                                    <?php
                                    $cntss1 = 0;
                                    $cntss = array();
                                    foreach ($commonone1 as $taxfederal1) {

                                        if ($taxfederal1->status == $taxx->id) {

                                            // echo $taxx->id;
                                            // echo $taxfederal1->tids;
                                            $cntss1++;
                                            // echo $cnts;
                                            // if(!in_array($taxfederal1->ids,$arrc))
                                            //{

                                            if (isset($taxfederal1->expiredate) != '' && isset($taxfederal1->expiredate) != null) {
                                                $exp_date = date('M-d-Y', strtotime("-30 days", strtotime($taxfederal1->expiredate)));
                                                $exp_dates = strtotime($exp_date);

                                                $notifyexp_date = date('M-d-Y', strtotime("-10 days", strtotime($taxfederal1->expiredate)));
                                                $notifyexp_dates = strtotime($exp_date);

                                                $today = date('M-d-Y');
                                                $match_today = strtotime($today);

                                                if ($match_today >= $exp_dates && $match_today <= $notifyexp_dates) {
                                                    $cntss[] = $cntss1++;

                                                }

                                                // }
                                            }
                                        }
                                    }
                                    //echo count($cntss);
                                    ?>

                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="headingtwo">
                                            <h4 class="panel-title" style="margin-bottom:0px;padding:0px;">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion"
                                                   class="collapsed"
                                                   href="#collapser_<?php echo e($taxx->id); ?>" aria-expanded="false"
                                                   style="display:inline-flex;width:100%">
                            <span class="count_acc"
                                  style="background: #005ed2;color: #ffffff;border: 1px solid #ffffff;"><?php //echo $cntss;?>
                            </span><span class="ac_name_first"><?php echo $taxx->title;?> </span>
                                                    <i class="more-less glyphicon glyphicon-plus"></i>
                                                    <i class="more-less glyphicon glyphicon-minus"></i>
                                                </a>
                                            </h4>
                                        </div>

                                        <div style="clear:both;"></div>
                                        <div id="collapser_<?php echo e($taxx->id); ?>" class="panel-collapse collapse"
                                             role="tabpanel" aria-labelledby="headingtwo" aria-expanded="false"
                                             style="height: 0px;">
                                            <div class="panel-body accordion-body table-responsive"
                                                 style="padding-top:15px;">
                                                <table class="table table-striped table-bordered">
                                                    <thead>
                                                    <tr style="text-align:center">
                                                        <th width="3%">No</th>
                                                        <th width="13%">Client ID</th>
                                                        <th width="14%">Client Name</th>
                                                        <th width="20%">Regarding</th>
                                                        <th width="18%">EE Responsible Person</th>
                                                        <th width="10%">Filling Status</th>
                                                        <th>Due Date</th>
                                                        <th>Status</th>
                                                        <th>Action</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                    //  echo "<pre>";print_r($arrc);
                                                    $cnts = 0;
                                                    $cnts11 = 0;
                                                    //echo "<pre>";print_r($commonone1);
                                                    // echo count($commonone1);

                                                    foreach($commonone1 as $taxfederal1)
                                                    {
                                                    //   echo "<pre>";print_r($taxfederal1);
                                                    //echo "<pre>";print_r($taxfederal1);
                                                    //echo $taxx->id;
                                                    // echo $taxfederal1->ids;
                                                    //    echo "<br>";
                                                    // print_r($arrc);
                                                    if($taxfederal1->status == $taxx->id)
                                                    {

                                                    //echo "<pre>";print_r($taxfederal1);

                                                    $cnts++;
                                                    // echo $cnts;     
                                                    //       if(!in_array($taxfederal1->ids,$arrc))
                                                    //    {

                                                    // echo $cnts;
                                                    //    if($taxfederal1->flags != '1')
                                                    //    {


                                                    // echo $cnts;

                                                    if(isset($taxfederal1->expiredate) != '' && isset($taxfederal1->expiredate) != null)
                                                    {
                                                    // echo $cnts;
                                                    $exp_date = date('M-d-Y', strtotime("-30 days", strtotime($taxfederal1->expiredate)));
                                                    $notifyexp_date = date('M-d-Y', strtotime("-10 days", strtotime($taxfederal1->expiredate)));

                                                    $exp_dates = strtotime($exp_date);
                                                    $notifyexp_dates = strtotime($notifyexp_date);


                                                    $today = date('M-d-Y');
                                                    $match_today = strtotime($today);

                                                    if($match_today >= $exp_dates && $match_today <= $notifyexp_dates)
                                                    {
                                                    $cnts11 = $cnts11 + 1;
                                                    //echo "<pre>"; print_r($taxfederal1);
                                                    ?>
                                                    <tr>

                                                        <td class="text-center"><?php echo e($cnts); ?>  </td>
                                                        <td><span class="alphabet">C</span>-<?php echo e($taxfederal1->filename); ?>

                                                        </td>
                                                        <td><?php if($taxfederal1->business_id =='6'): ?><?php echo e($taxfederal1->first_name); ?> <?php echo e($taxfederal1->middle_name); ?> <?php echo e($taxfederal1->last_name); ?> <?php else: ?> <?php echo e($taxfederal1->company_name); ?> <?php endif; ?></td>

                                                        <td><?php echo e($taxx->shortcode); ?></td>
                                                        <td><?php echo e($taxfederal1->firstName); ?> <?php echo e($taxfederal1->middleName); ?> <?php echo e($taxfederal1->lastName); ?></td>
                                                        <td>
                                                            <p style="background: aliceblue;border-radius: 50px;color: #006bf5;margin: 0px 2px 0px 2px;border:1px solid #000000;font-weight: bold;text-align: center;line-height: 1.6;vertical-align: top;width: 25px !important;height: 25px;float: left;">
                                                                A </p> - <?php echo e($taxfederal1->taxyears); ?></td>

                                                        <td><?php if (isset($taxfederal1->federalsduedate)) {
                                                                echo date('m/d/Y', strtotime($taxfederal1->federalsduedate));
                                                            } else if ($taxfederal1->expiredate != '0000-00-00') {
                                                                echo date('m/d/Y', strtotime($taxfederal1->expiredate));
                                                            }?></td>
                                                        <td><?php if ($taxfederal1->personalstatus == '0') {
                                                                echo 'Not Done';
                                                            } else {
                                                                echo 'Done';
                                                            }?></td>
                                                        <td class="text-center">

                                                            <a class="btn-action btn-view-edit"
                                                               href="fac-Bhavesh-0554/workrecord?id=<?php echo $taxfederal1->ids;?>&&action=tax"><i
                                                                    class="fa fa-edit"></i></a>

                                                        </td>

                                                    </tr>
                                                    <?php

                                                    }
                                                    }

                                                    }
                                                    //  }

                                                    }

                                                    ?>


                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    <!-- <table class="table table-striped table-bordered">
                            <thead>
                              <tr style="text-align:center">
                                <th>No</th>  
                                <th>ID</th>
                                <th>Regarding</th>
                                <th>Expire Date</th>
                                <th>Action</th>
                              </tr>
                            </thead>
                            <tbody>
                               
                            <?php $__currentLoopData = $adminupload; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $taxfederal): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php
                        if(isset($taxfederal->expiredate) != '' && isset($taxfederal->expiredate) != null)
                        {

                        $exp_date = date('M-d-Y', strtotime("-30 days", strtotime($taxfederal->expiredate)));
                        $exp_dates = strtotime($exp_date);

                        $today = date('M-d-Y');
                        $match_today = strtotime($today);

                        $exp_date2 = date('M-d-Y', strtotime("-11 days", strtotime($taxfederal->expiredate)));
                        $exp_dates2 = strtotime($exp_date2);


                        //$datetime1 = date_create($today);
                        //$datetime2 = date_create($adminlic->expiredate);
                        //$interval = date_diff($datetime1, $datetime2); 
                        //echo $dd= $interval->format('%R%a');
                        // if($dd=='+30' || $dd<='+20')
                        //if($dd>='+30' && $dd<='+20')
                        if($match_today >= $exp_dates && $match_today <= $exp_dates2)
                        {

                        ?>

                            <tr>
                            <td class="text-center"><?php echo e($loop->index+1); ?></td>
                            <td class="text-center"><span class="alphabet">A</span>-<?php echo e($taxfederal->id); ?></td>
                            <td class="text-center"><?php if($taxfederal->status==1): ?> Formation <?php elseif($taxfederal->status==3): ?> Professional  <?php else: ?>  Business  <?php endif; ?> - <a href="<?php echo e(url('public/adminupload')); ?>/<?php echo e($taxfederal->documentcopy); ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></td>
                            <td class="text-center"><?php echo e($taxfederal->expiredate); ?></td>
                            <td class="text-center">
                                <?php if($taxfederal->status==1): ?> <a href="https://financialservicecenter.net/fac-Bhavesh-0554/adminprofile?id=tab3primary"  target="_blank">Go</a> <?php elseif($taxfederal->status==3): ?> <a href="https://financialservicecenter.net/fac-Bhavesh-0554/adminprofile?id=tab5primary"  target="_blank">Go</a>  <?php else: ?>  <a href="https://financialservicecenter.net/fac-Bhavesh-0554/adminprofile?id=tab5primary"  target="_blank">Go</a>  <?php endif; ?>
                            </td>
                            <?php
                        }
                        }
                        ?>
                            </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                            
                            

     
    </tbody>
                        </table>-->
                    </div>
                    <div class="modal-footer">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    $arrc = array();
    foreach ($clientoriginal as $corg) {
        $arrc[] = $corg->client_id;
    }
    $cntssn = 0;
    foreach ($commonone1 as $taxfederal2) {
        // if(!in_array($taxfederal2->ids,$arrc))
        //{


        $cntssn++;

        //}
    }

    ?>
    <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document" style="width:70%;">
            <div class="modal-content">
                <div class="modal-header" style="background:#f39c12 !important;color:white;text-align:center;">
                    <h3 class="modal-title" id="exampleModalLabel" style="color:#ffffff!important;"><span
                            class="btn btn-primary countbox"
                            style="border:none !important;padding-right: 6px;">Count :</span> <span
                            class="btn btn-primary countbox"><?php echo $cntssn;?></span> Notification <small>( 10 Days
                            )</small>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </h3>
                </div>
                <div class="modal-body">

                    <div class="panel-group" id="accordion" role="tablist" style="margin-top:15px;">
                        <?php $__currentLoopData = $taxtitles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $taxx): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if(in_array($taxx->id,$ctax1)): ?>

                                <?php
                                $cntss1 = 0;
                                $cntss = array();
                                foreach ($commonone1 as $taxfederal1) {

                                    if ($taxfederal1->status == $taxx->id) {

                                        // if(!in_array($taxfederal1->ids,$arrc))
                                        //{
                                        // echo $taxx->id;
                                        // echo $taxfederal1->tids;
                                        $cntss1++;
                                        // echo $cnts;
                                        if (isset($taxfederal1->expiredate) != '' && isset($taxfederal1->expiredate) != null) {
                                            $exp_date = date('M-d-Y', strtotime("-2 days", strtotime($taxfederal1->expiredate)));
                                            $exp_dates = strtotime($exp_date);

                                            $today = date('M-d-Y');
                                            $match_today = strtotime($today);

                                            if ($match_today >= $exp_dates) {
                                                $cntss[] = $cntss1++;

                                            }
                                        }
                                        //}
                                    }
                                }
                                //echo count($cntss);
                                ?>


                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingtwo">
                                        <h4 class="panel-title" style="margin-bottom:0px;padding:0px;">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion"
                                               class="collapsed"
                                               href="#collapsen_<?php echo e($taxx->id); ?>" aria-expanded="false"
                                               style="display:inline-flex;width:100%">
                                                <span class="count_acc"
                                                      style="background: #d07f00;color: #ffffff;border: 1px solid #ffffff;">30</span><span
                                                    class="ac_name_first"><?php echo e($taxx->title); ?> </span>
                                                <i class="more-less glyphicon glyphicon-plus"></i>
                                                <i class="more-less glyphicon glyphicon-minus"></i>
                                            </a>
                                        </h4>
                                    </div>
                                    <div style="clear:both;"></div>
                                    <div id="collapsen_<?php echo e($taxx->id); ?>" class="panel-collapse collapse" role="tabpanel"
                                         aria-labelledby="headingtwo" aria-expanded="false" style="height: 0px;">
                                        <div class="panel-body accordion-body table-responsive"
                                             style="padding-top:15px;">
                                            <table class="table table-striped table-bordered">
                                                <thead>
                                                <tr style="text-align:center">
                                                    <th width="3%">No</th>
                                                    <th width="13%">Client ID</th>
                                                    <th width="14%">Client Name</th>
                                                    <th width="20%">Regarding</th>
                                                    <th width="18%">EE Responsible Person</th>
                                                    <th width="10%">Filling Status</th>
                                                    <th>Due Date</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                //  echo "<pre>";print_r($arrc);
                                                $cnts = 0;
                                                foreach($commonone1 as $taxfederal1)
                                                {
                                                //echo $taxx->id;
                                                //  echo $taxfederal1->ids;
                                                //    echo "<br>";
                                                // print_r($arrc);
                                                if($taxfederal1->status == $taxx->id)
                                                {

                                                if(!in_array($taxfederal1->ids, $arrc))
                                                {
                                                // echo $taxx->id;
                                                // echo $taxfederal1->tids;
                                                $cnts++;
                                                // echo $cnts;
                                                if(isset($taxfederal1->expiredate) != '' && isset($taxfederal1->expiredate) != null)
                                                {
                                                $exp_date = date('M-d-Y', strtotime("-10 days", strtotime($taxfederal1->expiredate)));
                                                $exp_dates = strtotime($exp_date);

                                                $today = date('M-d-Y');
                                                $match_today = strtotime($today);

                                                if($match_today >= $exp_dates)
                                                {
                                                //echo "<pre>"; print_r($taxfederal1);
                                                ?>
                                                <tr>

                                                    <td class="text-center"><?php echo e($cnts); ?></td>
                                                    <td><span class="alphabet">C</span>-<?php echo e($taxfederal1->filename); ?></td>
                                                    <td><?php if($taxfederal1->business_id =='6'): ?><?php echo e($taxfederal1->first_name); ?> <?php echo e($taxfederal1->middle_name); ?> <?php echo e($taxfederal1->last_name); ?> <?php else: ?> <?php echo e($taxfederal1->company_name); ?> <?php endif; ?></td>

                                                    <td><?php echo e($taxx->shortcode); ?></td>
                                                    <td><?php echo e($taxfederal1->firstName); ?> <?php echo e($taxfederal1->middleName); ?> <?php echo e($taxfederal1->lastName); ?></td>
                                                    <td>
                                                        <p style="background: aliceblue;border-radius: 50px;color: #006bf5;margin: 0px 2px 0px 2px;border:1px solid #000000;font-weight: bold;text-align: center;line-height: 1.6;vertical-align: top;width: 25px !important;height: 25px;float: left;">
                                                            A </p> - <?php echo e($taxfederal1->taxyears); ?></td>

                                                    <td><?php if ($taxfederal1->expiredate != '0000-00-00') {
                                                            echo date('m/d/Y', strtotime($taxfederal1->expiredate));
                                                        }?></td>
                                                    <td><?php if ($taxfederal1->personalstatus == '0') {
                                                            echo 'Not Done';
                                                        } else {
                                                            echo 'Done';
                                                        }?></td>
                                                    <td class="text-center">

                                                        <a class="btn-action btn-view-edit"
                                                           href="fac-Bhavesh-0554/workrecord?id=<?php echo $taxfederal1->ids;?>&&action=tax"><i
                                                                class="fa fa-edit"></i></a>

                                                    </td>

                                                </tr>
                                                <?php

                                                }
                                                }
                                                }
                                                }
                                                }
                                                ?>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    </div>
                <!--<table class="table table-striped table-bordered">
    <thead>
      <tr style="text-align:center">
                <th>No</th>  
                <th>ID</th>
                <th>Regarding</th>
                <th>Expire Date</th>
                <th>Action</th>

      </tr>
    </thead>
    <tbody>
        <?php $__currentLoopData = $adminupload1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $taxfederal1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                    <?php
                    if(isset($taxfederal1->expiredate) != '' && isset($taxfederal1->expiredate) != null)
                    {
                    $exp_date = date('M-d-Y', strtotime("-10 days", strtotime($taxfederal1->expiredate)));
                    $exp_dates = strtotime($exp_date);

                    $today = date('M-d-Y');
                    $match_today = strtotime($today);

                    $exp_date2 = date('M-d-Y', strtotime("-3 days", strtotime($taxfederal1->expiredate)));
                    $exp_dates2 = strtotime($exp_date2);


                    //             $today = date('M-d-Y');
                    // $datetime1 = date_create($today);
                    // $datetime2 = date_create($adminlic1->expiredate);
                    // $interval = date_diff($datetime1, $datetime2); 
                    // $dd1= $interval->format('%R%a');

                    // // if($dd1=='+10' || $dd1<='+3')
                    // if($dd1>='+10' && $dd1<='+3')
                    if($match_today >= $exp_dates && $match_today <= $exp_dates2)

                    {
                    ?>
                        <tr>
         <td class="text-center"><?php echo e($loop->index+1); ?></td>
                <td class="text-center"><span class="alphabet">A</span>-<?php echo e($taxfederal1->id); ?></td>
                <td class="text-center"><?php if($taxfederal1->status==1): ?> Formation - <a href="<?php echo e(url('public/adminupload')); ?>/<?php echo e($taxfederal1->documentcopy); ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a> <?php elseif($taxfederal1->status==3): ?> Professional -  <a href="https://financialservicecenter.net/public/adminupload/<?php echo e($taxfederal1->documentcopy); ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>  <?php else: ?>  Business - <a href="<?php echo e(url('public/adminupload')); ?>/<?php echo e($taxfederal1->documentcopy); ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a> <?php endif; ?>  </td>
                <td class="text-center"><?php echo e($taxfederal1->expiredate); ?></td>
                <td class="text-center">
                    <?php if($taxfederal1->status==1): ?> <a href="https://financialservicecenter.net/fac-Bhavesh-0554/adminprofile?id=tab3primary"  target="_blank">Go</a>  <?php elseif($taxfederal1->status==3): ?> <a href="https://financialservicecenter.net/fac-Bhavesh-0554/adminprofile?id=tab5primary"  target="_blank">Go</a>  <?php else: ?>  <a href="https://financialservicecenter.net/fac-Bhavesh-0554/adminprofile?id=tab5primary"  target="_blank">Go</a>  <?php endif; ?>
                        </td>
            <?php
                    }
                    }
                    ?>
                        </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
  </table>-->
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document" style="width:70%;">
            <div class="modal-content">
                <div class="modal-header" style="background:#dd4b39  !important;color:white;text-align:center;">
                    <?php
                    $arrc = array();
                    foreach ($clientoriginal as $corg) {
                        $arrc[] = $corg->client_id;
                    }
                    // $cntss=0;

                    foreach ($commonone1 as $taxfederal2) {
                        if (!in_array($taxfederal2->ids, $arrc)) {


                            // $cntss++;

                        }
                    }

                    ?>
                    <?php //echo count($c2);?>
                    <h3 class="modal-title " id="exampleModalLabel" style="color:#fff !important;"><span
                            class="btn btn-primary countbox"
                            style="border:none !important;padding-right: 6px;">Count :</span> <span
                            class="btn btn-primary notify_count countbox"><?php //echo count($commonone1);?></span>Warning
                        <small>( 2 Days )</small>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </h3>
                </div>
                <div class="modal-body">
                    <div class="panel-group" id="accordion" role="tablist" style="margin-top:15px;">
                        <?php $i = 0;?>
                        <?php $__currentLoopData = $taxtitles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $taxx): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if(in_array($taxx->id,$ctax1)): ?>

                                <?php
                                $cntss1 = 0;
                                $cntss = array();
                                foreach ($commonone1 as $taxfederal1) {

                                    if ($taxfederal1->status == $taxx->id) {

                                        // echo $taxx->id;
                                        // echo $taxfederal1->tids;
                                        $cntss1++;
                                        // echo $cnts;
                                        // if(!in_array($taxfederal1->ids,$arrc))
                                        //{

                                        if (isset($taxfederal1->expiredate) != '' && isset($taxfederal1->expiredate) != null) {
                                            $exp_date = date('M-d-Y', strtotime("-2 days", strtotime($taxfederal1->expiredate)));
                                            $exp_dates = strtotime($exp_date);

                                            $today = date('M-d-Y');
                                            $match_today = strtotime($today);

                                            if ($match_today >= $exp_dates) {
                                                $cntss[] = $cntss1++;

                                            }

                                            // }
                                        }
                                    }
                                }
                                //echo count($cntss);
                                ?>

                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingtwo">
                                        <h4 class="panel-title" style="margin-bottom:0px;padding:0px;">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion"
                                               class="collapsed"
                                               href="#collapsew_<?php echo e($taxx->id); ?>" aria-expanded="false"
                                               style="display:inline-flex;width:100%">
                                                <span
                                                    style="background: #dd4b39;color: #ffffff;border: 1px solid #ffffff;"
                                                    class="count_acc"><?php echo count($cntss);?></span><span
                                                    class="ac_name_first"><?php echo e($taxx->title); ?> </span>
                                                <i class="more-less glyphicon glyphicon-plus"></i>
                                                <i class="more-less glyphicon glyphicon-minus"></i>
                                            </a>
                                        </h4>
                                    </div>
                                    <div style="clear:both;"></div>
                                    <div id="collapsew_<?php echo e($taxx->id); ?>" class="panel-collapse collapse" role="tabpanel"
                                         aria-labelledby="headingtwo" aria-expanded="false" style="height: 0px;">
                                        <div class="panel-body accordion-body table-responsive"
                                             style="padding-top:15px;">
                                            <table class="table table-striped table-bordered" id="example">
                                                <thead>
                                                <tr style="text-align:center">
                                                    <th width="5%">No</th>
                                                    <th width="140px">Client ID</th>
                                                    <th width="185px">Client Name</th>

                                                    <th>Regarding</th>
                                                    <th width="160">EE Responsible Person</th>
                                                    <th width="100px">Period</th>


                                                    <th width="80px">Due Date</th>
                                                    <th width="67px">Status</th>

                                                    <th width="9%">Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                <?php
                                                //  echo "<pre>";print_r($arrc);
                                                $cnts = 0;
                                                $cnts11 = 0;
                                                //echo "<pre>";print_r($commonone1);
                                                // echo count($commonone1);

                                                foreach($commonone1 as $taxfederal1)
                                                {
                                                //   echo "<pre>";print_r($taxfederal1);
                                                //echo "<pre>";print_r($taxfederal1);
                                                //echo $taxx->id;
                                                // echo $taxfederal1->ids;
                                                //    echo "<br>";
                                                // print_r($arrc);
                                                //print_r($taxfederal1)
                                                if($taxfederal1->status == $taxx->id)
                                                {

                                                //echo "<pre>";print_r($taxfederal1);

                                                $cnts++;
                                                // echo $cnts;     
                                                //       if(!in_array($taxfederal1->ids,$arrc))
                                                //    {

                                                // echo $cnts;
                                                //    if($taxfederal1->flags != '1')
                                                //    {


                                                // echo $cnts;

                                                if(isset($taxfederal1->expiredate) != '' && isset($taxfederal1->expiredate) != null)
                                                {
                                                // echo $cnts;
                                                $exp_date = date('M-d-Y', strtotime("-2 days", strtotime($taxfederal1->expiredate)));
                                                $exp_dates = strtotime($exp_date);

                                                $today = date('M-d-Y');
                                                $match_today = strtotime($today);

                                                if($match_today >= $exp_dates)
                                                {
                                                $cnts11 = $cnts11 + 1;
                                                //    echo $taxfederal1->flag;
                                                // echo "<pre>"; print_r($taxfederal1);
                                                ?>
                                                <tr>

                                                    <td class="text-center"><?php echo e($cnts); ?>  </td>
                                                    <td><span class="alphabet">C</span>-<?php echo e($taxfederal1->filename); ?></td>
                                                    <td><?php if($taxfederal1->business_id =='6'): ?><?php echo e($taxfederal1->first_name); ?> <?php echo e($taxfederal1->middle_name); ?> <?php echo e($taxfederal1->last_name); ?> <?php else: ?> <?php echo e($taxfederal1->company_name); ?> <?php endif; ?></td>

                                                    <td><?php echo e($taxx->shortcode); ?></td>
                                                    <td><?php echo e($taxfederal1->firstName); ?> <?php echo e($taxfederal1->middleName); ?> <?php echo e($taxfederal1->lastName); ?></td>
                                                    <td>
                                                        <p style="background: aliceblue;border-radius: 50px;color: #006bf5;margin: 0px 2px 0px 2px;border:1px solid #000000;font-weight: bold;text-align: center;line-height: 1.6;vertical-align: top;width: 25px !important;height: 25px;float: left;">
                                                            A </p> - <?php echo e($taxfederal1->taxyears); ?></td>

                                                    <td><?php
                                                        if (isset($taxfederal1->federalsatax) && $taxfederal1->federalsatax != '') {
                                                            echo date('m/d/Y', strtotime($taxfederal1->federalsduedate));
                                                        } else if ($taxfederal1->expiredate != '0000-00-00') {
                                                            echo date('m/d/Y', strtotime($taxfederal1->expiredate));
                                                        }?></td>
                                                    <td><?php if ($taxfederal1->personalstatus == '0') {
                                                            echo 'Not Done';
                                                        } else {
                                                            echo 'Done';
                                                        }?></td>
                                                    <td class="text-center">

                                                        <a class="btn-action btn-view-edit"
                                                           href="fac-Bhavesh-0554/workrecord?id=<?php echo $taxfederal1->ids;?>&&action=tax&&yrs=<?php echo $taxfederal1->taxyears;?>"><i
                                                                class="fa fa-edit"></i></a>

                                                    </td>

                                                </tr>
                                                <?php

                                                }
                                                }

                                                }
                                                //  }

                                                }

                                                ?>


                                                </tbody>

                                            </table>
                                            <input type="hidden" class="notification_i"
                                                   id="notification_i<?php echo $i++;?>" value="<?php echo $cnts11;?>">


                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingtwo">
                                <h4 class="panel-title" style="margin-bottom:0px;padding:0px;">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" class="collapsed"
                                       href="#collapsewcorp_4" aria-expanded="false"
                                       style="display:inline-flex;width:100%">
                                        <span class="count_acc"
                                              style="background: #dd4b39;color: #ffffff;border: 1px solid #ffffff;">0<?php //echo count($cntss);?></span><span
                                            class="ac_name_first">Corporation </span>
                                        <i class="more-less glyphicon glyphicon-plus"></i>
                                        <i class="more-less glyphicon glyphicon-minus"></i>
                                    </a>
                                </h4>
                            </div>
                            <div style="clear:both;"></div>
                            <div id="collapsewcorp_4" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="headingtwo" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body accordion-body" style="padding-top:15px;">
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                        <tr style="text-align:center">
                                            <th width="3%">No</th>
                                            <th width="13%">Client ID</th>
                                            <th width="14%">Client Name</th>
                                            <th width="20%">Regarding</th>
                                            <th width="18%">EE Responsible Person</th>
                                            <th width="10%">Filling Status</th>
                                            <th>Due Date</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="text-center">&nbsp;</td>
                                            <td><span class="alphabet">C</span>-&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>


                            </div>
                            <div class="modal-footer">

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <!-- Modal -->
    <div id="newmyModal" class="modal fade " role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="background:#ffff99;border-bottom:5px solid green !important;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="headings"></h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="row form-group hideclients">
                            <label class="control-label col-md-3 text-right">Client ID:</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" id="entityids" readonly style="width:150px;"/>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="control-label col-md-3 text-right">Name</label>
                            <div class="col-md-7">
                                <input class="form-control" type="text" id="names" readonly/>
                                <input class="form-control names1" type="text" readonly/>

                            </div>
                        </div>
                        <div class="companyphone" style="display:none;">
                            <div class="row form-group">
                                <label class="control-label col-md-3 text-right">Telephone 1</label>
                                <div class="col-md-3">
                                    <input class="form-control telephones" type="text" id="" readonly/>
                                </div>
                                <div class="col-md-2">
                                    <input class="form-control teltype" type="text" id="" readonly/>
                                </div>
                                <div class="col-md-2">
                                    <input class="form-control exts" type="text" placeholder="Ext" readonly/>
                                </div>
                            </div>

                            <div class="row form-group">
                                <label class="control-label col-md-3 text-right">Telephone 2</label>
                                <div class="col-md-3">
                                    <input class="form-control telephonesNo2" type="text" id="" readonly/>
                                </div>
                                <div class="col-md-2">
                                    <input class="form-control" type="text" id="teltype2" readonly/>
                                </div>
                                <div class="col-md-2">
                                    <input class="form-control extss" type="text" placeholder="Ext" readonly/>
                                </div>
                            </div>
                        </div>
                        <div class="personalphone" style="display:none;">
                            <div class="row form-group">
                                <label class="control-label col-md-3 text-right"> Personal Telephone</label>
                                <div class="col-md-3">
                                    <input class="form-control telephones" type="text" id="" readonly/>
                                </div>
                                <div class="col-md-3">
                                    <input class="form-control teltype" type="text" id="" readonly/>
                                </div>
                                <div class="col-md-2">
                                    <input class="form-control exts" type="text" placeholder="ext" readonly/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="control-label col-md-3 text-right">FSC Telephone</label>
                                <div class="col-md-3">
                                    <input class="form-control telephonesNo2" type="text" id="" readonly/>
                                </div>
                                <div class="col-md-3">
                                    <input class="form-control telephonesNo2Type" type="text" id="" readonly/>
                                </div>
                                <div class="col-md-2">
                                    <input class="form-control extss" type="text" placeholder="ext" readonly/>
                                </div>
                            </div>
                        </div>
                        <div class="row form-group companyemail" style="display:none;">
                            <label class="control-label col-md-3 text-right">Email</label>
                            <div class="col-md-7">
                                <input class="form-control emails" type="text" id="" readonly/>
                            </div>
                        </div>
                        <div class="personalemail" style="display:none;">
                            <div class="row form-group">
                                <label class="control-label col-md-3 text-right">Personal Email</label>
                                <div class="col-md-7">
                                    <input class="form-control emails" type="text" id="" readonly/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="control-label col-md-3 text-right">FSC Email</label>
                                <div class="col-md-7">
                                    <input class="form-control fscemail" type="text" id="" readonly/>
                                </div>
                            </div>
                        </div>

                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
                <div class="modal-footer">
                    <span class="ahref"></span>&nbsp;&nbsp;<span class="ahref1"></span>&nbsp;&nbsp;
                    <button type="button" class="btn btn-default" data-dismiss="modal"
                            style="background-color: #e9e8e8;border-color: #b9b9b9;">Close
                    </button>
                </div>
            </div>

        </div>
    </div>


    <!---------------------->

    <div id="MyModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
         aria-hidden="true">

        <div class="modal-dialog modal-lg">

            <!-- Modal Content: begins -->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" style="position: relative; width: 98%; float: left;">
                        <img src="https://financialservicecenter.net/public/dashboard/images/fsc_logo.png" alt=""
                             class="img-responsive" style="width:70px; margin:0px auto;">
                        <form>
                            <input type="button" onclick="printDiv('myModal_FSC-120')"
                                   class="btn btn-primary btn-sm pull-right"
                                   style="margin-right: 15px; position: ABSOLUTE; right:0px;top: 0px;" value="Print">
                        </form>
                    </h4>
                </div>

                <!-- Modal Body -->
                <div class="modal-body" style="background:#ccc;">
                    <!--<div class="row">
              <div class="col-md-5 text-left">
                 <strong>EE ID : </strong> FSC-120                                  </div>
              <div class="col-md-6 text-left">
                  <strong>Name:</strong>Antionette C Coates<br/>
                  <strong>Schedule  Status: </strong> <br/>
                  <strong>Period : </strong>
              </div>
          </div>!-->
                    <table class="table myclass" border="0">
                        <thead>
                        <tr>
                            <th colspan="4" class="border-none"
                                style="border: 0 !important; background:none!important;">
                                <h3 style="margin-top:0px !important;"><b>
                                        Schedule</b></h3>
                            </th>
                        </tr>
                        <tr>
                        </tr>

                        <tr>
                            <th class="border-none" style="border: 0 !important;background:none!important;float:left;">
                                <b>Employee ID:</b>
                                <span class="btn btn-primary" style="background:none !important;color:#000 !important;">FSC-120</span>
                            </th>
                            <th colspan="2" class="border-none"
                                style="border: 0 !important; background:none!important;text-align:right !important;vertical-align: middle;">
                                Period:
                                Jun-01 2020
                                To
                                Jun-07 2020
                            </th>
                            <th class="border-none" style="border: 0 !important;background:none!important;float:right;">
                                <b>Employee Name :</b>
                                <span class="btn btn-primary" style="background:none !important;color:#000 !important;">Antionette C Coates</span>
                            </th>
                        </tr>


                        </thead>
                    </table>


                    <table class="table table-bordered table-striped">
                        <thead>

                        <tr>
                            <th>Jun-01-2020 <br> Monday</th>
                            <th>Jun-02-2020 <br> Tuesday</th>
                            <th>Jun-03-2020 <br> Wednesday</th>
                            <th>Jun-04-2020 <br> Thursday</th>
                            <th>Jun-05-2020 <br> Friday</th>
                            <th>Jun-06-2020 <br> Saturday</th>
                            <th>Jun-07-2020 <br> Sunday</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td style="text-align:center !important;">09:00 AM - <br><span
                                    style="margin-left:-6% !important;">05:30 PM</span></td>
                            <td style="text-align:center !important;">09:00 AM - <br><span
                                    style="margin-left:-6% !important;">05:30 PM</span></td>
                            <td style="text-align:center !important;">09:00 AM - <br><span
                                    style="margin-left:-6% !important;">05:30 PM</span></td>
                            <td style="text-align:center !important;">09:00 AM - <br><span
                                    style="margin-left:-6% !important;">05:30 PM</span></td>
                            <td style="text-align:center !important;">09:00 AM - <br><span
                                    style="margin-left:-6% !important;">05:30 PM</span></td>
                            <td style="width:0px !important;">&nbsp;</td>
                            <td style="width:0px !important;">&nbsp;</td>

                        </tr>
                        </tbody>
                    </table>
                </div>

                <!-- Modal Footer -->
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                    <!--<button id="btnPrint" type="button" class="btn btn-default">Print</button>
      !--></div>

            </div>
            <!-- Modal Content: ends -->

        </div>
    </div>

    <!-- Trigger the modal with a button -->
    <!--<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModaltest">Open Modal</button>
!-->
    <!-- Modal -->
    <div class="modal fade" id="myModaltest" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Modal Header</h4>
                </div>
                <div class="modal-body" style="height:350px; overflow:auto">
                    <div id="printMe">
                        <style>
                            @media  print {
                                table {
                                    font-family: Verdana, Geneva, Tahoma, sans-serif;
                                }

                                table tr td {
                                    padding: 4px;
                                    border-bottom: 1px solid #ccc;
                                }

                                table tr th {
                                    padding: 4px;
                                    border-bottom: 1px solid #ccc;
                                }
                            }
                        </style>
                        <table class="table table-bordered table-striped">
                            <thead>

                            <tr>
                                <th>Jun-08-2020 <br> Monday</th>
                                <th>Jun-09-2020 <br> Tuesday</th>
                                <th>Jun-10-2020 <br> Wednesday</th>
                                <th>Jun-11-2020 <br> Thursday</th>
                                <th>Jun-12-2020 <br> Friday</th>
                                <th>Jun-13-2020 <br> Saturday</th>
                                <th>Jun-14-2020 <br> Sunday</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td style="text-align:center !important;">10:00 AM - <br><span
                                        style="margin-left:-6% !important;">05:30 PM</span></td>
                                <td style="text-align:center !important;"> - <br><span
                                        style="margin-left:-6% !important;"></span></td>
                                <td style="text-align:center !important;">10:00 AM - <br><span
                                        style="margin-left:-6% !important;">05:30 PM</span></td>
                                <td style="text-align:center !important;"> - <br><span
                                        style="margin-left:-6% !important;"></span></td>
                                <td style="text-align:center !important;">10:00 AM - <br><span
                                        style="margin-left:-6% !important;">05:30 PM</span></td>
                                <td style="width:0px !important;">&nbsp;</td>
                                <td style="width:0px !important;">&nbsp;</td>

                            </tr>
                            <tr>
                                <td style="text-align:center !important;">10:00 AM - <br><span
                                        style="margin-left:-6% !important;">05:30 PM</span></td>
                                <td style="text-align:center !important;"> - <br><span
                                        style="margin-left:-6% !important;"></span></td>
                                <td style="text-align:center !important;">10:00 AM - <br><span
                                        style="margin-left:-6% !important;">05:30 PM</span></td>
                                <td style="text-align:center !important;"> - <br><span
                                        style="margin-left:-6% !important;"></span></td>
                                <td style="text-align:center !important;">10:00 AM - <br><span
                                        style="margin-left:-6% !important;">05:30 PM</span></td>
                                <td style="width:0px !important;">&nbsp;</td>
                                <td style="width:0px !important;">&nbsp;</td>

                            </tr>
                            <tr>
                                <td style="text-align:center !important;">10:00 AM - <br><span
                                        style="margin-left:-6% !important;">05:30 PM</span></td>
                                <td style="text-align:center !important;"> - <br><span
                                        style="margin-left:-6% !important;"></span></td>
                                <td style="text-align:center !important;">10:00 AM - <br><span
                                        style="margin-left:-6% !important;">05:30 PM</span></td>
                                <td style="text-align:center !important;"> - <br><span
                                        style="margin-left:-6% !important;"></span></td>
                                <td style="text-align:center !important;">10:00 AM - <br><span
                                        style="margin-left:-6% !important;">05:30 PM</span></td>
                                <td style="width:0px !important;">&nbsp;</td>
                                <td style="width:0px !important;">&nbsp;</td>

                            </tr>
                            <tr>
                                <td style="text-align:center !important;">10:00 AM - <br><span
                                        style="margin-left:-6% !important;">05:30 PM</span></td>
                                <td style="text-align:center !important;"> - <br><span
                                        style="margin-left:-6% !important;"></span></td>
                                <td style="text-align:center !important;">10:00 AM - <br><span
                                        style="margin-left:-6% !important;">05:30 PM</span></td>
                                <td style="text-align:center !important;"> - <br><span
                                        style="margin-left:-6% !important;"></span></td>
                                <td style="text-align:center !important;">10:00 AM - <br><span
                                        style="margin-left:-6% !important;">05:30 PM</span></td>
                                <td style="width:0px !important;">&nbsp;</td>
                                <td style="width:0px !important;">&nbsp;</td>

                            </tr>
                            <tr>
                                <td style="text-align:center !important;">10:00 AM - <br><span
                                        style="margin-left:-6% !important;">05:30 PM</span></td>
                                <td style="text-align:center !important;"> - <br><span
                                        style="margin-left:-6% !important;"></span></td>
                                <td style="text-align:center !important;">10:00 AM - <br><span
                                        style="margin-left:-6% !important;">05:30 PM</span></td>
                                <td style="text-align:center !important;"> - <br><span
                                        style="margin-left:-6% !important;"></span></td>
                                <td style="text-align:center !important;">10:00 AM - <br><span
                                        style="margin-left:-6% !important;">05:30 PM</span></td>
                                <td style="width:0px !important;">&nbsp;</td>
                                <td style="width:0px !important;">&nbsp;</td>

                            </tr>
                            <tr>
                                <td style="text-align:center !important;">10:00 AM - <br><span
                                        style="margin-left:-6% !important;">05:30 PM</span></td>
                                <td style="text-align:center !important;"> - <br><span
                                        style="margin-left:-6% !important;"></span></td>
                                <td style="text-align:center !important;">10:00 AM - <br><span
                                        style="margin-left:-6% !important;">05:30 PM</span></td>
                                <td style="text-align:center !important;"> - <br><span
                                        style="margin-left:-6% !important;"></span></td>
                                <td style="text-align:center !important;">10:00 AM - <br><span
                                        style="margin-left:-6% !important;">05:30 PM</span></td>
                                <td style="width:0px !important;">&nbsp;</td>
                                <td style="width:0px !important;">&nbsp;</td>

                            </tr>
                            <tr>
                                <td style="text-align:center !important;">10:00 AM - <br><span
                                        style="margin-left:-6% !important;">05:30 PM</span></td>
                                <td style="text-align:center !important;"> - <br><span
                                        style="margin-left:-6% !important;"></span></td>
                                <td style="text-align:center !important;">10:00 AM - <br><span
                                        style="margin-left:-6% !important;">05:30 PM</span></td>
                                <td style="text-align:center !important;"> - <br><span
                                        style="margin-left:-6% !important;"></span></td>
                                <td style="text-align:center !important;">10:00 AM - <br><span
                                        style="margin-left:-6% !important;">05:30 PM</span></td>
                                <td style="width:0px !important;">&nbsp;</td>
                                <td style="width:0px !important;">&nbsp;</td>

                            </tr>
                            <tr>
                                <td style="text-align:center !important;">10:00 AM - <br><span
                                        style="margin-left:-6% !important;">05:30 PM</span></td>
                                <td style="text-align:center !important;"> - <br><span
                                        style="margin-left:-6% !important;"></span></td>
                                <td style="text-align:center !important;">10:00 AM - <br><span
                                        style="margin-left:-6% !important;">05:30 PM</span></td>
                                <td style="text-align:center !important;"> - <br><span
                                        style="margin-left:-6% !important;"></span></td>
                                <td style="text-align:center !important;">10:00 AM - <br><span
                                        style="margin-left:-6% !important;">05:30 PM</span></td>
                                <td style="width:0px !important;">&nbsp;</td>
                                <td style="width:0px !important;">&nbsp;</td>

                            </tr>
                            <tr>
                                <td style="text-align:center !important;">10:00 AM - <br><span
                                        style="margin-left:-6% !important;">05:30 PM</span></td>
                                <td style="text-align:center !important;"> - <br><span
                                        style="margin-left:-6% !important;"></span></td>
                                <td style="text-align:center !important;">10:00 AM - <br><span
                                        style="margin-left:-6% !important;">05:30 PM</span></td>
                                <td style="text-align:center !important;"> - <br><span
                                        style="margin-left:-6% !important;"></span></td>
                                <td style="text-align:center !important;">10:00 AM - <br><span
                                        style="margin-left:-6% !important;">05:30 PM</span></td>
                                <td style="width:0px !important;">&nbsp;</td>
                                <td style="width:0px !important;">&nbsp;</td>

                            </tr>
                            <tr>
                                <td style="text-align:center !important;">10:00 AM - <br><span
                                        style="margin-left:-6% !important;">05:30 PM</span></td>
                                <td style="text-align:center !important;"> - <br><span
                                        style="margin-left:-6% !important;"></span></td>
                                <td style="text-align:center !important;">10:00 AM - <br><span
                                        style="margin-left:-6% !important;">05:30 PM</span></td>
                                <td style="text-align:center !important;"> - <br><span
                                        style="margin-left:-6% !important;"></span></td>
                                <td style="text-align:center !important;">10:00 AM - <br><span
                                        style="margin-left:-6% !important;">05:30 PM</span></td>
                                <td style="width:0px !important;">&nbsp;</td>
                                <td style="width:0px !important;">&nbsp;</td>

                            </tr>
                            <tr>
                                <td style="text-align:center !important;">10:00 AM - <br><span
                                        style="margin-left:-6% !important;">05:30 PM</span></td>
                                <td style="text-align:center !important;"> - <br><span
                                        style="margin-left:-6% !important;"></span></td>
                                <td style="text-align:center !important;">10:00 AM - <br><span
                                        style="margin-left:-6% !important;">05:30 PM</span></td>
                                <td style="text-align:center !important;"> - <br><span
                                        style="margin-left:-6% !important;"></span></td>
                                <td style="text-align:center !important;">10:00 AM - <br><span
                                        style="margin-left:-6% !important;">05:30 PM</span></td>
                                <td style="width:0px !important;">&nbsp;</td>
                                <td style="width:0px !important;">&nbsp;</td>

                            </tr>
                            <tr>
                                <td style="text-align:center !important;">10:00 AM - <br><span
                                        style="margin-left:-6% !important;">05:30 PM</span></td>
                                <td style="text-align:center !important;"> - <br><span
                                        style="margin-left:-6% !important;"></span></td>
                                <td style="text-align:center !important;">10:00 AM - <br><span
                                        style="margin-left:-6% !important;">05:30 PM</span></td>
                                <td style="text-align:center !important;"> - <br><span
                                        style="margin-left:-6% !important;"></span></td>
                                <td style="text-align:center !important;">10:00 AM - <br><span
                                        style="margin-left:-6% !important;">05:30 PM</span></td>
                                <td style="width:0px !important;">&nbsp;</td>
                                <td style="width:0px !important;">&nbsp;</td>

                            </tr>
                            <tr>
                                <td style="text-align:center !important;">10:00 AM - <br><span
                                        style="margin-left:-6% !important;">05:30 PM</span></td>
                                <td style="text-align:center !important;"> - <br><span
                                        style="margin-left:-6% !important;"></span></td>
                                <td style="text-align:center !important;">10:00 AM - <br><span
                                        style="margin-left:-6% !important;">05:30 PM</span></td>
                                <td style="text-align:center !important;"> - <br><span
                                        style="margin-left:-6% !important;"></span></td>
                                <td style="text-align:center !important;">10:00 AM - <br><span
                                        style="margin-left:-6% !important;">05:30 PM</span></td>
                                <td style="width:0px !important;">&nbsp;</td>
                                <td style="width:0px !important;">&nbsp;</td>

                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button onclick="printDiv('printMe')">Print only the above div</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>



    <!-- Modal -->
    <div id="myModalcustomershseet" class="modal fade" role="dialog">
        <div class="modal-dialog" style="width:1000px;">
            <form method="post" action="<?php echo e(route('addcoversationsheet')); ?>" class="form-horizontal"
                  enctype="multipart/form-data">
            <?php echo e(csrf_field()); ?>


            <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Conversation Sheet</h4>
                    </div>
                    <div class="modal-body">
                        <div style="max-width: 870px;margin-left: auto;">
                            <div class="row mt-3" style="margin-top:20px;">
                                <div class="col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <div class="">
                                            <label class="control-label col-md-5 text-right"
                                                   style="padding-right:28px!important;padding-left:0px;width: 115px;"><strong>Date:</strong></label>
                                            <div class="col-md-6 prr-auto" style="padding-left:2px; padding-right:0px">
                                                <!--<input type="text" class="form-control effective_date2"/>-->
                                                <input type="text" name="date" id="date"
                                                       class="form-control text-center" value="<?php echo e(date('m-d-Y')); ?>"
                                                       placeholder="Date" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="form-group">
                                        <div class="">
                                            <label class="control-label col-md-3 text-right"
                                                   style="padding-left: 0px !important; padding-right: 20px!important;"><strong>Day:</strong></label>
                                            <div class="col-md-7"
                                                 style="padding-right: 0px!important; padding-left: 0px;">
                                                <input type="text" name="day" id="day" class="form-control text-center"
                                                       placeholder="Day" value="<?php echo e(date('l')); ?>" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <div class="">
                                            <label class="control-label col-md-3"><strong>Time:</strong></label>
                                            <div class="col-md-6">
                                                <input type="text" name="time" id="time"
                                                       class="form-control text-center" value="<?php echo e(date("g:i a")); ?>"
                                                       placeholder="Time" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group<?php echo e($errors->has('type_user') ? ' has-error' : ''); ?>  col-md-5">
                                    <div class="row">
                                        <label class="control-label col-md-3 text-right" style="width:115px;"><strong>Select:</strong></label>
                                        <div class="col-md-8">
                                            <div class="">
                                                <select class="form-control fsc-input" name="type_user" id="type_user"
                                                        required>
                                                    <option>Select</option>
                                                    <option>Client</option>
                                                    <option>EE-User</option>
                                                    <option>Vendor</option>
                                                    <option>Other</option>
                                                </select>
                                            </div>
                                            <?php if($errors->has('type_user')): ?>
                                                <span class="help-block">
                        <strong><?php echo e($errors->first('type_user')); ?></strong>
                        </span>
                                            <?php endif; ?>
                                        </div>

                                    </div>
                                </div>

                                <div
                                    class="form-group<?php echo e($errors->has('conrelatedname') ? ' has-error' : ''); ?> col-md-7">
                                    <div class="row">
                                        <label class="control-label col-md-4"
                                               style="text-align:right;width: 123px;padding-left: 0px;padding-right: 0px;"><strong>Related
                                                to:</strong></label>
                                        <div class="one_line">
                                            <div class="col-md-6" style="width:36%;">
                                                <div class="">
                                                    <select class="form-control" name="conrelatedname"
                                                            id="conrelatedname" required>
                                                        <option>Select</option>
                                                        <?php $__currentLoopData = $relatedNames; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $relatedNames): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <option
                                                                value="<?php echo e($relatedNames->id); ?>"><?php echo e($relatedNames->relatednames); ?></option>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </select>
                                                </div>
                                                <?php if($errors->has('conrelatedname')): ?>
                                                    <span class="help-block">
                                <strong><?php echo e($errors->first('conrelatedname')); ?></strong>
                                </span>
                                                <?php endif; ?>
                                            </div>
                                            <div class="col-md-1" style="padding: 0px;margin-top:3px;width: 92px;">
                                                <a href="#" class="btn btn-primary" data-toggle="modal"
                                                   data-target="#basicExampleModal" class="redius"><i
                                                        class="fa fa-plus"></i></a> &nbsp;
                                                <a href="#" class="btn btn-primary" data-toggle="modal"
                                                   data-target="#basicExampleModal3" class="redius"><i
                                                        class="fa fa-minus"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" id="clientid" style="display:none;">
                                <div class="row">
                                    <label class="control-label col-md-3 text-right" style="width:115px;"><strong>Client
                                            :</strong></label>
                                    <div class="col-md-4" style="width:69.5%;">
                                        <select class="form-control selectclientid" name="clientid">
                                            <option>Select</option>
                                            <?php $__currentLoopData = $listclient; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $client): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php if($client->business_id == '6'): ?>
                                                    <option value="<?php echo e($client->cid); ?>"><?php echo e($client->entityid); ?>

                                                        | <?php echo e($client->fname); ?> <?php echo e($client->mname); ?> <?php echo e($client->lname); ?>

                                                        | <?php echo e($client->business_no); ?></option>
                                                <?php else: ?>
                                                    <option value="<?php echo e($client->cid); ?>"><?php echo e($client->entityid); ?>

                                                        | <?php echo e($client->cname); ?> | <?php echo e($client->business_no); ?></option>
                                                <?php endif; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div>

                                </div>
                            </div>

                            <div class="form-group" id="employeeuserid" style="display:none;">
                                <div class="row">
                                    <label class="control-label col-md-3 text-right" style="width:115px;"><strong>EE /
                                            User:</strong></label>
                                    <div class="col-md-5" style="width:69.5%;">
                                        <select class="form-control selectemployee" name="employeeuserid">
                                            <option>Select</option>
                                            <?php $__currentLoopData = $listemployeeuser; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $employee): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option
                                                    value="<?php echo e($employee->id); ?>"><?php echo e($employee->firstName); ?> <?php echo e($employee->middleName); ?> <?php echo e($employee->lastName); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div>

                                </div>
                            </div>

                            <div class="form-group" id="vendorid" style="display:none;">
                                <div class="row">
                                    <label class="control-label col-md-3 text-right" style="width:115px;"><strong>Vendor:</strong></label>
                                    <div class="col-md-5" style="width:69.5%;">
                                        <select class="form-control selectvendor" name="vendorid">
                                            <option>Select</option>
                                            <?php $__currentLoopData = $listvendoe; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vendoe): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($vendoe->id); ?>"><?php echo e($vendoe->business_name); ?> | <?php echo e($vendoe->telephoneNo1); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group" id="otherid" style="display:none;">
                                <div class="row">
                                    <label class="control-label col-md-3"
                                           style="width:115px; text-align:right; padding-left:0px!important;"><strong>Other
                                            :</strong></label>
                                    <div class="col-md-5" style="width:69.5%;">
                                        <input type="text" class="form-control" name="otherid">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <label class="control-label col-md-3 text-right"
                                           style="width:115px; text-align:right; padding-left:0px;"><strong>Conversation:</strong></label>
                                    <div class="col-md-9" style="width:69.5%;">
                                        <textarea rows="3" cols="12" class="form-control"
                                                  name="condescription"> </textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <label class="control-label col-md-4"
                                           style="width:115px; text-align:right; "><strong>Note:</strong></label>
                                    <div class="col-md-9" style="width:69.5%;">
                                        <textarea rows="3" cols="12" class="form-control" name="connotes"> </textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <label class="control-label col-md-3 no_resp"
                                           style="width:115px; text-align:right; padding-left:0px!important;"><strong>&nbsp;</strong></label>
                                    <div class="col-md-2 col-sm-4 col-xs-5">
                                        <input class="btn_new_save btn-primary1" id="recInsert" type="submit"
                                               name="submit" value="Save">
                                    </div>
                                    <div class="col-md-2 col-sm-4 col-xs-5">
                                        <button class="btn_new_cancel" data-dismiss="modal">Cancel</button>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                    <div class="modal-footer">
                        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                    </div>
                </div>

            </form>
        </div>
    </div>



    <!-- Modal -->
    <div id="mytask1" class="modal fade" role="dialog">
        <div class="modal-dialog" style="width:900px;">
            <form method="post" action="" class="form-horizontal" enctype="multipart/form-data">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"> Task</h4>
                    </div>
                    <div class="modal-body">
                        <div>
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered" id="sampleTable2">
                                    <thead>
                                    <tr>
                                        <th>Task Date</th>
                                        <th>Created By</th>
                                        <th>Assign To</th>
                                        <th>Client / Other</th>
                                        <th>Subject</th>
                                        <th>Task Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $__currentLoopData = $taskall; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $com): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php //echo $com->created_at;
                                        ?>
                                        <tr>

                                            <td><?php echo e(date('m/d/Y',strtotime($com->created_at))); ?></td>
                                            <td><?php if($com->admin_id==Auth::user()->id): ?> <?php echo e(Auth::user()->fname.' '.Auth::user()->lname); ?> <?php endif; ?> <?php $__currentLoopData = $empfsc; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $com1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php if($com->admin_id==$com1->id): ?> <?php echo e(ucwords($com1->name)); ?> <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></td>
                                            <td><?php $__currentLoopData = $set1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $com1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php if($com->employeeid==$com1->id): ?> <?php echo e(ucwords($com1->firstName)); ?> <?php echo e(ucwords($com1->middleName)); ?> <?php echo e(ucwords($com1->lastName)); ?> <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></td>
                                            <td>
                                                <?php if($com->whone =='Client'): ?>
                                                    <?php $__currentLoopData = $commonregister1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $com2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <?php if($com->client==$com2->id): ?>
                                                            <?php echo e(ucwords($com2->company_name)); ?>

                                                        <?php endif; ?>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php elseif($com->whone =='Other'): ?>
                                                    <?php echo e($com->othername); ?>

                                                <?php endif; ?></td>

                                            <td><?php echo e($com->title); ?></td>
                                            <td><?php if($com->status==2): ?> In Progress <?php endif; ?> <?php if($com->status==0): ?>
                                                    Wait <?php endif; ?>  <?php if($com->status==1): ?> Start <?php endif; ?> <?php if($com->status==3): ?>
                                                    End <?php endif; ?></td>
                                            <td class="text-center"><a class="btn-action btn-view-edit"
                                                                       href="<?php echo e(route('task.edit',$com->id)); ?>"><i
                                                        class="fa fa-edit"></i></a></td>
                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <div class="col-md-12 "
                             style="padding-left:6px; margin-top:3px; padding-right:0px; text-align:center;">
                            <div style="position:Absolute;right:0px">
                                <a href="<?php echo e(url('fac-Bhavesh-0554/task/create')); ?>" class="btn btn-warning"
                                   class="redius">New Task</a>&nbsp;&nbsp;&nbsp;
                                <a href="<?php echo e(url('fac-Bhavesh-0554/task')); ?>" class="btn btn-primary" class="redius">Task
                                    List</a>
                            </div>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
    <div id="mytask2" class="modal fade" role="dialog">
        <div class="modal-dialog" style="width:850px;">
            <form method="post" action="" class="form-horizontal" enctype="multipart/form-data">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"> How to Do</h4>
                    </div>
                    <div class="modal-body">
                        <div>
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered" id="sampleTable2">
                                    <thead>
                                    <tr>
                                        <th style="width:10%">Task Date</th>
                                        <th>Created By</th>
                                        <th>Assign To</th>
                                        <th>Client / Other</th>
                                        <th>Subject</th>
                                        <th>Task Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">

                    </div>
                </div>

            </form>
        </div>
    </div>
    <div id="mydemo1" class="modal fade" role="dialog">
        <div class="modal-dialog" style="width:850px;">
            <form method="post" action="" class="form-horizontal" enctype="multipart/form-data">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"> Email</h4>
                    </div>
                    <div class="modal-body">
                        <div style="max-width:700px; margin:0px auto;">

                            <img src="http://financialservicecenter.net/public/frontcss/images/fsc_logo.png" alt="img"
                                 style="width:100px;">
                            <br><br>
                            <table border='1'
                                   style='color: #333;font-family: Helvetica, Arial, sans-serif;width:100%;border-collapse:collapse; border-spacing: 0;'>
                                <tr style='background:#535da6;'>
                                    <td colspan='2' style='text-align:center;font-size:18px;color:#FFF;padding:10px 0;'>
                                        You have received New Apply Service
                                    </td>
                                </tr>
                                <tr>
                                    <td style='width:40%;text-align:left;font-weight:bold;border:1px solid #CCC;height:30px;padding:0 10px;'>
                                        <p style="margin-bottom:0px;">Full Name :</p>
                                    </td>
                                    <td style='width:60%;text-align:left;padding:0 10px; border: 1px solid #CCC; padding:0 10px; height: 30px;'>

                                        <p style="margin-bottom:0px;">
                                            test
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='width:40%;text-align:left;font-weight:bold;border:1px solid #CCC;height:30px;padding:0 10px;'>
                                        <p style="margin-bottom:0px;">Email :</p>
                                    </td>
                                    <td style='width:60%;text-align:left;padding:0 10px;border: 1px solid #CCC; height: 30px;'>

                                        <p style="margin-bottom:0px;">
                                            test
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='width:40%;text-align:left;font-weight:bold;border:1px solid #CCC;height:30px;padding:0 10px;'>
                                        <p style="margin-bottom:0px;">Type of Service :</p>
                                    </td>
                                    <td style='width:60%;text-align:left;padding:0 10px;border: 1px solid #CCC; height: 30px;'>

                                        <p style="margin-bottom:0px;">
                                            test
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='width:40%;text-align:left;font-weight:bold;border:1px solid #CCC;height:30px;padding:0 10px;'>
                                        <p style="margin-bottom:0px;">New Business :</p>
                                    </td>
                                    <td style='width:60%;text-align:left;padding:0 10px;border: 1px solid #CCC; height: 30px;'>

                                        <p style="margin-bottom:0px;">
                                            test
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='width:40%;text-align:left;font-weight:bold;border:1px solid #CCC;height:30px;padding:0 10px;'>
                                        <p style="margin-bottom:0px;">Company Legal Name : </p>
                                    </td>
                                    <td style='width:60%;text-align:left;padding:0 10px;border: 1px solid #CCC; height: 30px;'>

                                        <p style="margin-bottom:0px;">
                                            test
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='width:40%;text-align:left;font-weight:bold;border:1px solid #CCC;height:30px;padding:0 10px;'>
                                        <p style="margin-bottom:0px;">Business Name (DBA) :</p>
                                    </td>
                                    <td style='width:60%;text-align:left;padding:0 10px;border: 1px solid #CCC; height: 30px;'>

                                        <p style="margin-bottom:0px;">
                                            test
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='width:40%;text-align:left;font-weight:bold;border:1px solid #CCC;height:30px;padding:0 10px;'>
                                        <p style="margin-bottom:0px;">Type of Business :</p>
                                    </td>
                                    <td style='width:60%;text-align:left;padding:0 10px;border: 1px solid #CCC; height: 30px;'>

                                        <p style="margin-bottom:0px;">
                                            test
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='width:40%;text-align:left;font-weight:bold;border:1px solid #CCC;height:30px;padding:0 10px;'>
                                        <p style="margin-bottom:0px;">Business Location Address : </p>
                                    </td>
                                    <td style='width:60%;text-align:left;padding:0 10px;border: 1px solid #CCC; height: 30px;'>

                                        <p style="margin-bottom:0px;">
                                            test
                                        </p>
                                    </td>
                                </tr>
                            </table>


                        </div>
                    </div>
                    <div class="modal-footer">
                        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                    </div>
                </div>

            </form>
        </div>
    </div>
    <div id="modalComplaint" class="modal fade" role="dialog">
        <div class="modal-dialog" style="width:850px;">
            <form method="post" action="" class="form-horizontal" enctype="multipart/form-data">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"> Complaint</h4>
                    </div>
                    <div class="modal-body">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Date</th>
                                    <th>Subject</th>
                                    <th>Complaint From</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <form method="post" action="<?php echo e(route('prospect.storeProposalClient')); ?>" class="form-horizontal"
          name="proposal_to_client" id="proposal_to_client" enctype="multipart/form-data">
        <?php echo e(csrf_field()); ?>

        <div id="modalProposal" class="modal fade" role="dialog">
            <div class="modal-dialog" style="width:710px;">

                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close modal3" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"> Proposal</h4>
                    </div>
                    <div class="modal-body">

                        <h4 style="width: 130px;padding: 7px;text-align: center;margin: auto;margin-bottom: 20px;background: #d3edff;border: 1px solid #106097;">
                            Basic Info</h4>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label class="control-label col-md-3 text-right"><strong>Date :</strong></label>
                                    <div class="col-md-3">
                                        <input type="text" readonly value="<?php echo e(date('m/d/Y')); ?>"
                                               class="form-control text-center">
                                    </div>
                                    <label class="control-label col-md-2 text-right"><strong>Day :</strong></label>
                                    <div class="col-md-3">
                                        <input type="text" readonly value="<?php echo e(date('l')); ?>"
                                               class="form-control text-center">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-4">
                                        <input type="radio" id="new_client" name="client_type" value="1">
                                        <label for="new_client" style="margin-left:5px;">New Prospect</label>
                                    </div>
                                    <div class="col-md-4">
                                        <input type="radio" id="already_client" name="client_type" value="2" checked>
                                        <label for="already_client" style="margin-left:5px;">Already Client</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row" id="search">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label class="control-label col-md-3 text-right"><strong>Search Client
                                            :</strong></label>
                                    <div class="col-md-8">
                                        <input type="text" name="csearch" id="search_client_name" class="form-control"
                                               placeholder="Search">

                                        <ul id="clientList"></ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row" id="exists_client">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label class="control-label col-md-3 text-right"><strong id="lbl_id_change">Client
                                            ID :</strong></label>
                                    <div class="col-md-8">
                                        <input type="text" name="proposal_client_id" id="proposal_client_id"
                                               class="form-control" readonly>

                                        <ul id="clientList"></ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group <?php echo e($errors->has('praposal_client_name') ? ' has-error' : ''); ?>">
                                <div class="col-md-12">
                                    <label class="control-label col-md-3 text-right"><strong>Client Name : <span
                                                class="required">*</span></strong></label>
                                    <div class="col-md-8">
                                        <input type="text" id="praposal_client_name" name="praposal_client_name"
                                               class="form-control" readonly required>
                                        <?php if($errors->has('praposal_client_name')): ?>
                                            <span class="help-block">
											<strong><?php echo e($errors->first('praposal_client_name')); ?></strong>
										</span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group <?php echo e($errors->has('praposal_client_telephone') ? ' has-error' : ''); ?>">
                                <div class="col-md-12 ">
                                    <label class="control-label col-md-3 text-right"><strong>Telephone : <span
                                                class="required">*</span></strong></label>
                                    <div class="col-md-8">
                                        <input type="text" id="praposal_client_telephone"
                                               name="praposal_client_telephone" class="form-control" readonly required>
                                        <?php if($errors->has('praposal_client_telephone')): ?>
                                            <span class="help-block">
											<strong><?php echo e($errors->first('praposal_client_telephone')); ?></strong>
										</span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group <?php echo e($errors->has('praposal_client_email') ? ' has-error' : ''); ?>">
                                <div class="col-md-12 ">
                                    <label class="control-label col-md-3 text-right"><strong>Email : <span
                                                class="required">*</span></strong></label>
                                    <div class="col-md-8">
                                        <input type="text" id="praposal_client_email" name="praposal_client_email"
                                               class="form-control" readonly required>
                                        <?php if($errors->has('praposal_client_email')): ?>
                                            <span class="help-block">
											<strong><?php echo e($errors->first('praposal_client_email')); ?></strong>
										</span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group <?php echo e($errors->has('praposal_client_address') ? ' has-error' : ''); ?>">
                                <div class="col-md-12 ">
                                    <label class="control-label col-md-3 text-right"><strong>Address :</strong></label>
                                    <div class="col-md-8">
                                        <input type="text" id="praposal_client_address" name="praposal_client_address"
                                               class="form-control" readonly required>
                                        <?php if($errors->has('praposal_client_address')): ?>
                                            <span class="help-block">
											<strong><?php echo e($errors->first('praposal_client_address')); ?></strong>
										</span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group <?php echo e($errors->has('praposal_client_email') ? ' has-error' : ''); ?>">
                                <div class="col-md-12 ">
                                    <label class="control-label col-md-3 text-right"><strong>City / State / Zip
                                            :</strong></label>
                                    <div class="col-md-3">
                                        <input type="text" id="praposal_client_city" name="praposal_client_city"
                                               class="form-control" readonly required>
                                        <?php if($errors->has('praposal_client_city')): ?>
                                            <span class="help-block">
											<strong><?php echo e($errors->first('praposal_client_city')); ?></strong>
										</span>
                                        <?php endif; ?>
                                    </div>
                                    <div class="col-md-2">
                                        <input type="text" id="praposal_client_state" name="praposal_client_state"
                                               class="form-control" readonly required>
                                        <?php if($errors->has('praposal_client_state')): ?>
                                            <span class="help-block">
											<strong><?php echo e($errors->first('praposal_client_state')); ?></strong>
										</span>
                                        <?php endif; ?>
                                    </div>
                                    <div class="col-md-3">
                                        <input type="text" id="praposal_client_zip" name="praposal_client_zip"
                                               class="form-control" readonly required>
                                        <?php if($errors->has('praposal_client_zip')): ?>
                                            <span class="help-block">
											<strong><?php echo e($errors->first('praposal_client_zip')); ?></strong>
										</span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <hr style="border-top:2px solid #9f9f9f;margin-bottom:10px;">
                        <h4 style="width: 170px;padding: 7px;text-align: center;margin: auto;margin-bottom: 20px;background: #d3edff;border: 1px solid #106097;">
                            Scope Of Work</h4>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label class="control-label col-md-3 text-right"><strong>Type of Service :</strong></label>
                                    <div class="col-md-5">
                                        <select class="form-control" id="prospect_service" name="prospect_service"
                                                required>
                                            <option value="">Select Service</option>
                                            <?php if($servicearr): ?>
                                                <?php $__currentLoopData = $servicearr; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $service): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option
                                                        value="<?php echo e($service['id']); ?>"><?php echo e($service['typeofservice']); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endif; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row" id="div_taxtation_type">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label class="control-label col-md-3 text-right"><strong>Taxatation Type :</strong></label>
                                    <div class="col-md-2">
                                        <select class="form-control" id="taxtation_type" name="taxtation_type">
                                            <option value="">Select</option>
                                            <option value="Issue">Issue</option>
                                            <option value="Audit">Audit</option>
                                            <option value="Regular">Regular</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <select class="form-control" id="proposal_taxtitles" name="proposal_taxtitles">
                                            <option value="">Select</option>
                                            <?php $__currentLoopData = $taxtitle; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $taxx): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($taxx->id); ?>"><?php echo e($taxx->title); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row" id="period">
                            <div class="form-group">
                                <!--<div class="col-md-8 col-md-offset-3">-->
                                <!--    <div id="jstree">-->
                                <!--    </div>-->

                                <!--</div>-->

                                <div class="col-md-12">
                                    <label class="control-label col-md-3 text-right"><strong>Service Period
                                            :</strong></label>
                                    <div class="col-md-3">
                                        <select class="form-control" id="period_service" name="period_service" required>

                                        </select>
                                    </div>
                                </div>
                                <p class="text-danger" id="Error">No Service Found !</p>
                            </div>
                        </div>

                        <div class="row" id="div_period_val">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label class="control-label col-md-3 text-right"><strong>Service Period
                                            :</strong></label>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control" name="period_service"
                                               id="lbl_service_period" readonly/>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="">
                            <div class="form-group <?php echo e($errors->has('priority') ? ' has-error' : ''); ?>">
                                <div class="col-md-6">
                                    <div class="row">
                                        <label class="control-label col-md-6 text-right"><strong>Priority
                                                :</strong></label>
                                        <div class="col-md-6">
                                            <select class="form-control" name="priority" id="priority" required>
                                                <option value="">Select</option>
                                                <option value="Very Urgent">Very Urgent</option>
                                                <option value="Time Sensitive">Time Sensitive</option>
                                                <option value="Regular">Regular</option>
                                            </select>
                                            <?php if($errors->has('priority')): ?>
                                                <span class="help-block">
											<strong><?php echo e($errors->first('priority')); ?></strong>
										</span>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6" style="padding-left:0px;">
                                    <div class="" id="duedate_div">
                                        <div class="form-group ">
                                            <label class="control-label col-md-5 text-right"><strong>Due Date :</strong></label>
                                            <div class="col-md-5">
                                                <input type="text" class="form-control" name="duedate"
                                                       id="proposal_duedate" required/>
                                                <?php if($errors->has('duedate')): ?>
                                                    <span class="help-block">
    											<strong><?php echo e($errors->first('duedate')); ?></strong>
    										</span>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group <?php echo e($errors->has('scope_of_work') ? ' has-error' : ''); ?>">
                                <div class="col-md-12">
                                    <label class="control-label col-md-3 text-right"><strong>Scope of Work
                                            :</strong></label>
                                    <div class="col-md-8">
                                        <textarea type="text" class="form-control" rows="4" name="scope_of_work"
                                                  id="scope_of_work"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <input type="hidden" name="selective_services" id="selective_services" value=""/>


                        <div class="row">
                            <div class="form-group <?php echo e($errors->has('fsc_fee') ? ' has-error' : ''); ?>">
                                <div class="col-md-12">
                                    <label class="control-label col-md-3 text-right"><strong>Fees :</strong></label>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control only_num text-right" name="fsc_fee"
                                               id="fsc_fee" placeholder="0.00" required/>
                                        <?php if($errors->has('fsc_fee')): ?>
                                            <span class="help-block">
											<strong><?php echo e($errors->first('fsc_fee')); ?></strong>
										</span>
                                        <?php endif; ?>
                                    </div>
                                    <label class="control-label col-md-2 text-right"><strong>Discount :</strong></label>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control only_num text-right" name="discount"
                                               id="discount" placeholder="0.00" required/>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group <?php echo e($errors->has('fsc_fee') ? ' has-error' : ''); ?>">
                                <div class="col-md-12">
                                    <label class="control-label col-md-3 text-right"><strong>Advance Payment :</strong></label>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control only_num text-right" name="adv_payment"
                                               id="adv_payment" placeholder="0.00" required/>
                                    </div>
                                    <label class="control-label col-md-2 text-right"><strong>Net Amount
                                            :</strong></label>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control only_num text-right" name="net_amount"
                                               id="net_amount" placeholder="0.00" required disabled/>
                                        <input type="hidden" name="description" value="" id="lbl_description_val"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <label class="control-label col-md-3 no_resp"
                                   style="text-align:right; padding-left:0px!important;"><strong>&nbsp;</strong></label>
                            <!--         <div class="col-md-2 col-sm-4 col-xs-5" style="width:22.11%">-->
                            <!--             <input class="btn_new_save btn-primary1" id="btn_submit" type="submit" name="submit" value="Send">-->
                            <!--</div>-->
                            <div class="col-md-2 col-sm-4 col-xs-5" style="width:22.11%">
                                <a class="btn_new_cancel modal3" data-dismiss="modal">Cancel</a>
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-5" style="width:22.11%">
                                <a class="btn btn-success btn_new_preview" data-toggle="modal"
                                   data-target="#preview_proposal">Preview</a>
                            </div>
                        </div>

                    </div>

                </div>

            </div>
        </div>

        <div class="modal fade" id="preview_proposal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document" style="width:550px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close modal2" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="exampleModalLabel">Select Type of Preview</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12" style="padding-left:50px;">
                                <input type="radio" id="invoice" name="preview_types" value="invoice"
                                       class="custom_radio" checked><label for="invoice"
                                                                           style="padding-left:10px;font-size:16px;">Proposal
                                    / Invoice Format</label>

                            </div>
                            <div class="col-md-12" style="padding-left:50px;">
                                <input type="radio" id="agreement" name="preview_types" value="agreement"
                                       class="custom_radio"><label for="agreement"
                                                                   style="padding-left:10px;font-size:16px;">Agreement
                                    Format</label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary modal2" data-dismiss="modal" aria-label="Close">
                            Close
                        </button>
                        <a class="btn btn-primary pull-right" id="preview_type" data-toggle="modal"
                           data-target="#preview_invoice">Open</a>
                        <!--<a class="btn btn-primary pull-right" data-toggle="modal" data-target="#preview_invoice">Proposal Preview</a>-->
                        <!--<a class="btn btn-warning pull-right" data-toggle="modal" data-target="#preview_agreement">Agreement Preview</a>-->
                    </div>

                </div>
            </div>
        </div>

        <div class="modal fade" id="preview_agreement" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <!--<div class="modal-header">-->
                    <!--     <button type="button" class="close modal2" data-dismiss="modal" aria-label="Close">-->
                    <!--   <span aria-hidden="true">&times;</span>-->
                    <!--   </button>-->
                    <!--   <h4 class="modal-title" id="exampleModalLabel">Preview of Agreement</h4>-->
                    <!--</div>-->

                    <div class="modal-body">

                        <img src="https://financialservicecenter.net/public/dashboard/images/fsc_logo.png" width="100px"
                             style="margin:10px auto;" class="img-responsive">
                        <p class="text-center" id="text_title" style="font-size: 16px;font-weight: 600;margin-top: 10px;">
                            <span>4550 Jimmy Carter Blvd.</span>
                            <span style="padding: 0px 5px;"> | </span> Norcroos
                            <span style="padding: 0px 5px;"> | </span> GA
                            <span style="padding: 0px 5px;"> | </span> Tel. : +1 (770) 270-5597
                            <span style="padding: 0px 5px;"> | </span> Fax : +1 (770) 414-5351
                        </p>
                        <hr style="border-top: 1px solid #8e8e8e;margin-bottom:10px;">
                        <h3 class="text-center" style="margin-bottom:25px;text-transform: uppercase;">Agreement For</h3>
                        <p>
                            I / we the undersigned assign our Accounting and Tax matters to FINANCIAL SERVICE CENTER for
                            a term of one ( 1 ) year from the date of this
                            agreement. In return for the above consideration, I / we shall receive the following
                            services :
                        </p>
                        <h4>BOOKKEEPING</h4>
                        <ol>
                            <li>
                                <div class="lbl_description"></div>
                            </li>
                        </ol>
                        <h4>FEES ARE AS FOLLOWS :</h4>

                        <ul class=leaders>
                            <li><span>Installation Fee</span>
                                <span class="lbl_preview_amount"></span></li>
                            <li><span>Balance Due</span>
                                <span id="lbl_preview_bal_amount"></span></span></li>
                            <p style="font-size: 17px;margin: 17px 0px;">Any incident occurring prior to the date of
                                this agreement will not covered by our service.</p>
                        </ul>

                        <p>This agreement between the subscriber and FINANCIAL SERVICE CENTER shall be automatically
                            renewed for an additional year from date of agreement
                            unless written notice of cancellation is given at least 30 days before the expiration of the
                            year's service. Business will be reviewed in order to
                            establish any necessary adjustment in fee.</p>
                        <p>Client is responsible for submitting complete and accurate information to FINANCIAL CERVICE
                            CENTER.</p>

                        <div class="row">
                            <div class="col-sm-6">
                                <h4>FINANCIAL SERVICE CENTER</h4>
                                <p>Sign by :</p>
                                <p>_____________________</p>
                            </div>
                            <div class="col-sm-6">
                                <ul class=leaders1>
                                    <li><u><span class="lbl_preview_address">Address</span></u></li>
                                    <li><u><span class="lbl_preview_client">Name</span></u></li>
                                    <li><u><span><?php echo e(date('M/d/Y')); ?></span></u></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary modal2" data-dismiss="modal" aria-label="Close">
                            Close
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="preview_invoice" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <!--<div class="modal-header">-->
                    <!--     <button type="button" class="close modal2" data-dismiss="modal" aria-label="Close">-->
                    <!--   <span aria-hidden="true">&times;</span>-->
                    <!--   </button>-->
                    <!--   <h4 class="modal-title" id="exampleModalLabel">Preview of Proposal</h4>-->
                    <!--</div>-->

                    <div class="modal-body">

                        <img src="https://financialservicecenter.net/public/dashboard/images/fsc_logo.png" width="100px"
                             style="margin:10px auto;" class="img-responsive">
                        <p class="text-center" id="text_title" style="font-size: 16px;font-weight: 600;margin-top: 10px;">
                            <span>4550 Jimmy Carter Blvd.</span>
                            <span style="padding: 0px 5px;"> | </span> Norcroos
                            <span style="padding: 0px 5px;"> | </span> GA
                            <span style="padding: 0px 5px;"> | </span> Tel. : +1 (770) 270-5597
                            <span style="padding: 0px 5px;"> | </span> Fax : +1 (770) 414-5351
                        </p>
                        <hr style="border-top: 2px solid #8e8e8e;margin-bottom:10px;">
                        <h3 class="text-center" style="margin-top: 20px;font-weight: 600;font-size: 27px;">Proposal</h3>
                        <!--<hr style="border-top: 2px solid #8e8e8e;margin-top:10px;">-->
                        <div class="row">
                            <div class="col-md-6">
                                <strong>Proposal To:</strong>
                                <p class="lbl_preview_client_name" style="margin-bottom:0px;"></p>
                                <p class="lbl_preview_address" style="margin-bottom:0px;"></p>
                                <p class="lbl_preview_city_state_zip" style="margin-bottom:0px;"></p>
                                <p class="" style="margin-bottom:0px;">USA</p>
                            </div>

                            <div class="col-md-6">
                                <h4 id="lbl_proposal_no" style="margin-bottom:0px;text-align:right;margin:0;"><p
                                        style="width: 310px; margin: 0; float: left;">Proposal No : </p>
                                    <p style="float: right; margin: 0; font-weight: 100;"> M-20-1320 </p></h4>
                                <div class="clear clearfix"></div>
                                <h4 id="lbl_proposal_date" style="margin-bottom:0px;text-align:right;margin:0;"><p
                                        style="width:310px;margin: 5px 0;float: left;">Proposal Date : </p>
                                    <p style="float: right;margin: 5px 0;"> <?php echo e(date('m-d-Y')); ?> </p></h4>
                                <!--<p class="text-right" id="lbl_proposal_no" style="margin-bottom:0px;"><strong>Proposal No : </strong> M-20-1320 </p>-->
                            <!--<p class="text-right" id="lbl_proposal_date"><strong>Proposal Date : </strong> <?php echo e(date('m/d/Y')); ?> </p>-->
                            </div>
                        </div>
                        <div class=row>
                            <div class="col-md-6" style="margin-top:2%; margin-bottom:1%">
                                <strong>Customer ID:</strong><span class="lbl_preview_client_id"></span>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Priority</th>
                                    <th>Payment Terms</th>
                                    <th>Sales Rep ID</th>
                                    <th width="150px">Due Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="text-center"><p id="lbl_priority" style="margin-bottom:0px;"></p></td>
                                    <td class="text-center">Net Due</td>
                                    <td class="text-center"><p
                                            style="margin-bottom:0px;"><?php echo e(Auth::user()->minss); ?> <?php echo e(Auth::user()->fname); ?> <?php echo e(Auth::user()->mname); ?> <?php echo e(Auth::user()->lname); ?></p>
                                    </td>
                                    <td class="text-center"><p id="lbl_due_date" style="margin-bottom:0px;"></td>
                                </tr>
                                </tbody>
                            </table>
                            <table class="table table-bordered">
                                <thead>
                                <th>Description</th>
                                <th width="150px">Amount</th>
                                </thead>
                                <tbody>
                                <tr style="height:130px;">
                                    <td style="text-align:center;border-bottom: none !important; vertical-align:top">
                                        <div class="lbl_description"></div>
                                    </td>

                                    <td style="text-align:right;border-bottom: none !important; vertical-align:top"><p
                                            class="_lbl_preview_amount"></td>
                                </tr>
                                <tr>
                                    <td style="text-align:right;border-bottom: none !important;padding-bottom: 0px !important;">
                                        <strong>Total Proposal Amount</strong></td>
                                    <td style="text-align:right;border-bottom: none !important;padding-bottom: 0px !important;">
                                        <p id="total_prop_price" style="margin-bottom:0px;">0.00</p></td>
                                </tr>
                                <tr>
                                    <td style="text-align:right;border-bottom: none !important;border-top: none !important;padding-bottom: 0px !important;">
                                        <strong>Discount Amount</strong></td>
                                    <td style="text-align:right;border-bottom: none !important;border-top: none !important;padding-bottom: 0px !important;">
                                        <p id="lbl_preview_discountbox" style="margin-bottom:0px;">0.00</p></td>
                                </tr>
                                <tr>
                                    <td style="text-align:right;border-bottom: none !important;border-top: none !important;padding-bottom: 0px !important;">
                                        <strong>Required Advance Payment</strong></td>
                                    <td style="text-align:right;border-bottom: none !important;border-top: none !important;padding-bottom: 0px !important;">
                                        <p id="total_prop_advprice" style="margin-bottom:0px;">0.00</p></td>
                                </tr>
                                <tr>
                                    <td style="text-align:right;border-top: none !important;"><strong>Due
                                            Balance</strong></td>
                                    <td style="text-align:right;border-top: none !important;"><p
                                            id="total_prop_dueprice" style="margin-bottom:0px;">0.00</p></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <strong>Notes:</strong>
                                <p style="margin-bottom:0px;">*We accept Check,Cash and ACH from bank.</p>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <!--         <div class="col-md-2 col-sm-4 col-xs-5" style="width:22.11%">-->

                        <!--</div>-->
                        <input class="btn_new_save btn-primary1 modal2" style="display:inline-block;width:10%"
                               id="btn_submit" type="submit" name="submit" value="Send">
                        <button type="button" class="btn_new_cancel btn-secondary modal2"
                                style="display:inline-block;width:10%;" data-dismiss="modal" aria-label="Close">Close
                        </button>
                        <!--<button type="submit" class=" btn_new_save btn btn-secondary modal2">Send</button>-->
                    </div>
                </div>
            </div>
        </div>
    </form>
    <script>
        $(".modal2").click(function () {
            $("body").css("overflow", "hidden");
            $("#modalProposal").css("overflow-y", "auto");
        });
        $(".modal3").click(function () {
            $("body").css("overflow", "auto");
        });
    </script>
    <div id="myModalnotes" class="modal fade" role="dialog">
        <div class="modal-dialog" style="width:1000px;">
            <form method="post" action="<?php echo e(route('addnotesrelated')); ?>" class="form-horizontal"
                  enctype="multipart/form-data" id="noteforms">
            <?php echo e(csrf_field()); ?>


            <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"> Note</h4>
                    </div>
                    <div class="modal-body">
                        <div style="max-width: 900px;margin-left: auto;">
                            <div class="row mt-3" style="margin-top:20px;">
                                <div class="col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <div class="">
                                            <label class="control-label col-md-5 text-right"
                                                   style="padding-right:28px!important;padding-left:0px;width: 115px;"><strong>Date:</strong></label>
                                            <div class="col-md-6 prr-auto" style="padding-left:6px; padding-right:0px">
                                                <!--<input type="text" class="form-control effective_date2"/>-->
                                                <input type="text" name="notedate" id="date2"
                                                       class="form-control text-center" value="<?php echo e(date('m-d-Y')); ?>"
                                                       placeholder="Date" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="form-group">
                                        <div class="">
                                            <label class="control-label col-md-3 text-right"
                                                   style="padding-left: 0px !important; padding-right: 20px!important;"><strong>Day:</strong></label>
                                            <div class="col-md-7"
                                                 style=" padding-right:0px!important; padding-left:0px!important;">
                                                <input type="text" name="noteday" id="day2"
                                                       class="form-control text-center" placeholder="Day"
                                                       value="<?php echo e(date('l')); ?>" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <div class="">
                                            <label class="control-label col-md-3"><strong>Time:</strong></label>
                                            <div class="col-md-7">
                                                <input type="text" name="notetime" id="time2"
                                                       class="form-control text-center" value="<?php echo e(date("g:i a")); ?>"
                                                       placeholder="Time" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-5">
                                    <div class="row">
                                        <label class="control-label col-md-3 text-right" style="width:120px;"><strong>Select:</strong></label>
                                        <div class="col-md-7" style="padding-right: 0px;">
                                            <div class="">
                                                <select class="form-control fsc-input" name="notetype_user"
                                                        id="notetype_user" required>
                                                    <option>Select</option>
                                                    <option>Client</option>
                                                    <option>EE-User</option>
                                                    <option>Vendor</option>
                                                    <option>Other</option>
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group<?php echo e($errors->has('notesrelated') ? ' has-error' : ''); ?>  col-md-7">
                                    <div class="row">
                                        <label class="control-label col-md-4 text-right"
                                               style="text-align:right;width: 123px;padding-left: 0px;padding-right: 0px;"><strong>Related
                                                to :</strong></label>
                                        <div class="one_line">
                                            <div class="col-md-6" style="width:43%;">
                                                <div class="">
                                                    <select class="form-control" name="noterelated"
                                                            id="notesrelatedname" required>
                                                        <option>Select</option>
                                                        <?php $__currentLoopData = $notesRelated; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $notesRelated): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <option
                                                                value="<?php echo e($notesRelated->id); ?>"><?php echo e($notesRelated->notesrelated); ?></option>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </select>
                                                </div>
                                                <?php if($errors->has('notesrelated')): ?>
                                                    <span class="help-block">
                            <strong><?php echo e($errors->first('notesrelated')); ?></strong>
                            </span>
                                                <?php endif; ?>
                                            </div>
                                            <div class="col-md-1" style="padding: 0px;margin-top:3px;width: 92px;">
                                                <a href="#" class="btn btn-primary" data-toggle="modal"
                                                   data-target="#basicExampleModalNotes" class="redius"><i
                                                        class="fa fa-plus"></i></a>&nbsp;
                                                <a href="#" class="btn btn-primary" data-toggle="modal"
                                                   data-target="#basicExampleModal4" class="redius"><i
                                                        class="fa fa-minus"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>


                            <div class="form-group" id="noteclientid" style="display:none;">
                                <div class="row">
                                    <label class="control-label col-md-3 text-right" style="width:120px;"><strong>Client:</strong></label>
                                    <div class="col-md-5" style="width: 72.5%;">
                                        <select class="form-control selectclientid" name="noteclientid">
                                            <option>Select</option>
                                            <?php $__currentLoopData = $listclient; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $client): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php if($client->business_id == '6'): ?>
                                                    <option value="<?php echo e($client->cid); ?>"><?php echo e($client->entityid); ?>

                                                        | <?php echo e($client->fname); ?> <?php echo e($client->mname); ?> <?php echo e($client->lname); ?>

                                                        | <?php echo e($client->business_no); ?></option>
                                                <?php else: ?>
                                                    <option value="<?php echo e($client->cid); ?>"><?php echo e($client->entityid); ?>

                                                        | <?php echo e($client->cname); ?> | <?php echo e($client->business_no); ?></option>
                                                <?php endif; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div>

                                </div>
                            </div>

                            <div class="form-group" id="noteemployeeuserid" style="display:none;">
                                <div class="row">
                                    <label class="control-label col-md-3 text-right" style="width:120px;"><strong>Employee/User:</strong></label>
                                    <div class="col-md-5" style="width: 72.5%;">
                                        <select class="form-control selectemployee" name="noteemployeeuserid">
                                            <option>Select</option>
                                            <?php $__currentLoopData = $listemployeeuser; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $employee): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option
                                                    value="<?php echo e($employee->id); ?>"><?php echo e($employee->firstName); ?> <?php echo e($employee->middleName); ?> <?php echo e($employee->lastName); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div>

                                </div>
                            </div>

                            <div class="form-group" id="notevendorid" style="display:none;">
                                <div class="row">

                                    <label class="control-label col-md-3 text-right" style="width:120px;"><strong>Vendor
                                            :</strong></label>
                                    <div class="col-md-5" style="width: 72.5%;">
                                        <select class="form-control selectvendor" name="notevendorid">
                                            <option>Select</option>
                                            <?php $__currentLoopData = $listvendoe; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vendoe): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($vendoe->id); ?>"><?php echo e($vendoe->business_name); ?> | <?php echo e($vendoe->telephoneNo1); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div>

                                </div>
                            </div> 

                            <div class="form-group" id="noteotherid" style="display:none;">
                                <div class="row">

                                    <label class="control-label col-md-3 text-right" style="width:120px;"><strong>Other
                                            :</strong></label>
                                    <div class="col-md-5" style="width: 72.5%;">
                                        <input type="text" class="form-control" name="noteotherid">
                                    </div>

                                </div> 
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <label class="control-label col-md-3 text-right"
                                           style="width:120px; text-align:right; padding-left:0px;"><strong>Type of
                                            Note:</strong></label>
                                    <div class="col-md-3">
                                        <select class="form-control" name="notesrelatedcat" id="notesrelatedcat"
                                                required>
                                            <option>Select</option>
                                            <option>Permenant Note</option>
                                            <option>Temporary Note</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <label class="control-label col-md-3 text-right"
                                           style="width:120px; text-align:right; padding-left:0px;"><strong>Note:</strong></label>
                                    <div class="col-md-9" style="width: 72.5%;">
                                        <textarea rows="3" cols="12" class="form-control" name="notes"> </textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <label class="control-label col-md-3 no_resp"
                                           style="width:120px; text-align:right; padding-left:0px!important;"><strong>&nbsp;</strong></label>
                                    <div class="col-md-2 col-sm-4 col-xs-5">
                                        <input class="btn_new_save btn-primary1" id="recInsert2" type="submit"
                                               name="submit" value="Save">
                                    </div>
                                    <div class="col-md-2 col-sm-4 col-xs-5">
                                        <button class="btn_new_cancel" data-dismiss="modal">Cancel</button>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                    <div class="modal-footer">
                        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                    </div>
                </div>

            </form>
        </div>
    </div>

    <!--Add plus Notes Value Model Start-->
    <div class="modal fade" id="basicExampleModalNotes" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="exampleModalLabel">Related to Notes</h4>

                </div>
                <form action="" method="post" id="ajax2">
                    <?php echo e(csrf_field()); ?>

                    <div class="modal-body">
                        <input type="text" id="newnotesrelated" name="newopt2" class="form-control"
                               placeholder="Related Name"/>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="notesrelated" class="btn btn-primary">Save</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--Add plus value Model End-->


    <!--Add plus Conversation Value Model Start-->
    <div class="modal fade" id="basicExampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="exampleModalLabel">Related to Coversation</h4>

                </div>
                <form action="" method="post" id="ajax1">
                    <?php echo e(csrf_field()); ?>

                    <div class="modal-body">
                        <input type="text" id="newrelatednames" name="newopt" class="form-control"
                               placeholder="Related Name"/>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="addrelatedname" class="btn btn-primary">Save</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--Add plus value Model End-->


    <!--Minus Convesation value Model Start-->
    <div class="modal fade" id="basicExampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background:#038ee0;">
                    <h4 class="modal-title" id="exampleModalLabel" style="text-align:center;color:#000000;">Related to
                        Convesation
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </h4>

                </div>
                <div class="modal-body" style="background:#ffff99;padding:0px !important;">
                    <div class="curency curency_ref" id="conversationRelatedNameForDelete">
                        <?php $__currentLoopData = $relatedNames1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cur): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div id="cur_<?php echo e($cur->id); ?>" class="col-md-12" style="border:1px solid;background:#def9ff;">
                                <div class="col-md-12" style="margin-top: 7px;margin-bottom:5px;">

                                    <a class="deleterelated" style="color:#000;"
                                       id="<?php echo e($cur->id); ?>"><?php echo e($cur->relatednames); ?>


                                        <span class="pull-right"><i class="fa fa-trash"
                                                                    style="padding: 1px 6px!important;"></i></span></a>
                                </div>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
                <div class="modal-footer" style="text-align:center;">
                    <button type="button" style="margin-top:10px;border:1px solid;" class="btn btn-secondary"
                            data-dismiss="modal">Close
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!--MinusConvesation value Model End-->

    <!--Minus Notes value Model Start-->
    <div class="modal fade" id="basicExampleModal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background:#038ee0;">
                    <h4 class="modal-title" id="exampleModalLabel" style="text-align:center;color:#000;">Related to
                        Notes
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </h4>

                </div>
                <div class="modal-body" style="background:#ffff99;padding:0px !important;">
                    <div class="curency curency_ref" id="notesRelatedNameForDelete">
                        <?php $__currentLoopData = $notesRelated1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cur): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div id="cur_<?php echo e($cur->id); ?>" class="col-md-12" style="border:1px solid;background:#def9ff;">
                                <div class="col-md-12" style="margin-top: 7px;margin-bottom:5px;">
                                    <a class="deletenotes" style="color:#000;" id="<?php echo e($cur->id); ?>"><?php echo e($cur->notesrelated); ?>

                                        <span class="pull-right">
                                            <i class="fa fa-trash" style="padding: 1px 6px!important;"></i>
                                        </span>
                                    </a>
                                </div>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
                <div class="modal-footer" style="text-align:center;">
                    <button type="button" style="margin-top:10px;border:1px solid;" class="btn btn-secondary"
                            data-dismiss="modal">Close
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!--Minus Notes value Model End-->

    <!--Modal Federal tax Update Start-->
    <div id="federalModals" class="modal fade" role="dialog">
        <div class="modal-dialog" style="width:1000px;">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><span id="clid" class="companycode"></span>Client - Income Tax Filing -
                        (Form-1040) <span style="float:right; margin-right:5px;">Add Record</span></h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" method="post" action="<?php echo e(route('worktax.store')); ?>"
                          enctype="multipart/form-data">
                        <?php echo e(csrf_field()); ?>



                        <input type="hidden" name="client_id" id="federalid">

                        <div class="recordform">


                            <table style="width:100%; max-width:620px; margin:0px auto;" class="taxtable edittable">
                                <tr>
                                    <TD style="WIDTH:200PX;">
                                        <label class="control-label labels">Client Name:</label>
                                        <input type="text" class="form-control clname" readonly>
                                    </TD>

                                    <td>
                                        <label class="control-label labels">Filling Year</label>
                                        <select class="form-control federalyear" name="federalsyear" id="federalsyear"
                                                required>
                                            <option value="">Select</option>
                                            <?php

                                            $now = date('M-d-Y');
                                            if('Jul-25-2021' == $now)
                                            {
                                            $aa = date('Y') - 2;
                                            $bb = date('Y') - 1;
                                            ?>
                                            <option value="<?php echo $aa;?>"><?php echo $aa;?></option>
                                            <option value="<?php echo $bb;?>"><?php echo $bb;?></option>
                                            <?php
                                            }
                                            else
                                            {
                                            $aa = date('Y') - 1;
                                            ?>
                                            <option value="<?php echo $aa;?>"><?php echo $aa;?></option>
                                            <?php
                                            }

                                            ?>


                                        </select>

                                    </td>
                                    <td>
                                        <label class="control-label labels">Tax Return</label>

                                        <select class="form-control taxation" name="federalstax" required>
                                            <option value="">Select</option>
                                            <option value="Extension">Extension</option>
                                            <option value="Original">Original</option>
                                            <option value="Amendment">Amendment</option>
                                        </select>
                                    </td>
                                    <td style="width:150px;">
                                        <label class="control-label labels">Due Date</label>
                                        <input type="text" class="form-control duedate" name="federalsduedate"
                                               placeholder="Mar-15-2020" readonly/>
                                    </td>
                                </tr>
                            </table>

                            <table class="taxtable table table-bordered" style="margin-top:20px;" id="taxationtable">
                                <tr>

                                    <td style="width:215px;" colspan="2">
                                        <label class="form-label">Federal Form No.</label>
                                        <input type="text" readonly class="form-control formno" name="federalsform"
                                               style="width:100%; max-width:100%; margin:0px; padding:0px;" required>

                                    </td>
                                    <td style="width:120px;">
                                        <label class="form-label">Filling Method</label>
                                        <select class="form-control filingmethod" name="federalsmethod" required>
                                            <option value="">Select</option>
                                            <option value="E-File">E-File</option>
                                            <option value="Paperfile">Paperfile</option>
                                        </select>

                                    </td>
                                    <td style="width:120px;">
                                        <label class="form-label">Filling Date</label>
                                        <input type="text" class="form-control fdate fdatef" id="fillingdate"
                                               name="federalsdate" placeholder="MM/DD/YYYY"/>
                                        <input type="hidden" class="form-control" id="federalsfile" name="federalsfile"
                                               required/>

                                    </td>
                                    <td style="width:150px;">
                                        <label class="form-label">Status of Filling</label>
                                        <select class="form-control" name="federalsstatus" required>
                                            <option value="">Select</option>
                                            <option value="Accepted">Accepted</option>
                                            <option value="EF Processing">EF Processing</option>
                                            <option value="Pending">Pending</option>
                                            <option value="Rejected">Rejected</option>
                                        </select>


                                    </td>

                                    <td style="width:308px;">
                                        <label class="form-label">Note</label>
                                        <textarea class="form-control" name="federalsnote"></textarea>

                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td colspan="8" style="height:20px;"></td>
                                </tr>
                                <tr>
                                    <th><label class="form-label"> Resi. State</label></th>
                                    <th><label class="form-label">Form No.</label></th>
                                    <th><label class="form-label">Filling Method</label></th>
                                    <th><label class="form-label">Filling Date</label></th>
                                    <th><label class="form-label">Status of Filling</label></th>
                                    <th style="text-align:left!important;"><label class="form-label">Note</label></th>
                                    <th></th>
                                </tr>


                                <tr>
                                    <td style="width:105px;">

                                        <select class="form-control" name="statetax[]" id="statetax">
                                            <option>Select</option>
                                            <?php $__currentLoopData = $datastate; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $datastate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($datastate->code); ?>"><?php echo e($datastate->code); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>

                                    </td>
                                    <td style="width:105px;">

                                        <input type="text" readonly class="form-control" name="stateformno[]"
                                               id="statefederalformno" style="width:100px"/>

                                    </td>
                                    <td style="width:120px;">

                                        <select class="form-control filingmethod" name="statemethod[]">
                                            <option value="">Select</option>
                                            <option value="E-File">E-File</option>
                                            <option value="Paperfile">Paperfile</option>
                                        </select>

                                    </td>
                                    <td style="width:120px;">

                                        <input type="text" class="form-control fdate fdates" id="fillingdate"
                                               name="statedate[]" placeholder="MM/DD/YYYY"/>
                                        <input type="hidden" class="form-control" id="statefile" name="statefile"
                                               required/>
                                    </td>
                                    <td style="width:150px;">

                                        <select class="form-control" name="statestatus[]">
                                            <option value="">Select</option>
                                            <option value="Accepted">Accepted</option>
                                            <option value="EF Processing">EF Processing</option>
                                            <option value="Pending">Pending</option>
                                            <option value="Rejected">Rejected</option>
                                        </select>


                                    </td>

                                    <td style="width:308px;">

                                        <textarea class="form-control" name="statenote[]"></textarea>

                                    </td>
                                    <td><a href="#" class="btn btn-primary add-rowform"><i class="fa fa-plus"></i></a>
                                    </td>
                                </tr>
                            </table>


                            <div class="row form-group saves">
                                <label class="col-md-4 control-label">&nbsp;</label>
                                <div class="col-md-2">
                                    <input type="submit" class="btn_new_save primary" value="Save">
                                </div>
                                <div class="col-md-2">
                                    <a href="#" class="btn_new_cancel" data-dismiss="modal">Cancel</a>
                                </div>

                            </div>


                            <input type="hidden" name="client_taxation_id" value="<?php if (isset($common->id) != '') {
                                echo $common->id;
                            }?>">
                            <div class="row form-group">

                                <div class="col-md-4 federals" style="display:none;">

                                </div>

                                <div class="col-md-4 states" style="display:none;">
                                    <select class="form-control statesyear" name="statesyear">
                                        <option value="">Select</option>
                                        <?php
                                        $aa = date('Y') - 1;

                                        ?>
                                        <option value="<?php echo $aa;?>"><?php echo $aa;?></option>


                                    </select>
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-md-3 control-label labels" style="display:none;">Tax Return</label>
                                <div class="col-md-4 federals" style="display:none;">

                                </div>
                                <div class="col-md-4 states" style="display:none;">
                                    <select class="form-control taxation2" name="statestax">
                                        <option value="">Select</option>
                                        <option value="Extension">Extension</option>
                                        <option value="Original">Original</option>
                                        <option value="Amendment">Amendment</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-md-3 control-label labels" style="display:none;">Form No.</label>
                                <div class="col-md-4 federals" style="display:none;">

                                </div>

                                <div class="col-md-4 states" style="display:none;">
                                    <input type="text" class="form-control formno2" name="statesformno" readonly/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-md-3 control-label labels" style="display:none;">Due Date</label>
                                <div class="col-md-4 federals" style="display:none;">

                                </div>

                                <div class="col-md-4 states" style="display:none;">
                                    <input type="text" class="form-control duedate2" name="statesduedate"
                                           placeholder="Mar-15-2020" readonly/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-md-3 control-label labels" style="display:none;">Filing
                                    Method </label>
                                <div class="col-md-4 federals" style="display:none;">

                                </div>

                                <div class="col-md-4 states" style="display:none;">
                                    <select class="form-control filingmethod2" name="statesmethod">
                                        <option value="">Select</option>
                                        <option value="E-File">E-File</option>
                                        <option value="Paperfile">Paperfile</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-md-3 control-label labels" style="display:none;">Filing Date</label>
                                <div class="col-md-4 federals" style="display:none;">

                                </div>

                                <div class="col-md-4 states" style="display:none;">
                                    <input type="date" class="form-control" id="fillingdate2" name="statesdate"/>
                                </div>
                            </div>
                            <div class="row form-group statusof">
                                <label class="col-md-3 control-label labels" style="display:none;">Status of
                                    Filling </label>
                                <div class="col-md-4 federals" style="display:none;">

                                </div>

                                <div class="col-md-4 states" style="display:none;">
                                    <select class="form-control" name="statesstatus">
                                        <option value="">Select</option>
                                        <option value="Accepted">Accepted</option>
                                        <option value="Rejected">Rejected</option>
                                        <option value="Pending">Pending</option>
                                    </select>
                                </div>
                            </div>

                            <div class="row form-group">
                                <label class="col-md-3 control-label labels" style="display:none;">Note </label>
                                <div class="col-md-4 federals" style="display:none;">

                                </div>

                                <div class="col-md-4 states" style="display:none;">
                                    <textarea class="form-control" name="statesnote"></textarea>
                                </div>
                            </div>
                            <div class="row form-group saves" style="display:none;">
                                <label class="col-md-5 control-label">&nbsp;</label>
                                <div class="col-md-2">
                                    <input type="submit" class="btn_new_save primary" value="Save">
                                </div>
                                <div class="col-md-2">
                                    <a href="#" class="btn_new_cancel" data-dismiss="modal">Cancel</a>
                                </div>

                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <!--Modal Federal tax Update End-->



    <style>
        @media  screen {
            #printSection {
                display: none;
            }

        }

        @media  print {
            .close, .btn {
                display: none;
                font-size: 0px;
            }

            body * {
                visibility: hidden;
            }

            #printSection, #printSection * {
                visibility: visible;
            }

            #printSection {
                position: absolute;
                left: 0;
                top: 0;
            }
        }


    </style>


    <script>
        $('.selectvendor,.selectemployee,.selectclientid').select2();
        $('.selectvendor,.selectemployee,.selectclientid').on('select2:opening select2:closing', function (event) {
            var $searchfield = $(this).parent().find('.select2-search__field');
            $searchfield.prop('disabled', true);
        });

        $(function () {
            var notificationcount = 0;
            var count = $('.notification_i').length;
            for (var i = 0; i < count; i++) {
                notificationcount = notificationcount + parseInt($('#notification_i' + i).val());

            }

            // alert(notificationcount);
            $('.notify_count').text(notificationcount);
            $('.fdate').datepicker({
                autoclose: true
            });


        });
        $(document).ready(function () {

            var cnt = 1;

            $(".add-rowform").click(function () {
                cnt++;
                var fdatef = $('.fdatef').val();
                var fdates11 = $('.fdate' + cnt).val();
                //$('').show();
                //var aa=$('.statetax2').html();
                var markup = ' <tr><td><select class="form-control statetax' + cnt + '" name="statetax[]" id="statetax' + cnt + '"><option>Select</option> <?php $__currentLoopData = $datastate2; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $datastate3): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?><option value="<?php echo e($datastate3->code); ?>"><?php echo e($datastate3->code); ?></option><?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></select></td><td><input type="text" readonly class="form-control formno" name="stateformno[]" id="statefederalformno' + cnt + '" style="width:100px"/></td><td><select class="form-control filingmethod" name="statemethod[]"><option value="">Select</option><option value="E-File">E-File</option><option value="Paperfile">Paperfile</option></select></td><td><input type="text" class="form-control fdatess' + cnt + '" id="fillingdate" name="statedate[]" id="federaldate' + cnt + '" placeholder="MM/DD/YYYY"/></td><td><select class="form-control" name="statestatus[]" id="federalstatus' + cnt + '"><option value="">Select</option> <option value="Accepted">Accepted</option><option value="EF Processing">EF Processing</option><option value="Pending">Pending</option><option value="Rejected">Rejected</option></select></td><td><textarea class="form-control" name="statenote[]" id="federalnote' + cnt + '"></textarea></td><td><a href="#" class="btn btn-danger removetrrow"><i class="fa fa-minus"></i></a></td></tr>';
                $("#taxationtable tbody").append(markup);

                $(function () {
                    $('.fdate' + cnt).datepicker({
                        autoclose: true
                    });
                });

                $('.statetax' + cnt).on('change', function () {
                    var fdatef = $('.fdatef').val();
                    $('.fdatess' + cnt).val(fdatef);
                    var statetaxval = $('.statetax' + cnt).val();
                    $.get('<?php echo URL::to('getTaxstatedata'); ?>?statetaxval=' + statetaxval, function (data) {
                        if (data == '') {
                            $('#statefederalformno' + cnt).val();
                        } else {
                            $('#statefederalformno' + cnt).val(data.taxform);
                        }

                    });
                });

            });
            $("body").on("click", ".removetrrow", function () {
                $(this).parent().parent().remove();
            });

            $('#statetax').on('change', function () {
                var fdatef = $('.fdatef').val();
                $('.fdates').val(fdatef);
                var statetaxval = $("#statetax").val();
                $.get('<?php echo URL::to('getTaxstatedata'); ?>?statetaxval=' + statetaxval, function (data) {
                    //console.log(data);exit;
                    if (data == '') {
                        $("#statefederalformno").val();
                    } else {
                        $("#statefederalformno").val(data.taxform);
                    }

                });

            });


            $(".federalTaxation").click(function () {
                var federalid = $(this).attr('data-id');
                //alert(federalid);
                $("#federalid").val(federalid);
                $('#federalModals').modal('show');
                $.get('<?php echo URL::to('getClientfederaldata2'); ?>?federalid=' + federalid, function (data) {
                    //console.log(data);exit;
                    if (data == "") {

                    } else {
                        // var fullnames=data.first_name +' '+data.last_name;
                        //alert(data.filename);
                        //$('#id').val(data.id);
                        // $('#federalsyear').val(data.taxyears);
                        //  $('#clid').html(data.filename);
                        //$('.clname').val(fullnames);


                        if (data.business_id == '6') {
                            var fullnames = data.first_name + ' ' + data.last_name;
                        } else {
                            var fullnames = data.company_name;

                        }
//alert(data.statename);
//alert(data.taxyears);
                        //  $('#inputt').val(data.statename);
                        //$('#id').val(data.id);
                        // $('#federalsyear').val(data.taxyears);
                        alert(data.filename);
                        $('#clid').html(data.filename);
                        $('.clname').val(fullnames);
                        $('#statetax').val(data.statename);

                        $('#statefederalformno').val(data.taxform);


                        // $('#federalstax').val(data.federalstax);
                        //$('#federalsduedate').val(data.federalsduedate);
                        //$('#federalsduedate').val(date('d-m-Y',strtotime(data.federalsduedate)));
                        // $('#federalsform').val(data.federalsform);
                        // $('#federalsmethod').val(data.federalsmethod);
                        // $('.fillingdate').val(data.federalsdate);
                        // $('#federalsstatus').val(data.federalsstatus);
                        // $('#federalsfile').val(data.federalsfile);
                        // $('#federalsfile_1').html(data.federalsfile);
                        // $('#federalsnote').val(data.federalsnote);


                        var this1 = $('#federalsyear').val();
                        var aa = '<?php echo $aa = date('Y') - 1;?>';

                        if (this1 == aa) {
                            $(".duedate").val('Jul-15-<?php echo $aa + 1;?>');

                        }

                    }


                });
            });


            $('.federalyear').change(function () {

                var this1 = $(this).val();
                var aa = '<?php echo $aa = date('Y') - 1;?>';

                if (this1 == aa) {
                    $(".duedate").val('Jul-15-<?php echo $aa + 1;?>');

                }


                //  if(this1 == 'Extension')
                //  {
                //      $("select option[value='Extension']").hide();
                //      $(".duedate").val('Mar-15-<?php echo $aa + 1;?>');

                //  }
                // else if(this1 == bb)
                //  {
                //      $("select option[value='Extension']").hide();
                //      $(".duedate").val('Mar-15-<?php echo $aa + 1;?>');
                //  }
                //  else if(this1 == cc)
                //  {
                //      $("select option[value='Extension']").show();
                //      $(".duedate").val('Mar-15-<?php echo $aa + 1;?>');
                //  }

            });


            $('.statesyear').change(function () {

                var this1 = $(this).val();
                //alert(this1);
                var aa = '<?php echo $aa = date('Y');?>';
                var bb = '<?php echo $bb = date('Y') + 1;?>';
                var cc = '<?php echo $cc = date('Y') + 2;?>';
                if (this1 == aa) {
                    $("select option[value='Extension']").hide();
                    $(".duedate2").val('Jul-15-<?php echo $aa + 1;?>');

                } else if (this1 == bb) {
                    $("select option[value='Extension']").hide();
                    $(".duedate2").val('Jul-15-<?php echo $bb + 1;?>');
                } else if (this1 == cc) {
                    $("select option[value='Extension']").show();
                    $(".duedate2").val('Jul-15-<?php echo $cc + 1;?>');
                }

            });


            $('.taxation').change(function () {
                var businessid = '<?php if (isset($_REQUEST['id']) && $_REQUEST['id'] != '') {
                    echo $common->business_id;
                }?>';

                var thiss = $(this).val();
                if (thiss == 'Extension') {
                    if (businessid == '6') {
                        $('.formno').val('Form-4868');
                    } else {
                        $('.formno').val('Form-7004');
                    }

                    $("select option[value='E-File']").show();
                } else if (thiss == 'Original') {
                    $('.formno').val('Form-1040');
                    $("select option[value='E-File']").show();
                } else if (thiss == 'Amendment') {
                    $('.formno').val('Form-1040X');
                    $("select option[value='E-File']").hide();

                }
            });
            $(".fdates").blur(function () {
                var fdatef = $('.fdatef').val();

                var thiss = $(this).val();
                alert(thiss);
                if (thiss) {

                } else {
                    $('.fdates').val(fdatef);
                }

            });

            $('.taxation2').change(function () {
                var businessid = '<?php if (isset($_REQUEST['id']) && $_REQUEST['id'] != '') {
                    echo $common->business_id;
                }?>';
                var thiss = $(this).val();
                if (thiss == 'Extension') {
                    if (businessid == '6') {
                        $('.formno').val('Form-4868');
                    } else {
                        $('.formno').val('Form-7004');
                    }
                    $("select option[value='E-File']").show();
                } else if (thiss == 'Original') {
                    $('.formno2').val('Form-1040');
                    $("select option[value='E-File']").show();
                } else if (thiss == 'Amendment') {
                    $('.formno2').val('Form-1040X');
                    $("select option[value='E-File']").hide();

                }
            });


            $('.filingmethod').change(function () {
                var thiss = $(this).val();
                if (thiss == 'Paperfile') {
                    $('.statusof').hide();
                } else {
                    $('.statusof').show();
                }
            });

            $('.filingmethod2').change(function () {
                var thiss = $(this).val();
                if (thiss == 'Paperfile') {
                    $('.statusof2').hide();
                } else {
                    $('.statusof2').show();
                }
            });


            $('#notetype_user').on('change', function () {
                //otherid,clientid,employeeuserid,vendorid
                //Client,EE-User,Vendor,Other
                var thisss = $('#notetype_user').val();
                if (thisss == 'Client') {
                    $('#noteclientid').show();
                    $('#noteemployeeuserid').hide();
                    $('#notevendorid').hide();
                    $('#noteotherid').hide();
                } else if (thisss == 'EE-User') {
                    $('#noteclientid').hide();
                    $('#noteemployeeuserid').show();
                    $('#notevendorid').hide();
                    $('#noteotherid').hide();
                } else if (thisss == 'Vendor') {
                    $('#noteclientid').hide();
                    $('#noteemployeeuserid').hide();
                    $('#notevendorid').show();
                    $('#noteotherid').hide();
                } else if (thisss == 'Other') {
                    $('#noteclientid').hide();
                    $('#noteemployeeuserid').hide();
                    $('#notevendorid').hide();
                    $('#noteotherid').show();
                }
            });

            $('#type_user').on('change', function () {
                //otherid,clientid,employeeuserid,vendorid
                //Client,EE-User,Vendor,Other
                var thisss = $('#type_user').val();
                if (thisss == 'Client') {
                    $('#clientid').show();
                    $('#employeeuserid').hide();
                    $('#vendorid').hide();
                    $('#otherid').hide();
                } else if (thisss == 'EE-User') {
                    $('#clientid').hide();
                    $('#employeeuserid').show();
                    $('#vendorid').hide();
                    $('#otherid').hide();
                } else if (thisss == 'Vendor') {
                    $('#clientid').hide();
                    $('#employeeuserid').hide();
                    $('#vendorid').show();
                    $('#otherid').hide();
                } else if (thisss == 'Other') {
                    $('#clientid').hide();
                    $('#employeeuserid').hide();
                    $('#vendorid').hide();
                    $('#otherid').show();
                }
            });
        });
    </script>
    <script>


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        $(function () {

            //Ankit model closing
            $('#myModalcustomershseet').on('hidden.bs.modal', function () {
                $(this).find('form')[0].reset();
            });

            $('#myModalnotes').on('hidden.bs.modal', function () {
                $(this).find('form')[0].reset();
            });


            $('#notesrelated').click(function () { //alert();
                var newopt2 = $('#newnotesrelated').val();

                if (newopt2 == '') {
                    alert('Please enter somethings!');
                    return;
                }

                //check if the option value is already in the select box
                $('#vendor_product option').each(function (index) {
                    if ($(this).val() == newopt2) {
                        alert('Duplicate option, Please enter new!');
                    }
                })
                $.ajax({
                    type: "post",
                    url: "<?php echo route('notesrelated.notesrelateds'); ?>",
                    dataType: "json",
                    data: $('#ajax2').serialize(),
                    success: function (data) {
                        $('#notesrelatedname').append('<option value=' + newopt2 + '>' + newopt2 + '</option>');
                        $('#notesRelatedNameForDelete').append('<div id="cur_' + data["id"] + '" class="col-md-12" style="border:1px solid;background:#def9ff;">' +
                            '<div class="col-md-12" style="margin-top: 7px;margin-bottom:5px;">' +
                            '<a class="deletenotes" style="color:#000;" id="' + data["id"] + '">' + data["newopt2"] +
                            '<span class="pull-right"><i class="fa fa-trash" style="padding: 1px 6px!important;"></i></span>' +
                            '</a></div></div>');
                        $('#basicExampleModalNotes').modal('hide');
                        $("#newnotesrelated").val('');
                    },
                    error: function (data) {
                        alert("Error")
                    }
                });

                //$('#basicExampleModalposition').modal('hide');
            });


        });


        $(function () {
            $('#addrelatedname').click(function () { //alert();
                var newopt = $('#newrelatednames').val();

                if (newopt == '') {
                    alert('Please enter somethings!');
                    return;
                }

                //check if the option value is already in the select box
                $('#vendor_product option').each(function (index) {
                    if ($(this).val() == newopt) {
                        alert('Duplicate option, Please enter new!');
                    }
                })
                $.ajax({
                    type: "post",
                    url: "<?php echo route('relatedname.relatednames'); ?>",
                    dataType: "json",
                    data: $('#ajax1').serialize(),
                    success: function (data) {

                        $('#conrelatedname').append('<option value=' + newopt + '>' + newopt + '</option>');
                        $('#conversationRelatedNameForDelete').append('<div id="cur_' + data["id"] + '" class="col-md-12" style="border:1px solid;background:#def9ff;">' +
                            '<div class="col-md-12" style="margin-top: 7px;margin-bottom:5px;">' +
                            '<a class="deleterelated" style="color:#000;" id="' + data["id"] + '">' + data["newopt"] +
                            '<span class="pull-right"><i class="fa fa-trash" style="padding: 1px 6px!important;"></i></span>' +
                            '</a></div></div>');
                        $('#basicExampleModal').modal('hide');
                        //$("#div").load(" #div > *");
                        $("#newrelatednames").val('');
                    },
                    error: function (data) {
                        alert("Error")
                    }
                });

                //$('#basicExampleModalposition').modal('hide');
            });


            $(document).on('click', '.deleterelated', function () {
                var id = $(this).attr('id');//alert();
                if (confirm("Are you sure you want to Delete this data?")) {
                    $.ajax({
                        url: "<?php echo e(route('removerelated.removerelated1')); ?>",
                        mehtod: "get",
                        data: {id: id},
                        success: function (data) {
                            var json = JSON.parse(data);
                            //console.log(json);

                            $('#conrelatedname').empty();
                            $('#conversationRelatedNameForDelete').empty();
                            $('#conrelatedname').append('<option value="">Select</option>');
                            for (var i = 0; i < json.length; i++) {
                                console.log("name : " + json[i]['newopt']);
                                $('#conrelatedname').append('<option value=' + json[i]['newopt'] + '>' + json[i]['newopt'] + '</option>');
                                $('#conversationRelatedNameForDelete').append('<div id="cur_' + json[i]['id'] + '" class="col-md-12" style="border:1px solid;background:#def9ff;">' +
                                    '<div class="col-md-12" style="margin-top: 7px;margin-bottom:5px;">' +
                                    '<a class="deleterelated" style="color:#000;" id="' + json[i]['id'] + '">' + json[i]['newopt'] +
                                    '<span class="pull-right"><i class="fa fa-trash" style="padding: 1px 6px!important;"></i></span>' +
                                    '</a></div></div>');
                            }
                            $('#basicExampleModal3').modal('hide');
                        }
                    })
                } else {
                    return false;
                }
            });

            $(document).on('click', '.deletenotes', function () {
                var id = $(this).attr('id');//alert();
                if (confirm("Are you sure you want to Delete this data?")) {
                    $.ajax({
                        url: "<?php echo e(route('removenote.removenotes1')); ?>",
                        mehtod: "get",
                        data: {id: id},
                        success: function (data) {
                            var json = JSON.parse(data);
                            $('#notesrelatedname').empty();
                            $('#notesRelatedNameForDelete').empty();
                            $('#notesrelatedname').append('<option value="">Select</option>');
                            for (var i = 0; i < json.length; i++) {
                                console.log("name : " + json[i]['newopt']);
                                $('#notesrelatedname').append('<option value=' + json[i]['newopt'] + '>' + json[i]['newopt'] + '</option>');
                                $('#notesRelatedNameForDelete').append('<div id="cur_' + json[i]['id'] + '" class="col-md-12" style="border:1px solid;background:#def9ff;">' +
                                    '<div class="col-md-12" style="margin-top: 7px;margin-bottom:5px;">' +
                                    '<a class="deletenotes" style="color:#000;" id="' + json[i]['id'] + '">' + json[i]['newopt'] +
                                    '<span class="pull-right"><i class="fa fa-trash" style="padding: 1px 6px!important;"></i></span>' +
                                    '</a></div></div>');
                            }
                            $('#basicExampleModal4').modal('hide');
                        }
                    })
                } else {
                    return false;
                }
            });


        });

        function printdivs(val) {
            //alert(val);
            //var values=alert('printThis_'+val+'');
            printElement(document.getElementById('printThis_' + val + ''));
        }

        function printdivsAll() {
            //alert(val);
            //var values=alert('printThis_'+val+'');
            printElementAll(document.getElementById('printAllThis'));
        }

        //document.getElementsByClassName('btnPrint').onclick = function () {
        // document.getElementById("btnPrint").onclick = function () {

        //}
        function printElementAll(elem) {
            //  alert(elem);
            var domClone = elem.cloneNode(true);

            var $printSection = document.getElementById("printSection");

            if (!$printSection) {
                var $printSection = document.createElement("div");
                $printSection.id = "printSection";
                document.body.appendChild($printSection);
            }

            $printSection.innerHTML = "";
            $printSection.appendChild(domClone);
            window.print();
        }

        function printElement(elem) {
            // alert(elem);
            var domClone = elem.cloneNode(true);

            var $printSection = document.getElementById("printSection");

            if (!$printSection) {
                var $printSection = document.createElement("div");
                $printSection.id = "printSection";
                document.body.appendChild($printSection);
            }

            $printSection.innerHTML = "";
            $printSection.appendChild(domClone);
            window.print();
        }
    </script>
    <script>

    </script>
    <script>
        function myfun(id, type) {

            var _token = $('input[name="_token"]').val();

            $.ajax({
                url: "<?php echo e(route('admin.fetch1')); ?>",
                method: "POST",
                data: {'id': id, 'type': type, _token: _token},

                success: function (data) {
                    //alert('11');
                    //   $('.modal-body').html(data);
                    $('#newmyModal').modal('show');
                    // $('#newmyModal').modal('show'); 

                    //   alert("hello");
                    //JSON.parse(data);
                    //   $('.newmyModal').show();


                    var datashow = JSON.parse(data);
                    //     alert(datashow.Type);          
                    var entity = datashow.entityid;
                    var fname = datashow.fname;

                    var mname = datashow.mname;
                    var lname = datashow.lname;

                    var telephoneNo2 = datashow.telephoneNo2;
                    var telephoneNo2Type = datashow.telephoneNo2Type;
                    var fscemail = datashow.fscemail;


                    var firstext = datashow.ext1;
                    var secondext = datashow.ext2;


                    if (fname == null) {
                        var fnamess = '';
                    } else {
                        var fnamess = fname;
                    }


                    if (mname == null) {
                        var mnamess = '';
                    } else {
                        var mnamess = mname;
                    }

                    if (lname == null) {
                        var lnamess = '';
                    } else {
                        var lnamess = lname;
                    }


                    if (datashow.Type == 'employee' || datashow.Type == 'user' || datashow.Type == 'clientemployee' || datashow.Type == 'Vendor') {
                        var usertype = datashow.Type;

                    } else {
                        var usertype = 'Client';
                    }
                    // alert(usertype);
                    if (usertype == 'Client') {
                        $('#names').show();
                        $('.names1').hide();

                        if (datashow.business_id == '6') {
                            var headings = fnamess + ' ' + mnamess + ' ' + lnamess;
                        } else {

                            var headings = datashow.company_name + ' (' + datashow.bsname + ')';
                        }
                    } else if (datashow.Type == 'Vendor') {
                        var headings = datashow.company_name + ' (' + datashow.bsname + ')';
                        $('#names').hide();
                        $('.names1').show();

                        $('.names1').val(datashow.company_name);
                    } else if (datashow.Type == 'employee' || datashow.Type == 'user' || datashow.Type == 'clientemployee' || datashow.Type == 'bank') {
                        $('#names').show();
                        $('.names1').hide();

                        //  alert(datashow.fname);
                        if (fname == null) {
                            var fnamess = '';
                        } else {
                            var fnamess = fname;
                        }


                        if (mname == null) {
                            var mnamess = '';
                        } else {
                            var mnamess = mname;
                        }

                        if (lname == null) {
                            var lnamess = '';
                        } else {
                            var lnamess = lname;
                        }

                        if (datashow.Type == 'bank') {
                            var headings = datashow.cname;

                        } else {
                            var headings = fnamess + ' ' + mnamess + ' ' + lnamess;
                        }
                    }


                    if (usertype == 'Client') {
                        $('.companyphone').show();
                        $('.companyemail').show();

                        $('.personalphone').hide();
                        $('.personalemail').hide();


                        $('.ahref').html('<a href="https://financialservicecenter.net/fac-Bhavesh-0554/customer/' + datashow.cid + '/edit" class="btn btn-primary text-center">FSC Client</a>');
                        $('.ahref1').html('<a href="https://financialservicecenter.net/fac-Bhavesh-0554/clientsetup/' + datashow.cid + '/edit" class="btn btn-warning text-center">Client Mgmt</a>');


                    } else if (usertype == 'employee' || usertype == 'user' || usertype == 'clientemployee') {

                        $('.companyphone').hide();
                        $('.companyemail').hide();

                        $('.personalphone').show();
                        $('.personalemail').show();

                        $('.ahref').html('<a href="https://financialservicecenter.net/fac-Bhavesh-0554/employee/' + datashow.cid + '/edit" class="btn btn-primary">Go</a>');


                    } else if (usertype == 'Vendor') {

                        $('.companyphone').hide();
                        $('.companyemail').hide();

                        $('.personalphone').show();
                        $('.personalemail').show();

                        $('.ahref').html('<a href="https://financialservicecenter.net/fac-Bhavesh-0554/vendor/' + datashow.cid + '/edit" class="btn btn-primary">Go</a>');
                    } else if (usertype == 'bank') {

                        $('.companyphone').hide();
                        $('.companyemail').hide();

                        $('.personalphone').show();
                        $('.personalemail').show();

                        $('.ahref').html('<a href="https://financialservicecenter.net/fac-Bhavesh-0554/bankingsetup/' + datashow.cid + '/edit" class="btn btn-primary">Go</a>');
                    }

                    if (fname == null) {
                        var fnames = ' ';
                    } else {
                        var fnames = fname;
                    }

                    if (mname == null) {
                        var mnames = ' ';
                    } else {
                        var mnames = mname;
                    }
                    if (lname == null) {
                        var lnames = '';
                    } else {
                        var lnames = lname;
                    }
                    if (datashow.Type == 'bank') {
                        // alert(datashow.Type);
                        var fullname = datashow.cname;
                        $('.hideclients').hide();
                        var telephone = datashow.telephone;


                    } else if (datashow.business_id != '6') {
                        $('.hideclients').show();
                        var fullname = datashow.firstname + ' ' + datashow.lastname;
                        var telephone = datashow.telephone;
                        var telephonetype = datashow.telephonetype;
                        var telephonetype2 = datashow.telephonetype2;


                        var telephone2 = datashow.telephone2;


                    } else {
                        $('.hideclients').show();
                        var fullname = fnames + ' ' + mnames + ' ' + lnames;
                        var telephone = datashow.telephone;
                        var telephone2 = datashow.telephone2;
                        var telephonetype = datashow.telephonetype;
                        var telephonetype2 = datashow.telephonetype2;


                    }
                    var email = datashow.email;
                    $('#names').val(fullname);

                    $('#entityids').val(entity);
                    $('.emails').val(email);
                    $('.telephones').val(telephone);
                    $('#telephones2').val(telephone2);
                    $('.teltype').val(telephonetype);
                    $('#teltype2').val(telephonetype2);
                    $('#headings').html(headings);
                    $('.exts').val(firstext);
                    $('.extss').val(secondext);
                    $('.telephonesNo2').val(telephoneNo2);
                    $('.telephonesNo2Type').val(telephoneNo2Type);
                    $('.fscemail').val(fscemail);


                    $('#newmyModal').modal('show');

                    //var v = document.getElementById("newmyModal"); 
                    //v.classList.add("in"); 
                    //  $('#countryList').html(data);


                }
            });


        }

        $('.close').click(function () {
            // alert();
            $('#newmyModal').removeClass('in');
        })

        $('.modal-footer .btn-default').click(function () {
            //  alert();
            $('#newmyModal').removeClass('in');
        })

    </script>
    <script>
        $(document).ready(function () {
            $('.btnpopclose').click(function () {
                // alert();
                $('.modal').removeClass('in');
                $('.modal').hide();
            })

            $('#country_name').keyup(function () {
                var query = $(this).val();
                //alert(query);
                if (query != '') {
                    var _token = $('input[name="_token"]').val();
                    $.ajax({
                        url: "<?php echo e(route('admin.fetch')); ?>",
                        method: "POST",
                        data: {query: query, _token: _token},
                        success: function (data) {
                            $('#countryList').fadeIn();
                            $('#countryList').html(data);


                        }
                    });
                }
            });

            $(document).on('click', '.delete', function () {
                var id = $(this).attr('id');//alert();
                if (confirm("Are you sure you want to Delete this data?")) {
                    $.ajax({
                        url: "<?php echo e(route('removemsg.removemsg1')); ?>",
                        mehtod: "get",
                        data: {id: id},
                        success: function (data) {
                            location.reload(true);
                        }
                    })
                } else {
                    return false;
                }
            });


        });

        $('.accordion__answer:first').show();
        $('.accordion__question:first').addClass('expanded');

        $('.accordion__question').on('click', function () {
            var content = $(this).next();

            $('.accordion__answer').not(content).slideUp(400);
            $('.accordion__question').not(this).removeClass('expanded');
            $(this).toggleClass('expanded');
            content.slideToggle(400);
        });
    </script>


    <script>


        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            $('body').removeClass('skin-blue');
            $('body').addClass('whitefont');
            document.body.innerHTML = printContents;
            window.print();

            document.body.innerHTML = originalContents;
            $('body').addClass('skin-blue');
            $('body').removeClass('whitefont');

        }
    </script>


    <script>
        function showbox() {
            var t1 = $('#related').val();

            if (t1 == 'accounting') {
                $('#accounting').show();
                $('#taxation').hide();
                $('#taxtissue').hide();
                $('#payroll').hide();
                $('#corporation').hide();
                $('#residmtg').hide();
                $('#commtg').hide();

            } else if (t1 == 'taxtissue') {
                $('#accounting').hide();
                $('#taxation').hide();
                $('#taxtissue').show();
                $('#payroll').hide();
                $('#corporation').hide();
                $('#residmtg').hide();
                $('#commtg').hide();
            } else if (t1 == 'taxation') {
                $('#accounting').hide();
                $('#taxation').show();
                $('#taxtissue').hide();
                $('#payroll').hide();
                $('#corporation').hide();
                $('#residmtg').hide();
                $('#commtg').hide();
            } else if (t1 == 'payroll') {
                $('#accounting').hide();
                $('#taxation').hide();
                $('#taxtissue').hide();
                $('#payroll').show();
                $('#corporation').hide();
                $('#residmtg').hide();
                $('#commtg').hide();
            } else if (t1 == 'corporation') {
                $('#accounting').hide();
                $('#taxation').hide();
                $('#taxtissue').hide();
                $('#payroll').hide();
                $('#corporation').show();
                $('#residmtg').hide();
                $('#commtg').hide();
            } else if (t1 == 'residmtg') {
                $('#accounting').hide();
                $('#taxation').hide();
                $('#taxtissue').hide();
                $('#payroll').hide();
                $('#corporation').hide();
                $('#residmtg').show();
                $('#commtg').hide();
            } else if (t1 == 'commtg') {
                $('#accounting').hide();
                $('#taxation').hide();
                $('#taxtissue').hide();
                $('#payroll').hide();
                $('#corporation').hide();
                $('#residmtg').hide();
                $('#commtg').show();
            } else {

            }

        }

        $(document).ready(function () {

            $(".effective_date2").datepicker({
                autoclose: true,
                format: "mm/dd/yyyy",
                //endDate: "today"
            });
        });


        //  

    </script>



    <div id="newmyModalss" class="modal fade " role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="background:#ffff99;border-bottom:5px solid green !important;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="headings"></h4>
                </div>
                <div class="modal-body">
                    testtt
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            var table = $('#collapsew_6 #example').DataTable({
                dom: 'Bfrtlip',
                "columnDefs": [{
                    "searchable": true,
                    "orderable": true,
                    "autoWidth": false,
                    "targets": 0
                }],
                "order": [[0, 'asc']],

            });

        });
    </script>

    <script>

        var serviceJsonData;

        var fsc_fee_val;

        function currencyFormat(num) {
            var sign53 = parseFloat(Math.round(num * 100) / 100).toFixed(2);
            var no122 = sign53.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return no122;
        }


        // $('.custom_radio').click(function(e){
        //   val = $('input[name="preview_types"]:checked').val(); 
        //   if(val=="invoice"){
        //       $('#preview_types').attr('data-target','#preview_invoice');
        //   }else if(val=="agreement"){
        //       $('#preview_types').attr('data-target','#preview_agreement');
        //   }
        // });


        $('input[type=radio][name=preview_types]').change(function () {
            alert(this.value);
            if (this.value == 'invoice') {
                $('#preview_type').attr('data-target', '#preview_invoice');
            } else if (this.value == 'agreement') {
                $('#preview_type').attr('data-target', '#preview_agreement');
            }
        });


        $('#Error').hide();
        $('#duedate_div').hide();
        $('#div_taxtation_type').hide();
        $('#div_period_val').hide();
        $('#period').hide();

        $('#proposal_duedate').datepicker();
        $('#praposal_client_telephone').mask('(999)-999-9999');
        $('#praposal_client_zip').mask('99999');
        $('#priority').on('change', function () {

            var selectedItem = $('#priority').find(":selected").val();
            //alert(selectedItem);
            if (selectedItem == "Regular") {
                $('#proposal_duedate').removeAttr("required");
                $('#duedate_div').hide();
            } else if (selectedItem == "Time Sensitive" || selectedItem == "Urgent") {
                $('#proposal_duedate').attr("required", "required");
                $('#duedate_div').show();
            }

        });

        $('#proposal_duedate').blur(function () {
            $('#lbl_due_date').text($('#proposal_duedate').val());
        })


        $("input[name='client']").change(function () {
            var option1 = $('input[name="client"]:checked').val();
        });

        $('#search').show();
        $('#new_client').on('click', function () {

            $('#lbl_id_change').text('Prospect ID :');
            $('#praposal_client_name').removeAttr('readonly');
            $('#praposal_client_telephone').removeAttr('readonly');
            $('#praposal_client_email').removeAttr('readonly');

            $('#praposal_client_address').removeAttr('readonly');
            $('#praposal_client_city').removeAttr('readonly');
            $('#praposal_client_state').removeAttr('readonly');
            $('#praposal_client_zip').removeAttr('readonly');


            $('#praposal_client_name').val('');
            $('#praposal_client_telephone').val('');
            $('#praposal_client_email').val('');

            $('#praposal_client_address').val('');
            $('#praposal_client_city').val('');
            $('#praposal_client_state').val('');
            $('#praposal_client_zip').val('');


            $('#search_client_name').val("");
            $('#search').hide();

            $.get('<?php echo URL::to('fac-Bhavesh-0554/prospect/getpid'); ?>', function (data) {
                $('#proposal_client_id').val(data);
            });


        });

        $('#already_client').on('click', function () {
            $('#lbl_id_change').text('Client ID :');
            $('#proposal_client_id').val('');
            $('#praposal_client_name').attr('readonly', 'readonly');
            $('#praposal_client_telephone').attr('readonly', 'readonly');
            $('#praposal_client_email').attr('readonly', 'readonly');

            $('#praposal_client_address').attr('readonly', 'readonly');
            $('#praposal_client_city').attr('readonly', 'readonly');
            $('#praposal_client_state').attr('readonly', 'readonly');
            $('#praposal_client_zip').attr('readonly', 'readonly');

            $('#search').show();
        });


        $('#search_client_name').keyup(function () {
            var query = $(this).val();
            //alert(query);
            if (query != '') {
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url: "<?php echo e(route('admin.praposal_fetch')); ?>",
                    method: "POST",
                    data: {query: query, _token: _token},
                    success: function (data) {
                        $('#clientList').fadeIn();
                        $('#clientList').html(data);
                    }
                });
            } else {
                $('#clientList').empty();
            }
        });

        $("#fsc_fee").keypress(function (e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && this.value.split('.').length === 2) {
                //$("#errmsg").html("Digits Only").show().fadeOut("slow");
                return false;
            }
        });

        $("#adv_payment").keypress(function (e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && this.value.split('.').length === 2) {
                //$("#errmsg").html("Digits Only").show().fadeOut("slow");
                return false;
            }

        });
        $("#adv_payment").focus(function (e) {
            var val1 = $(this).val();
            if (val1.split('.').length === 2) {
                decimal = val1;
                $("#adv_payment").val(val1);
            } else {
                $("#adv_payment").val($("#adv_payment").val().substring(0, $("#adv_payment").val().length - 3));
            }
        });

        $("#discount").focus(function (e) {
            var val1 = $(this).val();
            if (val1.split('.').length === 2) {
                decimal = val1;
                $("#discount").val(val1);
            } else {
                $("#discount").val($("#discount").val().substring(0, $("#discount").val().length - 3));
            }
        });

        // $("#discount").click(function (e) {
        //     $("#discount").val();
        // });

        // $("#adv_payment").click(function (e) {
        //     $("#adv_payment").val();
        // });

        $("#fsc_fee").focus(function (e) {

            var val1 = $(this).val();
            if (val1.split('.').length === 2) {
                decimal = val1;
                $("#fsc_fee").val(val1);
            } else {
                $("#fsc_fee").val($("#fsc_fee").val().substring(0, $("#fsc_fee").val().length - 3));
            }
        });

        $("#discount").keypress(function (e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && this.value.split('.').length === 2) {
                //$("#errmsg").html("Digits Only").show().fadeOut("slow");
                return false;
            }
        });


        $("#fsc_fee").blur(function (e) {
            var val1 = $(this).val();
            var val3 = $("#discount").val();
            if ($("#discount").val().length == 3 || $("#discount").val().length == 0) {
                $('#btn_submit').show();
            }
            var decimal2 = '';
            if (val3.split('.').length === 2) {
                decimal2 = val3;
            } else {
                if (val3.length > 0) {
                    decimal2 = val3 + ".00";
                } else {
                    decimal2 = '';
                }

            }

            var decimal1 = '';

            var result = 0.00;
            if (val1.split('.').length === 2) {
                decimal1 = val1;

            } else {
                if (val1.length > 0) {
                    decimal1 = val1 + ".00";
                } else {
                    decimal1 = '';
                }
            }
            //console.log("Decimal Val F 1 :=> " +decimal1+ "  v3 : " +val3);
            //console.log("Decimal Val F 2 :=> " +decimal2+ "  v1 : " +val1);
            if (val1 >= val3) {
                var result = decimal1 - decimal2;
                $("#net_amount").val(result);
                $('#btn_submit').show();
            } else {
                $('#btn_submit').hide();
            }

            $("#fsc_fee").val(decimal1);
            $("#lbl_amount").text($("#fsc_fee").val());
            $("#lbl_preview_amount").text($("#fsc_fee").val());
        });

        $("#discount").blur(function (e) {
            var val1 = $(this).val();
            var val3 = $("#fsc_fee").val();
            if ($("#net_amount").val().length == 3 || $("#net_amount").val().length == 0) {
                $('#btn_submit').hide();
            }
            var decimal2 = '';
            if (val3.split('.').length === 2) {
                decimal2 = val3;
            } else {
                if (val3.length > 0) {
                    decimal2 = val3 + ".00";
                } else {
                    decimal2 = '';
                }

            }

            var decimal1 = '';

            var result = 0.00;
            if (val1.split('.').length === 2) {
                decimal1 = val1;

            } else {
                if (val1.length > 0) {
                    decimal1 = val1 + ".00";
                } else {
                    decimal1 = '';
                }

            }

            if (Math.round(decimal2 * 100) >= Math.round(decimal1 * 100)) {
                var result = decimal2 - decimal1;
                result = result.toFixed(2);
                $("#net_amount").val(result);
                console.log("Decimal Val 1 :=> " + decimal1 + "  v1 : " + val1);
                console.log("Decimal Val 2 :=> " + decimal2 + "  v3 : " + val3);
                $('#btn_submit').show();
            } else {
                $('#btn_submit').hide();
            }
            $("#discount").val(decimal1);
            //$("#lbl_preview_bal_amount").text(rel+".00");
            $('#lbl_preview_discountbox').text(decimal1);

        });


        $("#adv_payment").blur(function (e) {

            //$("#adv_payment").val('');
            var val1 = $(this).val();
            var decimal1 = '';
            var val3 = $("#net_amount").val();
            var decimal2 = '';
            if (val3.split('.').length === 2) {
                decimal2 = val3;
            } else {
                if (val3.length > 0) {
                    decimal2 = val3 + ".00";
                } else {
                    decimal2 = '';
                }
            }
            if (val1.split('.').length === 2) {
                decimal1 = val1;

            } else {
                if (val1.length > 0) {
                    decimal1 = val1 + ".00";
                }
            }
            var result = decimal2 - decimal1;
            $("#total_prop_dueprice").text(currencyFormat(result.toFixed(2)));
            $("#adv_payment").val(decimal1);
            $("#total_prop_advprice").text(decimal1);

        });


        $("#praposal_client_name").blur(function (e) {
            $(".lbl_preview_client_name").text($("#praposal_client_name").val());
            $("#lbl_client_name").text($("#praposal_client_name").val());
        });

        $("#praposal_client_telephone").blur(function (e) {
            $("#lbl_telephone").text($("#praposal_client_telephone").val());
        });

        $("#praposal_client_email").blur(function (e) {
            $("#lbl_email").text($("#praposal_client_email").val());
        });

        $("#praposal_client_address").blur(function (e) {
            $('.lbl_preview_address').text($('#praposal_client_address').val());
        });

        $("#praposal_client_city").blur(function (e) {
            $('.lbl_preview_city').text($('#praposal_client_city').val());
        });

        $("#praposal_client_state").blur(function (e) {
            $('.lbl_preview_state').text($('#praposal_client_state').val());
        });

        $("#praposal_client_fax").blur(function (e) {
            $('.lbl_preview_fax').text($("#praposal_client_fax").val());
        });

        $("#priority").on('change', function (e) {
            var selected = $("#priority").find(':selected').val();
            $("#lbl_priority").text(selected);
        });

        // $("#fsc_fee").blur(function (e) {
        //     $("#fsc_fee").val("$"+$(this).val()+".00");
        //     $("#lbl_amount").text($("#fsc_fee").val());
        // });


        function getClientdetailForPraposal(id) {
            //var query = $(this).val();
            //alert(id);
            if (id != '') {
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url: "<?php echo e(route('admin.getDetailPraposal')); ?>",
                    method: "POST",
                    data: {query: id, _token: _token},
                    success: function (data) {
                        var json = JSON.parse(data);
                        //console.log(data);
                        $("#lbl_title").empty();
                        for (var i = 0; i < json.length; i++) {
                            $('#praposal_client_name').val(json[i]['name']);
                            $('#praposal_client_telephone').val(json[i]['telephone']);
                            $('#praposal_client_email').val(json[i]['email']);
                            $('#praposal_client_address').val(json[i]['address']);

                            $('#proposal_client_id').val(json[i]['filename']);
                            $('#praposal_client_city').val(json[i]['city']);
                            $('#praposal_client_state').val(json[i]['state']);
                            $('#praposal_client_zip').val(json[i]['zip']);

                            $('.lbl_preview_client_name').text($('#praposal_client_name').val());
                            $('.lbl_preview_address').text($('#praposal_client_address').val());
                            $('.lbl_preview_city').text($('#praposal_client_city').val());
                            $('.lbl_preview_state').text($('#praposal_client_state').val());
                            $('.lbl_preview_fax').text(json[i]['fax']);
                            $('.lbl_preview_zip').text(json[i]['zip']);
                            $('.lbl_preview_client_id').text(json[i]['filename']);

                            $('.lbl_preview_city_state_zip').text(json[i]['city'] + ", " + json[i]['state'] + ", " + json[i]['zip'])


                            $(".lbl_preview_client").text($("#praposal_client_name").val());
                            $("#lbl_client_name").text($("#praposal_client_name").val());
                            $("#lbl_telephone").text($("#praposal_client_telephone").val());
                            $("#lbl_email").text($("#praposal_client_email").val());
                        }
                        $('#clientList').empty();
                    }
                });
            }
        }


        // $("#proposal_to_client").bootstrapValidator({
        //     rules: {
        //         praposal_client_name: {
        //             required: true
        //         },
        //         praposal_client_telephone: {
        //             required: true
        //         },
        //         praposal_client_email: {
        //             required: true
        //         },
        //         prospect_service: {
        //             required: true
        //         },
        //         priority: {
        //             required: true
        //         },
        //         fsc_fee: {
        //             required: true
        //         }

        //     },
        //     messages: {
        //         praposal_client_name: {
        //             required: "Please Enter Client Name"
        //         },
        //         praposal_client_telephone: {
        //             required: "Please Enter Client Telephone"
        //         },
        //         praposal_client_email: {
        //             required: "Please Enter Client Email"
        //         },
        //         prospect_service: {
        //             required: "Please Select Service"
        //         },
        //         priority: {
        //             required: "Please Select Priority"
        //         },
        //         fsc_fee: {
        //             required: "Please Enter FSC Fee"
        //         }

        //     },
        //     submitHandler: function(form) {
        //         form.submit();
        //     },
        //     errorHandler:function(form){
        //         alert("no sucess");
        //     }
        // });


        //   $(function () {
        //     $('#jstree').jstree(
        //     {   'core' : 
        //         {
        //             'data' : serviceJsonData
        //         },
        //         "checkbox" : {
        //           "keep_selected_style" : false
        //         },
        //         "plugins" : [ "checkbox" ]
        //     });
        //     $('#jstree').on("changed.jstree", function (e, data) {
        //       var non_selected_node = $('#jstree').jstree(true).get_json('#', {flat:true});
        //       //console.log(non_selected_node);
        //       var node = $('#jstree').jstree('get_selected');
        //       var node2 = $('#jstree').jstree('get_selected',true);
        //       var subnodeid = [];
        //       var nodeid=[];
        //       //console.log(node.length);
        //       $('#lbl_description').empty();
        //       $('#lbl_description').append("<ul>");

        //       for(var i=0;i<node2.length;i++){
        //           //var desc = desc + node2[i].text;
        //           var id = node2[i].id;
        //           if(node2[i].parent=="#"){
        //                 //$('#lbl_description').append("<li>"+ node2[i].text +"</li>");
        //                 nodeid.push(node2[i].id);
        //           }else if(id.substring(0, 1) == "s"){
        //                 var a = nodeid.indexOf(node2[i].parent);
        //                 if(a == -1){
        //                     //$('#lbl_description').append("<li>"+node2[i].text+"</li>");
        //                     nodeid.push(node2[i].parent);
        //                 }
        //                 subnodeid.push(node2[i].id);
        //           }
        //       }
        //       nodeid.push(subnodeid);

        //       for(var j=0;j<non_selected_node.length;j++){
        //           var isParent = non_selected_node[j].parent;

        //           if(isParent == "#"){
        //               var b=  nodeid.indexOf(non_selected_node[j].id);
        //               if(b != -1){
        //                   $('#lbl_description').append("<li>"+ non_selected_node[j].text +"</li>");
        //                   console.log("parent"+non_selected_node[j].text);
        //               }
        //           }else{
        //               var indexofArray = nodeid.length-1;
        //               var c =  nodeid[indexofArray].indexOf(non_selected_node[j].id);
        //               if(c != -1){
        //                   $('#lbl_description').append("<li style='margin-left:20px;'>"+ non_selected_node[j].text +"</li>");
        //                   console.log("parent"+non_selected_node[j].text);
        //               }
        //           }
        //       }


        //       console.log(nodeid[nodeid.length-1]);


        //       //var subnodeArray[] = subnodeid.split(',');
        //     //   for(var j=0;j<subnodeArray.length;j++)
        //     //   {

        //     //   }


        //       $('#lbl_description').append("</ul>");
        //       $('#selective_services').val(node);
        //     });
        //     // 8 interact with the tree - either way is OK
        //     $('button').on('click', function () {
        //       $('#jstree').jstree(true).select_node('child_node_1');
        //       $('#jstree').jstree('select_node', 'child_node_1');
        //       $.jstree.reference('#jstree').select_node('child_node_1');
        //     });

        //   });

        $('#prospect_service').on('change', function () {
            var selectedId = $('#prospect_service').find(':selected').val();
            //alert("<?php echo e(url('fac-Bhavesh-0554/services/getServiceMaster/')); ?>/"+selectedId);
            $('#period_service').empty();
            $.get('<?php echo URL::to('fac-Bhavesh-0554/admin/fetch_period'); ?>?id=' + selectedId, function (data) {
                serviceJsonData = JSON.parse(data);

                if (serviceJsonData.length > 0) {
                    if (selectedId == 3) {
                        $('#div_taxtation_type').show();
                        $('#div_period_val').show();
                        $('#proposal_taxtitles').attr('required', 'required');
                        $('#period').hide();
                        $('#period_service').removeAttr('required');
                    } else {
                        $('#div_taxtation_type').hide();
                        $('#div_period_val').hide();
                        $('#period').show();
                        $('#period_service').attr('required', 'required');
                        $('#proposal_taxtitles').removeAttr('required');
                    }

                    $('#Error').hide();
                    $('#period_service').append("<option value=''> Select Period </option>");
                    for (var i = 0; i < serviceJsonData.length; i++) {
                        //console.log(serviceJsonData[i]['period']);
                        $('#period_service').append("<option value='" + serviceJsonData[i]['pid'] + "'>" + serviceJsonData[i]['period'] + "</option>");
                    }
                    //console.log(serviceJsonData);
                } else {
                    $('#period').hide();
                    $('#period_service').removeAttr('required');
                    $('#Error').show();

                }
            });


            //   $.get("<?php echo e(url('fac-Bhavesh-0554/services/getServiceMaster/19')); ?>", function(data2, status){
            //         //serviceJsonData = data2;

            //         serviceJsonData = JSON.parse(data2);
            //         console.log(serviceJsonData);
            //         $('#jstree').jstree(true).settings.core.data = serviceJsonData;
            //         $('#jstree').jstree(true).refresh();
            //         ///alert(serviceJsonData);
            //     });

            $('#period_service').on('change', function () {
                var id = $('#prospect_service').find(':selected').val();
                var pid = $('#period_service').find(':selected').val();

                //alert(pid);
                $('#div_period_val').hide();
                if (pid == "" || pid == null || pid.length == 0) {
                    $('#fsc_fee').val('');
                    $(".lbl_preview_amount").text('');
                    $("._lbl_preview_amount").text('');
                    $("#total_prop_price").text('');
                    $(".lbl_description").append('');
                    $("#lbl_description_val").val('');
                } else {
                    //alert("<?php echo URL::to('fac-Bhavesh-0554/admin/fetch_period_price'); ?>?id="+id+"&pid="+pid);
                    $.get('<?php echo URL::to('fac-Bhavesh-0554/admin/fetch_period_price'); ?>?id=' + id + '&pid=' + pid, function (data) {
                        $(".lbl_description").empty();
                        $("#lbl_description_val").val('');
                        serviceJsonData = JSON.parse(data);
                        console.log(serviceJsonData['regularprice']);
                        fsc_fee_val = serviceJsonData['regularprice'].substring(2, serviceJsonData['regularprice'].length - 3);
                        fsc_fee_val = fsc_fee_val.replace(/,/g, '');
                        //alert(fsc_fee_val);
                        $('#fsc_fee').val(fsc_fee_val + ".00");
                        $('#net_amount').val(fsc_fee_val + ".00");
                        $('#discount').val("0.00");
                        $('#adv_payment').val("0.00");
                        $(".lbl_preview_amount").text(fsc_fee_val + ".00");
                        $("._lbl_preview_amount").text(currencyFormat(fsc_fee_val));
                        $("#total_prop_price").text(currencyFormat(fsc_fee_val));
                        $(".lbl_description").append("<p style='text-align:left'>" + serviceJsonData['serviceincludes'] + "</p>");
                        $("#lbl_description_val").val(serviceJsonData['serviceincludes']);
                    });
                }

            });

            $('#proposal_taxtitles').on('change', function () {
                var id = $('#prospect_service').find(':selected').val();
                var subid = $('#proposal_taxtitles').find(':selected').val();
                var tax_type = $('#taxtation_type').find(':selected').val();
                var url = '';

                //$('#period').hide();

                if (subid == 3 || subid == 2 || subid == 1) {
                    url = '<?php echo URL::to('fac-Bhavesh-0554/admin/fetch_taxtion_price'); ?>?id=' + id + '&pid=10&tid=' + subid;
                    $('#lbl_service_period').val("Monthly");
                } else if (subid == 5 || subid == 6 || subid == 7) {
                    url = '<?php echo URL::to('fac-Bhavesh-0554/admin/fetch_taxtion_price'); ?>?id=' + id + '&pid=7&tid=' + subid;
                    $('#lbl_service_period').val("Annually");
                } else if (subid == 8 || subid == 4) {
                    url = '<?php echo URL::to('fac-Bhavesh-0554/admin/fetch_taxtion_price'); ?>?id=' + id + '&pid=4&tid=' + subid;
                    $('#lbl_service_period').val("Quaterly");
                }

                //alert(url);


                //alert("<?php echo URL::to('fac-Bhavesh-0554/admin/fetch_period_price'); ?>?id="+id+"&pid="+pid);
                if (subid.length > 0) {
                    $.get(url, function (data) {
                        $(".lbl_description").empty();
                        $("#lbl_description_val").val('');
                        serviceJsonData = JSON.parse(data);
                        console.log(serviceJsonData['regularprice1']);
                        fsc_fee_val = serviceJsonData['regularprice1'].substring(2, serviceJsonData['regularprice1'].length - 3);
                        //alert(fsc_fee_val);
                        if (fsc_fee_val > 0) {
                            if (tax_type == 'Regular') {
                                $('#fsc_fee').val(fsc_fee_val + ".00");
                                $(".lbl_preview_amount").text(fsc_fee_val + ".00");
                                $("._lbl_preview_amount").text(currencyFormat(fsc_fee_val));
                                $("#total_prop_price").text(currencyFormat(fsc_fee_val));
                            }
                        } else {
                            $('#fsc_fee').val('0.00');
                            $(".lbl_preview_amount").text('-');
                            $("._lbl_preview_amount").text('-');
                            $("#total_prop_price").text('-');
                        }
                        $(".lbl_description").append("<p style='text-align:left'>" + serviceJsonData['serviceincludes1'] + "</p>");
                        $("#lbl_description_val").val(serviceJsonData['serviceincludes1']);


                    });
                } else {

                }


            });

            $("#taxtation_type").on('change', function (e) {
                var tax_type = $('#taxtation_type').find(':selected').val();
                $('#fsc_fee').val('0.00');
                $(".lbl_preview_amount").text('-');
                $("._lbl_preview_amount").text('-');
                $("#total_prop_price").text('-');
                if (tax_type == 'Regular') {
                    var price = fsc_fee_val + ".00";
                    $('#fsc_fee').val(price);
                    $(".lbl_preview_amount").text(price);
                    $("._lbl_preview_amount").text(currencyFormat(price));
                    $("#total_prop_price").text(currencyFormat(price));
                }
            });


            //   $.get("<?php echo e(url('fac-Bhavesh-0554/services/getServiceMaster/19')); ?>", function(data2, status){
            //         //serviceJsonData = data2;

            //         serviceJsonData = JSON.parse(data2);
            //         console.log(serviceJsonData);
            //         $('#jstree').jstree(true).settings.core.data = serviceJsonData;
            //         $('#jstree').jstree(true).refresh();
            //         ///alert(serviceJsonData);
            //     });

        });


        var refreshJStree = function (data) {

            //console.log(jsonData);
        }

    </script>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>