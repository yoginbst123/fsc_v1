<?php $__env->startSection('main-content'); ?>
<style>
.btn-box-tool.btn:hover{color: #333;}
.btn-box-tool.btn:active{color: #333;}
.box {background: #fff;}
.btn-box-tool{border:transparent}
.box-header > .fa, .box-header > .glyphicon, .box-header > .ion, .box-header .box-title {color: #333;}
.box-body{padding:10px}
</style>
<div class="content-wrapper">
      <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h2>Other Setup</h2>
    </section>
    <!-- Main content -->
    <section class="content">

 <!-- /Section -->
	<div class="row">
	    	<div class="col-md-12">
			<div class="box box-success">
			      <div class="box-header">
              
              <div class="box-tools pull-right">
                
              </div>
            </div>
				<div class="col-md-12">
	 <!-- /Position -->
        <div class="col-md-6 col-xs-12">
          <div class="box box-primary box-solid">
              <div class="box-header with-border">
              <h3 class="box-title">Position</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            
           <div class="box-body" style="">
<ul class="todo-list ui-sortable">
                <li><a href="<?php echo e(url('/fac-Bhavesh-0554/position/create')); ?>">Add New Position</a></li>
                <li><a href="<?php echo e(url('/fac-Bhavesh-0554/position')); ?>">View / Edit Position</a></li>
              </ul>
            </div>                      
          </div>    
        </div>
			 <!-- /End Position -->
 	 <!-- /Branch -->
        <div class="col-md-6 col-xs-12">
          <div class="box box-primary box-solid">
          
            <div class="box-header with-border">
              <h3 class="box-title">Branch</h3>
<div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <div class="box-body" style="">   
<ul class="todo-list ui-sortable">
                <li><a href="<?php echo e(url('/fac-Bhavesh-0554/branch/create')); ?>">Add New Branch</a></li>
                <li><a href="<?php echo e(url('/fac-Bhavesh-0554/branch')); ?>">View / Edit Branch</a></li>
              </ul>         
            </div>
          </div>
        </div>
     	 <!-- /End Branch -->
 <!-- /Branch -->
        <div class="col-md-6 col-xs-12">
          <div class="box box-primary box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Service</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>              
              </div>
            </div>
            <div class="box-body" style="">   
<ul class="todo-list ui-sortable">
                <li><a href="<?php echo e(url('/fac-Bhavesh-0554/cservice/create')); ?>">Add Service</a></li>
                <li><a href="<?php echo e(url('/fac-Bhavesh-0554/cservice')); ?>">View / Edit Service</a></li>
              </ul>         
            </div>
          </div>
        </div>
     	 <!-- /End Branch -->
<!-- /Branch -->
        <div class="col-md-6 col-xs-12">
          <div class="box box-primary box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Q & A</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>              
              </div>
            </div>
            <div class="box-body" style="">   
<ul class="todo-list ui-sortable">
                <li><a href="<?php echo e(url('/fac-Bhavesh-0554/question/create')); ?>">Add Questions</a></li>
                <li><a href="<?php echo e(url('/fac-Bhavesh-0554/question')); ?>">View / Edit Questions</a></li>
              </ul>         
            </div>
          </div>
        </div>
     	 <!-- /End Branch -->
      </div>
	  <!-- ./endsection -->
	  </div>
	  </div>
	  </div>
	   </section>	
<!--</div>-->

<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>