<?php $__env->startSection('main-content'); ?>
<style>ul.curency{ width:100%; padding:0; margin:0;}
ul.curency li{background: #286db5; padding: 5px 10px;margin-bottom: 7px;list-style:none;}
ul.curency li a{ color:#fff;}</style>
<div class="content-wrapper">
      <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>Message</h1>
    </section>
    <!-- Main content -->
    <section class="content">	
   <div class="row">
      <div class="col-md-12">
         <div class="box box-success">
			      <div class="box-header">
              <div class="box-tools pull-right">
              </div>
            </div>
				<div class="col-md-12">
				     <form method="post" action="<?php echo e(route('msg.store')); ?>" class="form-horizontal" enctype="multipart/form-data">
                  <?php echo e(csrf_field()); ?>

                  <div class="form-group">
                   <label class="control-label col-md-3">Date / Day / Time:</label>
                     <div class="col-lg-8 col-md-9">
                         <div class="row">
                     <div class="col-lg-3 col-md-4">
                        <div class="">
                           <input type="text" name="date" id="date" class="form-control" style="font-weight: normal;pointer-events:none; background:#ccc !important;" value="<?php echo e(date('m/d/Y')); ?>" placeholder="Date">
                        </div>
                     </div>
                      
                     <div class="col-lg-3 col-md-4 col-xs-6">
                        <div class="">
                           <input type="text" name="day" id="day" class="form-control" placeholder="Day" style="font-weight: normal;pointer-events:none; background:#ccc !important;" value="<?php echo e(date('l')); ?>">
                        </div>
                     </div>
                     <input type="hidden" name="state" readonly id="state" value="employee"  class="form-control">
                     <div class="col-lg-2 col-md-4 col-xs-6">
                        <div class="">
                           <input type="text" name="time" id="time" class="form-control" value="<?php echo e(date("g:i a")); ?>" style="font-weight: normal;pointer-events:none; background:#ccc !important;" placeholder="Time">
                        </div>
                     </div>
                     </div></div>
                  </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Massage From :</label>
                        <div class="col-lg-8 col-md-9">
                            <div class="row">
                                <div class="col-lg-8 col-md-12">
                                    <select name="type" id="type" class="form-control fsc-input">
                                        <option value="">---Select---</option>
                                        <option value="Admin">Admin</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" id="emp">
                        <label class="control-label col-md-3"><span class="emp2">Admin</span> :</label>
                        <div class="col-lg-8 col-md-9">
                            <div class="row">
                                <div class="col-lg-8 col-md-12">
                                    <select name="employee" id="employee" class="form-control selectpicker fsc-input">
                                        <option value="<?php echo e(Auth::user()->id); ?> "><?php echo e(Auth::user()->fname); ?> <?php echo e(Auth::user()->mname); ?> <?php echo e(Auth::user()->lname); ?></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group client" style="display:none">
                        <label class="control-label col-md-3"><span class="emp2">Employee</span><span class="emp3">Client</span> Business Name :</label>
                        <div class="col-lg-8 col-md-9">
                            <div class="row">
                                <div class="col-lg-8 col-md-12">
                                    <input type="text" name="busname" readonly id="busname" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group client3" style="display:none">
                        <label class="control-label col-md-3"><span class="emp2">Employee</span><span class="emp3">Client</span> Name :</label>
                        <div class="col-lg-8 col-md-9">
                            <div class="row">
                                <div class="col-lg-8 col-md-12">
                                    <input type="text" name="clientname" readonly id="clientname" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group clien2" style="display:none">
                        <label class="control-label col-md-3"><span class="emp2">Employee</span><span class="emp3">Client</span> File # :</label>
                        <div class="col-lg-8 col-md-9">
                            <div class="row">
                                <div class="col-lg-8 col-md-12">
                                    <input type="text" name="clientfile" readonly id="clientfile" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group client1" style="display:none">
                        <label class="control-label col-md-3"><span class="emp2">Employee</span><span class="emp3">Client</span> Telephone # :</label>
                        <div class="col-lg-8 col-md-9">
                            <div class="row">
                                <div class="col-lg-8 col-md-12">
                                    <input type="text" name="clientno" readonly id="clientno" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                   
                    <div class="form-group">
                        <label class="control-label col-md-3">Message To :</label>
                        <div class="col-lg-8 col-md-9">
                            <div class="row">
                                <div class="col-lg-8 col-md-12">
                                    <select name="employees" id="employees" class="form-control selectpicker fsc-input">
                                        <option value="">Select</option>
                                        <?php $__currentLoopData = $employee1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $employee2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                        <option value="<?php echo e($employee2->id); ?>" ><?php echo e($employee2->firstName); ?> <?php echo e($employee2->middleName); ?> <?php echo e($employee2->lastName); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" id="">
                        <label class="control-label col-md-3">Call Purpose :</label>
                        <div class="col-lg-8 col-md-9">
                            <div class="row" id="div">
                                <div class="col-lg-8 col-md-9">
                                    <select name="purpose" id="purpose2" class="form-control fsc-input">
                                        <option value="">---Select---</option>
                                        <?php $__currentLoopData = $purpose; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pr): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($pr->purposename); ?>"><?php echo e($pr->purposename); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                            
                            <div class="col-md-3">
                                <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i></button>   
                                <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#myModal1"><i class="fa fa-minus"></i></button>
                            </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">What is the Message :</label>
                        <div class="col-lg-8 col-md-9">
                            <div class="row">
                                <div class="col-lg-8 col-md-12">
                                    <input type="text" name="othermsg" id="othermsg" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Message Priority :</label>
                        <div class="col-lg-8 col-md-9">
                            <div class="row">
                                <div class="col-lg-8 col-md-12">
                                    <select name="purpose1" id="purpose1" class="form-control fsc-input">
                                        <option value="Regular">Regular</option>
                                        <option value="Important">Important</option>
                                        <option value="Urgent">Urgent</option>
                                    </select>
                                    <input type="hidden" value="" name="other" id="other" class="form-control fsc-input">
                                </div>
                            </div>
                        </div>
                    </div>
                   
                  <div class="card-footer">
                     <div class="row">
                         <label class="control-label col-md-3"> </label>
                        <div class="col-lg-8 col-md-9">
                              <div class="row">
                             <div class="col-xs-3" style="width:155px;">
                           <input class="btn_new_save  btn-primary1 primary1" type="submit" name="submit" value="Send">
                           </div>
                            <div class="col-xs-3" style="width:155px;">
                           <a class="btn_new_cancel" href="<?php echo e(url('/fscemployee/getmsg')); ?>">Cancel</a>
                        </div>
                        </div>
                        </div>
                         </div>
                  </div>
               </form>
               </div>
         </div>
      </div>
   </div>
      </section>
<!--</div>-->

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6/js/select2.min.js"></script>
<script>
$(".selectpicker").select2({
})
</script>
<!--<script>
$(document).ready(function(){
	$(document).on('change','#type', function()
	{
	var id = $(this).val();
	     
          
           $('#clientno').val("");
           $('#busname').val("");
           $('#clientfile').val("");
if(id=='Approval')
{ $('.client').hide();
    $('.client1').hide();
    $('.emp2').hide();
     $('.emp3').show();
  $('#emp').show();   $('.client3').hide();
}
if(id=='employee')
{
   
  $('.emp3').hide();
   $('#emp').show();
    $('.emp2').show();
  $('.client').hide();
    $('.client1').hide();
     $('.client3').hide();
}
$('#employee').empty();
if(id=='Other Person')
{   $('.clien2').hide();
     $('.emp3').hide();
   $('#emp').hide();
    $('.emp2').hide();
    
     $('.client').show();
    $('.client1').show();
    $('.client3').show();
    document.getElementById('busname').removeAttribute('readonly');
document.getElementById('clientfile').removeAttribute('readonly');
    document.getElementById('clientname').removeAttribute('readonly');
    document.getElementById('clientno').removeAttribute('readonly');
}

		$.get('<?php echo URL::to('/clientid'); ?>?id='+id, function(data)
		{  
            $('#employee').empty();
            $('#employee').append('<option value="">---Select---</option>');
           $.each(data, function(index, subcatobj)
		   {
 if(id=='employee')
{
    
  $('#employee').append('<option value="' + subcatobj.id + '" selected>' + subcatobj.firstName + ' ' + subcatobj.lastName + '</option>');  
 
}
if(id=='Approval')
{
     
 $('#employee').append('<option value="' + subcatobj.id + '">' + subcatobj.first_name + ' ' + subcatobj.last_name + '</option>');
}
	
		   })

		});
			
	});
});

$( "#date" ).datepicker({
    'dateFormat':'yy-mm-dd',
    onSelect: function(dateText){ alert();
        var seldate = $(this).datepicker('getDate');
        seldate = seldate.toDateString();
        seldate = seldate.split(' ');
        var weekday=new Array();
            weekday['Mon']="Monday";
            weekday['Tue']="Tuesday";
            weekday['Wed']="Wednesday";
            weekday['Thu']="Thursday";
            weekday['Fri']="Friday";
            weekday['Sat']="Saturday";
            weekday['Sun']="Sunday";
        var dayOfWeek = weekday[seldate[0]];
        $('#day').val(dayOfWeek);
    }
});

</script>-->
<script>
$(document).ready(function(){
	$(document).on('change','#employee', function()
	{ var id= $("#employee").select2('val');
	    var selectedCountry = $("#type option:selected").val();
	//	var id = $(this).val(); 
//	alert(id);
		$.get('<?php echo URL::to('/fscemployee/getmessage'); ?>?id='+id+'&state=' + selectedCountry, function(data)
		{  
           $('#clientno').val("");
           $('#busname').val("");
           $('#clientfile').val("");
           $.each(data, function(index, subcatobj)
		   {
            document.getElementById('busname').readOnly =true;
           
            document.getElementById('clientfile').readOnly =true;
            document.getElementById('clientno').readOnly =true;	

if('employee'==subcatobj.type)
{
   // $('.client').show();
    //  $('.client1').show();
//$('.clien2').show();
//$('#clientno').val(subcatobj.telephoneNo1);
//$('#busname').val(subcatobj.business_name);
//$('#clientfile').val(subcatobj.employee_id); 
}
if('Approval'==subcatobj.status)
{
    $('.client').show();
      $('.client1').show();
$('.clien2').show();
$('#clientno').val(subcatobj.business_no);
$('#busname').val(subcatobj.business_name);
$('#clientfile').val(subcatobj.filename);
}	
	})
	});
	});
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $("#date").change(function() {
      var startdate= $("#date").val();
      var monthNames = [
        "Jan", "Feb", "Mar",
        "Apr", "May", "Jun", "Jul",
        "Aug", "Sep", "Oct",
        "Nov", "Dec"
      ];
    var weekday = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
      var durtion=$('#duration').val();
      var  date = new Date(startdate);
      var day = weekday[date.getDay()];
      var monthly=30;
      var weekly=7;
      var bimonthly=15;
      var biweekly=14;
      var monthss=monthNames[(date.getMonth())];
      var yearss=date.getFullYear();
      var yyy=yearss % 4 ;
     
      if(durtion == "Weekly")
      {
        var totaldays=6;
      }
      else if(durtion == "Monthly")
      {
        if(monthss == 'Jan' || monthss == 'Mar' || monthss == 'May' || monthss == 'Jul' || monthss == 'Aug' || monthss == 'Oct' || monthss == 'Dec')
        {
          var totaldays=30;
        }
        else if(monthss == 'Feb')
        {
          //if(years / 4 = 0)
          if(yyy ==0)
          {
            var totaldays=28;
          }
          else if(yyy ==1)
          {
            var totaldays=27;
          }
        }
        else if(monthss == 'Apr' || monthss == 'Jun' || monthss == 'Sep' || monthss == 'Nov' )
        {
          var totaldays=29;
        }
      }
      else if(durtion == "Bi-Weekly")
      {
        var totaldays=13;
      }
      else if(durtion == "Bi-Monthly")
      {
        var totaldays=14;
      }
    // var vv = day + totaldays;
      
    date.setDate(date.getDate() + totaldays);// alert(vv);
      var date1 = ("0" + (date.getMonth() + 1)).slice(-2)  + "/" + ("0" + date.getDate()).slice(-2) + "/" + date.getFullYear();// alert(date.getDate())
     // alert(date1);
      var newdate = new Date(date1);
        var day1 = weekday[newdate.getDay()];
       //alert(newdate);
      var date2=monthNames[(date.getMonth())] + "/" + date.getDate() + "/" + date.getFullYear() ;
     // $('#sch_end_date').val(date1);
      $('#day').val(day);
      $('#sch_end_day').val(day1);
      //document.write(date2);
    });
    $("#duration").change(function() {
      $('#sch_end_date').val('');
      $('#sch_start_date').val('');
    });
  });
</script>
<style>
    .select2-container .select2-selection--single {
    box-sizing: border-box;
    cursor: pointer;
    display: block;
    height: 39px; border-redius:4px;
    user-select: none;
    -webkit-user-select: none;
}
    .select2 {width:100% !important;}
    .select2-container .select2-selection--single {border: 2px solid #00468F;}
ul.curency li a.delete i{ float:right; color:red; }
</style>
 <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Call Purpose</h4>
      </div>
      <div class="modal-body" style="display: inline-table; width:100%;">
           <form action="" method="post" id="ajax2">
                <?php echo e(csrf_field()); ?>

        <div class="form-group">
                   <label class="control-label col-md-3">Call Purpose :</label>
                     <div class="col-md-6">
                        <div class="">
                            <input type="text" name="newopt" id="newopt" class="form-control" placeholder="Call Purpose">
                        </div>
                     </div>
                     <div class="col-md-2">
                        <div class="">
                            <input type="button" id="addopt" class="btn btn-primary" value="Add Call Purpose">
                        </div>
                     </div>
                  </div>
                  </form>
                  <div class="form-group">
                   <label class="control-label col-md-3"></label>
                     
                  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<div id="myModal1" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Call Purpose</h4>
      </div>
      <div class="modal-body" style="display: inline-table; width:45%">
           <ul class="curency" id="currency">
               <?php $__currentLoopData = $purpose; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $typeofser1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <li id="ser_<?php echo e($typeofser1->id); ?>"><a class="delete" id="<?php echo e($typeofser1->id); ?>"><?php echo e($typeofser1->purposename); ?> <span><i class="fa fa-trash"></i></span></a></li>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script>
          $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')}
              });
             $(function () {
                $('#addopt').click(function () { //alert();
                    var newopt = $('#newopt').val();
                    if (newopt == '') {
                        alert('Please enter something!');
                        return;
                    }
                    $('#purpose1 option').each(function (index) {
                        if ($(this).val() == newopt) {
                            alert('Duplicate option, Please enter new!');
                        }
                    })
                    $.ajax({
        type: "post",
        url: "<?php echo route('purpose.purposes'); ?>",
        dataType: "json",
        data: $('#ajax2').serialize(),
        success: function(data){
             alert('Successfully Add');
             $('#purpose2').append('<option value=' + newopt + '>' + newopt + '</option>');
             $("#div").load(" #div > *");
             $("#newopt").val('');
        },
        error: function(data){
             alert("Error")
        }
    });
                $('#myModal').modal('hide');
                });
            });
   $(document).ready(function() {         
            $(document).on('click', '.delete', function(){
        var id = $(this).attr('id');//alert();
        if(confirm("Are you sure you want to Delete this data?"))
        {
            $.ajax({
                url:"<?php echo e(route('removepurpose.removepurpose1')); ?>",
                mehtod:"get",
                data:{id:id},
                success:function(data)
                {
                 // alert(data);
                   $('#cur_'+id).remove();
                   $("#currency").load(" #currency > *");
                }
            })
        }
        else
        {
            return false;
        }
    }); 
   }); 
</script>

<style>
    .select2-container .select2-selection--single {
    box-sizing: border-box;
    cursor: pointer;
    display: block;
    height: 39px; border-redius:4px;
    user-select: none;
    -webkit-user-select: none;
}
    .select2 {width:100% !important;}
    .select2-container .select2-selection--single {border: 2px solid #00468F;}
</style>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>