<?php $__env->startSection('main-content'); ?>
<style>
.box-tools{
    position:absolute !important;
    margin-top: 17px !important;
    margin-right: 150px !important;
}
.buttons-pdf {
    font-size: 0px !important;
    border-radius: 3px;
    padding: 8px 10px !important;
    background: #fff !important;
    border-color: #c6c6c6  !important;
    color:red !important;
}
.buttons-print {
    font-size: 0px !important;
    border-radius: 3px;
    padding: 8px 10px !important;
    background: #3c8dbc !important;
    border-color: #367fa9 !important;
}
.buttons-excel{
    font-size:0px !important;
    border-radius: 3px;
    padding: 8px 10px !important;
    background: #00a65a !important;
    border-color: #008d4c !important;
}
.buttons-excel:hover{
     background: #008d4c !important;
}
.buttons-pdf:hover{
     background: #f6f6f6  !important;
}
.buttons-print:hover{
     background: #367fa9 !important;
}
.fa{
    font-size: 16px !important;
}
@media  only screen and (max-width: 1280px){
    .table-responsive table.table {
        display: inline-table;
        overflow-x: auto;
        white-space: nowrap;
    }
}
@media  only screen and (max-width: 1010px){
    .table-responsive table.table {
        display: block;
        overflow-x: auto;
        white-space: nowrap;
    }
}
@media  only screen and (max-width: 991px){
    .table-title a {
        margin-top: 0px !important; 
        margin-right: -10px !important;
    }
}
@media  only screen and (max-width: 860px){
    .box-header>.box-tools{
        position: relative !important;
        margin-right: 5px !important;
        margin-top: 0px !important;
    }
}
@media  only screen and (max-width: 490px){
    div.dataTables_wrapper div.dataTables_filter{
        width: 100%;
        display: flex;
    }
    div.dataTables_wrapper div.dataTables_filter label{
        width: 84%;
    }
    .table-title a {
        margin-top: -34px !important;
        margin-right: 10px !important;
    }
    .box-header>.box-tools {
        position: absolute !important;
        margin-top: 51px !important;
        margin-right: 120px !important;
    }
}
</style>
<div class="content-wrapper">
    <section class="content-header page-title">
  <h2>List of upload</h2>
    </section>
 <section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-success">
			    <div class="box-header">
              <div class="box-tools pull-right" style="z-index:9999;">
                <div class="table-title">
						<a href="<?php echo e(route('adminupload.create')); ?>"><i class="fa fa-plus"></i>&nbsp; Add New</a>
					</div>
              </div>
            </div>
				<div class="col-md-12">
					<?php if(session()->has('success') ): ?>
    <div class="alert alert-success alert-dismissable"><?php echo e(session()->get('success')); ?></div>
                               <?php endif; ?>
					<div class="table-responsive">
						<table class="table table-hover table-bordered" id="example">
							<thead>
								<tr>
									<th>No</th>
									<th width="40%">Documant Name</th>
									<th>Upload</th>
							       <th>Expire Date</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
							    <?php $id1 = Auth::user()->business_license_jurisdiction;?>
                                <?php $__currentLoopData = $upload; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bus): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<tr>
									<td><?php echo e($loop->index + 1); ?></td>
									<td><?php if($bus->upload_name=='SOS Certificate'): ?> <?php echo e($bus->upload_name); ?> <?php endif; ?> <?php if($bus->upload_name=='SOS-AOI'): ?> <?php echo e($bus->upload_name); ?> <?php endif; ?>
									<?php if($bus->upload_name==$id1): ?> Business License <?php echo e($bus->upload_name); ?> <?php echo e(Auth::user()->business_license2); ?> <?php else: ?>  
									<?php $__currentLoopData = $professional; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $admin_pr): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
									<?php if($bus->upload_name==$admin_pr->pro_id): ?> 
									Professional License <?php echo e($bus->upload_name); ?> 
									<?php endif; ?>
									
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									<?php endif; ?>
									</td>
									<td><?php echo e($bus->upload); ?></td>
									<td><?php if($bus->expired_date==NULL): ?> Never Expired <?php else: ?> <?php echo e($bus->expired_date); ?> <?php endif; ?></td>
									<td class="text-center">
										<a class="btn-action btn-view-edit" href="<?php echo e(route('adminupload.edit',$bus->id)); ?>"><i class="fa fa-edit"></i></a>
                                        <form action="<?php echo e(route('adminupload.destroy',$bus->id)); ?>" method="post" style="display:none" id="delete-id-<?php echo e($bus->id); ?>">
                                        <?php echo e(csrf_field()); ?> <?php echo e(method_field('DELETE')); ?>

                                        </form>
										<a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record ?'))
                                                                                  {event.preventDefault();document.getElementById('delete-id-<?php echo e($bus->id); ?>').submit();} else{event.preventDefault();}" href=""><i class="fa fa-trash"></i></a>
									</td>
								</tr>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div> 
		
	</div>
	</section>	
<!--</div>-->
	<script>
$(document).ready(function() {
    var table = $('#example').DataTable( {
        dom: 'Bfrtlip',
    "columnDefs": [ {
            "searchable": true,
            "orderable": true,
            "targets": 0
        } ],
        "order": [[ 0, 'asc' ]],
        buttons: [
                 {
                extend:    'copyHtml5',
                text:      '<i class="fa fa-files-o"></i> &nbsp; Copy',
                titleAttr: 'Copy',
                title: $('h3').text(),
                  exportOptions: {
        columns: [0,1, 2, 3,4,5], // Only name, email and role
    }
            },
            {
                extend:    'excelHtml5',
                text:      '<i class="fa fa-file-excel-o"></i>&nbsp; Excel',
               titleAttr: 'Excel',
                title: $('h3').text(),
                 customize: function( xlsx ) {
                var sheet = xlsx.xl.worksheets['sheet1.xml'];
 $('row c', sheet).attr('s', '51');
            },
                  exportOptions: {
        columns: [0,1, 2, 3,4,5], // Only name, email and role
    }
            },
            {
                extend:    'csvHtml5',
                text:      '<i class="fa fa-file-text-o"></i> &nbsp; CSV',
                titleAttr: 'CSV',
                title: $('h3').text(),
                  exportOptions: {
        columns: [0,1, 2, 3,4,5], // Only name, email and role
    }
            },
            {
                extend:    'pdfHtml5',
                text:      '<i class="fa fa-file-pdf-o"></i>&nbsp;  PDF',
                
              customize: function (doc) {
						//Remove the title created by datatTables
						doc.content.splice(0,1);
						//Create a date string that we use in the footer. Format is dd-mm-yyyy
						var now = new Date();
						var jsDate = now.getDate()+'-'+(now.getMonth()+1)+'-'+now.getFullYear();
						
						doc.pageMargins = [20,60,20,20];
						doc.defaultStyle.fontSize = 10;
						doc.styles.tableHeader.fontSize = 10;
						doc['header']=(function() {
							return {
								columns: [{
									    alignment: 'left',
										image: logo,
										width: 50,margin: [200,5]
									},{
										alignment: 'CENTER',
										text: 'List of Client',
										fontSize: 20,
										margin: [10,35],
									},],
								margin: [20, 0, 0,12],alignment: 'center',
							}
						});
						var objLayout = {};
						objLayout['hLineWidth'] = function(i) { return 2; };
						objLayout['vLineWidth'] = function(i) { return 2; };
						objLayout['hLineColor'] = function(i) { return '#ccc'; };
						objLayout['vLineColor'] = function(i) { return '#ccc'; };
						objLayout['paddingLeft'] = function(i) { return 14; };
						objLayout['paddingRight'] = function(i) { return 14; };
						doc.content[0].layout = objLayout;
				},
				titleAttr: 'PDF',
                  exportOptions: {
        columns: [0,1, 2, 3,4,5], // Only name, email and role
    }
            },
            {
           extend: 'print',
          text: '<i class="fa fa-print"></i>&nbsp; Print',
          titleAttr: 'Print',
        customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '10pt' )
                        .prepend(
                            '<center><img src=""/></center>'
                        );
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );
                },
         exportOptions: {
          columns: [0,1, 2, 3,4,5]
      },
      footer: true,
      autoPrint: true
    },],
    } );
$('input.global_filter').on( 'keyup click', function () {
        filterGlobal();
    } );
 
    $('input.column_filter').on( 'keyup click', function () {
        filterColumn( $(this).parents('tr').attr('data-column') );
    } );
  table.on( 'order.dt search.dt', function () {
       table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
          cell.innerHTML = i + 1;
          table.cell(cell).invalidate('dom'); 
       } );
    } ).draw();
       table.columns(6)
        .search('^(?:(?!Inactive|New|1).)*$\r?\n?', true, false)
        .draw();
    $("#choice").on("change",function(){
 var _val = $(this).val();//alert(_val);
 
 if(_val == 'Inactive'){   
        table.columns(6).search(_val).draw();
  }
 else if(_val == 'New'){   
        table.columns(6).search(_val).draw();
  }
  else if(_val == 'Active'){  //alert();
         table.columns(6).search(_val).draw();
          table.columns(6)
        .search('^(?:(?!Inactive|New|1).)*$\r?\n?', true, false)
        .draw();
  }
  else{
        table
        .columns()
        .search('')
        .draw(); 
  }
  })
} );
   	</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>