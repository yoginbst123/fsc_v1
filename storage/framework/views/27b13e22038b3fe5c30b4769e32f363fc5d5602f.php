<?php $__env->startSection('main-content'); ?>
<style>
label{float:left;}
.box-tools{
        position:absolute !important;
        margin-top: 7px !important;
        margin-right: 150px !important;
    }
    .page-title {
    display: -ms-flexbox;
    -ms-flex-align: center;
    align-items: center;
    -ms-flex-pack: justify;
    justify-content: space-between;
    -ms-flex-direction: row;
    flex-direction: row;
    padding: 8px 18px !important;
    box-shadow: 0 1px 2px rgba(0,0,0,.1);
    background-color: #D6EBFA !important;
    text-align: center;
}
.buttons-pdf {
    font-size: 0px !important;
    border-radius: 3px;
    padding: 8px 10px !important;
    background: #fff !important;
    border-color: #c6c6c6  !important;
    color:red !important;
}
.buttons-print {
    font-size: 0px !important;
    border-radius: 3px;
    padding: 8px 10px !important;
    background: #3c8dbc !important;
            border-color: #367fa9 !important;
}
.buttons-excel{
    font-size:0px !important;
    border-radius: 3px;
    padding: 8px 10px !important;
    background: #00a65a !important;
    border-color: #008d4c !important;
}
.buttons-excel:hover{
     background: #008d4c !important;
}
.buttons-pdf:hover{
     background: #f6f6f6  !important;
}
.buttons-print:hover{
     background: #367fa9 !important;
}
.fa{
    font-size: 16px !important;
}
.content-header.page-title h2 {
    margin: 0;
    font-size: 22px!important;
    font-weight: 600!important;
    color: #222!important;
}
@media  only screen and (max-width: 991px){
    .table-title a {
        margin-top: 0px !important; 
        margin-right: -10px !important;
    }
}
@media  only screen and (max-width: 860px){
    .box-header>.box-tools{
        position: relative !important;
        margin-right: 5px !important;
        margin-top: 0px !important;
    }
}
@media  only screen and (max-width: 490px){
    div.dataTables_wrapper div.dataTables_filter{
        width: 100%;
        display: flex;
    }
    div.dataTables_wrapper div.dataTables_filter label{
        width: 84%;
    }
    .table-title a {
        margin-top: -34px !important;
        margin-right: 10px !important;
    }
    .box-header>.box-tools {
        position: absolute !important;
        margin-top: 51px !important;
        margin-right: 120px !important;
    }
    .box-header {
        padding: 10px !important;
    }
}
</style>
<div class="content-wrapper">
	 <!-- Content Header (Page header) -->
    <section class="content-header page-title" style="height:50px;">
     		<div >
     		    <div  style="text-align:center;">
     		        <h2>List of Business Brand Category</h2>
     		    </div>
     		    
     		</div>
    </section>
    <!-- Main content -->
    <section class="content">
	<div class="row">
	
		<div class="col-md-12">
			<div class="box box-success">
			      <div class="box-header"  style="padding:0px;">
             
              <div class="box-tools pull-right" style="position: absolute;margin-right: 132px;margin-top:7px;z-index:9999;">
                <div class="table-title">
					
						<a href="<?php echo e(route('business-brand-category.create')); ?>">Add New</a>
					</div>
              </div>
            </div>
				<div class="col-md-12">
					<?php if(session()->has('success') ): ?>
    <div class="alert alert-success alert-dismissable"><?php echo e(session()->get('success')); ?></div>
                               <?php endif; ?>
					<div class="table-responsive">
						<table class="table table-hover table-bordered" id="sampleTable2">
						
							<thead>
								<tr>
									<th>#</th>
									<th>Business Names</th>
									<th>Business Category</th>
									<th>Business Brand</th>
									<th>Business Brand Category</th>
									<th>Business Brand Image</th>
									<th>Action</th>
								</tr>
							</thead>
							
							<tbody>
							    <?php $cnts=0;?>
							<?php $__currentLoopData = $categorybusiness; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<?php $cnts++;?>
								<tr>
									<td style="text-align:center;"><?php echo $cnts;?></td>
									<td><?php echo e($cat->bussiness_name); ?></td>
									<td><?php echo e($cat->business_cat_name); ?></td>
									<td><?php echo e($cat->business_brand_name); ?></td>
									<td><?php echo e($cat->business_brand_category_name); ?></td>
									<td style="text-align:center;"><img  style="width: 100px;" src="<?php echo e(url('public/businessbrandcategory')); ?>/<?php echo e($cat->business_brand_category_image); ?>" class="extra-small-image" alt="" /></td>
									<td style="text-align:center;">
									<a class="btn-action btn-view-edit" href="<?php echo e(route('business-brand-category.edit', $cat->cid)); ?>"><i class="fa fa-edit"></i></a>
									<form action="<?php echo e(route('business-brand-category.destroy',$cat->cid)); ?>" method="post" style="display:none" id="delete-id-<?php echo e($cat->cid); ?>">
									<?php echo e(csrf_field()); ?> <?php echo e(method_field('DELETE')); ?>

									</form>
									<a class="btn-action btn-delete" href="#"onclick="if(confirm('Are you sure, You want to delete this record ?'))
									{event.preventDefault();document.getElementById('delete-id-<?php echo e($cat->cid); ?>').submit();} else{event.preventDefault();}"><i class="fa fa-trash"></i></a>
									</td>
								</tr>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>								
							</tbody>
							
						</table>
					</div>
				</div>
			</div>
		</div>
		
	</div>
		</section>
<!--</div>-->
	<style>.content-wrapper {
    
    height: 100%;
}</style>
<script>
$(document).ready(function() {
    var table = $('#sampleTable2').DataTable( {
        "ordering": true,
        dom: 'Bfrtlip',
    "columnDefs": [ {
            "searchable": false,
            "orderable": true,
            "targets": 0
        } ],
        buttons: [
                 {
                extend:    'copyHtml5',
                text:      '<i class="fa fa-files-o"></i> &nbsp; Copy',
                //titleAttr: 'Copy',
                title: $('h1').text(),
                  exportOptions: {
        columns: [0,1, 2, 3,4,5,6], // Only name, email and role
    }
            },
            {
                extend:    'excelHtml5',
                text:      '<i class="fa fa-file-excel-o"></i>&nbsp; Excel',
               // titleAttr: 'Excel',
                title: $('h1').text(),
                  exportOptions: {
        columns: [0,1, 2, 3,4,5,6], // Only name, email and role
    }
            },
            {
                extend:    'csvHtml5',
                text:      '<i class="fa fa-file-text-o"></i> &nbsp; CSV',
               // titleAttr: 'CSV',
                title: $('h1').text(),
                  exportOptions: {
        columns: [0,1, 2, 3,4,5,6], // Only name, email and role
    }
            },
            {
                extend:    'pdfHtml5',
                text:      '<i class="fa fa-file-pdf-o"></i>&nbsp;  PDF',
             //   titleAttr: 'PDF',
                title: $('h1').text(),
                exportOptions: { 
        columns: [0,1, 2, 3,4,5,6], // Only name, email and role
    }
            },
            {
           extend: 'print',
          text: '<i class="fa fa-print"></i>&nbsp; Print',
         title: $('h1').text(),
         exportOptions: {
          columns: ':not(.no-print)'
      },
      footer: true,
      autoPrint: true
    },
        ],
    } );
} );
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>