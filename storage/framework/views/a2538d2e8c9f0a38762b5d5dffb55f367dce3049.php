<?php $__env->startSection('title', 'Edit Technical Support'); ?>
<?php $__env->startSection('main-content'); ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>Technical Support</h1>
    </section>
    <!-- Main content -->
    <section class="content">
<div class="row">
		<div class="col-md-12">
		<div class="box box-success">
			      <div class="box-header">
             
              <div class="box-tools pull-right">
                
              </div>
            </div>
				<div class="card-body col-md-12">
                   
					<form method="post" action="#" class="form-horizontal" id="homecontent" name="homecontent" enctype="multipart/form-data">
					<?php echo e(csrf_field()); ?><?php echo e(method_field('PATCH')); ?>

					 <div class="form-group">
                   <label class="control-label col-md-3">Date / Day / Time:</label>
                     <div class="col-md-2" style="width: 130px;">
                        <div class="">
                           <input type="text" name="date" id="date" class="form-control" value="<?php echo e($homecontent->date); ?>" placeholder="Date">
                        </div>
                     </div>
                      
                     <div class="col-md-2" style="width: 130px;">
                        <div class="">
                           <input type="text" name="day" id="day" class="form-control" placeholder="Day" value="<?php echo e($homecontent->day); ?>">
                        </div>
                     </div>
                     
                     <div class="col-md-2" style="width: 115px;">
                        <div class="">
                           <input type="text" name="time" id="time" class="form-control" value="<?php echo e($homecontent->time); ?>" placeholder="Time">
                        </div>
                       
                     </div>
                  </div>
						<div class="form-group<?php echo e($errors->has('to') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">To :</label>
							<div class="col-md-8">
							    	<select class="form-control" id="to" name="to">
                                                <option value="">Select</option>
                                                <?php $__currentLoopData = $employee1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $employ): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php if(!empty($employ->technical_support)): ?>
                                                <option value="<?php echo e($employ->id); ?>" <?php if($employ->id==$homecontent->to_supporter): ?> selected <?php endif; ?>><?php echo e($employ->technical_support); ?> (<?php echo e($employ->firstName.' '.$employ->middleName.' '.$employ->lastName); ?>)</option>
                                                <?php endif; ?>
                                                 <?php if(!empty($employ->timing_support)): ?>
                                                <option value="<?php echo e($employ->id); ?>" <?php if($employ->id==$homecontent->to_supporter): ?> selected <?php endif; ?>><?php echo e($employ->timing_support); ?> (<?php echo e($employ->firstName.' '.$employ->middleName.' '.$employ->lastName); ?>)</option>
                                                <?php endif; ?>
                                                 <?php if(!empty($employ->system_support)): ?>
                                                <option value="<?php echo e($employ->id); ?>" <?php if($employ->id==$homecontent->to_supporter): ?> selected <?php endif; ?>><?php echo e($employ->system_support); ?> (<?php echo e($employ->firstName.' '.$employ->middleName.' '.$employ->lastName); ?>)</option>
                                                <?php endif; ?>
                                                 <?php if(!empty($employ->other_support)): ?>
                                                <option value="<?php echo e($employ->id); ?>" <?php if($employ->id==$homecontent->to_supporter): ?> selected <?php endif; ?>><?php echo e($employ->other_support); ?> (<?php echo e($employ->firstName.' '.$employ->middleName.' '.$employ->lastName); ?>)</option>
                                                <?php endif; ?>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
							
							</div>
						</div>			
						<div class="form-group <?php echo e($errors->has('subject') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Subject :</label>
							<div class="col-md-8">
								<input name="subject" type="text" id="subject" value="<?php echo e($homecontent->subject); ?>" class="form-control">

								<?php if($errors->has('subject')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('subject')); ?></strong>
										</span>
									<?php endif; ?>							
							</div>
						</div>
							
					<div class="form-group<?php echo e($errors->has('details') ? ' has-error' : ''); ?>">
						<label class="control-label col-md-3">Details :</label>
						<div class="col-md-8">
						<div class="">
							  <textarea id="editor1" name="details" rows="10" cols="80"><?php echo e($homecontent->details); ?></textarea>
					  </div>
							<?php if($errors->has('details')): ?>
									<span class="help-block">
										<strong><?php echo e($errors->first('details')); ?></strong>
									</span>
								<?php endif; ?>	
						</div>
					</div>
						<div class="form-group">
						<label class="control-label col-md-3">Technical Answer :</label>
						<div class="col-md-8">
						<div class="">
							  <textarea id="editor2" name="answer" rows="10" cols="80"><?php echo e($homecontent->answer); ?></textarea>
					  </div>
							<?php if($errors->has('details')): ?>
									<span class="help-block">
										<strong><?php echo e($errors->first('details')); ?></strong>
									</span>
								<?php endif; ?>	
						</div>
					</div>
						<div class="form-group">
						<label class="control-label col-md-3">Attachment :</label>
						<div class="col-md-8">
						<div class="">
							 <!--<label class="file-upload btn btn-primary">
                Browse for file ... <input type="file" class="form-control fsc-input" style="opacity:0" id="attachment" name="attachment" placeholder="Select Document">
            </label>-->
             <img src="<?php echo e(asset('public/attachment','')); ?>/<?php echo e($homecontent->attachment); ?>" title="<?php echo e($homecontent->subject); ?>" alt="<?php echo e($homecontent->subject); ?>" width="100px">
					  </div>
								
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3"> </label>
						<div class="col-md-8">
						<div class="">
							
                <input type="checkbox"  id="click" name="click" value="1" placeholder="Select Document">
            <label for="click">Click</label>
            
					  </div>
								
						</div>
					</div>
						<div class="card-footer">
							<div class="row">
								<div class="col-md-8 col-md-offset-3">
								<!--	<input class="btn btn-primary icon-btn" type="submit" name="submit" value="save">-->
								</div>
							</div>
						</div>
						
					</form>
				</div>
			</div>
		</div>
	</div>
 </section>	
<!--</div>-->
<script>
    $(document).ready(function(){
       $('#type').on('change', function(){
         if($('#type').val()=='Resposibilty')
         {
             $('.emp').show();
         }
         else
         {
            $('.emp').hide();  
         }
         
       });
    });
    
</script>
<style>
    input[type="file"] {
    display: block;
    position: absolute;
}
</style>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>