<?php $__env->startSection('main-content'); ?>
<style>
    .Branch1 {
    /*width: 70%;*/
    /*! float: left; */
    /*! margin: 0 0 12px 0; */
    text-align: center;
    background: #fff2b3 !important;
    border: 2px solid #103b68;
    padding: 4px 0;
    margin: auto;
    /*! margin-top: 0px; */
    /*! margin: 0 0 10; */
}

.Branch1 h1 {
    padding: 0px;
    margin: 0px 0 0 0;
    color: #103b68 !important;
    font-size: 22px;
}
ul.nav.nav-tabs.tabs1 {
        padding: 12px;
        background: #fffaf4 !important;
        border: 1px solid #df820f !important;
    }
    ul.nav.nav-tabs.tabs1>li>a {
        background: #ffe2bf !important;
        color: black !important;
        border: 1px solid #ff9a1e !important;
    }
    ul.nav.nav-tabs.tabs1 > li.active > a, ul.nav.nav-tabs.tabs1 > li.active > a:focus, ul.nav.nav-tabs.tabs1 > li.active > a:hover {
        border: 1px solid #945201;
        background: #ff9a1e !important;
        color: #ffffff !important;
    }

</style>
<div class="content-wrapper">
    <section class="page-title content-header">
     		<h1>Bank Information<span class="pull-right">Add</span></h1>
    </section>
    <section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-success">
			      <div class="box-header">
              <div class="box-tools pull-right">
              </div>
            </div>
				<div class="col-md-12">
				    <div class="panel with-nav-tabs panel-primary">
                        <div class="panel-heading">
                            <ul class="nav nav-tabs" id="myTab">
                                <li class="active"><a href="#tab1primary" data-toggle="tab" aria-expanded="true">Bank Info</a></li>
                                <li class=""><a data-toggle="tab" href="#tab2primary" aria-expanded="false">Branch Info</a></li>
                                <li class=""><a href="#tab3primary" data-toggle="tab" aria-expanded="false">Other</a></li>
                            </ul>
                        </div>
                    <form method="post" action="<?php echo e(route('bankingsetup.store')); ?>" class="form-horizontal" enctype="multipart/form-data">
                        <?php echo e(csrf_field()); ?>

                        <div class="panel-body">
                            <div class="tab-content">
                                <div class="tab-pane fade in active" data-step="0" id="tab1primary" >
                                    <div class="Branch1">
										<h1>Bank Information</h1>
									</div>
									<br>
									<div class="form-group <?php echo e($errors->has('bankname') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Bank Name : <span class="star-required">*</span></label>
							<div class="col-lg-6 col-md-8">
								<input name="bankname" type="text" id="bankname" class="form-control p-l-10" />                                                            
								<?php if($errors->has('bankname')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('bankname')); ?></strong>
										</span>
								<?php endif; ?>
							</div>
						</div>
						
					    <div class="form-group <?php echo e($errors->has('bankaddress') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3"> Address : <span class="star-required">*</span></label>
							<div class="col-lg-6 col-md-8">
								<input name="bankaddress" type="text" id="bankaddress" class="form-control p-l-10" />                                                            
								<?php if($errors->has('bankaddress')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('bankaddress')); ?></strong>
										</span>
								<?php endif; ?>
							</div>
						</div>
	                    				
					    <div class="form-group <?php echo e($errors->has('routingnumber') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3"> Bank Routing Number : <span class="star-required">*</span></label>
							<div class="col-lg-6 col-md-8">
								<input name="routingnumber" type="text" id="routingnumber" class="form-control p-l-10" />                                                            
								<?php if($errors->has('routingnumber')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('routingnumber')); ?></strong>
										</span>
								<?php endif; ?>
							</div>
						</div>
					
					    
                        <div class="form-group">
                            <label class="control-label col-md-3">Bank Tel. Number: <span class="star-required">*</span></label>
                            <div class="col-lg-6 col-md-8">
                                <div class="row">
                                <div class="col-md-6">
                                    <input name="contactnumber" type="text" id="contactnumber" class="form-control p-l-10" />                                                            
                                    <?php if($errors->has('contactnumber')): ?>
                                    <span class="help-block">
                                    <strong><?php echo e($errors->first('contactnumber')); ?></strong>
                                    </span>
                                    <?php endif; ?>
                                </div>
                                <div class="col-md-6">
                                    <select class="form-control" name="bankphonetype">
                                        <option value="">Select</option>
                                        <option value="Main">Main</option>
                                        <option value="Support">Support</option>
                                        <option value="CSR">CSR</option>
                                    </select>
                                </div>
                                </div>
                            </div>
                        </div>     
					    
					        
					    <div class="form-group <?php echo e($errors->has('bankwebsite') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Website :</label>
							<div class="col-lg-6 col-md-8">
								<input name="bankwebsite" type="text" id="bankwebsite" class="form-control p-l-10" />                                                            
								<?php if($errors->has('bankwebsite')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('bankwebsite')); ?></strong>
										</span>
								<?php endif; ?>
							</div>
						</div>   
                                </div>
                                <div class="tab-pane fade" data-step="0" id="tab2primary" >
                                    <div class="Branch1">
														<h1>Branch Information</h1>
													</div>
					    
					     	<div class="form-group" style="margin-top:20px;">
							<label class="control-label col-md-3">Branch Name : <span class="star-required">*</span></label>
							<div class="col-lg-6 col-md-8">
								<input name="branchname" type="text" id="" class="form-control" />                                                            
							</div>
						</div>
						
					    <div class="form-group">
							<label class="control-label col-md-3">Branch Address : <span class="star-required">*</span></label>
							<div class="col-lg-6 col-md-8">
								<input name="branchaddress" type="text" id="" class="form-control" />                                                            
								
							</div>
						</div>
	                   
	                   <div class="form-group">
							<label class="control-label col-md-3">City / State / Zip : <span class="star-required">*</span></label>
							<div class="col-lg-6 col-md-8">
    							<div class="row">
    							<div class="col-md-4">
    								<input name="branchcity" type="text" id="" class="form-control" />                                                            
    							</div>
    							<div class="col-md-4 col-xs-6">
    								<select class="form-control" name="branchstate" />
    								<option value="">Select</option>
    							    <?php $__currentLoopData = $statedata; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $state): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    							    <option value="<?php echo e($state->code); ?>"><?php echo e($state->code); ?></option>\
    							    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    								</select>
    							</div>
    							<div class="col-md-4 col-xs-6">
    								<input name="branchzip" id="zip" type="text"  class="form-control" />                                                            
    							</div>
    							</div>
							</div>
						</div>
	                   
	                   <div class="form-group">
							<label class="control-label col-md-3">Branch Contact Name / Position : <span class="star-required">*</span></label>
							<div class="col-lg-6 col-md-8">
							<div class="row">
							<div class="col-md-6">
								<input name="branchcontactname" type="text" id="" class="form-control" />                                                            
							</div>
							<div class="col-md-6">
								<select class="form-control" name="position"  />
    								<option value="">Select</option>
    								<option value="Manager">Manager</option>
    								<option value="Teller">Teller</option>
    								<option value="Personal Banker">Personal Banker</option>
    								<option value="Other">Other</option>
								</select>
							</div>
							</div>
							</div>
						</div>
	                  
	                    <div class="form-group">
							<label class="control-label col-md-3">Telephone 1 # : <span class="star-required">*</span></label>
							<div class="col-lg-6 col-md-8">
    							<div class="row">
    							<div class="col-md-6">
    								<input name="telephone1" type="text"  id="branchtelephone1" class="form-control" />                                                            
    							</div>
    							<div class="col-md-6">
    								<select class="form-control" name="telephone1type" />
    									<option value="">Select</option>
        								<option value="Office">Office</option>
        								<option value="Mobile">Mobile</option>
    								</select>
    							</div>
    							</div>
							</div>
						</div>
	                 
	                    <div class="form-group">
							<label class="control-label col-md-3">Telephone 2 # : <span class="star-required">*</span></label>
							<div class="col-lg-6 col-md-8">
							<div class="row">
							<div class="col-md-6">
								<input name="telephone2" type="text" id="branchtelephone2" class="form-control" />                                                            
								
							</div>
							<div class="col-md-6">
								<select class="form-control" name="telephone2type" />
									<option value="">Select</option>
							
								<option value="Office">Office</option>
								<option value="Mobile">Mobile</option>
								
								</select>
							</div>
							</div>
							</div>
						</div>
						
						   	<div class="form-group">
							<label class="control-label col-md-3">Email : <span class="star-required">*</span></label>
							<div class="col-lg-6 col-md-8">
								<input name="email" type="text" id="" class="form-control" />                                                            
							</div>
						</div>
					
	                      	<div class="form-group">
							<label class="control-label col-md-3">Notes :</label>
							<div class="col-lg-6 col-md-8">
								<textarea name="notes" cols="40" rows="4" class="form-control" /></textarea>                                                            
							</div>
						</div>
                                </div>
                                <div class="tab-pane fade" data-step="0" id="tab3primary" >
                                    <ul class="nav nav-tabs tabs1" role="tablist">
                                            <li role="presentation" class="active"><a href="#conv_sheet_tab" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="true"> <span>Conversation Sheet</span></a></li>
                                            <li role="presentation"><a href="#notes_tab" aria-controls="settings" role="tab" data-toggle="tab" aria-expanded="true"><span>Notes</span></a></li>
                                        </ul>
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane active" id="conv_sheet_tab">
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    				<div class="Branch" style="text-align:left; padding-left:15px;">
                                    					<h1 class="text-center">Conversation Sheet</h1>
                                    				</div>
                                    			</div>
                                     <br>
                                    
                                    <div class="col-md-12 col-sm-12 col-xs-12 text-right">
                                    
                                                    <div class="clear"></div>
                                                    <div class="table-responsive">
                                                     <table class="table table-hover table-bordered dataTable no-footer">
                                    		           <thead>
                                    		               <tr>
                                    		                    <th>No.</th>
                                    							<th> Date - Day - Time</th>
                                    							<th>Related To</th>
                                    							<th>Description</th>
                                    							<th>Notes</th>
                                    							<th>Action</th>
                                    		               </tr>
                                    		           </thead>
                                    		           <tbody>
                        						                    
                    						            </tbody>
                                    		           
                                    		       </table>
                                    		       </div>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="notes_tab">
    											<ul class="nav nav-tabs" role="tablist">
                                                    <li role="presentation" class="active"><a href="#temp_note" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="true"> <span> Temporary Note </span></a></li>
                                                    <li role="presentation"><a href="#per_note" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="true"><span> Permenant Note </span></a></li>
                                                </ul>
                                                <div class="tab-content">
                                                    <div role="tabpanel" class="tab-pane active" id="temp_note">
                                                        <div class="col-md-12 col-sm-12 col-xs-12">
            												<div class="Branch" style="text-align:left; padding-left:15px;">
            													<h1 class="text-center">Temporary Note</h1>
            												</div>
            											</div>
                                            				<br>
                                            				<div class="col-md-12 col-sm-12 col-xs-12 text-right">
            						                        <div class="clear"></div>
            						                         <table class="table table-hover table-bordered dataTable no-footer">
                            						           <thead>
                            						               <tr>
                            						                    <th>No.</th>
                                    									<th>Date - Day - Time</th>
                                    									<th>Related To</th>
                                    									<th>Description</th>
                                    									<th>Notes</th>
                                    									<th>Action</th>
                            						               </tr>
                            						           </thead>
                            						           <tbody>
                        						                    
                            						            </tbody>
                            						           
                            						       </table>
            						                    </div>
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane" id="per_note">
                                                        <div class="col-md-12 col-sm-12 col-xs-12">
            												<div class="Branch" style="text-align:left; padding-left:15px;">
            													<h1 class="text-center">Permenant Note</h1>
            												</div>
            											</div>
                                            				<br>
                                            				<div class="col-md-12 col-sm-12 col-xs-12 text-right">
            						                        <div class="clear"></div>
            						                         <table class="table table-hover table-bordered dataTable no-footer">
                            						           <thead>
                            						               <tr>
                            						                    <th>No.</th>
                                    									<th>Date - Day - Time</th>
                                    									<th>Related To</th>
                                    									<th>Description</th>
                                    									<th>Notes</th>
                                    									<th>Action</th>
                            						               </tr>
                            						           </thead>
                            						            <tbody>
                        						                    
                            						            </tbody>
                            						           
                            						       </table>
            						                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                        <div class="card-footer">
						    <div class="form-group">
    							<label class="control-label col-md-3"></label>
    							<div class="col-xs-2" style="width:155px;">
                                    <input class="btn_new_save" type="submit" name="submit" value="Save">
    							</div>
    							<div class="col-xs-2" style="width:155px;">
                                    <a class="btn_new_cancel" href="<?php echo e(url('fac-Bhavesh-0554/bankingsetup')); ?>">Cancel</a> 
    							</div>
    						</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	</section>
</div>
<style>
.select2-container--default .select2-selection--single .select2-selection__rendered{
    font-size:16px !important;
    padding:0;
     color:#000;
}
.p-l-10{ padding-left:10px;}
.select2-container--default .select2-selection--single .select2-selection__arrow b{ border-color:#000 transparent transparent transparent; }
.select2-container--default .select2-selection--single .select2-selection__arrow{ top:6px; right:4px; }
.select2-container {
    box-sizing: border-box;
    display: inline-block;
    margin: 0;
    position: relative;
    vertical-align: middle;
    width: 100% !important;
}.select2-container--default .select2-selection--single {
    background-color: #fff;
    /* border: 1px solid #aaa; */
    border-radius: 4px;
    border: 2px solid #2fa6f2;
    height:40px;
    padding:8px;
}</style>

<script>
$("#contactnumber").mask("(999) 999-9999");
$("#branchtelephone1").mask("(999) 999-9999");
$("#branchtelephone2").mask("(999) 999-9999");
$("#zip").mask("99999");
$("#routingnumber").mask("999999999");




</script> 


<script>
    $('.js-example-tags').select2({
    tags: true,
    tokenSeparators: [",", " "]
});

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>