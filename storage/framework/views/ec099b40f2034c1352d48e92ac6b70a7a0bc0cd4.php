<?php $__env->startSection('main-content'); ?>
<style>
    .main-box-border h3{ background:#ccffcc; padding:10px; }
    .box-border-bg{ padding: 15px; min-height:310px; margin-bottom:20px; border: solid 1px #333;}
    .box-border-bg-bottom { min-height:345px; padding: 15px; margin-bottom:20px; border: solid 1px #333; }
    .box-border-bg h4{ background:#ccffff ; font-size:18px; padding:10px; border:2px solid #103b68; }
    .box-border-bg h5{ background:#ffff99 ; font-size:18px; padding:10px; border:2px solid #103b68; }
    .box-border-bg-bottom h4{ background:#ccffff ; font-size:18px;  padding:10px; border:2px solid #103b68;}
    .box-border-bg-bottom h5{ background:#ffff99 ; font-size:18px; padding:10px; border:2px solid #103b68; }
    .content-wrapper{ min-height:900px !important; }
    .box-border-bg .control-label{ padding-right:0; }
    .box-border-bg-bottom .control-label{ padding-right:0; }
    .ex2{pointer-events:none; background:#ccc !important;}
    .form-horizontal .control-label.text-left{ text-align:left; }
    .form-control{ font-size:15px !important; }
    .date-m .form-control{ font-size:13px !important; }
    .date-m.date-width{ width:26%  !important; }
    .date-m.day-width{ width:19.5%  !important; }
    .date-m.time-width{ width:21%  !important; }
    
@media(max-width:1200px){
    .box-border-bg-bottom{
        min-height:auto;
    }
}
@media(max-width:991px){
    .box-border-bg-bottom .control-label{ width:100%; text-align:left; }
    .date-m.date-width{ width:35%  !important; float:left; }
    .date-m.day-width{ width:27%  !important; float:left; }
    .date-m.time-width{ width:38%  !important; float:left; }
    .btn_new_save{ margin-left:0 !important; }
}

@media(max-width:767px) 
{
    .btn_new_save{ margin-left:0 !important; }
    .date-m.date-width{ width:35%  !important; float:left; }
    .date-m.day-width{ width:27%  !important; float:left; }
    .date-m.time-width{ width:38%  !important; float:left; }
    .date-m .form-control{ font-size:12px !important; }
    .control-label{ width:100%; }
    /*.content-wrapper{ display:table; }*/
}
</style>
<div class="content-wrapper">
      <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>Message</h1>
    </section>
    <!-- Main content -->
    <section class="content">	
   <div class="row">
      <div class="col-md-12">
         <div class="box box-success">
			      <div class="box-header">
              <div class="box-tools pull-right">
              </div>
            </div>
<form method="post" action="<?php echo e(route('msg.update',$task->id)); ?>" class="form-horizontal" id="content" name="content" enctype="multipart/form-data">
                  <?php echo e(csrf_field()); ?> <?php echo e(method_field('PATCH')); ?>

            <div class="col-sm-12">
            <div class="row">
            <div class="col-sm-12 col-sx-12 col-lg-6 main-box-border">
                <div class=" box-border-bg-bottom">
                  <div class="text-center"><h4> Message Rec'd Info </h4></div>
                   <div class="form-group">
                   <label class="control-label col-md-4">Date-Day-Time :</label>
                     <div class="col-md-2 date-m date-width">
                        <div class="">
                           <input type="text" name="date" id="date11" style="pointer-events:none; background:#ccc !important;" class="form-control" placeholer="Date" value="<?php echo e($task->date); ?>">
                        </div>
                     </div>
                      <div class="col-md-2 date-m day-width">
                        <div class="row">
                            <input type="text" name="day" id="day" class="form-control"  style="pointer-events:none; background:#ccc !important;" placeholer="Day" value="<?php echo e($task->day); ?>">
                        </div>
                     </div>
                     <div class="col-md-2 date-m time-width">
                        <div class="">
                            <input type="text" name="time" id="time" class="form-control"  style="pointer-events:none; background:#ccc !important;" placeholer="Time" value="<?php echo e($task->time); ?>">
                        </div>
                     </div>
                </div>
                <div class="form-group">
                   <label class="control-label col-md-4">Rec'd By :</label>
                    <div class="col-md-8">
                        <div class="">
                            <select name="client_name1" id="client_name1" class="form-control fsc-input" style="display:none">
                              <option value="">---Select---</option>
                              <?php $__currentLoopData = $client; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $clien): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value="<?php echo e($clien->id); ?>" <?php if($task->client_name==$clien->id): ?> selected <?php endif; ?>><?php echo e($clien->firstName.' '.$clien->middleName.' '.$clien->lastName); ?></option>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                              </select>
                            
                          <input type="text" readonly  <?php $__currentLoopData = $client; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $clien): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php if($task->employee_id==$clien->id): ?> value="<?php echo e($clien->firstName.' '.$clien->middleName.' '.$clien->lastName); ?>"  <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> class="form-control" style="pointer-events:none; background:#ccc !important;">
                        </div>
                    </div>
                </div>
                  <div class="form-group">
                     <label class="control-label col-md-4">Msg. For Whom :</label>
                     <div class="col-md-8">
                         <?php 
                         if(isset($emp2))
                         {
                             $mfrom=ucwords($emp2->firstName.' '.$emp2->middleName.' '.$emp2->lastName);
                         }?>
                        <div class="">
                          <input type="text" name="busname1" readonly id="busname1" value="<?php if(empty($userclient1->first_name)): ?><?php if(isset($emp2)): ?><?php echo e(ucwords($emp2->firstName.' '.$emp2->middleName.' '.$emp2->lastName)); ?><?php endif; ?> <?php if(isset($userclient1)): ?><?php echo e(ucwords($userclient1->first_name.' '.$userclient1->middle_name.' '.$userclient1->last_name)); ?><?php endif; ?> <?php endif; ?>" class="form-control" style="pointer-events:none; background:#ccc !important;">
                        </div>
                     </div>
                  </div>
            </div>
            </div> 
            <div class="col-sm-12 col-sx-12 col-lg-6 main-box-border">
                <div class=" box-border-bg-bottom">
                <div class="text-center"><h5> Message From </h5></div>
                   <div class="form-group">
                   <label class="control-label col-md-4">Name :</label>
                     <div class="col-md-8">
                        <div class="">
                        <input type="text" name="clientname" readonly value="<?php if(empty($emp3)): ?> <?php if(empty($userclient2)): ?> <?php else: ?><?php echo e(ucwords($userclient2->first_name.' '.$userclient2->middle_name.' '.$userclient2->last_name)); ?> <?php endif; ?> <?php echo e($task->clientname); ?> <?php else: ?> <?php echo e(ucwords($emp3->firstName.' '.$emp3->middleName.' '.$emp3->lastName)); ?>   <?php endif; ?>" id="clientname" class="form-control"  style="pointer-events:none; background:#ccc !important;">
                        </div>
                     </div>
                    </div>
                    <div class="form-group">
                   <label class="control-label col-md-4">Client # :</label>
                     <div class="col-md-8">
                        <div class="">
                           <input type="text" name="clientfile" readonly id="clientfile" value="<?php echo e($task->clientfile); ?>" class="form-control"  style="pointer-events:none; background:#ccc !important;">
                        </div>
                     </div>
                    </div>
                <div class="form-group">
                   <label class="control-label col-md-4">Company Name :</label>
                    <div class="col-md-8">
                          <div class="">
                              <?php $name = $task->busname2; $name_explode = explode(" , ",$task->busname2); ?>
                               <input type="text" name="company" id="company" class="form-control fsc-input"  style="pointer-events:none; background:#ccc !important;" <?php $__currentLoopData = $userclient; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $clien1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?><?php $__currentLoopData = $name_explode; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php if($cat ==$clien1->id): ?>value="<?php echo e($clien1->company_name); ?>" <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>/>
                        </div>
                    </div>
                </div>
                 <div class="form-group">
                   <label class="control-label col-md-4">Call Purpose :</label>
                    <div class="col-md-8">
                          <div class="">
                               <input name="purpose" id="purpose2" class="form-control fsc-input" value="<?php echo e($task->purpose); ?>" style="pointer-events:none; background:#ccc !important;">
                        </div>
                    </div>
                </div>
                 <div class="form-group">
                   <label class="control-label col-md-4">Telephone :</label>
                     <div class="col-md-8">
                        <div class="">
                           <input type="text" name="clientno" readonly id="clientno" value="<?php echo e($task->clientno); ?>" class="form-control"  style="pointer-events:none; background:#ccc !important;">
                        </div>
                     </div>
                    </div>
            </div>
            </div>
            </div>
            
            <div class="row">
            <label class="control-label col-md-12 text-left">Message Detail :</label>    
            <div class="col-sm-12">
                    <textarea type="text" name="description" readonly="" id="description" value="" class="form-control" style="background:#fff !important; pointer-events:none; text-align:left; margin:0px 0 20px;"><?php echo $task->content; ?></textarea>
            </div>
            </div>
            <div class="row">
            <div class="col-sm-12 col-sx-12 col-lg-6 main-box-border">
                <div class=" box-border-bg-bottom">
                  <div class="text-center"><h4>Return Call Info	 </h4></div>
                   <div class="form-group">
                   <label class="control-label col-md-4">Status :</label>
                     <div class="col-md-8">
                        <div class="">
                            <select name="status1" id="status1" class="form-control fsc-input <?php if($task->status1=="Done"): ?> Green <?php endif; ?>">
                              <option value="">---Select---</option>
                              <option value="On Hold" class="Yellow" <?php if($task->status1=="On Hold"): ?> selected <?php endif; ?>>On Hold</option>
                              <option value="Forword" class="Red"  <?php if($task->status1=="Forword"): ?> selected <?php endif; ?>>Forword</option>
                              <option value="Under Progress" class="Blue" <?php if($task->status1=="Under Progress"): ?> selected <?php endif; ?>>Under Progress</option>
                              <option value="Done" class="Green" <?php if($task->status1=="Done"): ?> selected <?php endif; ?>>Done</option>
                           </select>
                        </div>
                     </div>
                </div>
                <div class="form-group">
                   <label class="control-label col-md-4">Msg. Return Date-Day-Time :</label>
                     <div class="col-md-2 date-m date-width">
                        <div class="">
                            <input type="text" name="returndate" value="<?php if(empty($task->returndate)): ?><?php echo e(date('m-d-Y')); ?> <?php else: ?> <?php echo e($task->returndate); ?> <?php endif; ?>" id="date" class="form-control">
                        </div>
                     </div>
                      <div class="col-md-2 date-m  day-width">
                        <div class="row">
                           <input type="text" name="returnday" id="returnday" class="form-control"  placeholer="Day" value="<?php if(empty($task->returnday)): ?><?php echo e(date('l')); ?> <?php else: ?> <?php echo e($task->returnday); ?> <?php endif; ?>">
                        </div>
                     </div>
                     <div class="col-md-2 date-m  time-width">
                        <div class="">
                           <input type="text" name="returntime" value="<?php if(empty($task->returntime)): ?><?php echo e(date('g:i a')); ?> <?php else: ?> <?php echo e($task->returntime); ?> <?php endif; ?>" id="time" class="form-control">
                        </div>
                     </div>
                </div>
                   <div class="form-group">
                     <label class="control-label col-md-4">Note :</label>
                     <div class="col-md-8">
                        <div class="">
                          <input type="text" name="notes" value="<?php echo e($task->notes); ?>" id="notes" class="form-control">
                        </div>
                     </div>
                  </div>
        </div>
            </div> 
            <div class="col-sm-12 col-sx-12 col-lg-6  main-box-border">
                <div class=" box-border-bg-bottom">
                  <div class="text-center"><h5>Message Forward Info</h5></div>
                   <div class="form-group">
                   <label class="control-label col-md-4">Msg. Forward To : <span class="star" style="color:red;display:none">*</span></label>
                     <div class="col-md-8">
                        <div class="">
                        <select name="client_name" id="client_name" class="form-control fsc-input ex2" style="">
                              <option value="">---Select---</option>
                              <?php $__currentLoopData = $client; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $clien): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value="<?php echo e($clien->id); ?>" <?php if($clien->id==$task->forword_id): ?> selected <?php endif; ?>><?php echo e($clien->firstName.' '.$clien->middleName.' '.$clien->lastName); ?></option>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                              </select>
                        </div>
                     </div>
                    </div>
                <div class="form-group">
                     <label class="control-label col-md-4">What To Do :</label>
                     <div class="col-md-8">
                        <div class="">
                          <input type="text" name="whattodo" value="<?php echo e($task->whattodo); ?>" id="whattodo" class="form-control ex2">
                        </div>
                     </div>
                  </div> 
                  <div class="form-group">
                     <label class="control-label col-md-4">Instruction :</label>
                     <div class="col-md-8">
                        <div class="">
                          <input type="text" name="instruction" value="<?php echo e($task->instruction); ?>" id="instruction" class="form-control ex2">
                        </div>
                     </div>
                  </div> 
                <div class="form-group">
                     <label class="control-label col-md-4">Note :</label>
                     <div class="col-md-8">
                        <div class="">
                          <input type="text" name="forwordmsg" value="<?php echo e($task->forwordmsg); ?>" id="forwordmsg" class="form-control ex2">
                        </div>
                        	
                     </div>
                  </div>
                </div>
            </div>
            </div>
            </div>
            <div class="col-sm-12">
                <div class="card-footer">
                     <div class="col-md-2 col-md-offset-4">
                         <?php if($task->status1=="Forword"): ?> 	<a class="btn_new_save btn-primary1 client" href="">Sent</a> <?php else: ?> 	<input class="btn_new_save btn-primary1 client" type="submit" name="submit" value="Save"> <?php endif; ?>
								
									<input class="btn_new_save btn-primary1 client2" style="display:none" type="submit" name="submit" value="Forword">
									</div>
									<div class="col-md-2 row" style="padding-left:30px !important;">
									<a class="btn_new_cancel" style="width:150px !important;" href="<?php echo e(url('fac-Bhavesh-0554/msg')); ?>">Cancel</a> 
									</div>
                  </div>
            </div>
            </form>
         </div>
      </div>
   </div>
      </section>
<!--</div>-->
<script>
$(document).ready(function(){
	$(document).on('change','#type', function()
	{
		var id = $(this).val();
if(id=='Other Person')
{
    $('.client').show();
    $('.client1').show();
    document.getElementById('busname').removeAttribute('readonly');
    document.getElementById('clientname').removeAttribute('readonly');
    document.getElementById('clientfile').removeAttribute('readonly');
    document.getElementById('clientno').removeAttribute('readonly');
}
$.get('<?php echo URL::to('/clientid'); ?>?id='+id, function(data)
		{  
            $('#employee').empty();
          //  $('#clientname').empty();
           // $('.client').hide(); 
           // $('#clientname').append('<option value="">---Select---</option>');
           $.each(data, function(index, subcatobj)
		   {
if(id=='employee')
{
  $('#employee').append('<option value="' + subcatobj.id + '">' + subcatobj.firstName + ' ' + subcatobj.middleName + ' ' + subcatobj.lastName + '</option>');  
}
if(id=='Approval')
{
 $('#employee').append('<option value="' + subcatobj.id + '">' + subcatobj.first_name + ' ' + subcatobj.middle_name + ' ' + subcatobj.last_name + '</option>');
}	
		   })
		});
	});
});
$(document).ready(function(){
	$(document).on('change','#status1', function() {
	  var id = $(this).val(); //alert(id);
	  if(id =='Forword')
	  {
	   $('#client_name').removeClass('ex2');
	   $('#whattodo').removeClass('ex2');
	   $('#instruction').removeClass('ex2');
	   $('#forwordmsg').removeClass('ex2');
	   $("#client_name").attr('required', '');
	   $(".star").show();
	  }
	  else{
	      $("#client_name").removeAttr('required', '');
	      $(".star").hide();
	   $('#client_name').addClass('ex2');  
	   $('#whattodo').addClass('ex2');
	   $('#instruction').addClass('ex2');
	   $('#forwordmsg').addClass('ex2');
	  }
	});
});
</script>
<script>
$(document).ready(function(){
	$(document).on('change','#employee', function()
	{
	    var selectedCountry = $("#type option:selected").val();
		var id = $(this).val(); //alert(selectedCountry);
		$.get('<?php echo URL::to('/clientid'); ?>?id='+id+'&state=' + selectedCountry, function(data)
		{  
           $('#clientname').empty();
           $('#clientname').empty();
           $('#clientno').empty();
           $('#busname').empty();
           $('#clientfile').empty();
           $.each(data, function(index, subcatobj)
		   {
            document.getElementById('busname').readOnly =true;
            document.getElementById('clientname').readOnly =true;
            document.getElementById('clientfile').readOnly =true;
            document.getElementById('clientno').readOnly =true;	
        //   alert(subcatobj.type);
if('employee'==subcatobj.type)
{
$('.client').show();
$('.client1').show();
$('#clientname').val(subcatobj.firstName);
$('#clientno').val(subcatobj.telephoneNo1);
$('#busname').val(subcatobj.business_name);
$('#clientfile').val(subcatobj.employee_id); 
}
if('Approval'==subcatobj.status)
{
     $('.client').show();
      $('.client1').show();
$('#clientname').val(subcatobj.first_name);
$('#clientno').val(subcatobj.business_no);
$('#busname').val(subcatobj.business_name);
$('#clientfile').val(subcatobj.filename);
}	
	})
	});
	});
});
</script>
 <script type="text/javascript">
 $( "#date" ).datepicker({
    'dateFormat':'yy-mm-dd',
    onSelect: function(dateText){ alert();
        var seldate = $(this).datepicker('getDate');
        seldate = seldate.toDateString();
        seldate = seldate.split(' ');
        var weekday=new Array();
            weekday['Mon']="Monday";
            weekday['Tue']="Tuesday";
            weekday['Wed']="Wednesday";
            weekday['Thu']="Thursday";
            weekday['Fri']="Friday";
            weekday['Sat']="Saturday";
            weekday['Sun']="Sunday";
        var dayOfWeek = weekday[seldate[0]];
        $('#day').val(dayOfWeek);
    }
});
  $(document).ready(function() {
    $("#date").change(function() {
      var startdate= $("#date").val();
      var monthNames = [
        "Jan", "Feb", "Mar",
        "Apr", "May", "Jun", "Jul",
        "Aug", "Sep", "Oct",
        "Nov", "Dec"
      ];
    var weekday = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
      var durtion=$('#duration').val();
      var  date = new Date(startdate);
      var day = weekday[date.getDay()];
      var monthly=30;
      var weekly=7;
      var bimonthly=15;
      var biweekly=14;
      var monthss=monthNames[(date.getMonth())];
      var yearss=date.getFullYear();
      var yyy=yearss % 4 ;
    
      if(durtion == "Weekly")
      {
        var totaldays=6;
      }
      else if(durtion == "Monthly")
      {
        if(monthss == 'Jan' || monthss == 'Mar' || monthss == 'May' || monthss == 'Jul' || monthss == 'Aug' || monthss == 'Oct' || monthss == 'Dec')
        {
          var totaldays=30;
        }
        else if(monthss == 'Feb')
        {
          //if(years / 4 = 0)
          if(yyy ==0)
          {
            var totaldays=28;
          }
          else if(yyy ==1)
          {
            var totaldays=27;
          }
        }
        else if(monthss == 'Apr' || monthss == 'Jun' || monthss == 'Sep' || monthss == 'Nov' )
        {
          var totaldays=29;
        }
      }
      else if(durtion == "Bi-Weekly")
      {
        var totaldays=13;
      }
      else if(durtion == "Bi-Monthly")
      {
        var totaldays=14;
      }
      date.setDate(date.getDate() + totaldays);// alert(vv);
      var date1 = ("0" + (date.getMonth() + 1)).slice(-2)  + "/" + ("0" + date.getDate()).slice(-2) + "/" + date.getFullYear();// alert(date.getDate())
     // alert(date1);
      var newdate = new Date(date1);
        var day1 = weekday[newdate.getDay()];
       //alert(newdate);
      var date2=monthNames[(date.getMonth())] + "/" + date.getDate() + "/" + date.getFullYear() ;
     // $('#sch_end_date').val(date1);
      $('#day').val(day);
      $('#sch_end_day').val(day1);
      //document.write(date2);
    });
    $("#duration").change(function() {
      $('#sch_end_date').val('');
      $('#sch_start_date').val('');
    });
  });
</script><!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog"><!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Call Purpose</h4>
      </div>
      <div class="modal-body" style="display: inline-table;">
           <form action="" method="post" id="ajax2">
                <?php echo e(csrf_field()); ?>

        <div class="form-group">
                   <label class="control-label col-md-3">Call Purpose :</label>
                     <div class="col-md-6">
                        <div class="">
                            <input type="text" name="newopt" id="newopt" class="form-control" placeholder="Call Purpose">
                        </div>
                     </div>
                     <div class="col-md-2">
                        <div class="">
                            <input type="button" id="addopt" class="btn btn-primary" value="Add Call Purpose">
                        </div>
                     </div>
                  </div>
                  </form>
                  <div class="form-group">
                   <label class="control-label col-md-3"></label>
                     
                  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script>
          $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  }
              });
             $(function () {
                $('#addopt').click(function () { //alert();
                    var newopt = $('#newopt').val();
                    if (newopt == '') {
                        alert('Please enter something!');
                        return;
                    }

                   //check if the option value is already in the select box
                    $('#purpose1 option').each(function (index) {
                        if ($(this).val() == newopt) {
                            alert('Duplicate option, Please enter new!');
                        } })
                    $.ajax({
        type: "post",
        url: "<?php echo route('purpose.purposes'); ?>",
        dataType: "json",
        data: $('#ajax2').serialize(),
        success: function(data){
             alert('Successfully Add');
             $('#purpose2').append('<option value=' + newopt + '>' + newopt + '</option>');
             $("#div").load(" #div > *");
             $("#newopt").val('');
        },
        error: function(data){
             alert("Error")
        }
    });
    $('#myModal').modal('hide');
                });
            });
</script>
<style>
    .select2-container .select2-selection--single {
    box-sizing: border-box;
    cursor: pointer;
    display: block;
    height: 39px; border-redius:4px;
    user-select: none;
    -webkit-user-select: none;
}
    .select2 {width:100% !important;}
    .select2-container .select2-selection--single {
   
    border: 2px solid #00468F;
}
.form-control1 {
    width: 100%;
    line-height: 1.44;
    color: #555!important;
    border: 2px solid #286db5;
    border-radius: 3px;
    transition: border-color ease-in-out .15s;
    padding: 3px 3px 7px 8px!important;
}
.Red{background-color:#ffff99;color:#000}
.Blue{background-color:rgb(124, 124, 255) !important;color:#fff}
.Green{background-color:#00ef00 !important;color:#fff}
.Yellow{background-color:Yellow !important;}
</style>
<script language="javascript">
    $(document).ready(function () {
        $("#date").datepicker({
            minDate: 0,
        });
    });

$(document).ready(function(){
    $("select#status1").change(function(){
        var selectedCountry = $(this).children("option:selected").val();
        var color = $("option:selected", this).attr("class");
          $("#status1").attr("class", color).addClass("form-control1 fsc-input");
    });
});

function myFunction() {
  var checkBox = document.getElementById("myCheck");
  var text = document.getElementById("text");
  if (checkBox.checked == true){
    $('.client').hide();
    $('.client2').show();
  } else {
      $('.client2').hide();
      $('.client').show();
  }
}
</script>
<style>
.select2 {width:100% !important;}
.select2-container .select2-selection--single {border: 2px solid #00468F;}
</style>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>