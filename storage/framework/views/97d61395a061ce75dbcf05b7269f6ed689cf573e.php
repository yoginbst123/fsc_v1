
<?php $__env->startSection('main-content'); ?>
    <style>
        .new-page-title {
            margin: -9px 0 6px 0;
            padding: 0px !important;
        }

        .nav-tabs > li {
            width: 16.1% !important;
            margin: 2px 0 0 5px !important;
        }

        .form-control-1 {
            /*min-height:100px;*/
        }

        .msg {
            display: none;
        }

        .error {
            color: red;
        }

        .form-control, .form-control-insu {
            font-size: 15px !important;
        }

        .success {
            color: green;
        }

        .Location {
            display: none;
        }

        select {
            font-family: 'FontAwesome';
        }

        .pricetype {
            background: #fff;
            border-radius: 4px;
        }

        glyphicon-chevron-right:before {
            content: "\e080";
            color: #000;
        }

        .nav-tabs > li > a {
            height: 42px;
        }

        .clients-name-r {
            font-size: 14px;
            margin: 6px 0;
            background-color: #286db5;
            color: #fff;
            padding: 8px;
        }

        .b-i-top {
            max-width: 90px;
            margin-top: -12px;
        }

        .page-title {
            height: 75px;
        }

        .star-required {
            color: red;
            position: absolute;
        }

        .tess {
            text-align: right;
            position: absolute;
            font-size: 11px;
            margin: 22px 16px 0 0;
            right: 0;
            width: 100%;
        }

        .fieldGroup {
            width: 100%;
            display: inline-block;
            border-bottom: 2px solid #512e90;
            padding-bottom: 20px;
        }

        .form-control {
            height: 35px !important;
        }

        .fieldGroup:last-child {
            border-bottom: transparent;
        }

        .glyphicon-chevron-right {
            top: 5px;
            text-align: center;
            font-size: 2rem;
        }

        .fsc-form-label {
            display: block;
        }

        .Red {
            background-color: red !important;
            color: #fff !important
        }

        .Blue {
            background-color: rgb(124, 124, 255) !important;
            color: #fff !important
        }

        .Green {
            background-color: #00ef00 !important;
            color: #fff !important
        }

        .Yellow {
            background-color: Yellow !important;
            color: #555 !important
        }

        .Orange {
            background-color: Orange !important;
            color: #fff !important
        }

        .Cyan {
            background-color: #2bd6ce !important;
            color: #fff !important
        }

        .arrow {
            width: 0px;
            position: relative;
            float: left;
            right: 12px;
            top: 7px;
        }

        .nav-tabs {
            padding: 2px 0px 4px 0px;
        }

        /*.form-horizontal .form-group{margin-left:0px;}*/
        .form-control1 {
            width: 100%;
            line-height: 1.44;
            color: #555;
            border: 2px solid #286db5;
            border-radius: 3px;
            transition: border-color ease-in-out .15s;
            padding: 3px 3px 7px 8px !important;
        }

        .ser:nth-of-type(odd) {
            padding: 15px 0;
            width: 100%;
            background-color: #e8f7fd;
            margin: auto;
            border: solid 1px #d2c292;
        }

        .ser:nth-of-type(even) {
            background-color: #fff5dd;
            padding: 15px 0;
            width: 100%;
            margin: auto;
            border: solid 1px #d2c292;
        }

        .bg-color {
            background: #286db5;
            width: 100%;
            margin: 0 auto 16px auto !important;
            padding: 5px 0;
            color: #fff;
        }

        #tab3primary label {
            text-align: left;
        }

        .model-width {
            width: 84%;
            border: #3668f6 1px solid;
            border-radius: 0;
        }

        .model-width .modal-header {
            padding: 4px 10px;
            background: #fff;
        }

        .model-width .modal-header h4 {
            font-size: 16px;
        }

        .model-width .modal-footer {
            text-align: center;
        }

        .primary {
            background-color: #d0d0d0 !important;
            border-radius: 0px;
            color: #000;
            padding: 5px 10px;
            font-weight: 200px;
        }

        .fa-trash {
            font-size: 18px;
        }

        .input_fields_wrap {
            margin-top: 15px;
        }

        .card ul.test li a {
            background: #ffcc66 !important;
        }

        .card ul.test li.active a {
            background: #00a0e3 !important;
        }

        .card ul li a {
            display: block;
            width: 100%;
            color: #333;
            text-transform: capitalize;
            background: linear-gradient(180deg, #fdff9a 30%, #e3e449 70%);
            border: 1px solid #979800 !important;
        }

        .card ul li a:hover {
            /*   width: auto;*/
            /*height: 30px;*/
            /*vertical-align: middle;*/
            /*border-radius: 50px 0px 0px 50px;*/
            /*padding: 2px 10px;*/
            /*line-height: 1.9;*/
            /*border-right: 1px solid #000000;*/
        }

        .card ul li.active {
            color: #333;
            background: linear-gradient(180deg, #fbff37 30%, #d3d40e 70%);
            border: 1px solid #333 !important;
        }

        .card ul li a.active {
            color: #333;
            background: linear-gradient(180deg, #fbff37 30%, #d3d40e 70%);
            border: 1px solid #333 !important;
        }

        .card-footer {
            padding: 7px;
        }

        .active-width {
            width: 100px;
            float: right;
            margin-bottom: 0;
            margin-right: 10px !important;
        }

        .Branch h1 {
            line-height: 35px;
        }

        .education_service .customer-service-information-bg {
            padding: 0 0px;
        }

        .customer-service-information-bg .form-control-1 {
            width: 100%;
            padding: 10px;
        }

        .customer-service-information-bg .form-control {
            font-size: small !important;
            font-size: small !important;
        }

        .new_images_sec {
            margin: 12px 0 0 0;
        }

        .new_images_sec .img-responsive {
            height: 50px;
        }

        .new_images_sec .col-md-4:first-child {
            margin-left: -20px;
        }

        .education_service .form-group .control-label {
            font-size: 12px;
        }

        .ser .control-label {
            font-size: 12px;
        }

        .service label {
            font-size: 12px;
        }

        .service, .subscription_hidden, .subscription_hidden2, .subscription_hidden1 {
            display: none;
        }

        .disabledd {
            pointer-events: none;
        }

        .form-control[disabled] {
            background-color: lightgreen !important;
        }

        .clear {
            clear: both !important;
        }

        .switch-field {
            display: flex;
            /*	margin-bottom: 36px;
*/
            overflow: hidden;
            width: 44px;
            border: 1px solid #fff;
            margin: 0px auto;
        }

        .switch-field input {
            position: absolute !important;
            clip: rect(0, 0, 0, 0);
            height: 1px;
            width: 1px;
            border: 0;
            overflow: hidden;
        }

        .switch-field label {
            background-color: #e4e4e4;
            color: rgba(0, 0, 0, 0.6);
            font-size: 14px;
            line-height: 1;
            text-align: center;
            padding: 8px 16px;
            margin-right: -1px;
            border: 1px solid rgba(0, 0, 0, 0.2);
            box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.3), 0 1px rgba(255, 255, 255, 0.1);
            transition: all 0.1s ease-in-out;
        }

        .switch-field label:hover {
            cursor: pointer;
        }

        .switch-field input:checked + label {
            background-color: #a5dc86;
            box-shadow: none;
            display: none;
        }

        .switch-field label:first-of-type {
            border-radius: 4px 4px 4px 4px;
        }

        .switch-field label:last-of-type {
            border-radius: 4px 4px 4px 4px;
        }

        .form-control {
            font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif;
        }

        .switch-field label.radiounlock {
            background: #ff0000 !important;
        }

        .switch-field label.radiolock {
            background: #6bef21 !important;
        }

        .new_company_name p {
            font-size: 15px !important;
        }

        .form-horizontal .control-label {
            text-align: right;
        }

        .disabled.selectreadonly {
            background-color: #eee !important;
            pointer-events: none !important;
            -webkit-appearance: none !important;
            appearance: none !important;

        }

        .services2, .services3, .services4 {
            border: 1px solid #cccccc;
            padding: 15px 0px !important;
            margin-bottom: 15px !important;
        }

        .clear {
            clear: both;
        }

        #alertss .modal-width {
            margin: auto;
            width: 390px !important;
            border: #3668f6 1px solid !important;
            border-radius: 0;
        }

        .mt27 {
            margin-top: 27px !important;
        }

        .form-horizontal .control-label.yellowback {
            width: 100%;
            text-align: left !important;
        }

        div.dataTables_wrapper div.dataTables_length select.form-control.input-sm {
            padding: 0px !important;
        }

        .formbox {
            display: flex;
            flex-direction: row;
            justify-content: flex-start;
            align-items: flex-start;
            list-style: none;
            margin: 0px 0px 10px;
            padding: 0px;
        }

        .formbox li {
            list-style-type: none;
            margin: 0px 5px;
            margin: 0px 5px;
            padding: 0px;
        }

        .spouseform label {
            height: 35px;
            display: flex;
            justify-content: flex-end;
            align-items: center;
            margin-bottom: 10px;
        }

        .spouseform h4 {
            background: #91d1ff;
            margin-top: 10px;
            padding: 10px 0px;
        }

        #tab7primary .nav.nav-tabs li {
            width: auto !important;
            min-width: 10.8%;
        }

        .checkbox input[type=checkbox] {
            display: block;
        }

        .checkbox input[type=checkbox] + label {
            background: url('https://financialservicecenter.net/public/images/icon_unlock.png') no-repeat left top;
            padding-left: 30px;
            height: 25px;
        }

        .checkbox input[type=checkbox]:checked + label {
            background: url('https://financialservicecenter.net/public/images/icon_lock.png') no-repeat left 0px;

            font-style: normal;
        }

        .spouce_self {
            border: 1px solid #000000;
        }

        .wdth205 {
            width: 193px;
        }
        #customFieldsbo .form-group:last-child hr{
            display:none;
        }
        @media (max-width: 1185px) {
            .nav > li > a {
                padding: 10px 2px;
            }
        }

        @media (max-width: 1114px) {
            .nav-tabs > li {
                width: 16.3% !important;
                margin: 2px 0 0 2px !important;
            }
        }

        @media (max-width: 1020px) {
            .nav-tabs > li {
                width: 31.7% !important;
                margin: 2px 0 0 8px !important;
            }
        }

        @media (max-width: 500px) {
            .nav-tabs > li {
                width: 47% !important;
                margin: 2px 0 0 8px !important;
            }
        }
    </style>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>

    <?php
    if($common->subscription_active != '1')
    {
    ?>
    <style>


        #subscription_question1 {

            -webkit-appearance: none;
            -moz-appearance: none;
            text-indent: 1px;
            text-overflow: '';
        }

        #subscription_question2 {

            -webkit-appearance: none;
            -moz-appearance: none;
            text-indent: 1px;
            text-overflow: '';
        }

        #subscription_question3 {

            -webkit-appearance: none;
            -moz-appearance: none;
            text-indent: 1px;
            text-overflow: '';
        }

        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            width: 95% !important;
        }

    </style>
    <?php
    }
    ?>
    <?php
    if($common->limited_active != '1')
    {
    ?>
    <style>
        #limited_question1 {

            -webkit-appearance: none;
            -moz-appearance: none;
            text-indent: 1px;
            text-overflow: '';
        }

        #limited_question2 {

            -webkit-appearance: none;
            -moz-appearance: none;
            text-indent: 1px;
            text-overflow: '';
        }

        #limited_question3 {

            -webkit-appearance: none;
            -moz-appearance: none;
            text-indent: 1px;
            text-overflow: '';
        }

    </style>
    <?php
    }
    ?>


    <?php
    if($common->user_active != '1')
    {
    ?>
    <style>
        #user_question1 {

            -webkit-appearance: none;
            -moz-appearance: none;
            text-indent: 1px;
            text-overflow: '';
        }

        #user_question2 {

            -webkit-appearance: none;
            -moz-appearance: none;
            text-indent: 1px;
            text-overflow: '';
        }

        #user_question3 {

            -webkit-appearance: none;
            -moz-appearance: none;
            text-indent: 1px;
            text-overflow: '';
        }

        .padleft7 {
            padding-left: 7px !important;
        }

        .padrightt7 {
            padding-right: 7px !important;
        }

        .newcmpname {
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            height: 75px;
        }
    </style>
    <?php
    }
    ?>



    <?php //echo $infoCount;die;?>

    <div class="content-wrapper">
        <section class="content-header" style="padding-left:0px;">
            <div class="new-page-title">
                <div class="new-page-title-tab">
                    <div class="col-md-2 col-sm-4 col-xs-12">
                        <div class="new_client_id">
                            <p><?php echo e($common->filename); ?> </p>
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-8 col-xs-12" style="margin-top:0px;">
                        <div class="newcmpname">
                            <?php if($common->business_id=='6'): ?>
                                <div class="new_company_name">
                                    <label> Name
                                        :</label><?php echo e(ucwords($common->nametype)); ?> <?php echo e($common->first_name); ?> <?php echo e($common->middle_name); ?> <?php echo e($common->last_name); ?>

                                </div>
                            <?php else: ?>
                                <div class="new_company_name">
                                    <label>Company Legal Name :</label>
                                    <p><?php echo e($common->company_name); ?></p>
                                </div>
                                <div class="new_company_name">
                                    <label>Business Name (DBA) :</label>
                                    <p><?php echo e($common->business_name); ?></p>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-12 col-xs-12">
                        <div class="new_images_sec">
                            <?php if(empty($common->business_cat_id)): ?>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <?php else: ?>
                                        <?php if(empty($common->business_brand_category_id)): ?>
                                            <div class="col-md-4 col-sm-12 col-xs-12">
                                                <?php else: ?>
                                                    <div class="col-md-4 col-sm-4 col-xs-4">
                                                        <?php endif; ?>
                                                        <?php endif; ?>
                                                        <?php $__currentLoopData = $business; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $busi): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php if($busi->id==$common->business_id): ?>
                                                                <img src="<?php echo e(url('public/frontcss/images/')); ?>/<?php echo e($busi->newimage); ?>"
                                                                     class="img-responsive">
                                                    </div>
                                                <?php endif; ?>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php $__currentLoopData = $category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <?php if($common->business_cat_id==$cate->id): ?>
                                                        <?php if(empty($common->business_brand_category_id)): ?>
                                                            <div class="arrow"><span
                                                                        class="glyphicon glyphicon-chevron-right"
                                                                        aria-hidden="true"></span></div>
                                                            <div class="col-md-4 col-sm-4 col-xs-4">
                                                                <?php else: ?>
                                                                    <div class="arrow">
                                                                        <span class="glyphicon glyphicon-chevron-right"
                                                                              aria-hidden="true"></span>
                                                                    </div>
                                                                    <div class="col-md-4 col-sm-4 col-xs-4">
                                                                        <?php endif; ?>
                                                                        <img src="<?php echo e(url('public/category')); ?>/<?php echo e($cate->business_cat_image); ?>"
                                                                             alt="" class="img-responsive"/>
                                                                    </div>
                                                                <?php endif; ?>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                <?php $__currentLoopData = $cb; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bb1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                    <?php if($common->business_brand_category_id == $bb1->id): ?>
                                                                        <div class="arrow">
                                                                            <span class="glyphicon glyphicon-chevron-right"
                                                                                  aria-hidden="true"></span>
                                                                        </div>
                                                                        <div class="col-md-4 col-sm-4 col-xs-4">
                                                                            <img src="<?php echo e(url('public/businessbrandcategory')); ?>/<?php echo e($bb1->business_brand_category_image); ?>"
                                                                                 alt="" class="img-responsive"/>
                                                                        </div>
                                                                    <?php endif; ?>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                            </div>
                                            </div>
                                </div>
                        </div>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-md-12" style="padding-left:0px; padding-right:0px;">

                    <div class="box box-success">
                        <div class="card-body">
                            <?php if(count($errors) > 0): ?>
                                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <p class="alert alert-danger"><?php echo e($error); ?></p>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endif; ?>
                            <div class="panel with-nav-tabs panel-primary">
                                <div class="panel-heading">
                                    <ul class="nav nav-tabs" id="myTab">
                                        <li class="active"><a href="#tab1primary" data-toggle="tab">Basic Info</a></li>
                                        <li><a href="#tab4primary" data-toggle="tab">Contact Info</a></li>
                                        <li><a href="#tab6primary" data-toggle="tab">Security Info</a></li>
                                        <li><a href="#tab3primary" data-toggle="tab">Service Info</a></li>
                                        <li><a href="#tab5primary" data-toggle="tab">EE Responsibilty</a></li>
                                        <li><a href="#tab7primary" data-toggle="tab">Other </a></li>
                                    </ul>
                                </div>
                                <?php if(session()->has('success')): ?>
                                    <div class="alert alert-success alert-dismissable"><?php echo e(session()->get('success')); ?></div>
                                <?php endif; ?>

                                <form method="post" action="<?php echo e(route('customer.update', $common->cid)); ?>"
                                      id="registrationForm" class="form-horizontal" enctype="multipart/form-data">
                                    <?php echo e(csrf_field()); ?><?php echo e(method_field('PATCH')); ?>

                                    <input class=" form-control fsc-input" type="hidden" value='<?php echo e($common->user_type); ?>'
                                           name="user_type" id="user_type">
                                    <input class=" form-control fsc-input" type="hidden" value='' name="text1"
                                           id="text1">
                                    <div class="panel-body">
                                        <div class="tab-content">
                                            <div class="col-md-12 col-sm-12 col-xs-12"><img src="" alt=""
                                                                                            class="img-responsive">
                                            </div>
                                            <div class="tab-pane fade in active" id="tab1primary">
                                                <div class="">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group">

                                                            <label class="col-lg-3 col-md-12 col-xs-12 left_1200 control-label">
                                                                Client ID (File No) : </label>
                                                            <div class="col-lg-2 col-md-6 col-sm-6 col-xs-6 fsc-element-margin">
                                                                <input type="hidden" name="user_id"
                                                                       value="<?php echo e($common->cid); ?>">
                                                                <input type="text" class="form-control fsc-input"
                                                                       maxlength="10" onkeyup="checkemail();" readonly
                                                                       placeholder="AAA-999-AA99" id="fileno"
                                                                       name="fileno" value="<?php echo e($common->filename); ?>"
                                                                       placeholder="">
                                                                <div id="email_status"></div>
                                                            </div>
                                                            <div class="col-lg-2 col-md-6 col-sm-6 col-xs-6  fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input"
                                                                       placeholder="Create Date" readonly
                                                                       name="creationdate" id="creationdate"
                                                                       value="<?php if(empty($common->creationdate)): ?> <?php echo e(date('m-d-Y')); ?> <?php else: ?>  <?php echo e($common->creationdate); ?> <?php endif; ?>">
                                                            </div>
                                                            <div class="col-lg-1 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                                <label class="fsc-form-label left_1200"
                                                                       style="float: right; padding-top:7px;">Status
                                                                    : </label>
                                                            </div>
                                                            <div class="col-lg-2 col-md-6 col-sm-12 col-xs-12  fsc-element-margin">
                                                                <select name="status" id="status"
                                                                        class="form-control fsc-input <?php if($common->status == 'Approval'): ?> Green  <?php endif; ?>
                                                                        <?php if($common->status == 'Pending'): ?> Orange <?php endif; ?>
                                                                        <?php if($common->status == 'Hold'): ?> Blue  <?php endif; ?>
                                                                        <?php if($common->status == 'Inactive'): ?> Red <?php endif; ?>
                                                                        <?php if($common->status == 'Active'): ?> Green  <?php endif; ?>">
                                                                    <option value="">Status</option>
                                                                    <option value="Hold"
                                                                            <?php if($common->status == 'Hold'): ?> selected
                                                                            <?php endif; ?> class="Blue">Hold
                                                                    </option>
                                                                    <option class="Orange" value="Pending"
                                                                            <?php if($common->status == 'Pending'): ?> selected <?php endif; ?>>
                                                                        Pending
                                                                    </option>
                                                                    <option value="Approval"
                                                                            <?php if($common->status == 'Approval'): ?> selected
                                                                            <?php endif; ?> class="Cyan">Approve
                                                                    </option>
                                                                    <option value="Active" class="Green"
                                                                            <?php if($common->status == 'Active'): ?> selected <?php endif; ?>>
                                                                        Active
                                                                    </option>
                                                                    <option value="Inactive" class="Red"
                                                                            <?php if($common->status == 'Inactive'): ?> selected
                                                                            <?php endif; ?> style="background:red;color:#fff">
                                                                        Inactive
                                                                    </option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="removecom col-md-12">
                                                        <?php if($common->business_id=='6'): ?>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Name :</label>
                                                                <div class="col-md-7">
                                                                    <div class="row">
                                                                        <div class="col-md-3"
                                                                             style="width:17% !important;padding-right:0px;">
                                                                            <select type="text"
                                                                                    class="form-control txtOnly"
                                                                                    id="nametype" name="nametypess">
                                                                                <option value="mr"
                                                                                        <?php if($common->nametype=='mr'): ?> selected="" <?php endif; ?>>
                                                                                    Mr.
                                                                                </option>
                                                                                <option value="mrs"
                                                                                        <?php if($common->nametype=='mrs'): ?> selected="" <?php endif; ?>>
                                                                                    Mrs.
                                                                                </option>
                                                                                <option value="miss"
                                                                                        <?php if($common->nametype=='miss'): ?> selected="" <?php endif; ?>>
                                                                                    Miss.
                                                                                </option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-md-3"
                                                                             style="width:26% !important;">
                                                                            <input type="text" class="form-control "
                                                                                   id="firstname" name="fname"
                                                                                   value="<?php echo e($common->first_name); ?>">
                                                                        </div>
                                                                        <div class="col-md-1"
                                                                             style="width:16% !important;">
                                                                            <div class="row">
                                                                                <input type="text" class="form-control "
                                                                                       id="middlename" name="mname"
                                                                                       maxlength="1"
                                                                                       value="<?php echo e($common->middle_name); ?>">
                                                                            </div>
                                                                        </div>
                                                                        <div class="">
                                                                            <div class="col-md-4" style="width: 41%;">
                                                                                <input type="text" class="form-control "
                                                                                       id="lastname" name="lname"
                                                                                       value="<?php echo e($common->last_name); ?>">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php else: ?>


                                                            <div class="form-group <?php echo e($errors->has('company_name') ? ' has-error' : ''); ?>">
                                                                <div class="">
                                                                    <label class="col-lg-3 col-md-12 left_1200 control-label">Company
                                                                        Legal Name : <span
                                                                                class="star-required">*</span><span
                                                                                class="tess">(Legal Name)</span></label>
                                                                    <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                        <input type="text"
                                                                               class="form-control fsc-input"
                                                                               name="company_name" id="company_name"
                                                                               value="<?php echo e($common->company_name); ?>">
                                                                        <?php if($errors->has('company_name')): ?>
                                                                            <span class="help-block">
   <strong><?php echo e($errors->first('company_name')); ?></strong>
   </span>
                                                                        <?php endif; ?>
                                                                    </div>
                                                                </div>
                                                            </div>



                                                            <div class="form-group <?php echo e($errors->has('business_name') ? ' has-error' : ''); ?>">
                                                                <div class="">
                                                                    <label class="col-lg-3 control-label">Business Name
                                                                        : <span class="tess">(DBA Name)</span></label>
                                                                    <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                        <input type="text"
                                                                               class="form-control fsc-input"
                                                                               name="business_name" id="business_name"
                                                                               value="<?php echo e($common->business_name); ?>">
                                                                        <?php if($errors->has('business_name')): ?>
                                                                            <span class="help-block">
   <strong><?php echo e($errors->first('business_name')); ?></strong>
   </span>
                                                                        <?php endif; ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php endif; ?>

                                                    </div>
                                                    <?php if($common->business_id=='6'): ?>
                                                        <style>
                                                            .cat1, .cat3 {
                                                                display: none;
                                                            }
                                                        </style>
                                                    <?php endif; ?>

                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label class="col-lg-3 control-label">Business Type
                                                                : </label>
                                                            <div class="col-lg-9">
                                                                <div class="row">
                                                                    <div class="col-xs-2 b_type1">
                                                                        <select name="business_id" id="business_id"
                                                                                class="form-control fsc-input category">
                                                                            <option value=''>---Select---</option>
                                                                            <?php $__currentLoopData = $business; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $busi): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                                <option value='<?php echo e($busi->id); ?>'
                                                                                        <?php if($busi->id==$common->business_id): ?> selected <?php endif; ?>><?php echo e($busi->bussiness_name); ?></option>
                                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                        </select>
                                                                        <input type="hidden" class="form-control"
                                                                               id="business_cat" readonly
                                                                               name="business_cat"
                                                                               value="<?php echo e($common->user_type); ?>">
                                                                    </div>

                                                                    <div class="arrow cat1"
                                                                         <?php if(empty($common->business_cat_id)): ?> style="display:none" <?php else: ?> <?php endif; ?>>
                                                                        <span class="glyphicon glyphicon-chevron-right"
                                                                              aria-hidden="true"
                                                                              style="top: -1px;"></span>
                                                                    </div>

                                                                    <div class="col-xs-3 cat1 b_type2"
                                                                         style="margin-left: 3px; "
                                                                         <?php if(empty($common->business_cat_id)): ?> style="display:none;width:28% !important" <?php else: ?> <?php endif; ?>>
                                                                        <select name="business_catagory_name"
                                                                                style="font-size: 15px !important;"
                                                                                id="business_catagory_name"
                                                                                class="form-control fsc-input category1">
                                                                            <option value=''>---Select Business Category
                                                                                Name---
                                                                            </option>
                                                                            <?php $__currentLoopData = $category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                                <option value='<?php echo e($cate->id); ?>'
                                                                                        <?php if($cate->id==$common->business_cat_id): ?> selected <?php endif; ?>><?php echo e($cate->business_cat_name); ?></option>
                                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                        </select>
                                                                        <input type="hidden" class="form-control"
                                                                               id="business_cat1" readonly
                                                                               name="business_cat1"
                                                                               value="<?php echo e($common->business_cat_name); ?>">
                                                                    </div>
                                                                    <div class="arrow cat2"
                                                                         <?php if(empty($common->business_brand_id)): ?> style="display:none" <?php else: ?> <?php endif; ?>>
                                                                        <span class="glyphicon glyphicon-chevron-right"
                                                                              aria-hidden="true"
                                                                              style="top: -1px;"></span>
                                                                    </div>

                                                                    <div class="col-xs-2  cat2 b_type3"
                                                                         style=" display:none "
                                                                         <?php if(empty($common->business_brand_id)): ?> style="display:none" <?php else: ?> <?php endif; ?>>
                                                                        <select name="business_brand_name"
                                                                                id="business_brand_name"
                                                                                class="form-control fsc-input">
                                                                            <option value="">---Brand---</option>
                                                                            <?php $__currentLoopData = $businessbrand; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $brand): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                                <option value='<?php echo e($brand->id); ?>'
                                                                                        <?php if($brand->id==$common->business_brand_id): ?> selected <?php endif; ?>><?php echo e($brand->business_brand_name); ?></option>
                                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                        </select>
                                                                    </div>

                                                                    <div class="arrow cat3"
                                                                         <?php if(empty($common->business_brand_category_id)): ?> style="display:none" <?php else: ?> <?php endif; ?> >
                                                                        <span class="glyphicon glyphicon-chevron-right"
                                                                              aria-hidden="true"
                                                                              style="top: -1px;"></span>
                                                                    </div>

                                                                    <div class="col-xs-2 cat3 cat3_1 b_type4"
                                                                         <?php if(empty($common->business_brand_category_id)): ?> style="display:none;" <?php else: ?> <?php endif; ?> >
                                                                        <select name="business_brand_category_name"
                                                                                id="business_brand_category_name"
                                                                                class="form-control fsc-input">
                                                                            <option value="">---Brand---</option>
                                                                            <?php $__currentLoopData = $cb; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $brand): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                                <option value='<?php echo e($brand->id); ?>'
                                                                                        <?php if($brand->id==$common->business_brand_category_id): ?> selected <?php endif; ?>><?php echo e($brand->business_brand_category_name); ?></option>
                                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                        </select>
                                                                        <input type="hidden" class="form-control"
                                                                               id="business_cat2" readonly
                                                                               name="business_cat3"
                                                                               value="<?php echo e($common->business_brand_category_name); ?>">
                                                                    </div>


                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="naicss"
                                                             <?php if(empty($common->business_cat_id)): ?> style="display:none" <?php else: ?> <?php endif; ?>>

                                                            <div class="form-group <?php echo e($errors->has('zip') ? 'has-error' : ''); ?> <?php echo e($errors->has('stateId') ? 'has-error' : ''); ?> <?php echo e($errors->has('city') ? 'has-error' : ''); ?> ">
                                                                <div class="">
                                                                    <label class="col-lg-3 control-label">NAICS / SIC
                                                                        Code: </label>
                                                                    <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                        <div class="row">
                                                                            <div class="col-lg-3 col-md-5 col-sm-5 col-xs-6 fsc-element-margin">
                                                                                <input type="text"
                                                                                       class="form-control fsc-input"
                                                                                       id="" name="naics" readonly
                                                                                       placeholder="NAICS"
                                                                                       value="<?php if (isset($categoryname->naics)) {
                                                                                           echo $categoryname->naics;
                                                                                       }?>">
                                                                                <?php if($errors->has('city')): ?>
                                                                                    <span class="help-block">
               <strong><?php echo e($errors->first('city')); ?></strong>
               </span>
                                                                                <?php endif; ?>
                                                                            </div>

                                                                            <div class="col-lg-3 col-md-5 col-sm-5 col-xs-6 fsc-form-col">
                                                                                <div class="dropdown"
                                                                                     style="margin-top: 1%;">
                                                                                    <input type="text"
                                                                                           class="form-control fsc-input"
                                                                                           placeholder="SIC" readonly
                                                                                           id="" name="sic"
                                                                                           value="<?php if (isset($categoryname->sic)) {
                                                                                               echo $categoryname->sic;
                                                                                           }?>">

                                                                                    <?php if($errors->has('stateId')): ?>
                                                                                        <span class="help-block">
               <strong><?php echo e($errors->first('stateId')); ?></strong>
               </span>
                                                                                    <?php endif; ?>
                                                                                </div>
                                                                            </div>

                                                                            <!--<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                <input type="text" class="form-control fsc-input" placeholder="Search" id="" name="" value="">
           </div>!-->

                                                                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 fsc-element-margin">
                                                                                <!--<input type="button" class="add-row btn btn-primary" value="Go to Link">-->
                                                                                <a href="https://www.naics.com/code-search/"
                                                                                   target="_blank"
                                                                                   class="add-row btn btn-primary">Go to
                                                                                    Link</a>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>

                                                    </div>


                                                    <div class="form-group personal" style="display:none">
                                                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                                                            <label class="fsc-form-label">Name : <span
                                                                        class="star-required">*</span></label>
                                                        </div>
                                                        <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin"
                                                             style="width:10% !important;">
                                                            <select class="form-control fsc-input" id="nametype"
                                                                    name="nametype">
                                                                <option value="mr">Mr.</option>
                                                                <option value="mrs">Mrs.</option>
                                                                <option value="miss">Miss.</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin"
                                                             style="width:15% !important;">
                                                            <input type="text" class="form-control  fsc-input textonly"
                                                                   id="first_name" name="first_name"
                                                                   placeholder="First Name">
                                                        </div>
                                                        <div class="col-lg-1 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                            <input type="text" class="form-control fsc-input textonly"
                                                                   id="middle_name" maxlength="1" name="middle_name"
                                                                   placeholder="M">
                                                        </div>
                                                        <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin"
                                                             style="width:16.6% !important;">
                                                            <input type="text" class="form-control fsc-input textonly"
                                                                   id="last_name" name="last_name"
                                                                   placeholder="Last name">
                                                        </div>
                                                    </div>


                                                    <div class="addcom"
                                                         <?php if($common->business_id=='6'): ?> style="display:block;"
                                                         <?php else: ?> style="display:none;" <?php endif; ?>>
                                                        <?php if($common->business_id !='6'): ?>
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Associate
                                                                        Client ID : </label>
                                                                    <div class="col-md-3">
                                                                        <input type="text"
                                                                               class="form-control fsc-input"
                                                                               value="<?php echo e($common->CL); ?>"
                                                                               placeholder="Associate Client ID"
                                                                               name="CL" id="CL">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php endif; ?>
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Maritial Status
                                                                    :</label>
                                                                <div class="col-md-3">
                                                                    <select class="form-control maritial"
                                                                            name="other_maritial_status1">
                                                                        <option value="">Select</option>
                                                                        <option value="Single"
                                                                                <?php if($common->other_maritial_status1=='Single'): ?> selected <?php endif; ?>>
                                                                            Single
                                                                        </option>
                                                                        <option value="Married"
                                                                                <?php if($common->other_maritial_status1=='Married'): ?> selected <?php endif; ?>>
                                                                            Married
                                                                        </option>
                                                                        <option value="Widowed"
                                                                                <?php if($common->other_maritial_status1=='Widowed'): ?> selected <?php endif; ?>>
                                                                            Widowed
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <div class="form-group showmaritial"
                                                                 <?php if($common->other_maritial_status1=='Married'): ?> style="display:block;"
                                                                 <?php else: ?> style="display:none;" <?php endif; ?>>

                                                                <label class="control-label col-md-3">Spouse Name
                                                                    : </label>

                                                                <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin"
                                                                     style="width:10% !important;">
                                                                    <select class="form-control fsc-input"
                                                                            id="personalname" name="personalname">
                                                                        <option value="Single"
                                                                                <?php if($common->personalname=='Single'): ?> selected <?php endif; ?>>
                                                                            Mr.
                                                                        </option>
                                                                        <option value="Married"
                                                                                <?php if($common->personalname=='Married'): ?> selected <?php endif; ?>>
                                                                            Mrs.
                                                                        </option>
                                                                        <option value="Widowed"
                                                                                <?php if($common->personalname=='Widowed'): ?> selected <?php endif; ?>>
                                                                            Miss.
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin"
                                                                     style="width:18% !important;">
                                                                    <input type="text"
                                                                           class="form-control  fsc-input textonly"
                                                                           value="<?php echo e($common->maritial_first_name); ?>"
                                                                           id="maritial_first_name"
                                                                           name="maritial_first_name"
                                                                           placeholder="First Name">
                                                                </div>
                                                                <div class="col-lg-1 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                    <input type="text"
                                                                           class="form-control fsc-input textonly"
                                                                           value="<?php echo e($common->maritial_middle_name); ?>"
                                                                           id="maritial_middle_name" maxlength="1"
                                                                           name="maritial_middle_name" placeholder="M">
                                                                </div>
                                                                <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin"
                                                                     style="width:22% !important;">
                                                                    <input type="text"
                                                                           class="form-control fsc-input textonly"
                                                                           value="<?php echo e($common->maritial_last_name); ?>"
                                                                           id="maritial_last_name"
                                                                           name="maritial_last_name"
                                                                           placeholder="Last name">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 <?php echo e($errors->has('address') ? 'has-error' : ''); ?>">
                                                        <div class="form-group">
                                                            <label class="col-lg-3 control-label"><span
                                                                        class="bus"> <?php if($common->business_id=='6'): ?> <?php else: ?>
                                                                        Business <?php endif; ?></span> Address
                                                                1: <?php if($common->business_id=='6'): ?> <?php else: ?><span
                                                                        class="tess">(Physical Address)</span> <?php endif; ?>
                                                            </label>
                                                            <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input"
                                                                       name="address" id="address"
                                                                       value="<?php echo e($common->address); ?>">
                                                                <?php if($errors->has('address')): ?>
                                                                    <span class="help-block">
       <strong><?php echo e($errors->first('address')); ?></strong>
       </span>
                                                                <?php endif; ?>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 <?php echo e($errors->has('address1') ? 'has-error' : ''); ?>">
                                                        <div class="form-group">
                                                            <label class="col-lg-3 control-label"><span
                                                                        class="bus"><?php if($common->business_id=='6'): ?> <?php else: ?>
                                                                        Business <?php endif; ?></span> Address
                                                                2: <?php if($common->business_id=='6'): ?> <?php else: ?> <span
                                                                        class="tess">(Physical Address)</span><?php endif; ?>
                                                            </label>
                                                            <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input"
                                                                       name="address1" id="address1"
                                                                       value="<?php echo e($common->address1); ?>">
                                                                <?php if($errors->has('address1')): ?>
                                                                    <span class="help-block">
       <strong><?php echo e($errors->first('address1')); ?></strong>
       </span>
                                                                <?php endif; ?>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label class="col-lg-3 control-label">Country: </label>
                                                            <div class="col-lg-7 col-md-12 fsc-element-margin">
                                                                <div class="row">
                                                                    <div class="col-lg-5 col-md-6 col-sm-6 fsc-element-margin">
                                                                        <div class="dropdown">
                                                                            <select name="countryId"
                                                                                    id="countries_states1"
                                                                                    class="form-control fsc-input bfh-countries"
                                                                                    data-country="<?php echo e($common->countryId); ?>">
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 <?php echo e($errors->has('zip') ? 'has-error' : ''); ?> <?php echo e($errors->has('stateId') ? 'has-error' : ''); ?> <?php echo e($errors->has('city') ? 'has-error' : ''); ?> ">
                                                        <div class="form-group">
                                                            <label class="col-lg-3 control-label">City / State / Zip
                                                                : </label>
                                                            <div class="col-lg-7  col-sm-12 col-xs-12 fsc-element-margin">
                                                                <div class="row">
                                                                    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                        <input type="text"
                                                                               class="form-control fsc-input textonly"
                                                                               id="city" name="city" placeholder="City"
                                                                               value="<?php echo e($common->city); ?>">
                                                                        <?php if($errors->has('city')): ?>
                                                                            <span class="help-block">
       <strong><?php echo e($errors->first('city')); ?></strong>
       </span>
                                                                        <?php endif; ?>
                                                                    </div>
                                                                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 fsc-form-col">
                                                                        <div class="dropdown" style="margin-top: 1%;">
                                                                            <select name="stateId" id="stateId"
                                                                                    class="form-control fsc-input bfh-states"
                                                                                    data-country="countries_states1"
                                                                                    data-state="<?php echo e($common->stateId); ?>">
                                                                            </select>
                                                                            <?php if($errors->has('stateId')): ?>
                                                                                <span class="help-block">
       <strong><?php echo e($errors->first('stateId')); ?></strong>
       </span>
                                                                            <?php endif; ?>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 fsc-element-margin">
                                                                        <input type="text"
                                                                               class="form-control fsc-input zip"
                                                                               id="zip" name="zip"
                                                                               value="<?php echo e($common->zip); ?>" maxlength="5"
                                                                               placeholder="Zip">
                                                                        <?php if($errors->has('zip')): ?>
                                                                            <span class="help-block">
       <strong><?php echo e($errors->first('zip')); ?></strong>
       </span>
                                                                        <?php endif; ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label class="col-lg-3 control-label"></label>
                                                            <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <input type="checkbox" value="1" class="check"
                                                                       name="mailing_address_check" id="businessaddress"
                                                                       <?php if(empty($common->mailing_address_check)): ?> <?php else: ?> checked
                                                                       <?php endif; ?> name="billingtoo"
                                                                       onclick="FillBilling(this.form)">
                                                                <label for="businessaddress">Same as
                                                                    above <?php if($common->business_id=='6'): ?> <?php else: ?> <span
                                                                            class="bus">business</span><?php endif; ?>
                                                                    Address</label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 <?php echo e($errors->has('mailing_address') ? 'has-error' : ''); ?>">
                                                        <div class="form-group">
                                                            <label class="col-lg-3 control-label">Mailing Address
                                                                1: </label>
                                                            <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input"
                                                                       id="mailing_address" name="mailing_address"
                                                                       value="<?php echo e($common->mailing_address); ?>"
                                                                       placeholder="Mailing Address">
                                                                <?php if($errors->has('mailing_address')): ?>
                                                                    <span class="help-block">
                   <strong><?php echo e($errors->first('mailing_address')); ?></strong>
                   </span>
                                                                <?php endif; ?>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 <?php echo e($errors->has('mailing_address1') ? 'has-error' : ''); ?>">
                                                        <div class="form-group">
                                                            <label class="col-lg-3 control-label">Mailing Address
                                                                2: </label>
                                                            <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <input type="text" class="form-control fsc-input"
                                                                       id="mailing_address1" name="mailing_address1"
                                                                       value="<?php echo e($common->mailing_address1); ?>"
                                                                       placeholder="Mailing Address 1">
                                                                <?php if($errors->has('mailing_address1')): ?>
                                                                    <span class="help-block">
               <strong><?php echo e($errors->first('mailing_address1')); ?></strong>
               </span>
                                                                <?php endif; ?>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 <?php echo e($errors->has('mailing_city') ? 'has-error' : ''); ?> <?php echo e($errors->has('mailing_state') ? 'has-error' : ''); ?><?php echo e($errors->has('mailing_zip') ? 'has-error' : ''); ?>">
                                                        <div class="form-group">
                                                            <label class="col-lg-3 control-label">City / State / Zip
                                                                : </label>
                                                            <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <div class="row">
                                                                    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                        <input type="text"
                                                                               class="form-control textonly fsc-input"
                                                                               id="mailing_city" name="mailing_city"
                                                                               placeholder="City"
                                                                               value="<?php echo e($common->mailing_city); ?>">
                                                                        <?php if($errors->has('mailing_city')): ?>
                                                                            <span class="help-block">
                       <strong><?php echo e($errors->first('mailing_city')); ?></strong>
                       </span>
                                                                        <?php endif; ?>
                                                                    </div>

                                                                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 fsc-form-col">
                                                                        <div class="dropdown" style="margin-top: 1%;">
                                                                            <select name="mailing_state"
                                                                                    id="mailing_state"
                                                                                    class="form-control fsc-input bfh-states"
                                                                                    data-country="countries_states1"
                                                                                    data-state="<?php echo e($common->mailing_state); ?>">
                                                                            </select>
                                                                            <?php if($errors->has('mailing_state')): ?>
                                                                                <span class="help-block">
                           <strong><?php echo e($errors->first('mailing_state')); ?></strong>
                           </span>
                                                                            <?php endif; ?>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 fsc-element-margin">
                                                                        <input type="text"
                                                                               class="form-control fsc-input zip"
                                                                               id="mailing_zip" name="mailing_zip"
                                                                               value="<?php echo e($common->mailing_zip); ?>"
                                                                               placeholder="Zip" maxlength="5">
                                                                        <?php if($errors->has('mailing_zip')): ?>
                                                                            <span class="help-block">
                           <strong><?php echo e($errors->first('mailing_zip')); ?></strong>
                           </span>
                                                                        <?php endif; ?>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 <?php echo e($errors->has('email') ? 'has-error' : ''); ?>">
                                                        <div class="form-group">
                                                            <label class="col-lg-3 control-label"><?php if($common->business_id=='6'): ?> <?php else: ?>
                                                                    <span class="bus">Business</span> <?php endif; ?> Email :
                                                            </label>
                                                            <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <div class="">
                                                                    <input type="email" class="form-control fsc-input"
                                                                           id="email" name='email'
                                                                           placeholder="abc@abc.com"
                                                                           value="<?php echo e($common->email); ?>">
                                                                    <span><b>Note : </b><b><span style="color:blue;">This is your Primary Email address</span></b></span>
                                                                    <?php if($errors->has('email')): ?>
                                                                        <span class="help-block">
                       <strong><?php echo e($errors->first('email')); ?></strong>
                       </span>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Telephone No 1. :
                                                                <span class="star-required"></span></label>
                                                            <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <div class="row">
                                                                    <div class="col-md-5">
                                                                        <input name="business_no"
                                                                               value="<?php echo e($common->business_no); ?>"
                                                                               type="tel" id="motelephoneile"
                                                                               class="form-control bfh-phone"
                                                                               data-country="countries_states1"
                                                                               data-format="  (999) 999-9999"
                                                                               placeholder="(999) 999-9999">
                                                                    </div>

                                                                    <div class="col-md-3 col-xs-6">
                                                                        <select name="businesstype"
                                                                                id="telephoneNo1Type"
                                                                                class="form-control fsc-input telephoneNo1Type">
                                                                            <option value="">Select</option>
                                                                            <option value="Business"
                                                                                    <?php if($common->businesstype=='Business'): ?>selected <?php endif; ?>>
                                                                                Business
                                                                            </option>
                                                                            <option value="Office"
                                                                                    <?php if($common->businesstype=='Office'): ?>selected <?php endif; ?>>
                                                                                Office
                                                                            </option>
                                                                            <option value="Resid"
                                                                                    <?php if($common->businesstype=='Resid'): ?>selected <?php endif; ?>>
                                                                                Resid
                                                                            </option>
                                                                            <option value="Mobile"
                                                                                    <?php if($common->businesstype=='Mobile'): ?>selected <?php endif; ?>>
                                                                                Mobile
                                                                            </option>
                                                                        </select>
                                                                    </div>

                                                                    <div class="col-md-4 col-xs-6 hideyourself hidebasicext"
                                                                         <?php if($common->businesstype == 'Mobile'): ?> style="display:none;" <?php endif; ?>>
                                                                        <input class="form-control fsc-input"
                                                                               id="businessext" maxlength="5" readonly
                                                                               name="businessext"
                                                                               value="<?php echo $common->businessext;?>"
                                                                               placeholder="Ext" type="text">
                                                                    </div>

                                                                    <div class="col-md-4 col-xs-6 yourself"
                                                                         style="display:none;">
                                                                        <select class="form-control">
                                                                            <option value="">Select</option>
                                                                            <option value="Yourself">Yourself</option>
                                                                            <option value="Spouse">Spouse</option>
                                                                        </select>
                                                                    </div>

                                                                    <div class="col-md-4 col-xs-6 fsc-element-margin showbasicguardian"
                                                                         <?php if($common->businesstype != 'Mobile'): ?> style="display:none;" <?php endif; ?>>
                                                                        <select name="basic_guardian1"
                                                                                id="basic_guardian1"
                                                                                class="form-control fsc-input">
                                                                            <option value="">Select</option>
                                                                            <option value="Himself"
                                                                                    <?php if($common->basic_guardian1=='Himself'): ?> selected <?php endif; ?>>
                                                                                Himself
                                                                            </option>
                                                                            <option value="Herself"
                                                                                    <?php if($common->basic_guardian1=='Herself'): ?> selected <?php endif; ?>>
                                                                                Herself
                                                                            </option>

                                                                            <option value="Spouse"
                                                                                    <?php if($common->basic_guardian1	=='Spouse'): ?> selected <?php endif; ?>>
                                                                                Spouse
                                                                            </option>
                                                                            <option value="Father"
                                                                                    <?php if($common->basic_guardian1	=='Father'): ?> selected <?php endif; ?>>
                                                                                Father
                                                                            </option>
                                                                            <option value="Mother"
                                                                                    <?php if($common->basic_guardian1	=='Mother'): ?> selected <?php endif; ?>>
                                                                                Mother
                                                                            </option>
                                                                            <option value="Daughter"
                                                                                    <?php if($common->basic_guardian1	=='Daughter'): ?> selected <?php endif; ?>>
                                                                                Daughter
                                                                            </option>
                                                                            <option value="Son"
                                                                                    <?php if($common->basic_guardian1	=='Son'): ?> selected <?php endif; ?>>
                                                                                Son
                                                                            </option>
                                                                            <option value="Uncle"
                                                                                    <?php if($common->basic_guardian1	=='Uncle'): ?> selected <?php endif; ?>>
                                                                                Uncle
                                                                            </option>
                                                                            <option value="Aunty"
                                                                                    <?php if($common->basic_guardian1	=='Aunty'): ?> selected <?php endif; ?>>
                                                                                Aunty
                                                                            </option>
                                                                            <option value="Other"
                                                                                    <?php if($common->basic_guardian1	=='Other'): ?> selected <?php endif; ?>>
                                                                                Other
                                                                            </option>


                                                                        </select>

                                                                    </div>


                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Telephone No 2.
                                                                :</label>
                                                            <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <div class="row">
                                                                    <div class="col-md-5">
                                                                        <input name="motelephoneile1"
                                                                               value="<?php echo e($common->motelephoneile1); ?>"
                                                                               type="tel" id="motelephoneile1"
                                                                               class="form-control bfh-phone"
                                                                               data-country="countries_states1"
                                                                               data-format="  (999) 999-9999"
                                                                               placeholder="(999) 999-9999">
                                                                    </div>
                                                                    <div class="col-md-3 col-xs-6">
                                                                        <select name="telephoneNo2Type"
                                                                                id="telephoneNo2Type"
                                                                                class="form-control fsc-input telephoneNo2Type">
                                                                            <option value="">Select</option>
                                                                            <option value="Business"
                                                                                    <?php if($common->telephoneNo2Type=='Business'): ?>selected <?php endif; ?>>
                                                                                Business
                                                                            </option>
                                                                            <option value="Office"
                                                                                    <?php if($common->telephoneNo2Type=='Office'): ?>selected <?php endif; ?>>
                                                                                Office
                                                                            </option>
                                                                            <option value="Resid"
                                                                                    <?php if($common->telephoneNo2Type=='Resid'): ?>selected <?php endif; ?>>
                                                                                Resid
                                                                            </option>
                                                                            <option value="Mobile"
                                                                                    <?php if($common->telephoneNo2Type=='Mobile'): ?>selected <?php endif; ?>>
                                                                                Mobile
                                                                            </option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-md-4 col-xs-6 hideyourself1 hidebasicext2"
                                                                         <?php if($common->telephoneNo2Type == 'Mobile'): ?> style="display:none;" <?php endif; ?>>
                                                                        <input class="form-control fsc-input" id="ext2"
                                                                               maxlength="5" readonly name="ext2"
                                                                               placeholder="Ext" type="text">
                                                                    </div>

                                                                    <div class="col-md-4 col-xs-6 yourself1"
                                                                         style="display:none;">

                                                                        <select class="form-control">
                                                                            <option value="">Select</option>

                                                                            <option value="Yourself">Yourself</option>
                                                                            <option value="Spouse">Spouse</option>

                                                                        </select>
                                                                    </div>
                                                                    <div class="col-md-4 col-xs-6 fsc-element-margin showbasicguardian2"
                                                                         <?php if($common->telephoneNo2Type != 'Mobile'): ?> style="display:none;" <?php endif; ?>>
                                                                        <select name="basic_guardian2"
                                                                                id="basic_guardian2"
                                                                                class="form-control fsc-input">
                                                                            <option value="">Select</option>
                                                                            <option value="Himself"
                                                                                    <?php if($common->basic_guardian2=='Himself'): ?> selected <?php endif; ?>>
                                                                                Himself
                                                                            </option>
                                                                            <option value="Herself"
                                                                                    <?php if($common->basic_guardian2=='Herself'): ?> selected <?php endif; ?>>
                                                                                Herself
                                                                            </option>

                                                                            <option value="Spouse"
                                                                                    <?php if($common->basic_guardian2	=='Spouse'): ?> selected <?php endif; ?>>
                                                                                Spouse
                                                                            </option>
                                                                            <option value="Father"
                                                                                    <?php if($common->basic_guardian2	=='Father'): ?> selected <?php endif; ?>>
                                                                                Father
                                                                            </option>
                                                                            <option value="Mother"
                                                                                    <?php if($common->basic_guardian2	=='Mother'): ?> selected <?php endif; ?>>
                                                                                Mother
                                                                            </option>
                                                                            <option value="Daughter"
                                                                                    <?php if($common->basic_guardian2	=='Daughter'): ?> selected <?php endif; ?>>
                                                                                Daughter
                                                                            </option>
                                                                            <option value="Son"
                                                                                    <?php if($common->basic_guardian1	=='Son'): ?> selected <?php endif; ?>>
                                                                                Son
                                                                            </option>
                                                                            <option value="Uncle"
                                                                                    <?php if($common->basic_guardian1	=='Uncle'): ?> selected <?php endif; ?>>
                                                                                Uncle
                                                                            </option>
                                                                            <option value="Aunty"
                                                                                    <?php if($common->basic_guardian1	=='Aunty'): ?> selected <?php endif; ?>>
                                                                                Aunty
                                                                            </option>
                                                                            <option value="Other"
                                                                                    <?php if($common->basic_guardian1	=='Other'): ?> selected <?php endif; ?>>
                                                                                Other
                                                                            </option>


                                                                        </select>

                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 <?php echo e($errors->has('business_fax') ? 'has-error' : ''); ?>">
                                                        <div class="form-group">
                                                            <label class="col-lg-3 control-label">Fax # : </label>
                                                            <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                <div class="row">
                                                                    <div class="col-lg-5 col-md-12 col-sm-6 fsc-element-margin">
                                                                        <input type="text"
                                                                               class="form-control fsc-input bfh-phone"
                                                                               data-country="countries_states1"
                                                                               placeholder="(999) 999-9999"
                                                                               value="<?php echo e($common->business_fax); ?>"
                                                                               id="business_fax" name="business_fax">
                                                                        <?php if($errors->has('business_fax')): ?>
                                                                            <span class="help-block">
                           <strong><?php echo e($errors->first('business_fax')); ?></strong>
                           </span>
                                                                        <?php endif; ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <?php if($common->business_id=='6'): ?>  <?php else: ?>
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 <?php echo e($errors->has('website') ? 'has-error' : ''); ?>">
                                                            <div class="form-group">
                                                                <label class="col-lg-3 control-label">Website : </label>
                                                                <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                    <div class="">
                                                                        <input type="text"
                                                                               class="form-control fsc-input"
                                                                               id="website" value="<?php echo e($common->website); ?>"
                                                                               name="website"
                                                                               placeholder="Website address">
                                                                        <?php if($errors->has('website')): ?>
                                                                            <span class="help-block">
           <strong><?php echo e($errors->first('website')); ?></strong>
           </span>
                                                                        <?php endif; ?>
                                                                        <span><b>Note : </b>[ http:// or https:// required ]</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label class="col-lg-3 control-label">Do you have other
                                                                    location? : </label>
                                                                <div class="col-lg-8 row col-md-12 col-8m-12 col-xs-12 fsc-element-margin">
                                                                    <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12 fsc-element-margin"
                                                                         style="width:91.4% !important;margin-top:1%;">
                                                                        <input type="radio" value="yes" id="locations"
                                                                               class="yes1"
                                                                               <?php if($common->locations=='yes'): ?> checked
                                                                               <?php endif; ?> onclick="show2();"
                                                                               name="locations"> Yes
                                                                        <input type="radio" value="no" id="locations"
                                                                               <?php if($common->locations=='no'): ?> checked
                                                                               <?php endif; ?> onclick="show1();" class="yes1"
                                                                               name="locations"> No
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php endif; ?>

                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 Location"
                                                         id="div1"
                                                         <?php if($common->locations=='yes'): ?> style="display:block" <?php endif; ?>>
                                                        <div class="form-group">
                                                            <label class="col-lg-3 control-label">How many Location
                                                                : </label>
                                                            <div class="col-lg-8 row col-md-12 col-8m-12 col-xs-12 fsc-element-margin">
                                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 fsc-element-margin">
                                                                    <select class="form-control fsc-input"
                                                                            name="multilocation">
                                                                        <option value="">Select</option>
                                                                        <option value="01"
                                                                                <?php if($common->multilocation=='01'): ?> selected <?php endif; ?>>
                                                                            01
                                                                        </option>
                                                                        <option value="02"
                                                                                <?php if($common->multilocation=='02'): ?> selected <?php endif; ?>>
                                                                            02
                                                                        </option>
                                                                        <option value="03"
                                                                                <?php if($common->multilocation=='03'): ?> selected <?php endif; ?>>
                                                                            03
                                                                        </option>
                                                                        <option value="04"
                                                                                <?php if($common->multilocation=='04'): ?> selected <?php endif; ?>>
                                                                            04
                                                                        </option>
                                                                        <option value="05"
                                                                                <?php if($common->multilocation=='05'): ?> selected <?php endif; ?>>
                                                                            05
                                                                        </option>
                                                                        <option value="06"
                                                                                <?php if($common->multilocation=='06'): ?> selected <?php endif; ?>>
                                                                            06
                                                                        </option>
                                                                        <option value="07"
                                                                                <?php if($common->multilocation=='07'): ?> selected <?php endif; ?>>
                                                                            07
                                                                        </option>
                                                                        <option value="08"
                                                                                <?php if($common->multilocation=='08'): ?> selected <?php endif; ?>>
                                                                            08
                                                                        </option>
                                                                        <option value="09"
                                                                                <?php if($common->multilocation=='09'): ?> selected <?php endif; ?>>
                                                                            09
                                                                        </option>
                                                                        <option value="10"
                                                                                <?php if($common->multilocation=='10'): ?> selected <?php endif; ?>>
                                                                            10
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <!--	<div class="switch-field">
		<input type="radio" id="radio-onedd" name="switch-one" value="yes" />
		<label for="radio-onedd">Yes</label>
		<input type="radio" id="radio-twodd" name="switch-one" value="no" checked/>
		<label for="radio-twodd">No</label>
	</div>-->

                                            <div class="tab-pane fade" id="tab5primary">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="clear clearfix"></div>
                                                        <div class="table-responsive">
                                                            <table class="table table-bordered" id="emptable">
                                                                <thead>
                                                                <tr>
                                                                    <th scope="col" style="text-align:center">Type of
                                                                        Service
                                                                    </th>
                                                                    <th scope="col"
                                                                        style="width: 112px;text-align:center;">Period
                                                                    </th>
                                                                    <th scope="col" style="text-align:center;">Employee
                                                                        Responsible
                                                                    </th>
                                                                    <th scope="col" style="text-align:center">Checking
                                                                        (Supervisor)
                                                                    </th>
                                                                    <th scope="col" style="text-align:center">Note</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>


                                                                <?php $cntss = -1;//echo "<pre>";print_r($clientser); ?>
                                                                <?php $__currentLoopData = $clientser; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $clientser1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                    <?php $cntss++;
                                                                    $prds = explode('-', $clientser1->periods);?>
                                                                    <tr>
                                                                    <!--  <input type="hidden" class="form-control" name="serviceid[]" value="<?php echo e($clientser1->id); ?>">
           !-->
                                                                        <input type="hidden" class="form-control"
                                                                               name="empid[]"
                                                                               value="<?php echo e($clientser1->id); ?>">
                                                                        <td style="width:17% !important;">
                                                                            <?php if($clientser1->typeofservice=='3'): ?>
                                                                                <input readonly type="text"
                                                                                       class="form-control"
                                                                                       value="<?php echo e($clientser1->typeofservice1); ?>">
                                                                            <?php else: ?>
                                                                                <input readonly type="text"
                                                                                       class="form-control"
                                                                                       <?php $__currentLoopData = $typeofser; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $typeofser1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  <?php if($typeofser1->id==$clientser1->typeofservice): ?> value="<?php echo e($typeofser1->typeofservice); ?>" <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>>
                                                                            <?php endif; ?>
                                                                        </td>
                                                                        <td style="width:10% !important">
                                                                            <select name="periods[]"
                                                                                    class="form-control"
                                                                                    style="display:none">
                                                                                <option value="">Select</option>
                                                                                <?php $__currentLoopData = $period; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $period1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                                                                    <option value="<?php echo e($period1->id); ?>"
                                                                                            <?php if($period1->id==$prds[0]): ?> selected <?php endif; ?>><?php echo e($period1->period); ?></option>
                                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                            </select>
                                                                            <input readonly type="text"
                                                                                   class="form-control"
                                                                                   <?php $__currentLoopData = $period; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $period1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  <?php if($period1->id==$prds[0]): ?> value="<?php echo e($period1->period); ?>" <?php endif; ?>  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>>
                                                                        </td>
                                                                        <td style="width:20% !important">
                                                                            <select name="employee_r[]"
                                                                                    class="form-control">
                                                                                <option value="">Select</option>

                                                                                <?php $__currentLoopData = $employee1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $emp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                                    <?php //echo $clientser1->employee_id;//print_r($emp);?>
                                                                                    <?php  $empp1 = explode(',', $emp->emp_service1);?>
                                                                                    <?php if(in_array($clientser1->typeofservice,$empp1)): ?>
                                                                                        <option value="<?php echo e($emp->id); ?>"
                                                                                                <?php if($clientser1->employee_id==$emp->id): ?> selected <?php endif; ?>><?php echo e($emp->firstName); ?> <?php if($emp->teams !=''): ?>
                                                                                                (<?php echo e($emp->teams); ?>

                                                                                                )<?php endif; ?></option>
                                                                                    <?php endif; ?>
                                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                            </select>

                                                                        </td>
                                                                        <td style="width:20% !important">
                                                                            <select class="form-control"
                                                                                    name="acc_check_by[]">
                                                                                <option value="">Select</option>
                                                                                <?php $__currentLoopData = $employee1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $emp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                                    <?php $empp2 = explode(',', $emp->acc_service_1);?>

                                                                                    <?php if(in_array($clientser1->typeofservice,$empp2)): ?>

                                                                                        <option value="<?php echo e($emp->id); ?>"
                                                                                                <?php if($clientser1->acc_check_by==$emp->id): ?> selected <?php endif; ?>><?php echo e($emp->firstName); ?> <?php if($emp->teams !=''): ?>
                                                                                                (<?php echo e($emp->teams); ?>

                                                                                                )<?php endif; ?></option>
                                                                                    <?php endif; ?>
                                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                                                            </select></td>
                                                                        <td style="width:38%">
                                                                            <input type="text" class="form-control"
                                                                                   name="monthlynote[]"
                                                                                   value="<?php echo e($clientser1->employeenote); ?>">
                                                                        </td>
                                                                    </tr>
                                                                    <script>
                                                                        /* var k=0;
           var row=<?php //echo count($clientser);?>;
        var y = k + row;
        var add_lock = $(".locked"); //Add button ID
        $(add_lock).on("click", function(e){ //on add input button click
        y++;
        
        });*/


                                                                        /*   $(document).ready(function(){
              $(document).on('click','.locked_<?php echo e($clientser1->id); ?>', function()
              { 
              
              var id =$(this).data("id");
              
              var servalue =$(this).data("servalue");
              var serviceid =$(this).data("serviceid");
              alert(serviceid);
              $.get('<?php echo URL::to('lock'); ?>?id='+id+'&clientid='+<?php echo e($clientser1->id); ?>+'&serviceid='+serviceid+'&servalue='+servalue, function(data)
              {  
                     $("#emptable").load(" #emptable > *");
                     $.each(data, function(index, subcatobj)
               {
                   
               })
              });
              });
              });
           */
                                                                    </script>


                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                                                <?php  $cnts12 = -1;
                                                                if ($common->business_id == '6') {
                                                                    $taxt1 = $taxservices1;

                                                                } else {
                                                                    $taxt1 = $taxservices;
                                                                }?>
                                                                <?php $__currentLoopData = $taxt1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $clientser1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                    <?php
                                                                    $cnts12++;

                                                                    //echo "<pre>";
                                                                    //print_r($clientser1);?>
                                                                    <tr>
                                                                    <!--  <input type="hidden" class="form-control" name="serviceid[]" value="<?php echo e($clientser1->id); ?>">
           !-->
                                                                        <input type="hidden" class="form-control"
                                                                               name="taxempid[]"
                                                                               value="<?php echo e($clientser1->id); ?>">
                                                                        <td style="width:25% !important;">
                                                                            <input readonly type="text"
                                                                                   class="form-control"
                                                                                   <?php $__currentLoopData = $taxtitle; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $taxtitle1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  <?php if($taxtitle1->id==$clientser1->taxation_service): ?> value="<?php echo e($taxtitle1->title); ?>" <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                            ">
                                                                        </td>
                                                                        <td style="width:10% !important">
                                                                            <select name="periods[]"
                                                                                    class="form-control"
                                                                                    style="display:none">
                                                                                <option value="">Select</option>
                                                                                <?php $__currentLoopData = $period; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $period1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                                    <option value="<?php echo e($period1->id); ?>"
                                                                                            <?php if($period1->id==$clientser1->taxation_service_period): ?> selected <?php endif; ?>><?php echo e($period1->period); ?></option>
                                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                            </select>
                                                                            <input readonly type="text"
                                                                                   class="form-control"
                                                                                   value="<?php echo e($clientser1->taxation_service_period); ?>">
                                                                        </td>
                                                                        <td style="width:20% !important">
                                                                            <select name="clemployee[]"
                                                                                    class="form-control taxupdateemployee_<?php echo $clientser1->id;?>">
                                                                                <option value="">Select</option>

                                                                                <?php $__currentLoopData = $employee1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $emp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                                    <?php  $empp3 = explode(',', $emp->emp_service2);?>

                                                                                    <?php if(in_array($clientser1->taxation_service,$empp3)): ?>

                                                                                        <option value="<?php echo e($emp->id); ?>"
                                                                                                <?php if($clientser1->employee_id==$emp->id): ?> selected <?php endif; ?>><?php echo e($emp->firstName); ?> <?php if($emp->teams !=''): ?>
                                                                                                (<?php echo e($emp->teams); ?>

                                                                                                )<?php endif; ?></option>
                                                                                    <?php endif; ?>
                                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                            </select>


                                                                        </td>
                                                                        <td style="width:20% !important">
                                                                            <select name="tax_acc_checkby[]"
                                                                                    class="form-control taxupdateacccheckby_<?php echo $clientser1->id;?>">
                                                                                <option value="">Select</option>

                                                                                <?php $__currentLoopData = $employee1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $emp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                                    <?php  $empp4 = explode(',', $emp->acc_service_2);?>

                                                                                    <?php if(in_array($clientser1->taxation_service,$empp4)): ?>

                                                                                        <option value="<?php echo e($emp->id); ?>"
                                                                                                <?php if($clientser1->tax_acc_checkby==$emp->id): ?> selected <?php endif; ?>><?php echo e($emp->firstName); ?> <?php if($emp->teams !=''): ?>
                                                                                                (<?php echo e($emp->teams); ?>

                                                                                                )<?php endif; ?></option>
                                                                                    <?php endif; ?>

                                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                            </select>


                                                                        </td>


                                                                        <td style="width:38%">
                                                                            <input type="text" class="form-control"
                                                                                   name="taxationnote[]"
                                                                                   value="<?php echo e($clientser1->note); ?>">
                                                                        </td>
                                                                    </tr>
                                                                    <script>
                                                                        /* var k=0;
           var row=<?php //echo count($clientser);?>;
        var y = k + row;
        var add_lock = $(".locked"); //Add button ID
        $(add_lock).on("click", function(e){ //on add input button click
        y++;
        
        });*/
                                                                        /*   $(document).ready(function(){
              $(document).on('click','.locked_<?php echo e($clientser1->id); ?>', function()
              { 
              
              var id =$(this).data("id");
              
              var servalue =$(this).data("servalue");
              var serviceid =$(this).data("serviceid");
              alert(serviceid);
              $.get('<?php echo URL::to('lock'); ?>?id='+id+'&clientid='+<?php echo e($clientser1->id); ?>+'&serviceid='+serviceid+'&servalue='+servalue, function(data)
              {  
                     $("#emptable").load(" #emptable > *");
                     $.each(data, function(index, subcatobj)
               {
                   
               })
              });
              });
              });
           */
                                                                    </script>


                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


                                                                </tbody>
                                                            </table>
                                                            <!--<a class="btn btn-primary pull-right" onclick="employee_responsivility();" style="margin-right: 0;"> Add</a>-->
                                                        </div>
                                                    <?php $cnts = 0;
                                                    // print_r($common);
                                                    $accountingtable1 = count($accountingtable);
                                                    if($common->business_id != '6')
                                                    {?>
                                                    <?php if($accountingtable1 >'0'): ?>
                                                        <?php $__currentLoopData = $accountingtable; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $accountingtable2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php $cnts++;?>
                                                            <!--<div class="col-md-3 yellowback">Type of Work</div><div class="col-md-3 yellowback"> Software</div><div class="col-md-3 yellowback">Location</div>-->
                                                                <div style="clear:both;"></div>
                                                                <div id="accountsoftware">
                                                                    <div class="form-group">
                                                                        <div class="col-md-3">
                                                                            <label>Type of Work</label>
                                                                            <div class="">
                                                                                <select class="form-control typeofwork_1"
                                                                                        name="typeofwork[]">
                                                                                    <option value="">Select</option>
                                                                                    <option value="Payroll"
                                                                                            <?php if($accountingtable2->typeofwork =='Payroll'): ?> selected <?php endif; ?>>
                                                                                        Payroll
                                                                                    </option>
                                                                                    <option value="Accounting"
                                                                                            <?php if($accountingtable2->typeofwork =='Accounting'): ?> selected <?php endif; ?>>
                                                                                        Accounting
                                                                                    </option>
                                                                                    <option value="Taxation"
                                                                                            <?php if($accountingtable2->typeofwork =='Taxation'): ?> selected <?php endif; ?>>
                                                                                        Taxation
                                                                                    </option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <label>Software</label>
                                                                            <div class="">
                                                                                <select class="form-control acsoftware"
                                                                                        name="accounting_software[]">
                                                                                    <option class="taxxation1" value="">
                                                                                        Select
                                                                                    </option>
                                                                                    <option class="proll"
                                                                                            value="Drake Accounting"
                                                                                            <?php if($accountingtable2->accounting_software =='Drake Accounting'): ?> selected <?php endif; ?>>
                                                                                        Drake Accounting
                                                                                    </option>
                                                                                    <option class="taxxation"
                                                                                            value="Drake Tax"
                                                                                            <?php if($accountingtable2->accounting_software =='Drake Tax'): ?> selected <?php endif; ?>>
                                                                                        Drake Tax
                                                                                    </option>
                                                                                    <option class="acount"
                                                                                            value="Quickbook"
                                                                                            <?php if($accountingtable2->accounting_software =='Quickbook'): ?> selected <?php endif; ?>>
                                                                                        Quickbook
                                                                                    </option>
                                                                                    <option class="proll acount"
                                                                                            value="Sage"
                                                                                            <?php if($accountingtable2->accounting_software =='Sage'): ?> selected <?php endif; ?>>
                                                                                        Sage
                                                                                    </option>
                                                                                <!--       <option value="Sage+Quickbook" <?php if($accountingtable2->accounting_software =='Sage+Quickbook'): ?> selected <?php endif; ?>>Sage+Quickbook</option>!-->
                                                                                    <option class="proll" value="Sage"
                                                                                            <?php if($accountingtable2->accounting_software =='Sage'): ?> selected <?php endif; ?>>
                                                                                        Sage
                                                                                    </option>

                                                                                </select>

                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <label>Location</label>
                                                                            <div class="">
                                                                                <select class="form-control"
                                                                                        name="accounting_location[]">
                                                                                    <option value="">Select</option>
                                                                                    <option value="Client Location"
                                                                                            <?php if($accountingtable2->accounting_location =='Client Location'): ?> selected <?php endif; ?>>
                                                                                        Client
                                                                                    </option>
                                                                                    <option value="FSC - Server"
                                                                                            <?php if($accountingtable2->accounting_location =='FSC - Server'): ?> selected <?php endif; ?>>
                                                                                        FSC Server
                                                                                    </option>
                                                                                    <option value="On the Cloud"
                                                                                            <?php if($accountingtable2->accounting_location =='On the Cloud'): ?> selected <?php endif; ?>>
                                                                                        Cloud
                                                                                    </option>
                                                                                </select>

                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-2">

                                                                            <?php if($cnts =='1'): ?><a
                                                                                    class="btn btn-primary  addacrow"
                                                                                    style="cursor:pointer;margin-top:25px;"><i
                                                                                        class="fa fa-plus"></i></a><?php endif; ?>

                                                                            <?php if($cnts > '1'): ?><a
                                                                                    class="btn btn-danger deleteacc"
                                                                                    id="<?php echo e($accountingtable2->id); ?>"
                                                                                    style="cursor:pointer;margin-top:25px;"><i
                                                                                        class="fa fa-minus"></i></a><?php endif; ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        <?php else: ?>

                                                            <div id="accountsoftware">
                                                                <div class="row">
                                                                    <div class="col-md-3">
                                                                        <div class="form-group">
                                                                            <label class="control-label yellowback">Type
                                                                                of Work</label>
                                                                            <select class="form-control"
                                                                                    name="typeofwork[]">
                                                                                <option value="">Select</option>
                                                                                <option value="Payroll">Payroll</option>
                                                                                <option value="Accounting">Accounting
                                                                                </option>
                                                                                <option value="Taxation">Taxation
                                                                                </option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <div class="form-group">
                                                                            <label class="control-label yellowback">Software</label>
                                                                            <select class="form-control"
                                                                                    name="accounting_software[]">
                                                                                <option value="">Select</option>
                                                                                <option value="Drake Accounting">Drake
                                                                                    Accounting
                                                                                </option>
                                                                                <option value="Drake Tax">Drake Tax
                                                                                </option>
                                                                                <option value="Quickbook">Quickbook
                                                                                </option>
                                                                                <option value="Sage">Sage</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <div class="form-group">
                                                                            <label class="control-label yellowback">Location</label>
                                                                            <select class="form-control"
                                                                                    name="accounting_location[]">
                                                                                <option value="">Select</option>
                                                                                <option value="Client">Client</option>
                                                                                <option value="FSC Server">FSC Server
                                                                                </option>
                                                                                <option value="Cloud">Cloud</option>
                                                                            </select>

                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-2">

                                                                        <a class="btn btn-primary  addacrow"
                                                                           style="cursor:pointer;mrgin-top:25px;"><i
                                                                                    class="fa fa-plus"></i></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php endif; ?>
                                                        <?php

                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="tab-pane fade" id="tab6primary">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="input_fields_wrap_text1">
                                                            <div class="Branch">
                                                                <h1 class="text-left" style="margin-left:10px;">
                                                                    Subscription Access
                                                                    <div class="active-width">
                                                                        <select name="subscription_active"
                                                                                id="subscription_active"
                                                                                class="form-control">
                                                                            <option value="">Select</option>
                                                                            <option value="1"
                                                                                    <?php if($common->subscription_active=='1'): ?> selected <?php endif; ?>>
                                                                                Active
                                                                            </option>
                                                                            <option value="2"
                                                                                    <?php if($common->subscription_active=='2'): ?> selected <?php endif; ?>>
                                                                                Inactive
                                                                            </option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="active-width">
                                                                        <select name="subscription_lock"
                                                                                id="subscription_lock"
                                                                                class="form-control">
                                                                            <option value="">Select</option>
                                                                            <option value="1"
                                                                                    <?php if($common->subscription_lock=='1'): ?> selected <?php endif; ?>>
                                                                                Lock
                                                                            </option>
                                                                            <option value="2"
                                                                                    <?php if($common->subscription_lock=='2'): ?> selected <?php endif; ?>>
                                                                                Unlock
                                                                            </option>
                                                                        </select>
                                                                    </div>
                                                                </h1>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-5 col-sm-12">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4">Primary User:</label>
                                                            <div class="col-md-8">
                                                                <input type="text" readonly class="form-control"
                                                                       id="subscription_user"
                                                                       value="<?php echo e($common->subscription_user); ?>"
                                                                       name="subscription_user">
                                                            </div>
                                                        </div>
                                                        <div class="form-group subscription_hidden2">
                                                            <label class="control-label col-md-4">Security Q-1 :</label>
                                                            <div class="col-md-8">
                                                                <select name="subscription_question1"
                                                                        id="subscription_question1"
                                                                        class="form-control disabledd" readonly>
                                                                    <?php if(isset($common->subscription_question1)): ?>
                                                                        <option value="<?php echo e($common->subscription_question1); ?>"><?php echo e($common->subscription_question1); ?></option>
                                                                    <?php else: ?>
                                                                        <option value=""></option>
                                                                        @endelse
                                                                    <?php endif; ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group subscription_hidden2">
                                                            <label class="control-label col-md-4">Security Q-2 :</label>
                                                            <div class="col-md-8">
                                                                <select name="subscription_question2"
                                                                        id="subscription_question2"
                                                                        class="form-control disabledd" readonly>
                                                                    <?php if(isset($common->subscription_question2)): ?>
                                                                        <option value="<?php echo e($common->subscription_question2); ?>"><?php echo e($common->subscription_question2); ?></option>
                                                                    <?php else: ?>
                                                                        <option value="">-</option>
                                                                        @endelse
                                                                    <?php endif; ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group subscription_hidden2">
                                                            <label class="control-label col-md-4">Security Q-3 :</label>
                                                            <div class="col-md-8">
                                                                <select name="subscription_question2"
                                                                        id="subscription_question2"
                                                                        class="form-control disabledd" readonly>
                                                                    <?php if(isset($common->subscription_question2)): ?>
                                                                        <option value="<?php echo e($common->subscription_question2); ?>"><?php echo e($common->subscription_question2); ?></option>
                                                                    <?php else: ?>
                                                                        <option value=""></option>
                                                                        @endelse
                                                                    <?php endif; ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-1 col-sm-12">
                                                        <div class="" style="margin-top:8px;">
                                                            <input type="checkbox" class="check" value="1"
                                                                   id="user_access3" name="user_access3">
                                                            <label for="user_access3">Show</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4 col-xs-12 left_991">Reset
                                                                Days:</label>
                                                            <div class="col-md-4 col-xs-6">
                                                                <select name="subscription_resetdays"
                                                                        id="subscription_resetdays"
                                                                        class="form-control disabledd" readonly>
                                                                    <option value="">---Select---</option>
                                                                    <option value="30"
                                                                            <?php if($common->subscription_resetdays=="30"): ?> selected <?php endif; ?>>
                                                                        30
                                                                    </option>
                                                                    <option value="45"
                                                                            <?php if($common->subscription_resetdays=="45"): ?> selected <?php endif; ?>>
                                                                        45
                                                                    </option>
                                                                    <option value="90"
                                                                            <?php if($common->subscription_resetdays=="90"): ?> selected <?php endif; ?>>
                                                                        90
                                                                    </option>
                                                                    <option value="120"
                                                                            <?php if($common->subscription_resetdays=="120"): ?> selected <?php endif; ?>>
                                                                        120
                                                                    </option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-4 col-xs-6"><input
                                                                        name="subscription_resetdate"
                                                                        value="<?php echo e($common->subscription_resetdate); ?>"
                                                                        readonly type="text" id="subscription_resetdate"
                                                                        class="form-control"/></div>
                                                        </div>
                                                        <div class="form-group subscription_hidden2">
                                                            <label class="control-label col-md-4">Anwser-1 :</label>
                                                            <div class="col-md-8">
                                                                <input readonly name="subscription_answer1"
                                                                       value="<?php echo e($common->subscription_answer1); ?>"
                                                                       type="text" readonly id="subscription_answer1"
                                                                       class="form-control"/>
                                                            </div>
                                                        </div>
                                                        <div class="form-group subscription_hidden2">
                                                            <label class="control-label col-md-4">Anwser-2 :</label>
                                                            <div class="col-md-8">
                                                                <input name="subscription_answer2" readonly
                                                                       value="<?php echo e($common->subscription_answer2); ?>"
                                                                       type="text" readonly id="subscription_answer2"
                                                                       class="form-control"/>
                                                            </div>
                                                        </div>
                                                        <div class="form-group subscription_hidden2">
                                                            <label class="control-label col-md-4">Anwser-3 :</label>
                                                            <div class="col-md-8">
                                                                <input name="subscription_answer3" readonly
                                                                       value="<?php echo e($common->subscription_answer3); ?>"
                                                                       type="text" id="subscription_answer3"
                                                                       class="form-control" readonly/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="input_fields_wrap_text1">
                                                            <div class="Branch">
                                                                <h1 class="text-left" style="margin-left:10px;">Limited
                                                                    Access
                                                                    <div class="form-group active-width">
                                                                        <select name="limited_active"
                                                                                id="limited_active"
                                                                                class="form-control">
                                                                            <option value="">Select</option>
                                                                            <option value="1"
                                                                                    <?php if($common->limited_active=='1'): ?> selected <?php endif; ?>>
                                                                                Active
                                                                            </option>
                                                                            <option value="2"
                                                                                    <?php if($common->limited_active=='2'): ?> selected <?php endif; ?>>
                                                                                Inactive
                                                                            </option>
                                                                        </select>
                                                                    </div>
                                                                </h1>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-5 col-sm-12">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4">Primary User:</label>
                                                            <div class="col-md-8">
                                                                <input type="text" class="form-control"
                                                                       id="limited_user" value="<?php echo e($common->email); ?>"
                                                                       name="limited_user" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="form-group subscription_hidden">
                                                            <label class="control-label col-md-4">Security Q-1 :</label>
                                                            <div class="col-md-8">
                                                                <select name="limited_question1" id="limited_question1"
                                                                        class="form-control disabledd" readonly>
                                                                    <option value=""></option>
                                                                    <option value="What was your favorite place to visit as a child?"
                                                                            <?php if($common->limited_question1=='What was your favorite place to visit as a child?'): ?> selected <?php endif; ?>>
                                                                        What was your favorite place to visit as a
                                                                        child?
                                                                    </option>
                                                                    <option value="Who is your favorite actor, musician, or artist?"
                                                                            <?php if($common->limited_question1=='Who is your favorite actor, musician, or artist?'): ?> selected <?php endif; ?>>
                                                                        Who is your favorite actor, musician, or artist?
                                                                    </option>
                                                                    <option value="What is the name of your favorite pet?"
                                                                            <?php if($common->limited_question1=='What is the name of your favorite pet?'): ?> selected <?php endif; ?>>
                                                                        What is the name of your favorite pet?
                                                                    </option>
                                                                    <option value="In what city were you born?"
                                                                            <?php if($common->limited_question1=='In what city were you born?'): ?> selected <?php endif; ?>>
                                                                        In what city were you born?
                                                                    </option>
                                                                    <option value="What is the name of your first school?"
                                                                            <?php if($common->limited_question1=='What is the name of your first school?'): ?> selected <?php endif; ?>>
                                                                        What is the name of your first school?
                                                                    </option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group subscription_hidden">
                                                            <label class="control-label col-md-4">Security Q-2 :</label>
                                                            <div class="col-md-8">
                                                                <select name="limited_question2" id="limited_question2"
                                                                        class="form-control disabledd" readonly>
                                                                    <option value=""></option>
                                                                    <option value="What is your favorite movie?"
                                                                            <?php if($common->limited_question2=='What is your favorite movie?'): ?> selected <?php endif; ?>>
                                                                        What is your favorite movie?
                                                                    </option>
                                                                    <option value="What was the make of your first car?"
                                                                            <?php if($common->limited_question2=='What was the make of your first car?'): ?> selected <?php endif; ?>>
                                                                        What was the make of your first car?
                                                                    </option>
                                                                    <option value="What is your favorite color?"
                                                                            <?php if($common->limited_question2=='What is your favorite color?'): ?> selected <?php endif; ?>>
                                                                        What is your favorite color?
                                                                    </option>
                                                                    <option value="What is your fathers middle name?"
                                                                            <?php if($common->limited_question2=='What is your fathers middle name?'): ?> selected <?php endif; ?>>
                                                                        What is your fathers middle name?
                                                                    </option>
                                                                    <option value="What is the name of your first grade teacher?"
                                                                            <?php if($common->limited_question2=='What is the name of your first grade teacher?'): ?> selected <?php endif; ?>>
                                                                        What is the name of your first grade teacher?
                                                                    </option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group subscription_hidden">
                                                            <label class="control-label col-md-4">Security Q-3 :</label>
                                                            <div class="col-md-8">
                                                                <select name="limited_question3" id="limited_question3"
                                                                        class="form-control disabledd" readonly>
                                                                    <option value=""></option>
                                                                    <option value="What was your high school mascot?"
                                                                            <?php if($common->limited_question3=='What was your high school mascot?'): ?> selected <?php endif; ?>>
                                                                        What was your high school mascot?
                                                                    </option>
                                                                    <option value="Which is your favorite web browser?"
                                                                            <?php if($common->limited_question3=='Which is your favorite web browser?'): ?> selected <?php endif; ?>>
                                                                        Which is your favorite web browser?
                                                                    </option>
                                                                    <option value="In what year was your father born?"
                                                                            <?php if($common->limited_question3=='In what year was your father born?'): ?> selected <?php endif; ?>>
                                                                        In what year was your father born?
                                                                    </option>
                                                                    <option value="What is the name of your favorite childhood friend?"
                                                                            <?php if($common->limited_question3=='What is the name of your favorite childhood friend?'): ?> selected <?php endif; ?>>
                                                                        What is the name of your favorite childhood
                                                                        friend?
                                                                    </option>
                                                                    <option value="What was your favorite food as a child?"
                                                                            <?php if($common->limited_question3=='What was your favorite food as a child?'): ?> selected <?php endif; ?>>
                                                                        What was your favorite food as a child?
                                                                    </option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-1 col-sm-12">
                                                        <div class="" style="margin-top:8px;">
                                                            <input type="checkbox" class="check" value="1"
                                                                   id="user_access4" name="user_access4"> <label
                                                                    for="user_access4">Show</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5 col-sm-12">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-4 col-xs-12 left_991">Reset
                                                                Days:</label>
                                                            <div class="col-md-4 col-xs-6">
                                                                <select name="limited_resetdays" id="limited_resetdays"
                                                                        class="form-control disabledd" readonly>
                                                                    <option value="">---Select---</option>
                                                                    <?php if($common->limited_active!=''): ?>
                                                                        <option value="30"
                                                                                <?php if($common->limited_resetdays=='30'): ?> selected <?php endif; ?>>
                                                                            30
                                                                        </option>
                                                                        <option value="45"
                                                                                <?php if($common->limited_resetdays=='45'): ?> selected <?php endif; ?>>
                                                                            45
                                                                        </option>
                                                                        <option value="90"
                                                                                <?php if($common->limited_resetdays=='90'): ?> selected <?php endif; ?>>
                                                                            90
                                                                        </option>
                                                                        <option value="120"
                                                                                <?php if($common->limited_resetdays=='120'): ?> selected <?php endif; ?>>
                                                                            120
                                                                        </option><?php endif; ?>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-4 col-xs-6">
                                                                <input name="limited_resetdate" readonly
                                                                       value=" <?php if($common->limited_active!=''): ?><?php echo e($common->limited_resetdate); ?><?php endif; ?>"
                                                                       type="text" id="limited_reset_date"
                                                                       class="form-control"/>
                                                            </div>
                                                        </div>
                                                        <div class="form-group subscription_hidden">
                                                            <label class="control-label col-md-4">Anwser-1 :</label>
                                                            <div class="col-md-8">
                                                                <input readonly name="limited_answer1" readonly
                                                                       value="<?php echo e($common->limited_answer1); ?> "
                                                                       type="text" id="limited_answer1"
                                                                       class="form-control"/>
                                                            </div>
                                                        </div>
                                                        <div class="form-group subscription_hidden">
                                                            <label class="control-label col-md-4">Anwser-2 :</label>
                                                            <div class="col-md-8">
                                                                <input name="limited_answer2" readonly readonly
                                                                       value="<?php echo e($common->limited_answer2); ?>"
                                                                       type="text" id="limited_answer2"
                                                                       class="form-control"/>
                                                            </div>
                                                        </div>
                                                        <div class="form-group subscription_hidden">
                                                            <label class="control-label col-md-4">Anwser-3 :</label>
                                                            <div class="col-md-8">
                                                                <input name="limited_answer3" readonly
                                                                       value="<?php echo e($common->limited_answer3); ?>"
                                                                       type="text" id="limited_answer3"
                                                                       class="form-control"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                                //echo $countuser;
                                                if($countuser > 0 )
                                                {?>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="input_fields_wrap_text1">
                                                            <div class="Branch">
                                                                <h1>User Access
                                                                    <div class="form-group active-width">
                                                                        <select name="user_active" id="user_active"
                                                                                class="form-control">
                                                                            <option value="" selected>Select</option>
                                                                            <option value="1"
                                                                                    <?php if($common->user_active=='1'): ?> selected <?php endif; ?>>
                                                                                Active
                                                                            </option>
                                                                            <option value="2"
                                                                                    <?php if($common->user_active=='2'): ?> selected <?php endif; ?>>
                                                                                Inactive
                                                                            </option>
                                                                        </select>
                                                                    </div>
                                                                </h1>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <?php $__currentLoopData = $userinfos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $users): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <div class="col-md-5 col-sm-5 col-xs-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-4">Name:</label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control"
                                                                           id="user_name" readonly
                                                                           value="<?php echo e(ucwords($users->firstname_sec)); ?>"
                                                                           name="user_name">
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="col-md-1 col-sm-1 col-xs-12">&nbsp;</div>
                                                        <div class="col-md-5 col-sm-5 col-xs-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-4">Reset
                                                                    Days:</label>
                                                                <div class="col-md-4">
                                                                    <select name="user_resetdays" id="user_resetdays"
                                                                            class="form-control disabledd" readonly>
                                                                        <option value="">---Select---</option>
                                                                        <option value="30"
                                                                                <?php if($common->user_resetdays=='30'): ?> selected <?php endif; ?>>
                                                                            30
                                                                        </option>
                                                                        <option value="45"
                                                                                <?php if($common->user_resetdays=='45'): ?> selected <?php endif; ?>>
                                                                            45
                                                                        </option>
                                                                        <option value="90"
                                                                                <?php if($common->user_resetdays=='90'): ?> selected <?php endif; ?>>
                                                                            90
                                                                        </option>
                                                                        <option value="120"
                                                                                <?php if($common->user_resetdays=='120'): ?> selected <?php endif; ?>>
                                                                            120
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <input name="user_resetdate" value="" type="text"
                                                                           id="user_reset_date" class="form-control"
                                                                           readonly>
                                                                </div>
                                                            </div>

                                                        </div>

                                                        <div class="clear"></div>
                                                        <div class="col-md-5 col-sm-2 col-xs-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-4">User
                                                                    Email:</label>
                                                                <div class="col-md-8">
                                                                    <input type="email" class="form-control"
                                                                           id="user_email" readonly
                                                                           value="<?php echo e($users->email_sec); ?>"
                                                                           name="user_email">
                                                                    <span id="msg"></span>
                                                                    <span class="msg">A valid email address!</span>
                                                                </div>
                                                            </div>
                                                            <script>
                                                                $(document).ready(function () {
                                                                    $("#user_access2_<?php echo $users->id;?>").click(function () {
                                                                        if ($(this).is(":checked")) {
                                                                            $(".subscription_hidden1_<?php echo $users->id;?>").show();
                                                                            // $("#AddPassport").hide();
                                                                        } else {
                                                                            $(".subscription_hidden1_<?php echo $users->id;?>").hide();
                                                                            // $("#AddPassport").show();
                                                                        }
                                                                    });
                                                                });

                                                            </script>
                                                            <div class="form-group subscription_hidden1_<?php echo $users->id;?>"
                                                                 style="display:none;">
                                                                <label class="control-label col-md-4">Security Q-1
                                                                    :</label>
                                                                <div class="col-md-8">
                                                                    <select name="user_question1" id="user_question1"
                                                                            class="form-control disabledd" readonly>
                                                                        <option value=""></option>
                                                                        <option value="What was your favorite place to visit as a child?"
                                                                                <?php if($common->user_question1=='What was your favorite place to visit as a child?'): ?> selected <?php endif; ?>>
                                                                            What was your favorite place to visit as a
                                                                            child?
                                                                        </option>
                                                                        <option value="Who is your favorite actor, musician, or artist?"
                                                                                <?php if($common->user_question1=='Who is your favorite actor, musician, or artist?'): ?> selected <?php endif; ?>>
                                                                            Who is your favorite actor, musician, or
                                                                            artist?
                                                                        </option>
                                                                        <option value="What is the name of your favorite pet?"
                                                                                <?php if($common->user_question1=='What is the name of your favorite pet?'): ?> selected <?php endif; ?>>
                                                                            What is the name of your favorite pet?
                                                                        </option>
                                                                        <option value="In what city were you born?"
                                                                                <?php if($common->user_question1=='In what city were you born?'): ?> selected <?php endif; ?>>
                                                                            In what city were you born?
                                                                        </option>
                                                                        <option value="What is the name of your first school?"
                                                                                <?php if($common->user_question1=='What is the name of your first school?'): ?> selected <?php endif; ?>>
                                                                            What is the name of your first school?
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group subscription_hidden1_<?php echo $users->id;?>"
                                                                 style="display:none;">
                                                                <label class="control-label col-md-4">Security Q-2
                                                                    :</label>
                                                                <div class="col-md-8">
                                                                    <select name="user_question2" id="user_question2"
                                                                            class="form-control disabledd" readonly>
                                                                        <option value=""></option>
                                                                        <option value="What is your favorite movie?"
                                                                                <?php if($common->user_question2=='What is your favorite movie?'): ?> selected <?php endif; ?>>
                                                                            What is your favorite movie?
                                                                        </option>
                                                                        <option value="What was the make of your first car?"
                                                                                <?php if($common->user_question2=='What was the make of your first car?'): ?> selected <?php endif; ?>>
                                                                            What was the make of your first car?
                                                                        </option>
                                                                        <option value="What is your favorite color?"
                                                                                <?php if($common->user_question2=='What is your favorite color?'): ?> selected <?php endif; ?>>
                                                                            What is your favorite color?
                                                                        </option>
                                                                        <option value="What is your fathers middle name?"
                                                                                <?php if($common->user_question2=='What is your fathers middle name?'): ?> selected <?php endif; ?>>
                                                                            What is your fathers middle name?
                                                                        </option>
                                                                        <option value="What is the name of your first grade teacher?"
                                                                                <?php if($common->user_question2=='What is the name of your first grade teacher?'): ?> selected <?php endif; ?>>
                                                                            What is the name of your first grade
                                                                            teacher?
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group subscription_hidden1_<?php echo $users->id;?>"
                                                                 style="display:none;">
                                                                <label class="control-label col-md-4">Security Q-3
                                                                    :</label>
                                                                <div class="col-md-8">
                                                                    <select name="user_question3" id="user_question3"
                                                                            class="form-control disabledd" readonly>
                                                                        <option value=""></option>
                                                                        <option value="What was your high school mascot?"
                                                                                <?php if($common->user_question3=='What was your high school mascot?'): ?> selected <?php endif; ?>>
                                                                            What was your high school mascot?
                                                                        </option>
                                                                        <option value="Which is your favorite web browser?"
                                                                                <?php if($common->user_question3=='Which is your favorite web browser?'): ?> selected <?php endif; ?>>
                                                                            Which is your favorite web browser?
                                                                        </option>
                                                                        <option value="In what year was your father born?"
                                                                                <?php if($common->user_question3=='In what year was your father born?'): ?> selected <?php endif; ?>>
                                                                            In what year was your father born?
                                                                        </option>
                                                                        <option value="What is the name of your favorite childhood friend?"
                                                                                <?php if($common->user_question3=='What is the name of your favorite childhood friend?'): ?> selected <?php endif; ?>>
                                                                            What is the name of your favorite childhood
                                                                            friend?
                                                                        </option>
                                                                        <option value="What was your favorite food as a child?"
                                                                                <?php if($common->user_question3=='What was your favorite food as a child?'): ?> selected <?php endif; ?>>
                                                                            What was your favorite food as a child?
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-1 col-sm-1 col-xs-12">
                                                            <div class="form-group" style="margin-top:-40px;">
                                                                <input type="checkbox" class="check" value="1"
                                                                       id="user_access2_<?php echo e($users->id); ?>"
                                                                       name="user_access2">
                                                                <label for="user_access2_<?php echo e($users->id); ?>">Show</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-5 col-sm-5 col-xs-12">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-4">User Cell
                                                                    #:</label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control phone"
                                                                           id="user_cell"
                                                                           value="<?php echo e($users->mobile_sec_1); ?>"
                                                                           name="user_cell" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="form-group subscription_hidden1_<?php echo $users->id;?>"
                                                                 style="display:none;">
                                                                <label class="control-label col-md-4">Anwser-1 :</label>
                                                                <div class="col-md-8">
                                                                    <input name="user_answer1"
                                                                           value="<?php echo e($common->user_answer1); ?>"
                                                                           type="text" id="user_answer1"
                                                                           class="form-control" readonly/>
                                                                </div>
                                                            </div>
                                                            <div class="form-group subscription_hidden1_<?php echo $users->id;?>"
                                                                 style="display:none;">
                                                                <label class="control-label col-md-4">Anwser-2 :</label>
                                                                <div class="col-md-8">
                                                                    <input name="user_answer2" readonly
                                                                           value="<?php echo e($common->user_answer2); ?>"
                                                                           type="text" id="user_answer2"
                                                                           class="form-control" readonly/>
                                                                </div>
                                                            </div>
                                                            <div class="form-group subscription_hidden1_<?php echo $users->id;?>"
                                                                 style="display:none;">
                                                                <label class="control-label col-md-4">Anwser-3 :</label>
                                                                <div class="col-md-8">
                                                                    <input name="user_answer3" readonly
                                                                           value="<?php echo e($common->user_answer3); ?>"
                                                                           type="text" id="user_answer3"
                                                                           class="form-control" readonly/>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </div>
                                                <?php
                                                }
                                                ?>

                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="input_fields_wrap_text1">
                                                            <div class="Branch">
                                                                <h1>Access By Following Person
                                                                    <div class="form-group active-width">
                                                                    </div>
                                                                </h1>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <div class="col-md-2 col-sm-4 col-xs-6">
                                                                <input type="checkbox" id="adminonly" name="adminonly"
                                                                       value="1" checked> <label class="fsc-form-label"
                                                                                                 style="text-align:none !important;"
                                                                                                 for="adminonly"> Admin
                                                                    Only</label>
                                                                <!--<p><label><input type="checkbox" /> Option 1</label></p>-->
                                                            </div>
                                                            <div class="col-md-2 col-sm-4 col-xs-6">
                                                                <input type="checkbox" id="supervisor" name="supervisor"
                                                                       value="1"
                                                                       <?php if($common->supervisor==1): ?> checked <?php else: ?>  <?php endif; ?>>
                                                                <label class="fsc-form-label" for="supervisor"
                                                                       style="text-align:none !important;"> FSC
                                                                    Supervisor</label>
                                                                <!--<input type="checkbox" /> Option 2</label></p>-->
                                                            </div>
                                                            <div class="col-md-2 col-sm-4 col-xs-6">
                                                                <input type="checkbox" id="fscee" name="fscee" value="1"
                                                                       <?php if($common->fscee==1): ?> checked <?php else: ?>  <?php endif; ?>>
                                                                <label class="fsc-form-label" for="fscee"
                                                                       style="text-align:none !important;"> FSC
                                                                    EE</label>
                                                                <!--<p><label><input type="checkbox" /> Option 3</label></p>-->
                                                            </div>
                                                            <div class="col-md-2 col-sm-4 col-xs-6">
                                                                <input type="checkbox" id="fscuser" name="fscuser"
                                                                       value="1"
                                                                       <?php if($common->fscuser==1): ?> checked <?php else: ?>  <?php endif; ?>>
                                                                <label class="fsc-form-label" for="fscuser"
                                                                       style="text-align:none !important;"> FSC
                                                                    User</label>
                                                                <!--<p><label><input type="checkbox" /> Option 4</label></p>-->
                                                            </div>
                                                            <div class="col-md-2 col-sm-4 col-xs-6">
                                                                <input type="checkbox" id="client" name="client"
                                                                       value="1"
                                                                       <?php if($common->client==1): ?> checked <?php else: ?>  <?php endif; ?>>
                                                                <label class="fsc-form-label" for="client"
                                                                       style="text-align:none !important;">Client</label>
                                                                <!--<p><label><input type="checkbox" /> Option 5</label></p>-->
                                                            </div>
                                                            <div class="col-md-2 col-sm-4 col-xs-6">
                                                                <input type="checkbox" id="checkAll" name="alluser"
                                                                       value="1"
                                                                       <?php if($common->alluser==1): ?> checked <?php else: ?>  <?php endif; ?>>
                                                                <label class="fsc-form-label" for="checkAll"
                                                                       style="text-align:none !important;"> All</label>
                                                                <!--<p><label><input type="checkbox" id="checkAll"/> Check all</label></p>-->
                                                            </div>

                                                        </div>

                                                        <script>

                                                            $("#checkAll").change(function () {
                                                                $("input:checkbox").prop('checked', $(this).prop("checked"));
                                                            });
                                                        </script>

                                                    <!-- <div class="form-group subscription_hidden">
           <label class="control-label col-md-4">Security Q-1 :</label>
           <div class="col-md-8">
           <select name="limited_question1" id="limited_question1" class="form-control disabledd" readonly>
           <option value=""> </option>
           <option value="What was your favorite place to visit as a child?" <?php if($common->limited_question1=='What was your favorite place to visit as a child?'): ?> selected <?php endif; ?>>What was your favorite place to visit as a child?</option>
           <option value="Who is your favorite actor, musician, or artist?" <?php if($common->limited_question1=='Who is your favorite actor, musician, or artist?'): ?> selected <?php endif; ?>>Who is your favorite actor, musician, or artist?</option>
           <option value="What is the name of your favorite pet?" <?php if($common->limited_question1=='What is the name of your favorite pet?'): ?> selected <?php endif; ?>>What is the name of your favorite pet?</option>
           <option value="In what city were you born?" <?php if($common->limited_question1=='In what city were you born?'): ?> selected <?php endif; ?>>In what city were you born?</option>
           <option value="What is the name of your first school?" <?php if($common->limited_question1=='What is the name of your first school?'): ?> selected <?php endif; ?>>What is the name of your first school?</option>
           </select>
           </div>
           </div>
           <div class="form-group subscription_hidden">
           <label class="control-label col-md-4">Security Q-2 :</label>
           <div class="col-md-8">
           <select name="limited_question2" id="limited_question2" class="form-control disabledd" readonly>
           <option value=""> </option>
           <option value="What is your favorite movie?" <?php if($common->limited_question2=='What is your favorite movie?'): ?> selected <?php endif; ?>>What is your favorite movie?</option>
           <option value="What was the make of your first car?" <?php if($common->limited_question2=='What was the make of your first car?'): ?> selected <?php endif; ?>>What was the make of your first car?</option>
           <option value="What is your favorite color?" <?php if($common->limited_question2=='What is your favorite color?'): ?> selected <?php endif; ?>>What is your favorite color?</option>
           <option value="What is your fathers middle name?" <?php if($common->limited_question2=='What is your fathers middle name?'): ?> selected <?php endif; ?>>What is your fathers middle name?</option>
           <option value="What is the name of your first grade teacher?" <?php if($common->limited_question2=='What is the name of your first grade teacher?'): ?> selected <?php endif; ?>>What is the name of your first grade teacher?</option>
           </select>
           </div>
           </div>
           <div class="form-group subscription_hidden">
           <label class="control-label col-md-4">Security Q-3 :</label>
           <div class="col-md-8">
           <select name="limited_question3" id="limited_question3" class="form-control disabledd" readonly>
           <option value=""> </option>
           <option value="What was your high school mascot?" <?php if($common->limited_question3=='What was your high school mascot?'): ?> selected <?php endif; ?>>What was your high school mascot?</option>
           <option value="Which is your favorite web browser?" <?php if($common->limited_question3=='Which is your favorite web browser?'): ?> selected <?php endif; ?>>Which is your favorite web browser?</option>
           <option value="In what year was your father born?" <?php if($common->limited_question3=='In what year was your father born?'): ?> selected <?php endif; ?>>In what year was your father born?</option>
           <option value="What is the name of your favorite childhood friend?" <?php if($common->limited_question3=='What is the name of your favorite childhood friend?'): ?> selected <?php endif; ?>>What is the name of your favorite childhood friend?</option>
           <option value="What was your favorite food as a child?" <?php if($common->limited_question3=='What was your favorite food as a child?'): ?> selected <?php endif; ?>>What was your favorite food as a child?</option>
           </select>
           </div>
           </div>!-->
                                                    </div>
                                                </div>

                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"
                                                     style="margin-top:3%;">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12"></div>
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                                                    </div>
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12"></div>
                                                </div>
                                            </div>

                                            <div class="tab-pane fade" id="tab7primary">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <!-- Nav tabs -->
                                                        <div class="card">
                                                            <ul class="nav nav-tabs" role="tablist">
                                                                <li role="presentation" class="active"><a href="#home"
                                                                                                          aria-controls="home"
                                                                                                          role="tab"
                                                                                                          data-toggle="tab">
                                                                        <span>Admin Note Only</span></a></li>
                                                                <li role="presentation"><a href="#profile"
                                                                                           aria-controls="profile"
                                                                                           role="tab" data-toggle="tab">
                                                                        <span>For FSC Only</span></a></li>
                                                                <li role="presentation"><a href="#messages"
                                                                                           aria-controls="messages"
                                                                                           role="tab" data-toggle="tab"><span>For Client Only</span></a>
                                                                </li>
                                                                <li role="presentation"><a href="#settings"
                                                                                           aria-controls="settings"
                                                                                           role="tab" data-toggle="tab"><span>For Everybody</span></a>
                                                                </li>
                                                                <!--<div style="clear:both"></div>-->
                                                                <li role="presentation"><a href="#home1"
                                                                                           aria-controls="home1"
                                                                                           role="tab" data-toggle="tab">
                                                                        <span>Link</span></a></li>
                                                                <li role="presentation"><a href="#payments"
                                                                                           aria-controls="payments"
                                                                                           role="tab" data-toggle="tab"><span>Payment</span></a>
                                                                </li>
                                                                <?php if($common->business_id=='6'): ?>  <?php else: ?>
                                                                    <li role="presentation"><a href="#location"
                                                                                               aria-controls="location"
                                                                                               role="tab"
                                                                                               data-toggle="tab"><span>Location</span></a>
                                                                    </li><?php endif; ?>
                                                                <li role="presentation"><a href="#otherinfo"
                                                                                           aria-controls="otherinfo"
                                                                                           role="tab" data-toggle="tab"><span><?php if($common->business_id=='6'): ?>
                                                                                Family <?php else: ?> Other <?php endif; ?></span></a>
                                                                </li>
                                                            </ul>
                                                            <!-- Tab panes -->
                                                            <div class="tab-content">
                                                                <div role="tabpanel" class="tab-pane" id="home1">
                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                        <br/>
                                                                        <div class="Branch">
                                                                            <h1>Customer Link List</h1>
                                                                        </div>

                                                                        <div class="after-add-more">
                                                                            <?php $strposlink = $common->productid; $splittedstringposlink = explode(",", $strposlink);?>
                                                                            <div class="form-group <?php echo e($errors->has('productid') ? ' has-error' : ''); ?>">
                                                                                <label class="control-label col-md-3">Link
                                                                                    Client ID :</label>
                                                                                <div class="col-md-6">
                                                                                    <select class="js-example-tags form-control"
                                                                                            style="width:86.5%"
                                                                                            name="productid[]"
                                                                                            id="vendor_product"
                                                                                            multiple="multiple">
                                                                                        <?php $__currentLoopData = $commonuser; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subcustomer1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                                            <?php
                                                                                            if($subcustomer1->filename != $common->filename)
                                                                                            {
                                                                                            if ($subcustomer1->business_id == '6') {
                                                                                                $entityname = $subcustomer1->first_name . ' ' . $subcustomer1->last_name;
                                                                                            } else {
                                                                                                $entityname = $subcustomer1->company_name;
                                                                                            }
                                                                                            ?>
                                                                                            <option value="<?php echo e($subcustomer1->filename); ?>"
                                                                                                    <?php $__currentLoopData = $splittedstringposlink; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php if($value ==$subcustomer1->filename): ?> selected <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> ><?php echo e($subcustomer1->filename); ?>

                                                                                                (<?php echo e($entityname); ?>)
                                                                                            </option>
                                                                                            <?php } ?>
                                                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                                    </select>
                                                                                    <?php if($errors->has('productid')): ?>
                                                                                        <span class="help-block">
           <strong><?php echo e($errors->first('productid')); ?></strong>
           </span>
                                                                                    <?php endif; ?>
                                                                                </div>
                                                                            </div>

                                                                            <div class="form-group">
                                                                                <label class="control-label col-md-3">Link
                                                                                    With Client :</label>
                                                                                <div class="col-md-9">
                                                                                    <?php
                                                                                    foreach($linkuser as $linkuse1)
                                                                                    {
                                                                                    ?>

                                                                                    <?php
                                                                                    if (!empty($linkuse1->filename)) {
                                                                                        // echo $linkuser->company_name;
                                                                                        if ($linkuse1->business_id == '6') {
                                                                                            $entitysname = $linkuse1->first_name . ' ' . $linkuse1->last_name;
                                                                                        } else {
                                                                                            $entitysname = $linkuse1->company_name;
                                                                                        }
                                                                                    }
                                                                                    ?>
                                                                                    <?php if(!empty($linkuse1->filename))
                                                                                    {
                                                                                    ?>
                                                                                    <span class='btn btn-success'><?php echo $linkuse1->filename;?> <?php echo '(' . $entitysname . ')';?></span>
                                                                                    <?php
                                                                                    }

                                                                                    ?>

                                                                                    <?php
                                                                                    }
                                                                                    ?>
                                                                                </div>
                                                                            </div>


                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div role="tabpanel" class="tab-pane active" id="home">
                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                        <div class="input_fields_wrap">
                                                                            <?php $l = 1;$notecon1 = count($admin_notes);?>
                                                                            <?php if($notecon1!=NULL): ?>
                                                                                <?php $__currentLoopData = $admin_notes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $notes): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                                    <input name="noteid[]"
                                                                                           value="<?php echo e($notes->id); ?>"
                                                                                           type="hidden" placeholder=""
                                                                                           id="noteid"
                                                                                           class=" form-control">
                                                                                    <input name="usid[]"
                                                                                           value="<?php echo e($notes->admin_id); ?>"
                                                                                           type="hidden" placeholder=""
                                                                                           id="usid"
                                                                                           class=" form-control">
                                                                                    <input name="notetype[]"
                                                                                           value="<?php echo e($notes->type); ?>"
                                                                                           type="hidden"
                                                                                           placeholder="Last Name"
                                                                                           id="notetype"
                                                                                           class=" form-control">
                                                                                    <div class="form-group">
                                                                                        <label class="control-label col-md-3">Subject <?php echo $l;?>
                                                                                            :</label>
                                                                                        <div class="col-md-6">
                                                                                            <input name="adminsubject[]"
                                                                                                   value="<?php echo e($notes->subject); ?>"
                                                                                                   type="text"
                                                                                                   placeholder="Create Subject"
                                                                                                   id="adminsubject"
                                                                                                   class=" form-control">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="form-group">
                                                                                        <label class="control-label col-md-3">Note <?php echo $l;?>
                                                                                            :</label>
                                                                                        <div class="col-md-6">
                                                                                            <input name="adminnotes[]"
                                                                                                   value="<?php echo e($notes->notes); ?>"
                                                                                                   type="text"
                                                                                                   placeholder="Create Note"
                                                                                                   id="adminnotes"
                                                                                                   class=" form-control">
                                                                                        </div>
                                                                                        <?php if($l==1): ?>
                                                                                            <button class="btn btn-success"
                                                                                                    type="button"
                                                                                                    onclick="education_fields();">
                                                                                                <i class="fa fa-plus"
                                                                                                   aria-hidden="true"></i>
                                                                                            </button>
                                                                                            <a href="<?php echo e(route('notes.notedelete',$notes->id)); ?>"
                                                                                               class="btn btn-danger remove_note"><span
                                                                                                        class="glyphicon glyphicon-minus"
                                                                                                        aria-hidden="true"></span></a>
                                                                                        <?php else: ?>
                                                                                            <a href="<?php echo e(route('notes.notedelete',$notes->id)); ?>"
                                                                                               class="btn btn-danger remove_note"><span
                                                                                                        class="glyphicon glyphicon-minus"
                                                                                                        aria-hidden="true"></span></a>
                                                                                        <?php endif; ?>
                                                                                    </div>
                                                                                    <?php  $l++; ?>
                                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                            <?php else: ?>
                                                                                <input name="usid[]" value=""
                                                                                       type="hidden"
                                                                                       placeholder="Last Name" id="usid"
                                                                                       class=" form-control">
                                                                                <input name="noteid[]" value=""
                                                                                       type="hidden" placeholder=""
                                                                                       id="noteid"
                                                                                       class=" form-control">
                                                                                <input name="notetype[]" value="admin"
                                                                                       type="hidden"
                                                                                       placeholder="Last Name"
                                                                                       id="notetype"
                                                                                       class=" form-control">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3">Subject
                                                                                        1 :</label>
                                                                                    <div class="col-md-6">
                                                                                        <input name="adminsubject[]"
                                                                                               value="" type="text"
                                                                                               placeholder="Create Subject"
                                                                                               id="adminsubject"
                                                                                               class=" form-control">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3">Note
                                                                                        1 :</label>
                                                                                    <div class="col-md-6">
                                                                                        <input name="adminnotes[]"
                                                                                               value="" type="text"
                                                                                               placeholder="Create Note"
                                                                                               id="adminnotes"
                                                                                               class=" form-control">
                                                                                    </div>
                                                                                    <div class="col-md-1">
                                                                                        <button class="btn btn-success"
                                                                                                type="button"
                                                                                                onclick="education_fields();">
                                                                                            <i class="fa fa-plus"
                                                                                               aria-hidden="true"></i>
                                                                                        </button>
                                                                                    </div>
                                                                                    <?php if($l==1): ?>
                                                                                    <?php else: ?>
                                                                                        <div class="col-md-1">
                                                                                        </div>
                                                                                    <?php endif; ?>
                                                                                </div>
                                                                            <?php endif; ?>
                                                                        </div>
                                                                        <div id="education_fields"></div>
                                                                    </div>
                                                                </div>
                                                                <div role="tabpanel" class="tab-pane" id="profile">
                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                        <div class="input_fields_wrap">
                                                                            <?php $l = 1; $notecon2 = count($fsc);?>
                                                                            <?php if($notecon2!=NULL): ?>
                                                                                <?php $__currentLoopData = $fsc; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $notes): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                                    <input name="noteid[]"
                                                                                           value="<?php echo e($notes->id); ?>"
                                                                                           type="hidden" placeholder=""
                                                                                           id="noteid"
                                                                                           class=" form-control">
                                                                                    <input name="usid[]"
                                                                                           value="<?php echo e($notes->admin_id); ?>"
                                                                                           type="hidden" placeholder=""
                                                                                           id="usid"
                                                                                           class=" form-control">
                                                                                    <input name="notetype[]"
                                                                                           value="<?php echo e($notes->type); ?>"
                                                                                           type="hidden"
                                                                                           placeholder="Last Name"
                                                                                           id="notetype"
                                                                                           class=" form-control">
                                                                                    <div class="form-group">
                                                                                        <label class="control-label col-md-3">Subject <?php echo $l; ?>
                                                                                            :</label>
                                                                                        <div class="col-md-6">
                                                                                            <input name="adminsubject[]"
                                                                                                   value="<?php echo e($notes->subject); ?>"
                                                                                                   type="text"
                                                                                                   placeholder="Create Subject"
                                                                                                   id="adminsubject"
                                                                                                   class=" form-control">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="form-group">
                                                                                        <label class="control-label col-md-3">Note <?php echo $l;?>
                                                                                            :</label>
                                                                                        <div class="col-md-6">
                                                                                            <input name="adminnotes[]"
                                                                                                   value="<?php echo e($notes->notes); ?>"
                                                                                                   type="text"
                                                                                                   placeholder="Create Note"
                                                                                                   id="adminnotes"
                                                                                                   class=" form-control">
                                                                                        </div>
                                                                                        <?php if($l==1): ?>
                                                                                            <button class="btn btn-success"
                                                                                                    type="button"
                                                                                                    onclick="education_field();">
                                                                                                <i class="fa fa-plus"
                                                                                                   aria-hidden="true"></i>
                                                                                            </button>
                                                                                            <a href="<?php echo e(route('notes.notedelete',$notes->id)); ?>"
                                                                                               class="btn btn-danger remove_note"><span
                                                                                                        class="glyphicon glyphicon-minus"
                                                                                                        aria-hidden="true"></span></a>
                                                                                        <?php else: ?>
                                                                                            <a href="<?php echo e(route('notes.notedelete',$notes->id)); ?>"
                                                                                               class="btn btn-danger remove_note"><span
                                                                                                        class="glyphicon glyphicon-minus"
                                                                                                        aria-hidden="true"></span></a>
                                                                                        <?php endif; ?>
                                                                                    </div>
                                                                                    <?php  $l++;?>
                                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                            <?php else: ?>
                                                                                <input name="usid[]" value=""
                                                                                       type="hidden"
                                                                                       placeholder="Last Name" id="usid"
                                                                                       class=" form-control">
                                                                                <input name="noteid[]" value=""
                                                                                       type="hidden" placeholder=""
                                                                                       id="noteid"
                                                                                       class=" form-control">
                                                                                <input name="notetype[]" value="fsc"
                                                                                       type="hidden"
                                                                                       placeholder="Last Name"
                                                                                       id="notetype"
                                                                                       class=" form-control">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3">Subject
                                                                                        1 :</label>
                                                                                    <div class="col-md-6">
                                                                                        <input name="adminsubject[]"
                                                                                               value="" type="text"
                                                                                               placeholder="Create Subject"
                                                                                               id="adminsubject"
                                                                                               class=" form-control">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3">Note
                                                                                        1 :</label>
                                                                                    <div class="col-md-6">
                                                                                        <input name="adminnotes[]"
                                                                                               value="" type="text"
                                                                                               placeholder="Create Note"
                                                                                               id="adminnotes"
                                                                                               class=" form-control">
                                                                                    </div>
                                                                                    <div class="">
                                                                                        <button class="btn btn-success"
                                                                                                type="button"
                                                                                                onclick="education_field();">
                                                                                            <i class="fa fa-plus"
                                                                                               aria-hidden="true"></i>
                                                                                        </button>
                                                                                    </div>
                                                                                    <?php if($l==1): ?>
                                                                                    <?php else: ?>
                                                                                        <div class="col-md-1">
                                                                                        </div>
                                                                                    <?php endif; ?>
                                                                                </div>
                                                                            <?php endif; ?>
                                                                        </div>
                                                                        <div id="education_field"></div>
                                                                    </div>
                                                                </div>
                                                                <div role="tabpanel" class="tab-pane" id="messages">
                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                        <div class="input_fields_wrap">
                                                                            <?php $l = 1; $notecon3 = count($client);?>
                                                                            <?php if($notecon3!=NULL): ?>
                                                                                <?php $__currentLoopData = $client; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $notes): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                                    <input name="noteid[]"
                                                                                           value="<?php echo e($notes->id); ?>"
                                                                                           type="hidden" placeholder=""
                                                                                           id="noteid"
                                                                                           class=" form-control">
                                                                                    <input name="usid[]"
                                                                                           value="<?php echo e($notes->admin_id); ?>"
                                                                                           type="hidden" placeholder=""
                                                                                           id="usid"
                                                                                           class=" form-control">
                                                                                    <input name="notetype[]"
                                                                                           value="<?php echo e($notes->type); ?>"
                                                                                           type="hidden"
                                                                                           placeholder="Last Name"
                                                                                           id="notetype"
                                                                                           class=" form-control">
                                                                                    <div class="form-group">
                                                                                        <label class="control-label col-md-3">Subject <?php echo $l;?>
                                                                                            :</label>
                                                                                        <div class="col-md-6">
                                                                                            <input name="adminsubject[]"
                                                                                                   value="<?php echo e($notes->subject); ?>"
                                                                                                   type="text"
                                                                                                   placeholder="Create Subject"
                                                                                                   id="adminsubject"
                                                                                                   class=" form-control">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="form-group">
                                                                                        <label class="control-label col-md-3">Note <?php echo $l;?>
                                                                                            :</label>
                                                                                        <div class="col-md-6">
                                                                                            <input name="adminnotes[]"
                                                                                                   value="<?php echo e($notes->notes); ?>"
                                                                                                   type="text"
                                                                                                   placeholder="Create Note"
                                                                                                   id="adminnotes"
                                                                                                   class=" form-control">
                                                                                        </div>
                                                                                        <?php if($l==1): ?>
                                                                                            <button class="btn btn-success"
                                                                                                    type="button"
                                                                                                    onclick="education_field1();">
                                                                                                <i class="fa fa-plus"
                                                                                                   aria-hidden="true"></i>
                                                                                            </button>
                                                                                            <a href="<?php echo e(route('notes.notedelete',$notes->id)); ?>"
                                                                                               class="btn btn-danger remove_note"><span
                                                                                                        class="glyphicon glyphicon-minus"
                                                                                                        aria-hidden="true"></span></a>
                                                                                        <?php else: ?>
                                                                                            <a href="<?php echo e(route('notes.notedelete',$notes->id)); ?>"
                                                                                               class="btn btn-danger remove_note"><span
                                                                                                        class="glyphicon glyphicon-minus"
                                                                                                        aria-hidden="true"></span></a>
                                                                                        <?php endif; ?>
                                                                                    </div>
                                                                                    <?php  $l++;?>
                                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                            <?php else: ?>
                                                                                <input name="usid[]"
                                                                                       value="<?php echo e($common->cid); ?>"
                                                                                       type="hidden"
                                                                                       placeholder="Last Name" id="usid"
                                                                                       class=" form-control">
                                                                                <input name="noteid[]" value=""
                                                                                       type="hidden" placeholder=""
                                                                                       id="noteid"
                                                                                       class=" form-control">
                                                                                <input name="notetype[]" value="client"
                                                                                       type="hidden"
                                                                                       placeholder="Last Name"
                                                                                       id="notetype"
                                                                                       class=" form-control">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3">Subject
                                                                                        1 :</label>
                                                                                    <div class="col-md-6">
                                                                                        <input name="adminsubject[]"
                                                                                               value="" type="text"
                                                                                               placeholder="Create Subject"
                                                                                               id="adminsubject"
                                                                                               class=" form-control">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3">Note
                                                                                        1 :</label>
                                                                                    <div class="col-md-6">
                                                                                        <input name="adminnotes[]"
                                                                                               value="" type="text"
                                                                                               placeholder="Create Note"
                                                                                               id="adminnotes"
                                                                                               class=" form-control">
                                                                                    </div>
                                                                                    <div class="col-md-1">
                                                                                        <button class="btn btn-success"
                                                                                                type="button"
                                                                                                onclick="education_field1();">
                                                                                            <i class="fa fa-plus"
                                                                                               aria-hidden="true"></i>
                                                                                        </button>
                                                                                    </div>
                                                                                    <?php if($l==1): ?>
                                                                                    <?php else: ?>
                                                                                        <div class="col-md-1">
                                                                                        </div>
                                                                                    <?php endif; ?>
                                                                                </div>
                                                                            <?php endif; ?>
                                                                        </div>
                                                                        <div id="education_field1"></div>
                                                                    </div>
                                                                </div>
                                                                <div role="tabpanel" class="tab-pane" id="settings">
                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                        <div class="input_fields_wrap">
                                                                            <?php $l = 1; $notecon = count($every);?>
                                                                            <?php if($notecon!=NULL): ?>
                                                                                <?php $__currentLoopData = $every; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $notes): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                                    <input name="noteid[]"
                                                                                           value="<?php echo e($notes->id); ?>"
                                                                                           type="hidden" placeholder=""
                                                                                           id="noteid"
                                                                                           class=" form-control">
                                                                                    <input name="usid[]" value=""
                                                                                           type="hidden" placeholder=""
                                                                                           id="usid"
                                                                                           class=" form-control">
                                                                                    <input name="notetype[]"
                                                                                           value="<?php echo e($notes->type); ?>"
                                                                                           type="hidden"
                                                                                           placeholder="Last Name"
                                                                                           id="notetype"
                                                                                           class=" form-control">
                                                                                    <div class="form-group">
                                                                                        <label class="control-label col-md-3">Subject <?php echo $l;?>
                                                                                            :</label>
                                                                                        <div class="col-md-6">
                                                                                            <input name="adminsubject[]"
                                                                                                   value="<?php echo e($notes->subject); ?>"
                                                                                                   type="text"
                                                                                                   placeholder="Create Subject"
                                                                                                   id="adminsubject"
                                                                                                   class=" form-control">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="form-group">
                                                                                        <label class="control-label col-md-3">Note <?php echo $l;?>
                                                                                            :</label>
                                                                                        <div class="col-md-6">
                                                                                            <input name="adminnotes[]"
                                                                                                   value="<?php echo e($notes->notes); ?>"
                                                                                                   type="text"
                                                                                                   placeholder="Create Note"
                                                                                                   id="adminnotes"
                                                                                                   class=" form-control">
                                                                                        </div>
                                                                                        <?php if($l==1): ?>
                                                                                            <button class="btn btn-success"
                                                                                                    type="button"
                                                                                                    onclick="education_field2();">
                                                                                                <i class="fa fa-plus"
                                                                                                   aria-hidden="true"></i>
                                                                                            </button>
                                                                                            <a href="<?php echo e(route('notes.notedelete',$notes->id)); ?>"
                                                                                               class="btn btn-danger remove_note"><span
                                                                                                        class="glyphicon glyphicon-minus"
                                                                                                        aria-hidden="true"></span></a>
                                                                                        <?php else: ?>
                                                                                            <a href="<?php echo e(route('notes.notedelete',$notes->id)); ?>"
                                                                                               class="btn btn-danger remove_note"><span
                                                                                                        class="glyphicon glyphicon-minus"
                                                                                                        aria-hidden="true"></span></a>
                                                                                        <?php endif; ?>
                                                                                    </div>
                                                                                    <?php $l++;?>
                                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                            <?php else: ?>
                                                                                <input name="usid[]" value=""
                                                                                       type="hidden"
                                                                                       placeholder="Last Name" id="usid"
                                                                                       class=" form-control">
                                                                                <input name="noteid[]" value=""
                                                                                       type="hidden" placeholder=""
                                                                                       id="noteid"
                                                                                       class=" form-control">
                                                                                <input name="notetype[]"
                                                                                       value="Everybody" type="hidden"
                                                                                       placeholder="Last Name"
                                                                                       id="notetype"
                                                                                       class=" form-control">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3">Subject
                                                                                        1 :</label>
                                                                                    <div class="col-md-6">
                                                                                        <input name="adminsubject[]"
                                                                                               value="" type="text"
                                                                                               placeholder="Create Subject"
                                                                                               id="adminsubject"
                                                                                               class=" form-control">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3">Note
                                                                                        1 :</label>
                                                                                    <div class="col-md-6">
                                                                                        <input name="adminnotes[]"
                                                                                               value="" type="text"
                                                                                               placeholder="Create Note"
                                                                                               id="adminnotes"
                                                                                               class=" form-control">
                                                                                    </div>
                                                                                    <div class="col-md-1">
                                                                                        <button class="btn btn-success"
                                                                                                type="button"
                                                                                                onclick="education_field2();">
                                                                                            <i class="fa fa-plus"
                                                                                               aria-hidden="true"></i>
                                                                                        </button>
                                                                                    </div>
                                                                                    <?php if($l==1): ?>
                                                                                    <?php else: ?>
                                                                                        <div class="col-md-1"></div>
                                                                                    <?php endif; ?>
                                                                                </div>
                                                                            <?php endif; ?>
                                                                        </div>
                                                                        <div id="education_field2"></div>
                                                                    </div>
                                                                </div>
                                                                <div role="tabpanel" class="tab-pane" id="payments">
                                                                    <br>
                                                                    <ul style="margin:0px 15px;padding: 3px 0px 5px 0px !important;margin-top:-11px;"
                                                                        class="nav nav-tabs test" role="tablist">
                                                                        <li role="presentation"
                                                                            style="width:48.8% !important;"
                                                                            class="active"><a href="#SubcriptionPayment"
                                                                                              aria-controls="SubcriptionPayment"
                                                                                              role="tab"
                                                                                              data-toggle="tab"> <span>Subscription Payment</span></a>
                                                                        </li>
                                                                        <li role="presentation"
                                                                            style="width:48.8% !important;" class=""><a
                                                                                    href="#SubcriptionPayment1"
                                                                                    aria-controls="SubcriptionPayment1"
                                                                                    role="tab" data-toggle="tab"> <span>FSC Client Payment</span></a>
                                                                        </li>
                                                                    </ul>

                                                                    <div class="tab-content">
                                                                        <div role="tabpanel" class="tab-pane active"
                                                                             id="SubcriptionPayment">
                                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"
                                                                                 style="margin-top:15px;">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3">Payment
                                                                                        Mode:</label>
                                                                                    <div class="col-md-6">
                                                                                        <select name="paymentmode"
                                                                                                id="paymentmode"
                                                                                                class="form-control">
                                                                                            <option value="">
                                                                                                ---Select---
                                                                                            </option>
                                                                                            <option value="Check"
                                                                                                    <?php if($common->paymentmode1=='Check'): ?> selected <?php endif; ?>>
                                                                                                Check
                                                                                            </option>
                                                                                            <option value="ACH"
                                                                                                    <?php if($common->paymentmode=='ACH'): ?> selected <?php endif; ?>>
                                                                                                ACH
                                                                                            </option>
                                                                                            <option value="CreditCard"
                                                                                                    <?php if($common->paymentmode=='CreditCard'): ?> selected <?php endif; ?>>
                                                                                                Credit Card
                                                                                            </option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group ACH"
                                                                                     <?php if($common->paymentmode=='ACH'): ?> <?php else: ?> style="display:none" <?php endif; ?>>
                                                                                    <label class="control-label col-md-3">Bank
                                                                                        Name:</label>
                                                                                    <div class="col-md-6">
                                                                                        <select class="form-control banknames"
                                                                                                name="bankname">
                                                                                            <option value="">Select
                                                                                            </option>
                                                                                            <?php $__currentLoopData = $clientbank; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cbank): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                                                <option value="<?php echo e($cbank->fourdigit); ?>"><?php echo e($cbank->bankname); ?>

                                                                                                    - <?php echo e($cbank->fourdigit); ?></option>
                                                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                                        </select>
                                                                                    <!-- <input name="bankname" value="<?php echo e($common->bankname); ?>" type="text" id="bankname" style="height: auto !important;" class="form-control">!-->
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group ACH"
                                                                                     <?php if($common->paymentmode=='ACH'): ?> <?php else: ?> style="display:none" <?php endif; ?>>
                                                                                    <label class="control-label col-md-3">Routing
                                                                                        Number:</label>
                                                                                    <div class="col-md-6">
                                                                                        <input name="routingno"
                                                                                               value="<?php echo e($common->routingno); ?>"
                                                                                               maxlength="9" type="text"
                                                                                               id="routingno"
                                                                                               style="height: auto !important;"
                                                                                               class="form-control">

                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group ACH"
                                                                                     <?php if($common->paymentmode=='ACH'): ?> <?php else: ?> style="display:none" <?php endif; ?>>
                                                                                    <label class="control-label col-md-3">Bank
                                                                                        A/C Number:</label>
                                                                                    <div class="col-md-6">
                                                                                        <input name="acountno"
                                                                                               value="<?php echo e($common->acountno); ?>"
                                                                                               maxlength="4" type="text"
                                                                                               id="acountno"
                                                                                               style="height: auto !important;"
                                                                                               class="form-control">
                                                                                        <span class="small">Last 4 digits</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group ACH"
                                                                                     <?php if($common->paymentmode=='ACH'): ?> <?php else: ?> style="display:none" <?php endif; ?>>
                                                                                    <label class="control-label col-md-3">Account
                                                                                        Name:</label>
                                                                                    <div class="col-md-6">
                                                                                        <input name="acountname"
                                                                                               value="<?php echo e($common->acountname); ?>"
                                                                                               type="text"
                                                                                               id="acountname"
                                                                                               style="height: auto !important;"
                                                                                               class="form-control">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group ACH1"
                                                                                     <?php if($common->paymentmode=='CreditCard'): ?> <?php else: ?> style="display:none" <?php endif; ?>>
                                                                                    <label class="control-label col-md-3">Type
                                                                                        of Card:</label>
                                                                                    <div class="col-md-6">
                                                                                        <select name="carttype"
                                                                                                id="carttype"
                                                                                                class="form-control">
                                                                                            <option value="Visa"
                                                                                                    <?php if($common->carttype=='Visa'): ?> selected <?php endif; ?>>
                                                                                                Visa
                                                                                            </option>
                                                                                            <option value="Master Card"
                                                                                                    <?php if($common->carttype=='Master Card'): ?> selected <?php endif; ?>>
                                                                                                Master Card
                                                                                            </option>
                                                                                            <option value="Discover"
                                                                                                    <?php if($common->carttype=='Discover'): ?> selected <?php endif; ?>>
                                                                                                Discover
                                                                                            </option>
                                                                                            <option value="AX"
                                                                                                    <?php if($common->carttype=='AX'): ?> selected <?php endif; ?>>
                                                                                                AX
                                                                                            </option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group ACH1"
                                                                                     <?php if($common->paymentmode=='CreditCard'): ?> <?php else: ?> style="display:none" <?php endif; ?>>
                                                                                    <label class="control-label col-md-3">Card
                                                                                        Number:</label>
                                                                                    <div class="col-md-6">
                                                                                        <input name="cartno"
                                                                                               value="<?php echo e($common->cartno); ?>"
                                                                                               maxlength="4" type="text"
                                                                                               id="cartno"
                                                                                               style="height: auto !important;"
                                                                                               class="form-control">
                                                                                        <small>Last 4 digits
                                                                                            only</small>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3">Note:</label>
                                                                                    <div class="col-md-6">
                                                                                        <textarea name="paymentnote"
                                                                                                  value="" type="text"
                                                                                                  id="paymentnote"
                                                                                                  style="height: auto !important;"
                                                                                                  class="form-control"><?php echo e($common->paymentnote); ?></textarea>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div role="tabpanel" class="tab-pane"
                                                                             id="SubcriptionPayment1">
                                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"
                                                                                 style="margin-top:15px;">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3">Payment
                                                                                        Mode:</label>
                                                                                    <div class="col-md-6">
                                                                                        <select name="paymentmode11"
                                                                                                id="paymentmode11"
                                                                                                class="form-control">
                                                                                            <option value="">
                                                                                                ---Select---
                                                                                            </option>
                                                                                            <option value="Check"
                                                                                                    <?php if($common->paymentmode11=='Check'): ?> selected <?php endif; ?>>
                                                                                                Check
                                                                                            </option>
                                                                                            <option value="ACH"
                                                                                                    <?php if($common->paymentmode11=='ACH'): ?> selected <?php endif; ?>>
                                                                                                ACH
                                                                                            </option>
                                                                                            <option value="CreditCard"
                                                                                                    <?php if($common->paymentmode11=='CreditCard'): ?> selected <?php endif; ?>>
                                                                                                Credit Card
                                                                                            </option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group ACH3"
                                                                                     <?php if($common->paymentmode11=='ACH'): ?> <?php else: ?> style="display:none" <?php endif; ?>>
                                                                                    <label class="control-label col-md-3">Bank
                                                                                        Name:</label>
                                                                                    <div class="col-md-6">
                                                                                        <select class="form-control banknames1"
                                                                                                name="bankname">
                                                                                            <option value="">Select
                                                                                            </option>
                                                                                            <?php $__currentLoopData = $clientbank; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cbank): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                                                <option value="<?php echo e($cbank->fourdigit); ?>"><?php echo e($cbank->bankname); ?>

                                                                                                    - <?php echo e($cbank->fourdigit); ?></option>
                                                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                                                                        </select>

                                                                                    <!--<input name="bankname1" value="<?php echo e($common->bankname1); ?>" type="text" id="bankname" style="height: auto !important;" class="form-control">!-->
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group ACH3"
                                                                                     <?php if($common->paymentmode11=='ACH'): ?> <?php else: ?> style="display:none" <?php endif; ?>>
                                                                                    <label class="control-label col-md-3">Routing
                                                                                        Number:</label>
                                                                                    <div class="col-md-6">
                                                                                        <input name="routingno1"
                                                                                               value="<?php echo e($common->routingno1); ?>"
                                                                                               maxlength="9" type="text"
                                                                                               id="routingno1"
                                                                                               style="height: auto !important;"
                                                                                               class="form-control">

                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group ACH3"
                                                                                     <?php if($common->paymentmode11=='ACH'): ?> <?php else: ?> style="display:none" <?php endif; ?>>
                                                                                    <label class="control-label col-md-3">Bank
                                                                                        A/C Number:</label>
                                                                                    <div class="col-md-6">
                                                                                        <input name="acountno1"
                                                                                               value="<?php echo e($common->acountno1); ?>"
                                                                                               maxlength="4" type="text"
                                                                                               id="acountno1"
                                                                                               style="height: auto !important;"
                                                                                               class="form-control">
                                                                                        <span class="small">Last 4 digits</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group ACH3"
                                                                                     <?php if($common->paymentmode11=='ACH'): ?> <?php else: ?> style="display:none" <?php endif; ?>>
                                                                                    <label class="control-label col-md-3">Account
                                                                                        Name:</label>
                                                                                    <div class="col-md-6">
                                                                                        <input name="acountname1"
                                                                                               value="<?php echo e($common->acountname1); ?>"
                                                                                               type="text"
                                                                                               id="acountname1"
                                                                                               style="height: auto !important;"
                                                                                               class="form-control">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group ACH2"
                                                                                     <?php if($common->paymentmode11=='CreditCard'): ?> <?php else: ?> style="display:none" <?php endif; ?>>
                                                                                    <label class="control-label col-md-3">Type
                                                                                        of Card:</label>
                                                                                    <div class="col-md-6">
                                                                                        <select name="carttype1"
                                                                                                id="carttype"
                                                                                                class="form-control">
                                                                                            <option value="Visa"
                                                                                                    <?php if($common->carttype1=='Visa'): ?> selected <?php endif; ?>>
                                                                                                Visa
                                                                                            </option>
                                                                                            <option value="Master Card"
                                                                                                    <?php if($common->carttype1=='Master Card'): ?> selected <?php endif; ?>>
                                                                                                Master Card
                                                                                            </option>
                                                                                            <option value="Discover"
                                                                                                    <?php if($common->carttype1=='Discover'): ?> selected <?php endif; ?>>
                                                                                                Discover
                                                                                            </option>
                                                                                            <option value="AX"
                                                                                                    <?php if($common->carttype1=='AX'): ?> selected <?php endif; ?>>
                                                                                                AX
                                                                                            </option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group ACH2"
                                                                                     <?php if($common->paymentmode11=='CreditCard'): ?> <?php else: ?> style="display:none" <?php endif; ?>>
                                                                                    <label class="control-label col-md-3">Card
                                                                                        Number:</label>
                                                                                    <div class="col-md-6">
                                                                                        <input name="cartno1"
                                                                                               value="<?php echo e($common->cartno1); ?>"
                                                                                               maxlength="4" type="text"
                                                                                               id="cartno"
                                                                                               style="height: auto !important;"
                                                                                               class="form-control">
                                                                                        <small>Last 4 digits
                                                                                            only</small>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3">Note:</label>
                                                                                    <div class="col-md-6">
                                                                                        <textarea name="paymentnote1"
                                                                                                  value="" type="text"
                                                                                                  id="paymentnote"
                                                                                                  style="height: auto !important;"
                                                                                                  class="form-control"><?php echo e($common->paymentnote1); ?></textarea>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div role="tabpanel" class="tab-pane" id="location">
                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                        <br>
                                                                        <div class="Branch">
                                                                            <h1>Location (Branch)</h1>
                                                                        </div>
                                                                        <br/>

                                                                        <?php $__currentLoopData = $newlocations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $newlocation): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                            <div class="row"
                                                                                 style="border-bottom:1px solid;margin-bottom:15px;padding-bottom:10px;">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-2"
                                                                                           style="width:14% !important;">Location:</label>
                                                                                    <div class="col-md-2">
                                                                                        <input name="location[]"
                                                                                               readonly
                                                                                               value="<?php echo e($newlocation->location); ?>"
                                                                                               type="text" id="location"
                                                                                               placeholder="Location"
                                                                                               class="form-control">
                                                                                    </div>
                                                                                    <div class="">
                                                                                        <label class="control-label col-md-2"
                                                                                               style="width:14% !important;">Location
                                                                                            ID:</label>
                                                                                        <div class="col-md-2">
                                                                                            <input name="location_id[]"
                                                                                                   value="<?php echo e($newlocation->location_id); ?>"
                                                                                                   type="text"
                                                                                                   id="location_id"
                                                                                                   placeholder="Location ID"
                                                                                                   class="form-control">
                                                                                            <input name="location_idd[]"
                                                                                                   value="<?php echo e($newlocation->id); ?>"
                                                                                                   type="hidden"
                                                                                                   id="location_idd"
                                                                                                   placeholder="Location ID"
                                                                                                   class="form-control">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="">
                                                                                        <label class="control-label col-md-2"
                                                                                               style="width:14% !important;">Location
                                                                                            Name:</label>
                                                                                        <div class="col-md-2">
                                                                                            <input name="location_name[]"
                                                                                                   type="text"
                                                                                                   id="location_name"
                                                                                                   class="form-control"
                                                                                                   placeholder="Location Name"
                                                                                                   value="<?php echo e($newlocation->location_name); ?>">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-2"
                                                                                           style="width:15% !important;font-size:13px">Location
                                                                                        Address :</label>
                                                                                    <div class="col-md-9"
                                                                                         style="width:76% !important;padding:5px !important;">
                                                                                        <input name="location_address[]"
                                                                                               value="<?php echo e($newlocation->location_address); ?>"
                                                                                               type="text"
                                                                                               id="location_address"
                                                                                               placeholder="Location Address"
                                                                                               class="form-control">
                                                                                    </div>

                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-2"
                                                                                           style="width:14% !important;">City:</label>
                                                                                    <div class="col-md-2">
                                                                                        <input name="location_city[]"
                                                                                               value="<?php echo e($newlocation->location_city); ?>"
                                                                                               type="text"
                                                                                               id="location_city"
                                                                                               placeholder="City"
                                                                                               class="form-control">
                                                                                    </div>
                                                                                    <div class="">
                                                                                        <label class="control-label col-md-2"
                                                                                               style="width:14% !important;">State:</label>
                                                                                        <div class="col-md-2">
                                                                                            <select name="location_state[]"
                                                                                                    id="location_state"
                                                                                                    data-state="<?php echo e($newlocation->location_state); ?>"
                                                                                                    class="form-control fsc-input bfh-states"
                                                                                                    data-country="countries_states1">
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="">
                                                                                        <label class="control-label col-md-2"
                                                                                               style="width:14% !important;">Zip:</label>
                                                                                        <div class="col-md-2">
                                                                                            <input name="location_zip[]"
                                                                                                   placeholder="zip"
                                                                                                   type="text"
                                                                                                   id="location_zip"
                                                                                                   class="form-control zip"
                                                                                                   value="<?php echo e($newlocation->location_zip); ?>">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-2"
                                                                                           style="width:14% !important;">Contact
                                                                                        Name :</label>
                                                                                    <div class="">
                                                                                        <div class="col-md-2"
                                                                                             style="width:16.7% !important;">
                                                                                            <input name="location_telephone[]"
                                                                                                   type="text"
                                                                                                   id="location_telephone"
                                                                                                   class="form-control"
                                                                                                   placeholder="Contact Name"
                                                                                                   value="<?php echo e($newlocation->location_telephone); ?>">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="">
                                                                                        <label class="control-label col-md-2"
                                                                                               style="width:14% !important;">Telephone
                                                                                            No :</label>
                                                                                        <div class="col-md-2">
                                                                                            <input name="location_zip[]"
                                                                                                   placeholder="Telephone No"
                                                                                                   type="text"
                                                                                                   id="location_zip"
                                                                                                   class="form-control phone">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <hr>
                                                                                <br></div>
                                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                        <?php $now = 01; $last = $common->multilocation; $c = count($newlocations);?>
                                                                        <?php if($c==$last): ?>
                                                                        <?php elseif($last>$c): ?>
                                                                            <?php $last1 = $common->multilocation; $c = count($newlocations); $last = $last1 - $c;?>
                                                                            <?php for($i = $now; $i <= $last; $i++): ?>
                                                                                <div class="row"
                                                                                     style="border-bottom:5px solid 	 #979800 ; ;margin-bottom:15px;padding-bottom:10px;">
                                                                                    <div class="form-group">
                                                                                        <label class="control-label col-md-2"
                                                                                               style="width:14% !important;">Location
                                                                                            :</label>
                                                                                        <div class="col-md-2">
                                                                                            <input name="location[]"
                                                                                                   style="text-align:center;font-size:25px !important;"
                                                                                                   readonly
                                                                                                   value="<?php echo e($i+$c); ?>"
                                                                                                   type="text"
                                                                                                   id="location"
                                                                                                   placeholder="Location"
                                                                                                   class="form-control">
                                                                                        </div>
                                                                                        <div class="">
                                                                                            <label class="control-label col-md-2"
                                                                                                   style="width:14% !important;">Location
                                                                                                ID :</label>
                                                                                            <div class="col-md-2">
                                                                                                <input name="location_id[]"
                                                                                                       value=""
                                                                                                       type="text"
                                                                                                       id="location_id"
                                                                                                       placeholder="Location ID"
                                                                                                       class="form-control">
                                                                                                <input name="location_idd[]"
                                                                                                       value=""
                                                                                                       type="hidden"
                                                                                                       id="location_idd"
                                                                                                       placeholder="Location ID"
                                                                                                       class="form-control">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="">
                                                                                            <label class="control-label col-md-2"
                                                                                                   style="width:14% !important;">Location
                                                                                                Name :</label>
                                                                                            <div class="col-md-2">
                                                                                                <input name="location_name[]"
                                                                                                       type="text"
                                                                                                       id="location_name"
                                                                                                       class="form-control"
                                                                                                       placeholder="Location Name">
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="form-group">
                                                                                        <label class="control-label col-md-2"
                                                                                               style="width:15% !important;font-size:13px">Location
                                                                                            Address :</label>
                                                                                        <div class="col-md-9"
                                                                                             style="width:76% !important;padding:5px !important;">
                                                                                            <input name="location_address[]"
                                                                                                   type="text"
                                                                                                   id="location_address"
                                                                                                   placeholder="Location Address"
                                                                                                   class="form-control">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="form-group">
                                                                                        <label class="control-label col-md-2"
                                                                                               style="width:14% !important;">City
                                                                                            :</label>
                                                                                        <div class="col-md-2">
                                                                                            <input name="location_city[]"
                                                                                                   type="text"
                                                                                                   id="location_city"
                                                                                                   placeholder="City"
                                                                                                   class="form-control">
                                                                                        </div>
                                                                                        <div class="">
                                                                                            <label class="control-label col-md-2"
                                                                                                   style="width:14% !important;">State
                                                                                                :</label>
                                                                                            <div class="col-md-2">
                                                                                                <select name="location_state[]"
                                                                                                        id="location_state"
                                                                                                        data-state="GA"
                                                                                                        class="form-control fsc-input bfh-states"
                                                                                                        data-country="countries_states1">
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="">
                                                                                            <label class="control-label col-md-2"
                                                                                                   style="width:14% !important;">Zip
                                                                                                :</label>
                                                                                            <div class="col-md-2">
                                                                                                <input name="location_zip[]"
                                                                                                       placeholder="zip"
                                                                                                       type="text"
                                                                                                       id="location_zip"
                                                                                                       class="form-control zip">
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="">
                                                                                        <label class="control-label col-md-2"
                                                                                               style="width:15% !important;">
                                                                                            Telephone No:</label>
                                                                                        <div class="col-md-2"
                                                                                             style="padding:0px 5px !important;width:15% !important;margin-left:2px;margin-right:12px;">
                                                                                            <input name="location_telephone[]"
                                                                                                   type="text"
                                                                                                   id="location_telephone"
                                                                                                   class="form-control"
                                                                                                   placeholder="Telephone No">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="">
                                                                                        <label class="control-label col-md-2"
                                                                                               style="width:14% !important;">Person
                                                                                            Name :</label>
                                                                                        <div class="col-md-2"
                                                                                             style="width:17% !important;">
                                                                                            <input name="location_zip[]"
                                                                                                   placeholder="Person Name"
                                                                                                   type="text"
                                                                                                   id="location_zip"
                                                                                                   class="form-control phone">
                                                                                        </div>
                                                                                    </div>
                                                                                    <hr>
                                                                                </div>
                                                                            <?php endfor; ?>
                                                                        <?php endif; ?>
                                                                    </div>
                                                                </div>

                                                                <div role="tabpanel" class="tab-pane" id="otherinfo">
                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"
                                                                         style="padding:0px;">
                                                                        <br>

                                                                        <div class="Branch">
                                                                            <h1><?php if($common->business_id=='6'): ?>
                                                                                    Family <?php else: ?> Other <?php endif; ?></h1>
                                                                        </div>
                                                                        <br/>

                                                                        <div class="row">
                                                                        <!--<div class="col-md-12 spouseform">
                <div class="form-group">
                    <label class="col-md-5 text-right">Marital Status:</label>
                    <div class="col-md-2"> <select class="form-control maritialsts" name="other_maritial_status">
                       <option value="">Select</option>
                     
                       <option value="Single"  <?php if($common->other_maritial_status=='Single'): ?> selected <?php endif; ?>>Single</option>
                       
                       <option value="Married" <?php if($common->other_maritial_status=='Married'): ?> selected <?php endif; ?>>Married</option>
                       <option value="Widowed" <?php if($common->other_maritial_status=='Widowed'): ?> selected <?php endif; ?>>Widowed</option>
                       
                   </select></div>
                </div>
            </div>!-->
                                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 "
                                                                                 <?php if('6'==$common->business_id): ?><?php else: ?> style="display:none" <?php endif; ?>>
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3">Maritial
                                                                                        Status :</label>
                                                                                    <div class="col-md-3">
                                                                                    <!--<select class="form-control maritial" name="" >
                           <option value="">Select</option>
                           <option value="Single"  <?php if($common->other_maritial_status1=='Single'): ?> selected <?php endif; ?>>Single</option>
                           <option value="Married" <?php if($common->other_maritial_status1=='Married'): ?> selected <?php endif; ?>>Married</option>
                           <option value="Widowed" <?php if($common->other_maritial_status1=='Widowed'): ?> selected <?php endif; ?>>Widowed</option>
                       </select>!-->
                                                                                        <input class="form-control"
                                                                                               disabled="disabled"
                                                                                               value="<?php echo e($common->other_maritial_status1); ?>">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="clear"></div>
                                                                            <div class="col-md-12">
                                                                                <div class="spouce_self">
                                                                                    <div class="col-md-2 spouseform"
                                                                                         style="border-right: 1px solid #000000;">
                                                                                        <div class="row">
                                                                                            <div class="col-md-12"
                                                                                                 style="height:55px;">
                                                                                                &nbsp;
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row">
                                                                                            <div class="col-md-12 text-right">
                                                                                                <label class="form-label">Name:</label>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row">
                                                                                            <div class="col-md-12 text-right">
                                                                                                <label class="form-label">DOB
                                                                                                    (MM/YYYY):</label>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row">
                                                                                            <div class="col-md-12 text-right">
                                                                                                <label class="form-label">Ethnic:</label>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row">
                                                                                            <div class="col-md-12 text-right">
                                                                                                <label class="form-label">Main
                                                                                                    Language:</label>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row">
                                                                                            <div class="col-md-12 text-right">
                                                                                                <label class="form-label">1st
                                                                                                    Language:</label>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row">
                                                                                            <div class="col-md-12 text-right"
                                                                                                 style="padding-bottom:4px;">
                                                                                                <label class="form-label">2nd
                                                                                                    Language:</label>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-5 spouseform"
                                                                                         style="border-right: 1px solid #000000;">
                                                                                        <div class="row">
                                                                                            <div class="col-md-12">
                                                                                                <h4 class="text-center">
                                                                                                    Your Self</h4>
                                                                                            </div>
                                                                                        </div>

                                                                                        <div class="row">
                                                                                            <div class="col-md-12">
                                                                                                <ul class="formbox"
                                                                                                    <?php if('6'==$common->business_id): ?><?php else: ?> style="display:none" <?php endif; ?>>
                                                                                                    <li style="width:80px;">
                                                                                                        <input type="text"
                                                                                                               class="form-control"
                                                                                                               readonly
                                                                                                               value="<?php echo ucfirst($common->nametype . '.');?>">
                                                                                                    </li>
                                                                                                    <li>
                                                                                                        <input type="text"
                                                                                                               class="form-control"
                                                                                                               readonly
                                                                                                               value="<?php echo $common->first_name;?>">
                                                                                                    </li>
                                                                                                    <li style="width:100px;">
                                                                                                        <input type="text"
                                                                                                               class="form-control"
                                                                                                               readonly
                                                                                                               value="<?php echo $common->middle_name;?>"
                                                                                                    </li>
                                                                                                    <li>
                                                                                                        <input type="text"
                                                                                                               class="form-control"
                                                                                                               readonly
                                                                                                               value="<?php echo $common->last_name;?>">
                                                                                                    </li>
                                                                                                </ul>

                                                                                                <ul class="formbox"
                                                                                                    <?php if('6'!=$common->business_id): ?><?php else: ?> style="display:none" <?php endif; ?>>
                                                                                                    <li style="width:80px;">
                                                                                                        <input type="text"
                                                                                                               class="form-control"
                                                                                                               readonly
                                                                                                               value="<?php echo ucfirst($common->contactnametype . '.');?>">
                                                                                                    </li>
                                                                                                    <li>
                                                                                                        <input type="text"
                                                                                                               class="form-control"
                                                                                                               readonly
                                                                                                               value="<?php echo $common->firstname;?>">
                                                                                                    </li>
                                                                                                    <li style="width:100px;">
                                                                                                        <input type="text"
                                                                                                               class="form-control"
                                                                                                               readonly
                                                                                                               value="<?php echo $common->middlename;?>"
                                                                                                    </li>
                                                                                                    <li>
                                                                                                        <input type="text"
                                                                                                               class="form-control"
                                                                                                               readonly
                                                                                                               value="<?php echo $common->lastname;?>">
                                                                                                    </li>
                                                                                                </ul>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row">
                                                                                            <?php
                                                                                            $yeartoday = date('Y');
                                                                                            if ($common->other_dob_day != '') {
                                                                                                $totayr = $yeartoday - $common->other_dob_day;
                                                                                            }
                                                                                            if ($common->other_dob_day_s != '') {

                                                                                                $totayr1 = $yeartoday - $common->other_dob_day_s;
                                                                                            }
                                                                                            ?>
                                                                                            <div class="col-md-12">
                                                                                                <ul class="formbox">
                                                                                                    <li> <?php $month_names = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");?>
                                                                                                        <select class="form-control"
                                                                                                                name="other_dob_month">
                                                                                                            <option value="">
                                                                                                                Select
                                                                                                            </option>
                                                                                                            <?php
                                                                                                            foreach($month_names as $month)
                                                                                                            {?>
                                                                                                            <option value="<?php echo $month;?>"
                                                                                                                    <?php if($common->other_dob_month==$month): ?> selected <?php endif; ?>><?php echo $month;?></option>
                                                                                                            <?php }
                                                                                                            ?>
                                                                                                        </select></li>
                                                                                                    <li style="width:76px;">
                                                                                                        <select class="form-control otherdob"
                                                                                                                name="other_dob_day">
                                                                                                            <option value="">
                                                                                                                Select
                                                                                                            </option>
                                                                                                            <?php for( $i = 1920; $i <= 2000; $i++ ){?>
                                                                                                            <option <?php if($common->other_dob_day==$i): ?> selected
                                                                                                                    <?php endif; ?> value="<?php echo $i;?>"><?php echo $i;?></option><?php }?>
                                                                                                        </select>
                                                                                                    </li>
                                                                                                    <li style="width:72px;">
                                                                                                        <input class="form-control totalyears"
                                                                                                               readonly
                                                                                                               <?php if($common->other_dob_day !=''): ?>value="<?php echo e($totayr); ?> Years" <?php endif; ?>>
                                                                                                    </li>

                                                                                                </ul>
                                                                                            </div>
                                                                                        </div>

                                                                                        <div class="row">
                                                                                            <div class="col-md-12">
                                                                                                <ul class="formbox">
                                                                                                    <li>
                                                                                                        <select class="form-control wdth205"
                                                                                                                name="other_ethnic">
                                                                                                            <option value="">
                                                                                                                Select
                                                                                                            </option>
                                                                                                            <?php $__currentLoopData = $ethnic; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lan1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                                                                <option value="<?php echo e($lan1->ethnic_name); ?>"
                                                                                                                        <?php if($common->other_ethnic==$lan1->ethnic_name): ?> selected <?php endif; ?>><?php echo e($lan1->ethnic_name); ?></option>
                                                                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                                                        </select>
                                                                                                    </li>


                                                                                                </ul>
                                                                                            </div>
                                                                                        </div>


                                                                                        <div class="row">

                                                                                            <div class="col-md-12">
                                                                                                <ul class="formbox">
                                                                                                    <li>
                                                                                                        <input class="form-control wdth205"
                                                                                                               readonly
                                                                                                               name="other_main_language"
                                                                                                               value="American English">
                                                                                                    </li>

                                                                                                </ul>
                                                                                            </div>
                                                                                        </div>

                                                                                        <div class="row">
                                                                                            <div class="col-md-12">
                                                                                                <ul class="formbox">
                                                                                                    <li>
                                                                                                        <select class="form-control firstlang wdth205"
                                                                                                                name="other_first_language"
                                                                                                                id="firstlang">
                                                                                                            <option value="1">
                                                                                                                Select
                                                                                                            </option>
                                                                                                            <?php $__currentLoopData = $language; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lan1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                                                                <option value="<?php echo e($lan1->language_name); ?>"
                                                                                                                        <?php if($common->other_first_language==$lan1->language_name): ?> selected <?php endif; ?>><?php echo e($lan1->language_name); ?></option>
                                                                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                                                        </select>
                                                                                                    </li>

                                                                                                </ul>
                                                                                            </div>
                                                                                        </div>

                                                                                        <div class="row">
                                                                                            <div class="col-md-12">
                                                                                                <ul class="formbox">
                                                                                                    <li>
                                                                                                        <select class="form-control firstlang wdth205"
                                                                                                                name="other_second_language"
                                                                                                                id="seclang">
                                                                                                            <option value="">
                                                                                                                Select
                                                                                                            </option>
                                                                                                            <?php $__currentLoopData = $language; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lan1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                                                                <option value="<?php echo e($lan1->language_name); ?>"
                                                                                                                        <?php if($common->other_second_language==$lan1->language_name): ?> selected <?php endif; ?>><?php echo e($lan1->language_name); ?></option>
                                                                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                                                        </select>
                                                                                                    </li>

                                                                                                </ul>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <?php if($common->other_maritial_status1 =='Married'): ?>
                                                                                        <div class="col-md-5 spouseform">
                                                                                            <h4 class="text-center">
                                                                                                Spouse</h4>
                                                                                            <div class="row">
                                                                                                <div class="col-md-12">
                                                                                                    <!--nametype,contactnametype-->
                                                                                                    <ul class="formbox"
                                                                                                        <?php if('6'==$common->business_id && $common->other_maritial_status1=='Married'): ?><?php else: ?> style="display:none" <?php endif; ?> >
                                                                                                        <li style="width:80px;">
                                                                                                            <?php if($common->personalname=='Single'): ?>
                                                                                                                <input type="text"
                                                                                                                       class="form-control"
                                                                                                                       readonly
                                                                                                                       value="Mr.">
                                                                                                            <?php elseif($common->personalname=='Married'): ?>
                                                                                                                <input type="text"
                                                                                                                       class="form-control"
                                                                                                                       readonly
                                                                                                                       value="Mrs.">
                                                                                                            <?php elseif($common->personalname=='Widowed'): ?>
                                                                                                                <input type="text"
                                                                                                                       class="form-control"
                                                                                                                       readonly
                                                                                                                       value="Miss.">
                                                                                                            <?php endif; ?>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <input type="text"
                                                                                                                   class="form-control"
                                                                                                                   readonly
                                                                                                                   value="<?php echo $common->maritial_first_name;?>">
                                                                                                        </li>
                                                                                                        <li style="width:100px;">
                                                                                                            <input type="text"
                                                                                                                   readonly
                                                                                                                   class="form-control"
                                                                                                                   value="<?php echo $common->maritial_middle_name;?>">
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <input type="text"
                                                                                                                   class="form-control"
                                                                                                                   readonly
                                                                                                                   value="<?php echo $common->maritial_last_name;?>">
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="row">
                                                                                                <div class="col-md-12">
                                                                                                    <ul class="formbox">
                                                                                                        <li> <?php $month_names = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");?>
                                                                                                            <select class="form-control"
                                                                                                                    name="other_dob_month_s">
                                                                                                                <option value="">
                                                                                                                    Select
                                                                                                                </option>
                                                                                                                <?php
                                                                                                                foreach($month_names as $month)
                                                                                                                {?>
                                                                                                                <option value="<?php echo $month;?>"
                                                                                                                        <?php if($common->other_dob_month_s==$month): ?> selected <?php endif; ?>><?php echo $month;?></option>
                                                                                                                <?php }
                                                                                                                ?>
                                                                                                            </select>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <select class="form-control otherdob1"
                                                                                                                    name="other_dob_day_s">
                                                                                                                <option value="">
                                                                                                                    Select
                                                                                                                </option>
                                                                                                                <?php for( $i = 1940; $i <= 2000; $i++ ){?>
                                                                                                                <option <?php if($common->other_dob_day_s==$i): ?> selected
                                                                                                                        <?php endif; ?> value="<?php echo $i;?>"><?php echo $i;?></option><?php }?>
                                                                                                            </select>
                                                                                                        </li>
                                                                                                        <li style="width:70px;">
                                                                                                            <input class="form-control totalyears1"
                                                                                                                   readonly
                                                                                                                   <?php if($common->other_dob_day_s !=''): ?> value="<?php echo e($totayr1); ?> Years" <?php endif; ?>>
                                                                                                        </li>

                                                                                                    </ul>
                                                                                                </div>
                                                                                            </div>

                                                                                            <div class="row">

                                                                                                <div class="col-md-12">
                                                                                                    <ul class="formbox">
                                                                                                        <li>
                                                                                                            <select class="form-control"
                                                                                                                    name="other_ethnic_s"
                                                                                                                    style="width:193px;">
                                                                                                                <option value="">
                                                                                                                    Select
                                                                                                                </option>
                                                                                                                <?php $__currentLoopData = $ethnic; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lan1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                                                                    <option value="<?php echo e($lan1->ethnic_name); ?>"
                                                                                                                            <?php if($common->other_ethnic_s==$lan1->ethnic_name): ?> selected <?php endif; ?>><?php echo e($lan1->ethnic_name); ?></option>
                                                                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                                                            </select>
                                                                                                        </li>

                                                                                                    </ul>
                                                                                                </div>
                                                                                            </div>


                                                                                            <div class="row">

                                                                                                <div class="col-md-12">
                                                                                                    <ul class="formbox">
                                                                                                        <li>
                                                                                                            <input class="form-control"
                                                                                                                   readonly
                                                                                                                   name="other_main_language"
                                                                                                                   value="American English"
                                                                                                                   style="width:193px;">
                                                                                                        </li>

                                                                                                    </ul>
                                                                                                </div>
                                                                                            </div>

                                                                                            <div class="row">

                                                                                                <div class="col-md-12">
                                                                                                    <ul class="formbox">
                                                                                                        <li>
                                                                                                            <select class="form-control firstlang"
                                                                                                                    name="other_first_language1"
                                                                                                                    id="firstlang"
                                                                                                                    style="width:193px;">
                                                                                                                <option value="1">
                                                                                                                    Select
                                                                                                                </option>
                                                                                                                <?php $__currentLoopData = $language; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lan1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                                                                    <option value="<?php echo e($lan1->language_name); ?>"
                                                                                                                            <?php if($common->other_first_language1==$lan1->language_name): ?> selected <?php endif; ?>><?php echo e($lan1->language_name); ?></option>
                                                                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                                                            </select>
                                                                                                        </li>

                                                                                                    </ul>
                                                                                                </div>
                                                                                            </div>

                                                                                            <div class="row">

                                                                                                <div class="col-md-12">
                                                                                                    <ul class="formbox">
                                                                                                        <li>
                                                                                                            <select class="form-control firstlang"
                                                                                                                    name="other_second_language1"
                                                                                                                    id="seclang"
                                                                                                                    style="width:193px;">
                                                                                                                <option value="">
                                                                                                                    Select
                                                                                                                </option>
                                                                                                                <?php $__currentLoopData = $language; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lan1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                                                                    <option value="<?php echo e($lan1->language_name); ?>"
                                                                                                                            <?php if($common->other_second_language1==$lan1->language_name): ?> selected <?php endif; ?>><?php echo e($lan1->language_name); ?></option>
                                                                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                                                            </select>
                                                                                                        </li>

                                                                                                    </ul>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    <?php endif; ?>
                                                                                    <div class="clear"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="row"
                                                                         style="border-bottom:1px solid;margin-bottom:15px;padding-bottom:10px; display:none">
                                                                        <div class="form-group"
                                                                             <?php if('6'==$common->business_id): ?><?php else: ?> style="display:none" <?php endif; ?>>
                                                                            <label class="control-label col-md-2"
                                                                                   style="width:15% !important;font-size:13px">Client
                                                                                Name :</label>
                                                                            <div class="col-md-1">
                                                                                <input type="text" class="form-control"
                                                                                       readonly
                                                                                       value="<?php echo ucfirst($common->nametype);?>">
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                                <input type="text" class="form-control"
                                                                                       readonly
                                                                                       value="<?php echo $common->first_name;?>">
                                                                            </div>
                                                                            <div class="col-md-1">
                                                                                <input type="text" class="form-control"
                                                                                       readonly
                                                                                       value="<?php echo $common->middle_name;?>">
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                                <input type="text" class="form-control"
                                                                                       readonly
                                                                                       value="<?php echo $common->last_name;?>">
                                                                            </div>


                                                                        </div>
                                                                        <div class="form-group"
                                                                             <?php if('6'==$common->business_id && $common->other_maritial_status1=='Married'): ?><?php else: ?> style="display:none" <?php endif; ?>>
                                                                            <label class="control-label col-md-2"
                                                                                   style="width:15% !important;font-size:13px">Spouse
                                                                                Name :</label>
                                                                            <div class="col-md-1">
                                                                                <input type="text" class="form-control"
                                                                                       value="<?php echo ucfirst($common->personalname);?>">
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                                <input type="text" class="form-control"
                                                                                       value="<?php echo $common->maritial_first_name;?>">
                                                                            </div>
                                                                            <div class="col-md-1">
                                                                                <input type="text" class="form-control"
                                                                                       value="<?php echo $common->maritial_middle_name;?>">
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                                <input type="text" class="form-control"
                                                                                       value="<?php echo $common->maritial_last_name;?>">
                                                                            </div>


                                                                        </div>

                                                                        <div class="form-group"
                                                                             <?php if('6'==$common->business_id): ?><?php else: ?> style="display:none" <?php endif; ?>>
                                                                            <label class="control-label col-md-2"
                                                                                   style="width:15% !important;font-size:13px">Maritial
                                                                                Status :</label>
                                                                            <div class="col-md-3">
                                                                                <select class="form-control maritialsts"
                                                                                        name="other_maritial_status">
                                                                                    <option value="">Select</option>

                                                                                    <option value="Single"
                                                                                            <?php if($common->other_maritial_status=='Single'): ?> selected <?php endif; ?>>
                                                                                        Single
                                                                                    </option>

                                                                                    <option value="Married"
                                                                                            <?php if($common->other_maritial_status=='Married'): ?> selected <?php endif; ?>>
                                                                                        Married
                                                                                    </option>
                                                                                    <option value="Widowed"
                                                                                            <?php if($common->other_maritial_status=='Widowed'): ?> selected <?php endif; ?>>
                                                                                        Widowed
                                                                                    </option>

                                                                                </select>
                                                                            </div>

                                                                        </div>


                                                                    <!--<div class="form-group"  <?php if('6'==$common->business_id): ?> <?php else: ?> style="display:none" <?php endif; ?> <?php if($common->other_maritial_status=='Widowed'): ?> style="display:none;" <?php endif; ?> >
                <label class="control-label col-md-2" style="width:15% !important;font-size:13px">DOB :</label>
               <div class="col-md-2" style="width:15% !important;">
                     <?php $month_names = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");?>
                                                                            <select class="form-control" name="other_dob_month">
                       <option value="">Select</option>
                         <?php
                                                                    foreach($month_names as $month)
                                                                    {?>
                                                                            <option value="<?php echo $month;?>" <?php if($common->other_dob_month==$month): ?> selected <?php endif; ?>><?php echo $month;?></option>
                            <?php }
                                                                    ?>
                                                                            </select>
               </div>
           
               <div class="col-md-1" style="width:10% !important;">
                   <select class="form-control" name="other_dob_day">
                       <option value="">Select</option>
                       <?php for( $i = 1; $i <= 31; $i++ ){?><option <?php if($common->other_dob_day==$i): ?> selected <?php endif; ?> value="<?php echo $i;?>"><?php echo $i;?></option><?php }?>
                                                                            </select>
               </div>
         
         
            </div>!-->

                                                                        <div class="form-group sps"
                                                                             <?php if($common->other_maritial_status=='Single'): ?> style="display:none;"
                                                                             <?php elseif($common->business_id=='6' && $common->other_maritial_status=='Married'): ?> style="display:block;"
                                                                             <?php else: ?> style="display:none;" <?php endif; ?> >
                                                                            <label class="control-label col-md-2"
                                                                                   style="width:15% !important;font-size:13px">Spouse
                                                                                DOB :</label>
                                                                            <div class="col-md-2"
                                                                                 style="width:13% !important;">
                                                                                <?php $month_names = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");?>
                                                                                <select class="form-control"
                                                                                        name="other_spouse_month">
                                                                                    <option value="">Select</option>
                                                                                    <?php
                                                                                    foreach($month_names as $month)
                                                                                    {?>
                                                                                    <option value="<?php echo $month;?>"
                                                                                            <?php if($common->other_spouse_month==$month): ?> selected <?php endif; ?>><?php echo $month;?></option>
                                                                                    <?php }
                                                                                    ?>

                                                                                </select>
                                                                            </div>

                                                                            <div class="col-md-1"
                                                                                 style="width:12% !important;">
                                                                                <select class="form-control"
                                                                                        name="other_spouse_day">
                                                                                    <option value="">Select</option>
                                                                                    <?php for( $i = 1; $i <= 31; $i++ ){?>
                                                                                    <option <?php if($common->other_spouse_day==$i): ?> selected
                                                                                            <?php endif; ?> value="<?php echo $i;?>"><?php echo $i;?></option><?php }?>
                                                                                </select>
                                                                            </div>


                                                                        </div>

                                                                        <div class="form-group mrgdate"
                                                                             <?php if($common->other_maritial_status=='Single'): ?> style="display:none;"
                                                                             <?php elseif($common->business_id=='6' && $common->other_maritial_status=='Married'): ?> style="display:block;"
                                                                             <?php else: ?> style="display:none;" <?php endif; ?> >
                                                                            <label class="control-label col-md-2"
                                                                                   style="width:15% !important;font-size:13px">Marriage
                                                                                Date :</label>
                                                                            <div class="col-md-2">

                                                                                <?php $month_names = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");?>
                                                                                <select class="form-control"
                                                                                        name="other_marriage_month">
                                                                                    <option value="">Select</option>
                                                                                    <?php
                                                                                    foreach($month_names as $month)
                                                                                    {?>
                                                                                    <option value="<?php echo $month;?>"
                                                                                            <?php if($common->other_marriage_month==$month): ?> selected <?php endif; ?>><?php echo $month;?></option>
                                                                                    <?php }
                                                                                    ?>
                                                                                </select>
                                                                            </div>


                                                                            <div class="col-md-1">
                                                                                <select class="form-control"
                                                                                        name="other_marriage_day">
                                                                                    <option value="">Select</option>
                                                                                    <?php for( $i = 1; $i <= 31; $i++ ){?>
                                                                                    <option <?php if($common->other_marriage_day==$i): ?> selected
                                                                                            <?php endif; ?> value="<?php echo $i;?>"><?php echo $i;?></option><?php }?>
                                                                                </select>
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group"
                                                                             <?php if($common->business_id=='6' && $common->other_maritial_status=='Married'): ?> style="display:none;"
                                                                             <?php else: ?> style="display:block;" <?php endif; ?>>
                                                                            <label class="control-label col-md-2"
                                                                                   style="width:15% !important;font-size:13px">Ethnic:</label>
                                                                            <div class="col-md-3">
                                                                                <select class="form-control" name="">
                                                                                    <option value="">Select</option>
                                                                                    <?php $__currentLoopData = $ethnic; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lan1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                                        <option value="<?php echo e($lan1->ethnic_name); ?>"
                                                                                                <?php if($common->other_ethnic==$lan1->ethnic_name): ?> selected <?php endif; ?>><?php echo e($lan1->ethnic_name); ?></option>
                                                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                                </select>
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-2"
                                                                                   style="width:15% !important;font-size:13px">Main
                                                                                Language:</label>
                                                                            <div class="col-md-3">
                                                                                <input class="form-control" readonly
                                                                                       name="other_main_language"
                                                                                       value="American English">
                                                                            </div>
                                                                        </div>

                                                                    <!--<div class="form-group">
               <label class="control-label col-md-2" style="width:15% !important;font-size:13px">1st Language:</label>
               <div class="col-md-3">
                   <select class="form-control firstlang" name="other_first_language1" id="firstlang">
                       <option value="1">Select</option>
                         <?php $__currentLoopData = $language; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lan1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                        <option value="<?php echo e($lan1->language_name); ?>" <?php if($common->other_first_language1==$lan1->language_name): ?> selected <?php endif; ?>><?php echo e($lan1->language_name); ?></option>
                         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                            </select>
               </div>
                  <div class="col-md-3" id="langonedrp"  <?php if($common->other_first_language1=='1'): ?> style="display:none;" <?php elseif($common->other_first_language1!=''): ?> <?php else: ?> style="display:none;" <?php endif; ?> >
                   <select class="form-control" id="other_first_language1" name="other_first_language1" >
                       <option value="">Select</option>
                      <?php if(!empty($other_first_language1)): ?>
                                                                        <option selected value="<?php echo e($other_first_language1->id); ?>"><?php echo e($other_first_language1->firstName.' '.$other_first_language1->middleName.' '.$other_first_language1->lastName); ?></option>
                      <?php endif; ?>
                                                                            </select>
               </div>
           </div>
           
                <div class="form-group">
               <label class="control-label col-md-2" style="width:15% !important;font-size:13px">2nd Language:</label>
               <div class="col-md-3">
                   <select class="form-control firstlang" name="other_second_language1" id="seclang">
                       <option value="">Select</option>
                         <?php $__currentLoopData = $language; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lan1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                        <option value="<?php echo e($lan1->language_name); ?>" <?php if($common->other_second_language1==$lan1->language_name): ?> selected <?php endif; ?>><?php echo e($lan1->language_name); ?></option>
                         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                            </select>
               </div>
              
               <!-- <div class="col-md-3"  id="langtwodrp" <?php if($common->other_second_language=='2'): ?> style="display:none;" <?php elseif($common->other_second_language!=''): ?>  <?php else: ?> style="display:none;" <?php endif; ?> >
                   <select class="form-control" name="other_second_language" id="other_second_language">
                       <option value="">Select</option>
                      
                         <?php $__currentLoopData = $secondlanguagename; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lan1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                        <option value="<?php echo e($lan1->employee_id); ?>" <?php if ($lan1->employee_id == $common->other_second_language) {
                                                                            echo 'Selected';
                                                                        }?>><?php echo e($lan1->firstName.' '.$lan1->middleName.' '.$lan1->lastName); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                                                            </select>
               </div>
           </div>
          !-->


                                                                    </div>


                                                                </div>

                                                                <br>
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"
                                                                     style="display:none">
                                                                    <br>
                                                                    <div class="Branch">
                                                                        <h1>Conversation List</h1>
                                                                    </div>
                                                                    <br/>

                                                                    <div class="col-md-12 col-sm-12 col-xs-12 text-right"
                                                                         style="display:none">

                                                                        <div class="clear"></div>
                                                                        <table class="table table-hover table-bordered dataTable no-footer">
                                                                            <thead>
                                                                            <tr>
                                                                                <th>No.</th>

                                                                                <th> Date</br> Day</br> Time</th>

                                                                                <th>Related To</th>
                                                                                <th>Description</th>
                                                                                <th>Notes</th>
                                                                                <th>Action</th>
                                                                            </tr>
                                                                            </thead>
                                                                            <?php
                                                                            $cnt = count($conversation);
                                                                            if($cnt > 0)
                                                                            {
                                                                            $cnt = 0;
                                                                            ?>
                                                                            <?php $__currentLoopData = $conversation; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $conversation): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>


                                                                                <tr>
                                                                                    <td style="text-align:center;"><?php echo e($loop->index+1); ?> </td>
                                                                                    <td style="text-align:center;"><?php echo e($conversation->creattiondate); ?>

                                                                                        <br> <?php echo e($conversation->day); ?>

                                                                                        <br> <?php echo e($conversation->time); ?>

                                                                                    </td>
                                                                                    <td style="text-align:center;"><?php echo e($conversation->relatednames); ?> </td>
                                                                                    <td><?php echo e($conversation->condescription); ?> </td>
                                                                                    <td><?php echo e($conversation->connotes); ?> </td>
                                                                                    <td style="text-align:center;">
                                                                                        <a class="btn-action btn-view-edit conversationID"
                                                                                           data-id="<?php echo e($conversation->id); ?>"><i
                                                                                                    class="fa fa-edit"></i></a>

                                                                                    </td>


                                                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                                                                </tr>
                                                                                <?php
                                                                                }
                                                                                else
                                                                                {
                                                                                ?>
                                                                                <tr>
                                                                                    <td style="text-align:center;"
                                                                                        colspan="7">No records found
                                                                                    </td>
                                                                                </tr>
                                                                                <?php
                                                                                }
                                                                                ?>
                                                                                </tbody>

                                                                        </table>
                                                                    </div>

                                                                </div>

                                                                <div class="col-md-12 col-sm-12 col-xs-12"
                                                                     style="display:none">
                                                                    <div class="Branch"
                                                                         style="text-align:left; padding-left:15px;">
                                                                        <h1 class="text-center">Notes List</h1>
                                                                    </div>
                                                                </div>
                                                                <br>

                                                                <div class="col-md-12 col-sm-12 col-xs-12 text-right"
                                                                     style="display:none">

                                                                    <div class="clear"></div>
                                                                    <table class="table table-hover table-bordered dataTable no-footer">
                                                                        <thead>
                                                                        <tr>
                                                                            <th>No.</th>

                                                                            <th> Date</br> Day</br> Time</th>

                                                                            <th>Related To</th>
                                                                            <th>Description</th>
                                                                            <th>Notes</th>
                                                                            <th>Action</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <?php
                                                                        $cnt = count($notesdata);
                                                                        if($cnt > 0)
                                                                        {
                                                                        $cnt = 0;
                                                                        ?>
                                                                        <?php $__currentLoopData = $notesdata; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $conversation): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                            <tr>
                                                                                <td style="text-align:center;"><?php echo e($loop->index+1); ?> </td>
                                                                                <td style="text-align:center;"><?php echo e($conversation->creattiondate); ?>

                                                                                    <br> <?php echo e($conversation->noteday); ?>

                                                                                    <br> <?php echo e($conversation->notetime); ?>

                                                                                </td>
                                                                                <td style="text-align:center;"><?php echo e($conversation->notesrelated); ?> </td>
                                                                                <td><?php echo e($conversation->notesrelatedcat); ?> </td>
                                                                                <td><?php echo e($conversation->notes); ?> </td>
                                                                                <td style="text-align:center;">

                                                                                    <a class="btn-action btn-view-edit notesID"
                                                                                       data-id="<?php echo e($conversation->id); ?>"><i
                                                                                                class="fa fa-edit"></i></a>
                                                                                </td>
                                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                            </tr>
                                                                            <?php
                                                                            }
                                                                            else
                                                                            {
                                                                            ?>
                                                                            <tr>
                                                                                <td style="text-align:center;"
                                                                                    colspan="7">No records found
                                                                                </td>
                                                                            </tr>
                                                                            <?php
                                                                            }
                                                                            ?>
                                                                            </tbody>

                                                                    </table>
                                                                </div>

                                                            </div>


                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="tab-pane fade" id="tab3primary">
                                                <?php

                                                if(!empty($clientser5->pricetype))
                                                {
                                                ?>

                                                <div class="form-group bg-color">
                                                    <label class="control-label col-md-2"
                                                           style="width: 145px;text-align:right;">Price Selection
                                                        :</label>
                                                    <div class="col-md-2">
                                                        <div class="pricetype">
                                                            <select name="pricetype" type="text" id="pricetype"
                                                                    class="form-control pricetype"
                                                                    <?php if((!empty($clientser5->pricetype)) and ($clientser5->pricetype!=1)): ?> style="pointer-events: none;" <?php endif; ?>>
                                                                <option value="1">Select</option>
                                                                <option value="Regular"
                                                                        <?php if(empty($clientser5->pricetype)): ?> <?php else: ?> <?php if($clientser5->pricetype=='Regular'): ?> selected <?php endif; ?> <?php endif; ?>>
                                                                    Regular
                                                                </option>
                                                                <option value="Combo"
                                                                        <?php if(empty($clientser5->pricetype)): ?> <?php else: ?> <?php if($clientser5->pricetype=='Combo'): ?> selected <?php endif; ?> <?php endif; ?>>
                                                                    Combo
                                                                </option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <label class="control-label col-md-1" style="width: 106px;">Currency
                                                        :</label>
                                                    <div class="col-md-2">
                                                        <div class="pricetype">
                                                            <select name="currency" type="text" id="currency"
                                                                    class="form-control currency1"
                                                                    <?php if(!empty($clientser5->currency)): ?> style="pointer-events: none;background: lightgreen none repeat scroll 0% 0%;" <?php endif; ?>>
                                                                <option value="">Select</option>
                                                                <?php $__currentLoopData = $currency; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cur): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                    <option value="<?php echo e($cur->id); ?>"
                                                                            <?php if(empty($clientser5->currency)): ?> <?php else: ?> <?php if($clientser5->currency==$cur->id): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e($cur->currency.' '.$cur->sign); ?></option>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                            </select>
                                                        </div>
                                                        <input name="ckq" type="hidden" id="ckq" class="ckq"
                                                               value="<?php if(empty($clientser5->sign)): ?> <?php else: ?> <?php echo e($clientser5->sign); ?> <?php endif; ?>">
                                                    </div>
                                                    <label class="control-label col-md-1" style="width: 142px;">Service
                                                        Period :</label>
                                                    <div class="col-md-2">
                                                        <div class="pricetype">
                                                            <select name="serviceperiod" type="text" id="serviceperiod"
                                                                    class="form-control serviceperiod"
                                                                    <?php if(!empty($clientser5->serviceperiod)): ?> style="pointer-events: none;" <?php endif; ?>>
                                                                <option value="">Select</option>
                                                                <?php $__currentLoopData = $period; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $period1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                    <option value="<?php echo e($period1->id); ?>"
                                                                            <?php if(empty($clientser5->serviceperiod)): ?> <?php else: ?> <?php if($clientser5->serviceperiod==$period1->id): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e($period1->period); ?></option>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-1">
                                                    <!--<a id="<?php echo e($common->cid); ?>" class="resetServiceInfo btn btn-danger">Reset1</a>-->
                                                        <a class="btn btn-danger"
                                                           onclick="return confirm('Are you sure you want to reset the form?')"
                                                           href="<?php echo e(route('customer.resetServiceInfo1',$common->cid)); ?>">Reset</a>

                                                    </div>
                                                </div>
                                                <?php

                                                }

                                                else if(!empty($taxclientser5->pricetype))
                                                {

                                                ?>

                                                <div class="form-group bg-color">
                                                    <label class="control-label col-md-2"
                                                           style="width: 145px;text-align:right;">Price Selection
                                                        :</label>
                                                    <div class="col-md-2">
                                                        <div class="pricetype">
                                                            <select name="pricetype" type="text" id="pricetype"
                                                                    class="form-control pricetype"
                                                                    <?php if((!empty($taxclientser5->pricetype)) and ($taxclientser5->pricetype!=1)): ?> style="pointer-events: none;" <?php endif; ?>>
                                                                <option value="1">Select</option>
                                                                <option value="Regular"
                                                                        <?php if(empty($taxclientser5->pricetype)): ?> <?php else: ?> <?php if($taxclientser5->pricetype=='Regular'): ?> selected <?php endif; ?> <?php endif; ?>>
                                                                    Regular
                                                                </option>
                                                                <option value="Combo"
                                                                        <?php if(empty($taxclientser5->pricetype)): ?> <?php else: ?> <?php if($taxclientser5->pricetype=='Combo'): ?> selected <?php endif; ?> <?php endif; ?>>
                                                                    Combo
                                                                </option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <label class="control-label col-md-1" style="width: 106px;">Currency
                                                        :</label>
                                                    <div class="col-md-2">
                                                        <div class="pricetype">
                                                            <select name="currency" type="text" id="currency"
                                                                    class="form-control currency1"
                                                                    <?php if(!empty($taxclientser5->currency)): ?> style="pointer-events: none;background: lightgreen none repeat scroll 0% 0%;" <?php endif; ?>>
                                                                <option value="">Select</option>
                                                                <?php $__currentLoopData = $currency; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cur): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                    <option value="<?php echo e($cur->id); ?>"
                                                                            <?php if(empty($taxclientser5->currency)): ?> <?php else: ?> <?php if($taxclientser5->currency==$cur->id): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e($cur->currency.' '.$cur->sign); ?></option>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                            </select>
                                                        </div>
                                                        <input name="ckq" type="hidden" id="ckq" class="ckq"
                                                               value="<?php if(empty($clientser5->sign)): ?> <?php else: ?> <?php echo e($clientser5->sign); ?> <?php endif; ?>">
                                                    </div>
                                                    <label class="control-label col-md-1" style="width: 142px;">Service
                                                        Period :</label>
                                                    <div class="col-md-2">
                                                        <div class="pricetype">
                                                            <select name="serviceperiod" type="text" id="serviceperiod"
                                                                    class="form-control serviceperiod"
                                                                    <?php if(!empty($taxclientser5->serviceperiod)): ?> style="pointer-events: none;" <?php endif; ?>>
                                                                <option value="">Select</option>
                                                                <?php $__currentLoopData = $period; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $period1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                    <option value="<?php echo e($period1->id); ?>"
                                                                            <?php if(empty($taxclientser5->serviceperiod)): ?> <?php else: ?> <?php if($taxclientser5->serviceperiod==$period1->id): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e($period1->period); ?></option>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-1"><a id="<?php echo e($common->cid); ?>"
                                                                             class="resetServiceInfo btn btn-danger">Reset</a>
                                                    </div>
                                                </div>
                                                <?php }

                                                else if(!empty($taxservices5->pricetype))
                                                {

                                                ?>

                                                <div class="form-group bg-color">
                                                    <label class="control-label col-md-2"
                                                           style="width: 145px;text-align:right;">Price Selection
                                                        :</label>
                                                    <div class="col-md-2">
                                                        <div class="pricetype">
                                                            <select name="pricetype" type="text" id="pricetype"
                                                                    class="form-control pricetype"
                                                                    <?php if((!empty($taxservices5->pricetype)) and ($taxservices5->pricetype!=1)): ?> style="pointer-events: none;" <?php endif; ?>>
                                                                <option value="1">Select</option>
                                                                <option value="Regular"
                                                                        <?php if(empty($taxservices5->pricetype)): ?> <?php else: ?> <?php if($taxservices5->pricetype=='Regular'): ?> selected <?php endif; ?> <?php endif; ?>>
                                                                    Regular
                                                                </option>
                                                                <option value="Combo"
                                                                        <?php if(empty($taxservices5->pricetype)): ?> <?php else: ?> <?php if($taxservices5->pricetype=='Combo'): ?> selected <?php endif; ?> <?php endif; ?>>
                                                                    Combo
                                                                </option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <label class="control-label col-md-1" style="width: 106px;">Currency
                                                        :</label>
                                                    <div class="col-md-2">
                                                        <div class="pricetype">
                                                            <select name="currency" type="text" id="currency"
                                                                    class="form-control currency1"
                                                                    <?php if(!empty($taxservices5->currency)): ?> style="pointer-events: none;background: lightgreen none repeat scroll 0% 0%;" <?php endif; ?>>
                                                                <option value="">Select</option>
                                                                <?php $__currentLoopData = $currency; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cur): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                    <option value="<?php echo e($cur->id); ?>"
                                                                            <?php if(empty($taxservices5->currency)): ?> <?php else: ?> <?php if($taxservices5->currency==$cur->id): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e($cur->currency.' '.$cur->sign); ?></option>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                            </select>
                                                        </div>
                                                        <input name="ckq" type="hidden" id="ckq" class="ckq"
                                                               value="<?php if(empty($clientser5->sign)): ?> <?php else: ?> <?php echo e($clientser5->sign); ?> <?php endif; ?>">
                                                    </div>
                                                    <label class="control-label col-md-1" style="width: 142px;">Service
                                                        Period :</label>
                                                    <div class="col-md-2">
                                                        <div class="pricetype">
                                                            <select name="serviceperiod" type="text" id="serviceperiod"
                                                                    class="form-control serviceperiod"
                                                                    <?php if(!empty($taxservices5->serviceperiod)): ?> style="pointer-events: none;" <?php endif; ?>>
                                                                <option value="">Select</option>
                                                                <?php $__currentLoopData = $period; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $period1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                    <option value="<?php echo e($period1->id); ?>"
                                                                            <?php if(empty($taxservices5->serviceperiod)): ?> <?php else: ?> <?php if($taxservices5->serviceperiod==$period1->id): ?> selected <?php endif; ?> <?php endif; ?>><?php echo e($period1->period); ?></option>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-1"><a id="<?php echo e($common->cid); ?>"
                                                                             class="resetServiceInfo btn btn-danger">Reset</a>
                                                    </div>
                                                </div>
                                                <?php }
                                                else
                                                {
                                                //echo "<pre>";print_r($taxservices); 
                                                // exit('22222222');
                                                ?>
                                                <div class="form-group bg-color">
                                                    <label class="control-label col-md-2"
                                                           style="width: 145px;text-align:right;">Price Selection
                                                        :</label>
                                                    <div class="col-md-2">
                                                        <div class="pricetype">
                                                            <select name="pricetype" type="text" id="pricetype"
                                                                    class="form-control pricetype">
                                                                <option value="1">Select</option>
                                                                <option value="Regular">Regular</option>
                                                                <option value="Combo">Combo</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <label class="control-label col-md-1" style="width: 106px;">Currency
                                                        :</label>
                                                    <div class="col-md-2">
                                                        <div class="pricetype">
                                                            <select name="currency" type="text" id="currency"
                                                                    class="form-control currency1">
                                                                <option value="">Select</option>
                                                                <?php $__currentLoopData = $currency; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cur): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                    <option value="<?php echo e($cur->id); ?>"><?php echo e($cur->currency.' '.$cur->sign); ?></option>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                            </select>
                                                        </div>
                                                        <input name="ckq" type="hidden" id="ckq" class="ckq"
                                                               value="<?php if(empty($clientser5->sign)): ?> <?php else: ?> <?php echo e($clientser5->sign); ?> <?php endif; ?>">
                                                    </div>
                                                    <label class="control-label col-md-1" style="width: 142px;">Service
                                                        Period :</label>
                                                    <div class="col-md-2">
                                                        <div class="pricetype">
                                                            <select name="serviceperiod" type="text" id="serviceperiod"
                                                                    class="form-control serviceperiod">
                                                                <option value="">Select</option>
                                                                <?php $__currentLoopData = $period; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $period1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                    <option value="<?php echo e($period1->id); ?>"><?php echo e($period1->period); ?></option>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-1"><a id="<?php echo e($common->cid); ?>"
                                                                             class="resetServiceInfo btn btn-danger">Reset</a>
                                                    </div>
                                                </div>

                                                <?php
                                                }
                                                ?>
                                                <div class="Branch">
                                                    <h1>Service information</h1>
                                                </div>

                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <div class="row">
                                                        <?php if($common->business_id !='0'): ?>
                                                            <div class="col-md-12">
                                                                <?php if($common->business_id !='0'): ?>
                                                                    <div class="form-group" style="margin-bottom: 0px;">

                                                                        <div class="col-lg-7 cust_left"
                                                                             style="padding-left: 0px;">
                                                                            <div class="row">
                                                                                <div class="col-md-6 col-sm-4">
                                                                                    <label class="control-label"
                                                                                           style="padding-left:0px !important;padding-right:0px !important;text-align:right !important;">Accounting
                                                                                        Service :</label>
                                                                                    <?php if($common->accounting_period != '')
                                                                                    {
                                                                                    ?>
                                                                                    <input type="hidden"
                                                                                           name="accounting_period"
                                                                                           value="<?php echo $common->accounting_period;?>">

                                                                                    <select class="form-control"
                                                                                            disabled="disabled">
                                                                                        <option value="">Select</option>
                                                                                        <option value="10-2" <?php if ($common->accounting_period == '10-2') {
                                                                                            echo 'Selected';
                                                                                        }?>>Monthly
                                                                                        </option>
                                                                                        <option value="4-2" <?php if ($common->accounting_period == '4-2') {
                                                                                            echo 'Selected';
                                                                                        }?>>Quarterly
                                                                                        </option>
                                                                                        <option value="5-2" <?php if ($common->accounting_period == '5-2') {
                                                                                            echo 'Selected';
                                                                                        }?>>Half-Yearly
                                                                                        </option>
                                                                                        <option value="7-2" <?php if ($common->accounting_period == '7-2') {
                                                                                            echo 'Selected';
                                                                                        }?>>Yearly
                                                                                        </option>
                                                                                    </select>
                                                                                    <?php
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                    ?>

                                                                                    <select class="form-control accounting_period"
                                                                                            name="accounting_period">
                                                                                        <option value="">Select</option>
                                                                                        <option value="10-2" <?php if ($common->accounting_period == '10-2') {
                                                                                            echo 'Selected';
                                                                                        }?>>Monthly
                                                                                        </option>
                                                                                        <option value="4-2" <?php if ($common->accounting_period == '4-2') {
                                                                                            echo 'Selected';
                                                                                        }?>>Quarterly
                                                                                        </option>
                                                                                        <option value="5-2" <?php if ($common->accounting_period == '5-2') {
                                                                                            echo 'Selected';
                                                                                        }?>>Half-Yearly
                                                                                        </option>
                                                                                        <option value="7-2" <?php if ($common->accounting_period == '7-2') {
                                                                                            echo 'Selected';
                                                                                        }?>>Yearly
                                                                                        </option>
                                                                                    </select>

                                                                                    <?php
                                                                                    }
                                                                                    ?>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-4 p_991"
                                                                                     style="padding-left:5px;">
                                                                                    <label class="control-label"
                                                                                           style="padding-left:0px !important;padding-right:0px !important;text-align:right !important;">St.
                                                                                        Month</label>
                                                                                    <select class="form-control"
                                                                                            name="ac_service_month">
                                                                                        <option value=""> Select
                                                                                        </option>
                                                                                        <option value="Jan" <?php if ($common->ac_service_month == 'Jan') {
                                                                                            echo 'Selected';
                                                                                        }?>>Jan
                                                                                        </option>
                                                                                        <option value="Feb" <?php if ($common->ac_service_month == 'Feb') {
                                                                                            echo 'Selected';
                                                                                        }?>>Feb
                                                                                        </option>
                                                                                        <option value="Mar" <?php if ($common->ac_service_month == 'Mar') {
                                                                                            echo 'Selected';
                                                                                        }?>>Mar
                                                                                        </option>
                                                                                        <option value="Apr" <?php if ($common->ac_service_month == 'Apr') {
                                                                                            echo 'Selected';
                                                                                        }?>>Apr
                                                                                        </option>
                                                                                        <option value="May" <?php if ($common->ac_service_month == 'May') {
                                                                                            echo 'Selected';
                                                                                        }?>>May
                                                                                        </option>
                                                                                        <option value="Jun" <?php if ($common->ac_service_month == 'Jun') {
                                                                                            echo 'Selected';
                                                                                        }?>>Jun
                                                                                        </option>
                                                                                        <option value="Jul" <?php if ($common->ac_service_month == 'Jul') {
                                                                                            echo 'Selected';
                                                                                        }?>>Jul
                                                                                        </option>
                                                                                        <option value="Aug" <?php if ($common->ac_service_month == 'Aug') {
                                                                                            echo 'Selected';
                                                                                        }?>>Aug
                                                                                        </option>
                                                                                        <option value="Sep" <?php if ($common->ac_service_month == 'Sep') {
                                                                                            echo 'Selected';
                                                                                        }?>>Sep
                                                                                        </option>
                                                                                        <option value="Oct" <?php if ($common->ac_service_month == 'Oct') {
                                                                                            echo 'Selected';
                                                                                        }?>>Oct
                                                                                        </option>
                                                                                        <option value="Nov" <?php if ($common->ac_service_month == 'Nov') {
                                                                                            echo 'Selected';
                                                                                        }?>>Nov
                                                                                        </option>
                                                                                        <option value="Dec" <?php if ($common->ac_service_month == 'Dec') {
                                                                                            echo 'Selected';
                                                                                        }?>>Dec
                                                                                        </option>

                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-4 p_991"
                                                                                     style="padding-left:0px;">
                                                                                    <label class="control-label"
                                                                                           style="padding-left:0px !important;padding-right:0px !important;text-align:right !important;">St.
                                                                                        Year</label>
                                                                                    <select class="form-control"
                                                                                            name="ac_service_year">
                                                                                        <option value=""> Select
                                                                                        </option>
                                                                                        <option value="2019" <?php if ($common->ac_service_year == '2019') {
                                                                                            echo 'Selected';
                                                                                        }?>>2019
                                                                                        </option>
                                                                                        <option value="2020" <?php if ($common->ac_service_year == '2020') {
                                                                                            echo 'Selected';
                                                                                        }?>>2020
                                                                                        </option>
                                                                                        <option value="2021" <?php if ($common->ac_service_year == '2021') {
                                                                                            echo 'Selected';
                                                                                        }?>>2021
                                                                                        </option>

                                                                                    </select>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                        <div class="col-lg-5 cust_right">
                                                                            <div class="col-md-3 col-sm-6 p_991"
                                                                                 style="padding-left:5px;">
                                                                                <label class="control-label"
                                                                                       style="padding-left:0px !important;padding-right:0px !important;text-align:right !important;">End
                                                                                    Month</label>
                                                                                <select class="form-control" name="">
                                                                                    <option value=""> Select</option>
                                                                                    <option value="Jan">Jan</option>
                                                                                    <option value="Feb">Feb</option>
                                                                                    <option value="Mar">Mar</option>
                                                                                    <option value="Apr">Apr</option>
                                                                                    <option value="May">May</option>
                                                                                    <option value="Jun">Jun</option>
                                                                                    <option value="Jul">Jul</option>
                                                                                    <option value="Aug">Aug</option>
                                                                                    <option value="Sep">Sep</option>
                                                                                    <option value="Oct">Oct</option>
                                                                                    <option value="Nov">Nov</option>
                                                                                    <option value="Dec">Dec</option>
                                                                                </select>
                                                                            </div>
                                                                            <div class="col-md-3 col-sm-6 p_991"
                                                                                 style="padding-left:5px;">
                                                                                <label class="control-label"
                                                                                       style="padding-left:0px !important;padding-right:0px !important;text-align:right !important;">End
                                                                                    Year</label>
                                                                                <select class="form-control" name="">
                                                                                    <option value=""> Select</option>
                                                                                    <option>2021</option>
                                                                                    <option>2022</option>
                                                                                </select>
                                                                            </div>
                                                                            <div class="col-md-6 col-sm-12 p_991"
                                                                                 style="padding-left:5px;">
                                                                                <label class="control-label"
                                                                                       style="padding-left:0px !important;padding-right:0px !important;text-align:right !important;">Reason</label>
                                                                                <textarea class="form-control"
                                                                                          rows="3"></textarea>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <hr style="margin:0px;border-top: 5px solid #868686;">
                                                                    <div class="form-group" style="margin-bottom: 0px;">
                                                                        <div class="col-lg-7 cust_left">
                                                                            <div id="customFieldsbo">
                                                                                <div class="form-group"
                                                                                     style="margin-bottom:0px;">
                                                                                    <div class="col-md-12"
                                                                                         style="padding-left:0px !important;">
                                                                                        <div class="row">
                                                                                            <div class="col-md-5 col-xs-12">
                                                                                                <label class="control-label">Taxation
                                                                                                    Service:</label>
                                                                                            </div>
                                                                                            <div class="col-md-2 col-xs-4 p_991"
                                                                                                 style="padding-left:5px;padding-right:0px;">
                                                                                                <label class="control-label">Period:</label>
                                                                                            </div>
                                                                                            <div class="col-md-2 col-xs-4 p_991">
                                                                                                <label class="control-label">Month:</label>
                                                                                            </div>
                                                                                            <div class="col-md-2 col-xs-4 p_991"
                                                                                                 style="padding-left:0px;">
                                                                                                <label class="control-label">Year:</label>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <?php  $taxservice2 = count($taxservices);

                                                                                if($taxservice2 == 0)
                                                                                {?>

                                                                                <div class="form-group">

                                                                                    <div class="col-md-12"
                                                                                         style="padding-left:0px !important;">
                                                                                        <div class="row">
                                                                                            <div class="col-md-5 col-xs-12">

                                                                                                <select class="form-control tax_service_1"
                                                                                                        name="taxation_service[]">
                                                                                                    <option value="">
                                                                                                        Select
                                                                                                    </option>
                                                                                                    <?php
                                                                                                    if($common->business_id == '6')
                                                                                                    {

                                                                                                    foreach($taxtitles as $titles)
                                                                                                    {
                                                                                                    if($titles->id == '7' || $titles->id == '8')
                                                                                                    {
                                                                                                    ?>
                                                                                                    <option value="<?php echo $titles->id;?>"><?php echo $titles->title;?></option>
                                                                                                    <?php

                                                                                                    }
                                                                                                    }
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                    foreach($taxtitle as $titles)
                                                                                                    {
                                                                                                    // if(in_array('7',$titles->id))
                                                                                                    // {
                                                                                                    if($titles->id == '1' || $titles->id == '2' || $titles->id == '3' || $titles->id == '4' || $titles->id == '5' || $titles->id == '6')
                                                                                                    {


                                                                                                    ?>
                                                                                                    <option value="<?php echo $titles->id;?>"><?php echo $titles->title;?></option>
                                                                                                    <?php

                                                                                                    }
                                                                                                    }

                                                                                                    }
                                                                                                    ?>
                                                                                                </select>

                                                                                                <select class="form-control tax_servicee"
                                                                                                        style="display:none;">
                                                                                                    <option value="">
                                                                                                        Select
                                                                                                    </option>
                                                                                                    <?php
                                                                                                    if($common->business_id == '6')
                                                                                                    {
                                                                                                    foreach($taxtitles as $titles)
                                                                                                    {
                                                                                                    if($titles->id == '7' || $titles->id == '8')
                                                                                                    {
                                                                                                    ?>
                                                                                                    <option value="<?php echo $titles->id;?>"><?php echo $titles->title;?></option>
                                                                                                    <?php
                                                                                                    }

                                                                                                    }
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                    foreach($taxtitle as $titles)
                                                                                                    {

                                                                                                    if($titles->id == '1' || $titles->id == '2' || $titles->id == '3' || $titles->id == '4' || $titles->id == '5' || $titles->id == '6')
                                                                                                    {


                                                                                                    ?>
                                                                                                    <option value="<?php echo $titles->id;?>"><?php echo $titles->title;?></option>
                                                                                                    <?php

                                                                                                    }
                                                                                                    }

                                                                                                    }
                                                                                                    ?>
                                                                                                </select>
                                                                                            </div>
                                                                                            <div class="col-md-2 col-xs-4 p_991"
                                                                                                 style="padding-left:5px;padding-right:0px;">

                                                                                                <select class="form-control tax_period tax_period_1"
                                                                                                        name="taxation_service_period[]">
                                                                                                    <option value="">
                                                                                                        Sel.
                                                                                                    </option>
                                                                                                    <option value="Monthly" <?php //if($tservice->taxation_service_period =='Monthly') { echo 'Selected';}?>>
                                                                                                        Monthly
                                                                                                    </option>
                                                                                                    <option value="Quarterly" <?php //if($tservice->taxation_service_period =='Quarterly') { echo 'Selected';}?>>
                                                                                                        Quarterly
                                                                                                    </option>
                                                                                                    <option value="Annually" <?php //if($tservice->taxation_service_period =='Annually') { echo 'Selected';}?>>
                                                                                                        Annually
                                                                                                    </option>
                                                                                                </select>
                                                                                            </div>

                                                                                            <div class="col-md-2 col-xs-4 p_991 taxservicemonth">
                                                                                                <select class="form-control "
                                                                                                        name="tax_service_month[]">
                                                                                                    <option value="">
                                                                                                        Sel.
                                                                                                    </option>
                                                                                                    <option value="Jan">
                                                                                                        Jan
                                                                                                    </option>
                                                                                                    <option value="Feb">
                                                                                                        Feb
                                                                                                    </option>
                                                                                                    <option value="Mar">
                                                                                                        Mar
                                                                                                    </option>
                                                                                                    <option value="Apr">
                                                                                                        Apr
                                                                                                    </option>
                                                                                                    <option value="May">
                                                                                                        May
                                                                                                    </option>
                                                                                                    <option value="Jun">
                                                                                                        Jun
                                                                                                    </option>
                                                                                                    <option value="Jul">
                                                                                                        Jul
                                                                                                    </option>
                                                                                                    <option value="Aug">
                                                                                                        Aug
                                                                                                    </option>
                                                                                                    <option value="Sep">
                                                                                                        Sep
                                                                                                    </option>
                                                                                                    <option value="Oct">
                                                                                                        Oct
                                                                                                    </option>
                                                                                                    <option value="Nov">
                                                                                                        Nov
                                                                                                    </option>
                                                                                                    <option value="Dec">
                                                                                                        Dec
                                                                                                    </option>

                                                                                                </select>
                                                                                            </div>
                                                                                            <div class="col-md-2 col-xs-4 p_991"
                                                                                                 style="padding-left:0px;">

                                                                                                <select class="form-control"
                                                                                                        name="tax_service_year[]">
                                                                                                    <option value="">
                                                                                                        Sel.
                                                                                                    </option>
                                                                                                    <option value="2019">
                                                                                                        2019
                                                                                                    </option>
                                                                                                    <option value="2020">
                                                                                                        2020
                                                                                                    </option>
                                                                                                    <option value="2021">
                                                                                                        2021
                                                                                                    </option>

                                                                                                </select>
                                                                                            </div>
                                                                                            <div class="col-md-1 col-xs-12 text-right p_991 mt10_991"
                                                                                                 style="padding:0px;">
                                                                                                <a class="btn btn-primary addCFnew"><i
                                                                                                            class="fa fa-plus"></i></a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-2"
                                                                                         style="display:none;">
                                                                                        <!-- <select class="form-control tax_period_1" name="taxation_service_period[]">
                                               <option value="">Select</option>
                                               <!--<option value="Monthly">Monthly</option>
                                               <option value="Quarterly">Quarterly</option>
                                               <option value="Annual">Annual</option>
                                           </select>
                                           <input type="text" class="form-control text_oneoption" name="taxation_service_period[]" readonly style="display:none;" disabled="disabled" />!-->
                                                                                    </div>
                                                                                    <div class="col-md-2 yearshide"
                                                                                         style="display:none;">
                                                                                        <select class="form-control"
                                                                                                name="taxyears[]">
                                                                                            <option value="">Select
                                                                                            </option>
                                                                                            <option value="2019">2019
                                                                                            </option>
                                                                                        </select>
                                                                                    </div>


                                                                                    <!--<div class="col-md-1" style="padding-right:0px;">-->
                                                                                    <!--    <label>&nbsp;</label>-->
                                                                                    <!--    <a class="btn btn-primary addCFnew"><i class="fa fa-plus"></i></a>-->
                                                                                    <!--</div>-->
                                                                                </div>

                                                                                <?php
                                                                                }
                                                                                else
                                                                                {

                                                                                $cnts1 = 0;
                                                                                if ($common->business_id == '6') {
                                                                                    $taxservicearray = $taxservices1;
                                                                                    // echo "<pre>"; print_r($taxservicearray);die;

                                                                                } else {
                                                                                    $taxservicearray = $taxservices;

                                                                                }

                                                                                foreach($taxservicearray as $tservice)
                                                                                {
                                                                                // echo "<pre>";print_r($tservice);
                                                                                $cnts1++;
                                                                                ?>
                                                                                <div class="form-group" style="margin-bottom:0px;">
                                                                                <?php if($cnts1 =='1'): ?>
                                                                                    <!--    <label class="control-label col-md-3" style="padding-left:0px !important;padding-right:0px !important;text-align:left !important;">Taxation Service:</label>!-->
                                                                                <?php else: ?>
                                                                                    <!--<label class="control-label col-md-3" style="text-align:left !important;padding:0px !important;"></label>!-->

                                                                                    <?php endif; ?>
                                                                                    <div class="col-md-12" style="padding-left:0px !important;">
                                                                                        <div class="row">
                                                                                            <div class="col-md-5 col-xs-12">
                                                                                                <select class="form-control disabled selectreadonly tax_service_<?php echo $cnts1;?>"
                                                                                                        name="taxation_service[]">
                                                                                                    <option value="">
                                                                                                        Select
                                                                                                    </option>
                                                                                                    <?php
                                                                                                    foreach($taxtitle as $titles)
                                                                                                    {

                                                                                                    ?>
                                                                                                    <option value="<?php echo $titles->id;?>" <?php if ($tservice->taxation_service == $titles->id) {
                                                                                                        echo 'Selected';
                                                                                                    }?>><?php echo $titles->title;?></option>
                                                                                                <?php
                                                                                                }

                                                                                                ?>
                                                                                                <!--<option value="COAM Reporting Service -3" <?php if ($tservice->taxation_service == 'COAM Reporting Service -3') {
                                                                                                    echo 'Selected';
                                                                                                }?>>COAM Reporting Service</option>
                                               <option value="Tobacco Tax Reporting Service -3" <?php if ($tservice->taxation_service == 'Tobacco Tax Reporting Service -3') {
                                                                                                    echo 'Selected';
                                                                                                }?>>Tobacco Tax Reporting Service</option>
                                               <option value="Sales Tax Reporting Service -3" <?php if ($tservice->taxation_service == 'Sales Tax Reporting Service -3') {
                                                                                                    echo 'Selected';
                                                                                                }?>>Sales Tax Reporting Service</option>
                                               <option value="Individual Income Tax Return -3" <?php if ($tservice->taxation_service == 'Individual Income Tax Return -3') {
                                                                                                    echo 'Selected';
                                                                                                }?>>Individual Income Tax Return</option>
                                                <option value="Business Income Tax Return -3" <?php if ($tservice->taxation_service == 'Business Income Tax Return -3') {
                                                                                                    echo 'Selected';
                                                                                                }?>>Business Income Tax Return</option>
                                              !-->
                                                                                                </select>
                                                                                                <?php $taxext = array();?>
                                                                                                <?php $__currentLoopData = $taxservices; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $taxexist): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                                                    <?php $taxext[] = $taxexist->taxation_service;?>
                                                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                                                <?php //print_r($taxext);?>
                                                                                                <select class="form-control showtitle tax_service_<?php echo $cnts1;?>"
                                                                                                        style="display:none;"
                                                                                                        disbaled="disabled">
                                                                                                    <option value="">
                                                                                                        Select
                                                                                                    </option>
                                                                                                    <?php
                                                                                                    foreach($taxtitle as $titles)
                                                                                                    {
                                                                                                    if(!in_array($titles->id, $taxext))
                                                                                                    {
                                                                                                    ?>
                                                                                                    <option value="<?php echo $titles->id;?>"><?php echo $titles->title;?></option>
                                                                                                    <?php
                                                                                                    }
                                                                                                    }

                                                                                                    ?>
                                                                                                </select>
                                                                                            </div>
                                                                                            <div class="col-md-2 col-xs-4 p_991"
                                                                                                 style="padding-left:5px;padding-right:0px;">
                                                                                                <?php  //echo $tservice->taxation_service_period; ?>
                                                                                                <select class="form-control disabled selectreadonly tax_period_<?php echo $cnts1;?>"
                                                                                                        name="taxation_service_period[]">
                                                                                                    <option value="">
                                                                                                        Select
                                                                                                    </option>
                                                                                                    <option value="Monthly" <?php if ($tservice->taxation_service_period == 'Monthly') {
                                                                                                        echo 'Selected';
                                                                                                    }?>>Monthly
                                                                                                    </option>
                                                                                                    <option value="Quarterly" <?php if ($tservice->taxation_service_period == 'Quarterly') {
                                                                                                        echo 'Selected';
                                                                                                    }?>>Quarterly
                                                                                                    </option>
                                                                                                    <option value="Annually" <?php if ($tservice->taxation_service_period == 'Annually') {
                                                                                                        echo 'Selected';
                                                                                                    }?>>Annually
                                                                                                    </option>
                                                                                                </select>
                                                                                                <?php if($tservice->taxation_service_period == 'Annually') {?>
                                                                                                <input type="hidden"
                                                                                                       name="tax_service_month[]"> <?php } ?>
                                                                                            </div>

                                                                                            <div class="col-md-2 col-xs-4 p_991 yearshide_<?php echo $cnts1;?>"
                                                                                                 <?php if($tservice->taxation_service_period =='Annually'): ?>    style="visibility:hidden;"
                                                                                                 <?php endif; ?>  <?php if($tservice->taxation_service_period =='Monthly'): ?>   style="display:none;" <?php endif; ?> >

                                                                                                <select class="form-control  years_<?php echo $cnts1;?>"
                                                                                                        name="taxyears[]">
                                                                                                    <option value="">
                                                                                                        Select
                                                                                                    </option>
                                                                                                    <option value="2019" <?php if ($tservice->taxyears == '2019') {
                                                                                                        echo 'selected';
                                                                                                    }?>>2019
                                                                                                    </option>
                                                                                                </select>
                                                                                            </div>
                                                                                            <?php //print_r($tservice);?>
                                                                                            <?php if($tservice->taxation_service_period != 'Annually') {?>
                                                                                            <div class="col-md-2 col-xs-4 p_991">

                                                                                                <select class="form-control disabled selectreadonly"
                                                                                                        name="tax_service_month[]">
                                                                                                    <option value="">
                                                                                                        Select
                                                                                                    </option>

                                                                                                    <option value="Jan" <?php if ($tservice->tax_service_month == 'Jan') {
                                                                                                        echo 'Selected';
                                                                                                    }?>>Jan
                                                                                                    </option>
                                                                                                    <option value="Feb" <?php if ($tservice->tax_service_month == 'Feb') {
                                                                                                        echo 'Selected';
                                                                                                    }?>>Feb
                                                                                                    </option>
                                                                                                    <option value="Mar" <?php if ($tservice->tax_service_month == 'Mar') {
                                                                                                        echo 'Selected';
                                                                                                    }?>>Mar
                                                                                                    </option>

                                                                                                    <option value="Apr" <?php if ($tservice->tax_service_month == 'Apr') {
                                                                                                        echo 'Selected';
                                                                                                    }?>>Apr
                                                                                                    </option>
                                                                                                    <option value="May" <?php if ($tservice->tax_service_month == 'May') {
                                                                                                        echo 'Selected';
                                                                                                    }?>>May
                                                                                                    </option>
                                                                                                    <option value="Jun" <?php if ($tservice->tax_service_month == 'Jun') {
                                                                                                        echo 'Selected';
                                                                                                    }?>>Jun
                                                                                                    </option>

                                                                                                    <option value="Jul" <?php if ($tservice->tax_service_month == 'Jul') {
                                                                                                        echo 'Selected';
                                                                                                    }?>>Jul
                                                                                                    </option>
                                                                                                    <option value="Aug" <?php if ($tservice->tax_service_month == 'Aug') {
                                                                                                        echo 'Selected';
                                                                                                    }?>>Aug
                                                                                                    </option>
                                                                                                    <option value="Sep" <?php if ($tservice->tax_service_month == 'Sep') {
                                                                                                        echo 'Selected';
                                                                                                    }?>>Sep
                                                                                                    </option>

                                                                                                    <option value="Oct" <?php if ($tservice->tax_service_month == 'Oct') {
                                                                                                        echo 'Selected';
                                                                                                    }?>>Oct
                                                                                                    </option>
                                                                                                    <option value="Nov" <?php if ($tservice->tax_service_month == 'Nov') {
                                                                                                        echo 'Selected';
                                                                                                    }?>>Nov
                                                                                                    </option>
                                                                                                    <option value="Dec" <?php if ($tservice->tax_service_month == 'Dec') {
                                                                                                        echo 'Selected';
                                                                                                    }?>>Dec
                                                                                                    </option>

                                                                                                </select>
                                                                                            </div>
                                                                                            <?php
                                                                                            }
                                                                                            ?>
                                                                                            <div class="col-md-2 col-xs-4 p_991"
                                                                                                 style="padding-left:0px;">

                                                                                                <select class="form-control disabled selectreadonly"
                                                                                                        name="tax_service_year[]">
                                                                                                    <option value="">
                                                                                                        Select
                                                                                                    </option>
                                                                                                    <option value="2019" <?php if ($tservice->tax_service_year == '2019') {
                                                                                                        echo 'Selected';
                                                                                                    }?>>2019
                                                                                                    </option>
                                                                                                    <option value="2020" <?php if ($tservice->tax_service_year == '2020') {
                                                                                                        echo 'Selected';
                                                                                                    }?>>2020
                                                                                                    </option>
                                                                                                    <option value="2021" <?php if ($tservice->tax_service_year == '2021') {
                                                                                                        echo 'Selected';
                                                                                                    }?>>2021
                                                                                                    </option>
                                                                                                </select>
                                                                                            </div>


                                                                                            <!--<div class="col-md-2 col-xs-4 p_991">
                                                                                                 <?php if($tservice->taxyears !=''): ?> style="display:none;" <?php endif; ?>>

                                                                                            </div>-->

                                                                                            <div class="col-md-1 col-xs-12 text-right p_991 mt10_991"
                                                                                                 style="padding-left:0px;">
                                                                                                <?php if($cnts1 == '1'){?>
                                                                                                <a class="btn btn-primary addCFnew"><i
                                                                                                            class="fa fa-plus"></i></a> <?php } else {?>

                                                                                                <a class="deletetax-row btn btn-danger"
                                                                                                   onclick="return confirm('Are you sure to remove this record?')"
                                                                                                   href="<?php echo e(route('customer.destroytax',$tservice->id)); ?>"><i
                                                                                                            class="fa fa-minus"></i></a>

                                                                                                <?php } ?>
                                                                                            </div>
                                                                                            <div class="clear"></div>
                                                                                            <hr style="margin-top: 10px;margin-bottom: 10px;">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>


                                                                                <?php

                                                                                }


                                                                                }
                                                                                ?>

                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-5 cust_right">
                                                                            <div class="customyear">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <div class="col-md-3 col-xs-4 p_991" style="padding-left:5px;">
                                                                                            <label class="control-label" style="padding-left:0px !important;padding-right:0px !important;text-align:right !important;">Final
                                                                                                Year</label>
                                                                                            <select class="form-control" name="">
                                                                                                <option value="">Select</option>
                                                                                                <option value="">2019</option>
                                                                                            </select>
                                                                                        </div>
                                                                                        <div class="col-md-9 col-sm-12 p_991" style="padding-left:5px;">
                                                                                            <label class="control-label" style="padding-left:0px !important;padding-right:0px !important;text-align:right !important;">Reason</label>
                                                                                            <textarea class="form-control" rows="1"></textarea>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <hr style="margin:0px;border-top: 5px solid #868686;">
                                                                    <div class="form-group" style="margin-bottom: 0px;">

                                                                        <div class="col-lg-7 cust_left"
                                                                             style="padding-left: 0px;">
                                                                            <div class="row">
                                                                                <div class="col-md-6 col-xs-12">
                                                                                    <label class="control-label"
                                                                                           style="padding-left:0px !important;text-align:right !important;padding-right:0px !important;">Payroll
                                                                                        Service:</label>
                                                                                    <?php if($common->payroll_period != '')
                                                                                    {
                                                                                    ?>
                                                                                    <input type="hidden"
                                                                                           name="payroll_period"
                                                                                           value="<?php echo $common->payroll_period;?>">
                                                                                    <select class="form-control"
                                                                                            disabled="disabled">
                                                                                        <option value="">Select</option>
                                                                                        <?php $__currentLoopData = $payrollservice; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $payroll): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                                                                            <option value="<?php echo e($payroll->id); ?>"
                                                                                                    <?php if($common->payroll_period == $payroll->id ): ?> selected <?php endif; ?>><?php echo e($payroll->servicename); ?></option>
                                                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                                    </select>
                                                                                    <?php
                                                                                    }
                                                                                    else
                                                                                    {


                                                                                    ?>
                                                                                    <select class="form-control payroll_period"
                                                                                            name="payroll_period">
                                                                                        <option value="">Select</option>
                                                                                        <?php $__currentLoopData = $payrollservice; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $payroll): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                                                                            <option value="<?php echo e($payroll->id); ?>"
                                                                                                    <?php if($common->payroll_period == $payroll->id ): ?> selected <?php endif; ?>><?php echo e($payroll->servicename); ?></option>
                                                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                                                                    </select>
                                                                                    <?php
                                                                                    }
                                                                                    ?>
                                                                                </div>
                                                                                <div class="col-md-3 p_991 col-xs-6"
                                                                                     style="padding-left:5px;">
                                                                                    <label class="control-label"
                                                                                           style="padding-left:0px !important;padding-right:0px !important;text-align:right !important;">
                                                                                        Month</label>
                                                                                    <select class="form-control"
                                                                                            name="payroll_service_month">
                                                                                        <option value=""> Select
                                                                                        </option>
                                                                                        <option value="Jan" <?php if ($common->payroll_service_month == 'Jan') {
                                                                                            echo 'Selected';
                                                                                        }?>>Jan
                                                                                        </option>
                                                                                        <option value="Feb" <?php if ($common->payroll_service_month == 'Feb') {
                                                                                            echo 'Selected';
                                                                                        }?>>Feb
                                                                                        </option>
                                                                                        <option value="Mar" <?php if ($common->payroll_service_month == 'Mar') {
                                                                                            echo 'Selected';
                                                                                        }?>>Mar
                                                                                        </option>
                                                                                        <option value="Apr" <?php if ($common->payroll_service_month == 'Apr') {
                                                                                            echo 'Selected';
                                                                                        }?>>Apr
                                                                                        </option>
                                                                                        <option value="May" <?php if ($common->payroll_service_month == 'May') {
                                                                                            echo 'Selected';
                                                                                        }?>>May
                                                                                        </option>
                                                                                        <option value="Jun" <?php if ($common->payroll_service_month == 'Jun') {
                                                                                            echo 'Selected';
                                                                                        }?>>Jun
                                                                                        </option>
                                                                                        <option value="Jul" <?php if ($common->payroll_service_month == 'Jul') {
                                                                                            echo 'Selected';
                                                                                        }?>>Jul
                                                                                        </option>
                                                                                        <option value="Aug" <?php if ($common->payroll_service_month == 'Aug') {
                                                                                            echo 'Selected';
                                                                                        }?>>Aug
                                                                                        </option>
                                                                                        <option value="Sep" <?php if ($common->payroll_service_month == 'Sep') {
                                                                                            echo 'Selected';
                                                                                        }?>>Sep
                                                                                        </option>
                                                                                        <option value="Oct" <?php if ($common->payroll_service_month == 'Oct') {
                                                                                            echo 'Selected';
                                                                                        }?>>Oct
                                                                                        </option>
                                                                                        <option value="Nov" <?php if ($common->payroll_service_month == 'Nov') {
                                                                                            echo 'Selected';
                                                                                        }?>>Nov
                                                                                        </option>
                                                                                        <option value="Dec" <?php if ($common->payroll_service_month == 'Dec') {
                                                                                            echo 'Selected';
                                                                                        }?>>Dec
                                                                                        </option>

                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-md-3 p_991 col-xs-6"
                                                                                     style="padding-left:0px;">
                                                                                    <label class="control-label"
                                                                                           style="padding-left:0px !important;padding-right:0px !important;text-align:right !important;">
                                                                                        Year</label>
                                                                                    <select class="form-control"
                                                                                            name="payroll_service_year">
                                                                                        <option value=""> Select
                                                                                        </option>
                                                                                        <option value="2019" <?php if ($common->payroll_service_year == '2019') {
                                                                                            echo 'Selected';
                                                                                        }?>>2019
                                                                                        </option>
                                                                                        <option value="2020" <?php if ($common->payroll_service_year == '2020') {
                                                                                            echo 'Selected';
                                                                                        }?>>2020
                                                                                        </option>
                                                                                        <option value="2021" <?php if ($common->payroll_service_year == '2021') {
                                                                                            echo 'Selected';
                                                                                        }?>>2021
                                                                                        </option>

                                                                                    </select>
                                                                                </div>

                                                                            </div>

                                                                        </div>
                                                                        <div class="col-lg-5 cust_right"></div>
                                                                    </div>
                                                                    <hr style="margin:0px;border-top: 5px solid #868686;">
                                                                <?php endif; ?>

                                                                <?php if($common->business_id !='6'): ?>
                                                                    <div class="form-group" style="margin-bottom:0px;">
                                                                        <div class="col-lg-7 cust_left"
                                                                             style="padding-left: 0px;">
                                                                            <label class="control-label"
                                                                                   style="text-align:left!important; padding-left:0px;">Other
                                                                                Service:</label>
                                                                            <div class="" style="padding-left:0px;">
                                                                                <input type="text" name="other_period"
                                                                                       class="form-control"
                                                                                       value="<?php if (isset($common->other_period) && $common->other_period != '') {
                                                                                           echo $common->other_period;
                                                                                       }?>"/>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <?php endif; ?>

                                                                <?php if($common->business_id !='6'): ?>
                                                                    <div class="form-group">
                                                                        <div class="col-lg-7 cust_left"
                                                                             style="padding-left: 0px;">
                                                                            <div id="customFieldsbo">
                                                                                <label class="control-label"
                                                                                       style="text-align:left!important; padding-left:0px; ">Note:</label>
                                                                                <div style="padding-left:0px;">
                                                                                    <input type="text"
                                                                                           name="other_period"
                                                                                           class="form-control"
                                                                                           value=""/>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <?php endif; ?>
                                                            </div>
                                                        <?php endif; ?>


                                                    </div>


                                                </div>

                                                <div class="clear"></div>
                                                <div class="Branch" style="margin-top:20px;">
                                                    <h1>Pricing information</h1>
                                                </div>
                                                <div class="clear"></div>
                                                <div class="customer-service-information-bg">
                                                    <?php
                                                    $taxservices2 = count($taxservices);
                                                    $clientser2 = count($clientser);
                                                    $taxclientser2 = count($taxclientser);
                                                    ?>
                                                    <?php if($common->accounting_period == '')
                                                    {?>
                                                    <div class="services2" style="display:none;"
                                                         disabled="disabled">
                                                        <div class="col-md-3">
                                                            <label class="control-label">Type of Service :</label>
                                                            <input type="text" class="form-control" readonly
                                                                   value="Accounting Service">
                                                            <input type="hidden" name="typeofservice[]" value="2">
                                                            <input type="hidden" name="acperiods1[]" class="acperiods1">


                                                            <input type="hidden" class="titletaxx1"
                                                                   name="typeofservice1[]">


                                                        </div>
                                                        <div id="regular">
                                                            <div class="col-md-4 pi_22">
                                                                <div class="row">
                                                                    <label class="control-label">Service Includes
                                                                        :</label>
                                                                    <textarea readonly cols="50" row="5"
                                                                              name="serviceincludes[]"
                                                                              id="serviceincludes"
                                                                              class="form-control serviceincludeacc">
                                                                        
                                                                    </textarea>
                                                                    <input name="serviceid[]" type="hidden"
                                                                           id="serviceid" class="form-control">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2 pi_333">
                                                                <label class=" readonly control-label">Regular Price
                                                                    :</label>
                                                                <input name="regularprice[]" type="text"
                                                                       id="regularprice"
                                                                       style="text-align:right;font-size:small !important"
                                                                       placeholder="Regular Price"
                                                                       readonly class="regularpriceacc form-control">
                                                            </div>
                                                            <div class="col-md-2 pi_44">
                                                                <div class="row">
                                                                    <label class="control-label">Combo Price :</label>
                                                                    <input name="comboprice[]" type="text"
                                                                           id="comboprice"
                                                                           style="text-align:right;font-size:small !important"
                                                                           placeholder="Combo Price" readonly
                                                                           class="combopriceacc form-control">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2 pi_55">
                                                                <label class="control-label">Your Price :</label>
                                                                <input readonly name="price[]" type="text" id="price"
                                                                       style="text-align:right;font-size:small !important"
                                                                       placeholder="Price"
                                                                       class="form-control priceacc">
                                                            </div>
                                                        </div>
                                                        <div style="clear:both;"></div>
                                                    </div>
                                                    <div style="clear:both;"></div>
                                                    <?php
                                                    }
                                                    ?>

                                                    <?php if($common->payroll_period == '')
                                                    {
                                                    //  exit('22222222222');
                                                    ?>

                                                    <div class="services3" style="display:none;" disabled="disabled">
                                                        <div class="col-md-3 pi_11">
                                                            <label class="control-label">Type of Service1 :</label>
                                                            <input type="text" class="form-control disc" readonly
                                                                   value="Payroll Service">
                                                            <input type="hidden" name="typeofservice[]" value="8"
                                                                   class="disc">
                                                            <input type="hidden" class="titletaxx1 disc"
                                                                   name="typeofservice1[]">
                                                            <!--<input type="hidden" name="acperiods1[]" class="acperiods2">
        !-->


                                                        </div>
                                                        <div id="regular">
                                                            <div class="col-md-4 pi_22">
                                                                <div class="row">
                                                                    <label class="control-label">Service Includes
                                                                        :</label>
                                                                    <textarea readonly rows="4" cols="50"
                                                                              name="serviceincludes[]"
                                                                              id="serviceincludes"
                                                                              class="form-control serviceincludepay disc"
                                                                              style="height:100px!important;"></textarea>
                                                                    <input name="serviceid[]" type="hidden"
                                                                           id="serviceid" class="form-control disc">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2 pi_33">
                                                                <label class=" readonly control-label">Regular Price
                                                                    :</label>
                                                                <input name="regularprice[]" type="text"
                                                                       id="regularprice"
                                                                       style="text-align:right;font-size:small !important"
                                                                       placeholder="Regular Price"
                                                                       readonly
                                                                       class="regularpricepay form-control disc">
                                                            </div>
                                                            <div class="col-md-2 pi_44">
                                                                <div class="row">
                                                                    <label class="control-label">Combo Price :</label>
                                                                    <input name="comboprice[]" type="text"
                                                                           id="comboprice"
                                                                           style="text-align:right;font-size:small !important"
                                                                           placeholder="Combo Price" readonly
                                                                           class="combopricepay form-control disc">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2 pi_55">
                                                                <label class="control-label">Your Price :</label>
                                                                <input readonly name="price[]" type="text" id="price"
                                                                       style="text-align:right;font-size:small !important"
                                                                       placeholder="Price"
                                                                       class="form-control pricepay disc">
                                                            </div>
                                                        </div>
                                                        <div class="clear"></div>
                                                    </div>
                                                    <?php
                                                    }
                                                    ?>


                                                    <?php if($clientser2 !=NULL): ?>
                                                        <?php $sum = 0;?>
                                                        <?php $__currentLoopData = $clientser; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $clientser1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php
                                                            if (isset($clientser1->price) && $clientser1->price != '') {
                                                                //     echo $clientser1->price;exit;
                                                                $strr = str_replace(',', '', $clientser1->price);
                                                                $sum += str_replace('$ ', '', $strr);
                                                            }
                                                            ?>
                                                            <style>.service {
                                                                    display: block !important
                                                                }</style>
                                                            <div class="form-group ser" id="ser_<?php echo e($clientser1->id); ?>">
                                                                <div class="">
                                                                    <div class="col-md-3 pi_11">
                                                                        <label class="control-label">Type of Service
                                                                            :</label>

                                                                        <input name="serviceid[]" type="hidden"
                                                                               id="serviceid" class="form-control"
                                                                               value="<?php echo e($clientser1->id); ?>">
                                                                        <?php if($clientser1->typeofservice =='2' || $clientser1->typeofservice =='8'): ?>
                                                                            <input type="hidden" name="acperiods1[]"
                                                                                   value="<?php echo e($clientser1->periods); ?>">

                                                                            <select name="typeofservice[]"
                                                                                    id="typeofservice_<?php echo e($clientser1->id); ?>"
                                                                                    class="form-control clock"
                                                                                    <?php if(!empty($clientser1->typeofservice)): ?> style="pointer-events: none;background: lightgreen none repeat scroll 0% 0%;" <?php endif; ?>>
                                                                                <option value="">Type of Service
                                                                                </option>
                                                                                <?php $__currentLoopData = $typeofser; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $typeofser1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                                    <option value="<?php echo e($typeofser1->id); ?>"
                                                                                            <?php if($clientser1->typeofservice==$typeofser1->id): ?> selected <?php endif; ?>><?php echo e($typeofser1->typeofservice); ?></option>
                                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                            </select>
                                                                        <?php else: ?>

                                                                            <input type="hidden" name="typeofservice[]"
                                                                                   id="typeofservice_<?php echo e($clientser1->id); ?>"
                                                                                   value="3">
                                                                            <input type="hidden"
                                                                                   id="typeofservice1_<?php echo e($clientser1->typeofservice1); ?>"
                                                                                   value="3">
                                                                            <input readonly name="typeofservice1[]"
                                                                                   type="hidden" class="form-control"
                                                                                   <?php $__currentLoopData = $taxtitle; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $taxtitle1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  <?php if($taxtitle1->id==$clientser1->typeofservice1): ?> value="<?php echo e($taxtitle1->id); ?>" <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>>

                                                                            <input readonly type="text"
                                                                                   class="form-control"
                                                                                   <?php $__currentLoopData = $taxtitle; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $taxtitle1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  <?php if($taxtitle1->id==$clientser1->typeofservice1): ?> value="<?php echo e($taxtitle1->title); ?>" <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>>
                                                                        <?php endif; ?>

                                                                    </div>
                                                                    <div id="regular">
                                                                        <div class="col-md-4 pi_22">
                                                                            <div class="row">
                                                                                <label class="control-label">Service
                                                                                    Includes :</label>
                                                                                <textarea readonly cols="50"
                                                                                          name="serviceincludes[]"
                                                                                          id="serviceincludes_<?php echo e($clientser1->id); ?>"
                                                                                          class="form-control-1"><?php echo e($clientser1->serviceincludes); ?></textarea>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-2 pi_33">
                                                                            <label class="control-label">Regular Price
                                                                                :</label>
                                                                            <input name="regularprice[]"
                                                                                   style="text-align:right;font-size:small !important;"
                                                                                   type="text"
                                                                                   id="regularprice_<?php echo e($clientser1->id); ?>"
                                                                                   placeholder="Regular Price"
                                                                                   value="<?php echo e($clientser1->regularprice); ?>"
                                                                                   readonly
                                                                                   class="regularprice_<?php echo e($clientser1->id); ?> form-control">
                                                                        </div>
                                                                        <div class="col-md-2 pi_44">
                                                                            <div class="row">
                                                                                <label class="control-label"
                                                                                       style="width: 130px;">Combo Price
                                                                                    :</label>
                                                                                <input name="comboprice[]"
                                                                                       style="text-align:right;font-size:small !important"
                                                                                       type="text"
                                                                                       id="comboprice_<?php echo e($clientser1->id); ?>"
                                                                                       placeholder="Combo Price"
                                                                                       value="<?php echo e($clientser1->comboprice); ?>"
                                                                                       readonly
                                                                                       class="comboprice_<?php echo e($clientser1->id); ?> form-control">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-2 pi_55">
                                                                            <label class="control-label">Your Price
                                                                                :</label>
                                                                            <input readonly name="price[]"
                                                                                   style="text-align:right" type="text"
                                                                                   id="price3_<?php echo e($clientser1->id); ?>"
                                                                                   placeholder="Your Price"
                                                                                   value="<?php echo e($clientser1->price); ?>"
                                                                                   class="form-control price">
                                                                        </div>
                                                                        <a class="delete1 btn-action btn-delete"
                                                                           id="<?php echo e($clientser1->id); ?>"
                                                                           style="float: right;width: 0px;margin: 28px 0 0 0px;color: red;position: relative;right:47px;"><i
                                                                                    class="fa fa-trash"></i></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <?php endif; ?>


                                                    <?php if($taxclientser2 !=NULL): ?>
                                                        <?php $sums = 0;?>
                                                        <?php $__currentLoopData = $taxclientser; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $taxclientser1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php
                                                            //echo $taxclientser1->taxregularprice;exit;
                                                            if (isset($taxclientser1->pricetype) && $taxclientser1->pricetype == 'Combo') {
                                                                if (isset($taxclientser1->taxcomboprice) && ($taxclientser1->taxcomboprice != '' || $taxclientser1->taxcomboprice != 'Note')) {
                                                                    if ($taxclientser1->taxcomboprice != 'Note') {
                                                                        $sums += str_replace('$ ', '', $taxclientser1->taxcomboprice);
                                                                    }

                                                                }
                                                            } else if (isset($taxclientser1->pricetype) && $taxclientser1->pricetype == 'Regular') {
                                                                if (isset($taxclientser1->taxregularprice) && $taxclientser1->taxregularprice != 'Note') {
                                                                    // exit('22');
                                                                    $sums += str_replace('$ ', '', $taxclientser1->taxregularprice);

                                                                }

                                                            }
                                                            ?>
                                                            <style>.service {
                                                                    display: block !important
                                                                }</style>
                                                            <div class="form-group ser " id="ser_<?php echo e($clientser1->id); ?>">
                                                                <div class="">
                                                                    <div class="col-md-3 pi_11">
                                                                        <label class="control-label">Type of Service
                                                                            :</label>

                                                                        <input name="taxserviceid[]" type="hidden"
                                                                               id="taxserviceid" class="form-control"
                                                                               value="<?php echo e($taxclientser1->id); ?>">

                                                                        <input readonly type="text" class="form-control"
                                                                               <?php $__currentLoopData = $taxtitle; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $taxtitle1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  <?php if($taxtitle1->id==$taxclientser1->titletax): ?> value="<?php echo e($taxtitle1->title); ?>" <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                        ">

                                                                        <input readonly type="hidden" name="titletax[]"
                                                                               class="ttax_<?php echo e($taxclientser1->id); ?>"
                                                                               value="<?php echo e($taxclientser1->titletax); ?>">
                                                                    </div>
                                                                    <div id="regular">
                                                                        <div class="col-md-4 pi_22">
                                                                            <div class="row">
                                                                                <label class="control-label">Note
                                                                                    :</label>
                                                                                <textarea readonly cols="50"
                                                                                          name="taxserviceincludes[]"
                                                                                          id="taxserviceincludes_<?php echo e($clientser1->id); ?>"
                                                                                          class="form-control-1"><?php echo e($taxclientser1->taxserviceincludes); ?></textarea>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-2 pi_33">
                                                                            <label class="control-label">Regular Price
                                                                                :</label>
                                                                            <input name="taxregularprice[]"
                                                                                   style="text-align:right;font-size:small !important;"
                                                                                   type="text"
                                                                                   id="regularprice_<?php echo e($taxclientser1->id); ?>"
                                                                                   placeholder="Regular Price"
                                                                                   value="<?php echo e($taxclientser1->taxregularprice); ?>"
                                                                                   readonly
                                                                                   class="taxregularprice_<?php echo e($taxclientser1->id); ?> form-control">
                                                                        </div>
                                                                        <div class="col-md-2 pi_44">
                                                                            <div class="row">
                                                                                <label class="control-label"
                                                                                       style="width: 130px;">Combo Price
                                                                                    :</label>
                                                                                <input name="taxcomboprice[]"
                                                                                       style="text-align:right;font-size:small !important"
                                                                                       type="text"
                                                                                       id="comboprice_<?php echo e($taxclientser1->id); ?>"
                                                                                       placeholder="Combo Price"
                                                                                       value="<?php echo e($taxclientser1->taxcomboprice); ?>"
                                                                                       readonly
                                                                                       class="taxcomboprice_<?php echo e($taxclientser1->id); ?> form-control">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-2 pi_55">
                                                                            <label class="control-label">Your Price
                                                                                :</label>
                                                                            <input readonly name="taxprice1[]"
                                                                                   style="text-align:right" type="text"
                                                                                   id="price3_<?php echo e($taxclientser1->id); ?>"
                                                                                   placeholder="Your Price"
                                                                                   value="<?php echo e($taxclientser1->taxprice1); ?>"
                                                                                   class="form-control taxprice">
                                                                        </div>
                                                                        <a class="taxdelete1 btn-action btn-delete"
                                                                           id="<?php echo e($taxclientser1->id); ?>"
                                                                           style="float: right;width: 0px;margin: 28px 0 0 0px;color: red;position: relative;right:47px;"><i
                                                                                    class="fa fa-trash"></i></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <?php endif; ?>



                                                    <?php

                                                    if($taxservices2 == '0')
                                                    {
                                                    // echo 'x'.$taxservices2;
                                                    // exit('11');
                                                    ?>
                                                    <div class="services4" style="display:none;" disabled="disabled">
                                                        <div class="col-md-3 pi_11">
                                                            <label class="control-label">Type of Service :</label>
                                                            <input type="text" class="form-control taxtitletax"
                                                                   name="taxtypeofservice[]" readonly>
                                                            <input type="hidden" class="form-control titletax"
                                                                   name="titletax[]" id="taxtitle" readonly>


                                                        </div>
                                                        <div id="regular">
                                                            <div class="col-md-4 pi_22">
                                                                <div class="row">
                                                                    <label class="control-label">Note :</label>
                                                                    <textarea readonly rows="4" cols="50"
                                                                              name="taxserviceincludes[]"
                                                                              id="taxserviceincludes"
                                                                              class="form-control taxserviceincludenote"
                                                                              style="height:100px!important;"></textarea>
                                                                    <input name="serviceid[]" type="hidden"
                                                                           id="serviceid" class="form-control">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2 pi_33">
                                                                <label class=" readonly control-label">Regular Price
                                                                    :</label>
                                                                <input name="taxregularprice[]" type="text"
                                                                       id="taxregularprice"
                                                                       style="text-align:right;font-size:small !important"
                                                                       placeholder="Regular Price"
                                                                       readonly class="taxregularpricetax form-control">
                                                            </div>
                                                            <div class="col-md-2 pi_44">
                                                                <div class="row">
                                                                    <label class="control-label">Combo Price :</label>
                                                                    <input name="taxcomboprice[]" type="text"
                                                                           id="taxcomboprice"
                                                                           style="text-align:right;font-size:small !important"
                                                                           placeholder="Combo Price" readonly
                                                                           class="taxcombopricetax form-control">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2 pi_55">
                                                                <label class="control-label">Your Price :</label>
                                                                <input readonly name="taxprice1[]" type="text"
                                                                       id="taxprice1"
                                                                       style="text-align:right;font-size:small !important"
                                                                       placeholder="Price"
                                                                       class="form-control taxpricetax">
                                                            </div>
                                                        </div>
                                                        <div class="clear"></div>
                                                    </div>
                                                    <?php
                                                    }
                                                    ?>
                                                    <?php
                                                    if($taxservices2 > '0')
                                                    {
                                                    // echo  'y'.$taxservices2;
                                                    //  exit('22');
                                                    ?>
                                                    <div class="services4 ser4" style="display:none;"
                                                         disabled="disabled">
                                                        <!-- <div class="col-md-3">
           <label class="control-label">Type of Serviceww :</label>
         <input type="text" class="form-control taxtitletax"  name="taxtypeofservice[]" readonly >
         <input type="hidden" class="form-control titletax"  name="titletax[]" id="taxtitle" readonly >
         
         
           </div>
           <div id="regular">
           <div class="col-md-4" style="width:39% !important"> <div class="row">
           <label class="control-label">Note :</label>
           <textarea readonly rows="4" cols="50" name="taxserviceincludes[]" id="taxserviceincludes" class="form-control taxserviceincludenote" style="height:100px!important;"></textarea>
           <input name="serviceid[]" type="hidden" id="serviceid" class="form-control">
           </div>
           </div>
           <div class="col-md-2" style="width:13%">
           <label class=" readonly control-label">Regular Price :</label>
           <input name="taxregularprice[]" type="text" id="taxregularprice" style="text-align:right;font-size:small !important" placeholder="Regular Price"  
           readonly class="taxregularpricetax form-control">
           </div>
           <div class="col-md-2" style="width:11%">
           <div class="row">
           <label class="control-label">Combo Price :</label>
           <input name="taxcomboprice[]" type="text" id="taxcomboprice" style="text-align:right;font-size:small !important" 
           placeholder="Combo Price" readonly class="taxcombopricetax form-control">
           </div>
           </div>
           <div class="col-md-2" style="width:11%">
           <label class="control-label">Your Price :</label>
           <input readonly name="taxprice1[]" type="text" id="taxprice1" 
           style="text-align:right;font-size:small !important" placeholder="Price"  class="form-control taxpricetax">
           </div>
           </div>  
           <div class="clear"></div>-->
                                                    </div>
                                                    <?php
                                                    }
                                                    ?>
                                                </div>

                                                <div class="education_service" id="education_service"></div>
                                                <div class="service col-md-12"
                                                     style="background-color:#eeeeed;padding: 15px 0px 23px 0px;width: 100%;">
                                                    <div class="">
                                                        <div class="col-md-7" style="padding-right:0px;">
                                                            <label style="">Note :</label>
                                                        <!--<input type="text" class="form-control" style=width:100%;margin-left:-2.35% placeholder="Note" name="pricenote" <?php if(empty($clientser5->note)): ?> <?php else: ?> value="<?php echo e($clientser5->note); ?>" <?php endif; ?>>-->
                                                            <textarea class="form-control-2"
                                                                      style="width:100%;min-height: 60px;" rows="2"
                                                                      name="pricenote"
                                                                      <?php if(empty($clientser5->note)): ?> <?php else: ?> value="<?php echo e($clientser5->note); ?>" <?php endif; ?>></textarea>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div class="row">
                                                                <div class="col-md-6 text-right" style="width:57%">
                                                                    <label style="padding-top:9px;">Discount :</label>
                                                                </div>
                                                                <div class="col-md-4"
                                                                     style="padding-right:0px; text-align:right; width:28%;">
                                                                    <input type="text"
                                                                           style="text-align:right;font-size:small !important; float:right;"
                                                                           class="form-control discountprice"
                                                                           placeholder="Discount Price"
                                                                           id="discountprice"
                                                                           <?php if(empty($clientser5->discountprice)): ?> <?php else: ?> value="<?php echo e($clientser5->discountprice); ?>"
                                                                           <?php endif; ?> name="discountprice">
                                                                    <input type="hidden" name="totall" id="totall">
                                                                </div>
                                                                <div class="col-md-1">&nbsp;</div>
                                                            </div>
                                                            <div class="row" style="margin-top:10px;">
                                                                <div class="col-md-6 text-right" style="width:57%">
                                                                    <label style="padding-top:9px;">Total Price
                                                                        :</label>
                                                                </div>
                                                                <div class="col-md-4"
                                                                     style="padding-right:0px; text-align:right; width:28%;">
                                                                    <?php
                                                                    if (isset($sum) && isset($sums)) {
                                                                        $totalsum = $sum + $sums;
                                                                    } else if (isset($sum)) {
                                                                        $totalsum = $sum;
                                                                    } else if (isset($sums)) {
                                                                        $totalsum = $sums;
                                                                    } else {
                                                                        $totalsum = '0';
                                                                    }


                                                                    ?>
                                                                    <?php if (isset($sums)) {
                                                                        $sum2 = $sums;
                                                                    }?>

                                                                    <?php //$totalsum=$sum1+$sum2;?>
                                                                    <input type="hidden"
                                                                           class="form-control totalprice1"
                                                                           id="totalpri" placeholder="Total Price"
                                                                           readonly name="totalprice"
                                                                           value="<?php echo '$ ' . number_format($totalsum, 2);?>"
                                                                           style="background:#fff2b3!important">
                                                                    <input type="text" class="form-control totalprice1"
                                                                           style="text-align:right;font-size:small !important; float:right; background:#fff2b3!important;"
                                                                           placeholder="Total Price" readonly
                                                                           name="totalprice1" id="totalprice1"
                                                                           value="<?php echo '$ ' . number_format($totalsum, 2);?>">
                                                                </div>
                                                                <div class="col-md-1">&nbsp;</div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <!-- <div class="col-md-12"><br>
           <a class="btn btn-primary pull-right" onclick="education_service();" style="margin-right:49px;"> Add</a>
           </div>!-->
                                                </div>
                                            </div>


                                            <div class="tab-pane fade" id="tab8primary">Coming Soon1</div>

                                            <div class="tab-pane fade" id="tab9primary">Coming Soon2</div>

                                            <div class="tab-pane fade" id="tab2primary">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                </div>
                                            </div>

                                            <div class="tab-pane fade" id="tab4primary">
                                                <div class="input_fields_wrap_text">
                                                    <div class="Branch">
                                                        <h1>Primary Contact Information</h1>
                                                    </div>
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Name :</label>
                                                            <div class="col-lg-7 col-md-8">
                                                                <div class="row">
                                                                    <div class="col-md-2 col-xs-4 p_cmn_991"
                                                                         style="padding-right:0px;">
                                                                        <select type="text" class="form-control txtOnly"
                                                                                id="contactnametype"
                                                                                name="contactnametype">
                                                                            <option value="Mr."
                                                                                    <?php if($common->contactnametype=='Mr.'): ?> selected="" <?php endif; ?>>
                                                                                Mr.
                                                                            </option>
                                                                            <option value="Mrs."
                                                                                    <?php if($common->contactnametype=='Mrs.'): ?> selected="" <?php endif; ?>>
                                                                                Mrs.
                                                                            </option>
                                                                            <option value="Miss."
                                                                                    <?php if($common->contactnametype=='Miss.'): ?> selected="" <?php endif; ?>>
                                                                                Miss.
                                                                            </option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-md-4 col-xs-8">
                                                                        <input type="text" class="form-control "
                                                                               id="firstname" name="firstname"
                                                                               value="<?php echo e($common->firstname); ?>">
                                                                    </div>
                                                                    <div class="col-md-2 col-xs-4 p_cmn_991"
                                                                         style="padding:0px;">
                                                                        <div class="">
                                                                            <input type="text" class="form-control "
                                                                                   id="middlename" name="middlename"
                                                                                   maxlength="1"
                                                                                   value="<?php echo e($common->middlename); ?>">
                                                                        </div>
                                                                    </div>
                                                                    <div class="">
                                                                        <div class="col-md-4 col-xs-8">
                                                                            <input type="text" class="form-control "
                                                                                   id="lastname" name="lastname"
                                                                                   value="<?php echo e($common->lastname); ?>">
                                                                        </div>
                                                                    </div>
                                                                </div>


                                                            </div>

                                                            <!--<a href="javascript:void(0)" class="btn btn-success addmore"><span style="margin-right: 0px;" class="glyphicon glyphicon glyphicon-plus" aria-hidden="true"></span></a>-->

                                                            <!--<input type="button" class="add-row btn btn-primary" value="+">-->

                                                            <input type="button"
                                                                   class="add-row btn btn-primary add_sec_info"
                                                                   value="+"></span>

                                                            <!--<button type="button" onclick="displayResult()">Insert new row</button>-->

                                                            <!--<input type="button" class="btn btn-primary" onclick="displayResult()" value="+"></span>-->

                                                        </div>
                                                        <div class="form-group" style="margin-bottom:0px;">
                                                            <label class="control-label col-md-3 hide_991"></label>
                                                            <div class="col-md-8">
                                                                <input type="checkbox" class="check" value="1"
                                                                       id="faxbli3" name="faxbli3"
                                                                       onclick="faxbli233(this.form)"
                                                                       <?php if(empty($common->faxbli3)): ?> <?php else: ?> checked <?php endif; ?>>
                                                                <label for="faxbli3"> Same
                                                                    as <?php if($common->business_id=='6'): ?> <?php else: ?>
                                                                        business <?php endif; ?> address</label>
                                                            </div>
                                                        </div>

                                                        <?php if($common->business_id=='6'): ?>  <?php else: ?>
                                                            <div class="form-group ">
                                                                <label class="control-label col-md-3">Title :</label>
                                                                <div class="col-lg-7 col-md-8">
                                                                    <div class="row">
                                                                        <div class="col-md-5">
                                                                            <input type="text" class="form-control"
                                                                                   id="contact_title"
                                                                                   name="contact_title"
                                                                                   value="<?php echo e($common->contact_title); ?>">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php endif; ?>

                                                        <div class="form-group ">
                                                            <label class="control-label col-md-3">Address 1 :</label>
                                                            <div class="col-lg-7 col-md-8">
                                                                <input type="text" class="form-control"
                                                                       id="contact_address1" name="contact_address1"
                                                                       value="<?php echo e($common->contact_address1); ?>">
                                                            </div>
                                                        </div>
                                                        <div class="form-group ">
                                                            <label class="control-label col-md-3">Address 2 :</label>
                                                            <div class="col-lg-7 col-md-8">
                                                                <input type="text" class="form-control"
                                                                       id="contact_address2" name="contact_address2"
                                                                       value="<?php echo e($common->contact_address2); ?>">
                                                            </div>
                                                        </div>
                                                        <div class="form-group ">
                                                            <label class="control-label col-md-3">City / State / Zip
                                                                :</label>
                                                            <div class="col-lg-7 col-md-8">
                                                                <div class="row">
                                                                    <div class="col-md-5">
                                                                        <input name="city_1" value="<?php echo e($common->city_1); ?>"
                                                                               type="text" id="city_1"
                                                                               class="form-control">
                                                                    </div>
                                                                    <div class="">
                                                                        <div class="col-md-3 col-xs-6">
                                                                            <select name="state_1" id="stateId1"
                                                                                    class="form-control fsc-input bfh-states"
                                                                                    data-country="countries_states1"
                                                                                    data-state="<?php echo e($common->state_1); ?>">
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="">
                                                                        <div class="col-md-4 col-xs-6">
                                                                            <input name="zip_1"
                                                                                   value="<?php echo e($common->zip_1); ?>"
                                                                                   type="text" id="zip_1"
                                                                                   class="form-control zip">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group" style="margin-bottom:0px;">
                                                            <label class="control-label col-md-3 hide_991"></label>
                                                            <div class="col-md-9">
                                                                <input type="checkbox" value="1" id="faxbli3_g"
                                                                       name="faxbli3_g" onclick="faxbli233_g(this.form)"
                                                                       <?php if(empty($common->faxbli3_g)): ?> <?php else: ?> checked <?php endif; ?>><label
                                                                        for="faxbli3_g"> Same As Basic Telephone</label>
                                                            </div>
                                                        </div>

                                                        <div class="form-group ">
                                                            <label class="control-label col-md-3">Telephone No.
                                                                1:</label>
                                                            <div class="col-lg-7 col-md-8">
                                                                <div class="row">
                                                                    <div class="col-md-5">
                                                                        <input name="etelephone1"
                                                                               placeholder="(999) 999-9999"
                                                                               value="<?php if (empty($common->faxbli3_g)) {
                                                                                   echo $common->etelephone1;
                                                                               } else {
                                                                                   echo $common->business_no;
                                                                               }?>" type="tel" id="mobile_sec_1"
                                                                               class="form-control bfh-phone"
                                                                               data-country="countries_states1"
                                                                               data-format="  (999) 999-9999">
                                                                    </div>
                                                                    <div class="col-md-3 col-xs-6 ">
                                                                        <select name="mobiletype_1" id="mobiletype_1"
                                                                                class="form-control fsc-input"
                                                                                style="height:auto">
                                                                            <option value="">Select</option>
                                                                            <option value="Business" <?php if (empty($common->faxbli3_g)) {
                                                                                if ($common->eteletype1 == 'Business') {
                                                                                    echo 'selected';
                                                                                }
                                                                            } else {
                                                                                if ($common->businesstype == 'Business') {
                                                                                    echo 'selected';
                                                                                }
                                                                            }?>>Business
                                                                            </option>
                                                                            <option value="Office" <?php if (empty($common->faxbli3_g)) {
                                                                                if ($common->eteletype1 == 'Office') {
                                                                                    echo 'selected';
                                                                                }
                                                                            } else {
                                                                                if ($common->businesstype == 'Office') {
                                                                                    echo 'selected';
                                                                                }
                                                                            }?>>Office
                                                                            </option>
                                                                            <option value="Resid" <?php if (empty($common->faxbli3_g)) {
                                                                                if ($common->eteletype1 == 'Resid') {
                                                                                    echo 'selected';
                                                                                }
                                                                            } else {
                                                                                if ($common->businesstype == 'Resid') {
                                                                                    echo 'selected';
                                                                                }
                                                                            }?>>Resid
                                                                            </option>
                                                                            <option value="Mobile" <?php if (empty($common->faxbli3_g)) {
                                                                                if ($common->eteletype1 == 'Mobile') {
                                                                                    echo 'selected';
                                                                                }
                                                                            } else {
                                                                                if ($common->businesstype == 'Mobile') {
                                                                                    echo 'selected';
                                                                                }
                                                                            }?>>Mobile
                                                                            </option>
                                                                            <option value="Other" <?php if (empty($common->faxbli3_g)) {
                                                                                if ($common->eteletype1 == 'Other') {
                                                                                    echo 'selected';
                                                                                }
                                                                            } else {
                                                                                if ($common->businesstype == 'Other') {
                                                                                    echo 'selected';
                                                                                }
                                                                            }?>>Other
                                                                            </option>

                                                                        </select>
                                                                    </div>
                                                                    <div class="col-md-4 col-xs-6"
                                                                         <?php if($common->eteletype1=='Mobile'): ?> style="display:none;" <?php endif; ?>>
                                                                        <input class="form-control fsc-input zip"
                                                                               id="ext2_1" name="ext2_1" maxlength="5"
                                                                               <?php if($common->eteletype1=='Business'): ?> <?php elseif($common->eteletype1=='Home'): ?> <?php else: ?> readonly
                                                                               <?php endif; ?> value="<?php echo e($common->eext1); ?>"
                                                                               placeholder="Ext" type="text">
                                                                    </div>

                                                                    <div class="col-md-2 col-xs-6 fsc-element-margin showguardian"
                                                                         <?php if($common->eteletype1 != 'Mobile'): ?> style="display:none;" <?php endif; ?>>
                                                                        <select name="guardian" id="guardian"
                                                                                class="form-control fsc-input">
                                                                            <option value="">Select</option>
                                                                            <option value="Himself" <?php if (empty($common->faxbli3_g)) {
                                                                                if ($common->guardian == 'Himself') {
                                                                                    echo 'selected';
                                                                                }
                                                                            } else {
                                                                                if ($common->basic_guardian1 == 'Himself') {
                                                                                    echo 'Selected';
                                                                                }
                                                                            }?>>Himself
                                                                            </option>
                                                                            <option value="Herself" <?php if (empty($common->faxbli3_g)) {
                                                                                if ($common->guardian == 'Herself') {
                                                                                    echo 'selected';
                                                                                }
                                                                            } else {
                                                                                if ($common->basic_guardian1 == 'Herself') {
                                                                                    echo 'Selected';
                                                                                }
                                                                            }?>>Herself
                                                                            </option>

                                                                            <option value="Spouse" <?php if (empty($common->faxbli3_g)) {
                                                                                if ($common->guardian == 'Spouse') {
                                                                                    echo 'selected';
                                                                                }
                                                                            } else {
                                                                                if ($common->basic_guardian1 == 'Spouse') {
                                                                                    echo 'Selected';
                                                                                }
                                                                            }?>>Spouse
                                                                            </option>
                                                                            <option value="Father" <?php if (empty($common->faxbli3_g)) {
                                                                                if ($common->guardian == 'Father') {
                                                                                    echo 'selected';
                                                                                }
                                                                            } else {
                                                                                if ($common->basic_guardian1 == 'Father') {
                                                                                    echo 'Selected';
                                                                                }
                                                                            }?>>Father
                                                                            </option>
                                                                            <option value="Mother" <?php if (empty($common->faxbli3_g)) {
                                                                                if ($common->guardian == 'Mother') {
                                                                                    echo 'selected';
                                                                                }
                                                                            } else {
                                                                                if ($common->basic_guardian1 == 'Mother') {
                                                                                    echo 'Selected';
                                                                                }
                                                                            }?>>Mother
                                                                            </option>
                                                                            <option value="Daughter" <?php if (empty($common->faxbli3_g)) {
                                                                                if ($common->guardian == 'Daughter') {
                                                                                    echo 'selected';
                                                                                }
                                                                            } else {
                                                                                if ($common->basic_guardian1 == 'Daughter') {
                                                                                    echo 'Selected';
                                                                                }
                                                                            }?>>Daughter
                                                                            </option>
                                                                            <option value="Son" <?php if (empty($common->faxbli3_g)) {
                                                                                if ($common->guardian == 'Son') {
                                                                                    echo 'selected';
                                                                                }
                                                                            } else {
                                                                                if ($common->basic_guardian1 == 'Son') {
                                                                                    echo 'Selected';
                                                                                }
                                                                            }?>>Son
                                                                            </option>
                                                                            <option value="Uncle" <?php if (empty($common->faxbli3_g)) {
                                                                                if ($common->guardian == 'Uncle') {
                                                                                    echo 'selected';
                                                                                }
                                                                            } else {
                                                                                if ($common->basic_guardian1 == 'Uncle') {
                                                                                    echo 'Selected';
                                                                                }
                                                                            }?>>Uncle
                                                                            </option>
                                                                            <option value="Aunty" <?php if (empty($common->faxbli3_g)) {
                                                                                if ($common->guardian == 'Aunty') {
                                                                                    echo 'selected';
                                                                                }
                                                                            } else {
                                                                                if ($common->basic_guardian1 == 'Aunty') {
                                                                                    echo 'Selected';
                                                                                }
                                                                            }?>>Aunty
                                                                            </option>
                                                                            <option value="Other" <?php if (empty($common->faxbli3_g)) {
                                                                                if ($common->guardian == 'Other') {
                                                                                    echo 'selected';
                                                                                }
                                                                            } else {
                                                                                if ($common->basic_guardian1 == 'Other') {
                                                                                    echo 'Selected';
                                                                                }
                                                                            }?>>Other
                                                                            </option>

                                                                        <!--  <option value="Spouse" <?php if($common->guardian	=='Spouse'): ?> selected <?php endif; ?>>Spouse</option>
                                               <option value="Father" <?php if($common->guardian	=='Father'): ?> selected <?php endif; ?>>Father</option>
                                               <option value="Mother" <?php if($common->guardian	=='Mother'): ?> selected <?php endif; ?>>Mother</option>
                                               <option value="Daughter" <?php if($common->guardian	=='Daughter'): ?> selected <?php endif; ?>>Daughter</option>
                                               <option value="Son" <?php if($common->guardian	=='Son'): ?> selected <?php endif; ?>>Son</option>
                                               <option value="Uncle" <?php if($common->guardian	=='Uncle'): ?> selected <?php endif; ?>>Uncle</option>
                                               <option value="Aunty" <?php if($common->guardian	=='Aunty'): ?> selected <?php endif; ?>>Aunty</option>
                                               <option value="Other" <?php if($common->guardian	=='Other'): ?> selected <?php endif; ?>>Other</option>
                                               !-->

                                                                        </select>

                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group ">
                                                            <label class="control-label col-md-3">Telephone No.
                                                                2:</label>
                                                            <div class="col-lg-7 col-md-8">
                                                                <div class="row">
                                                                    <div class="col-md-5">
                                                                        <input name="mobile_2"
                                                                               placeholder="(999) 999-9999"
                                                                               value="<?php if (empty($common->faxbli3_g)) {
                                                                                   echo $common->mobile_2;
                                                                               } else {
                                                                                   echo $common->motelephoneile1;
                                                                               }?>" type="tel" id="mobile_2"
                                                                               class="form-control bfh-phone"
                                                                               data-country="countries_states1"
                                                                               data-format="  (999) 999-9999">
                                                                    </div>
                                                                    <div class="col-md-3 col-xs-6">
                                                                        <select name="mobiletype_2" id="mobiletype_2"
                                                                                class="form-control fsc-input"
                                                                                style="height:auto">
                                                                            <option value="">Select</option>
                                                                            <option value="Business" <?php if (empty($common->faxbli3_g)) {
                                                                                if ($common->eteletype2 == 'Business') {
                                                                                    echo 'selected';
                                                                                }
                                                                            } else {
                                                                                if ($common->telephoneNo2Type == 'Business') {
                                                                                    echo 'selected';
                                                                                }
                                                                            }?>>Business
                                                                            </option>
                                                                            <option value="Office" <?php if (empty($common->faxbli3_g)) {
                                                                                if ($common->eteletype2 == 'Office') {
                                                                                    echo 'selected';
                                                                                }
                                                                            } else {
                                                                                if ($common->telephoneNo2Type == 'Office') {
                                                                                    echo 'selected';
                                                                                }
                                                                            }?>>Office
                                                                            </option>
                                                                            <option value="Resid" <?php if (empty($common->faxbli3_g)) {
                                                                                if ($common->eteletype2 == 'Resid') {
                                                                                    echo 'selected';
                                                                                }
                                                                            } else {
                                                                                if ($common->telephoneNo2Type == 'Resid') {
                                                                                    echo 'selected';
                                                                                }
                                                                            }?>>Resid
                                                                            </option>
                                                                            <option value="Mobile" <?php if (empty($common->faxbli3_g)) {
                                                                                if ($common->eteletype2 == 'Mobile') {
                                                                                    echo 'selected';
                                                                                }
                                                                            } else {
                                                                                if ($common->telephoneNo2Type == 'Mobile') {
                                                                                    echo 'selected';
                                                                                }
                                                                            }?>>Mobile
                                                                            </option>
                                                                            <option value="Other" <?php if (empty($common->faxbli3_g)) {
                                                                                if ($common->eteletype2 == 'Other') {
                                                                                    echo 'selected';
                                                                                }
                                                                            } else {
                                                                                if ($common->telephoneNo2Type == 'Other') {
                                                                                    echo 'selected';
                                                                                }
                                                                            }?>>Other
                                                                            </option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-md-4 col-xs-6 hideext2">
                                                                        <input class="form-control fsc-input zip"
                                                                               maxlength="5"
                                                                               <?php if($common->eteletype2=='Business'): ?> <?php elseif($common->eteletype2=='Home'): ?> <?php else: ?> readonly
                                                                               <?php endif; ?>  id="ext2_2" name="ext2_2"
                                                                               value="<?php echo e($common->eext2); ?>"
                                                                               placeholder="Ext" type="text">
                                                                    </div>

                                                                    <div class="col-md-4 col-xs-6 showguardian2"
                                                                         style="display:none;">
                                                                        <select name="guardian2" id="guardian2"
                                                                                class="form-control fsc-input">
                                                                            <option value="">Select</option>

                                                                            <option value="Himself" <?php if (empty($common->faxbli3_g)) {
                                                                                if ($common->guardian2 == 'Himself') {
                                                                                    echo 'selected';
                                                                                }
                                                                            } else {
                                                                                if ($common->basic_guardian2 == 'Himself') {
                                                                                    echo 'Selected';
                                                                                }
                                                                            }?>>Himself
                                                                            </option>
                                                                            <option value="Herself" <?php if (empty($common->faxbli3_g)) {
                                                                                if ($common->guardian2 == 'Herself') {
                                                                                    echo 'selected';
                                                                                }
                                                                            } else {
                                                                                if ($common->basic_guardian2 == 'Herself') {
                                                                                    echo 'Selected';
                                                                                }
                                                                            }?>>Herself
                                                                            </option>

                                                                            <option value="Spouse" <?php if (empty($common->faxbli3_g)) {
                                                                                if ($common->guardian2 == 'Spouse') {
                                                                                    echo 'selected';
                                                                                }
                                                                            } else {
                                                                                if ($common->basic_guardian2 == 'Spouse') {
                                                                                    echo 'Selected';
                                                                                }
                                                                            }?>>Spouse
                                                                            </option>
                                                                            <option value="Father" <?php if (empty($common->faxbli3_g)) {
                                                                                if ($common->guardian2 == 'Father') {
                                                                                    echo 'selected';
                                                                                }
                                                                            } else {
                                                                                if ($common->basic_guardian2 == 'Father') {
                                                                                    echo 'Selected';
                                                                                }
                                                                            }?>>Father
                                                                            </option>
                                                                            <option value="Mother" <?php if (empty($common->faxbli3_g)) {
                                                                                if ($common->guardian2 == 'Mother') {
                                                                                    echo 'selected';
                                                                                }
                                                                            } else {
                                                                                if ($common->basic_guardian2 == 'Mother') {
                                                                                    echo 'Selected';
                                                                                }
                                                                            }?>>Mother
                                                                            </option>
                                                                            <option value="Daughter" <?php if (empty($common->faxbli3_g)) {
                                                                                if ($common->guardian2 == 'Daughter') {
                                                                                    echo 'selected';
                                                                                }
                                                                            } else {
                                                                                if ($common->basic_guardian2 == 'Daughter') {
                                                                                    echo 'Selected';
                                                                                }
                                                                            }?>>Daughter
                                                                            </option>
                                                                            <option value="Son" <?php if (empty($common->faxbli3_g)) {
                                                                                if ($common->guardian2 == 'Son') {
                                                                                    echo 'selected';
                                                                                }
                                                                            } else {
                                                                                if ($common->basic_guardian2 == 'Son') {
                                                                                    echo 'Selected';
                                                                                }
                                                                            }?>>Son
                                                                            </option>
                                                                            <option value="Uncle" <?php if (empty($common->faxbli3_g)) {
                                                                                if ($common->guardian2 == 'Uncle') {
                                                                                    echo 'selected';
                                                                                }
                                                                            } else {
                                                                                if ($common->basic_guardian2 == 'Uncle') {
                                                                                    echo 'Selected';
                                                                                }
                                                                            }?>>Uncle
                                                                            </option>
                                                                            <option value="Aunty" <?php if (empty($common->faxbli3_g)) {
                                                                                if ($common->guardian2 == 'Aunty') {
                                                                                    echo 'selected';
                                                                                }
                                                                            } else {
                                                                                if ($common->basic_guardian2 == 'Aunty') {
                                                                                    echo 'Selected';
                                                                                }
                                                                            }?>>Aunty
                                                                            </option>
                                                                            <option value="Other" <?php if (empty($common->faxbli3_g)) {
                                                                                if ($common->guardian2 == 'Other') {
                                                                                    echo 'selected';
                                                                                }
                                                                            } else {
                                                                                if ($common->basic_guardian2 == 'Other') {
                                                                                    echo 'Selected';
                                                                                }
                                                                            }?>>Other
                                                                            </option>


                                                                        </select>

                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group" style="margin-bottom:0px;">
                                                            <label class="control-label col-md-3 hide_991"></label>
                                                            <div class="col-md-8">
                                                                <input type="checkbox" class="check" value="1"
                                                                       name="faxbli1" id="faxbli1"
                                                                       onclick="faxbli(this.form)"
                                                                       <?php if(empty($common->faxbli1)): ?> <?php else: ?> checked <?php endif; ?>>
                                                                <label for="faxbli1"> Same
                                                                    as <?php if($common->business_id=='6'): ?> <?php else: ?>
                                                                        business <?php endif; ?> Fax</label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group ">
                                                            <label class="control-label col-md-3">Fax No. :</label>
                                                            <div class="col-lg-7 col-md-8">
                                                                <div class="row">
                                                                    <div class="col-md-5">
                                                                        <input name="contact_fax_1"
                                                                               placeholder="(999) 999-9999"
                                                                               value="<?php echo e($common->contact_fax_1); ?>"
                                                                               type="tel" id="contact_fax_1"
                                                                               class="form-control bfh-phone"
                                                                               data-country="countries_states1"
                                                                               data-format="  (999) 999-9999">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group" style="margin-bottom:0px;">
                                                            <label class="control-label col-md-3 hide_991"></label>
                                                            <div class="col-md-8">
                                                                <input type="checkbox" class="check" value="1"
                                                                       name="emailbli1" id="emailbli1"
                                                                       onclick="emailbli(this.form)"
                                                                       <?php if(empty($common->emailbli1)): ?> <?php else: ?> checked <?php endif; ?>>
                                                                <label for="emailbli1" style="margin-left: -4px;">Same
                                                                    as Primary Email</label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group ">
                                                            <label class="control-label col-md-3">Email :</label>
                                                            <div class="col-lg-7 col-md-8">
                                                                <input type="text" class="form-control" id="email_1"
                                                                       name="email_1" value="<?php echo e($common->email_1); ?>">
                                                            </div>
                                                        </div>
                                                    </div>


                                                <?php
                                                if(isset($info) != '' && isset($info) != null )
                                                {

                                                ?>
                                                <?php $__currentLoopData = $info; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $in): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                                    <!--<div  class="con" id="con_<?php echo e($in); ?>">-->
                                                        <div class="fieldGroupCopy_<?php echo e($in); ?>" id="mytable">
                                                            <div class="Branch">
                                                                <h1>Secondary Contact Information</h1>
                                                            </div>
                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                <div class="form-group">

                                                                    <label class="control-label col-md-3">Name :</label>
                                                                    <div class="col-md-6">
                                                                        <div class="row">
                                                                            <div class="col-md-3"
                                                                                 style="width:17% !important;padding-right:0px;">
                                                                                <select type="text"
                                                                                        class="form-control txtOnly"
                                                                                        id="nametype_sec"
                                                                                        name="nametype_sec[]">
                                                                                    <option value="mr"
                                                                                            <?php if($in->nametype_sec=='mr'): ?> selected <?php endif; ?>>
                                                                                        Mr.
                                                                                    </option>
                                                                                    <option value="mrs"
                                                                                            <?php if($in->nametype_sec=='mrs'): ?> selected <?php endif; ?>>
                                                                                        Mrs.
                                                                                    </option>
                                                                                    <option value="miss"
                                                                                            <?php if($in->nametype_sec=='miss'): ?> selected <?php endif; ?>>
                                                                                        Miss.
                                                                                    </option>
                                                                                </select>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <input type="text"
                                                                                       class="form-control textonly"
                                                                                       id="firstname_sec"
                                                                                       name="firstname_sec[]"
                                                                                       value="<?php echo e($in->firstname_sec); ?>"
                                                                                       placeholder="First Name">
                                                                            </div>
                                                                            <div class="col-md-1"
                                                                                 style="width:16% !important;">
                                                                                <div class="row">
                                                                                    <input type="text"
                                                                                           class="form-control textonly"
                                                                                           id="middlename_sec"
                                                                                           value="<?php echo e($in->middlename_sec); ?>"
                                                                                           name="middlename_sec[]"
                                                                                           maxlength="1"
                                                                                           placeholder="M">
                                                                                </div>
                                                                            </div>
                                                                            <div class="">
                                                                                <div class="col-md-4">
                                                                                    <input type="text"
                                                                                           class="form-control textonly"
                                                                                           id="lastname_sec"
                                                                                           value="<?php echo e($in->lastname_sec); ?>"
                                                                                           name="lastname_sec[]"
                                                                                           value=""
                                                                                           placeholder="Last Name">
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                    </div>

                                                                    <a class="delete-row btn btn-danger"
                                                                       onclick="return confirm('Are you sure to remove this record?')"
                                                                       href="<?php echo e(route('customer.destroyinfo',$in->id)); ?>">-</a>

                                                                </div>

                                                                <div class="form-group ">
                                                                    <label class="control-label col-md-3">Title
                                                                        :</label>
                                                                    <div class="col-md-3">
                                                                        <input type="text" class="form-control"
                                                                               id="sec_title" name="sec_title[]"
                                                                               value="<?php echo e($in->sec_title); ?>">
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <input type="checkbox" class="check" value="1"
                                                                               id="user_access<?php echo e($in); ?>"
                                                                               name="user_access[]"
                                                                               <?php if($in->user_access): ?> checked
                                                                               <?php endif; ?> onclick="user_access(this.form)">
                                                                        <label for="user_access<?php echo e($in); ?>"> User
                                                                            Access</label>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group ">
                                                                    <label class="control-label col-md-3">Address 1
                                                                        :</label>
                                                                    <div class="col-md-6">
                                                                        <input type="text" class="form-control"
                                                                               id="contact_address_sec_1"
                                                                               placeholder="Address 1"
                                                                               name="contact_address_sec_1[]"
                                                                               value="<?php echo e($in->contact_address_sec_1); ?>">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group ">
                                                                    <label class="control-label col-md-3">Address 2
                                                                        :</label>
                                                                    <div class="col-md-6">
                                                                        <input type="text" class="form-control"
                                                                               id="contact_address_sec_2"
                                                                               value="<?php echo e($in->contact_address_sec_2); ?>"
                                                                               name="contact_address_sec_2[]"
                                                                               placeholder="Address 2">
                                                                    </div>
                                                                </div>

                                                                <div class="form-group ">
                                                                    <label class="control-label col-md-3">City / State /
                                                                        Zip :</label>
                                                                    <div class="col-md-2">
                                                                        <input name="city_sec[]"
                                                                               value="<?php echo e($in->city_sec); ?>" type="text"
                                                                               id="city_sec" placeholder="City"
                                                                               class="textonly form-control">
                                                                    </div>

                                                                    <div class="">
                                                                        <div class="col-md-2">
                                                                            <select name="state_sec[]" id="state_sec"
                                                                                    data-state="<?php echo e($in->state_sec); ?>"
                                                                                    class="form-control fsc-input bfh-states"
                                                                                    data-country="countries_states1">
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <div class="">
                                                                        <div class="col-md-2">
                                                                            <input name="zip_sec[]" placeholder="zip"
                                                                                   type="text" id="zip_sec"
                                                                                   class="form-control zip"
                                                                                   value="<?php echo e($in->zip_sec); ?>">
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group ">
                                                                    <label class="control-label col-md-3">Telephone No.
                                                                        1:</label>
                                                                    <div class="col-md-2">
                                                                        <input name="mobile_sec_1[]"
                                                                               placeholder="(999) 999-9999" type="tel"
                                                                               id="mobile_sec_1"
                                                                               class="form-control phone"
                                                                               value="<?php echo e($in->mobile_sec_1); ?>">
                                                                    </div>

                                                                    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin ">
                                                                        <select name="mobiletype_sec_1[]"
                                                                                id="mobiletype_sec_1"
                                                                                class="form-control fsc-input"
                                                                                style="height:auto">
                                                                            <option value="">Select</option>
                                                                            <option value="Business"
                                                                                    <?php if($in->mobiletype_sec_1=='Business'): ?> selected <?php endif; ?>>
                                                                                Business
                                                                            </option>
                                                                            <option value="Office"
                                                                                    <?php if($in->mobiletype_sec_1=='Office'): ?> selected <?php endif; ?>>
                                                                                Office
                                                                            </option>
                                                                            <option value="Resid"
                                                                                    <?php if($in->mobiletype_sec_1=='Resid'): ?> selected <?php endif; ?>>
                                                                                Resid
                                                                            </option>

                                                                            <option value="Mobile"
                                                                                    <?php if($in->mobiletype_sec_1=='Mobile'): ?> selected <?php endif; ?>>
                                                                                Mobile
                                                                            </option>
                                                                            <option value="Other"
                                                                                    <?php if($in->mobiletype_sec_1=='Other'): ?> selected <?php endif; ?>>
                                                                                Other
                                                                            </option>
                                                                        </select>
                                                                    </div>

                                                                    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                        <input class="form-control fsc-input zip"
                                                                               id="ext_sec_1" maxlength="5"
                                                                               name="ext_sec_1[]"
                                                                               <?php if($in->mobiletype_sec_1=='Business'): ?> <?php elseif($in->mobiletype_sec_1=='Home'): ?> <?php else: ?> readonly
                                                                               <?php endif; ?>   placeholder="Ext" type="text"
                                                                               value="<?php echo e($in->ext_sec_1); ?>">
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Telephone No.
                                                                        2:</label>
                                                                    <div class="col-md-2">
                                                                        <input name="mobile_sec_2[]"
                                                                               placeholder="(999) 999-9999"
                                                                               value="<?php echo e($in->mobile_sec_2); ?>" type="tel"
                                                                               id="mobile_sec_2"
                                                                               class="form-control phone">
                                                                    </div>

                                                                    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin ">
                                                                        <select name="mobiletype_sec_2[]"
                                                                                id="mobiletype_sec_2"
                                                                                class="form-control fsc-input"
                                                                                style="height:auto">
                                                                            <option value="">Select</option>
                                                                            <option value="Business"
                                                                                    <?php if($in->mobiletype_sec_2=='Business'): ?> selected <?php endif; ?>>
                                                                                Business
                                                                            </option>
                                                                            <option value="Office"
                                                                                    <?php if($in->mobiletype_sec_2	=='Office'): ?> selected <?php endif; ?>>
                                                                                Office
                                                                            </option>
                                                                            <option value="Resid"
                                                                                    <?php if($in->mobiletype_sec_2	=='Resid'): ?> selected <?php endif; ?>>
                                                                                Resid
                                                                            </option>

                                                                            <option value="Mobile"
                                                                                    <?php if($in->mobiletype_sec_2	=='Mobile'): ?> selected <?php endif; ?>>
                                                                                Mobile
                                                                            </option>
                                                                            <option value="Other"
                                                                                    <?php if($in->mobiletype_sec_2 =='Other'): ?> selected <?php endif; ?>>
                                                                                Other
                                                                            </option>
                                                                        </select>
                                                                    </div>

                                                                    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                        <input class="form-control fsc-input zip"
                                                                               maxlength="5" id="ext_sec_2"
                                                                               name="ext_sec_2[]"
                                                                               value="<?php echo e($in->ext_sec_2); ?>"
                                                                               placeholder="Ext" type="text"
                                                                               <?php if($in->mobiletype_sec2=='Business'): ?> <?php elseif($in->mobiletype_sec2=='Home'): ?> <?php else: ?> readonly <?php endif; ?>>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group" style="margin-bottom:0px;">
                                                                    <label class="control-label col-md-3"></label>
                                                                    <div class="col-md-3">
                                                                    <!--  <label><input type="checkbox" class="" name="faxbli2_<?php echo e($in->id); ?>" onclick="faxbli_<?php echo e($in->id); ?>(this.form)"> Same As Business Fax</label>-->
                                                                    </div>
                                                                </div>

                                                                <div class="form-group ">
                                                                    <label class="control-label col-md-3">Fax No.
                                                                        :</label>
                                                                    <div class="col-md-2">
                                                                        <input name="contact_fax_sec[]"
                                                                               placeholder="(999) 999-9999"
                                                                               value="<?php echo e($in->contact_fax_sec); ?>"
                                                                               type="tel" id="contact_fax_sec"
                                                                               class="phone form-control">
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <div class="form-group" style="margin-bottom:0px;">
                                                                        <label class="control-label col-md-3"></label>
                                                                        <div class="col-md-4">
                                                                        <!-- <label style="margin-left: 10px;"><input type="checkbox" class="" name="emailbli2_<?php echo e($in->id); ?>" onclick="emailbl_<?php echo e($in->id); ?>(this.form)"> Same As Above Email</label>-->
                                                                        </div>
                                                                    </div>

                                                                    <label class="control-label col-md-3">Email
                                                                        :</label>
                                                                    <div class="col-md-6">
                                                                        <input type="text" class="form-control"
                                                                               id="email_sec" name="email_sec[]"
                                                                               value="<?php echo e($in->email_sec); ?>"
                                                                               placeholer="Email">
                                                                        <input type="hidden" name="secid[]"
                                                                               value="<?php echo e($in->id); ?>">
                                                                    </div>
                                                                </div>

                                                                <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                                </div>
                                                            </div>


                                                            <script>

                                                                function emailbl_<?php echo e($in->id); ?>(f) {// alert();
                                                                    if (f.emailbli2_<?php echo e($in->id); ?>.checked == true) {// alert();
                                                                        f.email_sec.value = f.email.value;
                                                                    } else {
                                                                        f.email_sec.value = '';
                                                                    }
                                                                }


                                                                function faxbli_<?php echo e($in->id); ?>(f) { //alert();
                                                                    if (f.faxbli2_<?php echo e($in->id); ?>.checked == true) {
                                                                        f.contact_fax_sec.value = f.business_fax.value;
                                                                    } else {
                                                                        f.contact_fax_sec.value = '';
                                                                    }
                                                                }


                                                            </script>


                                                            <script>
                                                                $(document).ready(function () {
                                                                    var date = $('#ext_sec_<?php echo e($in->id); ?>').val();
                                                                    $('#mobiletype_sec_<?php echo e($in->id); ?>').on('change', function () {
                                                                        if (this.value == 'Home') {
                                                                            $("#ext_sec_<?php echo e($in->id); ?>").removeAttr("readonly");
                                                                            $('#ext_sec_<?php echo e($in->id); ?>').val();
                                                                        } else if (this.value == 'Business') {
                                                                            $("#ext_sec_<?php echo e($in->id); ?>").removeAttr("readonly");
                                                                            $('#ext_sec_<?php echo e($in->id); ?>').val();
                                                                        } else {
                                                                            $("#ext_sec_<?php echo e($in->id); ?>").attr("readonly", true);
                                                                            $('#ext_sec_<?php echo e($in->id); ?>').val('');
                                                                        }
                                                                    });
                                                                });
                                                            </script>
                                                            <script>
                                                                $(document).ready(function () {
                                                                    var date = $('#ext_sec_2_<?php echo e($in->id); ?>').val();
                                                                    $('#mobiletype_sec2_<?php echo e($in->id); ?>').on('change', function () {
                                                                        if (this.value == 'Home') {
                                                                            $("#ext_sec_2_<?php echo e($in->id); ?>").removeAttr("readonly");
                                                                            $('#ext_sec_2_<?php echo e($in->id); ?>').val();
                                                                        } else if (this.value == 'Business') {
                                                                            $("#ext_sec_2_<?php echo e($in->id); ?>").removeAttr("readonly");
                                                                            $('#ext_sec_2_<?php echo e($in->id); ?>').val();
                                                                        } else {
                                                                            $("#ext_sec_2_<?php echo e($in->id); ?>").attr("readonly", true);
                                                                            $('#ext_sec_2_<?php echo e($in->id); ?>').val('');
                                                                        }
                                                                    });
                                                                });
                                                            </script>

                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                                            <?php

                                                            }
                                                            ?>


                                                        </div>



                                                        <!--code put here-->


                                                        <!--<body>       -->
                                                        <!--    <table id="myTable" border="1">-->
                                                        <!--    </table>-->
                                                        <!--    <br />-->
                                                        <!--    <button type="button" onclick="displayResult()">Insert new row</button>            -->
                                                        <!--</body>-->



                                                        <table id="myTable" style="border: 1px solid black"
                                                               class="table">


                                                        </table>



                                                        <!--    <div class="fieldGroupCopy" id="mytable">-->
                                                        <!--    <div class="Branch">-->
                                                        <!--      <h1>Secondary Contact Information</h1>-->
                                                        <!--    </div>-->

                                                        <!--    <div class="col-md-12 col-sm-12 col-xs-12">-->
                                                        <!--        <div class="form-group">-->
                                                        <!--             <label class="control-label col-md-3">Name :</label>-->
                                                        <!--             <div class="col-md-6">-->
                                                        <!--                <div class="row">-->
                                                        <!--                   <div class="col-md-3" style="width:17% !important;padding-right:0px;">-->
                                                        <!--                      <select type="text" class="form-control txtOnly" id="nametype_sec0" name="nametype_sec[]">-->
                                                        <!--                         <option value="mr">Mr.</option>-->
                                                        <!--                         <option value="mrs">Mrs.</option>-->
                                                        <!--                         <option value="miss">Miss.</option>-->
                                                        <!--                      </select>-->
                                                        <!--                   </div>-->
                                                        <!--                   <div class="col-md-4">-->
                                                        <!--                      <input type="text" class="form-control textonly" id="firstname_sec0" name="firstname_sec[]" placeholder="First Name">				-->
                                                        <!--                   </div>-->
                                                        <!--                   <div class="col-md-1" style="width:16% !important;">-->
                                                        <!--                      <div class="row">-->
                                                        <!--                         <input type="text" class="form-control textonly" id="middlename_sec0" name="middlename_sec[]" maxlength="1" placeholder="M">				-->
                                                        <!--                      </div>-->
                                                        <!--                   </div>-->
                                                        <!--                   <div class="">-->
                                                        <!--                      <div class="col-md-4">-->
                                                        <!--                         <input type="text" class="form-control textonly" id="lastname_sec0" name="lastname_sec[]" placeholder="Last Name">				-->
                                                        <!--                      </div>-->
                                                        <!--                   </div>-->
                                                        <!--                </div>-->
                                                        <!--             </div>-->


                                                        <!--<input type="button" class="add-row btn btn-primary" value="+"></span>-->
                                                        <!--        <input type="button" class="delete-row btn btn-danger" value="-"></span>-->

                                                        <!--         </div>-->



                                                        <!--        <div class="form-group ">-->
                                                        <!--           <label class="control-label col-md-3">Title :</label>-->
                                                        <!--           <div class="col-md-3">-->
                                                        <!--           <input type="text" class="form-control" id="sec_title0" name="sec_title[]">				-->
                                                        <!--           </div>-->

                                                        <!--            <div class="col-md-3">-->
                                                        <!--                <input type="checkbox" class="check" value="1" id="user_access0" name="user_access[]"> <label for="user_access0"> User Access</label>-->
                                                        <!--           </div>-->
                                                        <!--        </div>-->

                                                        <!--        <div class="form-group ">-->
                                                        <!--             <label class="control-label col-md-3">Address 1 :</label>-->
                                                        <!--             <div class="col-md-6">-->
                                                        <!--                <input type="text" class="form-control" id="contact_address_sec_10"  name="contact_address_sec_1[]" placeholder="Address 1">				-->
                                                        <!--             </div>-->
                                                        <!--         </div>-->

                                                        <!--        <div class="form-group ">-->
                                                        <!--             <label class="control-label col-md-3">Address 2 :</label>-->
                                                        <!--             <div class="col-md-6">-->
                                                        <!--                <input type="text" class="form-control" id="contact_address_sec_20" name="contact_address_sec_2[]" placeholder="Address 2">				-->
                                                        <!--             </div>-->
                                                        <!--         </div>-->

                                                        <!--        <div class="form-group ">-->
                                                        <!--             <label class="control-label col-md-3">City / State / Zip :</label>-->
                                                        <!--             <div class="col-md-2">-->
                                                        <!--                <input name="city_sec[]" type="text" id="city_sec0" placeholder="City"  class="textonly form-control">-->
                                                        <!--             </div>-->
                                                        <!--             <div class="">-->
                                                        <!--                <div class="col-md-2">-->
                                                        <!--                   <select name="state_sec[]" id="state_sec0" class="form-control fsc-input">-->
                                                        <!--                       <option value="">Select</option>-->

                                                        <!--                     </select>-->
                                                        <!--                </div>-->
                                                        <!--             </div>-->
                                                        <!--             <div class="">-->
                                                        <!--                <div class="col-md-2">-->
                                                        <!--                   <input name="zip_sec[]" placeholder="zip" type="text" id="zip_sec0" class="form-control zip">-->
                                                        <!--                </div>-->
                                                        <!--             </div>-->
                                                        <!--        </div>-->

                                                        <!--        <div class="form-group ">-->
                                                        <!--             <label class="control-label col-md-3">Telephone No. 1:</label>-->
                                                        <!--             <div class="col-md-2">-->
                                                        <!--                <input name="mobile_sec_1[]"  data-country="countries_states1" placeholder="(999) 999-9999" type="tel" id="mobile_sec_10" class="form-control twxzrt bfh-phone">-->
                                                        <!--             </div>-->
                                                        <!--             <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">-->
                                                        <!--                <select name="mobiletype_sec_1[]" id="mobiletype_sec_10" class="form-control fsc-input mobiletype_sec" style="height:auto">-->
                                                        <!--                    <option value="">Select</option>-->
                                                        <!--                   <option value="Business">Business</option>-->
                                                        <!--                   <option value="Home">Office</option>-->
                                                        <!--                   <option value="Mobile">Mobile</option>-->
                                                        <!--                   <option value="Other">Other</option>-->
                                                        <!--                </select>-->
                                                        <!--             </div>-->
                                                        <!--             <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">-->
                                                        <!--                <input class="form-control fsc-input ext_sec_1" id="ext_sec_10" maxlength="5" name="ext_sec_1[]" readonly placeholder="Ext" type="text">-->
                                                        <!--             </div>-->
                                                        <!--         </div>-->

                                                        <!--        <div class="form-group ">-->
                                                        <!--             <label class="control-label col-md-3">Telephone No. 2:</label>-->
                                                        <!--             <div class="col-md-2">-->
                                                        <!--                <input name="mobile_sec_2[]" placeholder="(999) 999-9999" type="tel" id="mobile_sec_21" class="twxzrt form-control phone">-->
                                                        <!--             </div>-->

                                                        <!--             <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin ">-->
                                                        <!--                <select name="mobiletype_sec_2[]" id="mobiletype_sec_20" class="form-control fsc-input mobiletype_sec2" style="height:auto">-->
                                                        <!--                    <option value="">Select</option>-->
                                                        <!--                     <option value="Business">Business</option>-->
                                                        <!--                   <option value="Home">Office</option>-->
                                                        <!--                   <option value="Mobile">Mobile</option>-->
                                                        <!--                   <option value="Other">Other</option>-->
                                                        <!--                </select>-->
                                                        <!--             </div>-->
                                                        <!--             <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">-->
                                                        <!--                <input  type="text" class="form-control fsc-input ext_sec_2" id="ext_sec_20" name="ext_sec_2[]" maxlength="5" placeholder="Ext" readonly>-->
                                                        <!--             </div>-->
                                                        <!--         </div>-->

                                                        <!--        <div class="form-group" style="margin-bottom:0px;">-->
                                                        <!--             <label class="control-label col-md-3"></label>-->

                                                        <!--         </div>-->

                                                        <!--        <div class="form-group ">-->
                                                        <!--             <label class="control-label col-md-3">Fax No. :</label>-->
                                                        <!--             <div class="col-md-2">-->
                                                        <!--                <input name="contact_fax_sec[]" placeholder="(999) 999-9999" type="tel" id="contact_fax_sec0" class="contact_fax_sec twxzrt form-control">-->
                                                        <!--             </div>-->
                                                        <!--         </div>-->

                                                        <!--        <div class="form-group">-->
                                                        <!--             <div class="form-group" style="margin-bottom:0px;">-->
                                                        <!--                <label class="control-label col-md-3"></label>-->

                                                        <!--             </div>-->
                                                        <!--             <label class="control-label col-md-3">Email :</label>-->
                                                        <!--             <div class="col-md-4">-->
                                                        <!--                <input type="text" class="form-control email_sec" id="email_sec0" name="email_sec[]" placeholer="Email">	-->
                                                        <!--             </div>-->
                                                        <!--         </div>-->

                                                        <!--        <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">-->
                                                        <!--        </div>-->

                                                        <!--    </div>-->
                                                        <!--</div>-->


                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="card-footer">
                                        <label class="col-lg-3 control-label"> </label>
                                        <div class="col-md-7">
                                            <div class="">
                                                <div class="col-xs-3" style="width:auto;">
                                                    <input type="submit" class="btn_new_save primary" value="Save"
                                                           style="padding:8px 20px;color:#fff !important;">

                                                    <!--<button type="button" class="btn_new_save primary1" id="primary1" name="submit">Save</button>!-->
                                                </div>
                                                <div class="col-xs-3" style="width:auto;">
                                                    <?php if(empty($_REQUEST['status'])): ?>
                                                        <a href="<?php echo e(url('/fac-Bhavesh-0554/customer')); ?>"
                                                           class="btn_new_cancel" style="padding:8px 20px;">Cancel</a>
                                                    <?php else: ?>
                                                        <style>.tab-content {
                                                                pointer-events: none;
                                                            }

                                                            input, select, textarea {
                                                                background: #eee !important
                                                            }</style>
                                                        <a href="<?php echo e(url('/fac-Bhavesh-0554/fscclientreport')); ?>"
                                                           class="btn_new_cancel">Cancel</a>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="clear:both"></div>
                                </form>
                            </div>


                        </div>

                    </div>


                </div>
                <!--</div>-->

                <!--</div>-->
                <!--</div>-->
                <!--</div>-->
                <!--</div>-->
                <!--</div>-->
        </section>
        <!--</div>-->


        <div id="alertss" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content" style="background-color: #ededed;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Save</h4>
                    </div>
                    <div class="modal-body">
                        <p>Client Successfully Saved.</p>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default"
                                style="background-color: #bfbfbf;border: 1px solid #001ced !important;">Close
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <div id="alertss1" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content" style="background-color: #ededed;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Save</h4>
                    </div>
                    <div class="modal-body">
                        <p>Client Successfully Saved.</p>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default"
                                style="background-color: #bfbfbf;border: 1px solid #001ced !important;">Close
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <div id="alerts" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content model-width" style="background-color: #f3f3f3;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Save changes</h4>
                    </div>
                    <div class="modal-body" style="padding: 25px 15px;">
                        <center>
                            <p>Do you want to save the changes ?</p>
                        </center>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-default primary1 primary" style="width:69px;">Yes</button>
                        <button type="button" id="button" class="btn btn-default primary" style="width:69px;">No
                        </button>
                        <a class="btn btn-default primary no-button">Cancel</a>
                    </div>
                </div>
            </div>
        </div>


        <?php $__currentLoopData = $clientser; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ak1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div id="myModal_locked_<?php echo e($ak1->id); ?>" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Confirmation</h4>
                        </div>
                        <div class="modal-body">
                            <p>Do you want to this record ?</p>
                        </div>
                        <div class="modal-footer">
                            <a href="<?php echo e(route('locked.lockdelete',$ak1->id)); ?>" class="btn btn-danger">Delete</a>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>





    <!--Modal Conversation Update Start-->
        <form method="post" class="form-horizontal" enctype="multipart/form-data"
              action="<?php echo e(route('customer.updatecoversation')); ?>">
            <?php echo e(csrf_field()); ?>



            <div id="conversationModal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Conversation Sheet</h4>
                        </div>
                        <div class="modal-body">
                            <div style="max-width:700px; margin:0px auto;">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="control-label col-md-5 text-right"><strong>Date:</strong></label>
                                                <div class="col-md-7">
                                                    <!--<input type="text" class="form-control effective_date2"/>-->
                                                    <input type="hidden" name="CovID" id="CovID" class="form-control">
                                                    <input type="text" name="date" id="creattiondate1"
                                                           class="form-control" placeholder="Date" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="control-label col-md-5 text-right"><strong>Day:</strong></label>
                                                <div class="col-md-7">
                                                    <input type="text" name="day" id="day1" class="form-control"
                                                           placeholder="Day" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="control-label col-md-3"><strong>Time:</strong></label>
                                                <div class="col-md-8">
                                                    <input type="text" name="time" id="time1" class="form-control"
                                                           placeholder="Time" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group<?php echo e($errors->has('type_user') ? ' has-error' : ''); ?>">
                                    <div class="row">
                                        <label class="control-label col-md-3 text-right" style="width:100px;"><strong>Select:</strong></label>
                                        <div class="col-md-5">
                                            <div class="">
                                                <select class="form-control fsc-input type_user" name="type_user"
                                                        id="type_user1" required>
                                                    <option>Select</option>
                                                    <option>Client</option>
                                                    <option>EE-User</option>
                                                    <option>Vendor</option>
                                                    <option>Other</option>
                                                </select>
                                            </div>
                                            <?php if($errors->has('type_user')): ?>
                                                <span class="help-block">
                        <strong><?php echo e($errors->first('type_user')); ?></strong>
                        </span>
                                            <?php endif; ?>
                                        </div>

                                    </div>
                                </div>

                                <div class="form-group clientid2" id="clientid" style="display:none;">
                                    <div class="row">
                                        <label class="control-label col-md-3 text-right" style="width:100px;"><strong>Client
                                                :</strong></label>
                                        <div class="col-md-5">
                                            <select class="form-control" name="clientid" id="clientid1">
                                                <option>Select</option>
                                                <?php $__currentLoopData = $listclient; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $client): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($client->id); ?>"><?php echo e($client->first_name); ?> <?php echo e($client->middle_name); ?> <?php echo e($client->last_name); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>

                                    </div>
                                </div>

                                <div class="form-group employeeuserid2" id="employeeuserid" style="display:none;">
                                    <div class="row">
                                        <label class="control-label col-md-3 text-right" style="width:100px;"><strong>Employee/User:</strong></label>
                                        <div class="col-md-5">
                                            <select class="form-control" name="employeeuserid" id="employeeuserid1">
                                                <option>Select</option>
                                                <?php $__currentLoopData = $listemployeeuser; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $employee): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($employee->id); ?>"><?php echo e($employee->firstName); ?> <?php echo e($employee->middleName); ?> <?php echo e($employee->lastName); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>

                                    </div>
                                </div>

                                <div class="form-group vendorid2" id="vendorid" style="display:none;">
                                    <div class="row">
                                        <label class="control-label col-md-3 text-right" style="width:100px;"><strong>Vendor:</strong></label>
                                        <div class="col-md-5">
                                            <select class="form-control" name="vendorid" id="vendorid1">
                                                <option>Select</option>
                                                <?php $__currentLoopData = $listvendoe; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vendoe): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($vendoe->id); ?>"><?php echo e($vendoe->firstName); ?> <?php echo e($vendoe->middleName); ?> <?php echo e($vendoe->lastName); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>

                                    </div>
                                </div>

                                <div class="form-group otherid2" id="otherid" style="display:none;">
                                    <div class="row">
                                        <label class="control-label col-md-3"
                                               style="width:100px; text-align:right; padding-left:0px!important;"><strong>Other
                                                :</strong></label>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control" name="otherid" id="otherid1">
                                        </div>

                                    </div>
                                </div>

                                <div class="form-group<?php echo e($errors->has('conrelatedname') ? ' has-error' : ''); ?>">
                                    <div class="row">
                                        <label class="control-label col-md-3"
                                               style="width:100px; text-align:right; padding-left:0px!important;"><strong>Related
                                                to:</strong></label>
                                        <div class="col-md-5">
                                            <div class="">
                                                <select class="form-control" name="conrelatedname" id="conrelatedname1"
                                                        required>
                                                    <option>Select</option>
                                                    <?php $__currentLoopData = $relatedNames; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $relatedNames): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($relatedNames->id); ?>"><?php echo e($relatedNames->relatednames); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                            </div>
                                            <?php if($errors->has('conrelatedname')): ?>
                                                <span class="help-block">
                            <strong><?php echo e($errors->first('conrelatedname')); ?></strong>
                            </span>
                                            <?php endif; ?>
                                        </div>

                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="row">
                                        <label class="control-label col-md-3"
                                               style="width:100px; text-align:right; padding-left:0px!important;"><strong>Conversation:</strong></label>
                                        <div class="col-md-9">
                                            <textarea rows="3" cols="12" class="form-control" name="condescription"
                                                      id="condescription1"> </textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <label class="control-label col-md-3"
                                               style="width:100px; text-align:right; padding-left:0px!important;"><strong>Note</strong></label>
                                        <div class="col-md-9">
                                            <textarea rows="3" cols="12" class="form-control" name="connotes"
                                                      id="connotes1"> </textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <label class="control-label col-md-3"
                                               style="width:100px; text-align:right; padding-left:0px!important;"><strong>&nbsp;</strong></label>
                                        <div class="col-md-2">
                                            <input class="btn_new_save btn-primary1" id="recInsert" type="submit"
                                                   name="submit" value="Save">
                                        </div>
                                        <div class="col-md-2">
                                            <a class="btn_new_cancel"
                                               href="https://financialservicecenter.net/fac-Bhavesh-0554">Cancel</a>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

        </form>
        <!--Modal Conversation Update End-->


        <!--Modal Notes Update Start-->
        <div id="notesModal" class="modal fade" role="dialog">
            <div class="modal-dialog" style="width:850px;">
                <form method="post" class="form-horizontal" enctype="multipart/form-data"
                      action="<?php echo e(route('customer.updatenotes')); ?>">
                <?php echo e(csrf_field()); ?>



                <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title"> Note</h4>
                        </div>
                        <div class="modal-body">
                            <div style="max-width:700px; margin:0px auto;">
                                <div class="row mt-3" style="margin-top:20px;">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="control-label col-md-5 text-right"
                                                       style="padding-right:28px!important;"><strong>Date:</strong></label>
                                                <div class="col-md-6" style="padding-left:12px!important;">
                                                    <!--<input type="text" class="form-control effective_date2"/>-->
                                                    <input type="hidden" name="NoteID" id="NoteID">
                                                    <input type="text" name="notedate" id="date2" class="form-control"
                                                           value="<?php echo e(date('m-d-Y')); ?>" placeholder="Date" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="control-label col-md-4 text-right"><strong>Day:</strong></label>
                                                <div class="col-md-5">
                                                    <input type="text" name="noteday" id="day2" class="form-control"
                                                           placeholder="Day" value="<?php echo e(date('l')); ?>" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="control-label col-md-3"><strong>Time:</strong></label>
                                                <div class="col-md-5">
                                                    <input type="text" name="notetime" id="time2" class="form-control"
                                                           value="<?php echo e(date("g:i a")); ?>" placeholder="Time" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <label class="control-label col-md-3 text-right" style="width:110px;"><strong>Select:</strong></label>
                                        <div class="col-md-5">
                                            <div class="">
                                                <select class="form-control fsc-input notetype_user"
                                                        name="notetype_user" id="notetype_user1" required>
                                                    <option>Select</option>
                                                    <option>Client</option>
                                                    <option>EE-User</option>
                                                    <option>Vendor</option>
                                                    <option>Other</option>
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="form-group noteclientid2" id="noteclientid" style="display:none;">
                                    <div class="row">
                                        <label class="control-label col-md-3 text-right" style="width:100px;"><strong>Client
                                                :</strong></label>
                                        <div class="col-md-5">
                                            <select class="form-control" name="noteclientid" id="noteclientid1">
                                                <option>Select</option>
                                                <?php $__currentLoopData = $listclient; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $client): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($client->id); ?>"><?php echo e($client->first_name); ?> <?php echo e($client->middle_name); ?> <?php echo e($client->last_name); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>

                                    </div>
                                </div>

                                <div class="form-group noteemployeeuserid2" id="noteemployeeuserid"
                                     style="display:none;">
                                    <div class="row">
                                        <label class="control-label col-md-3 text-right" style="width:100px;"><strong>Employee/User:</strong></label>
                                        <div class="col-md-5">
                                            <select class="form-control" name="noteemployeeuserid"
                                                    id="noteemployeeuserid1">
                                                <option>Select</option>
                                                <?php $__currentLoopData = $listemployeeuser; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $employee): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($employee->id); ?>"><?php echo e($employee->firstName); ?> <?php echo e($employee->middleName); ?> <?php echo e($employee->lastName); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>

                                    </div>
                                </div>

                                <div class="form-group notevendorid2" id="notevendorid" style="display:none;">
                                    <div class="row">

                                        <label class="control-label col-md-3 text-right" style="width:110px;"><strong>Vendor
                                                :</strong></label>
                                        <div class="col-md-5">
                                            <select class="form-control" name="notevendorid" id="notevendorid1">
                                                <option>Select</option>
                                                <?php $__currentLoopData = $listvendoe; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vendoe): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($vendoe->id); ?>"><?php echo e($vendoe->firstName); ?> <?php echo e($vendoe->middleName); ?> <?php echo e($vendoe->lastName); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>

                                    </div>
                                </div>

                                <div class="form-group noteotherid2" id="noteotherid" style="display:none;">
                                    <div class="row">

                                        <label class="control-label col-md-3 text-right" style="width:110px;"><strong>Other
                                                :</strong></label>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control" name="noteotherid"
                                                   id="noteotherid1">
                                        </div>

                                    </div>
                                </div>

                                <div class="form-group<?php echo e($errors->has('notesrelated') ? ' has-error' : ''); ?>">
                                    <div class="row">
                                        <label class="control-label col-md-3 text-right" style="width:110px;"><strong>Related
                                                to :</strong></label>
                                        <div class="col-md-5">
                                            <div class="">
                                                <select class="form-control" name="notenotesrelatedname"
                                                        id="noterelated1" required>
                                                    <option>Select</option>
                                                    <?php $__currentLoopData = $NotesNames; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $notesRelated): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($notesRelated->id); ?>"><?php echo e($notesRelated->notesrelated); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                            </div>
                                            <?php if($errors->has('notesrelated')): ?>
                                                <span class="help-block">
                            <strong><?php echo e($errors->first('notesrelated')); ?></strong>
                            </span>
                                            <?php endif; ?>
                                        </div>
                                        <div class="col-md-2">
                                            <a href="#" class="btn btn-primary" data-toggle="modal"
                                               data-target="#basicExampleModalNotes" class="redius"><i
                                                        class="fa fa-plus"></i></a>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="row">
                                        <label class="control-label col-md-3"
                                               style="width:110px; text-align:right; padding-left:0px!important;"><strong>Types
                                                of Note:</strong></label>
                                        <div class="col-md-5">
                                            <select class="form-control" name="notesrelatedcat" id="notesrelatedcat1"
                                                    required>
                                                <option>Permenant Note</option>
                                                <option>Temporary Note</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <label class="control-label col-md-3"
                                               style="width:110px; text-align:right; padding-left:0px!important;"><strong>Note:</strong></label>
                                        <div class="col-md-9" style="padding-right:0px;">
                                            <textarea rows="3" cols="12" class="form-control" name="notes"
                                                      id="notes1"> </textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <label class="control-label col-md-3"
                                               style="width:110px; text-align:right; padding-left:0px!important;"><strong>&nbsp;</strong></label>
                                        <div class="col-md-2">
                                            <input class="btn_new_save btn-primary1" id="recInsert2" type="submit"
                                                   name="submit" value="Save">
                                        </div>
                                        <div class="col-md-2">
                                            <a class="btn_new_cancel"
                                               href="https://financialservicecenter.net/fac-Bhavesh-0554">Cancel</a>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="modal-footer">
                            <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                        </div>
                    </div>

                </form>
            </div>
        </div>
        <!--Modal Notes Update End-->


        <style>
            .table > caption + thead > tr:first-child > td, .table > caption + thead > tr:first-child > th, .table > colgroup + thead > tr:first-child > td, .table > colgroup + thead > tr:first-child > th, .table > thead:first-child > tr:first-child > td, .table > thead:first-child > tr:first-child > th {
                border: 1px solid black !important;
            }

            .table > thead > tr > th {
                color: black !important;
            }
        </style>


        <script>
            $(document).ready(function () {
                $('.type_user').on('change', function () {
                    //otherid,clientid,employeeuserid,vendorid
                    //Client,EE-User,Vendor,Other
                    var thisss = $('.type_user').val();
                    if (thisss == 'Client') {
                        $('#clientid').show();
                        $('#employeeuserid').hide();
                        $('#vendorid').hide();
                        $('#otherid').hide();
                    } else if (thisss == 'EE-User') {
                        $('#clientid').hide();
                        $('#employeeuserid').show();
                        $('#vendorid').hide();
                        $('#otherid').hide();
                    } else if (thisss == 'Vendor') {
                        $('#clientid').hide();
                        $('#employeeuserid').hide();
                        $('#vendorid').show();
                        $('#otherid').hide();
                    } else if (thisss == 'Other') {
                        $('#clientid').hide();
                        $('#employeeuserid').hide();
                        $('#vendorid').hide();
                        $('#otherid').show();
                    }
                });

                $('.notetype_user').on('change', function () {
                    //otherid,clientid,employeeuserid,vendorid
                    //Client,EE-User,Vendor,Other
                    var thisss = $('.notetype_user').val();
                    if (thisss == 'Client') {
                        $('#noteclientid').show();
                        $('#noteemployeeuserid').hide();
                        $('#notevendorid').hide();
                        $('#noteotherid').hide();
                    } else if (thisss == 'EE-User') {
                        $('#noteclientid').hide();
                        $('#noteemployeeuserid').show();
                        $('#notevendorid').hide();
                        $('#noteotherid').hide();
                    } else if (thisss == 'Vendor') {
                        $('#noteclientid').hide();
                        $('#noteemployeeuserid').hide();
                        $('#notevendorid').show();
                        $('#noteotherid').hide();
                    } else if (thisss == 'Other') {
                        $('#noteclientid').hide();
                        $('#noteemployeeuserid').hide();
                        $('#notevendorid').hide();
                        $('#noteotherid').show();
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(".notesID").click(function () {
                var ids = $(this).attr('data-id');
                //alert(ids);
                $("#notesid").val(ids);
                $('#notesModal').modal('show');
                //console.log(ids);           
                $.get('<?php echo URL::to('getNotesemp'); ?>?documentid=' + ids, function (data) {
                    //console.log(data);exit;
                    if (data == "") {

                    } else {
                        //console.log(data.id);exit;
                        $('#NoteID').val(data.id);
                        $('#creattiondate1').val(data.creattiondate);
                        $('#noteday1').val(data.noteday);
                        $('#notetime1').val(data.notetime);
                        $('#notetype_user1').val(data.notetype_user);
                        $('#noteclientid1').val(data.noteclientid);
                        $('#noteemployeeuserid1').val(data.noteemployeeuserid);
                        $('#notevendorid1').val(data.notevendorid);
                        $('#noteotherid1').val(data.noteotherid);
                        $('#noterelated1').val(data.noterelated);
                        $('#notesrelatedcat1').val(data.notesrelatedcat);
                        $('#notes1').val(data.notes);


                        var thisss = $('#notetype_user1').val();
                        if (thisss == 'Client') {
                            $('.noteclientid2').show();
                            $('.noteemployeeuserid2').hide();
                            $('.notevendorid2').hide();
                            $('.noteotherid2').hide();
                        } else if (thisss == 'EE-User') {
                            $('.noteclientid2').hide();
                            $('.noteemployeeuserid2').show();
                            $('.notevendorid2').hide();
                            $('.noteotherid2').hide();
                        } else if (thisss == 'Vendor') {
                            $('.noteclientid2').hide();
                            $('.noteemployeeuserid2').hide();
                            $('.notevendorid2').show();
                            $('.noteotherid2').hide();
                        } else if (thisss == 'Other') {
                            $('.noteclientid2').hide();
                            $('.noteemployeeuserid2').hide();
                            $('.notevendorid2').hide();
                            $('.noteotherid2').show();
                        }

                    }


                });
            });

            $(".conversationID").click(function () {
                var ids = $(this).attr('data-id');
                //alert(ids);
                $("#conversationid").val(ids);
                $('#conversationModal').modal('show');
                //console.log(ids);           
                $.get('<?php echo URL::to('getConversationdata'); ?>?documentid=' + ids, function (data) {
                    //console.log(data);exit;
                    if (data == "") {

                    } else {
                        //console.log(data.id);exit;
                        $('#CovID').val(data.id);
                        $('#creattiondate1').val(data.creattiondate);
                        $('#day1').val(data.day);
                        $('#time1').val(data.time);
                        $('#type_user1').val(data.type_user);
                        $('#clientid1').val(data.clientid);
                        $('#employeeuserid1').val(data.employeeuserid);
                        $('#vendorid1').val(data.vendorid);
                        $('#otherid1').val(data.otherid);
                        $('#conrelatedname1').val(data.conrelatedname);
                        $('#condescription1').val(data.condescription);
                        $('#connotes1').val(data.connotes);


                        var thisss = $('#type_user1').val();
                        if (thisss == 'Client') {
                            $('.clientid2').show();
                            $('.employeeuserid2').hide();
                            $('.vendorid2').hide();
                            $('.otherid2').hide();
                        } else if (thisss == 'EE-User') {
                            $('.clientid2').hide();
                            $('.employeeuserid2').show();
                            $('.vendorid2').hide();
                            $('.otherid2').hide();
                        } else if (thisss == 'Vendor') {
                            $('.clientid2').hide();
                            $('.employeeuserid2').hide();
                            $('.vendorid2').show();
                            $('.otherid2').hide();
                        } else if (thisss == 'Other') {
                            $('.clientid2').hide();
                            $('.employeeuserid2').hide();
                            $('.vendorid2').hide();
                            $('.otherid2').show();
                        }

                    }


                });
            });


            $(document).ready(function () {
                <?php
                if($infoCount > 0)
                {
                ?>
                var k = '<?php echo $infoCount;?>';
                <?php
                }
                else
                {
                ?>
                var k = 0;
                <?php
                }
                ?>
                $(".add-row").on("click", function () {
                    k++;
                    // alert(k);

                    // markup = "<div class='fieldGroupCopy_"+k+"''><div class='Branch'><h1>Secondary Contact Information</h1></div></div>";    
                    // markup = "<div class='fieldGroupCopy1'><div class='Branch'><h1>Secondary Contact Information</h1></div><div class='col-md-12 col-sm-12 col-xs-12'><div class='form-group'><label class='control-label col-md-3'>Name :</label><div class='col-md-6'><div class='row'><div class='col-md-3' style='width:17% !important;padding-right:0px;'><select type='text' class='form-control txtOnly' id='nametype_sec"+k+"' name='nametype_sec[]'><option value='mr'>Mr.</option><option value='mrs'>Mrs.</option><option value='miss'>Miss.</option></select></div><div class='col-md-4'><input type='text' class='form-control textonly' id='firstname_sec"+k+"' name='firstname_sec[]' placeholder='First Name'></div><div class='col-md-1' style='width:16% !important;'><div class='row'><input type='text' class='form-control textonly' id='middlename_sec"+k+"' name='middlename_sec[]' maxlength='1' placeholder='M'></div></div><div class=''><div class='col-md-4'><input type='text' class='form-control textonly' id='lastname_sec"+k+"' name='lastname_sec[]' placeholder='Last Name'></div></div></div></div><button type='button' class='delete-row btn btn-danger' onClick='deleterow();'>-</button></div><div class='form-group' style='margin-bottom:0px;'><div class='form-group'><label class='control-label col-md-3'>Title :</label><div class='col-md-3'><input type='text' class='form-control' id='sec_title"+k+"' name='sec_title[]'></div>  <div class='col-md-3'><input type='checkbox' class='check' value='1' id='user_access"+k+"' name='user_access[]'> <label for='user_access"+k+"'> User Access</label></div></div><div class='form-group'><label class='control-label col-md-3'>Address 1 :</label><div class='col-md-6'><input type='text' class='form-control' id='contact_address_sec_1"+k+"'  name='contact_address_sec_1[]' placeholder='Address 1'></div></div><div class='form-group'><label class='control-label col-md-3'>Address 2 :</label><div class='col-md-6'><input type='text' class='form-control' id='contact_address_sec_2"+k+"' name='contact_address_sec_2[]' placeholder='Address 2'></div></div><div class='form-group'><label class='control-label col-md-3'>City / State / Zip :</label><div class='col-md-2'><input name='city_sec[]' type='text' id='city_sec"+k+"' placeholder='City'  class='textonly form-control'></div><div class=''><div class='col-md-2'><select name='state_sec[]' id='state_sec"+k+"' class='form-control fsc-input'><option value=''>Select</option><option value='AK'>AK</option><option value='AS'>AS</option><option value='AZ'>AZ</option><option value='AR'>AR</option><option value='CA'>CA</option><option value='CO'>CO</option><option value='CT'>CT</option><option value='DE'>DE</option><option value='DC'>DC</option><option value='FM'>FM</option><option value='FL'>FL</option><option value='GA'>GA</option><option value='GU'>GU</option><option value='HI'>HI</option><option value='ID'>ID</option><option value='IL'>IL</option><option value='IN'>IN</option><option value='IA'>IA</option><option value='KS'>KS</option><option value='LA'>LA</option><option value='ME'>ME</option><option value='MH'>MH</option><option value='MD'>MD</option><option value='MA'>MA</option><option value='MI'>MI</option><option value='MN'>MN</option><option value='MS'>MS</option><option value='MO'>MO</option><option value='MT'>MT</option><option value='NE'>NE</option><option value='NV'>NV</option><option value='NH'>NH</option><option value='NJ'>NJ</option><option value='NM'>NM</option><option value='NY'>NY</option><option value='NC'>NC</option><option value='ND'>ND</option><option value='MP'>MP</option><option value='OH'>OH</option><option value='OK'>OK</option><option value='OR'>OR</option><option value='PW'>PW</option><option value='PA'>PA</option><option value='PR'>PR</option><option value='RI'>RI</option><option value='SC'>SC</option><option value='SD'>SD</option><option value='TN'>TN</option><option value='TX'>TX</option><option value='UT'>UT</option><option value='VT'>VT</option><option value='VI'>VI</option><option value='VA'>VA</option><option value='WA'>WA</option><option value='WV'>WV</option><option value='WI'>WI</option><option value='WY'>WY</option></select></div></div><div class=''><div class='col-md-2'><input name='zip_sec[]' placeholder='zip' type='text' id='zip_sec' class='form-control zip'></div></div></div><div class='form-group '><label class='control-label col-md-3'>Telephone No. 1:</label><div class='col-md-2'><input name='mobile_sec_1[]'  data-country='countries_states1' placeholder='(999) 999-9999' type='tel' id='mobile_sec_1' class='form-control twxzrt bfh-phone'></div><div class='col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin'><select name='mobiletype_sec_1[]' id='mobiletype_sec_1' class='form-control fsc-input mobiletype_sec' style='height:auto'><option value=''>Select</option><option value='Business'>Business</option><option value='Home'>Office</option><option value='Mobile'>Mobile</option><option value='Other'>Other</option></select></div><div class='col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin'><input class='form-control fsc-input ext_sec_1' id='ext_sec_1' maxlength='5' name='ext_sec_1[]' readonly placeholder='Ext' type='text'></div></div><div class='form-group '><label class='control-label col-md-3'>Telephone No. 2:</label><div class='col-md-2'><input name='mobile_sec_2[]' placeholder='(999) 999-9999' type='tel' id='mobile_sec_2' class='twxzrt form-control phone'></div><div class='col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin'><select name='mobiletype_sec_2[]' id='mobiletype_sec_2' class='form-control fsc-input mobiletype_sec2' style='height:auto'><option value=''>Select</option><option value='Business'>Business</option><option value='Home'>Office</option><option value='Mobile'>Mobile</option><option value='Other'>Other</option></select></div><div class='col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin'><input  type='text' class='form-control fsc-input ext_sec_2' id='ext_sec_2' name='ext_sec_2[]' maxlength='5' placeholder='Ext' readonly></div></div><div class='form-group' style='margin-bottom:0px;'><label class='control-label col-md-3'></label><div class='col-md-3'></div></div><div class='form-group'><label class='control-label col-md-3'>Fax No. :</label><div class='col-md-2'><input name='contact_fax_sec[]' placeholder='(999) 999-9999' type='tel' id='contact_fax_sec' class='contact_fax_sec twxzrt form-control'></div></div><div class='form-group'><div class='form-group' style='margin-bottom:0px;'><label class='control-label col-md-3'></label><div class='col-md-4'></div></div><label class='control-label col-md-3'>Email :</label><div class='col-md-4'><input type='text' class='form-control email_sec' id='email_sec' name='email_sec[]' placeholer='Email'></div></div><div class='col-lg-7 col-md-12 col-sm-12 col-xs-12 fsc-element-margin'></div></div></div>;    

                    document.getElementById("myTable").insertRow(-1).innerHTML = "<div class='fieldGroupCopy" + k + "'><div class='Branch'><h1>Secondary Contact Information</h1></div><div class='col-md-12 col-sm-12 col-xs-12'><div class='form-group'><label class='control-label col-md-3'>Name :</label><div class='col-lg-7 col-md-8'><div class='row'><div class='col-md-2 col-xs-4 p_cmn_991' style='padding-right:0px;'><select type='text' class='form-control txtOnly' id='nametype_sec" + k + "' name='nametype_sec[]'><option value='mr'>Mr.</option><option value='mrs'>Mrs.</option><option value='miss'>Miss.</option></select></div><div class='col-md-4 col-xs-8'><input type='text' class='form-control textonly' id='firstname_sec" + k + "' name='firstname_sec[]' placeholder='First Name'></div><div class='col-md-2 col-xs-4 p_cmn_991' style='padding:0px;'><div class=''><input type='text' class='form-control textonly' id='middlename_sec" + k + "' name='middlename_sec[]' maxlength='1' placeholder='M'></div></div><div class=''><div class='col-md-4 col-xs-8'><input type='text' class='form-control textonly' id='lastname_sec" + k + "' name='lastname_sec[]' placeholder='Last Name'></div></div></div></div><button type='button' class='delete-row btn btn-danger add_sec_info' onClick='deleterow();'>-</button></div><div class='' style='margin-bottom:0px;'><div class='form-group'><label class='control-label col-md-3'>Title :</label><div class='col-lg-7 col-md-8'><div class='row'><div class='col-md-5'><input type='text' class='form-control' id='sec_title" + k + "' name='sec_title[]'></div>  <div class='col-md-3'><input type='checkbox' class='check' value='1' id='user_access" + k + "' name='user_access[]'> <label for='user_access" + k + "'> User Access</label></div></div></div></div><div class='form-group'><label class='control-label col-md-3'>Address 1 :</label><div class='col-lg-7 col-md-8'><input type='text' class='form-control' id='contact_address_sec_1" + k + "'  name='contact_address_sec_1[]' placeholder='Address 1'></div></div><div class='form-group'><label class='control-label col-md-3'>Address 2 :</label><div class='col-lg-7 col-md-8'><input type='text' class='form-control' id='contact_address_sec_2" + k + "' name='contact_address_sec_2[]' placeholder='Address 2'></div></div><div class='form-group'><label class='control-label col-md-3'>City / State / Zip :</label><div class='col-lg-7 col-md-8'><div class='row'><div class='col-md-5'><input name='city_sec[]' type='text' id='city_sec" + k + "' placeholder='City'  class='textonly form-control'></div><div class=''><div class='col-md-3 col-xs-6'><select name='state_sec[]' id='state_sec" + k + "' class='form-control fsc-input'><option value=''>Select</option><option value='AK'>AK</option><option value='AS'>AS</option><option value='AZ'>AZ</option><option value='AR'>AR</option><option value='CA'>CA</option><option value='CO'>CO</option><option value='CT'>CT</option><option value='DE'>DE</option><option value='DC'>DC</option><option value='FM'>FM</option><option value='FL'>FL</option><option value='GA'>GA</option><option value='GU'>GU</option><option value='HI'>HI</option><option value='ID'>ID</option><option value='IL'>IL</option><option value='IN'>IN</option><option value='IA'>IA</option><option value='KS'>KS</option><option value='LA'>LA</option><option value='ME'>ME</option><option value='MH'>MH</option><option value='MD'>MD</option><option value='MA'>MA</option><option value='MI'>MI</option><option value='MN'>MN</option><option value='MS'>MS</option><option value='MO'>MO</option><option value='MT'>MT</option><option value='NE'>NE</option><option value='NV'>NV</option><option value='NH'>NH</option><option value='NJ'>NJ</option><option value='NM'>NM</option><option value='NY'>NY</option><option value='NC'>NC</option><option value='ND'>ND</option><option value='MP'>MP</option><option value='OH'>OH</option><option value='OK'>OK</option><option value='OR'>OR</option><option value='PW'>PW</option><option value='PA'>PA</option><option value='PR'>PR</option><option value='RI'>RI</option><option value='SC'>SC</option><option value='SD'>SD</option><option value='TN'>TN</option><option value='TX'>TX</option><option value='UT'>UT</option><option value='VT'>VT</option><option value='VI'>VI</option><option value='VA'>VA</option><option value='WA'>WA</option><option value='WV'>WV</option><option value='WI'>WI</option><option value='WY'>WY</option></select></div></div><div class=''><div class='col-md-4 col-xs-6'><input name='zip_sec[]' placeholder='zip' type='text' id='zip_sec' class='form-control zip'></div></div></div></div></div><div class='form-group '><label class='control-label col-md-3'>Telephone No. 1:</label><div class='col-lg-7 col-md-8'><div class='row'><div class='col-md-5'><input name='mobile_sec_1[]'  data-country='countries_states1' placeholder='(999) 999-9999' type='tel' id='mobile_sec_1' class='form-control twxzrt bfh-phone'></div><div class='col-md-3 col-xs-6 fsc-form-col fsc-element-margin'><select name='mobiletype_sec_1[]' id='mobiletype_sec_1' class='form-control fsc-input mobiletype_sec' style='height:auto'><option value=''>Select</option><option value='Business'>Business</option><option value='Office'>Office</option><option value='Resid'>Resid</option><option value='Mobile'>Mobile</option><option value='Other'>Other</option></select></div><div class='col-md-4 col-xs-6 fsc-element-margin'><input class='form-control fsc-input ext_sec_1' id='ext_sec_1' maxlength='5' name='ext_sec_1[]' readonly placeholder='Ext' type='text'></div></div></div></div><div class='form-group'><label class='control-label col-md-3'>Telephone No. 2:</label><div class='col-lg-7 col-md-8'><div class='row'><div class='col-md-5'><input name='mobile_sec_2[]' placeholder='(999) 999-9999' type='tel' id='mobile_sec_2' class='twxzrt form-control phone'></div><div class='col-md-3 col-xs-6 fsc-form-col fsc-element-margin'><select name='mobiletype_sec_2[]' id='mobiletype_sec_2' class='form-control fsc-input mobiletype_sec2' style='height:auto'><option value=''>Select</option><option value='Business'>Business</option><option value='Resid'>Resid</option><option value='Office'>Office</option><option value='Mobile'>Mobile</option><option value='Other'>Other</option></select></div><div class='col-md-4 col-xs-6 fsc-element-margin'><input  type='text' class='form-control fsc-input ext_sec_2' id='ext_sec_2' name='ext_sec_2[]' maxlength='5' placeholder='Ext' readonly></div></div></div></div><div class='form-group'><label class='control-label col-md-3'>Fax No. :</label><div class='col-lg-7 col-md-8'><div class='row'><div class='col-md-5'><input name='contact_fax_sec[]' placeholder='(999) 999-9999' type='tel' id='contact_fax_sec' class='contact_fax_sec twxzrt form-control'></div></div></div></div><div class='form-group'><label class='control-label col-md-3'>Email :</label><div class='col-lg-7 col-md-8'><input type='text' class='form-control email_sec' id='email_sec' name='email_sec[]' placeholer='Email'></div></div><div class='col-lg-7 col-md-12 col-sm-12 col-xs-12 fsc-element-margin'></div></div></div>";

                    //$("#mytable").append(markup);
                    //var minNumber = -100;
                    //var maxNumber = 100;
                });
            });


            $("body").on("click", ".delete-row", function () {
                $(this).parents(".fieldGroupCopy" + k + "'").remove();

                //$('#myTable').append('<tr><td><input type="text" class="fname" /></td><td><input type="button" value="Delete" /></td></tr>')
            });


            // function displayResult()
            // {
            //     <?php
            //     if($infoCount>0)
            //     {
            //         ?>
            //         var k='<?php echo $infoCount;?>';
            //         <?php
            //     }
            //     else
            //     {
            //         ?>
            //         var k=0;
            //         <?php
            //     }
            //     ?>
            //     // var k='<?php //echo $infoCount;?>';
            //     //$(".add-row").click(function(){
            //         k++;
            //          alert(k);

            //       // markup = "<div class='fieldGroupCopy_"+k+"''><div class='Branch'><h1>Secondary Contact Information</h1></div></div>";    
            //         // markup = "<div class='fieldGroupCopy1'><div class='Branch'><h1>Secondary Contact Information</h1></div><div class='col-md-12 col-sm-12 col-xs-12'><div class='form-group'><label class='control-label col-md-3'>Name :</label><div class='col-md-6'><div class='row'><div class='col-md-3' style='width:17% !important;padding-right:0px;'><select type='text' class='form-control txtOnly' id='nametype_sec"+k+"' name='nametype_sec[]'><option value='mr'>Mr.</option><option value='mrs'>Mrs.</option><option value='miss'>Miss.</option></select></div><div class='col-md-4'><input type='text' class='form-control textonly' id='firstname_sec"+k+"' name='firstname_sec[]' placeholder='First Name'></div><div class='col-md-1' style='width:16% !important;'><div class='row'><input type='text' class='form-control textonly' id='middlename_sec"+k+"' name='middlename_sec[]' maxlength='1' placeholder='M'></div></div><div class=''><div class='col-md-4'><input type='text' class='form-control textonly' id='lastname_sec"+k+"' name='lastname_sec[]' placeholder='Last Name'></div></div></div></div><button type='button' class='delete-row btn btn-danger' onClick='deleterow();'>-</button></div><div class='form-group' style='margin-bottom:0px;'><div class='form-group'><label class='control-label col-md-3'>Title :</label><div class='col-md-3'><input type='text' class='form-control' id='sec_title"+k+"' name='sec_title[]'></div>  <div class='col-md-3'><input type='checkbox' class='check' value='1' id='user_access"+k+"' name='user_access[]'> <label for='user_access"+k+"'> User Access</label></div></div><div class='form-group'><label class='control-label col-md-3'>Address 1 :</label><div class='col-md-6'><input type='text' class='form-control' id='contact_address_sec_1"+k+"'  name='contact_address_sec_1[]' placeholder='Address 1'></div></div><div class='form-group'><label class='control-label col-md-3'>Address 2 :</label><div class='col-md-6'><input type='text' class='form-control' id='contact_address_sec_2"+k+"' name='contact_address_sec_2[]' placeholder='Address 2'></div></div><div class='form-group'><label class='control-label col-md-3'>City / State / Zip :</label><div class='col-md-2'><input name='city_sec[]' type='text' id='city_sec"+k+"' placeholder='City'  class='textonly form-control'></div><div class=''><div class='col-md-2'><select name='state_sec[]' id='state_sec"+k+"' class='form-control fsc-input'><option value=''>Select</option><option value='AK'>AK</option><option value='AS'>AS</option><option value='AZ'>AZ</option><option value='AR'>AR</option><option value='CA'>CA</option><option value='CO'>CO</option><option value='CT'>CT</option><option value='DE'>DE</option><option value='DC'>DC</option><option value='FM'>FM</option><option value='FL'>FL</option><option value='GA'>GA</option><option value='GU'>GU</option><option value='HI'>HI</option><option value='ID'>ID</option><option value='IL'>IL</option><option value='IN'>IN</option><option value='IA'>IA</option><option value='KS'>KS</option><option value='LA'>LA</option><option value='ME'>ME</option><option value='MH'>MH</option><option value='MD'>MD</option><option value='MA'>MA</option><option value='MI'>MI</option><option value='MN'>MN</option><option value='MS'>MS</option><option value='MO'>MO</option><option value='MT'>MT</option><option value='NE'>NE</option><option value='NV'>NV</option><option value='NH'>NH</option><option value='NJ'>NJ</option><option value='NM'>NM</option><option value='NY'>NY</option><option value='NC'>NC</option><option value='ND'>ND</option><option value='MP'>MP</option><option value='OH'>OH</option><option value='OK'>OK</option><option value='OR'>OR</option><option value='PW'>PW</option><option value='PA'>PA</option><option value='PR'>PR</option><option value='RI'>RI</option><option value='SC'>SC</option><option value='SD'>SD</option><option value='TN'>TN</option><option value='TX'>TX</option><option value='UT'>UT</option><option value='VT'>VT</option><option value='VI'>VI</option><option value='VA'>VA</option><option value='WA'>WA</option><option value='WV'>WV</option><option value='WI'>WI</option><option value='WY'>WY</option></select></div></div><div class=''><div class='col-md-2'><input name='zip_sec[]' placeholder='zip' type='text' id='zip_sec' class='form-control zip'></div></div></div><div class='form-group '><label class='control-label col-md-3'>Telephone No. 1:</label><div class='col-md-2'><input name='mobile_sec_1[]'  data-country='countries_states1' placeholder='(999) 999-9999' type='tel' id='mobile_sec_1' class='form-control twxzrt bfh-phone'></div><div class='col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin'><select name='mobiletype_sec_1[]' id='mobiletype_sec_1' class='form-control fsc-input mobiletype_sec' style='height:auto'><option value=''>Select</option><option value='Business'>Business</option><option value='Home'>Office</option><option value='Mobile'>Mobile</option><option value='Other'>Other</option></select></div><div class='col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin'><input class='form-control fsc-input ext_sec_1' id='ext_sec_1' maxlength='5' name='ext_sec_1[]' readonly placeholder='Ext' type='text'></div></div><div class='form-group '><label class='control-label col-md-3'>Telephone No. 2:</label><div class='col-md-2'><input name='mobile_sec_2[]' placeholder='(999) 999-9999' type='tel' id='mobile_sec_2' class='twxzrt form-control phone'></div><div class='col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin'><select name='mobiletype_sec_2[]' id='mobiletype_sec_2' class='form-control fsc-input mobiletype_sec2' style='height:auto'><option value=''>Select</option><option value='Business'>Business</option><option value='Home'>Office</option><option value='Mobile'>Mobile</option><option value='Other'>Other</option></select></div><div class='col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin'><input  type='text' class='form-control fsc-input ext_sec_2' id='ext_sec_2' name='ext_sec_2[]' maxlength='5' placeholder='Ext' readonly></div></div><div class='form-group' style='margin-bottom:0px;'><label class='control-label col-md-3'></label><div class='col-md-3'></div></div><div class='form-group'><label class='control-label col-md-3'>Fax No. :</label><div class='col-md-2'><input name='contact_fax_sec[]' placeholder='(999) 999-9999' type='tel' id='contact_fax_sec' class='contact_fax_sec twxzrt form-control'></div></div><div class='form-group'><div class='form-group' style='margin-bottom:0px;'><label class='control-label col-md-3'></label><div class='col-md-4'></div></div><label class='control-label col-md-3'>Email :</label><div class='col-md-4'><input type='text' class='form-control email_sec' id='email_sec' name='email_sec[]' placeholer='Email'></div></div><div class='col-lg-7 col-md-12 col-sm-12 col-xs-12 fsc-element-margin'></div></div></div>";    

            //          document.getElementById("myTable").insertRow(-1).innerHTML = "<div class='fieldGroupCopy1'><div class='Branch'><h1>Secondary Contact Information</h1></div><div class='col-md-12 col-sm-12 col-xs-12'><div class='form-group'><label class='control-label col-md-3'>Name :</label><div class='col-md-6'><div class='row'><div class='col-md-3' style='width:17% !important;padding-right:0px;'><select type='text' class='form-control txtOnly' id='nametype_sec"+k+"' name='nametype_sec[]'><option value='mr'>Mr.</option><option value='mrs'>Mrs.</option><option value='miss'>Miss.</option></select></div><div class='col-md-4'><input type='text' class='form-control textonly' id='firstname_sec"+k+"' name='firstname_sec[]' placeholder='First Name'></div><div class='col-md-1' style='width:16% !important;'><div class='row'><input type='text' class='form-control textonly' id='middlename_sec"+k+"' name='middlename_sec[]' maxlength='1' placeholder='M'></div></div><div class=''><div class='col-md-4'><input type='text' class='form-control textonly' id='lastname_sec"+k+"' name='lastname_sec[]' placeholder='Last Name'></div></div></div></div><button type='button' class='delete-row btn btn-danger' onClick='deleterow();'>-</button></div><div class='form-group' style='margin-bottom:0px;'><div class='form-group'><label class='control-label col-md-3'>Title :</label><div class='col-md-3'><input type='text' class='form-control' id='sec_title"+k+"' name='sec_title[]'></div>  <div class='col-md-3'><input type='checkbox' class='check' value='1' id='user_access"+k+"' name='user_access[]'> <label for='user_access"+k+"'> User Access</label></div></div><div class='form-group'><label class='control-label col-md-3'>Address 1 :</label><div class='col-md-6'><input type='text' class='form-control' id='contact_address_sec_1"+k+"'  name='contact_address_sec_1[]' placeholder='Address 1'></div></div><div class='form-group'><label class='control-label col-md-3'>Address 2 :</label><div class='col-md-6'><input type='text' class='form-control' id='contact_address_sec_2"+k+"' name='contact_address_sec_2[]' placeholder='Address 2'></div></div><div class='form-group'><label class='control-label col-md-3'>City / State / Zip :</label><div class='col-md-2'><input name='city_sec[]' type='text' id='city_sec"+k+"' placeholder='City'  class='textonly form-control'></div><div class=''><div class='col-md-2'><select name='state_sec[]' id='state_sec"+k+"' class='form-control fsc-input'><option value=''>Select</option><option value='AK'>AK</option><option value='AS'>AS</option><option value='AZ'>AZ</option><option value='AR'>AR</option><option value='CA'>CA</option><option value='CO'>CO</option><option value='CT'>CT</option><option value='DE'>DE</option><option value='DC'>DC</option><option value='FM'>FM</option><option value='FL'>FL</option><option value='GA'>GA</option><option value='GU'>GU</option><option value='HI'>HI</option><option value='ID'>ID</option><option value='IL'>IL</option><option value='IN'>IN</option><option value='IA'>IA</option><option value='KS'>KS</option><option value='LA'>LA</option><option value='ME'>ME</option><option value='MH'>MH</option><option value='MD'>MD</option><option value='MA'>MA</option><option value='MI'>MI</option><option value='MN'>MN</option><option value='MS'>MS</option><option value='MO'>MO</option><option value='MT'>MT</option><option value='NE'>NE</option><option value='NV'>NV</option><option value='NH'>NH</option><option value='NJ'>NJ</option><option value='NM'>NM</option><option value='NY'>NY</option><option value='NC'>NC</option><option value='ND'>ND</option><option value='MP'>MP</option><option value='OH'>OH</option><option value='OK'>OK</option><option value='OR'>OR</option><option value='PW'>PW</option><option value='PA'>PA</option><option value='PR'>PR</option><option value='RI'>RI</option><option value='SC'>SC</option><option value='SD'>SD</option><option value='TN'>TN</option><option value='TX'>TX</option><option value='UT'>UT</option><option value='VT'>VT</option><option value='VI'>VI</option><option value='VA'>VA</option><option value='WA'>WA</option><option value='WV'>WV</option><option value='WI'>WI</option><option value='WY'>WY</option></select></div></div><div class=''><div class='col-md-2'><input name='zip_sec[]' placeholder='zip' type='text' id='zip_sec' class='form-control zip'></div></div></div><div class='form-group '><label class='control-label col-md-3'>Telephone No. 1:</label><div class='col-md-2'><input name='mobile_sec_1[]'  data-country='countries_states1' placeholder='(999) 999-9999' type='tel' id='mobile_sec_1' class='form-control twxzrt bfh-phone'></div><div class='col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin'><select name='mobiletype_sec_1[]' id='mobiletype_sec_1' class='form-control fsc-input mobiletype_sec' style='height:auto'><option value=''>Select</option><option value='Business'>Business</option><option value='Home'>Office</option><option value='Mobile'>Mobile</option><option value='Other'>Other</option></select></div><div class='col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin'><input class='form-control fsc-input ext_sec_1' id='ext_sec_1' maxlength='5' name='ext_sec_1[]' readonly placeholder='Ext' type='text'></div></div><div class='form-group '><label class='control-label col-md-3'>Telephone No. 2:</label><div class='col-md-2'><input name='mobile_sec_2[]' placeholder='(999) 999-9999' type='tel' id='mobile_sec_2' class='twxzrt form-control phone'></div><div class='col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin'><select name='mobiletype_sec_2[]' id='mobiletype_sec_2' class='form-control fsc-input mobiletype_sec2' style='height:auto'><option value=''>Select</option><option value='Business'>Business</option><option value='Home'>Office</option><option value='Mobile'>Mobile</option><option value='Other'>Other</option></select></div><div class='col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin'><input  type='text' class='form-control fsc-input ext_sec_2' id='ext_sec_2' name='ext_sec_2[]' maxlength='5' placeholder='Ext' readonly></div></div><div class='form-group' style='margin-bottom:0px;'><label class='control-label col-md-3'></label><div class='col-md-3'></div></div><div class='form-group'><label class='control-label col-md-3'>Fax No. :</label><div class='col-md-2'><input name='contact_fax_sec[]' placeholder='(999) 999-9999' type='tel' id='contact_fax_sec' class='contact_fax_sec twxzrt form-control'></div></div><div class='form-group'><div class='form-group' style='margin-bottom:0px;'><label class='control-label col-md-3'></label><div class='col-md-4'></div></div><label class='control-label col-md-3'>Email :</label><div class='col-md-4'><input type='text' class='form-control email_sec' id='email_sec' name='email_sec[]' placeholer='Email'></div></div><div class='col-lg-7 col-md-12 col-sm-12 col-xs-12 fsc-element-margin'></div></div></div>";            

            //     //    $("#mytable").append(markup);
            //       //  var minNumber = -100;
            //     //    var maxNumber = 100;
            //     //});
            // } 


            //   $("body").on("click",".delete-row",function(){ 
            //     $(this).parents(".fieldGroupCopy_"+k+"'").remove();
            // });


            // $(document).ready(function()
            //  {
            //         <?php
            //         if($infoCount>0)
            //         {
            //             ?>
            //             var k='<?php //echo $infoCount;?>';
            //             <?php
            //         }
            //         else
            //         {
            //             ?>
            //             var k=0;
            //             <?php
            //         }
            //         ?>
            //         // var k='<?php //echo $infoCount;?>';
            //         $(".add-row").click(function(){
            //             k++;
            //              alert(k);

            //           // markup = "<div class='fieldGroupCopy_"+k+"''><div class='Branch'><h1>Secondary Contact Information</h1></div></div>";    
            //             // markup = "<div class='fieldGroupCopy1'><div class='Branch'><h1>Secondary Contact Information</h1></div><div class='col-md-12 col-sm-12 col-xs-12'><div class='form-group'><label class='control-label col-md-3'>Name :</label><div class='col-md-6'><div class='row'><div class='col-md-3' style='width:17% !important;padding-right:0px;'><select type='text' class='form-control txtOnly' id='nametype_sec"+k+"' name='nametype_sec[]'><option value='mr'>Mr.</option><option value='mrs'>Mrs.</option><option value='miss'>Miss.</option></select></div><div class='col-md-4'><input type='text' class='form-control textonly' id='firstname_sec"+k+"' name='firstname_sec[]' placeholder='First Name'></div><div class='col-md-1' style='width:16% !important;'><div class='row'><input type='text' class='form-control textonly' id='middlename_sec"+k+"' name='middlename_sec[]' maxlength='1' placeholder='M'></div></div><div class=''><div class='col-md-4'><input type='text' class='form-control textonly' id='lastname_sec"+k+"' name='lastname_sec[]' placeholder='Last Name'></div></div></div></div><button type='button' class='delete-row btn btn-danger' onClick='deleterow();'>-</button></div><div class='form-group' style='margin-bottom:0px;'><div class='form-group'><label class='control-label col-md-3'>Title :</label><div class='col-md-3'><input type='text' class='form-control' id='sec_title"+k+"' name='sec_title[]'></div>  <div class='col-md-3'><input type='checkbox' class='check' value='1' id='user_access"+k+"' name='user_access[]'> <label for='user_access"+k+"'> User Access</label></div></div><div class='form-group'><label class='control-label col-md-3'>Address 1 :</label><div class='col-md-6'><input type='text' class='form-control' id='contact_address_sec_1"+k+"'  name='contact_address_sec_1[]' placeholder='Address 1'></div></div><div class='form-group'><label class='control-label col-md-3'>Address 2 :</label><div class='col-md-6'><input type='text' class='form-control' id='contact_address_sec_2"+k+"' name='contact_address_sec_2[]' placeholder='Address 2'></div></div><div class='form-group'><label class='control-label col-md-3'>City / State / Zip :</label><div class='col-md-2'><input name='city_sec[]' type='text' id='city_sec"+k+"' placeholder='City'  class='textonly form-control'></div><div class=''><div class='col-md-2'><select name='state_sec[]' id='state_sec"+k+"' class='form-control fsc-input'><option value=''>Select</option><option value='AK'>AK</option><option value='AS'>AS</option><option value='AZ'>AZ</option><option value='AR'>AR</option><option value='CA'>CA</option><option value='CO'>CO</option><option value='CT'>CT</option><option value='DE'>DE</option><option value='DC'>DC</option><option value='FM'>FM</option><option value='FL'>FL</option><option value='GA'>GA</option><option value='GU'>GU</option><option value='HI'>HI</option><option value='ID'>ID</option><option value='IL'>IL</option><option value='IN'>IN</option><option value='IA'>IA</option><option value='KS'>KS</option><option value='LA'>LA</option><option value='ME'>ME</option><option value='MH'>MH</option><option value='MD'>MD</option><option value='MA'>MA</option><option value='MI'>MI</option><option value='MN'>MN</option><option value='MS'>MS</option><option value='MO'>MO</option><option value='MT'>MT</option><option value='NE'>NE</option><option value='NV'>NV</option><option value='NH'>NH</option><option value='NJ'>NJ</option><option value='NM'>NM</option><option value='NY'>NY</option><option value='NC'>NC</option><option value='ND'>ND</option><option value='MP'>MP</option><option value='OH'>OH</option><option value='OK'>OK</option><option value='OR'>OR</option><option value='PW'>PW</option><option value='PA'>PA</option><option value='PR'>PR</option><option value='RI'>RI</option><option value='SC'>SC</option><option value='SD'>SD</option><option value='TN'>TN</option><option value='TX'>TX</option><option value='UT'>UT</option><option value='VT'>VT</option><option value='VI'>VI</option><option value='VA'>VA</option><option value='WA'>WA</option><option value='WV'>WV</option><option value='WI'>WI</option><option value='WY'>WY</option></select></div></div><div class=''><div class='col-md-2'><input name='zip_sec[]' placeholder='zip' type='text' id='zip_sec' class='form-control zip'></div></div></div><div class='form-group '><label class='control-label col-md-3'>Telephone No. 1:</label><div class='col-md-2'><input name='mobile_sec_1[]'  data-country='countries_states1' placeholder='(999) 999-9999' type='tel' id='mobile_sec_1' class='form-control twxzrt bfh-phone'></div><div class='col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin'><select name='mobiletype_sec_1[]' id='mobiletype_sec_1' class='form-control fsc-input mobiletype_sec' style='height:auto'><option value=''>Select</option><option value='Business'>Business</option><option value='Home'>Office</option><option value='Mobile'>Mobile</option><option value='Other'>Other</option></select></div><div class='col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin'><input class='form-control fsc-input ext_sec_1' id='ext_sec_1' maxlength='5' name='ext_sec_1[]' readonly placeholder='Ext' type='text'></div></div><div class='form-group '><label class='control-label col-md-3'>Telephone No. 2:</label><div class='col-md-2'><input name='mobile_sec_2[]' placeholder='(999) 999-9999' type='tel' id='mobile_sec_2' class='twxzrt form-control phone'></div><div class='col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin'><select name='mobiletype_sec_2[]' id='mobiletype_sec_2' class='form-control fsc-input mobiletype_sec2' style='height:auto'><option value=''>Select</option><option value='Business'>Business</option><option value='Home'>Office</option><option value='Mobile'>Mobile</option><option value='Other'>Other</option></select></div><div class='col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin'><input  type='text' class='form-control fsc-input ext_sec_2' id='ext_sec_2' name='ext_sec_2[]' maxlength='5' placeholder='Ext' readonly></div></div><div class='form-group' style='margin-bottom:0px;'><label class='control-label col-md-3'></label><div class='col-md-3'></div></div><div class='form-group'><label class='control-label col-md-3'>Fax No. :</label><div class='col-md-2'><input name='contact_fax_sec[]' placeholder='(999) 999-9999' type='tel' id='contact_fax_sec' class='contact_fax_sec twxzrt form-control'></div></div><div class='form-group'><div class='form-group' style='margin-bottom:0px;'><label class='control-label col-md-3'></label><div class='col-md-4'></div></div><label class='control-label col-md-3'>Email :</label><div class='col-md-4'><input type='text' class='form-control email_sec' id='email_sec' name='email_sec[]' placeholer='Email'></div></div><div class='col-lg-7 col-md-12 col-sm-12 col-xs-12 fsc-element-margin'></div></div></div>";    

            //              document.getElementById("myTable").insertRow(-1).innerHTML = "<div class='fieldGroupCopy'><div class='Branch'><h1>Secondary Contact Information</h1></div><div class='col-md-12 col-sm-12 col-xs-12'><div class='form-group'><label class='control-label col-md-3'>Name :</label><div class='col-md-6'><div class='row'><div class='col-md-3' style='width:17% !important;padding-right:0px;'><select type='text' class='form-control txtOnly' id='nametype_sec"+k+"' name='nametype_sec[]'><option value='mr'>Mr.</option><option value='mrs'>Mrs.</option><option value='miss'>Miss.</option></select></div><div class='col-md-4'><input type='text' class='form-control textonly' id='firstname_sec"+k+"' name='firstname_sec[]' placeholder='First Name'></div><div class='col-md-1' style='width:16% !important;'><div class='row'><input type='text' class='form-control textonly' id='middlename_sec"+k+"' name='middlename_sec[]' maxlength='1' placeholder='M'></div></div><div class=''><div class='col-md-4'><input type='text' class='form-control textonly' id='lastname_sec"+k+"' name='lastname_sec[]' placeholder='Last Name'></div></div></div></div><button type='button' class='delete-row btn btn-danger' onClick='deleterow();'>-</button></div></div>";            

            //             $("#mytable").append(markup);
            //             var minNumber = -100;
            //             var maxNumber = 100;
            //         });
            //     }); 


            //       $("body").on("click",".delete-row",function(){ 
            //         $(this).parents(".fieldGroupCopy_"+k+"'").remove();
            //     });
        </script>


        <script>
            function emailbl12(f) { //alert();
                if (f.emailbli2.checked == true) {// alert();
                    $('.email_sec').val(f.email.value);
                } else {
                    $('.email_sec').val('');
                }

            }

            function faxbli23(f) {// alert();
                if (f.faxbli2.checked == true) { //alert(f.business_fax.value);
                    $('.contact_fax_sec').val(f.business_fax.value);
                } else {
                    $('.contact_fax_sec').val('');
                }

            }
        </script>
        <script>
            $(document).ready(function () {


                $(document).on('change', '#paymentmode', function () {
                    var payment = $(this).val();//alert(payment);
                    if (payment == 'ACH') {
                        $(".ACH").show();
                        $(".ACH1").hide();
                    } else if (payment == 'CreditCard') {
                        $(".ACH1").show();
                        $(".ACH").hide();
                    } else {
                        $(".ACH").hide();
                        $(".ACH1").hide();
                    }
                });


                $(document).on('change', '#paymentmode11', function () {
                    var payment = $(this).val();//alert(payment);
                    if (payment == 'ACH') {
                        $(".ACH3").show();
                        $(".ACH2").hide();
                    } else if (payment == 'CreditCard') {
                        $(".ACH2").show();
                        $(".ACH3").hide();
                    } else {
                        $(".ACH3").hide();
                        $(".ACH2").hide();
                    }
                });


                $(document).on('change', '#paymentmode1', function () {
                    var payment = $(this).val();//alert(payment);
                    if (payment == 'CreditCard') {
                        $(".ACH1").show();
                    } else {
                        $(".ACH1").hide();
                    }
                });
            });


            $('select').on('change', function () {
                $('#text1').val(this.value);
            });

            jQuery(function ($) {
                var input = $('input,textarea,select,checkbox');
                input.on('keydown', function () {
                    var key = event.keyCode || event.charCode || event.which;
                    if (key == 8 || key == 46) {
                        $('#text1').val(key);
                    } else {
                        $('#text1').val(key);
                    }
                });
                $(".check").click(function () { //alert();
                    $("#text1").val('121');
                });
                $(".check").click(function () { //alert();
                    $("#text1").prop("checked", false);
                });

            });
            $(document).ready(function () {


                $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                    var currentTab = $(e.target).text(); // get current tab
                    var vf = $('#text1').val();//alert(vf);
                    var current_tab = e.target;
                    var previousTab = $(e.relatedTarget).text();
                    var target = $(e.relatedTarget).attr("href");
                    $('.no-button').eq(0).attr('href', target);
                    var href = $('.no-button').attr('href');

                    if (target == href) {
                        if (vf) {// alert('2');
                            $("#alerts").modal({
                                show: true,
                            });

                        } else {

                        }
                    } else {
                    }

                });
            });

            $(function () {

                $('#login-form-link').click(function (e) {
                    $("#login-form").delay(100).fadeIn(100);
                    $("#register-form").fadeOut(100);
                    $('#register-form-link').removeClass('active');
                    $(this).addClass('active');
                    e.preventDefault();
                });
                $('#register-form-link').click(function (e) {
                    $("#register-form").delay(100).fadeIn(100);
                    $("#login-form").fadeOut(100);
                    $('#login-form-link').removeClass('active');
                    $(this).addClass('active');
                    e.preventDefault();
                });

            });


            // $(document).on('click', '.no-button', function () {
            //  var href = $(this).attr('href');
            // var $link = $('li.active a[data-toggle="tab"]');
            //  $link.parent().removeClass('active');
            //  var tabLink = $link.attr('href');//alert(tabLink);
            //   $('#alerts').modal('hide');
            //  $('#myTab a[href="' + href + '"]').tab('show');

            // });


            $(document).ready(function () {
                $(".twxzrt").keypress(function (e) {// alert();
                    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                        return false;
                    }
                    var curchr = this.value.length;
                    var curval = $(this).val();
                    if (curchr == 3 && curval.indexOf("(") <= -1) {
                        $(this).val("(" + curval + ")" + " ");
                    } else if (curchr == 4 && curval.indexOf("(") > -1) {
                        $(this).val(curval + ")-");
                    } else if (curchr == 5 && curval.indexOf(")") > -1) {
                        $(this).val(curval + "-");
                    } else if (curchr == 9) {
                        $(this).val(curval + "-");
                        $(this).attr('maxlength', '14');
                    }
                });
                var date = $('.ext_sec_2').val();
                $('.mobiletype_sec2').on('change', function () {
                    if (this.value == 'Home') {
                        //alert();
                        //document.getElementById('#ext_sec_35').removeAttribute('readonly');
                        $(".ext_sec_2").removeAttr("readonly");
                        $('.ext_sec_2').val();
                    } else if (this.value == 'Business') {
                        $(".ext_sec_2").removeAttr("readonly");
                        $('.ext_sec_2').val();
                    } else {
                        $(".ext_sec_2").attr("readonly", true);
                        $('.ext_sec_2').val('');
                    }
                });
            });

            $(document).ready(function () {
                $(document).on('change', '.typeofwork_1', function () {
                    var thiss = $(this).val();
                    //alert(thiss);
                    if (thiss == 'Payroll') {
                        $('.taxxation1').show();

                        $('.acsoftware option:selected').removeAttr('selected');
                        $('.proll').show();
                        $('.taxxation').hide();
                        $('.acount').hide();

                    } else if (thiss == 'Accounting') {
                        $('.taxxation1').show();

                        $('.acsoftware option:selected').removeAttr('selected');
                        $('.proll').hide();
                        $('.taxxation').hide();
                        $('.acount').show();

                    } else if (thiss == 'Taxation') {
                        $('.acsoftware option:selected').removeAttr('selected');

                        $('.taxxation').show();
                        $('.taxxation1').hide();
                        $('.proll').hide();
                        $('.acount').hide();

                    }

                });
                $(document).on('change', '#pricetype', function () { ///();
                    var id = $(this).val();
                    if (id == 'Regular') {
                        //   $('.service').show();
                    } else if (id == 'Combo') {
                        //   $('.service').show();
                    } else if (id == '1') {
                        //    $('.service').hide();
                    } else {
                        //  $('.service').hide();   
                    }
                });
            });

            function show1() {
                document.getElementById('div1').style.display = 'none';
            }

            function show2() {
                document.getElementById('div1').style.display = 'block';
            }
        </script>
        <script>
            $(document).ready(function () {
                var date = $('.ext_sec_1').val();
                $('.mobiletype_sec').on('change', function () {
                    if (this.value == 'Home') {
                        //alert();
                        //document.getElementById('#ext_sec_35').removeAttribute('readonly');
                        $(".ext_sec_1").removeAttr("readonly");
                        $('.ext_sec_1').val();
                    } else if (this.value == 'Business') {
                        $(".ext_sec_1").removeAttr("readonly");
                        $('.ext_sec_1').val();
                    } else {
                        $(".ext_sec_1").attr("readonly", true);
                        $('.ext_sec_1').val('');
                    }
                });
            });

            $('.js-example-tags').select2({
                tags: true,
                tokenSeparators: [",", " "]
            });
        </script>

        <script>
            $(document).ready(function () {
                $('#serviceperiod').on('change', function () {
                    if (this.value == '7') {
                        $(".taxservicemonth").css("visibility", "hidden");
                    } else {
                        $(".taxservicemonth").css("visibility", "visible");
                    }
                });
            });
            $(document).ready(function () {

                $(document).on('change', '.tax_service_1', function () {


                    var id = $(this).val();
                    if (id == '3' || id == '2') {
                        $('.tax_period_1').html('<option value="Monthly">Monthly</option>');
                        $('.text_oneoption').removeAttr('disabled');

                        // $('.tax_period_1').hide();
                        // $('.tax_period_1').attr("disabled", "disabled");
                        $('.text_oneoption').show();
                        // $('.text_oneoption').val('Monthly');


                    } else if (id == '4' || id == '8') {
                        $('.text_oneoption').removeAttr('disabled');
                        //alert();
                        $('.tax_period_1').html('<option value="Quarterly">Quarterly</option>');
                        // $('.tax_period_1').hide();
                        //$('.tax_period_1').attr("disabled", "disabled");
                        $('.text_oneoption').show();
                        //  $('.tax_period_1').html('<option value="Quarterly">Quarterly</option>');
                    } else if (id == '1') {
                        // $('.tax_period_1').show();
                        $('.tax_period_1').removeAttr('disabled');
                        $('.tax_period_1').html('<option value="Monthly">Monthly</option><option value="Quarterly">Quarterly</option><option value="Annually">Annually</option>');
                        $('.text_oneoption').hide();
                        $('.text_oneoption').attr("disabled", "disabled");

                    } else if (id == '6' || id == '5') {
                        $('.tax_period_1').html('<option value="Annually">Annually</option>');

                        $('.taxservicemonth').css("visibility", "hidden");
                        //    $('.tax_period_1').attr("disabled", "disabled");

                        $('.text_oneoption').show();
                        $('.text_oneoption').removeAttr('disabled');

                        $('.text_oneoption').val('Annually');
                    } else if (id == '7') {
                        $('.tax_period_1').html('<option value="Annually">Annually</option>');
                        //$('.yearshide').show();
                        //   $('.tax_period_1').hide();
                        //            $('.tax_period_1').attr("disabled", "disabled");

                        $('.text_oneoption').show();
                        $('.text_oneoption').removeAttr('disabled');

                        $('.text_oneoption').val('Annually');
                    }


                });

                $(document).on('change', '#business_id', function () {
                    var id = $(this).val();
                    if (id == '6') {
                        $('.naicss').hide();

                    } else {
                        $('.naicss').show();
                    }

                });

                $(document).on('change', '.category', function () {
                    var id = $(this)//.val();
                    $('#image1').empty('');
                    if (id == '6') {
                        $('.personal').show();
                        $('.cat1').hide();
                        $('.cat2').hide();
                        $('.cat3').hide();
                    } else {
                        $('.personal').hide();

                    }
                });
            });
        </script>


        <script>
            $(document).on('change', '.banknames', function () {
                var thiss = $(this).val();
                //  alert(thiss);
                $.get('<?php echo URL::to('getbankdata'); ?>?id=' + thiss, function (data) {
                    //alert(data.fourdigit);
                    //	$.each(data, function(index, subcatobj)
                    //	{

                    // alert(data.routingno);
                    $('#routingno').val(data.routingno);
                    $('#acountno').val(data.fourdigit);
                    $('#acountname').val(data.nick_name);


                    //});
                });

            });

            $(document).on('change', '.banknames1', function () {
                var thiss = $(this).val();
                //  alert(thiss);
                $.get('<?php echo URL::to('getbankdata'); ?>?id=' + thiss, function (data) {
                    //alert(data.fourdigit);
                    //	$.each(data, function(index, subcatobj)
                    //	{

                    // alert(data.routingno);
                    $('#routingno1').val(data.routingno);
                    $('#acountno1').val(data.fourdigit);
                    $('#acountname1').val(data.nick_name);


                    //});
                });

            });

            $(document).on('change', '.accounting_period', function () {
                var id = $(this).val();
                $('.acperiods1').val(id);
                if (id == '') {
                    $(".services2").attr("disabled", 'disabled');
                    $('.services2').hide();

                } else {
                    $('.services2').show();
                    $(".services2").attr("disabled", false);

                }
                var result = id.split('-');
                var currency = $("#currency option:selected").val();
                var typeofservice = result[1];
                var serviceperiod = $("#serviceperiod  option:selected").val();
                var totalprice1 = $(".totalprice1").val();
                var pricing = $("#pricetype  option:selected").val();
                //	alert(pricing);
                var ckq = $(".ckq").val();//alert(typeofservice);
                $.get('<?php echo URL::to('salestaxacc'); ?>?id=' + result[0] + '&typeofservice=' + typeofservice + '&serviceperiod=' + serviceperiod + '&currency=' + currency, function (data) {
                    $.each(data, function (index, subcatobj) {
                        if (pricing == 'Regular') {
                            //  alert(data.regularprice);
                            $('.services2').removeAttr("disabled");
                            $('.services2').show();

                            $('.serviceincludeacc').val(data.serviceincludes);
                            $('.regularpriceacc').val(data.regularprice);
                            $('.combopriceacc').val(data.comboprice);
                            $('.priceacc').val(data.regularprice);

                        }
                        if (pricing == 'Combo') {
                            $(".services2").prop("disabled", false);

                            $('.services2').show();
                            $('.serviceincludeacc').val(data.serviceincludes);
                            $('.regularpriceacc').val(data.regularprice);
                            $('.combopriceacc').val(data.comboprice);
                            $('.priceacc').val(data.comboprice);

                        }
                    })
                });
            });

            $(".disc").attr("disabled", 'disabled');

            $(document).on('change', '.payroll_period', function () {
                var id = $(this).val();
                //alert(id);

                $('.acperiods2').val(id);
                if (id == '') {
                    $(".disc").attr("disabled", 'disabled');

                    $(".services3").attr("disabled", 'disabled');
                    $('.services3').hide();

                    $(".services6").attr("disabled", 'disabled');
                    $('.services6').hide();

                } else {
                    // alert();
                    $(".disc").attr("disabled", false);

                    $(".services3").attr("disabled", false);

                    $('.services3').show();

                    $(".services6").attr("disabled", false);
                    $('.services6').show();

                }
                var result = id.split('-');
                var currency = $("#currency option:selected").val();
                var typeofservice = result[1];
                var serviceperiod = $("#serviceperiod  option:selected").val();
                var totalprice1 = $(".totalprice1").val();
                var pricing = $("#pricetype  option:selected").val();
                //	alert(pricing);
                var ckq = $(".ckq").val();//alert(typeofservice);
                $.get('<?php echo URL::to('salestaxpay'); ?>?typeofservice=' + id + '&serviceperiod=' + serviceperiod + '&currency=' + currency, function (data) {

                    //  		$(regularprice).val('');
                    //	$(comboprice1).val('');
                    //$(sumprice).val('');
                    $.each(data, function (index, subcatobj) {

                        if (pricing == 'Regular') {
                            //  alert(data.regularprice);
                            $('.removephp').hide();

                            $('.services3').removeAttr("disabled");

                            $('.services3').show();

                            $('.services6').removeAttr("disabled");

                            $('.services6').show();

                            $('.serviceincludepay').val(data.serviceincludes);
                            $('.regularpricepay').val(data.regularprice);
                            $('.combopricepay').val(data.comboprice);
                            $('.pricepay').val(data.regularprice);
                        }
                        if (pricing == 'Combo') {
                            $('.removephp').hide();
                            // alert('111');
                            $('.services3').removeAttr("disabled");
                            $('.services3').show();
                            $('.services6').removeAttr("disabled");

                            $('.services6').show();
                            $('.serviceincludepay').val(data.serviceincludes);
                            $('.regularpricepay').val(data.regularprice);
                            $('.combopricepay').val(data.comboprice);
                            $('.pricepay').val(data.comboprice);

                        }
                    })
                });
            });


            $(document).on('change', '.tax_service_1', function () {
                var id = $(this).val();
                //alert(id);
                var result = id.split(' -');
                var currency = $("#currency option:selected").val();
                var typeofservice = '3';
                var titles = id;

                var serviceperiod = $("#serviceperiod  option:selected").val();
                var totalprice1 = $(".totalprice1").val();
                var pricing = $("#pricetype  option:selected").val();
                //	alert(pricing);
                var ckq = $(".ckq").val();//alert(typeofservice);
                $.get('<?php echo URL::to('salestaxations'); ?>?typeofservice=' + typeofservice + '&title=' + titles + '&serviceperiod=' + serviceperiod + '&currency=' + currency, function (data) {

                    //  		$(regularprice).val('');
                    //	$(comboprice1).val('');
                    //$(sumprice).val('');
                    var counts = Object.keys(data).length;

                    //	alert(counts);
                    if (counts > 0) {

                        if (pricing == 'Regular') {

                            //alert(data.regularprice1);
                            $('.services4').removeAttr("disabled");
                            //  alert(data.regularprice);
                            $('.services4').show();

                            $('.taxtitletax').val(data.taxtitle);
                            $('.titletax').val(data.title);


                            $('.taxserviceincludenote').val(data.serviceincludes1);
                            $('.taxregularpricetax').val(data.regularprice1);
                            $('.taxcombopricetax').val(data.comboprice1);
                            $('.taxpricetax').val(data.regularprice1);
                        }
                        if (pricing == 'Combo') {

                            $('.services4').removeAttr("disabled");
                            $('.services4').show();
                            $('.titletax').val(data.title);


                            $('.taxtitletax').val(data.taxtitle);
                            $('.taxserviceincludenote').val(data.serviceincludes1);
                            $('.taxregularpricetax').val(data.regularprice1);
                            $('.taxcombopricetax').val(data.comboprice1);
                            $('.taxpricetax').val(data.comboprice1);

                        }
                    }


                });
            });

            var room1 = 0;
            var coun = 0;
            var z = room1 + coun;

            function education_field_note() {
                room1++;
                z++;
                var objTo = document.getElementById('table')
                var divtest = document.createElement("tbody");
                divtest.setAttribute("class", "form-group removeclass" + z);
                divtest.innerHTML = '<tr><td><input type="hidden" name="taxid[]" value=""><select class="form-control pricetype_' + z + '" name="titles[]" id="titles"> <option value="">---Select Service---</option> <?php $__currentLoopData = $taxtitle; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ti): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?><option value="<?php echo e($ti->title); ?>" <?php $__currentLoopData = $clientsertitle; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $clientsertitle1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php if($clientsertitle1->titles==$ti->title): ?> disabled <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>><?php echo e($ti->title); ?></option><?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></select></td><td><input readonly type="text" name="taxnote[]" id="note_' + z + '" placeholder="Note" class="form-control"></td><td><input readonly type="text" placeholder="Regular Price" class="regularprice_' + z + ' form-control" name="regularprice1[]" style="text-align:right;"></td><td><input readonly type="text" style="text-align:right;" placeholder="Combo Price" class=" form-control comboprice_' + z + '" name="comboprice1[]"></td><td><input readonly style="text-align:right;" type="text" id="taxprice_' + z + '" class=" form-control price taxprice_' + z + '" placeholder="Your Price" name="taxprice[]"></td><td><button class="btn btn-danger" type="button" onclick="remove_education_fields(' + z + ');"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span> </button></td></tr>';
                var rdiv = 'removeclass' + z;
                var rdiv1 = 'Schoolname' + z;
                var pricetype = '.pricetype_' + z;
                var comboprice1 = '.comboprice_' + z;
                var regularprice = '.regularprice_' + z;
                var sumprice = '#taxprice_' + z;
                var note1 = '#note_' + z;
                $(document).on('change', pricetype, function () {
                    var id = $(this).val();
                    //	alert(id);
                    var currency = $("#currency option:selected").val();
                    var typeofservice = 3;
                    var serviceperiod = $("#serviceperiod  option:selected").val();
                    var totalprice1 = $(".totalprice1").val();
                    var pricing = $("#pricetype  option:selected").val();
                    var ckq = $(".ckq").val();//alert(typeofservice);
                    $.get('<?php echo URL::to('salestax'); ?>?id=' + id + '&typeofservice=' + typeofservice + '&serviceperiod=' + serviceperiod + '&currency=' + currency, function (data) {
                        $(regularprice).val('');
                        $(comboprice1).val('');
                        $(sumprice).val('');
                        $.each(data, function (index, subcatobj) {
                            $(regularprice).val(subcatobj.regularprice1);
                            $(comboprice1).val(subcatobj.comboprice1);
                            var comboprice = subcatobj.comboprice1;
                            $(note1).val(subcatobj.serviceincludes1);
                            if (pricing == 'Regular') {
                                $(sumprice).val(subcatobj.regularprice1);
                                var price = subcatobj.regularprice1;
                                var price1 = parseFloat(price.replace(/[^\d\.]*/g, ''));//alert(price1);
                                var price2 = totalprice1;
                                var price3 = parseFloat(price2.replace(/[^\d\.]*/g, ''));
                                var x = price1 + price3;//alert(price3);
                                var x2 = parseFloat(Math.round(x * 100) / 100).toFixed(2);
                                var no = x2.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                var x1 = ckq + ' ' + no;
                                $(".totalprice1").val(x1);
                            }
                            if (pricing == 'Combo') {
                                $(sumprice).val(subcatobj.comboprice1);
                                var price = subcatobj.comboprice1;
                                var price1 = parseFloat(price.replace(/[^\d\.]*/g, ''));
                                var price2 = totalprice1;
                                var price3 = parseFloat(price2.replace(/[^\d\.]*/g, ''));
                                var x = price1 + price3;
                                var x2 = parseFloat(Math.round(x * 100) / 100).toFixed(2);
                                var no = x2.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                var x1 = ckq + ' ' + no;
                                $(".totalprice1").val(x1);
                            }
                        })
                    });
                });
                objTo.appendChild(divtest)
            }

            function remove_education_fields(rid) {
                // alert(rid);
                var price_1 = '#taxprice_' + rid;
                var ckq = $(".ckq").val();
                var pr = $(price_1).val();
                // alert(pr);
                var price1 = parseFloat(pr.replace(/[^\d\.]*/g, ''));
                var totalprice1 = $(".totalprice1").val();
                var totals = parseFloat(totalprice1.replace(/[^\d\.]*/g, ''));
                var tt = totals - price1;
                var x1 = ckq + ' ' + tt;
                var totalprice2 = $(".totalprice1").val(x1);
                $('.removeclass' + rid).remove();
                room1--;
                z--;
            }
        </script>
        <script>
            $(document).on('change', '.maritialsts', function () {
                var ids = $(this).val();
                //alert(ids);
                if (ids == 'Married') {
                    $('.sps').show();
                    $('.mrgdate').show();

                } else {
                    $('.sps').hide();
                    $('.mrgdate').hide();

                }
            });
        </script>
        <script>
            var names = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15'];
            /* $(document).on('change','.clock', function(){ 
       var name = $('.clock').val(); 
        if(jQuery.inArray(name, names)!='-1') { 
             $(".clock").not(this).find("option[value="+name+"]").attr('disabled', true);
           } else { 
             $(".clock").not(this).find("option[value="+name+"]").attr('disabled', true);
           }
        }); */
            var room1 = 0;
            var coun = 0;
            var z = room1 + coun;

            function education_service() {
                room1++;
                z++;
                var objTo = document.getElementById('education_service')
                var divtest = document.createElement("div");
                divtest.setAttribute("class", "form-group ser removeclass" + z);
                divtest.innerHTML = '<div class="customer-service-information-bg"><div><div class="col-md-3"><label class="control-label">Type of Service :</label><input name="serviceid[]" type="hidden" id="serviceid" class="form-control"><select name="typeofservice[]" type="text" id="typeofservice_' + z + '" class="form-control clock"><option value="">Type of Service</option><?php $__currentLoopData = $typeofser; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $typeofser1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?><option value="<?php echo e($typeofser1->id); ?>" <?php $__currentLoopData = $clientser; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $clientser1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php if($clientser1->typeofservice==$typeofser1->id): ?> disabled style="pointer-events: none;background:lightgreen none repeat scroll 0% 0%;" <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>><?php echo e($typeofser1->typeofservice); ?></option><?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></select></div><div id="regular_' + z + '"><div class="col-md-4" style="widh:36%;"><div class="row"><label class="control-label">Service Includes :</label> <textarea readonly cols="50" name="serviceincludes[]" type="text" id="serviceinclude1_' + z + '" class="form-control-1"></textarea></div></div><div class="col-md-2" style="width:13%"><label class="control-label">Regular Price :</label><input name="regularprice[]" style="text-align:right" type="text" id="regularprice2_' + z + '" placeholder="Regular Price"  readonly class="regularprice2_' + z + ' form-control"></div><div class="col-md-2" style="width:11%"><div class="row"><label class="control-label" style="width: 130px;">Combo Price :</label><input name="comboprice[]" type="text" id="comboprice2_' + z + '" style="text-align:right" placeholder="Combo Price" readonly class="comboprice2_' + z + ' form-control"></div></div><div class="col-md-2" style="width:11%"><label class="control-label">Your Price :</label><input name="price[]" value="" readonly style="text-align:right;" type="text" id="price_' + z + '" placeholder="Your Price" class="form-control price"></div><div><span style="color: red;position: relative;right:47px;text-align: right;float: right;margin-top: 31px; width: 0;" class="fa fa-trash" onclick="remove_service(' + z + ');" aria-hidden="true"></span></div></div></div><div class="input_fields_wrap_notes_' + z + '" style="display:none"><div class="clear"></div><table class="table" id="table_' + z + '" style="width:100%;margin:15px auto 0px;"><thead><tr style="background:#00a0e3;color:#fff"><th scope="col" style="width: 262px;">Title</th><th scope="col">Note</th><th scope="col" style="width: 120px;">Regular Price</th><th style="width: 120px;" scope="col">Combo Price</th><th style="width: 120px;" scope="col">Your Price</th><th scope="col" style="width: 120px;">Action</th></tr></thead><tbody><tr><td style="width:262px;"><select class="form-control" name="titles[]" id="titles_' + z + '"><option value="">---Select Service---</option><?php $__currentLoopData = $taxtitle; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ti): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?><option value="<?php echo e($ti->title); ?>"><?php echo e($ti->title); ?></option><?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></select></td><td><input type="hidden" name="taxid[]"><input readonly type="text" class="form-control" placeholder="Note" name="taxnote[]" id="note_' + z + '"></td><td style="width: 120px;"><input readonly type="text" style="font-size:small !important;font-size:small !important;text-align:right;" placeholder="Regular Price" class="form-control" name="regularprice1[]"  id="regularprice1_' + z + '"></td><td style="width: 120px;"><input readonly type="text" style="text-align:right;" class="form-control" placeholder="Combo Price" name="comboprice1[]" id="comboprice1_' + z + '"></td><td style="width: 120px;"><input readonly type="text" style="text-align:right;" class="form-control price" placeholder="Your Price" name="taxprice[]" id="taxprice1_' + z + '"></td><td style="width: 120px;"><a class="btn btn-primary" onclick="education_field_table(' + z + ');"> Add</a></td></tr></tbody></table></div></div>';
                var rdiv = 'removeclass' + z;
                var rdiv1 = 'Schoolname' + z;
                var pricetype1 = '#titles_' + z;
                var totalprice = '.totalprice';
                var regularprice1 = '#regularprice1_' + z;
                var typeofservice1 = '#typeofservice_' + z;
                var typeofservice2 = '#typeofservice_' + z + ':selected';
                var regularprice2 = '#regularprice2_' + z;
                var table = '#table_' + z;
                var input_fields_wrap_notes = '.input_fields_wrap_notes_' + z;
                var regular = '#regular_' + z;
                var currency1 = '#currency';
                var serviceperiod1 = '#serviceperiod';
                var comboprice = '#comboprice1_' + z;
                var comboprice2 = '#comboprice2_' + z;
                var note1 = '#note_' + z;
                var serviceinclude1 = '#serviceinclude1_' + z;
                var price_1 = '#price_' + z;
                var sumprice = '#taxprice1_' + z;
                var ckq = $(".ckq").val();
                $('.customer-service-information-bg').load('.customer-service-information-bg');
                var names = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15'];

                $(document).on('blur', '.price', function () {
                    var sum = 0;
                    var vk = parseFloat($(this).val().replace(/[^\d\.]*/g, '') || 0);
                    var sign3 = parseFloat(Math.round(vk * 100) / 100).toFixed(2);
                    var n1 = sign3.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    var x2 = ckq + ' ' + n1;
                    $(this).val(x2);
                    // $('.price').each(function() {
                    //   sum += parseFloat($(this).val().replace( /[^\d\.]*/g, '') || 0);
                    // });
                    // var sum1 = sum;
                    // var sign2 =parseFloat(Math.round(sum1 * 100) / 100).toFixed(2);
                    // var no = sign2.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    // var x1 = ckq+' '+no;
                    // $('.totalprice1').val(x1);
                });
                $(document).on('change', typeofservice1, function () {
                    var sign1 = $(this).val();
                    if (sign1 == '3') {
                        $(input_fields_wrap_notes).show();
                        $(regular).hide();
                    } else {
                        $(input_fields_wrap_notes).hide();
                        $(regular).show();
                    }
                });
                $(document).on('change', pricetype1, function () {
                    var id = $(this).val();
                    var currency = $(currency1).val();
                    var typeofservice = $(typeofservice1).val();
                    var serviceperiod = $(serviceperiod1).val();
                    var totalprice1 = $(".totalprice1").val();
                    var pricing = $("#pricetype  option:selected").val();
                    var ckq = $(".ckq").val();
                    $.get('<?php echo URL::to('salestax'); ?>?id=' + id + '&typeofservice=' + typeofservice + '&serviceperiod=' + serviceperiod + '&currency=' + currency, function (data) {
                        $(regularprice1).val('');
                        $(comboprice).val('');
                        $.each(data, function (index, subcatobj) {
                            $(regularprice1).val(subcatobj.regularprice1);
                            $(comboprice).val(subcatobj.comboprice1);
                            $(note1).val(subcatobj.serviceincludes1);
                            if (pricing == 'Regular') {
                                $(sumprice).val(subcatobj.regularprice1);
                                var price = subcatobj.regularprice1;
                                var price1 = parseFloat(price.replace(/[^\d\.]*/g, ''));//alert(price1);
                                var price2 = totalprice1;
                                var price3 = parseFloat(price2.replace(/[^\d\.]*/g, ''));
                                var x = price1 + price3;//alert(price3);
                                var x2 = parseFloat(Math.round(x * 100) / 100).toFixed(2);
                                var no = x2.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                var x1 = ckq + ' ' + no;
                                $(".totalprice1").val(x1);
                            }
                            if (pricing == 'Combo') {
                                $(sumprice).val(subcatobj.comboprice1);
                                var price = subcatobj.comboprice1;
                                var price1 = parseFloat(price.replace(/[^\d\.]*/g, ''));
                                var price2 = totalprice1;
                                var price3 = parseFloat(price2.replace(/[^\d\.]*/g, ''));
                                var x = price1 + price3;
                                var x2 = parseFloat(Math.round(x * 100) / 100).toFixed(2);
                                var no = x2.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                var x1 = ckq + ' ' + no;
                                $(".totalprice1").val(x1);
                            }
                        })
                    });
                });
                /*	 var countries = [];

        $.each($(".clock option:selected"), function(){
             var val = $(this).val();
           
            countries.push($(this).val());
 $(".clock").not(this).find("option[value="+ $(this).val() + "]").attr('disabled', true);
        });*/
                $(document).on('change', typeofservice1, function () {
                    var id = $(this).val();
                    var currency = $(currency1).val();
                    var typeofservice = $(typeofservice1).val();
                    var serviceperiod = $(serviceperiod1).val();
                    var totalprice1 = $(".totalprice1").val();
                    var ckq = $(".ckq").val();
                    var pricing = $("#pricetype  option:selected").val();//alert(totalprice1);
                    $.get('<?php echo URL::to('salestax'); ?>?id=' + id + '&typeofservice=' + typeofservice + '&serviceperiod=' + serviceperiod + '&currency=' + currency, function (data) {
                        $(regularprice2).val('');
                        $(comboprice2).val('');
                        $(serviceinclude1).val('');
                        $.each(data, function (index, subcatobj) {
                            $(regularprice2).val(subcatobj.regularprice);
                            var price = subcatobj.regularprice;
                            $(comboprice2).val(subcatobj.comboprice);
                            var comboprice = subcatobj.comboprice;
                            $(serviceinclude1).val(subcatobj.serviceincludes);//alert(pricing);
                            if (pricing == 'Regular') {
                                $(price_1).val(price);
                                var price1 = parseFloat(price.replace(/[^\d\.]*/g, ''));//alert(price1);
                                var price2 = totalprice1;
                                var price3 = parseFloat(price2.replace(/[^\d\.]*/g, ''));
                                var x = price1 + price3;//alert(price3);
                                var x2 = parseFloat(Math.round(x * 100) / 100).toFixed(2);
                                var no = x2.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                var x1 = ckq + ' ' + no;
                                // var x1 = ckq+' '+x;
                                $(".totalprice1").val(x1);
                            }
                            if (pricing == 'Combo') {
                                $(price_1).val(comboprice);
                                var price1 = parseFloat(comboprice.replace(/[^\d\.]*/g, ''));
                                var price2 = totalprice1;
                                var price3 = parseFloat(price2.replace(/[^\d\.]*/g, ''));
                                var x = price1 + price3;
                                var x2 = parseFloat(Math.round(x * 100) / 100).toFixed(2);
                                var no = x2.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                var x1 = ckq + ' ' + no;
                                $(".totalprice1").val(x1);
                            }
                        })
                    });
                });

                objTo.appendChild(divtest)
            }

            function remove_service(rid) {
                var price_1 = '#price_' + rid;
                var ckq = $(".ckq").val();
                var pr = $(price_1).val();
                var price1 = parseFloat(pr.replace(/[^\d\.]*/g, ''));
                var totalprice1 = $(".totalprice1").val();
                var totals = parseFloat(totalprice1.replace(/[^\d\.]*/g, ''));
                var tt = totals - price1;
                var x1 = ckq + ' ' + tt;
                var totalprice2 = $(".totalprice1").val(x1);
                $('.removeclass' + rid).remove();

                room1--;
                z--;
            }

            function education_field_table(rid) {
                z++;
                var table = 'table_' + rid;//alert(table);
                var objTo = document.getElementById(table)
                var divtest = document.createElement("tbody");
                divtest.setAttribute("class", "form-group removeclass" + z);
                divtest.innerHTML = '<tr><td><select class="form-control pricetype1_' + z + '" id="pricetype1_' + z + '" name="titles[]"><option value="">---Select Service---</option> <?php $__currentLoopData = $taxtitle; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ti): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?><option value="<?php echo e($ti->title); ?>"><?php echo e($ti->title); ?></option><?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></select></td><td><input type="text" id="note_' + z + '" name="taxnote[]" class="form-control"  placeholder="Note"><input type="hidden" name="taxid[]"></td><td><input type="text" placeholder="Regular Price" id="regularprice1_' + z + '" class="regularprice1_' + z + ' form-control" name="regularprice1[]" style="text-align:right"></td><td><input type="text" style="text-align:right" placeholder="Combo Price" class=" form-control comboprice1_' + z + '" id="comboprice1_' + z + '" name="comboprice1[]"></td><td><input type="text" placeholder="Your Price" style="text-align:right" class="form-control price taxprice1_' + z + '" id="taxprice1_' + z + '" name="taxprice[]"></td><td><button class="btn btn-danger" type="button" onclick="remove_education_fields(' + z + ');"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span> </button></td></tr>';
                var rdiv = 'removeclass' + z;
                var rdiv1 = 'Schoolname' + z;
                var currency1 = '#currency';
                var serviceperiod1 = '#serviceperiod';
                var pricetype = '#pricetype1_' + z;
                var comboprice11 = '#comboprice1_' + z;
                var regularprice11 = '#regularprice1_' + z;
                var note1 = '#note_' + z;
                var typeofservice1 = '#typeofservice_' + rid;
                var sumprice = '#taxprice1_' + z;
                $(document).on('change', pricetype, function () {
                    var id = $(this).val();
                    var currency = $(currency1).val();
                    var typeofservice = $(typeofservice1).val();
                    var serviceperiod = $(serviceperiod1).val();
                    var totalprice1 = $(".totalprice1").val();
                    var pricing = $("#pricetype option:selected").val();
                    var ckq = $(".ckq").val();
                    $.get('<?php echo URL::to('salestax'); ?>?id=' + id + '&typeofservice=' + typeofservice + '&serviceperiod=' + serviceperiod + '&currency=' + currency, function (data) {
                        $(regularprice11).val('');
                        $(comboprice11).val('');
                        $.each(data, function (index, subcatobj) {
                            $(regularprice11).val(subcatobj.regularprice1);
                            $(comboprice11).val(subcatobj.comboprice1);
                            $(note1).val(subcatobj.serviceincludes1);
                            if (pricing == 'Regular') {
                                $(sumprice).val(subcatobj.regularprice1);
                                var price = subcatobj.regularprice1;
                                var price1 = parseFloat(price.replace(/[^\d\.]*/g, ''));//alert(price1);
                                var price2 = totalprice1;
                                var price3 = parseFloat(price2.replace(/[^\d\.]*/g, ''));
                                var x = price1 + price3;//alert(price3);
                                var x2 = parseFloat(Math.round(x * 100) / 100).toFixed(2);
                                var no = x2.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                var x1 = ckq + ' ' + no;
                                $(".totalprice1").val(x1);
                            }
                            if (pricing == 'Combo') {
                                $(sumprice).val(subcatobj.comboprice1);
                                var price = subcatobj.comboprice1;
                                var price1 = parseFloat(price.replace(/[^\d\.]*/g, ''));
                                var price2 = totalprice1;
                                var price3 = parseFloat(price2.replace(/[^\d\.]*/g, ''));
                                var x = price1 + price3;
                                var x2 = parseFloat(Math.round(x * 100) / 100).toFixed(2);
                                var no = x2.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                var x1 = ckq + ' ' + no;
                                $(".totalprice1").val(x1);
                            }
                        })
                    });
                });
                objTo.appendChild(divtest)
            }

            function remove_education_fields(rid) {
                // alert(rid);
                var price_1 = '#taxprice1_' + rid;
                var ckq = $(".ckq").val();
                var pr = $(price_1).val();
                var price1 = parseFloat(pr.replace(/[^\d\.]*/g, ''));
                var totalprice1 = $(".totalprice1").val();
                var totals = parseFloat(totalprice1.replace(/[^\d\.]*/g, ''));
                var tt = totals - price1;
                var x1 = ckq + ' ' + tt;
                var totalprice2 = $(".totalprice1").val(x1);
                $('.removeclass' + rid).remove();
                room1--;
                z--;
            }
        </script>
        <script>
            $(document).ready(function () {
                setColor();
                $('select.pricetype').change(function () {
                    setColor();
                });
                setColor1();
                $('select.currency1').change(function () {
                    setColor1();
                });
                setColor2();
                $('select.serviceperiod').change(function () {
                    setColor2();
                });
            });

            function setColor() {
                var color = $('select.pricetype').find('option:selected').attr('value');
                var val1 = $('select.pricetype').val();
                // alert(val1);
                if (val1 == '1') {
                    $('select.pricetype').css('background', 'none');

                } else {
                    $('select.pricetype').css('background', 'lightgreen');

                }
            }

            function setColor1() {
                var color = $('select.currency1').find('option:selected').attr('value');
                var val1 = $('select.currency1').val();
                // alert(val1);
                if (val1 == '') {
                    $('select.currency1').css('background', 'none');

                } else {
                    $('select.currency1').css('background', 'lightgreen');

                }
            }

            function setColor2() {
                var color = $('select.serviceperiod').find('option:selected').attr('value');
                var val1 = $('select.serviceperiod').val();
                // alert(val1);
                if (val1 == '') {
                    $('select.serviceperiod').css('background', 'none');
                } else {
                    $('select.serviceperiod').css('background', 'lightgreen');
                }
            }


        </script>

        <script>
            $(".ext").mask("99999");
            $("#bussiness_zip").mask("99999");
            $("#acountno").mask("9999");
            $("#cartno").mask("9999");
            $("#routingno").mask("999999999");
            $("#mobile_sec_1").mask("(999) 999-9999");
            $("#mobile_sec_10").mask("(999) 999-9999");

            $("#mobile_2").mask("(999) 999-9999");
            $("#contact_fax_1").mask("(999) 999-9999");


        </script>
        <script>
            function user_access(f) { //alert();
                if (f.user_access1.checked == true) {
                    f.user_name.value = f.nametype_sec.value + ' ' + f.firstname_sec.value + ' ' + f.middlename_sec.value + ' ' + f.lastname_sec.value;
                    f.user_email.value = f.email_sec.value;
                    f.user_cell.value = f.mobile_sec.value;
                } else {
                    f.user_name.value = '';
                    f.user_email.value = '';
                    f.user_cell.value = '';
                }
            }

            function FillBilling(f) { //alert();
                if (f.businessaddress.checked == true) {
                    f.mailing_address.value = f.address.value;
                    f.mailing_address1.value = f.address1.value;
                    f.mailing_city.value = f.city.value;
                    f.mailing_state.value = f.stateId.value;
                    f.mailing_zip.value = f.zip.value;
                } else {
                    f.mailing_address.value = '';
                    f.mailing_address1.value = '';
                    f.mailing_city.value = '';
                    f.mailing_state.value = '';
                    f.mailing_zip.value = '';
                }
            }

            function emailbli(f) { //alert();
                if (f.emailbli1.checked == true) {
                    f.email_1.value = f.email.value;
                } else {
                    f.email_1.value = '';
                }
            }

            function faxbli(f) { //alert();
                if (f.faxbli1.checked == true) {
                    f.contact_fax_1.value = f.business_fax.value;
                } else {
                    f.contact_fax_1.value = '';
                }
            }

            function faxbli233(f) { //alert();
                if (f.faxbli3.checked == true) {
                    f.contact_address1.value = f.address.value;
                    f.contact_address2.value = f.address1.value;
                    f.city_1.value = f.city.value;
                    f.stateId1.value = f.stateId.value;
                    f.zip_1.value = f.zip.value;
                } else {
                    f.contact_address1.value = '';
                    f.contact_address2.value = '';
                    f.city_1.value = '';
                    f.stateId1.value = '';
                    f.zip_1.value = '';
                }
            }

            function faxbli233_g(f) {
                if (f.faxbli3_g.checked == true) {
                    // alert(f.guardian.value);
                    //  alert(f.motelephoneile1.value);
                    f.etelephone1.value = f.business_no.value;
                    f.mobiletype_1.value = f.businesstype.value;
                    f.ext2_1.value = f.businessext.value;
                    f.guardian.value = f.basic_guardian1.value;
                    f.guardian2.value = f.basic_guardian2.value;


                    f.mobile_2.value = f.motelephoneile1.value;
                    f.mobiletype_2.value = f.telephoneNo2Type.value;
                    f.ext2_2.value = f.ext2.value;

                } else {
                    f.guardian.value = '';
                    f.guardian2.value = '';
                    f.etelephone1.value = '';
                    f.mobiletype_1.value = '';
                    f.ext2_1.value = '';

                    f.mobile_2.value = '';
                    f.mobiletype_2.value = '';
                    f.ext2_2.value = '';

                }

            }

        </script>
        <script>
            function showDiv(elem) {
                if (elem.value == 'Federal') {
                    document.getElementById('hidden_div').style.display = "none";
                } else {
                    document.getElementById('hidden_div').style.display = "block";
                }
            }
        </script>
        <script>
            $(document).ready(function () {
                $(document).on('keyup', '#address', function () {
                    var busaddress = $('#businessaddress').is(':checked');
                    // alert(busaddress);
                    var busaddress1 = $('#address').val();
                    var address2 = $('#address1').val();

                    var mailaddress1 = $('#mailing_address').val();
                    //  $('#mailing_address').val(busaddress1);
                    var mailaddress2 = $('#mailing_address1').val();

                    if (busaddress) {
                        // alert(busaddress);
                        $('#mailing_address').val(busaddress1);
                        $('#mailing_address1').val(address2);

                        //alert(busaddress);
                    }
                    //var busaddress=$('#businessaddress').val();

                });

                $(document).on('keyup', '#address1', function () {
                    var busaddress = $('#businessaddress').is(':checked');
                    var busaddress1 = $('#address1').val();

                    var mailaddress1 = $('#mailing_address').val();

                    if (busaddress) {
                        // alert(busaddress);
                        $('#mailing_address1').val(busaddress1);

                        //alert(busaddress);
                    }

                });

                $(document).on('keyup', '#city', function () {
                    var busaddress = $('#businessaddress').is(':checked');
                    var busaddress1 = $('#city').val();
                    //  alert(busaddress1);    
                    var mailaddress1 = $('#mailing_city').val();
                    //  alert(mailaddress1);
                    if (busaddress) {
                        // alert(busaddress);
                        $('#mailing_city').val(busaddress1);

                        //alert(busaddress);
                    }

                });

                $(document).on('change', '#stateId', function () {
                    var busaddress = $('#businessaddress').is(':checked');
                    var busaddress1 = $('#stateId').val();
                    //  alert(busaddress1);    
                    var mailaddress1 = $('#mailing_state').val();
                    //  alert(mailaddress1);
                    if (busaddress) {
                        // alert(busaddress);
                        $('#mailing_state').val(busaddress1);

                        //alert(busaddress);
                    }

                });

                $(document).on('change', '#zip', function () {
                    var busaddress = $('#businessaddress').is(':checked');
                    var busaddress1 = $('#zip').val();
                    //  alert(busaddress1);    
                    var mailaddress1 = $('#mailing_zip').val();
                    //  alert(mailaddress1);
                    if (busaddress) {
                        // alert(busaddress);
                        $('#mailing_zip').val(busaddress1);

                        //alert(busaddress);
                    }

                });

                $(document).on('keyup', '#motelphoneile', function () {
                    var busaddress = $('#businessaddress').is(':checked');
                    var busaddress1 = $('#motelphoneile').val();
                    var mailaddress1 = $('#mobile_sec_1').val();

                    if (busaddress) {
                        $('#mobile_sec_1').val(busaddress1);
                    }

                });

                $(document).on('change', '#telephoneNo1Type', function () {
                    var busaddress = $('#businessaddress').is(':checked');
                    var busaddress1 = $('#telephoneNo1Type').val();
                    var mailaddress1 = $('#mobiletype_1').val();

                    if (busaddress) {
                        $('#mobiletype_1').val(busaddress1);
                    }

                });

                $(document).on('keyup', '#businessext', function () {
                    var busaddress = $('#businessaddress').is(':checked');
                    var busaddress1 = $('#businessext').val();
                    var mailaddress1 = $('#ext2_1').val();

                    if (busaddress) {
                        $('#ext2_1').val(busaddress1);
                    }

                });

                $(document).on('keyup', '#motelphoneile1', function () {
                    var busaddress = $('#businessaddress').is(':checked');
                    var busaddress1 = $('#motelphoneile1').val();
                    var mailaddress1 = $('#mobile_2').val();

                    if (busaddress) {
                        $('#mobile_2').val(busaddress1);
                    }

                });

                $(document).on('keyup', '#business_fax', function () {
                    var busaddress = $('#businessaddress').is(':checked');
                    var busaddress1 = $('#business_fax').val();
                    var mailaddress1 = $('#contact_fax_1').val();

                    if (busaddress) {
                        $('#contact_fax_1').val(busaddress1);
                    }

                });

                $(document).on('keyup', '#email', function () {
                    var busaddress = $('#businessaddress').is(':checked');
                    var busaddress1 = $('#email').val();
                    var mailaddress1 = $('#email_1').val();

                    if (busaddress) {
                        $('#email_1').val(busaddress1);
                    }

                });

                $(document).on('change', '#telephoneNo2Type', function () {
                    var busaddress = $('#businessaddress').is(':checked');
                    var busaddress1 = $('#telephoneNo2Type').val();
                    var mailaddress1 = $('#mobiletype_2').val();

                    if (busaddress) {
                        $('#mobiletype_2').val(busaddress1);
                    }

                });

                $(document).on('change', '#guardian2', function () {
                    var busaddress = $('#businessaddress').is(':checked');
                    var busaddress1 = $('#guardian2').val();
                    var mailaddress1 = $('#basic_guardian2').val();

                    if (busaddress) {
                        $('#basic_guardian2').val(busaddress1);
                    }

                });

                $(document).on('change', '.category', function () {
                    var id = $(this).val();
                    // 	alert(id);
                    $.get('<?php echo URL::to('getRequest'); ?>?id=' + id, function (data) {
                        if (data == "") {
                            $('.cat1').hide();
                            $('#business_catagory_name').append('<option value="">---Select---</option>');
                            $('#business_catagory_name').empty('');
                        } else {
                            $('#business_catagory_name').append('<option value="">---Select---</option>');
                            $('#business_catagory_name').empty('');

                        }
                        $('.cat1').hide();
                        $('.cat2').hide();
                        $('.cat3').hide();
                        $('#business_catagory_name').append('<option value="">---Select---</option>');
                        $('#business_brand_name').append('<option value="">---Select---</option>');
                        $('#business_brand_category_name').append('<option value="">---Select---</option>');
                        $.each(data, function (index, subcatobj) {
                            $('.cat1').show();
                            $('#business_catagory_name').append('<option value="' + subcatobj.id + '">' + subcatobj.business_cat_name + '</option>');
                        })
                    });
                });
            });
        </script>
        <script>
            $(document).ready(function () {
                $(document).on('change', '.otherdob', function () {
                    var thisval = $(this).val();
                    var today = '<?php echo date('Y');?>';

                    var total = parseFloat(today) - parseFloat(thisval);
                    var total1 = total + ' Years';
                    $('.totalyears').val(total1);
                });

                $(document).on('change', '.otherdob1', function () {
                    var thisval = $(this).val();
                    var todays = '<?php echo date('Y');?>';

                    var totals1 = parseFloat(todays) - parseFloat(thisval);
                    var totals1 = totals1 + ' Years';
                    $('.totalyears1').val(totals1);
                });

                $(document).on('change', '#business_brand_category_name', function () {
                    //console.log('htm');
                    var id = $(this).val();
                    $.get('<?php echo URL::to('business_brand_category_name_1'); ?>?id=' + id, function (data) {
                        $('#image3').empty();
                        $('#business_brand_name').append('<option value="">---Select---</option>');
                        $.each(data, function (index, subcatobj) {
                            $('#image3').append('<img src="/public/businessbrandcategory/' + subcatobj.business_brand_category_image + '" alt="" class="img-responsive">');
                        })
                    });
                });
            });
        </script>
        <script>
            $(document).ready(function () {
                $(document).on('change', '#business_brand_name', function () {
                    var id = $(this).val();
                    $.get('<?php echo URL::to('/getcatmm'); ?>?id=' + id, function (data) {
                        $('.cat3').hide();
                        $('#business_brand_category_name').empty();
                        $('#business_brand_category_name').append('<option value="">---Select---</option>');
                        $.each(data, function (index, subcatobj) {
                            $('.cat3').show();
                            $('#business_brand_category_name').append('<option value="' + subcatobj.id + '">' + subcatobj.business_brand_category_name + '</option>');
                        })
                    });
                });
            });
        </script>
        <script>
            $(document).ready(function () {
                $(document).on('change', '.maritial', function () {
                    var thiss = $(this).val();
                    if (thiss == 'Married') {
                        $('.showmaritial').show();
                    } else {
                        $('.showmaritial').hide();
                    }
                });
                $(document).on('change', '.category', function () {
                    var id = $(this).val();
                    if (id == '6') {
                        $('.addcom').show();
                        $('.removecom').hide();
                        $('.bus').hide();
                        $('.yourself').show();
                        $('.hideyourself').hide();
                    } else {
                        $('.bus').show();
                        $('.yourself').hide();
                        $('.hideyourself').show();
                        $('.addcom').hide();
                        $('.removecom').show();
                    }
                    $.get('<?php echo URL::to('getcat'); ?>?id=' + id, function (data) {
                        $('#image').empty();
                        $('#user_type').empty();
                        $.each(data, function (index, subcatobj) {
                            $('#user_type').val(subcatobj.bussiness_name);
                        })
                    });
                });
            });
        </script>
        <script>
            $(document).ready(function () {
                $(document).on('change', '.pricetype', function () {
                    var id = $(this).val();
                    // alert(id);
                    var currency = $("#currency option:selected").val();
                    var typeofservice = $("#typeofservice option:selected").val();
                    var serviceperiod = $("#serviceperiod  option:selected").val();
                    var totalprice1 = $(".totalprice").val();
                    var pricing = $("#pricetype  option:selected").val();
                    $.get('<?php echo URL::to('salestax'); ?>?id=' + id + '&typeofservice=' + typeofservice + '&serviceperiod=' + serviceperiod + '&currency=' + currency, function (data) {
                        $('.regularprice').val('');
                        $('.comboprice').val('');
                        $('#serviceincludes').val('');
                        $.each(data, function (index, subcatobj) {
                            $('.regularprice1').val(subcatobj.regularprice1);
                            $('.comboprice1').val(subcatobj.comboprice1);//alert();
                            if (pricing == 'Regular') {
                                $('.sumprice').val(subcatobj.regularprice1);
                                $(".totalprice1").val(subcatobj.regularprice1);

                                $("#taxprice").val(subcatobj.regularprice1);
                            }
                            if (pricing == 'Combo') {
                                $('.sumprice').val(subcatobj.comboprice1);
                                $(".totalprice1").val(subcatobj.comboprice1);
                                $("#taxprice").val(subcatobj.comboprice1);
                            }
                        })
                    });
                });
            });
            $(document).ready(function () {
                $(document).on('change', '#typeofservice', function () {
                    var id = $(this).val();
                    //alert(id);
                    var currency = $("#currency option:selected").val();
                    var typeofservice = $("#typeofservice option:selected").val();
                    var serviceperiod = $("#serviceperiod  option:selected").val();
                    var pricing = $("#pricetype  option:selected").val();
                    var total = $("#totalpri").val();
                    $.get('<?php echo URL::to('salestax'); ?>?id=' + id + '&typeofservice=' + typeofservice + '&serviceperiod=' + serviceperiod + '&currency=' + currency, function (data) {
                        $('.regularprice').val('');
                        $('.comboprice').val('');
                        $('.serviceincludes').val('');
                        $('#totalpri').val('');
                        $('#totalprice1').val('');
                        $.each(data, function (index, subcatobj) {
                            $('.regularprice').val(subcatobj.regularprice);
                            $('.comboprice').val(subcatobj.comboprice);
                            if (pricing == 'Regular') {
                                $('#price').val(subcatobj.regularprice);
                                $(".totalprice1").val(subcatobj.regularprice);
                            }
                            if (pricing == 'Combo') {
                                $('#price').val(subcatobj.comboprice);
                                $(".totalprice1").val(subcatobj.comboprice);
                            }
                            $('#serviceincludes').val(subcatobj.serviceincludes);
                        })
                    });
                });
                // discount	
                $('.discountprice').blur(function () {

                    var disprice = $(this).val();
                    var ckq = $(".ckq").val();
                    var totalprice = $('#totalprice1').val();
                    // alert(totalprice);
                    var totalprice11 = totalprice.replace('$ ', '');
                    // alert(totalprice);
                    var sumtotal = parseFloat(totalprice11) + parseFloat(disprice);
                    var minusdis = parseFloat(sumtotal) - parseFloat(disprice);
                    //alert(sumtotal);

                    var totalprice1 = $("#totalpri").val();
                    var totalprices = totalprice1.replace('$ ', '');

                    var total = parseFloat(totalprices.replace(/[^\d\.]*/g, '') || 0);
                    var vk = parseFloat($(this).val().replace(/[^\d\.]*/g, '') || 0);
                    var sign3 = parseFloat(Math.round(vk * 100) / 100).toFixed(2);
                    var n1 = sign3.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    var x2 = ckq + ' ' + n1;
                    $(this).val(x2);
                    var dis = parseFloat(x2.replace(/[^\d\.]*/g, ''));
                    var ciscount = (total + dis ? total - dis : total);
                    var x21 = parseFloat(Math.round(ciscount * 100) / 100).toFixed(2);
                    var no2 = x21.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    var x11 = ckq + ' ' + no2;
                    $("#totalprice1").val(x11);
                });
                // we used jQuery 'keyup' to trigger the computation as the user type
                $('.price').blur(function () {
                    var ckq = $(".ckq").val();
                    var sum = 0;
                    var vk = parseFloat($(this).val().replace(/[^\d\.]*/g, '') || 0);
                    var sign3 = parseFloat(Math.round(vk * 100) / 100).toFixed(2);
                    var n1 = sign3.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    var x2 = ckq + ' ' + n1;
                    //$(this).val(x2);
                    // $('.price').each(function() 
                    //{
                    // sum += parseFloat($(this).val().replace( /[^\d\.]*/g, '') || 0);//alert(sum);
                    // });
                    // var sum1 = sum;
                    // var sign2 =parseFloat(Math.round(sum1 * 100) / 100).toFixed(2);
                    // var no = sign2.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    // var x1 = ckq+' '+no;
                    //    $('.totalprice1').val(x1);
                });

                $(document).on('change', '#currency', function () {
                    var id = $("#currency").val();
                    var clientid = "<?php echo $common->cid;?>";
                    $.get('<?php echo URL::to('getsign'); ?>?id=' + id + '&clientid=' + clientid, function (data) {
                        $('#ckq').empty();
                        $.each(data, function (index, subcatobj) {
                            $(".ckq").val(subcatobj.sign);
                            $(".ser").load(" .ser>*");
                        })
                    });
                });

                $(document).on('change', '#pricetype', function () {
                    var id = $("#pricetype").val();
                    var clientid = "<?php echo $common->cid;?>";
                    $.get('<?php echo URL::to('getsign'); ?>?id=' + id + '&clientid=' + clientid, function (data) {
                        $.each(data, function (index, subcatobj) {
                            $(".ser").load(" .ser>*");
                        })
                    });
                });
                $(document).on('change', '#serviceperiod', function () {
                    var id = $("#serviceperiod").val();
                    var clientid = "<?php echo $common->cid;?>";
                    //alert(clientid);
                    $.get('<?php echo URL::to('getsign'); ?>?id=' + id + '&clientid=' + clientid, function (data) {
                        // $('#ckq').empty();
                        $.each(data, function (index, subcatobj) {
                            //  $(".ckq").val(subcatobj.sign);
                            $(".ser").load(" .ser>*");
                        })
                    });
                });
            });
        </script>
        <script>
            $(document).ready(function () {
                $(document).on('change', '#business_catagory_name', function () {
                    var id = $(this).val(); //alert();
                    $.get('<?php echo URL::to('getcatm'); ?>?id=' + id, function (data) {
                        if (data == "") {

                        } else {

                        }
                        $('#image1').empty();
                        $('#business_brand_name').empty();
                        $('.cat2').hide();
                        $('.cat3').hide();
                        $('#business_brand_name').append('<option value="">---Select---</option>');
                        $('#business_brand_category_name').append('<option value="">---Select---</option>');
                        $.each(data, function (index, subcatobj) {
                            $('.cat2').show();
                            $('#business_brand_name').append('<option value="' + subcatobj.id + '">' + subcatobj.business_brand_name + '</option>');
                        })
                    });
                });
            });
        </script>
        <script type="text/javascript">
            function checkemail() {
                var client_id = document.getElementById("fileno").value;
                if (client_id) {
                    $.ajax({
                        type: 'get',
                        url: '<?php echo URL::to('/getclick'); ?>',
                        data: {
                            client_id: client_id,
                        },
                        success: function (response) {
                            $('#email_status').html(response);
                            if (response == "OK") {
                                return true;
                            } else {
                                return false;
                            }
                        }
                    });
                } else {
                    $('#email_status').html("");
                    return false;
                }
            }

            function checkall() {
                var emailhtml = document.getElementById("email_status").innerHTML;

                if ((emailhtml) == "OK") {
                    return true;
                } else {
                    return false;
                }
            }
        </script>
        <script>
            $(document).ready(function () {
                $("select#status").change(function () {
                    var selectedCountry = $(this).children("option:selected").val();
                    // alert(selectedCountry);
                    var color = $("option:selected", this).attr("class");
                    $("#status").attr("class", color).addClass("form-control1 fsc-input");
                });
            });
        </script>
        <script>
            $(document).ready(function () {
                $("#mailing_zip").keyup(function () {
                    //console.log('htm');
                    var id = $(this).val();
                    $.get('<?php echo URL::to('/getzip'); ?>?zip=' + id, function (data) {
                        $('#mailing_state').empty();
                        $('#mailing_city').empty();//$('#countryId').empty();
                        $.each(data, function (index, subcatobj) {
                            $('#city').removeAttr("disabled");
                            $('#stateId').removeAttr("disabled");
                            $('#mailing_city').val(subcatobj.city);
                            $('#mailing_state').append('<option value="' + subcatobj.state + '">' + subcatobj.state + '</option>');
                            //$('#countryId').append('<option value="'+subcatobj.country+'">'+subcatobj.country+'</option>');
                        })
                    });
                });
            });
        </script>
        <script>
            $(document).ready(function () {
                $("#bussiness_zip").keyup(function () {
                    var id = $(this).val();
                    $.get('<?php echo URL::to('/getzip'); ?>?zip=' + id, function (data) {
                        $('#business_state').empty();
                        $('business_city').empty();
                        $('#business_country').empty();
                        $.each(data, function (index, subcatobj) {
                            $('#city').removeAttr("disabled");
                            $('#stateId').removeAttr("disabled");
                            $('#business_city').val(subcatobj.city);
                            $('#business_state').append('<option value="' + subcatobj.state + '">' + subcatobj.state + '</option>');
                            $('#business_country').append('<option value="' + subcatobj.country + '">' + subcatobj.country + '</option>');
                        })
                    });
                });
            });
        </script>
        <script>

            $('.telephoneNo1Type').on('change', function () {
                if (this.value == 'Mobile') {

                    $('.hidebasicext').hide();
                    $('.showbasicguardian').show();

                } else {
                    $('.hidebasicext').hide();
                    $('.showbasicguardian').show();

                }

            });

            $('.telephoneNo2Type').on('change', function () {
                if (this.value == 'Mobile') {
                    //alert();
                    $('.hidebasicext2').hide();
                    $('.showbasicguardian2').show();

                } else {
                    $('.hidebasicext2').hide();
                    $('.showbasicguardian2').show();

                }

            });

            var date = $('#ext2_1').val();
            $('#mobiletype_1').on('change', function () {
                if (this.value == 'Home') {
                    $('.hideext').show();
                    $('.showguardian').hide();

                    document.getElementById('ext2_1').removeAttribute('readonly');
                    $('#ext2_1').val();
                } else if (this.value == 'Mobile') {
                    $('.hideext').hide();
                    $('.showguardian').show();

                } else if (this.value == 'Business') {
                    $('.hideext').show();
                    $('.showguardian').hide();

                    document.getElementById('ext2_1').removeAttribute('readonly');
                    $('#ext2_1').val();
                } else {
                    $('.hideext').show();
                    $('.showguardian').hide();

                    document.getElementById('ext2_1').readOnly = true;
                    $('#ext2_1').val('');
                }
            })
        </script>
        <script>
            var date = $('#ext2_2').val();
            var mobi = $('#mobiletype_2').val();
            if (mobi == 'Mobile') {
                $('.showguardian2').show();
                $('.hideext2').hide();
            } else {
                $('.showguardian2').hide();
                $('.hideext2').show();
            }
            $('#mobiletype_2').on('change', function () {
                if (this.value == 'Home') {
                    $('.hideext2').show();
                    $('.showguardian2').hide();

                    document.getElementById('ext2_2').removeAttribute('readonly');
                    $('#ext2_2').val();
                } else if (this.value == 'Business') {
                    $('.hideext2').show();
                    $('.showguardian2').hide();

                    document.getElementById('ext2_2').removeAttribute('readonly');
                    $('#ext2_2').val();
                } else if (this.value == 'Mobile') {
                    $('.hideext2').hide();
                    $('.showguardian2').show();

                } else {
                    $('.hideext2').show();
                    $('.showguardian2').hide();

                    document.getElementById('ext2_2').readOnly = true;
                    $('#ext2_2').val('');
                }
            })
        </script>
        <script>
            var date = $('#businessext').val();


            var officess1 = $('#mobiletype_1').val();
            if (officess1 == 'Office' || officess1 == 'Business') {
                document.getElementById('ext2_1').removeAttribute('readonly');

            }
            $('#mobiletype_1').on('change', function () {
                if (this.value == 'Office') {
                    document.getElementById('ext2_1').removeAttribute('readonly');
                    $('#ext2_1').val();
                } else if (this.value == 'Business') {
                    document.getElementById('ext2_1').removeAttribute('readonly');
                    $('#ext2_1').val();
                } else {
                    document.getElementById('ext2_1').readOnly = true;
                    $('#ext2_1').val('');
                }
            });


            var officess = $('#telephoneNo1Type').val();
            if (officess == 'Office' || officess == 'Business') {
                document.getElementById('businessext').removeAttribute('readonly');

            }
            $('#telephoneNo1Type').on('change', function () {
                if (this.value == 'Office') {
                    document.getElementById('businessext').removeAttribute('readonly');
                    $('#businessext').val();
                } else if (this.value == 'Business') {
                    document.getElementById('businessext').removeAttribute('readonly');
                    $('#businessext').val();
                } else {
                    document.getElementById('businessext').readOnly = true;
                    $('#businessext').val('');
                }
            });


            $('#businesstype').on('change', function () {
                if (this.value == 'Office') {
                    document.getElementById('businessext').removeAttribute('readonly');
                    $('#businessext').val();
                } else if (this.value == 'Business') {
                    document.getElementById('businessext').removeAttribute('readonly');
                    $('#businessext').val();
                } else {
                    document.getElementById('businessext').readOnly = true;
                    $('#businessext').val('');
                }
            })
        </script>
        <script>
            $(document).ready(function () {
                $('#business_name').keyup(function () {
                    var aa = this.value;
                    $('#business_store_name').val(aa);
                });
                $('#address').keyup(function () {
                    var aa1 = this.value;
                    $('#business_address').val(aa1);
                });
            });
        </script>
        <script>
            $(document).ready(function () {
                $(document).on('change', '#location_county', function () {
                    var id = $('#location_county').val();// alert(id);
                    $.get('<?php echo URL::to('getcountycod'); ?>?id=' + id, function (data) {
                        $('#physical_county_no').empty();
                        $.each(data, function (index, subcatobj) {
                            $('#physical_county_no').val(subcatobj.countycode);
                        })
                    });
                });
            });
        </script>
        <script>
            $(document).on('click', '.resetServiceInfo', function () {
                var id = $(this).attr('id'); //alert(id);
                if (confirm("Are you sure you want to reset service information data?")) {
                    $.ajax({
                        url: "<?php echo e(route('resetServiceInfo.resetServiceInfo')); ?>",
                        mehtod: "get",
                        async: true,
                        // cache:false,
                        data: {id: id},
                        success: function (data) {

                            window.location.href = window.location.href;

                        }
                    })
                } else {
                    alert('22');
                    return false;
                }
            });

        </script>
        <script>
            $(document).on('click', '.delete2', function () {
                var id = $(this).attr('id'); //alert(id);
                var price_1 = '#taxprice3_' + id;
                var ckq = $(".ckq").val();
                var pr = $(price_1).val(); //alert(pr);
                var price1 = parseFloat(pr.replace(/[^\d\.]*/g, ''));
                var totalprice1 = $(".totalprice1").val();
                var totals = parseFloat(totalprice1.replace(/[^\d\.]*/g, ''));
                var tt = totals - price1;
                var x1 = ckq + ' ' + tt;
                var totalprice2 = $(".totalprice1").val(x1);
                if (confirm("Are you sure you want to Delete this data?")) {
                    $.ajax({
                        url: "<?php echo e(route('servicedeletes.servicedels')); ?>",
                        mehtod: "get",
                        data: {id: id},
                        success: function (data) {
                            //  alert(data);
                            $('#ser_' + id).remove();
                            $("#table").load(" #table > *");
                            window.location.reload();
                        }
                    })
                } else {
                    return false;
                }
            });

            $(document).on('click', '.delete1', function () {
                var id = $(this).attr('id');

                var price_1 = '#price3_' + id;
                var typeofservices = '#typeofservice_' + id;
                //alert(typeofservices);
                var tt1 = $(typeofservices).val();
                // alert(tt1);
                //return false;
                var clientid = "<?php echo $common->cid;?>";

                var ckq = $(".ckq").val();
                var pr = $(price_1).val(); //alert(pr);
                var price1 = parseFloat(pr.replace(/[^\d\.]*/g, ''));
                var totalprice1 = $(".totalprice1").val();
                var totals = parseFloat(totalprice1.replace(/[^\d\.]*/g, ''));
                var tt = totals - price1;
                var x1 = ckq + ' ' + tt;
                var totalprice2 = $(".totalprice1").val(x1);

                if (confirm("Are you sure you want to Delete this data?")) {

                    $.ajax({
                        url: "<?php echo e(route('servicedeletes.servicedel1')); ?>",
                        mehtod: "get",
                        data: {'id': id, 'tos': tt1, 'clientid': clientid},
                        async: false,
                        success: function (data) {
                            // alert(data);
                            $('#ser_' + id).remove();
                            $("#typeofservice").load(" #typeofservice > *");
                            // location.reload(true);
                            window.location.href = window.location.href;


                        }
                    })
                } else {
                    return false;
                }
            });
            $(document).on('click', '.deleteacc', function () {
                var id = $(this).attr('id');
                var clientid = "<?php echo $common->cid;?>";
                if (confirm("Are you sure you want to Delete this data?")) {

                    $.ajax({
                        url: "<?php echo e(route('servicedeletes.deleteacc')); ?>",
                        mehtod: "get",
                        data: {'id': id, 'clientid': clientid},
                        async: false,
                        success: function (data) {

                            window.location.href = window.location.href;

                        }
                    })
                } else {
                    return false;
                }
            });
            $(document).on('click', '.taxdelete1', function () {
                var id = $(this).attr('id');

                // var price_1 = '#price3_'+id;
                // alert(tt1);
                //return false;
                var clientid = "<?php echo $common->cid;?>";
                var ttax = '.ttax_' + id;
                var tt1 = $(ttax).val();


                if (confirm("Are you sure you want to Delete this data?")) {

                    $.ajax({
                        url: "<?php echo e(route('servicedeletes.taxservicedel1')); ?>",
                        mehtod: "get",
                        data: {'id': id, 'ttax': tt1, 'clientid': clientid},
                        async: false,
                        success: function (data) {
                            // alert(data);
                            $('#ser_' + id).remove();
                            //   $("#typeofservice").load(" #typeofservice > *");
                            // location.reload(true);
                            window.location.href = window.location.href;


                        }
                    })
                } else {
                    return false;
                }
            });

            function FillBilling1(f) {
                if (f.billingtoo1.checked == true) { //alert();
                    f.business_store_name.value = f.business_name.value;
                    f.business_address.value = f.address.value;
                    f.business_address_1.value = f.address1.value;
                    f.business_city.value = f.city.value;
                    f.business_state.value = f.stateId.value;
                    f.bussiness_zip.value = f.zip.value;
                    f.business_country.value = f.countryId.value;
                } else {
                    f.business_store_name.value = '';
                    f.business_address.value = '';
                    f.business_address_1.value = '';
                    f.business_city.value = '';
                    f.business_state.value = '';
                    f.bussiness_zip.value = '';
                    f.business_state.value = '';
                    f.business_country.value = '';
                }
            }
        </script>
        <script>

            $(".effective_date").datepicker({
                autoclose: true,
                orientation: "top",
                endDate: "today"
            });
            <?php if($notecon1){?>
            var room2 = <?php echo $notecon1;?>;
            <?php
            }
            else
            {
            ?>
            var room2 = 1;
            <?php
            }
            ?>
            var room = room2;//alert(room);
            function education_fields() {
                room++;
                var objTo = document.getElementById('education_fields')
                var divtest = document.createElement("div");
                divtest.setAttribute("class", "row removeclass" + room);
                divtest.innerHTML = '<div class="col-md-12"><div class="form-group"><label class="control-label col-md-3">Subject ' + room + ' :</label><div class="col-md-6"><input name="adminsubject[]" value=""  type="text" placeholder="Create Subject" id="adminsubject" class=" form-control"></div></div></div><div class="col-md-12"><div class="form-group"><label class="control-label col-md-3">Note ' + room + ' :</label><div class="col-md-6"><input name="noteid[]" value="" type="hidden" placeholder="" id="noteid" class="textonly form-control"><input name="usid[]" value="" type="hidden" placeholder="Last Name" id="usid" class="textonly form-control"><input name="notetype[]" value="admin" type="hidden" placeholder="Last Name" id="notetype" class="textonly form-control"><input name="adminnotes[]" value="" type="text" id="adminnotes" placeholder="Create Note" class="textonly form-control" /></div><button class="btn btn-danger" type="button" onclick="remove_education_fields(' + room + ');"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></button></div></div></div></div></div>';
                var rdiv = 'removeclass' + room;
                var rdiv1 = 'Schoolname' + room;
                objTo.appendChild(divtest)
                $(".effective_date").datepicker({
                    autoclose: true,
                    orientation: "top",
                    endDate: "today"
                });
                $('select').on('change', function () {
                    $('#text1').val(this.value);
                });
                jQuery(function ($) {
                    var input = $('input,textarea,select,checkbox');
                    input.on('keydown', function () {
                        var key = event.keyCode || event.charCode || event.which;
                        if (key == 8 || key == 46) {
                            $('#text1').val(key);
                        } else {
                            $('#text1').val(key);
                        }
                    });
                    $(".check").click(function () { //alert();
                        $("#text1").val('121');
                    });
                    $(".check").click(function () { //alert();
                        $("#text1").prop("checked", false);
                    });
                });
                $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                    var currentTab = $(e.target).text(); // get current tab
                    var vf = $('#text1').val();//alert(vf);
                    var current_tab = e.target;
                    var previousTab = $(e.relatedTarget).text();
                    var target = $(e.relatedTarget).attr("href");
                    $('.no-button').eq(0).attr('href', target);
                    var href = $('.no-button').attr('href');
                    if (target == href) {
                        if (vf) {
                            $("#alerts").modal({
                                show: true,
                            });
                        } else {
                        }
                    } else {
                    }
                });
                //$(document).on('click', '.no-button', function () {
                //   var href = $(this).attr('href');
                //    var $link = $('li.active a[data-toggle="tab"]');
                //$link.parent().removeClass('active');
                // var tabLink = $link.attr('href');//alert(tabLink);
                //  $('#alerts').modal('hide');
                //  $('#myTab a[href="' + href + '"]').tab('show');
                //  });
            }

            function remove_education_fields(rid) {
                alert(rid);
                //  alert('1xx');
                var price_1 = '#taxprice1_' + rid;
                var ckq = $(".ckq").val();
                var pr = $(price_1).val();
                if (pr) {
                    var pr1 = pr;
                } else {
                    var pr1 = '0';
                }
                // alert(pr);
                var price1 = parseFloat(pr1.replace(/[^\d\.]*/g, ''));
                var totalprice1 = $(".totalprice1").val();
                var totals = parseFloat(totalprice1.replace(/[^\d\.]*/g, ''));
                var tt = totals - price1;
                var x1 = ckq + ' ' + tt;
                var totalprice2 = $(".totalprice1").val(x1);
                $('.removeclass' + rid).remove();
                room - 1;
            }

            $('#filename').mask('aa-999-9999999');
            $('input[name="filename"]').focusout(function () {
                $('input[name="filename"]').val(this.value.toUpperCase());
            });
            var room2 = <?php echo $notecon2;?>;
            var room3 = 1;
            var room1 = room2 + room3;

            function education_field() {
                room1++;
                var objTo = document.getElementById('education_field')
                var divtest = document.createElement("div");
                divtest.setAttribute("class", "row removeclass1" + room1);
                divtest.innerHTML = '<div class="col-md-12"><div class="form-group"><label class="control-label col-md-3">Subject ' + room1 + ' :</label><div class="col-md-6"><input name="adminsubject[]" value=""  type="text" placeholder="Create Subject" id="adminsubject" class=" form-control"></div></div></div><div class="col-md-12"><div class="form-group"><label class="control-label col-md-3">Note ' + room1 + ' :</label><div class="col-md-6"><input name="noteid[]" value="" type="hidden" placeholder="" id="noteid" class="textonly form-control"><input name="usid[]" value="" type="hidden" placeholder="Last Name" id="usid" class="textonly form-control"><input name="notetype[]" value="fsc" type="hidden" placeholder="Last Name" id="notetype" class="textonly form-control"><input name="adminnotes[]" value="" type="text" id="adminnotes" placeholder="Create Note" class="textonly form-control" /></div><button class="btn btn-danger" type="button" onclick="remove_education_field(' + room1 + ');"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span> </button></div></div></div></div></div>';
                var rdiv = 'removeclass1' + room1;
                var rdiv1 = 'Schoolname' + room1;
                objTo.appendChild(divtest)
                $(".effective_date").datepicker({
                    autoclose: true,
                    orientation: "top",
                    endDate: "today"
                });
                $('select').on('change', function () {
                    $('#text1').val(this.value);
                });
                jQuery(function ($) {
                    var input = $('input,textarea,select,checkbox');
                    input.on('keydown', function () {
                        var key = event.keyCode || event.charCode || event.which;

                        if (key == 8 || key == 46) {
                            $('#text1').val(key);
                        } else {
                            $('#text1').val(key);
                        }
                    });
                    $(".check").click(function () {
                        $("#text1").val('121');
                    });
                    $(".check").click(function () {
                        $("#text1").prop("checked", false);
                    });
                });
                $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                    var currentTab = $(e.target).text(); // get current tab
                    var vf = $('#text1').val();//alert(vf);
                    var current_tab = e.target;
                    var previousTab = $(e.relatedTarget).text();
                    var target = $(e.relatedTarget).attr("href");
                    $('.no-button').eq(0).attr('href', target);
                    var href = $('.no-button').attr('href');
                    if (target == href) {
                        if (vf) {// alert('2');
                            $("#alerts").modal({
                                show: true,
                            });
                        } else {
                        }
                    } else {
                    }
                });
                // $(document).on('click', '.no-button', function () {
                // var href = $(this).attr('href');
                //  var $link = $('li.active a[data-toggle="tab"]');
                // $link.parent().removeClass('active');
                // var tabLink = $link.attr('href');//alert(tabLink);
                //  $('#alerts').modal('hide');
                // $('#myTab a[href="' + href + '"]').tab('show');
                // });
            }

            function remove_education_field(rid) {
                $('.removeclass1' + rid).remove();
            }

            var room4 = <?php echo $notecon3;?>;
            var room3 = 1;
            var room2 = room4 + room3;

            function education_field1() {
                room2++;
                var objTo = document.getElementById('education_field1')
                var divtest = document.createElement("div");
                divtest.setAttribute("class", "row removeclass2" + room2);
                divtest.innerHTML = '<div class="col-md-12"><div class="form-group"><label class="control-label col-md-3">Subject ' + room2 + ' :</label><div class="col-md-6"><input name="adminsubject[]" value=""  type="text" placeholder="Create Subject" id="adminsubject" class=" form-control"></div></div></div><div class="col-md-12"><div class="form-group"><label class="control-label col-md-3">Note ' + room2 + ' :</label><div class="col-md-6"><input name="noteid[]" value="" type="hidden" placeholder="" id="noteid" class="textonly form-control"><input name="notetype[]" value="client" type="hidden" placeholder="Last Name" id="notetype" class="textonly form-control"><input name="usid[]" value="<?php echo e($common->cid); ?>" type="hidden" placeholder="Last Name" id="usid" class="textonly form-control"><input name="adminnotes[]" value="" type="text" id="adminnotes" placeholder="Create Note" class="textonly form-control" /></div><button class="btn btn-danger" type="button" onclick="remove_education_field1(' + room2 + ');"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span> </button></div></div></div></div></div>';
                var rdiv = 'removeclass2' + room2;
                var rdiv1 = 'Schoolname' + room2;
                objTo.appendChild(divtest)
                $(".effective_date").datepicker({
                    autoclose: true,
                    orientation: "top",
                    endDate: "today"
                });
                $('select').on('change', function () {
                    $('#text1').val(this.value);
                });
                jQuery(function ($) {
                    var input = $('input,textarea,select,checkbox');
                    input.on('keydown', function () {
                        var key = event.keyCode || event.charCode || event.which;

                        if (key == 8 || key == 46) {
                            $('#text1').val(key);
                        } else {
                            $('#text1').val(key);
                        }
                    });
                    $(".check").click(function () { //alert();
                        $("#text1").val('121');
                    });
                    $(".check").click(function () { //alert();
                        $("#text1").prop("checked", false);
                    });
                });
                $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                    var currentTab = $(e.target).text(); // get current tab
                    var vf = $('#text1').val();//alert(vf);
                    var current_tab = e.target;
                    var previousTab = $(e.relatedTarget).text();
                    var target = $(e.relatedTarget).attr("href");
                    $('.no-button').eq(0).attr('href', target);
                    var href = $('.no-button').attr('href');

                    if (target == href) {
                        if (vf) {// alert('2');
                            $("#alerts").modal({
                                show: true,
                            });
                        } else {
                        }
                    } else {
                    }
                });
                //$(document).on('click', '.no-button', function () {
                //var href = $(this).attr('href');
                //  var $link = $('li.active a[data-toggle="tab"]');
                // $link.parent().removeClass('active');
                // var tabLink = $link.attr('href');//alert(tabLink);
                //  $('#alerts').modal('hide');
                ///  $('#myTab a[href="' + href + '"]').tab('show');
                // });
            }

            function remove_education_field1(rid) {
                $('.removeclass2' + rid).remove();
            }

            var room4 = <?php echo $notecon;?>;
            var room5 = 1;
            var room3 = room4 + room5;

            function education_field2() {
                room3++;
                var objTo = document.getElementById('education_field2')
                var divtest = document.createElement("div");
                divtest.setAttribute("class", "row removeclass3" + room3);
                divtest.innerHTML = '<div class="col-md-12"><div class="form-group"><label class="control-label col-md-3">Subject ' + room3 + ' :</label><div class="col-md-6"><input name="adminsubject[]" value=""  type="text" placeholder="Create Subject" id="adminsubject" class=" form-control"></div></div></div><div class="col-md-12"><div class="form-group"><label class="control-label col-md-3">Note ' + room3 + ' :</label><div class="col-md-6"><input name="noteid[]" value="" type="hidden" placeholder="" id="noteid" class="textonly form-control"><input name="notetype[]" value="Everybody" type="hidden" placeholder="Last Name" id="notetype" class="textonly form-control"><input name="usid[]" value="" type="hidden" placeholder="Last Name" id="usid" class="textonly form-control"><input name="adminnotes[]" value="" type="text" id="adminnotes" placeholder="Create Note" class="textonly form-control" /></div><button class="btn btn-danger" type="button" onclick="remove_education_field2(' + room3 + ');"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span> </button></div></div></div></div></div>';
                var rdiv = 'removeclass3' + room3;
                var rdiv1 = 'Schoolname' + room3;
                objTo.appendChild(divtest)
                $(".effective_date").datepicker({
                    autoclose: true,
                    orientation: "top",
                    endDate: "today"
                });
                $('select').on('change', function () {
                    $('#text1').val(this.value);
                });
                jQuery(function ($) {
                    var input = $('input,textarea,select,checkbox');
                    input.on('keydown', function () {
                        var key = event.keyCode || event.charCode || event.which;
                        if (key == 8 || key == 46) {
                            $('#text1').val(key);
                        } else {
                            $('#text1').val(key);
                        }
                    });
                    $(".check").click(function () { //alert();
                        $("#text1").val('121');
                    });
                    $(".check").click(function () { //alert();
                        $("#text1").prop("checked", false);
                    });
                });
                $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                    var currentTab = $(e.target).text(); // get current tab
                    var vf = $('#text1').val();//alert(vf);
                    var current_tab = e.target;
                    var previousTab = $(e.relatedTarget).text();
                    var target = $(e.relatedTarget).attr("href");
                    $('.no-button').eq(0).attr('href', target);
                    var href = $('.no-button').attr('href');
                    if (target == href) {
                        if (vf) {// alert('2');
                            $("#alerts").modal({
                                show: true,
                            });
                        } else {
                        }
                    } else {
                    }
                });
                //$(document).on('click', '.no-button', function () {
                //var href = $(this).attr('href');
                //  var $link = $('li.active a[data-toggle="tab"]');
                //   $link.parent().removeClass('active');
                //   var tabLink = $link.attr('href');//alert(tabLink);
                //    $('#alerts').modal('hide');
                // $('#myTab a[href="' + href + '"]').tab('show');
                //});
            }

            function remove_education_field2(rid) {
                $('.removeclass3' + rid).remove();
            }
        </script>
        <script>
            $(document).ready(function () {
                $('#resetdays').on('change', function () {
                    var reset = parseInt($('#resetdays').val());
                    var date = new Date();
                    var t = new Date();
                    //var n = $("#resetdays").val(); 
                    //alert(offset);
                    t.setDate(t.getDate() + reset);
                    var month = "0" + (t.getMonth() + 1);
                    var date = "0" + t.getDate();
                    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];
                    const d = new Date();
                    month = month.slice(-2);
                    date = date.slice(-2);
                    //var date = date +"/"+month+"/"+t.getFullYear();
                    var date = monthNames[t.getMonth()] + "-" + date + "-" + t.getFullYear();
                    $('#reset_date').val(date);
                });
            });
            $(document).ready(function () {
                /***phone number format***/
                $(".phone").keypress(function (e) {
                    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                        return false;
                    }
                    var curchr = this.value.length;
                    var curval = $(this).val();
                    if (curchr == 3 && curval.indexOf("(") <= -1) {
                        $(this).val("(" + curval + ")" + " ");
                    } else if (curchr == 4 && curval.indexOf("(") > -1) {
                        $(this).val(curval + ") ");
                    } else if (curchr == 5 && curval.indexOf(")") > -1) {
                        $(this).val(curval + "-");
                    } else if (curchr == 9) {
                        $(this).val(curval + "-");
                        $(this).attr('maxlength', '14');
                    }
                });
                $(document).on('change', '#typeofservice', function () {
                    var sign1 = $(this).val();
                    if (sign1 == '3') {
                        $('.input_fields_wrap_notes').show();
                        $('#regular').hide();
                    } else {
                        $('.input_fields_wrap_notes').hide();
                        $('#regular').show();
                    }
                });
            });

            function getTotal() {
                var total = 0;
                $('.price').each(function () {
                    total += parseFloat(this.innerHTML)
                });
                $('#total').text(total);
            }

            getTotal();
        </script>
        <?php $__currentLoopData = $note; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ak1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php if($ak1->notes==NULL): ?>
            <?php else: ?>
                <div id="myModalnote_<?php echo e($ak1->id); ?>" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;
                                </button>
                                <h4 class="modal-title">Confirmation</h4>
                            </div>
                            <div class="modal-body">
                                <p>Do you want to this record ?</p>
                            </div>
                            <div class="modal-footer">
                                <a href="<?php echo e(route('notes.notedelete',$ak1->id)); ?>" class="btn btn-danger">Delete</a>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <script>
            var emp = 0;
            var empcount = 0;
            var z1 = emp + empcount;

            function employee_responsivility() {
                emp++;
                z1++;
                var objTo = document.getElementById('emptable')
                var divtest = document.createElement("tbody");
                divtest.setAttribute("class", "form-group empclass" + z1);
                divtest.innerHTML = '<tr><input type="hidden" class="form-control" name="empid[]" value=""><td style="width:25% !important;"><select name="typeofservices[]" type="text" id="typeofservices_' + z1 + '" class="form-control"><option value="">Type of Service</option><?php $__currentLoopData = $typeofser; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $typeofser1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?><option value="<?php echo e($typeofser1->id); ?>"><?php echo e($typeofser1->typeofservice); ?></option> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></select></td><td><select name="periods[]" id="periods_' + z1 + '" class="form-control"><option value="">Select</option> <?php $__currentLoopData = $period; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $period1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?><option value="<?php echo e($period1->id); ?>" ><?php echo e($period1->period); ?></option> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></select></td><td><select name="employee_res[]" class="form-control"><option value="">Select</option><?php $__currentLoopData = $employee1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $emp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?><option value="<?php echo e($emp->id); ?>"><?php echo e($emp->firstName.' '.$emp->middleName.' '.$emp->lastName); ?><?php if($emp->teams !=''): ?> (<?php echo e($emp->teams); ?>)<?php endif; ?></option><?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></select></td><td><input type="text" class="form-control" name="monthlynote[]"></td><td style="width:9.2%"></td><td><button class="btn btn-danger" type="button" onclick="remove_employee_responsivilitys(' + z1 + ');"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></button></td></tr>';
                var rdiv = 'empclass' + z1;
                objTo.appendChild(divtest)
                $('select').on('change', function () {
                    $('#text1').val(this.value);
                });
                jQuery(function ($) {
                    var input = $('input,textarea,select,checkbox');
                    input.on('keydown', function () {
                        var key = event.keyCode || event.charCode || event.which;

                        if (key == 8 || key == 46) {
                            $('#text1').val(key);
                        } else {
                            $('#text1').val(key);
                        }
                    });
                    $(".check").click(function () {
                        $("#text1").val('121');
                    });
                    $(".check").click(function () {
                        $("#text1").prop("checked", false);
                    });

                });
                $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                    var currentTab = $(e.target).text(); // get current tab
                    var vf = $('#text1').val();//alert(vf);
                    var current_tab = e.target;
                    var previousTab = $(e.relatedTarget).text();
                    var target = $(e.relatedTarget).attr("href");
                    $('.no-button').eq(0).attr('href', target);
                    var href = $('.no-button').attr('href');

                    if (target == href) {
                        if (vf) {// alert('2');
                            $("#alerts").modal({
                                show: true,
                            });
                        } else {
                        }
                    } else {
                    }
                });
                //$(document).on('click', '.no-button', function () {
                // var href = $(this).attr('href');
                // var $link = $('li.active a[data-toggle="tab"]');
                // $link.parent().removeClass('active');
                // var tabLink = $link.attr('href');//alert(tabLink);
                //  $('#alerts').modal('hide');
                //  $('#myTab a[href="' + href + '"]').tab('show');
                // });
            }

            function remove_employee_responsivilitys(rid) {
                $('.empclass' + rid).remove();
                emp--;
                z1--;
            }

            $(document).ready(function () {
                $('#user_email').mask('[a-za-zA-Z0-9!#$%&' * +/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?');
                $(document).on('change', '.regularprice', function () {
                    var id = $('#currency').val();
                    var sign1 = $(this).val();
                    var sign2 = parseFloat(Math.round(sign1 * 100) / 100).toFixed(2);
                    var no = sign2.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    $.get('<?php echo URL::to('getsign'); ?>?id=' + id, function (data) {
                        $('.regularprice').empty();
                        $.each(data, function (index, subcatobj) {
                            var val = subcatobj.sign + ' ' + no;
                            $('.regularprice').val(val);
                        })
                    });
                });
            });

            jQuery(function ($) {
                $(document).on('click', '.deletecontact', function () {
                    var id = $(this).attr('id'); //alert(id);
                    if (confirm("Are you sure you want to Delete this data?")) {
                        $.ajax({
                            url: "<?php echo e(route('delete1.delete')); ?>",
                            mehtod: "get",
                            data: {id: id},
                            success: function (data) {
                                // alert(data);
                                $('#con_' + id).remove();
                                $(".con").load(".con > *");
                                window.location.reload();

                            }
                        })
                    } else {
                        return false;
                    }
                });
            });

        </script>
        <script type="text/javascript">
            //$(document).ready(function(){
            // 	$('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
            // 		localStorage.setItem('activeTab', $(e.target).attr('href'));
            // 	});
            // 	var activeTab = localStorage.getItem('activeTab');
            ///	if(activeTab){
            //	$('#myTab a[href="' + activeTab + '"]').tab('show');
            // 	}
            // });

            $(document).on('click', '.no-button', function () {
                var href = $(this).attr('href');
                var $link = $('li.active a[data-toggle="tab"]');
                $link.parent().removeClass('active');
                var tabLink = $link.attr('href');//alert(tabLink);
                $('#alerts').modal('hide');
                $('#myTab a[href="' + href + '"]').tab('show');

            });
        </script>


        <script>
            $('select').on('change', function () {
                $('#text1').val(this.value);
            });
            jQuery(function ($) {
                var input = $('input,textarea,select,checkbox');
                input.on('keydown', function () {
                    var key = event.keyCode || event.charCode || event.which;

                    if (key == 8 || key == 46) {
                        $('#text1').val(key);
                    } else {
                        $('#text1').val(key);
                    }
                });
                $(".check").click(function () { //alert();
                    $("#text1").val('121');
                });
                $(".check").click(function () { //alert();
                    $("#text1").prop("checked", false);
                });

            });
            $(document).ready(function () {


                $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                    var currentTab = $(e.target).text(); // get current tab
                    var vf = $('#text1').val();//alert(vf);
                    var current_tab = e.target;
                    var previousTab = $(e.relatedTarget).text();
                    var target = $(e.relatedTarget).attr("href");
                    $('.no-button').eq(0).attr('href', target);
                    var href = $('.no-button').attr('href');

                    if (target == href) {
                        if (vf) {// alert('2');
                            $("#alerts").modal({
                                show: true,
                            });

                        } else {

                        }
                    } else {
                    }

                });
            });

            $(function () {

                $('#login-form-link').click(function (e) {
                    $("#login-form").delay(100).fadeIn(100);
                    $("#register-form").fadeOut(100);
                    $('#register-form-link').removeClass('active');
                    $(this).addClass('active');
                    e.preventDefault();
                });
                $('#register-form-link').click(function (e) {
                    $("#register-form").delay(100).fadeIn(100);
                    $("#login-form").fadeOut(100);
                    $('#login-form-link').removeClass('active');
                    $(this).addClass('active');
                    e.preventDefault();
                });

            });


        </script>
        <script>
            $(function () {
                var eeres = '<?php echo $common->cid;?>';
                $('.primary1').click(function () {
                    $.ajax({
                        type: "post",
                        url: "<?php echo e(route('customer.update', $common->cid)); ?>",
                        dataType: "json",
                        data: $('#registrationForm').serialize(),
                        success: function (data) {
                            alert('1');
                            //   $.get('<?php echo URL::to('/getEEResponsibility'); ?>?client='+eeres, function(data)
                            //     { 
                            //       alert(data);

                            //     });
                            /*  $.ajax({
                    type: "post",
                    url: "<?php echo e(url('/getEEResponsibility')); ?>",
                    data:{'id':eeres},
                    success: function(data){
                        alert(data);
                    }
              });*/

                            //  $.ajax({
                            //  type: "post",
                            //url: "example.php",
                            //    data: 'page=' + a,
                            //  success: function (data) {
//
                            //              }
                            //        });


                            //  $("#location").load(" #location > *");
                            //  $('#alerts').modal('hide');
                            // $('#alertss').modal('show');
                            var vf = $('#text1').val('');
                        },
                        error: function (data) {
                            $.get('<?php echo URL::to('/getEEResponsibility'); ?>?client=' + eeres, function (data) {
                                // alert('testtt');
                            });

                            //  $.ajax({
                            //  type: "get",
                            //url: "<?php echo e(url('/fac-Bhavesh-0554/customer/getEEResponsibility')); ?>",
                            //data:{'id':eeres},
                            //success: function(data){
                            //  alert(data);
                            //    }
                            //});


                            $('#alerts').modal('hide');
                            var vf = $('#text1').val('');
                            $('#alertss1').modal('show');
                            $("#location").load(" #location > *");
                        }
                    });
                });
            });
        </script>
        <script type="text/javascript">
            $(document).on('change', '#limited_active', function () {
                //alert();
                var value = $('#limited_active').val();
                var value1 = $('#email').val();
                var value2 = '<?php echo date('M-d-Y', strtotime("+30 days"));?>';
                var value3 = $('#limited_active').val();
                //alert(value);
                if (value == '1') {
                    $('#limited_user').val(value1);
                    $('#limited_reset_date').val(value2);
                    $('#limited_resetdays').append('<option value="30" selected>30</option>');
                } else {

                }
            });
            $(document).ready(function () {


                $("#button").click(function () {
                    location.reload(true);
                });
            });
        </script>
        <script>
            //   $(document).ready(function(){

            //       //group add limit
            //       var maxGroup = 10;

            //       //add more fields group
            //       $(".addmore").click(function(){  

            //           if($('body').find('.input_fields_wrap_text').length < maxGroup){

            //               var fieldHTML = '<div class="input_fields_wrap_text">'+$(".fieldGroupCopy").html()+'</div>';
            //               $('body').find('.input_fields_wrap_text:last').after(fieldHTML);
            //           }else{
            //               alert('Maximum '+maxGroup+' groups are allowed.');
            //           }
            //       });

            //       //remove fields group
            //       $("body").on("click",".removetext",function(){ 
            //           $(this).parents(".input_fields_wrap_text").remove();
            //       });
            //   });
        </script>

        <script>
            $(document).ready(function () {
                $('#user_resetdays').on('change', function () {
                    var reset = parseInt($('#user_resetdays').val());
                    var date = new Date();
                    var t = new Date();
                    //var n = $("#resetdays").val(); 
                    //alert(offset);
                    t.setDate(t.getDate() + reset);
                    var month = "0" + (t.getMonth() + 1);
                    var date = "0" + t.getDate();

                    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];
                    const d = new Date();

                    month = month.slice(-2);
                    date = date.slice(-2);
                    //var date = date +"/"+month+"/"+t.getFullYear();
                    var date = monthNames[t.getMonth()] + "-" + date + "-" + t.getFullYear();
                    $('#user_reset_date').val(date);
                });
            });

            $(document).ready(function () {
                $('#limited_resetdays').on('change', function () {
                    var reset = parseInt($('#limited_resetdays').val());
                    var date = new Date();
                    var t = new Date();
                    //var n = $("#resetdays").val(); 
                    //alert(offset);
                    t.setDate(t.getDate() + reset);
                    var month = "0" + (t.getMonth() + 1);
                    var date = "0" + t.getDate();

                    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];
                    const d = new Date();

                    month = month.slice(-2);
                    date = date.slice(-2);
                    //var date = date +"/"+month+"/"+t.getFullYear();
                    var date = monthNames[t.getMonth()] + "-" + date + "-" + t.getFullYear();
                    $('#limited_reset_date').val(date);
                });
            });
            $(document).ready(function () {
                $('#subscription_resetdays').on('change', function () {
                    var reset = parseInt($('#subscription_resetdays').val());
                    var date = new Date();
                    var t = new Date();
                    //var n = $("#resetdays").val(); 
                    //alert(offset);
                    t.setDate(t.getDate() + reset);
                    var month = "0" + (t.getMonth() + 1);
                    var date = "0" + t.getDate();

                    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];
                    const d = new Date();

                    month = month.slice(-2);
                    date = date.slice(-2);
                    //var date = date +"/"+month+"/"+t.getFullYear();
                    var date = monthNames[t.getMonth()] + "-" + date + "-" + t.getFullYear();
                    $('#subscription_resetdate').val(date);
                });
            });

            /*  
$("#primary1").blur(function () {
    if(testEmailAddress($("#user_email").val())) {
        $("#msg").text("Is valid email");
    } else {
        $("#msg").text("Is NOT valid email");
    }
});*/

            function testEmailAddress(emailToTest) {
                // check for @
                var atSymbol = emailToTest.indexOf("@");
                if (atSymbol < 1) return false;

                var dot = emailToTest.indexOf(".");
                if (dot <= atSymbol + 2) return false;

                // check that the dot is not at the end
                if (dot === emailToTest.length - 1) return false;

                return true;
            }
        </script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js"></script>
        <script>
            $(document).ready(function () {
                $('#business_brand_name').on('change', function () {
                    if (this.value == 12) {
                        $(".cat3_1").css("margin-top", "10px");
                    } else {
                        $(".cat3_1").css("margin-top", "00px");
                    }
                });
            });
        </script>
        <script>
            // $('#firstlang').change(function(){
            //  $('#langonedrp').show();
            //  var flangval = $('#firstlang').val();
            //  alert(flangval);
            //    seclang.remove(firstlang.selectedIndex);
            //  })

            $(document).on('change', '#firstlang', function () {

                var language = $("#firstlang").val();

                /*  $("#seclang option[value=" + language + "]").hide();*/
                // alert(language);
                if (language == '1') {
                    $('#langonedrp').hide();
                } else if (language != '') {
                    $('#langonedrp').show();
                    $.get('<?php echo URL::to('/getlanguage'); ?>?language=' + language, function (data) {
                        //  var obj = jQuery.parseJSON(data);
                        //$('#other_first_language').val('');
                        $('#other_first_language').empty();
                        $('#other_first_language').append('<option value="">---Select---</option>');
                        $.each(data, function (index, subcatobj) {
                            $('#other_first_language').append('<option value="' + subcatobj.id + '">' + subcatobj.firstName + ' ' + subcatobj.lastName + ' </option>');
                        })


                    });

                } else {

                }
                // var language = "<?php //echo $common->cid;?>"; 

            });

            $(document).on('change', '#seclang', function () {

                var language = $("#seclang").val();
                // alert(language);
                if (language == '2') {
                    $('#langtwodrp').hide();
                } else if (language != '') {
                    $('#langtwodrp').show();
                    $.get('<?php echo URL::to('/getsecondlanguage'); ?>?language=' + language, function (data) {
                        //  var obj = jQuery.parseJSON(data);
                        //$('#other_first_language').val('');
                        $('#other_second_language').empty();
                        $('#other_second_language').append('<option value="">---Select---</option>');
                        $.each(data, function (index, subcatobj) {

                            $('#other_second_language').append('<option value="' + subcatobj.id + '">' + subcatobj.firstName + ' ' + subcatobj.lastName + ' </option>');
                        })


                    });

                } else {
                    $('#langtwodrp').hide();
                }
                // var language = "<?php //echo $common->cid;?>"; 

            });
            /*(function () {
      var previous

    $("#firstlang").on('focus', function () {
        // Store the current value on focus and on change
       
        previous = this.value;
        
    }).change(function() {
          $('#langonedrp').show();
        // Do something with the previous value after the change
         var flangval = $('#firstlang').val();
    
        alert(previous);
         seclang.add(previous);
        seclang.remove(firstlang.selectedIndex);
        
        
       

        // Make sure the previous value is updated
       //previous = this.value;
    });
})();
*/


            $('.firstlang').change(function () {
                $('.firstlang').removeClass('yourClass');
                // loop each select and set the selected value to disabled in all other selects
                $('.firstlang').each(function () {
                    var $this = $(this);
                    $('.firstlang').not($this).find('option').each(function () {
                        if ($(this).attr('value') == $this.val()) {
                            $(this).addClass('yourClass');
                        } else if ($(this).attr('value') == '') {
                            $(this).removeClass('yourClass');
                        } else {
                            $(this).removeClass('yourClass');
                        }
                    });
                });
            });

            $(document).ready(function () {
                $("#user_access4").click(function () {
                    if ($(this).is(":checked")) {
                        $(".subscription_hidden").show();
                        // $("#AddPassport").hide();
                    } else {
                        $(".subscription_hidden").hide();
                        // $("#AddPassport").show();
                    }
                });
            });

            $(document).ready(function () {
                $("#user_access3").click(function () {
                    if ($(this).is(":checked")) {
                        $(".subscription_hidden2").show();
                        // $("#AddPassport").hide();
                    } else {
                        $(".subscription_hidden2").hide();
                        // $("#AddPassport").show();
                    }
                });
            });
            $(document).ready(function () {

                $("#user_access2").click(function () {
                    if ($(this).is(":checked")) {
                        $(".subscription_hidden1").show();
                        // $("#AddPassport").hide();
                    } else {
                        $(".subscription_hidden1").hide();
                        // $("#AddPassport").show();
                    }
                });

                var cnts1 = 1;
                var countplus = '<?php  if (isset($taxservice2)) {
                    echo $taxservice2;
                }?>';
                var counters = cnts1 + countplus;

                $(".addCFnew").click(function () {
                    var businessids = '<?php echo $common->business_id;?>';

                    var taxservice1 = $('.tax_service_1').val();
                    if (taxservice1 == '7') {
                        $('.showblank').hide();
                    } else if (taxservice1 == '8') {
                        $('.showblank').show();
                    }
                    // $('.tax_servicee').show();
                    var taxserv = '<?php  if (isset($taxservice2)) {
                        echo $taxservice2;
                    }?>';
                    // alert(taxserv);
                    if (taxserv > 0) {
                        var showservice = $('.showtitle').html();
                    } else {
                        var showservice = $('.tax_servicee').html();
                    }
                    var taxserviceone = $('.tax_servicee').html();
                    var taxserviceone1 = $('.showtitle').html();
                    //  alert(taxserviceone);
                    counters++;
                    //  $('.zzz_'+counters).hide();
                    $("#customFieldsbo").append('<div class="form-group" style="margin-bottom:0px;"><div class="col-md-12" style="padding-left:0px !important;"><div class="row"><div class="col-md-5"><select class="form-control showtitle tax_service_' + counters + '" name="taxation_service[]">' + showservice + '</select></div><div class="col-md-2" style="padding-left:5px;padding-right:0px;"><select style="padding-left: 2px;" class="form-control tax_period tax_period_' + counters + ' textpriodbox" name="taxation_service_period[]"></select> <input type="text"  class="form-control  taxpariodbox tax_periodbox_' + counters + '" name="taxation_service_period[]" style="display:none;" readonly/></div><div class="col-md-2" ><select class="form-control taxservicemonth_' + counters + '" name="tax_service_month[]"><option value="">Sel.</option><option value="Jan">Jan</option><option value="Feb">Feb</option><option value="Mar">Mar</option><option value="Apr">Apr</option><option value="May">May</option><option value="Jun">Jun</option><option value="Jul">Jul</option><option value="Aug">Aug</option><option value="Sep">Sep</option><option value="Oct">Oct</option><option value="Nov">Nov</option><option value="Dec">Dec</option></select>  </div><div class="col-md-2" style="padding-left:0px;"><select class="form-control" name="tax_service_year[]"><option value="">Sel.</option><option value="2019">2019</option><option value="2020">2020</option><option value="2021">2021</option></select></div><div class="col-md-2 zzz_' + counters + '" style="display:none;"></div><div class="col-md-2 yearshide_' + counters + '" style="display:none;"><select class="form-control" name="taxyears[]"><option value="">Sel.</option><option value="2019">2019</option></select></div><div class="col-md-1 col-xs-12 text-right p_991 mt10_991" style="padding-left:0px;"><a  class="btn btn-danger remCFnew"><i class="fa fa-minus"></i></a></div></div></div><div class="clear"></div><hr style="margin-top: 10px;margin-bottom: 10px;"></div>');
                    $(".customyear").append('<div class="clear clearfix"></div><div id="rem_customyearid" class="col-md-12  rem_customyear"><div class="form-group"><hr style="margin-top: 0px;margin-bottom: 10px;"><div class="col-md-3 col-sm-12 p_991" style="padding-left:5px;"><select class="form-control" name=""><option value="">Select</option><option value="">2019</option></select></div><div class="col-md-9 col-sm-12 p_991" style="padding-left:5px;"><textarea class="form-control" rows="1"></textarea></div><hr style="margin-top: 10px;margin-bottom: 10px;"></div></div>');

                    //$('.tax_period_1 option:selected').hide();
                    //$('.tax_period_'+counters+' option:selected').remove();

                    var taxation = '.tax_service_' + counters;
                    var taxationperiod = '.tax_period_' + counters;
                    var tax_pariodbox = '.tax_periodbox_' + counters;

                    //alert(taxserviceone1);
                    $(taxation).on('change', function () {
                        var this2 = $(taxation).val();
                        var taxid = $('.tax_service_1').val();
                        // alert(val2);
                        $(".services4").append('<div class="col-md-3"><label class="control-label">Type of Service :</label><input type="text" class="form-control taxtitletax_' + counters + '" name="taxtypeofservice[]" readonly ><input class="titletax_' + counters + '" type="hidden" name="titletax[]"></div><div id="regular"><div class="col-md-4" style="width:36%;"><div class="row"><label class="control-label">Note :</label><textarea readonly cols="50" name="taxserviceincludes[]" id="taxserviceincludes" class="form-control taxserviceincludenote_' + counters + '" style="height:80px!important;"></textarea><input name="taxserviceid[]" type="hidden" id="taxserviceid" class="form-control"></div></div><div class="col-md-2" style="width:13%"><label class=" readonly control-label">Regular Price :</label><input name="taxregularprice[]" type="text" id="taxregularprice" style="text-align:right;font-size:small !important" placeholder="Regular Price"  readonly class="taxregularpricetax_' + counters + ' form-control"></div><div class="col-md-2" style="width:11%"><div class="row"><label class="control-label" style="width:130px;">Combo Price :</label><input name="taxcomboprice[]" type="text" id="taxcomboprice" style="text-align:right;font-size:small !important" placeholder="Combo Price" readonly class="taxcombopricetax_' + counters + ' form-control"></div></div><div class="col-md-2" style="width:11%"><label class="control-label">Your Price :</label><input readonly name="taxprice1[]" type="text" id="taxprice1" style="text-align:right;font-size:small !important" placeholder="Price" class="form-control taxpricetax_' + counters + '"></div></div><div class="clear"></div>');
                        //alert(this2);
                        if (this2 == '3' || this2 == '2') {
                            //  alert('2')

                            $('.yearshide_' + counters).hide();
                            $(taxationperiod).html('<option value="Monthly">Monthly</option>');
                            // $('.taxservicemonth_'+counters).css("visibility","visible");
                            $(taxationperiod).hide();


                            $(tax_pariodbox).show();
                            $(taxationperiod).attr("disabled", "disabled");
                            $(tax_pariodbox).prop("disabled", "");
                            $(tax_pariodbox).val('Monthly');

                        }
                        if (this2 == '1') {
                            $(taxationperiod).html('<option value="Monthly">Monthly</option><option value="Quarterly">Quarterly</option><option value="Annually">Annually</option>');
                            //$(taxationperiod).attr("disabled", "" );
                            //   $('.yearshide_'+counters).hide();
                            $(tax_pariodbox).attr("disabled", "disabled");
                            $('.taxservicemonth_' + counters).css("visibility", "visible");
                            $(taxationperiod).show();
                            $(tax_pariodbox).hide();
                            // $(tax_pariodbox).val('Monthly');
                        }
                        if (this2 == '4') {
                            // $(taxationperiod).html('<option value="Quarterly">Quarterly</option>');
                            //      $(taxationperiod).attr("disabled", "disabled");
                            $(tax_pariodbox).prop("disabled", "");
                            $('.taxservicemonth').css("visibility", "visible");
                            $(taxationperiod).hide();
                            $(tax_pariodbox).show();
                            $(tax_pariodbox).val('Quarterly');
                        }
                        if (this2 == '8') {
                            $('.zzz_' + counters).show();
                            $('.taxservicemonth_' + counters).css("visibility", "visible");
                            // $(taxationperiod).html('<option value="Quarterly">Quarterly</option>');
                            $('.taxservicemonth_' + counters).css("visibility", "visible");
                            $(taxationperiod).prop("disabled", "disabled");
                            $(tax_pariodbox).prop("disabled", "");

                            $(taxationperiod).hide();
                            $(tax_pariodbox).show();
                            $(tax_pariodbox).val('Quarterly');
                        }


                        if (this2 == '7') {
                            $('.yearshide_' + counters).show();
                            $('.zzz_' + counters).hide();
                            //  $('.taxservicemonth_'+counters).hide();
                            $('.taxservicemonth_' + counters).css("visibility", "hidden");
                            $(taxationperiod).prop("disabled", "disabled");
                            $(tax_pariodbox).prop("disabled", "");

                            $(taxationperiod).hide();
                            $(tax_pariodbox).show();
                            $(tax_pariodbox).val('Annually');
                        } else {
                            $('.taxservicemonth_' + counters).css("visibility", "visible");
                        }

                        if (this2 == '6' || this2 == '5') {
                            $(taxationperiod).prop("disabled", "disabled");
                            $(tax_pariodbox).prop("disabled", "");

                            // $('.taxservicemonth').hide();
                            $('.taxservicemonth_' + counters).css("visibility", "hidden");

                            $(taxationperiod).hide();
                            $(tax_pariodbox).show();
                            $(tax_pariodbox).val('Annually');
                        } else {
                            $('.taxservicemonth_' + counters).css("visibility", "visible");
                        }


                        var currency = $("#currency option:selected").val();
                        var typeofservice = '3';
                        var titles = this2;

                        var serviceperiod = $("#serviceperiod  option:selected").val();
                        var totalprice1 = $(".totalprice1").val();
                        var pricing = $("#pricetype  option:selected").val();
                        //	alert(pricing);
                        var ckq = $(".ckq").val();//alert(typeofservice);
                        /* 	$.get('<?php echo URL::to('salestaxations'); ?>?typeofservice=' + typeofservice+'&title='+titles+'&serviceperiod=' + serviceperiod+'&currency=' + currency, function(data)
   	{ 
   	   
 //  		$(regularprice).val('');
   //	$(comboprice1).val('');
   	//$(sumprice).val('');
   	//alert(data.title);
   		var counts=Object.keys(data).length;
   	//	alert(counts);
   if(counts > 0)
   {
       
   	if(pricing == 'Regular')
   	{
   	    
   	  //  alert(data.regularprice);
   	   // $('.services4_'+counters).show();
   	    $('.titletax_'+counters).val(data.title);
   	    $('.taxtitletax_'+counters).val(data.taxtitle);
   	    $('.taxserviceincludenote_'+counters).val(data.serviceincludes1);
   	    $('.taxregularpricetax_'+counters).val(data.regularprice1);
   	    $('.taxcombopricetax_'+counters).val(data.comboprice1);
   	    $('.taxpricetax_'+counters).val(data.regularprice1);
   	}
   	if(pricing=='Combo')
   	{
   	     //$('.services4_'+counters).show();
   	     $('.titletax_'+counters).val(data.title);
   	     $('.taxtitletax_'+counters).val(data.taxtitle);
   	    $('.taxserviceincludenote_'+counters).val(data.serviceincludes1);
   	    $('.taxregularpricetax_'+counters).val(data.regularprice1);
   	    $('.taxcombopricetax_'+counters).val(data.comboprice1);
   	    $('.taxpricetax_'+counters).val(data.comboprice1);
   	  
   	}
   }
   
   	});
   	*/
                        $.get('<?php echo URL::to('salestaxations'); ?>?typeofservice=' + typeofservice + '&title=' + titles + '&serviceperiod=' + serviceperiod + '&currency=' + currency, function (data) {

                            /*	$.ajax({
            async: false,
            type: "GET",
            url: '<?php echo URL::to('salestaxations'); ?>?typeofservice=' + typeofservice+'&title='+titles+'&serviceperiod=' + serviceperiod+'&currency=' + currency,
           // dataType: "json",
           
            success: function (data) {*/
                            var counts = Object.keys(data).length;
                            //	alert(counts);
                            if (counts > 0) {

                                if (pricing == 'Regular') {
                                    //  $('.services4').removeAttr("disabled");
                                    //  alert(data.regularprice);
                                    $('.services4').show();

                                    //  alert(data.regularprice);
                                    // $('.services4_'+counters).show();
                                    $('.titletax_' + counters).val(data.title);
                                    $('.taxtitletax_' + counters).val(data.taxtitle);
                                    $('.taxserviceincludenote_' + counters).val(data.serviceincludes1);
                                    $('.taxregularpricetax_' + counters).val(data.regularprice1);
                                    $('.taxcombopricetax_' + counters).val(data.comboprice1);
                                    $('.taxpricetax_' + counters).val(data.regularprice1);
                                }
                                if (pricing == 'Combo') {
                                    //$('.services4').removeAttr("disabled");
                                    //  alert(data.regularprice);
                                    $('.services4').show();

                                    //$('.services4_'+counters).show();
                                    $('.titletax_' + counters).val(data.title);
                                    //  alert($('.titletax_'+counters).val(data.title));
                                    $('.taxtitletax_' + counters).val(data.taxtitle);
                                    $('.taxserviceincludenote_' + counters).val(data.serviceincludes1);
                                    $('.taxregularpricetax_' + counters).val(data.regularprice1);
                                    $('.taxcombopricetax_' + counters).val(data.comboprice1);
                                    $('.taxpricetax_' + counters).val(data.comboprice1);

                                }
                            }

                            // }
                            //  });


                        });

                    });


                    $("#customFieldsbo").on('click', '.remCFnew', function () {
                        //  flag--;
                        //alert();
                        $(this).parent().parent().parent().parent().remove();
                        $('#rem_customyearid').remove();
                    });

                });


                $("#accountsoftware").on('click', '.addacrow', function () {
                    $("#accountsoftware").append('<div class="form-group"><div class="col-md-3"><div class="form-group"><select class="form-control" name="typeofwork[]"><option value="">Select</option><option value="Payroll">Payroll</option><option value="Accounting">Accounting</option><option value="Taxation">Taxation</option></select></div></div><div class="col-md-3"><div class="form-group"><select class="form-control" name="accounting_software[]"><option value="">Select</option><option value="Drake Accounting">Drake Accounting</option><option value="Drake Tax">Drake Tax</option><option value="Quickbook">Quickbook</option><option value="Sage">Sage</option></select></div></div><div class="col-md-3"><div class="form-group"><select class="form-control" name="accounting_location[]"><option value="">Select</option><option value="Client Location">Client</option><option value="FSC - Server">FSC Server</option><option value="On the Cloud">Cloud</option></select></div></div><div class="col-md-2"><a  class="btn btn-danger remCFnew"><i class="fa fa-minus"></i></a></div></div>');
                })

                $("#accountsoftware").on('click', '.remCFnew', function () {
                    //  flag--;
                    //alert();
                    $(this).parent().parent().remove();
                });
            });


        </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>