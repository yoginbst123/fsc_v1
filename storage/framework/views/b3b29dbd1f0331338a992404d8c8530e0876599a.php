<?php $__env->startSection('main-content'); ?>
<div class="content-wrapper">
   <div class="page-title">
      <h1>Edit Email </h1>
   </div>
   <section class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="box box-success">
               <div class="box-header">
                  <div class="box-tools pull-right">
                  </div>
               </div>
               <div class="col-md-12">
                  <form method="post" id="" action="<?php echo e(route('emailsetup.update',$leave->id)); ?>" class="form-horizontal" enctype="" novalidate="">
                   <?php echo e(csrf_field()); ?><?php echo e(method_field('PATCH')); ?>

                     <div class="form-group ">
                        <label class="control-label col-md-3"> To Whom : <span class="star-required">*</span></label>
                        <div class="col-md-6">
                           <div class="row">
                              <div class="col-md-12">
                                 <select name="type" id="type" class="form-control fsc-input">
                                    <option value="">---Select---</option>
                                    <option value="Approval" <?php if($leave->type=='Approval'): ?> selected <?php endif; ?>>Client</option>
                                    <option value="employee" <?php if($leave->type=='employee'): ?> selected <?php endif; ?>>Employee</option>
                                    <option value="user" <?php if($leave->type=='user'): ?> selected <?php endif; ?>>User</option>
                                 </select>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="form-group ">
                        <label class="control-label col-md-3 text-right">To : <span class="star-required">&nbsp;</span></label>
                        <div class="col-lg-6 col-md-9">
                           <div class="row">
                              <div class="col-md-12">
                                 <select name="employee" id="employee" class="form-control fsc-input">
                                    <?php $__currentLoopData = $emp; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $e): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                      <option value="<?php echo e($e->id); ?>" <?php if($e->id == $leave->userid): ?> selected <?php endif; ?>><?php echo e(ucwords($e->firstName.' '.$e->middleName.' '.$e->lastName)); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                 </select>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="form-group ">
                        <label class="control-label col-md-3 text-right">Subject : <span class="star-required">*</span></label>
                        <div class="col-lg-6 col-md-9">
                           <div class="row">
                              <div class="col-md-12">
                                 <input type="text" class="form-control fsc-input" placeholder="" name="subject" id="subject" value="<?php echo e($leave->subject); ?>">
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="form-group ">
                        <label class="control-label col-md-3 text-right">Message : <span class="star-required">&nbsp;</span></label>
                        <div class="col-lg-6 col-md-9">
                           <div class="row">
                              <div class="col-md-12">
                                 <textarea id="editor1" name="description" rows="10" cols="80"><?php echo e($leave->description); ?></textarea>
                                    <?php if($errors->has('description')): ?>
                                    <span class="help-block">
                                    <strong><?php echo e($errors->first('description')); ?></strong>
                                    </span>
                                    <?php endif; ?>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="card-footer">
                         <div class="row">
                         <div class="col-md-3"></div>
                        <div class="col-xs-2" style="width:155px;">
                           <input class="btn_new_save" type="submit" value="Save">
                        </div>
                        <div class="col-xs-2" style="width:155px;">
                           <a class="btn_new_cancel" href="<?php echo e(url('fac-Bhavesh-0554/emailsetup')); ?>">Cancel</a> 
                        </div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
<!--</div>-->
</section>
<!--</div>-->
<script>
   function myFunction() { 
       var y = '@financialservicecenter.net';
       var x = document.getElementById("email"); 
       t = x.value;
       z = t + y;
       $('#email').val(z);
   }
</script>
<script>
$(document).ready(function(){
	$(document).on('change','#type', function()
	{
	var id = $(this).val();
$('#employee').empty();
 	$.get('<?php echo URL::to('/clientid'); ?>?id='+id, function(data)
		{  
            $('#employee').empty();
            $('#employee').append('<option value="">---Select---</option>');
           $.each(data, function(index, subcatobj)
		   {
 if(id=='employee')
{
  $('#employee').append('<option value="' + subcatobj.id + '">' + subcatobj.firstName + ' ' + subcatobj.lastName + '</option>');  
}
else if(id=='user')
{
  $('#employee').append('<option value="' + subcatobj.id + '">' + subcatobj.firstName + ' ' + subcatobj.lastName + '</option>');  
}
if(id=='Active')
{
 $('#employee').append('<option value="' + subcatobj.id + '">' + subcatobj.first_name + ' ' + subcatobj.last_name + '</option>');
}
	})
		});
	});
});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>