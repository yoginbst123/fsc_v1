<?php $__env->startSection('main-content'); ?>
<div class="content-wrapper">
    <section class="page-title content-header">
     		<h1>Edit Account Code </h1>
    </section>
    <section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-success">
			      <div class="box-header">
              <div class="box-tools pull-right">
              </div>
            </div>
				<div class="col-md-12">
             <form method="post" action="<?php echo e(route('accountcode.update',$accountcode->id)); ?>" class="form-horizontal" enctype="multipart/form-data">
					<?php echo e(csrf_field()); ?> <?php echo e(method_field('PATCH')); ?>

						
						<div class="form-group <?php echo e($errors->has('accountcode') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Account Code : <span class="star-required">*</span></label>
							<div class="col-lg-5 col-md-8">
							    <select class="js-example-tags form-control" id="accountcode" name="accountcode">
          <option value="">Select</option>
                                               <?php $__currentLoopData = $accountcode1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cur): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                               <option value="<?php echo e($cur->accountcode); ?>" <?php if($cur->accountcode==$accountcode->accountcode): ?> selected <?php endif; ?>><?php echo e($cur->accountcode); ?></option>
                                               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</select>
							                                                          
								<?php if($errors->has('accountcode')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('accountcode')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>
						<div class="form-group <?php echo e($errors->has('account_name') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Account Name : <span class="star-required">*</span></label>
							<div class="col-lg-5 col-md-8">
								<input name="account_name" type="text" id="account_name" class="form-control p-l-10" value="<?php echo e($accountcode->account_name); ?>" />                                                            
								<?php if($errors->has('account_name')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('account_name')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>
						<div class="form-group <?php echo e($errors->has('account_belongs_to') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Account Belongs To: <span class="star-required">*</span></label>
							<div class="col-lg-5 col-md-8">
								<select class="form-control" name="account_belongs_to" id="account_belongs_to">
								      <option value="">Select</option>
                                    <option value="Cost of Sales" <?php if($accountcode->account_belongs_to=='Cost of Sales'): ?> selected <?php endif; ?>>Cost of Sales</option>
                                    <option value="Expenses" <?php if($accountcode->account_belongs_to=='Expenses'): ?> selected <?php endif; ?>>Expenses</option>
                                    <option value="Cash and Bank Balance" <?php if($accountcode->account_belongs_to=='Cash and Bank Balance'): ?> selected <?php endif; ?>>Cash and Bank Balance</option>
                                    <option value="Accounts Payable" <?php if($accountcode->account_belongs_to=='Accounts Payable'): ?> selected <?php endif; ?>>Accounts Payable</option>
                                    <option value="Account Receivable" <?php if($accountcode->account_belongs_to=='Account Receivable'): ?> selected <?php endif; ?>>Account Receivable</option>
                                    <option value="Other Assets" <?php if($accountcode->account_belongs_to=='Other Assets'): ?> selected <?php endif; ?>>Other Assets</option>
                                
                                <option value="Income" <?php if($accountcode->account_belongs_to=='Income'): ?> selected <?php endif; ?>>Income</option>
                                    <option value="Inventory" <?php if($accountcode->account_belongs_to=='Inventory'): ?> selected <?php endif; ?>>Inventory</option>
                                    <option value="Long Term Liabilities" <?php if($accountcode->account_belongs_to=='Long Term Liabilities'): ?> selected <?php endif; ?>>Long Term Liabilities</option>
                                    <option value="Other Current Assets" <?php if($accountcode->account_belongs_to=='Other Current Assets'): ?> selected <?php endif; ?>>Other Current Assets</option>
                                    <option value="Other Current Liabilities" <?php if($accountcode->account_belongs_to=='Other Current Liabilities'): ?> selected <?php endif; ?>>Other Current Liabilities</option>
<option value="Fixed Assets">Fixed Assets</option>
                                </select>
								<?php if($errors->has('account_belongs_to')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('account_belongs_to')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>
						<div class="card-footer">
						    <div class="form-group">
							<label class="control-label col-md-3"></label>
							<div class="col-xs-2" style="width:155px;">
<input class="btn_new_save" type="submit" name="submit" value="Save">
							</div>
							<div class="col-xs-2" style="width:155px;">
<a class="btn_new_cancel" href="<?php echo e(url('fac-Bhavesh-0554/accountcode')); ?>">Cancel</a> 
							</div>
						</div>
						  </div>
					</form>
				</div>
			</div>
		</div>
	</div>
	</section>
<!--</div>-->
<style>
.select2-container--default .select2-selection--single .select2-selection__rendered{
    font-size:16px !important;
    padding:0;
     color:#000;
}
.p-l-10{ padding-left:10px;}
.select2-container--default .select2-selection--single .select2-selection__arrow b{ border-color:#000 transparent transparent transparent; }
.select2-container--default .select2-selection--single .select2-selection__arrow{ top:6px; right:4px; }
.select2-container {
    box-sizing: border-box;
    display: inline-block;
    margin: 0;
    position: relative;
    vertical-align: middle;
    width: 100% !important;
}.select2-container--default .select2-selection--single {
    background-color: #fff;
    /* border: 1px solid #aaa; */
    border-radius: 4px;
    border: 2px solid #2fa6f2;
    height:40px;
    padding:8px;
}</style>
<script>
    $('.js-example-tags').select2({
    tags: true,
    tokenSeparators: [",", " "]
});

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>