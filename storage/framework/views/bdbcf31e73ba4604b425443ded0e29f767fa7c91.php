<?php $__env->startSection('main-content'); ?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>Position</h1>
    </section>
    <!-- Main content -->
    <section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-success">
			      <div class="box-header">
          
              <div class="box-tools pull-right">
                
              </div>
            </div>
				<div class="col-md-12">
             <form method="post" action="<?php echo e(route('position.store')); ?>" class="form-horizontal" id="positionname" name="positionname" enctype="multipart/form-data">
					<?php echo e(csrf_field()); ?>

						<div class="form-group <?php echo e($errors->has('position') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Position Name :</label>
							<div class="col-lg-5 col-md-8">
								<input name="position" type="text" id="position" class="form-control" value="" />                                                            <?php if($errors->has('position')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('position')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>					
						<div class="card-footer">
							<div class="row">
							    <div class="col-md-3"></div>
								<div class="col-xs-2" style="width:155px;">
									<input class="btn_new_save btn-primary1" type="submit" name="submit" value="Save">
									</div>
									<div class="col-xs-2" style="width:155px;">
									<a class="btn_new_cancel" href="<?php echo e(url('fac-Bhavesh-0554/position')); ?>">Cancel</a> 
									</div>
							</div>
						</div>
						
					</form>
				</div>
			</div>
		</div>
	</div>
	</section>
<!--</div>-->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>