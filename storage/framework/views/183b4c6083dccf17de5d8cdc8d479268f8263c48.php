<?php $__env->startSection('main-content'); ?>
<div class="content-wrapper">
<section class="content-header page-title">
    <?php 
   // print_r($employment);
    if($employment->countryId == 'IND')
    {
        $cnts='IND';
    }
    else if($employment->countryId == 'USA')
    {
        $cnts='USA';
    }?>
    <h1><span class="pull-left"><?php echo e($employment->firstName); ?> <?php echo e($employment->middleName); ?> <?php echo e($employment->lastName); ?></span> Application Receive</h1>
    </section>
      <section class="content">
   <div class="row">
      <div class="col-md-12">
         <div class="box box-success">
             <div class="box-header">
              
            </div>
            <div class="">
                <?php //echo "<pre>";print_r($employment);?>
               <form method="post" action="<?php echo e(route('employeapplication.update',$employment->cid)); ?>" class="form-horizontal" id="" name="content" enctype="multipart/form-data">
                  <?php echo e(csrf_field()); ?><?php echo e(method_field('PATCH')); ?>        
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
				    <div class="form-group">
						<label class="control-label col-md-3">Name : <span class="star-required">*</span></label>
					<div class="col-lg-7 col-md-9">
					    <div class="row">
				     	<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin" style="padding-right:0px !important;">
							<select class="form-control fsc-input" id="nametype" name="nametype">
							    <option value="mr" <?php if($employment->nametype=='mr'): ?> selected <?php endif; ?>>Mr.</option>
							    <option value="mrs" <?php if($employment->nametype=='mrs'): ?> selected <?php endif; ?>>Mrs.</option>
							    <option value="miss" <?php if($employment->nametype=='miss'): ?> selected <?php endif; ?>>Miss.</option>
							</select>
						</div>
					<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin" >	
					
<input type="hidden" class="form-control fsc-input" name='employment_id' id="employment_id" placeholder="First Name" value="<?php echo e($employment->employment_id); ?>">
<input type="text" class="form-control fsc-input textonly" name='firstName' id="firstName" placeholder="First Name" value="<?php echo e($employment->firstName); ?>">
					</div>
				
					<div class="col-lg-1 col-md-12 col-sm-12 col-xs-12 fsc-element-margin"> <div class="row">
						<input type="text" class="form-control textonly  fsc-input" maxlength="" name='middleName' id="middleName" placeholder="M" value="<?php echo e($employment->middleName); ?>">
					</div></div>
					<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
					   
						<input type="text" class="form-control textonly  fsc-input" name="lastName" value="<?php echo e($employment->lastName); ?>" id="lastName" placeholder="Last Name">
					
					</div>
				</div>	
				</div>
					    </div>
					</div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
				    <div class="form-group">
					    <label class="col-md-3 control-label">Address 1 : <span class="star-required">*</span></label>
					    <div class="col-lg-7 col-md-9">
    					    <div class="row">
            					<div class="col-lg-11 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
            						<input type="text" class="form-control fsc-input" id="address1" value="<?php echo e($employment->address1); ?>" name='address1' placeholder="Address">
            					</div>
        					</div>
    					</div>
				    </div>				
				</div>	
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
				    <div class="form-group">
					    <label class="col-md-3 control-label">Address 2 : <span class="star-required">*</span></label>
					    <div class="col-lg-7 col-md-9">
    					    <div class="row">
            					<div class="col-lg-11 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
            						<input type="text" class="form-control fsc-input" id="address2" name='address2' placeholder="Address" value="<?php echo e($employment->address2); ?>">
            					</div>
        					</div>
    					</div>
				    </div>				
				</div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Country : <span class="star-required">*</span></label>
                        <div class="col-lg-7 col-md-9">
                            <div class="row">
                                <div class="col-lg-4 col-md-5 col-sm-12 col-xs-12 fsc-element-margin">
                                    <select name="countryId" id="countries_states1" class="form-control fsc-input bfh-countries" data-country="<?php echo e($cnts); ?>">
                                        
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>				
                </div>
				
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                    <div class="form-group">
                        <label class="col-lg-3 control-label">City / State / Zip : <span class="star-required">*</span></label>
                        <div class="col-lg-7 col-md-9">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 fsc-element-margin">
                                    <input type="text" class="form-control fsc-input" id="city" value="<?php echo e($employment->city); ?>" name='city' placeholder="City">
                                </div>
                                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6 fsc-form-col fsc-element-margin">
                                    <select name="stateId" id="stateId" class="form-control fsc-input bfh-states" data-country="countries_states1" data-state="<?php echo e($employment->stateId); ?>">
                                    
                                    </select>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 fsc-element-margin">
                                    <input type="text" class="form-control fsc-input" id="zip" value="<?php echo e($employment->zip); ?>" name='zip' placeholder="Zip" maxlength='6'>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
				
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                    <div class="form-group">
                        <label class="col-lg-3 control-label">Telephone 1 : <span class="star-required">*</span></label>
                        <div class="col-lg-7 col-md-9">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 fsc-element-margin">
                                    <input type="tel" class="form-control fsc-input bfh-phone" data-country="countries_states1" id="telephoneNo1" value='<?php echo e($employment->telephoneNo1); ?>' name='telephoneNo1' placeholder="(999) 999-9999"  data-inputmask="'alias': 'phone'">
                                </div>
                                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6 fsc-form-col fsc-element-margin">
                                    <select name="telephoneNo1Type" id="telephoneNo1Type" class="form-control fsc-input">  
                                        <option value='Mobile' <?php if($employment->telephoneNo1Type=='Mobile'): ?> selected <?php endif; ?>>Mobile</option>
                                        <option value='Home' <?php if($employment->telephoneNo1Type=='Home'): ?> selected <?php endif; ?>>Home</option>
                                        <option value='Work' <?php if($employment->telephoneNo1Type=='Work'): ?> selected <?php endif; ?>>Work</option>
                                        <option value='Other' <?php if($employment->telephoneNo1Type=='Other'): ?> selected <?php endif; ?>>Other</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
				
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                    <div class="form-group">
                        <label class="col-lg-3 control-label">Telephone 2 : <span class="star-required">&nbsp;&nbsp;</span></label>
                        <div class="col-lg-7 col-md-9">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 fsc-element-margin">
                                    <input type="tel" class="form-control fsc-input bfh-phone" data-country="countries_states1" value='<?php echo e($employment->telephoneNo2); ?>' name='telephoneNo2' id="telephoneNo2" placeholder="(999) 999-9999"  data-inputmask="'alias': 'phone'">
                                </div>
                                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6 fsc-form-col fsc-element-margin">
                                    <select name="telephoneNo2Type" id="telephoneNo2Type" class="form-control fsc-input">                                 
                                        <option value='Mobile' <?php if($employment->telephoneNo2Type=='Mobile'): ?> selected <?php endif; ?>>Mobile</option>
                                        <option value='Home' <?php if($employment->telephoneNo2Type=='Home'): ?> selected <?php endif; ?>>Home</option>
                                        <option value='Work' <?php if($employment->telephoneNo2Type=='Work'): ?> selected <?php endif; ?>>Work</option>
                                        <option value='Other' <?php if($employment->telephoneNo2Type=='Other'): ?> selected <?php endif; ?>>Other</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
				
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Email : <span class="star-required">*</span></label>
                        <div class="col-lg-7 col-md-9">
                            <div class="row">
                                <div class="col-lg-7 col-md-5 col-sm-12 col-xs-12 fsc-element-margin">
                                    <input type="text" class="form-control fsc-input" value='<?php echo e($employment->email); ?>' readonly id="email" name='email' placeholder="Email Address">
                                </div>
                            </div>
                        </div>
                    </div>				
                </div>
				
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Position : <span class="star-required">*</span></label>
                        <div class="col-lg-7 col-md-9">
                            <div class="row">
                                <div class="col-lg-7 col-md-5 col-sm-12 col-xs-12 fsc-element-margin">
                                    <div class="dropdown">
                                        <select id='requiremnetId' name='requiremnetId' class='form-control  fsc-input' style='height:auto'>                                                 
                                            <?php $__currentLoopData = $employment1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $emp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value='<?php echo e($emp->id); ?>' <?php if($employment->requiremnetId == $emp->id): ?> selected <?php else: ?> <?php endif; ?>><?php echo e($emp->position_name); ?></option>                                              
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>    
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>				
                </div>
				
				
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
				     <div class="form-group">
						<label class="col-lg-3 control-label">Resume :<span class="star-required">&nbsp;&nbsp;</span> </label>
					
					<div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">

<!--<label class="file-upload btn btn-primary">
                Browse for file ... <input type="file" class="form-control fsc-input" id="resume" name='resume' placeholder="Select Document">
            </label>
            			
&nbsp;&nbsp;

<?php if(empty($employment->resume)): ?>
<img src="<?php echo e(asset('public/images/file-not-found.png')); ?>" width="60" alt="">
<?php else: ?>
<a href="<?php echo e(asset('public/resumes/')); ?>/<?php echo e($employment->resume); ?>" target="_blank" class="btn btn-success">

<?php if(pathinfo($employment->resume, PATHINFO_EXTENSION) == 'doc'): ?>
<i class="fa fa-file" aria-hidden="true"></i>
<?php endif; ?>
<?php if(pathinfo($employment->resume, PATHINFO_EXTENSION) == 'pdf'): ?>
<i class="fa fa-file-pdf-o"></i>
<?php endif; ?>
<?php if(pathinfo($employment->resume, PATHINFO_EXTENSION) == 'docx'): ?>
<i class="fa fa-file-pdf-o"></i>
<?php endif; ?> Download</a>
<?php endif; ?>

<input type="hidden" class="form-control fsc-input" id="resume1" name='resume1' value="<?php echo e($employment->resume); ?>" placeholder="Select Document">
&nbsp;&nbsp;
!-->
	<a href="<?php echo e(route('send.sendmail',[$employment->firstName,$employment->email])); ?>" id="<?php echo e($employment->cid); ?>" class="btn btn-success">Send Question</a> 
	<a data-toggle="modal" data-target="#myModalResume" class="btn btn-primary">Show Resume</a>
	
				</div></div>
				</div>
				
				
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                    <div class="form-group">
                        <label class="col-lg-3 control-label">Telephone 2 : <span class="star-required">&nbsp;&nbsp;</span></label>
                        <div class="col-lg-7 col-md-9">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 fsc-element-margin">
                                    <input type="text" class="form-control"  value="<?php echo date('M-d Y',strtotime($employment->created_at));?>"  readonly placeholder="Date">&nbsp;&nbsp;
                                </div>
                                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6 fsc-form-col fsc-element-margin">
                                    <input type="text" class="form-control" value="<?php echo date('D',strtotime($employment->created_at));?>" readonly  placeholder="Day">&nbsp;&nbsp;
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 fsc-element-margin">
                                    <input type="text" class="form-control"   value="<?php echo date('g:i A',strtotime($employment->created_at));?>" readonly placeholder="Time">
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
				
				
				<div class="card-footer" >
				    <div style="margin-bottom:20px" >
    					<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"></div>
    					<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
    					 <button type="submit" class="btn btn-primary fsc-form-submit">Save</button>&nbsp&nbsp <a href="https://financialservicecenter.net/fac-Bhavesh-0554/employeapplication" class="btn btn-primary fsc-form-submit">Cancel</a>
    					</div>
					</div>
<br>
				<br>
<div class="col-md-12"><center><p>Accept will convert into prospect <a href="<?php echo e(route('co.update1',$employment->cid)); ?>" id="<?php echo e($employment->cid); ?>" class="btn btn-success userStatus">Accept</a> &nbsp; <a class="btn btn-danger userStatus" href="<?php echo e(route('co.deleteto',$employment->cid)); ?>" id="<?php echo e($employment->cid); ?>" >Reject</a> Reject will be deleted</p></center></div>
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
<div class="" id="Register"></div>
				</div>
				
			</form>
          
         </div>
      </div>  </div>
      </div> 
      </section>
      </div>
      
      
      <!-- Modal -->
<div id="myModalResume" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
       <iframe src="<?php echo e(asset('public/resumes/')); ?>/<?php echo e($employment->resume); ?>" width="100%" height="500"></iframe>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
      
<script>
//$("#telephoneNo1").mask("(999) 999-9999");
$(".ext").mask("999");
//$("#telephoneNo2").mask("(999) 999-9999");
//$("#mobile_no").mask("(999) 999-9999");
//$(".usapfax").mask("(999) 999-9999");
$("#zip").mask("9999");
	$(document).ready(function(){
  /***phone number format***/
  $(".phone").keypress(function (e) {
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
      return false;
    }
    var curchr = this.value.length;
    var curval = $(this).val();
    if (curchr == 3 && curval.indexOf("(") <= -1) {
      $(this).val("(" + curval + ")" + " ");
    } else if (curchr == 4 && curval.indexOf("(") > -1) {
      $(this).val(curval + ")-");
    } else if (curchr == 5 && curval.indexOf(")") > -1) {
      $(this).val(curval + "-");
    } else if (curchr == 9) {
      $(this).val(curval + "-");
      $(this).attr('maxlength', '14');
    }
  });
});
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>