<?php $__env->startSection('main-content'); ?>
<div class="content-wrapper">
    <section class="page-title content-header">
 		<h2>Appointment Rule Setup</h2>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-success" style="padding-bottom:15px;">
                    <div class="box-header">
                        <div class="box-tools pull-right"></div>
                    </div>
                    <div class="card col-md-12" style="padding:0px;">
                        <div class="col-lg-6">
                            <div class="box" style="background-color:#038ee0;border:1px solid;">
                                <div class="box-header with-border">
                                  <h3 class="box-title" style="color:#ffffff;line-height:2;">Regarding Rules</h3>
                                  <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse" style="color:#ffffff;"><i class="fa fa-minus"></i></button>               
                                  </div>
                                </div>
                               <div class="box-body" style="background-color: #ffffe6;border-left:1px solid;border-top:1px solid;">
                                    <form action=<?php echo e(URL('fac-Bhavesh-0554/setup/storeRegard')); ?> class="form-horizontal changepassword bv-form" id="" method="post" novalidate="novalidate">
            						    <?php echo e(csrf_field()); ?>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Regarding :</label>
                                            <div class="col-md-9">
                                                <input name="regarding" type="text" placeholder="Regarding" id="" class="form-control">
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Short Name :</label>
                                            <div class="col-md-9">
                                                <input name="short_name" type="text" placeholder="Short Name" id="" class="form-control">
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 hide_991"></label>
                                            <div class="col-md-5">
                                                <div class="col-xs-3" style="padding-left:1px;width:120px;">
                    							    <button type="submit" class="btn_new_save">Submit</button>
                    							</div>
                                            </div> 
                                        </div>
                                    </form>
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th width="71%">Regarding</th>
                                                    <th width="20%">Short Name</th>
                                                    <th width="26%" rowspan="2">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php if(isset($regards)): ?>
                                                <?php $__currentLoopData = $regards; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <tr>
                                                    <td><?php echo e($val->regarding); ?></td>
                                                    <td><?php echo e($val->short_name); ?></td>
                                                    <td class="text-center">
                                                        <a class="btn-action btn-view-edit"  data-toggle="modal" data-target="#edit_regard<?php echo e($key+1); ?>" href="#"><i class="fa fa-edit"></i></a>
                                                        <a class="btn-action btn-delete" href="<?php echo e(URL('fac-Bhavesh-0554/setup/delRegards')); ?>/<?php echo e($val->id); ?>"><i class="fa fa-trash"></i></a>
                                                    </td>
                                                </tr>
                                                
                                                <div id="edit_regard<?php echo e($key+1); ?>" class="modal fade" role="dialog">
                                                        <div class="modal-dialog" role="document">
                                                          <div class="modal-content">
                                                             <div class="modal-header">
                                                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">×</span>
                                                                </button>
                                                                <h4 class="modal-title" id="exampleModalLabel">Edit Types</h4>
                                                             </div>
                                                             <form action="<?php echo e(URL('fac-Bhavesh-0554/setup/updateRegard')); ?>" method="post" id="" class="form-horizontal">
                                                                 <?php echo e(csrf_field()); ?>

                                                                <input type="hidden" value="<?php echo e($val->id); ?>" name="regard_id"/>
                                                                <div class="modal-body">
                                                                    <div class="form-group col-md-12">
                                                                        <label class="control-label col-md-3">Regarding :</label>
                                                                        <div class="col-md-9">
                                                                            <input name="regarding" type="text" value="<?php echo e($val->regarding); ?>" placeholder="Regarding" id="" class="form-control">
                                                                        </div> 
                                                                    </div>
                                                                    <div class="form-group col-md-12">
                                                                        <label class="control-label col-md-3">Short Name :</label>
                                                                        <div class="col-md-9">
                                                                            <input name="short_name" type="text" value="<?php echo e($val->short_name); ?>" placeholder="Short Name" id="" class="form-control">
                                                                        </div> 
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                   <button type="submit" id="" class="btn btn-primary">Save</button>
                                                                   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                </div>
                                                             </form>
                                                          </div>
                                                       </div>
                                                    </div>
                                                
                                                
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php endif; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>                      
                              </div>
                            
    						
                        </div>
                        <div class="col-lg-6 col-xs-12">
                         <div class="box" style="background-color:#038ee0;border:1px solid;">
                            <div class="box-header with-border">
                              <h3 class="box-title" style="color:#ffffff;line-height:2;">Work Type Rules</h3>
                              <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse" style="color:#ffffff;"><i class="fa fa-minus"></i></button>               
                              </div>
                            </div>
                           <div class="box-body" style="background-color: #ffffe6;border-left:1px solid;border-top:1px solid;">
                                <form action=<?php echo e(URL('fac-Bhavesh-0554/setup/storeWorkType')); ?> class="form-horizontal changepassword bv-form" id="" method="post" novalidate="novalidate">
                                    <?php echo e(csrf_field()); ?>

                                    <div class="form-group ">
                                        <label class="control-label col-md-3">Work Type :</label>
                                        <div class="col-md-9">
                                            <input type="text" name="worktype" placeholder="work type" class="form-control">
                                        </div> 
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Short Name :</label>
                                        <div class="col-md-4">
                                            <input type="text" name="short_names" placeholder="Short Name" class="form-control">
                                        </div> 
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-xs-12 left_991">Color :</label>
                                        <div class="col-md-2 col-xs-3">
                                            <input type="color"  id="myColor" name="" class="form-control" onchange="myFunction()" style="padding:0px;border:1px solid;">
                                            <input type="hidden" id="colorText" name="color" value=""/>
                                        </div>
                                        <div class="col-md-3">
                                            <h4 id="demo1"></h4>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="control-label col-md-3 hide_991"></label>
                                        <div class="col-md-6">
                                            <div class="col-xs-3" style="padding-left:1px;width:120px;">
                							    <button type="submit" class="btn_new_save">Submit</button>
                							</div>
                                        </div> 
                                    </div>
                                </form>
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th width="51%">Work Type</th>
                                                <th width="21%">Short Name</th>
                                                <th width="19%">Color</th>
                                                <th width="19%" rowspan="2">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(isset($work_type)): ?>
                                                <?php $__currentLoopData = $work_type; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <tr>
                                                        <td><?php echo e($val->worktype); ?></td>
                                                        <td><?php echo e($val->short_name); ?></td>
                                                        <td><label style="background-color:<?php echo e($val->color); ?>; width:50px; height:20px;"></label></td>
                                                        <td class="text-center">
                                                            <a class="btn-action btn-view-edit"  data-toggle="modal" data-target="#add_type<?php echo e($key+1); ?>" href="#"><i class="fa fa-edit"></i></a>
                                                            
                                                            <a class="btn-action btn-delete" href="<?php echo e(URL('fac-Bhavesh-0554/setup/delWorkType')); ?>/<?php echo e($val->id); ?>"><i class="fa fa-trash"></i></a>
                                                        </td>
                                                    </tr>
                                                    
                                                    <div id="add_type<?php echo e($key+1); ?>" class="modal fade" role="dialog">
                                                        <div class="modal-dialog" role="document">
                                                          <div class="modal-content">
                                                             <div class="modal-header">
                                                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">×</span>
                                                                </button>
                                                                <h4 class="modal-title" id="exampleModalLabel">Edit Types</h4>
                                                             </div>
                                                             <form action="<?php echo e(URL('fac-Bhavesh-0554/setup/updateWorkType')); ?>" method="post" id="" class="form-horizontal">
                                                                 <?php echo e(csrf_field()); ?>

                                                                <input type="hidden" value="<?php echo e($val->id); ?>" name="work_id"/>
                                                                <div class="modal-body">
                                                                    <div class="form-group col-md-12">
                                                                        <label class="control-label col-md-4">Type of Work:</label>
                                                                        <div class="col-md-8">
                                                                            <input type="text" id="" name="worktype" class="form-control" placeholder="" value="<?php echo e($val->worktype); ?>">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group col-md-12">
                                                                        <label class="control-label col-md-4">Short Name:</label>
                                                                        <div class="col-md-4">
                                                                            <input type="text" id="" name="short_names" class="form-control" placeholder="" value="<?php echo e($val->short_name); ?>">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group col-md-12">
                                                                        <label class="control-label col-md-4">Color :</label>
                                                                        <div class="col-md-2">
                                                                            <input type="color"  id="<?php echo e($key+1); ?>" name="" value="<?php echo e($val->color); ?>" class="form-control" onchange="myForUpdateFunction(this)" style="padding:0px;border:1px solid;">
                                                                            <input type="hidden" id="updatecolor_<?php echo e($key+1); ?>" name="color" value="<?php echo e($val->color); ?>"/>
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <h4 id="demo2<?php echo e($key+1); ?>"><?php echo e($val->color); ?></h4>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                  
                                                                    
                                                                </div>
                                                                <div class="modal-footer">
                                                                   <button type="submit" id="" class="btn btn-primary">Save</button>
                                                                   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                </div>
                                                             </form>
                                                          </div>
                                                       </div>
                                                    </div>
                                                    
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endif; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>                      
                          </div> 
                        </div>
                        
                        
                    </div>
                </div>
            </div>
        </div>
        <div id="rem_type" class="modal fade" role="dialog">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                 <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="exampleModalLabel">Remove Types</h4>
                 </div>
                 <form action="" method="post" id="" class="form-horizontal">
                    <div class="modal-body">
                       <div class="table-responsive">
                           <table class="table table-bordered">
                               <thead>
                                   <tr>
                                       <th width="5%">No</th>
                                       <th>Type</th>
                                       <th width="8%">Action</th>
                                   </tr>
                               </thead>
                               <tbody>
                                   <tr>
                                       <td class="text-center">1</td>
                                       <td></td>
                                       <td class="text-center"><a class="btn-action btn-delete" href="#"><i class="fa fa-trash"></i></a></td>
                                   </tr>
                               </tbody>
                           </table>
                       </div>
                    </div>
                    <div class="modal-footer">
                       <button type="button" id="addrelatedname" class="btn btn-primary">Save</button>
                       <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                 </form>
              </div>
           </div>
        </div>
<script>
function myFunction() {
  var x = document.getElementById("myColor");
  var currentVal = x.value;
    document.getElementById("demo1").innerHTML = currentVal;
    document.getElementById("colorText").value = currentVal;
}

function myForUpdateFunction(e) {
    var x = document.getElementById(e.id);
    var currentVal = x.value;
    document.getElementById("demo2"+e.id).innerHTML = currentVal;
    document.getElementById("updatecolor_"+e.id).value = currentVal;
}
</script>
	</section>
	 
<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>