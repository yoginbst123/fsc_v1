<?php $__env->startSection('main-content'); ?>
<div class="content-wrapper">
<!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>Submission</h1>
    </section>
    <!-- Main content -->
    <section class="content">
	

	<div class="row">
		<div class="col-md-12">
			<div class="box box-success">
			      <div class="box-header">
              
              <div class="box-tools pull-right">
                
              </div>
            </div>
				<div class="col-md-12">
             <form method="post" action="<?php echo e(route('submissionreq.store')); ?>" class="form-horizontal" id="submission" name="submission" enctype="multipart/form-data">
					<?php echo e(csrf_field()); ?>

						<div class="form-group <?php echo e($errors->has('submission_name') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-4">Submission Name :</label>
							<div class="col-lg-6 col-md-8">
								<input name="submission_name" type="text" id="submission_name" class="form-control" value="" />                                                            <?php if($errors->has('submission_name')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('submission_name')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>
						
						<div class="form-group <?php echo e($errors->has('submission_image') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-4">Submission Image :</label>
							<div class="col-lg-6 col-md-8">
<label class="file-upload btn btn-primary">
								<input type="file" name="submission_image" id="submission_image" class="form-control"  />                  

                Browse for file ... 
            </label>                              <?php if($errors->has('submission_image')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('submission_image')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>
						<div class="form-group <?php echo e($errors->has('singleimage') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-4">Submission Page Image :</label>
							<div class="col-lg-6 col-md-8">
<label class="file-upload btn btn-primary">
								<input type="file" name="singleimage" id="singleimage" class="form-control"  />                  

                Browse for file ... 
            </label>                              <?php if($errors->has('singleimage')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('singleimage')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>
						
						<div class="form-group <?php echo e($errors->has('siteimage') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-4">Submission Page Image 1:</label>
							<div class="col-lg-6 col-md-8">
<label class="file-upload btn btn-primary">
								<input type="file" name="siteimage" id="siteimage" class="form-control"  />                  

                Browse for file ... 
            </label>                              <?php if($errors->has('siteimage')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('siteimage')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>
						<div class="form-group <?php echo e($errors->has('link') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-4">Url :</label>
							<div class="col-lg-6 col-md-8">
								<input name="link" type="text" id="link" class="form-control" value="" />
								<?php if($errors->has('link')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('link')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>	
						
							<div class="form-group <?php echo e($errors->has('description') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-4">Description :</label>
							<div class="col-lg-6 col-md-8">
								<input name="description" type="text" id="description" class="form-control" value="" />
								<?php if($errors->has('description')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('description')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>	
						<div class="card-footer">
						    <div class="form-group">
							<label class="control-label col-md-4"></label>
							<div class="col-xs-2" style="width:155px;">
<input class="btn_new_save btn-primary1" type="submit" name="submit" value="Save">
							</div>
							<div class="col-xs-2" style="width:155px;">
<a class="btn_new_cancel" href="<?php echo e(url('fac-Bhavesh-0554/submission')); ?>">Cancel</a> 
							</div>
						</div>
						  </div>
					
						
					</form>
				</div>
			</div>
		</div>
	</div>
	 </section>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>