<!--<style>
    .search-btn {
    position: absolute;
    top: 46px;
    right: 16px;
    background:transparent;
    border:transparent;
}

</style>
<div class="content-wrapper">
	<div class="page-title">
		<h1>Work Status</h1>
	</div>
	<div class="row"> 

<div class="col-md-12">
			<div class="box box-success">
			      <div class="box-header">
                   <div class="col-md-9" style="margin-left:-1.6%">
              <div class="row">
              <div class="col-md-2">
                  <div class="col-md-"><label style="margin-left:2%;margin-top: 11px;">Filter: </label></div>
                <select name="choice" style="width: 92%;margin-left: 4px;" id="choice" class="form-control">
        <option value="1">All</option>
        <option value="2" selected="">Active</option>
        <option value="3">Inactive</option>
        <option value="4">Client Id</option>
        <option value="5">Client Name</option>
       </select>
    </div>
       <div class="col-md-4">
         <div class="col-md-1"><label style="margin-top: 11px;">Search: </label></div>
    <select name="types" style="width: 92%;margin-left: 4px;" id="types" class="form-control">
        <option value="All" selected="">All</option>
        <option value="Type">Client ID</option>
        <option value="EE / User ID">Company Name</option>
        <option value="Employee Name">Bussiness Name</option>
        <option value="Email ID">Bussiness Telephone</option>
        <option value="Tel. Number">Contact Name</option>
        <option value="Contact Telephone">Contact Telephone</option>
    </select>
    </div>
     <div class="col-md-3">
            <div class="col-md-1"><label style="margin-top: 11px;"> &nbsp;</label></div>
     <table style="width: 100%; margin: 0 auto 2em auto;" cellspacing="0" cellpadding="3" border="0">
        <tbody>
            <tr id="filter_global">
                <td align="center"><input type="text" class="global_filter form-control" id="global_filter" placeholder="All Search"><button class="search-btn"><i class="fa fa-search"></i></button></td>
            </tr>
            <tr id="filter_col2" data-column="1" style="display:none">
                <td align="center"><input type="text" class="column_filter form-control" id="col1_filter" placeholder="Client ID"><button class="search-btn"><i class="fa fa-search"></i></button></td>
            </tr>
            <tr id="filter_col3" data-column="2" style="display:none">
                <td align="center"><input type="text" class="column_filter form-control" id="col2_filter" placeholder="Company Name"><button class="search-btn"><i class="fa fa-search"></i></button></td>
            </tr>
            <tr id="filter_col4" data-column="3" style="display:none">
                <td align="center"><input type="text" class="column_filter form-control" id="col3_filter" placeholder="Bussiness Name"><button class="search-btn"><i class="fa fa-search"></i></button></td>
            </tr>
            <tr id="filter_col5" data-column="4" style="display:none">
                <td align="center"><input type="text" class="column_filter form-control" id="col4_filter" placeholder="Bussiness Telephone"><button class="search-btn"><i class="fa fa-search"></i></button></td>
            </tr>
            <tr id="filter_col6" data-column="5" style="display:none">
                <td align="center"><input type="text" class="column_filter form-control" id="col5_filter" placeholder="Contact Name"><button class="search-btn"><i class="fa fa-search"></i></button></td>
            </tr>
            <tr id="filter_col7" data-column="6" style="display:none">
                <td align="center"><input type="text" class="column_filter form-control" id="col6_filter" placeholder="Contact Telephone"><button class="search-btn"><i class="fa fa-search"></i></button></td>
            </tr>
        </tbody>
    </table>
    </div>
    </div>
              </div>
             	
            </div>
				<div class="col-md-12">
					<div class="table-title">
					</div>
										<div class="table-responsive">
						<div id="example_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer"><div class="dt-buttons" style="margin-bottom:10px;">          <button class="dt-button buttons-copy buttons-html5" tabindex="0" aria-controls="example"><span><i class="fa fa-files-o"></i> &nbsp; Copy</span></button> <button class="dt-button buttons-excel buttons-html5" tabindex="0" aria-controls="example"><span><i class="fa fa-file-excel-o"></i>&nbsp; Excel</span></button> <button class="dt-button buttons-csv buttons-html5" tabindex="0" aria-controls="example"><span><i class="fa fa-file-text-o"></i> &nbsp; CSV</span></button> <button class="dt-button buttons-pdf buttons-html5" tabindex="0" aria-controls="example"><span><i class="fa fa-file-pdf-o"></i>&nbsp;  PDF</span></button> <button class="dt-button buttons-print" tabindex="0" aria-controls="example"><span><i class="fa fa-print"></i>&nbsp; Print</span></button> </div><div id="example_filter" class="dataTables_filter"></div></div><table class="table table-hover table-bordered dataTable no-footer" id="example" role="grid" aria-describedby="example_info">
							<thead>
								<tr role="row"><th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 19px;" aria-label="No: activate to sort column ascending">No</th><th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 70px;" aria-label="Client ID: activate to sort column ascending">Client ID</th><th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 185.933px;" aria-label="Company Name (Legal Name): activate to sort column descending" aria-sort="ascending">Client Legal Name <br><span style="font-size: 13px;">(Legal Name)</span></th><th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 65px;" aria-label="Action: activate to sort column ascending">Action</th></tr>
							</thead>							
							<tbody>
	
														<tr role="row" class="odd">
									<td style="text-align:center">1</td>
									<td>GA-214-1</td>
									<td class="sorting_1">Ashish of Gwinnett, Inc.</td>
								    <td style="text-align:center;">                                     <a class="btn-action btn-view-edit btn-primary" style="background:#367fa9 !important" href="https://financialservicecenter.net/fac-Bhavesh-0554/customer/73/edit"><i class="fa fa-edit"></i></a>
									<a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record ?'))
                                                                                  {event.preventDefault();document.getElementById('delete-id-73').submit();} else{event.preventDefault();}" href="https://financialservicecenter.net/fac-Bhavesh-0554/customer/73"><i class="fa fa-trash"></i></a>
<form action="https://financialservicecenter.net/fac-Bhavesh-0554/customer/73" method="post" style="display:none" id="delete-id-73">
                                        <input type="hidden" name="_token" value="UIc72HzKfEcynHkwpo8DmzSmaVrzZquPqmhXJjIr"> <input type="hidden" name="_method" value="DELETE">
                                        </form></td>
								</tr><tr role="row" class="even">
									
								</tr></tbody>
						</table><div class="dataTables_info" id="example_info" role="status" aria-live="polite">Showing 1 to 10 of 11 entries </div><div class="dataTables_paginate paging_simple_numbers" id="example_paginate"><ul class="pagination"><li class="paginate_button previous disabled" id="example_previous"><a href="#" aria-controls="example" data-dt-idx="0" tabindex="0">Previous</a></li><li class="paginate_button active"><a href="#" aria-controls="example" data-dt-idx="1" tabindex="0">1</a></li><li class="paginate_button "><a href="#" aria-controls="example" data-dt-idx="2" tabindex="0">2</a></li><li class="paginate_button next" id="example_next"><a href="#" aria-controls="example" data-dt-idx="3" tabindex="0">Next</a></li></ul></div></div>
					</div>
				</div>
			</div>
		</div>
		</div>
		</div>!-->



<?php $__env->startSection('main-content'); ?>
<style>
    .search-btn {
    position: absolute;
    top: 46px;
    right: 16px;
    background:transparent;
    border:transparent;
}
.panel-primary{
    /*border-color:#ffffff !important;*/
}

/**************** dropdown list ********************/    
    
a.disabled {
  /*pointer-events: none;*/
  cursor: default;
  opacity: 0.5;
  
  /*color: currentColor;*/
  /*cursor: not-allowed;*/ 
  /*opacity: 0.5;*/
  /*text-decoration: none;*/
}    
.table-bordered, .table-bordered > tbody > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > td, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > thead > tr > th {
    border-color: #555!important;
}
#countryList{margin:40px 0px 0px 0px;
    padding:0px 0px;
    border: 0px solid #ccc;
    max-height: 200px;
    overflow: auto; position: absolute;
    width: 360px;
    z-index: 99999;}

#countryList li {
    border-bottom: 1px solid #025b90;
    background: #ef7c30;
}
#countryList li a{padding:0px; display:block; color:#fff!important; height:40px;}
#countryList li a span.clientalign{display: flex;
    width: 100px;
    float: left;
    line-height: 15px;
    padding: 4px 0px;
    flex-direction: row;
    justify-content: flex-start;
    align-items: center;
    align-content: center;
    min-height: 40px; background:#038ee0;
    margin-left: 0;
    padding-left: 5px;}
    
    #countryList li a span.entityname{display: flex;
    width:230px;
    float: left;
    line-height: 15px;
    padding: 4px 0px; font-size:13px;
    flex-direction: row;
    justify-content: flex-start;
    align-items: center;
    align-content: center;
    min-height: 38px;
    margin-left: 10px;}




.new_images_sec .col-md-4{position:relative;}
.new_images_sec .col-md-4 .arrow{position:absolute; top:10px; right:-6px;}
.new_images_sec .col-md-4{position:relative;}
.new_images_sec .col-md-4 img{height:53px!important;}
.new_images_sec .col-md-4 .arrow{position:absolute; top:10px; right:-6px;}
.new_images_sec .col-md-4.lastimgbox .arrow{position:absolute; top:10px; left:-6px;}
.searchboxmain{float: left;  display: FLEX;  margin-top: -5px; justify-content: space-between;}
.searchboxmain .form-control{margin-right:10px!important;}
.clear{clear:both;}
.page-title h1{float:left; margin-left:15%;}
.page-title{padding:5px 15px 0px!important;}
.nav.nav-tabs li{width:10.6%!important;}
.clear{clear:both;}
.mt25{margin-top:25px;}
.text-center{text-align:center!important;}
.text-right{text-align:right!important;}
.table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th{padding:6px 8px 8px 8px!important;}
.pointernone{pointer-events:none}
.pad20{padding:20px;}.content-header
.nav-tabs > li.active > a.yel, .nav-tabs > li.active > a.yel:focus, .nav-tabs > li.active > a.yel:hover {
       cursor:default;
}
.nav-tabs > li{
    margin: 0px 0 0 4px;
}
.nav > li > a.yel:hover, .nav > li > a.yel:active, .nav > li > a.yel:focus {
    border-color:#000 !important;
    color:#000 !important;
    background:#ffff99;
    border-radius: 5px !important;
}
.nav-tabs {
    padding: 12px;
    border: 1px solid #3598dc !important;
}
.btnaddmore{background: #337ab7; display:inline-block; margin-bottom:5px;
    padding: 6px 10px;
    color: #fff;
    border-radius: 4px}
.btnremove{background: #ff0000;
    padding: 6px 10px;
    color: #fff;
    border-radius: 4px}
.padzero{padding:0px!important;}
.officermainbox{border:1px solid #ccc; padding:20px; margin-bottom:30px;}
.table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
    padding: 6px 8px 8px 8px !important;
}
.table-bordered, .table-bordered > tbody > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > td, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > thead > tr > th
{border-color:#555!important;}
.custom-file-upload {
  border: 1px solid #ccc;
  display: inline-block;
  padding: 6px 12px;
  cursor: pointer;
}
.card ul li{width:24% !important;}
   .card ul.test li a{background:#ffcc66 !important;}
   .card ul.test li.active a{background:#00a0e3 !important;}
   .card ul li a{display: block;width: 100%;color: #333;text-transform: capitalize;background: linear-gradient(180deg, #fdff9a 30%, #e3e449 70%);border: 1px solid #979800 !important;}
   .card ul li a:hover{color: #333;
   background: linear-gradient(180deg, #fbff37 30%, #d3d40e 70%);border: 1px solid #333 !important;}
   .card ul li.active{color: #333;background: linear-gradient(180deg, #fbff37 30%, #d3d40e 70%);border: 1px solid #333 !important;}
   .card ul li a.active, .card ul li a:hover, .card ul li.active a:hover{color: #fff!important;background: #12186b!important;border: 1px solid #12186b !important;}
   .card .nav-tabs{border:0px!important;}
.feeschargesbox .form-control{text-align:right;}
.hrdivider{border-bottom: 2px solid #ccc!important; width: 100%; height: 1px; margin-top: 33px; }
.hrdivider2{margin-top: 14px;   margin-bottom: 30px; border-bottom: 2px solid #ccc!important; width: 100%; height: 1px;}
.officerchange .form-control{background:#fff!important;}
.add-row{background: #007bff; padding: 3px 5px;  color: #fff;  border-radius: 3px; cursor: pointer;}
.delete-row{background:#dc3545; padding: 3px 5px;  color: #fff;  border-radius: 3px; cursor: pointer;}
.nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover {
    color: #FFF !important;
    border: 1px solid #12186b!important;
    background: #12186b!important;
    cursor: default;
    border-radius: 8px;
}
table th label.form-label {
    margin-bottom: 0px;
}
#renewRecord .form-control{margin-bottom:15px;}
#renewRecord  label.text-right{text-align:right!important; width:100%;}
.new_company_name p{font-size:13px!important;}

.nav>li>a {
    position: relative;
    display: block;
    padding: 10px 5px;
}
#myTab2 li a {
    padding: 8px 5px !important;
    font-size: 15px !important;
}
input[type="file"]{position: relative!important; margin-top:11px;
    width: 100px!important;
    font-size: 12px!important;
    opacity: 1!important;}

.searchboxmain .form-control{height:34px!important;}
.searchboxmain .btn-action{background: #367fa9 !important;
    padding: 8px 15px!important;
    margin-top: 0px;}
    .btnview{background:#5fb114!important;}
    .btnaddrecord {
    width: 120px;
    float: right;
    cursor: pointer;
}
.panel.panel-default .panel-heading h4 {
    font-size: 20px!important;
    padding: 10px!important;
    background: #b3e4a6!important;
}
.panel-title {
    padding: 20px;
    background: #5e77de;
    color: #fff;
    width: 100%;
    float: left;
    margin: 0 0 4px 0;
}
.panel.panel-default .panel-heading h4 a:hover, .panel.panel-default .panel-heading h4 a {
    color: #103b68!important;
}
.panel-title a.collapsed .glyphicon-plus {
    display: block;
}
.panel-title a .glyphicon-plus {
    display: none;
}
.glyphicon {
    position: relative;
    top: 1px;
    display: inline-block;
    font-family: 'Glyphicons Halflings';
    font-style: normal;
    font-weight: 400;
    line-height: 1;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
}
.panel-title a.collapsed .glyphicon-minus {
    display: none;
}
.panel-title a .glyphicon-minus {
    display: block;
}
.ac_name_first {
    width: 97%!important;
}
.ac_name_first {
    float: left;
}
#myModalAddrecord label {
    float: left;
}
.new_company_name label {
    width: 180px;
}
.new_company_name p {
    width: auto;
}
.modal-header{background:#f5efa8!important; border-bottom:2px solid #1b5bab;}
.modal-body label{text-align:right;}
.tab-pane.fade{display:none;}
.tab-pane.fade.in{display:block;}
.edittable tr td{padding:5px 8px 5px 8px !important;}
.table.table-bordered td{text-align:center;}
@media (max-width:1200px){
    .nav.nav-tabs li {
        width: 19.3%!important;
        margin: 2px;
    }
}
@media (max-width:850px){
    .nav.nav-tabs li {
        width: 19.1%!important;
    }
}
@media (max-width:550px){
    .nav.nav-tabs li {
        width: 48%!important;
    }
}
</style>
<div class="content-wrapper">
   
	<section class="page-title content-header">
	    <div class="searchboxmain" style="position:absolute;">
	         <input type="text" name="search" id="country_name" class="form-control" placeholder="Search Client">	 
                 <?php echo e(csrf_field()); ?>

                 <ul id="countryList"></ul>
                  <a class="btn-action btn-view-edit btn-primary" style="background:#367fa9 !important; padding:10px 20px;" href="https://financialservicecenter.net/fac-Bhavesh-0554/workstatus">Reset</a>
	        
	   </div>
		<h2>Work Status</h2>
		<div class="clear"></div>
	</section>
<div class="clear"></div>
	<div class="row"> 
<div class="col-md-12">
			<div class="">
		
             <?php if(!empty($common->filename)): ?>
      <section class="content-header">
      <div class="new-page-title" style="padding:0px;margin: -7px 0 7px 0;">
         <div class="new-page-title-tab">
            <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
               <div class="new_client_id">
                  <p><?php echo e($common->filename); ?> </p>
               </div>
            </div>
            <?php if($common->business_id=='6'): ?>
            <div class="col-lg-5 col-md-9 col-sm-9 col-xs-12">
            
        <div class="new_company_name" style="margin-top:25px !important;">
            <label> Name :</label> <?php echo e(ucwords($common->nametype)); ?>   <?php echo e($common->first_name); ?> <?php echo e($common->middle_name); ?> <?php echo e($common->last_name); ?>  </div></div>
          <?php else: ?>
         
            <div class="col-lg-5 col-md-9 col-sm-9 col-xs-12">
               <div class="new_company_name">
                  <label>Company Name :</label>
                  <p><?php echo e($common->company_name); ?></p>
               </div>
               <div class="new_company_name">
                  <label>Business Name :</label>
                  <p><?php echo e($common->business_name); ?></p>
               </div>
            </div>
            <?php endif; ?>
            <div class="col-lg-5 col-md-10 col-xs-12">
               <div class="new_images_sec">
                  <?php if(empty($common->business_cat_id)): ?> 
                  <div class="">
                     <?php else: ?> 
                     <?php if(empty($common->business_brand_category_id)): ?>
                     <div class="col-md-4 col-sm-4 col-xs-4">
                        <?php else: ?> 
                        <div class="col-md-4 col-sm-4 col-xs-4"> 
                           <?php endif; ?>
                           <?php endif; ?>
                           <?php $__currentLoopData = $business; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $busi): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                           <?php if($busi->id==$common->business_id): ?>
                           <img src="<?php echo e(url('public/frontcss/images/')); ?>/<?php echo e($busi->newimage); ?>" class="img-responsive" style="height:53px!important;">
                            <div class="arrow"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></div>
                        </div>
                        <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php $__currentLoopData = $category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>                                        
                        <?php if($common->business_cat_id==$cate->id): ?>
                        <?php if(empty($common->business_brand_category_id)): ?>
                       
                        <div class="col-md-4 col-sm-4 col-xs-4">
                           <?php else: ?> 
                         <!--  <div class="arrow">
                              <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                           </div>-->
                           <div class="col-md-4 col-sm-4 col-xs-4"> 
                              <?php endif; ?>
                              <img src="<?php echo e(url('public/category')); ?>/<?php echo e($cate->business_cat_image); ?>" alt="" class="img-responsive" />
                           </div>
                           <?php endif; ?>
                           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                           <?php $__currentLoopData = $cb; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bb1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>                                     
                           <?php if($common->business_brand_category_id == $bb1->id): ?>
                           
                           <div class="col-md-4 col-sm-4 col-xs-4 lastimgbox">
                               <div class="arrow">
                              <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                           </div>
                              <img src="<?php echo e(url('public/businessbrandcategory')); ?>/<?php echo e($bb1->business_brand_category_image); ?>" alt="" class="img-responsive" />
                           </div>
                           <?php endif; ?>
                           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                     </div>
                  </div>
                  </div>
   </section>
    <?php else: ?>

	<?php endif; ?><div class="clear"></div>
				<div class="col-md-12">
				   
				
					<?php if(!empty($common->filename)): ?>
				 <div class="panel with-nav-tabs panel-primary">
   <div class="panel-heading">
   <ul class="nav nav-tabs" id="myTab" style="padding:3px;">
   <li><a href="#tab10primary" data-toggle="tab">Accounting</a></li>
        
         <?php if($common->business_id !='6'): ?>
                     <li <?php if(empty($_REQUEST['action'])) { ?> class="active" <?php } ?>><a href="#tab1primary" data-toggle="tab">Corporation</a></li>
   <li><a href="#tab2primary" data-toggle="tab">License</a></li>
     <?php endif; ?>
                       
   <li <?php if(isset($_REQUEST['action']) && $_REQUEST['action'] =='tax') { ?>class="active"<?php } ?>><a href="#tab3primary"   data-toggle="tab">Taxation</a></li>
   <li><a href="#tab4primary" data-toggle="tab">Banking</a></li>
   <li><a href="#tab5primary" data-toggle="tab">Income</a></li>
   <li><a href="#tab6primary" data-toggle="tab">Expense </a></li>
    <li><a href="#tab60primary" data-toggle="tab">Asset </a></li>
   <li <?php if($common->business_id =='6') { ?><?php } ?>><a href="#tab7primary" data-toggle="tab">Other </a></li>
   </ul>
   </div>
   
   <?php 
   //echo "<pre>";print_r($common);?>
   <div class="panel-body tab-content" style="padding-left:0px;padding-right:0px;">
        <div class="tab-pane" id="tab10primary">
            <div class="panel-heading">
   <ul class="nav nav-tabs" id="myTab" style="padding:3px;">
   <li><a href="#tab10primary" data-toggle="tab">Accounting</a></li>
        
         <?php if($common->business_id !='6'): ?>
                     <li class="tab-pane fade in <?php if(empty($_REQUEST['action'])) { ?> active <?php } ?>  newcheckbox"><a href="#tab1primary" data-toggle="tab">Corporation</a></li>
   <li><a href="#tab2primary" data-toggle="tab">License</a></li>
     <?php endif; ?>
                       
   <li><a href="#tab3primary"  <?php if($common->business_id =='6'): ?> class="active" <?php endif; ?> data-toggle="tab">Taxation</a></li>
   <li><a href="#tab4primary" data-toggle="tab">Banking</a></li>
   <li><a href="#tab5primary" data-toggle="tab">Income</a></li>
   <li><a href="#tab6primary" data-toggle="tab">Expense </a></li>
    <li><a href="#tab60primary" data-toggle="tab">Asset </a></li>
   <li <?php if($common->business_id =='6') { ?> <?php } ?>><a href="#tab7primary" data-toggle="tab">Other </a></li>
   </ul>
   </div>
   
          </div>
           <div class="tab-pane <?php if($common->business_id =='6') { ?><?php } ?>" id="tab7primary">
                
               <div class="form-group col-md-12  card" style="background: #e0f1fd !important; border: 1px solid #3598dc !important;padding:0px; margin-right:5px; margin-left:0px;margin-bottom:0px;">
	                                        <div class="panel-heading">
                                                  <ul class="nav nav-tabs" id="myTab2" style="padding:5px !important;">
                                                      <li style="auto !important;"><a href="#subtab05primaryi" class="" data-toggle="tab" aria-expanded="true">Message</a></li>
                                                    
                                                      <li style="auto !important;"><a href="#subtab06primaryi" class="" data-toggle="tab" aria-expanded="true">Conversation</a></li>
                                                      <li style="auto !important;"><a href="#subtab08primaryi" class="" data-toggle="tab" aria-expanded="true">Task</a></li>
                                                     
                                                      <li style="auto !important;"><a href="#subtab07primaryi" class="" data-toggle="tab" aria-expanded="true">Notes</a></li>
                                                      <li style="auto !important;"><a href="#subtab09primaryi" class="" data-toggle="tab" aria-expanded="true">tab5</a></li>
                                                     
                                                  </ul>
                                                  </div>
                                                  </div>
	                <div class="tab-content">
	                    
    	                 <div class="tab-pane fade in active  newcheckbox" id="subtab05primaryi">
    	                    <br>
    	                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <br>
        						<div class="Branch" style="text-align:left;padding-left: 15px;">
        							<h1 class="text-center">Message History</h1>
        						</div>
        					</div>
    					    <div class="col-md-12 text-right">
    						       
    						        <div class="clear"></div>
    						        <div class="table-responsive">
    						       <table class="table table-hover table-bordered" id="example">
    						           <thead>
    						               <tr>
    						                    <th width="4%">No.</th>
            								    <th width="7%">Priority</th>
            									<th style="width:3% !important"> Date</br> Day</br> Time</th>
            								
            									<th style="width: 10% !important;">Telephone</th>
            									<th>Purpose</th>
            									<th width="18%">For Whom?</th>
            									<th width="18%">Received By</th>
            								    <th width="7%">Status</th>
            								    <th width="6%">Action</th>
    						               </tr>
    						           </thead>
    						           	<?php 
    						           	$cnt=count($fullmsg);
    						           	if($cnt>0)
    						           	{
    						           	$cnt=0;
    						           	?>
                                        <?php $__currentLoopData = $fullmsg; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $com): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php $cnt++;?>
                                        <?php 
                                        if($com->call_back =='1')
                                        {
                                            $callback='Regular';
                                        }
                                        else if($com->call_back =='2')
                                        {
                                            $callback='Important';
                                        }
                                        else if($com->call_back =='3')
                                        {
                                            $callback='Urgent';
                                        }
                                        
                                        ?>
    								<tr <?php if($com->status =='2'): ?> style="background:#b9f0b2;" <?php endif; ?>>
    								    <td style="text-align:center;"> <?php echo e($cnt); ?></td>
    								    <td><?php echo e($callback); ?> <?php if($com->call_back=='2'): ?> <img src="<?php echo e(URL::asset('public/img/Blinking_warning.gif')); ?>" alt="<?php echo e($callback); ?>" width="30px"> <?php elseif($com->call_back=='3'): ?>  <img src="<?php echo e(URL::asset('public/img/giphy.gif')); ?>" alt="<?php echo e($callback); ?>" width="30px"> <?php endif; ?></td>
    									<td><?php echo e($com->date); ?><br> <?php echo e($com->day); ?> <br> <?php echo e($com->time); ?></td>
    									
    									<td><?php echo e($com->clientno); ?></td>
    									<td><?php echo $com->purpose; ?></td>
    								    <td><?php $__currentLoopData = $emp2; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $com1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php if($com->employeeid==$com1->id): ?> <?php echo e(ucwords($com1->firstName.' '.$com1->middleName.' '.$com1->lastName)); ?> <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </td>
    									<td><?php $__currentLoopData = $emp1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $com1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php if($com->employee_id==$com1->id): ?> <?php echo e(ucwords($com1->firstName.' '.$com1->middleName.' '.$com1->lastName)); ?> <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></td>
    
                                    	<td><?php if($com->status =='2')  {?> <img src="<?php echo e(asset('public/dashboard/images/newimage.gif')); ?>" style="display:inline" alt="" class="img-responsive"><?php echo 'New'; } else if($com->status =='1'){ echo 'All';}?></td>
                                        
    								    <td class="text-center">
    									           <a class="btn-action btn-view-edit" href="<?php echo e(route('msg.edit',$com->id)); ?>"><i class="fa fa-edit"></i></a>			    
    				                                <!--<a class="btn-action btn-delete" href="#"><i class="fa fa-trash"></i></a>-->
    									</td>
    
    						                 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>												           
    						                 </tr>
    						        <?php
    						        }
    						        else
    						        {
    						            ?>
    										<tr><td style="text-align:center;" colspan="9">No records found</td></tr>
    								    <?php
    						        }
    						        ?>
    						            </tbody>
    						           
    						       </table>
    						    </div>
    						    </div>
    						    
    	                </div>
    	                
    	                <div class="tab-pane fade newcheckbox" id="subtab06primaryi">
    	                    <br>
    	                    <div class="col-md-12 col-sm-12 col-xs-12">
    	                        <br>
    							<div class="Branch" style="text-align:left; padding-left:15px;">
    								<h1 class="text-center">Conversation List</h1>
    							</div>
    						</div>
            				<br>
                                
                            <div class="col-md-12 col-sm-12 col-xs-12 text-right">
    						       
    						                        <div class="clear"></div>
    						                        <div class="table-responsive">
    						                         <table class="table table-hover table-bordered dataTable no-footer">
                    						           <thead>
                    						               <tr>
                    						                    <th width="7%">No.</th>
                            									<th width="13%"> Date</br> Day</br> Time</th>
                            									<th width="15%">Related To</th>
                            									<th width="30%">Description</th>
                            									<th width="30%">Notes</th>
                            									<th width="10%">Action</th>
                    						               </tr>
                    						           </thead>
                    						           	<?php 
                    						           	$cnt=count($conversation);
                    						           	if($cnt>0)
                    						           	{
                    						           	    $cnt=0;
                    						           	?>
                    						           <?php $__currentLoopData = $conversation; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $conversation): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    						           
                    						           
                    								    <tr>
                    								        <td style="text-align:center;"><?php echo e($loop->index+1); ?> </td>
                    								        <td style="text-align:center;"><?php echo e($conversation->creattiondate); ?><br> <?php echo e($conversation->day); ?> <br> <?php echo e($conversation->time); ?></td>
                    								        <td style="text-align:center;"><?php echo e($conversation->relatednames); ?> </td>
                    								        <td><?php echo e($conversation->condescription); ?> </td>
                                                            <td><?php echo e($conversation->connotes); ?> </td>
                                                            <td style="text-align:center;">
    									                        <a class="btn-action btn-view-edit conversationID" data-id="<?php echo e($conversation->id); ?>"><i class="fa fa-edit"></i></a>			    
    				                                            
    									                   </td>
    									                
    									                
                    						            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    						            
                    						            </tr>
                    						           <?php
                    						           	}
                    						           	else
                    						           	{
                    						           ?>
                    						            <tr><td style="text-align:center;" colspan="7">No records found</td></tr>
                    						            <?php
                    						           	}
                    						            ?>
                    						            </tbody>
                    						           
                    						       </table>
    						                    </div>
    						                    </div>
    	                </div>
    	                
    	                <div class="tab-pane fade newcheckbox" id="subtab07primaryi">
    	                    <br>
    	                    <div class="col-md-12 col-sm-12 col-xs-12">
    	                        <br>
    							<div class="Branch" style="text-align:left; padding-left:15px;">
    								<h1 class="text-center">Notes List</h1>
    							</div>
    						</div>
            				<br>
            							
            				<div class="col-md-12 col-sm-12 col-xs-12 text-right">
            						       
            						                        <div class="clear"></div>
            						                        <div class="table-responsive">
            						                         <table class="table table-hover table-bordered dataTable no-footer">
                            						           <thead>
                            						               <tr>
                            						                    <th width="7%">No.</th>
                                    								    
                                    									<th width="13%"> Date</br> Day</br> Time</th>
                                    								
                                    									<th width="15%">Related To</th>
                                    									<th width="30%">Description</th>
                                    									<th width="30%">Notes</th>
                                    									<th width="10%">Action</th>
                            						               </tr>
                            						           </thead>
                            						           <?php 
                            						           	$cnt=count($notesdata);
                            						           	if($cnt>0)
                            						           	{
                            						           	    $cnt=0;
                            						           	?>
                            						           <?php $__currentLoopData = $notesdata; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $conversation): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            								    <tr>
                            								        <td style="text-align:center;"><?php echo e($loop->index+1); ?> </td>
                            								        <td style="text-align:center;"><?php echo e($conversation->creattiondate); ?><br> <?php echo e($conversation->noteday); ?> <br> <?php echo e($conversation->notetime); ?></td>
                            								        <td style="text-align:center;"><?php echo e($conversation->notesrelated); ?> </td>
                            								        <td><?php echo e($conversation->notesrelatedcat); ?> </td>
                                                                    <td><?php echo e($conversation->notes); ?> </td>
                                                                    <td style="text-align:center;">
            									                        
            				                                             <a class="btn-action btn-view-edit notesID" data-id="<?php echo e($conversation->id); ?>"><i class="fa fa-edit"></i></a>
            									                   </td>
                            						            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            						            </tr>
                            						            <?php
                            						           	}
                            						           	else
                            						           	{
                            						           ?>
                            						            <tr><td style="text-align:center;" colspan="7">No records found</td></tr>
                            						            <?php
                            						           	}
                            						            ?>
                            						            </tbody>
                            						           
                            						       </table>
            						                    </div>    
            						                    </div>    
    	               </div>
    	               
    	                <div class="tab-pane fade newcheckbox" id="subtab08primaryi">
    	                       <br>
    	                    <div class="col-md-12 col-sm-12 col-xs-12">
    	                        <br>
    							<div class="Branch" style="text-align:left; padding-left:15px;">
    								<h1 class="text-center">Task List</h1>
    							</div>
    						</div>
            				<br>
                                
                            <div class="col-md-12 col-sm-12 col-xs-12 text-right">
    						       
    						                        <div class="clear"></div>
    						                       <div class="table-responsive">
						<table class="table table-hover table-bordered dataTable no-footer">
							<thead>
								<tr>
									<th width="13%">Task Date</th>
									<th width="20%">Created By</th>
									<th width="20%">Assign To</th>
									<th width="21%">Subject</th>
								    <th width="18%">Task Status</th>
                                    <th width="10%">Action</th>
								</tr>
							</thead>
							<tbody>
							    <?php $__currentLoopData = $taskall; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $com): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							    <?php //echo $com->created_at;
							    ?>
								<tr>
								    
									<td style="width:10%"><?php echo e(date('m/d/Y',strtotime($com->created_at))); ?></td>
									<td style="width:15%"><?php if($com->admin_id==Auth::user()->id): ?> <?php echo e(Auth::user()->fname.' '.Auth::user()->lname); ?> <?php endif; ?> <?php $__currentLoopData = $empfsc; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $com1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php if($com->admin_id==$com1->id): ?> <?php echo e(ucwords($com1->name)); ?> <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></td>
									<td style="width:15%"><?php $__currentLoopData = $set1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $com1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php if($com->employeeid==$com1->id): ?> <?php echo e(ucwords($com1->firstName)); ?> <?php echo e(ucwords($com1->middleName)); ?> <?php echo e(ucwords($com1->lastName)); ?> <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></td>
								
									<td style="width:15%"><?php echo e($com->title); ?></td>
								    <td><?php if($com->status==2): ?> In Progress <?php endif; ?> <?php if($com->status==0): ?> Wait <?php endif; ?>  <?php if($com->status==1): ?> Start <?php endif; ?> <?php if($com->status==3): ?> End <?php endif; ?></td>
                                    <td><a class="btn-action btn-view-edit" href="<?php echo e(route('task.edit',$com->id)); ?>"><i class="fa fa-edit"></i></a></td>
								</tr>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</tbody>
						</table>
            </div>
    						                    </div>
    	                
    	                </div>
    	                <div class="tab-pane fade newcheckbox" id="subtab09primaryi">
    	                     Income Tax 5
    	               </div>
	               </div>
    		   
            </div>
      <?php if($common->business_id !='6'): ?>  
       <div class="tab-pane fade <?php if(empty($_REQUEST['action'])) { ?> in  active <?php } ?> disablebox newcheckbox" id="tab1primary">
         <div class="col-md-12" style="margin-top:5px;">
               
            <div class="form-group row  card" style="background: #e0f1fd !important; border: 1px solid #3598dc !important; margin-right:5px; margin-left:0px;margin-bottom:0px;">
                <div class="col-lg-4 col-md-12" style="padding-top:5px; padding-right:0px;display: inline-block;padding-left:0px;">
                  <label class="col-lg-1 col-xs-4 text-right padtop7 wstate1" style="padding-left: 0px; margin-top:10px;">State :</label> 
                  <div class="col-md-3 wstate2 col-xs-3" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" value="<?php echo $common->stateId;?>" class="form-control fsc-input" readonly="">
                  </div>
                    <label class="col-lg-2 col-xs-4 text-right padtop7 wcontrol1" style="padding-left: 0px;margin-top:10px;">Control #:</label> 
                    <div class="col-md-3 wcontrol2 col-xs-3" style="padding-left: 0px;padding-right: 0px;">
                        <input type="text" value="<?php echo $common->contact_number;?>" class="form-control fsc-input" readonly="">
                    </div>
              </div>
            <div class="col-lg-8 col-md-12" style="padding-right:0px;padding-left:0px">
                <div class="panel-heading">
                    <ul class="nav nav-tabs" id="myTab2" style="padding:5px !important;float:right;">
                          <li style=" width: auto !important;" <?php if(empty($_REQUEST['action'])) { ?> class="active" <?php } ?>><a href="#subtab1primary" class="" data-toggle="tab">Corporation Renewal</a></li>
                          <li style=" width: auto !important;"><a href="#subtab2primary" class="" data-toggle="tab">Share Ledger</a></li>
                          <li style=" width: auto !important;"><a href="#subtabprimary6" class="" data-toggle="tab">Share Certi.</a></li>
                          <li style=" width: auto !important;"><a href="#subtabprimary7" class="" data-toggle="tab">Share Trf.</a></li>
                          <li style=" width:auto !important;"><a href="#subtab3primary" class="" data-toggle="tab">Amendment</a></li>
                          <li style=" width:auto !important;"><a href="#subtab4primary" class="" data-toggle="tab">Minutes</a></li>
                          <li style=" width:auto !important;"><a href="#subtab5primary" class="" data-toggle="tab">Docs</a></li>
                    </ul>
                </div>
            </div>
    </div>
              
                                        <div class="tab-content" style="padding-top:5px; ">
                                            <div class="tab-pane fade in <?php if(empty($_REQUEST['action'])) { ?> active <?php } ?>  newcheckbox " id="subtab1primary">
                                                <?php if(isset($formation1)!=''): ?>
                                                
                                                <a href="workrecord?id=<?php echo $common->id;?>" style="display:block; cursor:pointer;margin-bottom:10px;"   class="btn_new btn-renew btnaddrecord disabled">Add Record</a>
                                                <?php else: ?>
                                                <a class="btn_new btn-renew btnaddrecord"  data-toggle="modal" data-target="#addNewFormation" style="margin-bottom:10px">Add Record</a>
                                                
                                                <?php endif; ?>
                                            <div class="clear"></div>
                                                
                           <div class="table-responsive">
                                <table class="table table-bordered tablestriped">
                                    <thead>
                                        <tr>
                                            <th>Renew Year</th>
                                            <th>Renew For</th>
                                            <th>Paid Amount</th>
                                            <th>Payment Method</th>
                                            <th>Corporation Status</th>
                                            <th>SOS Reciept</th>
                                            <th>Annual Officer</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                   
                                    
                                    <tbody>
                                        <?php if($formation!=''): ?>
                                        <?php $__currentLoopData = $formation; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $formation): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td class="text-center"><?php echo e($formation->formation_yearbox); ?> <?php if($formation->formation_yearbox!=''): ?> Year <?php else: ?> <?php endif; ?></td>
                                            <td class="text-center"><?php echo e($formation->formation_yearvalue); ?></td>
                                            <td class="text-center"><?php if($formation->formation_amount ==''): ?> -- <?php else: ?>  $<?php echo e($formation->record_totalamt); ?> <?php endif; ?></td>
                                            <td class="text-center"><?php if($formation->formation_payment !=''): ?> <?php echo e($formation->formation_payment); ?> <?php else: ?> -- <?php endif; ?></td>
                                            <td class="text-center"><?php echo e($formation->record_status); ?></td>
                                            <td class="text-center"><?php if($formation->annualreceipt !='') {?>
                                            <a href="<?php echo e(url('public/adminupload')); ?>/<?php echo e($formation->annualreceipt); ?>" target="_blank" class="">
                                                <i class="fa fa-eye" aria-hidden="true"></i></a><?php } else { echo '--';} ?></td>
                                            <td class="text-center"><?php if($formation->formation_work_officer !='') {?>
                                            <a href="<?php echo e(url('public/adminupload')); ?>/<?php echo e($formation->formation_work_officer); ?>" target="_blank" class="">
                                                <i class="fa fa-eye" aria-hidden="true"></i></a><?php } else { echo '--';}?></td>
                                            <td class="text-center">
                                                
                                                <button type="button" class="btn-action btn-view-edit formationID" data-id="<?php echo e($formation->id); ?>"><i class="fa fa-edit"></i></button>            
    								            <form action="<?php echo e(route('workstatus.destroy',$formation->id)); ?>" method="post" style="display:none" id="delete-id-<?php echo e($formation->id); ?>">
                                                    <?php echo e(csrf_field()); ?> 
                                                </form>
                                            
    										    <a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record ?'))
                                                                                      {event.preventDefault();document.getElementById('delete-id-<?php echo e($formation->id); ?>').submit();}" href=""><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php else: ?>
                                        <tr><td style="text-align:center;" colspan="8">No records found</td></tr>
                                        <?php endif; ?>
                                        
                                    </tbody>
                                   
                                </table>
               
               
                               <div id="myModals_" class="modal fade">
                                   <div class="modal-dialog">
                                      <div class="modal-content">
                                         <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title">Annual Receipt  </h4>
                                         </div>
                                         <div class="modal-body">
                                            <p><iframe height="450" width="530" src="https://financialservicecenter.net/public/adminupload/"></iframe></p>
                                         </div>
                                         <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                         </div>
                                      </div>
                                   </div>
                                </div>


                                <div id="myModals1_" class="modal fade">
                                   <div class="modal-dialog">
                                      <div class="modal-content">
                                         <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title">Work Officer  </h4>
                                         </div>
                                         <div class="modal-body">
                                            <p><iframe height="450" width="530" src="https://financialservicecenter.net/public/adminupload/"></iframe></p>
                                         </div>
                                         <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                         </div>
                                      </div>
                                   </div>
                                </div>
                                               
               </div>
           </div>
                                            <div class="clear"></div>
                    <div class="tab-pane fade  newcheckbox" id="subtab2primary">
                        <div class="table-responsive">
                             <table class="table table-bordered table-striped">
                                 <thead>
                        <tr>
                            <th></th>
                            <th>NAME OF CERTIFICATE HOLDER</th>
                            <th>CERTIFICATES ISSUED  NO. SHARES* </th>
                            <th>FROM WHOM TRANSFERRED (If Original Issue Enter As Such)</th>
                            <th>AMOUNT PAID THEREON</th>
                            <th>DATE OF TRANSFER OF SHARES*</th>
                            <th>CERTIFICATES SURRENDERED CERTIF. NOS.</th>
                            <th>CERTIFICATES SURRENDERED NO. SHARES*</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>A</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>B</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>C</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>D</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        </tbody>
                        
                    </table>
                        </div>
                    </div>
                                            <div class="tab-pane fade  newcheckbox" id="subtab3primary">
                      Amendment
                    </div>
                                            <div class="tab-pane fade newcheckbox" id="subtab4primary">
                                                Minutes
                                            </div>
                                            <div class="tab-pane fade newcheckbox" id="subtab5primary">
                                                
                                                <div class="table-responsive">
                                               <table class="table table-hover table-bordered dataTable no-footer">
                                                    <thead>
                                                    <tr>
                                                        <th style="width:50px">No.</th>
                                                        <th class="text-left">Document Name</th>
                                                        <th style="width:80px;">View</th>
                                                        <th style="width:80px;">Action</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    
                                                    <tr>
                                                        <?php $__currentLoopData = $documentupload; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $doc1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <td class="text-center"><?php echo e($loop->index+1); ?></td>
                                                        <td class="text-left"><?php echo e($doc1->documentsname); ?></td>
                                                           <td class="text-center"><a href="<?php echo e(url('public/clientupload')); ?>/<?php echo e($doc1->clientdocument); ?>" target="_blank" class="" class="btn-action btn-view-edit btn-primary"><i class="fa fa-eye"></i></a></td>
                                                        <!--<td class="text-center"><?php echo e($doc1->clientdocument); ?></td>-->
                                                        <td class="text-center">
                                                            
                                                            <button type="button" class="btn-action btn-view-edit passingID" data-id="<?php echo e($doc1->id); ?>"><i class="fa fa-edit"></i></button>
                            							    <a onclick="return confirm('Are you sure to remove this record?')" href="<?php echo e(route('workstatus.destroydocument',[$doc1->id,$doc1->client_id])); ?>" class="btn-action btn-delete"><i class="fa fa-trash"></i></a>
                                                            
                                                        </td>
                                                    </tr>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    
                                                    
                                                    </tbody>
                                                </table> 
                                               <div class="clear"></div>
                                            </div>
                                            </div>
                                            
                                            <div class="tab-pane fade newcheckbox" id="subtabprimary6">
                                                <a id="add_certi" class="btn_new btn-renew btnaddrecord" style="width:170px;margin-bottom:10px;">Add Share Certificate</a>
                                                <label style="font-size:18px;display:flex;font-weight: 500;"><span style="padding-top:4px;">Starting Certi. No. : </span><input type="text" class="form-control" style="width: 90px;height: 35px;margin-left:10px;"> </label>
                                               <div class="clear"></div>
                                               <div class="table-responsive">
                                               <table class="table table-hover table-bordered dataTable no-footer">
                                                    <thead>
                                                    <tr>
                                                        <th width="10%">Certi. No.</th>
                                                        <th >Certi. Holder Name</th>
                                                        <th width="16%">No. of Share</th>
                                                        <th width="13%">Effective Date</th>
                                                        <th width="10%">Amount</th>
                                                        <th width="5%">Void</th>
                                                        <th width="10%">Action</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody class="addsharecerti">
                                                    
                                                    <tr>
                                                       <td><input type="text" class="form-control"></td>
                                                       <td><input type="text" class="form-control"></td>
                                                       <td><input type="text" class="form-control"></td>
                                                       <td><input type="date" class="form-control"></td>
                                                       <td><input type="text" class="form-control"></td>
                                                       <td><input type="checkbox" id="myCheck" name="myCheck"> <label class="fsc-form-label" for="myCheck" style="margin:0px;vertical-align:middle;"></label></td>
                                                        <td class="text-center">
                                                            <!--<a class="btn-action btn-view-edit"><i class="fa fa-eye"></i></a>-->
                            							    <a class="btn-action btn-view-edit" data-toggle="modal" data-target="#mycertipdf"><i class="fa fa-print"></i></a>
                                                            
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table> 
                                               </div>
                                            </div>
                                            <div class="tab-pane fade newcheckbox" id="subtabprimary7">
                                                Share Transfer
                                            </div>
                                            
                                        </div>
               
                                       <div class="clear"></div>
                                                                           </div>
        </div>
        <?php endif; ?>
        <div class="tab-pane fade disablebox newcheckbox" id="tab2primary">
           <div class="col-md-12">
    													<div class="Branch" style="text-align:left;padding-left: 15px;">
    														<h1 class="text-center">Business License History</h1>
    													</div>
    												</div>
												    <div class="col-md-12 text-right">
												       <a href="#myModalbusinesspopup" class="btn_new btn-renew inlinebutton pull-right" data-toggle="modal" data-target="#myModalbusinesspopup" style="margin-right: 10px;width: 120px;font-weight: bold; padding: 5px 0 !important;border-radius:0px;">Add Record</a>
												        <div class="clear"></div>
												        <div class="table-responsive">
												       <table class="table table-hover table-bordered dataTable no-footer">
												           <thead>
												               <tr>
												                   <th style="width:70px;">Year</th>
												                  
												                   <th style="width:130px;">License Gross</th>
												                    <th style="width:130px;">License Fee</th>
												                
												                   <th style="width:110px;">License #</th>
												                     <th style="width:130px;">License Issue Date</th>
												                   <th style="width:100px;">License Copy</th>
												                   <th style="width:80px">Status</th>
												                   <th style="width:100px">Action</th>
												                   <th style="width:80px">Form</th>
												               </tr>
												           </thead>
												           <tbody>
												               <tr>
												                 <?php $__currentLoopData = $buslicense; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bus_lic): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                												   	
												                   <td class="text-center"> <?php echo e($bus_lic->license_year); ?></td>
												                   <td class="text-center"><?php echo e($bus_lic->license_gross); ?></td>
												                   <td class="text-center"><?php echo e($bus_lic->license_fee); ?></td>
												                   <td> <?php echo e($bus_lic->license_no); ?></td>
												                   <td class="text-center"><?php echo date('m/d/Y',strtotime($bus_lic->license_renew_date))?></td>
												                   <td class="text-center"><a href="<?php echo e(url('public/adminupload')); ?>/<?php echo e($bus_lic->license_copy); ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></td>
												                   <td class="text-center"><?php echo e($bus_lic->license_status); ?></td>
												                   <td class="text-center">
												                        <a class="btn-action btn-view-edit" style="background:#367fa9 !important"><i class="fa fa-edit"></i></a>
				                                                        <a class="btn-action btn-delete" href="#"><i class="fa fa-trash"></i></a>
												                   </td>
												                   <td class="text-center"><a href="#" class="btn btn-primary btn-sm">Create</a></td>
												                 
												               </tr>
												                 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>												           
												            </tbody>
												           
												       </table>
												    </div>
												    </div>
        </div>
        <div class="tab-pane disablebox fade <?php if(isset($_REQUEST['action']) && $_REQUEST['action'] =='tax') { ?> in active<?php } ?>" id="tab3primary">
           <div class="form-group row  card" style="background: #e0f1fd !important; border: 1px solid #3598dc !important; margin-right:5px; margin-left:0px;margin-bottom:0px;">
                                      
                                      <div class="col-md-12" style="padding:0px;">
                                              <div class="panel-heading">
                                                  <ul class="nav nav-tabs" id="myTab2" style="padding:5px !important;">
                                                      <li style=" width:auto !important;" class="active"><a href="#subtab5primaryi" class="" data-toggle="tab" aria-expanded="true">Income Tax</a></li>
                                                     <?php if($common->business_id !='6'): ?>
                                                      <li style=" width:auto !important;"><a href="#subtab6primary" class="" data-toggle="tab" aria-expanded="true">Payroll Tax</a></li>
                                                       <li style=" width:auto !important;"><a href="#subtab6sprimary" class="" data-toggle="tab" aria-expanded="true">Sales Tax</a></li>
                                                       <li style=" width:auto !important;"><a href="#subtab6tprimary" class="" data-toggle="tab" aria-expanded="true">Tobacco Excise Tax</a></li>
                                                       <li style=" width:auto !important;"><a href="#subtab6coamprimary" class="" data-toggle="tab" aria-expanded="true">COAM</a></li>
                                                      <li style=" width:auto !important;"><a href="#subtab7primary" class="" data-toggle="tab" aria-expanded="true">Personal Property Tax</a></li>
                                                       <?php endif; ?>
                                                  </ul>
                                                  </div>
                                        </div>
                                    </div>
                                      <!--<a href="workrecord?id=<?php echo $_REQUEST['id'];?>"class="btn_new btn-renew btnaddrecord" disabled="disabled" data-target="">Add Record</a>-->
          
                                    <div class="tab-pane fade in active newcheckbox" id="subtab5primaryi">
                                        <?php if($Incomes1 ==0) {?>
										    <a class="btn_new btn-renew btnaddrecord" style="margin-bottom: 10px;" data-toggle="modal" data-target="#myModalAddrecord">Add Record</a><?php } ?>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        										    <div class="row">
            											<div class="Branch">
            												<div class="col-md-4" style="text-align:left;">
            													
            												</div>
            												<div class="col-md-3">
            													<h1> Taxation</h1>
            												</div>
            												
            												<div class="col-md-4" style="text-align:right;">
            													
            												</div>
            												
            											</div>
            										</div>
            										
            										<div class="clear"></div>
            										
                									<?php
                								//	print_r($Incometax3);
                									foreach($Incometaxfederal as $Income)
                									{
                									    
                									?>	
                									
                									<div class="panel-group" id="accordion" role="tablist"  style="margin-top:15px;">
                                                        <div class="panel panel-default">
                                                         
                                                            <div class="panel-heading" role="tab" id="headingtwo">
                                                                 <h4 class="panel-title" style="margin-bottom:0px;">
                                                                    <a role="button" data-toggle="collapse" data-parent="#accordion" class="collapsed" href="#collapse<?php echo $Income->id;?>">
                                                                    <span class="ac_name_first"><?php if($Income->federalsyear!=''): ?><?php echo e($Income->federalsyear); ?> <?php endif; ?></span>
                                                                   
                                                                    <i class="more-less glyphicon glyphicon-plus"></i>
                                                                    <i class="more-less glyphicon glyphicon-minus"></i>
                                                                   
                                                                    </a>
                                                                 </h4>
                                                            </div>
                                                      
                                                            <div style="clear:both;"></div>
                                                                
                                                            <div id="collapse<?php echo $Income->id;?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingtwo">
                                                                <div class="panel-body accordion-body" style="padding-top:15px;">
                                                                    <div class="table-responsive">
                                                                        <div class="table-responsive">
                                                                            <table class="table table-bordered tablestriped">
                                                                                        <thead>
                                                                                            <tr>
                                                                                                <th width="10%">Year</th>
                                                                                                <th width="15%">Tax Return</th>
                                                                                                <th width="15%">Form No.</th>
                                                                                                <th width="15%">Filing Date</th>
                                                                                                <th width="15%">Filing Method</th>
                                                                                                <th width="15%">Federal Status</th>
                                                                                                <th width="7%">PDF</th>
                                                                                                <th width="10%">State</th>
                                                                                                <th width="10%%">Action</th>
                                                                                            </tr>
                                                                                        </thead>
                                                                                       
                                                                                        
                                                                                        <tbody>
                                                                                           <?php //echo count($Incometax3);?>
                                                                                                <?php
                                                                                                foreach($Incometax3 as $Income2)
                									                                            {
                									                                              // echo count($Income2);
                									                                               if($Income2->federalsyear==$Income->federalsyear)
                									                                                {
                                                                                                ?>
                                                                                                 
                                                                                                  <tr>
                                                                                               <td class="text-center"><?php echo e($Income2->federalsyear); ?> </td>
                                                                                               <td class="text-center"><?php echo e($Income2->federalstax); ?> </td>
                                                                                               <td class="text-center"><?php echo e($Income2->federalsform); ?> </td>
                                                                                               <td class="text-center"><?php echo e(date('m/d/Y',strtotime($Income2->federalsdate))); ?> </td>
                                                                                               <td class="text-center"><?php echo e($Income2->federalsmethod); ?> </td>
                                                                                               <td class="text-center">
                                                                                                    <?php if($Income2->federalsstatus =='Accepted') { ?><a class="btn btn-success"><?php echo e($Income2->federalsstatus); ?> </a>
                                                                                                    <?php }
                                                                                                    else if($Income2->federalsstatus =='EF Processing'){ ?> <a class="btn btn-warning"><?php echo e($Income2->federalsstatus); ?> </a> <?php } 
                                                                                                    else if($Income2->federalsstatus =='Rejected'){ ?><a class="btn btn-warning"><?php echo e($Income2->federalsstatus); ?> </a><?php }
                                                                                                    else if($Income2->federalsstatus =='Pending'){ ?><a class="btn btn-danger"><?php echo e($Income2->federalsstatus); ?> </a><?php } ?>
                                                                                                   </td>
                                                                                               <td class="text-center"><?php if($Income2->federalsfile !='') {?>
                                            <a href="<?php echo e(url('public/adminupload')); ?>/<?php echo e($Income2->federalsfile); ?>" target="_blank" class="">
                                                <i class="fa fa-eye" aria-hidden="true"></i></a><?php } else { echo '--';} ?></td>
                                                                                               <td class="text-center"><a class="btn btn-primary" data-toggle="modal" data-target="#state_show_<?php echo $Income2->id;?>">State</a> 
                                                                                               
                                                                                               <div id="state_show_<?php echo $Income2->id;?>" class="modal fade">
                                                                                                       <div class="modal-dialog">
                                                                                                          <div class="modal-content">
                                                                                                             <div class="modal-header">
                                                                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                                                                <h4 class="modal-title"><span style="float:left;"><?php echo e($common->filename); ?></span>State<span style="float:right;"><?php echo e($common->first_name); ?> <?php echo e($common->last_name); ?></span>  </h4>
                                                                                                             </div>
                                                                                                             <div class="modal-body">
                                                                                                                 <table class="table table-bordered">
                                                                                                                    <thead>
                                                                                                                        <tr>
                                                                                                                            <th>State</th>
                                                                                                                            <th>Form No.</th>
                                                                                                                            <th>Filling Date</th>
                                                                                                                            <th>Filling Method</th>
                                                                                                                            <Th>PDF</Th>
                                                                                                                            <th>Filling Status</th>
                                                                                                                        </tr>
                                                                                                                     </thead>
                                                                                                                     <tbody>
                                                                                                                      <?php 
                                                                                                               foreach($Incometax2 as $ittax)
                                                                                                               {
                                                                                                                   if($Income2->id ==$ittax->federal_id)
                                                                                                                   {
                                                                                                               ?>
                                                                                                                     <tr>
                                                                                                                         <td><?php echo $ittax->statetax;?></td>
                                                                                                                         <td> <?php echo $ittax->stateformno;?></td>
                                                                                                                         <td> <?php if(isset($ittax->statedate) && $ittax->statedate !='1969-12-31')  { echo date('m/d/Y',strtotime($ittax->statedate));}?></td>
                                                                                                                         <td> <?php echo $ittax->statemethod;?></td>
                                                                                                                         <td><?php if($ittax->statefile !='') {?>
                                            <a href="<?php echo e(url('public/adminupload')); ?>/<?php echo e($ittax->statefile); ?>" target="_blank" class="">
                                                <i class="fa fa-eye" aria-hidden="true"></i></a><?php } else { echo '--';} ?></td>
                                                                                                                         <td> <?php if($ittax->statestatus =='EF Processing') {?><span class="btn btn-warning btn-xs"><?php echo $ittax->statestatus;?></span><?php } else if($ittax->statestatus =='Accepted'){?><span class="btn btn-success btn-xs"><?php echo $ittax->statestatus;?></span><?php } ?></td>
                                                                                                                         
                                                                                                                    </tr>
                                                                                                                      <?php
                                                                                                               }
                                                                                                               }
                                                                                                               ?>
                                                                                                               </tbody>
                                                                                                                 </table>
                                                                                                             
                                                                                                             </div>
                                                                                                             <div class="modal-footer">
                                                                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                                                             </div>
                                                                                                          </div>
                                                                                                       </div>
                                                                                                    </div>
                                                                                               </td>
                                                                                                
                                                                                              
                                                                                               
                                                                                               
                                                                                               
                                                                                               <td class="text-center">
                                                                                                <!--  <button type="button" class="btn-action btn-view-edit federalTaxation" data-id="<?php echo e($Income2->id); ?>"><i class="fa fa-edit"></i></button>!-->
                                                                                                <a href="<?php echo e(route('workstatustaxation.edit',$Income2->id)); ?>" class="btn-action btn-view-edit"><i class="fa fa-edit"></i></a>
                                                    					                        <a onclick="return confirm('Are you sure to remove this record?')" href="<?php echo e(route('workrecord.destroyfederal',$Income2->id)); ?>" class="btn-action btn-delete"><i class="fa fa-trash"></i></a>
                                                                                              </td>
                                                                                            
                                                                                            </tr>
                                                                                <?php
                									                                                }
                									                   
                									                                            }
                                                                                            ?>
                                                                                        </tbody>
                                               
                                                                                        
                                                                            </table>
                                                                            </div>	
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        
                                                        </div>
                                                    
                                                        
                                                    </div>
                                                    
                                                    <?php 
                									}
                									?>
                                        </div>
									
									
									 <div class="tab-pane fade  newcheckbox" id="subtab6sprimary">
									   <div class="Branch">
            												<div class="col-md-12" style="text-align:center;">
            													<h1>Sales Tax </h1>
            												</div>
            							</div>
									      <a class="btn_new btn-renew btnaddrecord"  data-toggle="modal" data-target="#addNewFormation">Add Record</a>
									    </div>
									    <div class="tab-pane fade  newcheckbox" id="subtab6tprimary">
									   
									     <div class="Branch">
            												<div class="col-md-12" style="text-align:center;">
            													<h1>Tobacco Excise Tax </h1>
            												</div>
            												
            							</div>
									      <a class="btn_new btn-renew btnaddrecord"  data-toggle="modal" data-target="#addNewFormation">Add Record</a>
									    </div>
									    <div class="tab-pane fade newcheckbox" id="subtab6coamprimary">
									    
									     <div class="Branch">
            												<div class="col-md-12" style="text-align:center;">
            													<h1>COAMx </h1>
            												</div>
            											
            							</div>
            							 <a class="btn_new btn-renew btnaddrecord"  data-toggle="modal" data-target="#addNewFormation">Add Record</a>
									    </div>
									 
									 
									   <!--<div class="tab-pane fade in active  newcheckbox" id="subtab5primaryi">-->
									   <!--<div class="tab-pane fade in active  newcheckbox" id="tab3primary">-->
									            
									            
                                                
                                                <!--<div class="tab-pane fade in active newcheckbox" id="subtab5primaryi1">-->
										           
                                                <!--</div>-->
                                                
                                                
									            
    										    
                                        <!--</div>     -->
									 
									   <div class="tab-pane fade newcheckbox" id="subtab6primary">
										        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    										        <div class="row">
        											<div class="Branch">
        												<div class="col-md-3" style="text-align:left;">
        													<h1>Federal / State</h1>
        												</div>
        												<div class="col-md-6">
        													<h1>Payroll Tax </h1>
        												</div>
        											</div>
        										</div>
    										
                                                    <a class="btn_new btn-renew btnaddrecord" data-toggle="modal" data-target="#">Add Record</a>
    										        <div class="clear"></div>
    										
    										        <div class="panel-group" id="accordion" role="tablist"  style="margin-top:15px;">
                                                <div class="panel panel-default">
                                             
                                                <div class="panel-heading" role="tab" id="headingtwo">
                                                     <h4 class="panel-title" style="margin-bottom:0px;">
                                                        <a role="button" data-toggle="collapse" data-parent="#accordion" class="collapsed" href="#collapsetwopt">
                                                        <span class="ac_name_first">2012</span>
                                                       
                                                        <i class="more-less glyphicon glyphicon-plus"></i>
                                                        <i class="more-less glyphicon glyphicon-minus"></i>
                                                       
                                                        </a>
                                                     </h4>
                                                  </div>
                                          
                                                <div style="clear:both;"></div>
                                                    
                                                <div id="collapsetwopt" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingtwo">
                                                    <div class="panel-body accordion-body" style="padding-top:15px;">
                                                    
                                                        <div class="table-responsive">
                                                            Year
                                                        </div>
                                                     
                                                    </div>
                                                </div>
                                            
                                                </div>
                                        
                                            
                                            </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade newcheckbox" id="subtab7primary">
										        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    										        <div class="row">
        											<div class="Branch">
        												<div class="col-md-3" style="text-align:left;">
        													<h1>Federal / State</h1>
        												</div>
        												<div class="col-md-6">
        													<h1>Personal Property Tax </h1>
        												</div>
        											</div>
        										</div>
    										
    										
    										
                                                    <a class="btn_new btn-renew btnaddrecord" data-toggle="modal" data-target="#">Add Record</a>
    										        <div class="clear"></div>
    										
    										        <div class="panel-group" id="accordion" role="tablist"  style="margin-top:15px;">
                                                 <div class="panel panel-default">
                                             
                                                  <div class="panel-heading" role="tab" id="headingtwo">
                                                     <h4 class="panel-title" style="margin-bottom:0px;">
                                                        <a role="button" data-toggle="collapse" data-parent="#accordion" class="collapsed" href="#collapsetwoppt">
                                                        <span class="ac_name_first">2012</span>
                                                       
                                                        <i class="more-less glyphicon glyphicon-plus"></i>
                                                        <i class="more-less glyphicon glyphicon-minus"></i>
                                                       
                                                        </a>
                                                     </h4>
                                                  </div>
                                          
                                                    <div style="clear:both;"></div>
                                                    
                                            <div id="collapsetwoppt" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingtwo">
                                             <div class="panel-body accordion-body" style="padding-top:15px;">
                                                
                                                     <div class="table-responsive">
                                                        Year
                                                    </div>
                                                 
                                            </div>
                                            </div>
                                            
                                        </div>
                                        
                                            
                                    </div>
                                                </div>
                                            </div>		
										
										  
        </div>
        <div class="tab-pane fade disablebox newcheckbox" id="tab4primary">
          <h1>Banking</h1>
        </div>
        
        <div class="tab-pane fade" id="tab5primary">
               <div class="row">
                   <div class="col-md-12">
                       <div class="Branch">	<h1>Income </h1></div>
                   </div>
                   
                   
                    <div class="col-md-12">
                                        
                    <div class="dividerbox"></div>
                    <?php
					foreach($incomes as $personal)
					{
					?>	
					<div class="panel-group" id="accordion" role="tablist"  style="margin-top:15px;">
                        <div class="panel panel-default">
                         
                            <div class="panel-heading" role="tab" id="headingtwo">
                                 <h4 class="panel-title" style="margin-bottom:0px;">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" class="collapsed" href="#collapse<?php echo $personal->id;?>">
                                    <span class="ac_name_first"><?php echo e($personal->wrkyear); ?>  <a></a></span>
                                    <i class="more-less glyphicon glyphicon-plus"></i>
                                    <i class="more-less glyphicon glyphicon-minus"></i>
                                   
                                    </a>
                                 </h4>
                            </div>
                      
                            <div style="clear:both;"></div>
                                
                            <div id="collapse<?php echo $personal->id;?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingtwo">
                                <div class="panel-body accordion-body" style="padding-top:15px;">
                                    <div class="table-responsive">
                                        <div class="table-responsive">
                                            <table class="table table-bordered tablestriped">
                                                        <thead>
                                                            <tr>
                                                                <th>Year</th>
                                                                <th>Wages</th>
                                                                <th>Tax Intrest</th>
                                                                <th>Dividends</th>
                                                                <th>Pension</th>
                                                                <th>Social Security</th>
                                                                <th>capital</th>
                                                                <th>Other Income</th>
                                                                <th>Total Amount</th>
                                                                <th width="100">Action</th>
                                                            </tr>
                                                        </thead>
                                                       
                                                             
                                                        <tbody>
                                                            <tr>
                                                                <td style="text-align:center;"><?php echo $personal->wrkyear;?></th>
                                                                <td style="text-align:center;"><?php if($personal->wagetotal!=''){ echo "$".''.number_format($personal->wagetotal,2);}?></th>
                                                                <td style="text-align:center;"><?php if($personal->taxintrest!=''){echo "$".''.number_format($personal->taxintrest,2);}?></th>
                                                                <td style="text-align:center;"><?php if($personal->qulifieddividends!=''){echo "$".''.number_format($personal->qulifieddividends,2);}?></th>
                                                                <td style="text-align:center;"><?php if($personal->pension!=''){echo "$".''.number_format($personal->pension,2);}?></th>
                                                                <td style="text-align:center;"><?php if($personal->socialsecurity!=''){echo "$".''.number_format($personal->socialsecurity,2);}?></th>
                                                                <td style="text-align:center;"><?php if($personal->capital !=''){echo "$".''.number_format($personal->capital,2);}?></th>
                                                                <td style="text-align:center;"><?php if($personal->otherincome!=''){echo "$".''.number_format($personal->otherincome,2);}?></th>
                                                                <td style="text-align:center;"><?php if($personal->totalamount!=''){echo "$".''.number_format($personal->totalamount,2);}?></th>
                                                                <td class="text-center"  style="text-align:center;">
                                                                    <button type="button" class="btn-action btn-view-edit incomeDetail" data-id="<?php echo e($personal->id); ?>"><i class="fa fa-edit"></i></button>
                    					                            <a onclick="return confirm('Are you sure to remove this record?')" href="<?php echo e(route('workrecord.destroyincomes',$personal->id)); ?>" class="btn-action btn-delete"><i class="fa fa-trash"></i></a>
                                                                </td>
                                                            </tr>
                                                                 
                                                        </tbody>
                                                    
               
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        
                        </div>
                    
                        
                    </div>
                    <?php 
					}
					?>
                                             
                                             
                    </div>
                   
               </div>
            </div>
        
        
        <div class="tab-pane fade  disablebox newcheckbox" id="tab6primary">
          <h1>tab6</h1>
        </div>
        <div class="tab-pane fade  disablebox newcheckbox" id="tab60primary">
          <h1>Asset</h1>
        </div>
  
   </div>
   </div>
						<?php endif; ?>
					</div>
				</div>
			<!--</div>-->
		<!--</div>-->
		<!--</div>-->
		<!--</div>-->









<!-- The Modal -->
<div class="modal" id="myModalbusinesspopup">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
        <div class="modal-body">
        
           <form method="post"  action="<?php echo e(route('workrecord.store')); ?>" class="form-horizontal" enctype="multipart/form-data">
                     <?php echo e(csrf_field()); ?>


            <?php
            if(isset($common->id)!='')
            {
                 //@if(!empty($common->filename))

                ?>
                    <input type="hidden" name="client_id" value="<?php echo e($common->id); ?>">
                <?php
            }
            ?>
            
            
            <div class="form-group row">
            <label class="col-md-4 control-label text-right">Year</label>
            <div class="col-md-6">
                <select class="form-control" name="license_year" required>
                    <option disabled>Select</option>
                    <option>2020</option>
                    <option>2021</option>
                    <option>2022</option>
                    <option>2023</option>
                </select>
            </div>
            </div>
        
            <div class="form-group row">
            <label class="col-md-4 control-label text-right">Gross Revenue</label>
            <div class="col-md-6"><input type="text" class="form-control" name="license_gross" id="license_gross" required/></div>
        </div>
        
            <div class="form-group row">
            <label class="col-md-4 control-label text-right">License Fee </label>
            <div class="col-md-6"><input type="text" class="form-control txtinput_1" name="license_fee" id="license_fee" required/></div>
        </div>
        
            <div class="form-group row">
            <label class="col-md-4 control-label text-right">License Status</label>
            <div class="col-md-6">
                <select class="form-control" name="license_status" required>
                    <option>Active</option>
                    <option>Inactive</option>
                    <option>Hold</option>
                </select>
            </div>
        </div>
        
        <div class="form-group row">
            <label class="col-md-4 control-label text-right">License Copy</label>
            <!--<div class="col-md-6">-->
            <!--    <input type="file" name="license_copy" required/>-->
            <!--</div>-->
            
            <div class="col-md-8">
                <label class="file-upload btn btn-primary">
                <input type="file" class="form-control" type="file" name="license_copy" style="display:none;" required/>
                Browse for file ... </label>
           </div>
        </div>
        
            <div class="form-group row">
            <label class="col-md-4 control-label text-right">Certificate No</label>
            <div class="col-md-6"><input type="text" class="form-control" name="license_no" required/></div>
        </div>
        
        <!--    <div class="form-group row">-->
        <!--    <label class="col-md-4 control-label text-right">Website Link</label>-->
        <!--    <div class="col-md-6"><input type="text" class="form-control" name="website_link" required/></div>-->
        <!--</div>-->

        <div class="form-group row">
            <label class="col-md-4 control-label text-right">Note</label>
            <div class="col-md-6"><input type="text" class="form-control" name="license_note" required/></div>
        </div>

            
            <div class="form-group row">
                <label class="col-md-4 control-label text-right">License Issue Date </label>
                <div class="col-md-6"><input type="date" class="form-control" name="license_renew_date" required/></div>
            </div>

            <div class="form-group row">
            <label class="col-md-4 control-label text-right"></label>
            <div class="col-md-3">
                <input class="btn_new_save btn-primary1 primary1" type="submit" value="Save">
            </div>
            <div class="col-md-3">
                <a class="btn_new_cancel" href="https://financialservicecenter.net/fac-Bhavesh-0554/employee">Cancel</a>
            </div>
        </div>
    
        </form>
        
      </div>

      <!-- Modal footer -->
      <!--<div class="modal-footer">-->
      <!--  <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>-->
      <!--</div>-->

    </div>
  </div>
</div>		



                                        <!-- Modal -->
                                        <div class="modal fade" id="myModal" role="dialog">
                                            <div class="modal-dialog">
                                            
                                              <!-- Modal content-->
                                              <div class="modal-content">
                                                <div class="modal-header">
                                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                  <h4 class="modal-title">Update Document Record</h4>
                                                </div>
                                                <div class="modal-body">
                                                <?php
                                                if(isset($doc1->id)!='')
                                                {
                                                
                                                ?>
                                                    <form method="post" class="form-horizontal" enctype="multipart/form-data" action="<?php echo e(route('workstatus.updatedocuments',$doc1->id)); ?>">
        									<?php echo e(csrf_field()); ?>

        								<?php
                                        }
                                        ?>
                                                    <input type="hidden" class="form-control" name="idaa" id="idkl">
                                                    <input type="hidden" class="form-control" name="filename_idss" id="filename_idss">
                                                    <input type="hidden" class="form-control" name="client_id" id="client_id">
                                                    <!--<input type="text" class="form-control" name="documentsname" id="documentsname">-->
                                                    <!--<input type="text" class="form-control" name="clientdocument" id="clientdocument">-->
                                                  
                                                        </br>
                                                           <div class="row">
                                                                <div class="col-md-4"><label class="control-label text-right">Document Name :</label></div>
                                                                <div class="col-md-8">
                                                                   <select class="form-control"  style="width:85%;" name="documentsname"  id="documentsname" required>
                                                                      <?php $__currentLoopData = $document; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cur): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                      <option value="<?php echo e($cur->documentname); ?>"><?php echo e($cur->documentname); ?></option>
                                                                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                   </select>
                                                                   <?php if($errors->has('documentsname')): ?>
                                                                   <span class="help-block">
                                                                   <strong><?php echo e($errors->first('documentsname')); ?></strong>
                                                                   </span>
                                                                   <?php endif; ?>
                                                                </div>
                                                            </div>

                                                            </br>
                                                            <div class="row">
                                                               <div class="col-md-4"><label class="control-label text-right">Upload Document : </label>
                                                               </div>
                                                               <div class="col-md-8">
                                                                   
                                                                    <label class="file-upload btn btn-primary">
                                                                    <input type="file" class="form-control" style="display:none;" name="clientdocument" />
                                                                    Browse for file ... </label>
                                                                     <input type="hidden"  name="clientdocument_1" id="clientdocument"/><span id="clientdocument22"></span>
                                                               </div>
                                                           </div>
                                       
                                                            </br>
                                                            <div class="row">
                                                               <div class="col-md-4">
                                                               </div>
                                                               <div class="col-md-2" style=" padding-right:3px;">
                                                           
                                                                   <input class="btn_new_save btn-primary1 primary1" type="submit" id="primary1" name="submit" value="Save">
                                                                  </div>
                                                                    <div class="col-md-2" style="padding-left:3px;">
                                                                   <a class="btn_new_cancel" data-dismiss="modal">Cancel</a>
                                                                </div>
                                                            </div>
                                                    
                                                  </form>
                                                </div>
                                                <div class="modal-footer">
                                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                </div>
                                              </div>
                                              
                                            </div>
                                          </div>
                                       
                                    </div>
                                </div>
                                
                                
                                
                                



<!--Modal Formation Add new Start-->
                                       <?php 
                                       if(isset($common->id)!='')
                                       {
                                       ?>
                                      
                                            <form method="post" class="form-horizontal" enctype="multipart/form-data" action="<?php echo e(route('workstatus.addformation')); ?>">
                            				    <?php echo e(csrf_field()); ?>

                                                     
              
                                                     <?php //print_r($getdataclient);?>
                                                    <div class="modal fade" id="addNewFormation" role="dialog">
                                                        <div class="modal-dialog modal-lg">        
                                                            <!-- Modal content-->
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                    <h4 class="modal-title">
                                                                        <span class="pull-left"><?php echo $getdataclient->filename;?></span>
                                                                        Corporation Renewal Info
                                                                        <span class="pull-right" style="margin-right: 15px;"><?php echo $getdataclient->company_name;?></span>
                                                                    </h4>
                                                                  </div>
                                                                <?php
                                                                $forms=array();
                                                              //  print_r($formation);exit;
                                                                foreach($formationarray as $form)
                                                                {
                                                                    $forms[]=$form->formation_yearvalue;
                                                                }
                                                                ?>
                                                                <div class="modal-body">
                                                                     <div class="form-group row">
                                                                       
                                                                           <label class="col-md-4 control-label text-right">Year : </label>
                                                                       
                                                                       <div class="col-md-5">
                                                                            
                                                                            <select class="form-control fsc-input selectyear">
                                                                               <option value="">Select</option>
                                                                               <option value="2020" <?php if(in_array('2020',$forms)){ ?>style="display:none;"<?php } ?>>2020</option>
                                                                               <option value="2021" <?php if(in_array('2021',$forms)){ ?>style="display:none;"<?php } ?>>2021</option>
                                                                               <option value="2022" <?php if(in_array('2022',$forms)){ ?>style="display:none;"<?php } ?>>2022</option>
                                                                           </select>
                                                               
                                                                           
                                                                       </div>
                                                                    </div>
                                                                   
                                                                    <div class="form-group row">
                                                                    <?php if(!empty($common->id)): ?>
                                                                      <input type="hidden" name="formation_client_id" value="<?php echo $common->id;?>">
                                                                      <?php endif; ?>
                                                                       
                                                                           <label class="col-md-4 control-label text-right">Renew For : </label>
                                                                       
                                                                       <div class="col-md-5">
                                                                            
                                                                            <select class="form-control fsc-input" name="formation_yearbox" id="formation_yearbox11" onChange="getvalYear11();">
                                                                               <option value="">Select</option>
                                                                               <option value="1">1 Year</option>
                                                                               <option value="2">2 Year</option>
                                                                               <option value="3">3 Year</option>
                                                                           </select>
                                                               
                                                                           
                                                                       </div>
                                                                    </div>
                                                                    
                                                                   
                                                                    <div class="form-group row">
                                                                       
                                                                           <label class="col-md-4 control-label text-right">Renew Year : </label>
                                                                       
                                                                       <div class="col-md-5">
                                                                            <input type="text" class="form-control showw" name="formation_yearvalue" id="" required readonly/>
                                                                       </div>
                                                                       
                                                                      
                                                                    </div>
                                                                    
                                                                
                                                                   
                                                                   <div class="form-group row">
                                                                       
                                                                           <label class="col-md-4 control-label text-right">Paid By : </label>
                                                                       
                                                                       <div class="col-md-5">
                                                                            <select class="form-control fsc-input paidby2" name="record_paid_by">
                                                                               <option value="">Select</option>
                                                                               <option value="Client">Client</option>
                                                                               <option value="FSC">FSC</option>
                                                                           </select>
                                                                           
                                                                       </div>
                                                                    </div>
                                                                    
                                                                    <div class="hidepaid2">
                                                                        <div class="form-group row">
                                                                           
                                                                               <label class="col-md-4 control-label text-right">Annual Fees ($) : </label>
                                                                           <div class="col-md-5">
                                                                                <input type="text" class="form-control annualfees" name="formation_amount" id="formation_amount11"/>
                                                                           </div>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                           
                                                                               <label class="col-md-4 control-label text-right">Penalty Charges ($) : </label>
                                                                           
                                                                           
                                                                           <div class="col-md-5">
                                                                              <input class="form-control penaltycharges" type="text" name="formation_penalty" id="formation_penalty">
                                                                            </div>
                                                                            
                                                                       </div>
                                                                       <div class="form-group row">
                                                                           
                                                                               <label class="col-md-4 control-label text-right">Processing Charges ($) : </label>
                                                                           <div class="col-md-5">
                                                                              <input class="form-control processingfees" type="text" name="work_processingfees" id="work_processingfees">
                                                                            </div>
                                                                            
                                                                       </div>
                                                                       <div class="form-group row">
                                                                           
                                                                               <label class="col-md-4 control-label text-right">Total Amount ($) : </label>
                                                                           <div class="col-md-5">
                                                                                <input type="text" class="form-control totalamt" name="record_totalamt" id="record_totalamt" readonly/>
                                                                           </div>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                           
                                                                               <label class="col-md-4 control-label text-right">Method of Payment : </label>
                                                                           <div class="col-md-5">
                                                                              <input class="form-control" type="text" name="formation_payment" id="formation_payment">
                                                                            </div>
                                                                            
                                                                       </div>
                                                                       <div class="form-group row">
                                                                           
                                                                               <label class="col-md-4 control-label text-right">Paid Date : </label>
                                                                           <div class="col-md-5">
                                                                              <input class="form-control paiddate" type="text" name="paiddate" id="paiddate" readonly>
                                                                            </div>
                                                                            
                                                                       </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                       <label class="col-md-4 control-label text-right">Status : </label>
                                                                       
                                                                       <div class="col-md-5">
                                                                           <select class="form-control" name="record_status" id="record_status" required>
                                                                                 <option value="">Select</option>
                                                                                 <option value="Active Owes Curr. Yr. AR">Active Owes Curr. Yr. AR</option>
                                                                                 <option value="Active Compliance">Active Compliance</option>
                                                                                 <option value="Active Noncompliance">Active Noncompliance</option>
                                                                                 <option value="Admin. Dissolved">Admin. Dissolved</option>
                                                                                 <option value="Dis-Cancel-Termin">Dis-Cancel-Termin</option>
                                                                             
                                                                            </select>
                                                                       </div>
                                                                   </div>
                                                                   <div class="form-group row">
                                                                       <label class="col-md-4 control-label text-right">Annually Receipt : </label>
                                                                       <div class="col-md-6">
                                                                           
                                                                            <label class="file-upload btn btn-primary">
                                                                            <input type="file" class="form-control" name="annualreceipt" style="display:none;" />
                                                                            Browse for file ... </label>
                                                                             
                                                                       </div>
                                                                   </div>
                                                                    <div class="form-group row">
                                                                       <label class="col-md-4 control-label text-right">Officer : </label>
                                                                       <div class="col-md-6">
                                                                           
                                                                            <label class="file-upload btn btn-primary">
                                                                             <input type="file" class="form-control" name="formation_work_officer" style="display:none;" />
                                                                             Browse for file ... </label>
                                                                             
                                                                       </div>
                                                                   </div>
                                                                   
                                                                   <div class="form-group row">
                                                                       <label class="col-md-4 control-label text-right">Note : </label>
                                                                       <div class="col-md-7">
                                                                             <textarea class="form-control" name="record_note" id="record_note" style="height:70px;" requiired></textarea>
                                                                       </div>
                                                                   </div>
                                                                   
                                                                    <div class="form-group row">
                                                                       <div class="col-md-4">
                                                                       </div>
                                                                       <div class="col-md-2" style=" padding-right:3px;">
                                                    
                                                                           <input class="btn_new_save btn-primary1 primary1" type="submit" id="primary1" name="submit" value="Save">
                                                                          </div>
                                                                            <div class="col-md-2" style="padding-left:3px;">
                                                                           <a class="btn_new_cancel" data-dismiss="modal">Cancel</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                
                                                                <!--<div class="modal-footer">-->
                                                                    <!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                                                                <!--</div>-->
                                                            </div>
                                                        </div>
                                                    </div>
                                            </form>
                                       <?php
                                       }
                                       ?>                            
                                        <!--Modal Formation Add new End-->


   
        
                                        <!--Modal Formation Update Start-->
                                       
                                            <form method="post" class="form-horizontal" enctype="multipart/form-data" action="<?php echo e(route('workstatus.update')); ?>">
                            				    <?php echo e(csrf_field()); ?>

                                                    
                                                    
                                                    <div class="modal fade" id="formationModal" role="dialog">
                                                        <div class="modal-dialog">
                                                            <!-- Modal content-->
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                    <h4 class="modal-title">SOS - Corporation Renewal</h4>
                                                                  </div>
                                                                <div class="modal-body">
                                                                    <div class="form-group row">
                                                                       <div class="col-md-4">
                                                                           <label class="control-label text-right">Renew For : </label>
                                                                       </div>
                                                                       <div class="col-md-5">
                                                                            
                                                                            <input type="hidden" name="formationid" id="formationid">
                                                                            <input type="hidden" name="client_id" id="client_id77">
                                                                            <input type="text" class="form-control" name="formation_yearbox" id="formation_yearbox77" required readonly/>
                                                                           
                                                                       </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                       <div class="col-md-4">
                                                                           <label class="control-label text-right">Renew Year : </label>
                                                                       </div>
                                                                       
                                                                       <div class="col-md-5">
                                                                            <input type="text" class="form-control" name="formation_yearvalue" id="formation_yearvalue77" required readonly/>
                                                                       </div>
                                                                       
                                                                      
                                                                    </div>
                                                                    
                                                                    <!--<div class="form-group row">-->
                                                                    <!--   <div class="col-md-4">-->
                                                                    <!--       <label class="control-label text-right">Renew Amount : </label>-->
                                                                    <!--   </div>-->
                                                                       
                                                                    <!--   <div class="col-md-5">-->
                                                                    <!--      <input class="form-control" type="text" readonly name="formation_amount" id="formation_amount77" required>-->
                                                                    <!--    </div>-->
                                                                        
                                                                    <!--</div>-->
                                                                   
                                                                   
                                                                    
                                                                   
                                                                   
                                                                   
                                                                   
                                                                   <div class="form-group row">
                                                                       <div class="col-md-4">
                                                                           <label class="control-label text-right">Paid By : </label>
                                                                       </div>
                                                                       <div class="col-md-5">
                                                                            <select class="form-control fsc-input paidby2 record_paid_by88" name="record_paid_by" id="record_paid_by77">
                                                                               <option value="">Select</option>
                                                                               <option value="Client">Client</option>
                                                                               <option value="FSC">FSC</option>
                                                                           </select>
                                                                           
                                                                       </div>
                                                                    </div>
                                                                    
                                                                    <div class="hidepaid2">
                                                                        <div class="form-group row">
                                                                           <div class="col-md-4">
                                                                               <label class="control-label text-right">Annual Fees ($) : </label>
                                                                           </div>
                                                                           
                                                                           <div class="col-md-5">
                                                                                <input type="text" class="form-control annualfees22" name="formation_amount" id="formation_amount77"/>
                                                                           </div>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                           <div class="col-md-4">
                                                                               <label class="control-label text-right">Penalty Charges ($) : </label>
                                                                           </div>
                                                                           
                                                                           <div class="col-md-5">
                                                                              <input class="form-control penaltycharges22" type="text" name="formation_penalty" id="formation_penalty77">
                                                                            </div>
                                                                            
                                                                       </div>
                                                                       <div class="form-group row">
                                                                           <div class="col-md-4">
                                                                               <label class="control-label text-right">Processing Charges ($) : </label>
                                                                           </div>
                                                                           
                                                                           <div class="col-md-5">
                                                                              <input class="form-control processingfees22" type="text" name="work_processingfees" id="work_processingfees77">
                                                                            </div>
                                                                            
                                                                       </div>
                                                                       <div class="form-group row">
                                                                           <div class="col-md-4">
                                                                               <label class="control-label text-right">Total Amount ($) : </label>
                                                                           </div>
                                                                           
                                                                           <div class="col-md-5">
                                                                                <input type="text" class="form-control totalamt22" name="record_totalamt" id="record_totalamt77" readonly/>
                                                                           </div>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                           <div class="col-md-4">
                                                                               <label class="control-label text-right">Method of Payment : </label>
                                                                           </div>
                                                                           
                                                                           <div class="col-md-5">
                                                                              <input class="form-control" type="text" name="formation_payment" id="formation_payment77">
                                                                            </div>
                                                                            
                                                                       </div>
                                                                       <div class="form-group row">
                                                                           <div class="col-md-4">
                                                                               <label class="control-label text-right">Paid Date : </label>
                                                                           </div>
                                                                           
                                                                           <div class="col-md-5">
                                                                              <input class="form-control paiddate" type="text" name="paiddate" id="paiddate77" readonly>
                                                                            </div>
                                                                            
                                                                       </div>
                                                                    </div>
                                                                                                                                                       
                                                                  
                                                                   
                                                                    
                                                                   
                                                                   
                                                                   
                                                                    <!--<div class="form-group row">
                                                                       <div class="col-md-4"><label class="control-label text-right">Status : </label>
                                                                       </div>
                                                                       <div class="col-md-5">
                                                                           <select class="form-control" name="record_status" id="record_status77" required>
                                                                                 <option value="">Select</option>
                                                                                 <option value="Active Owes Curr. Yr. AR">Active Owes Curr. Yr. AR</option>
                                                                                 <option value="Active Compliance">Active Compliance</option>
                                                                                 <option value="Active Noncompliance">Active Noncompliance</option>
                                                                                 <option value="Admin. Dissolved">Admin. Dissolved</option>
                                                                                 <option value="Dis-Cancel-Termin">Dis-Cancel-Termin</option>
                                                                             
                                                                            </select>
                                                                       </div>
                                                                   </div>!-->
                                                                   
                                                                    <?php if(isset($formation->record_status)!=''): ?>  
                                                                    <div class="form-group row">
                                                                       <div class="col-md-4"><label class="control-label text-right">Status : </label>
                                                                       </div>
                                                                       <div class="col-md-5">
                                                                           <!--<input class="form-control" name="record_status"  id="record_status77" value="<?php echo e($formation->record_status); ?>" required>-->
                                                                           <select class="form-control" name="record_status" id="record_status77" required>
                                                                                 <option value="">Select</option>
                                                                                 <option value="Active Owes Curr. Yr. AR">Active Owes Curr. Yr. AR</option>
                                                                                 <option value="Active Compliance">Active Compliance</option>
                                                                                 <option value="Active Noncompliance">Active Noncompliance</option>
                                                                                 <option value="Admin. Dissolved">Admin. Dissolved</option>
                                                                                 <option value="Dis-Cancel-Termin">Dis-Cancel-Termin</option>
                                                                            </select>
                                                                       </div>
                                                                    </div>
                                                                    <?php else: ?>
                                                                    <div class="form-group row">
                                                                        <input type="hidden" name="record_status" value="Active Compliance">
                                                                        <div class="clear"></div>
                                                                    </div>    
                                                                    <?php endif; ?>
                                                                    
                                                                    <div class="form-group row">
                                                                       <div class="col-md-4"><label class="control-label text-right">Annually Receipt : </label>
                                                                       </div>
                                                                       <div class="col-md-6">
                                                                           
                                                                            <label class="file-upload btn btn-primary">
                                                                            <input type="file" class="form-control" name="annualreceipt" style="display:none;" />
                                                                            Browse for file ... </label>
                                                                             <input type="hidden"  name="annualreceipt_1" id="annualreceipt77" /><span id="annualreceipt2_77"></span>
                                                                       </div>
                                                                   </div>
                                                                    <div class="form-group row">
                                                                       <div class="col-md-4"><label class="control-label text-right">Officer : </label>
                                                                       </div>
                                                                       <div class="col-md-6">
                                                                           
                                                                            <label class="file-upload btn btn-primary">
                                                                             <input type="file" class="form-control" name="formation_work_officer" style="display:none;" />
                                                                             Browse for file ... </label>
                                                                             <input type="hidden" class="form-control" name="officers" id="formation_work_officer77"/><span id="formation_work_officer2_77"></span>
                                                                       </div>
                                                                   </div>
                                                                   
                                                                   <div class="form-group row">
                                                                       <div class="col-md-4"><label class="control-label text-right">Note : </label>
                                                                       </div>
                                                                       <div class="col-md-7">
                                                                             <textarea class="form-control" name="record_note" id="record_note77" style="height:70px;" requiired></textarea>
                                                                       </div>
                                                                   </div>
                                                                   
                                                                    <div class="form-group row">
                                                                       <div class="col-md-4">
                                                                       </div>
                                                                       <div class="col-md-2" style=" padding-right:3px;">
                                                    
                                                                           <input class="btn_new_save btn-primary1 primary1" type="submit" id="primary1" name="submit" value="Save">
                                                                          </div>
                                                                            <div class="col-md-2" style="padding-left:3px;">
                                                                           <a class="btn_new_cancel" data-dismiss="modal">Cancel</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                
                                                                <div class="modal-footer">
                                                                    <!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </form>
                                                                   
                                        <!--Modal Formation Update End-->
    
    
    <!-- Modal -->
                    
   <div id="myModalAddrecord" class="modal fade" role="dialog">
  <div class="modal-dialog" style="width:1000px;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><span class="pull-left"><?php if(!empty($common->filename)): ?> <?php echo e($common->filename); ?> <?php endif; ?></span>Client - Income Tax Filing Return <span style="float:right;">Add Record</span></h4>
      </div>
      <div class="modal-body">
          <form class="form-horizontal" method="post" action="<?php echo e(route('workrecord.storefederaladmin')); ?>" enctype="multipart/form-data">
              <?php echo e(csrf_field()); ?> 
              
              <?php if(!empty($common->id)): ?>
              <input type="hidden" name="client_id" value="<?php echo $common->id;?>">
              <?php endif; ?>
            <div class="recordform">
              
                <table style="width:100%;" class="taxtable table">
                <tr>
                    <td style="width:360px;">
                        <label class="control-label labels">Client Name:</label>
                        <?php 
                        if(isset($common->business_id) && $common->business_id =='6') 
                        {
                            $names=$common->first_name.' '.$common->middle_name.' '.$common->last_name;
                            
                        }
                        else if(isset($common->business_id) && $common->business_id !='6')
                        {
                             $names=$common->company_name;
                        }?>
                        <input type="text" class="form-control clname" value="<?php if(isset($common->business_id)) { echo $names; }?>" readonly="">
                         
                    </td>
                     <?php
                      $carray=array();
                   
                     if($clientorg)
                     {
                    foreach($clientorg as $corg)
                    {
                       $carray[]=$corg->federalsyear; 
                    }
                     }
                    ?>
                   
                    <td style="width:140px;">
                        <label class="control-label labels">Filling Year</label>
                            <select class="form-control federalyear" name="federalsyear" required>
                        
                        <option value="">Select</option>
                        
                        
                                   <?php if(!in_array('2019',$carray)) {?> <option value="2019">2019</option><?php } ?>
                               <?php if(!in_array('2020',$carray)) {?><option value="2020">2020</option><?php } ?>
                     
                    </select>
                        
                    </td>
                    <td style="width:150px;">
                         <label class="control-label labels">Tax Return</label>
                         
                         <select class="form-control taxation" name="federalstax" required>
                            <option value="">Select</option>
                            <?php if($clientfedext < 1) { ?> <option value="Extension">Extension </option><?php } ?>
                            <?php if($clientfed < 1) { ?><option value="Original">Original</option><?php } ?>
                           <!-- <option value="Amendment">Amendment</option>!-->
                        </select>
                    </td>
                    <td style="width:140px;">
                        <label class="control-label labels">Due Date</label>
                         <input type="text" class="form-control duedate duedatess" name="federalsduedate" placeholder="Mar-15-2020" readonly/>
                    </td>
                    <td></td>
                </tr>
            </table>
            <div class="table-responsive">
            <table class="taxtable table table-bordered" style="margin-top:20px;">
                <thead>
                <tr>
                  <!--<th width="105px" class="text-left">  <label class="form-label">Federal</label></th>-->
                  <th width="235px" class="text-left"><label class="form-label">Federal Form No.</label></th>
                  <th width="125px" class="text-left"><label class="form-label">Filling Method</label></th>
                  <th width="140px" class="text-left"><label class="form-label">Filling Date</label></th>
                  <th width="150px" class="text-left "> <label class="form-label">Status of Filling</label></th>
                  <th width="100px" class="text-left"> <label class="form-label">File</label></th>
                  <th class="text-left"><label class="form-label">Note</label></th>
              </tr>
              </thead>
              <tbody>
             <tr>
                 <!--<td width="105px"></td>-->
                  <td>
                      <input type="text" readonly class="form-control formno" name="federalsform" required>
                 </td>
                  <td width="105px">
                      <select class="form-control filingmethod" name="federalsmethod" required>
                        <option value="">Select</option>
                        <option value="E-File">E-File</option>
                        <option value="Paperfile">Paperfile</option>
                    </select>
                     
                 </td>
                  <td width="140px">
                     <input type="text" class="form-control fdate" id="fillingdate" name="federalsdate" placeholder="MM/DD/YYYY" required/>
                 </td>
                  <td class="">
                     <select class="form-control hidefilling federalsstatus"  name="federalsstatus" required>
                        <option value="">Select</option>
                        <option value="Accepted">Accepted</option>
                        <option value="EF Processing">EF Processing</option>
                        <option value="Pending">Pending</option>
                        <option value="Rejected">Rejected</option>
                    </select>
                 </td>
                  <td style="width:100px">
                      <!--<input type="file" class="form-control" name="federalsfile" style="display:block!important;margin-top:0px;"/>-->
                      <label class="file-upload btn btn-primary">								
                      <input type="file" name="federalsfile"  class="form-control" style="display:none;">Browse for file</label>
                 </td>
                 <td>
                       <textarea class="form-control" name="federalsnote"></textarea>
                 </td>
             </tr>
             </tbody>
         </table>
         </div>
         <div class="table-responsive">
            <table class="taxtable table table-bordered" style="margin-top:20px;" id="taxationtable">
                <thead>
                    <tr>
                        <th width="105px" class="text-left">  <label class="form-label">Resi. State</label></th>
                        <th width="130px" class="text-left"><label class="form-label">Form No.</label></th>
                        <th width="125px" class="text-left "><label class="form-label">Filling Method</label></th>
                        <th width="140px" class="text-left"><label class="form-label">Filling Date</label></th>
                        <th width="150px" class="text-left "> <label class="form-label">Status of Filling</label></th>
                        <th width="105px" class="text-left"> <label class="form-label">File</label></th>
                        <th class="text-left"><label class="form-label">Note</label></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                     <tr id="trblock">
                         <td>
                           <?php if(isset($_REQUEST['id'])&& $_REQUEST['id']!=''): ?>
                             <select class="form-control" style="display:none;"  id="statetaxx2">
                                 
                                 <option>Select</option>
                                 <?php $__currentLoopData = $datastate; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $datastate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                 <option value="<?php echo e($datastate->code); ?>" <?php if($datastate->code ==$common->stateId): ?> selected <?php endif; ?>><?php echo e($datastate->code); ?></option>
                                 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                             </select>
                             
                                  <input class="form-control" name="statetax[]" id="statetax" readonly value="<?php echo e($common->stateId); ?>" >
                               
                            <?php endif; ?>
                         </td>
                          <td>
                              <input type="text" readonly class="form-control" name="stateformno[]" id="statefederalformno"  <?php if($stateform): ?>value="<?php echo e($stateform->taxform); ?>" <?php endif; ?> style="font-size: 14px !important;padding: 0px 1px 0px 4px;"/>
                         </td>
                          <td>
                            <select class="form-control statemethod" name="statemethod[]">
                                <option value="">Select</option>
                                <option value="E-File">E-File</option>
                                <option value="Paperfile">Paperfile</option>
                            </select>
                         </td>
                          <td>
                             <input type="text" class="form-control fdatess" id="fillingdate" name="statedate[]" placeholder="MM/DD/YYYY"/>
                         </td>
                          <td class="">
                             <select class="form-control hidestatefilling statestatus" name="statestatus[]">
                                <option value="">Select</option>
                                <option value="Accepted">Accepted</option>
                                <option value="EF Processing">EF Processing</option>
                                <option value="Pending">Pending</option>
                                <option value="Rejected">Rejected</option>
                            </select>
                         </td>
                          <td style="width:100px">
                              <!--<input type="file" class="form-control" name="statefile[]" style="display:block; margin-top:0px;"/>-->
                              <label class="file-upload btn btn-primary">								
                              <input type="file" name="statefile[]"  class="form-control" style="display:none;">Browse for file</label>
                         </td>
                         <td>
                               <textarea class="form-control" name="statenote[]"></textarea>
                         </td>
                         <td><a href="#" class="btn btn-primary add-rowform"><i class="fa fa-plus"></i></a></td>
                     </tr>
                 </tbody>
         </table>
         </div>
         
                <div class="row form-group saves ">
                <label class="col-md-4 control-label">&nbsp;</label>
                <div class="col-md-2">
                    <input type="submit" class="btn_new_save primary savebutton" value="Save">
                </div>
                 <div class="col-md-2">
                    <a href="#" class="btn_new_cancel" data-dismiss="modal">Cancel</a>
                </div>
               
            </div>
            
            
         
            <input type="hidden" name="client_taxation_id" value="<?php if(isset($common->id)!=''){echo $common->id;}?>">
            <div class="row form-group">
                
                <div class="col-md-4 federals" style="display:none;">
                     
                </div>
                
                <div class="col-md-4 states" style="display:none;">
                     <select class="form-control statesyear" name="statesyear">
                        <option value="">Select</option>
                         <?php
                            $aa=date('Y')-1;
                            
                        ?>
                        <option value="<?php echo $aa;?>"><?php echo $aa;?></option>
                        
                        
                    </select>
                </div>
            </div>
             <div class="row form-group">
                <label class="col-md-3 control-label labels" style="display:none;">Tax Return</label>
                <div class="col-md-4 federals" style="display:none;">
                    
                </div>
                <div class="col-md-4 states" style="display:none;">
                    <select class="form-control taxation2" name="statestax">
                       <option value="">Select</option>
                        <option value="Extension">Extension </option>
                        <option value="Original">Original</option>
                        <!--<option value="Amendment">Amendment</option>!-->
                </select>
                </div>
            </div>
              <div class="row form-group">
                <label class="col-md-3 control-label labels"  style="display:none;">Form No.</label>
                <div class="col-md-4 federals" style="display:none;">
                   
                </div>
              
                <div class="col-md-4 states" style="display:none;">
                    <input type="text" class="form-control formno2" name="statesformno" readonly/>
                </div>
            </div>
              <div class="row form-group">
                <label class="col-md-3 control-label labels" style="display:none;">Due Date</label>
                <div class="col-md-4 federals" style="display:none;">
                   
                </div>
             
                <div class="col-md-4 states" style="display:none;">
                    <input type="text" class="form-control duedate2" name="statesduedate" placeholder="Mar-15-2020" readonly/>
                </div>
            </div>
            <div class="row form-group">
                <label class="col-md-3 control-label labels" style="display:none;">Filing Method </label>
                <div class="col-md-4 federals" style="display:none;">
                   
                </div>
               
                <div class="col-md-4 states" style="display:none;" >
                    <select class="form-control filingmethod2" name="statesmethod">
                        <option value="">Select</option>
                        <option value="E-File">E-File</option>
                        <option value="Paperfile">Paperfile</option>
                    </select>
                </div>
            </div>
            <div class="row form-group">
                <label class="col-md-3 control-label labels" style="display:none;">Filing Date</label>
                <div class="col-md-4 federals" style="display:none;">
                    
                </div>
              
                <div class="col-md-4 states" style="display:none;">
                    <input type="date" class="form-control" id="fillingdate2" name="statesdate"/>
                </div>
            </div>
             <div class="row form-group statusof">
                <label class="col-md-3 control-label labels" style="display:none;">Status of Filling </label>
                <div class="col-md-4 federals" style="display:none;" >
                    
                </div>
               
                <div class="col-md-4 states" style="display:none;">
                    <select class="form-control" name="statesstatus">
                        <option value="">Select</option>
                        <option value="Accepted">Accepted</option>
                        <option value="Rejected">Rejected</option>
                        <option value="Pending">Pending</option>
                    </select>
                </div>
            </div>
             <div class="row form-group">
                <label class="col-md-3 control-label labels" style="display:none;">Add File </label>
                <div class="col-md-4 federals" style="display:none;">
                  
                </div>
               
                <div class="col-md-4 states" style="display:none;">
                   <input type="file" class="form-control" name="statesfile"/>
                </div>
            </div>
              <div class="row form-group">
                <label class="col-md-3 control-label labels" style="display:none;">Note </label>
                <div class="col-md-4 federals" style="display:none;">
                 
                </div>
                
                <div class="col-md-4 states" style="display:none;">
                   <textarea class="form-control" name="statesnote"></textarea>
                </div>
            </div>
            
            <div class="row form-group saves savebutton" style="display:none;">
                <label class="col-md-5 control-label">&nbsp;</label>
                <div class="col-md-2">
                    <input type="submit" class="btn_new_save primary" value="Save">
                </div>
                 <div class="col-md-2">
                    <a href="#" class="btn_new_cancel" data-dismiss="modal">Cancel</a>
                </div>
               
            </div>
        </div>
        </form>
      </div>

    </div>

  </div>
</div>  
 
   
   
   <!--Modal Conversation Update Start-->
    <form method="post" class="form-horizontal" enctype="multipart/form-data" action="<?php echo e(route('customer.updatecoversation')); ?>">
    <?php echo e(csrf_field()); ?>

                                                    
    
        <div id="conversationModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Conversation Sheet</h4>
      </div>
      <div class="modal-body">
          <div style="max-width:700px; margin:0px auto;">
       <div class="row">
           <div class="col-md-4">
               <div class="form-group">
               <div class="row">
                   <label class="control-label col-md-5 text-right"><strong>Date:</strong></label>
                   <div class="col-md-7">
                    <!--<input type="text" class="form-control effective_date2"/>-->
                    <input type="hidden" name="CovID" id="CovID" class="form-control">
                    <input type="text" name="date" id="creattiondate1" class="form-control"  placeholder="Date" readonly>
                   </div>
               </div>
               </div>
           </div>
            <div class="col-md-4">
                <div class="form-group">
                <div class="row">
               <label class="control-label col-md-5 text-right" ><strong>Day:</strong></label>
               <div class="col-md-7">
               <input type="text" name="day" id="day1" class="form-control" placeholder="Day" readonly>
               </div>
               </div>
               </div>
           </div>
            <div class="col-md-4">
                <div class="form-group">
                <div class="row">
                   <label class="control-label col-md-3"><strong>Time:</strong></label>
                   <div class="col-md-8">
                        <input type="text" name="time" id="time1" class="form-control"  placeholder="Time" readonly>
                   </div>
               </div>
               </div>
           </div>
       </div>
       
       <div class="form-group<?php echo e($errors->has('type_user') ? ' has-error' : ''); ?>">
            <div class="row">
            <label class="control-label col-md-3 text-right" style="width:100px;"><strong>Select:</strong></label>
                <div class="col-md-5">
                    <div class="">
                    <select class="form-control fsc-input type_user" name="type_user" id="type_user1" required>
                        <option>Select</option>
                        <option>Client</option>
                        <option>EE-User</option>
                        <option>Vendor</option>
                        <option>Other</option>
                    </select>
                    </div>
                    <?php if($errors->has('type_user')): ?>
                        <span class="help-block">
                        <strong><?php echo e($errors->first('type_user')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>
              
            </div>
       </div>
       
       <div class="form-group clientid2" id="clientid" style="display:none;">
            <div class="row">
            <label class="control-label col-md-3 text-right" style="width:100px;"><strong>Client :</strong></label>
                <div class="col-md-5">
                    <select class="form-control" name="clientid" id="clientid1">
                        <option>Select</option>
                        <?php $__currentLoopData = $listclient; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $client): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($client->id); ?>"><?php echo e($client->first_name); ?> <?php echo e($client->middle_name); ?> <?php echo e($client->last_name); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>      
                    </select>
                </div>
               
            </div>
       </div>
       
       <div class="form-group employeeuserid2" id="employeeuserid" style="display:none;">
            <div class="row">
            <label class="control-label col-md-3 text-right" style="width:100px;"><strong>Employee/User:</strong></label>
                <div class="col-md-5">
                    <select class="form-control" name="employeeuserid" id="employeeuserid1">
                        <option>Select</option>
                        <?php $__currentLoopData = $listemployeeuser; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $employee): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($employee->id); ?>"><?php echo e($employee->firstName); ?> <?php echo e($employee->middleName); ?> <?php echo e($employee->lastName); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>      
                    </select>
                </div>
               
            </div>
       </div>
       
       <div class="form-group vendorid2" id="vendorid" style="display:none;">
            <div class="row">
            <label class="control-label col-md-3 text-right" style="width:100px;"><strong>Vendor:</strong></label>
                <div class="col-md-5">
                    <select class="form-control" name="vendorid" id="vendorid1">
                        <option>Select</option>
                        <?php $__currentLoopData = $listvendoe; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vendoe): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($vendoe->id); ?>"><?php echo e($vendoe->firstName); ?> <?php echo e($vendoe->middleName); ?> <?php echo e($vendoe->lastName); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>      
                    </select>
                </div>
               
            </div>
       </div>
       
       <div class="form-group otherid2" id="otherid" style="display:none;">
           <div class="row">
                <label class="control-label col-md-3" style="width:100px; text-align:right; padding-left:0px!important;"><strong>Other :</strong></label>
                   <div class="col-md-5">
                        <input type="text" class="form-control" name="otherid" id="otherid1">
                   </div>
                   
           </div>
       </div>
       
       <div class="form-group<?php echo e($errors->has('conrelatedname') ? ' has-error' : ''); ?>">
           <div class="row">
                <label class="control-label col-md-3" style="width:100px; text-align:right; padding-left:0px!important;"><strong>Related to:</strong></label>
                   <div class="col-md-5">
                       <div class="">
                        <select class="form-control" name="conrelatedname" id="conrelatedname1" required>
                              <option>Select</option>
                              <?php $__currentLoopData = $relatedNames; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $relatedNames): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value="<?php echo e($relatedNames->id); ?>"><?php echo e($relatedNames->relatednames); ?></option>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                        </div>
                        <?php if($errors->has('conrelatedname')): ?>
                            <span class="help-block">
                            <strong><?php echo e($errors->first('conrelatedname')); ?></strong>
                            </span>
                        <?php endif; ?>
                   </div>
                   
           </div>
       </div>
        

        
       
       
        <div class="form-group">
            <div class="row">
                <label class="control-label col-md-3" style="width:100px; text-align:right; padding-left:0px!important;"><strong>Conversation:</strong></label>
                <div class="col-md-9">
                    <textarea rows="3" cols="12" class="form-control" name="condescription" id="condescription1"> </textarea>
               </div>
            </div>
       </div>
       
        <div class="form-group">
           <div class="row">
                <label class="control-label col-md-3" style="width:100px; text-align:right; padding-left:0px!important;"><strong>Note</strong></label>
                   <div class="col-md-9">
                  <textarea rows="3" cols="12" class="form-control" name="connotes" id="connotes1"> </textarea>
                   </div>
           </div>
       </div>
       
        <div class="form-group">
            <div class="row">
            <label class="control-label col-md-3" style="width:100px; text-align:right; padding-left:0px!important;"><strong>&nbsp;</strong></label>
                <div class="col-md-2">
                    <input class="btn_new_save btn-primary1" id="recInsert" type="submit" name="submit" value="Save">
			    </div>
				<div class="col-md-2">
					<a class="btn_new_cancel" href="https://financialservicecenter.net/fac-Bhavesh-0554">Cancel</a>
               </div>
            </div>
       </div>
       
       
       </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
            </div>
        </div>                                                 
                                                    
    </form>
    <!--Modal Conversation Update End-->

   
   <!--Modal Notes Update Start-->
    <div id="notesModal" class="modal fade" role="dialog">
        <div class="modal-dialog" style="width:850px;">
            <form method="post" class="form-horizontal" enctype="multipart/form-data" action="<?php echo e(route('customer.updatenotes')); ?>">
                <?php echo e(csrf_field()); ?>

                                                    
    
                <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> Note</h4>
      </div>
      <div class="modal-body">
          <div style="max-width:700px; margin:0px auto;">
       <div class="row mt-3" style="margin-top:20px;">
           <div class="col-md-4">
               <div class="form-group">
               <div class="row">
                   <label class="control-label col-md-5 text-right"  style="padding-right:28px!important;" ><strong>Date:</strong></label>
                   <div class="col-md-6" style="padding-left:12px!important;">
                    <!--<input type="text" class="form-control effective_date2"/>-->
                    <input type="hidden" name="NoteID" id="NoteID"> 
                    <input type="text" name="notedate" id="date2" class="form-control" value="<?php echo e(date('m/d/Y')); ?>" placeholder="Date" readonly>
                   </div>
               </div>
               </div>
           </div>
            <div class="col-md-4">
                <div class="form-group">
                <div class="row">
               <label class="control-label col-md-4 text-right" ><strong>Day:</strong></label>
               <div class="col-md-5">
               <input type="text" name="noteday" id="day2" class="form-control" placeholder="Day" value="<?php echo e(date('l')); ?>" readonly>
               </div>
               </div>
               </div>
           </div>
            <div class="col-md-4">
                <div class="form-group">
                <div class="row">
                   <label class="control-label col-md-3"><strong>Time:</strong></label>
                   <div class="col-md-5">
                        <input type="text" name="notetime" id="time2" class="form-control" value="<?php echo e(date("g:i a")); ?>" placeholder="Time" readonly>
                   </div>
               </div>
               </div>
           </div>
       </div>
               
       <div class="form-group">
            <div class="row">
            <label class="control-label col-md-3 text-right" style="width:110px;"><strong>Select:</strong></label>
                <div class="col-md-5">
                    <div class="">
                    <select class="form-control fsc-input notetype_user" name="notetype_user" id="notetype_user1" required>
                        <option>Select</option>
                        <option>Client</option>
                        <option>EE-User</option>
                        <option>Vendor</option>
                        <option>Other</option>
                    </select>
                    </div>
                </div>
              
            </div>
       </div>
       
        <div class="form-group noteclientid2" id="noteclientid" style="display:none;">
            <div class="row">
            <label class="control-label col-md-3 text-right" style="width:100px;"><strong>Client :</strong></label>
                <div class="col-md-5">
                    <select class="form-control" name="noteclientid" id="noteclientid1">
                        <option>Select</option>
                        <?php $__currentLoopData = $listclient; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $client): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($client->id); ?>"><?php echo e($client->first_name); ?> <?php echo e($client->middle_name); ?> <?php echo e($client->last_name); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>      
                    </select>
                </div>
               
            </div>
       </div>
       
       <div class="form-group noteemployeeuserid2" id="noteemployeeuserid" style="display:none;">
            <div class="row">
            <label class="control-label col-md-3 text-right" style="width:100px;"><strong>Employee/User:</strong></label>
                <div class="col-md-5">
                    <select class="form-control" name="noteemployeeuserid" id="noteemployeeuserid1">
                        <option>Select</option>
                        <?php $__currentLoopData = $listemployeeuser; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $employee): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($employee->id); ?>"><?php echo e($employee->firstName); ?> <?php echo e($employee->middleName); ?> <?php echo e($employee->lastName); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>      
                    </select>
                </div>
               
            </div>
       </div>
       
       <div class="form-group notevendorid2" id="notevendorid" style="display:none;">
            <div class="row">
            
            <label class="control-label col-md-3 text-right" style="width:110px;"><strong>Vendor :</strong></label>
                <div class="col-md-5">
                    <select class="form-control" name="notevendorid" id="notevendorid1">
                        <option>Select</option>
                        <?php $__currentLoopData = $listvendoe; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vendoe): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($vendoe->id); ?>"><?php echo e($vendoe->firstName); ?> <?php echo e($vendoe->middleName); ?> <?php echo e($vendoe->lastName); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>      
                    </select>
                </div>
               
            </div>
       </div>
       
       <div class="form-group noteotherid2" id="noteotherid" style="display:none;">
           <div class="row">
                
                <label class="control-label col-md-3 text-right" style="width:110px;"><strong>Other :</strong></label>
                   <div class="col-md-5">
                        <input type="text" class="form-control" name="noteotherid" id="noteotherid1">
                   </div>
                   
           </div>
       </div>
       
       <div class="form-group<?php echo e($errors->has('notesrelated') ? ' has-error' : ''); ?>">
           <div class="row">
                <label class="control-label col-md-3 text-right" style="width:110px;"><strong>Related to :</strong></label>
                   <div class="col-md-5">
                       <div class="">
                        <select class="form-control" name="notenotesrelatedname" id="noterelated1" required>
                              <option>Select</option>
                              <?php $__currentLoopData = $NotesNames; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $notesRelated): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value="<?php echo e($notesRelated->id); ?>"><?php echo e($notesRelated->notesrelated); ?></option>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                        </div>
                        <?php if($errors->has('notesrelated')): ?>
                            <span class="help-block">
                            <strong><?php echo e($errors->first('notesrelated')); ?></strong>
                            </span>
                        <?php endif; ?>
                   </div>
                   <div class="col-md-2">
                       <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#basicExampleModalNotes" class="redius"><i class="fa fa-plus"></i></a>
                   </div>
           </div>
       </div>

       
        <div class="form-group">
            <div class="row">
                <label class="control-label col-md-3" style="width:110px; text-align:right; padding-left:0px!important;"><strong>Types of Note:</strong></label>
                <div class="col-md-5">
                    <select class="form-control" name="notesrelatedcat" id="notesrelatedcat1" required>
                        <option>Permenant Note</option>
                        <option>Temporary  Note</option>
                    </select>
               </div>
            </div>
       </div>
       
        <div class="form-group">
           <div class="row">
                <label class="control-label col-md-3" style="width:110px; text-align:right; padding-left:0px!important;"><strong>Note:</strong></label>
                   <div class="col-md-9" style="padding-right:0px;">
                  <textarea rows="3" cols="12" class="form-control" name="notes" id="notes1"> </textarea>
                   </div>
           </div>
       </div>
       
        <div class="form-group">
            <div class="row">
            <label class="control-label col-md-3" style="width:110px; text-align:right; padding-left:0px!important;"><strong>&nbsp;</strong></label>
                <div class="col-md-2">
                    <input class="btn_new_save btn-primary1" id="recInsert2" type="submit" name="submit" value="Save">
			    </div>
				<div class="col-md-2">
					<a class="btn_new_cancel" href="https://financialservicecenter.net/fac-Bhavesh-0554">Cancel</a>
               </div>
            </div>
       </div>
       
       
       </div>
      </div>
      <div class="modal-footer">
       <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
      </div>
    </div>
                                                    
            </form>
        </div>
    </div>
    <!--Modal Notes Update End-->

    <!--Modal Starttttttttttttttttttttttt-->
    <div id="myModalAddrecord1" class="modal fade" role="dialog">
  <div class="modal-dialog" style="width:1000px;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Client - Income Tax Filing - (Form-1040) <span style="float:right;">Add Record</span></h4>
      </div>
      <div class="modal-body">
          <form class="form-horizontal" method="post" action="<?php echo e(route('workstatus.storefederalss')); ?>" enctype="multipart/form-data">
              <?php echo e(csrf_field()); ?> 
              
              <?php if(!empty($common->id)): ?>
              <input type="hidden" name="client_id" value="<?php echo $common->id;?>">
              <?php endif; ?>
            <div class="recordform">
              
                <table style="width:100%; max-width:620px; margin:0px auto;" class="taxtable">
                <tr>
                    <td>
                        <label class="control-label labels">Filling Year</label>
                            <?php if($Incometaxfederal2 > 0): ?>
                            <select class="form-control federalyear federalyear3" name="federalsyear" required>
                        <?php else: ?>
                            <select class="form-control federalyear" name="federalsyear" required>
                        <?php endif; ?>
                      
                        <option value="">Select</option>
                        
                        
                        <?php
                            
                            $now=date('M-d-Y');
                            if('Jul-25-2021'==$now)
                            {
                                $aa=date('Y')-2;
                                $bb=date('Y')-1;
                                ?>
                                <option value="<?php echo $aa;?>"><?php echo $aa;?></option>
                                <option value="<?php echo $bb;?>"><?php echo $bb;?></option>
                                <?php
                            }
                            else
                            {
                                $aa=date('Y')-1;
                                ?>
                                <option value="<?php echo $aa;?>" <?php if($countext >0) { echo 'selected';}?>><?php echo $aa;?></option>
                                <?php
                            }
                            
                        ?>

                        
                    </select>
                        
                    </td>
                    <td>
                         <label class="control-label labels">Tax Return</label>
                         
                         <select class="form-control taxation" name="federalstax" required>
                            <option value="">Select</option>
                            <option value="Extension">Extension </option>
                            <option value="Original">Original</option>
                            <option value="Amendment">Amendment</option>
                        </select>
                    </td>
                    <td style="width:150px;">
                        <label class="control-label labels">Due Date</label>
                         <input type="text" class="form-control duedate duedatess" name="federalsduedate" placeholder="Mar-15-2020" readonly/>
                    </td>
                </tr>
            </table>
            
                <table class="taxtable table table-bordered" style="margin-top:20px;">
             <tr>
                 <td>
                     <label class="form-label">Federal</label>
                     
                 </td>
                  <td>
                     <label class="form-label">Form No.</label>
                      <input type="text" readonly class="form-control formno" name="federalsform" style="width:100px"/ required>
                     
                 </td>
                  <td>
                     <label class="form-label">Filling Method</label>
                      <select class="form-control filingmethod" name="federalsmethod" required>
                        <option value="">Select</option>
                        <option value="E-File">E-File</option>
                        <option value="Paperfile">Paperfile</option>
                    </select>
                     
                 </td>
                  <td>
                     <label class="form-label">Filling Date</label>
                     <input type="text" class="form-control fdate" id="fillingdate" name="federalsdate" placeholder="MM/DD/YYYY" required/>
                     
                 </td>
                  <td>
                     <label class="form-label">Status of Filling</label>
                     <select class="form-control hidefilling" name="federalsstatus" required>
                        <option value="">Select</option>
                        <option value="Accepted">Accepted</option>
                        <option value="EF Processing">EF Processing</option>
                        <option value="Pending">Pending</option>
                        <option value="Rejected">Rejected</option>
                    </select>
                     
                     
                 </td>
                  <td style="width:100px">
                     <label class="form-label">File</label>
                      <input type="file" class="form-control" name="federalsfile" style="display:block!important;margin-top:0px;"/>
                 </td>
                 <td>
                     <label class="form-label">Note</label>
                       <textarea class="form-control" name="federalsnote"></textarea>
                     
                 </td>
             </tr>
         </table>
         
                <table class="taxtable table table-bordered" style="margin-top:20px;" id="taxationtable">
                <tr>
                  <th>  <label class="form-label">Resi. State</label></th>
                  <th><label class="form-label">Form No.</label></th>
                  <th><label class="form-label">Filling Method</label></th>
                  <th><label class="form-label">Filling Date</label></th>
                  <th> <label class="form-label">Status of Filling</label></th>
                  <th> <label class="form-label">File</label></th>
                  <th><label class="form-label">Note</label></th>
                  <th></th>
              </tr>
              
              
             <tr>
                 <td>
                   <?php if(isset($_REQUEST['id'])&& $_REQUEST['id']!=''): ?>
                     <select class="form-control" name="statetax[]" id="statetax">
                         
                         <option>Select</option>
                         <?php $__currentLoopData = $datastate3; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $datastates): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                         <?php //print_r($datastates);exit;?>
                         <option value="<?php echo e($datastates->state); ?>"><?php echo e($datastates->state); ?></option>
                         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                     </select>
                     
                      
                      <?php endif; ?>
                     
                 </td>
                  <td>
                     
                      <input type="text" readonly class="form-control" name="stateformno[]" id="statefederalformno" style="width:100px"/>
                     
                 </td>
                  <td>
                    
                    <select class="form-control filingmethod" name="statemethod[]">
                        <option value="">Select</option>
                        <option value="E-File">E-File</option>
                        <option value="Paperfile">Paperfile</option>
                    </select>
                     
                 </td>
                  <td>
                     
                     <input type="text" class="form-control fdatess" id="fillingdate" name="statedate[]" placeholder="MM/DD/YYYY"/>
                     
                 </td>
                  <td>
                   
                     <select class="form-control" name="statestatus[]">
                        <option value="">Select</option>
                        <option value="Accepted">Accepted</option>
                        <option value="EF Processing">EF Processing</option>
                        <option value="Pending">Pending</option>
                        <option value="Rejected">Rejected</option>
                    </select>
                     
                     
                 </td>
                  <td style="width:100px">
                    
                      <input type="file" class="form-control" name="statefile[]" style="display:block; margin-top:0px;"/>
                 </td>
                 <td>
                     
                       <textarea class="form-control" name="statenote[]"></textarea>
                     
                 </td>
                 <td><a href="#" class="btn btn-primary add-rowform"><i class="fa fa-plus"></i></a></td>
             </tr>
         </table>
         
                <div class="row form-group saves ">
                <label class="col-md-4 control-label">&nbsp;</label>
                <div class="col-md-2">
                    <input type="submit" class="btn_new_save primary savebutton" value="Save">
                </div>
                 <div class="col-md-2">
                    <a href="#" class="btn_new_cancel" data-dismiss="modal">Cancel</a>
                </div>
               
            </div>
            
            
         
            <input type="hidden" name="client_taxation_id" value="<?php if(isset($common->id)!=''){echo $common->id;}?>">
            <div class="row form-group">
                
                <div class="col-md-4 federals" style="display:none;">
                     
                </div>
                
                <div class="col-md-4 states" style="display:none;">
                     <select class="form-control statesyear" name="statesyear">
                        <option value="">Select</option>
                         <?php
                            $aa=date('Y')-1;
                            
                        ?>
                        <option value="<?php echo $aa;?>"><?php echo $aa;?></option>
                        
                        
                    </select>
                </div>
            </div>
             <div class="row form-group">
                <label class="col-md-3 control-label labels" style="display:none;">Tax Return</label>
                <div class="col-md-4 federals" style="display:none;">
                    
                </div>
                <div class="col-md-4 states" style="display:none;">
                    <select class="form-control taxation2" name="statestax">
                       <option value="">Select</option>
                        <option value="Extension">Extension </option>
                        <option value="Original">Original</option>
                        <option value="Amendment">Amendment</option>
                </select>
                </div>
            </div>
              <div class="row form-group">
                <label class="col-md-3 control-label labels"  style="display:none;">Form No.</label>
                <div class="col-md-4 federals" style="display:none;">
                   
                </div>
              
                <div class="col-md-4 states" style="display:none;">
                    <input type="text" class="form-control formno2" name="statesformno" readonly/>
                </div>
            </div>
              <div class="row form-group">
                <label class="col-md-3 control-label labels" style="display:none;">Due Date</label>
                <div class="col-md-4 federals" style="display:none;">
                   
                </div>
             
                <div class="col-md-4 states" style="display:none;">
                    <input type="text" class="form-control duedate2" name="statesduedate" placeholder="Mar-15-2020" readonly/>
                </div>
            </div>
            <div class="row form-group">
                <label class="col-md-3 control-label labels" style="display:none;">Filing Method </label>
                <div class="col-md-4 federals" style="display:none;">
                   
                </div>
               
                <div class="col-md-4 states" style="display:none;" >
                    <select class="form-control filingmethod2" name="statesmethod">
                        <option value="">Select</option>
                        <option value="E-File">E-File</option>
                        <option value="Paperfile">Paperfile</option>
                    </select>
                </div>
            </div>
            <div class="row form-group">
                <label class="col-md-3 control-label labels" style="display:none;">Filing Date</label>
                <div class="col-md-4 federals" style="display:none;">
                    
                </div>
              
                <div class="col-md-4 states" style="display:none;">
                    <input type="date" class="form-control" id="fillingdate2" name="statesdate"/>
                </div>
            </div>
             <div class="row form-group statusof">
                <label class="col-md-3 control-label labels" style="display:none;">Status of Filling </label>
                <div class="col-md-4 federals" style="display:none;" >
                    
                </div>
               
                <div class="col-md-4 states" style="display:none;">
                    <select class="form-control" name="statesstatus">
                        <option value="">Select</option>
                        <option value="Accepted">Accepted</option>
                        <option value="Rejected">Rejected</option>
                        <option value="Pending">Pending</option>
                    </select>
                </div>
            </div>
             <div class="row form-group">
                <label class="col-md-3 control-label labels" style="display:none;">Add File </label>
                <div class="col-md-4 federals" style="display:none;">
                  
                </div>
               
                <div class="col-md-4 states" style="display:none;">
                   <input type="file" class="form-control" name="statesfile"/>
                </div>
            </div>
              <div class="row form-group">
                <label class="col-md-3 control-label labels" style="display:none;">Note </label>
                <div class="col-md-4 federals" style="display:none;">
                 
                </div>
                
                <div class="col-md-4 states" style="display:none;">
                   <textarea class="form-control" name="statesnote"></textarea>
                </div>
            </div>
            
            <div class="row form-group saves savebutton">
                <label class="col-md-5 control-label">&nbsp;</label>
                <div class="col-md-2">
                    <input type="submit" class="btn_new_save primary" value="Save">
                </div>
                 <div class="col-md-2">
                    <a href="#" class="btn_new_cancel" data-dismiss="modal">Cancel</a>
                </div>
               
            </div>
        </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div> 
    <!--Modal Enddddddddddddddddddddddddddd-->

   
    <!--Modal Federal tax Update Start-->
        <div id="federalModals" class="modal fade" role="dialog">
            <div class="modal-dialog" style="width:1000px;">

                <!-- Modal content-->
                <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Client - Income Tax Filing - (Form-1040) Add Record</h4>
                      </div>
                      <div class="modal-body">
                          <form class="form-horizontal" method="post" action="<?php echo e(route('workrecord.storefederalupdate')); ?>" enctype="multipart/form-data">
                              <?php echo e(csrf_field()); ?> 
                              
                              
                            <input type="hidden" name="federalid" id="federalid">
                              
                            <div class="recordform">
                              
                            <table style="width:100%; max-width:620px; margin:0px auto;" class="taxtable">
                                <tr>
                                    <td>
                                        <label class="control-label labels">Filling Year</label>
                                        <select class="form-control federalyear" name="federalsyear" id="federalsyear">
                                            <option value="">Select</option>
                                            <?php
                                                
                                                $now=date('M-d-Y');
                                                if('Jul-25-2021'==$now)
                                                {
                                                    $aa=date('Y')-2;
                                                    $bb=date('Y')-1;
                                                    ?>
                                                    <option value="<?php echo $aa;?>"><?php echo $aa;?></option>
                                                    <option value="<?php echo $bb;?>"><?php echo $bb;?></option>
                                                    <?php
                                                }
                                                else
                                                {
                                                    $aa=date('Y')-1;
                                                    ?>
                                                    <option value="<?php echo $aa;?>"><?php echo $aa;?></option>
                                                    <?php
                                                }
                                                
                                            ?>
                                        </select>
                                    </td>
                                    <td>
                                         <label class="control-label labels">Tax Return</label>
                                         <select class="form-control taxation" name="federalstax" id="federalstax">
                                            <option value="">Select</option>
                                            <option value="Extension">Extension </option>
                                            <option value="Original">Original</option>
                                            <option value="Amendment">Amendment</option>
                                        </select>
                                    </td>
                                    <td>
                                        <label class="control-label labels">Due Date</label>
                                         <input type="text" class="form-control duedate" name="federalsduedate" id="federalsduedate" placeholder="Mar-15-2020" readonly/>
                                    </td>
                                </tr>
                            </table>
                         
                            <table class="taxtable table table-bordered" style="margin-top:20px;">
                             <tr>
                                 <td>
                                     <label class="form-label">Federal</label>
                                     
                                 </td>
                                  <td>
                                     <label class="form-label">Form No.</label>
                                      <input type="text" readonly class="form-control formno" name="federalsform" id="federalsform" style="width:100px"/>
                                     
                                 </td>
                                  <td>
                                     <label class="form-label">Filling Method</label>
                                      <select class="form-control filingmethod" name="federalsmethod" id="federalsmethod">
                                        <option value="">Select</option>
                                        <option value="E-File">E-File</option>
                                        <option value="Paperfile">Paperfile</option>
                                    </select>
                                     
                                 </td>
                                  <td>
                                     <label class="form-label">Filling Date</label>
                                     <input type="date" class="form-control fillingdate" id="fillingdate" name="federalsdate"/>
                                     
                                 </td>
                                  <td>
                                     <label class="form-label">Status of Filling</label>
                                     <select class="form-control hidefilling" name="federalsstatus" id="federalsstatus">
                                        <option value="">Select</option>
                                        <option value="Accepted">Accepted</option>
                                        <option value="Rejected">Rejected</option>
                                        <option value="Pending">Pending</option>
                                    </select>
                                     
                                     
                                 </td>
                                 
                                 <td style="width:100px">
                                    <label class="form-label">File</label>
                                    <input type="file" class="form-control" name="federalsfile" style="display:none!important;margin-top:0px;"/>
                                        Browse for file ... </label>
                                    <input type="hidden" class="form-control" name="federalsfile" id="federalsfile"><span id="federalsfile_1"></span>
                                 </td>
                                 <td>
                                     <label class="form-label">Note</label>
                                       <textarea class="form-control" name="federalsnote" id="federalsnote"></textarea>
                                     
                                 </td>
                             </tr>
                         </table>
                            
                            
                            <div class="row form-group saves">
                                <label class="col-md-4 control-label">&nbsp;</label>
                                <div class="col-md-2">
                                    <input type="submit" class="btn_new_save primary" value="Save">
                                </div>
                                 <div class="col-md-2">
                                    <a href="#" class="btn_new_cancel" data-dismiss="modal">Cancel</a>
                                </div>
                               
                            </div>
                            
                        </div>
                        </form>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      </div>
                </div>

            </div>
        </div>  
    <!--Modal Federal tax Update End-->

    <!--Modal State tax Update Start-->
        <div id="stateModals" class="modal fade" role="dialog">
            <div class="modal-dialog" style="width:1000px;">

                <!-- Modal content-->
                <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Client - Income Tax Filing - (Form-1040) Add Record</h4>
                      </div>
                      <div class="modal-body">
                          <form class="form-horizontal" method="post" action="<?php echo e(route('workrecord.stateupdate')); ?>" enctype="multipart/form-data">
                              <?php echo e(csrf_field()); ?> 
                              
                              
                            <input type="hidden" name="stateid" id="stateid">
                              
                            <div class="recordform">
                            
                            <table class="taxtable table table-bordered" style="margin-top:20px;" id="taxationtable">
                                <tr>
                                      <th><label class="form-label">State</label></th>
                                      <th><label class="form-label">Form No.</label></th>
                                      <th><label class="form-label">Filling Method</label></th>
                                      <th><label class="form-label">Filling Date</label></th>
                                      <th> <label class="form-label">Status of Filling</label></th>
                                      <th> <label class="form-label">File</label></th>
                                      <th><label class="form-label">Note</label></th>
                                      
                                </tr>
              
                                <tr>
                                     <td>
                                       <?php if(isset($_REQUEST['id'])&& $_REQUEST['id']!=''): ?>
                                         <select class="form-control statetax" name="statetax" id="statetaxstate">
                                             <option>Select</option>
                                             <?php $__currentLoopData = $datastate3; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $datastate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                             <option value="<?php echo e($datastate->state); ?>"><?php echo e($datastate->state); ?></option>
                                             <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                         </select>
                                         
                                          
                                          <?php endif; ?>
                                         
                                     </td>
                                      <td>
                                         
                                          <input type="text" readonly class="form-control stateformno" name="stateformno" id="statefederalformnostate" style="width:100px"/>
                                         
                                     </td>
                                      <td>
                                        
                                        <select class="form-control filingmethod" name="statemethod" id="statemethod">
                                            <option value="">Select</option>
                                            <option value="E-File">E-File</option>
                                            <option value="Paperfile">Paperfile</option>
                                        </select>
                                         
                                     </td>
                                      <td>
                                         
                                         <input type="date" class="form-control statedate" id="fillingdate" name="statedate"/>
                                         
                                     </td>
                                      <td>
                                       
                                         <select class="form-control" name="statestatus" id="statestatus">
                                            <option value="">Select</option>
                                            <option value="Accepted">Accepted</option>
                                            <option value="Rejected">Rejected</option>
                                            <option value="Pending">Pending</option>
                                        </select>
                                         
                                         
                                     </td>
                                      <td style="width:100px">
                                        
                                          <input type="file" class="form-control" name="statefile"  style="display:none; margin-top:0px;"/>
                                          Browse for file ... </label>
                                          <input type="hidden" class="form-control" name="statefile_1" id="statefile"><span id="statefile_1"></span>
                                     </td>
                                     <td>
                                         
                                           <textarea class="form-control" name="statenote" id="statenote"></textarea>
                                         
                                     </td>
                                     
                                 </tr>
                            </table>
                         
                            <div class="row form-group saves">
                                <label class="col-md-4 control-label">&nbsp;</label>
                                <div class="col-md-2">
                                    <input type="submit" class="btn_new_save primary" value="Save">
                                </div>
                                 <div class="col-md-2">
                                    <a href="#" class="btn_new_cancel" data-dismiss="modal">Cancel</a>
                                </div>
                               
                            </div>
                            
                            
                         
                            
                        </div>
                        </form>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      </div>
                </div>

            </div>
        </div>  
    <!--Modal State tax Update End-->
   
   
   <script>
  
   
    $(document).ready(function()
    {
         $('.fdate').blur(function()
        {
            var taxtion=$('.taxation').val();
            if(taxtion !='Extension')
            {
            var thisdate=$(this).val();
            $('.fdatess').val(thisdate);
            }
        });
       
         $(".fdates").blur(function(){
           //  alert('11');
          var fdatef=$('.fdatef').val();
             
          var thiss=$(this).val();
         // alert(thiss);
          if(thiss)
          {
              
          }
          else
          {
              $('.fdates').val(fdatef);
          }
             
         });

        $(".income_number").on("input", function(evt) 
        {
    		var self = $(this);
    		self.val(self.val().replace(/[^\d].+/, ""));
    		if ((evt.which < 48 || evt.which > 57)) 
    		{
    			evt.preventDefault();
    		}
        });
        
        $('.wagestotal').blur(function () {
        var sum = 0;
        $('.wagestotal').each(function() {
            sum += Number($(this).val());
        });
        $('.totalamts').val(sum);
        });
        
        
        $(".incomeDetail").click(function ()
        {
            var incomeid = $(this).attr('data-id');
            $("#incomeid").val(incomeid);
            $('#myModalIncomeUpdate').modal('show');
            $.get('<?php echo URL::to('getIncomedata'); ?>?incomeid='+incomeid, function(data)
            {  
                //console.log(data);exit;
                if(data == "")
                {
                    
                }
                else
                {
                    $('#wrkyear').val(data.wrkyear);
                    $('#wagetotal').val(data.wagetotal);
                    $('#taxintrest').val(data.taxintrest);
                    $('#qulifieddividends').val(data.qulifieddividends);
                    $('#pension').val(data.pension);
                    $('#socialsecurity').val(data.socialsecurity);
                    $('#capital').val(data.capital);
                    $('#otherincome').val(data.otherincome);
                    $('#totalamount').val(data.totalamount);
                }
                

                
            });
        });
        
        
        $('#statetax').on('change', function()
        {
               var fdatef=$('.fdatef').val();
          // alert(fdatef);
            $('.fdate').val(fdatef);
         
        
            var statetaxval=$("#statetax").val();
            $.get('<?php echo URL::to('getTaxstatedata'); ?>?statetaxval='+statetaxval,function(data)
            {  
                //console.log(data);exit;
                if(data=='')
                {
                   $("#statefederalformno").val(); 
                }
                else
                {
                    $("#statefederalformno").val(data.taxform);
                }
                
            });
   
        });
   
   
        $('#statetaxstate').on('change', function()
        {
            var statetaxval=$("#statetaxstate").val();
            $.get('<?php echo URL::to('getTaxstatedata'); ?>?statetaxval='+statetaxval,function(data)
            {  
                //console.log(data);exit;
                if(data=='')
                {
                   $("#statefederalformnostate").val(); 
                }
                else
                {
                    $("#statefederalformnostate").val(data.taxform);
                }
                
            });
   
        });
        
        
            var cnt=1;
            $(".add-rowform").click(function()
            {
                cnt++;
                 var aa=$('#statetaxx2').html();
               
                //$('').show();
                //var aa=$('.statetax2').html();
                var markup = ' <tr><td><select class="form-control statetax'+cnt+'" name="statetax[]" id="statetax'+cnt+'"><option>Select</option> <?php $__currentLoopData = $datastate2; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $datastate3): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?><option value="<?php echo e($datastate3->code); ?>"><?php echo e($datastate3->code); ?></option><?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></select></td><td><input type="text" readonly class="form-control formno" name="stateformno[]" id="statefederalformno'+cnt+'" style="width:100px"/></td><td><select class="form-control filingmethod'+cnt+'" name="statemethod[]"><option value="">Select</option><option value="E-File">E-File</option><option value="Paperfile">Paperfile</option></select></td><td><input type="text" class="form-control fdate'+cnt+'" id="fillingdate" name="statedate[]" id="federaldate'+cnt+'" placeholder="MM/DD/YYYY"/></td><td><select class="form-control" name="statestatus[]" id="federalstatus'+cnt+'"><option value="">Select</option> <option value="Accepted">Accepted</option><option value="EF Processing">EF Processing</option><option value="Pending">Pending</option><option value="Rejected">Rejected</option></select></td> <td><textarea class="form-control" name="statenote[]" id="federalnote'+cnt+'"></textarea></td><td><a href="#" class="btn btn-danger removetrrow"><i class="fa fa-minus"></i></a></td></tr>';
                $("#taxationtable tbody").append(markup);
             var feddtax='<?php echo $clientfedext;?>';
    
              var feddyear=$('.federalyear').val();
       // alert(feddyear);
       
        var ContractEndYear = parseFloat(feddyear) + parseFloat(1);
       // alert(ContractEndYear);
       if(feddtax =='1')
       {
           
    //var newdate = ContractEndYear+"-"+9+"-"+15;
    var ttt=new Date('2025-12-31');
    
       }
       else
       {
          var newdate = ContractEndYear+"-"+7+"-"+15;
          var ttt=new Date(newdate);
     
       }


                 $( function()
                 {
                    $('.fdate'+cnt).datepicker({
                        autoclose: true,endDate:ttt
                    });
                    
                });             
        $('.filingmethod'+cnt).on('change', function()
                {
                    var thiss=$(this).val();
                    //alert(thiss);
                      if(thiss == 'E-File')
                    {
                         $('#federalstatus'+cnt).show();
                    
                      $('#federalstatus'+cnt+' option[value="EF Processing"]').attr("selected","selected");
                    }
                      else if(thiss == 'Paperfile')
                      
                      {
                          
                           $('#federalstatus'+cnt).hide();
                        
                     }
                  else
                  {
                      
                           $('#federalstatus'+cnt+' option[value="EF Processing"]').removeAttr("selected");
                 
                      $('#federalstatus'+cnt).show();
                     }
        
                });
                
                $('.statetax'+cnt).on('change', function()
                {
                        var statetaxval=$('.statetax'+cnt).val();
                        $.get('<?php echo URL::to('getTaxstatedata'); ?>?statetaxval='+statetaxval,function(data)
                        {  
                            if(data=='')
                            {
                               $('#statefederalformno'+cnt).val(); 
                            }
                            else
                            {
                                $('#statefederalformno'+cnt).val(data.taxform);
                            }
                            
                        });
               });
           
            });
            $("body").on("click",".removetrrow",function(){ 
                $(this).parent().parent().remove();
            });
        
        
        $(".stateTaxation").click(function ()
        {
            var stateid = $(this).attr('data-id');
            $("#stateid").val(stateid);
            $('#stateModals').modal('show');
            $.get('<?php echo URL::to('getClientstatedata'); ?>?stateid='+stateid, function(data)
            {  
                //console.log(data);exit;
                if(data == "")
                {
                    
                }
                else
                {
                    $('#stateyear').val(data.stateyear);
                    $('.statetax').val(data.statetax);
                    $('#federalsduedate').val(data.federalsduedate);
                    //$('#federalsduedate').val(date('d-m-Y',strtotime(data.federalsduedate)));
                    $('.stateformno').val(data.stateformno);
                    $('#statemethod').val(data.statemethod);
                    $('.statedate').val(data.statedate);
                    $('#statestatus').val(data.statestatus);
                    $('#statefile').val(data.statefile);
                    $('#statefile_1').html(data.statefile);
                    $('#statenote').val(data.statenote);
                    
                }
                

                
            });
        });
        
        
        $(".federalTaxation").click(function ()
        {
            var federalid = $(this).attr('data-id');
            $("#federalid").val(federalid);
            $('#federalModals').modal('show');
            $.get('<?php echo URL::to('getClientfederaldata'); ?>?federalid='+federalid, function(data)
            {  
                //console.log(data);exit;
                if(data == "")
                {
                    
                }
                else
                {

                    //$('#id').val(data.id);
                    $('#federalsyear').val(data.federalsyear);
                    $('#federalstax').val(data.federalstax);
                    $('#federalsduedate').val(data.federalsduedate);
                    //$('#federalsduedate').val(date('d-m-Y',strtotime(data.federalsduedate)));
                    $('#federalsform').val(data.federalsform);
                    $('#federalsmethod').val(data.federalsmethod);
                    $('.fillingdate').val(data.federalsdate);
                    $('#federalsstatus').val(data.federalsstatus);
                    $('#federalsfile').val(data.federalsfile);
                    $('#federalsfile_1').html(data.federalsfile);
                    $('#federalsnote').val(data.federalsnote);
                    
                }
                

                
            });
        });
    
        
        $('.type_user').on('change', function()
        {   
             //otherid,clientid,employeeuserid,vendorid
             //Client,EE-User,Vendor,Other
             var thisss=$('.type_user').val();
             if(thisss =='Client')
             {
                 $('#clientid').show();
                 $('#employeeuserid').hide();
                 $('#vendorid').hide();
                 $('#otherid').hide();
             }
             else if(thisss =='EE-User')
             {
                 $('#clientid').hide();
                 $('#employeeuserid').show();
                 $('#vendorid').hide();
                 $('#otherid').hide();
             }
             else if(thisss =='Vendor')
             {
                 $('#clientid').hide();
                 $('#employeeuserid').hide();
                 $('#vendorid').show();
                 $('#otherid').hide();
             }
             else if(thisss =='Other')
             {
                 $('#clientid').hide();
                 $('#employeeuserid').hide();
                 $('#vendorid').hide();
                 $('#otherid').show();
             }
        });
        
        $('.notetype_user').on('change', function()
        {   
             //otherid,clientid,employeeuserid,vendorid
             //Client,EE-User,Vendor,Other
             var thisss=$('.notetype_user').val();
             if(thisss =='Client')
             {
                 $('#noteclientid').show();
                 $('#noteemployeeuserid').hide();
                 $('#notevendorid').hide();
                 $('#noteotherid').hide();
             }
             else if(thisss =='EE-User')
             {
                 $('#noteclientid').hide();
                 $('#noteemployeeuserid').show();
                 $('#notevendorid').hide();
                 $('#noteotherid').hide();
             }
             else if(thisss =='Vendor')
             {
                 $('#noteclientid').hide();
                 $('#noteemployeeuserid').hide();
                 $('#notevendorid').show();
                 $('#noteotherid').hide();
             }
             else if(thisss =='Other')
             {
                 $('#noteclientid').hide();
                 $('#noteemployeeuserid').hide();
                 $('#notevendorid').hide();
                 $('#noteotherid').show();
             }
        });
    });
</script>
<script type="text/javascript">
    $(".notesID").click(function () 
        {
            var ids = $(this).attr('data-id');
            //alert(ids);
            $("#notesid").val(ids);
            $('#notesModal').modal('show');
            //console.log(ids);           
            $.get('<?php echo URL::to('getNotesemp'); ?>?documentid='+ids, function(data)
            {  
                //console.log(data);exit;
                if(data == "")
                {
                    
                }
                else
                {
                    //console.log(data.id);exit;
                    $('#NoteID').val(data.id);
                    $('#creattiondate1').val(data.creattiondate);
                    $('#noteday1').val(data.noteday);
                    $('#notetime1').val(data.notetime);
                    $('#notetype_user1').val(data.notetype_user);
                    $('#noteclientid1').val(data.noteclientid);
                    $('#noteemployeeuserid1').val(data.noteemployeeuserid);
                    $('#notevendorid1').val(data.notevendorid);
                    $('#noteotherid1').val(data.noteotherid);
                    $('#noterelated1').val(data.noterelated);
                    $('#notesrelatedcat1').val(data.notesrelatedcat);
                    $('#notes1').val(data.notes);
                    
                    
                       
                                 var thisss=$('#notetype_user1').val();
                                 if(thisss =='Client')
                                 {
                                     $('.noteclientid2').show();
                                     $('.noteemployeeuserid2').hide();
                                     $('.notevendorid2').hide();
                                     $('.noteotherid2').hide();
                                 }
                                 else if(thisss =='EE-User')
                                 {
                                     $('.noteclientid2').hide();
                                     $('.noteemployeeuserid2').show();
                                     $('.notevendorid2').hide();
                                     $('.noteotherid2').hide();
                                 }
                                 else if(thisss =='Vendor')
                                 {
                                     $('.noteclientid2').hide();
                                     $('.noteemployeeuserid2').hide();
                                     $('.notevendorid2').show();
                                     $('.noteotherid2').hide();
                                 }
                                 else if(thisss =='Other')
                                 {
                                     $('.noteclientid2').hide();
                                     $('.noteemployeeuserid2').hide();
                                     $('.notevendorid2').hide();
                                     $('.noteotherid2').show();
                                 }
                    
                }
                

                
            });
        });

    $(".conversationID").click(function () 
        {
            var ids = $(this).attr('data-id');
            //alert(ids);
            $("#conversationid").val(ids);
            $('#conversationModal').modal('show');
            //console.log(ids);           
            $.get('<?php echo URL::to('getConversationdata'); ?>?documentid='+ids, function(data)
            {  
                //console.log(data);exit;
                if(data == "")
                {
                    
                }
                else
                {
                    //console.log(data.id);exit;
                    $('#CovID').val(data.id);
                    $('#creattiondate1').val(data.creattiondate);
                    $('#day1').val(data.day);
                    $('#time1').val(data.time);
                    $('#type_user1').val(data.type_user);
                    $('#clientid1').val(data.clientid);
                    $('#employeeuserid1').val(data.employeeuserid);
                    $('#vendorid1').val(data.vendorid);
                    $('#otherid1').val(data.otherid);
                    $('#conrelatedname1').val(data.conrelatedname);
                    $('#condescription1').val(data.condescription);
                    $('#connotes1').val(data.connotes);
                    
                    
                       
                                 var thisss=$('#type_user1').val();
                                 if(thisss =='Client')
                                 {
                                     $('.clientid2').show();
                                     $('.employeeuserid2').hide();
                                     $('.vendorid2').hide();
                                     $('.otherid2').hide();
                                 }
                                 else if(thisss =='EE-User')
                                 {
                                     $('.clientid2').hide();
                                     $('.employeeuserid2').show();
                                     $('.vendorid2').hide();
                                     $('.otherid2').hide();
                                 }
                                 else if(thisss =='Vendor')
                                 {
                                     $('.clientid2').hide();
                                     $('.employeeuserid2').hide();
                                     $('.vendorid2').show();
                                     $('.otherid2').hide();
                                 }
                                 else if(thisss =='Other')
                                 {
                                     $('.clientid2').hide();
                                     $('.employeeuserid2').hide();
                                     $('.vendorid2').hide();
                                     $('.otherid2').show();
                                 }
                    
                }
                

                
            });
        });

        
        
           $('.hidepaid2').hide();
                    
        $(".formationID").click(function () 
        {
            var ids = $(this).attr('data-id');
            //alert(ids);
            $("#formationid").val(ids);
            $('#formationModal').modal('show');
            //console.log(ids);           
            $.get('<?php echo URL::to('getFormationdata'); ?>?documentid='+ids, function(data)
            {  
                //console.log(data);exit;
                if(data == "")
                {
                    
                }
                else
                {
                    //console.log(data.id);exit;
                    var famt='$'+ data.formation_amount;
                    $('#id').val(data.id);
                    $('#client_id77').val(data.client_id);
                    $('#formation_yearbox77').val(data.formation_yearbox);
                    $('#formation_yearvalue77').val(data.formation_yearvalue);
                    $('#formation_amount77').val(famt);
                    $('#formation_payment77').val(data.formation_payment);
                    $('#record_status77').val(data.record_status);
                    $('#annualreceipt77').val(data.annualreceipt);
                    $('#annualreceipt2_77').html(data.annualreceipt);
                    $('#formation_work_officer77').val(data.formation_work_officer);
                    $('#formation_work_officer2_77').html(data.formation_work_officer);
                    
                    $('.record_paid_by88').val(data.record_paid_by);
                    $('#record_paid_by77').val(data.record_paid_by);
                    $('#formation_penalty77').val('$'+data.formation_penalty);
                    $('#work_processingfees77').val('$'+data.work_processingfees);
                    $('#record_totalamt77').val('$'+data.record_totalamt);
                    $('#paiddate77').val(data.paiddate);
                    $('#record_note77').val(data.record_note);
                    
                    var paidby2=$('.record_paid_by88').val();
                    //alert(paidby2);
                    if(paidby2 =='Client')
                    {
                         $('.hidepaid2').hide();
                    }
                    else if(paidby2 == 'FSC')
                    {
                         $('.hidepaid2').show();
                    }
                    else
                    {
                         $('.hidepaid2').hide();
                    }
                }
                

                
            });
        });
   
   
        $(".passingID").click(function () 
        {
            var ids = $(this).attr('data-id');
            //alert(ids);
            $("#idkl").val(ids);
            $('#myModal').modal('show');
            //console.log(ids);           
            $.get('<?php echo URL::to('getClientDocumentdata'); ?>?documentid='+ids, function(data)
            {  
                console.log(data);
                if(data == "")
                {
                    
                }
                else
                {
                    //$('#county').html(data);
                    //console.log(data.id);exit;
                    $('#id').val(data.id);
                    $('#filename_idss').val(data.filename_id);
                    $('#client_id').val(data.client_id);
                    $('#documentsname').val(data.documentsname);
                    $('#clientdocument').val(data.clientdocument);
                    $('#clientdocument22').html(data.clientdocument);
                }
                

                
            });
        });
   </script>


<script>

    function federalclick()
    { 
        var checkBox = document.getElementById("federal");
        if (checkBox.checked == true){
        $('.labels').show();
        $('.saves').show();
        $('.federals').show();
        
       }
       else
       {
           
            $('.federals').hide();
          
       }
    }
   
   
    function stateclick()
    { 
        var checkBox1 = document.getElementById("states");
        if (checkBox1.checked == true)
        {
            $('.labels').show();
            $('.saves').show();
            $('.states').show();
        }
        else
        {
   
            $('.states').hide();
        }
   }
    
    
        
    
 $(document).ready(function()
    {
        
        $('.federalyear3').change(function()
        {
            var this1=$(this).val();
            //alert();
            var countrxt='<?php echo $countext;?>';
            alert(countrxt);
            var aa='<?php echo $aa=date('Y')-1;?>';
            if(this1 == aa && countrxt !='1')
            {
                $(".savebutton").hide();
                alert('This year already exists!');
            }
            else
            {
                $(".savebutton").show();
            }
        });
        
        $('.federalyear').change(function()
        {
             var thiss=$(this).val();
            // alert(thiss);
             var taxserviceyear='<?php if(isset($clienttax->tax_service_year)) { echo $clienttax->tax_service_year;}?>';
             if(thiss =='')
             {
                 $('.btn_new_save ').hide();
             }
             else if(thiss == taxserviceyear)
             {
                 $('.btn_new_save ').hide();
             }
             else
             {
                 
                     $('.btn_new_save ').show();
             
             }
             
              var feddtax='<?php echo $clientfedext;?>';
       //alert(feddtax);
        var ContractEndYear = parseFloat(thiss) + parseFloat(1);
       // alert(ContractEndYear);
       if(feddtax =='1')
       {
        //  alert(feddtax); 
    //var newdate = ContractEndYear+"-"+9+"-"+15;
    var ttt1=new Date('2025-12-31');
    
       }
       else
       {
          // alert(feddtax);
          var newdate = ContractEndYear+"-"+7+"-"+15;
          var ttt1=new Date(newdate);
     
       }

     $(".fdate").datepicker({autoclose: true,endDate:ttt1});
     $(".fdatess").datepicker({autoclose: true,endDate:ttt1});
     
     
     
      

             var this1=$(this).val();
             var this2=parseFloat(this1) + parseFloat(1);
             var aa='<?php echo $aa=date('Y');?>';
             //if(this1 =='2019')
             //{
               //  $(".duedate").val('Jul-15-'+this2);
                 
             //}
             //else if(this1 =='' )
            // {
              //   $(".duedate").val(' ');
                 
             //}
             //else
             //{
               //  $(".duedate").val('Apr-15-'+this2);
                 
        //     }
            
            
            
            //  if(this1 == 'Extension')
            //  {
            //      $("select option[value='Extension']").hide();
            //      $(".duedate").val('Mar-15-<?php echo $aa+1;?>');
                 
            //  }
            // else if(this1 == bb)
            //  {
            //      $("select option[value='Extension']").hide();
            //      $(".duedate").val('Mar-15-<?php echo $aa+1;?>');
            //  }
            //  else if(this1 == cc)
            //  {
            //      $("select option[value='Extension']").show();
            //      $(".duedate").val('Mar-15-<?php echo $aa+1;?>');
            //  }
      
        });
      
        $('.statesyear').change(function()
        {
             
             var this1=$(this).val();
             //alert(this1);
             var aa='<?php echo $aa=date('Y');?>';
             var bb='<?php echo $bb=date('Y')+1;?>';
             var cc='<?php echo $cc=date('Y')+2;?>';
             if(this1 == aa)
             {
                 $("select option[value='Extension']").hide();
                 $(".duedate2").val('Jul-15-<?php echo $aa+1;?>');
                 
             }
            else if(this1 == bb)
             {
                 $("select option[value='Extension']").hide();
                 $(".duedate2").val('Jul-15-<?php echo $bb+1;?>');
             }
             else if(this1 == cc)
             {
                 $("select option[value='Extension']").show();
                 $(".duedate2").val('Jul-15-<?php echo $cc+1;?>');
             }
      
        });
      
      
        $('.taxation').change(function()
        {
              var businessid='<?php if(isset($_REQUEST['id']) && $_REQUEST['id'] !='') { echo $common->business_id; }?>';
           // alert('11');
            var fed='<?php echo $clientfedext;?>';
             
          var thiss=$(this).val();
          //alert(thiss);
          var this1=$('.federalyear').val();
          var this21=parseFloat(this1) + parseFloat(1);
           // alert(this21);
          if(thiss == 'Extension')
          {
               $('#statefederalformno').val('Same as a Fed.');
               
                $('#trblock').css("pointer-events", 'none');
                $('.statemethod').attr("disabled","disabled");
                $('.fdatess').attr("disabled","disabled");
                $('.statestatus').attr("disabled","disabled");
              
              if(businessid =='6')
              {
                  $('.formno').val('Form-4868');
              }
              else
              {
              $('.formno').val('Form-7004');    
              }
              
              if(this1 =='2019')
             {
                  
                // alert(this1);
              
                 $(".duedatess").val('Jul-15-'+this21);
              }
             else if(this1 =='' )
             {
                //  var this2=parseFloat(this1) + parseFloat(1);
            
                 $(".duedatess").val(' ');
                 //$('#statefederalformno').val('');
                 
             }
              
              $("select option[value='E-File']").show();
          }
          
          else if(thiss == 'Original')
          {
            // alert(this2);
              var ii='<?php if(!empty($stateform)) { echo $stateform->taxform;}?>';
              var stateform=(ii);
              // $('.formno').val(stateform);
               if(this1 =='2019')
             {
                 if(fed =='1')
                 {
                    $(".duedatess").val('Sep-15-'+this21);
                  
                 }
                 else
                 {
                 $(".duedatess").val('Jul-15-'+this21);
                 }
             }
               
               
                $('#trblock').css("pointer-events", 'auto');
              $('.statemethod').removeAttr('disabled');
             $('.fdatess').removeAttr('disabled');
             $('.statestatus').removeAttr('disabled');
              $('.formno').val('<?php if(isset($common->type_form)) { echo $common->type_form;}?>');
               
             $('#statefederalformno').val('<?php if(isset($common->type_form_file2)) { echo $common->type_form_file2;}?>');
            
            //  $('.formno').val('Form-1120S');
              $("select option[value='E-File']").show();
          }
          else if(thiss == 'Amendment')
          {
              var ii='<?php if(!empty($stateform)) { echo $stateform->taxform;}?>';
              var stateform=(ii);
               $('#statefederalformno').val(stateform);
               
                $('#trblock').css("pointer-events", 'auto');
              $('.statemethod').removeAttr('disabled');
             $('.fdatess').removeAttr('disabled');
             $('.statestatus').removeAttr('disabled');
              $('.formno').val('Form-1040X');
              $("select option[value='E-File']").hide();
                     
          }
          else
          {
              var ii='<?php if(!empty($stateform)) { echo $stateform->taxform;}?>';
              var stateform=(ii);
               $('#statefederalformno').val(stateform);
              
                $('#trblock').css("pointer-events", 'auto');
                $('.statemethod').removeAttr('disabled');
             $('.fdatess').removeAttr('disabled');
             $('.statestatus').removeAttr('disabled');
             $('.statemethod').removeAttr('disabled');
             $('.fdatess').removeAttr('disabled');
             $('.statestatus').removeAttr('disabled');
          }
        });
        
        $('.taxation2').change(function()
        {
            var businessid='<?php if(isset($_REQUEST['id']) && $_REQUEST['id'] !='') { echo $common->business_id; }?>';
            var thiss=$(this).val();
          if(thiss == 'Extension')
          {
               if(businessid =='6')
              {
                  $('.formno').val('Form-4868');
              }
              else
              {
              $('.formno').val('Form-7004');    
              }
              $("select option[value='E-File']").show();
          }
          
          else if(thiss == 'Original')
          {
              $('.formno2').val('Form-1120S');
              $("select option[value='E-File']").show();
          }
          else if(thiss == 'Amendment')
          {
              $('.formno2').val('Form-1040X');
              $("select option[value='E-File']").hide();
                     
          }
        });
        
    
           $('.filingmethod').change(function()
        {
          var thiss=$(this).val();
          if(thiss == 'E-File')
          {
                 $('.hidefilling').css("visibility","visible");
              $('.statusof').css("visibility","visible");
         
              $('.federalsstatus option[value="EF Processing"]').attr("selected","selected");
          }
          else if(thiss == 'Paperfile')
          
          {
                     $('.statusof').css("visibility","hidden");
              
               $('.hidefilling').css("visibility","hidden");
            
         }
          else
          {
              
                   $('.federalsstatus option[value="EF Processing"]').removeAttr("selected");
         
              $('.hidefilling').show();
              $('.statusof').show();
          }
        });
        
            $('.statemethod').change(function()
        {
          var thiss=$(this).val();
          if(thiss == 'E-File')
          {
                 $('.hidestatefilling').css("visibility","visible");
          
              $('.statestatus option[value="EF Processing"]').attr("selected","selected");
          }
          else if(thiss == 'Paperfile')
          
          {
              
               $('.hidestatefilling').css("visibility","hidden");
            
         }
          else
          {
              
                   $('.statestatus option[value="EF Processing"]').removeAttr("selected");
         
              $('.hidestatefilling').show();
          }
        });
        
    
        
     
        $('.filingmethod2').change(function()
        {
          var thiss=$(this).val();
          if(thiss == 'Paperfile')
          {
              $('.statusof2').hide();
          }
          
          else
          {
              $('.statusof2').show();
          }
        });
      
    });
</script>
   

<script type="text/javascript">

function getvalYear11()
    {
         var selecttear=$('.selectyear').val(); //2020
          var yearbox=$('#formation_yearbox11').val(); // 1
       
       if(yearbox =='1')
       {
           var tyear=parseFloat(selecttear) + parseFloat(yearbox);
        //   alert(tyear);
       }
       else if(yearbox =='2')
       {
              var tyear=parseFloat(selecttear) + parseFloat(yearbox);
         // alert(tyear);
     
       }
       else if(yearbox =='3')
       {
              var tyear=parseFloat(selecttear) + parseFloat(yearbox);
        
       }
     
    var text="";
 //   var i=;
    for (var i = selecttear; i < tyear; i++) {
  console.log(i);
  text += i +",";
 // alert(i);
 //alert(i);


  
}
var str1=text.replace(/,\s*$/, "");
$('.showw').val(str1);
//var tt=text.join(",");
//alert(text);
//alert(text);        
        <?php
        if(!empty($formation_last->formation_yearvalue)=='2020')
        {
        ?>
        if(yearbox=='')
        {
          $("#formation_yearvalue").val('');
        }
        else if(yearbox==1)
        {
            $("#formation_yearvalue11").val('2021');
            $("#formation_amount11").val('$50.00');
        }
        else if(yearbox==2)
        {
            $("#formation_yearvalue11").val('2021,2022');
            $("#formation_amount11").val('$100.00');
        }
        else if(yearbox==3)
        {
            $("#formation_yearvalue11").val('2021,2022,2023');
            $("#formation_amount11").val('$150.00');
        }
        
        if(yearbox2=='')
        {
          //$("#formation_yearvalue").val('');
        }
        else if(yearbox2==1)
        {
            $("#formation_yearvalue11").val('2021');
            $("#formation_amount11").val('$50.00');
        }
        else if(yearbox2==2)
        {
            $("#formation_yearvalue11").val('2021,2022');
            $("#formation_amount11").val('$100.00');
        }
        else if(yearbox2==3)
        {
            $("#formation_yearvalue11").val('2021,2022,2023');
            $("#formation_amount11").val('$150.00');
        }
        
        if(yearbox3=='')
        {
          //$("#formation_yearvalue").val('');
        }
        else if(yearbox3==1)
        {
            $("#formation_yearvalue11").val('2021');
            $("#formation_amount11").val('$50.00');
        }
        else if(yearbox3==2)
        {
            $("#formation_yearvalue11").val('2021,2022');
            $("#formation_amount11").val('$100.00');
        }
        else if(yearbox3==3)
        {
            $("#formation_yearvalue11").val('2021,2022,2023');
            $("#formation_amount11").val('$150.00');
        }
        
        <?php
        }
        else
        {
            ?>
        
        if(yearbox=='')
        {
          $("#formation_yearvalue").val('');
        }
        else if(yearbox==1)
        {
            $("#formation_yearvalue11").val('2020');
            $("#formation_amount11").val('$50.00');
            $(".penaltycharges").val('$0.00');
            $(".processingfees").val('$0.00');
            
            $("#record_totalamt").val('$50.00');
            
            
        }
        else if(yearbox==2)
        {
            $("#formation_yearvalue11").val('2020,2021');
            $("#formation_amount11").val('$100.00');
             $(".penaltycharges").val('$0.00');
            $(".processingfees").val('$0.00');
             $("#record_totalamt").val('$100.00');
           
           
        }
        else if(yearbox==3)
        {
            $("#formation_yearvalue11").val('2020,2021,2022');
            $("#formation_amount11").val('$150.00');
             $(".penaltycharges").val('$0.00');
            $(".processingfees").val('$0.00');
             $("#record_totalamt").val('$150.00');
           
           
        }
        
        if(yearbox2=='')
        {
          //$("#formation_yearvalue").val('');
        }
        else if(yearbox2==1)
        {
            $("#formation_yearvalue11").val('2020');
            $("#formation_amount11").val('$50.00');
        }
        else if(yearbox2==2)
        {
            $("#formation_yearvalue11").val('2020,2021');
            $("#formation_amount11").val('$100.00');
        }
        else if(yearbox2==3)
        {
            $("#formation_yearvalue11").val('2020,2021,2022');
            $("#formation_amount11").val('$150.00');
        }
        
        if(yearbox3=='')
        {
          //$("#formation_yearvalue").val('');
        }
        else if(yearbox3==1)
        {
            $("#formation_yearvalue11").val('2020');
            $("#formation_amount11").val('$50.00');
        }
        else if(yearbox3==2)
        {
            $("#formation_yearvalue11").val('2020,2021');
            $("#formation_amount11").val('$100.00');
        }
        else if(yearbox3==3)
        {
            $("#formation_yearvalue11").val('2020,2021,2022');
            $("#formation_amount11").val('$150.00');
        }
        
    <?php    
      }
    ?>
    }

$(document).ready(function()
 {
     
     
     
    $('.paiddate').datepicker();
     
    var paidby=$('.paidby').val();
    if(paidby =='Client')
    {
         $('.hidepaid').hide();
    
    }
    else if(paidby == 'FSC')
    {
         $('.hidepaid').show();
    }
    else
    {
         $('.hidepaid').hide();
    }
    

    
 $('.paidby').on('change',function()
 { 
     var thiss=$(this).val();
     if(thiss=='Client')
     {
         $('.annualfees').val('');
         $('.hidepaid').hide();
     }
     else if(thiss =='FSC')
     {
         var foryear=$('.formation_yearbox22').val();
         if(foryear =='1')
         {
             $('.annualfees').val('$50.00');
         }
         else if(foryear =='2')
         {
             $('.annualfees').val('$100.00');
         }
         else if(foryear =='3')
         {
             $('.annualfees').val('$150.00');
         }
         $('.hidepaid').show();
     }
     else
     {
         $('.hidepaid').hide();
     }
 });
 
 
    
    
 $('.paidby2').on('change',function()
 { 
     var thiss=$(this).val();
    // alert(thiss);
     if(thiss=='Client')
     {
         $('.hidepaid2').hide();
     }
     else if(thiss =='FSC')
     {
         $('.hidepaid2').show();
     }
     else
     {
         $('.hidepaid2').hide();
     }
 });
 
 
    $('.annualfees').on('blur',function()
 { 
        var annualfees=$('.annualfees').val();
        var ann=annualfees.replace('$','');
       // alert(annualfees);
        var sign53 =parseFloat(Math.round(ann * 100) / 100).toFixed(2);
	    var annual = sign53.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	    var dollar = '$';
	    var finalyannual=dollar.concat(annual);
        $(".annualfees").val(finalyannual);
});   

$('.penaltycharges').on('blur',function()
{ 
       
        var penaltyfees=$('.penaltycharges').val();
     var pen=penaltyfees.replace('$','');
        
        var sign53 =parseFloat(Math.round(pen * 100) / 100).toFixed(2);
	    var penaltyfess = sign53.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	    var dollar = '$';
	    var finalypenaltyfess=dollar.concat(penaltyfess);
        $(".penaltycharges").val(finalypenaltyfess);
});   


 $('.processingfees').on('blur',function()
 { 
    var processingfees=$('.processingfees').val();
    var proc=processingfees.replace('$','');
     
    var ccc= processingfees.replace('$','');
    var sign53 =parseFloat(Math.round(proc * 100) / 100).toFixed(2);
    var dollar = '$';
	var finalyprocessingfees=dollar.concat(sign53);   
	$(".processingfees").val(finalyprocessingfees);
	
	var annualfees=$('.annualfees').val();
    var aaa= annualfees.replace('$','');
    var penaltycharges=$('.penaltycharges').val();
    var bbb= penaltycharges.replace('$','');
    
    
    
    if(aaa)
    {
       var annfees= aaa;
    }
    else 
    {
        var annfees= '0.00';
    }
    
    if(bbb)
    {
        var penalty= bbb;
    }
    else
    {
        var penalty= '0.00';
    }
    
    if(ccc)
    {
        var processing= ccc;
    }
    else
    {
        var processing= '0.00';
    }
    
    var totalamts=parseFloat(annfees)+parseFloat(processing)+parseFloat(penalty);
        
        var sign54 =parseFloat(Math.round(totalamts * 100) / 100).toFixed(2);
	    var totalsamts = sign54.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	    //alert(totalsamts);
	    var finalytotalsamts=dollar.concat(totalsamts);
        $(".totalamt").val(finalytotalsamts);
});   
  
     
//  Add New Start

$('.annualfees22').on('blur',function()
 { 
        var annualfees=$('.annualfees22').val();
        var ann=annualfees.replace('$','');
        
        var sign53 =parseFloat(Math.round(ann * 100) / 100).toFixed(2);
	    var annual = sign53.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	    var dollar = '$';
	    var finalyannual=dollar.concat(annual);
        $(".annualfees22").val(finalyannual);
});   

$('.penaltycharges22').on('blur',function()
{ 
        var penaltyfees=$('.penaltycharges22').val();
        var pen=penaltyfees.replace('$','');
        
        var sign53 =parseFloat(Math.round(pen * 100) / 100).toFixed(2);
	    var penaltyfess = sign53.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	    var dollar = '$';
	    var finalypenaltyfess=dollar.concat(penaltyfess);
        $(".penaltycharges22").val(finalypenaltyfess);
});   


 $('.processingfees22').on('blur',function()
 { 
    var processingfees=$('.processingfees22').val();
    var ccc= processingfees.replace('$','');
    var sign53 =parseFloat(Math.round(ccc * 100) / 100).toFixed(2);
    var dollar = '$';
	var finalyprocessingfees=dollar.concat(sign53);   
	$(".processingfees22").val(finalyprocessingfees);
	
	var annualfees=$('.annualfees22').val();
    var aaa= annualfees.replace('$','');
    var penaltycharges=$('.penaltycharges22').val();
    var bbb= penaltycharges.replace('$','');
    
    
    
    if(aaa)
    {
       var annfees= aaa;
    }
    else 
    {
        var annfees= '0.00';
    }
    
    if(bbb)
    {
        var penalty= bbb;
    }
    else
    {
        var penalty= '0.00';
    }
    
    if(ccc)
    {
        var processing= ccc;
    }
    else
    {
        var processing= '0.00';
    }
    
    var totalamts=parseFloat(annfees)+parseFloat(processing)+parseFloat(penalty);
        
        var sign54 =parseFloat(Math.round(totalamts * 100) / 100).toFixed(2);
	    var totalsamts = sign54.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	    //alert(totalsamts);
	    var finalytotalsamts=dollar.concat(totalsamts);
        $(".totalamt22").val(finalytotalsamts);
});   
  
//  Add New End     
     
     
     $("#license_gross").on("input", function(evt) {
		var self = $(this);
		self.val(self.val().replace(/[^\d].+/, ""));
		if ((evt.which < 48 || evt.which > 57)) 
		{
			evt.preventDefault();
		}});  

    $("#license_fee").on("input", function(evt) {
		var self = $(this);
		self.val(self.val().replace(/[^\d].+/, ""));
		if ((evt.which < 48 || evt.which > 57)) 
		{
			evt.preventDefault();
		}});  

     
     
  (function($) {
    var minNumber = -100;
    var maxNumber = 100;
      $('.spinner .btn:first-of-type').on('click', function() {
        if ($('.spinner input').val() == maxNumber) {
          return false;
        } else {
          $('.spinner input').val(parseInt($('.spinner input').val(), 10) + 5 + '%');
        }
      });

  $('.txtinput_1').on("blur", function() {
    var inputVal = parseFloat($(this).val().replace('%', '')) || 0
    if (minNumber > inputVal) {
      //inputVal = -100;
    } else if (maxNumber < inputVal) {
      //inputVal = 100;
    }
    $(this).val(inputVal + '.00');
  });

      $('.spinner .btn:last-of-type').on('click', function() {
        if ($('.spinner input').val() == minNumber) {
          return false;
        } else {
          $('.spinner input').val(parseInt($('.spinner input').val(), 10) - 5 + '%');
        }
      });
})(jQuery);

    }); 

</script>


<script>
function getvalYear22()
{
    $('.penaltycharges').val('');
    $('.processingfees').val('');
    $('.totalamt').val('');
    
        var yearboxval = document.getElementById("formation_yearbox22").value;
        var yearvaluesub1 = '<?php $dd=date('Y'); echo $dd-1;?>'; 
        //var yearvaluesub1 = '2018'; 
        <?php
        if(isset($common->id)!='')
        {
            ?>
                var clientid = <?php echo $common->id;?>
            <?php
        }
        ?>
        
        //alert(yearvaluesub1);
        if(yearboxval!='')
        {
            //alert(yearboxval);
            for (var n = 1; n <= yearboxval; ++ n)
            {
                var total = parseFloat(yearvaluesub1)+ parseFloat(n);
                //$("#formation_yearvalue11").val(total);
                $.get('<?php echo URL::to('getFormationexistdata'); ?>?totalid='+total,'clientid='+clientid, function(data)
                {  
                    //console.log(data);exit;
                    if(data>0)
                    {
                        $("#primary3").show();
                        $("#primary4").hide();
                        $("#yeardalreadyExist").show();
                        // $(".yeardalreadyExist1").show();
                        // $(".yeardalreadyExist2").hide();
                    }
                    else
                    {
                        $("#primary3").hide();
                        $("#primary4").show();
                        $("#yeardalreadyExist").hide();
                        // $(".yeardalreadyExist1").hide();
                        // $(".yeardalreadyExist2").show();
                    }
                    
                });
            }
            
            if(yearboxval=='')
            {
                $("#formation_yearvalue11").val(total);
                $("#formation_amount11").val('');
            }
            else if(yearboxval==1)
            {
                yearss1 = '<?php $dd=date('Y'); echo $dd;?>';
                $("#formation_yearvalue11").val(yearss1);
                $("#formation_amount11").val('$50.00');
            }
            else if(yearboxval==2)
            {
                yearss1 = '<?php $dd=date('Y'); echo $dd;?>';
                yearss2 = '<?php $dd=date('Y'); echo $dd+1;?>';
                yearss4 = ',';
	            var finalyyear2=yearss1.concat(yearss4).concat(yearss2);
                $("#formation_yearvalue11").val(finalyyear2);
                $("#formation_amount11").val('$100.00');
            }
            else if(yearboxval==3)
            {
                yearss1 = '<?php $dd=date('Y'); echo $dd;?>';
                yearss2 = '<?php $dd=date('Y'); echo $dd+1;?>';
                yearss3 = '<?php $dd=date('Y'); echo $dd+2;?>';
                yearss4 = ',';
                var finalyyear3=yearss1.concat(yearss4).concat(yearss2).concat(yearss4).concat(yearss3);
                $("#formation_yearvalue11").val(finalyyear3);
                $("#formation_amount11").val('$150.00');
            }
            else if(yearboxval==4)
            {   
                $("#formation_amount11").val('200.00');
            }
        }
        
        var yearvaluesub = document.getElementById("formation_yearvalue_already").value;
        
        
        
        
        if(yearvaluesub!='')
        {
            for (var n = 1; n <= yearboxval; ++ n)
            {
                var total = parseFloat(yearvaluesub)+ parseFloat(n);
                //$("#formation_yearvalue11").val(total);
                // $.get('<?php echo URL::to('getFormationexistdata'); ?>?totalid='+total, function(data)
                $.get('<?php echo URL::to('getFormationexistdata'); ?>?totalid='+total,'clientid='+clientid, function(data)
                {  
                    //console.log(data);exit;
                    if(data>0)
                    {
                        $("#yeardalreadyExist").show();
                        $(".yeardalreadyExist1").show();
                        $(".yeardalreadyExist2").hide();
                    }
                    else
                    {
                        $("#yeardalreadyExist").hide();
                        $(".yeardalreadyExist1").hide();
                        $(".yeardalreadyExist2").show();
                    }
                    
                });
            }
        }
        else
        {
            for (var n = 1; n <= yearboxval; ++ n)
            {
                var total = parseFloat(yearvaluesub1)+ parseFloat(n);
                //$("#formation_yearvalue11").val(total);
            }
        }
        
        
        
        if(yearboxval=='')
            {
                $("#formation_yearvalue11").val(total);
                $("#formation_amount11").val('');
            }
            else if(yearboxval==1)
            {
                yearss1 = '<?php $dd=date('Y'); echo $dd;?>';
                $("#formation_yearvalue11").val(yearss1);
                $("#formation_amount11").val('$50.00');
            }
            else if(yearboxval==2)
            {
                yearss1 = '<?php $dd=date('Y'); echo $dd;?>';
                yearss2 = '<?php $dd=date('Y'); echo $dd+1;?>';
                yearss4 = ',';
	            var finalyyear2=yearss1.concat(yearss4).concat(yearss2);
                $("#formation_yearvalue11").val(finalyyear2);
                $("#formation_amount11").val('$100.00');
            }
            else if(yearboxval==3)
            {
                yearss1 = '<?php $dd=date('Y'); echo $dd;?>';
                yearss2 = '<?php $dd=date('Y'); echo $dd+1;?>';
                yearss3 = '<?php $dd=date('Y'); echo $dd+2;?>';
                yearss4 = ',';
                var finalyyear3=yearss1.concat(yearss4).concat(yearss2).concat(yearss4).concat(yearss3);
                $("#formation_yearvalue11").val(finalyyear3);
                $("#formation_amount11").val('$150.00');
            }
            else if(yearboxval==4)
            {   
                $("#formation_amount11").val('200.00');
            }      
       
}
    
    

    
// function getvalYear33()
// {
//         var yearboxval = document.getElementById("formation_yearbox33").value;
//         var yearvaluesub = document.getElementById("formation_yearvalue_already").value;
        <?php
        if(isset($common->id)!='')
        {
            ?>
                var clientid = <?php echo $common->id;?>
            <?php
        }
        ?>
//         //alert(yearboxval);
        
//         if(yearboxval=='')
//         {
//           var yearv=$("#formation_yearvalue12").val('');
          
//         }
//         else if(yearboxval==1)
//         {
//             var yearv='2019';
//             $("#formation_yearvalue12").val('2019');
//             $("#formation_amount12").val('50.00');
            
//         }
//         else if(yearboxval==2)
//         {
//             var yearv='2020';
//             $("#formation_yearvalue12").val('2020');
//             $("#formation_amount12").val('100.00');
            
//         }
//         else if(yearboxval==3)
//         {
//             var yearv='2021';
//             var yearv=$("#formation_yearvalue12").val('2021');
//             $("#formation_amount12").val('150.00');
            
//         }
//         else if(yearboxval==4)
//         {
//             var yearv='2022';
//             $("#formation_yearvalue12").val('2022');
//             $("#formation_amount12").val('200.00');
            
//         }      

//         //console.log(yearv);exit;
//         if(yearv!='')
//         {
            
//             //$.get('<?php echo URL::to('getFormationexistdata'); ?>?totalid='+yearv, function(data)
//             $.get('<?php echo URL::to('getFormationexistdata'); ?>?totalid='+yearv,'clientid='+clientid, function(data)
//             {  
//                 //console.log(data);exit;
//                 if(data>0)
//                 {
//                     $("#yeardalreadyExist").show();
//                     $("#primary3").show();
//                     $("#primary4").hide();
                    
//                 }
//                 else
//                 {
//                     $("#yeardalreadyExist").hide();
//                     $("#primary3").hide();
//                     $("#primary4").show();
//                 }
                
//             });
           
//         }
// }







</script>


<script>
$(document).ready(function()
{

    $('#country_name').keyup(function()
    { 
        var query = $(this).val();
        if(query != '')
        { 
         var _token = $('input[name="_token"]').val();
         $.ajax({
          url:"<?php echo e(route('workstatus.fetch')); ?>",
          method:"POST",
          data:{query:query, _token:_token},
          success:function(data){
           $('#countryList').fadeIn();  
                    $('#countryList').html(data);
          }
         });
        }
    });

    $(document).on('click', 'tr', function(){  
        
        //$('#country_name').val($(this).text());  
        $('#countryList').fadeOut();  
    });  

});

$(document).on('click', '#add_certi', function () {
	    $(".addsharecerti").append('<tr><td><input type="text" class="form-control"></td> <td><input type="text" class="form-control"></td><td><input type="text" class="form-control"></td> <td><input type="date" class="form-control"></td><td><input type="text" class="form-control"></td><td><input type="checkbox" id="myCheck1" name="myCheck1"> <label class="fsc-form-label" for="myCheck1" style="margin:0px;vertical-align:middle;"></label></td>  <td style="text-align:center;"> <a class="btn-action btn-view-edit" data-toggle="modal" data-target="#mycertipdf"><i class="fa fa-print"></i></a>  <a class="btn-action btn-delete"><i class="fa fa-trash remCerti" ></i></a> </td> </tr>');
	});
	$(".addsharecerti").on('click','.remCerti',function(){
        $(this).parent().parent().parent().remove();
    });
</script>


<div id="mycertipdf" class="modal fade">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">Share Certificate </h4>
         </div>
         <div class="modal-body">
            <div>
                <div class="row" style="margin-bottom:10px">
                    <div class="col-md-3">
                        <span class="imgicon"><img src="https://www.financialservicecenter.net/public/images/th1.jpg" alt="img" style="width:50px !important;float:left;margin-right:10px;"></span>
                        <h4>Sample 1</h4>
                    </div>
                    <div class="col-md-3">
                        <span class="imgicon"><img src="https://www.financialservicecenter.net/public/images/th1.jpg" alt="img" style="width:50px !important;float:left;margin-right:10px;"></span>
                        <h4>Sample 2</h4>
                    </div>
                    <div class="col-md-3">
                        <span class="imgicon"><img src="https://www.financialservicecenter.net/public/images/th1.jpg" alt="img" style="width:50px !important;float:left;margin-right:10px;"></span>
                        <h4>Sample 3</h4>
                    </div>
                    <div class="col-md-3">
                        <span class="imgicon"><img src="https://www.financialservicecenter.net/public/images/th1.jpg" alt="img" style="width:50px !important;float:left;margin-right:10px;"></span>
                        <h4>Sample 4</h4>
                    </div>
                </div>
                <h1 class="text-center" style="position: absolute;width: 100%;margin-top: 28%;font-size: 30px;">certificate Holder Name</h1>
                <img src="https://financialservicecenter.net/public/images/share_certi.jpg" class="img-responsive">
            </div>
         </div>
         <div class="modal-footer">
            <a class="btn btn-primary">Print Certificate</a>
            <a class="btn btn-warning">Print Data Only</a>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>

<!-- Income Modal Start Update -->
<div id="myModalIncomeUpdate" class="modal fade" role="dialog">
  <div class="modal-dialog">
    
    <form method="post" action="<?php echo e(route('workrecord.updateincome')); ?>">
    <?php echo e(csrf_field()); ?>

    
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Income Update</h4>
          </div>
          
          <input type="hidden" name="incomeid" id="incomeid">
          <div class="modal-body">
              
                            
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <label class="control-label text-right">Year</label>
                                </div>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" name="wrkyear" id="wrkyear" readonly>
                                    
                                    
                                </div>
                            </div>
                            
                            
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <label class="control-label text-right">Wages,salaries,tips,etc.</label>
                                </div>
                                <div class="col-md-4">
                                    <input type="text" class="form-control wagestotal txtinput_1 income_number" name="wagetotal" id="wagetotal">
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <label class="control-label text-right">Tax-exempt interest</label>
                                </div>
                                <div class="col-md-4">
                                    <input type="text" class="form-control wagestotal txtinput_1 income_number" name="taxintrest" id="taxintrest">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <label class="control-label text-right">Qualified dividends</label>
                                </div>
                                <div class="col-md-4">
                                    <input type="text" class="form-control wagestotal txtinput_1 income_number" name="qulifieddividends" id="qulifieddividends">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <label class="control-label text-right">Pension and annuities</label>
                                </div>
                                <div class="col-md-4">
                                    <input type="text" class="form-control wagestotal txtinput_1 income_number" name="pension" id="pension">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <label class="control-label text-right">Social security benefits</label>
                                </div>
                                <div class="col-md-4">
                                    <input type="text" class="form-control wagestotal txtinput_1 income_number" name="socialsecurity" id="socialsecurity">
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <label class="control-label text-right">Capital gain or loss.</label>
                                </div>
                                <div class="col-md-4">
                                    <input type="text" class="form-control wagestotal txtinput_1 income_number" name="capital" id="capital">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <label class="control-label text-right">Other Income</label>
                                </div>
                                <div class="col-md-4">
                                    <input type="text" class="form-control wagestotal txtinput_1 income_number" name="otherincome" id="otherincome">
                                </div>
                            </div>
                            
                         <div class="form-group row">
                                <div class="col-md-3">
                                    <label class="control-label text-right">Total</label>
                                </div>
                                <div class="col-md-4">
                                    <input type="text" class="form-control totalamts txtinput_1 income_number" name="totalamount" id="totalamount" readonly>
                                </div>
                            </div>
                           
                        <div class="form-group row">
                            <div class="col-md-3"></div>
                            <div class="col-md-9">
                                <div class="row">
                                <div class="col-md-3">
                                <input type="submit" class="btn_new_save primary" name="submit" value="Save"> </div>
                                 <div class="col-md-3">
                                <a href="#" class="btn_new_cancel">Cancel</a> </div>
                                </div>
                            </div>
                        </div>
                  </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
    </form>
  </div>
</div>
<!-- Modal End -->
   

<?php $__env->stopSection(); ?>


<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>