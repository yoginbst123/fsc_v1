<?php $__env->startSection('main-content'); ?>
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>Logo</h1>
    </section>
    <!-- Main content -->
    <section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-success">
			      <div class="box-header">
              <div class="box-tools pull-right">
              </div>
            </div>
				<div class="col-md-12">
					<form method="post" action="<?php echo e(route('logo.update',$business->id)); ?>" class="form-horizontal" id="businessname" name="businessname" enctype="multipart/form-data">
					<?php echo e(csrf_field()); ?><?php echo e(method_field('PATCH')); ?>

						<div class="form-group <?php echo e($errors->has('logoname') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Logo Name :</label>
							<div class="col-lg-6 col-md-8">
								<input name="logoname" type="text" value="<?php echo e($business->logoname); ?>" id="logoname" class="form-control" value="" /><?php if($errors->has('logoname')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('logoname')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>
						<div class="form-group <?php echo e($errors->has('logourl') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Logo Url :</label>
							<div class="col-lg-6 col-md-8">
								<input name="logourl" type="text" value="<?php echo e($business->logourl); ?>" id="logourl" class="form-control" value="" /><?php if($errors->has('logourl')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('logourl')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Logo Image :</label>
							<div class="col-lg-6 col-md-8">
								<input type="file" name="logo" id="logo" class="form-control" value="<?php echo e($business->logo); ?>"/>
	<label class="file-upload btn btn-primary">
	                Browse for file ... <input name="photo" style="opecity:0" placeholder="Upload Service Image" name="bussiness_image_name" id="logo" type="file" value="<?php echo e($business->logo); ?>"/>
	            </label><img src="https://financialservicecenter.net/public/business/<?php echo e($business->logo); ?>" title="<?php echo e($business->logoname); ?>" alt="<?php echo e($business->logoname); ?>" width="100px">
							</div>
                            <input type="hidden" name="logo1" id="logo1" value="<?php echo e($business->logo); ?>">
						</div>
											
						<div class="card-footer">
						    <div class="form-group">
							<label class="control-label col-md-3"></label>
							<div class="col-xs-2" style="width:155px;">
<input class="btn_new_save btn-primary1" type="submit" name="submit" value="Save">
							</div>
							<div class="col-xs-2" style="width:155px;">
<a class="btn_new_cancel" href="<?php echo e(url('fac-Bhavesh-0554/logo')); ?>">Cancel</a> 
							</div>
						</div>
						  </div>
					</form>
				</div>
			</div>
		</div>
	</div>
	</section>
<!--</div>-->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>