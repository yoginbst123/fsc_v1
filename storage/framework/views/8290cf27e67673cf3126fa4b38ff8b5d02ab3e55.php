<?php $__env->startSection('main-content'); ?>
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>Business Category</h1>
    </section>
    <!-- Main content -->
    <section class="content">
	
	<div class="row">
		<div class="col-md-12">
				<div class="box box-success">
			      <div class="box-header">
              
              <div class="box-tools pull-right">
                
              </div>
            </div>
				<div class="col-md-12">
					<form method="post" class="form-horizontal" id="" action="<?php echo e(route('businesscategory.store')); ?>" enctype="multipart/form-data">
					<?php echo e(csrf_field()); ?>

						<div class="form-group <?php echo e($errors->has('bussiness_name') ? ' has-error' : ''); ?>">
							<label class="control-label col-lg-3 col-md-4">Business Name :</label>
							<div class="col-lg-4 col-md-8">
								<select class="form-control" id="bussiness_name" name="bussiness_name">
                                    <option value=''>Please select Business</option>
                                    <?php $__currentLoopData = $business; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bus): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>								
									<option value='<?php echo e($bus->id); ?>'><?php echo e($bus->bussiness_name); ?></option>								
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</select>
                                                                       <?php if($errors->has('bussiness_name')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('bussiness_name')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>
						
						<div class="form-group <?php echo e($errors->has('business_cat_name') ? ' has-error' : ''); ?>">
							<label class="control-label col-lg-3 col-md-4">Business Category :</label>
							<div class="col-lg-4 col-md-8">
								<input name="business_cat_name" type="text" id="business_cat_name" class="form-control" value="" />                                                   <?php if($errors->has('business_cat_name')): ?>
									<span class="help-block">
										<strong><?php echo e($errors->first('business_cat_name')); ?></strong>
									</span>
								<?php endif; ?>
							</div>
						</div>
    <div class="naicss">
               
       <div class="form-group">
       <label class="col-lg-3 col-md-4 col-xs-12 left_991 control-label">NAICS / SIC Code: </label>
           <div class="col-lg-2 col-md-3 col-xs-4 fsc-element-margin">
           <input type="text" class="form-control fsc-input naics" id="" name="naics" placeholder="NAICS" value="">
           </div>
           
           <div class="col-lg-2 col-md-3 col-xs-4 fsc-form-col">
               <div class="dropdown">
               <input type="text" class="form-control fsc-input sic" placeholder="SIC" id="" name="sic" value="">
               
               </div>
           </div>
           
           <!--<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                <input type="text" class="form-control fsc-input" placeholder="Search" id="" name="" value="">
           </div>!-->
           
           <div class="col-lg-2 col-md-2 col-xs-4 fsc-element-margin">
               <!--<input type="button" class="add-row btn btn-primary" value="Go to Link">-->
               <a href="https://www.naics.com/code-search/" target="_blank" class="add-row btn btn-primary">Go to Link</a>
           </div>
       </div>

           </div>
       						
						<div class="form-group <?php echo e($errors->has('business_cat_image') ? ' has-error' : ''); ?>">
							<label class="control-label col-lg-3 col-md-4">Business Category Image :</label>
							<div class="col-lg-4 col-md-8">
							    <label class="file-upload btn btn-primary">
		                Browse for file ... <input name="business_cat_image" style="opecity:0" placeholder="Upload Service Image" id="business_cat_image" type="file">
		            </label>
							    
							
<?php if($errors->has('business_cat_image')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('business_cat_image')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>
						<div class="form-group <?php echo e($errors->has('link') ? ' has-error' : ''); ?>">
							<label class="control-label col-lg-3 col-md-4">Url :</label>
							<div class="col-lg-4 col-md-8">
								<input name="link" type="text" value="" id="link" class="form-control"/>
<?php if($errors->has('link')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('link')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>
						<div class="card-footer">
						    <div class="form-group">
							<label class="control-label col-lg-3 col-md-4"></label>
							<div class="col-xs-2" style="width:155px;">
<input class="btn_new_save btn-primary1 primary1" type="submit" id="primary1" name="submit" value="Save">
							</div>
							<div class="col-xs-2" style="width:155px;">
<a class="btn_new_cancel" href="<?php echo e(url('fac-Bhavesh-0554/businesscategory')); ?>">Cancel</a> 
							</div>
						</div>
						  </div>
					
					
						
					</form>
				</div>
			</div>
		</div>
	</div>
	 </section>
</div>
<script>
      $(".naics").mask("999999");
      $(".sic").mask("9999");
      
 
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>