<?php $__env->startSection('main-content'); ?>
<style>
label{float:left;}
.dt-buttons{margin-bottom:10px;}
.search-btn{position:absolute;top:10px;right:-9px;background:transparent;border: transparent;}
.dt-buttons {
    margin-top: -41px;
    position: absolute;
    right:15px;
}
.page-title{
        padding: 8px 19px !important;
}
 .box-tools{
    position: absolute !important;
    margin-top: 9px !important;
    margin-right: 160px !important;
    }
.buttons-pdf {
    font-size: 0px !important;
    border-radius: 3px;
    padding: 8px 10px !important;
    background: #fff !important;
    border-color: #c6c6c6  !important;
    color:red !important;
}
.buttons-print {
    font-size: 0px !important;
    border-radius: 3px;
    padding: 8px 10px !important;
    background: #3c8dbc !important;
            border-color: #367fa9 !important;
}
.buttons-excel{
    font-size:0px !important;
    border-radius: 3px;
        padding: 8px 10px !important;
         background: #00a65a !important;
    border-color: #008d4c !important;
}
.buttons-excel:hover{
     background: #008d4c !important;
}
.buttons-pdf:hover{
     background: #f6f6f6  !important;
}
.buttons-print:hover{
     background: #367fa9 !important;
}
.fa{
    font-size: 16px !important;
}
.Urgent
{ text-align:center!important;
   background:#a4d6f9;  
}
.Immedialtely {  background:red;text-align:center!important; }
.Regular {background:#fff; text-align:center!important;}
.imgicon {
    background: #fff;
    display: block;
    width:35px;
    float: left;
    margin-right: 10px;
    float: left;
    margin-right: 10px;
    border-radius: 2px;
    padding: 3px;
    border: 1px solid #12186b;
    margin-top: -7px;
    height: 35px;
}
.imgicon img{max-width:100%;}
@media (max-width: 991px){
    .dt-buttons {
        margin-top: 0px;
        position: relative;
        right: 3px;
    }
    .add_btn_resp{
        top: 45px !important;
    }
}
@media  only screen and (max-width : 860px) {
    span.right_title {
        display: block;
        position: relative !important;
        margin-top: 5px;
        padding-right: 0px !important;
    }
    .content-header.page-title h2 {
        text-align: right;
    }
}
@media (max-width: 499px){
    .dt-buttons {
        margin-top: 6px;
        position: absolute;
        right: 14px;
    }
    .add_btn_resp{
        top: 0px !important;
    }
    .table_align_mob{
        margin-top:-50px;
    }
}
</style>
<div class="content-wrapper">
    
    <section class="content-header page-title" style="height: 39px;">
     		<div class="">
     		    <div class="" style="text-align:center;">
     		        <!--<span><i class="fa fa-users" style="float:left;margin-top:5px;"></i></span>-->
     		        <span class="imgicon"> <img src="https://www.financialservicecenter.net/public/images/Work-to-Do-04.png" alt="img" ></span>
     		        <h2>List of Work To Do <span class="right_title" style="text-align:right;padding-right: 20px;position: absolute;right: 0;">Add / View / Edit</span></h2>
     		    </div>
     		   
     		</div>
    </section>
      <section class="content">
   <div class="row">
      <div class="col-md-12">
         <div class="box box-success">
             <div class="box-header">
                 
                    <div class="col-md-12 resp_abs">
                        <div class="row">
                            <div class="col-md-3 cs1">
                                <label style="margin-top: 11px;margin-right:10px;">Search: </label>
                                <select name="types" style="width: 92%;margin-left: 4px;" id="types" class="form-control">
                                    <option value="All" selected>All</option>
                                    <option value="Createddate">Created Date</option>
                                    <option value="Priority">Priority</option>
                                    <option value="Clientid">Client ID</option>
                                    <option value="Clientname">Client Name</option>
                                    <option value="Category">Category</option>
                                </select>
                            </div>
                            <div class="col-md-3 cs3" style="">
                                <table style="width: 100%; margin: 0 auto 0 10px;" cellspacing="0" cellpadding="3" border="0">
                                    <tbody>
                                        <tr id="filter_global">
                                            <td align="center"><input type="text" class="global_filter form-control" id="global_filter" placeholder="All Search"><button class="search-btn"><i class="fa fa-search"></i></button></td>
                                        </tr>
                                        <tr id="filter_col2" data-column="1" style="display:none">
                                            <td align="center"><input type="text" class="column_filter form-control" id="col1_filter" placeholder="Created Date"><button class="search-btn"><i class="fa fa-search"></i></button></td>
                                        </tr>
                                        <tr id="filter_col3" data-column="2" style="display:none">
                                            <td align="center"><input type="text" class="column_filter form-control" id="col2_filter" placeholder="Priority"><button class="search-btn"><i class="fa fa-search"></i></button></td>
                                        </tr>
                                        <tr id="filter_col4" data-column="3" style="display:none">
                                            <td align="center"><input type="text" class="column_filter form-control" id="col3_filter" placeholder="Client ID"><button class="search-btn"><i class="fa fa-search"></i></button></td>
                                        </tr>
                                        <tr id="filter_col5" data-column="4" style="display:none">
                                            <td align="center"><input type="text" class="column_filter form-control" id="col4_filter" placeholder="Client Name"><button class="search-btn"><i class="fa fa-search"></i></button></td>
                                        </tr>
                                        <tr id="filter_col6" data-column="5" style="display:none">
                                            <td align="center"><input type="text" class="column_filter form-control" id="col5_filter" placeholder="Category"><button class="search-btn"><i class="fa fa-search"></i></button></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            
                            <div class="col-md-2" style="padding-left: 0px; padding-right: 0px; width: 95px; margin-top:4px;">
                            <div class="box-tools pull-right" data-toggle="tooltip" title="Status"></div>
                            </div>
                        </div>
                    </div>
                    <div class="pull-right add_btn_resp" style="position: relative;margin-right: 135px;z-index:9999;top:3px;">
                        <div class="table-title">
                            <a href="#" >Add Work To Do</a>
                        </div>
                    </div>
            </div>
            <div class="col-md-12 table_align_mob">
               <?php if( session()->has('success') ): ?>
               <div class="alert alert-success alert-dismissable"><?php echo e(session()->get('success')); ?></div>
               <?php endif; ?>
               <div class="table-responsive">
                  <table class="table table-hover table-bordered" id="example">
                     <thead>
                        <tr>
                            <th style="text-align:center">No</th>
                            <th style="text-align:center" width="9%">Create Date</th>
                            <th style="text-align:center" width="8%">Priority</th>
                            <th style="text-align:center">Client ID </th>
                            <th style="text-align:center">Client Name</th>
                            <th style="text-align:center" width="19%">Category</th>
                            <th width="9%">Due Date</th>
                            <th>Responsible Person</th>
                            <th style="text-align:center">Action</th>
                        </tr>
                     </thead>
                     <tbody>
                    <?php $__currentLoopData = $worktodo; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $worktodo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                           <td style="text-transform: capitalize;text-align:center;"><?php echo e($loop->index+1); ?></td>
                           <td><?php echo date('M-d Y',strtotime($worktodo->created_at))?></td>
                           <td <?php if($worktodo->worknew_priority =='Urgent') { ?> class="Urgent"<?php } else if($worktodo->worknew_priority =='Immediately') { ?> class="Immedialtely" 
                        <?php } else if($worktodo->worknew_priority =='Regular') { ?> class="Regular" <?php } ?>><?php echo e($worktodo->worknew_priority); ?></td> 
                           <!--<td><?php echo e($worktodo->filename); ?></td>-->
                           <td><?php if($worktodo->filename!=''): ?> <?php echo e($worktodo->filename); ?> <?php else: ?>   <?php endif; ?></td>
                           
                           <td><?php echo e($worktodo->company_name); ?></td>
                           <td><?php echo e($worktodo->category_name); ?></td>
                           <td><?php echo e(date('M-d Y',strtotime($worktodo->worknew_duedate))); ?></td>
                           
                           <td><?php echo e($worktodo->firstName); ?> <?php echo e($worktodo->middleName); ?> <?php echo e($worktodo->lastName); ?></td>
                           <td style="text-align:center;">
            
                                  <a class="btn-action btn-view-edit" href="<?php echo e(route('worktodo.edit',$worktodo->id)); ?>"  style=""><i class="fa fa-edit"></i></a>
                                    
                                    <form action="<?php echo e(route('worktodo.destroy',$worktodo->id)); ?>" method="post" style="display:none" id="delete-id-<?php echo e($worktodo->id); ?>">
                                        <?php echo e(csrf_field()); ?> <?php echo e(method_field('DELETE')); ?>

                                        <!--<input type="hidden" name="ids" value="<?php echo e($worktodo->id); ?>">-->
                                        </form>
                                        
										<a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record ?'))
                                                                          {event.preventDefault();document.getElementById('delete-id-<?php echo e($worktodo->id); ?>').submit();} else{event.preventDefault();}" href=""><i class="fa fa-trash"></i></a>

                                 
								</td>

                        </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
    </section>
<!--</div>-->
<script>
   	    (function ($) {
    function calcDisableClasses(oSettings) {
        var start = oSettings._iDisplayStart;
        var length = oSettings._iDisplayLength;
        var visibleRecords = oSettings.fnRecordsDisplay();
        var all = length === -1;
 
        // Gordey Doronin: Re-used this code from main jQuery.dataTables source code. To be consistent.
        var page = all ? 0 : Math.ceil(start / length);
        var pages = all ? 1 : Math.ceil(visibleRecords / length);
 
        var disableFirstPrevClass = (page > 0 ? '' : oSettings.oClasses.sPageButtonDisabled);
        var disableNextLastClass = (page < pages - 1 ? '' : oSettings.oClasses.sPageButtonDisabled);
 
        return {
            'first': disableFirstPrevClass,
            'previous': disableFirstPrevClass,
            'next': disableNextLastClass,
            'last': disableNextLastClass
        };
    }
 
    function calcCurrentPage(oSettings) {
        return Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength) + 1;
    }
 
    function calcPages(oSettings) {
        return Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength);
    }
 
    var firstClassName = 'first';
    var previousClassName = 'previous';
    var nextClassName = 'next';
    var lastClassName = 'last';
 
    var paginateClassName = 'paginate';
    var paginatePageClassName = 'paginate_page';
    var paginateInputClassName = 'paginate_input';
    var paginateTotalClassName = 'paginate_total';
 
    $.fn.dataTableExt.oPagination.input = {
        'fnInit': function (oSettings, nPaging, fnCallbackDraw) {
            var nFirst = document.createElement('span');
            var nPrevious = document.createElement('span');
            var nNext = document.createElement('span');
            var nLast = document.createElement('span');
            var nInput = document.createElement('input');
            var nTotal = document.createElement('span');
            var nInfo = document.createElement('span');
 
            var language = oSettings.oLanguage.oPaginate;
            var classes = oSettings.oClasses;
            var info = language.info || 'Page _INPUT_ of _TOTAL_';
 
            nFirst.innerHTML = language.sFirst;
            nPrevious.innerHTML = language.sPrevious;
            nNext.innerHTML = language.sNext;
            nLast.innerHTML = language.sLast;
 
            nFirst.className = firstClassName + ' ' + classes.sPageButton;
            nPrevious.className = previousClassName + ' ' + classes.sPageButton;
            nNext.className = nextClassName + ' ' + classes.sPageButton;
            nLast.className = lastClassName + ' ' + classes.sPageButton;
 
            nInput.className = paginateInputClassName;
            nTotal.className = paginateTotalClassName;
 
            if (oSettings.sTableId !== '') {
                nPaging.setAttribute('id', oSettings.sTableId + '_' + paginateClassName);
                nFirst.setAttribute('id', oSettings.sTableId + '_' + firstClassName);
                nPrevious.setAttribute('id', oSettings.sTableId + '_' + previousClassName);
                nNext.setAttribute('id', oSettings.sTableId + '_' + nextClassName);
                nLast.setAttribute('id', oSettings.sTableId + '_' + lastClassName);
            }
 
            nInput.type = 'text';
 
            info = info.replace(/_INPUT_/g, '</span>' + nInput.outerHTML + '<span>');
            info = info.replace(/_TOTAL_/g, '</span>' + nTotal.outerHTML + '<span>');
            nInfo.innerHTML = '<span>' + info + '</span>';
 
            nPaging.appendChild(nFirst);
            nPaging.appendChild(nPrevious);
            $(nInfo).children().each(function (i, n) {
                nPaging.appendChild(n);
            });
            nPaging.appendChild(nNext);
            nPaging.appendChild(nLast);
 
            $(nFirst).click(function() {
                var iCurrentPage = calcCurrentPage(oSettings);
                if (iCurrentPage !== 1) {
                    oSettings.oApi._fnPageChange(oSettings, 'first');
                    fnCallbackDraw(oSettings);
                }
            });
 
            $(nPrevious).click(function() {
                var iCurrentPage = calcCurrentPage(oSettings);
                if (iCurrentPage !== 1) {
                    oSettings.oApi._fnPageChange(oSettings, 'previous');
                    fnCallbackDraw(oSettings);
                }
            });
 
            $(nNext).click(function() {
                var iCurrentPage = calcCurrentPage(oSettings);
                if (iCurrentPage !== calcPages(oSettings)) {
                    oSettings.oApi._fnPageChange(oSettings, 'next');
                    fnCallbackDraw(oSettings);
                }
            });
 
            $(nLast).click(function() {
                var iCurrentPage = calcCurrentPage(oSettings);
                if (iCurrentPage !== calcPages(oSettings)) {
                    oSettings.oApi._fnPageChange(oSettings, 'last');
                    fnCallbackDraw(oSettings);
                }
            });
 
            $(nPaging).find('.' + paginateInputClassName).keyup(function (e) {
                // 38 = up arrow, 39 = right arrow
                if (e.which === 38 || e.which === 39) {
                    this.value++;
                }
                // 37 = left arrow, 40 = down arrow
                else if ((e.which === 37 || e.which === 40) && this.value > 1) {
                    this.value--;
                }
 
                if (this.value === '' || this.value.match(/[^0-9]/)) {
                    /* Nothing entered or non-numeric character */
                    this.value = this.value.replace(/[^\d]/g, ''); // don't even allow anything but digits
                    return;
                }
 
                var iNewStart = oSettings._iDisplayLength * (this.value - 1);
                if (iNewStart < 0) {
                    iNewStart = 0;
                }
                if (iNewStart >= oSettings.fnRecordsDisplay()) {
                    iNewStart = (Math.ceil((oSettings.fnRecordsDisplay()) / oSettings._iDisplayLength) - 1) * oSettings._iDisplayLength;
                }
 
                oSettings._iDisplayStart = iNewStart;
                oSettings.oInstance.trigger("page.dt", oSettings);
                fnCallbackDraw(oSettings);
            });
 
            // Take the brutal approach to cancelling text selection.
            $('span', nPaging).bind('mousedown', function () { return false; });
            $('span', nPaging).bind('selectstart', function() { return false; });
 
            // If we can't page anyway, might as well not show it.
            var iPages = calcPages(oSettings);
            if (iPages <= 1) {
                $(nPaging).hide();
            }
        },
 
        'fnUpdate': function (oSettings) {
            if (!oSettings.aanFeatures.p) {
                return;
            }
 
            var iPages = calcPages(oSettings);
            var iCurrentPage = calcCurrentPage(oSettings);
 
            var an = oSettings.aanFeatures.p;
            if (iPages <= 1) // hide paging when we can't page
            {
                $(an).hide();
                return;
            }
 
            var disableClasses = calcDisableClasses(oSettings);
 
            $(an).show();
 
            // Enable/Disable `first` button.
            $(an).children('.' + firstClassName)
                .removeClass(oSettings.oClasses.sPageButtonDisabled)
                .addClass(disableClasses[firstClassName]);
 
            // Enable/Disable `prev` button.
            $(an).children('.' + previousClassName)
                .removeClass(oSettings.oClasses.sPageButtonDisabled)
                .addClass(disableClasses[previousClassName]);
 
            // Enable/Disable `next` button.
            $(an).children('.' + nextClassName)
                .removeClass(oSettings.oClasses.sPageButtonDisabled)
                .addClass(disableClasses[nextClassName]);
 
            // Enable/Disable `last` button.
            $(an).children('.' + lastClassName)
                .removeClass(oSettings.oClasses.sPageButtonDisabled)
                .addClass(disableClasses[lastClassName]);
 
            // Paginate of N pages text
            $(an).find('.' + paginateTotalClassName).html(iPages);
 
            // Current page number input value
            $(an).find('.' + paginateInputClassName).val(iCurrentPage);
        }
    };
})(jQuery);
   	</script>
<script>
$(document).ready(function() {
    var table = $('#example').DataTable( {
        dom: 'Bfrtlip',
        "pagingType": "input",
    "columnDefs": [ {
            "searchable": true,
            "orderable": true,
            "targets": 0
        } ],
        "order": [[ 0, 'asc' ]],
        buttons: [
                 {
                extend:    'copyHtml5',
                text:      '<i class="fa fa-files-o"></i> &nbsp; Copy',
                titleAttr: 'Copy',
                title: $('h3').text(),
                  exportOptions: {
        columns: [0,1, 2, 3,4,5], // Only name, email and role
    }
            },
            {
                extend:    'excelHtml5',
                text:      '<i class="fa fa-file-excel-o"></i>&nbsp; Excel',
               titleAttr: 'Excel',
                title: $('h3').text(),
                 customize: function( xlsx ) {
                var sheet = xlsx.xl.worksheets['sheet1.xml'];
 $('row c', sheet).attr('s', '51');
            },
                  exportOptions: {
        columns: [0,1, 2, 3,4,5], // Only name, email and role
    }
            },
            {
                extend:    'csvHtml5',
                text:      '<i class="fa fa-file-text-o"></i> &nbsp; CSV',
                titleAttr: 'CSV',
                title: $('h3').text(),
                  exportOptions: {
        columns: [0,1, 2, 3,4,5], // Only name, email and role
    }
            },
            {
                extend:    'pdfHtml5',
                text:      '<i class="fa fa-file-pdf-o"></i>&nbsp;  PDF',
                
              customize: function (doc) {
						//Remove the title created by datatTables
						doc.content.splice(0,1);
						//Create a date string that we use in the footer. Format is dd-mm-yyyy
						var now = new Date();
						var jsDate = now.getDate()+'-'+(now.getMonth()+1)+'-'+now.getFullYear();
						
						doc.pageMargins = [20,60,20,20];
						doc.defaultStyle.fontSize = 10;
						doc.styles.tableHeader.fontSize = 10;
						doc['header']=(function() {
							return {
								columns: [{
									    alignment: 'left',
										image: logo,
										width: 50,margin: [200,5]
									},{
										alignment: 'CENTER',
										text: 'List of Client',
										fontSize: 20,
										margin: [10,35],
									},],
								margin: [20, 0, 0,12],alignment: 'center',
							}
						});
						var objLayout = {};
						objLayout['hLineWidth'] = function(i) { return 2; };
						objLayout['vLineWidth'] = function(i) { return 2; };
						objLayout['hLineColor'] = function(i) { return '#ccc'; };
						objLayout['vLineColor'] = function(i) { return '#ccc'; };
						objLayout['paddingLeft'] = function(i) { return 14; };
						objLayout['paddingRight'] = function(i) { return 14; };
						doc.content[0].layout = objLayout;
				},
				titleAttr: 'PDF',
                  exportOptions: {
        columns: [0,1, 2, 3,4,5], // Only name, email and role
    }
            },
            {
           extend: 'print',
          text: '<i class="fa fa-print"></i>&nbsp; Print',
          titleAttr: 'Print',
        customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '10pt' )
                        .prepend(
                            '<center><img src=""/></center>'
                        );
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );
                },
         exportOptions: {
          columns: [0,1, 2, 3,4,5]
      },
      footer: true,
      autoPrint: true
    },],
    } );
/*$('input.global_filter').on( 'keyup click', function () {
        filterGlobal();
    } );
 
    $('input.column_filter').on( 'keyup click', function () {
        filterColumn( $(this).parents('tr').attr('data-column') );
    } );
  table.on( 'order.dt search.dt', function () {
       table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
          cell.innerHTML = i + 1;
          table.cell(cell).invalidate('dom'); 
       } );
    } ).draw();
       table.columns(6)
        .search('^(?:(?!Inactive|New|1).)*$\r?\n?', true, false)
        .draw();
    $("#choice").on("change",function(){
 var _val = $(this).val();//alert(_val);
 
 if(_val == 'Inactive'){   
        table.columns(6).search(_val).draw();
  }
 else if(_val == 'New'){   
        table.columns(6).search(_val).draw();
  }
  else if(_val == 'Active'){  //alert();
         table.columns(6).search(_val).draw();
          table.columns(6)
        .search('^(?:(?!Inactive|New|1).)*$\r?\n?', true, false)
        .draw();
  }
  else{
        table
        .columns()
        .search('')
        .draw(); 
  }
  })
*/
    
} );
   	
function filterGlobal () {
    $('#example').DataTable().search(
        $('#global_filter').val(),
        $('#global_regex').prop('checked'),
        $('#global_smart').prop('checked')
    ).draw();
}
 
function filterColumn ( i ) {
    $('#example').DataTable().column( i ).search(
        $('#col'+i+'_filter').val(),
        $('#col'+i+'_regex').prop('checked'),
        $('#col'+i+'_smart').prop('checked')
    ).draw();
}
$( "#types" ).on('change',function() {
  // For unique choice
  var selVal = $( "#types option:selected" ).val(); 
 
  if(selVal=='Createddate')  
  {
      $('#filter_global').hide();
      $('#filter_col3').hide();
      $('#filter_col4').hide();
      $('#filter_col5').hide();
      $('#filter_col6').hide();
      $('#filter_col2').show();
  }
  
  else if(selVal=='Priority')  
  {
      $('#filter_global').hide();
      $('#filter_col2').hide();
      $('#filter_col4').hide();
      $('#filter_col5').hide();
      $('#filter_col6').hide();
      $('#filter_col3').show();
  }
  else if(selVal=='Clientid')  
  {
      $('#filter_global').hide();
      $('#filter_col2').hide();
      $('#filter_col4').show();
      $('#filter_col5').hide();
      $('#filter_col6').hide();
      $('#filter_col3').hide();
  }
    else if(selVal=='Clientname')  
  {
      $('#filter_global').hide();
      $('#filter_col2').hide();
      $('#filter_col3').hide();
      $('#filter_col5').show();
      $('#filter_col6').hide();
      $('#filter_col4').hide();
  }
   else if(selVal=='Category')  
  {
      $('#filter_global').hide();
      $('#filter_col2').hide();
      $('#filter_col3').hide();
      $('#filter_col4').hide();
      $('#filter_col6').show();
      $('#filter_col5').hide();
  }
 
  else{
      $('#filter_global').show();
       $('#filter_col3').hide();
      $('#filter_col4').hide();
      $('#filter_col5').hide();
      $('#filter_col6').hide();
      $('#filter_col2').hide();
  }
}); 
</script>

<style>
    .dataTables_filter{display:none;}
</style>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>