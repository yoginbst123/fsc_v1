<?php $__env->startSection('main-content'); ?>
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>Marque</h1>
    </section>
    <!-- Main content -->
    <section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-success">
			      <div class="box-header">
              
              <div class="box-tools pull-right">
                
              </div>
            </div>
				<div class="col-md-12">
             <form method="post" action="<?php echo e(route('marque.update',$branch->id)); ?>" class="form-horizontal" id="businessname" name="businessname" enctype="multipart/form-data">
					<?php echo e(csrf_field()); ?><?php echo e(method_field('PATCH')); ?>


						<div class="form-group <?php echo e($errors->has('marque') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Name :</label>
							<div class="col-lg-6 col-md-9">
								<input name="marque" type="text" id="marque" class="form-control" value="<?php echo e($branch->marque); ?>" />                                                            <?php if($errors->has('marque')): ?>
										<span class="help-block">
										<strong><?php echo e($errors->first('marque')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>
						
						
						<div class="card-footer">
						    <div class="col-md-3"></div>
						    <div class="col-xs-2" style="width:155px;">
								<input class="btn_new_save btn-primary1 primary1" style="margin-left:-5%" type="button" id="primary1" name="submit" value="Save">
							</div>
							<div class="col-xs-2" style="width:155px;">
								<a class="btn_new_cancel" style="margin-left:-15%" href="<?php echo e(url('fac-Bhavesh-0554/marque')); ?>">Cancel</a> 
							</div>
						</div>
						
					</form>
				</div>
			</div>
		</div>
	</div>
	</section>
<!--</div>-->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>