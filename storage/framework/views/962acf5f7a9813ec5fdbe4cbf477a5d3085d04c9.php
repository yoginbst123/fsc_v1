<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="<?php echo e(URL::asset('public/dashboard/css/main.css')); ?>">
	<title>FSC - Forgot Passowrd</title>
<style>
.help-block{ color:red}   
.semibold-text a { color: red; }
</style>
</head>

<body>

<div class="employee-logo-bg">

	<section class="employee-login-content">
	
		<div class="employee-logo">
			<img src="<?php echo e(asset('public/dashboard/images/logo_employee.png')); ?>" alt="" />
		</div>
		
		<div class="employee-login-box">

			<form class="login-form" method="post" action="<?php echo e(route('adminpassword.store')); ?>" id="admin-login">
			<?php echo e(csrf_field()); ?>

				<h3 class="login-head">Admin Forgot Password</h3>
				
				<div class="form-group">
					<label class="employee-control-label">Email</label>
					<input class="employee-form-control" type="text"  name="email" value="<?php echo e(old('email')); ?>" id="email" placeholder="Email" autofocus>
					<?php if($errors->has('email')): ?>
					<span class="help-block">
						<strong><?php echo e($errors->first('email')); ?></strong>
					</span>
					<?php endif; ?>
				</div>
				
				
				<div class="form-group btn-container">
					<button  type="submit" name="submit" class="btn_submit">Send Link</button>
				</div>
				
				
				
				<center><?php if( session()->has('success') ): ?>
				<div class="alert alert-success alert-dismissable"><?php echo e(session()->get('success')); ?></div>
				<?php endif; ?>
				<?php if( session()->has('error') ): ?>
				<div class="alert alert-danger alert-dismissable"><?php echo e(session()->get('error')); ?></div>
				<?php endif; ?>
				<div id="Register" style="margin-top:20px;width: 100%;display: inline-block;"></div></center>
				
			</form>
			
		
		
			
			<?php if(session('status')): ?>
			<div class="alert alert-success">
			<?php echo e(session('status')); ?>

			</div>
			<?php endif; ?>
		</div>
		
	</section>
	
</div>

</body>

<script src="<?php echo e(asset('public/dashboard/js/jquery-2.1.4.min.js')); ?>"></script>
<script src="<?php echo e(asset('public/dashboard/js/bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('public/dashboard/js/plugins/pace.min.js')); ?>"></script>
<script src="<?php echo e(asset('public/dashboard/js/main.js')); ?>"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js"></script>


</html>



