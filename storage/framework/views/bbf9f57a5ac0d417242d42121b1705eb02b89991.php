<?php $__env->startSection('main-content'); ?>
<style>
    .form-check{width: 50%;float: left;}
</style>
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>How To Do</h1>
    </section>
    <!-- Main content -->
    <section class="content">
	<div class="row">
		<div class="col-md-12">
				<div class="box box-success">
			      <div class="box-header">
              
              <div class="box-tools pull-right">
                
              </div>
            </div>
				<div class="col-md-12">			
					<form method="post" action="<?php echo e(route('howtodo.store')); ?>" class="form-horizontal" id="content" name="content" enctype="multipart/form-data">
					<?php echo e(csrf_field()); ?>

						<div class="form-group<?php echo e($errors->has('subject') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Subject :</label>
							<div class="col-lg-6 col-md-8">
								<input name="subject" type="type" id="subject" class="form-control">
								<?php if($errors->has('subject')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('subject')); ?></strong>
										</span>
									<?php endif; ?>							
							</div>
						</div>	
							<div class="form-group<?php echo e($errors->has('website') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Website :</label>
							<div class="col-lg-6 col-md-8">
									<input name="website" type="text" id="website" class="form-control">			
							</div>
						</div>
							<div class="form-group<?php echo e($errors->has('software') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Software :</label>
							<div class="col-lg-6 col-md-8">
									<input name="software" type="text" id="software" class="form-control">			
							</div>
						</div>
							<div class="form-group<?php echo e($errors->has('telephone') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Telephone :</label>
							<div class="col-lg-3 col-md-8">
								<input name="telephone" type="text" id="telephone" class="form-control">
								<?php if($errors->has('telephone')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('telephone')); ?></strong>
										</span>
									<?php endif; ?>							
							</div>
						</div>	
						
						<div class="input_fields_wrap_notes">
							<input name="stepid[]" value="" type="hidden" placeholder="" id="stepid" class="textonly form-control">
						    <div class="form-group">
								<label class="control-label col-md-3 col-sm-12 left_991">Step 1:</label>
								<div class="col-lg-4 col-md-6 col-sm-8">
							        <textarea name="step[]" value="" rows="1" type="text" placeholder="Create Step" id="step" class="form-control"></textarea>
							    </div>   
							
								<div class="col-lg-2 col-md-2 col-sm-3 fsc-element-margin p_cmn_767 add_htd_btn" style="padding-left:0px;">
                                  <label class="file-upload btn btn-primary">
                                  Browse for file <input name="photo[]" style="opecity:0" placeholder="Upload Service Image" id="photo" type="file">
                                  </label>
                                </div>
                             
								<div class="col-lg-2 col-sm-1 p_cmn_767 add_htd_btn" style="padding-left:0px;">
								   <a class="btn btn-primary" onclick="education_field_note();" style="width: 50px;">Add</a>
								</div>
							
						    </div>
					    </div>
						
						<div id="input_fields_wrap_notes"></div>
						
						<div class="card-footer">
						    <div class="form-group">
							<label class="control-label col-md-3 hide_991"></label>
							<div class="col-xs-2" style="width:155px;">
							    <input class="btn_new_save btn-primary1" type="submit" name="submit" value="Save">
							</div>
							<div class="col-xs-2" style="width:155px;">
							    <a class="btn_new_cancel" href="<?php echo e(url('fac-Bhavesh-0554/howtodo')); ?>">Cancel</a> 
							</div>
						</div>
						  </div>
						
					</form>
				</div>
			</div>
		</div>
	</div>
	</section>
<!--</div>-->
   <script>
   $("#telephone").mask("(999) 999-9999");
var room1 = 1;
var coun ='';
var z = room1 + coun; 
function education_field_note() {
room1++;
z++;
var objTo = document.getElementById('input_fields_wrap_notes')
var divtest = document.createElement("div");
divtest.setAttribute("class", "form-group removeclass"+z);
divtest.innerHTML = '<label class="control-label col-md-3 col-sm-12 left_991">Step '+ z +' :</label><div class="col-lg-4 col-md-6 col-sm-8"><input name="stepid[]" value="" type="hidden" placeholder="" id="stepid" class=""><textarea name="step[]"  rows="1" type="text" id="step" placeholder="Create Step" class="form-control"></textarea></div><div class="col-lg-2 col-md-2 col-sm-3 fsc-element-margin p_cmn_767 add_htd_btn" style="padding-left:0px;"><label class="file-upload btn btn-primary">Browse for file<input name="photo[]" style="opecity:0" placeholder="Upload Service Image" id="photo" type="file"></label></div></div><div class="col-lg-2 col-sm-1 p_cmn_767 add_htd_btn" style="padding-left:0px;"> <button class="btn btn-danger" type="button" onclick="remove_education_fields('+ z +');"  style="width: 50px;"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span> </button></div></div></div>';
var rdiv = 'removeclass'+z;
var rdiv1 = 'Schoolname'+z;
objTo.appendChild(divtest)
}
function remove_education_fields(rid) {
$('.removeclass'+rid).remove();
//z--;
room1--;
}
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>