<?php $__env->startSection('main-content'); ?>
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>Business Brand Category</h1>
    </section>
    <!-- Main content -->
    <section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-success">
			      <div class="box-header">
           
              <div class="box-tools pull-right">
                
              </div>
            </div>
				<div class="col-md-12">
			
<form method="post" enctype="multipart/form-data" class="form-horizontal" action="<?php echo e(route('business-brand-category.store')); ?>" id="">
					<?php echo e(csrf_field()); ?>

						<div class="form-group <?php echo e($errors->has('business_id') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Business Name :</label>
							<div class="col-lg-4 col-md-8">
								<select class="form-control category" id="" name="business_id">
								<option value=''>Please select Business</option>
									<?php $__currentLoopData = $business; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bus): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>								
									<option value='<?php echo e($bus->id); ?>'><?php echo e($bus->bussiness_name); ?></option>								
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>	
								</select>
                                                                      <?php if($errors->has('business_id')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('business_id')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>
						
						<div class="form-group <?php echo e($errors->has('business_cat_id') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Business Category Name :</label>
							<div class="col-lg-4 col-md-8">
								<select class="form-control category1 category2" id="business_cat_id" name="business_cat_id">
									<option value=''>Please select Business Category</option>
								</select>
                                                                     <?php if($errors->has('business_cat_id')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('business_cat_id')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>						
						<div class="form-group <?php echo e($errors->has('business_brand_id') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Business Brand :</label>
							<div class="col-lg-4 col-md-8">
								<select class="form-control" id="business_brand_id" name="business_brand_id">
									<option value=''>Please select Business Brand</option>									
									
								</select>
                                                                      <?php if($errors->has('business_brand_id')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('business_brand_id')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>
						
						<div class="form-group <?php echo e($errors->has('business_brand_category_name') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Business Brand Category :</label>
							<div class="col-lg-4 col-md-8">
								<input name="business_brand_category_name" type="text" id="business_brand_category_name" class="form-control" value="" />
                                                                      <?php if($errors->has('business_brand_category_name')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('business_brand_category_name')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>
						
						<div class="form-group <?php echo e($errors->has('business_brand_category_image') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Business Brand Image :</label>
							<div class="col-lg-4 col-md-8">
							    	<label class="file-upload btn btn-primary">
		                Browse for file ... <input name="business_brand_category_image" style="opecity:0" placeholder="Upload Service Image" id="business_brand_category_image" type="file">
		            </label>
								
                                                                      <?php if($errors->has('business_brand_category_image')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('business_brand_category_image')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>
						<div class="form-group <?php echo e($errors->has('link') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Url :</label>
							<div class="col-lg-4 col-md-8">
								<input name="link" type="text" id="link" class="form-control" value="" />
                                                                      <?php if($errors->has('link')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('link')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>
						<div class="card-footer">
						    <div class="form-group">
							<label class="control-label col-md-3"></label>
							<div class="col-xs-2" style="width:155px;">
<input class="btn_new_save btn-primary1" type="submit" name="submit" value="Save">
							</div>
							<div class="col-xs-2" style="width:155px;">
<a class="btn_new_cancel" href="<?php echo e(url('fac-Bhavesh-0554/business-brand-category')); ?>">Cancel</a> 
							</div>
						</div>
						  </div>
						
					</form>
				</div>
			</div>
		</div>
	</div>
 </section>	
</div>
<script>
$(document).ready(function(){
	$(document).on('change','.category', function()
	{
		//console.log('htm');
		var id = $(this).val();
		$.get('<?php echo URL::to('getRequest'); ?>?id='+id, function(data)
		{  $('#business_cat_id').empty();
           $.each(data, function(index, subcatobj)
		   {
			   $('#business_cat_id').append('<option value="'+subcatobj.id+'">'+subcatobj.business_cat_name+'</option>');
		   })

		});
			
	});
});
</script>
<script>
$(document).ready(function(){
	$(document).on('change','.category2', function()
	{
		//console.log('htm');
		var id = $(this).val();
		$.get('<?php echo URL::to('getRequestbrand'); ?>?id='+id, function(data)
		{  $('#business_brand_id').empty();
           $.each(data, function(index, subcatobj)
		   {
			   $('#business_brand_id').append('<option value="'+subcatobj.id+'">'+subcatobj.business_brand_name+'</option>');
		   })

		});
			
	});
});
</script>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>