<?php $__env->startSection('main-content'); ?>
<style>
.modal-dialog {
    max-width: 535px;
    margin: 163px auto;
}
.modal-header .close {
    margin-top: -2px;
    font-size: 20px;
}
.modal-content {    display: inline-block;}
.modal{    background: rgba(0,0,0,.7);}
</style>
<div class="col-lg-9 col-md-8 col-sm-12 col-xs-12 fsc-wwd-right-div">

	<div class="row">
	
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"></div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="border-bottom:1px solid #d1d1d1;">
				<h4 class=" fsc-sect-head">FINANCIAL</h4>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"></div>
		</div>
		
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%; text-align:center;">
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"></div>
			<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 fsc-col-grid" style="display:table;">
				<img src="<?php echo e(URL::asset('public/frontcss/images/financial1.png')); ?>" alt="img09"/><br><br>
				<h4 style="color:#103b68;">Right Direction</h4>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 fsc-col-grid" style="display:table;">
				<img src="<?php echo e(URL::asset('public/frontcss/images/financial2.png')); ?>" alt="img09"/><br><br>
				<h4 style="color:#103b68;">Money</h4>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 fsc-col-grid" style="display:table;">
				<img src="<?php echo e(URL::asset('public/frontcss/images/financial3.png')); ?>" alt="img09"/><br><br>
				<h4 style="color:#103b68;">Success</h4>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"></div>
		</div>
		
	</div>
	
	<div class="row">
	
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:4%;">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"></div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="border-bottom:1px solid #d1d1d1;">
				<h4 class="fsc-sect-head">SERVICE</h4>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"></div>
		</div>
		
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%; text-align:center;">
			<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>
			<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 fsc-col-grid" style="display:table;">
				<img src="<?php echo e(URL::asset('public/frontcss/images/Services1.png')); ?>" alt="img09"/><br><br>
				<h4 style="color:#103b68; text-align:center;">Accounting Service</h4>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 fsc-col-grid" style="display:table;">
				<img src="<?php echo e(URL::asset('public/frontcss/images/Services2.png')); ?>" alt="img09"/><br><br>
				<h4 style="color:#103b68; text-align:center;">Residential Service</h4>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 fsc-col-grid" style="display:table;">
				<img src="<?php echo e(URL::asset('public/frontcss/images/Services3.png')); ?>" alt="img09"/><br><br>
				<h4 style="color:#103b68; text-align:center;">Commercial Service</h4>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 fsc-col-grid" style="display:table;">
				<img src="<?php echo e(URL::asset('public/frontcss/images/Services4.png')); ?>" alt="img09"/><br><br>
				<h4 style="color:#103b68; text-align:center;">Financial Service</h4>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 fsc-col-grid" style="display:table;">
				<img src="<?php echo e(URL::asset('public/frontcss/images/Services5.png')); ?>" alt="img09"/><br><br>
				<h4 style="color:#103b68; text-align:center;">Insurance Service</h4>
			</div>
			<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>
		</div>
		
	</div>
	
	<div class="row">
	
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:4%;">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"></div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="border-bottom:1px solid #d1d1d1;">
				<h4 class="fsc-sect-head">CENTER</h4>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"></div>
		</div>
		
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%; text-align:center;">
			<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"></div>
			<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 fsc-col-grid" style="display:table; margin-top:2%;">
				<img src="<?php echo e(URL::asset('public/frontcss/images/Center1.png')); ?>" alt="img09"/>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 fsc-col-grid" style="display:table; margin-top:2%;">
				<img src="<?php echo e(URL::asset('public/frontcss/images/Center2.png')); ?>" alt="img09"/>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 fsc-col-grid" style="display:table; margin-top:2%;">
				<img src="<?php echo e(URL::asset('public/frontcss/images/Center3.png')); ?>" alt="img09"/>
			</div>
<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">&nbsp</div>
<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 fsc-col-grid" style="display:table; margin-top:6%;">
				<a target="_blank" href="#" data-toggle="modal" data-target="#exampleModal222" class="fsc-apply-btn" style="padding: 5px 16px;"><!--<img src="<?php echo e(URL::asset('public/frontcss/images/Center3.png')); ?>" alt="img09"/>--> Presentation</a>
			</div>
			
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
				<h4 style="color:#103b68; text-align:center;">Where all the services come together.</h4>
			</div>

		</div>
		
	</div>
	
</div>

<?php $__env->stopSection(); ?>
<div class="modal fade" id="exampleModal222" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel222" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="float:right; ">
   
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
<br>
      <div class="modal-body" style="   margin-top: 30px;">
       <iframe style="width:500px;height:400px" src="https://www.youtube.com/embed/5YtUIfnQqrk" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
      </div>
      
    </div>
  </div>
</div>

<?php echo $__env->make('front-section.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>