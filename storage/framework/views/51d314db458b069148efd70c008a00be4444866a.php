<?php $__env->startSection('main-content'); ?>
<div class="content-wrapper">
	 <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>Service Master</h1>
    </section>
    <!-- Main content -->
    <section class="content">
    	<div class="row">
    	    <div class="col-md-12">
    	        <div class="box box-success">
    	            <div class="box-header"></div>
    	            <div class="card-body">
    	                <form method="post" action="<?php echo e(url('fac-Bhavesh-0554/services/masterStore')); ?>" class="form-horizontal" id="content" name="content">
                            <?php echo e(csrf_field()); ?>


                            <div class="col-md-12">
         <!--                   <div class="form-group <?php echo e($errors->has('service_names') ? ' has-error' : ''); ?>">-->
         <!--                       <label class="control-label col-md-3">Service Name :</label>-->
         <!--                       <div class="col-md-5">-->
         <!--                           <select class="form-control" name="service_names" id="service_names">-->
         <!--                               <option>Select Service</option>-->
         <!--                               <?php $__currentLoopData = $service; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $services): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>-->
         <!--                                   <option value="<?php echo e($services->id); ?>"><?php echo e($services->service_name); ?></option>-->
         <!--                               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>-->
         <!--                           </select>-->
         <!--                            <?php if($errors->has('service_names')): ?>-->
									<!--	<span class="help-block">-->
									<!--		<strong><?php echo e($errors->first('service_names')); ?></strong>-->
									<!--	</span>-->
									<!--<?php endif; ?>-->
         <!--                       </div>-->
         <!--                   </div>-->
                            
                            
                            <div class="ser_value">
                                <div class="form-group <?php echo e($errors->has('service_values') ? ' has-error' : ''); ?>" >
                                    <label class="control-label col-md-3">Service Name:</label>
                                    <div class="col-md-5">
                                        <input type="text" name="service_value[]" id="service_values" class="form-control">
                                    </div>
                                    <!--<div class="col-md-2">-->
                                    <!--    <input type="text" name="" id="service_price[]" class="form-control only_num" placeholder="$0.00">-->
                                    <!--</div>-->
                                    <!--<div class="col-md-1" style="padding-left:0px;">-->
                                    <!--    <a class="btn btn-primary add_ser_value"><i class="fa fa-plus"></i></a>-->
                                    <!--</div>-->
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3"> </label>
                                <div class="col-md-5">
                                    <input type="checkbox" id="subservice" name="subservice">
                                    <label class="fsc-form-label" for="subservice"> If Sub Service Exists </label>
                                </div>
                            </div>
                            
                            <div class="sub_service">
                                <div class="sub_cat">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Sub Service Name :</label>
                                        <div class="col-md-5">
                                            <input type="text" name="subservice_name[]" id="subservice_name" class="form-control">
                                        </div>
                                        <div class="col-md-1" style="padding-left:0px;">
                                            <a class="btn btn-primary add_sub_ser_name">Add</a>
                                        </div>
                                    </div>
                                    <!--<div class="sub_ser_value">-->
                                    <!--    <div class="form-group">-->
                                    <!--        <label class="control-label col-md-3">Sub Service Value :</label>-->
                                    <!--        <div class="col-md-3">-->
                                    <!--            <input type="text" name="subservice_val[]" id="subservice_val" class="form-control">-->
                                    <!--        </div>-->
                                    <!--        <div class="col-md-2">-->
                                    <!--            <input type="text" name="subservice_price[]" id="" class="form-control only_num" placeholder="$0.00">-->
                                    <!--        </div>-->
                                    <!--        <div class="col-md-1" style="padding-left:0px;">-->
                                    <!--            <a class="btn btn-warning add_sub_ser_value"><i class="fa fa-plus"></i></a>-->
                                    <!--        </div>-->
                                    <!--    </div>-->
                                    <!--</div>-->
                                </div>
                            </div>
                            
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-md-2 col-md-offset-3">
                                        <input class="btn_new_save btn-primary1" type="submit" name="submit" value="Save">
                                    </div>
                                    <div class="col-md-2 row">
                                        <a class="btn_new_cancel" href="">Cancel</a> 
                                    </div>
                                </div>
                            </div>
                            </div>
	                    </form>
	                </div>
    	        </div>
            </div>
    	</div>
<script type="text/javascript">
    $(document).ready(function(){
        $(".sub_service").hide();
        $("#subservice").click(function () {
            if ($(this).is(":checked")) {
                $(".sub_service").show();
            } else {
                $(".sub_service").hide();
            }
        });
        
        $(".add_ser_value").click(function(){
            $(".ser_value").append('<div class="form-group"><label class="control-label col-md-3">Service Value :</label><div class="col-md-3"><input type="text" name="service_value[]" id="service_value" class="form-control"></div><div class="col-md-2"><input type="text" name="service_price[]" id="service_price" class="form-control only_num" placeholder="$0.00"></div><div class="col-md-1" style="padding-left:0px;"><a class="btn btn-danger rem_ser_value"><i class="fa fa-minus"></i></a></div></div>');
        });
        
        $(".ser_value").on('click','.rem_ser_value',function(){
            $(this).parent().parent().remove();
        });
        
        $(".add_sub_ser_value").click(function(){
            $(".sub_ser_value").append('<div class="form-group"><label class="control-label col-md-3">Sub Service Value :</label><div class="col-md-3"><input type="text" name="subservice_val[]" class="form-control"></div><div class="col-md-2"><input type="text" name="subservice_price[]" id="" class="form-control only_num" placeholder="$0.00"></div><div class="col-md-1" style="padding-left:0px;"><a class="btn btn-danger rem_sub_ser_value"><i class="fa fa-minus"></i></a></div></div>');
        });
        
        $(".sub_ser_value").on('click','.rem_sub_ser_value',function(){
            $(this).parent().parent().remove();
        });
        
        $(".add_sub_ser_name").click(function(){
            $(".sub_service").append('<div class="sub_cat"><div class="form-group"><label class="control-label col-md-3">Sub Service Name :</label><div class="col-md-5"><input type="text" name="subservice_name[]" class="form-control"></div><div class="col-md-1" style="padding-left:0px;"><a class="btn btn-danger rem_sub_service"><i class="fa fa-minus"></i></a></div></div>  </div>');
        });
        $(".sub_service").on('click','.rem_sub_service',function(){
            $(this).parent().parent().parent().remove();
        });
        
        $('.sub_service').on('click', '.add_sub_ser_value2', function(){
            $(".sub_ser_value1").append('<div class="form-group"><label class="control-label col-md-3">Sub Service Value2 :</label><div class="col-md-3"><input type="text" name="subservice_name[]" class="form-control"></div><div class="col-md-2"><input type="text" name="subservice_price[]" id="" class="form-control only_num" placeholder="$0.00"></div><div class="col-md-1" style="padding-left:0px;"><a class="btn btn-danger rem_sub_ser_value2"><i class="fa fa-minus"></i></a></div></div>');
        });
        
        $(".sub_service").on('click','.rem_sub_ser_value2',function(){
            $(this).parent().parent().remove();
        });
    });
</script>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>