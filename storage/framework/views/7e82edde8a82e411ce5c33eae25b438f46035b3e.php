<?php $__env->startSection('title', 'Edit Technical Support'); ?>
<?php $__env->startSection('main-content'); ?>
<div class="content-wrapper">
	    <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>Technical Support</h1>
    </section>
    <!-- Main content -->
    <section class="content">
<div class="row">
		<div class="col-md-12">
			<div class="box box-success">
			      <div class="box-header">
            
              <div class="box-tools pull-right">
                
              </div>
            </div>
				<div class="col-md-12">
					<form method="post" action="<?php echo e(route('clienttechnical.update',$homecontent->id)); ?>" class="form-horizontal" id="homecontent" name="homecontent" enctype="multipart/form-data">
					<?php echo e(csrf_field()); ?><?php echo e(method_field('PATCH')); ?> <div class="form-group">
                   <label class="control-label col-md-3">Date / Day / Time:</label>
                     <div class="col-lg-2 col-md-3">
                        <div class="">
                           <input type="text" name="date" id="date" class="form-control" value="<?php if(empty($homecontent->date)): ?> <?php echo e(date('m/d/Y')); ?> <?php else: ?> <?php echo e($homecontent->date); ?> <?php endif; ?>" placeholder="Date">
                        </div>
                     </div>
                      
                     <div class="col-lg-2 col-md-3">
                        <div class="">
                           <input type="text" name="day" id="day" class="form-control" placeholder="Day" value="<?php if(empty($homecontent->day)): ?> <?php echo e(date('l')); ?> <?php else: ?> <?php echo e($homecontent->day); ?> <?php endif; ?>">
                        </div>
                     </div>
                     
                     <div class="col-lg-2 col-md-3">
                        <div class="">
                           <input type="text" name="time" id="time" class="form-control" value="<?php if(empty($homecontent->time)): ?> <?php echo e(date('H:i a')); ?> <?php else: ?><?php echo e($homecontent->time); ?> <?php endif; ?>" placeholder="Time">
                        </div>
                       
                     </div>
                  </div>
						<div class="form-group<?php echo e($errors->has('type') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">To :</label>
							<div class="col-lg-8 col-md-9">
							     <select class="form-control"id="to" name="to">
							         <option value="">Select</option>
							         <?php $__currentLoopData = $tech; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tech1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							         <?php if(!empty($tech1->technical_support)): ?>
                                                <option value="<?php echo e($tech1->id); ?>" <?php if($tech1->id==$homecontent->to_supporter): ?> selected <?php endif; ?>><?php echo e($tech1->technical_support); ?> (<?php echo e($tech1->firstName.' '.$tech1->middleName.' '.$tech1->lastName); ?>)</option>
                                                                        <?php endif; ?>
                                           <?php if(!empty($tech1->timing_support)): ?>
                                                <option value="<?php echo e($tech1->id); ?>" <?php if($tech1->id==$homecontent->to_supporter): ?> selected <?php endif; ?>><?php echo e($tech1->timing_support); ?> (<?php echo e($tech1->firstName.' '.$tech1->middleName.' '.$tech1->lastName); ?>)</option>
                                                                        <?php endif; ?>
                                                                        
							         <?php if(!empty($tech1->system_support)): ?>
                                                <option value="<?php echo e($tech1->id); ?>" <?php if($tech1->id==$homecontent->to_supporter): ?> selected <?php endif; ?>><?php echo e($tech1->system_support); ?> (<?php echo e($tech1->firstName.' '.$tech1->middleName.' '.$tech1->lastName); ?>)</option>
                                                                        <?php endif; ?>
							         <?php if(!empty($tech1->other_support)): ?>
                                                <option value="<?php echo e($tech1->id); ?>" <?php if($tech1->id==$homecontent->to_supporter): ?> selected <?php endif; ?>><?php echo e($tech1->other_support); ?> (<?php echo e($tech1->firstName.' '.$tech1->middleName.' '.$tech1->lastName); ?>)</option>
                                                                        <?php endif; ?>                                                                        
                                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                 </select>
			
							</div>
						</div>	
						
					
						<div class="form-group<?php echo e($errors->has('subject') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Subject :</label>
							<div class="col-lg-8 col-md-9">
								<input name="subject" type="text" id="subject" value="<?php echo e($homecontent->subject); ?>" class="form-control">

								<?php if($errors->has('subject')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('subject')); ?></strong>
										</span>
									<?php endif; ?>							
							</div>
						</div>
							
					<div class="form-group<?php echo e($errors->has('details') ? ' has-error' : ''); ?>">
						<label class="control-label col-md-3">Details :</label>
						<div class="col-lg-8 col-md-9">
						<div class="">
							  <textarea id="editor1" name="details" rows="10" cols="80"><?php echo e($homecontent->details); ?></textarea>
					  </div>
							<?php if($errors->has('details')): ?>
									<span class="help-block">
										<strong><?php echo e($errors->first('details')); ?></strong>
									</span>
								<?php endif; ?>	
						</div>
					</div>
					
					<div class="form-group">
						<label class="control-label col-md-3">Technical Answer :</label>
						<div class="col-lg-8 col-md-9">
						<div class="">
						    <?php if(Auth::user()->id=='30'): ?>
							  <textarea id="editor2" name="answer" rows="10" cols="80" ><?php echo e($homecontent->answer); ?></textarea>
							  <?php else: ?>
							  <textarea id="editor2" name="answer" rows="10" cols="80" ><?php echo e($homecontent->answer); ?></textarea>
							  <?php endif; ?>
					  </div>
						
						</div>
					</div>
					
						<div class="form-group">
						<label class="control-label col-md-3">Attachment :</label>
						<div class="col-lg-8 col-md-9">
						<div class="">
							 <label class="file-upload btn btn-primary">
                Browse for file ... <input type="file" class="form-control fsc-input" style="opacity:0" id="attachment" name="attachment" placeholder="Select Document">
            </label> <img src="<?php echo e(asset('public/attachment','')); ?>/<?php echo e($homecontent->attachment); ?>" title="<?php echo e($homecontent->subject); ?>" alt="<?php echo e($homecontent->subject); ?>" width="100px">
					  </div>
								
						</div>
					</div>
						<div class="card-footer">
						    <div class="row">
							<!--<div class="row">-->
							<!--	<div class="col-md-8 col-md-offset-3">-->
							<!--		<input class="btn btn-primary icon-btn" type="submit" name="submit" value="save">-->
							<!--	</div>-->
							<!--</div>-->
							<div class="col-md-3"></div>
							<div class="col-xs-2" style="width:155px;">
    							<input class="btn_new_save" type="submit" value="save">
    						</div>
    						<div class="col-xs-2" style="width:155px;">
    							<a class="btn_new_cancel" href="<?php echo e(url('fac-Bhavesh-0554/clienttechnical')); ?>">Cancel</a> 
    						</div>
    						</div>
						</div>
						
					</form>
				</div>
			</div>
		</div>
	</div>
	  </section>
<!--</div>-->
<script>
    $(document).ready(function(){
       $('#type').on('change', function(){
         if($('#type').val()=='Resposibilty')
         {
             $('.emp').show();
         }
         else
         {
            $('.emp').hide();  
         }
         
       });
    });
    
</script>
<style>
    input[type="file"] {
    display: block;
    position: absolute;
}
</style>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>