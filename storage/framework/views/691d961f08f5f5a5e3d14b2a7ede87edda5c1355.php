<?php $__env->startSection('main-content'); ?>
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>Service Type</h1>
    </section>
    <!-- Main content -->
    <section class="content">
	<div class="row">
		<div class="col-md-12">
				<div class="box box-success">
			      <div class="box-header">
            
              <div class="box-tools pull-right">
                
              </div>
            </div>
				<div class="col-md-12">
             <form method="post" action="<?php echo e(route('servicetype.store')); ?>" class="form-horizontal" id="businessname" name="businessname" enctype="multipart/form-data">
					<?php echo e(csrf_field()); ?>

						<div class="form-group <?php echo e($errors->has('servicetype') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Service Type :</label>
							<div class="col-md-4">
<select name="servicetype" class="form-control">
<option value="">Select</option>
<?php $__currentLoopData = $service; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ser): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<option value="<?php echo e($ser->id); ?>"><?php echo e($ser->service_name); ?></option>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                                           

</select>
 <?php if($errors->has('servicetype')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('servicetype')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>
						
						
						<div class="form-group <?php echo e($errors->has('servicename') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Service Name :</label>
							<div class="col-md-4">
								<input name="servicename" type="text" id="servicename" class="form-control" value="" />
								<?php if($errors->has('servicename')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('servicename')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>					
						<div class="card-footer">
						    <div class="form-group">
							<label class="control-label col-md-3"></label>
							<div class="col-md-2">
<input class="btn_new_save btn-primary1" type="submit" name="submit" value="Save">
							</div>
							<div class="col-md-2 row">
<a class="btn_new_cancel" href="<?php echo e(url('fac-Bhavesh-0554/servicetype')); ?>">Cancel</a> 
							</div>
						</div>
						  </div>
						
					</form>
				</div>
			</div>
		</div>
	</div>
	 </section>
<!--</div>-->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>