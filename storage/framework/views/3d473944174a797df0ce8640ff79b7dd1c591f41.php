      <?php if(isset($data)){ if($data['HasData'] == 1){ setlocale(LC_MONETARY,"en_US"); echo money_format("%", $data['fsc_fee']);?>
        <div class="proposal" style="width: 850px;padding: 10px;margin: auto;border: 1px solid #000000;">   
            <img src="https://financialservicecenter.net/public/dashboard/images/fsc_logo.png" width="100px" style="margin:10px auto;display:block;" class="img-responsive">
            <p style="font-size: 16px;font-weight: 600;margin-top: 10px;text-align:center;">
                <span>4550 Jimmy Carter Blvd.</span>
                <span style="padding: 0px 4px;"> | </span> Norcroos
                <span style="padding: 0px 4px;"> | </span> GA
                <span style="padding: 0px 4px;"> | </span> FAX : +1 (770) 414-5351
            </p>
            <hr style="border-top: 2px solid #8e8e8e;margin-bottom:10px;">
            <h3 style="margin-top:40px;text-align: center !important;"><span style="background: #d3edff;padding: 10px;border: 1px solid #106097;">Proposal</span></h3>
            <div class="row" style="margin-top:-60px;">
                <div style="width:50%;float:left;">
                    <strong>Proposal To:</strong>
                    <p class="lbl_preview_client_name"><?php echo e($data['client_name']); ?></p>
                    <p class="lbl_preview_address"><?php echo e($data['client_address']); ?></p>
                    <p class="lbl_preview_city"><?php echo e($data['city']); ?></p>
                    <p class="lbl_preview_state"><?php echo e($data['state']); ?></p>
                </div>
                <div class="col-md-6" style="width:50%;float:left;">
                    <p id="lbl_proposal_no" style="margin-bottom:0px;text-align:right;margin:0;"><strong>Proposal No : </strong><br>PRO-<?php echo e(date('y')); ?>-<?php echo e($data['pid']); ?> </p>
                    <p id="lbl_proposal_date" style="text-align:right;margin:0 0 10px;"><strong>Proposal Date : </strong><br> <?php echo e($data['duedate']); ?> </p>
                </div>
            </div>
            <div class="table-responsive">
                <table border="1" style="font-size:14px;color:#333;font-family:Helvetica, Arial, sans-serif;width:100%;margin:auto;border-collapse:collapse;border-spacing:0;margin-bottom:20px;">
                    <thead style="background:#ffff99">
                        <tr>
                            <th style="padding: 5px 8px 5px 8px !important;">Priority</th>
                            <th style="padding: 5px 8px 5px 8px !important;">Payment Terms</th>
                            <th style="padding: 5px 8px 5px 8px !important;">Sales Rep ID</th>
                            <th style="padding: 5px 8px 5px 8px !important;" width="150px">Due Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td style="text-align:center;padding: 5px 8px 5px 8px !important;"><p id="lbl_priority"><?php echo e($data['priority']); ?></p></td>
                            <td style="text-align:center;padding: 5px 8px 5px 8px !important;">Net Due</td>
                            <td style="text-align:center;padding: 5px 8px 5px 8px !important;"><p><?php echo e($data['sales_rep_id']); ?></p></td>
                            <td style="text-align:center;padding: 5px 8px 5px 8px !important;"><p id="lbl_due_date"><?php echo e($data['duedate']); ?></p></td>
                        </tr>
                    </tbody>
                </table>
                <table border="1" style="font-size:14px;color:#333;font-family:Helvetica, Arial, sans-serif;width:100%;margin:auto;border-collapse:collapse;border-spacing:0;">
                    <thead style="background:#ffff99">
                        <tr>
							<th style="text-align:left !important;padding: 5px 8px 5px 8px !important;">Description</th>
							<th style="padding: 5px 8px 5px 8px !important;" width="150px">Amount</th>
						</tr>
					</thead>
                    <tbody>
                        <tr>
                            <td style="text-align:top;height: auto;border-bottom: none !important;padding: 5px 8px 5px 8px !important;"><div class="lbl_description"><?php echo e($data['description']); ?></div></td>
                            <td style="text-align:right;border-bottom: none !important;padding: 5px 8px 5px 8px !important;"><p class="_lbl_preview_amount"><?php echo e(money_format("%n",$data['fsc_fee'])); ?></p></td>
                        </tr>
                        <tr>
                            <td style="text-align:right;border-bottom: none !important;padding: 5px 8px 5px 8px !important;"><strong>Total Proposal Amount</strong></td>
                            <td style="text-align:right;border-bottom: none !important;padding: 5px 8px 5px 8px !important;"><p id="total_prop_price"><?php echo e(money_format("%n",$data['fsc_fee'])); ?></p></td>
                        </tr>
                        <tr>
                            <td style="text-align:right;border-bottom: none !important;padding: 5px 8px 5px 8px !important;"><strong>Discount Amount</strong></td>
                            <td style="text-align:right;border-bottom: none !important;padding: 5px 8px 5px 8px !important;"><p id="lbl_preview_discountbox"><?php echo e(money_format("%n",$data['discount'])); ?></p></td>
                        </tr>
                        <tr>
                            <td style="text-align:right;border-bottom: none !important;border-top: none !important;padding: 5px 8px 5px 8px !important;"><strong>Required Advance Payment</strong></td>
                            <td style="text-align:right;border-top: none !important;border-top: none !important;padding: 5px 8px 5px 8px !important;"><p id="total_prop_advprice"><?php echo e(money_format("%n",$data['adv_payment'])); ?></p></td>
                        </tr>
                        <tr>
                            <td style="text-align:right;padding: 5px 8px 5px 8px !important;"><strong>Due Balance</strong> </td>
                            <td style="text-align:right;padding: 5px 8px 5px 8px !important;"><p id="total_prop_dueprice"><?php echo e(money_format("%n",$data['due_bal'])); ?></p></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <a class="btn btn-primary" href="<?php echo e($data['link']); ?>">Click Here</a>   For Acceptation 
        <?php }else{?>
           <h1 style="text-align:center">Your Link Expired Or used</h1>
           <a class="btn btn-primary" href="https://financialservicecenter.net"> Click here </a> For Back to Our Site.
        <?php } 
        
        }else {?>
        <h1 style="text-align:center">:( Sorry Propsal</h1>
                <a class="btn btn-primary" href="https://financialservicecenter.net"> Click here </a> For Back to Our Site.
        <?php }?>