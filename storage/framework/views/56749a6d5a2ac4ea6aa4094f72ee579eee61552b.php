<?php $__env->startSection('main-content'); ?>
<style>
.help-block {color: #fb1919;font-size: 14px;}
.star-required1 {color: transparent;}
.form-control-feedback{right: 10px;}
</style>
<div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 fsc-content-head">
			<h4>EMPLOYMENT</h4>
		</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-section-head">
			<h4>Application</h4>
		</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-content-box">
			<form id='employment'  enctype="multipart/form-data" method='post' action="<?php echo e(route('apply-employment.store')); ?>">
			         <?php echo e(csrf_field()); ?>

                   <input type="hidden" class="form-control fsc-input" name='employment_id' value="<?php echo e(Request::segment(2)); ?>" id="employment_id" placeholder="First Name">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 4%;">
					<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
						<label class="fsc-form-label">Name : <span class="star-required">*</span></label>
					</div>
					<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
					    	<div class="row">
					<div class="col-lg-3 col-md-2 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
														<select class="form-control fsc-input" id="nametype" name="nametype">
														   <option value="mr">Mr.</option>
														    <option value="mrs">Mrs.</option>
														    <option value="miss">Miss.</option>
														</select>
														
													</div>
					<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
						<input type="text" class="form-control textonly fsc-input" name='firstName' id="firstName" placeholder="First">
</div>
					<div class="col-lg-1 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
					    <div class="row">
						<input type="text" class="form-control textonly fsc-input" maxlength="1" name='middleName' id="middleName" placeholder="M">
						</div>
					</div>
					<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
						<input type="text" class="form-control fsc-input textonly " name="lastName" id="lastName" placeholder="Last">
					</div>
				</div>
				</div>
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 2%;">
					<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
						<label class="fsc-form-label">Address 1 : <span class="star-required">*</span></label>
					</div>
					<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
						<input type="text" class="form-control fsc-input" id="address1" name='address1' placeholder="Address">
					</div>
				</div>
				
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
					<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
						<label class="fsc-form-label">Address 2 : <span class="star-required1">*</span> </label>
					</div>
					<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
						<input type="text" class="form-control fsc-input" id="address2" name='address2' placeholder="Address">
					</div>
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 2%;">
					<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row"> 
						<label class="fsc-form-label">Country : <span class="star-required">*</span></label>
					</div>
					<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
						<select name="countryId" id="countries_states1" class="form-control fsc-input bfh-countries fsc-input" data-country="USA" style='height:auto'>
						            <option value=''>---Select---</option>
						 </select>
					</div>
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 2%;">
					<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
						<label class="fsc-form-label">City / State / Zip : <span class="star-required">*</span></label>
					</div>
					
					
                   <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
						<input type="text" class="form-control textonly fsc-input" id="city" name='city' placeholder="City">
					</div>
                      <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
						<select name="stateId" id="stateId" class="form-control fsc-input bfh-states" data-country="countries_states1" style='height:auto'>
						</select>
						 <select class="form-control bfh-timezones" style="display:none" name="timezone" data-country="countries_states1"></select>
					</div>
					<div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
						<input type="text" class="form-control fsc-input zip" id="zip"  name='zip' maxlength="6" placeholder="Zip" maxlength='5'>
					</div>
				</div>
				
				
				
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 2%;">
					<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
						<label class="fsc-form-label">Telephone 1 : <span class="star-required">*</span></label>
					</div>
					<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
						<input type="tel" class="form-control fsc-input bfh-phone" data-country="countries_states1" data-format="  (999) 999-9999" id="telephoneNo1" name='telephoneNo1' placeholder="(999) 999-9999">
					</div>
					<div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
						<select name="telephoneNo1Type" id="telephoneNo1Type" class="form-control fsc-input" style='height:auto'><option value=''>Type</option>
							<option value='Mobile'>Mobile</option>
							<option value='Home'>Home</option>
							<option value='Work'>Work</option>
							<option value='Other'>Other</option>
						</select>
					</div>

<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
						<input type="text" class="form-control fsc-input zip" id="" readonly name="ext1" placeholder="Ext.">
					</div>
</div>
				
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 2%;">
					<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
						<label class="fsc-form-label">Telephone 2 : <span class="star-required1">*</span> </label>
					</div>
					<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
						<input type="tel" class="form-control fsc-input bfh-phone" data-country="countries_states1" data-format="  (999) 999-9999" name='telephoneNo2' id="telephoneNo2" placeholder="(999) 999-9999">
					</div>
					<div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
						<select name="telephoneNo2Type" id="telephoneNo2Type" class="form-control fsc-input" style='height:auto'><option value=''>Type</option>
							<option value='Mobile'>Mobile</option>
							<option value='Home'>Home</option>
							<option value='Work'>Work</option>
							
							<option value='Other'>Other</option>
						</select>
					</div>
<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
						<input type="text" class="form-control fsc-input zip" id="" name="ext2" readonly placeholder="Ext.">
					</div>
				</div>
				
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 2%;">
					<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
						<label class="fsc-form-label">Email : <span class="star-required">*</span> </label>
					</div>
					<div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
						<input type="text" class="form-control fsc-input" id="email" name='email' placeholder="Email Address">
					</div>
				</div>
				
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 2%;">
					<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
						<label class="fsc-form-label">Position : <span class="star-required">*</span></label>
					</div>
					<div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
						<div class="dropdown">
							<select id='requiremnetId' name='requiremnetId' class='form-control  fsc-input' style='height:auto'>
								<option value=''>Select Position You Are Applying For</option>
                                         <?php $__currentLoopData = $employe; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $emp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                          <option value='<?php echo e($emp->id); ?>' <?php if($emp->position_name ==Request::segment(3)): ?> selected <?php endif; ?>><?php echo e($emp->position_name); ?></option>
                                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</select>
						</div>
					</div>
				</div>
				
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 2%;">
					<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
						<label class="fsc-form-label">Resume : <span class="star-required1">*</span></label>
					</div>
					<div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
						<input type="file" class="form-control fsc-input" id="resume" name='resume' placeholder="Select Document">
					</div>
				</div>
				
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 3%;">
					
					<center>
						<button type="submit" class="btn btn-primary btn-lg fsc-form-submit" style="float:none">Save</button> &nbsp&nbsp<a  href="http://financialservicecenter.net/employment" class="btn btn-primary btn-lg fsc-form-submit" style="float:none">Cancel</a>
					</center>
				</div>
				
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
<div class="" id="Register"></div>
				</div>
				
			</form>
		</div>
	</div>
	
</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js"></script>
<script>
$.ajaxSetup({
    headers:
    {
        'X-CSRF-Token': $('input[name="_token"]').val()
    }
});
$(document).ready(function() {

$('#employment').bootstrapValidator({        
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
		fields: {
			firstName: {
                validators: {
                        stringLength: {
                        min: 2,
                    },
                        notEmpty: {
                        message: 'Please Enter Your First Name'
					},
					regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'The First Name can consist of alphabetical characters and spaces only'
                    }
                }
            },
		
             lastName: {
                validators: {
                     stringLength: {
                        min: 2,
                    },
                    notEmpty: {
                        message: 'Please Enter Your Last Name'
					},
					
					regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'The Last name can consist of alphabetical characters and spaces only'
                    }
                }
            },
email: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Your Email Address'
					},				
                    emailAddress: {
                        message: 'Please Enter Your Valid Email Address'
					},					
				
					remote: {
						url: "<?php echo e(URL::to('/check_unique')); ?>",
						data: function(validator) {
							return {
								email: validator.getFieldElements('email').val()
							}
						},
						message: 'This Email Id ALready exit.'
					}
						
                }
            },
            address1: {
                validators: {
                     stringLength: {
						min: 8,
						message: 'Please Enter Your 8 Charactor'
                    },
                    notEmpty: {
                        message: 'Please Enter Your Address'
                    }
                }
            },
		
            city: {
                validators: {
                     stringLength: {
						min: 2,
						
                    },
                    notEmpty: {
                        message: 'Please Enter Your City'
                    },
					regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'The City can consist of alphabetical characters and spaces only'
                    }
                }
            },
            stateId: {
                validators: {
                    notEmpty: {
                        message: 'Please Select Your State'
                    }
                }
            },
			countryId: {
                validators: {
                    notEmpty: {
                        message: 'Please Select Your Country'
                    }
                }
            },
            zip: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter Your Zip Code'
                    }					
                }
            },
			telephoneNo1: {
                validators: {
                    stringLength: {
                        min: 14,
                        
                    },
                    notEmpty: {
                        message: 'Please Enter Your Telephone Number'
                    }, mobile_no: {
                        country: 'USA',
                        message: 'Please supply a vaild Telephone number with area code'
                    }
                }
            },
			/*telephoneNo2: {
                validators: {
                    stringLength: {
                        min: 15,
                        message:'Please enter at least 10 characters and no more than 10'
                    },
                    notEmpty: {
                        message: 'Please Enter Your Telephone Number'
                    }, mobile_no: {
                        country: 'USA',
                        message: 'Please supply a vaild Telephone number with area code'
                    }
                }
            },*/
			requiremnetId: {
                validators: {
                    notEmpty: {
                        message: 'Please Select Your Position'
                    }
                   
                }
            },
     /*resume: {
                validators: {
                    notEmpty: {
                        message: 'Please Select Your Resume'
                    },
                    resume: {
                        extension: 'doc,docx,pdf,zip,rtf',
                        type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf,application/zip',
                        maxSize: 5*1024*1024,
                        message: 'The selected file is not valid, it should be (doc,docx,pdf,zip,rtf) and 5 MB at maximum.'
                    }
                }
                   
                
            },*/
         
            }
        }).on('success.form.bv', function(e) {
            $('#success_message').slideDown({ opacity: "show" }, "slow") // Do something ...
                $('#employment').data('bootstrapValidator').resetForm();
            // Prevent form submission
            e.preventDefault();
            // Get the form instance
            var $form = $(e.target);

            // Get the BootstrapValidator instance
            var bv = $form.data('bootstrapValidator');

            // Use Ajax to submit form data
            $.post($form.attr('action'), $form.serialize(), function(result) {
               // console.log(result);
            }, 'json');
        });
});
$(document).ready(function(){
  /***phone number format***/
  $(".phone").keypress(function (e) {
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
      return false;
    }
    var curchr = this.value.length;
    var curval = $(this).val();
    if (curchr == 3 && curval.indexOf("(") <= -1) {
      $(this).val("(" + curval + ")" + " ");
    } else if (curchr == 4 && curval.indexOf("(") > -1) {
      $(this).val(curval + ")-");
    } else if (curchr == 5 && curval.indexOf(")") > -1) {
      $(this).val(curval + "-");
    } else if (curchr == 9) {
      $(this).val(curval + "-");
      $(this).attr('maxlength', '14');
    }
  });
});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front-section.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>