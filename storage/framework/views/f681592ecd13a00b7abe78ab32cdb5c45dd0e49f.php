<!--<style>
    .search-btn {
    position: absolute;
    top: 46px;
    right: 16px;
    background:transparent;
    border:transparent;
}

</style>
<div class="content-wrapper">
	<div class="page-title">
		<h1>Work Status</h1>
	</div>
	<div class="row"> 

<div class="col-md-12">
			<div class="box box-success">
			      <div class="box-header">
                   <div class="col-md-9" style="margin-left:-1.6%">
              <div class="row">
              <div class="col-md-2">
                  <div class="col-md-"><label style="margin-left:2%;margin-top: 11px;">Filter: </label></div>
                <select name="choice" style="width: 92%;margin-left: 4px;" id="choice" class="form-control">
        <option value="1">All</option>
        <option value="2" selected="">Active</option>
        <option value="3">Inactive</option>
        <option value="4">Client Id</option>
        <option value="5">Client Name</option>
       </select>
    </div>
       <div class="col-md-4">
         <div class="col-md-1"><label style="margin-top: 11px;">Search: </label></div>
    <select name="types" style="width: 92%;margin-left: 4px;" id="types" class="form-control">
        <option value="All" selected="">All</option>
        <option value="Type">Client ID</option>
        <option value="EE / User ID">Company Name</option>
        <option value="Employee Name">Bussiness Name</option>
        <option value="Email ID">Bussiness Telephone</option>
        <option value="Tel. Number">Contact Name</option>
        <option value="Contact Telephone">Contact Telephone</option>
    </select>
    </div>
     <div class="col-md-3">
            <div class="col-md-1"><label style="margin-top: 11px;"> &nbsp;</label></div>
     <table style="width: 100%; margin: 0 auto 2em auto;" cellspacing="0" cellpadding="3" border="0">
        <tbody>
            <tr id="filter_global">
                <td align="center"><input type="text" class="global_filter form-control" id="global_filter" placeholder="All Search"><button class="search-btn"><i class="fa fa-search"></i></button></td>
            </tr>
            <tr id="filter_col2" data-column="1" style="display:none">
                <td align="center"><input type="text" class="column_filter form-control" id="col1_filter" placeholder="Client ID"><button class="search-btn"><i class="fa fa-search"></i></button></td>
            </tr>
            <tr id="filter_col3" data-column="2" style="display:none">
                <td align="center"><input type="text" class="column_filter form-control" id="col2_filter" placeholder="Company Name"><button class="search-btn"><i class="fa fa-search"></i></button></td>
            </tr>
            <tr id="filter_col4" data-column="3" style="display:none">
                <td align="center"><input type="text" class="column_filter form-control" id="col3_filter" placeholder="Bussiness Name"><button class="search-btn"><i class="fa fa-search"></i></button></td>
            </tr>
            <tr id="filter_col5" data-column="4" style="display:none">
                <td align="center"><input type="text" class="column_filter form-control" id="col4_filter" placeholder="Bussiness Telephone"><button class="search-btn"><i class="fa fa-search"></i></button></td>
            </tr>
            <tr id="filter_col6" data-column="5" style="display:none">
                <td align="center"><input type="text" class="column_filter form-control" id="col5_filter" placeholder="Contact Name"><button class="search-btn"><i class="fa fa-search"></i></button></td>
            </tr>
            <tr id="filter_col7" data-column="6" style="display:none">
                <td align="center"><input type="text" class="column_filter form-control" id="col6_filter" placeholder="Contact Telephone"><button class="search-btn"><i class="fa fa-search"></i></button></td>
            </tr>
        </tbody>
    </table>
    </div>
    </div>
              </div>
             	
            </div>
				<div class="col-md-12">
					<div class="table-title">
					</div>
										<div class="table-responsive">
						<div id="example_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer"><div class="dt-buttons" style="margin-bottom:10px;">          <button class="dt-button buttons-copy buttons-html5" tabindex="0" aria-controls="example"><span><i class="fa fa-files-o"></i> &nbsp; Copy</span></button> <button class="dt-button buttons-excel buttons-html5" tabindex="0" aria-controls="example"><span><i class="fa fa-file-excel-o"></i>&nbsp; Excel</span></button> <button class="dt-button buttons-csv buttons-html5" tabindex="0" aria-controls="example"><span><i class="fa fa-file-text-o"></i> &nbsp; CSV</span></button> <button class="dt-button buttons-pdf buttons-html5" tabindex="0" aria-controls="example"><span><i class="fa fa-file-pdf-o"></i>&nbsp;  PDF</span></button> <button class="dt-button buttons-print" tabindex="0" aria-controls="example"><span><i class="fa fa-print"></i>&nbsp; Print</span></button> </div><div id="example_filter" class="dataTables_filter"></div></div><table class="table table-hover table-bordered dataTable no-footer" id="example" role="grid" aria-describedby="example_info">
							<thead>
								<tr role="row"><th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 19px;" aria-label="No: activate to sort column ascending">No</th><th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 70px;" aria-label="Client ID: activate to sort column ascending">Client ID</th><th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 185.933px;" aria-label="Company Name (Legal Name): activate to sort column descending" aria-sort="ascending">Client Legal Name <br><span style="font-size: 13px;">(Legal Name)</span></th><th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 65px;" aria-label="Action: activate to sort column ascending">Action</th></tr>
							</thead>							
							<tbody>
	
														<tr role="row" class="odd">
									<td style="text-align:center">1</td>
									<td>GA-214-1</td>
									<td class="sorting_1">Ashish of Gwinnett, Inc.</td>
								    <td style="text-align:center;">                                     <a class="btn-action btn-view-edit btn-primary" style="background:#367fa9 !important" href="https://financialservicecenter.net/fac-Bhavesh-0554/customer/73/edit"><i class="fa fa-edit"></i></a>
									<a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record ?'))
                                                                                  {event.preventDefault();document.getElementById('delete-id-73').submit();} else{event.preventDefault();}" href="https://financialservicecenter.net/fac-Bhavesh-0554/customer/73"><i class="fa fa-trash"></i></a>
<form action="https://financialservicecenter.net/fac-Bhavesh-0554/customer/73" method="post" style="display:none" id="delete-id-73">
                                        <input type="hidden" name="_token" value="UIc72HzKfEcynHkwpo8DmzSmaVrzZquPqmhXJjIr"> <input type="hidden" name="_method" value="DELETE">
                                        </form></td>
								</tr><tr role="row" class="even">
									
								</tr></tbody>
						</table><div class="dataTables_info" id="example_info" role="status" aria-live="polite">Showing 1 to 10 of 11 entries </div><div class="dataTables_paginate paging_simple_numbers" id="example_paginate"><ul class="pagination"><li class="paginate_button previous disabled" id="example_previous"><a href="#" aria-controls="example" data-dt-idx="0" tabindex="0">Previous</a></li><li class="paginate_button active"><a href="#" aria-controls="example" data-dt-idx="1" tabindex="0">1</a></li><li class="paginate_button "><a href="#" aria-controls="example" data-dt-idx="2" tabindex="0">2</a></li><li class="paginate_button next" id="example_next"><a href="#" aria-controls="example" data-dt-idx="3" tabindex="0">Next</a></li></ul></div></div>
					</div>
				</div>
			</div>
		</div>
		</div>
		</div>!-->



<?php $__env->startSection('main-content'); ?>
<style>
    .search-btn {
    position: absolute;
    top: 46px;
    right: 16px;
    background:transparent;
    border:transparent;
}
section.content-header.page-title {
    padding: 7px 12px 4px !important;
    height: auto !important;
}
.new_images_sec .col-md-4{position:relative;}
.new_images_sec .col-md-4 .arrow{position:absolute; top:16px; right:-6px;}
.new_images_sec .col-md-4{position:relative;}
.new_images_sec .col-md-4 img{height:53px!important;}
.new_images_sec .col-md-4 .arrow{position:absolute; top:16px; right:-6px;}
.new_images_sec .col-md-4.lastimgbox .arrow{position:absolute; top:10px; left:-6px;}
.searchboxmain{float: left;  display: FLEX;  margin-top: -6px; justify-content: space-between;}
.searchboxmain .form-control{    margin-right: 10px!important;
    margin-top: 5px;}
.clear{clear:both;}
.page-title{position:relative;}
.page-title .btn.btn-success{position:absolute; top:5px; right:15px;}
.btn-default {
    border-color: #286090;
}
</style>

<?php 
if(isset($common->filename)!='')
{
?>
<style>
.page-title h1{float:left; margin-left:17%;}

</style>

<?php 
}
?>

<style>
.page-title {
   padding:5px 15px 0px!important
}
/**************** dropdown list ********************/    
    
#countryList{margin:40px 0px 0px 0px;
    padding:0px 0px;
    border: 0px solid #ccc;
    max-height: 200px;
    overflow: auto; position: absolute;
    width: 360px;
    z-index: 99999;}

#countryList li {
    border-bottom: 1px solid #025b90;
    background: #ef7c30;
}
.searchboxmain {
    margin-top: -8px;
}
#countryList li a{padding:0px !important; display:block; color:#fff!important; height:40px;margin: 0px !important;}
#countryList li a span.clientalign{display: flex;
    width: 100px;
    float: left;
    line-height: 15px;
    padding: 4px 0px;
    flex-direction: row;
    justify-content: flex-start;
    align-items: center;
    align-content: center;
    min-height: 40px; background:#038ee0;
    margin-left: 0;
    padding-left: 5px;}
    
    #countryList li a span.entityname{display: flex;
    width:230px;
    float: left;
    line-height: 15px;
    padding: 4px 0px; font-size:13px;
    flex-direction: row;
    justify-content: flex-start;
    align-items: center;
    align-content: center;
    min-height: 38px;
    margin-left: 10px;}
.nav.nav-tabs li{width:15.8%!important;}
.clear{clear:both;}
.mt25{margin-top:25px;}
.text-center{text-align:center!important;}
.text-right{text-align:right!important;}
.table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th{padding:6px 8px 8px 8px!important;}
.pointernone{pointer-events:none}
.pad20{padding:20px;}
.nav-tabs > li.active > a.yel, .nav-tabs > li.active > a.yel:focus, .nav-tabs > li.active > a.yel:hover {
       cursor:default;
}
.nav-tabs > li{
    margin: 0px 0 0 8px;
}
.nav > li > a.yel:hover, .nav > li > a.yel:active, .nav > li > a.yel:focus {
    border-color:#000 !important;
    color:#000 !important;
    background:#ffff99;
    border-radius: 5px !important;
}
.nav-tabs {
    padding: 12px;
    border: 1px solid #3598dc !important;
}
.btnaddmore{background: #337ab7; display:inline-block; margin-bottom:5px;
    padding: 6px 10px;
    color: #fff;
    border-radius: 4px}
.btnremove{background: #ff0000;
    padding: 6px 10px;
    color: #fff;
    border-radius: 4px}
.padzero{padding:0px!important;}
.officermainbox{border:1px solid #ccc; padding:20px; margin-bottom:30px;}
.table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
    padding: 6px 8px 8px 8px !important;
}
.table-bordered, .table-bordered > tbody > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > td, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > thead > tr > th
{border-color:#ccc!important;}
.custom-file-upload {
  border: 1px solid #ccc;
  display: inline-block;
  padding: 6px 12px;
  cursor: pointer;
}
.card ul li{width:24% !important;}
   .card ul.test li a{background:#ffcc66 !important;}
   .card ul.test li.active a{background:#00a0e3 !important;}
   .card ul li a{display: block;width: 100%;color: #333;text-transform: capitalize;background: linear-gradient(180deg, #fdff9a 30%, #e3e449 70%);border: 1px solid #979800 !important;}
   .card ul li a:hover{color: #333;
   background: linear-gradient(180deg, #fbff37 30%, #d3d40e 70%);border: 1px solid #333 !important;}
   .card ul li.active{color: #333;background: linear-gradient(180deg, #fbff37 30%, #d3d40e 70%);border: 1px solid #333 !important;}
   .card ul li a.active, .card ul li a:hover, .card ul li.active a:hover{color: #fff!important;background: #12186b!important;border: 1px solid #12186b !important;}
   .card .nav-tabs{border:0px!important;}
.feeschargesbox .form-control{text-align:right;}
.hrdivider{border-bottom: 2px solid #ccc!important; width: 100%; height: 1px; margin-top: 33px; }
.hrdivider2{margin-top: 14px;   margin-bottom: 30px; border-bottom: 2px solid #ccc!important; width: 100%; height: 1px;}
.officerchange .form-control{background:#fff!important;}
.add-row{background: #007bff; padding: 3px 5px;  color: #fff;  border-radius: 3px; cursor: pointer;}
.delete-row{background:#dc3545; padding: 3px 5px;  color: #fff;  border-radius: 3px; cursor: pointer;}
.nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover {
    color: #FFF !important;
    border: 1px solid #12186b!important;
    background: #12186b!important;
    cursor: default;
    border-radius: 8px;
}
.btn-success{
background:#e67300 !important;    
}
.btn-success:hover{
background:#e67300 !important;    
}
.newcmpname {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    height: 77px;
}
.toggleswitch{float:left; margin-right:15px;}
.newprospects label{text-align:right; padding-top:6px; text-align:right!important; width:100%;}
.newprospects{ padding:15px;}
/*.imgicon{background: #fff; display: block;  width:35px; float: left;  margin-right: 10px; float:left; margin-right:10px; border-radius:2px; padding:3px; border:1px solid #12186b;}*/
.imgicon img{max-width:100%;}
@media  only screen and (max-width : 450px) {
    .btn{
        padding:6px 7px;
    }
    .redius{
        width:36px !important;
    }
}
@media  only screen and (max-width: 1130px){
    .searchboxmain {
        margin-left: 0%;
        width: 33%;
        padding-top: 0px;
        position: absolute !important;
        float: left !important;
        margin-top: -6px !important;
    }
    input#country_name {
        width: 100% !important;
    }
}
@media  only screen and (max-width: 630px){
    .searchboxmain {
        margin-left: 0%;
    width: 96%;
    padding-top: 0px;
    position: relative !important;
    float: initial !important;
    }
    input#country_name {
        width: 100% !important;
    }
}
</style>
<div class="content-wrapper">
   
	<section class="page-title content-header">
	    <span class="imgicon">
	        <!--<img src="https://www.financialservicecenter.net/public/images/2.png" alt="img" style="width:50px !important;float:left;">-->
	        	    <?php if(session()->has('success')): ?>
                           <div class="alert alert-success alert-dismissable"><?php echo e(session()->get('success')); ?></div>
                    <?php endif; ?>
                    	<?php if(session()->has('error')): ?>
                           <div class="alert alert-danger alert-dismissable"><?php echo e(session()->get('error')); ?></div>
                    <?php endif; ?>
             </span>
	        
	     <div class="btn-group btn-toggle toggleswitch" style="z-index:100">
	               <a href="<?php echo e(route('customer.create')); ?>" class="btn btn-warning pull-right" style="height:auto !important;">New Client</a>
	     <?php if(empty($common->filename)): ?>
            <button class="btn btn-default" onClick="nopress();" style="margin-right:7px;">New prospect work</button>
         <?php endif; ?>    
            <button class="btn btn-primary active"  onClick="noonepress();" style="margin-right:6px;">New work</button>
            
          </div>
  
	    <div class="searchboxmain" style="position:Absolute;z-index:100;">
	         <input type="text"  name="search" id="country_name" class="form-control" placeholder="Search Client">	 
                 <?php echo e(csrf_field()); ?>

                 <ul id="countryList"></ul>
                  <a class="btn-action btn-view-edit btn-primary" style="background: #367fa9 !important;padding: 11px 15px !important;margin-top: 5px;" href="https://financialservicecenter.net/fac-Bhavesh-0554/worknew" style="background: #367fa9 !important;padding: 10px 20px;">Reset</a>
	        
	    </div>
	                    
	<div style="text-align:center !important;font-size:22px !important;font-weight: 600;display: inline-block;position:absolute;" class="work_new_center">New Work / New Prospect Work</h1></div>
		<div class="clear"></div>
	</section>
	
	<div  class="box box-success mbtmzero">
	   <!-- <div class="btn btn-success pull-right" style="margin-top: 10px !important;margin-bottom: 10px; margin-right:20px;">
		    <a style="color:#fff;" href="<?php echo e(url('fac-Bhavesh-0554/customer/create')); ?>">Add Client</a></div>!-->
		    <div class="clearfix"></div>
	    <div class="newprospects" style="display:none">
	        <form method="post" action="https://financialservicecenter.net/fac-Bhavesh-0554/worknew/fetch2" enctype="multipart/form-data">
                <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                <input type="hidden" name="adminid" value="1">
	        
	            <div class="row form-group">
    	            <div class="col-md-3">
    	                <label class="control-label">Name : </label>
    	            </div>
    	            <div class="col-lg-1 col-md-2 col-sm-2 col-xs-3">
    	                <select class="form-control" name="worknew_petname">
    	                    <option>Mr.</option>
    	                    <option>Mrs.</option>
    	                    <option>Miss.</option>
    	               </select>
    	            </div>
    	            
    	            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-9" style="">
    	                <input type="text" name="worknew_fname" class="form-control" placeholder="First Name"/>
    	            </div>
    	            
    	            <div class="col-lg-1 col-md-1 col-sm-2 col-xs-3" style="">
    	                <input type="text" name="worknew_mname" class="form-control" placeholder="M"/>
    	            </div>
    	            
    	             <div class="col-lg-2 col-md-3 col-sm-4 col-xs-9" style="">
    	                <input type="text" name="worknew_lname" class="form-control" placeholder="Last Name"/>
    	            </div>
    	        </div>
	        
	            <div class="row form-group">
    	            <div class="col-md-3">
    	                <label class="control-label">Telephone No. :</label>
    	            </div>
    	             <div class="col-lg-2 col-md-3 col-sm-6 col-xs-7">
    	                <input type="tel" name="worknew_telephone" id="worknew_telephone" class="form-control" placeholder=""/>
    	            </div>
    	            <div class="col-lg-2 col-md-3 col-sm-6 col-xs-5" style="">
    	                <select class="form-control">
    	                    <option>Office</option>
    	                    <option>Mobile</option>
    	                    <option>Home</option>
	                    </select>
    	            </div>
    	        </div>
	        
	            <div class="row form-group">
    	            <div class="col-md-3 ">
    	                <label class="control-label">Email :</label>
    	            </div>
    	             <div class="col-lg-3 col-md-4 col-sm-12">
    	                <input type="email" name="worknew_email" class="form-control" placeholder=""/>
    	            </div>
    	        </div>
	        
	            <div class="row form-group">
                    <div class="col-md-3">
                        <label class="control-label">Work Category : <span class="star-required">*</span></label>
                    </div>  
                    <div class="one_line">
                        <div class="col-lg-3 col-md-4 col-sm-12 col-xs-10">
        					<select name="worknew_category" id=""  onchange="myFunction(this)" class="js-example-tags  form-control fsc-input category1 nametype">
            					<option value=''>---Select Work Category---</option>
            					<?php $__currentLoopData = $workcategory; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            					<option value='<?php echo e($cate->id); ?>' ><?php echo e($cate->category_name); ?></option>
            					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        					</select>
        				</div>
        				<?php if($errors->has('worknew_category')): ?>
        					<span class="help-block">
        						<strong><?php echo e($errors->first('worknew_category')); ?></strong>
        					</span>
        				<?php endif; ?>
    				   <div class="col-md-3 col-sm-3 col-xs-3" style="padding:0px;">
    				       <a href="#" data-toggle="modal" data-target="#basicExampleModal" class="redius btn btn-primary" style="width:40px;margin-top:2px;"><i class="fa fa-plus"></i></a>&nbsp;
    				       <a href="#" data-toggle="modal" data-target="#basicExampleModal3" class="redius btn btn-primary" style="width:40px;margin-top:2px;"><i class="fa fa-minus"></i></a> 
    			       </div>
			       </div>
				</div>
									
                <div class="row form-group resi_finance" id="" style="display:none;">
                    <div class="col-md-3">
                        <label class="control-label">Work Type :</label>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6"> 
                        <select class="form-control fsc-input"  name="worknew_type">
                            <option value="">Select</option>
                            <option value="Purchase">Purchase</option>
                            <option value="Refinance">Refinance</option>
                        </select>
                    </div>
                     
                </div>
                                
                <div class="row form-group com_finance" id="" style="display:none;">
                    <div class="col-md-3">
                        <label class="control-label">Work Type :</label>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6"> 
                            <select class="form-control fsc-input" name="worknew_type">
                                <option value="">Select</option>
                             <option value="Purchase">Purchase</option>
                           <option value="Refinance">Refinance</option>
                      </select>
                    </div>
                </div>
                                
                <div class="row form-group corp_llc" id="" style="display:none;">
                    <div class="col-md-3">
                                   
                        <label class="control-label">Work Type :</label>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6"> 
                        <select class="form-control fsc-input" name="worknew_type">
                            <option value="">Select</option>
                            <option value="Create New Corp/ LLC">Create New Corp/ LLC</option>
                            <option value="Update Corp / LLC">Update Corp / LLC</option>
                            <option value="Create Documents">Create Documents</option>
                        </select>
                    </div>
                </div>
                                
                <div class="row form-group license" id="" style="display:none;">
                    <div class="col-md-3">
                        <label class="control-label">Work Type :</label>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6"> 
                        <select class="form-control fsc-input" name="worknew_type">
                            <option value="">Select</option>
                            <option value="Renewal License">Renewal License</option>
                            <option value="New License">New License</option>
                            <option value="Update License">Update License</option>
                        </select>
                    </div>
                    
                </div>
                                
                <div class="row form-group insurance" id="" style="display:none;">
                    <div class="col-md-3">
                        <label class="control-label">Work Type :</label>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6"> 
                        <select class="form-control fsc-input" name="worknew_type">
                            <option value="">Select</option>
                            <option value="Life Insurance">Life Insurance</option>
                            <option value="Health Insurance">Health Insurance</option>
                            <option value="Auto Insurance">Auto Insurance</option>
                            <option value="Home Insurance">Home Insurance</option>
                            <option value="Business Insurance">Business Insurance</option>
                            <option value="Other Insurance">Other Insurance</option>
                        </select>
                    </div>
                </div>
                                
                <div class="row form-group">
                    <div class="col-md-3">
                        <label class="control-label">Work Priority :</label>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6"> 
                    <select class="form-control fsc-input" name="worknew_priority" required>
                        <option value="">Select</option>
                       <option value="Regular"> Regular</option>
                       <option value="Immediately">Immediately</option>
                       <option value="Urgent">Urgent</option>
                    </select>
                    </div>
                 </div>
                                     
                <div class="row form-group">
                    <div class="col-md-3">
                        <label class="control-label">Due Date :</label>
                    </div>
                        <div class="col-lg-3 col-md-4 col-sm-6"> 
                        <input type="text" class="form-control datepicker" id="" name="worknew_duedate"/>
                    </div>
                </div>
                                
                <div class="row form-group">
                    <div class="col-md-3">
                        <label class="control-label">Responsible Person :</label>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6"> 
                        <select class="form-control fsc-input" name="worknew_emp" required>
                            <option value="">Select</option>
                            <?php
                            foreach($empname as $emp)
                            {
                            ?>
                            <option value="<?php echo $emp->employee_id;?>"><?php echo $emp->firstName.' '.$emp->middleName.' '.$emp->lastName;?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                            
                <div class="row form-group">
                    <div class="col-md-3">
                        <label class="control-label">Work To Do :</label>
                    </div>
                     <div class="col-md-6 col-sm-12"> 
                       <textarea class="form-control" name="worknew_details" style="height:100px;"></textarea>
                     </div>
                 </div>
                            
                <div class="row form-group">
                    <div class="col-md-3">
                        <label class="control-label text-right">Work Note :</label>
                    </div>
                     <div class="col-md-6 col-sm-12"> 
                       <textarea class="form-control" name="worknew_note" style="height:100px;"></textarea>
                     </div>
                 </div>
                                
	            <div class="row form-group">
	                <div class="col-sm-12" style="padding-left:5px;">
						<div class="col-md-2 col-md-offset-3 col-sm-4 col-xs-6">											
						    <input class="btn_new_save btn-primary" type="submit" value="Save"></div>
						<div class="col-md-2 col-sm-4 col-xs-6 ">	
						    <a class="btn_new_cancel" href="https://financialservicecenter.net/fac-Bhavesh-0554/employee">Cancel</a> 
						</div>
				    </div>
	            </div>
	        
	        </form>
	    </div>
	<?php if(!empty($common->filename)): ?>
	<div class="row">
	    <div class="col-md-12">
	        <div>
	             <section class="content-header" style="margin-bottom:10px;padding-top:10px !important; height:90px;">
                        <div class="new-page-title" style="padding:0px;margin: -7px 0 7px 0;">
             <div class="new-page-title-tab">
                <div class="col-md-2 col-sm-2 col-xs-12">
                   <div class="new_client_id">
                      <p><?php echo e($common->filename); ?> </p>
                   </div>
                </div>
                <?php if($common->business_id=='6'): ?>
                <div class="col-md-5 col-sm-5 col-xs-12">
                
                    <div class="new_company_name" style="margin-top:25px !important;">
                        <label> Name :</label> <?php echo e(ucwords($common->nametype)); ?>   <?php echo e($common->first_name); ?> <?php echo e($common->middle_name); ?> <?php echo e($common->last_name); ?>  </div></div>
                      <?php else: ?>
             
                    <div class="col-md-5 col-sm-5 col-xs-12">
                        <div class="newcmpname">
                           <div class="new_company_name">
                              <label>Company Name :</label>
                              <p><?php echo e($common->company_name); ?></p>
                           </div>
                           <div class="new_company_name">
                              <label>Business Name :</label>
                              <p><?php echo e($common->business_name); ?></p>
                           </div>
                      </div>
                </div>
                <?php endif; ?>
                <div class="col-md-5 col-sm-5 col-xs-12">
                   <div class="new_images_sec">
                      <?php if(empty($common->business_cat_id)): ?> 
                      <div class="col-md-12 col-sm-12 col-xs-12">
                         <?php else: ?> 
                         <?php if(empty($common->business_brand_category_id)): ?>
                         <div class="col-md-4 col-sm-12 col-xs-12">
                            <?php else: ?> 
                            <div class="col-md-4 col-sm-12 col-xs-12"> 
                               <?php endif; ?>
                               <?php endif; ?>
                               <?php $__currentLoopData = $business; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $busi): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                               <?php if($busi->id==$common->business_id): ?>
                               <img src="<?php echo e(url('public/frontcss/images/')); ?>/<?php echo e($busi->newimage); ?>" class="img-responsive"  style="height:53px!important;">
                              
                            </div>
                            <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php $__currentLoopData = $category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>                                        
                            <?php if($common->business_cat_id==$cate->id): ?>
                            <?php if(empty($common->business_brand_category_id)): ?>
                            <div class="arrow" style="float:left; margin-top:16px;"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></div>
                            <div class="col-md-4 col-sm-12 col-xs-12">
                               <?php else: ?> 
                             <!--  <div class="arrow">
                                  <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                               </div>-->
                               <div class="col-md-4 col-sm-12 col-xs-12"> 
                                  <?php endif; ?>
                                  
                                  <img src="<?php echo e(url('public/category')); ?>/<?php echo e($cate->business_cat_image); ?>" alt="" class="img-responsive" style="height:53px!important;"/>
                               </div>
                               <?php endif; ?>
                               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                               <?php $__currentLoopData = $cb; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bb1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>                                     
                               <?php if($common->business_brand_category_id == $bb1->id): ?>
                               
                               <div class="col-md-4 col-sm-12 col-xs-12 lastimgbox">
                                   <div class="arrow">
                                  <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                               </div>
                                  <img src="<?php echo e(url('public/businessbrandcategory')); ?>/<?php echo e($bb1->business_brand_category_image); ?>" alt="" class="img-responsive" style="height:53px!important;"/>
                               </div>
                               <?php endif; ?>
                               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                         </div>
                      </div>
                      </div>
       </section>
       
        <div style="background:#ffffff; border:1px solid #337ab7; width:97%; margin:0px 15px;">
            <div class="card-body col-md-offset-1" >
            	<div class="row"> 
                    <form method="post" action="https://financialservicecenter.net/fac-Bhavesh-0554/worknew/fetch1" enctype="multipart/form-data">
                     <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                      <input type="hidden" name="clientsid" value="<?php echo $common->cid;?>">
               
            	                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 <?php echo e($errors->has('firstName') ? ' has-error' : ''); ?><?php echo e($errors->has('middleName') ? ' has-error' : ''); ?><?php echo e($errors->has('lastName') ? ' has-error' : ''); ?>">
                                    <!-- <div class="form-group" style="margin-top:15px;">
                                       <label class="col-lg-2 control-label"></label>
                                          <div class="row">
                                             <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                        <input type="radio" id="worknew_prospect" name="worknew_prospect" value="New Work" <?php if($common->worknew_prospect =='New Work'): ?> checked <?php endif; ?>>
                                        <label for="male">New Work</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="worknew_prospect" name="worknew_prospect" value="New Prospect" <?php if($common->worknew_prospect =='New Prospect'): ?> checked <?php endif; ?>>
                                        <label for="male">New Prospect</label>
                                             </div>
                                          </div>
                                       
                                    </div>
                                   !-->
                                    <div class="form-group" style="margin-top:20px;">
                                         <div class="row">
                                       <label class="col-lg-2 control-label text-right" style=" padding-right: 7px; margin-top: 7px;">Work Category : <span class="star-required">*</span></label>
                                         
                                            <!-- <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                <select class="form-control fsc-input" id="nametype" name="worknew_category" onchange="myFunction(this)" required>
                                                    <option value="">Select</option>
                                                   <option value="AccountingWork">Accounting Work</option>
                                                   <option value="TaxationWork">Taxation Work</option>
                                                   <option value="ResidentialFinance">Residential Finance</option>
                                                   <option value="CommercialFinance">Commercial Finance</option>
                                                   <option value="Corporation/LLC">Corporation / LLC</option>
                                                   <option value="License">License</option>
                                                   <option value="Insurance">Insurance</option>
                                                   <option value="Other Service">Other Service</option>
                                                </select>
                                             </div>!-->
                                             
                                             	<div class="col-md-3 ac" style="width: 34.1%;">
        											<div class="row">
        											  	<div class="col-md-12">
        													<select name="worknew_category" id=""  onchange="myFunction(this)" class="js-example-tags  form-control fsc-input category1 nametype">
        													<option value=''>---Select Work Category---</option>
        													<?php $__currentLoopData = $workcategory; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        													<option value='<?php echo e($cate->id); ?>' ><?php echo e($cate->category_name); ?></option>
        													<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        													</select>
        												</div>
        											</div>
        											<?php if($errors->has('worknew_category')): ?>
                										<span class="help-block">
                											<strong><?php echo e($errors->first('worknew_category')); ?></strong>
                										</span>
                									<?php endif; ?>
        										</div>
        											<div class="col-md-3"><a href="#" data-toggle="modal" data-target="#basicExampleModal" class="redius"><i class="fa fa-plus"></i></a>&nbsp;&nbsp;&nbsp; <a href="#" data-toggle="modal" data-target="#basicExampleModal3" class="redius"><i class="fa fa-minus"></i></a> </div>
                                      
                                          </div>
                                          </div>
                                          <div class="form-group resi_finance" id="" style="display:none;">
                                            <div class="row">
                                                <label class="col-lg-2 control-label text-right">Work Type :</label>
                                                 <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin"> 
                                                    <select class="form-control fsc-input"  name="worknew_type">
                                                        <option value="">Select</option>
                                                   <option value="Purchase">Purchase</option>
                                                   <option value="Refinance">Refinance</option>
                                                </select>
                                                 </div>
                                             </div>
                                        </div>
                                        
                                         <div class="form-group com_finance" id="" style="display:none;">
                                            <div class="row">
                                                <label class="col-lg-2 control-label text-right">Work Type :</label>
                                                 <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin"> 
                                                    <select class="form-control fsc-input" name="worknew_type">
                                                        <option value="">Select</option>
                                                     <option value="Purchase">Purchase</option>
                                                   <option value="Refinance">Refinance</option>
                                              </select>
                                                 </div>
                                             </div>
                                        </div>
                                        
                                         <div class="form-group corp_llc" id="" style="display:none;">
                                            <div class="row">
                                                <label class="col-lg-2 control-label text-right">Work Type :</label>
                                                 <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin"> 
                                                    <select class="form-control fsc-input" name="worknew_type">
                                                        <option value="">Select</option>
                                                   <option value="Create New Corp/ LLC">Create New Corp/ LLC</option>
                                                   <option value="Update Corp / LLC">Update Corp / LLC</option>
                                                   <option value="Create Documents">Create Documents</option>
                                                </select>
                                                 </div>
                                             </div>
                                        </div>
                                        
                                        <div class="form-group license" id="" style="display:none;">
                                            <div class="row">
                                                <label class="col-lg-2 control-label text-right">Work Type :</label>
                                                 <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin"> 
                                                    <select class="form-control fsc-input" name="worknew_type">
                                                        <option value="">Select</option>
                                                   <option value="Renewal License">Renewal License</option>
                                                   <option value="New License">New License</option>
                                                   <option value="Update License">Update License</option>
                                                </select>
                                                 </div>
                                             </div>
                                        </div>
                                        
                                        
                                         <div class="form-group insurance" id="" style="display:none;">
                                            <div class="row">
                                                <label class="col-lg-2 control-label text-right">Work Type :</label>
                                                 <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin"> 
                                                    <select class="form-control fsc-input" name="worknew_type">
                                                        <option value="">Select</option>
                                                   <option value="Life Insurance">Life Insurance</option>
                                                   <option value="Health Insurance">Health Insurance</option>
                                                   <option value="Auto Insurance">Auto Insurance</option>
                                                    <option value="Home Insurance">Home Insurance</option>
                                                     <option value="Business Insurance">Business Insurance</option>
                                                      <option value="Other Insurance">Other Insurance</option>
                                                </select>
                                                 </div>
                                             </div>
                                        </div>
                                        
                                           <div class="form-group">
                                            <div class="row">
                                                <label class="col-lg-2 control-label text-right" style="margin-top: 7px;">Work Priority :</label>
                                                 <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin"> 
                                                    <select class="form-control fsc-input" name="worknew_priority" required>
                                                        <option value="">Select</option>
                                                   <option value="Regular"> Regular</option>
                                                   <option value="Immediately">Immediately</option>
                                                   <option value="Urgent">Urgent</option>
                                                </select>
                                                 </div>
                                             </div>
                                        </div>
                                        
                                         <div class="form-group">
                                            <div class="row">
                                                <label class="col-lg-2 control-label text-right" style="margin-top: 7px;">Due Date :</label>
                                                 <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin"> 
                                                    <input type="text" class="form-control datepicker" id="" name="worknew_duedate"/>
                                                   
                                                 </div>
                                             </div>
                                        </div>
                                         <div class="form-group">
                                            <div class="row">
                                                <label class="col-lg-2 control-label text-right" style="margin-top: 7px;">Responsible Person :</label>
                                                 <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin"> 
                                                   <select class="form-control fsc-input" name="worknew_emp" required>
                                                   <option value="">Select</option>
                                                   <?php
                                                   foreach($empname as $emp)
                                                   {
                                                   ?>
                                                        <option value="<?php echo $emp->employee_id;?>"><?php echo $emp->firstName.' '.$emp->middleName.' '.$emp->lastName;?></option>
                                                   <?php
                                                   }
                                                   ?>
                                                </select>
                                                 </div>
                                             </div>
                                        </div>
                                        
                                           <div class="form-group">
                                            <div class="row">
                                                <label class="col-lg-2 control-label text-right" style="margin-top: 40px;">Work To Do :</label>
                                                 <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin"> 
                                                   <textarea class="form-control" name="worknew_details" style="height:100px;"></textarea>
                                                 </div>
                                             </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-lg-2 control-label text-right" style="margin-top: 40px;">Work Note :</label>
                                                 <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin"> 
                                                   <textarea class="form-control" name="worknew_note" style="height:100px;"></textarea>
                                                 </div>
                                             </div>
                                        </div>
                                        
                                        
                                        
                                    <br>
                                 <!--    <div class="form-group">
                                       <label class="col-lg-2 control-label">New Prospect : <span class="star-required">*</span></label>
                                          <div class="row">
                                             <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                                <select class="form-control fsc-input" id="nametype" name="nametype">
                                                   <option value="Residence Mortgage">Residence Mortgage</option>
                                                   <option value="Commercial Mortgage">Commercial Mortgage</option>
                                                   <option value="License">License</option>
                                                   <option value="Corporation">Corporation</option>
                                                
                                                </select>
                                             </div>
                                          </div>
                                       
                                    </div>!-->
                                 </div>
                                 
                                 <div class="card-footer" STYLE="margin-left:10px;">
                                    <div class="col-md-1 col-md-offset-2" style="padding-left:0px; margin-left: 17.3%;">
                                       <input class="btn_new_save" type="submit" value="Create">
                                    </div>
                                    <div class="col-md-1" style="padding-left:0px;">
                                       <a class="btn_new_cancel" href="<?php echo e(url('fac-Bhavesh-0554/worknew')); ?>">Cancel</a> 
                                    </div>
                                 </div>
                            </form>
            	</div>
        	</div>	
    	</div>
    
       <?php endif; ?>
    			
    		  <!--</div>-->
    		<!--</div>-->
    	</div>
   </div>
	  </div>
	  </section>
	  </div>
	  </div>
	  </div>
	  <!--</div>-->
	  <!--</div>-->
	  <!--</div>-->
	  <!--</div>-->

	  <script>
      $.ajaxSetup({
           headers: {
               'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
           }
       });
    
      $(function () {
         $('#addopt').click(function () { //alert();
             var newopt = $('#newopt').val();
             if (newopt == '') {
                 alert('Please enter something!');
                 return;
             }
   
            //check if the option value is already in the select box
             $('#vendor_product option').each(function (index) {
                 if ($(this).val() == newopt) {
                     alert('Duplicate option, Please enter new!');
                 }
             })
             $.ajax({
   type: "post",
   url: "<?php echo route('work.workcategorys'); ?>",
   dataType: "json",
   data: $('#ajax').serialize(),
   success: function(data){
       $('#vendor_product').append('<option value=' + newopt + '>' + newopt + '</option>');
       $("#div").load(" #div > *");
      $("#newopt").val('');
   
      alert('Successfully Added');
      location.reload();
     },
   error: function(data){
      alert("Error")
   }
   });
            
              $('#basicExampleModal').modal('hide');
         });
     });
 
</script>
<div class="modal fade" id="basicExampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header" style="background:#038ee0;">
            <h4 class="modal-title" id="exampleModalLabel" style="text-align:center;color:#fff;">Work Category<button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
               </button>
            </h4>
         </div>
         <div class="modal-body" style="background:#ffff99;padding:0px !important;">
            <div class="curency curency_ref" id="div">
                <?php
                    if(isset($common->cid)!='')
                    {
                ?>
               <?php $__currentLoopData = $workcategory; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cur): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
               <div id="cur_<?php echo e($cur->id); ?>" class="col-md-12" style="border:1px solid;background:#ffff99;">
                  <div class="col-md-12" style="margin-top: 7px;margin-bottom:5px;">
                     <a class="delete" style="color:#000;" onclick="return confirm('Are you sure to remove this record?')" href="<?php echo e(route('worknew.destroydworkcat',[$cur->id,$common->cid])); ?>" id="<?php echo e($cur->id); ?>"><?php echo e($cur->category_name); ?>

                     <span class="pull-right"><i class="fa fa-trash btn btn-danger" style="padding:6px!important;"></i></span></a>
                  </div>
               </div>
               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>  
               <?php
                    }
                ?>
            </div>
         </div>
         <div class="modal-footer" style="text-align:center;">
            <button type="button" style="margin-top:10px;border:1px solid;" class="btn btn-secondary" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="basicExampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Workcategory</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <form action="" method="post" id="ajax">
            <?php echo e(csrf_field()); ?>

            <div class="modal-body">
               <input type="text" id="newopt" name="newopt"  class="form-control" placeholder="Workcategory Name"/>
            </div>
            <div class="modal-footer">
               <button type="button" id="addopt" class="btn btn-primary">Save</button>
               <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
         </form>
      </div>
   </div>
</div>

<script>

function myFunction(){
      var catval = $('.nametype').val();;
      
     
     if(catval=='4'){
         $('.resi_finance').show();
     } else {
         $('.resi_finance').hide();
     }
     
      if(catval=='5'){
         $('.com_finance').show();
     } else {
         $('.com_finance').hide();
     }
     
     if(catval=='6'){
         $('.corp_llc').show();
     } else {
         $('.corp_llc').hide();
     }
     
      if(catval=='7'){
         $('.license').show();
     } else {
         $('.license').hide();
     }
     
       if(catval=='8'){
         $('.insurance').show();
     } else {
         $('.insurance').hide();
     }
     
     
     
     
    }
$(document).ready(function(){
    
$("#worknew_telephone").mask("(999) 999-9999");

$(function () {
  $(".datepicker").datepicker({ 
        autoclose: true, 
        todayHighlight: true
  }).datepicker();
});


 $('#country_name').keyup(function(){ 
        var query = $(this).val();
        if(query != '')
        { 
         var _token = $('input[name="_token"]').val();
         $.ajax({
          url:"<?php echo e(route('worknew.fetch')); ?>",
          method:"POST",
          data:{query:query, _token:_token},
          success:function(data){
           $('#countryList').fadeIn();  
                    $('#countryList').html(data);
          }
         });
        }
    });

    $(document).on('click', 'tr', function(){  
        $('#country_name').val($(this).text());  
        $('#countryList').fadeOut();  
    });  
    
   
    

});

$('.btn-toggle').click(function() {
    $(this).find('.btn').toggleClass('active');  
    
    if ($(this).find('.btn-primary').length>0) {
    	$(this).find('.btn').toggleClass('btn-primary');
    }
    if ($(this).find('.btn-danger').length>0) {
    	$(this).find('.btn').toggleClass('btn-danger');
    }
    if ($(this).find('.btn-success').length>0) {
    	$(this).find('.btn').toggleClass('btn-success');
    }
    if ($(this).find('.btn-info').length>0) {
    	$(this).find('.btn').toggleClass('btn-info');
    }
    
    $(this).find('.btn').toggleClass('btn-default');
       
});


function nopress(){
  $('.searchboxmain').hide();
 $('.newprospects').show();
}
function noonepress(){
   $('.searchboxmain').show();
    $('.newprospects').hide();
}
</script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>