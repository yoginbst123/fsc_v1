<?php $__env->startSection('main-content'); ?>
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>Home Content</h1>
    </section>
    <!-- Main content -->
    <section class="content">
<div class="row">
		<div class="col-md-12">
			<div class="box box-success">
			      <div class="box-header">
       
              <div class="box-tools pull-right">
                
              </div>
            </div>
				<div class="col-md-12">
                   
					<form method="post" action="<?php echo e(route('homecontent.update',$homecontent->id)); ?>" class="form-horizontal" id="homecontent" name="homecontent" enctype="multipart/form-data">
					<?php echo e(csrf_field()); ?><?php echo e(method_field('PATCH')); ?>

						<div class="form-group<?php echo e($errors->has('title') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Title :</label>
							<div class="col-md-8">
								<input name="title" type="text" value="<?php echo e($homecontent->title); ?>" id="title" class="form-control"/>								
								<?php if($errors->has('title')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('title')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>						
						<div class="form-group<?php echo e($errors->has('content') ? ' has-error' : ''); ?>">
						<label class="control-label col-md-3">Content :</label>
						<div class="col-md-8">
						
							  <textarea id="editor1" name="content" rows="10" cols="80"><?php echo $homecontent->content; ?></textarea>
					  
							<?php if($errors->has('content')): ?>
									<span class="help-block">
										<strong><?php echo e($errors->first('content')); ?></strong>
									</span>
								<?php endif; ?>	
						</div>
					</div>
						<div class="card-footer">
						<div class="row">
						    <div class="col-md-3"></div>
						   <div class="col-xs-2" style="width:155px;">
								<input class="btn_new_save btn-primary1 primary1" type="button" id="primary1" name="submit" value="Save">
							</div>
							<div class="col-xs-2" style="width:155px;">
								<a class="btn_new_cancel" href="<?php echo e(url('fac-Bhavesh-0554/homecontent')); ?>">Cancel</a> 
							</div>
						</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	    </section>
<!--</div>-->

<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>