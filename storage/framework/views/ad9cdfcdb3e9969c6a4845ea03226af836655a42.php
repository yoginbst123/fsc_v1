<?php $__env->startSection('main-content'); ?>
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>Logo</h1>
    </section>
    <!-- Main content -->
    <section class="content">

	<div class="row">
		<div class="col-md-12">
			<div class="box box-success">
			      <div class="box-header">
          
              <div class="box-tools pull-right">
                
              </div>
            </div>
				<div class="col-md-12">
             <form method="post" action="<?php echo e(route('logo.store')); ?>" class="form-horizontal" id="businessname" name="businessname" enctype="multipart/form-data">
					<?php echo e(csrf_field()); ?>

						<div class="form-group <?php echo e($errors->has('logoname') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Logo Name :</label>
							<div class="col-lg-6 col-md-8">
								<input name="logoname" type="text" id="logoname" class="form-control" value="" />                                                            <?php if($errors->has('logoname')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('logoname')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>
							<div class="form-group <?php echo e($errors->has('logourl') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Logo Url :</label>
							<div class="col-lg-6 col-md-8">
								<input name="logourl" type="text" value="" id="logourl" class="form-control" value="" /><?php if($errors->has('logourl')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('logourl')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>
						<div class="form-group <?php echo e($errors->has('logo') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Logo Image :</label>
							<div class="col-lg-6 col-md-8">
							    <label class="file-upload btn btn-primary">
	                Browse for file ... <input name="logo" style="opecity:0" placeholder="Upload Service Image" id="logo" type="file" value=""/>
	            </label>
							                                              <?php if($errors->has('logo')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('logo')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>
										
						<div class="card-footer">
						    <div class="form-group">
							<label class="control-label col-md-3"></label>
							<div class="col-xs-2" style="width:155px;">
<input class="btn_new_save btn-primary1 primary1" type="submit" id="primary1" name="submit" value="Save">
							</div>
							<div class="col-xs-2" style="width:155px;">
<a class="btn_new_cancel" href="<?php echo e(url('fac-Bhavesh-0554/logo')); ?>">Cancel</a> 
							</div>
						</div>
						  </div>
					
						
					</form>
				</div>
			</div>
		</div>
	</div>
	</section>
<!--</div>-->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>