 

<?php $__env->startSection('main-content'); ?>
<div class="content-wrapper">
 <section class="page-title content-header">
     		<h1>Business Brand</h1>
    </section>
	 <section class="content">
	<div class="row">
		<div class="col-md-12">
				<div class="box box-success">
			      <div class="box-header">
             
              <div class="box-tools pull-right">
                
              </div>
            </div>
				<div class="col-md-12">
		      
					<form method="post" class="form-horizontal" enctype="multipart/form-data" id="" action="<?php echo e(route('business-brand.update',$businessbrand->cid)); ?>">
					<?php echo e(csrf_field()); ?> 
                        <?php echo e(method_field('PATCH')); ?>

						<div class="form-group <?php echo e($errors->has('business_id') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Business Name :</label>
							<div class="col-lg-5 col-md-8">
								<select class="form-control category" id="" name="business_id">									
								   <?php $__currentLoopData = $business; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bus): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>									
											
                                    <?php if($businessbrand->business_id == $bus->id): ?>                                    
									<option value='<?php echo e($businessbrand->business_id); ?>' selected><?php echo e($businessbrand->bussiness_name); ?></option>	
                                    <?php else: ?>
                                    <option value='<?php echo e($bus->id); ?>'><?php echo e($bus->bussiness_name); ?></option>	                                   
                                   <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									
								</select>
                                                                        <?php if($errors->has('business_id')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('business_id')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>
						
						<div class="form-group <?php echo e($errors->has('business_cat_id') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Business Category Name :</label>
							<div class="col-lg-5 col-md-8">
								<select class="form-control category1" id="business_cat_id" name="business_cat_id">								
									<option value='<?php echo e($businessbrand->business_cat_id); ?>' selected><?php echo e($businessbrand->business_cat_name); ?></option>												
								</select>
                                                                <?php if($errors->has('business_cat_id')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('business_cat_id')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>
						
						<div class="form-group <?php echo e($errors->has('business_brand_name') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Business Brand :</label>
							<div class="col-lg-5 col-md-8">
								<input name="business_brand_name" value="<?php echo e($businessbrand->business_brand_name); ?>" type="text" id="business_brand_name" class="form-control" value="" />
 <?php if($errors->has('business_brand_name')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('business_brand_name')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>
						
						<div class="form-group ">
							<label class="control-label col-md-3">Business Brand Image :</label>
							<div class="col-lg-5 col-md-8">
							<label class="file-upload btn btn-primary">
		                Browse for file ... <input name="business_brand_image" style="opecity:0" placeholder="Upload Service Image" id="business_brand_image" type="file">
		            </label>
<img style="margin-left:25px;" src="https://financialservicecenter.net/public/businessbrand/<?php echo e($businessbrand->business_brand_image); ?>" width="100px">
							</div>
						</div><input type="hidden" name="business_brand_image1" id="business_brand_image1" value="<?php echo e($businessbrand->business_brand_image); ?>">
						<div class="form-group <?php echo e($errors->has('business_brand_image') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Business Brand Url :</label>
							<div class="col-md-4">
								<input name="link" type="text" id="link" class="form-control" value="<?php echo e($businessbrand->link); ?>" /><?php if($errors->has('link')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('link')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>
						<div class="card-footer">
						    <div class="form-group">
							<label class="control-label col-md-3"></label>
							<div class="col-xs-2" style="width:155px;">
<input class="btn_new_save btn-primary1" type="submit" name="submit" value="Save">
							</div>
							<div class="col-xs-2" style="width:155px;">
<a class="btn_new_cancel" href="<?php echo e(url('fac-Bhavesh-0554/business-brand')); ?>">Cancel</a> 
							</div>
						</div>
						  </div>
						
					</form>
					</div>
			</div>
		</div>
	</div>
	</section>
<!--</div>-->
<script>
$(document).ready(function(){
	$(document).on('change','.category', function()
	{
		//console.log('htm');
		var id = $(this).val();
		$.get('<?php echo URL::to('getRequest'); ?>?id='+id, function(data)
		{  $('#business_cat_id').empty();
           $.each(data, function(index, subcatobj)
		   {
			   $('#business_cat_id').append('<option value="'+subcatobj.id+'">'+subcatobj.business_cat_name+'</option>');
		   })

		});
			
	});
});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>