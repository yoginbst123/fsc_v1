
<?php $__env->startSection('main-content'); ?>
    <style>

        .tab-content > .tab-pane {
            padding: 5px 0 !important;
        }

        .new_company_name {
            margin: 3px 0 !important;
        }

        .nav-tabs > li {
            /* width: 19.6% !important;*/
            width: 18.6% !important;
            margin: 2px 3px 2px 3px !important;
        }

        .panel.panel-default .panel-heading h4 {
            font-size: 20px !important;
            padding: 10px !important;
            background: #b3e4a6 !important;
        }

        .text-center {
            text-align: center !important;
        }

        .padleftzero {
            padding-left: 0px !important;
        }

        .box-header {
            color: #444;
            display: block;
            padding: 3px !important;
            position: relative;
        }

        .new-page-title {
            margin: 6px 0 6px 0 !important;
            padding: 0 !important;
        }

        .nav-tabs {
            padding: 5px !important;
        }

        .ac_name_first {
            font-size: 17px !important;
        }

        .bg-color {
            background: #286db5;
            width: 100%;
            margin: 0 auto 16px auto !important;
            padding: 5px 0;
            color: #fff;
        }

        .input_fields_wrap_1 {
            display: inline-table;
            width: 100%;
            margin-top: 12px;
        }

        .btn-add {
            z-index: 9999;
            position: absolute;
            left: 70%;
            right: 0;
            bottom: -65px;
            width: 150px;
        }

        .nav-tabs > li > a {
            height: 40px;
            font-size: 16px !important;
            color: #000 !important;
        }

        .tab-content > .tab-pane {
            padding: 20px 0;
        }

        .tess {
            text-align: right;
            position: absolute;
            font-size: 11px;
            margin: 22px 16px 0 0;
            right: 0;
            width: 100%;
        }

        .cc {
            text-align: right;
            position: absolute;
            display: none;
            top: 4px;
            right: 26px;
        }

        .second-pro {
            margin-bottom: 10px;
            border: 2px solid #286db5;
        }

        .Pro-btn {
            background-color: #103b68;
            border-color: #103b68;
            width: 100%;
        }

        .Certificate-btn {
            font-size: 10.5px !important;
        }

        .clients-name-r {
            font-size: 16px;
            margin: 0;
            background-color: #286db5;
            color: #fff;
            padding: 8px;
        }

        .page-title {
            height: 80px;
        }

        .glyphicon-chevron-right:before {
            content: "\e080";
            color: #000;
        }

        .non-profit-img {
            max-width: 70px;
        }

        .pc-row {
            display: none;
        }

        .cc:after {
            content: "\f295";
            font-family: FontAwesome;
        }

        .Branch {
            display: inline-block;
            margin-bottom: 28px;
        }

        .star-required {
            color: red;
            position: absolute;
        }

        .fieldGroup {
            width: 100%;
            display: inline-block;
            border-bottom: 2px solid #512e90;
            padding-bottom: 20px;
        }

        .fieldGroup:last-child {
            border-bottom: transparent;
        }

        .glyphicon-chevron-right {
            top: 15px;
            text-align: center;
            font-size: 2rem;
            right: 9px;
        }

        .btn3d.btn-info {
            background: linear-gradient(#e7f3ff, #74b7fd);
            font-size: 11px;
            padding: 9px 10px;
            color: #000000;
            font-weight: 600;
            border: 1px solid #000;
            width: 100%;
            transition: 0.2s;
            text-align: center;
            box-shadow: 0 4px 5px 0 rgba(0, 0, 0, 0.14), 0 1px 10px 0 rgba(0, 0, 0, 0.12), 0 2px 4px -1px rgba(0, 0, 0, 0.3);
            -webkit-box-shadow: 0 4px 5px 0 rgba(0, 0, 0, 0.14), 0 1px 10px 0 rgba(0, 0, 0, 0.12), 0 2px 4px -1px rgba(0, 0, 0, 0.3);
        }

        .btn3d.btn-info:hover,
        .btn3d.btn-info.active {
            background: linear-gradient(#74b7fd, #e7f3ff);
            transition: 0.2s;
        }

        .field_wrapper {
            float: left;
            width: 100%;
        }

        #field0 {
            margin-bottom: 10px;
            float: left;
            width: 100%;
        }

        .dd {
            border: 1px solid #286db5;
            padding: 10px 0;
            margin-bottom: 10px;
            float: left;
            width: 100%;
        }

        .dd:nth-child(odd) {
            background: #e8f7fd;
        }

        .dd:nth-child(even) {
            background: #fff5dd;
        }

        .form-control-insu {
            border: 2px solid #286db5;
            border-radius: 3px;
            font-weight: normal;
        }

        .share_tabs_main {
            float: left;
            width: 100%;
            margin-bottom: 0px;
        }

        .share_tabs {
            float: left;
            width: 100%;
        }

        .share_tabs_other {
            float: left;
            width: 100%;
        }

        .share_tab {
            float: left;
            margin: 10px 0.8%;
        }

        .share_firstn {
            width: 14%;
        }

        .share_m {
            width: 5%;
        }

        .share_lastn {
            width: 14%;
        }

        .share_position {
            width: 15%;
        }

        .share_persentage {
            width: 14%;
        }

        .share_date {
            width: 15%;
        }

        .share_add {
            float: left;
            width: 7%;
            margin: 10px 1% 0 1%;
        }

        .share_remove {
            float: left;
            width: 7%;
            margin: 15px 1% 0 1%;
        }

        .share_total {
            text-align: right;
            width: 100%;
        }

        .addbtn {
            display: block;
            width: 90px;
            font-size: 14px;
            padding: 5px 0;
            border: 1px solid #359e0b;
            border-radius: 2px;
            vertical-align: top;
        }

        .removebtn,
        .remove {
            display: block;
            width: 90px;
            font-size: 14px;
            padding: 5px 0;
            border: 1px solid #d9534f;
            border-radius: 2px;
            vertical-align: top;
        }

        .arrow {
            width: 0px;
            position: relative;
            float: left;
        }

        .payroll-btn {
            margin-top: 1%;
            text-align: center;
            padding: 7px;
            border-radius: 5px;
            font-size: 17px;
            font-weight: bold;
        }

        .nav-tabs > li.bluebutton,
        .nav-tabs > li.active.bluebutton,
        .nav-tabs > li.bluebutton > a,
        .nav-tabs > li.active.bluebutton > a {
            background: #fff !important;
            border-color: inherit !important;
            overflow: hidden !important;
        }

        .nav-tabs > li.bluebutton:hover,
        .nav-tabs > li.active.bluebutton:hover,
        .nav-tabs > li.active.bluebutton,
        .nav-tabs > li.bluebutton:hover a,
        .nav-tabs > li.active.bluebutton:hover a,
        .nav-tabs > li.active.bluebutton > a {
            background: #12186b !important;
            border-color: #12186b !important;
            overflow: hidden !important;
            color: #fff !important;
        }

        .disablebox input[type="text"],
        .disablebox select,
        .disablebox input[type="email"],
        .disablebox input[type="tel"],
        .disablebox input[type="number"],
        .disablebox textarea {
            background: #eeeeee !important;
            cursor: no-drop !important;
        }

        .disableboxnew input[type="text"],
        .disableboxnew select,
        .disableboxnew input[type="email"],
        .disableboxnew input[type="tel"],
        .disableboxnew input[type="number"],
        .disableboxnew textarea {
            background: #eeeeee !important;
            cursor: no-drop !important;
        }

        .panel.panel-default .panel-heading h4 a:hover,
        .panel.panel-default .panel-heading h4 a {
            color: #103b68 !important;
            width: 100%;
        }

        .ac_name_first {
            float: left;
            width: 41%;
        }

        .ac_name_middel {
            width: 56%;
            float: left;
        }

        #customFields,
        #customFields2,
        #customFields3 {
            margin-top: 20px;
        }

        #customFields tr th,
        #customFields2 tr th,
        #customFields3 tr th {
            background: #cccccc;
            height: 40px;
            padding: 10px !important;
            text-align: left !important;
        }

        #customFields tr,
        #customFields2 tr,
        #customFields3 tr {
            border-bottom: 1px solid #ccc;
        }

        #customFields tr td,
        #customFields2 tr td,
        #customFields3 tr td {
            padding: 5px !important;
            vertical-align: middle;
            font-size: 16px;
        }

        .btnno.btn-success.active {
            background: #ff0000;
            color: #fff;
        }

        .btnno.btn-success {
            background-color: #f4f4f4;
            color: #444;
            border-color: #ddd;
        }

        #customFields tr td:first-child,
        #customFields tr td:last-child {
            text-align: center !important;
        }

        .event {
            pointer-events: none;
        }

        .active-width {
            width: 100px;
            float: right;
            margin-bottom: 0;
            margin-right: 10px !important;
        }

        .service,
        .subscription_hidden,
        .subscription_hidden2,
        .limit_hidden,
        .user_hidden {
            display: none;
        }

        #user_question1 {
            -webkit-appearance: none;
            -moz-appearance: none;
            text-indent: 1px;
            text-overflow: '';
        }

        #user_question2 {
            -webkit-appearance: none;
            -moz-appearance: none;
            text-indent: 1px;
            text-overflow: '';
        }

        #user_question3 {
            -webkit-appearance: none;
            -moz-appearance: none;
            text-indent: 1px;
            text-overflow: '';
        }

        .padleft7 {
            padding-left: 7px !important;
        }

        .padrightt7 {
            padding-right: 7px !important;
        }

        .newcmpname {
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            height: 75px;
        }

        .new_company_name p {
            font-size: 13px !important;
        }

        .table > tbody > tr > td,
        .table > tbody > tr > th,
        .table > tfoot > tr > td,
        .table > tfoot > tr > th,
        .table > thead > tr > td,
        .table > thead > tr > th {
            padding: 6px 8px 8px 8px !important;
        }

        .panel-title .glyphicon-plus {
            display: none;
        }

        .panel-title .collapsed .glyphicon-plus,
        .panel-title .glyphicon-minus {
            display: block;
        }

        .collapsed .glyphicon-minus {
            display: none;
        }

        div.dataTables_wrapper div.dataTables_length select {
            height: 35px !important;
        }

        .form-control,
        .form-control-insu,
        select.greenText,
        .statusselectbox select {
            height: 35px !important;
            border-radius: 3px;
            border: 2px solid #2fa6f2;
            font-size: 16px !important;
            outline: 0;
            color: #000;
            padding: 6px 6px;
        }

        .new_images_sec img {
            height: 53px;
        }

        .Branch {
            margin-bottom: 12px;
        }

        .nav-tabs > li > a {
            background: #36b8ea !important;
            color: black !important;
        }

        .nav-tabs .interviewtab a {
            background: #ff9393 !important;
            border-color: #ff9393 !important;
        }

        .nav-tabs .interviewtab:hover a,
        .nav-tabs .interviewtab.active a {
            background: #f75252 !important;
            border-color: #f75252 !important;
        }

        .pr-0 {
            padding-right: 0px !important;
        }

        .pl-0 {
            padding-left: 0px !important;
        }

        .plusbox {
            position: absolute;
            right: -3px;
            font-size: 15px;
            font-weight: bold;
            top: 37px;
        }

        #renewRecord .form-control {
            margin-bottom: 15px;
        }

        #renewRecord label.text-right {
            text-align: right !important;
            width: 100%;
        }

        .newcmpname {
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
        }

        .Branch .titleleft {
            width: 45%;
            float: left;
            text-align: left;
            padding-left: 15px;
            padding-top: 5px;
        }

        .togglebox {
            float: right;
            margin-right: 10px;
        }

        .togglebox label {
            padding-right: 10px;
        }

        .customfieldsbox {
            border: 1px solid #ccc;
            padding: 15px;
            margin-bottom: 15px;
        }

        .mt30 {
            margin-top: 30px;
        }

        .btn-toggle a {
            margin: 0px 5px;
            border-radius: 4px !important;
        }

        .nav-tabs > li {
            width: 19.4% !important;
        }

        .btnyes.active {
            background-color: #00a65a !important;
            border-color: #008d4c !important;
            color: #fff !important;
        }

        #alerts .modal-width {
            margin: auto;
            width: 390px !important;
            border: #3668f6 1px solid !important;
            border-radius: 0;
        }

        .dividerbox {
            width: 100%;
            height: 2px;
            background: #00a65a;
            margin: 20px 0px;
        }

        .wagesform label {
            text-align: right !important;
            display: block;
        }

        .panel-heading h4.panel-title {
            display: flex;
            flex-direction: row;
            justify-content: space-between;
        }

        ul.nav.nav-tabs.tabs1 {
            padding: 12px;
            background: #fffaf4 !important;
            border: 1px solid #df820f !important;
        }

        ul.nav.nav-tabs.tabs1 > li > a {
            background: #ffe2bf !important;
            color: black !important;
            border: 1px solid #ff9a1e !important;
        }

        ul.nav.nav-tabs.tabs1 > li.active > a,
        ul.nav.nav-tabs.tabs1 > li.active > a:focus,
        ul.nav.nav-tabs.tabs1 > li.active > a:hover {
            border: 1px solid #945201;
            background: #ff9a1e !important;
            color: #ffffff !important;
        }

        .serviceslist {
            display: flex;
            flex-direction: column;
            border: 2px solid #337ab7;
            flex-wrap: nowrap;
            overflow: auto;
            padding: 3px 0px;
            border-radius: 5px;
            margin-bottom: 5px;
        }

        .serviceslist ul {
            list-style: none;
            margin: 0px 0px 0px;
            padding: 0px;
            display: flex;
            flex-wrap: nowrap;
            overflow: auto;
        }

        .serviceslist ul li {
            font-size: 12px;
            color: #fff;
            width: auto;
            margin: 0px 3px;
            white-space: nowrap;
            display: flex;
            flex-direction: column;
            justify-content: space-between;
            border-radius: 40px;
            padding: 0px
        }

        .serviceslist ul li a {
            color: #000;
            white-space: nowrap;
            font-weight: 500;
            font-size: 12px;
        }

        .serviceslist ul li:first-child {
            border-left: 0px;
        }

        .serviceslist ul li .subvalue {
            margin-bottom: 0px !important;
            line-height: 1.9;
            margin: 5px 0px;
            background: #fff4a6;
            height: 30px;
            font-weight: bold;
            padding: 5px 10px;
            border-radius: 25px;
            border: 1px solid #000000;
        }

        .modal-header {
            background: #ffff99 !important;
            text-align: center;
            border-bottom: 2px solid #9c9c46 !important;
        }

        .accrlink {
            text-align: right !important;
        }

        .nav-tabs > li > a.interviewbox {
            background: #ce06a6 !important;
            color: #ffffff !important;
            font-weight: bold !important;
        }

        .nav-tabs > li > a.interviewbox:hover {
            background: #545494 !important;
            color: #ffffff !important;
            border-color: #c30606;
            font-weight: bold !important;
        }

        .subvalue p {
            padding-top: 0px;
            vertical-align: middle;
        }

        .subvalue .new_company_name label {
            width: auto;
            border-radius: 50px 0px 0px 50px;
            padding: 1px 10px;
            height: 28px;
            border-right: 1px solid #000000;
            padding-right: 0px;
            padding-left: 5px;
            margin-right: 0px;
            vertical-align: middle;
        }

        .new_company_name label {
            width: 180px;
        }

        .new_company_name p {
            width: auto;
        }

        .b_type_1 {
            margin-top: 2.5px;
            margin-bottom: 2.5px;
        }

        .b_type_2 {
            margin-top: 2.5px;
            margin-bottom: 2.5px;
        }

        .b_type_3 {
            margin-top: 2.5px;
            margin-bottom: 2.5px;
        }

        .b_type_4 {
            margin-top: 2.5px;
            margin-bottom: 2.5px;
        }

        .Branch h1 {
            /*margin-left: 10px;*/
            /*line-height: 35px;*/
        }

        .professional_profession {
            width: 31%;
        }

        .professional_note {
            width: 38%;
        }

        @media (min-width: 1200px) {
            .ser_currency {
                width: 95px;
            }

            .ser_period {
                width: 140px;
            }
        }

        @media (max-width: 1252px) {
            .nav-tabs > li {
                margin: 2px 2px 2px 2px !important;
            }
        }

        @media (min-width: 1200px) {
            .b_type_2 {
                width: 40%;
            }

            .b_type_3 {
                width: 25% !important;
            }

            .b_type_4 {
                width: 26.5%;
            }
        }

        @media (max-width: 1200px) {
            .b_type_1 {
                width: 40%;
            }

            .b_type_2 {
                width: 60%;
            }

            .b_type_3 {
                width: 25% !important;
            }

            .b_type_4 {
                width: 37%;
            }

            .btn.btn-success.addmore,
            .btn.btn-danger.deletecontact {
                float: right;
                /*margin-top:5px;*/
                margin-right: 15px;
            }
        }

        @media (max-width: 1120px) {
            .nav > li > a {
                padding: 10px 5px;
            }
        }

        @media  only screen and (max-width: 1050px) {
            .professional_profession {
                width: 35%;
            }

            .professional_note {
                width: 68%;
            }

            .professional_state {
                width: 13%;
            }

            .professional_effective {
                width: 21%;
            }

            .professional_license {
                width: 20%;
            }

            .professional_expire {
                width: 25%;
            }

            .professional_btn_cust {
                width: 25% !important;
            }

            .professional_add {
                width: 30px;
            }
        }

        @media (max-width: 1023px) {
            .nav-tabs > li {
                width: 19.6% !important;
                margin: 5px 0 0 2.5px !important;
            }

            .nav > li > a {
                padding: 10px 4px;
                font-size: 15px !important;
            }
        }

        @media (max-width: 991px) {
            .Branch h1 {
                text-align: center;
            }

            .full_991 {
                width: 100% !important;
            }

            #tab14primary .Branch .titleleft {
                width: 100% !important;
                text-align: center !important;
                border-bottom: 1px solid #6d6d6d !important;
            }

            #tab14primary .btn-group.btn-toggle {
                align-items: center !important;
                display: inline-flex !important;
                float: none !important;
                margin: 0px !important;
            }
        }

        @media (max-width: 980px) {
            .nav-tabs > li {
                width: 24.5% !important;
            }

            .nav > li > a {
                padding: 10px 4px;
            }
        }

        @media (max-width: 973px) {
            .ac_name_first {
                width: 39%;
            }
        }

        @media (max-width: 820px) {
            .nav-tabs > li {
                width: 32.6% !important;
            }

            .nav > li > a {
                padding: 10px 4px;
            }
        }

        @media (max-width: 767px) {
            .b_type_1 {
                width: 97%;
                float: left;
            }

            .b_type_2 {
                width: 97%;
                float: left;
            }

            .b_type_3 {
                width: 50% !important;
                float: left;
            }

            .b_type_4 {
                width: 50%;
                float: left;
            }
        }

        @media  only screen and (max-width: 600px) {

            .professional_profession,
            .professional_state,
            .professional_effective,
            .professional_license,
            .professional_expire,
            .professional_note {
                width: 98%;
            }

            .professional_btn_cust {
                width: auto !important;
            }

            .professional_btn_cust a {
                padding: 5px 10px !important;
            }
        }

        @media (max-width: 550px) {
            .ac_name_first {
                width: 100%;
                text-align: center;
            }

            .ac_name_middel {
                width: 94%;
                text-align: center;
            }
        }

        @media (max-width: 450px) {
            .nav-tabs > li {
                width: 49% !important;
            }
        }

        p.clientsetup {
            background: aliceblue;
            border-radius: 50px;
            color: #006bf5;
            margin: 0px 2px 0px 2px;
            border: 1px solid #000000;
            width: 24px !important;
            height: 24px !important;
            font-weight: bold;
            text-align: center;
            line-height: 1.8;
            vertical-align: top;
        }

    </style>
    <div class="content-wrapper">
        <div class="new-page-title" style="padding-bottom: 0px !important;">
            <div class="new-page-title-tab">
                <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                    <div class="new_client_id">
                        <p><?php echo e($common->filename); ?></p>
                    </div>
                </div>
                <div class="col-lg-5 col-md-9 col-sm-9 col-xs-12">
                    <div class="newcmpname">
                        <?php if($common->business_id='6'): ?>
                            <div class="new_company_name">
                                <label for="">
                                    Name</label><?php echo e(ucwords($common->nametype)); ?> <?php echo e($common->first_name); ?> <?php echo e($common->middle_name); ?> <?php echo e($common->last_name); ?>

                            </div>
                        <?php else: ?>
                            <div class="new_company_name" style="margin-top: 6px !important;">
                                <label for="">Company Legal Name:</label>
                                <p><?php echo e($common->company_name); ?></p>
                            </div>
                            <div class="new_company_name">
                                <label for="">Business Name :(DBA)</label>
                                <p><?php echo e($common->business_name); ?></p>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="col-lg-5 col-md-10 col-xs-12">
                    <div class="new_images_sec">
                        <div
                            class="col-md-4 col-sm-4 col-xs-4" <?php if(empty($common->business_brand_category_id)): ?> <?php echo e('style="padding-left:12px;"'); ?> <?php endif; ?>>
                            <?php $__currentLoopData = $business; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $busi): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if($busi->id==$common->business_id): ?>
                                    <img src="https://financialservicecenter.net/public/frontcss/images/business.png"
                                         alt=""
                                         class="img-responsive">
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                        <?php $__currentLoopData = $category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($common->business_cat_id==$cate->id): ?>
                                <div class="arrow"><span class="glyphicon glyphicon-chevron-right"
                                                         aria-hidden="true"></span>
                                </div>
                                <?php if(empty($common->business_brand_category_id)): ?>
                                    <div class="arrow"><span class="glyphicon glyphicon-chevron-right"
                                                             aria-hidden="true"></span>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-4">
                                        <img src="https://financialservicecenter.net/public/category/Misc-Business.png"
                                             alt=""
                                             class="img-responsive"/>
                                    </div>
                                <?php endif; ?>
                            <?php endif; ?>

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php $__currentLoopData = $cb; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bb1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($common->business_brand_category_id == $bb1->id): ?>
                                <div class="arrow">
                                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                    <img
                                        src="<?php echo e(url('public/businessbrandcategory')); ?>/<?php echo e($bb1->business_brand_category_image); ?>"
                                        alt="" class="img-responsive"/>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    </div>
                </div>
            </div>
        </div>
        <section class="content">
            <div class="row">
                <div class="col-md-12" style="padding: 0px !important;">
                    <?php if(session()->has('success')): ?>
                        <div class="alert alert-success alert-dismissable"><?php echo e(session()->get('success')); ?></div>
                    <?php endif; ?>
                    <div class="box box-success">
                        <div class="box-header">
                            <div class="box-tools pull-right">

                            </div>
                        </div>
                        <div class="col-md-12 setup_label" style="padding: 5px !important;">
                            <div class="serviceslist">
                                <ul class="mainservices">
                                    <?php $__currentLoopData = $employeeassign; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $assign): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li>
                                            <div class="subvalue">
                                                <div class="new_company_name">
                                                    <label style="background: #701288">
                                                        <a href="javascript:void(0)" style="color:#fff;font-weight: bold;">
                                                            <?php echo e($assign->shortcode); ?>

                                                            <p class="clientsetup">
                                                                <?php if($common->accounting_period='10-2'): ?>
                                                                    <?php echo e('M'); ?>

                                                                <?php elseif($common->accounting_period='4-2'): ?>
                                                                    <?php echo e('Q'); ?>

                                                                <?php elseif($common->accounting_period='5-2'): ?>
                                                                    <?php echo e('H'); ?>

                                                                <?php elseif($common->accounting_period='7-2'): ?>
                                                                    <?php echo e('A'); ?>

                                                                <?php endif; ?>
                                                            </p>
                                                        </a>
                                                    </label>
                                                    <p style="width:auto;">
                                                        <img src="https://financialservicecenter.net/public/images/img_arrow.png" style="margin-right: 0px;width: 13px;"/>
                                                        <a href="#" style="font-weight:bold;"><?php echo e($assign->Team); ?></a>
                                                    </p>
                                                </div>
                                            </div>
                                        </li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php $__currentLoopData = $employeetaxassign; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$assign1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li>
                                            <div class="subvalue" style="padding: 0px;padding-right: 6px;">
                                                <div class="new_company_name" style="margin:0px !important;background:transparent;border:none;">
                                                    <label style="background: <?php if($key+1==1): ?> #b7f4ee <?php elseif($key+1==2): ?> #64bbfb <?php elseif($key+1==3): ?> #ffba77 <?php elseif($key+1==4): ?> #83fd97 <?php elseif($key+1==5): ?> #ffb2d1 <?php endif; ?> ;">
                                                        <a href="#" style="font-weight:bold;">
                                                            <?php echo e($assign1->shortcode); ?>

                                                            <p class="clientsetup">
                                                                <?php if($assign1->taxation_service_period='Annually'): ?>
                                                                    <?php echo e('A'); ?>

                                                                <?php elseif($assign1->taxation_service_period='Monthly'): ?>
                                                                    <?php echo e('M'); ?>

                                                                <?php elseif($assign1->taxation_service_period='Quarterly'): ?>
                                                                    <?php echo e('Q'); ?>

                                                                <?php endif; ?>
                                                            </p>
                                                        </a>
                                                    </label>
                                                    <p style="width:auto;"><img src="https://financialservicecenter.net/public/images/img_arrow.png" style="margin-right: 0px;width: 13px;"/>
                                                        <a href="#" style="font-weight:bold;"><?php echo e($assign1->Team); ?></a>
                                                    </p>
                                                </div>
                                            </div>
                                        </li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                                <ul class="mainservices">
                                    <?php $__currentLoopData = $accountingtable; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $accounts): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li>
                                            <div class="subvalue" style="padding: 0px;padding-right: 6px;">
                                                <div class="new_company_name" style="margin:0px !important;background:transparent;border:none;">
                                                    <label style="background: #701288 ;">
                                                        <a href="#" style="font-weight:bold;">
                                                            <?php echo e($assign1->shortcode); ?>

                                                            <p class="clientsetup">
                                                                <?php if($accounts->typeofwork='Accounting'): ?>
                                                                    <?php echo e('ACTG.'); ?>

                                                                <?php elseif($accounts->typeofwork='Payroll'): ?>
                                                                    <?php echo e('PR'); ?>

                                                                <?php elseif($accounts->typeofwork='Taxation'): ?>
                                                                    <?php echo e('ITR'); ?>

                                                                <?php endif; ?>
                                                            </p>
                                                        </a>
                                                    </label>
                                                    <p style="width:auto;"><img src="https://financialservicecenter.net/public/images/img_arrow.png" style="margin-right: 0px;width: 13px;"/>
                                                        <a href="#" style="font-weight:bold;"><?php echo e($accounts->accounting_software); ?></a>
                                                    </p>
                                                    <p style="width:auto;"><img src="https://financialservicecenter.net/public/images/img_arrow.png" style="margin-right: 0px;width: 13px;"/>
                                                        <a href="#" style="font-weight:bold;"><?php echo e($accounts->accounting_location); ?></a>
                                                    </p>
                                                </div>
                                            </div>
                                        </li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div>
                        </div>
                        <div class="panel with-nav-tabs panel-primary">
                            <?php $active = 'active' ?>
                            <?php $Inactive = '' ?>

                            <?php if(isset($_GET['tab'])): ?>
                                <?php $active = '' ?>
                                <?php $Inactive = 'active' ?>
                            <?php else: ?>
                                <?php $active = 'active' ?>
                                <?php $Inactive = '' ?>
                            <?php endif; ?>

                            <div class="panel-heading">
                                <ul class="nav nav-tabs" id="myTab">
                                    <?php echo e('TEstign '.$common->business_id); ?>

                                    <?php if($common->business_id=='6'): ?>
                                        <li class="<?php echo e($active); ?> basicbutton"><a href="#tab1primary" class="securetab" data-toggle="tab">Basic</a></li>
                                        <li class=""><a href="#tab2primary" class="securetab" data-toggle="tab">Contact Information</a></li>
                                        <li class=""><a href="#tab6primary" class="securetab" data-toggle="tab">Security</a></li>
                                        <li class=""><a href="#tab11primary" class="securetab" data-toggle="tab">Service</a></li>
                                        <li class="bluebutton"><a href="#tabtaxation" data-toggle="tab">Taxation</a></li>
                                        <li class="bluebutton"><a href="#tabIncome" data-toggle="tab">Income</a></li>
                                        <li class="bluebutton expensebutton"><a href="#tabexpense" data-toggle="tab">Expense</a></li>
                                        <li class="bluebutton"><a href="#tabDependent" data-toggle="tab">Dependent</a></li>
                                        <li class="bluebutton"><a href="#" data-toggle="tab">Y</a></li>
                                        <li class="<?php echo e($Inactive); ?> bluebutton"><a href="#tab7primary" data-toggle="tab"> Other</a></li>
                                    <?php else: ?>
                                        <li class="<?php echo e($active); ?>"><a href="#tab1primary" class="securetab" data-toggle="tab">Basic Information</a></li>
                                        <li class=""><a href="#tab2primary" class="securetab" data-toggle="tab">Contact Information</a></li>
                                        <li class=""><a href="#tab6primary" class="securetab" data-toggle="tab">Security </a></li>
                                        <li class=""><a href="#tab11primary" class="securetab" data-toggle="tab">Service </a></li>
                                        <li class="bluebutton interviewtab"><a href="#tab4primary" class="interviewbox" data-toggle="tab">Interview</a></li>
                                        <li class="bluebutton"><a href="#tab3primary" data-toggle="tab">Formation </a></li>
                                        <li class="bluebutton"><a href="#tab16primary" data-toggle="tab">Business </a></li>
                                        <li class="bluebutton"><a href="#tab8primary" data-toggle="tab">License </a></li>
                                        <li class="bluebutton"><a href="#tab5primary" data-toggle="tab">Taxation </a></li>
                                        <li class="bluebutton"><a href="#tab25primary" data-toggle="tab">Documents </a></li>
                                        <li class="bluebutton"><a href="#tab9primary" data-toggle="tab">Revenue </a></li>
                                        <li class="bluebutton"><a href="#tab10primary" data-toggle="tab">Expense </a></li>
                                        <li class="bluebutton"><a href="#tab15primary" data-toggle="tab">Payroll</a></li>
                                        <li class="bluebutton"><a href="#tab14primary" data-toggle="tab">Banking </a></li>
                                        <li class="<?php echo e($Inactive); ?> bluebutton"><a href="#tab7primary" data-toggle="tab"> Other</a></li>
                                    <?php endif; ?>
                                </ul>
                            </div>
                            <form class="form-horizontal" enctype="multipart/form-data" action="<?php echo e(route('clientsetup.update',$common->cid)); ?>" id="registrationForm">
                                <?php echo e(csrf_field()); ?>

                                <?php echo e(method_field('PATCH')); ?>

                                <div class="panel-body" style="padding-left: 6px; padding-right: 6px;">
                                    <div class="tab-content">
                                        <div class="tab-pane fade in <?php echo e($active); ?> disablebox newcheckbox" id="tab1primary" style="pointer-events: none;">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <?php if(empty($user->user_type)): ?><?php else: ?>
                                                    <input name="user_type" value="<?php echo e($user->user_type); ?>" type="hidden" id="user_type"/>
                                                <?php endif; ?>
                                                <div class="form-group">
                                                    <?php if(isset($common->id)): ?> <?php $ccid=$common->id ?> <?php endif; ?>
                                                    <label for="" class="col-md-3 control-label">Client ID(File No) / Status :</label>
                                                    <div class="col-lg-2 col-md3 fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" maxlength="10" onkeyup="checkemail();" placeholder="AAA-999-AA99" id="fileno" name="fileno" value="<?php echo e($common->filename); ?>" readonly/>
                                                    </div>
                                                    <div class="col-lg-2 col-md-3 col-xs-6  fsc-element-margin">
                                                        <input type="text" class="form-control fsc-input" placeholder="Create Date" name="creationdate" id="creationdate" value="<?php if(empty($common->creationdate)): ?> <?php echo e(date('m-d-Y')); ?> <?php else: ?>  <?php echo e($common->creationdate); ?> <?php endif; ?>">
                                                    </div>
                                                        <div class="col-lg-2 col-md-3 col-xs-6  fsc-element-margin">
                                                            <select name="status" id="status" class="form-control fsc-input" style="background-color:#00ef00 !important;color:#fff !important" >
                                                                <option value="">Status</option>
                                                                <option value="Hold" <?php if($common->status == 'Hold'): ?> selected <?php endif; ?> class="Red">Hold</option>
                                                                <option  class="Orange" value="Pending" <?php if($common->status == 'Pending'): ?> selected <?php endif; ?>>Pending</option>
                                                                <option value="Approval" <?php if($common->status == 'Approval'): ?> selected <?php endif; ?> class="Green"  >Approve</option>
                                                                <option value="Active" style="background-color:#00ef00 !important;color:#fff !important"  <?php if($common->status == 'Active'): ?> selected <?php endif; ?>>Active</option>
                                                                <option value="Inactive" class="Blue" <?php if($common->status == 'Inactive'): ?> selected <?php endif; ?> style="background:blue;color:#fff">Inactive</option>
                                                            </select>
                                                        </div>
                                                </div>
                                                
                                                <?php if($common->business_id=='6'): ?>
                                                    <div class="form-group">
                                                        <label for="" class="control-label col-md-3">Name:</label>
                                                        <div class="col-lg-6 col-md-9">
                                                            <div class="row">
                                                                <div class="col-md-2 col-xs-4">
                                                                    <select name="nametype" id="nametype" class="form-control txtOnly">
                                                                        <option value="mr" <?php if($common->nametype=='mr'): ?> selected="" <?php endif; ?>>Mr.</option>
                                                                        <option value="mrs" <?php if($common->nametype=='mrs'): ?> selected="" <?php endif; ?>>Mrs.</option>
                                                                        <option value="miss" <?php if($common->nametype=='miss'): ?> selected="" <?php endif; ?>>Miss.</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-4 col-xs-8">
                                                                    <input type="text" class="form-control " id="firstname" name="firstname" value="<?php echo e($common->first_name); ?>">
                                                                </div>
                                                                <div class="col-md-2 col-xs-4">
                                                                    <div class="">
                                                                        <input type="text" class="form-control " id="middlename" name="middlename" maxlength="1" value="<?php echo e($common->middle_name); ?>">
                                                                    </div>
                                                                </div>
                                                                <div class="">
                                                                    <div class="col-md-4 col-xs-8">
                                                                        <input type="text" class="form-control " id="lastname" name="lastname" value="<?php echo e($common->last_name); ?>">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php endif; ?>
                                                
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </form>


                        </div>

                    </div>
                </div>
            </div>
        </section>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>