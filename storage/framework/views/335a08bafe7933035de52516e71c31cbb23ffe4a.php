<?php $__env->startSection('main-content'); ?>
<style>
    .form-check{width: 50%;float: left;}
</style>
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>Rules / Responsibility</h1>
    </section>
    <!-- Main content -->
    <section class="content">
<div class="row">
		<div class="col-md-12">
			<div class="box box-success">
			      <div class="box-header">
            
              <div class="box-tools pull-right">
                
              </div>
            </div>
				<div class="col-md-12">
                   
					<form method="post" action="<?php echo e(route('rules.update',$homecontent->id)); ?>" class="form-horizontal" id="homecontent" name="homecontent" enctype="multipart/form-data">
					<?php echo e(csrf_field()); ?><?php echo e(method_field('PATCH')); ?>

						 <div class="form-group">
                   <label class="control-label col-md-3">Date / Day / Time:</label>
                     <div class="col-lg-8 col-md-9">
                         <div class="row">
                     <div class="col-lg-3 col-md-4">
                        <div class="">
                           <input type="text" name="date" id="date" class="form-control" value="<?php echo $homecontent->date; ?>" placeholder="Date">
                        </div>
                     </div>
                      
                     <div class="col-lg-3 col-md-4 col-xs-6">
                        <div class="">
                           <input type="text" name="day" id="day" class="form-control" placeholder="Day"  value="<?php echo $homecontent->day; ?>">
                        </div>
                     </div>
                     <input type="hidden" name="state" readonly id="state" value="employee"  class="form-control">
                     <div class="col-lg-2 col-md-4 col-xs-6">
                        <div class="">
                           <input type="text" name="time" id="time" class="form-control"  value="<?php echo $homecontent->time; ?>" placeholder="Time">
                        </div>
                     </div>
                     </div></div>
                  </div>
						<div class="form-group<?php echo e($errors->has('type') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Type :</label>
							<div class="col-lg-8 col-md-9">
							<div class="row">
							<div class="col-lg-8 col-md-12">
								<select name="type" type="text" id="type" class="form-control">
 <option value="">---Select---</option>
 <option value="Rules" <?php if($homecontent->type=='Rules'): ?> selected <?php endif; ?>>Rules</option>
 <option value="Resposibilty" <?php if($homecontent->type=='Resposibilty'): ?> selected <?php endif; ?>>Responsibility</option>
                                                                </select>							
								<?php if($errors->has('type')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('type')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
							</div>
							</div>
						</div>	
						
							<div class="form-group<?php echo e($errors->has('type') ? ' has-error' : ''); ?> emp" <?php if($homecontent->type=='Resposibilty'): ?> <?php else: ?> style="display:none" <?php endif; ?> >
							<label class="control-label col-md-3">Employee / User :</label>
							<div class="col-lg-8 col-md-9">
							<div class="row">
							<div class="col-lg-8 col-md-12">
								<select name="employee_id" id="employee_id" class="form-control">
                                      <option value="">---Select Employee---</option>
                                      <?php $__currentLoopData = $employee; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $as): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                              <option value="<?php echo e($as->id); ?>" <?php if($homecontent->employee_id==$as->id): ?> selected <?php endif; ?>><?php echo e(ucfirst($as->firstName.' '.$as->middleName.' '.$as->lastName)); ?> (<?php echo e($as->teams); ?>)</option>
                                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>	
								<?php if($errors->has('type')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('type')); ?></strong>
										</span>
									<?php endif; ?>							
							</div>
							</div>
							</div>
						</div>	
							<div class="form-group">
							<label class="control-label col-md-3">User Type :</label>
							<div class="col-lg-8 col-md-9">
							<div class="row">
							<div class="col-lg-8 col-md-12">
								<select name="usertype" type="text" id="usertype" class="form-control">
 <option value="">---Select---</option>
 <option value="FSC Employee" <?php if($homecontent->usertype=='FSC Employee'): ?> selected <?php endif; ?>>FSC Employee</option>
 <option value="FSC User" <?php if($homecontent->usertype=='FSC User'): ?> selected <?php endif; ?>>FSC User</option>
 <option value="FSC Client" <?php if($homecontent->usertype=='FSC Client'): ?> selected <?php endif; ?>>FSC Client</option>
 <option value="Client" <?php if($homecontent->usertype=='Client'): ?> selected <?php endif; ?>>Client</option>
                                                                </select>	
													
							</div>
							</div>
							</div>
						</div>
					<div class="form-group emp" <?php if($homecontent->type=='Resposibilty'): ?> <?php else: ?> style="display:none" <?php endif; ?>>
							<label class="control-label col-md-3">Responsibility Type :</label>
							<div class="col-lg-8 col-md-9">
							<div class="row">
							<div class="col-lg-8 col-md-12">
								<select name="respon_type" id="respon_type" class="form-control">
             <option value="">Select</option>
             <option value="Daily"  <?php if($homecontent->respon_type=='Daily'): ?> selected <?php endif; ?>>Daily</option>
             <option value="Weekly" <?php if($homecontent->respon_type=='Weekly'): ?> selected <?php endif; ?>>Weekly</option>
             <option value="Monthly" <?php if($homecontent->respon_type=='Monthly'): ?> selected <?php endif; ?>>Monthly</option>
             <option value="Quarterly" <?php if($homecontent->respon_type=='Quarterly'): ?> selected <?php endif; ?>>Quarterly</option>
             <option value="Regular" <?php if($homecontent->respon_type=='Regular'): ?> selected <?php endif; ?>>Regular</option>
             <option value="Half-Yearly" <?php if($homecontent->respon_type=='Half-Yearly'): ?> selected <?php endif; ?>>Half-Yearly</option>
             <option value="Yearly" <?php if($homecontent->respon_type=='Yearly'): ?> selected <?php endif; ?>>Yearly</option>
                                </select>	
													
							</div>
							</div>
							</div>
						</div>
						
							<div class="form-group">
							<label class="control-label col-md-3">How To Do :</label>
							<div class="col-lg-8 col-md-9">
							<div class="row">
							<div class="col-lg-8 col-md-12">
								<select name="howtodo_id" id="howtodo_id" class="form-control">
             <option value="">Select</option>
             <?php $__currentLoopData = $howtodos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $howtodo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
             <option value="<?php echo e($howtodo->id); ?>" <?php if($homecontent->howtodo_id ==$howtodo->id): ?> selected <?php endif; ?>><?php echo e($howtodo->subject); ?></option>
             <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></select>	
							</div>
							</div>
							</div>
						</div>
					
						<div class="form-group choose_day"  <?php if($homecontent->respon_type=='Weekly'): ?>  <?php else: ?> style="display:none" <?php endif; ?> >
							<label class="control-label col-md-3">Choose Day :</label>
							<div class="col-lg-8 col-md-9">
							<div class="row">
							<div class="col-lg-8 col-md-12">
								
										<div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="exampleCheck1" <?php if($homecontent->choose_day1=='Monday'): ?> checked <?php endif; ?> name="choose_day1" value="Monday">
                                    <label class="form-check-label" for="exampleCheck1">Monday</label>
                                 </div>	
                                 	<div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="exampleCheck2" name="choose_day2" value="Tuesday" <?php if($homecontent->choose_day2=='Tuesday'): ?> checked <?php endif; ?>>
                                    <label class="form-check-label" for="exampleCheck2">Tuesday</label>
                                 </div>	
                                 <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="exampleCheck3" name="choose_day3" value="Wednesday" <?php if($homecontent->choose_day3=='Wednesday'): ?> checked <?php endif; ?>>
                                    <label class="form-check-label" for="exampleCheck3">Wednesday</label>
                                 </div>	
                                 	<div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="exampleCheck4" name="choose_day4" value="Thursday" <?php if($homecontent->choose_day4=='Thursday'): ?> checked <?php endif; ?>>
                                    <label class="form-check-label" for="exampleCheck4">Thursday</label>
                                 </div>	
                                 <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="exampleCheck5" name="choose_day5" value="Friday" <?php if($homecontent->choose_day5=='Friday'): ?> checked <?php endif; ?>>
                                    <label class="form-check-label" for="exampleCheck5">Friday</label>
                                 </div>	
                                 	<div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="exampleCheck6" name="choose_day6" value="Saturday" <?php if($homecontent->choose_day6=='Saturday'): ?> checked <?php endif; ?>>
                                    <label class="form-check-label" for="exampleCheck6">Saturday</label>
                                 </div>	
                                 	<div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="exampleCheck7" name="choose_day7" value="Sunday" <?php if($homecontent->choose_day7=='Sunday'): ?> checked <?php endif; ?>>
                                    <label class="form-check-label" for="exampleCheck7">Sunday</label>
                                 </div>					
							</div>
							</div>
							</div>
						</div>
						
						<div class="form-group emp1" <?php if(!empty($homecontent->choose_date)): ?> <?php else: ?> style="display:none" <?php endif; ?>>
							<label class="control-label col-md-3">Choose Date :</label>
							<div class="col-lg-8 col-md-9">
							<div class="row">
							<div class="col-lg-8 col-md-12">
								<select name="choose_date" id="choose_date" class="form-control">
              <option value="">Select</option>
                <?php 
for( $i=1; $i<=31; $i++ )
{?>
<option value="<?php echo $i;?>" <?php if($homecontent->choose_date==$i): ?> selected <?php endif; ?>><?php echo $i;?></option>
<?php 
}?>                     
                                </select>	
													
							</div>
							</div>
							</div>
						</div>
						 <div class="form-group <?php echo e($errors->has('typeofservice') ? ' has-error' : ''); ?>">
                     <label class="control-label col-md-3">Type of Service :</label>
                     <div class="col-lg-8 col-md-9">
							<div class="row">
							<div class="col-lg-8 col-md-12">
                        <select name="typeofservice" type="text" id="typeofservice" class="form-control">
                           <option value="">Type of Service</option>
                          <?php $__currentLoopData = $typeofser; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $typeofser1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <option value="<?php echo e($typeofser1->id); ?>" <?php if($typeofser1->id==$homecontent->typeofservice): ?> selected <?php endif; ?>><?php echo e($typeofser1->typeofservice); ?></option>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                        <?php if($errors->has('typeofservice')): ?>
                        <span class="help-block">
                        <strong><?php echo e($errors->first('typeofservice')); ?></strong>
                        </span>
                        <?php endif; ?>
                     </div>
                     </div>
                     </div>
                      </div>
							<div class="form-group<?php echo e($errors->has('title') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Title :</label>
							<div class="col-lg-8 col-md-9">
							<div class="row">
							<div class="col-lg-8 col-md-12">
								<input name="title"  type="text" id="title" value="<?php echo $homecontent->title; ?>" class="form-control">

								<?php if($errors->has('title')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('title')); ?></strong>
										</span>
									<?php endif; ?>							
							</div>
							</div>
							</div>
						</div>	
						<div class="form-group<?php echo e($errors->has('rules') ? ' has-error' : ''); ?>">
						<label class="control-label col-md-3">Rules / Resposibilty :</label>
						<div class="col-lg-8 col-md-9">
							<div class="row">
							<div class="col-lg-8 col-md-12">
							  <textarea id="editor1" name="rules" rows="10" cols="80"><?php echo $homecontent->rules; ?></textarea>
					  </div>
							<?php if($errors->has('rules')): ?>
									<span class="help-block">
										<strong><?php echo e($errors->first('rules')); ?></strong>
									</span>
								<?php endif; ?>	
						</div>
						</div>
					</div>
					<div class="form-group<?php echo e($errors->has('rules') ? ' has-error' : ''); ?>">
						<label class="control-label col-md-3"></label>
						<div class="col-lg-8 col-md-9">
							<div class="row">
							<div class="col-lg-8 col-md-12">
							  	<label><input id="checked" type="checkbox" name="checked" value="2" <?php if($homecontent->status=='2'): ?> checked <?php endif; ?>> Click Here</label>
					  </div>
							<?php if($errors->has('rules')): ?>
									<span class="help-block">
										<strong><?php echo e($errors->first('rules')); ?></strong>
									</span>
								<?php endif; ?>	
						</div>
						</div>
					</div>
						<div class="card-footer">
						    <div class="form-group">
							<label class="control-label col-md-3"></label>
							<div class="col-xs-2" style="width:155px;">
<input class="btn_new_save btn-primary1" type="submit" name="submit" value="Save">
							</div>
							<div class="col-xs-2 " style="width:155px;">
<a class="btn_new_cancel" href="<?php echo e(url('fac-Bhavesh-0554/rules')); ?>">Cancel</a> 
							</div>
							<div class="col-xs-2" style="width:155px;">
  
                                        
										<a class="btn_new_cancel" style="background:red !important;" onclick="if(confirm('Are you sure, You want to delete this record ?'))
                                                                                  {event.preventDefault();document.getElementById('delete-id-<?php echo e($homecontent->id); ?>').submit();} else{event.preventDefault();}" href="">Delete</a>
								 
							</div>
							
						</div>
						  </div>
						
					</form>
					<form action="<?php echo e(route('rules.destroy',$homecontent->id)); ?>" method="post" style="display:none" id="delete-id-<?php echo e($homecontent->id); ?>">
                                        <?php echo e(csrf_field()); ?> <?php echo e(method_field('DELETE')); ?>

                                        </form>
				</div>
			</div>
		</div>
	</div>
</section>	
<!--</div>-->
<script>
    $(document).ready(function(){
       $('#type').on('change', function(){
         if($('#type').val()=='Resposibilty')
         {
             $('.emp').show();
         }
         else
         {
            $('.emp').hide();  
         }
         
       });
    });
    
</script>
<script>
    $(document).ready(function(){
       $('#respon_type').on('change', function(){
         if($('#respon_type').val()=='Weekly')
         {
             $('.choose_day').show();
             $('.emp1').hide();
         }
         else if($('#respon_type').val()=='Monthly')
         {
             
             $('.emp1').show();
             $('.choose_day').hide();
         }
         else
         {
             $('.emp1').hide();
            $('.choose_day').hide();  
         }
         
       });
    });
    
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>