<?php $__env->startSection('main-content'); ?>
<style>
.dataTables_filter{
    margin-top: -47px;position: absolute;
}
.dt-buttons
{
    position: absolute;
    margin-top: -40px;
    margin-left: 86.6%;
}
</style>
<style>
    .buttons-pdf {
    font-size: 0px !important;
    border-radius: 3px;
    padding: 8px 10px !important;
    background: #fff !important;
    border-color: #c6c6c6  !important;
    color:red !important;
}
.buttons-print {
    font-size: 0px !important;
    border-radius: 3px;
    padding: 8px 10px !important;
    background: #3c8dbc !important;
            border-color: #367fa9 !important;
}
.buttons-excel{
    font-size:0px !important;
    border-radius: 3px;
        padding: 8px 10px !important;
        
         background: #00a65a !important;
    border-color: #008d4c !important;
        

}
.buttons-excel:hover{
     background: #008d4c !important;

}
.buttons-pdf:hover{
     background: #f6f6f6  !important;
}

.buttons-print:hover{
     background: #367fa9 !important;
}


.fa{
    font-size: 16px !important;
}
</style>
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
    <section class="page-title content-header" style="height:50px !important;">
     		<h1 class="col-sm-12"><div class="col-sm-7" style="text-align:right;"><?php if(empty($_REQUEST['online'])): ?> List of Service <?php else: ?> Online Access <?php endif; ?> Fees </div><div class="col-sm-5" style="text-align:right;">View / Edit</div></h1>
    </section>
    <!-- Main content -->
    <section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-success">
			      <div class="box-header">
              <div class="box-tools pull-right" style="margin-right: 150px;">
                <div class="table-title">
						<a href="<?php echo e(route('price.create')); ?>?online=<?php echo e($_REQUEST['online']); ?>">Add New</a>
					</div>
              </div>
            </div>
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table table-hover table-bordered" id="sampleTable3" >
							<thead>
								<tr>
									<th>#</th>
									<th>Currency</th>
									<th>Type of Service</th>
                 					<th>Service Period</th>
                 					<th style="width: 353px;">Note</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
                                <?php $__currentLoopData = $price; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bus): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                
								<tr>
									<td><?php echo e($loop->index + 1); ?></td>
									<td><?php echo e($bus->currency.' '.$bus->sign); ?></td>
									<td><?php echo e($bus->typeofservice); ?></td>	
                                    <td><?php if($bus->typeofservice =='Payroll Service'): ?> <?php echo e($bus->servicename); ?> <?php else: ?> <?php echo e($bus->period); ?> <?php endif; ?></td>
                                    <td><?php echo e($bus->serviceincludes); ?></td>
                                    <td style="text-align:center;">
										<a class="btn-action btn-view-edit" href="<?php echo e(route('price.edit', $bus->id)); ?>?online=<?php echo e($_REQUEST['online']); ?>"><i class="fa fa-edit"></i></a>
                                        <form action="<?php echo e(route('price.destroy',$bus->id)); ?>" method="post" style="display:none" id="delete-id-<?php echo e($bus->id); ?>">
                                        <?php echo e(csrf_field()); ?> <?php echo e(method_field('DELETE')); ?>

                                        </form>
                                    </td>
								</tr>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	</section>	
<!--</div>-->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>