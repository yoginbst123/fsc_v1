<?php $__env->startSection('main-content'); ?>
<style>
    .page-title{border-bottom:3px solid #00a65a!important;}
    .maincontainer{background:#fff; padding:5px 25px;}
    .clear{clear:both;}
    .clientcompany{   background: #e0f1fd !important;
    border: 1px solid #3598dc !important;
    padding: 15px;
    margin-bottom:5px;}
 .clientcompany label{padding-top:8px;}
 .padleftzero{padding-left:0px!important;}
 .padrightzero{padding-right:0px!important;}
 .text-left{text-align:left!important;}
 .btn-renew {
    display: block;
    width:150px; float:right; cursor:pointer;
    color: #333 !important;
    text-transform: capitalize;
    background: linear-gradient(180deg, #ffe4a3 30%, #f5c34d 70%);
    border: 1px solid #e2a616 !important;
}
.clientcompany2{ background: #fbf3e0 !important;
    border: 1px solid #b39e6c !important;
    padding: 15px;
   ; margin-bottom:5px;}
.clientcompany2 label{text-align:left!important;}
.worktodopopup ul{margin:0px; padding:0px;}
.worktodopopup ul li{list-style-type:none; margin-bototm:6px;}
.worktodopopup ul li input[type="radio"]{margin-right:10px; margin-top:3px; float:left;}
.btndelete{
    /*position: absolute;*/
    bottom: 20px;
    left: 36%;
    font-size: 16px;
    font-weight: bold;
    padding: 9px 18px;
    border-radius: 0px;
    border-top: 1px solid #545454;
    border-left: 1px solid #545454;
    border-right: 1px solid #545454;
    border-bottom: 2px solid #545454;
    height: 41px;}
.modal-header{background:#f5efa8!important; border-bottom:2px solid #1b5bab;}
</style>
<div class="content-wrapper">
    <section class="content-header page-title" style="">
     		<div class="">
     		    <div class="" style="text-align:center;">
     		        <h2>Work To Do </h2>
     		    </div>
     		   
     		</div>
    </section>

 <div class="maincontainer">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-body">
				      
				      
				<form method="post" action="https://financialservicecenter.net/fac-Bhavesh-0554/worktodo/fetch3" enctype="multipart/form-data">
                    <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                        <input type="hidden" name="ids" value="<?php echo e($worktodo->ids); ?>">
   
				    <div class="formain">
				        <div class="clientcompany">
				        <div class="row">
				            
				            <?php 
				        if($worktodo->clientid !='')
                        {
                            ?>
                            <div class="col-md-3 col-sm-5">
				                <div class="">
				                 <label class="col-lg-5 control-label text-right padleftzero">Client ID:</label>
                                 <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 fsc-form-col padleftzero fsc-element-margin"> 
                                <input class="form-control" value="<?php echo e($worktodo->filename); ?>" readonly type="text">
                            </div>
                            </div>
                            </div>
                            
                            <?php if($worktodo->business_id !='6')
                            {
                            ?>
                                <div class="col-md-5 col-sm-6">
				                <div class="">
                                 <label class="col-lg-4 control-label text-right padleftzero">Company Name:</label>
                                  <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin padleftzero"> 
                                  <input class="form-control" value="<?php echo e($worktodo->company_name); ?>" readonly type="text">
                                </div>
                                </div>
                                </div>
                           <?php
                            }
                            else
                            {
                            ?>
                                <div class="col-md-5 col-sm-6">
				                <div class="">
                                 <label class="col-lg-4 control-label text-right padleftzero">Client Name:</label>
                                  <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin padleftzero"> 
                                  <input class="form-control" value="<?php echo e($worktodo->first_name); ?> <?php echo e($worktodo->last_name); ?>" readonly type="text">
                                </div>
                                </div>
                                </div>
                            <?php
                            }
                            ?>     
                            
                            
                            <?php
                        }
                        else
                        {
                            ?>
                            
                    <!--        <div class="col-md-3">-->
    				            <!--    <div class="row">-->
    				            <!--     <label class="col-lg-5 control-label text-right padleftzero">Admin ID:</label>-->
                    <!--                 <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 fsc-form-col padleftzero fsc-element-margin"> -->
                    <!--                <input class="form-control" value="" readonly type="text">-->
                    <!--            </div>-->
                    <!--            </div>-->
                    <!--            </div>-->
                            
                            
                    <!--            <div class="col-md-5 padleftzero">-->
				                <!--<div class="row">-->
                    <!--             <label class="col-lg-4 control-label text-right padleftzero">Company Name:</label>-->
                    <!--              <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin padleftzero"> -->
                                
                    <!--            </div>-->
                    <!--            </div>-->
                    <!--            </div>-->
                           
                    <!--            <div class="col-md-5 padleftzero">-->
				                <!--<div class="row">-->
                    <!--             <label class="col-lg-4 control-label text-right padleftzero">Client Name:</label>-->
                    <!--              <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin padleftzero"> -->
                                
                    <!--            </div>-->
                    <!--            </div>-->
                    <!--            </div>-->
                            
                            <div class="row form-group">
                	            <div class="col-md-3">
                	                <label class="control-label">Name : </label>
                	            </div>
                	            <div class="col-md-1">
                	                <select class="form-control" name="worknew_petname">
                	                    <option>Select.</option>
                	                    <option value="Mr." <?php if($worktodo->worknew_petname =='Mr.'): ?> selected <?php endif; ?>>Mr.</option>
                	                    <option value="Mrs" <?php if($worktodo->worknew_petname =='Mrs'): ?> selected <?php endif; ?>>Mrs.</option>
                	                    <option value="Miss." <?php if($worktodo->worknew_petname =='Miss.'): ?> selected <?php endif; ?>>Miss.</option>
                	               </select>
                	            </div>
                	            
                	            <div class="col-md-2">
                	                <input type="text" name="worknew_fname" value="<?php echo e($worktodo->worknew_fname); ?>" class="form-control" placeholder="First Name"/>
                	            </div>
                	            
                	            <div class="col-mdt-1">
                	                <input type="text" name="worknew_mname" value="<?php echo e($worktodo->worknew_mname); ?>" class="form-control" placeholder="Middle Name"/>
                	            </div>
                	            
                	             <div class="col-md-2">
                	                <input type="text" name="worknew_lname" value="<?php echo e($worktodo->worknew_lname); ?>" class="form-control" placeholder="Last Name"/>
                	            </div>
                	        </div>
                                      
                                    <div class="row form-group">
                	            <div class="col-md-3">
                	                <label class="control-label">Telephone No.</label>
                	            </div>
                	             <div class="col-md-3">
                	                <input type="tel" name="worknew_telephone" id="worknew_telephone" value="<?php echo e($worktodo->worknew_telephone); ?>" class="form-control" placeholder=""/>
                	            </div>
                	        </div>
            	            
            	            <div class="row form-group">
                	            <div class="col-md-3">
                	                <label class="control-label">Email</label>
                	            </div>
                	             <div class="col-md-3">
                	                <input type="email" name="worknew_email" value="<?php echo e($worktodo->worknew_email); ?>" class="form-control" placeholder=""/>
                	            </div>
                	        </div>
    
                            
                            <?php
                            
                        }
				        ?>
				            
				        </div>
				        </div>
				        <div class="clientcompany2">
				            <div class="row form-group">
				                <div class="col-md-3 todo_1">
				                    <div class="row">
				                        <label class="col-lg-12 control-label text-right ">Type of work:</label>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin "> 
                                            <select name="worknew_category"  class="form-control fsc-input nametypes" onchange="myFunction(this)">
											    <option value=''>---Select Work Category---</option>
											    <?php $__currentLoopData = $workcategory; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											    <option value='<?php echo e($cate->id); ?>' <?php if($worktodo->worknew_category == $cate->id): ?> selected <?php endif; ?>><?php echo e($cate->category_name); ?></option>
											    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</select>
											<?php if($errors->has('worknew_category')): ?>
										    <span class="help-block">
											    <strong><?php echo e($errors->first('worknew_category')); ?></strong>
										    </span>
									        <?php endif; ?>
                                        </div> 
				                    </div>
				                </div>
				                
				                <div class="col-md-3 four todo_2" style="display:none;">
				                    <div class="row">
				                    <label class="col-lg-12 control-label text-right ">Work Type:</label>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin "> 
                                            <select class="form-control fsc-input" name="worknew_type" >
                                                <option value="Purchase" <?php if($worktodo->worknew_type =='Purchase'): ?> selected <?php endif; ?>>Purchase</option>
                                                <option value="Refinance" <?php if($worktodo->worknew_type =='Refinance'): ?> selected <?php endif; ?>>Refinance</option>
                                            </select>
                                        </div>
                                    </div>
				                </div>
				                
				                <div class="col-md-3 five todo_3" style="display:none;">
				                    <div class="row">
				                    <label class="col-lg-12 control-label text-right ">Work Type:</label>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin "> 
                                            <select class="form-control fsc-input" name="worknew_type" >
                                                <option value="Purchase" <?php if($worktodo->worknew_type =='Purchase'): ?> selected <?php endif; ?>>Purchase</option>
                                                <option value="Refinance" <?php if($worktodo->worknew_type =='Refinance'): ?> selected <?php endif; ?>>Refinance</option>
                                            </select>
                                        </div>
                                    </div>
				                </div>
				                
				                <div class="col-md-3 six todo_4" style="display:none;">
				                    <div class="row">
				                    <label class="col-lg-12 control-label text-right ">Work Type:</label>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin "> 
                                            <select class="form-control fsc-input" name="worknew_type" >
                                                <option value="Create New Corp/ LLC" <?php if($worktodo->worknew_type =='Create New Corp/ LLC'): ?> selected <?php endif; ?>>Create New Corp/ LLC</option>
                                                <option value="Update Corp / LLC" <?php if($worktodo->worknew_type =='Update Corp / LLC'): ?> selected <?php endif; ?>>Update Corp / LLC</option>
                                                <option value="Create Documents" <?php if($worktodo->worknew_type =='Create Documents'): ?> selected <?php endif; ?>>Create Documents</option>
                                      
                                            </select>
                                        </div>
                                    </div>
				                </div>
				                
				                <div class="col-md-3 seven todo_5" style="display:none;">
				                    <div class="row">
				                    <label class="col-lg-12 control-label text-right ">Work Type:</label>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin "> 
                                            <select class="form-control fsc-input" name="worknew_type" >
                                                <option value="Renewal License"  <?php if($worktodo->worknew_type =='Renewal License'): ?> selected <?php endif; ?>>Renewal License</option>
                                                <option value="New License"  <?php if($worktodo->worknew_type =='New License'): ?> selected <?php endif; ?>>New License</option>
                                                <option value="Update License"  <?php if($worktodo->worknew_type =='Update License'): ?> selected <?php endif; ?>>Update License</option>
                                            </select>
                                        </div>
                                    </div>
				                </div>
				                
				                <div class="col-md-3 eight todo_6" style="display:none;">
				                    <div class="row">
				                     <label class="col-lg-12 control-label text-right ">Work Type:</label>
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin "> 
                                           <select class="form-control fsc-input" name="worknew_type" >
                                                <option value="Life Insurance" <?php if($worktodo->worknew_type =='Life Insurance'): ?> selected <?php endif; ?>>Life Insurance</option>
                                                <option value="Health Insurance" <?php if($worktodo->worknew_type =='Health Insurance'): ?> selected <?php endif; ?>>Health Insurance</option>
                                                <option value="Auto Insurance" <?php if($worktodo->worknew_type =='Auto Insurance'): ?> selected <?php endif; ?>>Auto Insurance</option>
                                                <option value="Home Insurance" <?php if($worktodo->worknew_type =='Home Insurance'): ?> selected <?php endif; ?>>Home Insurance</option>
                                                <option value="Business Insurance" <?php if($worktodo->worknew_type =='Business Insurance'): ?> selected <?php endif; ?>>Business Insurance</option>
                                                <option value="Other Insurance" <?php if($worktodo->worknew_type =='Other Insurance'): ?> selected <?php endif; ?>>Other Insurance</option>
                                   
                                        </select>
                                        </div>
                                    </div>
				                </div>
				                
				                
				                
				                <div class="col-md-2 todo_7">
				                    <div class="row">
				                     <label class="col-lg-12 control-label text-right ">Work Priority:</label>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin "> 
                                            <select class="form-control fsc-input" name="worknew_priority">
                                                <option value=" Regular" <?php if($worktodo->worknew_priority =='Regular'): ?> selected <?php endif; ?>> Regular</option>
                                                <option value="Immediately" <?php if($worktodo->worknew_priority =='Immediately'): ?> selected <?php endif; ?>>Immediately</option>
                                                <option value="Urgent" <?php if($worktodo->worknew_priority =='Urgent'): ?> selected <?php endif; ?>>Urgent</option>
                                            </select>
                                        </div>
                                    </div>
				                </div>
				                
				                 <div class="col-md-2 todo_8">
				                    <div class="row">
				                     <label class="col-lg-12 control-label text-right">Due Date:</label>
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin  "> 
                                           <input type="text" class="form-control datepicker" id="datepicker" name="worknew_duedate" 
                                        value="<?php if($worktodo->worknew_duedate !='0000-00-00') { echo date('M-d Y',strtotime($worktodo->worknew_duedate));}?>" />
                                       
                                        </div>
                                    </div>
				                </div> 
				                <div class="col-md-3 todo_9">
				                    <div class="row">
				                     <label class="col-lg-12 control-label text-right">Responsible Person:</label>
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin"> 
                                           <select class="form-control fsc-input" name="worknew_emp">
                                       <option value=" All FSC-EE"> All FSC-EE</option>
                                       <?php
                                       foreach($empname as $emp)
                                       {
                                       ?>
                                       <option value="<?php echo $emp->employee_id;?>" <?php if($emp->employee_id ==$worktodo->worknew_emp): ?> selected <?php endif; ?>><?php echo $emp->firstName.' '.$emp->middleName.' '.$emp->lastName;?></option>
                                       <?php
                                       }
                                       ?>
                                    </select>
                                        </div>
                                    </div>
				                </div>
				            </div>
				             
				        </div>
				        <a class="btn_new btn-renew nn"   data-toggle="modal" data-target="#myModalActivity">Activity</a>
				        <p>&nbsp; <br/><br/></p>
				    </div>
				    
				      <div class="form-group">
                                <div class="row">
                                    <label class="col-lg-2 col-md-2 control-label text-right" style="">Work To Do :</label>
                                     <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin"> 
                                       <textarea class="form-control" name="worknew_details" style="height:100px;"><?php echo $worktodo->worknew_details;?></textarea>
                                     </div>
                                 </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="row">
                                    <label class="col-lg-2 col-md-2 control-label text-right" style="">Work Note :</label>
                                     <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin"> 
                                       <textarea class="form-control" name="worknew_note" style="height:100px;"><?php echo $worktodo->worknew_note;?></textarea>
                                     </div>
                                 </div>
                            </div>
                            <div class="card-footer" STYLE="">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-lg-2 col-md-2 control-label text-right" style=""></label>
                                        <div class="col-xs-1" style="width:auto;">
                                            <input class="btn_new_save" type="submit" value="Update" style="padding: 8px 20px;width:auto;">
                                        </div>
                                        <div class="col-xs-1" style="padding-left:0px; width:auto">
                                            <a class="btn_new_cancel" href="<?php echo e(url('fac-Bhavesh-0554/worktodo')); ?>" style="padding: 8px 20px;width:auto;">Cancel</a> 
                                        </div>
                                        <div class="col-xs-1" style="padding:0px; width:auto">
                                            <a class="btn-action btn-delete btndelete" style="background:#ff0000;padding: 8px 20px;" onclick="if(confirm('Are you sure, You want to delete this record ?'))
                                            {event.preventDefault();document.getElementById('delete-id-<?php echo e($worktodo->ids); ?>').submit();} else{event.preventDefault();}" href="">Delete</a>
                                        </div>
                                    </div>
                                </div>
                            </div>  
				    
				    </FORM>
				    
				    <form action="<?php echo e(route('worktodo.destroy',$worktodo->ids)); ?>" method="post" style="display:none" id="delete-id-<?php echo e($worktodo->ids); ?>">
                        <?php echo e(csrf_field()); ?> <?php echo e(method_field('DELETE')); ?>

                        <!--<input type="hidden" name="ids" value="<?php echo e($worktodo->id); ?>">-->
                    </form>
                        
                    </div>
              	</div>
			</div>
		</div>
	</div>
<!--</div>-->
	

<script>
      $.ajaxSetup({
           headers: {
               'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
           }
       });
    
      $(function () {
         $('#addopt').click(function () { //alert();
             var newopt = $('#newopt').val();
             if (newopt == '') {
                 alert('Please enter something!');
                 return;
             }
   
            //check if the option value is already in the select box
             $('#vendor_product option').each(function (index) {
                 if ($(this).val() == newopt) {
                     alert('Duplicate option, Please enter new!');
                 }
             })
             $.ajax({
   type: "post",
   url: "<?php echo route('work.workcategorys'); ?>",
   dataType: "json",
   data: $('#ajax').serialize(),
   success: function(data){
      alert('Successfully Added');
      $('#vendor_product').append('<option value=' + newopt + '>' + newopt + '</option>');
      $("#div").load(" #div > *");
      $("#newopt").val('');
   },
   error: function(data){
      alert("Error")
   }
   });
            
              $('#basicExampleModal').modal('hide');
         });
     });
 
</script>
<div class="modal fade" id="basicExampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header" style="background:#038ee0;">
            <h4 class="modal-title" id="exampleModalLabel" style="text-align:center;color:#fff;">Work Category<button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
               </button>
            </h4>
         </div>
         <div class="modal-body" style="background:#ffff99;padding:0px !important;">
            <div class="curency curency_ref" id="div">
               <?php $__currentLoopData = $workcategory; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cur): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
               <div id="cur_<?php echo e($cur->id); ?>" class="col-md-12" style="border:1px solid;background:#ffff99;">
                  <div class="col-md-12" style="margin-top: 7px;margin-bottom:5px;">
                     <a class="delete" style="color:#000;" id="<?php echo e($cur->id); ?>"><?php echo e($cur->category_name); ?>

                     <span class="pull-right"><i class="fa fa-trash btn btn-danger" style="padding:6px!important;"></i></span></a>
                  </div>
               </div>
               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>  
            </div>
         </div>
         <div class="modal-footer" style="text-align:center;">
            <button type="button" style="margin-top:10px;border:1px solid;" class="btn btn-secondary" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="basicExampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Workcategory</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <form action="" method="post" id="ajax">
            <?php echo e(csrf_field()); ?>

            <div class="modal-body">
               <input type="text" id="newopt" name="newopt"  class="form-control" placeholder="Workcategory Name"/>
            </div>
            <div class="modal-footer">
               <button type="button" id="addopt" class="btn btn-primary">Save</button>
               <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
         </form>
      </div>
   </div>
</div>


<!-- Modal -->
<div id="myModalActivity" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Activity Method </h4>
      </div>
      <div class="modal-body">
        <div class="worktodopopup">
       <ul>
           <li>
               <input type="radio" name="radio1"><label>Call</label>
           </li>
            <li>
               <input type="radio" name="radio1"><label>Fax</label>
           </li>
           <li>
               <input type="radio" name="radio1"><label>By Personal</label>
           </li>
           <li>
               <input type="radio" name="radio1"><label>By Meeting</label>
           </li>
       </ul>
     
       </div>
      </div>
      <div class="modal-footer">
            <a href="#myModalActivity2" data-toggle="modal" data-target="#myModalActivity2" class="btn btn-primary">OK</a>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<div id="myModalActivity2" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Activity Modal </h4>
      </div>
      <div class="modal-body">
        <div class="worktodopopup">
       <ul>
           <li>
               <input type="radio" name="radio1"><label>Call</label>
           </li>
            <li>
               <input type="radio" name="radio1"><label>Fax</label>
           </li>
           <li>
               <input type="radio" name="radio1"><label>By Personal</label>
           </li>
           <li>
               <input type="radio" name="radio1"><label>By Meeting</label>
           </li>
       </ul>
       </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<script>

    var nametypes=$('.nametypes').val();
    if(nametypes == '4')
    {
        $('.four').show();
        
    }
    else
    {
        $('.four').hide();
    }
    
    if(nametypes=='5')
    {
         $('.five').show();
    }
    else 
    {
        $('.five').hide();
    }
     
    if(nametypes=='6')
    {
        $('.six').show();
    } 
    else 
    {
        $('.six').hide();
    }
     
    if(nametypes=='7')
    {
        $('.seven').show();
    } 
    else 
    {
        $('.seven').hide();
    }
     
    if(nametypes=='8')
    {
        $('.eight').show();
    } 
    else 
    {
        $('.eight').hide();
    }
         

    function myFunction()
    {
        var catval = $('.nametypes').val();;
        // alert(catval);
         if(catval=='4'){
             $('.four').show();
         } else {
             $('.four').hide();
         }
         
          if(catval=='5'){
             $('.five').show();
         } else {
             $('.five').hide();
         }
         
         if(catval=='6'){
             $('.six').show();
         } else {
             $('.six').hide();
         }
         
          if(catval=='7'){
             $('.seven').show();
         } else {
             $('.seven').hide();
         }
         
           if(catval=='8'){
             $('.eight').show();
         } else {
             $('.eight').hide();
         }
    }
    
    
    $(document).ready(function(){
        $("#worknew_telephone").mask("(999) 999-9999");
        
        $(function () {
        $(".datepicker").datepicker({ 
            autoclose: true, 
            todayHighlight: true
            }).datepicker();
        });

    
        $('.js-example-tags').select2({
           tags: true,
           tokenSeparators: [",", " "]
        });

         var catval = document.getElementById('nametype').value;
         if(catval=='ResidentialFinance'){
             $('#resi_finance').show();
         } else {
             $('#resi_finance').hide();
         }
         
          if(catval=='CommercialFinance'){
             $('#com_finance').show();
         } else {
             $('#com_finance').hide();
         }
         
         if(catval=='Corporation/LLC'){
             $('#corp_llc').show();
         } else {
             $('#corp_llc').hide();
         }
         
          if(catval=='License'){
             $('#license').show();
         } else {
             $('#license').hide();
         }
         
           if(catval=='Insurance'){
             $('#insurance').show();
         } else {
             $('#insurance').hide();
         }
});




</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>