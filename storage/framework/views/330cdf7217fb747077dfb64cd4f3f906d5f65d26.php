<?php $__env->startSection('main-content'); ?>
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>Branch</h1>
    </section>
    <!-- Main content -->
    <section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-success">
			      <div class="box-header">
             
              <div class="box-tools pull-right">
                
              </div>
            </div>
				<div class="col-md-12">
             <form method="post" action="<?php echo e(route('branch.store')); ?>" class="form-horizontal" id="businessname" name="businessname" enctype="multipart/form-data">
					<?php echo e(csrf_field()); ?>

<div class="form-group <?php echo e($errors->has('branchtype') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Branch Type:</label>
							<div class="col-lg-5 col-md-8">
								<select name="branchtype" id="branchtype" class="form-control">
                                                            <option value="">---Branch Type---</option>         
                                                            <option value="FSC">FSC</option>
                                                     
                                                                 </select>                                                
                                                              <?php if($errors->has('branchtype')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('branchtype')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>
						<div class="form-group <?php echo e($errors->has('branch') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Branch Name :</label>
							<div class="col-lg-5 col-md-8">
								<input name="branch" type="text" id="branch" class="form-control" value="" />                                                            <?php if($errors->has('branch')): ?>
										<span class="help-block">
										<strong><?php echo e($errors->first('branch')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>
						
						<div class="form-group <?php echo e($errors->has('country') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Country :</label>
							<div class="col-lg-5 col-md-8">
								<select name="country" id="country" class="form-control">
                                                                       <option value="USA">USA</option>
                                                                       <option value="IN">IN</option>
                                                                 </select>                                                
                                                              <?php if($errors->has('country')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('country')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>
						<div class="form-group <?php echo e($errors->has('city') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">City :</label>
							<div class="col-lg-5 col-md-8">
								<input name="city" type="text" id="city" class="form-control" value="" />
								<?php if($errors->has('city')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('city')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>					
						
						<div class="card-footer">
						    <div class="col-md-3"></div>
						    <div class="col-xs-2" style="width:155px;">
								<input class="btn_new_save btn-primary1" type="submit" name="submit" value="Save">
							</div>
							<div class="col-xs-2" style="width:155px;">
								<a class="btn_new_cancel" href="<?php echo e(url('fac-Bhavesh-0554/branch')); ?>">Cancel</a> 
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	</section>
<!--</div>-->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>