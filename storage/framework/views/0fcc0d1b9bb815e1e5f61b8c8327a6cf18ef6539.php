<?php $__env->startSection('main-content'); ?>
<div class="content-wrapper">
      <section class="content-header page-title"><h1>Hiring </h1>
    </section>
    <!-- Main content -->
    <section class="content">
   <div class="row">
      <div class="col-md-12">
         <div class="box box-success">
             <div class="box-header">
             
            </div>
            <div class="card-body">
               <form method="post" action="<?php echo e(route('employment.update',$employment->id)); ?>" class="form-horizontal" id="content" name="content" enctype="multipart/form-data">
                  <?php echo e(csrf_field()); ?><?php echo e(method_field('PATCH')); ?>

                  <div class="col-md-12">
                <div class="form-group<?php echo e($errors->has('date') ? ' has-error' : ''); ?>">
                     <label class="control-label col-md-3 col-xs-12 left_991">Posting Date :</label>
                     <div class="col-md-2 col-xs-6">
                        
                           <input name="date" type="text" value="<?php echo date('M-d-Y',strtotime($employment->created_at));?>" id="date1" class="form-control"> 
                        
                        <?php if($errors->has('date')): ?>
                        <span class="help-block">
                        <strong><?php echo e($errors->first('date')); ?></strong>
                        </span>
                        <?php endif; ?>	
                     </div>
                  </div>
                  <div class="form-group<?php echo e($errors->has('position_name') ? ' has-error' : ''); ?>">
                     <label class="control-label col-md-3">Position Name :</label>
                     <div class="col-md-4">
                        <input name="position_name" type="text" id="position_name" class="form-control" value="<?php echo e($employment->position_name); ?>" />	
                        <?php if($errors->has('position_name')): ?>
                        <span class="help-block">
                        <strong><?php echo e($errors->first('position_name')); ?></strong>
                        </span>
                        <?php endif; ?>							
                     </div>
                  </div>


                <div class="form-group<?php echo e($errors->has('country') ? ' has-error' : ''); ?> <?php echo e($errors->has('city') ? ' has-error' : ''); ?> <?php echo e($errors->has('state') ? ' has-error' : ''); ?>">
                    <label class="control-label col-md-3">City/State/Country:</label>
                    <div class="col-lg-2 col-md-3">
                        <input name="city" value="<?php echo e($employment->city); ?>" type="text" id="city" class="form-control"> 
                        <?php if($errors->has('city')): ?>
                        <span class="help-block">
                            <strong><?php echo e($errors->first('city')); ?></strong>
                        </span>
                        <?php endif; ?>	
                    </div>
                    <div class="col-md-2 col-xs-6">
                        <select name="country" id="countryId" class="form-control fsc-input">
                            <option value=''>---Select---</option>
                            <option value='USA' <?php if($employment->country=='USA'): ?> selected <?php endif; ?>>USA</option>
                            <option value='IND' <?php if($employment->country=='INDIA'): ?> selected <?php endif; ?>>IND</option>
                        </select>
                    </div>
                    <div class="col-md-2 col-xs-6">
                        <select name="state" id="stateId" class="form-control fsc-input">
                            <option value="<?php echo e($employment->state); ?>"><?php echo e($employment->state); ?></option>
                        </select>
                        <?php if($errors->has('state')): ?>
                        <span class="help-block">
                            <strong><?php echo e($errors->first('state')); ?></strong>
                        </span>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="form-group<?php echo e($errors->has('type') ? ' has-error' : ''); ?>">
                    <label class="control-label col-md-3">Job Type :</label>
                    <div class="col-md-2">
                        <select name="type" id="type" class="form-control fsc-input">
                            <option value="<?php echo $employment->type; ?>"><?php echo $employment->type; ?></option>
                            <option value='Full Time'>Full Time</option>
                            <option value='Part Time'>Part Time</option>
                        </select>
                        <?php if($errors->has('type')): ?>
                        <span class="help-block">
                            <strong><?php echo e($errors->first('type')); ?></strong>
                        </span>
                        <?php endif; ?>	
                    </div>
                </div>
                <div class="form-group<?php echo e($errors->has('description') ? ' has-error' : ''); ?>">
                    <label class="control-label col-md-3">Description :</label>
                    <div class="col-md-8">
                        <textarea id="editor1" name="description" rows="10" cols="80"><?php echo $employment->description; ?></textarea>
                        <?php if($errors->has('description')): ?>
                        <span class="help-block">
                            <strong><?php echo e($errors->first('description')); ?></strong>
                        </span>
                        <?php endif; ?>	
                    </div>
                </div>
                 
                  <div class="form-group<?php echo e($errors->has('link') ? ' has-error' : ''); ?>">
                     <!--<label class="control-label col-md-3">Job Url :</label>-->
                     <div class="col-md-8">
                   
                           <input name="link" type="hidden" id="link" class="form-control" value="<?php echo $employment->link; ?>"> 
                       
                        <?php if($errors->has('link')): ?>
                        <span class="help-block">
                        <strong><?php echo e($errors->first('link')); ?></strong>
                        </span>
                        <?php endif; ?>	
                     </div>
                  </div>
                  </div>
                    <div class="card-footer">
                        <label class="control-label col-md-3"></label>
                        <div class="col-xs-2" style="width:auto;">
                            <input class="btn_new_save btn-primary1" type="submit" name="submit" value="Save" style="padding:8px 25px;">
                        </div>
                        <div class="col-xs-2" style="width:auto;">
                            <a class="btn_new_cancel" href="<?php echo e(url('fac-Bhavesh-0554/employment')); ?>" style="padding:8px 25px;">Cancel</a> 
                        </div>
                    </div>
               </form>
            </div>
         </div>
      </div>
   </div>
   </section>
<!--</div>-->

<script>
   $(document).ready(function() {
  var dateInput = $('input[name="date"]'); // Our date input has the name "date"
  var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : 'body';
  dateInput.datepicker({
    format: 'M-dd-yyyy',
    container: container,
    todayHighlight: true,
    //autoclose: true,
   // startDate: truncateDate(new Date()) // <-- THIS WORKS
  });

 $('#date').datepicker('setStartDate', truncateDate(new Date())); // <-- SO DOES THIS
});

function truncateDate(date) {
  return new Date(date.getFullYear(), date.getMonth(), date.getDate());
}
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>