<?php $__env->startSection('main-content'); ?>
<style>label{float:left} 
th{ text-align:center !important}
.input-sm{width: 110% !important;margin-left: 10px !important;}
.dt-buttons{margin-bottom:10px;}
.dataTables_filter{display:none;}
.table > thead > tr > th:nth-child(2){  width:100px !important; }
.table > thead > tr > th:nth-child(7){  width:100px !important; }
.table > thead > tr > th:last-child{width: 85px !important; }
.input-sm{
    width:50% !important;
    height: 32px !important;
}
.search-btn{position: absolute;top: 10px;right: -9px;background: transparent;border: transparent;}
.content-header.page-title h2.customercount{background: #038ee0;
    color: #fff!important;
    display: inline-block;
    padding:5px;
    border-radius: 30px;
    position: absolute;
    top: -12px;
    left: 0px;
    margin-top:7px;
}
.dataTables_info{
    /*margin-top:-35px !important;*/
}
.page-title{
        padding: 8px 19px !important;
}
.box-header{
    padding-right:0px;
    padding-bottom:0px;
}
.dt-button{
    display:none;
}

.actioncolumn{width:80px!important;}
.businesscolumn{width:120px!important;}
.dataTables_length .form-control.input-sm{height:32px!important; padding:0px!important;}
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<div class= style="padding-right:0px !important;">
     		    <div class="" style="text-align:left;position:relative">
     		        <h2 class="customercount">B-<?php echo e($countb); ?> / P-<?php echo e($countp); ?></h2>
     		       
     		    </div>
     		    <div class="" style="text-align:center;">
     		        <h2>List of Client</h2>
     		         <h4 style="display:none">List of Client</h4>
     		    </div>
     		    
     		</div>
    </section>
    <!-- Main content -->
    <section class="content">		
	<div class="row">
		<div class="col-md-12">
			<div class="box box-success">
			      <div class="box-header">
                   <div class="col-md-12 resp_abs" style="">
              <div class="row">
              <div class="col-md-3 cs1" style="">
                    <label style=";margin-top: 11px;margin-right:10px;">Filter: </label>
                    <select name="choice" style="width: 92%;margin-left: 4px;" id="choice" class="form-control">
                        <option value="1">All</option>
                        <option value="2" selected>Active</option>
                        <option value="3">Approval</option>
                        <option value="4">Pending</option>
                        <option value="5">Hold</option>
                        <option value="6">Inactive</option>
                    </select>
            </div>
            
       <div class="col-md-4 cs2" style="">
         <label style="mmargin-left:5%;margin-top: 11px;margin-right:10px;">Search: </label>
    <select name="types" style="width: 70%;margin-left: 4px;" id="types" class="form-control">
        <option value="All" selected>All</option>
        <option value="Type">Client ID</option>
        <option value="EE / User ID">Company Name</option>
        <option value="Employee Name">Bussiness Name</option>
        <option value="Email ID">Bussiness Telephone</option>
        <option value="Tel. Number">Contact Name</option>
        <option value="Telephone">Telephone</option>
    </select>
    </div>
     <div class="col-md-3 cs3" style="">
        <table style="width: 100%;margin: 0 auto 0 10px;" cellspacing="0" cellpadding="3" border="0">
        <tbody>
            <tr id="filter_global">
                <td align="center"><input type="text" class="global_filter form-control" id="global_filter" placeholder="All Search"><button class="search-btn"><i class="fa fa-search"></i></button></td>
            </tr>
            <tr id="filter_col2" data-column="1" style="display:none">
                <td align="center"><input type="text" class="column_filter form-control" id="col1_filter" placeholder="Client ID"><button class="search-btn"><i class="fa fa-search"></i></button></td>
            </tr>
            <tr id="filter_col3" data-column="2" style="display:none">
                <td align="center"><input type="text" class="column_filter form-control" id="col2_filter" placeholder="Company Name"><button class="search-btn"><i class="fa fa-search"></i></button></td>
            </tr>
            <tr id="filter_col4" data-column="3" style="display:none">
                <td align="center"><input type="text" class="column_filter form-control" id="col3_filter" placeholder="Bussiness Name"><button class="search-btn"><i class="fa fa-search"></i></button></td>
            </tr>
            <tr id="filter_col5" data-column="4" style="display:none">
                <td align="center"><input type="text" class="column_filter form-control" id="col4_filter" placeholder="Bussiness Telephone"><button class="search-btn"><i class="fa fa-search"></i></button></td>
            </tr>
            <tr id="filter_col6" data-column="5" style="display:none">
                <td align="center"><input type="text" class="column_filter form-control" id="col5_filter" placeholder="Contact Name"><button class="search-btn"><i class="fa fa-search"></i></button></td>
            </tr>
            <tr id="filter_col7" data-column="6" style="display:none">
                <td align="center"><input type="text" class="column_filter form-control" id="col6_filter" placeholder="Telephone"><button class="search-btn"><i class="fa fa-search"></i></button></td>
            </tr>
        </tbody>
    </table>
    </div>
    </div>
              </div>
             	<div class="col-md-9"></div>
             	<div class="col-md-3">
					<div class="table-title">  
						<a style="margin-top:5px;" href="<?php echo e(route('customer.create')); ?>">Add Client</a>
					</div>
					
					</div>
            </div>
				<div class="col-md-12">
					<div class="table-title">
					</div>
					<?php if(session()->has('success')): ?>
                           <div class="alert alert-success alert-dismissable"><?php echo e(session()->get('success')); ?></div>
                    <?php endif; ?>
                    	<?php if(session()->has('error')): ?>
                           <div class="alert alert-danger alert-dismissable"><?php echo e(session()->get('error')); ?></div>
                    <?php endif; ?>
				
					<div class="table-responsive">
						<table class="table table-hover table-bordered hidden" id="example" style="width:100%">
							<thead>
								<tr style="border:1px solid !important;">
								   <th style="border:1px solid !important;">No</th>
								   <th class="clientidwidth" style="border:1px solid !important;">Client ID</th>
								   <th  style="width:150px !important;border:1px solid !important;">Company Name <br><span style="font-size: 13px;">(Legal Name)</span></th>
							      	   <th style="border:1px solid !important;">Business Name  <br><span style="font-size: 13px;">(DBA Name)</span></th>	
							       <th class="businesscolumn" style="border:1px solid !important;"  width="11%">Business <br/> Telephone </th>
							       <th style="border:1px solid !important;">Contact Name</th>
								   <th style="border:1px solid !important;" width="13%">Contact <br/>Telephone </th>	
								   <th style="border:1px solid !important;">Status</th>	
								   <th  style="width:110px !important;border:1px solid !important;" class="actioncolumn">Action</th>
								</tr>
							</thead>							
							<tbody>
                            <?php $__currentLoopData = $common; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $com): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<tr   <?php if($com->business_id=='6'): ?> style="background:#b9f0b2 !important;border:1px solid !important;" <?php endif; ?>>
									<td style="text-align:center;border:1px solid !important;"><?php echo e($loop->index+1); ?></td>
<!--									<td style="border:1px solid !important;"><?php echo e($com->filename); ?> <?php //if($com->recstatus=='1'){?> <div class="gif">-->
<!--<img src="<?php echo e(asset('public/dashboard/images/newimage.gif')); ?>" alt="" class="img-responsive">-->
<!--</div> <?php //}else{}?></td>-->
                                    <td style="border:1px solid !important;"><?php echo e($com->filename); ?> </td>
									<td style="border:1px solid !important;"><?php if(empty($com->company_name)): ?> <?php echo e($com->first_name); ?> <?php echo e($com->middle_name); ?> <?php echo e($com->last_name); ?> <?php else: ?><?php echo e($com->company_name); ?> <?php endif; ?></td>
								    <td style="border:1px solid !important;"><?php echo e($com->business_name); ?></td>
									<td style="border:1px solid !important;"><?php echo e($com->business_no); ?></td>
									<td style="border:1px solid !important;"><?php echo e($com->firstname); ?> <?php echo e($com->middlename); ?> <?php echo e($com->lastname); ?></td>
									<td style="border:1px solid !important;"><?php echo e($com->etelephone1); ?></td>
									<td style="text-align:center;border:1px solid !important;"> <?php if($com->status=='Active'): ?><a style="display:none" class="btn-action  btn-view-edit btn_edit" href="#">2</a> <a style="text-align:center" class="btn_edit btn-action  btn-view-edit" href="#">Active</a> <?php elseif($com->status=='Approval'): ?><a style="display:none" class="btn-action btn_approval btn-view-edit" href="#">3</a><a style="background-color: Yellow !important; text-align:center;color:#000 !important" class="btn-action btn_approval btn-view-edit" href="#">Approval</a>  <?php elseif($com->status=='Pending'): ?><a style="display:none" class="btn-action btn_pending btn-view-edit" href="#">4</a><a style="background-color: Orange !important; text-align:center" class="btn-action btn_pending btn-view-edit" href="#">Pending</a> <?php elseif($com->status=='Hold'): ?><a style="display:none" class="btn-action btn_hold btn-view-edit" href="#">5</a> <a class="btn-action btn_hold btn-delete" href="#">Hold</a> <?php else: ?> <a style="display:none" class="btn-action btn_inactive btn-view-edit" href="#">6</a> <a class="btn-action btn_inactive btn-delete" href="#">Inactive</a>  <?php endif; ?></td>
									<td style="text-align:center;border:1px solid !important;"><?php if($com->newclient==1): ?><a class="" href="<?php echo e(route('customer.edit',$com->cid)); ?>"><img src="<?php echo e(asset('public/dashboard/images/newimage.gif')); ?>" alt="" width="50px"></a><br><?php endif; ?>
                                     <a class="btn-action btn-view-edit btn-primary" style="" href="<?php echo e(route('customer.edit',$com->cid)); ?>"><i class="fa fa-edit"></i></a>
									<a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record ?'))
                                                                                  {event.preventDefault();document.getElementById('delete-id-<?php echo e($com->cid); ?>').submit();} else{event.preventDefault();}" href="<?php echo e(route('customer.destroy',$com->cid)); ?>"><i class="fa fa-trash"></i></a>
<form action="<?php echo e(route('customer.destroy',$com->cid)); ?>" method="post" style="display:none" id="delete-id-<?php echo e($com->cid); ?>">
                                        <?php echo e(csrf_field()); ?> <?php echo e(method_field('DELETE')); ?>

                                        </form></td>
                                        
								</tr>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</tbody>
						</table>
						<!--<div class="col-md-12" style="display:flex;margin-left: 160px;margin-top: -44px;position:absolute;width:250px;">-->
					 <!--       <input type="text" class="form-control" style="width:80px;margin-right:7px;height: 32px;margin-top: 4px;" placeholder="Go to...">-->
					 <!--       <button class="btn btn-primary" style="height: 32px;margin-top: 3px;padding: 0px 8px;">Go</button>-->
				  <!--      </div>-->
					</div>
				</div>
			</div>
		</div>
	</div>
	</section>
<!--</div>-->
<script>
   	    (function ($) {
    function calcDisableClasses(oSettings) {
        var start = oSettings._iDisplayStart;
        var length = oSettings._iDisplayLength;
        var visibleRecords = oSettings.fnRecordsDisplay();
        var all = length === -1;
 
        // Gordey Doronin: Re-used this code from main jQuery.dataTables source code. To be consistent.
        var page = all ? 0 : Math.ceil(start / length);
        var pages = all ? 1 : Math.ceil(visibleRecords / length);
 
        var disableFirstPrevClass = (page > 0 ? '' : oSettings.oClasses.sPageButtonDisabled);
        var disableNextLastClass = (page < pages - 1 ? '' : oSettings.oClasses.sPageButtonDisabled);
 
        return {
            'first': disableFirstPrevClass,
            'previous': disableFirstPrevClass,
            'next': disableNextLastClass,
            'last': disableNextLastClass
        };
    }
 
    function calcCurrentPage(oSettings) {
        return Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength) + 1;
    }
 
    function calcPages(oSettings) {
        return Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength);
    }
 
    var firstClassName = 'first';
    var previousClassName = 'previous';
    var nextClassName = 'next';
    var lastClassName = 'last';
 
    var paginateClassName = 'paginate';
    var paginatePageClassName = 'paginate_page';
    var paginateInputClassName = 'paginate_input';
    var paginateTotalClassName = 'paginate_total';
 
    $.fn.dataTableExt.oPagination.input = {
        'fnInit': function (oSettings, nPaging, fnCallbackDraw) {
            var nFirst = document.createElement('span');
            var nPrevious = document.createElement('span');
            var nNext = document.createElement('span');
            var nLast = document.createElement('span');
            var nInput = document.createElement('input');
            var nTotal = document.createElement('span');
            var nInfo = document.createElement('span');
 
            var language = oSettings.oLanguage.oPaginate;
            var classes = oSettings.oClasses;
            var info = language.info || 'Page _INPUT_ of _TOTAL_';
 
            nFirst.innerHTML = language.sFirst;
            nPrevious.innerHTML = language.sPrevious;
            nNext.innerHTML = language.sNext;
            nLast.innerHTML = language.sLast;
 
            nFirst.className = firstClassName + ' ' + classes.sPageButton;
            nPrevious.className = previousClassName + ' ' + classes.sPageButton;
            nNext.className = nextClassName + ' ' + classes.sPageButton;
            nLast.className = lastClassName + ' ' + classes.sPageButton;
 
            nInput.className = paginateInputClassName;
            nTotal.className = paginateTotalClassName;
 
            if (oSettings.sTableId !== '') {
                nPaging.setAttribute('id', oSettings.sTableId + '_' + paginateClassName);
                nFirst.setAttribute('id', oSettings.sTableId + '_' + firstClassName);
                nPrevious.setAttribute('id', oSettings.sTableId + '_' + previousClassName);
                nNext.setAttribute('id', oSettings.sTableId + '_' + nextClassName);
                nLast.setAttribute('id', oSettings.sTableId + '_' + lastClassName);
            }
 
            nInput.type = 'text';
 
            info = info.replace(/_INPUT_/g, '</span>' + nInput.outerHTML + '<span>');
            info = info.replace(/_TOTAL_/g, '</span>' + nTotal.outerHTML + '<span>');
            nInfo.innerHTML = '<span>' + info + '</span>';
 
            nPaging.appendChild(nFirst);
            nPaging.appendChild(nPrevious);
            $(nInfo).children().each(function (i, n) {
                nPaging.appendChild(n);
            });
            nPaging.appendChild(nNext);
            nPaging.appendChild(nLast);
 
            $(nFirst).click(function() {
                var iCurrentPage = calcCurrentPage(oSettings);
                if (iCurrentPage !== 1) {
                    oSettings.oApi._fnPageChange(oSettings, 'first');
                    fnCallbackDraw(oSettings);
                }
            });
 
            $(nPrevious).click(function() {
                var iCurrentPage = calcCurrentPage(oSettings);
                if (iCurrentPage !== 1) {
                    oSettings.oApi._fnPageChange(oSettings, 'previous');
                    fnCallbackDraw(oSettings);
                }
            });
 
            $(nNext).click(function() {
                var iCurrentPage = calcCurrentPage(oSettings);
                if (iCurrentPage !== calcPages(oSettings)) {
                    oSettings.oApi._fnPageChange(oSettings, 'next');
                    fnCallbackDraw(oSettings);
                }
            });
 
            $(nLast).click(function() {
                var iCurrentPage = calcCurrentPage(oSettings);
                if (iCurrentPage !== calcPages(oSettings)) {
                    oSettings.oApi._fnPageChange(oSettings, 'last');
                    fnCallbackDraw(oSettings);
                }
            });
 
            $(nPaging).find('.' + paginateInputClassName).keyup(function (e) {
                // 38 = up arrow, 39 = right arrow
                if (e.which === 38 || e.which === 39) {
                    this.value++;
                }
                // 37 = left arrow, 40 = down arrow
                else if ((e.which === 37 || e.which === 40) && this.value > 1) {
                    this.value--;
                }
 
                if (this.value === '' || this.value.match(/[^0-9]/)) {
                    /* Nothing entered or non-numeric character */
                    this.value = this.value.replace(/[^\d]/g, ''); // don't even allow anything but digits
                    return;
                }
 
                var iNewStart = oSettings._iDisplayLength * (this.value - 1);
                if (iNewStart < 0) {
                    iNewStart = 0;
                }
                if (iNewStart >= oSettings.fnRecordsDisplay()) {
                    iNewStart = (Math.ceil((oSettings.fnRecordsDisplay()) / oSettings._iDisplayLength) - 1) * oSettings._iDisplayLength;
                }
 
                oSettings._iDisplayStart = iNewStart;
                oSettings.oInstance.trigger("page.dt", oSettings);
                fnCallbackDraw(oSettings);
            });
 
            // Take the brutal approach to cancelling text selection.
            $('span', nPaging).bind('mousedown', function () { return false; });
            $('span', nPaging).bind('selectstart', function() { return false; });
 
            // If we can't page anyway, might as well not show it.
            var iPages = calcPages(oSettings);
            if (iPages <= 1) {
                $(nPaging).hide();
            }
        },
 
        'fnUpdate': function (oSettings) {
            if (!oSettings.aanFeatures.p) {
                return;
            }
 
            var iPages = calcPages(oSettings);
            var iCurrentPage = calcCurrentPage(oSettings);
 
            var an = oSettings.aanFeatures.p;
            if (iPages <= 1) // hide paging when we can't page
            {
                $(an).hide();
                return;
            }
 
            var disableClasses = calcDisableClasses(oSettings);
 
            $(an).show();
 
            // Enable/Disable `first` button.
            $(an).children('.' + firstClassName)
                .removeClass(oSettings.oClasses.sPageButtonDisabled)
                .addClass(disableClasses[firstClassName]);
 
            // Enable/Disable `prev` button.
            $(an).children('.' + previousClassName)
                .removeClass(oSettings.oClasses.sPageButtonDisabled)
                .addClass(disableClasses[previousClassName]);
 
            // Enable/Disable `next` button.
            $(an).children('.' + nextClassName)
                .removeClass(oSettings.oClasses.sPageButtonDisabled)
                .addClass(disableClasses[nextClassName]);
 
            // Enable/Disable `last` button.
            $(an).children('.' + lastClassName)
                .removeClass(oSettings.oClasses.sPageButtonDisabled)
                .addClass(disableClasses[lastClassName]);
 
            // Paginate of N pages text
            $(an).find('.' + paginateTotalClassName).html(iPages);
 
            // Current page number input value
            $(an).find('.' + paginateInputClassName).val(iCurrentPage);
        }
    };
})(jQuery);
   	</script>
<script>
$(document).ready(function() {
    var table = $('#example').DataTable( {
        
        dom: 'Bfrtlip',
        "pagingType": "input",
    //   "sDom": '<"customercount"i>rt<"bottom"<"#refresh">flp><"clear">',
    "columnDefs": [ {
            "searchable": true,
            "orderable": true,
            "targets": [0],
        } ],
       "order": [[ 1, 'asc' ]],
        buttons: [
                 {
                extend:    'copyHtml5',
                text:      '<i class="fa fa-files-o"></i> &nbsp; Copy',
                //titleAttr: 'Copy',
                title: $('h1').text(),
            },{
                extend:'excelHtml5',
                text:'<i class="fa fa-file-excel-o"></i>&nbsp; Excel',
               // titleAttr: 'Excel',
                title: $('h4').text(),
                 customize: function( xlsx ) {
                var sheet = xlsx.xl.worksheets['sheet1.xml'];
                        $('row c[r^="A"]',sheet).attr('s','51'); 
                       //   $('row c[r^="C"]',sheet).attr('s','51'); 
                          $('row:first c',sheet).attr('s','51');
            },
            exportOptions: {
                columns: [0,1,2,3,4,5,6], // Only name, email and role
                }
            },
            {
                extend:    'csvHtml5',
                text:      '<i class="fa fa-file-text-o"></i> &nbsp; CSV',
               // titleAttr: 'CSV',
                title: $('h1').text(),
            },
            {
                extend:    'pdfHtml5',
                text:      '<i class="fa fa-file-pdf-o"></i>&nbsp;  PDF',
             customize: function (doc) {
						//Remove the title created by datatTables
						doc.content.splice(0,1);
						//Create a date string that we use in the footer. Format is dd-mm-yyyy
						var now = new Date();
						var jsDate = now.getDate()+'-'+(now.getMonth()+1)+'-'+now.getFullYear();
						var logo = 'data:image/jpeg;base64,<?php echo e($logo->logourl); ?>';
						doc.pageMargins = [20,60,20,20];
						doc.defaultStyle.fontSize = 10;
						doc.styles.tableHeader.fontSize = 10;
						doc['header']=(function() {
							return {
								columns: [{
									    alignment: 'left',
										image: logo,
										width: 50,margin: [200,5]
									},{
										alignment: 'CENTER',
										text: 'List of Client',
										fontSize: 20,
										margin: [50,35,50,40],
									},],
								margin: [20, 0, 0,12],alignment: 'center',
							}
						});
						var objLayout = {};
						objLayout['hLineWidth'] = function(i) { return 2; };
						objLayout['vLineWidth'] = function(i) { return 2; };
						objLayout['hLineColor'] = function(i) { return '#ccc'; };
						objLayout['vLineColor'] = function(i) { return '#ccc'; };
						objLayout['paddingLeft'] = function(i) { return 14; };
						objLayout['paddingRight'] = function(i) { return 14; };
						doc.content[0].layout = objLayout;
				},
                 exportOptions: {
                 columns: [0,1,2,3,4,5,6], // Only name, email and role
                }
            },
            {
           extend: 'print',
          text: '<i class="fa fa-print"></i>&nbsp; Print',
         customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '10pt' )
                        .prepend(
                            '<center><img src="https://financialservicecenter.net/public/business/<?php echo e($logo->logo); ?>"/></center>'
                        );
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );
                },
         exportOptions: {
          columns: [0,1,2,3,4,5,6]
      },
      footer: true,
      autoPrint: true
    },
        ],
      
    } );

$('input.global_filter').on('keyup click', function() {
        filterGlobal();
    } );
    $('input.column_filter').on( 'keyup click', function() {
        filterColumn( $(this).parents('tr').attr('data-column'));
    } );
var cou =  table.on( 'order.dt search.dt', function () {
       table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i)
       {
          cell.innerHTML = i + 1;
          table.cell(cell).invalidate('dom'); 
       } );
    } ).draw();

    table.columns(7)
        .search('^(?:(?!6|3|4|5|1).)*$\r?\n?', true, false)
        .draw();
    $("#choice").on("change",function(){
 var _val = $(this).val();//alert(_val);
 if(_val == '6'){   
        table.columns(7).search(_val).draw();
  }
  else if(_val == '2'){  
  table.columns(7).search(_val).draw();
//  table.columns(8).search(_val).draw();
   }
  else if(_val == '3'){  //alert();
         table.columns(7).search(_val).draw();
  }
   else if(_val == '4'){  //alert();
         table.columns(7).search(_val).draw();
  }
  else if(_val == '5'){  //alert();
         table.columns(7).search(_val).draw();
  }
  else{
        table.columns().search('').draw(); 
  }
  });
    $("#choice1").on("change",function(){
 var _val = $(this).val();//alert(_val);
 if(_val == '1'){   
          table.columns().search('').draw(); 
  }   
   <?php $__currentLoopData = $common; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $com): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
else if(_val == '<?php echo e($com->business_name); ?>'){   
        table.columns(3).search(_val).draw();
  }
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  else{
        table.columns().search('').draw(); 
  }
  })
});
function filterGlobal() {
    $('#example').DataTable().search(
        $('#global_filter').val(),
        $('#global_regex').prop('checked'),
        $('#global_smart').prop('checked')
    ).draw();
}
function filterColumn(i) {
    $('#example').DataTable().column( i ).search(
        $('#col'+i+'_filter').val(),
        $('#col'+i+'_regex').prop('checked'),
        $('#col'+i+'_smart').prop('checked')
    ).draw();
}
$("#types").on('change',function() {
  // For unique choice
  var selVal = $( "#types option:selected" ).val(); 
   if(selVal=='Type')  
  {
      $('#filter_global').hide();
      $('#filter_col3').hide();
      $('#filter_col4').hide();
      $('#filter_col5').hide();
      $('#filter_col6').hide();
      $('#filter_col7').hide();
      $('#filter_col2').show();
  }
  else if(selVal=='EE / User ID')  
  {
        $('#filter_col7').hide();
      $('#filter_global').hide();
      $('#filter_col2').hide();
      $('#filter_col4').hide();
      $('#filter_col5').hide();
      $('#filter_col6').hide();
      $('#filter_col3').show();
  }
    else if(selVal=='Employee Name')  
  {
      $('#filter_global').hide();
      $('#filter_col2').hide();
      $('#filter_col3').hide();
      $('#filter_col5').hide();
      $('#filter_col6').hide();
      $('#filter_col4').show();  
      $('#filter_col7').hide();
  }
   else if(selVal=='Email ID')  
  {
      $('#filter_global').hide();
      $('#filter_col2').hide();  
      $('#filter_col7').hide();
      $('#filter_col3').hide();
      $('#filter_col4').hide();
      $('#filter_col6').hide();
      $('#filter_col5').show();
  }
  else if(selVal=='Tel. Number')  
  {
      $('#filter_global').hide();
      $('#filter_col2').hide();
      $('#filter_col3').hide();
      $('#filter_col4').hide();
      $('#filter_col5').hide();  
      $('#filter_col7').hide();
      $('#filter_col6').show();
  }
  else if(selVal=='Tel. Number')  
  {
      $('#filter_global').hide();
      $('#filter_col2').hide();
      $('#filter_col3').hide();
      $('#filter_col4').hide();
      $('#filter_col5').hide();  
      $('#filter_col6').hide();
      $('#filter_col7').show();
  }
  else{
      $('#filter_global').show();
       $('#filter_col3').hide();
      $('#filter_col4').hide();
      $('#filter_col5').hide();
      $('#filter_col6').hide();  
      $('#filter_col7').hide();
      $('#filter_col2').hide();
  }
});
</script>
<script type="text/javascript">
 $(function () {
     $('#example').removeClass('hidden');
 });
</script>
<style>.hidden{ display:none;}</style>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>