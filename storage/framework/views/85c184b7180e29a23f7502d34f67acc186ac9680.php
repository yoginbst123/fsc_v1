<?php $__env->startSection('main-content'); ?>
<style>
td{ text-align:center}
</style>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>Link</h1>
    </section>
    <!-- Main content -->
    <section class="content">	
	<div class="row">
		<div class="col-md-12">
				<div class="box box-success">
			      <div class="box-header">
              
              <div class="box-tools pull-right">
                
              </div>
            </div>
				<div class="col-md-12">
             <form method="post" action="<?php echo e(route('link.update',$link->id)); ?>" class="form-horizontal" id="businessname" name="businessname" enctype="multipart/form-data">
					<?php echo e(csrf_field()); ?><?php echo e(method_field('PATCH')); ?>

					
<div class="form-group <?php echo e($errors->has('type') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Type :</label>
							<div class="col-lg-5 col-md-8">



<select name="type" type="text" id="type" class="form-control category1" value="" >
<option value="">Select</option> 
<option value="Federal" <?php if($link->type=='Federal'): ?> selected <?php endif; ?>>Federal</option> 
<option value="State" <?php if($link->type=='State'): ?> selected <?php endif; ?>>State</option> 
<option value="County" <?php if($link->type=='County'): ?> selected <?php endif; ?>>County</option> 
<option value="Local City" <?php if($link->type=='Local City'): ?> selected <?php endif; ?>>Local City</option> 
<option value="Community Link" <?php if($link->type=='Community Link'): ?> selected <?php endif; ?>>Community Link</option> 
<option value="Other Link" <?php if($link->type=='Other Link'): ?> selected <?php endif; ?>>Other Link</option> 
</select>
								               <?php if($errors->has('type')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('type')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>
<div class="form-group <?php echo e($errors->has('name') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Name :</label>
							<div class="col-lg-5 col-md-8">
								<input name="name" type="text" id="name" class="form-control" value="<?php echo e($link->name); ?>"/>  <?php if($errors->has('name')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('name')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>
						
<div class="form-group <?php echo e($errors->has('subtype') ? ' has-error' : ''); ?>">
<label class="control-label col-md-3">Category :</label>
<div class="col-lg-5 col-md-8">
<select name="category" type="text" id="category" class="form-control" value="">
<option value="<?php echo e($link->category); ?>"><?php echo e($link->category); ?></option> 

</select> 
                                                         <?php if($errors->has('category')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>
							<div class="form-group dis <?php echo e($errors->has('link') ? ' has-error' : ''); ?>" <?php if($link->link): ?> <?php else: ?> style="display:none" <?php endif; ?>>
							<label class="control-label col-md-3">Link :</label>
							<div class="col-lg-5 col-md-8">
								<input name="link" type="text"  id="link" class="form-control" value="<?php echo e($link->link); ?>" />                                                            <?php if($errors->has('post_id')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('link')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>
<div class="divhide" <?php if($link->type=='Federal' || $link->type=='State'): ?> style="display:none" <?php endif; ?>>
						<div class="form-group <?php echo e($errors->has('post_id') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Post Id :</label>
							<div class="col-lg-5 col-md-8">
								<input name="post_id" type="text" readonly id="post_id" class="form-control" value="<?php echo e($link->post_id); ?>" />                                                            <?php if($errors->has('post_id')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('post_id')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>
<div class="form-group <?php echo e($errors->has('post_date') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Post Date :</label>
							<div class="col-lg-5 col-md-8">
								<input name="post_date" type="text" id="post_date" class="form-control" value="<?php echo date('M-d-Y');?>" readonly/>                                                            <?php if($errors->has('post_date')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('post_date')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>
						<div class="form-group <?php echo e($errors->has('linkimage') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Image :</label>
							<div class="col-lg-5 col-md-8">
								<!--<input type="file" name="linkimage[]" multiple id="linkimage" class="form-control"  />  -->
<label class="file-upload btn btn-primary">
	                Browse for file ... <input name="photo" style="opecity:0" placeholder="Upload Service Image" name="linkimage[]" multiple id="linkimage" type="file">
	            </label>
                                     <?php if($errors->has('linkimage')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('linkimage')); ?></strong>
										</span>
									<?php endif; ?>
<input type="hidden" name="linkimage1" id="linkimage1" class="form-control"  value="<?php echo e($link->linkimage); ?>" />

<?php if(empty($link->linkimage)): ?>
<img src="<?php echo e(url('public/images/')); ?>/noimage.svg" style="width:35%"> <?php else: ?>
<?php $images = explode('|',$link->linkimage); ?>
<?php $__currentLoopData = $images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<img src="<?php echo e(asset('public/link')); ?>/<?php echo e($image); ?>" alt="" style="width:35%" />   
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> <?php endif; ?>

 

							</div>
						</div>
						<div class="form-group <?php echo e($errors->has('link') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Description :</label>
							<div class="col-md-8">
								<textarea id="editor1" name="description"><?php echo e($link->content); ?></textarea>
                                                                   <?php if($errors->has('description')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('description')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>					
<div class="form-group">
							<label class="control-label col-md-3">Client No. :</label>
							<div class="col-lg-5 col-md-8">
								<select name="clientname" class="form-control client_id">
<option value="">Select</option> 
<?php $__currentLoopData = $client; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $client1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<?php if(empty($client1->filename)): ?>
<?php else: ?>
<option value="<?php echo e($client1->filename); ?>"><?php echo e($client1->filename); ?></option> 
<?php endif; ?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</select> 
                                                        
							</div>
						</div>
<div id="email_status"></div>  
</div>
						<div class="card-footer">
						    <div class="row">
						    <div class="col-md-3"></div>
							<div class="col-xs-2" style="width:155px;">
								<input class="btn_new_save btn-primary1 primary1" type="submit" id="primary1" name="submit" value="Save">
							</div>
							<div class="col-xs-2" style="width:155px;">
								<a class="btn_new_cancel" href="<?php echo e(url('fac-Bhavesh-0554/link')); ?>">Cancel</a> 
							</div>
							</div>
						</div>
						
					</form>
				</div>
			</div>
		</div>
	</div>
	</section>
<!--</div>-->
<script>
$(document).ready(function(){
	$(document).on('change','.category1', function()
	{ 
		
		var id = $(this).val(); //alert(id);
	
		if(id == 'Federal' || id=='State')
		{
		    $('.divhide').hide();$('.dis').hide();
		}
		else if(id == 'Other Link')
		{
		    $('.divhide').hide();
		    $('.dis').show();
		}
		else
		{
		    $('.dis').hide();
		    $('.divhide').show();
		}
		$.get('<?php echo URL::to('/getlink'); ?>?id='+id, function(data)
		{ //alert(data);
            $('#category').empty();
           $.each(data, function(index, subcatobj)
		   {
			   $('#category').append('<option value="'+subcatobj.category+'">'+subcatobj.category+'</option>');
		   })

		});
			
	});
});
</script>
<script type="text/javascript">
$(document).ready(function(){
	$(document).on('change','.client_id', function()
	{ 
		
		var id = $(this).val(); //alert(id);
		$.get('<?php echo URL::to('/clientno1'); ?>?id='+id, function(data)
		{ //alert(data);
            $('#email_status').empty();
           $.each(data, function(index, subcatobj)
		   {
			$('#email_status').append('<div class="table-responsive"><table class="table"><thead><tr><th>Client No.</th><th>Client Name</th><th>Client Phone No.</th><th>Client Email</th></tr></thead><tbody><tr><td>'+subcatobj.filename+'</td><td>'+subcatobj.first_name+'</td><td>'+subcatobj.business_no+'</td><td>'+subcatobj.email+'</td></tr></tbody></table></div>');
		   })

		});
			
	});
});
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>