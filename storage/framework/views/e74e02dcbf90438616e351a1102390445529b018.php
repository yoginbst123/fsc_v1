<?php $__env->startSection('main-content'); ?>
<div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 fsc-content-head">
			<h4>Investor Registration</h4>
		</div>
	</div>
	
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-reg-business-category-div">
	<?php $__currentLoopData = $category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>		
	<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-reg-business-category-img">
		<a href="<?php if($cat->link == 'comman-registration/create'): ?> <?php echo e(url($cat->link,[Request::segment(2),Request::segment(3),$cat->id])); ?> <?php else: ?> <?php echo e(url($cat->link,[$cat->id,Request::segment(2),Request::segment(3)])); ?> <?php endif; ?>"><img src="<?php echo e(URL::asset('public/category/')); ?>/<?php echo e($cat->business_cat_image); ?>"/></a>
		<center><div class="services-tab"> <a href="<?php if($cat->link == 'comman-registration/create'): ?> <?php echo e(url($cat->link,[Request::segment(2),Request::segment(3),$cat->id])); ?> <?php else: ?> <?php echo e(url($cat->link,[$cat->id,Request::segment(2),Request::segment(3)])); ?> <?php endif; ?>"><span><div class="st_title"><?php echo e($cat->business_cat_name); ?></div></span></a></div></center>
	</div>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	<!--<?php $__currentLoopData = $category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>		
	<form id="logout-form1" action="<?php if($cat->link == 'comman-registration'): ?> <?php echo e(route('comman-registration.create')); ?> <?php else: ?> <?php echo e(url($cat->link,$cat->id)); ?> <?php endif; ?>" method="get">
	<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-reg-business-category-img">
	<button type="submit" name="submit"><img src="<?php echo e(URL::asset('category/')); ?>/<?php echo e($cat->business_cat_image); ?>"/></button>
	<center><div class="services-tab"><button type="submit" name="submit" value="submit"><span><div class="st_title"><?php echo e($cat->business_cat_name); ?></div></span></button></div></center>
	</div>
	<input type="hidden" value="<?php echo e($cat->bussiness_name); ?>" name="business_id">
		 <?php if(isset($_GET['user_type'])): ?>
		 <input type="hidden" value="<?php echo e($_GET['user_type']); ?>" name="user_type">
		 <?php endif; ?>
	     <input type="hidden" value="<?php echo e($cat->id); ?>" name="business_cat_id">
		 <input type="hidden" id="business_brand_id" name="business_brand_id" value="" />
		 <input type="hidden" id="business_brand_category_id" name="business_brand_category_id" value="" />
</form>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>-->
	</div>	
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('front-section.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>