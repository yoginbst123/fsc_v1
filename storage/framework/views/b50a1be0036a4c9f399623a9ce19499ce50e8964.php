<?php $__env->startSection('main-content'); ?>
<div class="content-wrapper">
<!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>Address Book</h1>
    </section>
    <!-- Main content -->
    <section class="content">

	<div class="row">
		<div class="col-md-12">
			<div class="box box-success">
			      <div class="box-header">
             
              <div class="box-tools pull-right">
                
              </div>
            </div>
				<div class="col-md-12">
					<form method="post" action="#" class="form-horizontal" id="businessname" name="businessname" enctype="multipart/form-data">
				
						
						
						<div class="col-md-12">
						    <div class="form-group ">
							<label class="control-label col-md-3">Type :</label>
							<div class="col-lg-6 col-md-8">
								<input name="controlname" type="text" id=""  class="form-control" readonly value="<?php if($gold->type=='Approval'): ?> Client <?php else: ?> <?php echo e(ucwords($gold->type)); ?> <?php endif; ?>">
															</div>
						</div>
						
						    <div class="form-group ">
							<label class="control-label col-md-3">Full Name :</label>
							<div class="col-lg-6 col-md-8">
								<input name="controlname" type="text" id=""  class="form-control" readonly value="<?php echo e(ucwords($gold->firstName).' '.ucwords($gold->middleName).' '.ucwords($gold->lastName)); ?>">
															</div>
						</div>
						
						    <div class="form-group ">
							<label class="control-label col-md-3">Address :</label>
							<div class="col-lg-6 col-md-8">
								<input name="controlname" type="text" id=""  class="form-control" readonly value="<?php echo e(ucwords($gold->address1)); ?>">
															</div>
						</div>

							<div class="form-group ">
							<label class="control-label col-md-3">Country / State  / City :</label>
							<div class="col-lg-6 col-md-8">
							<div class="row">
							<div class="col-md-4">
                            <input type="text" class="form-control" id="city" name="countryId" readonly placeholder="Country" value="<?php echo e($gold->countryId); ?>">
							</div>
							<div class="col-md-4">
                          <input type="text" class="form-control" id="city" name="countryId"  readonly placeholder="State" value="<?php echo e($gold->stateId); ?>">
							</div>
							<div class="col-md-4">
                            <input type="text" class="form-control" id="city" name="countryId"  readonly placeholder="City" value="<?php echo e($gold->city); ?>">
							</div>
							</div>
							</div>
						</div>
						
							<div class="form-group ">
							<label class="control-label col-md-3">Email :</label>
							<div class="col-lg-6 col-md-8">
							<input type="text" class="form-control" id="" name="company_email"  readonly value="<?php echo e($gold->email); ?>">
															</div>
						</div>
						
							<div class="form-group ">
							<label class="control-label col-md-3">Phone :</label>
							<div class="col-lg-6 col-md-8">
							<div class="row">
							<div class="col-md-4">
							<input name="telephone" type="tel" id="" value="<?php echo e($gold->telephoneNo1); ?>"  readonly class="form-control" placeholder="(000)000-0000">
															</div>
														<!--	<div class="col-md-2">
							<select name="telephoneNo1Type" id="telephoneNo1Type" class="form-control fsc-input" style="height:auto">
                                                <option value="Office" selected="">Office</option><option value="Mobile">Mobile</option>
                                             </select>
															</div>
															<div class="col-md-2">
							<input class="form-control fsc-input" id="ext1" maxlength="5" name="ext1" value="" placeholder="Ext" type="text">
															</div>-->
						    </div>
						    </div>
						    </div>
						</div>
						
						
						<div class="card-footer">
					        <div class="col-md-12">
						    	<div class="row">
								    <div class="col-md-3"></div>
								    <div class="col-xs-3" style="width:155px;">
									<a href="<?php echo e(url('/fac-Bhavesh-0554/addressbook')); ?>" class="btnPrevious btn btn-primary">Back</a>
									</div>
								</div>
							</div>
						</div>
						
					</form>
				</div>
			</div>
		</div>
	</div>
	</section>
<!--</div>-->

<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>