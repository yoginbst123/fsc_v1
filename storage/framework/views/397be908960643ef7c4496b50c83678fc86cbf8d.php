<?php $__env->startSection('main-content'); ?>
<div class="content-wrapper">
      <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>Edit Task</h1>
    </section>
   <!-- Main content -->
    <section class="content">
   <div class="row">
      <div class="col-md-12">
         <div class="box box-success">
			      <div class="box-header">
          
              <div class="box-tools pull-right">
                
              </div>
            </div>
            <div class="card-body col-md-12">
               <form method="post" action="<?php echo e(route('task.update',$task->id)); ?>" class="form-horizontal" id="content" name="content" enctype="multipart/form-data">
                  <?php echo e(csrf_field()); ?> <?php echo e(method_field('PATCH')); ?>

                   <div class="form-group">
                   <label class="control-label col-md-3">Date / Day / Time:</label>
                     <div class="col-lg-6 col-md-9">
                         <div class="row">
                     <div class="col-lg-4 col-md-4">
                        <div class="">
                           <input type="text" name="date" id="date" class="form-control" value="<?php echo e(date('M-d Y',strtotime($task->created_at))); ?>" readonly placeholder="Date">
                        </div>
                     </div>
                      
                     <div class="col-lg-4 col-md-4 col-xs-6">
                        <div class="">
                           <input type="text" name="day" id="day" class="form-control" placeholder="Day" readonly value="<?php echo e(date('l',strtotime($task->created_at))); ?>">
                        </div>
                     </div>
                     <input type="hidden" name="state" readonly id="state" value="employee"  class="form-control">
                     <div class="col-lg-3 col-md-4 col-xs-6">
                        <div class="">
                           <input type="text" name="time" id="time" class="form-control" readonly value="<?php echo e(date('g:i a',strtotime($task->created_at))); ?>" placeholder="Time">
                        </div>
                     </div>
                     </div></div>
                  </div>
<div class="form-group<?php echo e($errors->has('employee') ? ' has-error' : ''); ?>">
                     <label class="control-label col-md-3">Employee / User :</label>
                     <div class="col-lg-6 col-md-9">
                        <div class="row">
                            <div class="col-lg-11 col-md-12">
                           <select name="employee" id="employee" class="form-control fsc-input">
                              <option value="">Select Employee</option>
                               
                              <?php $__currentLoopData = $emp1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $em): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                               <option value='<?php echo e($em->id); ?>' <?php if($task->employeeid==$em->id): ?> selected <?php endif; ?>><?php echo e($em->firstName.' '.$em->middleName.' '.$em->lastName); ?></option>
                                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                           </select>
                        </div>
                        </div>
                        <?php if($errors->has('employee')): ?>
                        <span class="help-block">
                        <strong><?php echo e($errors->first('employee')); ?></strong>
                        </span>
                        <?php endif; ?>	
                     </div>
                  </div>
                     <div class="form-group<?php echo e($errors->has('employee') ? ' has-error' : ''); ?>">
                     <label class="control-label col-md-3">For Whom :</label>
                     <div class="col-lg-6 col-md-9">
                        <div class="row">
                            <div class="col-lg-11 col-md-12">
                           <select name="whone" id="whone" class="form-control fsc-input">
                              <option value="">Select</option>
                              <option value='Client' <?php if($task->whone=='Client'): ?> selected <?php endif; ?>>Client</option>
                              <option value='Other' <?php if($task->whone=='Other'): ?> selected <?php endif; ?>>Other</option>
                           </select>
                        </div>
                        </div>
                        	
                     </div>
                  </div>
                   <div class="form-group<?php echo e($errors->has('client') ? ' has-error' : ''); ?>" style="display:none;"  id="clients">
                     <label class="control-label col-md-3">Client :</label>
                     <div class="col-lg-6 col-md-9">
                        <div class="row">
                            <div class="col-lg-11 col-md-12">
                            <?php //echo "<pre>";print_r($customer);?>
                           <select name="client" id="client" class="form-control fsc-input">
                              <option value="">Select</option>
                             <?php $__currentLoopData = $customer; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cust): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value='<?php echo e($cust->id); ?>' <?php if($task->client==$cust->id) { echo 'Selected';}?>><?php if($cust->business_id !='6') { echo $cust->company_name.' ('.$cust->filename.')';} else { echo $cust->first_name.' '.$cust->last_name.' ('.$cust->filename.')';}?></option>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                           </select>
                        </div>
                    	</div>
                     </div>
                  </div>
                   <div class="form-group<?php echo e($errors->has('other') ? ' has-error' : ''); ?>" style="display:none;"  id="client1">
                     <label class="control-label col-md-3">Other :</label>
                     <div class="col-lg-6 col-md-9">
                        <div class="row">
                            <div class="col-lg-11 col-md-12">
                            <input type="text" name="othername" id="othername" class="form-control" value="<?php echo e($task->othername); ?>">
                        </div>
                    	</div>
                     </div>
                  </div>
                  <div class="form-group<?php echo e($errors->has('title') ? ' has-error' : ''); ?>">
                     <label class="control-label col-md-3">Subject :</label>
                     <div class="col-lg-6 col-md-9">
                        <div class="row">
                            <div class="col-lg-11 col-md-12">
                           <input type="text" value="<?php echo e($task->title); ?>" name="title" id="title" class="form-control fsc-input">
                            <input type="hidden" value="<?php echo e($task->admin_id); ?>" name="admin_id" id="admin_id" class="form-control fsc-input">
                        </div>
                        </div>
                     </div>
                  </div>
                   <div class="form-group">
                     <label class="control-label col-md-3">Priority :</label>
                     <div class="col-lg-6 col-md-9">
                        <div class="row">
                            <div class="col-lg-11 col-md-12">
                           <select name="priority" id="priority" class="form-control fsc-input">
                              <option value='Important' <?php if($task->priority=='Important'): ?> selected <?php endif; ?>>Regular</option>
                             <option value='Urgent' <?php if($task->priority=='Urgent'): ?> selected <?php endif; ?>>Urgent</option>
                             <option value='Very Urgent' <?php if($task->priority=='Very Urgent'): ?> selected <?php endif; ?>>Very Urgent</option>
                             
                           </select>
                        </div>
                        </div>
                        
                     </div>
                  </div>
                  
                  <div class="form-group">
                     <label class="control-label col-md-3">Due Date :</label>
                     <div class="col-lg-6 col-md-9">
                        <div class="row">
                            <div class="col-lg-11 col-md-12">
                           <input type="text" name="duedate" id="duedate" value="<?php echo e($task->duedate); ?>" class="form-control fsc-input" readonly>
                        </div>
                        </div>
                     
                     </div>
                  </div>
                  <div class="form-group<?php echo e($errors->has('description') ? ' has-error' : ''); ?>">
                     <label class="control-label col-md-3">Description :</label>
                     <div class="col-lg-6 col-md-9">
                        <div class="row">
                            <div class="col-lg-11 col-md-12">
                           <textarea id="editor1" name="description" rows="10" cols="80"><?php echo $task->content; ?></textarea>
                        </div>
                        <?php if($errors->has('description')): ?>
                        <span class="help-block">
                        <strong><?php echo e($errors->first('description')); ?></strong>
                        </span>
                        <?php endif; ?>	
                     </div>
                     </div>
                  </div>
                    
                   <div class="form-group">
                     <label class="control-label col-md-3"></label>
                     <div class="col-md-8">
                        <div class="row">
                           <div class="card-footer">
                      <div class="col-xs-2" style="width:155px;">
									<input class="btn_new_save btn-primary1" type="submit" name="submit" value="Send">
									</div>
									<div class="col-xs-2" style="width:155px;">
									<a class="btn_new_cancel" href="<?php echo e(url('fac-Bhavesh-0554/task')); ?>">Cancel</a> 
									</div>
                  
                  </div>
                        </div>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
       </section>
</div>

<script>
   $(document).ready(function() {
  var dateInput = $('input[name="duedate"]'); 
  var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : 'body';
  dateInput.datepicker({
    format: 'M-dd-yyyy',
    container: container,
    todayHighlight: true,
    autoclose: true,
    startDate: truncateDate(new Date())
  });

  $('#duedate').datepicker('setStartDate', truncateDate(new Date())); 
});

function truncateDate(date) {
  return new Date(date.getFullYear(), date.getMonth(), date.getDate());
}
</script>
<script>
$(document).ready(function()
{
    var tvalue=$('#whone').val();
    //alert(tvalue);
    if(tvalue=='Client')
      {
          $('#clients').show();
          $('#client1').hide();
      }
      else if(tvalue=='Other')
      {
          $('#clients').hide();
          $('#client1').show();
      }
      else
      {
          $('#clients').hide();
          $('#client1').hide();
      }
    
    $('#whone').on('change', function() {
  if(this.value=='Client')
  {
      $('#clients').show();
      $('#client1').hide();
  }
  else if(this.value=='Other'){
      $('#clients').hide();
      $('#client1').show();
      
  }
  else
  {
      $('#clients').hide();
      $('#client1').hide();
  }
});
    });
</script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6/js/select2.min.js"></script>
<script>
$("#client").select2({
})
</script>
<style>
    .select2 {width:100% !important;}
    .select2-container .select2-selection--single {
   
    border: 2px solid #00468F;
}.select2-container .select2-selection--single{ height:36px;}
</style
<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>