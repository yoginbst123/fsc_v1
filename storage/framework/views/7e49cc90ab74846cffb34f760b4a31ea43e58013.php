<?php $__currentLoopData = $slider; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $slide): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<div class="item <?php if($loop->first): ?> active <?php endif; ?>"><img class="img-responsive" src="<?php echo e(URL::asset('public/slider')); ?>/<?php echo e($slide->slider_image); ?>" alt="<?php echo e($slide->slider_name); ?>"></div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>	
