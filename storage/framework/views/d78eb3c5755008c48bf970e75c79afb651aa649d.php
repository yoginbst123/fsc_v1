<?php $__env->startSection('main-content'); ?>
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>Services</h1>
    </section>
    <!-- Main content -->
    <section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-success">
			      <div class="box-header">
             
              <div class="box-tools pull-right">
                
              </div>
            </div>
				<div class="col-md-12">
<form method="post" enctype="multipart/form-data" class="form-horizontal" action="<?php echo e(route('services.update', $service->id)); ?>" id="">
					<?php echo e(csrf_field()); ?>

					<?php echo e(method_field('PATCH')); ?>

						<div class="form-group <?php echo e($errors->has('service_name') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Service Name :</label>
							<div class="col-md-8">
							<input name="service_name" type="text" id="service_name" class="form-control" value="<?php echo e($service->service_name); ?>" />
                                    <?php if($errors->has('service_name')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('service_name')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>



						
						<div class="form-group <?php echo e($errors->has('<div id="div" style="display: none;">

  </div>') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Description :</label>
							<div class="col-md-8">
							<textarea id="editor1" name="description"><?php echo e($service->description); ?></textarea>
                                                                   <?php if($errors->has('description')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('description')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>		

<div class="row">
<div class="col-md-3">
</div>
<div class="col-md-8">
<div class="danger" style="background-color: #ff8c8c;border-left: 6px solid #f44336; padding:15px; font-size:16px; margin-bottom:10px;">
  <strong>Step-2 apply services</strong> 
</div>
</div>
</div>


<div class="form-group <?php echo e($errors->has('<div id="div" style="display: none;">

  </div>') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Description1 :</label>
							<div class="col-md-8">
							<textarea id="editor2" name="description1"><?php echo e($service->description1); ?></textarea>
                                                                   <?php if($errors->has('description1')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('description1')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>						
				<div class="card-footer">
						    <div class="form-group">
							<label class="control-label col-md-3"></label>
							<div class="col-xs-2" style="width:155px;">
<input class="btn_new_save btn-primary1" type="submit" name="submit" value="Save">
							</div>
							<div class="col-xs-2" style="width:155px;">
<a class="btn_new_cancel" href="<?php echo e(url('fac-Bhavesh-0554/services')); ?>">Cancel</a> 
							</div>
						</div>
						  </div>
						
					</form>
				</div>
			</div>
		</div>

	</div>
 </section>	
<!--</div>-->



<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>