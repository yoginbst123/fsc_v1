<?php $__env->startSection('main-content'); ?>
<style>
    .form-check{width: 50%;float: left;}
</style>
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>How To Do</h1>
    </section>
    <!-- Main content -->
    <section class="content">
	<div class="row">
		<div class="col-md-12">
				<div class="box box-success">
			      <div class="box-header">
              
              <div class="box-tools pull-right">
                
              </div>
            </div>
				<div class="col-md-12">			
					<form method="post" action="<?php echo e(route('howtodo.update',$homecontent->id)); ?>" class="form-horizontal" id="content" name="content" enctype="multipart/form-data">
					<?php echo e(csrf_field()); ?> <?php echo e(method_field('PATCH')); ?>

						<div class="form-group<?php echo e($errors->has('subject') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Subject :</label>
							<div class="col-md-4">
								<input name="subject" type="type" id="subject" value="<?php echo e($homecontent->subject); ?>" class="form-control">
								<?php if($errors->has('subject')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('subject')); ?></strong>
										</span>
									<?php endif; ?>							
							</div>
						</div>	
							<div class="form-group<?php echo e($errors->has('website') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Website :</label>
							<div class="col-md-4">
									<input name="website" type="text" id="website"  value="<?php echo e($homecontent->website); ?>" class="form-control">			
							</div>
						</div>
						
						<div class="form-group<?php echo e($errors->has('software') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Software :</label>
							<div class="col-md-4">
									<input name="software" type="text" id="software" value="<?php echo e($homecontent->software); ?>" class="form-control">			
							</div>
						</div>
						
						<div class="form-group<?php echo e($errors->has('telephone') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Telephone :</label>
							<div class="col-md-2">
								<input name="telephone" type="text" id="telephone"  value="<?php echo e($homecontent->telephone); ?>" class="form-control">
								<?php if($errors->has('telephone')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('telephone')); ?></strong>
										</span>
									<?php endif; ?>							
							</div>
						</div>
						
						    <div class="input_fields_wrap_notes">
												   
													 <?php $l=1; $notecon = count($howtodo);?>
													    <?php if($notecon != NULL): ?>
													    	<?php if($l==1): ?>
												            <div class="col-md-12">
														        <a class="btn btn-primary" style="position: absolute;right: 143px;" onclick="education_field_note();">Add </a>
														    </div>
														<?php endif; ?>
														    
													     <?php $__currentLoopData = $howtodo; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $notes): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php if(empty($notes->noteid)): ?>
													     <input name="noteid[]" value="" type="hidden" placeholder="" id="noteid" class="">
													     <input name="stepid[]" value="<?php echo e($notes->id); ?>" type="hidden" placeholder="" id="stepid" class="textonly form-control">
													     <input type="hidden" name="photos2[]"value="<?php echo e($notes->photo); ?>"> 
													     <div class="form-group">
															<label class="control-label col-md-3">Step <?php echo $l; $l++;?>:</label>
															<div class="col-md-4">
														        <textarea name="step[]"  rows="1" type="text" placeholder="Create Note YY" id="step" class="form-control"><?php echo e($notes->steps); ?></textarea>
														    </div> 
														<div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                              <label class="file-upload btn btn-primary">
                                                              Browse for file ... <input name="photo[]" style="opecity:0" placeholder="Upload Service Image" id="photo" type="file">
                                                              </label>
                                                              <?php echo $notes->photo;?>
                                                            </div>
                                                         
			                                                            											
                                                                                        
														
														    <div class="col-md-1">
														        <a href="#myModalnote_<?php echo e($notes->id); ?>" id="add_row_note" role="button" class="btn btn-danger remove_note" title="Remove field" data-toggle="modal"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></a>
														    </div>	
														</div>
														
														<div id="input_fields_wrap_notes<?php echo e($notes->id); ?>"></div>
														 <?php $__currentLoopData = $howtodo1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $notes1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
														 <?php if($notes1->noteid==$notes->id): ?>
														 <input name="noteid[]" value="<?php echo e($notes1->noteid); ?>" type="hidden" placeholder="" id="noteid" class="">
													     <input name="stepid[]" value="<?php echo e($notes1->id); ?>" type="hidden" placeholder="" id="stepid" class="textonly form-control">
													     
													     <div class="form-group">
															<label class="control-label col-md-3">Step <?php echo $l; $l++;?>:</label>
															<div class="col-md-6">
														<textarea name="step[]"  rows="1" type="text" placeholder="Create Note" id="step" class="form-control"><?php echo e($notes1->steps); ?></textarea>
														</div>   
														
														
														    
														    
														<div class="col-md-1">
														    <a href="#myModalnote_<?php echo e($notes1->id); ?>" id="add_row_note" role="button" class="btn btn-danger remove_note" title="Remove field" data-toggle="modal"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></a>
														    </div>	
														</div>
														
														<div id="input_fields_wrap_notes<?php echo e($notes1->id); ?>"></div>
														
														<script>
                                                            var room1<?php echo e($notes1->id); ?> = 0;
                                                            var coun ='';
                                                            var z = room1<?php echo e($notes1->id); ?>; 
                                                            function education_field_note<?php echo e($notes1->id); ?>() {
                                                            room1<?php echo e($notes->id); ?>++;
                                                            z++;
                                                            var objTo = document.getElementById('input_fields_wrap_notes<?php echo e($notes1->id); ?>');
                                                            var divtest = document.createElement("div");
                                                            divtest.setAttribute("class", "form-group removeclass"+z);
                                                            divtest.innerHTML = '<label class="control-label col-md-3">Step '+ z +' :</label><div class="col-md-6"><input name="noteid[]" value="<?php echo e($notes->id); ?>" type="hidden" placeholder="" id="noteid" class=""><input name="stepid[]" value="" type="hidden" placeholder="" id="stepid" class=""><textarea name="step[]"  rows="1" type="text" id="step" placeholder="Create Step 888" class="form-control" ></textarea></div><div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin"><label class="file-upload btn btn-primary">Browse for file ... <input name="photo[]" style="opecity:0" placeholder="Upload Service Image" id="photo" type="file"></label></div></div></div><div class="col-md-1"> <button class="btn btn-danger" type="button" onclick="remove_education_fields<?php echo e($notes->id); ?>('+ z +');"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span> </button></div></div></div>';
                                                            //divtest.innerHTML = '<label class="control-label col-md-3">Step '+ z +' :</label><div class="col-md-5"><input name="stepid[]" value="" type="hidden" placeholder="" id="stepid" class=""><textarea name="step[]"  rows="1" type="text" id="step" placeholder="Create Step" class="form-control"></textarea></div><div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin"><label class="file-upload btn btn-primary">Browse for file ... <input name="photo[]" style="opecity:0" placeholder="Upload Service Image" id="photo" type="file"></label></div></div><div class="col-md-1"> <button class="btn btn-danger" type="button" onclick="remove_education_fields('+ z +');"  style="width: 50px;  margin-left: 8px;"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span> </button></div></div></div>';
                                                            var rdiv = 'removeclass'+z;
                                                            var rdiv1 = 'Schoolname'+z;
                                                            objTo.appendChild(divtest);
                                                            }
                                                            function remove_education_fields<?php echo e($notes1->id); ?>(rid) {
                                                            $('.removeclass'+rid).remove();
                                                            z--;
                                                            room1<?php echo e($notes1->id); ?>--;
                                                            }
                                                        </script>
														<?php endif; ?>
														<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
														
														
                                                        <script>
                                                        var room1<?php echo e($notes->id); ?> = 0;
                                                        var coun ='';
                                                        var z = room1<?php echo e($notes->id); ?>; 
                                                        function education_field_note<?php echo e($notes->id); ?>() {
                                                        room1<?php echo e($notes->id); ?>++;
                                                        z++;
                                                        var objTo = document.getElementById('input_fields_wrap_notes<?php echo e($notes->id); ?>');
                                                        var divtest = document.createElement("div");
                                                        divtest.setAttribute("class", "form-group removeclass"+z);
                                                        divtest.innerHTML = '<label class="control-label col-md-3">Step '+ z +' :</label><div class="col-md-6"><input name="noteid[]" value="<?php echo e($notes->id); ?>" type="hidden" placeholder="" id="noteid" class=""><input name="stepid[]" value="" type="hidden" placeholder="" id="stepid" class=""><textarea name="step[]"  rows="1" type="text" id="step" placeholder="Create Step 7777" class="form-control" ></textarea></div></div><div class="col-md-1"> <button class="btn btn-danger" type="button" onclick="remove_education_fields<?php echo e($notes->id); ?>('+ z +');"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span> </button></div></div></div>';
                                                        var rdiv = 'removeclass'+z;
                                                        var rdiv1 = 'Schoolname'+z;
                                                        objTo.appendChild(divtest);
                                                        }
                                                        function remove_education_fields<?php echo e($notes->id); ?>(rid) {
                                                        $('.removeclass'+rid).remove();
                                                        z--;
                                                        room1<?php echo e($notes->id); ?>--;
                                                        }
                                                        </script>	
                                                        <?php endif; ?>
														<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
													
													    <?php else: ?>
    													<input name="stepid[]" value="" type="hidden" placeholder="" id="stepid" class="textonly form-control">
    													<input name="noteid[]" value="" type="hidden" placeholder="" id="noteid" class="">
    													
													    <div class="form-group">
															<label class="control-label col-md-3">Step :</label>
															<div class="col-md-7">
														<input name="step[]" value=""  type="text" placeholder="Create Note AAA" id="step" class="textonly form-control">
														</div>   
														<div class="col-md-2">
														   <a class="btn btn-primary" onclick="education_field_note();" style="width: 50px;  margin-left: 8px;">Add</a>
														    </div>
														<?php if($l==2): ?>
														
														<?php else: ?>
														<div class="col-md-1">
														   
														    </div>
														    <?php endif; ?>
														</div>
														<?php endif; ?>
														<div id="input_fields_wrap_notes"></div>
													</div>
													
											
                            						<div class="card-footer">
                            						    <div class="form-group">
                                							<label class="control-label col-md-3"></label>
                                							<div class="col-md-2">
                                                                <input class="btn_new_save btn-primary1" type="submit" name="submit" value="Save">
                                							</div>
                                							<div class="col-md-2 row">
                                                                <a class="btn_new_cancel" href="<?php echo e(url('fac-Bhavesh-0554/howtodo')); ?>">Cancel</a> 
                                							</div>
                                						</div>
                            						</div>
						
					</form>
				</div>
			</div>
		</div>
	</div>
	</section>
<!--</div>-->
<script>
   $("#telephone").mask("(999) 999-9999");

</script>
<script>
var room1 = 0;
var coun ='';
var z = room1; 
function education_field_note() {
room1++;
z++;
var objTo = document.getElementById('input_fields_wrap_notes');
var divtest = document.createElement("div");
divtest.setAttribute("class", "form-group removeclass"+z);
//divtest.innerHTML = '<label class="control-label col-md-3">Step '+ z +' :</label><div class="col-md-6"><input name="noteid[]" value="" type="hidden" placeholder="" id="noteid" class=""><input name="stepid[]" value="" type="hidden" placeholder="" id="stepid" class=""><textarea name="step[]"  rows="1" type="text" id="step" placeholder="Create Step 000" class="form-control" ></textarea></div></div><div class="col-md-1"> <button class="btn btn-danger" type="button" onclick="remove_education_fields('+ z +');"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span> </button></div></div></div>';
divtest.innerHTML = '<label class="control-label col-md-3">Step '+ z +' :</label><div class="col-md-6"><input name="noteid[]" value="" type="hidden" placeholder="" id="noteid" class=""><input name="stepid[]" value="" type="hidden" placeholder="" id="stepid" class=""><textarea name="step[]"  rows="1" type="text" id="step" placeholder="Create Step" class="form-control"></textarea></div><div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin"><label class="file-upload btn btn-primary">Browse for file ... <input name="photo[]" style="opecity:0" placeholder="Upload Service Image" id="photo" type="file"></label></div></div><div class="col-md-1"> <button class="btn btn-danger" type="button" onclick="remove_education_fields('+ z +');"  style="width: 50px;  margin-left: 8px;"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span> </button></div></div></div>';
var rdiv = 'removeclass'+z;
var rdiv1 = 'Schoolname'+z;
objTo.appendChild(divtest);
}
function remove_education_fields(rid) {
$('.removeclass'+rid).remove();
z--;
room1--;
}
</script>
<?php $__currentLoopData = $howtodo1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $notes): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<div id="myModalnote_<?php echo e($notes->id); ?>" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Confirmation</h4>
			</div>
			<div class="modal-body">
				<p>Do you want to delete this record ?</p>
			</div>
			<div class="modal-footer">
     <a href="<?php echo e(route('howtodo1.howtododelete',$notes->id)); ?>" class="btn btn-danger">Delete</a>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>