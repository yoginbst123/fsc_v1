<?php $__env->startSection('main-content'); ?>
<div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 fsc-content-head">
			<h4>ARE YOU OWNER OF ?</h4>
		</div>
	</div>
	
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:4%;">
<?php if( session()->has('success') ): ?>
    <div class="alert alert-success alert-dismissable"><?php echo e(session()->get('success')); ?></div>
<?php endif; ?>
<?php if( session()->has('error') ): ?>
    <div class="alert alert-danger alert-dismissable"><?php echo e(session()->get('error')); ?></div>
<?php endif; ?> 
	<?php $__currentLoopData = $business; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $busi): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>		
		<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-sub-imgs">
	      <a href="<?php if($busi->link == 'comman-registration/create'): ?> <?php echo e(url($busi->link,[$busi->id,$busi->bussiness_name])); ?> <?php else: ?> <?php echo e(url($busi->link,[$busi->id,$busi->bussiness_name])); ?> <?php endif; ?>"><img style="cursor:pointer;" class="img-responsive" src="<?php echo e(url('public/business')); ?>/<?php echo e($busi->bussiness_image_name); ?>"/></a>
		<div class="services-tab"> <a href="<?php if($busi->link == 'comman-registration/create'): ?> <?php echo e(url($busi->link,[$busi->id,$busi->bussiness_name])); ?> <?php else: ?> <?php echo e(url($busi->link,[$busi->id,$busi->bussiness_name])); ?> <?php endif; ?>"><span><div class="st_title"><?php echo e($busi->bussiness_name); ?></div></span></a></div>
		</div>
         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>		
		<!--<?php $__currentLoopData = $business; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $busi): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>	
		<form id="logout-form1" action="<?php if($busi->link == 'comman-registration'): ?> <?php echo e(route('comman-registration.create')); ?> <?php else: ?> <?php echo e(url($busi->link,$busi->id)); ?> <?php endif; ?>" method="get">	
		<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-sub-imgs">
		<button type="submit" name="submit"><img style="cursor:pointer;" class="img-responsive" src="<?php echo e(url('business')); ?>/<?php echo e($busi->bussiness_image_name); ?>"/></button>
		<div class="services-tab"> <button type="submit" name="submit" value="submit"><span><div class="st_title"><?php echo e($busi->bussiness_name); ?></div></span></button></div>
		</div>
		<input type="hidden" value="<?php echo e($busi->bussiness_name); ?>" name="user_type">
	     <input type="hidden" value="<?php echo e($busi->id); ?>" name="business_id">
		 <input type="hidden" value="" name="business_cat_id">
		 <input type="hidden" id="business_brand_id" name="business_brand_id" value="" />
		 <input type="hidden" id="business_brand_category_id" name="business_brand_category_id" value="" />
		 </form>
         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>		
	<!--<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-sub-imgs">
			<a href="<?php echo e(URL::to('non-profit-organization-registration')); ?>"><img style="cursor:pointer;" class="img-responsive" src="<?php echo e(URL::asset('frontcss/images/registration/Non-Profits-Org.png')); ?>"/></a>
		</div>
		<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-sub-imgs">
			<a href="<?php echo e(URL::to('service-industry-registration')); ?>"><img style="cursor:pointer;" class="img-responsive" src=" <?php echo e(URL::asset('frontcss/images/registration/ServiceIndustry.png')); ?>"/></a>
		</div>
	</div>
	
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:4%;">
		<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-sub-imgs">
			<a href="<?php echo e(URL::to('professions-registration')); ?>"><img style="cursor:pointer;" class="img-responsive" src="<?php echo e(URL::asset('frontcss/images/registration/Professions.png')); ?>"/></a>
		</div>
		<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-sub-imgs">
			<a href="<?php echo e(URL::to('investor-registration')); ?>"><img style="cursor:pointer;" class="img-responsive" src="<?php echo e(URL::asset('frontcss/images/registration/Investor.png')); ?>"/></a>
		</div>
		<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-sub-imgs">
			<a href="<?php echo e(URL::to('personal-registration')); ?>"><img style="cursor:pointer;" class="img-responsive" src="<?php echo e(URL::asset('frontcss/images/registration/Personal.png')); ?>"/></a>
		</div>-->
	</div>
	
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front-section.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>