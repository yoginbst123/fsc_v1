<?php $__env->startSection('main-content'); ?>
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>Language</h1>
    </section>
    <!-- Main content -->
    <section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-success">
			      <div class="box-header">
              <div class="box-tools pull-right">
              </div>
            </div>
				<div class="col-md-12">
					<form method="post" action="<?php echo e(route('language.update',$language->id)); ?>" class="form-horizontal" id="languagename" name="languagename">
					<?php echo e(csrf_field()); ?><?php echo e(method_field('PATCH')); ?>

						<div class="form-group <?php echo e($errors->has('language_name') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Language Name :</label>
							<div class="col-lg-5 col-md-8">
								<input name="language_name" type="text" value="<?php echo e($language->language_name); ?>" id="language_name" class="form-control" value="" /><?php if($errors->has('language_name')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('language_name')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>
						<div class="card-footer">
						    <div class="form-group">
							<label class="control-label col-md-3"></label>
							<div class="col-xs-2" style="width:155px;">
<input class="btn_new_save btn-primary1" type="submit" name="submit" value="Save">
							</div>
							<div class="col-xs-2" style="width:155px;">
<a class="btn_new_cancel" href="<?php echo e(url('fac-Bhavesh-0554/language')); ?>">Cancel</a> 
							</div>
						</div>
						  </div>
					</form>
				</div>
			</div>
		</div>
	</div>
	</section>
<!--</div>-->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>