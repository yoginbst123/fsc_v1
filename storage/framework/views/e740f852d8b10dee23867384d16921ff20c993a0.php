<?php
if(isset($employee->type)!='')
{
    if($employee->type=='user')
    {
        ?>
    <style> .employee{ display:none}</style>    
        <?php
    }
} 
//print_r($employee);exit('testt');
    ?>
    

<style>
.sidebar-menu>li>a {
  line-height: 42px;
  
}
.skin-blue .wrapper, .skin-blue .main-sidebar, .skin-blue .left-side {
    /*background-color: #ffffff !important;*/
}
.aligntext{
    margin-left:1%;
}
.sidebar-menu > li > a > .fa, .sidebar-menu > li > a > .glyphicon, .sidebar-menu > li > a > .sidebar-icon {
    width: 48px;
    display: inline-block;
    background: #0271b1;
    text-align: center;
   padding:10px;
    margin-right: 15px;
    background-image: linear-gradient(to right bottom, #2399f4, #008ff6, #0084f7, #0078f7, #006bf5, #006af5, #006af6, #0069f6, #0075fa, #0081fc, #008bfe, #0d96ff);
}

.skin-blue .sidebar a{
    /*background-color:#269ef3 !important;*/
}
.iconclientmanagement i.fa-clientmaagement, .iconclientmanagement i.fa-workmg, .iconclientmanagement i.fa-emg{padding:12px 0px!important;}
    .iconclientmanagement i.fa-clientmaagement:before{content:'C'!important; font-size:21px; font-weight:bold; font-family:TimesNewRoman !important;color:#103b68;}
    .iconclientmanagement i.fa-workmg:before{content:'W'!important; font-size:19px; font-weight:bold; font-family:TimesNewRoman !important;color:#103b68;}
    .iconclientmanagement i.fa-emg:before{content:'E'!important; font-size:19px; font-weight:bold; font-family:TimesNewRoman !important;color:#103b68;}   
</style>

<aside class="main-sidebar">
 <section class="sidebar">
    
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
      <li><a href="<?php echo e(url('fscemployee')); ?>"><!--<i class="fa fa-tachometer sidebar-icon"></i>--><img class="sidebar-icon" src="https://www.financialservicecenter.net/public/images/dashboard (1).png" style="height:46px; width:46px;color:white !important;;"><span style="font-size:16px !important;">Dashboard </span></a></li>
       <li class="treeview">
                 
				<a href="#"><!--<i class="fa fa-user-circle-o sidebar-icon"></i>--><img class="sidebar-icon" src="https://www.financialservicecenter.net/public/images/user.png" style="height:46px; width:46px;color:white !important;;"><span style="font-size:16px !important;">Profile </span> <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span></a>
				<ul class="treeview-menu">
	               <li><a href="<?php echo e(url('fscemployee/employeeprofile')); ?>?tab=0" class="fsemp"><i class="fa fa-circle-o sidebar-icon"></i><span>Profile</span></a></li>
	               <li><a href="<?php echo e(url('fscemployee/empchangepassword')); ?>"><i class="fa fa-circle-o"></i><span> Login Info</span></a></li>
				</ul>
			</li>
			<!----------- Start !--------------------->
				<li class="treeview iconclientmanagement">
				<a href="#"><i style="background: linear-gradient(to bottom, #b3dced 0%,#29b8e5 50%,#bce0ee 100%) !important;" class="fa fa-clientmaagement sidebar-icon"></i> <span><span style="font-weight:bold;color:#103b68;font-size: 18px;">C</span>lient Management</span> <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span></a>	
				<ul class="treeview-menu">
				
					<!--<li class=""><a href="<?php echo e(url('fscemployee/clients')); ?>"><i class="fa fa-chevron-right"></i> <span>Client Edit / View  </span></a></li>!-->
					<li class=""><a href="<?php echo e(url('fscemployee/addressbook-fscemployee')); ?>"><i class="fa fa-chevron-right"></i> <span>View Address Book</span></a></li>
					<!--<li class=""><a href="<?php echo e(url('fscemployee/employees')); ?>"><i class="fa fa-chevron-right"></i> <span>View / Edit -- Employee  </span></a></li>-->
				</ul>
			</li>
			<li class="treeview iconclientmanagement">
				<a href="#"><i style="background: linear-gradient(to bottom, #b3dced 0%,#29b8e5 50%,#bce0ee 100%) !important;" class="fa fa-workmg sidebar-icon"></i> <span><span style="font-weight:bold;color:#103b68;font-size: 18px;">W</span>ork Management</span> 
				<span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span></a>	
			
                <ul class="treeview-menu">
                    <li><a href="<?php echo e(url('fscemployee/mywork')); ?>" class=""><i class="fa fa-circle-o sidebar-icon"></i><span>My Work</span></a></li>
                    <li><a href="<?php echo e(url('fscemployee/worknew')); ?>"><i class="fa fa-chevron-right"></i> <span>Work -- New</span></a></li>
                    <li><a href="<?php echo e(url('fscemployee/workregular')); ?>"><i class="fa fa-chevron-right"></i> <span>Work -- Regular</span></a></li>
                    <li><a href="<?php echo e(url('fscemployee/fschowtodo')); ?>"><i class="fa fa-chevron-right"></i> Work -- To Do</a></li>
                    <li><a href="<?php echo e(url('fscemployee/workrecord')); ?>"><i class="fa fa-chevron-right"></i> <span>Work -- Record</span></a></li>
                    <li><a href="<?php echo e(url('fscemployee/workstatus')); ?>"><i class="fa fa-chevron-right"></i> <span>Work -- Status</span></a></li>
                    <li><a href="<?php echo e(url('fscemployee/submissionrequest')); ?>"><i class="fa fa-chevron-right"></i> <span>Work -- Submission</span> </a></li>
                    <li class=""><a href="#"><i class="fa fa-chevron-right"></i> <span>Work -- Upload</span></a></li>
                    <li><a href="<?php echo e(url('fscemployee/tasks')); ?>"><i class="fa fa-chevron-right"></i></i> <span>Task</span></a></li>
                    <li><a href="<?php echo e(url('fscemployee/eemail')); ?>"><i class="fa fa-chevron-right"></i> <span>Email Access</span></a></li>
                    <li class="treeview">
                        <a href="#"><i class="fa fa-circle-o" aria-hidden="true"></i> <span>Message</span> <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                        </span></a>
                        <ul class="treeview-menu">
                            <li><a href="<?php echo e(url('fscemployee/getmsg')); ?>"><i class="fa fa-chevron-right"></i></i> <span>Message Out</span></a></li>
                            <li><a href="<?php echo e(url('fscemployee/sendmessage')); ?>"><i class="fa fa-chevron-right"></i> <span>Message  In</span></a></li>
                            <!--<li><a href="<?php echo e(url('/fac-Bhavesh-0554/msg/create')); ?>"><i class="fa fa-circle-o"></i> New Message</a></li>
                            <li><a href="<?php echo e(url('/fac-Bhavesh-0554/msg/')); ?>"><i class="fa fa-circle-o"></i>  Edit / View Message</a></li>-->
                        </ul>
                    </li>
                    <li><a href="<?php echo e(url('fscemployee/appointment')); ?>"><i class="fa fa-chevron-right"></i> <span>Appointment</span></a></li>
                    <!--<li class="treeview">
                        <a href="#"><i class="fa fa-circle-o"></i> <span>Calendar</span> <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                        </span></a>
                        <ul class="treeview-menu">
                            <li><a href="#"><i class="fa fa-chevron-right"></i> Appointment</a></li>	
                            <li><a href="#"><i class="fa  fa-chevron-right"></i> Calendar</a></li>	
                        </ul>
                    </li>-->
                </ul>
			</li>
			
			<!--EEEEEEEEEEE-->
			<li class="treeview iconclientmanagement">
				<a href="#"><i style="background: linear-gradient(to bottom, #b3dced 0%,#29b8e5 50%,#bce0ee 100%) !important;" class="fa fa-emg sidebar-icon"></i> <span><span style="font-weight:bold;color:#103b68;font-size: 18px;">E</span>mployee  Management</span> <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span></a>	
				<ul class="treeview-menu">
				    <li>
						<a href="<?php echo e(url('fscemployee/schedules')); ?>"><i class="fa fa-chevron-right"></i> <span>Edit / View Scheduled</span></a>
					</li>
				    <li>
						<a href="<?php echo e(url('fscemployee/leave')); ?>"><i class="fa fa-chevron-right"></i><span>Edit / View Leave Application</span></a>
					</li> 
				    <li>
				        <a href="<?php echo e(url('fscemployee/employees')); ?>"><i class="fa fa-chevron-right"></i><span>Client Employee</span></a>
				    </li>
				  
					
				</ul>
			</li>
		
	    <li class="treeview">
						<a href="#"><i class="fa fa-circle-o" aria-hidden="true"></i> <span>Task Manager</span> <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span></a>
			<ul class="treeview-menu">
                <li><a href="<?php echo e(url('fscemployee/tasks')); ?>"><i class="fa fa-chevron-right"></i></i> <span>Task</span></a></li>
                <li><a href="<?php echo e(url('fscemployee/eemail')); ?>"><i class="fa fa-chevron-right"></i> <span>Email Access</span></a></li>
                <li class="treeview">
                    <a href="#"><i class="fa fa-circle-o" aria-hidden="true"></i> <span>Message</span> <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                    </span></a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo e(url('fscemployee/getmsg')); ?>"><i class="fa fa-chevron-right"></i></i> <span>Message Out</span></a></li>
                        <li><a href="<?php echo e(url('fscemployee/sendmessage')); ?>"><i class="fa fa-chevron-right"></i> <span>Message  In</span></a></li>
                        <!--<li><a href="<?php echo e(url('/fac-Bhavesh-0554/msg/create')); ?>"><i class="fa fa-circle-o"></i> New Message</a></li>
                        <li><a href="<?php echo e(url('/fac-Bhavesh-0554/msg/')); ?>"><i class="fa fa-circle-o"></i>  Edit / View Message</a></li>-->
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#"><i class="fa fa-circle-o"></i> <span>Calender</span> <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                    </span></a>
                    <ul class="treeview-menu">
                        <li><a href="#"><i class="fa fa-chevron-right"></i> Appointment</a></li>	
                        <li><a href="#"><i class="fa  fa-chevron-right"></i> Calender</a></li>	
                    </ul>
                </li>
			</ul>
		</li>
                 
			<li class="treeview">
				<a href="#"><!--<i class="fa fa-users sidebar-icon"></i>--><img class="sidebar-icon" src="https://www.financialservicecenter.net/public/images/report (1).png" style="height:46px; width:46px;color:white !important;;"> <span style="font-size:16px !important;">Report</span> <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span></a>
            		<ul class="treeview-menu">
				
					<li><a href="<?php echo e(url('/fscemployee/empclientreport')); ?>"><i class="fa fa-chevron-right"></i> Client Report</a></li>
			
								<li class=""><a href="<?php echo e(url('/fscemployee/empworkreport')); ?>"><i class="fa fa-circle-o"></i> Work Report</a></li>
								
									<li class="treeview"><a href="#"><i class="fa fa-circle-o"></i>  Employee Report <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span></a>
					<ul class="treeview-menu">
					    	<li class="treeview"><a href="#"><i class="fa fa-circle-o"></i> <span> FSC EE Report </span> <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span></a>
					<ul class="treeview-menu">
							<li><a href="<?php echo e(url('/fscemployee/fscemployeereport')); ?>"><i class="fa fa-chevron-right"></i> FSC EE Timesheet</a></li>
							<li><a href="<?php echo e(url('/fscemployee/ee-schedule')); ?>"><i class="fa fa-chevron-right"></i> FSC EE  Schedule</a></li>
							<li><a href="<?php echo e(url('/fscemployee/fscemployeeworkreport')); ?>"><i class="fa fa-chevron-right"></i> FSC EE / USER  Work</a></li>
							
						</ul>
					</li>
						<li class="treeview"><a href="#"><i class="fa fa-circle-o"></i> Client EE Report <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span></a>
					<ul class="treeview-menu">
							<li><a href="<?php echo e(url('/fscemployee/clientreport')); ?>"><i class="fa fa-chevron-right"></i> Client EE Timesheet</a></li>
							<li><a href="<?php echo e(url('/fscemployee/clientschedules')); ?>"><i class="fa fa-chevron-right"></i> Client EE  Schedule</a></li>
						</ul>
				</li>
				
							
						</ul>
				</li>
								
				</ul>
		
				<ul class="treeview-menu">
					<li><a href="<?php echo e(url('fscemployee/report/')); ?>"><i class="fa fa-circle-o"></i> <span>Timesheet</span></a></li>
				    <li><a href="<?php echo e(url('fscemployee/empclientreport')); ?>"><i class="fa fa-circle-o"></i> <span> Client Report </span></a></li>
				    <li><a href="<?php echo e(url('fscemployee/empworkreport')); ?>"><i class="fa fa-circle-o"></i> <span> Work Report </span></a></li>
					<?php if($employee->timesheet=='Time Sheet')
				{
				    ?>
				
			
					<li><a href="<?php echo e(url('fscemployee/allemployeesheet')); ?>"><i class="fa fa-circle-o"></i> <span> FSC EE Timesheet Report </span></a></li>
					<li><a href="<?php echo e(url('fscemployee/schedules')); ?>"><i class="fa fa-circle-o"></i> <span> FSC EE Schedule </span></a></li>
				<?php } ?>
				</ul>
			</li>
			<li class="treeview">
				<a href="#"><!--<i class="fa fa-question-circle sidebar-icon"></i>--><img class="sidebar-icon" src="https://www.financialservicecenter.net/public/images/support.png" style="height:46px; width:46px;color:white !important;;"> <span style="font-size:16px !important;">Support</span> <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span></a>
				<ul class="treeview-menu">
				<li class="treeview">
						<a href="#"><i class="fa fa-circle-o"></i> Employee Support  <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span></a>
						<ul class="treeview-menu">
						    <li><a href="<?php echo e(url('fscemployee/technicalsupport/')); ?>"><i class="fa fa-chevron-right"></i><span>Technical Support  View</span></a></li>
						    <li><a href="<?php echo e(url('fscemployee/technicalsupport/create')); ?>"><i class="fa fa-chevron-right"></i><span>Technical Support</span></a></li>
						</ul>
					</li>
				<li class="treeview">
						<a href="#"><i class="fa fa-circle-o"></i> Client Support <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span></a>
						<ul class="treeview-menu">
					        <li><a href="<?php echo e(url('fscemployee/clientsupports')); ?>"><i class="fa fa-chevron-right"></i><span>Technical Support</span></a></li>
    					</ul>
					</li>	
				</ul>
			</li>
			<li class="treeview">
				<a href="#"><!--<i class="fa fa-question-circle sidebar-icon"></i>--><img class="sidebar-icon" src="https://www.financialservicecenter.net/public/images/question.png" style="height:46px; width:46px;color:white !important;"> <span style="font-size:16px !important;">Info</span> <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i></span></a>
				 <ul class="treeview-menu">
				      <li><a href="<?php echo e(url('fscemployee/howtodo')); ?>"><i class="fa fa-chevron-right"></i><span> How To Do</span></a></li>
				    <li><a href="<?php echo e(url('fscemployee/empaccountcode')); ?>"><i class="fa fa-chevron-right"></i><span> Account Code</span></a></li>  
				   <!-- <li><a href="<?php echo e(url('fscemployee/eemail')); ?>"><i class="fa fa-chevron-right"></i>Email / Telephone Ext</a></li>
			        !--><li><a href="<?php echo e(url('fscemployee/emptaxation')); ?>"><i class="fa fa-chevron-right"></i><span> License</span></a></li>
			        
			       <li class="treeview">
						<a href="#"><i class="fa fa-circle-o"></i> Taxation<span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span></a>
						<ul class="treeview-menu">
						  <li><a href="#"><i class="fa fa-chevron-right"></i><span> City / Local</span></a></li>
					      <li><a href="<?php echo e(url('fscemployee/empsstates')); ?>"><i class="fa fa-chevron-right"></i><span> County</span></a></li>
					      <li><a href="<?php echo e(url('fscemployee/empsstates')); ?>"><i class="fa fa-chevron-right"></i><span> Federal</span></a></li>
					      <li><a href="<?php echo e(url('fscemployee/empstates')); ?>"><i class="fa fa-chevron-right"></i><span> State</span></a></li>
    					</ul>
					</li>
					<?php if($employee->type=='clientemployee'): ?> <?php else: ?>
			        <li><a href="<?php echo e(url('fscemployee/empvendor')); ?>"><i class="fa fa-circle-o"></i> Vendor</a></li>	
		          	<?php endif; ?>
				</ul>
			</li>
			<?php if($employee->type=='clientemployee'): ?>
				<li><a href="<?php echo e(route('logout')); ?>" onclick="event.preventDefault();  document.getElementById('logout-form').submit();"><!--<i class="fa fa-sign-out sidebar-icon"></i>--><img class="sidebar-icon" src="https://www.financialservicecenter.net/public/images/arrow (1).png" style="height:46px; width:46px;color:white !important;"><span class="aligntext" style="font-size:16px !important;">Log Out</span></a>
						<form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;"><?php echo e(csrf_field()); ?> </form></li>
						<?php else: ?>
    		<li><a href="<?php echo e(route('fscemployee.logout')); ?>" onclick="event.preventDefault();  document.getElementById('logout-form').submit();"><!--<i class="fa fa-sign-out sidebar-icon"></i>--><img class="sidebar-icon" src="https://www.financialservicecenter.net/public/images/arrow (1).png" style="height:46px; width:46px;color:white !important;"><span class="aligntext">Log Out</span></a>
						<form id="logout-form" action="<?php echo e(route('fscemployee.logout')); ?>" method="POST" style="display: none;"><?php echo e(csrf_field()); ?> </form></li>
			    	<?php endif; ?>
      </ul>
    </section>
		</aside>
	