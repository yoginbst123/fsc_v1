<?php $__env->startSection('main-content'); ?>
<div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 fsc-content-head">
			<h4>Service Industry Registration</h4>
		</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-reg-business-category-div">
	<?php $__currentLoopData = $category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>		
	<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-reg-business-category-img">
		<a href="<?php if($cat->link == 'comman-registration/create'): ?> <?php echo e(url($cat->link,[Request::segment(2),Request::segment(3),$cat->id])); ?> <?php else: ?> <?php echo e(url($cat->link,[$cat->id,Request::segment(2),Request::segment(3)])); ?> <?php endif; ?>"><img src="<?php echo e(URL::asset('public/category/')); ?>/<?php echo e($cat->business_cat_image); ?>"/></a>
		<center><div class="services-tab"> <a href="<?php if($cat->link == 'comman-registration/create'): ?> <?php echo e(url($cat->link,[Request::segment(2),Request::segment(3),$cat->id])); ?> <?php else: ?> <?php echo e(url($cat->link,[$cat->id,Request::segment(2),Request::segment(3)])); ?> <?php endif; ?>"><span><div class="st_title"><?php echo e($cat->business_cat_name); ?></div></span></a></div></center>
	</div>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

	</div>
	

	
	
	
	
	
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('front-section.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>