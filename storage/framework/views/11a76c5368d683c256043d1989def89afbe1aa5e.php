	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="<?php echo e(csrf_token()); ?>" />
	<title>Financial Service Center</title>
	<link rel="icon" href="favicon.ico" type="image/x-icon" />
	<link href="https://fonts.googleapis.com/css?family=Proza+Libre" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="<?php echo e(URL::asset('public/frontcss/css/animate.css')); ?>" />
	<link rel="stylesheet" href="<?php echo e(URL::asset('public/frontcss/css/bootstrap.min.css')); ?>"/>
	<link rel="stylesheet" type="text/css" href="<?php echo e(URL::asset('public/frontcss/css/bootstrap.css')); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo e(URL::asset('public/frontcss/css/style.css')); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo e(URL::asset('public/frontcss/css/font-awesome.min.css')); ?>" />
	<style>
	.well{ background-color:#e4e2e2; }
	.nav>li>a{ padding:0; }
	.has-error .checkbox, .has-error .checkbox-inline, .has-error .control-label, .has-error .help-block, .has-error .radio, .has-error .radio-inline, .has-error.checkbox label, .has-error.checkbox-inline label, .has-error.radio label, .has-error.radio-inline label{ font-size:14px}
.help-block {
    color: #fb1919;
    font-size: 14px;
}
.form-control-feedback{right: 10px;}
#success_message{ display: none;}
	</style>
<script type="text/javascript" src="<?php echo e(URL::asset('public/frontcss/js/jquery.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(URL::asset('public/frontcss/js/bootstrap.min.js')); ?>"></script>
<script type='text/javascript' src="<?php echo e(URL::asset('public/dashboard/js/jquery.maskedinput.js')); ?>"></script>
<script src="<?php echo e(URL::asset('public/dashboard/js/jquery.maskedinput.min.js')); ?>"></script>
<script src="<?php echo e(URL::asset('public/frontcss/js/bootstrap-formhelpers.min.js')); ?>"></script>
<script type="text/javascript">
		function startTime() {
			var today = new Date();
			var dd = today.getDate();
			var h = today.getHours();
			var m = today.getMinutes();
			var s = today.getSeconds();
			m = checkTime(m);
			s = checkTime(s);
			var mm = today.getMonth()+1;
			var yyyy = today.getFullYear();
			if(dd<10) {
				dd = '0'+dd
			}
			if(mm<10) {
				mm = '0'+mm
			}
			document.getElementById('txt').innerHTML = monthNames[mm]+" "+dd+" "+yyyy+" "+h + ":" + m + ":" + s;
			var t = setTimeout(startTime, 500);
		}
		var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
		function checkTime(i) {
			if (i < 10) {i = "0" + i};
			return i;
		}
	</script>
	
	
<script type="text/javascript">
$(document).ready(function(){
	var url = window.location;

// for sidebar menu but not for treeview submenu
$('ul.fsc-menu li a').filter(function() {
    return this.href == url;
}).parent().siblings().removeClass('active').end().addClass('active');

// for treeview which is like a submenu
$('ul.submenu li a').filter(function() {
    return this.href == url;
}).parentsUntil(".submenu").siblings().removeClass('active').end().addClass('active');
$('')
});
</script>




