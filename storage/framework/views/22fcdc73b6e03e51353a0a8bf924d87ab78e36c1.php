<?php $__env->startSection('main-content'); ?>
<style>
    @media  only screen and (max-width: 991px){
        .table-title a {
            margin-top: 0px !important;
            margin-right: 0px !important;
        }
    }
</style>
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>Forms</h1>
    </section>
    <!-- Main content -->
    <section class="content">
	
	<div class="row">
		<div class="col-md-12">
				<div class="box box-success">
			      <div class="box-header">
           
              <div class="box-tools pull-right">
                <div class="table-title">
					
						<a href="<?php echo e(route('formcategory.create')); ?>">Add New Category</a>
					</div>
              </div>
            </div>
				<div class="col-md-12">
                   

					<form method="post" class="form-horizontal" id="" action="<?php echo e(route('forms.store')); ?>" enctype="multipart/form-data">
					<?php echo e(csrf_field()); ?>

						<div class="form-group <?php echo e($errors->has('form_department') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Department Name :</label>
							<div class="col-lg-4 col-md-6">
								<select class="form-control category1" id="form_department" name="form_department">
                                    <option value=''>Please select Department Name</option>
                                    <option value="Federal">Federal </option>
                                    <option value="State">State </option>
                                    <option value="County">County </option>
                                    <option value="Local (City)">Local (City) </option>
                                    <option value="Other">Other </option>
								</select>
                                    <?php if($errors->has('form_department')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('form_department')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>
<div class="form-group <?php echo e($errors->has('category') ? ' has-error' : ''); ?>">
<label class="control-label col-md-3">Category :</label>
<div class="col-lg-4 col-md-6">
<select name="category" type="text" id="category" class="form-control">
<option value="">Select</option> 
</select> 
<span id="loader" style="display:none"><i class="fa fa-spinner fa-3x fa-spin"></i></span>
                                                        <?php if($errors->has('category')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('category')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>
                            <div class="form-group <?php echo e($errors->has('form_name') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Form Name :</label>
							<div class="col-lg-4 col-md-6">
								<input name="form_name" type="text" id="form_name" class="form-control" value="" />      
								 	<?php if($errors->has('form_name')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('form_name')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>

						<div class="form-group <?php echo e($errors->has('form_no') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Form No :</label>
							<div class="col-lg-4 col-md-6">
								<input name="form_no" type="text" id="form_no" class="form-control" value="" />      
								 	<?php if($errors->has('form_no')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('form_no')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>
						
						<div id="weblink" class="form-group <?php echo e($errors->has('link') ? ' has-error' : ''); ?>" style="display:none">
							<label class="control-label col-md-3">Website Link :</label>
							<div class="col-lg-4 col-md-6">
								<input name="link" type="text" id="link" class="form-control" value="" />      
								 	<?php if($errors->has('link')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('link')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>
						<div class="form-group <?php echo e($errors->has('form_upload') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Upload Form :</label>
							<div class="col-lg-4 col-md-6">
								


<label class="file-upload btn btn-primary">
Browse for file ... <input name="form_upload" style="opecity:0" placeholder="Upload Service Image" id="form_upload" type="file">
</label>
	<?php if($errors->has('form_upload')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('form_upload')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>
						<div class="card-footer">
						<div class="row">
						    <div class="col-md-3"></div>
							<div class="col-xs-2" style="width:155px;">
								<input class="btn_new_save btn-primary1 primary1" type="submit" id="primary1" name="submit" value="Save">
							</div>
							<div class="col-xs-2" style="width:155px;">
								<a class="btn_new_cancel" href="<?php echo e(url('fac-Bhavesh-0554/forms')); ?>">Cancel</a> 
							</div>
						</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	</section>
</div>


<script>
$(document).ready(function(){
	$(document).on('change','.category1', function()
	{ 
	var id = $(this).val();
	if(id=='Other')
	{
	    $('#weblink').hide();
	}
	else
	{
	     $('#weblink').show();
	}
	
		$.get('<?php echo URL::to('/getform'); ?>?id='+id, function(data)
		{ //alert(data);
            $('#category').empty();
           $.each(data, function(index, subcatobj)
		   {
			   $('#category').append('<option value="'+subcatobj.category+'">'+subcatobj.category+'</option>');
		   })

		});
			
	});
});
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>