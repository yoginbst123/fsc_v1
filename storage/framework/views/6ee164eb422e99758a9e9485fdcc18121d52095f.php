<?php $__env->startSection('main-content'); ?>
<div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 fsc-content-head">
			<h4>Brand List</h4>
		</div>
	</div>	
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-reg-business-category-div" style="margin-top: 8%;">
	<?php $__currentLoopData = $category1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $busi): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-sub-imgs">
      <a href="<?php if($busi->link == 'comman-registration/create'): ?> <?php echo e(url($busi->link,[Request::segment(3),Request::segment(5),Request::segment(4),Request::segment(2),$busi->id])); ?> <?php else: ?> <?php echo e(url($busi->link,[$busi->id,Request::segment(3),Request::segment(2),Request::segment(4),Request::segment(5)])); ?> <?php endif; ?>"><img style="cursor:pointer;" class="img-responsive" src="<?php echo e(url('public/businessbrandcategory')); ?>/<?php echo e($busi->business_brand_category_image); ?>"/></a>
    <div class="services-tab"> <a href="<?php if($busi->link == 'comman-registration/create'): ?> <?php echo e(url($busi->link,[Request::segment(3),Request::segment(5),Request::segment(4),Request::segment(2),$busi->id])); ?> <?php else: ?> <?php echo e(url($busi->link,[$busi->id,Request::segment(3),Request::segment(2),Request::segment(4),Request::segment(5)])); ?> <?php endif; ?>"><span><div class="st_title"><?php echo e($busi->business_brand_category_name); ?></div></span></a></div>
    </div>
     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>	
<!--<?php $__currentLoopData = $category1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $busi): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	<form id="logout-form1" action="<?php if($busi->link == 'comman-registration'): ?> <?php echo e(route('comman-registration.create')); ?> <?php else: ?> <?php echo e(url($busi->link,$busi->id)); ?> <?php endif; ?>" method="get">
    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-sub-imgs">
	<button type="submit" name="submit" value="submit"><img style="cursor:pointer;" class="img-responsive" src="<?php echo e(url('businessbrandcategory')); ?>/<?php echo e($busi->business_brand_category_image); ?>"/></button>
	<center><div class="services-tab"><button type="submit" name="submit" value="submit"><span><div class="st_title"><?php echo e($busi->business_brand_category_name); ?></div></span></button></div></center>
    </div>
	<?php if(isset($_GET['user_type'])): ?>
	<input type="hidden" value="<?php echo e($_GET['user_type']); ?>" name="user_type">
	<?php endif; ?>
	<input type="hidden" value="<?php echo e($busi->business_id); ?>" name="business_id">
	<input type="hidden" value="<?php echo e($busi->business_cat_id); ?>" name="business_cat_id">
	<input type="hidden" id="business_brand_id" name="business_brand_id" value="<?php echo e($busi->business_brand_id); ?>" />
	<input type="hidden" id="business_brand_category_id" name="business_brand_category_id" value="<?php echo e($busi->id); ?>"" />
	</form>
     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>	-->
	</div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('front-section.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>