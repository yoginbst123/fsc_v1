<?php $__env->startSection('main-content'); ?>
<style>
.table > thead > tr > th {background: #ffff99; border-bottom:8px solid #993366; text-align:center;}
label{float:left;}
.dt-buttons{
    margin-bottom:10px;
}
.page-title{
        padding: 8px 15px;

}

    .buttons-pdf {
    font-size: 0px !important;
    border-radius: 3px;
    padding: 8px 10px !important;
    background: #fff !important;
    border-color: #c6c6c6  !important;
    color:red !important;
}
.buttons-print {
    font-size: 0px !important;
    border-radius: 3px;
    padding: 8px 10px !important;
    background: #3c8dbc !important;
            border-color: #367fa9 !important;
}
.buttons-excel{
    font-size:0px !important;
    border-radius: 3px;
        padding: 8px 10px !important;
        
         background: #00a65a !important;
    border-color: #008d4c !important;
        

}
.buttons-excel:hover{
     background: #008d4c !important;

}
.buttons-pdf:hover{
     background: #f6f6f6  !important;
}

.buttons-print:hover{
     background: #367fa9 !important;
}
.box-header{
    padding-top:0px;
    padding-bottom:0px;
}
.fa{
    font-size: 16px !important;
}
div.dataTables_wrapper div.dataTables_filter input {
width:75% !important;
    
}
/*.taskoxmian{*/
/*    width: 300px;*/
/*    position: absolute;*/
/*    z-index: 999;*/
/*    display: flex;*/
/*    flex-direction: row;*/
/*    left: 23%;*/
/*    top: 10px;}*/
.taskoxmian form{width: 100%;
    display: flex;}
.taskoxmian .firstbox{width: 250px;}
.taskoxmian {
    position: absolute;
    z-index: 999;
    left: 260px;
    top: 10px;
}
@media  only screen and (max-width: 991px){
.box-header>.box-tools {
    position: relative !important;
    margin-right: 5px !important;
    margin-top: 0px !important;
}
.table-title a {
    position: relative !important;
    margin-top: 0px !important;
    margin-right: -10px !important;
}
.taskoxmian {
    top: 42px;
}
.taskoxmian .btn-success{
    margin-top: 8px;
}
.cs3{
    width:190px;
}
}
@media  only screen and (max-width: 860px){
    .table-title a {
    margin-top: 6px !important;
    margin-right: 120px !important;
}
.dt-buttons {
    margin-bottom: 10px;
    margin-top: -30px;
    position: absolute;
    right: 10px;
}
.taskoxmian {
    top: 48px;
}
}
@media  only screen and (max-width: 500px){
    div.dataTables_wrapper div.dataTables_filter label{
        width:100%;
    }
    .cs3 {
        width: 100%;
        position: relative;
        left: 0;
    }
    div.dataTables_wrapper div.dataTables_filter label {
        margin-top: 40px;
    }
    .table-title a {
        margin-top: -34px !important;
    }
    div.dataTables_wrapper div.dataTables_filter input {
        width: 80% !important;
    }   
}
</style>

<div class="content-wrapper">
      <!-- Content Header (Page header) -->
    
    <section class="content-header page-title" style="height:50px;">
     		<div class="" style="">
     		    <div class="" style="text-align:center;">
     		        <h2>List of Task <span class="right_title" style="text-align:right;padding-right: 20px ;position: absolute;right: 0;">Add / View / Edit</span></h2>
     		    </div>
     		    
     		</div>
    </section>
      <!-- Main content -->
    <section class="content">		
	<div class="row">
	
		<div class="col-md-12">
		        <?php if( session()->has('success') ): ?>
                        <div class="alert alert-success alert-dismissable"><?php echo e(session()->get('success')); ?></div>
                <?php endif; ?>
			<div class="box box-success">
    			 <div class="box-header">
    			     <div class="taskoxmian cs3">
                <form  class="form-horizontal" action="<?php echo e(url('fac-Bhavesh-0554/task/search')); ?>"  method="post">
                
                  <?php echo e(csrf_field()); ?>

                  
              
    <div class="
firstbox"><select name="choice" style="width: 92%;margin-left: 4px;" id="choice" class="form-control">
         <option value="">All</option>
        <option value="1" <?php if(isset($_POST['choice']) && $_POST['choice'] =='1') { echo 'Selected';}?>>In Progress</option>
        <option value="2" <?php if(isset($_POST['choice']) && $_POST['choice'] =='2') { echo 'Selected';}?>>Wait</option>
        <option value="3" <?php if(isset($_POST['choice']) && $_POST['choice'] =='3') { echo 'Selected';}?>>Done</option>
        
    </select></div>
           
             <div>
                 <input type="submit" class="btn btn-success" name="sub" value="Ok">
    </div>
    
              </form>
              </div>
                      <div class="box-tools pull-right" style="position: relative;margin-right: 0px;z-index:9999;">
                        	<div class="table-title">
        						<a href="<?php echo e(url('fac-Bhavesh-0554/task/create')); ?>" style="position: absolute;width:auto;margin-left: -240px;margin-top: 8px;">New Task</a>
        					</div>
                      </div>
                </div>
                
				<div class="card-body col-md-12">
				
					
					<div class="table-responsive">
						<table class="table table-hover table-bordered" id="example">
						
							<thead>
								<tr>
									<th>No</th>
									<th style="width:10%">Creation Date</th>
								    <th>Priority</th>
								    <th>Employee / User</th>
								    <th>For Whom</th>
									
									<th>Subject</th>
									
									
									<th>Send By</th>
									<th>Status</th>
									<th style="width:8%">Action</th>
								</tr>
							</thead>							
							<tbody>
                            <?php $__currentLoopData = $task; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $com): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
								<tr>
									<td><?php echo e($loop->index+1); ?></td>
									<td><?php if($com->created_at!=''): ?> <?php echo date('m-d-Y',strtotime($com->created_at)); ?><?php endif; ?></td>
										<td <?php if($com->priority == 'Regular'): ?> style='background:#5cacf5 !important;border:1px solid !important;' <?php endif; ?> <?php if($com->priority == 'Very Urgent'): ?> style='background:#f55c5c !important;border:1px solid !important;' <?php endif; ?>
								<?php if($com->priority == 'Urgent'): ?>  style='background:#ffa210 !important;border:1px solid !important;' <?php endif; ?>><?php echo $com->priority; ?></td>
								
					                <td> 
									<?php $__currentLoopData = $emp; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $com1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php if($com->employeeid==$com1->id): ?> <?php echo e(ucwords($com1->firstName.' '.$com1->middleName.' '.$com1->lastName)); ?> <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></td>				
									<td><?php 
										    if($com->whone =='Client') 
										    {
										    foreach($clients as $com1) 
										    { 
										    if($com->client==$com1->id) 
										    {
										    if($com1->business_id =='6') 
										    {
										    echo $com->whone.' ('.ucwords($com1->first_name.' '.$com1->middle_name.' '.$com1->last_name).')';
										    }
										    else 
										    {
										    echo $com->whone.' ('.ucwords($com1->company_name).')';
										    }
										    }
										    }
										    }
										    else if($com->whone =='Other')
										    {
										    echo $com->othername;
										    }?></td>
									<td><?php echo e($com->title); ?></td>
									
									<td><?php if($com->admin_id==Auth::user()->id): ?> <?php echo e(Auth::user()->fname.' '.Auth::user()->lname); ?> <?php endif; ?> 
									<?php $__currentLoopData = $emp; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $com1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php if($com->admin_id==$com1->id): ?> <?php echo e(ucwords($com1->name)); ?> <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></td>
									<td <?php if($com->status==3): ?> style='background:#b9f0b2 !important;border:1px solid !important;' <?php endif; ?>> <?php if($com->status==1): ?> In Progress <?php endif; ?>  <?php if($com->status==2): ?> Wait <?php endif; ?> <?php if($com->status==3): ?> Done <?php endif; ?></td>
									<td>

										<a class="btn-action btn-view-edit" href="<?php echo e(route('task.edit',$com->id)); ?>"><i class="fa fa-edit"></i></a>
										<a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record ?'))
                                                                                  {event.preventDefault();document.getElementById('delete-id-<?php echo e($com->id); ?>').submit();} else{event.preventDefault();}" href="<?php echo e(route('clientsetup.destroy',$com->id)); ?>"><i class="fa fa-trash"></i></a>
<form action="<?php echo e(route('task.destroy',$com->id)); ?>" method="post" style="display:none" id="delete-id-<?php echo e($com->id); ?>">
                                        <?php echo e(csrf_field()); ?> <?php echo e(method_field('DELETE')); ?>

                                        </form>
									</td>
								</tr>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</tbody>
							
						</table>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	    </section>
<!--</div>-->
   	<script>
   	    (function ($) {
    function calcDisableClasses(oSettings) {
        var start = oSettings._iDisplayStart;
        var length = oSettings._iDisplayLength;
        var visibleRecords = oSettings.fnRecordsDisplay();
        var all = length === -1;
 
        // Gordey Doronin: Re-used this code from main jQuery.dataTables source code. To be consistent.
        var page = all ? 0 : Math.ceil(start / length);
        var pages = all ? 1 : Math.ceil(visibleRecords / length);
 
        var disableFirstPrevClass = (page > 0 ? '' : oSettings.oClasses.sPageButtonDisabled);
        var disableNextLastClass = (page < pages - 1 ? '' : oSettings.oClasses.sPageButtonDisabled);
 
        return {
            'first': disableFirstPrevClass,
            'previous': disableFirstPrevClass,
            'next': disableNextLastClass,
            'last': disableNextLastClass
        };
    }
 
    function calcCurrentPage(oSettings) {
        return Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength) + 1;
    }
 
    function calcPages(oSettings) {
        return Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength);
    }
 
    var firstClassName = 'first';
    var previousClassName = 'previous';
    var nextClassName = 'next';
    var lastClassName = 'last';
 
    var paginateClassName = 'paginate';
    var paginatePageClassName = 'paginate_page';
    var paginateInputClassName = 'paginate_input';
    var paginateTotalClassName = 'paginate_total';
 
    $.fn.dataTableExt.oPagination.input = {
        'fnInit': function (oSettings, nPaging, fnCallbackDraw) {
            var nFirst = document.createElement('span');
            var nPrevious = document.createElement('span');
            var nNext = document.createElement('span');
            var nLast = document.createElement('span');
            var nInput = document.createElement('input');
            var nTotal = document.createElement('span');
            var nInfo = document.createElement('span');
 
            var language = oSettings.oLanguage.oPaginate;
            var classes = oSettings.oClasses;
            var info = language.info || 'Page _INPUT_ of _TOTAL_';
 
            nFirst.innerHTML = language.sFirst;
            nPrevious.innerHTML = language.sPrevious;
            nNext.innerHTML = language.sNext;
            nLast.innerHTML = language.sLast;
 
            nFirst.className = firstClassName + ' ' + classes.sPageButton;
            nPrevious.className = previousClassName + ' ' + classes.sPageButton;
            nNext.className = nextClassName + ' ' + classes.sPageButton;
            nLast.className = lastClassName + ' ' + classes.sPageButton;
 
            nInput.className = paginateInputClassName;
            nTotal.className = paginateTotalClassName;
 
            if (oSettings.sTableId !== '') {
                nPaging.setAttribute('id', oSettings.sTableId + '_' + paginateClassName);
                nFirst.setAttribute('id', oSettings.sTableId + '_' + firstClassName);
                nPrevious.setAttribute('id', oSettings.sTableId + '_' + previousClassName);
                nNext.setAttribute('id', oSettings.sTableId + '_' + nextClassName);
                nLast.setAttribute('id', oSettings.sTableId + '_' + lastClassName);
            }
 
            nInput.type = 'text';
 
            info = info.replace(/_INPUT_/g, '</span>' + nInput.outerHTML + '<span>');
            info = info.replace(/_TOTAL_/g, '</span>' + nTotal.outerHTML + '<span>');
            nInfo.innerHTML = '<span>' + info + '</span>';
 
            nPaging.appendChild(nFirst);
            nPaging.appendChild(nPrevious);
            $(nInfo).children().each(function (i, n) {
                nPaging.appendChild(n);
            });
            nPaging.appendChild(nNext);
            nPaging.appendChild(nLast);
 
            $(nFirst).click(function() {
                var iCurrentPage = calcCurrentPage(oSettings);
                if (iCurrentPage !== 1) {
                    oSettings.oApi._fnPageChange(oSettings, 'first');
                    fnCallbackDraw(oSettings);
                }
            });
 
            $(nPrevious).click(function() {
                var iCurrentPage = calcCurrentPage(oSettings);
                if (iCurrentPage !== 1) {
                    oSettings.oApi._fnPageChange(oSettings, 'previous');
                    fnCallbackDraw(oSettings);
                }
            });
 
            $(nNext).click(function() {
                var iCurrentPage = calcCurrentPage(oSettings);
                if (iCurrentPage !== calcPages(oSettings)) {
                    oSettings.oApi._fnPageChange(oSettings, 'next');
                    fnCallbackDraw(oSettings);
                }
            });
 
            $(nLast).click(function() {
                var iCurrentPage = calcCurrentPage(oSettings);
                if (iCurrentPage !== calcPages(oSettings)) {
                    oSettings.oApi._fnPageChange(oSettings, 'last');
                    fnCallbackDraw(oSettings);
                }
            });
 
            $(nPaging).find('.' + paginateInputClassName).keyup(function (e) {
                // 38 = up arrow, 39 = right arrow
                if (e.which === 38 || e.which === 39) {
                    this.value++;
                }
                // 37 = left arrow, 40 = down arrow
                else if ((e.which === 37 || e.which === 40) && this.value > 1) {
                    this.value--;
                }
 
                if (this.value === '' || this.value.match(/[^0-9]/)) {
                    /* Nothing entered or non-numeric character */
                    this.value = this.value.replace(/[^\d]/g, ''); // don't even allow anything but digits
                    return;
                }
 
                var iNewStart = oSettings._iDisplayLength * (this.value - 1);
                if (iNewStart < 0) {
                    iNewStart = 0;
                }
                if (iNewStart >= oSettings.fnRecordsDisplay()) {
                    iNewStart = (Math.ceil((oSettings.fnRecordsDisplay()) / oSettings._iDisplayLength) - 1) * oSettings._iDisplayLength;
                }
 
                oSettings._iDisplayStart = iNewStart;
                oSettings.oInstance.trigger("page.dt", oSettings);
                fnCallbackDraw(oSettings);
            });
 
            // Take the brutal approach to cancelling text selection.
            $('span', nPaging).bind('mousedown', function () { return false; });
            $('span', nPaging).bind('selectstart', function() { return false; });
 
            // If we can't page anyway, might as well not show it.
            var iPages = calcPages(oSettings);
            if (iPages <= 1) {
                $(nPaging).hide();
            }
        },
 
        'fnUpdate': function (oSettings) {
            if (!oSettings.aanFeatures.p) {
                return;
            }
 
            var iPages = calcPages(oSettings);
            var iCurrentPage = calcCurrentPage(oSettings);
 
            var an = oSettings.aanFeatures.p;
            if (iPages <= 1) // hide paging when we can't page
            {
                $(an).hide();
                return;
            }
 
            var disableClasses = calcDisableClasses(oSettings);
 
            $(an).show();
 
            // Enable/Disable `first` button.
            $(an).children('.' + firstClassName)
                .removeClass(oSettings.oClasses.sPageButtonDisabled)
                .addClass(disableClasses[firstClassName]);
 
            // Enable/Disable `prev` button.
            $(an).children('.' + previousClassName)
                .removeClass(oSettings.oClasses.sPageButtonDisabled)
                .addClass(disableClasses[previousClassName]);
 
            // Enable/Disable `next` button.
            $(an).children('.' + nextClassName)
                .removeClass(oSettings.oClasses.sPageButtonDisabled)
                .addClass(disableClasses[nextClassName]);
 
            // Enable/Disable `last` button.
            $(an).children('.' + lastClassName)
                .removeClass(oSettings.oClasses.sPageButtonDisabled)
                .addClass(disableClasses[lastClassName]);
 
            // Paginate of N pages text
            $(an).find('.' + paginateTotalClassName).html(iPages);
 
            // Current page number input value
            $(an).find('.' + paginateInputClassName).val(iCurrentPage);
        }
    };
})(jQuery);
   	</script>
   	<script>
$(document).ready(function() {
    var table = $('#example').DataTable( {
        dom: 'Bfrtlip',
        "pagingType": "input",
    "columnDefs": [ {
            "searchable": true,
            "orderable": true,
            "targets": 0
        } ],
        "order": [[ 0, 'asc' ]],
        buttons: [
                 {
                extend:    'copyHtml5',
                text:      '<i class="fa fa-files-o"></i> &nbsp; Copy',
                titleAttr: 'Copy',
                title: $('h3').text(),
                  exportOptions: {
        columns: [0,1, 2, 3,4,5], // Only name, email and role
    }
            },
            {
                extend:    'excelHtml5',
                text:      '<i class="fa fa-file-excel-o"></i>&nbsp; Excel',
               titleAttr: 'Excel',
                title: $('h3').text(),
                 customize: function( xlsx ) {
                var sheet = xlsx.xl.worksheets['sheet1.xml'];
 $('row c', sheet).attr('s', '51');
            },
                  exportOptions: {
        columns: [0,1, 2, 3,4,5], // Only name, email and role
    }
            },
            {
                extend:    'csvHtml5',
                text:      '<i class="fa fa-file-text-o"></i> &nbsp; CSV',
                titleAttr: 'CSV',
                title: $('h3').text(),
                  exportOptions: {
        columns: [0,1, 2, 3,4,5], // Only name, email and role
    }
            },
            {
                extend:    'pdfHtml5',
                text:      '<i class="fa fa-file-pdf-o"></i>&nbsp;  PDF',
                
              customize: function (doc) {
						//Remove the title created by datatTables
						doc.content.splice(0,1);
						//Create a date string that we use in the footer. Format is dd-mm-yyyy
						var now = new Date();
						var jsDate = now.getDate()+'-'+(now.getMonth()+1)+'-'+now.getFullYear();
						
						doc.pageMargins = [20,60,20,20];
						doc.defaultStyle.fontSize = 10;
						doc.styles.tableHeader.fontSize = 10;
						doc['header']=(function() {
							return {
								columns: [{
									    alignment: 'left',
										image: logo,
										width: 50,margin: [200,5]
									},{
										alignment: 'CENTER',
										text: 'List of Client',
										fontSize: 20,
										margin: [10,35],
									},],
								margin: [20, 0, 0,12],alignment: 'center',
							}
						});
						var objLayout = {};
						objLayout['hLineWidth'] = function(i) { return 2; };
						objLayout['vLineWidth'] = function(i) { return 2; };
						objLayout['hLineColor'] = function(i) { return '#ccc'; };
						objLayout['vLineColor'] = function(i) { return '#ccc'; };
						objLayout['paddingLeft'] = function(i) { return 14; };
						objLayout['paddingRight'] = function(i) { return 14; };
						doc.content[0].layout = objLayout;
				},
				titleAttr: 'PDF',
                  exportOptions: {
        columns: [0,1, 2, 3,4,5], // Only name, email and role
    }
            },
            {
           extend: 'print',
          text: '<i class="fa fa-print"></i>&nbsp; Print',
          titleAttr: 'Print',
        customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '10pt' )
                        .prepend(
                            '<center><img src=""/></center>'
                        );
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );
                },
         exportOptions: {
          columns: [0,1, 2, 3,4,5]
      },
      footer: true,
      autoPrint: true
    },],
    } );
$('input.global_filter').on( 'keyup click', function () {
        filterGlobal();
    } );
 
    $('input.column_filter').on( 'keyup click', function () {
        filterColumn( $(this).parents('tr').attr('data-column') );
    } );
  table.on( 'order.dt search.dt', function () {
       table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
          cell.innerHTML = i + 1;
          table.cell(cell).invalidate('dom'); 
       } );
    } ).draw();
       table.columns(6)
        .search('^(?:(?!Inactive|New|1).)*$\r?\n?', true, false)
        .draw();
    $("#choice").on("change",function(){
 var _val = $(this).val();//alert(_val);
 
 if(_val == 'Inactive'){   
        table.columns(6).search(_val).draw();
  }
 else if(_val == 'New'){   
        table.columns(6).search(_val).draw();
  }
  else if(_val == 'Active'){  //alert();
         table.columns(6).search(_val).draw();
          table.columns(6)
        .search('^(?:(?!Inactive|New|1).)*$\r?\n?', true, false)
        .draw();
  }
  else{
        table
        .columns()
        .search('')
        .draw(); 
  }
  })
} );
   	</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>