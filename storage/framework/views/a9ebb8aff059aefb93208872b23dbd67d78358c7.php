<?php $__env->startSection('main-content'); ?>
<style>
label{float:left;}
.dt-buttons{margin-bottom:10px;}
.search-btn{position:absolute;top:10px;right:16px;background:transparent;border: transparent;}
.dt-buttons {
    margin-top: -41px;
    position: absolute;
    right:15px;
}
.page-title{position: absolute
        padding: 8px 19px !important;
}
 .box-tools{
        position:absolute !important;
        margin-left: 280px !important;
    }
.dataTables_filter{
    display:none;
}
    .buttons-pdf {
    font-size: 0px !important;
    border-radius: 3px;
    padding: 8px 10px !important;
    background: #fff !important;
    border-color: #c6c6c6  !important;
    color:red !important;
}
.buttons-print {
    font-size: 0px !important;
    border-radius: 3px;
    padding: 8px 10px !important;
    background: #3c8dbc !important;
            border-color: #367fa9 !important;
}
.buttons-excel{
    font-size:0px !important;
    border-radius: 3px;
        padding: 8px 10px !important;
        
         background: #00a65a !important;
    border-color: #008d4c !important;
        

}
.buttons-excel:hover{
     background: #008d4c !important;

}
.buttons-pdf:hover{
     background: #f6f6f6  !important;
}

.buttons-print:hover{
     background: #367fa9 !important;
}


.fa{
    font-size: 16px !important;
}
.imgicon {
    background: #fff;
    display: block;
    width: 35px;
    float: left;
    margin-right: 10px;
    float: left;
    margin-right: 10px;
    border-radius: 2px;
    padding: 3px;
    border: 1px solid #12186b;
    height: 35px;
    margin-top: -6px;
    overflow: hidden;
}
.imgicon img{max-width:100%;}

@media  only screen and (max-width: 1280px){
    #example_wrapper table#example{
        display: table !important;
        overflow-x: auto;
        white-space: nowrap;
    }
}
@media  only screen and (max-width: 991px){
section.content-header.page-title.center h1 {
    text-align: center !important;
    width: 100% !important;
}
}
@media  only screen and (max-width: 880px){
    #example_wrapper table#example{
        display: block !important;
        overflow-x: auto;
        white-space: nowrap;
    }
}
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    
    <section class="content-header page-title center" >
     		<div class="">
     		    <div class="" style="text-align:center;">
     		        <h1>List of Schedule Details<span style="padding-right:10px;float:right;">Add / View / Edit</span></h1>
     		    </div>
     		    
     		</div>
    </section>
    <!-- Main content -->
    <section class="content">
   <div class="row">
      <div class="col-md-12">
     <div class="box box-success">
			      <div class="box-header">
              <div class="col-md-8" style="padding-left:0px; padding-right:0px;width:100% !important;">
              <div class="row">
              <div class="col-md-1 col-xs-1" style="width: 7.333% !important;"><label style="margin-left:28%;margin-top: 11px;">Filter: </label></div>
              <div class="col-md-3 col-xs-3" style="width: 130px; padding-right:0px;">
                <select name="choice" style="margin-left: 4px;" id="choice" class="form-control">
        <option value="1">All</option>
        <option value="Weekly">Weekly</option>
        <option value="Bi-Weekly">Bi-Weekly</option>
        <option value="Bi-Weekly">Monthly</option>
       
       
    </select>
                </div>
                 
     <div class="col-md-3 col-xs-3" style="width: 196px;">
     <table style="width: 100%;" cellspacing="0" cellpadding="3" border="0">
        <tbody>
         <tr id="filter_global">
                <td align="center"><input type="text" class="global_filter form-control" id="global_filter" placeholder="All Search"><button class="search-btn"><i class="fa fa-search"></i></button></td>
            </tr>
         
            <tr id="filter_col2" data-column="1" style="display:none">
                <td align="center"><input type="text" class="column_filter form-control" id="col1_filter" placeholder="Type"><button class="search-btn"><i class="fa fa-search"></i></button></td>
            </tr>
            <tr id="filter_col3" data-column="2" style="display:none">
                <td align="center"><input type="text" class="column_filter form-control" id="col2_filter" placeholder="EE / User Id"><button class="search-btn"><i class="fa fa-search"></i></button></td>
            </tr>
            <tr id="filter_col4" data-column="3" style="display:none">
                <td align="center"><input type="text" class="column_filter form-control" id="col3_filter" placeholder="Employee Name"><button class="search-btn"><i class="fa fa-search"></i></button></td>
            </tr>
            <tr id="filter_col5" data-column="4" style="display:none">
                <td align="center"><input type="text" class="column_filter form-control" id="col4_filter" placeholder="Email Id"><button class="search-btn"><i class="fa fa-search"></i></button></td>
            </tr>
            <tr id="filter_col6" data-column="5" style="display:none">
                <td align="center"><input type="text" class="column_filter form-control" id="col5_filter" placeholder="Tel. Number"><button class="search-btn"><i class="fa fa-search"></i></button></td>
            </tr>
        </tbody>
    </table>
    </div>
    
    </div>
    </div>
              <div class="box-tools pull-right">
                <div class="table-title">
                  <!--<a href="<?php echo e(route('schedules.create')); ?>">Add New Schedule</a>-->
               </div>
              </div>
            </div>
            <div class="col-md-12">
               <?php if( session()->has('success') ): ?>
               <div class="alert alert-success alert-dismissable"><?php echo e(session()->get('success')); ?></div>
               <?php endif; ?>
               <div class="table-responsive">
                  <table class="table table-hover table-bordered" id="example">
                     <thead>
                        <tr style="text-align:center">
                           <th style="text-align:center">No </th>
                           <th style="text-align:center">Employee Name </th>
                           <th style="text-align:center">Employer City </th>
                           <th style="text-align:center">Duration </th>
                           <th style="text-align:center">Start Date </th>
                           <th style="text-align:center">End Date </th>
                           <th style="text-align:center">Action</th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php $__currentLoopData = $schedule; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $employ): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							    <tr class="cc<?php echo e($employ->id); ?>">
							        <td style="text-align:center"><?php $__currentLoopData = $emp; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $employ1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php if($employ1->id==$employ->emp_name): ?><?php echo e($employ1->employee_id); ?>  <?php if($employ1->check=='1'): ?> <?php else: ?> <style>.cc<?php echo e($employ->id); ?>{ display:none}</style> <?php endif; ?> <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></td>
									<td><?php $__currentLoopData = $emp; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $employ1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?><?php if($employ1->id==$employ->emp_name): ?><?php echo e($employ1->firstName); ?> <?php echo e($employ1->middleName); ?> <?php echo e($employ1->lastName); ?> <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></td>
									<td><?php echo e($employ->emp_city); ?></td>
									<td><?php echo e($employ->duration); ?></td>
									<td style="text-align:center"><?php echo e($employ->sch_start_date); ?></td>
									<td style="text-align:center"><?php echo e($employ->sch_end_date); ?></td>
									<td style="text-align:center"><a class="btn-action btn-view-edit" href="<?php echo e(route('schedules.edit', $employ->id)); ?>"><i class="fa fa-edit"></i></a>
                                        <form action="<?php echo e(route('schedules.destroy',$employ->id)); ?>" method="post" style="display:none" id="delete-id-<?php echo e($employ->id); ?>">
                                        <?php echo e(csrf_field()); ?> <?php echo e(method_field('DELETE')); ?>

                                        </form>
                                        <a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record ?')){event.preventDefault();document.getElementById('delete-id-<?php echo e($employ->id); ?>').submit();} else{event.preventDefault();}" href=""><i class="fa fa-trash"></i></a></td>
								</tr>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
    </section>
</div>
<script>
$(document).ready(function() {
    
table.on( 'order.dt search.dt', function () {
        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
    $("#choice").on("change",function(){
 var _val = $(this).val();
 if(_val == 'Weekly'){ 
        table.columns(4).search(_val).draw();
          table
        .columns(4)
        .search('^(?:(?!Bi-Weekly).)*$\r?\n?', true, false)
        .draw();}
  else if(_val == 'Bi-Weekly'){
         table.columns(4).search(_val).draw();
          table
        .columns(4)
        .search('^(?:(?!Weekly).)*$\r?\n?', true, false)
        .draw();
  }
  else if(_val == 'Monthly'){ 
         table.columns(4).search(_val).draw();
          table
        .columns(4)
        .search('^(?:(?!Bi-Weekly).)*$\r?\n?', true, false)
        .draw();  table
        .columns(4)
        .search('^(?:(?!Weekly).)*$\r?\n?', true, false)
        .draw();
  }
  else{
        table.columns().search('').draw(); 
  }
  })
} );
</script>
<script>
$(document).ready(function() {
    var table = $('#example').DataTable( {
        dom: 'Bfrtlip',
    "columnDefs": [ {
            "searchable": false,
            "orderable": true,
            "targets": 0
        } ],
        "order": [[ 0, 'asc' ]],
        buttons: [
                 {
                extend:    'copyHtml5',
                text:      '<i class="fa fa-files-o"></i> &nbsp; Copy',
                titleAttr: 'Copy',
                title: $('h3').text(),
                  exportOptions: {
        columns: [0,1, 2, 3,4,5], // Only name, email and role
    }
            },
            {
                extend:    'excelHtml5',
                text:      '<i class="fa fa-file-excel-o"></i>&nbsp; Excel',
               titleAttr: 'Excel',
                title: $('h3').text(),
                 customize: function( xlsx ) {
                var sheet = xlsx.xl.worksheets['sheet1.xml'];
 $('row c', sheet).attr('s', '51');
            },
                  exportOptions: {
        columns: [0,1, 2, 3,4,5], // Only name, email and role
    }
            },
            {
                extend:    'csvHtml5',
                text:      '<i class="fa fa-file-text-o"></i> &nbsp; CSV',
                titleAttr: 'CSV',
                title: $('h3').text(),
                  exportOptions: {
        columns: [0,1, 2, 3,4,5], // Only name, email and role
    }
            },
            {
                extend:    'pdfHtml5',
                text:      '<i class="fa fa-file-pdf-o"></i>&nbsp;  PDF',
                
              customize: function (doc) {
						//Remove the title created by datatTables
						doc.content.splice(0,1);
						//Create a date string that we use in the footer. Format is dd-mm-yyyy
						var now = new Date();
						var jsDate = now.getDate()+'-'+(now.getMonth()+1)+'-'+now.getFullYear();
						var logo = 'data:image/jpeg;base64,<?php echo e($logo->logourl); ?>';
						doc.pageMargins = [20,60,20,20];
						doc.defaultStyle.fontSize = 10;
						doc.styles.tableHeader.fontSize = 10;
						doc['header']=(function() {
							return {
								columns: [{
									    alignment: 'left',
										image: logo,
										width: 50,margin: [200,5]
									},{
										alignment: 'CENTER',
										text: 'List of Client',
										fontSize: 20,
										margin: [10,35],
									},],
								margin: [20, 0, 0,12],alignment: 'center',
							}
						});
						var objLayout = {};
						objLayout['hLineWidth'] = function(i) { return 2; };
						objLayout['vLineWidth'] = function(i) { return 2; };
						objLayout['hLineColor'] = function(i) { return '#ccc'; };
						objLayout['vLineColor'] = function(i) { return '#ccc'; };
						objLayout['paddingLeft'] = function(i) { return 14; };
						objLayout['paddingRight'] = function(i) { return 14; };
						doc.content[0].layout = objLayout;
				},
				titleAttr: 'PDF',
                  exportOptions: {
        columns: [0,1, 2, 3,4,5], // Only name, email and role
    }
            },
            {
           extend: 'print',
          text: '<i class="fa fa-print"></i>&nbsp; Print',
          titleAttr: 'Print',
        customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '10pt' )
                        .prepend(
                            '<center><img src="https://financialservicecenter.net/public/business/<?php echo e($logo->logo); ?>"/></center>'
                        );
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );
                },
         exportOptions: {
          columns: [0,1, 2, 3,4,5]
      },
      footer: true,
      autoPrint: true
    },],
    } );
$('input.global_filter').on( 'keyup click', function () {
        filterGlobal();
    } );
 
    $('input.column_filter').on( 'keyup click', function () {
        filterColumn( $(this).parents('tr').attr('data-column') );
    } );
  table.on( 'order.dt search.dt', function () {
       table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
          cell.innerHTML = i + 1;
          table.cell(cell).invalidate('dom'); 
       } );
    } ).draw();
       table.columns(6)
        .search('^(?:(?!Inactive|New|1).)*$\r?\n?', true, false)
        .draw();
    $("#choice").on("change",function(){
 var _val = $(this).val();//alert(_val);
 
 if(_val == 'Inactive'){   
        table.columns(6).search(_val).draw();
  }
 else if(_val == 'New'){   
        table.columns(6).search(_val).draw();
  }
  else if(_val == 'Active'){  //alert();
         table.columns(6).search(_val).draw();
          table.columns(6)
        .search('^(?:(?!Inactive|New|1).)*$\r?\n?', true, false)
        .draw();
  }
  else{
        table
        .columns()
        .search('')
        .draw(); 
  }
  })
} );
function filterGlobal () {
    $('#example').DataTable().search(
        $('#global_filter').val(),
        $('#global_regex').prop('checked'),
        $('#global_smart').prop('checked')
    ).draw();
}
 
function filterColumn ( i ) {
    $('#example').DataTable().column( i ).search(
        $('#col'+i+'_filter').val(),
        $('#col'+i+'_regex').prop('checked'),
        $('#col'+i+'_smart').prop('checked')
    ).draw();
}
$( "#types" ).on('change',function() {
  // For unique choice
  var selVal = $( "#types option:selected" ).val(); 
 
  if(selVal=='Type')  
  {
      $('#filter_global').hide();
      $('#filter_col3').hide();
      $('#filter_col4').hide();
      $('#filter_col5').hide();
      $('#filter_col6').hide();
      $('#filter_col2').show();
  }
  else if(selVal=='EE / User ID')  
  {
      $('#filter_global').hide();
      $('#filter_col2').hide();
      $('#filter_col4').hide();
      $('#filter_col5').hide();
      $('#filter_col6').hide();
      $('#filter_col3').show();
  }
    else if(selVal=='Employee Name')  
  {
      $('#filter_global').hide();
      $('#filter_col2').hide();
      $('#filter_col3').hide();
      $('#filter_col5').hide();
      $('#filter_col6').hide();
      $('#filter_col4').show();
  }
   else if(selVal=='Email ID')  
  {
      $('#filter_global').hide();
      $('#filter_col2').hide();
      $('#filter_col3').hide();
      $('#filter_col4').hide();
      $('#filter_col6').hide();
      $('#filter_col5').show();
  }
  else if(selVal=='Tel. Number')  
  {
      $('#filter_global').hide();
      $('#filter_col2').hide();
      $('#filter_col3').hide();
      $('#filter_col4').hide();
      $('#filter_col5').hide();
      $('#filter_col6').show();
  }
  else{
      $('#filter_global').show();
       $('#filter_col3').hide();
      $('#filter_col4').hide();
      $('#filter_col5').hide();
      $('#filter_col6').hide();
      $('#filter_col2').hide();
  }
}); 
</script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('fscemployee.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>