<?php $__env->startSection('main-content'); ?>
<style>
   .redius{height: 30px; text-align: center;  line-height: 30px; width: 30px; background: #1685cc; color: #fff ; padding:0px; border-radius: 50%; margin-top: 3px; float: left; margin-right: 10px;}
   ul.curency{ width:100%; padding:0; margin:0;}
   ul.curency li{background: #286db5; padding: 5px 10px;margin-bottom: 7px;list-style:none;}
   ul.curency li a{ color:#fff;}
   ul.curency li a span{float:right; color:red;}
   .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th{ padding:6px 8px 6px 8px !important; }
   .table > tbody > tr > td .comboprice1{ width:60%; margin:0 auto; }
   .removebtn, .remove { color: red; border:0; padding: 9px 0; text-align: center;}
   .removebtn, .remove:hover { color: red; }
   .table tbody tr td {
    border: 1px solid #616161;
   }
</style>
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="page-title content-header">
      <h1><?php if(empty($_REQUEST['online'])): ?> Add New Service <?php else: ?> Online Access <?php endif; ?> Fees Price</h1>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="box box-success">
               <div class="box-header">
                  <div class="box-tools pull-right">
                  </div>
               </div>
               <div class="col-md-12">
                  <form method="post" action="<?php echo e(route('price.store')); ?>" class="form-horizontal" id="businessname" name="businessname" enctype="multipart/form-data">
                     <?php echo e(csrf_field()); ?>

                     <div class="form-group <?php echo e($errors->has('currency') ? ' has-error' : ''); ?>">
                        <label class="control-label col-md-3">Currency :</label>
                        <div class="col-lg-4 col-md-7">
                           <select name="currency" type="text" id="currency" class="form-control">
                              <option value="">Currency</option>
                              <?php $__currentLoopData = $currency; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cur): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value="<?php echo e($cur->id); ?>"><?php echo e($cur->currency.' '.$cur->sign); ?></option>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                           </select>
                           <?php if($errors->has('currency')): ?>
                           <span class="help-block">
                           <strong><?php echo e($errors->first('currency')); ?></strong>
                           </span>
                           <?php endif; ?>
                        </div>
                        <div class="col-lg-3 col-md-2"><a href="#" data-toggle="modal" data-target="#basicExampleModal" class="redius"><i class="fa fa-plus"></i></a>&nbsp;&nbsp;&nbsp; <a href="#" data-toggle="modal" data-target="#basicExampleModal3" class="redius"><i class="fa fa-minus"></i></a> </div>
                     </div>
                     <div class="form-group <?php echo e($errors->has('typeofservice') ? ' has-error' : ''); ?>">
                        <label class="control-label col-md-3">Type of Service :</label>
                        <div class="col-lg-4 col-md-7">
                           <select name="typeofservice" type="text" id="typeofservice" class="form-control">
                              <option value="">Type of Service</option>
                              <?php $__currentLoopData = $typeofser; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $typeofser1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value="<?php echo e($typeofser1->id); ?>"><?php echo e($typeofser1->typeofservice); ?></option>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                           </select>
                           <?php if($errors->has('typeofservice')): ?>
                           <span class="help-block">
                           <strong><?php echo e($errors->first('typeofservice')); ?></strong>
                           </span>
                           <?php endif; ?>
                        </div>
                        <div class="col-lg-3 col-md-2"><a href="#" data-toggle="modal" data-target="#basicExampleModal1" class="redius"><i class="fa fa-plus"></i></a> &nbsp;&nbsp;&nbsp;<a href="#" data-toggle="modal" data-target="#basicExampleModal4" class="redius"><i class="fa fa-minus"></i></a></div>
                     </div>
                     
                     <div class="form-group <?php echo e($errors->has('typeofservice') ? ' has-error' : ''); ?>">
                        <label class="control-label col-md-3">Note:</label>
                        <div class="col-lg-5 col-md-9">
                           <input type="text" id="note" class="form-control" placeholder="Note" name="note">
                        </div>
                     </div>
                     
                     <div id="hide_payroll">
                     <div class="form-group sss" id="hideperiod">
                        <label class="control-label col-md-3">Service Period :</label>
                        <div class="col-lg-4 col-md-7">
                           <select name="serviceperiod" type="text" id="serviceperiod" class="form-control">
                              <option value="">Service Period</option>
                              <?php $__currentLoopData = $period; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $period1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($period1->id); ?>" id="dis_<?php echo e($period1->id); ?>"><?php echo e($period1->period); ?></option>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                           </select>
                           <?php if($errors->has('serviceperiod')): ?>
                           <span class="help-block">
                                <strong><?php echo e($errors->first('serviceperiod')); ?></strong>
                           </span>
                           <?php endif; ?>
                        </div>
                        <div class="col-lg-3 col-md-2"><a href="#" data-toggle="modal" data-target="#basicExampleModal2" class="redius"><i class="fa fa-plus"></i></a> &nbsp;&nbsp;&nbsp;<a href="#" data-toggle="modal" data-target="#basicExampleModal5" class="redius"><i class="fa fa-minus"></i></a></div>
                     </div>
                     </div>
                       <div class="form-group showperiod">
                        <label class="control-label col-md-3">Payroll Service :</label>
                        <div class="col-lg-4 col-md-7">
                           <select name="payrollservice" type="text" id="payrollservice" class="form-control">
                              <option value="">Service Payroll Service</option>
                              <?php $__currentLoopData = $payrollservice; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $period1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value="<?php echo e($period1->id); ?>"><?php echo e($period1->servicename); ?></option>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                           </select>
                        </div>
                        <div class="col-lg-3 col-md-2"><a href="#" data-toggle="modal" data-target="#basicExampleModal2" class="redius"><i class="fa fa-plus"></i></a> &nbsp;&nbsp;&nbsp;<a href="#" data-toggle="modal" data-target="#basicExampleModal5" class="redius"><i class="fa fa-minus"></i></a></div>
                     </div>
                   
                     <div id="regular">
                        <div class="form-group ">
                           <label class="control-label col-md-3">Service Includes :</label>
                           <div class="col-lg-5 col-md-9">
                              <textarea name="serviceincludes" type="text" id="serviceincludes" class="form-control"></textarea>
                           </div>
                        </div>
                        <div class="form-group <?php echo e($errors->has('regularprice') ? ' has-error' : ''); ?>">
                           <label class="control-label col-md-3">Regular Price :</label>
                           <div class="col-md-3">
                              <input name="regularprice" type="text" id="regularprice"  placeholder="Regular Price" class="regularprice form-control">
                           </div>
                        </div>
                        <div class="form-group <?php echo e($errors->has('comboprice') ? ' has-error' : ''); ?>">
                           <label class="control-label col-md-3">Combo Price :</label>
                           <div class="col-md-3">
                              <input name="comboprice" type="text" id="comboprice" onblur="cal1();" placeholder="Combo Price" class="comboprice form-control">
                              <div class="com"></div>
                           </div>
                        </div>
                      <!--  <table class="table table-hover table-bordered dataTable no-footer" id="maintable" style="display:none">
                           <thead>
                              <tr style="border-bottom:1px solid #222 !important;">
                                 <th>EE Count</th>
                                 <th>Weekly</th>
                                 <th>Bi-Weekly</th>
                                 <th>Semi-Monthly</th>
                                 <th>Monthly</th>
                                 <th>Delete</th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td><input name="eecount[]" type="text" id="" onblur="" placeholder="EE Count" class="form-control"><input name="priceid[]" type="hidden" id="" onblur="" placeholder="EE Count" class="form-control"></td>
                                 <td><input name="weekly[]" type="text" id="" onblur="" placeholder="Weekly" class="form-control"></td>
                                 <td><input name="biweekly[]" type="text" id="" onblur="" placeholder="Bi-Weekly" class="form-control"></td>
                                 <td><input name="semimonthly[]" type="text" id="" onblur="" placeholder="Semi-monthly" class="form-control"></td>
                                 <td><input name="monthly[]" type="text" id="" onblur="" placeholder="Monthly" class="form-control"></td>
                                 <td><a href='javascript:void(0);'  class='remove'><span class='glyphicon glyphicon-remove'></span></a></td>
                              </tr>
                           </tbody>
                        </table>
                        <div class="">
                           <div class="table-title">
                              <a href="javascript:void(0);" style="font-size:18px;display:none" id="addMore" title="Add More Person"><span class="glyphicon glyphicon-plus"></span></a>
                           </div>
                        </div>-->
                     </div>
                     
                     
                     
                     <div class="input_fields_wrap_notes1" style="display:none">
                        <br>
                        <br>
                        <div class="table-responsive">
                        <table class="table" id="table1" >
                           <thead>
                              <tr style="background:#00a0e3;color:#222 !important;border-bottom:1px solid #222 !important;">
                                 <th style="border-bottom:1px solid #222 !important;" scope="col">#</th>
                                 <th style="border-bottom:1px solid #222 !important;" scope="col"># of Employees</th>
                                 <th style="border-bottom:1px solid #222 !important;" scope="col">Weekly</th>
                                 <th style="border-bottom:1px solid #222 !important;" scope="col">Bi-Weekly </th>
                                 <th style="border-bottom:1px solid #222 !important;" scope="col">Semi-Weekly </th>
                                 <th style="border-bottom:1px solid #222 !important;" scope="col">Monthly</th>
                                 <th style="border-bottom:1px solid #222 !important;" scope="col">Action</th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td> 1. </td>
                                 <td><input type="text" class="form-control" placeholder="Number of Employees" name="eecount[]"><input name="priceid[]" type="hidden" value="" placeholder="EE Count" class="form-control"></td>
                                 <td><input type="text" class="form-control" placeholder="Weekly" name="weekly[]"></td>
                                 <td><input type="text" class="form-control" placeholder="Bi-Weekly" name="biweekly[]"></td>
                                 <td><input type="text" class="form-control" placeholder="Semi-Weekly" name="semimonthly[]"></td>
                                 <td><input type="text" class="form-control" placeholder="Monthly" name="monthly[]"></td>
                                 <td><div class="form-control" style=" min-width: 160px;"><a class="btn btn-primary" style="margin-top:-4px !important;background:#e67300 !important;" onclick="education_field_note1();"> Add</a></div> </td>
                              </tr>
                           </tbody>
                        </table>
                        </div>
                     </div>
                     
                     <div class="input_fields_wrap_notes" style="display:none">
                        <br>
                        <br>
                        <div class="table-responsive">
                        <table class="table" id="table" >
                           <thead>
                              <tr style="background:#00a0e3;color:#222 !important;border-bottom:1px solid #222 !important;">
                                 <th style="border-bottom:1px solid #222 !important;" scope="col">#</th>
                                 <th style="border-bottom:1px solid #222 !important;" scope="col">Title</th>
                                 <th style="border-bottom:1px solid #222 !important;" scope="col">Regular Price</th>
                                 <th style="border-bottom:1px solid #222 !important;" scope="col">Combo Price</th>
                                 <th style="border-bottom:1px solid #222 !important;" scope="col">Action</th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                <td style="width:50px !important;"><input type="hidden" name="taxid[]"><input type="text" class="form-control" placeholder="1"></td>
                                <td>
                                    <select class="form-control" name="titles[]" id="titles">
                                       <?php $__currentLoopData = $taxtitle; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ti): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                       <option value="<?php echo e($ti->id); ?>"><?php echo e($ti->title); ?></option>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </td>
                                <td><input type="text" class="regularprice form-control" placeholder="Regular Price" name="regularprice1[]"></td>
                                <td><input type="text" class=" form-control comboprice" placeholder="Combo Price" name="comboprice1[]"></td>
                                <td>
                                     <div>
                                        <a class="btn btn-primary pull-right" style="margin-top:-4px !important;background:#e67300 !important;" onclick="education_field_note();"> Add</a>
                                        <a href="#" style="margin-top:-3px !important;" data-toggle="modal" data-target="#basictitle" class="redius">
                                             <i style="margin-top:-3px !important;" class="fa fa-plus"></i>
                                        </a>&nbsp;&nbsp;&nbsp; 
                                        <a href="#" style="margin-top:-3px !important;" data-toggle="modal" data-target="#basictitle1" class="redius">
                                            <i style="margin-top:-4px !important;" class="fa fa-minus"></i>
                                        </a>
                                    </div>
                                </td>
                              </tr>
                           </tbody>
                        </table>
                        </div>
                     </div>
                     <div id="input_fields_wrap_notes"></div>
                     
                     <div class="input_fields_wrap_notes2" style="display:none">
                        <br>
                        <br>
                        <table class="table" id="table2">
                           <thead>
                              <tr style="background:#00a0e3;color:#222 !important;border-bottom:1px solid #222 !important;">
                                 <th style="border-bottom:1px solid #222 !important;" scope="col">No</th>
                                 <th style="border-bottom:1px solid #222 !important;" scope="col">Type of License</th>
                                 <th style="border-bottom:1px solid #222 !important;" scope="col">Jurisdiction</th>
                                 <th style="border-bottom:1px solid #222 !important;" scope="col" width="250px">FSC Fees</th>
                                 <th style="border-bottom:1px solid #222 !important;" scope="col">Action</th>
                              </tr>
                           </thead> 
                           <tbody class="add_license_row">
                              <tr>
                                 <td style="width:50px !important;"><input type="text" class="form-control" placeholder="1"></td>
                                 <td>
                                    <select class="form-control">
                                        <option>--- Select Type---</option>
                                    </select>
                                 </td>
                                 <td><input type="text" class="form-control" placeholder="Jurisdiction"></td>
                                 <td><input type="text" class=" form-control" placeholder="FSC Fees"></td>
                                 <td>
                                     <div><a class="btn btn-warning" id="add_license"> Add</a></div></td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                     <div class="card-footer">
                         <div class="row">
                         <div class="col-md-3"></div>
                        <div class="col-xs-2" style="width:155px;">
                           <input class="btn_new_save btn-primary1" type="submit" name="submit" value="Save">
                        </div>
                        <div class="col-xs-2" style="width:155px;">
                           <a class="btn_new_cancel" href="<?php echo e(url('fac-Bhavesh-0554/price')); ?>?online=<?php echo e($_REQUEST['online']); ?>">Cancel</a> 
                        </div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </section>
<!--</div>-->
<!-- Currency--->
<div class="modal fade" id="basicExampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Currency</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <form action="" method="post" id="ajax">
            <?php echo e(csrf_field()); ?>

            <div class="modal-body">
               <input type="text" id="newopt" name="newopt"  class="form-control" placeholder="Currency"/><br>
               <input type="text" id="sign" name="sign"  class="form-control" placeholder="Currency Sign"/>
            </div>
            <div class="modal-footer">
               <button type="button" id="addopt" class="btn btn-primary">Save</button>
               <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
         </form>
      </div>
   </div>
</div>
<div class="modal fade" id="basicExampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Currency</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <ul class="curency curency_ref" id="div">
               <?php $__currentLoopData = $currency; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cur): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
               <li id="cur_<?php echo e($cur->id); ?>"><a class="delete"  id="<?php echo e($cur->id); ?>"><?php echo e($cur->currency.' '.$cur->sign); ?> <span><i class="fa fa-trash"></i></span></a></li>
               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>  
            </ul>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<!-- Currency--->
<!-- Title--->
<div class="modal fade" id="basictitle" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <form action="" method="post" id="ajax5">
            <?php echo e(csrf_field()); ?>

            <div class="modal-body">
               <input type="text" id="title" name="title" class="form-control" placeholder="Title"/>
            </div>
            <div class="modal-footer">
               <button type="button" id="addopttitle" class="btn btn-primary">Save</button>
               <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
         </form>
      </div>
   </div>
</div>
<!-- End Title--->

<!-- Title--->
<div class="modal fade" id="basictitle1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         
            <div class="modal-body">
                <?php $__currentLoopData = $taxtitle; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $taxt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div id="cur_<?php echo e($cur->id); ?>" class="col-md-12" style="border:1px solid;background:#def9ff;">
                        <div class="col-md-12" style="margin-top: 7px;margin-bottom:5px;">
    
                            <a class="deleterelated" style="color:#000;"
                               id="<?php echo e($taxt->id); ?>"><?php echo e($taxt->title); ?>

    
                                <span class="pull-right"><i class="fa fa-trash"
                                                            style="padding: 1px 6px!important;"></i></span></a>
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
            <div class="modal-footer">
               <button type="button" id="addopttitle" class="btn btn-primary">Save</button>
               <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
      </div>
   </div>
</div>
<!-- End Title--->


<div class="modal fade" id="basicExampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Type of Service</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <form action="" method="post" id="ajax1">
            <?php echo e(csrf_field()); ?>

            <div class="modal-body">
               <input type="text" id="newopt1" name="newopt1"  class="form-control" placeholder=""/>
            </div>
            <div class="modal-footer">
               <button type="button" id="addopt1" class="btn btn-primary">Save</button>
               <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
         </form>
      </div>
   </div>
</div>
<div class="modal fade" id="basicExampleModal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Type of Service</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <ul class="curency" id="serv">
               <?php $__currentLoopData = $typeofser; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $typeofser1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li id="ser_<?php echo e($typeofser1->id); ?>">
                    <a class="delete1" id="<?php echo e($typeofser1->id); ?>"><?php echo e($typeofser1->typeofservice); ?> 
                        <span><i class="fa fa-trash"></i></span>
                    </a>
                </li>
               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="basicExampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Service Period</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <form action="" method="post" id="ajax2">
            <?php echo e(csrf_field()); ?>

            <div class="modal-body">
               <input type="text" id="newopt2" name="newopt2"  class="form-control"/>
            </div>
            <div class="modal-footer">
               <button type="button" id="addopt2" class="btn btn-primary">Save</button>
               <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
         </form>
      </div>
   </div>
</div>
<div class="modal fade" id="basicExampleModal5" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5
            class="modal-title" id="exampleModalLabel">Service Period</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <ul class="curency"  id="serv1">
               <?php $__currentLoopData = $period; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $period1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
               <li id="per_<?php echo e($period1->id); ?>"><a class="delete2"  id="<?php echo e($period1->id); ?>"><?php echo e($period1->period); ?> <span><i class="fa fa-trash"></i></span></a></li>
               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<script>
   $.ajaxSetup({
           headers: {
               'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
           }
       });
      $(function () {
         $('#addopt').click(function () { //alert();
             var newopt = $('#newopt').val();
             if (newopt == '') {
                 alert('Please enter something!');
                 return;
             }
            //check if the option value is already in the select box
             $('#currency option').each(function (index) {
                 if ($(this).val() == newopt) {
                     alert('Duplicate option, Please enter new!');
                 }
             })
             $.ajax({
   type: "post",
   url: "<?php echo route('currency.currencies'); ?>",
   dataType: "json",
   data: $('#ajax').serialize(),
   success: function(data){
      alert('Successfully Add');
      $('#currency').append('<option value=' + newopt + '>' + newopt + '</option>');
      $("#div").load(" #div > *");
      $("#newopt").val('');
   },
   error: function(data){
      alert("Error")
   }
   });
            $('#basicExampleModal').modal('hide');
         });
     });
       $(function () {
         $('#addopttitle').click(function () { //alert();
             var title = $('#title').val();
             if (title == '') {
                 alert('Please enter something!');
                 return;
             }
            //check if the option value is already in the select box
             $('#titles option').each(function (index) {
                 if ($(this).val() == title) {
                     alert('Duplicate option, Please enter new!');
                 }
             })
             $.ajax({
   type: "post",
   url: "<?php echo route('title.taxtitle'); ?>",
   dataType: "json",
   data: $('#ajax5').serialize(),
   success: function(data){
       alert("Error")
   },
   error: function(data){
     alert('Successfully Add');
      $('#titles').append('<option value=' + title + '>' + title + '</option>');
      $("#div").load(" #div > *");
     $("#title").val('');
     
   }
   });
            
              $('#basictitle').modal('hide');
         });
     });
     
      $(function () {
         $('#addopt1').click(function () { //alert();
             var newopt = $('#newopt1').val();
             if (newopt == '') {
                 alert('Please enter something!');
                 return;
             }
   
             //check if the option value is already in the select box
             $('#typeofservice option').each(function (index) {
                 if ($(this).val() == newopt) {
                     alert('Duplicate option, Please enter new!');
                 }
             })
   
             //add the new option to the select box
             $('#typeofservice').append('<option value=' + newopt + '>' + newopt + '</option>');
   
             //select the new option (particular value)
            // $('#typeofservice option[value="' + newopt + '"]').prop('selected', true);
            
             $.ajax({
   type: "post",
   url: "<?php echo route('typeofser.typeofsers'); ?>",
   dataType: "json",
   data: $('#ajax1').serialize(),
   success: function(data){
      // alert("Data Save: " + data);
      alert('Successfully Add');
      $("#serv").load(" #serv > *");
       $("#newopt1").val('');
   },
   error: function(data){
      alert("Error")
   }
   });
   $('#basicExampleModal1').modal('hide');
         });
     });
     $(function () {
         $('#addopt2').click(function () {
             var newopt = $('#newopt2').val();
             if (newopt == '') {
                 alert('Please enter something!');
                 return;
             }
   
             //check if the option value is already in the select box
             $('#serviceperiod option').each(function (index) {
                 if ($(this).val() == newopt) {
                     alert('Duplicate option, Please enter new!');
                 }
             })
   
             //add the new option to the select box
             $('#serviceperiod').append('<option value=' + newopt + '>' + newopt + '</option>');
              
             //select the new option (particular value)
            // $('#serviceperiod option[value="' + newopt + '"]').prop('selected', true);
             $.ajax({
   type: "post",
   url: "<?php echo route('period.periods'); ?>",
   dataType: "json",
   data: $('#ajax2').serialize(),
   success: function(data){
      // alert("Data Save: " + data);
       alert('Successfully Add');
        $("#serv1").load(" #serv1 > *");
       $("#newopt2").val('');
   },
   error: function(data){
      alert("Error")
   }
   });
           //  $("#newopt2").val('');
              $('#basicExampleModal2').modal('hide');
         });
     });
</script>
<script>
   $(document).ready(function() {
   $(document).on('click', '.delete', function(){
           var id = $(this).attr('id');
           if(confirm("Are you sure you want to Delete this data?"))
           {
               $.ajax({
                   url:"<?php echo e(route('removecurrency.removecurrency1')); ?>",
                   mehtod:"get",
                   data:{id:id},
                   success:function(data)
                   {
                    // alert(data);
                      $('#cur_'+id).remove();
                      $("#currency").load(" #currency > *");
                   }
               })
           }
           else
           {
               return false;
           }
       }); 
       
       
       $(document).on('click', '.delete1', function(){
           var id = $(this).attr('id');
           if(confirm("Are you sure you want to Delete this data?"))
           {
               $.ajax({
                   url:"<?php echo e(route('removetypeofservice.removetypeofservice1')); ?>",
                   mehtod:"get",
                   data:{id:id},
                   success:function(data)
                   {
                       alert(data);
                     $('#ser_'+id).remove();
                      $("#typeofservice").load(" #typeofservice > *");
                   }
               })
           }
           else
           {
               return false;
           }
       }); 
       
         $(document).on('click', '.delete2', function(){
           var id = $(this).attr('id');
           if(confirm("Are you sure you want to Delete this data?"))
           {
               $.ajax({
                   url:"<?php echo e(route('removeperiod.removeperiod1')); ?>",
                   mehtod:"get",
                   data:{id:id},
                   success:function(data)
                   {
                       alert(data);
                      $('#per_'+id).remove();
                      $("#serviceperiod").load(" #serviceperiod > *");
                   }
               })
           }
           else
           {
               return false;
           }
       }); 
 });   
</script>
<script>
   $(document).ready(function(){
   	$(document).on('change','.regularprice', function()
   	{// alert();
   		 var id = $('#currency').val();
   		var sign1 = $(this).val();
   		var sign2 =parseFloat(Math.round(sign1 * 100) / 100).toFixed(2);
   		var no = sign2.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
   		$.get('<?php echo URL::to('getsign'); ?>?id='+id, function(data)
   		{  
               $('.regularprice').empty();
              $.each(data, function(index, subcatobj)
   		   {
   		 
   		  var val = subcatobj.sign+' '+no;
             $('.regularprice').val(val);
              //$('.regularprice').val(val);
   		   })
   
   		});
   			
   	});
   	
   	
   		$(document).on('change','#currency', function()
         	{  
         	    var typeofservice =  $('#typeofservice').val();
         	  //  alert(typeofservice);
   		 var id = $('#currency').val();
   	//	 alert(id);
   		var sign1 = $(this).val();
   	//	alert(sign1);
   		//var sign2 =parseFloat(Math.round(sign1 * 100) / 100).toFixed(2);
   	//	var no = sign2.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
   	  
   		$.get('<?php echo URL::to('/ckcurrency'); ?>?id='+sign1+'&state=' + typeofservice, function(data)
   		{  
              // $('.regularprice').empty();
              $("#serviceperiod option[value='13']").prop("disabled", false);
              $("#serviceperiod option[value='2']").prop("disabled", false);
              $("#serviceperiod option[value='3']").prop("disabled", false);
              $("#serviceperiod option[value='4']").prop("disabled", false);
              $("#serviceperiod option[value='15']").prop("disabled", false);
              $("#serviceperiod option[value='14']").prop("disabled", false);
              $("#serviceperiod option[value='12']").prop("disabled", false);
              $("#serviceperiod option[value='13']").prop("disabled", false);
              $("#serviceperiod option[value='7']").prop("disabled", false);
              $("#serviceperiod option[value='8']").prop("disabled", false);
              $("#serviceperiod option[value='9']").prop("disabled", false);
              $("#serviceperiod option[value='10']").prop("disabled", false);
              $("#serviceperiod option[value='12']").prop("disabled", false);
              $("#serviceperiod option[value='11']").prop("disabled", false);
               $("#serviceperiod option[value='5']").prop("disabled", false);
              $.each(data, function(index, subcatobj)
   		   { 
   		 $("#serviceperiod option[value='"+subcatobj.period+"']").prop("disabled", true); 
   		   })
   
   		});
   			
   	});
   	 $('.showperiod').hide(); 
   	    	   
   $(document).on('change','#typeofservice', function()
   	{
   	    	var payroll = $(this).val();
   	    	if(payroll=='8')
   	    	{
   	    	 
   	    	    
   	    	    
   	    	    $('#dis_11').show(); 
   	    	    $('#dis_12').show();   	    
   	    	    $('#dis_14').show(); 
   	    	    $('.showperiod').show(); 
   	    	    $('#hide_payroll').hide(); 
   	    	    
   	    	        $('#dis_10').show();
   	    	       $('#dis_5').show(); 
   	    	        $('#dis_7').show(); 
   	    	         $('#dis_4').show();
   	    	}
   	    	
   	    	else if(payroll=='11')
   	    	{
   	    	    $('.showperiod').hide(); 
   	    	    $('#hideperiod').show(); 
   	    	    $('#hide_payroll').show(); 
   	    	    
   	    	    $('#dis_11').show(); 
   	    	    $('#dis_12').show();   	    
   	    	    $('#dis_14').show(); 
   	    	        $('#dis_10').show();
   	    	       $('#dis_5').show(); 
   	    	        $('#dis_7').show(); 
   	    	         $('#dis_4').show();
   	    	}
   	    	
   	    	else if(payroll=='9')
   	    	{
   	    	     $('.showperiod').hide(); 
   	    	    $('#hideperiod').show(); 
   	    	    $('#hide_payroll').show(); 
   	    	   
   	    	   $('#dis_11').hide(); 
   	    	    $('#dis_12').hide();   	    
   	    	     $('#dis_14').hide(); 
   	    	    $('#dis_10').hide();
   	    	       $('#dis_5').hide(); 
   	    	        $('#dis_7').hide();
   	    	         $('#dis_4').hide();
   	    	}
   	    	else if(payroll=='10')
   	    	{
   	    	     $('.showperiod').hide(); 
   	    	    $('#hideperiod').show(); 
   	    	    $('#hide_payroll').show(); 
   	    	   
   	    	   $('#dis_11').hide(); 
   	    	    $('#dis_12').hide();   	    
   	    	     $('#dis_14').hide(); 
   	    	     $('#dis_10').hide();
   	    	       $('#dis_5').hide(); 
   	    	        $('#dis_7').hide(); 
   	    	         $('#dis_4').hide();
   	    	}
   	    	else
   	    	{
   	    	     $('.showperiod').hide(); 
   	    	    $('#hideperiod').show(); 
   	    	    $('#hide_payroll').show(); 
   	    	   
   	    	    $('#dis_11').hide(); 
   	    	    $('#dis_12').hide();   	    
   	    	    $('#dis_14').hide(); 
   	    	    $('#dis_10').show();
   	    	    $('#dis_5').show(); 
   	    	    $('#dis_7').show(); 
   	    	    $('#dis_4').show();
   	    	}
   	});
   });
   
   
   $(document).ready(function(){
   	$(document).on('change','.comboprice', function()
   	{// alert();
   		 var id = $('#currency').val();
   		var sign1 = $(this).val();
   		var sign2 =parseFloat(Math.round(sign1 * 100) / 100).toFixed(2);
   		var no = sign2.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
   		$.get('<?php echo URL::to('getsign'); ?>?id='+id, function(data)
   		{  
             $('.comboprice').empty();
              $.each(data, function(index, subcatobj)
   		   {
   		  var val = subcatobj.sign+' '+no;
             $('.comboprice').val(val);
   		   })
   
   		});
   			
   	});
   	
   	$(document).on('change','#typeofservice', function()
   	{      var currency =  $('#currency').val();
   		   var id = $('#typeofservice').val();//alert(currency);
               $("#serviceperiod option[value='13']").prop("disabled", false);
              $("#serviceperiod option[value='2']").prop("disabled", false);
              $("#serviceperiod option[value='3']").prop("disabled", false)
              $("#serviceperiod option[value='5']").prop("disabled", false);
              $("#serviceperiod option[value='4']").prop("disabled", false);
              $("#serviceperiod option[value='15']").prop("disabled", false);
              $("#serviceperiod option[value='14']").prop("disabled", false);
              $("#serviceperiod option[value='12']").prop("disabled", false);
              $("#serviceperiod option[value='13']").prop("disabled", false);
              $("#serviceperiod option[value='7']").prop("disabled", false);
              $("#serviceperiod option[value='8']").prop("disabled", false);
              $("#serviceperiod option[value='9']").prop("disabled", false);
              $("#serviceperiod option[value='10']").prop("disabled", false);
              $("#serviceperiod option[value='12']").prop("disabled", false);
              $("#serviceperiod option[value='11']").prop("disabled", false);
   		$.get('<?php echo URL::to('/ckcurrency'); ?>?id='+currency+'&state=' + id, function(data)
   		{  
   		    $.each(data, function(index, subcatobj)
   		   {
   		$("#serviceperiod option[value='"+subcatobj.period+"']").prop("disabled", true); 
   		   })
   		});
   	});	
   	
   $(document).on('change','#typeofservice', function()
   {
   var sign1 = $(this).val();
   //alert(sign1);
   if(sign1=='3')
   {
       $('.input_fields_wrap_notes').show();
       $('.input_fields_wrap_notes1').hide();
       $('.input_fields_wrap_notes2').hide();
       $('#regular').hide();
         $('.sss').show();
   }
   else if(sign1=='4')
   {
       $('.input_fields_wrap_notes2').show();
        $('.input_fields_wrap_notes').hide();
        $('.input_fields_wrap_notes1').hide();
       $('#regular').hide();
        $('.sss').hide();
   }
   else if(sign1=='11')
   {
       $('.input_fields_wrap_notes1').show();
        $('.input_fields_wrap_notes').hide();
        $('.input_fields_wrap_notes2').hide();
       $('#regular').hide();
        $('.sss').hide();
   }
   else
   {
      $('.input_fields_wrap_notes').hide();
      $('.input_fields_wrap_notes1').hide();
      $('.input_fields_wrap_notes2').hide();
       $('#regular').show();  
            $('.sss').show();
   }
   });
   });
</script>
<script>
   var room1 = 0;
   var coun = 0;
   var z = room1 + coun; 
   function education_field_note() {
   room1++;
   z++;
   var objTo = document.getElementById('table')
   var divtest = document.createElement("tbody");
   divtest.setAttribute("class", "form-group removeclass"+z);
   divtest.innerHTML = '<tr><td>'+ z +'  <input type="hidden" name="taxid[]"></td><td><select class="form-control" name="titles[]" id="titles"> <?php $__currentLoopData = $taxtitle; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ti): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?><option value="<?php echo e($ti->title); ?>"><?php echo e($ti->title); ?></option><?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></select></td><td><input type="text" class="regularprice_'+z+' form-control" name="regularprice1[]" placeholder="Regular Price"></td><td><input placeholder="Combo Price" type="text" class=" form-control comboprice_'+z+'" name="comboprice1[]"></td><td><button class="btn btn-danger" type="button" onclick="remove_education_fields('+ z +');"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span> </button></td></tr>';
   var rdiv = 'removeclass'+z;
   var rdiv1 = 'Schoolname'+z;
   var comboprice = '.comboprice_'+z;
   var regularprice = '.regularprice_'+z;
   $(document).on('change',comboprice, function()
   	{ //alert();
   		 var id = $('#currency').val();
   		var sign1 = $(this).val();
   		var sign2 =parseFloat(Math.round(sign1 * 100) / 100).toFixed(2);
   		var no = sign2.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
   		$.get('<?php echo URL::to('getsign'); ?>?id='+id, function(data)
   		{  
             $(comboprice).empty();
              $.each(data, function(index, subcatobj)
   		   {
   		  var val = subcatobj.sign+' '+no;
             $(comboprice).val(val);
   		   })
   		});
   	});
   $(document).on('change',regularprice, function()
   	{
   		 var id = $('#currency').val();
   		var sign1 = $(this).val();
   		var sign2 =parseFloat(Math.round(sign1 * 100) / 100).toFixed(2);
   		var no = sign2.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
   		$.get('<?php echo URL::to('getsign'); ?>?id='+id, function(data)
   		{  
               $(regularprice).empty();
              $.each(data, function(index, subcatobj)
   		   {
   		  var val = subcatobj.sign+' '+no;
             $(regularprice).val(val);
   		   })
   		});
   	});
   objTo.appendChild(divtest)
   }
   function remove_education_fields(rid) {
   $('.removeclass'+rid).remove();
   room1--;
   z--;
   }
   
   
   
   function cal1() { 
           var a = $(".regularprice").val();
          var sign2 =  parseFloat(a.replace( /[^\d\.]*/g, ''));
          var b = $(".comboprice").val();//alert(a); 
           var sign =  parseFloat(b.replace( /[^\d\.]*/g, '')); //alert(sign2);
          // alert(b);
           //c2 = a > b;
           if(sign2 >= sign)
           {
           $(".com").show();
           $(".com").html('Please Enter Combo Price > Regular Price');
           }
           else if(sign >= sign2)
           {
           $(".com").hide();     
           }
           else
           {
          // $(".com").hide(); 
           }
           }
</script>
<script>
   $(function(){
       $('#addMore').on('click', function() {
                 var data = $("#maintable tr:eq(1)").clone(true).appendTo("#maintable");
                 data.find("input").val('');
        });
        $(document).on('click', '.remove', function() {
            var trIndex = $(this).closest("tr").index();
               if(trIndex>0) {
                $(this).closest("tr").remove();
              } else {
                alert("Sorry!! Can't remove first row!");
              }
         });
   });      
   $('#add_license').on('click', function() {
        $(".add_license_row tr:last-child").after('<tr><td><input type="text" class="form-control"></td><td><select class="form-control"> <option> --- Select Type --- </option> </select></td><td><input type="text" class="form-control"></td><td><input type="text" class="form-control"></td><td><a class="btn btn-danger" id="remove_license"><i class="fa fa-minus"></i></a></td></tr>');
   });
    $("body").on('click','#remove_license',function()
 	{
        $(this).parent().parent().remove();
    });
</script>


<script>
     var room1 = 1;
   var coun = 0;
   var z = room1 + coun; 
   function education_field_note1() {
   room1++;
   z++;
   var objTo = document.getElementById('table1')
   var divtest = document.createElement("tbody");
   divtest.setAttribute("class", "form-group removeclass"+z);
   divtest.innerHTML = '<tr><td>'+ z +'<input type="hidden" name="priceid[]"></td><td><input type="text" class="form-control" placeholder="Number of Employees" name="eecount[]"></td><td><input type="text" class="form-control" placeholder="Weekly" name="weekly[]"></td><td><input type="text" class="form-control" placeholder="Bi-Weekly" name="biweekly[]"></td><td><input type="text" class="form-control" placeholder="Semi-Weekly" name="semimonthly[]"></td><td><input type="text" class="form-control" placeholder="Monthly" name="monthly[]"></td><td><button class="btn btn-danger" type="button" onclick="remove_education_fields1('+ z +');"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></button></td></tr>';
   var rdiv = 'removeclass'+z;
   var rdiv1 = 'Schoolname'+z;
   var comboprice = '.comboprice_'+z;
   var regularprice = '.regularprice_'+z;
   $(document).on('change',comboprice, function()
   	{ //alert();
   		 var id = $('#currency').val();
   		var sign1 = $(this).val();
   		var sign2 =parseFloat(Math.round(sign1 * 100) / 100).toFixed(2);
   		var no = sign2.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
   		$.get('<?php echo URL::to('getsign'); ?>?id='+id, function(data)
   		{  
             $(comboprice).empty();
              $.each(data, function(index, subcatobj)
   		   {
   		  var val = subcatobj.sign+' '+no;
             $(comboprice).val(val);
   		   })
   		});
   	});
   $(document).on('change',regularprice, function()
   	{// alert();
   		 var id = $('#currency').val();
   		var sign1 = $(this).val();
   		var sign2 =parseFloat(Math.round(sign1 * 100) / 100).toFixed(2);
   		var no = sign2.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
   		$.get('<?php echo URL::to('getsign'); ?>?id='+id, function(data)
   		{  
               $(regularprice).empty();
              $.each(data, function(index, subcatobj)
   		   {
   		  var val = subcatobj.sign+' '+no;
             $(regularprice).val(val);
              //$('.regularprice').val(val);
   		   })
   		});
   	});
   objTo.appendChild(divtest)
   
   }
   function remove_education_fields1(rid) {
   $('.removeclass'+rid).remove();
   room1--;
   z--;
   }
    
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>