<?php $__env->startSection('main-content'); ?>
<style>
.fsc-section-head img{    margin-top: -4px;
    width: 23px;
}
text{font-size:18px !important}
.collapse.in {
    display: block;
    border: 2px solid #428bca;
    background-color: #e9f7ff;
}
.fa-file-pdf-o{color:#E63625}
.fsc-section-head h4 {
    padding: 10px;
}
a:focus, a:hover {
    color: #ffff !important;
    text-decoration: none;
        text-decoration-color: currentcolor;
        text-decoration-line: none;
        text-decoration-style: solid;
}
#accordion {
    margin-top: 4%;
}
.jsmaps-text{ display:none !important}
.panel-heading  a:before {
   font-family: 'FontAwesome';
   content: "\f103";
   float: right;
   transition: all 0.5s;
font-size: 31px;
margin-top: -5px;
}
.panel-heading.active a:before {
	-webkit-transform: rotate(180deg);
	-moz-transform: rotate(180deg);
	transform: rotate(180deg);color: white;
} 
.jsmaps-select select {
    display: block;
    width: 100%;
    max-width: 100%;
    height: auto;
    margin-bottom: 20px;
    padding: 7px 13px 5px 13px !important;
    border: 1px solid #428bca;
    border-radius: 3px;
    appearance: none;
    -webkit-appearance: none;
    -moz-appearance: none;
    background: inherit;
    font-size: 17px;
    background: #fff !important;
}
.jsmaps-wrapper .jsmaps{
width: 100% !important;
    height: 450px !important;
}
.panel-title{cursor: pointer;
color: white;
font-family: 'Proza Libre', serif;
font-size: 2em;
background-color: #428bca;
padding: 0%;
border-top-left-radius: 5px;
border-top-right-radius: 5px;
margin: 0;}
.panel-default > .panel-heading {
    color: #fff;
    background-color: #428bca;
    border-color: #ddd;
}
.jsmaps-wrapper {
    position: relative;
    margin: 0 auto;
    width: 100% !important;
    height: 450px !important;
}

svg {
width: 100% !important;
    height: 450px !important;
}
</style>
<div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 fsc-content-head">
			<h4>Link</h4>
		</div>
	</div>

 <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                       
                      Federal 
                    </a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
<?php if(!empty($federal->type)): ?>
<?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $form): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<?php if($form->type=='Federal'): ?> 
		<h4 style="padding: 1% 5%;"><a style="font-size:20px;color:#0A3867" href="" target="_blank"><i class="fa fa-file-pdf-o" style="color:#E63625"></i>&nbsp;&nbsp;</a>
<a style="font-size:20px;color:#0A3867" href="" target="_blank" class="pull-right">&nbsp;&nbsp;Downlaod</a></h4>
<?php endif; ?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>	
<?php else: ?>
<center><h2>Not Availble at this time</h2></center>
<?php endif; ?>
                </div>
            </div>
        </div>
<div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingfive">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefive" aria-expanded="true" aria-controls="collapsefive">
                        
                State
                    </a>
                </h4>
            </div>
            <div id="collapsefive" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingfive">
                <div class="panel-body">
                 <div class="col-lg-3 col-md-3 col-sm-10 col-xs-10 col-md-offset-4"> 
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		   <div class="jsmaps-wrapper" id="usa-map"></div>
	</div>

	


<?php $__currentLoopData = $employment; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $form): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	<?php if($form->type=='State'): ?> 
		<h4 style="padding: 1% 5%;"><a style="font-size:20px;color:#0A3867" href="" target="_blank"><i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp;<?php echo e($form->name); ?></a>

		<a style="font-size:20px;color:#0A3867" href="" target="_blank" class="pull-right">&nbsp;&nbsp;Downlaod</a></h4>
<?php else: ?>

	<?php endif; ?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>	

                </div>
            </div>
        </div>




 <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingfour">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefour" aria-expanded="true" aria-controls="collapsefour">
                       
                    County
                    </a>
                </h4>
            </div>
            <div id="collapsefour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfour">
                <div class="panel-body">
                   <?php if(!empty($county->type)): ?>
<?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $form): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	<?php if($form->type=='County'): ?> 
		<h4 style="padding: 1% 5%;"><a style="font-size:20px;color:#0A3867" href="" target="_blank"><i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp;</a>

		<a style="font-size:20px;color:#0A3867" href="" target="_blank" class="pull-right">&nbsp;&nbsp;Downlaod</a></h4>
<?php else: ?>

	<?php endif; ?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>	
<?php else: ?>
<center><h2>Not Availble at this time</h2></center>
<?php endif; ?>

                </div>
            </div>
        </div>



        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingTwo">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
            
                     City / Local
                    </a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                <div class="panel-body">
                  <?php if(!empty($city->type)): ?>
<?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $form): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	<?php if($form->type=='Local City'): ?> 
		<h4 style="padding: 1% 5%;"><a style="font-size:20px;color:#0A3867" href="" target="_blank"><i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp;<?php echo e($form->form_no); ?></a>

		<a style="font-size:20px;color:#0A3867" href="" target="_blank" class="pull-right">&nbsp;&nbsp;Downlaod</a></h4>
<?php else: ?>

	<?php endif; ?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>	
<?php else: ?>
<center><h2>Not Availble at this time</h2></center>
<?php endif; ?>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingThree">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                       Community Link
                    </a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                <div class="panel-body">
                  <?php if($community->type =='Community Link'): ?>
<?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $form): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<span style="color:#fff"><?php echo e(++$i); ?></span>
<?php if($form->type=='Community Link'): ?> 
 
		<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-sub-imgs">
	      <a href=" <?php echo e(url('linktype',[$form->category])); ?>"><img style="cursor:pointer;" class="img-responsive" src="<?php echo e(url('public/linkcategory')); ?>/<?php echo e($form->linkimage); ?>"/></a>
		</div>

<?php else: ?>


	<?php endif; ?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>	
<?php echo $data->links('pagination'); ?>

<?php else: ?>
<center><h2>Not Availble at this time</h2></center>
<?php endif; ?>
                </div>
            </div>
        </div>
        
        
        
           <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOther">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOther" aria-expanded="false" aria-controls="collapseOther">
                       Other Link
                    </a>
                </h4>
            </div>
            <div id="collapseOther" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOther">
                <div class="panel-body">
            
<?php $__currentLoopData = $other; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $form): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
 	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-sub-imgs">
	      <a href="<?php echo e($form->link); ?>" style="font-size:14px" target="_blank"><?php echo e($form->link); ?></a>
		</div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>	

                </div>
            </div>
        </div>

    </div><!-- panel-group -->



<div>
</div>


</div>


<script>

 $('.panel-collapse').on('show.bs.collapse', function () { 
    $(this).siblings('.panel-heading').addClass('active');
  });

  $('.panel-collapse').on('hide.bs.collapse', function () {
    $(this).siblings('.panel-heading').removeClass('active');
  });
</script>
  <link href="<?php echo e(URL::asset('public/frontcss/css/jsmaps.css')); ?>" rel="stylesheet" type="text/css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="<?php echo e(URL::asset('public/frontcss/js/jsmaps-libs.js')); ?>" type="text/javascript"></script>
  <!--<script src="<?php echo e(URL::asset('public/frontcss/js/jsmaps-panzoom.js')); ?>"></script>-->
  <script src="<?php echo e(URL::asset('public/frontcss/js/jsmaps.min.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(URL::asset('public/frontcss/js/usa.js')); ?>" type="text/javascript"></script>
  <script type="text/javascript">
    $(function() {      
      $('#usa-map').JSMaps({
        map: 'usa'
      });
    });    
  </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front-section.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>