<?php $__env->startSection('main-content'); ?>
<style>
label{ text-align:right;float: right;}
.tess { text-align:right; position:absolute; font-size:11px; margin:2px 16px 0 0; right:0; width:100%;top:13px; }
.star-required{color:red; position:absolute; top:0; right:0;}
.fieldGroup{width: 100%;display: inline-block;border-bottom: 2px solid #512e90;padding-bottom: 20px;}

.nav-tabs>li>a { height:40px; }
.fieldGroup:last-child{border-bottom:transparent;}
.glyphicon-chevron-right{top: 25px;text-align: center;font-size: 3rem;}
.fsc-reg-sub-header-div{ border-radius:0; padding:6px 4%; border-top:2px solid #fff; border-bottom:2px solid #fff; float:left; width:100%; text-align: center; margin-top: 20px; }
.Libre.fsc-reg-sub-header { margin-top:0; padding:0; margin:0; }
.Red{background-color:red;color:#fff}
.Blue{background-color:blue !important;color:#fff}
.Green{background-color:green !important;color:#fff}
.Yellow{background-color:Yellow !important;}
.Orange{background-color:Orange !important;color:#fff}
.nav-tabs > li {
    float: left !important;
    width: 32.3% !important;}
input[type="radio"], input[type="checkbox"] { margin:3px 0 14px;}
.star-required1{color:#fff;position:absolute;}
</style>
<div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     	<h1><span class="pull-left"><?php echo e($common->first_name); ?> <?php echo e($common->middle_name); ?> <?php echo e($common->last_name); ?></span>Client Registartion Form </h1>
    </section>
  <!-- Main content -->
    <section class="content">
   <div class="row">
      <div class="col-md-12">
         <div class="box box-success">
             <div class="box-header">
            
              <div class="box-tools pull-right">
              </div>
            </div>
            <div class="col-md-12">
               
               <div class="panel with-nav-tabs panel-primary">
					<div class="panel-heading">
						<ul class="nav nav-tabs" id="myTab">
							<li class="active"><a href="#tab1primary" data-toggle="tab">Step-1</a></li>
							<li><a href="#tab2primary" data-toggle="tab">Step-2</a></li>
							<li><a href="#tab3primary" data-toggle="tab">Step-3</a></li>
						</ul>
					</div>
 <!--<IMG SRC="http://api.hostip.info/flag.php" width="30" height="20" BORDER="0" ALT="Your Choice">-->
<form method="post" action="<?php echo e(route('customerconvert.update', $common->id)); ?>" class="form-horizontal" enctype="multipart/form-data">
<?php echo e(csrf_field()); ?><?php echo e(method_field('PATCH')); ?>	
<div class="panel-body">
                        <div class="tab-content">
<div class="col-md-12 col-sm-12 col-xs-12"><img src="" alt="" class="img-responsive"></div>
                           <div class="tab-pane fade in active" id="tab1primary">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <input type="hidden" class="form-control fsc-input" name="user_type" id="user_type" value="<?php echo e($business->bussiness_name); ?>">
                               
                                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 <?php echo e($errors->has('company_name') ? ' has-error' : ''); ?>">
                                    <div class="form-group">
                                       <label class="col-lg-3 form-label">Company name : <span class="star-required">*</span><span class="tess">(Legal Name)</span></label>
                                    
                                    <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                       <input type="text" class="form-control fsc-input" name="company_name" id="company_name" value="<?php echo e($common->company_name); ?>">
                                       <?php if($errors->has('company_name')): ?>
                                       <span class="help-block">
                                       <strong><?php echo e($errors->first('company_name')); ?></strong>
                                       </span>
                                       <?php endif; ?>
                                    </div>
                                 </div>
                                 </div>
 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 <?php echo e($errors->has('business_name') ? ' has-error' : ''); ?>">
                                    <div class="form-group">
                                       <label class="col-lg-3 form-label">Business name : <span class="star-required">*</span><span class="tess">(DBA Name)</span></label>
                                    
                                    <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                       <input type="text" class="form-control fsc-input" name="business_name" id="business_name" value="<?php echo e($common->business_name); ?>">
                                       <?php if($errors->has('business_name')): ?>
                                       <span class="help-block">
                                       <strong><?php echo e($errors->first('business_name')); ?></strong>
                                       </span>
                                       <?php endif; ?>
                                    </div>
                                 </div>
                                 </div>
                                 
                                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 <?php echo e($errors->has('address') ? 'has-error' : ''); ?>">
                                    <div class="form-group">
                                       <label class="col-lg-3 form-label">Business Address 1 : <span class="star-required">*</span></label>
                                    
                                    <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                       <input type="text" class="form-control fsc-input" name="address" id="address" value="<?php echo e($common->address); ?>">
                                       <?php if($errors->has('address')): ?>
                                       <span class="help-block">
                                       <strong><?php echo e($errors->first('address')); ?></strong>
                                       </span>
                                       <?php endif; ?>
                                    </div>
                                 </div>
                                 </div>
                                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 <?php echo e($errors->has('address1') ? 'has-error' : ''); ?>">
                                    <div class="form-group">
                                       <label class="col-lg-3 form-label">Business Address 2 : <span class="star-required1">*</span></label>
                               
                                    <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                       <input type="text" class="form-control fsc-input" name="address1" id="address1" value="<?php echo e($common->address1); ?>">
                                       <?php if($errors->has('address1')): ?>
                                       <span class="help-block">
                                       <strong><?php echo e($errors->first('address1')); ?></strong>
                                       </span>
                                       <?php endif; ?>
                                    </div>
                                 </div>
                                 </div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  <?php echo e($errors->has('countryId') ? 'has-error' : ''); ?>">
                                    <div class="form-group">
                                       <label class="col-lg-3 form-label">Country : <span class="star-required">*</span></label>
                           
                                   <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                       <div class="row">
                                          <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                             <div class="dropdown">
                                                <select name="countryId" id="countries_states1" class="form-control fsc-input bfh-countries" data-country="<?php echo e($common->countryId); ?>">
                                                 
                                                </select>
                                                  </div>                               
                                         </div>
                                    </div>
                                          
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 <?php echo e($errors->has('zip') ? 'has-error' : ''); ?> <?php echo e($errors->has('stateId') ? 'has-error' : ''); ?> <?php echo e($errors->has('city') ? 'has-error' : ''); ?> ">
                                    <div class="form-group">
                                       <label class="col-lg-3 form-label">City / State / Zip : <span class="star-required">*</span></label>
                                    
                                    <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                       <div class="row">
                                          <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                             <input type="text" class="form-control fsc-input" id="city" name="city" placeholder="City" value="<?php echo e($common->city); ?>">
                                             <?php if($errors->has('city')): ?>
                                             <span class="help-block">
                                             <strong><?php echo e($errors->first('city')); ?></strong>
                                             </span>
                                             <?php endif; ?>
                                          </div>
                                         
                                          <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                             <div class="dropdown" style="margin-top: 1%;">
                                                <select name="stateId" id="stateId" class="form-control fsc-input bfh-states" data-country="countries_states1" data-state="<?php echo e($common->stateId); ?>">
                                                   
                                                </select>
                                                <?php if($errors->has('stateId')): ?>
                                                <span class="help-block">
                                                <strong><?php echo e($errors->first('stateId')); ?></strong>
                                                </span>
                                                <?php endif; ?>
                                             </div>
                                          </div>
                                          <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
               <input type="text" class="form-control fsc-input" id="zip" name="zip" max-length="6" value="<?php echo e($common->zip); ?>" placeholder="Zip">
                                             <?php if($errors->has('zip')): ?>
                                             <span class="help-block">
                                             <strong><?php echo e($errors->first('zip')); ?></strong>
                                             </span>
                                             <?php endif; ?>
                                          </div>
                                       </div>
                                    </div>
                                    </div>
                                 </div>
 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                       <label class="col-lg-3 form-label">Business Tele #  : <span class="star-required">*</span></label>
                                   
                                    <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                       <div class="row">
                                    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                       <input type="text" class="form-control fsc-input bfh-phone" data-country="countries_states1" placeholder="(999) 999-9999" id="" value="<?php echo e($common->business_no); ?>" name="business_no">
                                    </div>
                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                       <select class="form-control fsc-input" id="businesstype" name="businesstype" data-bv-field="telephone1type">
<option>Type</option>
<option value="Office" <?php if($common->businesstype=='Office'): ?>selected <?php endif; ?>>Office</option>
<option value="Mobile" <?php if($common->businesstype=='Mobile'): ?>selected <?php endif; ?>>Mobile</option>
<option value="Resid" <?php if($common->businesstype=='Resid'): ?>selected <?php endif; ?>>Res</option>
</select>
                                    </div>
                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                      <input  <?php if($common->businessext == "Office"): ?> <?php else: ?> readonly <?php endif; ?> class="form-control fsc-input" maxlength="5" id="businessext" name="businessext" placeholder="Ext" " type="text" value="<?php echo e($common->businessext); ?>">
                                    </div>
                                 </div>  </div>  </div>
                                 </div>
                                
                                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 <?php echo e($errors->has('business_fax') ? 'has-error' : ''); ?>">
                                    <div class="form-group">
                                       <label class="col-lg-3 form-label">Business Fax # : <span class="star-required1">*</span></label>
                                    
                                    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                       <div class="row">
<div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                       <input type="text" class="form-control fsc-input bfh-phone" data-country="countries_states1" placeholder="(999) 999-9999" value="<?php echo e($common->business_fax); ?>" name="business_fax">
                                      
                                    </div></div>
                                 </div></div>
                                 </div>
                                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 <?php echo e($errors->has('website') ? 'has-error' : ''); ?>">
                                    <div class="form-group">
                                       <label class="col-lg-3 form-label">Website : <span class="star-required1">*</span></label>
                                   
                                     <div class="col-lg-6 row col-md-12 col-sm-12 col-xs-12 fsc-element-margin"><div class="col-lg-10 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                       <input type="text" class="form-control fsc-input" id="website" value="<?php echo e($common->website); ?>" name="website" placeholder="Website address">
                                      </div>
                                    </div></div></div>
                                 
<div class="row Branch">
					<h1>Contact Person Information</h1>
				</div>
 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" <?php echo e($errors->has('middle_name') ? 'has-error' : ''); ?> <?php echo e($errors->has('first_name') ? 'has-error' : ''); ?><?php echo e($errors->has('last_name') ? 'has-error' : ''); ?>>
                                    <div class="form-group">
                                       <label class="col-lg-3 form-label">Name : <span class="star-required">*</span></label>
                                   
                                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
														<select class="form-control fsc-input" id="nametype" name="nametype">
														   
														    <option value="mr"  <?php if($common->nametype=='mr'): ?>selected <?php endif; ?>>Mr.</option>
														    <option value="mrs"  <?php if($common->nametype=='mrs'): ?>selected <?php endif; ?>>Mrs.</option>
														    <option value="miss"  <?php if($common->nametype=='miss'): ?>selected <?php endif; ?>>Miss.</option>
														</select>
														
													</div>
                                    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                       <input type="text" class="form-control txtOnly fsc-input txtOnly " id="first_name" name="first_name" placeholder="First Name" value="<?php echo e($common->first_name); ?>">
 <?php if($errors->has('first_name')): ?>
                                       <span class="help-block">
                                       <strong><?php echo e($errors->first('first_name')); ?></strong>
                                       </span>
                                       <?php endif; ?>
                                    </div>
                                    <div class="col-lg-1 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                       <input type="text" class="form-control txtOnly fsc-input txtOnly " maxlength="1" id="middle_name" name="middle_name" placeholder="Middle Name" value="<?php echo e($common->middle_name); ?>">
 <?php if($errors->has('middle_name')): ?>
                                       <span class="help-block">
                                       <strong><?php echo e($errors->first('middle_name')); ?></strong>
                                       </span>
                                       <?php endif; ?>
                                    </div>
                                    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                       <input type="text" class="form-control txtOnly fsc-input txtOnly " id="last_name" name="last_name" placeholder="Last Name" value="<?php echo e($common->last_name); ?>">
 <?php if($errors->has('last_name')): ?>
                                       <span class="help-block">
                                       <strong><?php echo e($errors->first('last_name')); ?></strong>
                                       </span>
                                       <?php endif; ?>
                                    </div>
                                 </div>
                                 </div>
 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                       <label class="col-lg-3 form-label">Telephone 1  : <span class="star-required">*</span></label>
                                 
                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                       <input type="text" class="form-control fsc-input bfh-phone" data-country="countries_states1" id="telephone1" name="telephone1" placeholder="(999) 999-9999" value="<?php echo e($common->etelephone1); ?>">
                                    </div>
                                    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                       <select class="form-control fsc-input" id="telephone1type" name="telephone1type" data-bv-field="telephone1type">
<option>Type</option>
<option value="Office" <?php if($common->eteletype1=='Office'): ?>selected <?php endif; ?>>Office</option>
<option value="Mobile" <?php if($common->eteletype1=='Mobile'): ?>selected <?php endif; ?>>Mobile</option>
<option value="Resid" <?php if($common->eteletype1=='Resid'): ?>selected <?php endif; ?>>Res</option>
</select>
                                    </div>
                                    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                      <input  <?php if($common->eteletype1=='Office'): ?> <?php else: ?> readonly="" <?php endif; ?> class="form-control fsc-input" maxlength="5" id="telephone1ext" name="telephone1ext" placeholder="Ext" " type="text" value="<?php echo e($common->eext1); ?>">
                                    </div>
                                 </div>
                                 </div>
 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                       <label class="col-lg-3 form-label">Telephone 2 : <span class="star-required1">*</span></label>
                                 
                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                       <input class="form-control fsc-input bfh-phone" data-country="countries_states1" id="telephone2" name="telephone2" placeholder="(999) 999-9999" type="text"  value="<?php echo e($common->etelephone2); ?>">
                                    </div>
                                    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                      <select class="form-control fsc-input" id="telephone2type" name="telephone2type" >
<option>Type</option>
<option value="Office" <?php if($common->eteletype2=='Office'): ?>selected <?php endif; ?>>Office</option>
<option value="Mobile" <?php if($common->eteletype2=='Mobile'): ?>selected <?php endif; ?>>Mobile</option>
<option value="Resid" <?php if($common->eteletype2=='Resid'): ?>selected <?php endif; ?>>Res</option>
</select>
                                    </div>
                                    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                     <input type="text" class="form-control fsc-input" maxlength="5" id="telephone2ext" name="telephone2ext" placeholder="Ext"  value="<?php echo e($common->eext2); ?>" <?php if($common->eteletype2=='Office'): ?> <?php else: ?> readonly="" <?php endif; ?>>
                                    </div>
                                 </div>
                                 </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 <?php echo e($errors->has('email') ? 'has-error' : ''); ?>">
                                    <div class="form-group">
                                       <label class="col-lg-3 form-label">Email : <span class="star-required">*</span></label>
                                   
                                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
<div class="row">
<div class="col-lg-10 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                       <input type="email" class="form-control  fsc-input" readonly id="email" name='email' placeholder="abc@abc.com" value="<?php echo e($common->email); ?>">
                                       <?php if($errors->has('email')): ?>
                                       <span class="help-block">
                                       <strong><?php echo e($errors->first('email')); ?></strong>
                                       </span>
                                       <?php endif; ?>
                                    </div>
                                 </div> 
                                 </div>
                                 </div>
                          </div>
                        </div>
                     </div>



 <div class="tab-pane fade" id="tab2primary">
<div class="form-section">
                           <div class="">
                              <div class="row">
                                 <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                                    <div class="title-form-group">
                                       <h4>Bulletin</h4>
                                    </div>
                                 </div>
                                 <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                                    <div class="title-form-group">
                                       <h4>Sales Tax Reporting</h4>
                                       <p>Bulletin</p>
                                    </div>
                                 </div>
                                 <div class="col-lg-4 col-md-4 col-sm-3 col-xs-12">
                                    <div class="title-form-group">
                                       <br>
                                       <br>
                                       <p>Daily</p>
                                       <p>Monthly</p>
                                       <!--<p>Monthly</p>-->
                                       <p>Comparion by Week/Month/Year</p>
                                       <p>Notification of Sales Tax Payment</p>
                                       <p>Any time pulled out history for your sales report</p>
                                    </div>
                                 </div>
                                 <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                                    <div class="title-form-group">
                                       <h4>Check Mark</h4>

<?php 
$aColors = array("Daily", "Monthly", "Comparion by Week/Month/Year", "Notification of Sales Tax Payment", "Any time pulled out history for your sales report");
$dbcolors= explode(',', $common->bulletin);
?>
<?php $__currentLoopData = $aColors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $info): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<?php if(in_array($info,$dbcolors)): ?>
<input  type="checkbox" value="<?php echo e($info); ?>" name="bulletin[]" checked><Label for="checkBox"></Label><br>
<?php else: ?>
<input  type="checkbox" value="<?php echo e($info); ?>" name="bulletin[]" > <Label for="checkBox1"></Label><br>
<?php endif; ?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                  
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                                    <div class="title-form-group">
                                    </div>
                                 </div>
                                 <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                                    <div class="title-form-group">
                                       <h4>Employee Management</h4>
                                    </div>
                                 </div>
                                 <div class="col-lg-4 col-md-4 col-sm-3 col-xs-12">
                                    <div class="title-form-group">
                                       <br>
                                       <br>
                                       <p>Schedule</p>
                                       <p>Clock In/Out</p>
                                       <p>Their Work Report</p>
                                       <p>Reporting Hrs. for Payroll</p>
                                    </div>
                                 </div>
                                 <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                                    <div class="title-form-group">
                                       <h4>Check Mark</h4>
<?php 
$aColors1 = array("Schedule", "Clock In/Out", "Their Work Report", "Reporting Hrs. for Payroll");
$dbcolors1= explode(',', $common->employee);
?>
<?php $__currentLoopData = $aColors1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $info1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<?php if(in_array($info1,$dbcolors1)): ?>
    <input id="checkBox2" type="checkbox" value="<?php echo e($info1); ?>" name="employee[]" checked><label for="checkBox2"></label><br>
<?php else: ?>
    <input id="checkBox3" type="checkbox" value="<?php echo e($info1); ?>" name="employee[]"><label for="checkBox3"></label><br>
<?php endif; ?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                                    <div class="title-form-group">
                                    </div>
                                 </div>
                                 <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                                    <div class="title-form-group">
                                       <h4>Taxation</h4>
                                    </div>
                                 </div>
                                 <div class="col-lg-4 col-md-4 col-sm-3 col-xs-12">
                                    <div class="title-form-group">
                                    </div>
                                 </div>
                                 <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                                    <div class="title-form-group">
                                       <br>
                                       <input id="checkBox4" type="checkbox" value="Taxation"  <?php if($common->taxation=='Taxation'): ?> checked <?php endif; ?> name="taxation"><label for="checkBox4"></label>
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                                    <div class="title-form-group">
                                    </div>
                                 </div>
                                 <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                                    <div class="title-form-group">
                                       <h4>Due Dates</h4>
                                    </div>
                                 </div>
                                 <div class="col-lg-4 col-md-4 col-sm-3 col-xs-12">
                                    <div class="title-form-group">
                                       <br>
                                       <br>
                                       <p>Inform about due dates for the following</p>
                                       <p>Secertary of State annual Renewal</p>
                                       <p>Tobacco License</p>
                                    </div>
                                 </div>
                                 <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                                    <div class="title-form-group">
                                       <h4>Check Mark</h4>


<?php 
$aColors2 = array("Inform about due dates for the following", "Secertary of State annual Renewal", "Tobacco License");
$dbcolors2= explode(',', $common->due_date1);
?>
<?php $__currentLoopData = $aColors2; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $info2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<?php if(in_array($info2,$dbcolors2)): ?>
    <input id="checkBox5" type="checkbox" value="<?php echo e($info2); ?>" name="due_date[]" checked><label for="checkBox5"></label><br>
<?php else: ?>
    <input id="checkBox6" type="checkbox" value="<?php echo e($info2); ?>" name="due_date[]"><label for="checkBox6"></label><br>
<?php endif; ?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
           
                                    </div>
                                 </div>


                              </div>
                           </div>
<div class="form-group">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:2%;">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-row">		
                                 <input id="checkBox7" type="checkbox" <?php if($common->terms=='Terms and conditions and agreement'): ?> checked <?php endif; ?>  value="Terms and conditions and agreement" name="terms"><label for="terms" style="font-size: 14px;"> Terms and conditions and agreement</label>
                              </div>
                             
                           </div>
                        </div>
                        </div>

</div>					
					





<div class="tab-pane fade" id="tab3primary">

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div class="row Branch">
<h1>Price and Payment Info</h1>
				</div>
                        </div>
                       
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                       <label class="col-lg-3 form-label">Monthly Fee : </label>
                             
                              <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                 <input type="text" class="form-control fsc-input num" onkeypress="return isNumberKey(event)" id="monthly_fee" name="monthly_fee" placeholder="Monthly Fee" value="<?php echo e($common->monthly_fee); ?>">
                              </div>
                           </div>
                        </div>
                        
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="form-group">
                                       <label class="col-lg-3 form-label">Yearly Fee :</label>
                             
                              <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                 <input type="text" class="form-control fsc-input num" onkeypress="return isNumberKey(event)" placeholder="Yearly Fee" name="yearly_fee" id="yearly_fee"  value="<?php echo e($common->yearly_fee); ?>" onblur="getTotal()">
                              </div>
                           </div>
                        </div>
                    
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="form-group">
                                       <label class="col-lg-3 form-label">Amount : </label>
                             
                              <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                 <input type="text" placeholder="Amount" onkeypress="return isNumberKey(event)" readonly class="form-control num fsc-input" name="amount" id="amount" value="<?php echo e($common->amount); ?>"> 
                                 
                              </div>
                           </div>
                        </div>
                       
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="form-group">
                                       <label class="col-lg-3 form-label">Credit Card No : <span class="star-required">*</span></label>
                            
                              <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                 <input type="text" class="form-control fsc-input num" id="credit_card" name="credit_card" placeholder="Credit Card No" value="<?php echo e($common->credit_card); ?>">
                               
                              </div>
                           </div>
                        </div>
                        <div id="test">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="form-group">
                                       <label class="col-lg-3 form-label">Expire : </label>
                           
                              <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                 <div class="row">
                                    <div class="col-md-12">
                                       <input type="text" class="form-control fsc-input" id="expire" name="expire" placeholder="Expire" value="<?php echo e($common->expire); ?>">
                                    </div>
                                   </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                      
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="form-group">
                                       <label class="col-lg-3 form-label">Security Code : </label>
                              
                              <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                 <div class="row">
                                    <div class="col-md-12">
                                       <input type="text" class="form-control fsc-input" id="Security Code" name="security_code" placeholder="Security Code" value="<?php echo e($common->security_code); ?>">
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="form-group">
                                       <label class="col-lg-3 form-label">Credit Card holder Name : <span class="star-required">*</span></label>
                           
                              <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                 <input type="text" class="form-control fsc-input textonly" id="credit_card_holder_name" name="credit_card_holder_name" placeholder="Credit Card holder Name" value="<?php echo e($common->credit_card_holder_name); ?>">
                              
                              </div>
                           </div>
                        </div>
                        
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="form-group">
                                       <label class="col-lg-3 form-label">Address : <span class="star-required">*</span></label>
                         
                              <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                 <input type="text" class="form-control fsc-input" id="credit_address" name="credit_address" placeholder="Address" value="<?php echo e($common->credit_address); ?>">
                                
                              </div>
                           </div>
                        </div>
                     
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="form-group">
                                       <label class="col-lg-3 form-label">City / State / Zip : <span class="star-required">*</span></label>
                             
                              <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                 <input type="text" class="form-control fsc-input textonly" id="credit_city" name="credit_city" placeholder="City" value="<?php echo e($common->credit_city); ?>">
                                
                              </div>
                              <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                 <select name="credit_state" id="credit_state" class="form-control fsc-input" >

<option value="<?php echo e($common->credit_state); ?>"><?php echo e($common->credit_state); ?></option>                                    <option value="ID">ID</option>

                                    <option value="AK">AK</option>
                                    <option value="AS">AS</option>
                                    <option value="AZ">AZ</option>
                                    <option value="AR">AR</option>
                                    <option value="CA">CA</option>
                                    <option value="CO">CO</option>
                                    <option value="CT">CT</option>
                                    <option value="DE">DE</option>
                                    <option value="DC">DC</option>
                                    <option value="FM">FM</option>
                                    <option value="FL">FL</option>
                                    <option value="GA">GA</option>
                                    <option value="GU">GU</option>
                                    <option value="HI">HI</option>
                                    <option value="ID">ID</option>
                                    <option value="IL">IL</option>
                                    <option value="IN">IN</option>
                                    <option value="IA">IA</option>
                                    <option value="KS">KS</option>
                                    <option value="KY">KY</option>
                                    <option value="LA">LA</option>
                                    <option value="ME">ME</option>
                                    <option value="MH">MH</option>
                                    <option value="MD">MD</option>
                                    <option value="MA">MA</option>
                                    <option value="MI">MI</option>
                                    <option value="MN">MN</option>
                                    <option value="MS">MS</option>
                                    <option value="MO">MO</option>
                                    <option value="MT">MT</option>
                                    <option value="NE">NE</option>
                                    <option value="NV">NV</option>
                                    <option value="NH">NH</option>
                                    <option value="NJ">NJ</option>
                                    <option value="NM">NM</option>
                                    <option value="NY">NY</option>
                                    <option value="NC">NC</option>
                                    <option value="ND">ND</option>
                                    <option value="MP">MP</option>
                                    <option value="OH">OH</option>
                                    <option value="OK">OK</option>
                                    <option value="OR">OR</option>
                                    <option value="PW">PW</option>
                                    <option value="PA">PA</option>
                                    <option value="PR">PR</option>
                                    <option value="RI">RI</option>
                                    <option value="SC">SC</option>
                                    <option value="SD">SD</option>
                                    <option value="TN">TN</option>
                                    <option value="TX">TX</option>
                                    <option value="UT">UT</option>
                                    <option value="VT">VT</option>
                                    <option value="VI">VI</option>
                                    <option value="VA">VA</option>
                                    <option value="WA">WA</option>
                                    <option value="WV">WV</option>
                                    <option value="WI">WI</option>
                                    <option value="WY">WY</option>
                                 </select>
                                
                              </div>
                              <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                 <input type="text" class="form-control fsc-input num" maxlength="5"  id="credit_zip" name="credit_zip" placeholder="Postel Code" value="<?php echo e($common->credit_zip); ?>" >
                                
                              </div>
                           </div>
                        </div>
                       
                  </div>
<div class="row Branch">
					<h1>For Office Use Only</h1>
				</div><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 <?php echo e($errors->has('client_id') ? ' has-error' : ''); ?>">
                              <div class="form-group">
                                       <label class="col-lg-3 form-label">Client Id : <span class="star-required">*</span></label>
                               
                                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                       <input type="text" class="form-control fsc-input" onkeyup="checkemail();" value="<?php echo e($common->filename); ?>" placeholder="Please Enter Client Id" name="client_id" id="client_id" value="">
                                       <?php if($errors->has('client_id')): ?>
                                       <span class="help-block">
                                       <strong><?php echo e($errors->first('client_id')); ?></strong>
                                       </span>
                                       <?php endif; ?>
<div id="email_status"></div></div>
                                    </div>
                                 </div>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 <?php echo e($errors->has('status') ? ' has-error' : ''); ?>">
                              <div class="form-group">
                                       <label class="col-lg-3 form-label">Status : <span class="star-required">*</span></label>
                                    
                                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
<div class="row">
<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
<select name="status"  onchange="showDiv1(this)" id="status" class="form-control fsc-input" <?php if($common->status == 'Approval'): ?> style="background:yellow"  <?php endif; ?> 
<?php if($common->status == 'Pending'): ?> style="background:orange" <?php endif; ?>
 <?php if($common->status == 'Hold'): ?> style="background:red" <?php endif; ?>
 <?php if($common->status == 'Inactive'): ?> style="background:blue" <?php endif; ?>  
<?php if($common->status == 'Active'): ?> style="background:green"  <?php endif; ?> >
<option value="">Status</option> 
                                         
                                         <option value="Hold" <?php if($common->status == 'Hold'): ?> selected <?php endif; ?> class="Red">Hold</option> 
                                         <option  class="Orange" value="Pending" <?php if($common->status == 'Pending'): ?> selected <?php endif; ?>>Pending</option>
<option value="Approval" <?php if($common->status == 'Approval'): ?> selected <?php endif; ?> class="Yellow"  >Approve</option> 
<option value="Active" class="Green"  <?php if($common->status == 'Active'): ?> selected <?php endif; ?>>Active</option> 
<option value="Inactive" class="Blue" <?php if($common->status == 'Inactive'): ?> selected <?php endif; ?> style="background:blue;color:#fff">Inactive</option> 
                                       </select>
                                    <?php if($errors->has('status')): ?>
                                       <span class="help-block">
                                       <strong><?php echo e($errors->first('status')); ?></strong>
                                       </span>
                                       <?php endif; ?>
                                    </div>
                                 </div></div>
                                 </div>
                                   </div></div>
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:3%;">
                       <!-- <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12"></div>-->
                        <div class="col-lg-6 col-lg-offset-3 col-md-12 col-sm-12 col-xs-12">
<?php if($common->status == 'Hold' or $common->status == 'Pending'): ?>
<button type="submit" class="btn btn-primary fsc-form-submit" id="bt1">Hold</button>
<style>
.bt1,.bt{display:none}
</style>
<?php endif; ?>
<?php if($common->status == 'Approve'): ?>
<button type="submit" class="btn btn-primary fsc-form-submit bt" id="bt">Convert to client</button>
<style>
.bt1,.bt{display:none}
</style>
<?php endif; ?>
<button type="submit" class="btn btn-primary fsc-form-submit bt" id="bt1">Hold</button>
<button type="submit" class="btn btn-primary fsc-form-submit bt1" id="bt">Convert to client</button>

                            
                          <a href="<?php echo e(url('/fac-Bhavesh-0554/customerconvert')); ?>" class="btn btn-primary fsc-form-submit">Cancel</a>
                        </div>
<!--                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12"></div>-->
                     </div>
                  
               </div>
            </div>
            </form>
         </div>
      </div>
   </div>
</div>
</div>
</section>
</div>
<!-- copy of input fields group -->


<script>
//$("#business_no").mask("(999) 999-9999");
$(".ext").mask("999");
///$("#business_fax").mask("(999) 999-9999");
///$(".business_fax1").mask("(999) 999-9999");
///$(".residence_fax").mask("(999) 999-9999");
//$("#mobile_no").mask("(999) 999-9999");
//$(".cell").mask("(999) 999-9999");
//$(".telephone").mask("(999) 999-9999");
//$(".usapfax").mask("(999) 999-9999");
//$("#zip").mask("99999");
//$("#mailing_zip").mask("99999");
//$("#bussiness_zip").mask("99999");
</script>

<script type="text/javascript">
function checkemail()
{
 var client_id=document.getElementById("client_id").value;
	//alert(client_id);
 if(client_id)
 {
  $.ajax({
  type: 'get',
  url: '<?php echo URL::to('/getclick'); ?>',
  data: {
   client_id:client_id,
  },
  success: function (response) {
   $( '#email_status').html(response);
   if(response=="OK")	
   {
    return true;	
   }
   else
   {
    return false;	
   }
  }
  });
 }
 else
 {
  $( '#email_status' ).html("");
  return false;
 }
}

function checkall()
{
 var emailhtml=document.getElementById("email_status").innerHTML;

 if((emailhtml)=="OK")
 {
  return true;
 }
 else
 {
  return false;
 }
}

</script>
<script>
   $(document).ready(function(){
       //group add limit
       var maxGroup = 3;
       
       //add more fields group
       $(".addMore").click(function(){
           if($('body').find('.fieldGroup').length < maxGroup){
               var fieldHTML = '<div class="form-group fieldGroup">'+$(".fieldGroupCopy").html()+'</div>';
               $('body').find('.fieldGroup:last').after(fieldHTML);
           }else{
               alert('Maximum '+maxGroup+' Persons are allowed.');
           }
       });
       
       //remove fields group
       $("body").on("click",".remove",function(){ 
           $(this).parents(".fieldGroup").remove();
       });
   });
</script>
<script>
   function FillBilling(f) {
   if(f.billingtoo.checked == true) {
    f.mailing_address.value = f.address.value;
    f.mailing_address1.value = f.address1.value;
   }
   }
</script>
<script>
   function showDiv(elem){ 
      if(elem.value == 'Federal'){
         document.getElementById('hidden_div').style.display = "none";
   }
   else
   {
   document.getElementById('hidden_div').style.display = "block";
   }
   }
</script>
<script>
   $(document).ready(function(){
   	$(document).on('change','.category', function()
   	{
   		//console.log('htm');
   		var id = $(this).val();
   		$.get('<?php echo URL::to('getRequest'); ?>?id='+id, function(data)
   		{  $('#business_cat_id').empty();
              $.each(data, function(index, subcatobj)
   		   {
   			   $('#business_cat_id').append('<option value="'+subcatobj.id+'">'+subcatobj.business_cat_name+'</option>');
   		   })
   
   		});
   			
   	});
   });
</script>
<script>
var dat1 = $('#telephone1ext').val();
$('#telephone1type').on('change', function() {

if(this.value=='Office')
{
document.getElementById('telephone1ext').removeAttribute('readonly');
$('#telephone1ext').val(<?php echo e($common->eext1); ?>); 
}
else
{
document.getElementById('telephone1ext').readOnly =true;
$('#telephone1ext').val('');
}
})
</script>
<script>
var dat1 = $('#telephone2ext').val();
$('#telephone2type').on('change', function() {

if(this.value=='Office')
{
document.getElementById('telephone2ext').removeAttribute('readonly');
$('#telephone2ext').val(<?php echo e($common->eext2); ?>); 
}
else
{
document.getElementById('telephone2ext').readOnly =true;
$('#telephone2ext').val('');
}
})
</script>



<script>
   $(document).ready(function(){
   	$(document).on('change','.category', function()
   	{
   		//console.log('htm');
   		var id = $(this).val();
   		$.get('<?php echo URL::to('getcat'); ?>?id='+id, function(data)
   		{  $('#user_type').empty();
              $.each(data, function(index, subcatobj)
   		   {
   			   $('#user_type').val(subcatobj.bussiness_name);
   		   })
   
   		});
   			
   	});
   });
</script>

<!--<script>
   function showDiv(elem){ 
      if(elem.value == 'Approval'){
         document.getElementById('bt1').style.display = "none";
        document.getElementById('bt').style.display = "block";
   }
   else if(elem.value == 'Hold' || elem.value == 'Pending')
   {
 
   }
   }
</script>-->

<script>
jQuery.fn.getNum = function() {
    var val = $.trim($(this).val());
    if(val.indexOf(',') > -1) {
        val = val.replace(',', '.');
    }
    var num = parseFloat(val);
    var num = num.toFixed(2);
    if(isNaN(num)) {
        num = '';
    }
    return num;
}

$(function() {

    $('#monthly_fee,#yearly_fee,#amount').blur(function() {
        $(this).val($(this).getNum());
    });

});
</script>
<script>
$('input').blur(function(){
   $('input[name=amount]').val( +($('input[name=yearly_fee]').val()) +(+ $('input[name=monthly_fee]').val()) );
});

</script>
<script>
/*$('#client_id').mask('aa-999-9999999');
$('input[name="client_id"]').focusout(function() {
   $('input[name="client_id"]').val( this.value.toUpperCase() );
});
*/


   function showDiv1(elem){ 
      if(elem.value == 'Hold'){
         $('#status').css("background", "red");
  document.getElementById('bt').style.display = "none";
document.getElementById('bt1').style.display = "block";
   }
 else if(elem.value == 'Pending'){
         $('#status').css("background", "orange");
  document.getElementById('bt').style.display = "none";
document.getElementById('bt1').style.display = "block";
   }
 else if(elem.value == 'Approval'){
         $('#status').css("background", "yellow");
   document.getElementById('bt1').style.display = "none";
        document.getElementById('bt').style.display = "block";
   }
 else if(elem.value == 'Active'){
         $('#status').css("background", "green");
   }
else if(elem.value == 'Inactive'){
         $('#status').css("background", "blue");
   }
   else
   {
 
   }
   }
</script>
<script>
   $(document).ready(function(){
   $("#credit_zip").keyup(function() {
   		//console.log('htm');
   		var id = $(this).val();
   		$.get('<?php echo URL::to('/getzip'); ?>?zip='+id, function(data)
   		{ 
  $('#credit_city').empty();
 $('#credit_state').empty();//$('#countryId').empty();
              $.each(data, function(index, subcatobj)
   		  {$('#city').removeAttr("disabled"); $('#stateId').removeAttr("disabled"); 
$('#credit_city').val(subcatobj.city);
$('#credit_state').append('<option value="'+subcatobj.state+'">'+subcatobj.state+'</option>');
 //$('#countryId').append('<option value="'+subcatobj.country+'">'+subcatobj.country+'</option>');
   		   })
   
   		});
   			
   	});
   });
   	$(document).ready(function(){
  /***phone number format***/
  $(".phone").keypress(function (e) {
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
      return false;
    }
    var curchr = this.value.length;
    var curval = $(this).val();
    if (curchr == 3 && curval.indexOf("(") <= -1) {
      $(this).val("(" + curval + ")" + " ");
    } else if (curchr == 4 && curval.indexOf("(") > -1) {
      $(this).val(curval + ")-");
    } else if (curchr == 5 && curval.indexOf(")") > -1) {
      $(this).val(curval + "-");
    } else if (curchr == 9) {
      $(this).val(curval + "-");
      $(this).attr('maxlength', '14');
    }
  });
});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>