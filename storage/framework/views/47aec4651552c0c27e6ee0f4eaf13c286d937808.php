<?php $__env->startSection('title', 'Profile'); ?>
<?php $__env->startSection('main-content'); ?>
<style>
    .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover{ margin-right:0; }
    .nav-tabs > li > a{ color:#444;  border-radius: 5px; margin-right:0;  }
   .nav-tabs > li {width: 210px;margin: auto;}
   .rules-one {font-size: 16px;line-height: 30px;} 
   .nav-tabs>li>a {height: 45px;}
   .btn-center {margin: auto;width: 250px;}
   .Branch {width: 100%;margin: 0px 0 20px 0;text-align: center;background: #428bca;border-top-left-radius: 5px;border-top-right-radius: 5px;padding: 5px;}
   .btn3d.btn-info:hover, .btn3d.btn-info.active {background: linear-gradient(#74b7fd, #e7f3ff);transition: 0.2s;}
   .btn3d.btn-info {background: linear-gradient(#e7f3ff, #74b7fd);font-size: 11px;padding: 9px 10px;color: #000000;font-weight: 600;border: 1px solid #000;width: 100%;transition: 0.2s;text-align: center;box-shadow: 0 4px 5px 0 rgba(0,0,0,0.14), 0 1px 10px 0 rgba(0,0,0,0.12), 0 2px 4px -1px rgba(0,0,0,0.3);-webkit-box-shadow: 0 4px 5px 0 rgba(0,0,0,0.14), 0 1px 10px 0 rgba(0,0,0,0.12), 0 2px 4px -1px rgba(0,0,0,0.3);}
   .btn-info:hover {color: #fff;background-color: #0e8df2;}
   .Branch h1{padding: 0px;margin: 0px 0 0 0;color: #fff;font-size: 24px;}
   label{float:right}
   .im{text-decoration: none;position: absolute;top: 21px;margin: -15px auto auto auto;color: #fff;padding: 4px;float: left;}
   .remove {display: block;padding: 6px 16px;margin: -48px 0 0 0;position: relative;right: 110px;top: 0;}
   .addMore {position: absolute;right: 151px;top: 80px;}
   label.file-upload {position: relative;overflow: hidden;float: left;}
   input[type="file"] {position: absolute;top: 0;right: 0;min-width: 100%;min-height: 100%;font-size: 100px;text-align: right;filter: alpha(opacity=0);opacity: 0;outline: none;background: white;cursor: inherit;display: block;}
   .form-control1 {width: 100%;line-height: 1.44;color: #555 !important;border: 2px solid #286db5;border-radius: 3px;transition: border-color ease-in-out .15s;padding: 3px 3px 7px 8px!important;}
   .Red{background-color:red;color:#fff}
   .Blue{background-color:rgb(124, 124, 255) !important;color:#fff}
   .Green{background-color:#00ef00 !important;color:#fff}
   .Yellow{background-color:Yellow !important;}
   .Orange{background-color:Orange !important;color:#fff}
   .model-width{margin: auto; width: 485px;border: #3668f6 1px solid;border-radius: 0;}
   .model-width .modal-header {padding: 4px 10px; background: #fff;}
   .model-width .modal-header h4{font-size:16px;}
   .model-width .modal-footer{text-align:center;}
   th{text-align:center !important;}
   .primary {background-color: #d0d0d0 !important;border-radius: 0px;color: #000;padding: 5px 10px;width: 93px;font-weight: 200px;}
   .form-control {font-size: 16px !important;font-weight: 500;}
   .nav-tabs > li {width: 19.1% !important;margin: 2px 0 0 8px;border: 1px solid #ccc;background: #fff;border-radius: 8px;/*margin: 5px 5px 5px 6px !important;*/}
   .langcheckbox label{float:none!important;}
   .cke_bottom {
    display: none;
    }
    .panel-body {
        padding: 6px 0px;
    }
    #tab10primary .dataTables_filter,#tab10primary .dt-buttons{
        display:none !important;
    }
</style>
<?php if($emp->type=='user'): ?>
<style>
    .tttt{background: #ededed !important;
	    
	}
	.tttt1{display:none !important;}
</style>
<?php endif; ?>
<div class="content-wrapper">
   <div class="page-title" style="height: 42px;">
      <div class="col-md-4"></div>
      <div class="col-md-4">
         <h1><?php echo e(ucfirst($emp->firstName)); ?> <?php echo e(ucfirst($emp->lastName)); ?></h1>
      </div>
      <div class="col-md-4">
         <h1 class="pull-right">FSC <?php echo e(ucfirst($emp->type)); ?></h1>
      </div>
   </div>
    <section class="content">
   
   <div class="row">
      <div class="col-md-12">
         <div class="card">
            <div class="card-body">
               <div class="panel with-nav-tabs panel-primary">
                  <div class="panel-heading">
                     <?php if($_GET['tab']==1){?>      
                     <script>
                        $(function () {
                        $('#myTab a:eq(8)').tab('show');
                        
                        })
                     </script>
                     <?php } else{ ?>
                     <script>
                        $(function () {
                        $('#myTab a:first').tab('show');
                        })
                     </script>
                     <?php }?>
                     <ul class="nav nav-tabs" id="myTab" style="padding:5px !important;">
                        <li class="active rules1" ><a href="#tab1primary" data-toggle="tab">General Info </a></li>
                        <li><a data-toggle="tab" href="#tab2primary" class="hvr-shutter-in-horizontal">Hiring Info</a></li>
                        <li><a href="#tab3primary" data-toggle="tab" class="hvr-shutter-in-horizontal">Pay Info</a></li>
                        <li><a href="#tab4primary" data-toggle="tab" class="hvr-shutter-in-horizontal">Personal Info</a></li>
                        <li><a href="#tab5primary" data-toggle="tab" class="hvr-shutter-in-horizontal">Security Info</a></li>
                        <li><a href="#tab7primary" data-toggle="tab" class="hvr-shutter-in-horizontal">User Rights Info</a></li>
                        <li><a href="#tab6primary" data-toggle="tab" class="hvr-shutter-in-horizontal">Other Info</a></li>
                        <li class="rules"><a href="#tab8primary" data-toggle="tab" class="hvr-shutter-in-horizontal">Rule</a></li>
                        <li><a href="#tab9primary" data-toggle="tab" class="hvr-shutter-in-horizontal">Responsibility</a></li>
                        <li><a href="#tab10primary" data-toggle="tab" class="hvr-shutter-in-horizontal">Other</a></li>
                     </ul>
                  </div>
                  <form method="post" action="<?php echo e(route('employeeprofile.update',Auth::user()->user_id)); ?>" id="registrationForm" class="form-horizontal"  enctype="multipart/form-data">
                     <?php echo e(csrf_field()); ?><?php echo e(method_field('PATCH')); ?>

                     <input type="hidden" class="form-control" id="text1" name="text1" value="">
                     <div class="panel-body">
                        <div class="tab-content">
                           <div class="tab-pane fade in active" id="tab1primary">
                              <div class="col-md-12">
                                 <div class="Branch">
                                    <h1>General Information</h1>
                                 </div>
                                 <div class="form-group <?php echo e($errors->has('employee_id') ? ' has-error' : ''); ?>">
                                    <label class="control-label col-md-3">Employee ID :</label>
                                    <div class="col-md-6">
                                       <div class="row">
                                          <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                             <input type="text" class="form-control fsc-input" name="employee_id" id="employee_id" value="<?php echo e($emp->employee_id); ?>"> <?php if($errors->has('employee_id')): ?>
                                             <span class="help-block">
                                             <strong><?php echo e($errors->first('employee_id')); ?></strong>
                                             </span>
                                             <?php endif; ?>
                                             <?php if($emp->check=='0'): ?>
                                             <input name="password1" value="<?php echo mt_rand();?>" class="form-control fsc-input" id="password1" readonly="" type="hidden">
                                             <?php endif; ?>
                                          </div>
                                          <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                             <label class="control-label">Status : </label>
                                          </div>
                                          <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                <input style="pointer-events:none" type="text" <?php if($emp->check=='1'): ?> class="form-control fsc-input Green"  <?php endif; ?>  
                                                 <?php if($emp->check=='0'): ?> class="form-control fsc-input Blue" <?php endif; ?>  id="check" name="check" id="check" value="<?php if($emp->check=='1'): ?> Active <?php endif; ?> <?php if($emp->check=='0'): ?> In-Active <?php endif; ?>"> 
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="form-group <?php echo e($errors->has('firstName') ? ' has-error' : ''); ?><?php echo e($errors->has('lastName') ? ' has-error' : ''); ?>">
                                    <label class="control-label col-md-3">Name :</label>
                                    <div class="col-md-6">
                                       <div class="row">
                                            <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
														<select class="form-control fsc-input" id="nametype" name="nametype">
														    <option value="mr"  <?php if($emp->nametype=='mr'): ?> selected <?php endif; ?>>Mr.</option>
														    <option value="mrs" <?php if($emp->nametype=='mrs'): ?> selected <?php endif; ?>>Mrs.</option>
														    <option value="miss" <?php if($emp->nametype=='miss'): ?> selected <?php endif; ?>>Miss.</option>
														</select>
													</div>
                                          <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                             <input type="text" class="form-control fsc-input" id="firstName" name="firstName" placeholder="First" value="<?php echo e($emp->firstName); ?>"><?php if($errors->has('firstName')): ?>
                                             <span class="help-block">
                                             <strong><?php echo e($errors->first('firstName')); ?></strong>
                                             </span>
                                             <?php endif; ?>
                                          </div>
                                          <div class="col-lg-1 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                              <div class="row">
                                             <input type="text" maxlength="1" class="form-control fsc-input " id="middleName" name="middleName" placeholder="M" value="<?php echo e($emp->middleName); ?>">
                                           </div>
                                            
                                          </div>
                                          <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                             <input type="text" class="form-control fsc-input " id="lastName" name="lastName" value="<?php echo e($emp->lastName); ?>" placeholder="Last">
                                             <input type="hidden" class="form-control fsc-input  " id="type" name="type" value="<?php echo e($emp->type); ?>" placeholder="Last">
                                             <?php if($errors->has('lastName')): ?>
                                             <span class="help-block">
                                             <strong><?php echo e($errors->first('lastName')); ?></strong>
                                             </span>
                                             <?php endif; ?>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="form-group <?php echo e($errors->has('address1') ? ' has-error' : ''); ?>">
                                    <label class="control-label col-md-3">Address 1 : </label>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-11">
                                                <input type="text" placeholder="Address 1" class="form-control fsc-input" name="address1" id="address1" value="<?php echo e($emp->address1); ?>"><?php if($errors->has('address1')): ?>
                                                <span class="help-block">
                                                    <strong><?php echo e($errors->first('address1')); ?></strong>
                                                </span>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label class="control-label col-md-3">Address 2 :</label>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-11">
                                                <input type="text"placeholder="Address 2" class="form-control fsc-input" name="address2" id="address2" value="<?php echo e($emp->address2); ?>">
                                            </div>
                                        </div>
                                    </div>
                                 </div>
                                     <div class="form-group <?php echo e($errors->has('countryId') ? ' has-error' : ''); ?>">
                                    <label class="control-label col-md-3">Country :</label>
                                    <div class="col-md-6">
                                       <div class="row">
                                          <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                             <div class="dropdown">
                                             <select name="countryId" id="countries_states1" class="form-control fsc-input bfh-countries" data-country="<?php echo e($emp->countryId); ?>" style='height:auto'></select>
                                              </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="form-group <?php echo e($errors->has('city') ? ' has-error' : ''); ?><?php echo e($errors->has('stateId') ? ' has-error' : ''); ?><?php echo e($errors->has('zip') ? ' has-error' : ''); ?> ">
                                    <label class="control-label col-md-3">City/State/Zip :</label>
                                    <div class="col-md-6">
                                       <div class="row">
                                          <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                             <input type="text" class="form-control fsc-input textonly" id="city" name="city" placeholder="City" value="<?php echo e($emp->city); ?>">
                                             <?php if($errors->has('city')): ?>
                                             <span class="help-block">
                                             <strong><?php echo e($errors->first('city')); ?></strong>
                                             </span>
                                             <?php endif; ?>
                                          </div>
                                          <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                             <div class="dropdown" style="margin-top: 1%;">
                                                <select name="stateId" id="stateId" class="form-control fsc-input bfh-states" data-country="countries_states1" data-state="<?php echo e($emp->stateId); ?>">
                                                 
                                                </select>
                                                <?php if($errors->has('stateId')): ?>
                                                <span class="help-block">
                                                <strong><?php echo e($errors->first('stateId')); ?></strong>
                                                </span>
                                                <?php endif; ?>
                                             </div>
                                          </div>
                                          <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                             <input type="text" class="form-control fsc-input zip" id="zip" name="zip" maxlength="6" value="<?php echo e($emp->zip); ?>" placeholder="Zip">
                                             <?php if($errors->has('zip')): ?>
                                             <span class="help-block">
                                             <strong><?php echo e($errors->first('zip')); ?></strong>
                                             </span>
                                             <?php endif; ?>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                             
                                 <div class="form-group <?php echo e($errors->has('telephoneNo1') ? ' has-error' : ''); ?> <?php echo e($errors->has('telephoneNo1Type') ? ' has-error' : ''); ?> ">
                                    <label class="control-label col-md-3">Telephone 1 :</label>
                                    <div class="col-md-6">
                                       <div class="row">
                                          <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                             <input type="text" class="form-control fsc-input bfh-phone" data-country="countries_states1"  id="telephoneNo1" name="telephoneNo1"  placeholder="(999) 999-9999" value="<?php echo e($emp->telephoneNo1); ?>"> <?php if($errors->has('telephoneNo1')): ?>
                                             <span class="help-block">
                                             <strong><?php echo e($errors->first('telephoneNo1')); ?></strong>
                                             </span>
                                             <?php endif; ?>
                                          </div>
                                          <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                             <div class="dropdown" style="margin-top: 1%;">
                                                <select name="telephoneNo1Type" id="telephoneNo1Type" class="form-control fsc-input">
                                                    <option value='Mobile' <?php if($emp->telephoneNo1Type=='Mobile'): ?> selected <?php endif; ?>>Mobile</option>
                                                    <option value='Home' <?php if($emp->telephoneNo1Type=='Home'): ?> selected <?php endif; ?>>Home</option>
                                                    <option value='Work' <?php if($emp->telephoneNo1Type=='Work'): ?> selected <?php endif; ?>>Work</option>
                                                    <option value='Office' <?php if($emp->telephoneNo1Type=='Office'): ?> selected <?php endif; ?>>Office</option>
                                                    <option value='Other' <?php if($emp->telephoneNo1Type=='Other'): ?> selected <?php endif; ?>>Other</option>
                                                <!--    
                                                <option value="Mobile" <?php if($emp->telephoneNo1Type=='Mobile'): ?> selected <?php endif; ?>>Work</option>
                                                <option value="Resident" <?php if($emp->telephoneNo1Type=='Resident'): ?> selected <?php endif; ?>>Resident</option>
                                                <option value="Office"  <?php if($emp->telephoneNo1Type=='Office'): ?> selected <?php endif; ?>>Cell</option>
                                                <option value="Other" <?php if($emp->telephoneNo1Type=='Other'): ?> selected <?php endif; ?>>Other</option>-->
                                                </select>
                                                <?php if($errors->has('telephoneNo1Type')): ?>
                                                <span class="help-block">
                                                <strong><?php echo e($errors->first('telephoneNo1Type')); ?></strong>
                                                </span>
                                                <?php endif; ?>
                                             </div>
                                          </div>
                                          <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                             <input type="text" class="form-control fsc-input" readonly id="ext1" maxlength="3" name="ext1" value="<?php echo e($emp->ext1); ?>" placeholder="Ext">
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="form-group <?php echo e($errors->has('telephoneNo2') ? ' has-error' : ''); ?> <?php echo e($errors->has('telephoneNo2Type') ? ' has-error' : ''); ?> ">
                                    <label class="control-label col-md-3">Telephone 2 :</label>
                                    <div class="col-md-6">
                                       <div class="row">
                                          <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                             <input type="text" class="form-control fsc-input bfh-phone" data-country="countries_states1" id="telephoneNo2" name="telephoneNo2"  placeholder="(9999) 999-9999"  value="<?php echo e($emp->telephoneNo2); ?>">
                                             <?php if($errors->has('telephoneNo2')): ?>
                                             <span class="help-block">
                                             <strong><?php echo e($errors->first('telephoneNo2')); ?></strong>
                                             </span>
                                             <?php endif; ?>
                                          </div>
                                          <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                             <div class="dropdown" style="margin-top: 1%;">
                                                <select name="telephoneNo2Type" id="telephoneNo2Type" class="form-control fsc-input">
                                                <!--<option value="Mobile" <?php if($emp->telephoneNo2Type=='Mobile'): ?> selected <?php endif; ?>>Work</option>
                                                <option value="Resident" <?php if($emp->telephoneNo2Type=='Resident'): ?> selected <?php endif; ?>>Resident</option>
                                                <option value="Office"  <?php if($emp->telephoneNo2Type=='Office'): ?> selected <?php endif; ?>>Cell</option>
                                                <option value="Other" <?php if($emp->telephoneNo2Type=='Other'): ?> selected <?php endif; ?>>Other</option>-->
                                                 <option value='Mobile' <?php if($emp->telephoneNo2Type=='Mobile'): ?> selected <?php endif; ?>>Mobile</option>
                           <option value='Home' <?php if($emp->telephoneNo2Type=='Home'): ?> selected <?php endif; ?>>Home</option>
                           <option value='Work' <?php if($emp->telephoneNo2Type=='Work'): ?> selected <?php endif; ?>>Work</option>
                           <option value='Office' <?php if($emp->telephoneNo2Type=='Office'): ?> selected <?php endif; ?>>Office</option>
                           <option value='Other' <?php if($emp->telephoneNo2Type=='Other'): ?> selected <?php endif; ?>>Other</option>
                                                </select>
                                                <?php if($errors->has('telephoneNo2Type')): ?>
                                                <span class="help-block">
                                                <strong><?php echo e($errors->first('telephoneNo2Type')); ?></strong>
                                                </span>
                                                <?php endif; ?>
                                             </div>
                                          </div>
                                          <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                             <input type="text" class="form-control fsc-input" maxlength="3" readonly id="ext2" name="ext2" value="<?php echo e($emp->ext2); ?>" placeholder="Ext">
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="form-group <?php echo e($errors->has('fax') ? ' has-error' : ''); ?>">
                                    <label class="control-label col-md-3">Fax :</label>
                                    <div class="col-md-6">
                                       <div class="row">
                                          <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                             <div class="dropdown">
                                                <input type="tel" class="form-control fsc-input bfh-phone"  data-country="countries_states1" id="fax" name="fax" value="<?php echo e($emp->fax); ?>" placeholder="(9999) 999-9999">
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label class="control-label col-md-3">Email :</label>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-11">
                                                <input type="text" class="form-control fsc-input" id="email" readonly name="email" value="<?php echo e($emp->email); ?>" placeholder="Email">
                                            </div>
                                        </div>
                                    </div>
                                 </div>
                                 <div class="form-group <?php echo e($errors->has('photo') ? ' has-error' : ''); ?>">
                                    <label class="control-label col-md-3">Picture :</label>
                                    <div class="col-md-6">
                                       <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                                <div style="width:100px;height:100px; border-radius: 50%; border: 1px solid whitesmoke ;text-align: center;position: relative;float:left;" id="image">
                                                    <img width="100%" style="border-radius: 50%;" height="100%" id="preview_image_5" src="<?php if(empty($emp->photo)): ?> <?php echo e(asset('public/images/noimage.jpg')); ?> <?php else: ?> <?php echo e(asset('public/employeeimage/')); ?>/<?php echo e($emp->photo); ?><?php endif; ?>"/>
                                                    <i id="loading5" class="fa fa-spinner fa-spin fa-3x fa-fw" style="position: absolute;left: 10%;top:10%;display: none"></i>
                                                </div>
                                                <a href="javascript:photo_upload()" style="text-decoration: none;margin-top:15px" class="im btn btn-primary"><i class="glyphicon glyphicon-edit"></i> Change</a>
                                                <input type="file" id="photo" style="display: none"/>
                                                <input type="hidden" id="photo_name"/>
                                                <input type="hidden" id="user_id11" value="<?php echo e($emp->id); ?>"/>
                                            </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="tab-pane fade" id="tab2primary">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									
										<div class="Branch">
											<h1>Hiring Information</h1>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3">Hire Date :</label>
											<div class="col-md-6">
												<div class="row">
													<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col"> 
														<div class="dropdown" style="margin-top: 1%;">
															<input name="hiremonth" type="text" value="<?php echo e($emp->hiremonth); ?>" id="hiremonth" class="form-control date1 tttt">
														</div>
													</div>
													<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
														<label class="control-label">Termination Date : </label>
													</div>
													<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
														<input name="termimonth" type="text" value="<?php echo e($emp->termimonth); ?>" id="termimonth" class="form-control date1 tttt" >
													</div>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3">Note :</label>
											<div class="col-md-6">
												<div class="row">
													<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
														<textarea name="tnote" id="tnote"  class="form-control fsc-input tttt"><?php echo e($emp->tnote); ?></textarea>
													</div>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3">Re-Hire Date :</label>
											<div class="col-md-6">
												<div class="row">
													<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
														<div class="dropdown" style="margin-top: 1%;">
															<input name="rehiremonth" type="text" value="<?php echo e($emp->rehiremonth); ?>" id="rehiremonth" class="form-control date1 tttt">
														</div>
													</div>
													<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
														<div class="dropdown" style="margin-top: 1%;">
															<label class="control-label">Termination Date :</label>
														</div>
													</div>
													<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
														<div class="dropdown" style="margin-top: 1%;">
															<input name="rehireyear" type="text" value="<?php echo e($emp->rehireyear); ?>" id="rehireyear" class="form-control date1 tttt">
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3">Note :</label>
											<div class="col-md-6">
												<div class="row">
													<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
														<textarea name="tnote" id="tnote"  class="form-control fsc-input tttt"><?php echo e($emp->tnote); ?></textarea>
													</div>
												</div>
											</div>
										</div>
										<div class="Branch">
											<h1>Branch / Department Information</h1>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3">Branch City:</label>
											<div class="col-md-6">
												<div class="row">
													<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
														<div class="dropdown" style="margin-top: 1%;">
															<select name="branch_city" id="branch_city" class="form-control fsc-input category tttt">
																<option value="">---Select---</option>
																<?php $__currentLoopData = $branch; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pos): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  
																<option value="<?php echo e($pos->city); ?>" <?php if($emp->branch_city ==$pos->city): ?> selected <?php endif; ?>><?php echo e($pos->city); ?></option>
																<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
															</select>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3">Branch Name:</label>
											<div class="col-md-6">
												<div class="row">
													<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
														<input type="text" readonly class="form-control fsc-input tttt" id="branch_name" name="branch_name" placeholder="" value="<?php echo e($emp->branch_name); ?>">
													</div>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3">Position :</label>
											<div class="col-md-6">
												<div class="row">
													<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
														<div class="dropdown" style="margin-top: 1%;">
															<select name="position" id="position" class="form-control fsc-input tttt">
																<option value="" >---Select Position---</option>
																<?php $__currentLoopData = $position; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pos): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  
																<option value="<?php echo e($pos->id); ?>" <?php if($emp->position ==$pos->id): ?> selected <?php endif; ?>><?php echo e($pos->position); ?></option>
																<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
															</select>
														</div>
													</div>
												</div>
											</div>
												<div class="col-md-2">	<input type="checkbox" id="super" class="tttt" name="super" value="1" <?php if($emp->super =='1'): ?> checked <?php endif; ?>><label for="super" class="tttt"> Supervisor</label></div>
										</div>
										
										<div class="form-group">
											<label class="control-label col-md-3">Note :</label>
											<div class="col-md-6">
												<div class="row">
													<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
														<textarea name="note" id="note" class="form-control fsc-input tttt"><?php echo e($emp->note); ?></textarea>
													</div>
												</div>
											</div>
										</div>
										
										<!--<div class="Branch">-->
										<!--	<h1>Review Information</h1>-->
										<!--</div>-->
											
<!--										<div class="review">-->
<!--										    	<div class="form-group">-->
<!--												<div class="col-md-12">-->
<!--													<a href="javascript:void(0)" class="ad pull-right btn btn-primary tttt" style="margin:10px 0;position: absolute;right: 113px;top: 0px;"><i class="fa fa-plus" aria-hidden="true"></i> Add Review</a>    -->
												
<!--										</div>-->
<!--										</div>-->
<!--										<?php $i=1; $j=1; $count = count($review1);?>-->
										
<!--										<?php if($count!=null): ?>-->
<!--											<?php $__currentLoopData = $review1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $re): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>-->
<!--											<script>-->
<!--    $(document).ready(function(){-->
<!--        $('#first_rev_day<?php echo e($re->id); ?>').on('keyup',function(){-->
<!--           var hiremonth = $('#hiremonth').val();-->
           var reset1 = parseInt($('#first_rev_day<?php echo e($re->id); ?>').val()); //alert(reset);
<!--            var tt = hiremonth.split("-").reverse().join(" ");-->
<!--         var t = new Date(tt); -->
         
<!--		t.setDate(t.getDate() + reset1);-->
<!--		var month = "0"+(t.getMonth()+1);-->
<!--		var date = "0"+t.getDate();-->
<!--		const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "June","July", "Aug", "Sept", "Oct", "Nov", "Dec"];-->
<!--		const d = new Date();-->
<!--		month = month.slice(-2);-->
<!--		date = date.slice(-2);-->
<!--		var date = monthNames[t.getMonth()]+"-"+date+"-"+t.getFullYear();-->
<!--		if(reset1=='')-->
<!--		{-->
<!--		    $('#reviewmonth<?php echo e($re->id); ?>').empty();-->
<!--		}-->
<!--		else-->
<!--		{-->
<!--           $('#reviewmonth<?php echo e($re->id); ?>').val(date);-->
<!--		}-->
<!--        });-->
<!--    });-->
<!--</script>-->
<!--											<div class="form-group">-->
<!--												<label class="control-label col-md-3"><?php echo $i; $i++;?> Review Days :</label>-->
<!--												<div class="col-md-6">-->
<!--													<div class="row">-->
<!--														<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">-->
<!--															<div class="dropdown" style="margin-top: 1%;">-->
<!--																<input type="text" class="form-control fsc-input first_rev_day tttt" maxlength="3" onkeypress="return isNumberKey(event)" name="first_rev_day[]" id="first_rev_day<?php echo e($re->id); ?>" value="<?php echo e($re->first_rev_day); ?>">-->
<!--															</div>-->
<!--														</div>-->
<!--														<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">-->
<!--															<div class="dropdown" style="margin-top: 1%;">-->
<!--																<label class="control-label"><?php echo $j; $j++;?> Review Date :</label>-->
<!--															</div>-->
<!--														</div>-->
<!--														<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">-->
<!--															<div class="dropdown" style="margin-top: 1%;">-->
<!--																<input name="reviewmonth[]" type="text" value="<?php echo e($re->reviewmonth); ?>" id="reviewmonth<?php echo e($re->id); ?>" class="form-control reviewmonth tttt" readonly="readonly">-->
<!--																<input name="ree[]" type="hidden" value="<?php echo e($re->id); ?>" id="ree" class="form-control tttt">-->
<!--															</div>-->
<!--														</div>-->
<!--													</div>-->
<!--												</div>-->
<!--											</div>-->
												
<!--											<div class="form-group">-->
<!--												<label class="control-label col-md-3">Comments :</label>-->
<!--												<div class="col-md-6">-->
<!--													<div class="row">-->
<!--														<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">-->
<!--															<textarea name="hiring_comments[]" id="hiring_comments" class="form-control fsc-input tttt"><?php echo e($re->hiring_comments); ?></textarea>-->
<!--														</div>-->
<!--													</div>-->
<!--												</div>-->
<!--											</div>-->
<!--					    	<?php if($i == 2): ?>-->
<!--							<?php else: ?> -->
<!--							<a href="#myModal<?php echo e($re->id); ?>" role="button" class="btn btn-danger remove pull-right tttt" title="Add field" data-toggle="modal"> <i class="fa fa-trash-o" aria-hidden="true"></i> </a>-->
<!--							<?php endif; ?>-->
<!--											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>-->
<!--											<?php else: ?>-->
<!--											<div class="form-group">-->
<!--												<label class="control-label col-md-3">Review Days :</label>-->
<!--												<div class="col-md-6">-->
<!--													<div class="row">-->
<!--														<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">-->
<!--															<div class="dropdown" style="margin-top: 1%;">-->
<!--																<input type="text" class="form-control fsc-input first_rev_day tttt" onkeypress="return isNumberKey(event)" maxlength="3" name="first_rev_day[]" id="first_rev_day">-->
<!--															</div>-->
<!--														</div>-->
<!--														<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">-->
<!--															<div class="dropdown" style="margin-top: 1%;">-->
<!--																<label class="control-label">Review Date :</label>-->
<!--															</div>-->
<!--														</div>-->
<!--														<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">-->
<!--															<div class="dropdown" style="margin-top: 1%;">-->
<!--																<input name="reviewmonth[]" type="text" id="reviewmonth" class="form-control reviewmonth tttt" readonly="readonly">-->
<!--															</div>-->
<!--														</div>-->
<!--													</div>-->
<!--												</div>-->
<!--											</div> -->
<!--										<input name="ree[]" type="hidden" value="" id="ree" class="form-control">-->
<!--											<div class="form-group">-->
<!--												<label class="control-label col-md-3">Comments :</label>-->
<!--												<div class="col-md-6">-->
<!--													<div class="row">-->
<!--														<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">-->
<!--															<textarea name="hiring_comments[]" id="hiring_comments" rows="1" class="form-control fsc-input tttt"></textarea>-->
<!--														</div>-->
<!--													</div>-->
<!--												</div>-->
<!--												</div>-->
<!--											<?php endif; ?>-->
<!--											<div class="review-1">-->
<!--											<div class="review-2">-->
<!--												<div class="input_fields_wrap_1"></div>   -->
<!--											</div>-->
<!--											</div>-->
<!--										   </div>	-->
											</div>		
											</div>
                                	<div class="tab-pane fade" id="tab3primary">
                                	    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										    <?php if($emp->type !='user')
										    {?>
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding:0px;">
												<div class="Branch" style="display: grid;padding: 7px 10px !important;">
													<h1>Pay Information</h1>
												    <div class="review" style="text-align:right;position: absolute;right: 10px;">
    												    <a class="btn btn-warning" id="add_pay" style="margin-top: -5px;">Add Pay</a>
    												</div>
												</div>
												<div class="clearfix"></div>
												
												
												<!--<div class="form-group ">
													<label class="control-label col-md-3">Pay Method :</label>
													<div class="col-md-6">
														<div class="row">
															<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
																<div class="dropdown" style="margin-top: 1%;">
																	<select name="pay_method" id="pay_method" class="form-control tttt">
																		<option value="Salary" <?php if($emp->pay_method=='Salary'): ?> selected <?php endif; ?>>Salary</option>
																		<option value="Hourly" <?php if($emp->pay_method=='Hourly'): ?> selected <?php endif; ?>>Hourly</option>
																	</select>
																</div>
															</div>
															<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
																<label class="fsc-form-label">Pay Duration :</label>
															</div>
															<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
																<select name="pay_frequency" id="pay_frequency" class="form-control tttt">
																	<option value="Weekly" <?php if($emp->pay_frequency=='Weekly'): ?> selected <?php endif; ?>>Weekly</option>
																	<option value="Bi-Weekly" <?php if($emp->pay_frequency=='Bi-Weekly'): ?> selected <?php endif; ?>>Bi-Weekly</option>
																	<option value="Bi-Monthly" <?php if($emp->pay_frequency=='Semi-Monthly'): ?> selected <?php endif; ?>>Semi-Monthly</option>
																	<option value="Monthly" <?php if($emp->pay_frequency=='Monthly'): ?> selected <?php endif; ?>>Monthly</option>
																</select>
															</div>
														</div>
													</div>
												</div>
												<div class="fieldGroup ">	<a href="javascript:void(0)" class="addMore pull-right"><i class="fa fa-plus" aria-hidden="true"></i> Add Pay</a>
											
													<?php $k=1 ; $in=count($info );?><?php if($in!=null): ?> <?php $__currentLoopData = $info; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $in): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
													<div id="field<?php echo e($in->id); ?>">
														<div class="form-group">
															<label class="control-label col-md-3">Pay Rate :</label>
															<div class="col-md-6">
																<div class="row">
																	<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
																		<div class="dropdown" style="margin-top: 1%;">
																			<input name="pay_scale[]" value="<?php echo e($in->pay_scale); ?>" type="text" maxlength="10" id="pay_scale" class="form-control tttt pay_scale" />
																			<input name="employee[]" value="<?php echo e($in->id); ?>" type="hidden" id="employee" class="form-control" />
																		</div>
																	</div>
																	<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
																		<label class="fsc-form-label">Effective Date :</label>
																	</div>
																	<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
																		<input name="effective_date[]" value="<?php echo e($in->effective_date); ?>" type="text" id="effective_date<?php echo e($in->id); ?>" class="form-control date1 tttt" />
																	</div>
																</div>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-3">Note :</label>
															<div class="col-md-6">
																<textarea id="fields" name="fields[]" class="form-control tttt fsc-input"><?php echo e($in->fields); ?></textarea>
															</div>
														</div>
														<?php $k; $k++;?><?php if($k == 2): ?> <?php else: ?>
														<div class="form-group">
															<div class="col-md-12"><a href="#myModal1<?php echo e($in->id); ?>" role="button" class="btn btn-danger remove11 pull-right" data-toggle="modal"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
															</div>
														</div><?php endif; ?></div><?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> <?php else: ?>
													<div id="Pay Method">
														<div class="form-group">
															<label class="control-label col-md-3">Pay Rate :</label>
															<div class="col-md-6">
																<div class="row">
																	<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
																		<div class="dropdown" style="margin-top: 1%;">
																			<input name="pay_scale[]" value="" type="text" id="pay_scale" maxlength="10" onkeypress="return isNumberKey(event)" class="form-control tttt pay_scale" />
																			<input name="employee[]" value="" type="hidden" id="employee" class="form-control" />
																		</div>
																	</div>
																	<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
																		<label class="fsc-form-label">Effective Date :</label>
																	</div>
																	<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
																		<input name="effective_date[]" value="" type="text" id="effective_date" class="form-control date1 tttt" />
																	</div>
																</div>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-3">Note :</label>
															<div class="col-md-6">
																<textarea id="fields" name="fields[]" class="form-control fsc-input tttt"></textarea>
															</div>
														</div>
													</div><?php endif; ?></div>!-->
													
												<div class="">
												    <div class="table-responsive">
												        <table class="table table-bordered">
												            <thead>
												                <tr>
												                    <th style="width:10%;">Pay Method</th>
												                    <th style="width:12%;">Pay Duration</th>
												                    <th style="width:11%;">Pay Rate</th>
												                    <th style="width:12%;">Effective Date</th>
												                    <th style="width:40%;">Note</th>
												                    <th style="width:5%;">Action</th>
												                </tr>
												            </thead>
												            <tbody class="addpayinfo">
												                <?php 
												                
												                 echo $countinfos=count($info);exit;
												                if($countinfos =='0')
												                {?>
												                <tr>
												                    <td><select class="form-control" name="pay_method[]"><option value="">Select</option><option value="Salary">Salary</option><option value="Hourly">Hourly</option> </select></td>
												                    <td><select class="form-control" name="pay_frequency[]"><option value="">Select</option> <option value="Weekly">Weekly</option><option value="Bi-Weekly">Bi-Weekly</option><option value="Semi-Monthly">Semi-Monthly</option><option value="Monthly">Monthly</option></select></td>
												                    <td><input type="text" name="pay_scale[]" class="form-control pay_scale_1s text-center"></td>
												                    <td><input type="text" name="effective_date[]" class="form-control paydate_1"></td>
												                    <td><textarea cols="40" rows="1" name="fields[]" class="form-control"></textarea></td>
                                                                    <td style="text-align:center;">

                                                                    </td>
												                </tr>
												                <?php 
												                }
												                else
												                {
												                    $cnts=1;
												                foreach($info as $iinfo)
												                {
												                    $cnts++;
												                ?>
												                    <tr>
												                    <td>
												                        <select class="form-control" name="pay_method[]">
												                        <option value="">Select</option>
												                        <option value="Salary" <?php if($iinfo->pay_method =='Salary'): ?> selected <?php endif; ?>>Salary</option>
												                        <option value="Hourly" <?php if($iinfo->pay_method =='Hourly'): ?> selected <?php endif; ?>>Hourly</option>
												                        </select></td>
												                    <td><select class="form-control" name="pay_frequency[]">
												                        <option value="">Select</option>
												                        <option value="Weekly" <?php if($iinfo->pay_frequency =='Weekly'): ?> selected <?php endif; ?>>Weekly</option>
												                        <option value="Bi-Weekly" <?php if($iinfo->pay_frequency =='Bi-Weekly'): ?> selected <?php endif; ?>>Bi-Weekly</option>
												                        <option value="Semi-Monthly" <?php if($iinfo->pay_frequency =='Semi-Monthly'): ?> selected <?php endif; ?>>Semi-Monthly</option>
												                        <option value="Monthly" <?php if($iinfo->pay_frequency =='Monthly'): ?> selected <?php endif; ?>>Monthly</option>
												                        </select></td>
												                    <td><input type="text" name="pay_scale[]" value="<?php echo e($iinfo->pay_scale); ?>"  class="text-center form-control pay_scale_<?php echo e($cnts); ?>"></td>
												                    <td><input type="text" name="effective_date[]" value="<?php echo e(date('m/d/Y',strtotime($iinfo->effective_date))); ?>" class="form-control paydate_<?php echo $cnts;?>"></td>
												                    <td><textarea cols="40" rows="1" name="fields[]"  class="form-control"><?php echo e($iinfo->fields); ?></textarea></td>
                                                                    <td style="text-align:center;">
                                                                        <a href="#myModalpay_<?php echo e($iinfo->id); ?>" id="add_row_pay" role="button" class="btn btn-danger remove_pay" title="Remove field" data-toggle="modal"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></a>
                                                                    </td>
												                </tr>
												                <script>
												                	$(function() {
	
												                	$('.pay_scale_<?php echo $cnts;?>').blur(function() {
												                	    
                                                            			$(this).val('$' + $(this).getNum());
                                                            			});
                                                            		});
		
                                                            		$(".paydate_<?php echo $cnts;?>").datepicker({
                                                            			autoclose: true,
                                                            	format: "mm/dd/yyyy",
                                                            			//endDate: "today"
                                                            		});
												                </script>
												                <?php
												                }
												                    
												                    
												                }
												                ?>
												                
												            </tbody>
												        </table>
												    </div>
											    </div>
											</div>
											<?php
										    }
										    ?>
										    <div class="Branch" style="display: grid;padding: 7px 10px !important;">
												<h1>Review Information</h1>
											    <div class="review" style="text-align:right;position: absolute;right: 25px;">
												    <a class="btn btn-warning" id="add_review" style="margin-top: -5px;">Add Review</a>
												</div>
											</div>
											<div class="clearfix"></div>
										    
											<!--	<div class="review ">
													<div class="form-group">
														<div class="col-md-12">	<a href="javascript:void(0)" class="ad pull-right btn btn-primary tttt" style="margin:10px 0;position: absolute;right: 113px;top: 0px;"><i class="fa fa-plus" aria-hidden="true"></i> Add Review</a> 
														</div>
													</div>
													<?php $i=1; $j=1; $count=count($review1);?><?php if($count!=null): ?> <?php $__currentLoopData = $review1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $re): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
													<script>
														$(document).ready(function(){
														        $('#first_rev_day<?php echo e($re->id); ?>').on('keyup',function(){
														           var hiremonth = $('#hiremonth').val();
														           var reset1 = parseInt($('#first_rev_day<?php echo e($re->id); ?>').val()); //alert(reset);
														            var tt = hiremonth.split("-").reverse().join(" ");
														         var t = new Date(tt); 
														        // alert(reset1);
																t.setDate(t.getDate() + reset1);
																var month = "0"+(t.getMonth()+1);
																var date = "0"+t.getDate();
																const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "June","July", "Aug", "Sept", "Oct", "Nov", "Dec"];
																const d = new Date();
																month = month.slice(-2);
																date = date.slice(-2);
																var date = monthNames[t.getMonth()]+"-"+date+"-"+t.getFullYear();
																if($(this).val() == '') { // check if value changed
														         $('#reviewmonth<?php echo e($re->id); ?>').val('');
																}
																else
																{
														           $('#reviewmonth<?php echo e($re->id); ?>').val(date);
																}
														        });
														    });
													</script>
													<div class="form-group">
														<label class="control-label col-md-3">
															<?php echo '('.$i.') '; $i++;?>Review Days :</label>
														<div class="col-md-6">
															<div class="row">
																<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
																	<div class="dropdown" style="margin-top: 1%;">
																		<input type="text" class="form-control tttt fsc-input first_rev_day" maxlength="3" onkeypress="return isNumberKey(event)" name="first_rev_day[]" id="first_rev_day<?php echo e($re->id); ?>" value="<?php echo e($re->first_rev_day); ?>">
																	</div>
																</div>
																<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
																	<div class="dropdown" style="margin-top: 1%;">
																		<label class="control-label">
																			<?php echo '('.$j.') '; $j++;?>Review Date :</label>
																	</div>
																</div>
																<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
																	<div class="dropdown" style="margin-top: 1%;">
																		<input name="reviewmonth[]" type="text" value="<?php echo e($re->reviewmonth); ?>" id="reviewmonth<?php echo e($re->id); ?>" class="form-control tttt reviewmonth">
																		<input name="ree[]" type="hidden" value="<?php echo e($re->id); ?>" id="ree" class="form-control">
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3">Comments :</label>
														<div class="col-md-6">
															<div class="row">
																<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																	<textarea name="hiring_comments[]" id="hiring_comments" class="form-control tttt fsc-input"><?php echo e($re->hiring_comments); ?></textarea>
																</div>
															</div>
														</div>
													</div><?php if($i == 2): ?> <?php else: ?>
													<a href="#myModal<?php echo e($re->id); ?>" role="button" class="btn btn-danger remove pull-right" title="Add field" data-toggle="modal"> <i class="fa fa-trash-o" aria-hidden="true"></i> 
													</a><?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> <?php else: ?>
													<div class="form-group">
														<label class="control-label col-md-3">Review Days :</label>
														<div class="col-md-6">
															<div class="row">
																<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
																	<div class="dropdown" style="margin-top: 1%;">
																		<input type="text" class="form-control tttt fsc-input first_rev_day" onkeypress="return isNumberKey(event)" maxlength="3" name="first_rev_day[]" id="first_rev_day">
																	</div>
																</div>
																<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
																	<div class="dropdown" style="margin-top: 1%;">
																		<label class="control-label">Review Date :</label>
																	</div>
																</div>
																<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
																	<div class="dropdown" style="margin-top: 1%;">
																		<input name="reviewmonth[]" type="text" id="reviewmonth" class="form-control tttt reviewmonth">
																	</div>
																</div>
															</div>
														</div>
													</div>
													<input name="ree[]" type="hidden" value="" id="ree" class="form-control">
													<div class="form-group">
														<label class="control-label col-md-3">Comments :</label>
														<div class="col-md-6">
															<div class="row">
																<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																	<textarea name="hiring_comments[]" id="hiring_comments" rows="1" class="form-control tttt fsc-input"></textarea>
																</div>
															</div>
														</div>
													</div><?php endif; ?>
													<div class="review-1">
														<div class="review-2">
															<div class="input_fields_wrap_1">
										</div>
														</div>
													</div>
													
												</div>!-->
												<div class="review " style="text-align:right;">
												    <!--<a class="btn btn-primary" data-toggle="modal" data-target="#addReview" style="margin-bottom:10px;">Add Review</a>-->
												    <div class="table-responsive">
												        <table class="table table-bordered">
												            <thead>
												                <tr>
												                    <th style="width:7%;">Review No.</th>
												                    <th style="width:12%;">Review Date</th>
												                    <th style="width:10%;">Next Review</th>
												                    <th style="width:12%;">Next Review Date</th>
												                    <th style="width:11%;">Next Pay Rate</th>
												                    <th style="width:35%;">Comments</th>
												                    <th style="width:5%;">Action</th>
												                </tr>
												            </thead>
												            <tbody class="addreviewinfo">
												                 <?php $countreviewinfo=count($reviewinfo);
												                 
												                if($countreviewinfo =='0')
												                {?>
												               
												                <tr>
												                    <td></td>
												                    <td><input type="text" id="reviewdate_1" name="review_date[]" class="form-control review_date_1"></td>
												                    <td><input type="text" name="review_day[]" class="form-control review_day_1  text-center"></td>
												                    <td><input type="text" readonly name="next_review_date[]" class="form-control next_review_date_1"></td>
												                    <td><input type="text" name="review_rate[]" class="form-control pay_review_scale_1 text-center"></td>
												                    <td><textarea cols="40" name="review_comments[]" rows="1" class="form-control"></textarea></td>
                                                                    <td style="text-align:center;">
                                                                    </td>
												                </tr>
												                <?php
												                }
												                else
												                {
												                         $cnts=1;
												                         $cnts11=0;
												                         
												                foreach($reviewinfo as $rinfo)
												                {
												                    $cnts++;
												                    $cnts11++;
												                    
												               
												                    ?>
												                <tr>
												                    <td style="text-align:center"><?php echo e($cnts11); ?></td>
												                    <td><input type="text" name="review_date[]" value="<?php echo e(date('m/d/Y',strtotime($rinfo->review_date))); ?>" class="form-control review_date_<?php echo e($cnts); ?>"></td>
												                    <td><input type="text" name="review_day[]" value="<?php echo e($rinfo->review_day); ?>" class="form-control text-center review_day_<?php echo e($cnts); ?>"></td>
												                    <td><input type="text" readonly name="next_review_date[]" value="<?php echo e(date('m/d/Y',strtotime($rinfo->next_review_date))); ?>" class="form-control next_review_date_<?php echo e($cnts); ?>"></td>
												                    <td><input type="text" name="review_rate[]" value="<?php echo e($rinfo->review_rate); ?>" class="form-control text-center pay_review_scale_<?php echo e($cnts); ?>"></td>
												                    <td><textarea cols="40" name="review_comments[]" rows="1" class="form-control"><?php echo e($rinfo->review_comments); ?></textarea></td>
                                                                    <td style="text-align:center;">
                                                                            <a href="#myModalreview_<?php echo e($rinfo->id); ?>" id="add_row_review" role="button" class="btn btn-danger remove_review" title="Remove field" data-toggle="modal"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></a>
                                                               
                                                                    </td>
												                </tr>
												                   <script>
												                	$(function() {
	
												                	$('.pay_review_scale_<?php echo $cnts;?>').blur(function() {
												                	    
                                                            			$(this).val('$' + $(this).getNum());
                                                            			});
                                                            		});
                                                            			$(".review_date_<?php echo $cnts;?>").datepicker({
                                                            			autoclose: true,
                                                            	        format: "mm/dd/yyyy",
                                                            			//endDate: "today"
                                                            		});
                                                            		
		
												                </script>
												                <script>
												                    (function($, window, document, undefined){
                                                $(".review_day_<?php echo $cnts;?>").blur(function(){
                                                   var date = new Date($(".review_date_<?php echo $cnts;?>").val()),
                                                       days = parseInt($(".review_day_<?php echo $cnts;?>").val(), 10);
                                                    
                                                    if(!isNaN(date.getTime())){
                                                        date.setDate(date.getDate() + days);
                                                        $(".next_review_date_<?php echo $cnts;?>").val(date.toInputFormat());
                                                    } else {
                                                        alert("Invalid Date");  
                                                    }
                                                });
                                                
                                                
                                                //From: http://stackoverflow.com/questions/3066586/get-string-in-yyyymmdd-format-from-js-date-object
                                                Date.prototype.toInputFormat = function() {
                                                   var yyyy = this.getFullYear().toString();
                                                   var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
                                                   var dd  = this.getDate().toString();
                                                //   return yyyy + "-" + (mm[1]?mm:"0"+mm[0]) + "-" + (dd[1]?dd:"0"+dd[0]); // padding
                                                   return (mm[1]?mm:"0"+mm[0]) + "/" + (dd[1]?dd:"0"+dd[0]) + "/" + yyyy; // padding
                                                };
                                            })(jQuery, this, document);
	
		
												                </script>
												                 
												                <?php
												                }
												                }
												                ?>
												            </tbody>
												        </table>
												    </div>
											    </div>
												
										    
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="padding:0px;">
												<div class="Branch" style="display: grid;padding: 7px 10px !important;">
													<h1>Tax Withholding Information</h1>
												    <div class="review" style="text-align:right;position: absolute;right: 10px;">
    												    <a class="btn btn-warning" id="add_tax" style="margin-top: -5px;">Add Tax Withhold</a>
    												</div>
												</div>
												<div class="clearfix"></div>
												<!--<div class="form-group">
													<label class="control-label col-md-3">Filling Status :</label>
													<div class="col-md-6">
														<select name="filling_status" id="filling_status" class="form-control tttt">
															<option value="Single" <?php if($emp->filling_status=='Single'): ?> selected <?php endif; ?>>Single</option>
															<option value="Married File Jointly" <?php if($emp->filling_status=='Married File Jointly'): ?> selected <?php endif; ?>>Married File Jointly</option>
															<option value="Married File Separately" <?php if($emp->filling_status=='Married File Separately'): ?> selected <?php endif; ?>>Married File Separately</option>
															<option value="Head of Household" <?php if($emp->filling_status=='Head of Household'): ?> selected <?php endif; ?>>Head of Household</option>
														</select>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Federal Claim :</label>
													<div class="col-md-8">
														<div class="row">
															<div class="col-lg-2 col-md-12 col-sm-12 col-xs-12">
																<div class="dropdown" style="margin-top: 1%;">
																	<input name="fedral_claim" value="<?php echo e($emp->fedral_claim); ?>" maxlength="3" onkeypress="return isNumberKey(event)" type="text" id="fedral_claim" onkeypress="return isNumberKey(event)" class="form-control tttt" />
																</div>
															</div>
															<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
																<label class="fsc-form-label martop7">Additional Withholding :</label>
															</div>
															<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
																<input name="additional_withholding" value="<?php echo e($emp->additional_withholding); ?>" type="text" id="additional_withholding" class="form-control tttt" />
															</div>
															<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
																<center>
																<div class="dropdown" style="margin-top: 1%;">
																	<label class="file-upload btn btn-primary">Browse for file ...
																		<input name="additional_attach" style="opecity:0" placeholder="Upload Service Image"  type="file">
																	</label>
																	<input name="file_name_2" placeholder="Upload  Image" value="<?php echo e($emp->additional_attach); ?>" 
																	class="form-control fsc-input" id="" type="hidden">	<span>
															<?php echo e($emp->additional_attach); ?>

															</span>
																</div>
																</center>
															</div>
															
														</div>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">State Claim :</label>
													<div class="col-md-8">
														<div class="row">
															<div class="col-lg-2 col-md-12 col-sm-12 col-xs-12">
																<div class="dropdown" style="margin-top: 1%;">
																	<input name="state_claim" value="<?php echo e($emp->state_claim); ?>" maxlength="3" type="text" id="state_claim" onkeypress="return isNumberKey(event)" class="form-control tttt" />
																</div>
															</div>
															<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
																<label class="fsc-form-label martop7">Additional Withholding :</label>
															</div>
															<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
																<input name="additional_withholding_1" value="<?php echo e($emp->additional_withholding_1); ?>" type="text" id="additional_withholding_1" class="form-control tttt" />
															</div>
															
															
																<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
																<center>
																<div class="dropdown" style="margin-top: 1%;">
																	<label class="file-upload btn btn-primary">Browse for file ...
																		<input name="additional_attach_1" style="opecity:0" placeholder="Upload  Image" id="" type="file">
																	</label>
																	<input name="file_name_1" placeholder="Upload  Image" value="<?php echo e($emp->additional_attach_1); ?>" class="form-control fsc-input" 
																	id="file_name_1" type="hidden">	<span>
															<?php echo e($emp->additional_attach_1); ?>

															</span>
																	</div>
																</center>
															</div>
														
														</div>
													</div>
													
												
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Local Claim :</label>
													<div class="col-md-8">
														<div class="row">
															<div class="col-lg-2 col-md-12 col-sm-12 col-xs-12">
																<div class="dropdown" style="margin-top: 1%;">
																	<input name="local_claim" value="<?php echo e($emp->local_claim); ?>" maxlength="3" type="text" id="local_claim" class="form-control tttt" />
																</div>
															</div>
															<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
																<label class="fsc-form-label martop7">Additional Withholding :</label>
															</div>
															<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
																<input name="additional_withholding_2" value="<?php echo e($emp->additional_withholding_2); ?>" type="text" id="additional_withholding_2" class="form-control tttt" />
															</div>
															
															<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
																<center>
																<div class="dropdown" style="margin-top: 1%;">
																	<label class="file-upload btn btn-primary">Browse for file ...
																		<input name="additional_attach_2" style="opecity:0" placeholder="Upload  Image" id="" type="file">
																	</label>
																	<input name="file_name" placeholder="Upload  Image" value="<?php echo e($emp->additional_attach_2); ?>" class="form-control fsc-input" 
																	id="file_name" type="hidden"></div>
																	<span><?php echo e($emp->additional_attach_2); ?></span>
																</center>
															</div>
														
											 			</div>
													</div>
													
												
												</div>!-->
												
												
												<div class="table-responsive">
												    <table class="table table-bordered">
												        <thead>
												            <tr>
												                <th width="10%">Year</th>
												                <th width="27%;">Filling Status</th>
												                <th width="9%">Fed. Claim</th>
												                <th width="11%">Fed. Add. Wh.</th>
												                <th width="9%">St. Claim</th>
												                <th width="11%">St. Add. Wh.</th>
												                <th width="9%">Local Claim</th>
												                <th width="12%">Local Add. Wh.</th>
												                <th width="5%">Action</th>
												            </tr>
												        </thead>
												        <tbody class="addtaxinfo">
												            <?php 
												            $whcount=count($whinfo);
												            if($whcount =='0')
												            {?>
												            <tr>
												                <td><select class="form-control" name="year_wh[]"><option value="">Select</option><option value="2021">2021</option></select></td>
												                <td>	
    											                    <select name="fillingstatus[]" id="fillingstatus" class="form-control tttt">
        											                    <option value="">Select</option>
            														    <option value="Single">Single</option>
            														    <option value="Married File Jointly">Married File Jointly</option>
            														    <option value="Married File Separately">Married File Separately</option>
            														    <option value="Head of Household">Head of Household</option>
                													</select>
                												</td>
												                <td><input class="form-control text-center" name="fed_claim[]"></td>
												                <td><input class="form-control text-center fedwh_1" name="fed_wh[]"></td>
												                <td><input class="form-control text-center" name="st_claim[]"></td>
												                <td><input class="form-control text-center stwh_1" name="st_wh[]"></td>
												                <td><input class="form-control text-center" name="local_claims[]"></td>
												                <td><input class="form-control text-center localwh_1" name="local_wh[]"></td>
												                <td style="text-align:center;">

                                                                </td>
												            </tr>
												            <?php
												            }
												            else
												            {
												                $cnts=1;
												                foreach($whinfo as $whinfo)
												                {
												                    $cnts++;
												            ?>
												            <tr>
												                <td><select class="form-control" name="year_wh[]">
												                    <option value="">Select</option>
												                    <option value="2021" <?php if($whinfo->year_wh =='2021'): ?> selected <?php endif; ?>>2021</option>
												                    </select>
												                    </td>
												                <td>	<select name="fillingstatus[]" id="fillingstatus" class="form-control tttt">
												                    <option>Select</option>
															<option value="Single" <?php if($whinfo->fillingstatus =='Single'): ?> selected <?php endif; ?>>Single</option>
															<option value="Married File Jointly" <?php if($whinfo->fillingstatus =='Married File Jointly'): ?> selected <?php endif; ?>>Married File Jointly</option>
															<option value="Married File Separately" <?php if($whinfo->fillingstatus =='Married File Separately'): ?> selected <?php endif; ?>>Married File Separately</option>
															<option value="Head of Household" <?php if($whinfo->fillingstatus =='Head of Household'): ?> selected <?php endif; ?>>Head of Household</option>
														</select>
													</td>
												                <td><input class="form-control text-center" name="fed_claim[]" value="<?php echo e($whinfo->fed_claim); ?>"></td>
												                <td><input class="form-control text-center fedwh_<?php echo $cnts;?>" name="fed_wh[]" value="<?php echo e($whinfo->fed_wh); ?>"></td>
												                <td><input class="form-control text-center" name="st_claim[]" value="<?php echo e($whinfo->st_claim); ?>"></td>
												                <td><input class="form-control text-center stwh_<?php echo $cnts;?>" name="st_wh[]" value="<?php echo e($whinfo->st_wh); ?>"></td>
												                <td><input class="form-control text-center" name="local_claims[]" value="<?php echo e($whinfo->local_claims); ?>"></td>
												                <td><input class="form-control text-center localwh_<?php echo $cnts;?>" name="local_wh[]" value="<?php echo e($whinfo->local_wh); ?>"></td>
												                <td style="text-align:center;">
                                                                <a href="#myModalwh_<?php echo e($whinfo->id); ?>" id="add_row_wh" role="button" class="btn btn-danger remove_wh" title="Remove field" data-toggle="modal"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></a>
                                                               
                                                                </td>
												            </tr>
												             <script>
												                	$(function() {
	
												                	$('.fedwh_<?php echo $cnts;?>').blur(function() {
												                	    $(this).val('$' + $(this).getNum());
                                                            			});
                                                            		
                                                            		
                                                            		$('.stwh_<?php echo $cnts;?>').blur(function() {
												                	    
                                                            			$(this).val('$' + $(this).getNum());
                                                            			});
                                                            		
                                                            	
                                                            		$('.localwh_<?php echo $cnts;?>').blur(function() {
												                	    
                                                            			$(this).val('$' + $(this).getNum());
                                                            			});
                                                            		});
                                                            		
		
												                </script>
												                
												            <?php 
												            }
												            }
												            ?>
												        </tbody>
												    </table>
												</div>
											</div>
											<div class="Branch">
                                                <h1>Company Benefits</h1>
                                            </div>
                                            <div class="responsibility">
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <table class="table table-bordered">
                                                            <thead>
                                                                <tr style="text-align:center">
                                                                    <th width="5%">No.</th>
                                                                    <th width="10%">Paid Leaves</th>
                                                                    <th width="7%">Frequency</th>
                                                                    <th width="10%">Sick Leaves</th>
                                                                    <th width="7%">Frequency</th>
                                                                    <th width="25%">Holiday</th>
                                                                    <th width="25%">Note</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3" style="padding-top:0px;">Holiday :</label>
                                                <div class="col-md-9">
                                                    <div class="col-md-12 col-sm-12 col-xs-12 checkboxbox">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <input type="checkbox"  id="holiday_1" name="holidays[]" value=""> 
                                                                <label class="fsc-form-label pull-left" for="holiday_1"> Christmas Day (World)</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
										</div>
									</div>
									 
							<div class="tab-pane fade" id="tab4primary">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<div class="Branch">
											<h1>Personal Information</h1>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3">Gender :</label>
											<div class="col-md-6">
												<div class="row">
													<div class="col-md-4">
														<div class="dropdown" style="margin-top: 1%;">
															<select name="gender" id="gender" class="form-control fsc-input">
																<option value="Male" <?php if($emp->gender=='Male'): ?> selected <?php endif; ?>>Male</option>
																<option value="Female" <?php if($emp->gender=='Female'): ?> selected <?php endif; ?>>Female</option>
															</select>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3">Marital Status :</label>
											<div class="col-md-6">
												<div class="row">
													<div class="col-md-4">
														<div class="dropdown" style="margin-top: 1%;">
															<select name="marital" id="marital" class="form-control fsc-input">
																<option value="">Select</option>
																<option value="Married" <?php if($emp->marital=='Married'): ?> selected <?php endif; ?>>Married</option>
																<option value="UnMarried" <?php if($emp->marital=='UnMarried'): ?> selected <?php endif; ?>>UnMarried</option>
															</select>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3">Date of Birth : </label>
											<div class="col-md-6">
												<div class="row">
													<div class="col-md-4">
														<div class="dropdown" style="margin-top: 1%;">
															<input type="text" class="form-control date1 fsc-input" name="month"  id="month" value="<?php echo e($emp->month); ?>">
														</div>
													</div>
												</div>
											</div>
										</div>
										
											<div class="form-group">
												    <label class="control-label col-md-3">Languages Known:</label>
												    <div class="col-md-6 langcheckbox"> 
												        <div class="row">
												     <?php
												           $myString = $emp->languages;
$array =explode(",",$myString); ?>                           
												             <?php $__currentLoopData = $language; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lan1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
												            <div class="col-md-4"><input type="checkbox"  id="lanbengali_<?php echo e($lan1->language_name); ?>" 
												            <?php foreach($array as $arr){ if($arr==$lan1->language_name){?> checked <?php } } ?> 
												            name="languages[]" value="<?php echo e($lan1->language_name); ?>"> 
												            <label class="fsc-form-label" for="lanbengali_<?php echo e($lan1->language_name); ?>"> <?php echo e($lan1->language_name); ?></label></div>
												            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
												        </div>
												       
												    </div>
												   
												</div>
											
										<div class="form-group">
											<label class="control-label col-md-3">ID Proof 1 :</label>
											<div class="col-md-6">
												<div class="row">
													<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
														<div class="dropdown" style="margin-top: 1%;">
															<select name="pf1" class="form-control fsc-input" id="pf1">
																<option value="">Select</option>
																<option value="State ID" <?php if($emp->pf1=='State ID'): ?> selected <?php endif; ?>>State ID</option>
																<option value="Voter Id" <?php if($emp->pf1=='Voter Id'): ?> selected <?php endif; ?>>Voter ID</option>
																<option value="Driving Licence" <?php if($emp->pf1=='Driving Licence'): ?> selected <?php endif; ?>>Driving Licence</option>
																<option value="Pan Card" <?php if($emp->pf1=='DPan Card'): ?> selected <?php endif; ?>>Pan Card</option>
																<option value="Pass Port" <?php if($emp->pf1=='Pass Port'): ?> selected <?php endif; ?>>Pass Port</option>
															</select>
														</div>
													</div>
													<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 <?php echo e($errors->has('pfid1') ? ' has-error' : ''); ?>">
                                                        <div class="dropdown">
                                                            <span>
                                                                <div style="width:50px;height:50px;border-radius:50%; border: 1px solid whitesmoke ;text-align: center;position: relative;float:left;" id="image">
                                                                    <img width="100%" height="100%" style="border: 1px solid #00468f;" id="preview_image_3" src="<?php if(empty($emp->pfid1)): ?> <?php echo e(asset('public/images/noimage.jpg')); ?> <?php else: ?> <?php echo e(asset('public/employeeProof1/')); ?>/<?php echo e($emp->pfid1); ?><?php endif; ?>"/>
                                                                    <i id="loading3" class="fa fa-spinner fa-spin fa-3x fa-fw" style="position: absolute;left: 10%;top:10%;display: none"></i>
                                                                </div>
                                                                <a class="im btn btn-primary" href="javascript:pfid_upload()" style="text-decoration: none;margin-top: -10px;margin-left: 10px;"><i class="glyphicon glyphicon-edit"></i> Change</a>
                                                                <input type="file" id="pfid1" style="display: none"/>
                                                                <input type="hidden" id="pfid1_name"/>
                                                            </span>
                                                        </div>
													</div>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3">ID Proof 2 :</label>
											<div class="col-md-6">
												<div class="row">
													<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
														<div class="dropdown" style="margin-top: 1%;">
															<select name="pf2" class="form-control fsc-input" id="pf2">
																<option value="">Select</option>
																	<option value="State ID" <?php if($emp->pf2=='State ID'): ?> selected <?php endif; ?>>State ID</option>
																<option value="Voter Id" <?php if($emp->pf2=='Voter Id'): ?> selected <?php endif; ?>>Voter ID</option>
																<option value="Driving Licence" <?php if($emp->pf2=='Driving Licence'): ?> selected <?php endif; ?>>Driving Licence</option>
																<option value="Pan Card" <?php if($emp->pf2=='DPan Card'): ?> selected <?php endif; ?>>Pan Card</option>
																<option value="Pass Port" <?php if($emp->pf2=='Pass Port'): ?> selected <?php endif; ?>>Pass Port</option>
															</select>
														</div>
													</div>
													<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 <?php echo e($errors->has('pfid2') ? ' has-error' : ''); ?>">
                                                        <span>
                                                            <div style="width:50px;height:50px; border-radius:50%;; border: 1px solid whitesmoke ;text-align: center;position: relative;float:left;" id="image">
                                                                <img width="100%" height="100%" style="border: 1px solid #00468f;" id="preview_image_4" src="<?php if(empty($emp->pfid2)): ?> <?php echo e(asset('public/images/noimage.jpg')); ?> <?php else: ?> <?php echo e(asset('public/employeeProof2/')); ?>/<?php echo e($emp->pfid2); ?><?php endif; ?>"/>
                                                                <i id="loading4" class="fa fa-spinner fa-spin fa-3x fa-fw" style="position: absolute;left: 10%;top:10%;display: none"></i>
                                                            </div>
                                                            <a href="javascript:pfid2_upload()" style="text-decoration: none;margin-top: -10px;margin-left: 10px;" class="im btn btn-primary"><i class="glyphicon glyphicon-edit"></i> Change</a>
                                                            <input type="file" id="pfid2" style="display: none"/>
                                                            <input type="hidden" id="pfid2_name"/>
                                                        </span>
                                                     </div>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3">Resume :</label>
											<div class="col-md-6">
												<div class="row">
													<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 <?php echo e($errors->has('resume') ? ' has-error' : ''); ?>">
														<div class="dropdown" style="margin-top: 1%;">
														<label class="file-upload btn btn-primary">
														Browse for file ... <input name="resume" style="opecity:0" placeholder="Upload Service Image" id="resume" type="file">
														</label>
															<input name="resume_1" placeholder="Upload Service Image" value="<?php echo e($emp->resume); ?>" class="form-control fsc-input" id="resume_1" type="hidden">
															<?php if($errors->has('resume')): ?>
															<span class="help-block">
															<strong><?php echo e($errors->first('resume')); ?></strong>
															</span>
															<?php endif; ?>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3">Type of Agreement :</label>
											<div class="col-md-6">
												<div class="row">
													<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
														<div class="dropdown" style="margin-top: 1%;">
															<select name="type_agreement" class="form-control fsc-input" id="type_agreement">
																<option value="">Select </option>
																<option value="Hiring Letter" <?php if($emp->type_agreement=='Hiring Letter'): ?> selected <?php endif; ?>>Hiring Letter</option>
																<option value="Employment Agreement" <?php if($emp->type_agreement=='Employment Agreement'): ?> selected <?php endif; ?>>Employment Agreement</option>
															</select>
														</div>
													</div>
													<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 <?php echo e($errors->has('agreement') ? ' has-error' : ''); ?>">
														<div class="dropdown" style="margin-top: 1%;">
														    <label class="file-upload btn btn-primary">Browse for file ...
															<input name="agreement" placeholder="Upload Service Image" class="form-control fsc-input" id="agreement" type="file">
															</label>
															<input name="agreement_1" value="<?php echo e($emp->agreement); ?>" placeholder="Upload Service Image" class="form-control fsc-input" id="agreement_1" type="hidden">
															<img src="<?php echo e(asset('public/agreement')); ?>/<?php echo e($emp->agreement); ?>" alt="" id="blah-3" alt="your image" style="margin-top:10px;width:69px;">
														
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="Branch">
											<h1>Emergency Contact Info</h1>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3">Contact Person Name : </label>
											<div class="col-md-6">
												<div class="row">
													<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
														<input type="text" class="form-control fsc-input " id="firstName_1" name="firstName_1" placeholder="First" value="<?php echo e($emp->firstName_1); ?>">
													</div>
													<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
														<input type="text" maxlength="1" class="form-control  fsc-input" id="middleName_1" name="middleName_1" placeholder="Middle" value="<?php echo e($emp->middleName_1); ?>">
													</div>
													<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
														<input type="text" class="form-control  fsc-input" id="lastName_1" name="lastName_1" value="<?php echo e($emp->lastName_1); ?>" placeholder="Last">
													</div>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3">Address 1 : </label>
											<div class="col-md-6">
												<input type="text" placeholder="Address 1" class="form-control fsc-input" name="address11" id="address11" value="<?php echo e($emp->address11); ?>">
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3">Address 2:</label>
											<div class="col-md-6">
												<input type="text" class="form-control fsc-input" name="eaddress1" id="eaddress1" value="<?php echo e($emp->eaddress1); ?>"><input type="hidden" class="form-control fsc-input" name="status" id="status" value="<?php echo e($emp->status); ?>">
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3">City/State/Zip : </label>
											<div class="col-md-6">
												<div class="row">
													<div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
														<input type="text" class="form-control textonly fsc-input" id="ecity" name="ecity" placeholder="City" value="<?php echo e($emp->ecity); ?>">
													</div>
													<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
														<div class="dropdown" style="margin-top: 1%;">
															<select name="estate" id="estate" class="form-control fsc-input">
																<option value="ID">Select</option>
																<option value="ID">ID</option>
																<option value="AK">AK</option>
																<option value="AS">AS</option>
																<option value="AZ">AZ</option>
																<option value="AR">AR</option>
																<option value="CA">CA</option>
																<option value="CO">CO</option>
																<option value="CT">CT</option>
																<option value="DE">DE</option>
																<option value="DC">DC</option>
																<option value="FM">FM</option>
																<option value="FL">FL</option>
																<option value="GA">GA</option>
																<option value="GU">GU</option>
																<option value="HI">HI</option>
																<option value="ID">ID</option>
																<option value="IL">IL</option>
																<option value="IN">IN</option>
																<option value="IA">IA</option>
																<option value="KS">KS</option>
																<option value="KY">KY</option>
																<option value="LA">LA</option>
																<option value="ME">ME</option>
																<option value="MH">MH</option>
																<option value="MD">MD</option>
																<option value="MA">MA</option>
																<option value="MI">MI</option>
																<option value="MN">MN</option>
																<option value="MS">MS</option>
																<option value="MO">MO</option>
																<option value="MT">MT</option>
																<option value="NE">NE</option>
																<option value="NV">NV</option>
																<option value="NH">NH</option>
																<option value="NJ">NJ</option>
																<option value="NM">NM</option>
																<option value="NY">NY</option>
																<option value="NC">NC</option>
																<option value="ND">ND</option>
																<option value="MP">MP</option>
																<option value="OH">OH</option>
																<option value="OK">OK</option>
																<option value="OR">OR</option>
																<option value="PW">PW</option>
																<option value="PA">PA</option>
																<option value="PR">PR</option>
																<option value="RI">RI</option>
																<option value="SC">SC</option>
																<option value="SD">SD</option>
																<option value="TN">TN</option>
																<option value="TX">TX</option>
																<option value="UT">UT</option>
																<option value="VT">VT</option>
																<option value="VI">VI</option>
																<option value="VA">VA</option>
																<option value="WA">WA</option>
																<option value="WV">WV</option>
																<option value="WI">WI</option>
																<option value="WY">WY</option>
															</select>
														</div>
													</div>
													<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
														<input type="text" class="form-control zip fsc-input" id="ezipcode" maxlength="5" name="ezipcode" value="<?php echo e($emp->ezipcode); ?>" placeholder="Zip">
													</div>
												</div>
											</div>
										</div>
										
										<div class="form-group">
											<label class="control-label col-md-3">Telephone 1 :</label>
											<div class="col-md-6">
												<div class="row">
													<div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
														<input type="text" class="form-control fsc-input" id="etelephone1" name="etelephone1" placeholder="(000) 000-0000" value="<?php echo e($emp->etelephone1); ?>">
													</div>
													<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
														<div class="dropdown" style="margin-top: 1%;">
															<select name="eteletype1" id="eteletype1" class="form-control fsc-input">
															     <option value='Mobile' <?php if($emp->eteletype1=='Mobile'): ?> selected <?php endif; ?>>Mobile</option>
                                                                 <option value='Home' <?php if($emp->eteletype1=='Home'): ?> selected <?php endif; ?>>Home</option>
                                                                 <option value='Work' <?php if($emp->eteletype1=='Work'): ?> selected <?php endif; ?>>Work</option>
                                                                 <option value='Office' <?php if($emp->eteletype1=='Office'): ?> selected <?php endif; ?>>Office</option>
                                                                 <option value='Other' <?php if($emp->eteletype1=='Other'): ?> selected <?php endif; ?>>Other</option>
															</select>
														</div>
													</div>
													<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
														<input type="text" class="form-control fsc-input" maxlength="5" value="<?php echo e($emp->eext1); ?>" readonly id="eext1" name="eext1"  placeholder="Ext">
													</div>
												</div>
											</div>
										</div>
										
										<div class="form-group">
											<label class="control-label col-md-3">Telephone 2 :</label>
											<div class="col-md-6">
												<div class="row">
													<div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
														<input type="text" class="form-control fsc-input" id="etelephone2" name="etelephone2" placeholder="(000) 000-0000" value="<?php echo e($emp->etelephone2); ?>">
													</div>
													<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
														<div class="dropdown" style="margin-top: 1%;">
															<select name="eteletype2" id="eteletype2" class="form-control fsc-input">
															    <option value='Mobile' <?php if($emp->eteletype2=='Mobile'): ?> selected <?php endif; ?>>Mobile</option>
                                                                <option value='Home' <?php if($emp->eteletype2=='Home'): ?> selected <?php endif; ?>>Home</option>
                                                                <option value='Work' <?php if($emp->eteletype2=='Work'): ?> selected <?php endif; ?>>Work</option>
                                                                <option value='Office' <?php if($emp->eteletype2=='Office'): ?> selected <?php endif; ?>>Office</option>
                                                                <option value='Other' <?php if($emp->eteletype2=='Other'): ?> selected <?php endif; ?>>Other</option>
															</select>
														</div>
													</div>
													<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
														<input type="text" class="form-control fsc-input" maxlength="5" value="<?php echo e($emp->eext2); ?>" readonly id="eext2" name="eext2" placeholder="Ext">
													</div>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3">Fax :</label>
											<div class="col-md-6">
												<div class="row">
													<div class="col-md-5">
														<input type="text" class="form-control fsc-input" placeholder="(000) 000-0000" name="efax" id="efax" value="<?php echo e($emp->efax); ?>">
													</div>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3">E-mail :</label>
											<div class="col-md-6">
												<input type="text" class="form-control fsc-input" name="eemail" id="eemail" value="<?php echo e($emp->eemail); ?>">
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3">Relationship :</label>
											<div class="col-md-6">
												<input type="text" class="form-control fsc-input" name="relation" id="relation" value="<?php echo e($emp->relation); ?>">
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3">Note For Emergency :</label>
											<div class="col-md-6">
												<textarea name="comments1" id="comments1" rows="1" class="form-control fsc-input"><?php echo e($emp->comments1); ?></textarea>
											</div>
										</div>
										
									</div>
								</div>
                       <div class="tab-pane fade" id="tab5primary">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <div class="Branch">
                                    <h1>Security Information</h1>
                                 </div>
                                 <div class="form-group">
                                    <label class="control-label col-md-3">User Name :</label>
                                    <div class="col-md-5">
                                       <div class="row">
                                          <div class="col-md-12">
                                             <input id="uname" class="form-control fsc-input" placeholder="User Name" value="<?php echo e($emp->email); ?>" readonly="" name="uname"  type="text">
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label class="control-label col-md-3">Password :</label>
                                    <div class="col-md-5">
                                       <div class="row">
                                          <div class="col-md-12">
                                             <input placeholder="Password" class="form-control fsc-input" id="password" name="newpassword" value="<?php echo e(Auth::user()->newpassword); ?>" readonly="" type="password">
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <!--<div class="form-group">
                                    <label class="control-label col-md-3">Reset Days : </label>
                                    <div class="col-md-6">
                                       <div class="row">
                                          <div class="col-md-3">
                                             <select name="reset" id="reset" class="form-control fsc-input" readonly="">
                                             <option value="30" <?php if($emp->reset=='30'): ?> selected <?php endif; ?>>30</option>
                                             <option value="60" <?php if($emp->reset=='60'): ?> selected <?php endif; ?>>60</option>
                                             <option value="90" <?php if($emp->reset=='90'): ?> selected <?php endif; ?>>90</option>
                                             <option value="120" <?php if($emp->reset=='120'): ?> selected <?php endif; ?>>120</option>
                                             </select>
                                          </div>
                                          <div class="col-md-4">
                                             <input name="reset_date" id="reset_date" class="form-control fsc-input" readonly="" value="<?php echo e(Auth::user()->enddate); ?>">
                                          </div>
                                       </div>
                                    </div>
                                 </div>-->
                                 <div class="form-group">
                                    <label class="control-label col-md-3">Question 1 : </label>
                                    <div class="col-md-5">
                                       <select name="question1" id="question1" class="form-control fsc-input">
                                          <option value="">Select</option>
                                          <option value="What was your favorite place to visit as a child?">What was your favorite place to visit as a child?</option>
                                          <option value="Who is your favorite actor, musician, or artist?">Who is your favorite actor, musician, or artist?</option>
                                          <option value="What is the name of your favorite pet?">What is the name of your favorite pet?</option>
                                          <option value="In what city were you born?" selected="selected">In what city were you born?</option>
                                          <option value="What is the name of your first school?">What is the name of your first school?</option>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label class="control-label col-md-3">Answer 1 :</label>
                                    <div class="col-md-5">
                                       <input name="answer1" value="<?php echo e($emp->answer1); ?>" placeholder="" class="form-control fsc-input" id="answer1" type="text">
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label class="control-label col-md-3">Question 2 :</label>
                                    <div class="col-md-5">
                                       <select name="question2" id="question2" class="form-control fsc-input">
                                          <option value="">Select</option>
                                          <option value="What is your favorite movie?">What is your favorite movie?</option>
                                          <option value="What was the make of your first car?">What was the make of your first car?</option>
                                          <option value="What is your favorite color?" selected="selected">What is your favorite color?</option>
                                          <option value="What is your father's middle name?">What is your fathers middle name?</option>
                                          <option value="What is the name of your first grade teacher?">What is the name of your first grade teacher?</option>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label class="control-label col-md-3">Answer 2 :</label>
                                    <div class="col-md-5">
                                       <input name="answer2" value="<?php echo e($emp->answer2); ?>" placeholder="" class="form-control fsc-input" id="answer2" type="text">
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label class="control-label col-md-3">Question 3 :</label>
                                    <div class="col-md-5">
                                       <select name="question3" id="question3" class="form-control fsc-input">
                                          <option value="">Select</option>
                                          <option value="What was your high school mascot?">What was your high school mascot?</option>
                                          <option value="Which is your favorite web browser?">Which is your favorite web browser?</option>
                                          <option value="In what year was your father born?">In what year was your father born?</option>
                                          <option value="What is the name of your favorite childhood friend?" selected="selected">What is the name of your favorite childhood friend?</option>
                                          <option value="What was your favorite food as a child?">What was your favorite food as a child?</option>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label class="control-label col-md-3">Answer 3 :</label>
                                    <div class="col-md-5">
                                       <input name="answer3" value="<?php echo e($emp->answer3); ?>" placeholder="" class="form-control fsc-input" id="answer3" type="text">
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="tab-pane fade" id="tab6primary">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <div class="Branch">
                                    <h1>Coming Soon...</h1>
                                 </div>
                              </div>
                           </div>
                           <div class="tab-pane fade" id="tab8primary">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <div class="Branch">
                                    <h1>Employee Rules</h1>
                                 </div>
                                 <div class="clear clearfix"></div>
                                 <div class="responsibility-8 row">
                                    <div id="responsibility">
                                       <ul>
                                          <?php $__currentLoopData = $rules; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rule): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                          <?php if($rule->type=='Rules'): ?>
                                          <li><b><?php echo e($rule->title); ?> : </b><br><?php echo $rule->rules; ?></li>
                                          <?php endif; ?>
                                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                             	<input <?php if($emp->read=='2'): ?> checked <?php else: ?>  <?php endif; ?> type="checkbox" id="terms" name="terms" value="2">  
                                            <label for="terms" style="float:left;padding-top:2px;"> I read an acknowledge the rules of Financial Service Center and I will follow as per company's rule. </label>
                                            <div class=""><div class="col-md-2" style="width:190px;margin-top: -7px;;"><?php if($emp->rulesdate): ?><input name="rulesdate" value="<?php echo e(date("F-d-Y",strtotime($emp->rulesdate))); ?>" placeholder="" class="form-control fsc-input" id="rulesdate" readonly="" type="text"> <?php else: ?> 	<input name="rulesdate" value="<?php echo e(date("M-d-Y",strtotime(date('y-m-d')))); ?>" placeholder="" class="form-control fsc-input" id="rulesdate" readonly="" type="text"> <?php endif; ?></div>                                            
                                        <div class="col-md-2" style="margin-top: -7px;"><input name="signature" value="" placeholder="" class="form-control fsc-input" id="signature" type="text"></div>
                                       </div>
                                       </ul>
                                    </div>
                                    <div class="clear clearfix"></div>
                                 </div>
                              </div>
                           </div>
                           <div class="tab-pane fade" id="tab9primary">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <div class="Branch">
                                    <h1>Work Responsibility</h1>
                                 </div>
                                 <div class="responsibility">
                                    <div class="form-group">
                                       <div class="col-md-12">
                                          <table id="sampleTable3_wrapper" class="table table-striped table-bordered customers" style="width:100%">
                                             <thead>
                                                <tr style="text-align:center">
                                                   <th>No.</th>
                                                   <th>Creation Date</th>
                                                    <th>Frequency</th>
                                                   <th>Responsibility Subject</th>
                                                  
                                                   <th width="20%;">Responsibility From</th>
                                                   <th>Checked</th>
                                                   <th>Checked Date</th>
                                                   <th>Action</th>
                                                </tr>
                                             </thead>
                                             <tbody>
                                                <?php $__currentLoopData = $resposibilty; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $resposibilty1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                               
 
                                                 
                                                <?php if($resposibilty1->type=='Resposibilty' && $resposibilty1->employee_id==$emp->id): ?>
                                                <tr style="text-align:center">
                                                   <td><?php echo e($loop->index+1); ?></td>
                                                   <td><?php echo $resposibilty1->date; ?></td>
                                                    <td><?php echo $resposibilty1->respon_type; ?></td>
                                                   <td><?php echo $resposibilty1->title; ?></td>
                                                  
                                                   <td>Mr. Vijay Bombaywala</td>
                                                   <td><center><?php if($resposibilty1->status=='2'): ?> <img src="<?php echo e(URL::asset('public/img/green.jpg')); ?>" alt="" width="25"> <?php else: ?>  <img src="<?php echo e(URL::asset('public/img/red.jpg')); ?>" alt="" width="25"> <?php endif; ?> </center></td>
                                                   <td><?php if($resposibilty1->rulesdate): ?><?php echo e(date("m/d/Y", strtotime($resposibilty1->rulesdate))); ?><?php endif; ?></td>
                                                   <td><a href="<?php echo e(route('resposibilty.edit',$resposibilty1->id)); ?>" class="btn btn-primary"><i class="fa fa-edit"></i></a></td>
                                                </tr>
                                                <?php endif; ?>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                             </tbody>
                                          </table>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                            <div class="tab-pane fade" id="tab10primary">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    OTHER
                                </div>
                            </div>
                           
                           <div class="tab-pane fade" id="tab7primary">
                           	<div class="col-md-12">
									   <div class="Branch">
											<h1>User Rights</h1>
										</div>
									    		<div class="form-group">
											<label class="control-label col-md-3">Supervisor Name :</label>
											<div class="col-md-5">
												<div class="row">
													<div class="col-md-12">
													    <select name="question3" id="question3" class="form-control fsc-input" readonly="">
													<option value="">Select</option>
													 <?php $__currentLoopData = $super1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sup1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
													    <?php $__currentLoopData = $super; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sup): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
													<option value="<?php echo e($sup1->id); ?>" <?php if($sup->username==$sup1->id): ?> selected <?php endif; ?>><?php echo e($sup1->firstName.' '.$sup1->middleName.' '.$sup1->lastName); ?></option>
													<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
														<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
												</select>
														</div>
												</div>
											</div>
										</div>
									</div>	
									</div>
                        </div>
                     </div>
                     <div class="card-footer">
						<div class="col-md-offset-3 col-md-7">
							<div class="row">
								<div class="col-md-3">
									<input class="btn_new_save btn-primary1" name="submit" type="submit" value="Save">
								</div>
								<div class="col-md-3">
									<a class="btn_new_cancel" style="margin-left:6%" href="https://financialservicecenter.net/fscemployee">Cancel</a> 
								</div>
									<div class="col-md-3">
								</div>
							</div>
						</div>
					</div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
   </section>
</div>
</div>
</div>
</div>
<!-- copy of input fields group -->
<script src="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/d004434a5ff76e7b97c8b07c01f34ca69e635d97/src/js/bootstrap-datetimepicker.js"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>-->
<!-- copy of input fields group -->
<div class="form-group fieldGroupCopy" style="display: none;">
<div><input type="hidden" class="form-control fsc-input" name="employee[]" id="employee" value=""><div class="form-group"><label class="control-label col-md-3">Pay Rate :</label><div class="col-md-6"><div class="row"><div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"><div class="dropdown" style="margin-top: 1%;"><input name="pay_scale[]" value="" type="text" id="pay_scale"  maxlength="10" class="form-control pay_scale" /></div></div><div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"><label class="fsc-form-label">Effective Date :</label></div><div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"><input name="effective_date[]"  type="text" id="effective_date" class="form-control date1" /></div></div></div></div><div class="form-group"><label class="control-label col-md-3">Note :</label><div class="col-md-6"><textarea id="fields" name="fields[]" class="form-control fsc-input"></textarea></div></div><div class="form-group"><div class="col-md-12"><a href="javascript:void(0)" class="btn btn-danger remove pull-right"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div></div></div>
  <script>
												                	$(function() {
	
												                	$('.fedwh_1').blur(function() {
												                	    $(this).val('$' + $(this).getNum());
                                                            			});
                                                            		
                                                            		
                                                            		$('.stwh_1').blur(function() {
												                	    
                                                            			$(this).val('$' + $(this).getNum());
                                                            			});
                                                            		
                                                            	
                                                            		$('.localwh_1').blur(function() {
												                	    
                                                            			$(this).val('$' + $(this).getNum());
                                                            			});
                                                            		});
                                                            		
		
												                </script>
												           
 <script>
												                	$(function() {
	
												                	$('.pay_scale_1s').blur(function() {
												                	    //alert();
                                                            			$(this).val('$' + $(this).getNum());
                                                            			});
                                                            		});
		
                                                            		$(".paydate_1").datepicker({
                                                            			autoclose: true,
                                                            	format: "mm/dd/yyyy",
                                                            			//endDate: "today"
                                                            		});
                                                            		
                                                            			$(".review_date_1").datepicker({
			autoclose: true,
	format: "mm/dd/yyyy",
			//endDate: "today"
		});
		
		$('.pay_review_scale_1').blur(function() {
												                	    
                        $(this).val('$' + $(this).getNum());
                                                            			});
            
												                </script>
												               
<script>
$(document).on('blur','.pay_scale',function () {
    var sum = 0;
    ckq ="$"
   //var n = parseInt($(this).val().replace(/\D/g,'') || 0);
   // var n1 = n.toLocaleString();
     var vk = parseFloat($(this).val().replace( /[^\d\.]*/g, '') || 0);
    var sign3 =parseFloat(Math.round(vk * 100) / 100).toFixed(2);
    var n1 = sign3.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    var x2 = ckq+' '+n1;
$(this).val(x2);

    $('.price').each(function() {
        sum += parseFloat($(this).val().replace( /[^\d\.]*/g, '') || 0);
    });
   
});
$(document).on('blur','.additional_withholding',function () {
    var sum = 0;
    ckq ="$"
   //var n = parseInt($(this).val().replace(/\D/g,'') || 0);
   // var n1 = n.toLocaleString();
     var vk = parseFloat($(this).val().replace( /[^\d\.]*/g, '') || 0);
    var sign3 =parseFloat(Math.round(vk * 100) / 100).toFixed(2);
    var n1 = sign3.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    var x2 = ckq+' '+n1;
$(this).val(x2);

    $('.price').each(function() {
        sum += parseFloat($(this).val().replace( /[^\d\.]*/g, '') || 0);
    });
   
});

</script>
</div>
</div>
<script>
jQuery.fn.getNum = function() {
			var val = $.trim($(this).val());
			if(val.indexOf(',') > -1) {
			val = val.replace(',', '.');
			}
			var num = parseFloat(val);
			var num = num.toFixed(2);
			if(isNaN(num)) {
			num = '';
			}
			return num;
		}
		
$('.pay_scale_1').blur(function() {
			$(this).val('$' + $(this).getNum());
			
			});
			
    function changeProfile() {
        $('#file').click();
    }
    $('#file').change(function () {
        if ($(this).val() != '') {
            upload(this);

        }
    });

function upload(img) {
        var form_data = new FormData();
        form_data.append('file', img.files[0]);
        form_data.append('_token', '<?php echo e(csrf_token()); ?>');
        $('#loading2').css('display', 'block');
        $.ajax({
            url: "<?php echo e(url('additional')); ?>/" + $('#user_id11').val(),
            data: form_data,
            type: 'POST',
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.fail) {
                    $('#preview_image').attr('src', '<?php echo e(asset('public/images/noimage.jpg')); ?>');
                    alert(data.errors['file']);
                }
                else {
                    $('#file_name').val(data);
                    $('#preview_image').attr('src', '<?php echo e(asset('public/uploads')); ?>/' + data);
                }
                $('#loading2').css('display', 'none');
            },
            error: function (xhr, status, error) {
                alert(xhr.responseText);
                $('#preview_image').attr('src', '<?php echo e(asset('public/images/noimage.jpg')); ?>');
            }
        });
    }
</script>
<script>
    function photo_upload() {
        $('#photo').click();
    }
    $('#photo').change(function () {
        if ($(this).val() != '') 
        {
            upload5(this);
        }
    });
    function upload5(img) {
        var form_data = new FormData(); //alert(form_data);
        form_data.append('photo', img.files[0]);
        form_data.append('_token', '<?php echo e(csrf_token()); ?>');
        $('#loading5').css('display', 'block');
        $.ajax({
            url: "<?php echo e(url('profilephoto1')); ?>/" + $('#user_id11').val(),
            data: form_data,
            type: 'POST',
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.fail) {
                    $('#preview_image_5').attr('src', '<?php echo e(asset('public/images/noimage.jpg')); ?>');
                    alert(data.errors['file']);
                }
                else {
                    $('#photo_name').val(data);//
                    
                   // alert();
                    $('#preview_image_5').attr('src', '<?php echo e(asset('public/employeeimage')); ?>/' + data);
                }
                $('#loading5').css('display', 'none');
            },
            error: function (xhr, status, error) {
                alert(xhr.responseText);
                $('#preview_image_5').attr('src', '<?php echo e(asset('public/images/noimage.jpg')); ?>');
            }
        });
    }
</script>

<script type="text/javascript">
$(document).ready(function(){
	var maxField = 10; 
	var addButton = $('.ad'); 
	var x = 1;
	var y = <?php echo $count;?>;
	var z = x + y;
	var wrapper = $('.review-1');
	$(addButton).click(function(){ 	
		if(z < maxField){
		$(wrapper).append('<div class="review-2"><div class="input_fields_wrap_1"><div class="form-group"><label class="control-label col-md-3">'+z+' Review Days :</label><div class="col-md-6"><div class="row"><div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"><div class="dropdown" style="margin-top: 1%;"><input name="ree[]" type="hidden" value="" id="ree" class="form-control"><input type="text"  maxlength="3" class="form-control fsc-input first_rev_day" name="first_rev_day[]" id="first_rev_day'+z+'" value=""></div></div><div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"><div class="dropdown" style="margin-top: 1%;"><label class="control-label">'+z+' Review Date :</label></div></div><div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"><div class="dropdown" style="margin-top: 1%;"><input name="reviewmonth[]" readonly type="text" value="" id="reviewmonth'+z+'" class="form-control reviewmonth" readonly="readonly"></div></div></div></div></div><div class="form-group"><label class="control-label col-md-3">Comments :</label><div class="col-md-6"><div class="row"><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><textarea name="hiring_comments[]" id="hiring_comments'+z+'" rows="1" class="form-control fsc-input"></textarea></div></div></div></div><a href="javascript:void(0)" class="btn btn-danger remove pull-right "><i class="fa fa-trash-o" aria-hidden="true"></i></a></div></div></div>'); // Add field html
        var rdiv1 = '#first_rev_day'+z;
            var rdiv2 = '#reviewmonth'+z;//alert(rdiv2);
      $(rdiv1).on('keyup',function(){ 
           var hiremonth = $('#hiremonth').val();
           var gg = $(rdiv1).val();
           var reset1 = parseInt(gg); //alert(gg);
          var tt = hiremonth.split("-").reverse().join(" ");
         var t = new Date(tt); 
         
		t.setDate(t.getDate() + reset1);
		var month = "0"+(t.getMonth()+1);
		var date = "0"+t.getDate();
		const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "June","July", "Aug", "Sept", "Oct", "Nov", "Dec"];
		const d = new Date();
		month = month.slice(-2);
		date = date.slice(-2);
		var date = monthNames[t.getMonth()]+"-"+date+"-"+t.getFullYear();
		if(reset1=='')
		{
		    $(rdiv2).empty();
		}
		else
		{
           $(rdiv2).val(date);
		}
        });
    x++;
    z++;
		}
	});
	$(wrapper).on('click', '.ad', function(e){ //Once remove button is clicked
		e.preventDefault();
		$(wrapper).append(fieldHTML); //Remove field html
		x++; 
		z++;
	});  
	$(wrapper).on('click', '.remove', function(e){ //Once remove button is clicked
		e.preventDefault();
		$(this).parent().parent('.review-2').remove(); //Remove field html
		x--;
		z--; //Decrement field counter
	});
});
</script>

<script>
    function additional_attach_update() {
        $('#file_1').click();
    }
    $('#file_1').change(function () {
        if ($(this).val() != '') {
            upload1(this);
        }
    });
    function upload1(img) {
        var form_data = new FormData();
        form_data.append('file_1', img.files[0]);
        form_data.append('_token', '<?php echo e(csrf_token()); ?>');
        $('#loading1').css('display', 'block');
        $.ajax({
            url: "<?php echo e(url('additional2')); ?>/" + $('#user_id11').val(),
            data: form_data,
            type: 'POST',
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.fail) {
                    $('#preview_image_1').attr('src', '<?php echo e(asset('public/images/noimage.jpg')); ?>');
                    alert(data.errors['file']);
                }
                else {
                    $('#file_name_1').val(data);
                    $('#preview_image_1').attr('src', '<?php echo e(asset('public/uploads')); ?>/' + data);
                }
                $('#loading1').css('display', 'none');
            },
            error: function (xhr, status, error) {
                alert(xhr.responseText);
                $('#preview_image_1').attr('src', '<?php echo e(asset('public/images/noimage.jpg')); ?>');
            }
        });
    }
    function additional_attach_remove() {
        if ($('#file_name_1').val() != '')
            if (confirm('Are you sure want to remove profile picture?')) {
                $('#loading1').css('display', 'block');
                var form_data = new FormData();
                form_data.append('_method', 'DELETE');
                form_data.append('_token', '<?php echo e(csrf_token()); ?>');
                $.ajax({
                    url: "additional3/" + $('#file_name_1').val(),
                    data: form_data,
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        $('#preview_image_1').attr('src', '<?php echo e(asset('public/images/noimage.jpg')); ?>');
                        $('#file_name_1').val('');
                        $('#loading1').css('display', 'none');
                    },
                    error: function (xhr, status, error) {
                        alert(xhr.responseText);
                    }
                });
            }
    }
</script>
<script>
    function additional_attach() {
        $('#file_2').click();
    }
    $('#file_2').change(function () {
        if ($(this).val() != '') {
            upload2(this);
        }
    });
    function upload2(img) {
        var form_data = new FormData();
        form_data.append('file_2', img.files[0]);
        form_data.append('_token', '<?php echo e(csrf_token()); ?>');
        $('#loading').css('display', 'block');
        $.ajax({
            url: "<?php echo e(url('additional3')); ?>/" + $('#user_id11').val(),
            data: form_data,
            type: 'POST',
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.fail) {
                    $('#preview_image_2').attr('src', '<?php echo e(asset('public/images/noimage.jpg')); ?>');
                    alert(data.errors['file']);
                }
                else {
                    $('#file_name_2').val(data);
                    $('#preview_image_2').attr('src', '<?php echo e(asset('public/uploads')); ?>/' + data);
                }
                $('#loading').css('display', 'none');
            },
            error: function (xhr, status, error) {
                alert(xhr.responseText);
                $('#preview_image_2').attr('src', '<?php echo e(asset('public/images/noimage.jpg')); ?>');
            }
        });
    }
</script>

<script>
    function pfid_upload() {
        $('#pfid1').click();
    }
    $('#pfid1').change(function () {
        if ($(this).val() != '') {
            upload3(this);
        }
    });
    function upload3(img) {
        var form_data = new FormData();
        form_data.append('pfid1', img.files[0]);
        form_data.append('_token', '<?php echo e(csrf_token()); ?>');
        $('#loading3').css('display', 'block');
        $.ajax({
            url: "<?php echo e(url('pfidd1')); ?>/" + $('#user_id11').val(),
            data: form_data,
            type: 'POST',
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.fail) {
                    $('#preview_image_3').attr('src', '<?php echo e(asset('public/images/noimage.jpg')); ?>');
                    alert(data.errors['file']);
                }
                else {
                    $('#pfid1_name').val(data);
                    $('#preview_image_3').attr('src', '<?php echo e(asset('public/employeeProof1')); ?>/' + data);
                }
                $('#loading3').css('display', 'none');
            },
            error: function (xhr, status, error) {
                alert(xhr.responseText);
                $('#preview_image_3').attr('src', '<?php echo e(asset('public/images/noimage.jpg')); ?>');
            }
        });
    }

</script>

<script>
    function pfid2_upload() {
        $('#pfid2').click();
    }
    $('#pfid2').change(function () {
        if ($(this).val() != '') {
            upload4(this);
        }
    });
    function upload4(img) {
        var form_data = new FormData();
        form_data.append('pfid2', img.files[0]);
        form_data.append('_token', '<?php echo e(csrf_token()); ?>');
        $('#loading4').css('display', 'block');
        $.ajax({
            url: "<?php echo e(url('pfidd')); ?>/" + $('#user_id11').val(),
            data: form_data,
            type: 'POST',
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.fail) {
                    $('#preview_image_4').attr('src', '<?php echo e(asset('public/images/noimage.jpg')); ?>');
                    alert(data.errors['file']);
                }
                else 
                {
                    $('#pfid2_name').val(data);
                    $('#preview_image_4').attr('src', '<?php echo e(asset('public/employeeProof2')); ?>/' + data);
                }
                $('#loading4').css('display', 'none');
            },
            error: function (xhr, status, error) {
                alert(xhr.responseText);
                $('#preview_image_4').attr('src', '<?php echo e(asset('public/images/noimage.jpg')); ?>');
            }
        });
    }
</script>
<script>
	$(document).ready(function(){
	var maxGroup = 120;
	$(".addMore").click(function(){
	if($('body').find('.fieldGroup').length < maxGroup){
	var fieldHTML = '<div class="fieldGroup">'+$(".fieldGroupCopy").html()+'</div>';
	$('body').find('.fieldGroup:last').after(fieldHTML);
	}
	else
	{
	alert('Maximum '+maxGroup+' Persons are allowed.');
	}
	});
	$("body").on("click",".remove",function(){ 
	$(this).parents(".fieldGroup").remove();
	});
	});
</script>

<!----script---->
<?php $__currentLoopData = $review1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $re): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<div id="myModal<?php echo e($re->id); ?>" class="modal fade">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Confirmation</h4>
         </div>
         <div class="modal-body">
            <p>Do you want to delete this record ?</p>
         </div>
         <div class="modal-footer">
            <a href="<?php echo e(route('review1.reviewdelete1',$re->id)); ?>" class="btn btn-danger">Delete</a>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php $__currentLoopData = $info; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $in): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<div id="myModal1<?php echo e($in->id); ?>" class="modal fade">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Confirmation</h4>
         </div>
         <div class="modal-body">
            <p>Do you want to delete this record ?</p>
         </div>
         <div class="modal-footer">
            <a href="<?php echo e(route('pay1.paydelete1',$in->id)); ?>" class="btn btn-danger">Delete</a>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<script>
   $("#efax").mask("(999) 999-9999");
   $(".ext").mask("99999");
   $("#ext1").mask("99999");
   $("#ext2").mask("99999");
   $("#eext1").mask("99999");
   $("#eext2").mask("99999");
   $("#mobile_no").mask("(999) 999-9999");
   $("#etelephone2").mask("(999) 999-9999");
   $("#etelephone1").mask("(999) 999-9999");
   $("#computer_ip").mask("999.999.999.999");
</script>
<script>
   $(document).ready(function(){
     	$(document).on('change','.category', function()
     	{ 
     	var id = $(this).val();
     		$.get('<?php echo URL::to('/getbranchs'); ?>?id='+id, function(data)
     		{  
                $('#branch_name').empty();
                $.each(data, function(index, subcatobj)
     		   {
     			   $('#branch_name').val(subcatobj.branchname);
     		   })
     		});
     	});
     });
</script>
<script>
$('.date1').mask("99/99/9999", {placeholder: 'mm/dd/yyyy' });
</script>
<script>
    $(document).ready(function(){
        $('#reset').on('change',function(){
           var reset = parseInt($('#reset').val()); 
           var date = new Date();
         var t = new Date(); 
		t.setDate(t.getDate() + reset);
		var month = "0"+(t.getMonth()+1);
		var date = "0"+t.getDate();
		const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "June","July", "Aug", "Sept", "Oct", "Nov", "Dec"];
		const d = new Date();
		month = month.slice(-2);
		date = date.slice(-2);
		var date = monthNames[t.getMonth()]+"-"+date+"-"+t.getFullYear();
           $('#reset_date').val(date);
        });
    });
</script>
<script>
	var date = $('#ext1').val(<?php echo e($emp->ext1); ?>);
	$('#telephoneNo1Type').on('change', function() {
	if(this.value=='Office' || this.value=='Work'){
	document.getElementById('ext1').removeAttribute('readonly');
	$('#ext1').val(<?php echo e($emp->ext1); ?>);
	}
	else{
	document.getElementById('ext1').readOnly =true;
	$('#ext1').val('');
	}
	})
</script>

<script>
	var dat1 = $('#ext2').val(<?php echo e($emp->ext2); ?>);
	$('#telephoneNo2Type').on('change', function() {
	if(this.value=='Office' || this.value=='Work')
	{
	document.getElementById('ext2').removeAttribute('readonly');
	$('#ext2').val(<?php echo e($emp->ext2); ?>); 
	}
	else
	{
	document.getElementById('ext2').readOnly =true;
	$('#ext2').val('');
	}
	})
</script>

<script>
	$('#eteletype2').on('change', function() {
	if(this.value=='Office' || this.value=='Work')
	{
	$('#eext2').val(<?php echo e($emp->eext2); ?>);
	document.getElementById('eext2').removeAttribute('readonly');
	}
	else
	{
	document.getElementById('eext2').readOnly =true;
	$('#eext2').val('');
	}
	})
</script>
<script>
	var dat2 = $('#eext1').val(<?php echo e($emp->eext1); ?>);
	$('#eteletype1').on('change', function() {
	if(this.value=='Office' || this.value=='Work')
	{
	document.getElementById('eext1').removeAttribute('readonly');
	$('#eext1').val(<?php echo e($emp->eext1); ?>);
	}
	else
	{
	document.getElementById('eext1').readOnly =true;
	$('#eext1').val('');
	}
	})
</script>
<script>
    $(document).ready(function(){
        $('#first_rev_day').on('keyup',function(){
           var hiremonth = $('#hiremonth').val();
           var reset1 = parseInt($('#first_rev_day').val()); 
          var tt = hiremonth.split("-").reverse().join(" ");
         var t = new Date(tt); 
		t.setDate(t.getDate() + reset1);
		var month = "0"+(t.getMonth()+1);
		var date = "0"+t.getDate();
		const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "June","July", "Aug", "Sept", "Oct", "Nov", "Dec"];
		const d = new Date();
		month = month.slice(-2);
		date = date.slice(-2);
		var date = monthNames[t.getMonth()]+"-"+date+"-"+t.getFullYear();
		if(reset1=='')
		{
		    $('#reviewmonth').empty();
		}
		else
		{
           $('#reviewmonth').val(date);
		}
        });
    });
    
    $(document).ready(function(){
    $("select#check").change(function(){
        var selectedCountry = $(this).children("option:selected").val();
        var color = $("option:selected", this).attr("class");
          $("#check").attr("class", color).addClass("form-control1 fsc-input");
    });
});
</script>
<div id="alertss" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content" style="background-color: #ededed;">
      <div class="modal-header">
  <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Save</h4>
      </div>
      <div class="modal-body">
        <p>Employee Profile Successfully Saved.</p>
      </div>
      <div class="modal-footer">
        <button data-dismiss="modal" class="btn btn-default" style="background-color: #bfbfbf;">Close</button>
      </div>
    </div> 
  </div>
</div>
<div id="alertss1" class="modal fade" role="dialog" >
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content" style="background-color: #ededed;">
      <div class="modal-header">
  <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Save</h4>
      </div>
      <div class="modal-body">
        <p>Employee Profile not Successfully Saved.</p>
      </div>
      <div class="modal-footer">
        <button data-dismiss="modal" class="btn btn-default" style="background-color: #bfbfbf;">Close</button>
      </div>
    </div> 
  </div>
</div>

<div id="alerts" class="modal fade" role="dialog" >
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content model-width" style="background-color: #f3f3f3;">
      <div class="modal-header">
  <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Save changes</h4>
      </div>
      <div class="modal-body">
        <center><p>Do you want to save the changes ?</p></center>
      </div>
      <div class="modal-footer">
         <button type="submit" class="btn btn-default primary1 primary" style="">Yes</button>
         <button type="button" id="button" class="btn btn-default primary">No</button>
         <a class="btn btn-default primary no-button">Cancel</a>
         </div>
    </div> 

  </div>
</div>
<script>
$('select').on('change', function() {
    $('#text1').val(this.value);
});
   jQuery(function($) {
  var input = $('input,textarea,select');
  input.on('keydown', function() {
    var key = event.keyCode || event.charCode || event.which;
if( key == 8 || key == 46 ){
   $('#text1').val(key);
    }
     else{   
         $('#text1').val(key);
     }
  });
});
$(document).ready(function(){
$('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        var currentTab = $(e.target).text(); // get current tab
        var vf = $('#text1').val();//alert(vf);
        var current_tab = e.target;
        var previousTab = $(e.relatedTarget).text(); 
        var target = $(e.relatedTarget).attr("href");
        $('.no-button').eq(0).attr('href',target);
        var href = $('.no-button').attr('href');
     if(target==href){ 
        if(vf)
        {// alert('2');
         $("#alerts").modal({
		show: true,
	});
        } 
        else
        {
        }
     }
     else
     { 
     }
    });
});

$(function() {
    $('#login-form-link').click(function(e) {
		$("#login-form").delay(100).fadeIn(100);
 		$("#register-form").fadeOut(100);
		$('#register-form-link').removeClass('active');
		$(this).addClass('active');
		e.preventDefault();
	});
	$('#register-form-link').click(function(e) {
		$("#register-form").delay(100).fadeIn(100);
 		$("#login-form").fadeOut(100);
		$('#login-form-link').removeClass('active');
		$(this).addClass('active');
		e.preventDefault();
	});
});

$(document).on('click', '.no-button', function () {
    var href = $(this).attr('href');
     var $link = $('li.active a[data-toggle="tab"]');
    $link.parent().removeClass('active');
    var tabLink = $link.attr('href');//alert(tabLink);
     $('#alerts').modal('hide');
    $('#myTab a[href="' + href + '"]').tab('show');
});
</script>
<script>
$(function () {
        $('.primary1').click(function () {  //alert(newopt);
        $.ajax({
        type: "post",
        url: "<?php echo e(route('employeeprofile.update',Auth::user()->id)); ?>",
        dataType: "json",
        data: $('#registrationForm').serialize(),
        success: function(data){
            $('#alerts').modal('hide');
            $('#alertss').modal('show');
           $('#text1').val('');
        },
        error: function(data){
            $('#alerts').modal('hide');
            $('#alertss1').modal('show');
        }
    });
        });
            });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#button").click(function(){
            location.reload(true);
         });
    });
    $(".date1").datepicker({
		autoclose: true,
format: "mm/dd/yyyy",
		//endDate: "today"
	});
</script>
<?php 
if($employee->type !='user')
{?>
<script>

var countinfo='<?php echo $countsinfo;?>';
 var cnts1=countinfo + 1;
 

    $(document).on('click', '#add_pay', function () {
        cnts1++;
	   $(".addpayinfo").append('<tr><td><select class="form-control" name="pay_method[]"><option value="">Select</option><option value="Salary">Salary</option><option value="Hourly">Hourly</option></select></td><td><select class="form-control" name="pay_frequency[]"><option value="">Select</option><option value="Weekly">Weekly</option><option value="Bi-Weekly">Bi-Weekly</option><option value="Semi-Monthly">Semi-Monthly</option><option value="Monthly">Monthly</option></select></td> <td><input type="text" class="form-control text-center pay_scale_'+cnts1+'" name="pay_scale[]"></td> <td><input type="text" class="form-control paydate_'+cnts1+'" name="effective_date[]"></td> <td><textarea cols="40" rows="1" class="form-control" name="fields[]"></textarea></td> <td style="text-align:center;"> <a class="btn-action btn-delete btn btn-danger"><i class="fa fa-trash remPay" ></i></a> </td> </tr>');
		$('.pay_scale_'+cnts1).blur(function() {
			$(this).val('$' + $(this).getNum());
			
			});
				$(".paydate_"+cnts1).datepicker({
			autoclose: true,
	format: "mm/dd/yyyy",
			//endDate: "today"
		});
	
        
    });
   	$(".addpayinfo").on('click','.remPay',function(){
        $(this).parent().parent().parent().remove();
    });
</script>
<?php } 
?>
<script>
var reviewcountinfo='<?php echo $countreviewinfo;?>';
var cnts2=reviewcountinfo + 1;
 	  
$(document).on('click', '#add_review', function () {
     cnts2++;

	$(".addreviewinfo").append('<tr><td></td><td><input type="text" name="review_date[]" class="form-control review_date_'+cnts2+'"></td> <td><input type="text" name="review_day[]" class="form-control text-center review_day_'+cnts2+'"></td> <td><input type="text" readonly name="next_review_date[]" class="form-control next_review_date_'+cnts2+'"></td> <td><input type="text" name="review_rate[]" class="form-control text-center pay_review_scale_'+cnts2+'"></td> <td><textarea cols="40" rows="1" class="form-control" name="review_comments[]"></textarea></td> <td style="text-align:center;"> <a class="btn-action btn-delete btn btn-danger"><i class="fa fa-trash remReview"></i></a> </td> </tr>');
        $('.pay_review_scale_'+cnts2).blur(function() {
			$(this).val('$' + $(this).getNum());
			
			});
		$(".review_date_"+cnts2).datepicker({
			autoclose: true,
	        format: "mm/dd/yyyy",
			//endDate: "today"
		});
		
		 (function($, window, document, undefined){
                                                $(".review_day_"+cnts2).blur(function(){
                                                   var date = new Date($(".review_date_"+cnts2).val()),
                                                       days = parseInt($(".review_day_"+cnts2).val(), 10);
                                                    
                                                    if(!isNaN(date.getTime())){
                                                        date.setDate(date.getDate() + days);
                                                        $(".next_review_date_"+cnts2).val(date.toInputFormat());
                                                    } else {
                                                        alert("Invalid Date");  
                                                    }
                                                });
                                                
                                                
                                                //From: http://stackoverflow.com/questions/3066586/get-string-in-yyyymmdd-format-from-js-date-object
                                                Date.prototype.toInputFormat = function() {
                                                   var yyyy = this.getFullYear().toString();
                                                   var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
                                                   var dd  = this.getDate().toString();
                                                //   return yyyy + "-" + (mm[1]?mm:"0"+mm[0]) + "-" + (dd[1]?dd:"0"+dd[0]); // padding
                                                   return (mm[1]?mm:"0"+mm[0]) + "/" + (dd[1]?dd:"0"+dd[0]) + "/" + yyyy; // padding
                                                };
                                            })(jQuery, this, document);
	
	

	});
	$(".addreviewinfo").on('click','.remReview',function(){
        $(this).parent().parent().parent().remove();
    });
    
var whcount='<?php echo $whcount;?>';
//alert(whcount);
var cnts3=parseFloat(whcount) + parseFloat(1);
   // alert(cnts3);
    $(document).on('click', '#add_tax', function () {
	  cnts3++;
	    $(".addtaxinfo").append('<tr><td><select class="form-control" name="year_wh[]"><option value="">Select</option><option value="2021">2021</option></select></td><td><select name="fillingstatus[]" id="fillingstatus" class="form-control tttt"> <option value="Single">Single</option> <option value="Married File Jointly" selected="">Married File Jointly</option> <option value="Married File Separately">Married File Separately</option> <option value="Head of Household">Head of Household</option> </select> </td> <td><input name="fed_claim[]" class="form-control text-center"></td> <td><input class="form-control text-center fedwh_'+cnts3+'" name="fed_wh[]"></td><td><input class="form-control text-center" name="st_claim[]"></td> <td><input class="form-control text-center stwh_'+cnts3+'" name="st_wh[]"></td> <td><input class="form-control text-center" name="local_claims[]"></td> <td><input class="form-control text-center localwh_'+cnts3+'" name="local_wh[]"></td> <td style="text-align:center;"> <a class="btn-action btn-delete btn btn-danger"><i class="fa fa-trash remTax"></i></a> </td> </tr>');
	
        $('.fedwh_'+cnts3).blur(function() {
			$(this).val('$' + $(this).getNum());
			
			});
			
			$('.stwh_'+cnts3).blur(function() {
			$(this).val('$' + $(this).getNum());
			
			});
		
		$('.localwh_'+cnts3).blur(function() {
			$(this).val('$' + $(this).getNum());
			
			});
	
    });
	$(".addtaxinfo").on('click','.remTax',function(){
        $(this).parent().parent().parent().remove();
    });
    
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('fscemployee.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>