<?php $__env->startSection('main-content'); ?>

<div class="content-wrapper">
 <section class="page-title content-header">
     		  <h1>Supervisor  </h1>
    </section>
    <!-- Main content -->
    <section class="content">
   <div class="row">
      <div class="col-md-12">
         <div class="box box-success" style="padding-top:15px;">
            <div class="card-body col-md-12">
               <form method="post" action="<?php echo e(route('supervisor.update',$task->id)); ?>" class="form-horizontal" id="content" name="content" enctype="multipart/form-data">
                  <?php echo e(csrf_field()); ?> <?php echo e(method_field('PATCH')); ?>

    <div class="form-group<?php echo e($errors->has('supervisorname') ? ' has-error' : ''); ?>">
                     <label class="control-label col-md-3">Supervisor Name :</label>
                     <div class="col-md-5">
                        <div class="">
                          <select name="supervisorname" id="supervisorname" class="form-control fsc-input">
                            <option value="">--- Select---</option>    
                            <?php $__currentLoopData = $emp; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ep): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($ep->id); ?>" <?php if($task->username==$ep->id): ?> selected <?php endif; ?>><?php echo e($ep->firstName.' '.$ep->middleName.' '.$ep->lastName); ?></option>    
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                           </select>
                        </div>
                        <?php if($errors->has('supervisorname')): ?>
                        <span class="help-block">
                        <strong><?php echo e($errors->first('supervisorname')); ?></strong>
                        </span>
                        <?php endif; ?>	
                     </div>
                  </div>
                  
                  <div class="form-group<?php echo e($errors->has('supervisorcode') ? ' has-error' : ''); ?>">
                     <label class="control-label col-md-3">Supervisor Code :</label>
                     <div class="col-md-5">
                        <div class="">
                          <input type="password" name="supervisorcode" id="password-field"  value="<?php echo e($task->code); ?>" class="form-control fsc-input">
                           <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                        </div>
                        <?php if($errors->has('supervisorcode')): ?>
                        <span class="help-block">
                        <strong><?php echo e($errors->first('supervisorcode')); ?></strong>
                        </span>
                        <?php endif; ?>	
                     </div>
                  </div>
                  
                    <div class="card-footer">
                        <div class="row">
                            <label class="control-label col-md-3"></label>
                            <div class="col-xs-2" style="width:auto;">
                                <input class="btn_new_save btn-primary1" type="submit" name="submit" value="save" style="padding:8px 25px;">
                            </div>
                            <div class="col-xs-2" style="width:auto;">
                                <a class="btn_new_cancel" href="<?php echo e(url('/fac-Bhavesh-0554/supervisor')); ?>" style="padding:8px 25px;">Cancel</a> 
                            </div>
                        </div>
                    </div>
                   
               </form>
            </div>
         </div>
      </div>
   </div>
   </section>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>