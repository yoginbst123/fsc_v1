<?php $__env->startSection('main-content'); ?>
<style>
label{float:left;}
.dataTables_filter{display:none;}
.dt-buttons{
    margin-bottom:10px;
}
.page-title{
        padding: 8px 19px !important;
}
.buttons-pdf {
    font-size: 0px !important;
    border-radius: 3px;
    padding: 8px 10px !important;
    background: #fff !important;
    border-color: #c6c6c6  !important;
    color:red !important;
}
.buttons-print {
    font-size: 0px !important;
    border-radius: 3px;
    padding: 8px 10px !important;
    background: #3c8dbc !important;
            border-color: #367fa9 !important;
}
.buttons-excel{
    font-size:0px !important;
    border-radius: 3px;
        padding: 8px 10px !important;
        
         background: #00a65a !important;
    border-color: #008d4c !important;
        

}
.buttons-excel:hover{
     background: #008d4c !important;

}
.buttons-pdf:hover{
     background: #f6f6f6  !important;
}

.buttons-print:hover{
     background: #367fa9 !important;
}


.fa{
    font-size: 16px !important;
}
.imgicon{background: #fff; display: block;  width:35px; height:35px; float: left;  margin-right: 10px; float:left; margin-right:10px; margin-top:-6px; border-radius:2px; padding:5px 3px; border:1px solid #12186b;}
.imgicon img{max-width:100%;}
@media (max-width:991px){
    .dt-buttons {
        margin-top: -38px;
        position: absolute;
        right: 15px;
    }
}
@media (max-width: 500px){
    .dt-buttons {
        margin-top: 10px;
        position: initial;
    }
}
</style>
<div class="content-wrapper">
     <section class="content-header page-title" style="height:40px;">
     		<div>
     		    <div class="" style="text-align:center;">
     		        <h2>List of Client Employee <span class="right_title" style="text-align:right;padding-right: 20px;position: absolute;right: 0;"> View / Edit</span></h2>
     		    </div>
     		    
     		</div>
    </section>
    
      <section class="content">
   <div class="row">
      <div class="col-md-12">
         <div class="box box-success">
                <div class="box-header" style="padding-bottom: 3px;">
                    <div class="col-md-12 resp_abs">
                        <div class="row">
                            <div class="col-md-3 cs1">
                                <label style="margin-top: 11px;margin-right:10px;">Filter: </label>
                                <select name="choice" style="width: 92%;margin-left: 4px;" id="choice" class="form-control">
                                    <option value="1">All</option>
                                    <option value="Active" selected>Active</option>
                                    <option value="Approval">Approval</option>
                                    <option value="Pending">Pending</option>
                                    <option value="Hold">Hold</option>
                                    <option value="In-Active">Inactive</option>
                                </select>
                            </div>
                            <div class="col-md-4 cs2">
                                <label style="margin-top: 11px;margin-right:10px;">Search: </label>
                                <select name="types" style="width: 92%;margin-left: 4px;" id="types" class="form-control">
                                    <option value="All" selected>All</option>
                                    <option value="Type">Client ID</option>
                                    <option value="EE / User ID">Employee ID</option>
                                    <option value="Employee Name">Employee Name</option>
                                    <option value="Email ID">Email ID</option>
                                    <option value="Tel. Number">Tel. Number</option>
                                </select>
                            </div>
                            <div class="col-md-3 cs3" style="">
                                <table style="width: 100%; margin: 0 auto 0 10px;" cellspacing="0" cellpadding="3" border="0">
                                    <tbody>
                                        <tr id="filter_global">
                                            <td align="center"><input type="text" class="global_filter form-control" id="global_filter" placeholder="All Search"></td>
                                        </tr>
                                        <tr id="filter_col2" data-column="1" style="display:none">
                                            <td><input type="text" class="column_filter form-control" id="col1_filter" placeholder="Client ID"></td>
                                        </tr>
                                        <tr id="filter_col3" data-column="2" style="display:none">
                                            <td ><input type="text" class="column_filter form-control" id="col2_filter" placeholder="Employee ID"></td>
                                        </tr>
                                        <tr id="filter_col4" data-column="3" style="display:none">
                                            <td align="center"><input type="text" class="column_filter form-control" id="col3_filter" placeholder="Employee Name"></td>
                                        </tr>
                                        <tr id="filter_col5" data-column="4" style="display:none">
                                            <td align="center"><input type="text" class="column_filter form-control" id="col4_filter" placeholder="Email ID"></td>
                                        </tr>
                                        <tr id="filter_col6" data-column="5" style="display:none">
                                            <td align="center"><input type="text" class="column_filter form-control" id="col5_filter" placeholder="Tel.Number"></td>
                                        </tr>
                                        <tr id="filter_col7" data-column="6" style="display:none">
                                            <td align="center"><input type="text" class="column_filter form-control" id="col6_filter" placeholder="Contact Telephone"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
             
            <div class="col-md-12">
               <?php if( session()->has('success') ): ?>
               <div class="alert alert-success alert-dismissable"><?php echo e(session()->get('success')); ?></div>
               <?php endif; ?>
               <div class="table-responsive">
                  <table class="table table-hover table-bordered" id="example">
                     <thead>
                        <tr>
                            <th width="5%">No </th>
                            <th width="12%">Client ID </th>
                            <th width="12%">Employee ID </th>
                            <th width="26%">Employee Name</th>
                            <th width="26%">Email ID</th>
                            <th width="12%">Telephone</th>
                            <th width="11%">Status</th>
                            <th width="10%">Action</th>
                        </tr>
                     </thead>
                     <tbody>
<?php $__currentLoopData = $employee; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $employ): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<td style="text-align:center;"> </td>
                        <td style=><?php $__currentLoopData = $customer; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $employ1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php if($employ1->id==$employ->clienttype): ?> <?php echo e($employ1->filename); ?> <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></td>
                           <td><?php echo e($employ->employee_id); ?></td>
                           <td><?php echo e($employ->firstName); ?> <?php echo e($employ->middleName); ?> <?php echo e($employ->lastName); ?></td>
                           <td><?php echo e($employ->email); ?></td>
                           <td><?php echo e($employ->telephoneNo1); ?></td>
                           <td style="    text-align: center;"><?php if($employ->check==1): ?><a class="btn-action btn-view-edit" style="background:#689203 !important; text-align:center"  href="">Active</a> <?php else: ?> <a class="btn-action btn-delete" href="">Inactive</a> <?php endif; ?></td>
                           <td style="text-align:center;">
<?php if($employ->newemp==1): ?><a  style="background: #367fa9 !important" href="<?php echo e(route('clientemployee.edit',$employ->cid)); ?>"><img src="<?php echo e(asset('public/dashboard/images/newimage.gif')); ?>" alt="" width="50px"></a><br><?php endif; ?>
<a class="btn-action btn-view-edit" href="<?php echo e(route('clientemployee.edit',$employ->id)); ?>"><i class="fa fa-edit"></i></a>
                                    <form action="<?php echo e(route('clientemployee.destroy',$employ->id)); ?>" method="post" style="display:none" id="delete-id-<?php echo e($employ->id); ?>">
                                        <?php echo e(csrf_field()); ?> <?php echo e(method_field('DELETE')); ?>

                                    </form>
				<a class="btn-action btn-delete" href="#" onclick="if(confirm('Are you sure, You want to delete this record ?'))
                                                                            {event.preventDefault();document.getElementById('delete-id-<?php echo e($employ->id); ?>').submit();} else{event.preventDefault();}"><i class="fa fa-trash"></i></a>
								</td>
                        </tr>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
    </section>
<!--</div>-->

<script>
$(document).ready(function() {
    var table = $('#example').DataTable( {
        dom: 'Bfrtlip',
    "columnDefs": [ {
            "searchable": true,
            "orderable": true,
            "targets": 0
        } ],
        "order": [[ 0, 'asc' ]],
        buttons: [
                 {
                extend:    'copyHtml5',
                text:      '<i class="fa fa-files-o"></i> &nbsp; Copy',
                titleAttr: 'Copy',
                title: $('h3').text(),
                  exportOptions: {
        columns: [0,1, 2, 3,4,5], // Only name, email and role
    }
            },
            {
                extend:    'excelHtml5',
                text:      '<i class="fa fa-file-excel-o"></i>&nbsp; Excel',
               titleAttr: 'Excel',
                title: $('h3').text(),
                 customize: function( xlsx ) {
                var sheet = xlsx.xl.worksheets['sheet1.xml'];
 $('row c', sheet).attr('s', '51');
            },
                  exportOptions: {
        columns: [0,1, 2, 3,4,5], // Only name, email and role
    }
            },
            {
                extend:    'csvHtml5',
                text:      '<i class="fa fa-file-text-o"></i> &nbsp; CSV',
                titleAttr: 'CSV',
                title: $('h3').text(),
                  exportOptions: {
        columns: [0,1, 2, 3,4,5], // Only name, email and role
    }
            },
            {
                extend:    'pdfHtml5',
                text:      '<i class="fa fa-file-pdf-o"></i>&nbsp;  PDF',
                
              customize: function (doc) {
						//Remove the title created by datatTables
						doc.content.splice(0,1);
						//Create a date string that we use in the footer. Format is dd-mm-yyyy
						var now = new Date();
						var jsDate = now.getDate()+'-'+(now.getMonth()+1)+'-'+now.getFullYear();
						
						doc.pageMargins = [20,60,20,20];
						doc.defaultStyle.fontSize = 10;
						doc.styles.tableHeader.fontSize = 10;
						doc['header']=(function() {
							return {
								columns: [{
									    alignment: 'left',
										image: logo,
										width: 50,margin: [200,5]
									},{
										alignment: 'CENTER',
										text: 'List of Client',
										fontSize: 20,
										margin: [10,35],
									},],
								margin: [20, 0, 0,12],alignment: 'center',
							}
						});
						var objLayout = {};
						objLayout['hLineWidth'] = function(i) { return 2; };
						objLayout['vLineWidth'] = function(i) { return 2; };
						objLayout['hLineColor'] = function(i) { return '#ccc'; };
						objLayout['vLineColor'] = function(i) { return '#ccc'; };
						objLayout['paddingLeft'] = function(i) { return 14; };
						objLayout['paddingRight'] = function(i) { return 14; };
						doc.content[0].layout = objLayout;
				},
				titleAttr: 'PDF',
                  exportOptions: {
        columns: [0,1, 2, 3,4,5], // Only name, email and role
    }
            },
            {
           extend: 'print',
          text: '<i class="fa fa-print"></i>&nbsp; Print',
          titleAttr: 'Print',
        customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '10pt' )
                        .prepend(
                            '<center><img src=""/></center>'
                        );
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );
                },
         exportOptions: {
          columns: [0,1, 2, 3,4,5]
      },
      footer: true,
      autoPrint: true
    },],
    } );
$('input.global_filter').on( 'keyup click', function () {
        filterGlobal();
    } );
 
    $('input.column_filter').on( 'keyup click', function () {
        filterColumn( $(this).parents('tr').attr('data-column') );
    } );
  table.on( 'order.dt search.dt', function () {
       table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
          cell.innerHTML = i + 1;
          table.cell(cell).invalidate('dom'); 
       } );
    } ).draw();
       table.columns(6)
        .search('^(?:(?!Inactive|New|1).)*$\r?\n?', true, false)
        .draw();
    $("#choice").on("change",function(){
 var _val = $(this).val();//alert(_val);
 
 if(_val == 'Inactive'){   
        table.columns(6).search(_val).draw();
  }
 else if(_val == 'New'){   
        table.columns(6).search(_val).draw();
  }
  else if(_val == 'Active'){  //alert();
         table.columns(6).search(_val).draw();
          table.columns(6)
        .search('^(?:(?!Inactive|New|1).)*$\r?\n?', true, false)
        .draw();
  }
  else{
        table
        .columns()
        .search('')
        .draw(); 
  }
  })
} );
   	
function filterGlobal () {
    $('#example').DataTable().search(
        $('#global_filter').val(),
        $('#global_regex').prop('checked'),
        $('#global_smart').prop('checked')
    ).draw();
}
 
function filterColumn ( i ) {
    $('#example').DataTable().column( i ).search(
        $('#col'+i+'_filter').val(),
        $('#col'+i+'_regex').prop('checked'),
        $('#col'+i+'_smart').prop('checked')
    ).draw();
}


$( "#types" ).on('change',function() {
  // For unique choice
  var selVal = $( "#types option:selected" ).val(); 
   if(selVal=='Type')  
  {
      $('#filter_global').hide();
      $('#filter_col3').hide();
      $('#filter_col4').hide();
      $('#filter_col5').hide();
      $('#filter_col6').hide();
      $('#filter_col7').hide();
      $('#filter_col2').show();
  }
  else if(selVal=='EE / User ID')  
  {
        $('#filter_col7').hide();
      $('#filter_global').hide();
      $('#filter_col2').hide();
      $('#filter_col4').hide();
      $('#filter_col5').hide();
      $('#filter_col6').hide();
      $('#filter_col3').show();
  }
    else if(selVal=='Employee Name')  
  {
      $('#filter_global').hide();
      $('#filter_col2').hide();
      $('#filter_col3').hide();
      $('#filter_col5').hide();
      $('#filter_col6').hide();
      $('#filter_col4').show();  $('#filter_col7').hide();
  }
   else if(selVal=='Email ID')  
  {
      $('#filter_global').hide();
      $('#filter_col2').hide();  $('#filter_col7').hide();
      $('#filter_col3').hide();
      $('#filter_col4').hide();
      $('#filter_col6').hide();
      $('#filter_col5').show();
  }
  else if(selVal=='Tel. Number')  
  {
      $('#filter_global').hide();
      $('#filter_col2').hide();
      $('#filter_col3').hide();
      $('#filter_col4').hide();
      $('#filter_col5').hide();  $('#filter_col7').hide();
      $('#filter_col6').show();
  }
  else if(selVal=='Tel. Number')  
  {
      $('#filter_global').hide();
      $('#filter_col2').hide();
      $('#filter_col3').hide();
      $('#filter_col4').hide();
      $('#filter_col5').hide();  $('#filter_col6').hide();
      $('#filter_col7').show();
  }
  else{
      $('#filter_global').show();
       $('#filter_col3').hide();
      $('#filter_col4').hide();
      $('#filter_col5').hide();
      $('#filter_col6').hide();  $('#filter_col7').hide();
      $('#filter_col2').hide();
  }
});
</script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>