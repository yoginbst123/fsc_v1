<?php $__env->startSection('main-content'); ?>
<style>
.ui-timepicker-container{ z-index:999999 !important}
#sch_start_date,#sch_end_date,#schedule_in_time,#schedule_out_time{ text-align:center;}
.content-wrapper{z-index:99999999!important; position:relative;}

</style>
<div class="content-wrapper">
     <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h2>Add Holiday</h2>
    </section>
    <!-- Main content -->
    <section class="content">
  
   <div class="row">
      <div class="col-md-12">
         <div class="box box-success">
			      <div class="box-header">
            
              <div class="box-tools pull-right">
                
              </div>
            </div>
            <div class="card-body">
               <form method="post" action="<?php echo e(route('holiday.store')); ?>" class="form-horizontal" id="holiday" name="holiday" autocomplete="off">
                  <?php echo e(csrf_field()); ?>   
                  
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 <?php echo e($errors->has('holiday_name') ? ' has-error' : ''); ?>">
                     <div class="form-group">
							<label class="control-label col-md-3 left_991">Holiday Name : <span class="star-required">*</span></label>

                     <div class="col-lg-5 col-md-8">
                        <input type="text" class="form-control fsc-input"  id="holiday_name"  name='holiday_name' placeholder="Holiday Name"><?php if($errors->has('holiday_name')): ?>
                        <span class="help-block">
                        <strong><?php echo e($errors->first('holiday_name')); ?></strong>
                        </span>
                        <?php endif; ?>
                     </div>

                  </div>
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 <?php echo e($errors->has('holiday_date') ? ' has-error' : ''); ?>">
                     <div class="form-group">
							<label class="control-label col-md-3 left_991">Holiday Date: <span class="star-required">*</span></label>
 
                     <div class="col-lg-5 col-md-8">
                      <input type="text" readonly class="form-control txtOnly fsc-input startdate"  style="text-align:left"  id='holiday_date' name='holiday_date' placeholder="mm/dd/yyyy">
                        <?php if($errors->has('holiday_date')): ?>
                        <span class="help-block">
                        <strong><?php echo e($errors->first('holiday_date')); ?></strong>
                        </span>
                        <?php endif; ?>
                  </div>
             </div>
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    
    <div class="card-footer">
						    <div class="form-group">
							<label class="control-label col-md-3"></label>
							<div class="col-xs-2" style="width:155px;">
<input class="btn_new_save btn-primary1" type="submit" name="submit" value="Save">
							</div>
							<div class="col-xs-2" style="width:155px;">
<a class="btn_new_cancel" href="<?php echo e(url('fac-Bhavesh-0554/holiday')); ?>">Cancel</a>
							</div>
						</div>
						  </div>             
                 
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                     <div class="" id="Register"></div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
  </section>
<!--</div>-->
<!--</div>-->
<!--</div>-->
   




 

<script>
  $(document).ready(function(){ 

$("#holiday_date").datepicker({
format: "M-d yyyy",startDate: new Date()});
});

</script>



<?php $__env->stopSection(); ?>


<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>