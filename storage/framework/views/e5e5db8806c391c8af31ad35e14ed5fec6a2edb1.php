<?php $__env->startSection('main-content'); ?>
<div class="col-lg-9 col-md-8 col-sm-12 col-xs-12" style="padding:0px;">
<?php $__currentLoopData = $homecontent; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $home): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	<div class="well well-lg" style="margin-top: 1%;">
		<h4 class="fsc-home-text-head" style='text-align:left'><?php echo e($home->title); ?></h4>
		<h4 class="fsc-home-text"><?php echo $home->content; ?></h4>
	</div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>	
	<div class="fsc-well" style="margin-top:3%; width:100%;">
		<a href="https://financialservicecenter.net/what-we-do" class="fsc-home-wedo-head">This is what we do.</a>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front-section.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>