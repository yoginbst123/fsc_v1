<?php $__env->startSection('main-content'); ?>
<style>
   label{float:right}
   .ui-timepicker-container{ z-index:999999 !important}
</style>
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="page-title content-header">
      <h1>Edit Schedule</h1>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="box box-success">
               <div class="box-header">
                  <div class="box-tools pull-right">
                  </div>
               </div>
               <div class="card-body col-md-12">
                  <form method="post" action="<?php echo e(route('schedule.update',$schedule->id)); ?>" class="form-horizontal" id="content" name="content" enctype="multipart/form-data">
                     <?php echo e(csrf_field()); ?> <?php echo e(method_field('PATCH')); ?>                     
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 <?php echo e($errors->has('emp_city') ? ' has-error' : ''); ?>">
                        <div class="form-group">
                           <label class="control-label col-lg-3 col-md-4 left_991">Employer City : <span class="star-required">*</span></label>
                           <div class="col-lg-5 col-md-8 col-sm-12 col-xs-12 fsc-element-margin">
                              <div class="row">
                                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                    <select class="form-control category" name="emp_city" id="emp_city">
                                       <option value=" ">Select Employer City</option>
                                       <?php $__currentLoopData = $branch; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bran): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                       <option value="<?php echo e($bran->city); ?>" <?php if($schedule->emp_city==$bran->city): ?> selected <?php endif; ?>><?php echo e($bran->city); ?></option>  
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                   
                                    </select>
                                    <?php if($errors->has('emp_city')): ?>
                                    <span class="help-block">
                                    <strong><?php echo e($errors->first('emp_city')); ?></strong>
                                    </span>
                                    <?php endif; ?>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 <?php echo e($errors->has('emp_name') ? ' has-error' : ''); ?>">
                        <div class="form-group">
                           <label class="control-label col-lg-3 col-md-4 left_991">Employee Name : <span class="star-required">*</span></label>
                           <div class="col-lg-5 col-md-8 col-sm-12 col-xs-12 fsc-element-margin">
                              <div class="row">
                                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                    <select class="form-control emp" name="emp_name" id="emp_name">
                                       <option value="<?php echo e($schedule->emp_name); ?>" selected><?php echo e($emp->firstName); ?></option>
                                    </select>
                                    <?php if($errors->has('emp_name')): ?>
                                    <span class="help-block">
                                    <strong><?php echo e($errors->first('emp_name')); ?></strong>
                                    </span>
                                    <?php endif; ?>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 <?php echo e($errors->has('duration') ? ' has-error' : ''); ?>">
                        <div class="form-group">
                           <label class="control-label col-lg-3 col-md-4 left_991">Duration : <span class="star-required">&nbsp;&nbsp;</span></label>
                           <div class="col-lg-5 col-md-8 col-sm-12 col-xs-12 fsc-element-margin">
                              <div class="row">
                                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                    <input type="text" class="form-control txtOnly fsc-input" readonly id="duration"  name='duration' placeholder="Duration" value="<?php echo e($schedule->duration); ?>"><?php if($errors->has('duration')): ?>
                                    <span class="help-block">
                                    <strong><?php echo e($errors->first('duration')); ?></strong>
                                    </span>
                                    <?php endif; ?>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 <?php echo e($errors->has('sch_start_date') ? ' has-error' : ''); ?>">
                        <div class="form-group">
                           <label class="control-label col-lg-3 col-md-4 left_991">Schedule Start Date / Day : <span class="star-required">&nbsp;&nbsp;</span></label>
                           <div class="col-lg-5 col-md-8 col-sm-12 col-xs-12 fsc-element-margin">
                              <div class="row">
                                 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 fsc-element-margin">
                                    <input type="text" value="<?php echo e($schedulesetup->sch_start_date); ?>" class="ttt form-control txtOnly fsc-input" style="text-align:left"  readonly name='sch_start_date' placeholder="Schedule Start Date" id="sch_start_date">
                                    <?php if($errors->has('sch_start_date')): ?>
                                    <span class="help-block">
                                    <strong><?php echo e($errors->first('sch_start_date')); ?></strong>
                                    </span>
                                    <?php endif; ?>
                                 </div>
                                 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 fsc-element-margin">
                                    <select class="ttt form-control fsc-input" name='sch_start_day'  id="sch_start_day">
                                       <option value="">---Select---</option>
                                       <option value="Sunday" <?php if($schedulesetup->sch_start_day=='Sunday'): ?> selected <?php endif; ?>>Sunday</option>
                                       <option value="Monday"  <?php if($schedulesetup->sch_start_day=='Monday'): ?> selected <?php endif; ?>>Monday</option>
                                       <option value="Tuesday"  <?php if($schedulesetup->sch_start_day=='Tuesday'): ?> selected <?php endif; ?>>Tuesday</option>
                                       <option value="Wednesday"  <?php if($schedulesetup->sch_start_day=='Wednesday'): ?> selected <?php endif; ?>>Wednesday</option>
                                       <option value="Thursday"  <?php if($schedulesetup->sch_start_day=='Thursday'): ?> selected <?php endif; ?>>Thursday</option>
                                       <option value="Friday"  <?php if($schedulesetup->sch_start_day=='Friday'): ?> selected <?php endif; ?>>Friday</option>
                                       <option value="Saturday"  <?php if($schedulesetup->sch_start_day=='Saturday'): ?> selected <?php endif; ?>>Saturday</option>
                                    </select>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 <?php echo e($errors->has('sch_end_date') ? ' has-error' : ''); ?>">
                        <div class="form-group">
                           <label class="control-label col-lg-3 col-md-4 left_991">Schedule End Date / Day : <span class="star-required">&nbsp;&nbsp;</span></label>
                           <div class="col-lg-5 col-md-8 col-sm-12 col-xs-12 fsc-element-margin">
                              <div class="row">
                                 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 fsc-element-margin">
                                    <input type="text" value="<?php echo e($schedulesetup->sch_end_date); ?>" class="form-control txtOnly fsc-input" style="text-align:left" readonly id="sch_end_date"  name='sch_end_date' placeholder="Schedule End Date">
                                    <?php if($errors->has('sch_end_date')): ?>
                                    <span class="help-block">
                                    <strong><?php echo e($errors->first('sch_end_date')); ?></strong>
                                    </span>
                                    <?php endif; ?>
                                 </div>
                                 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 fsc-element-margin">
                                    <select  class="ttt form-control fsc-input" name='sch_end_day'  id="sch_end_day">
                                       <option value="">---Select---</option>
                                       <option value="Sunday" <?php if($schedulesetup->sch_end_day=='Sunday'): ?> selected <?php endif; ?>>Sunday</option>
                                       <option value="Monday"  <?php if($schedulesetup->sch_end_day=='Monday'): ?> selected <?php endif; ?>>Monday</option>
                                       <option value="Tuesday"  <?php if($schedulesetup->sch_end_day=='Tuesday'): ?> selected <?php endif; ?>>Tuesday</option>
                                       <option value="Wednesday"  <?php if($schedulesetup->sch_end_day=='Wednesday'): ?> selected <?php endif; ?>>Wednesday</option>
                                       <option value="Thursday"  <?php if($schedulesetup->sch_end_day=='Thursday'): ?> selected <?php endif; ?>>Thursday</option>
                                       <option value="Friday"  <?php if($schedulesetup->sch_end_day=='Friday'): ?> selected <?php endif; ?>>Friday</option>
                                       <option value="Saturday"  <?php if($schedulesetup->sch_end_day=='Saturday'): ?> selected <?php endif; ?>>Saturday</option>
                                    </select>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 <?php echo e($errors->has('schedule_in_time') ? ' has-error' : ''); ?> <?php echo e($errors->has('schedule_out_time') ? ' has-error' : ''); ?>">
                        <div class="form-group">
                           <label class="control-label col-lg-3 col-md-4 left_991">Everyday Time In / Out : <span class="star-required">&nbsp;&nbsp;</span></label>
                           <div class="col-lg-5 col-md-8 col-sm-12 col-xs-12 fsc-element-margin">
                              <div class="row">
                                 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 fsc-element-margin">
                                    <input type="text" value="<?php echo e($schedule->schedule_in_time); ?>" class="form-control txtOnly fsc-input"  id="schedule_in_time"  name='schedule_in_time' placeholder="Time In">
                                    <?php if($errors->has('schedule_in_time')): ?>
                                    <span class="help-block">
                                    <strong><?php echo e($errors->first('schedule_in_time')); ?></strong>
                                    </span>
                                    <?php endif; ?>
                                 </div>
                                 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 fsc-element-margin">
                                    <input type="text" class="form-control txtOnly fsc-input" value="<?php echo e($schedule->schedule_out_time); ?>"  id="schedule_out_time"  name='schedule_out_time' placeholder="Time Out">
                                    <?php if($errors->has('schedule_out_time')): ?>
                                    <span class="help-block">
                                    <strong><?php echo e($errors->first('schedule_out_time')); ?></strong>
                                    </span>
                                    <?php endif; ?>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 20px;">
                           <div class="row">
                              <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 fsc-form-row">
                              </div>
                              <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                 <div class="row">
                                    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                       <label class="fsc-form-label" style="float:initial;display: block;text-align: center !important;">Date</label>
                                    </div>
                                    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                       <label class="fsc-form-label" style="float:initial;display: block;text-align: center !important;">Day</label>
                                    </div>
                                    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                       <label class="fsc-form-label" style="float:initial;display: block;text-align: center !important;">Time In</label>
                                    </div>
                                    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                       <label class="fsc-form-label"  style="float:initial;display: block;text-align: center !important;">Clock Out</label>
                                    </div>
                                    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                       <label class="fsc-form-label"  style="float:left;">Day Off</label><br>
                                       <span style="width: 100%;display: inline-block;">Check Mark</span>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <?php $i=1;?>
                        <?php $__currentLoopData = $empschedule; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $empsch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                        <?php  
                        //echo "<br>";
                        $startdate=strtotime($schedulesetup->sch_start_date);
                        $enddate=strtotime($schedulesetup->sch_end_date);
                           //  @if(date("m/d/Y", $empsch->date_1) >= $schedulesetup->sch_start_date)
                   
                        ?>
                        
                        <?php if($empsch->date_1 >= $startdate && $empsch->date_1 <= $enddate ): ?>
                        
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 dd" id="dd<?php echo e($empsch->id); ?>">
                           <div class="">
                              <div class="form-group">
                                 <label class="control-label col-md-3">Day-<?php echo $i; $i++;?> : <span class="star-required">&nbsp;&nbsp;</span></label>
                                 <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 fsc-element-margin" id="per_<?php echo e($empsch->id); ?>">
                                    <div class="row">
                                       <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                          <input type="text" value="<?php echo e(date("m/d/Y", $empsch->date_1)); ?>" readonly class="form-control txtOnly fsc-input"  id="schedule_date_<?php echo e($empsch->id); ?>"  name='schedule_date[]' placeholder="Date" style="padding: 6px 6px !important;">
                                       </div>
                                       <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                          <input type="text" value="<?php echo e(date("l", $empsch->date_1)); ?>" class="form-control txtOnly fsc-input"  id="schedule_date_<?php echo e($empsch->id); ?>"  name='' placeholder="Date">
                                       </div>
                                       <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                          <input type="text" value="<?php echo e($empsch->clockin); ?>" class="form-control txtOnly fsc-input"  id="schedule_clockin_<?php echo e($empsch->id); ?>"  name='schedule_clockin[]' placeholder="Time In">
                                          <input type="hidden" value="<?php echo e($empsch->id); ?>" class="form-control txtOnly fsc-input"  id="<?php echo e($empsch->id); ?>"  name='schedule_id[]' placeholder="Time In">
                                       </div>
                                       <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                          <input type="text" class="form-control txtOnly fsc-input" value="<?php echo e($empsch->clockout); ?>"  id="schedule_clockout_<?php echo e($empsch->id); ?>"  name='schedule_clockout[]' placeholder="Time Out">
                                          <input type="hidden" class="form-control txtOnly fsc-input" value="<?php echo e($empsch->status); ?>"  id="schedule_status1_<?php echo e($empsch->id); ?>"  name='schedule_status1[]' placeholder="Time Out">
                                       </div>
                                       <div class="col-lg-1 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                          <center>
                                             <label class="container1" style="float:none">
                                                <input type="checkbox" value="<?php echo e($empsch->id); ?>" <?php if($empsch->status=='1'): ?> checked <?php else: ?> checked  <?php endif; ?> id="schedule_status_<?php echo e($empsch->id); ?>" name='schedule_status[]'>
                                                <?php if($empsch->status=='1'): ?>
                                                <span class="checkmark"></span>
                                                <?php else: ?>
                                                <span class="checkmark1"></span>
                                                <?php endif; ?>
                                          </center>
                                       </div>
                                       <div class="col-lg-1 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
                                       <center>
                                       <label class="container1" style="float:none">
                                       <input type="checkbox" value="<?php echo e($empsch->id); ?>" <?php if($empsch->schedule_status11=='1'): ?> checked <?php else: ?> checked  <?php endif; ?> id="schedule_status11_<?php echo e($empsch->id); ?>" name='schedule_status11[]'>
                                       <?php if($empsch->schedule_status11=='1'): ?>
                                       <span class="checkmark1"></span>
                                       <?php else: ?>
                                       <span class="checkmark"></span>
                                       <?php endif; ?>
                                       </center>
                                       </div>
                                       <!-- <a class="delete2" id="<?php echo e($empsch->id); ?>" style="position: absolute;"><i class="fa fa-trash" style="color:red"></i></a>
                                          <a href="" style="position: absolute;top: 34px;"><i class="fa fa-edit" style="color:green"></i></a>-->
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <?php endif; ?>
                        <script type="text/javascript">
                           $( "#schedule_clockin_<?php echo e($empsch->id); ?>").timepicker();
                           $( "#schedule_clockout_<?php echo e($empsch->id); ?>").timepicker();
                        </script>
                        <script>
                           $('#schedule_status_<?php echo e($empsch->id); ?>').change(function(e) {
                                e.preventDefault();
                                var companyId = $(this).val();
                               // alert(companyId);
                                var sumprice = '#schedule_status1_'+companyId;
                                var companyId1 = $(sumprice).val();
                               // alert(companyId1);
                                var sumprice1 = '#schedule_clockin_'+companyId;
                                var companyId2 = $(sumprice1).val();
                               // alert(companyId2);
                                var sumprice2 = '#schedule_clockout_'+companyId;
                                var companyId3 = $(sumprice2).val();
                             // alert(companyId1);
                              $.get("<?php echo URL::to('fetch-company'); ?>", {companyId: companyId,companyId1:companyId1,companyId2: companyId2,companyId3:companyId3} , function(data){
                                       // Display the returned data in browser
                                       $("#dd<?php echo e($empsch->id); ?>").load(" #dd<?php echo e($empsch->id); ?> > *");
                                      // location.reload();
                                       window.location = "https://financialservicecenter.net/fac-Bhavesh-0554/schedule/<?php echo e($schedule->id); ?>/edit";
                                   });
                               
                             });
                        </script>
                        <style>#schedule_clockout_<?php echo e($empsch->id); ?>,#schedule_clockin_<?php echo e($empsch->id); ?>,#schedule_date_<?php echo e($empsch->id); ?>{ text-align:center;}</style>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <div class="card-footer">
                        <div class="form-group">
                        <label class="control-label col-md-3"></label>
                        <div class="col-md-2">
                        <input class="btn_new_save btn-primary1" type="submit" name="submit" value="Save">
                        </div>
                        <div class="col-md-2 row">
                        <a class="btn_new_cancel" href="<?php echo e(url('fac-Bhavesh-0554/schedule')); ?>">Cancel</a>
                        </div>
                        </div>
                        </div>    
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                           <div class="" id="Register"></div>
                        </div>
                  
                  </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </section>
<!--</div>-->
<script type="text/javascript">
   $( "#schedule_in_time" ).timepicker();
   $( "#schedule_out_time" ).timepicker();
</script>
<script>
   $(document).ready(function(){
   	$(document).on('change','.category', function()
   	{// alert();
   		//console.log('htm');
   		var id = $(this).val(); //alert(id);
   		
   		$.get('<?php echo URL::to('getschedule'); ?>?id='+id, function(data)
   		{  $('#emp_name').empty();
   		         $('#emp_name').append('<option value="">---Select Employee---</option>');
              $.each(data, function(index, subcatobj)
   		   {
   		  
   $('#emp_name').append('<option value="'+subcatobj.id+'">'+subcatobj.firstName+' </option>');
   		   })
   
   		});
   			
   	});
   });
</script>
<script>
   $(document).ready(function(){
   	$(document).on('change','.emp', function()
   	{
   		//console.log('htm');
   		var id = $(this).val();//alert(id);
   
   		$.get('<?php echo URL::to('getduration'); ?>?id='+id, function(data)
   		{  
               $('#duration').empty();
    
              $.each(data, function(index, subcatobj)
   		   {        
                    $('#duration').val(subcatobj.duration);
                    $('#sch_start_date').val(subcatobj.sch_start_date);
                    $('#sch_start_day').val(subcatobj.sch_start_day);
                    $('#sch_end_date').val(subcatobj.sch_end_date);
                    $('#sch_end_day').val(subcatobj.sch_end_day);
                          
   		   })
   
   		});
   			
   	});
   });
</script>
<script>
   $(document).ready(function()
                     {   
     $("#sch_start_date").datepicker({
       format: "mm/dd/yyyy"});
       $(document).on('click', '.delete2', function(){
         var id = $(this).attr('id');
         if(confirm("Are you sure you want to Delete this data?"))
         {
             $.ajax({
                 url:"<?php echo e(route('removeclockin1.removeclockin')); ?>",
                 mehtod:"get",
                 data:{id:id},
                 success:function(data)
                 {
                     alert(data);
                    $('#per_'+id).remove();
                    $(".card-body").load(".card-body > *");
                 }
             })
         }
         else
         {
             return false;
         }
     }); 
   });
</script>
<!--
   <script type="text/javascript">
   $(document).ready(function() {
     $("#sch_start_date").change(function() {
       var startdate= $("#sch_start_date").val();
       var monthNames = [
         "Jan", "Feb", "Mar",
         "Apr", "May", "Jun", "Jul",
         "Aug", "Sep", "Oct",
         "Nov", "Dec"
       ];
     var weekday = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
       var durtion=$('#duration').val();
       var  date = new Date(startdate);
       var day = weekday[date.getDay()];
       var monthly=30;
       var weekly=7;
       var bimonthly=15;
       var biweekly=14;
       var monthss=monthNames[(date.getMonth())];
       var yearss=date.getFullYear();
       var yyy=yearss % 4 ;
       //if(yyy)
       //{
       //alert('true');
       //}
       //else
       //{
       //alert('false');
       //}
       if(durtion == "Weekly")
       {
         var totaldays=6;
       }
       else if(durtion == "Monthly")
       {
         if(monthss == 'Jan' || monthss == 'Mar' || monthss == 'May' || monthss == 'Jul' || monthss == 'Aug' || monthss == 'Oct' || monthss == 'Dec')
         {
           var totaldays=30;
         }
         else if(monthss == 'Feb')
         {
           //if(years / 4 = 0)
           if(yyy ==0)
           {
             var totaldays=28;
           }
           else if(yyy ==1)
           {
             var totaldays=27;
           }
         }
         else if(monthss == 'Apr' || monthss == 'Jun' || monthss == 'Sep' || monthss == 'Nov' )
         {
           var totaldays=29;
         }
       }
       else if(durtion == "Bi-Weekly")
       {
         var totaldays=13;
       }
       else if(durtion == "Bi-Monthly")
       {
         var totaldays=14;
       }
     // var vv = day + totaldays;
       
     date.setDate(date.getDate() + totaldays);// alert(vv);
       var date1 = ("0" + (date.getMonth() + 1)).slice(-2)  + "/" + ("0" + date.getDate()).slice(-2) + "/" + date.getFullYear();// alert(date.getDate())
       
       var newdate = new Date(date1);
         var day1 = weekday[newdate.getDay()];
        //alert(newdate);
       var date2=monthNames[(date.getMonth())] + "/" + date.getDate() + "/" + date.getFullYear() ;
       $('#sch_end_date').val(date1);
       $('#sch_start_day').val(day);
       $('#sch_end_day').val(day1);
       //document.write(date2);
     });
     $("#duration").change(function() {
       $('#sch_end_date').val('');
       $('#sch_start_date').val('');
     });
   });
   </script>-->
<style>
   /* The container */
   .container1 {
   display: block;
   position: relative;
   padding-left: 35px;
   margin-bottom: 12px;
   cursor: pointer;
   font-size: 22px;
   -webkit-user-select: none;
   -moz-user-select: none;
   -ms-user-select: none;
   user-select: none;
   }
   /* Hide the browser's default checkbox */
   .container1 input {
   position: absolute;
   opacity: 0;
   cursor: pointer;
   height: 0;
   width: 0;
   }
   /* Create a custom checkbox */
   .checkmark {
   position: absolute;
   top: -10px;
   left:20px;
   height: 25px;
   width: 25px;
   border: 2px solid #326db5;
   background: red;
   }
   .checkmark1 {
   position: absolute;
   top: -10px;
   left:20px;
   height: 25px;
   width: 25px;
   border: 2px solid #326db5;
   background: #d8d4d4;
   }
   /* On mouse-over, add a grey background color */
   .container1:hover input ~ .checkmark {
   background-color: #ccc;
   }
   .container1:hover input ~ .checkmark1 {
   background-color: #ccc;
   }
   /* Create the checkmark/indicator (hidden when not checked) */
   .checkmark:after {
   content: "";
   position: absolute;
   display: none;
   }
   .checkmark1:after {
   content: "";
   position: absolute;
   display: none;
   }
   /* Show the checkmark when checked */
   .container1 input:checked ~ .checkmark:after {
   display: block;
   }
   .container1 input:checked ~ .checkmark1:after {
   display: block;
   }
   /* Style the checkmark/indicator */
   .container1 .checkmark:after {
   left: 9px;
   top: 5px;
   width: 5px;
   height: 10px;
   border: solid white;
   border-width: 0 3px 3px 0;
   -webkit-transform: rotate(45deg);
   -ms-transform: rotate(45deg);
   transform: rotate(45deg);
   }
   .container1 .checkmark1:after {
   left: 9px;
   top: 5px;
   width: 5px;
   height: 10px;
   border: solid white;
   border-width: 0 3px 3px 0;
   -webkit-transform: rotate(45deg);
   -ms-transform: rotate(45deg);
   transform: rotate(45deg);
   }
   #sch_start_date,#sch_end_date,#schedule_in_time,#schedule_out_time{ text-align:center;}
</style>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>