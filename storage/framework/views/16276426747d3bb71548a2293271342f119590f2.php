<?php $__env->startSection('main-content'); ?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>Link Category</h1>
    </section>
    <!-- Main content -->
    <section class="content">	
	<div class="row">
		<div class="col-md-12">
				<div class="box box-success">
			      <div class="box-header">
  
              <div class="box-tools pull-right">
                
              </div>
            </div>
				<div class="col-md-12">
             <form method="post" action="<?php echo e(route('linkcategory.update',$link->id)); ?>" class="form-horizontal" id="businessname" name="businessname" enctype="multipart/form-data">
					<?php echo e(csrf_field()); ?><?php echo e(method_field('PATCH')); ?>

						<div class="form-group <?php echo e($errors->has('category') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Category :</label>
							<div class="col-lg-5 col-md-8">
								<input name="category" type="text" id="category" class="form-control" value="<?php echo e($link->category); ?>"/>  <?php if($errors->has('category')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('category')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>
<div class="form-group <?php echo e($errors->has('type') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Type :</label>
							<div class="col-lg-5 col-md-8">



<select name="type" type="text" id="type" class="form-control" value="" >
<option value="">Select</option> 
<option value="Federal" <?php if($link->type=='Federal'): ?> selected <?php endif; ?>>Federal</option> 
<option value="State" <?php if($link->type=='State'): ?> selected <?php endif; ?>>State</option> 
<option value="County" <?php if($link->type=='County'): ?> selected <?php endif; ?>>County</option> 
<option value="Local City" <?php if($link->type=='Local City'): ?> selected <?php endif; ?>>Local City</option> 
<option value="Community Link" <?php if($link->type=='Community Link'): ?> selected <?php endif; ?>>Community Link</option> 
<option value="Other Link" <?php if($link->type=='Other Link'): ?> selected <?php endif; ?>>Other Link</option> 
</select>
								               <?php if($errors->has('name')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('name')); ?></strong>
										</span>
									<?php endif; ?>
							</div>
						</div>



						
						<div class="form-group <?php echo e($errors->has('linkimage') ? ' has-error' : ''); ?>">
							<label class="control-label col-md-3">Image :</label>
							<div class="col-lg-5 col-md-8">
								<label class="file-upload btn btn-primary">
	                Browse for file ... <input name="linkimage" style="opecity:0" placeholder="Upload Service Image" id="linkimage" type="file">
	            </label>


                                        <?php if($errors->has('linkimage')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('linkimage')); ?></strong>
										</span>
									<?php endif; ?>
<input type="hidden" name="linkimage1" id="linkimage1" class="form-control"  value="<?php echo e($link->linkimage); ?>" /> 
 <img src="<?php echo e(asset('public/linkcategory','')); ?>/<?php echo e($link->linkimage); ?>" title="<?php echo e($link->name); ?>" alt="<?php echo e($link->name); ?>" width="100px">
							</div>
						</div>
									
						
						<div class="card-footer">
						    <div class="row">
						        <div class="col-md-3"></div>
						        <div class="col-xs-2" style="width:155px;">
									<input class="btn_new_save btn-primary1 primary1" style="margin-left:-5%" type="submit" id="primary1" name="submit" value="Save">
								</div>
								<div class="col-xs-2" style="width:155px;">
									<a class="btn_new_cancel" style="margin-left:-5%" href="<?php echo e(url('fac-Bhavesh-0554/linkcategory')); ?>">Cancel</a> 
								</div>
							</div>
						</div>
						
					</form>
				</div>
			</div>
		</div>
	</div>
	 </section>
<!--</div>-->
<script type="text/javascript">
    $(function () {
        $("#category").change(function () {
            var selectedText = $(this).find("option:selected").text();
            var selectedValue = $(this).val();
if(selectedValue=='Business For Sale')
{
document.getElementById('hidden_div').style.display = "block";
document.getElementById('hidden_div1').style.display = "none";
}
if(selectedValue=='Looking For Business')
{
document.getElementById('hidden_div1').style.display = "block";
document.getElementById('hidden_div').style.display = "none";
}
        });
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>