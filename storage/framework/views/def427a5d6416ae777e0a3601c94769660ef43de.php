<!DOCTYPE html>
<html lang="en">
<head>
<?php echo $__env->make('front-section.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</head>
<body>
<div class="container-fluid">
    <div class="row" style="margin-top: 1%;">
         <?php echo $__env->make('front-section.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px;">
                 <?php echo $__env->make('front-section.leftsidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                 <?php $__env->startSection('main-content'); ?>
                <?php echo $__env->yieldSection(); ?>
<?php echo $__env->make('front-section.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</body>
</html