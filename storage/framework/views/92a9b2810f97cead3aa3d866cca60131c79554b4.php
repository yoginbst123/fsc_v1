<?php $__env->startSection('main-content'); ?> 
<style>
.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{padding:5px 8px !important;vertical-align:inherit !important;}
.headerclr{background:#ffff99 !important;}
.table > thead > tr > th{background:none !important;}
.addagemodal .form-control:focus, .addagemodal .form-control:hover{background:#ffff99;}
span.required
{
    color:red;
}
.dt-buttons {
    margin-bottom: 60px;
}
div#examples_info,div#examples_paginate {
    width: 33%;
    float: left;
}
.countboxmian{}
.table > thead > tr > th{padding:8px!important;}
/*.subtitle{height: 25px;  max-width: 600px;  margin: 0px auto 15px; width: 100%;*/
/*    padding-right: 0!important;*/
/*    position: relative; max-width:inherit!important; */
/*    left:0%;*/
/*}*/
.subtitle{margin: 15px auto 0px; border: 2px solid #a5a5a5;
    width: 100%;
    padding-right: 0!important;
    position: relative;
    max-width: inherit!important;
    left: 0%;
    display: flex;
    height: 35px;
    margin-bottom: -85px;
    position: unset;
    margin-top: -10px;
    flex-direction: row;
    /*justify-content: center;*/
    background: #e2e2e2;
    padding: 5px 0px;}
.subtitle span{font-size:16px; color:#000;}
.floatleft{float:left;}
.floatright{float:right;}
.form-group{margin-left:0px!important; margin-right:0px!important;}
.clear{clear:both;}
.buttonarea{position:absolute; bottom:-48px; right:0px;}
.buttonarea a{font-size:16px;}

.reporttable tr td, .reporttable tr th{white-space:nowrap; padding:5px!important; text-align:left!important; border:1px solid #ccc!important;}
div.dataTables_wrapper div.dataTables_length select.form-control.input-sm{padding:0px!important;}
#example_wrapper .table tr td{padding:5px!important;}
/* Fixed Headers */

#example_filter
{
    display:none !important;
}

.dataTables_wrapper  .row:first-child{
    position:absolute; bottom:0px; z-index:99999;
}
@media  print {
   .close, .btn{display:none; font-size:0px;}
  body * {
    visibility:hidden;
  }
  #printSection, #printSection * {
    visibility:visible;
  }
  #printSection {
    position:absolute;
    left:0;
    top:0;
  }
}

#example_info{margin-left:94%;}

</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1> FSC Client Report</h1>
    </section>
    <section class="content">
   <div class="row">
      <div class="col-md-12">
        <div class="box box-success">
			      <div class="box-header">
              <div class="box-tools pull-right">
              </div>
            </div>
				<div class="col-md-12">
               <form  class="form-horizontal" actio="<?php echo e(route('fscclientreport.index')); ?>"   method="post">
                  <?php echo e(csrf_field()); ?>

                  <div class="row">
                     <div class="col-md-12 col-sm-12 col-xs-12">
                             <div class="col-md-2" style="width:10.5%;">
                                 <div class="form-group">
                                     <label for="focusedinput" class="control-label">Client : <span class="required"> * </span></label>
                                      <select class="form-control" name="status" id="status">
                                        <option value="1" <?php if(isset($_POST['status']) && $_POST['status'] =='1') { echo 'selected';}?>>All</option>
                                        <option value="Active" <?php if(isset($_POST['status']) && $_POST['status']  =='Active') { echo 'selected';}?>>Active</option>
                                        <option value="Inactive" <?php if(isset($_POST['status']) && $_POST['status']  =='Inactive') { echo 'selected';}?>>Inactive</option>
                                     </select>
                                </div>
                             </div>
                             
                             <div class="col-md-2" style="padding-left:0px;">
                                 <div class="form-group">
                                     <label for="focusedinput" class="control-label">Type of Business: </label>
                                        <select name="business_id" id="business_id" class="form-control fsc-input category">
                                            <option value="">Select</option>
                                            <option value="all" <?php if(isset($_POST['business_id']) && $_POST['business_id'] =='all') { echo 'selected';}?>>All</option>
                                            <?php $__currentLoopData = $business; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value='<?php echo e($cate->id); ?>' <?php if(isset($_POST['business_id']) && $_POST['business_id']  ==$cate->id) { echo 'selected';}?>><?php echo e($cate->bussiness_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    <input class=" form-control fsc-input" type="hidden" value='' name="" id="user_type">
                                </div>
                             </div>
                            
                             <div class="col-md-3 type_of_service" <?php if(isset($_POST['business_id']) && $_POST['business_id'] ==''){?> style="padding-left:0px;display:none;"<?php } ?> style="padding-left:0px;">
                                 <div class="form-group">
                                     <label for="focusedinput" class="control-label">Type of Services: </label>
                                        <select name="type_of_service" id="type_of_service" class="form-control fsc-input servicetype">
                                            <option value="">Select</option>
                                            <option value="Accounting Service" <?php if(isset($_POST['type_of_service']) && $_POST['type_of_service'] =='Accounting Service') { echo 'selected';}?>>Accounting Service</option>
                                            <option value="Payroll Service" <?php if(isset($_POST['type_of_service']) && $_POST['type_of_service']  =='Payroll Service') { echo 'selected';}?>>Payroll Service</option>
                                            <?php $__currentLoopData = $taxtitles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value='<?php echo e($cate->id); ?>' <?php if(isset($_POST['type_of_service']) && $_POST['type_of_service'] ==$cate->id) { echo 'selected';}?>><?php echo e($cate->title); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    <input class=" form-control fsc-input" type="hidden" value='' name="" id="user_type">
                                </div>
                             </div>
                             <?php //echo $_POST['acperiod'];?>
                           
                             
                             
                              <div class="col-md-2 payroll_period" style="padding-left:0px;display:none;">
                                 <div class="form-group">
                                     <label for="focusedinput" class="control-label">Payroll Period: </label>
                            
                             <select class="form-control" name="">
                                   <option value="">Select</option>
                                   <option value="Regular-8" <?php if(isset($_POST['acperiod']) && $_POST['acperiod']=='Regular-8') { echo 'selected';}?>>Regular With Accounting</option>
                                   <option value="Full-8" <?php if(isset($_POST['acperiod']) && $_POST['acperiod'] =='Full-8') { echo 'selected';}?>>Full Payroll Service</option>
                                   <option value="Limited-8" <?php if(isset($_POST['acperiod']) && $_POST['acperiod'] =='Limited-8') { echo 'selected';}?>>Limited Payroll Service</option>
                                   <option value="Paystubs-8" <?php if(isset($_POST['acperiod']) && $_POST['acperiod'] =='Paystubs-8') { echo 'selected';}?>>Paystubs Only</option>
                                   <option value="None-8" <?php if(isset($_POST['acperiod']) && $_POST['acperiod'] =='None-8') { echo 'selected';}?>>None</option>
                                   
                               </select>
                            </div>
                            </div>
                             
                             
                              <div class="col-md-3 showpersonal" style="padding-left:0px;display:none;">
                                 <div class="form-group">
                                     <label for="focusedinput" class="control-label">Type of Service: </label>
                                        <select name="personaltype" id="personaltype" class="form-control fsc-input hideyears personaltype">
                                            <option value="">Select</option>
                                            <?php $__currentLoopData = $taxtitles1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value='<?php echo e($cate->id); ?>' <?php if(isset($_POST['personaltype']) && $_POST['personaltype'] ==$cate->id){ echo 'selected';}?> ><?php echo e($cate->title); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                </div>
                             </div>
                             
                             
                                 
                           <div style="display:none; padding-left:0px!important;" id="business_catagory_name_2" class="col-md-3">
                            <label for="focusedinput" class="control-label" >Business Category: </label>
                                 <select name="business_catagory_name" id="business_catagory_name" class="form-control fsc-input category1">
                                       <option value=''>---Select Business Category Name---</option>
                                       <?php $__currentLoopData = $category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                       <option value='<?php echo e($cate->id); ?>' <?php if(isset($_POST['business_catagory_name']) && $_POST['business_catagory_name']  ==$cate->id) { echo 'selected';}?>><?php echo e($cate->business_cat_name); ?></option>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                 </select>
                          </div>
                          
                            <div class="col-md-2 accounting_period" <?php if(isset($_POST['acperiod']) && $_POST['acperiod'] ==''){ ?>style="padding-left:0px;display:none;"<?php } ?>  style="padding-left:0px;">
                                 <div class="form-group">
                                     <label for="focusedinput" class="control-label">Accounting Period:<?php //print_r($_POST);?> </label>
                                        <select name="acperiod" id="" class="form-control fsc-input">
                                            <option value="">Select</option>
                                    <option value="10-2" <?php if(isset($_POST['acperiod']) && $_POST['acperiod'] =='10-2') { echo 'selected';}?>>Monthly</option>
                                   <option value="4-2" <?php if(isset($_POST['acperiod']) && $_POST['acperiod'] =='4-2') { echo 'selected';}?>>Quarterly</option>
                                   <option value="5-2" <?php if(isset($_POST['acperiod']) && $_POST['acperiod'] =='5-2') { echo 'selected';}?>>Half-Yearly</option>
                                   <option value="7-2" <?php if(isset($_POST['acperiod']) && $_POST['acperiod'] =='7-2') { echo 'selected';}?>>Yearly</option></select>
                                </div>
                             </div>
                             
                                   <div class="col-md-2 accsoftware" style="display:none;padding-left:0px;">
                                 <div class="form-group">
                                     <label for="focusedinput" class="control-label">Accounting Software:<?php //print_r($_POST);?> </label>
                                        <select class="form-control" name="accounting_software">
                        <option value="">Select</option>
                        <option value="Sage">Sage</option>
                        <option value="Quickbook">Quickbook</option> 
                        <option value="Sage+Quickbook">Sage+Quickbook</option>   
                    </select>
                                </div>
                             </div>
                             
                             <div class="col-md-2 acclocation" style="display:none;padding-left:0px;">
                                 <div class="form-group">
                                     <label for="focusedinput" class="control-label">Accounting Location:<?php //print_r($_POST);?> </label>
                                        <select class="form-control" name="accounting_location">
                        <option value="">Select</option>
                        <option value="On the Cloud">On the Cloud</option>
                        <option value="FSC - Server">FSC - Server</option>
                         <option value="Client Location">Client Location</option>   
                    </select>
                                </div>
                             </div>
                        
                          
                          <div class="col-md-2 periodhide" <?php if(isset($_POST['personalyear']) && $_POST['personalyear'] !=''){?>style="padding-left:0px;display:none;"<?php } ?> style="padding-left:0px;">
                           <label for="focusedinput" class="control-label">Period : </label>
                                <select class="form-control" name="monthlytype" id="monthlytype">
                                    <option value="">Select</option>
                                    <?php $__currentLoopData = $period; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $period1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($period1->id); ?>" <?php if(isset($_POST['monthlytype']) && $_POST['monthlytype'] ==$period1->id) { echo 'selected';}?>><?php echo e($period1->period); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                          </div>
                              <div class="col-md-1 showyears" style="padding-left:0px;" <?php if(isset($_POST['personalyear']) ==''){?>style="padding-left:0px;display:none;"<?php } ?>>
                                 <div class="form-group">
                                     <label for="focusedinput" class="control-label">Year: </label>
                                        <select name="personalyear" id="personalyear" class="form-control fsc-input" style="padding:6px 6px 0px 0px !important;">
                                            <option value="">Select</option>
                                            <option value="2021" <?php if(isset($_POST['personalyear']) && $_POST['personalyear'] =='2021') { echo 'selected';}?>>2021</option>
                                            <option value="2020" <?php if(isset($_POST['personalyear']) && $_POST['personalyear'] =='2020') { echo 'selected';}?>>2020</option>
                                            <option value="2019" <?php if(isset($_POST['personalyear']) && $_POST['personalyear'] =='2019') { echo 'selected';}?>>2019</option>
                                        </select>
                                </div>
                             </div>
                         
                          <div class="col-md-1"  style="padding-left:0px;width:13%;">
                               <button class="btn-success btn" type="submit" name="search" style="width:80px; margin-top:30px;"> Ok</button>
                                <button class="btn-danger btn" type="button" name="search" style="width:60px; margin-top:30px;"onClick="Refresh();"> Reset</button>
                           </div>
                            <!--<div class="form-group <?php echo e($errors->has('business_catagory_name') ? ' has-error' : ''); ?>" style="display:none" id="business_catagory_name_2">-->
                            <!--           <label class="control-label col-md-3">Client Category Name : <span class="star-required">*</span></label>-->
                            <!--           <div class="col-md-6">-->
                            <!--              <div class="row">-->
                            <!--                 <div class="col-md-8">-->
                            <!--                    <select name="business_catagory_name" id="business_catagory_name" class="form-control fsc-input category1">-->
                            <!--                       <option value=''>---Select Business Category Name---</option>-->
                            <!--                       <?php $__currentLoopData = $category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cates): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>-->
                            <!--                       <option value='<?php echo e($cates->id); ?>'><?php echo e($cates->business_cat_name); ?></option>-->
                            <!--                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>-->
                            <!--                    </select>-->
                            <!--                 </div>-->
                            <!--                 <div class="col-md-3 pull-right">-->
                            <!--                    <div id="image1"></div>-->
                            <!--                 </div>-->
                            <!--              </div>-->
                            <!--           </div>-->
                            <!--        </div>-->
                        
                        
                     </div>
                  </div>
                  </form>
                  
                  </div>
                  <div class="clear"></div>
                  <div style="border-top:1px solid #ccc; height:2px; width:100%; margin:10px 0px 10px; display:block;"></div>
                  
                  <div style="width:100%; overflow:auto; border:0px solid #000;" class="table-responsive">
<!--<table cellspacing="0" cellpadding="0" class="reporttable">
<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td bgcolor="#ccffff">DJ</td>
    <td colspan="2" bgcolor="#ffcc99">RP</td>
    <td bgcolor="#99ccff">CJ</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2" bgcolor="#ffff99" style="font-size:25px; font-weight:bold">WRITE UP SERVICE STATUS</td>
    <td bgcolor="#ffff99"> </td>
    <td bgcolor="#ffff99">&nbsp;</td>
    <td bgcolor="#ffff99">&nbsp;</td>
    <td bgcolor="#ffff99">&nbsp;</td>
    <td bgcolor="#ffff99">&nbsp;</td>
    <td bgcolor="#ffff99">&nbsp;</td>
    <td bgcolor="#ffff99">&nbsp;</td>
    <td bgcolor="#ffff99">&nbsp;</td>
    <td bgcolor="#ffff99">&nbsp;</td>
    <td bgcolor="#ffff99">&nbsp;</td>
    <td bgcolor="#ffff99">&nbsp;</td>
    <td bgcolor="#ffff99">&nbsp;</td>
    <td bgcolor="#ffff99">&nbsp;</td>
    <td bgcolor="#ffff99">&nbsp;</td>
    <td bgcolor="#ffff99">&nbsp;</td>
    <td bgcolor="#ffff99">&nbsp;</td>
    <td bgcolor="#ffff99">&nbsp;</td>
    <td bgcolor="#ffff99">&nbsp;</td>
    <td bgcolor="#ffff99">&nbsp;</td>
    <td bgcolor="#ffff99">&nbsp;</td>
    <td bgcolor="#ffff99">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>Vender Update</td>
    <td></td>
    <td> </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <th align="center" valign="bottom" bgcolor="#ffff99"><strong>No</strong></th>
    <th align="center" valign="bottom" bgcolor="#ffff99"><strong>File No</strong></th>
    <th align="center" valign="bottom" bgcolor="#ffff99"><strong>Company Name</strong></th>
    <th align="center" valign="bottom" bgcolor="#ffff99"><strong>Type of    Business</strong></th>
    <th align="center" valign="bottom" bgcolor="#ffff99"><strong>M / Q</strong></th>
    <th align="center" valign="bottom" bgcolor="#ffff99"><strong>Monthly</strong></th>
    <th align="center" valign="bottom" bgcolor="#ffff99"><strong>Qtrly</strong></th>
    <th align="center" valign="bottom" bgcolor="#ffff99"><strong>HY/Yrly</strong></th>
    <th align="center" valign="bottom" bgcolor="#ffff99">&nbsp;</th>
    <th align="center" valign="bottom" bgcolor="#ffff99"><strong>Only PR Service</strong></th>
    <th align="center" valign="bottom" bgcolor="#ffff99"><strong>Full PR Service</strong></th>
    <th align="center" valign="bottom" bgcolor="#ffff99"><strong>Only Acct</strong></th>
    <th align="center" valign="bottom" bgcolor="#ffff99"><strong>Regular Acct+PR+ST</strong></th>
    <th align="center" valign="bottom" bgcolor="#ffff99">&nbsp;</td>
    <th colspan="2" align="center" valign="bottom" bgcolor="#ffff99"><strong>Location</strong></th>
    <th align="center" valign="bottom" bgcolor="#ffff99"><strong>QB</strong></th>
    <th align="center" bgcolor="#ffff99">&nbsp;</th>
    <th align="center" valign="bottom" bgcolor="#ffff99"><strong>Team-AC</strong></th>
    <th colspan="2" align="center" valign="bottom" bgcolor="#ccffff"><strong>Team-DJ</strong></th>
    <th colspan="2" align="center" valign="bottom" bgcolor="#ffcc99"><strong>Team-RP</strong></th>
    <th align="center" valign="bottom" bgcolor="#99ccff"><strong>Team-JC</strong></th>
  </tr>
  <tr>
    <td colspan="2" align="center" bgcolor="#ffff99"><strong>Georgia</strong></td>
    <td align="center" bgcolor="#ffff99">&nbsp;</td>
    <td align="center" bgcolor="#ffff99">&nbsp;</td>
    <td align="center" bgcolor="#ffff99">&nbsp;</td>
    <td align="center" bgcolor="#ffff99">&nbsp;</td>
    <td align="center" bgcolor="#ffff99">&nbsp;</td>
    <td align="center" bgcolor="#ffff99">&nbsp;</td>
    <td align="center" bgcolor="#ffff99">&nbsp;</td>
    <td align="center" bgcolor="#ffff99">&nbsp;</td>
    <td align="center" bgcolor="#ffff99">&nbsp;</td>
    <td align="center" bgcolor="#ffff99">&nbsp;</td>
    <td align="center" bgcolor="#ffff99">&nbsp;</td>
    <td align="center" bgcolor="#ffff99">&nbsp;</td>
    <td align="center" bgcolor="#ffff99"><strong>Server</strong></td>
    <td align="center" bgcolor="#ffff99"><strong>Room-1</strong></td>
    <td align="center" bgcolor="#ffff99">&nbsp;</td>
    <td align="center" bgcolor="#ffff99">&nbsp;</td>
    <td align="center" bgcolor="#ccc0da"><strong>Antionte</strong></td>
    <td align="center" bgcolor="#ccffff"><strong>Gunjan</strong></td>
    <td align="center" bgcolor="#ccffff"><strong>Sunil</strong></td>
    <td align="center" bgcolor="#ffcc99"><strong>Rohit</strong></td>
    <td align="center" bgcolor="#ffcc99"><strong>2nd person</strong></td>
    <td align="center" bgcolor="#99ccff"><strong>Ankit</strong></td>
  </tr>
  <tr>
    <td align="center">1</td>
    <td>GA-01-012</td>
    <td>Comm3 Creative, Inc</td>
    <td>Graphic Designs</td>
    <td align="center">Monthly</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center"></td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center"></td>
    <td align="center" bgcolor="#92d050">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">2</td>
    <td>GA-109-3</td>
    <td>Sifan One Inc </td>
    <td>Liquor Store</td>
    <td align="center">Monthly</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center" bgcolor="#92d050">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">3</td>
    <td>GA-166</td>
    <td>Singadia Investment    of America Inc.</td>
    <td>Com. Property Rental</td>
    <td align="center" bgcolor="#cc99ff">Half-Yearly</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center" bgcolor="#92d050">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">4</td>
    <td>GA-201-2</td>
    <td>Mahadev Inc.</td>
    <td>Com. Property Rental</td>
    <td align="center" bgcolor="#ffcc99">Quarterly</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center" bgcolor="#92d050">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">5</td>
    <td>GA-219</td>
    <td>ZK, Corporation</td>
    <td>Restaurant-Indian</td>
    <td align="center">Monthly</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center"></td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center"></td>
    <td align="center" bgcolor="#92d050">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">6</td>
    <td>GA-226-1</td>
    <td>Mandeep of USA Inc.</td>
    <td>Tobacco Store</td>
    <td align="center" bgcolor="#ffcc99">Quarterly</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center"></td>
    <td align="center" bgcolor="#92d050">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
  </tr>
  <tr>
    <td align="center">7</td>
    <td>GA-231</td>
    <td>Servitix Pest    Control, Inc.</td>
    <td>Pest Control</td>
    <td align="center">Monthly</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center" bgcolor="#92d050">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
  </tr>
  <tr>
    <td align="center">8</td>
    <td>GA-251-1</td>
    <td>Sabrin, Inc.</td>
    <td>Fast Food Rest-DQ</td>
    <td align="center">Monthly</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center" bgcolor="#92d050">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
  </tr>
  <tr>
    <td align="center">9</td>
    <td>GA-258-1</td>
    <td>Shreeji Shlok Inc</td>
    <td>Conv. Store With Gas</td>
    <td align="center">Monthly</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center" bgcolor="#92d050">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
  </tr>
  <tr>
    <td align="center">10</td>
    <td>GA-260-7</td>
    <td>PKS Zevar LLC</td>
    <td>Jewellery-Indian</td>
    <td align="center">Monthly</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center" bgcolor="#92d050">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">11</td>
    <td>GA-266-7</td>
    <td>Shakti Gauri Inc.</td>
    <td>Hotel/Motel</td>
    <td align="center">Monthly</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center" bgcolor="#92d050">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">12</td>
    <td>GA-305-1</td>
    <td>Kiplistore Inc</td>
    <td>Mail Box Shipping</td>
    <td align="center" bgcolor="#ffcc99">Quarterly</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">13</td>
    <td>GA-314</td>
    <td>Travel Universe, Inc.</td>
    <td>Travel Agency</td>
    <td align="center">Monthly</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center" bgcolor="#92d050">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">14</td>
    <td>GA-318-1</td>
    <td>Avinash Patel LLC</td>
    <td>Liquor Store</td>
    <td align="center">Monthly</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center" bgcolor="#92d050">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">15</td>
    <td>GA-324</td>
    <td>Sai  World, LLC</td>
    <td>Sandwich Shop-Blimpie</td>
    <td align="center">Monthly</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center" bgcolor="#92d050">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">16</td>
    <td>GA-324-1</td>
    <td>Jay &amp; Sam, LLC</td>
    <td>Sandwich Shop-Blimpie</td>
    <td align="center">Monthly</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center" bgcolor="#92d050">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">17</td>
    <td>GA-331</td>
    <td>Macon Medical Clinic, LLC</td>
    <td bgcolor="#ff0000">Medical Services</td>
    <td align="center" bgcolor="#ffcc99">Quarterly</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center" bgcolor="#92d050">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center"><strong>17</strong></td>
    <td>&nbsp;</td>
    <td bgcolor="#ffcc00"><strong>TOTAL</strong></td>
    <td>&nbsp;</td>
    <td align="center"><strong>17 </strong></td>
    <td align="center" bgcolor="#ffc000"><strong>12</strong></td>
    <td align="center" bgcolor="#ffc000"><strong>4</strong></td>
    <td align="center" bgcolor="#ffc000"><strong>1</strong></td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center" bgcolor="#ffcc00"><strong>9 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>7 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>1 </strong></td>
    <td align="center" bgcolor="#ffcc00">&nbsp;</td>
    <td align="center" bgcolor="#ffcc00"><strong>3 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>5 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>3 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>2 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>4 </strong></td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td colspan="2">South Carolina</td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">1</td>
    <td>SC-002</td>
    <td>Jay Bhathiji Inc</td>
    <td>Conv. Store With Gas</td>
    <td align="center">Monthly</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center" bgcolor="#92d050">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center"><strong>1</strong></td>
    <td>&nbsp;</td>
    <td bgcolor="#ffcc00"><strong>TOTAL</strong></td>
    <td>&nbsp;</td>
    <td align="center"><strong>1 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>1 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center" bgcolor="#ffcc00"><strong>1 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00">&nbsp;</td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>1 </strong></td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" align="center">&nbsp;</td>
    <td colspan="2">Tennessee</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">1</td>
    <td>TN-001</td>
    <td>PC of America Corp.</td>
    <td>Grocery Store</td>
    <td align="center">Monthly</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">2</td>
    <td>TN-001-1</td>
    <td>J K Of America    Corporation </td>
    <td>-</td>
    <td align="center">Monthly</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center"><strong>2</strong></td>
    <td>&nbsp;</td>
    <td bgcolor="#ffcc00"><strong>TOTAL</strong></td>
    <td>&nbsp;</td>
    <td align="center"><strong>2 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>2 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center" bgcolor="#ffcc00"><strong>2 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00">&nbsp;</td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>2 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" align="center">&nbsp;</td>
    <td colspan="2">Alabama</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">1</td>
    <td>AL-003</td>
    <td>Milestone, Inc.</td>
    <td>Conv. Store With Gas</td>
    <td align="center">Monthly</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
  </tr>
  <tr>
    <td align="center">2</td>
    <td>AL-003-1</td>
    <td>M &amp; M Petroliem    LLC</td>
    <td>Fuel Dealer</td>
    <td align="center" bgcolor="#ffcc99">Quarterly</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">3</td>
    <td>AL-005</td>
    <td>Sharna Corp.</td>
    <td>Conv. Store With Gas</td>
    <td align="center">Monthly</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">1</td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
    <td> </td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">3</td>
    <td>&nbsp;</td>
    <td bgcolor="#ffcc00"><strong>TOTAL</strong></td>
    <td>&nbsp;</td>
    <td align="center"><strong>3 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>2 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>1 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center" bgcolor="#ffcc00"><strong>2 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>1 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00">&nbsp;</td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>1 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>2 </strong></td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" align="center">&nbsp;</td>
    <td colspan="2">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
    <td bgcolor="#ffcc00"><strong>TOTAL</strong></td>
    <td>&nbsp;</td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00">&nbsp;</td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00">&nbsp;</td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
    <td align="center" bgcolor="#ffcc00"><strong>0 </strong></td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">23 </td>
    <td><u>&nbsp;</u></td>
    <td bgcolor="#ffcc00"><strong>GRAND TOTAL</strong></td>
    <td>&nbsp;</td>
    <td align="center"><strong>23 </strong></td>
    <td align="center" valign="middle" bgcolor="#ffcc00"><strong>       17 </strong></td>
    <td align="center" valign="middle" bgcolor="#ffcc00"><strong>5 </strong></td>
    <td align="center" valign="middle" bgcolor="#ffcc00"><strong>1 </strong></td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center"><strong>14 </strong></td>
    <td align="center"><strong>8 </strong></td>
    <td align="center"><strong>1 </strong></td>
    <td align="center">&nbsp;</td>
    <td align="center"><strong>3 </strong></td>
    <td align="center"><strong>5 </strong></td>
    <td align="center"><strong>6 </strong></td>
    <td align="center"><strong>2 </strong></td>
    <td align="center"><strong>0 </strong></td>
    <td align="center"><strong>7 </strong></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
</table>
!--></div>
<div class="col-md-12 col-sm-12 col-xs-12">  
<?php
//echo "<pre>";
   // print_r($categoryfirst);die;
?>
<?php if(!empty($_REQUEST['status'])){ 
//exit('11');?>

<center>
    <img src="https://financialservicecenter.net/public/dashboard/images/fsc_logo.png" width="80px" class="img-responsive">
</center>
<br>
<?php //count($client1);?>
<div style="position:relative">
<!--<h3 style="font-size:20px; text-align:center; font-size:20px; color:#000; margin:0px 0px 10px;"> FSC Client Report</h3>-->
<div class="subtitle"style="max-width:90%; padding-right:85px;height:35px;display:block !important;">
    <?php //echo "<pre>";
   //echo "<pre>"; print_r($_POST);?>
    <span class="floatleft pull-left countboxmian" style="margin-right:40px;margin-left: 10px; position:absolute;">Count: <b><?php echo count($clients1);?></b></span>
        <?php if(isset($_POST['accounting_location']) && $_POST['accounting_location'] !='') {?><span class=" pull-right" style="right:10px;position:absolute;">Accounting Location: <b><?php  echo $_POST['accounting_location'];?></b></span><?php } ?>
    <?php if(isset($taxyesas) && $taxyesas !='') {?><span class="floatleft pull-left" style="margin-left: 100px; position:absolute;">year: <b><?php  echo $taxyesas;?></b></span><?php } ?>
    <?php if(isset($_POST['accounting_software']) && $_POST['accounting_software'] !='') {?><span class="" style="text-align: center;display: block;">Accounting Software: <b><?php  echo $_POST['accounting_software'];?></b></span><?php } ?>

    
    <?php if(isset($clients1[0]->taxation_service)) {?><span class=""  style="text-align: center;display: block;">Type of Service: <b><?php  echo $clients1[0]->title;?> </b></span><?php } ?>
  
   <?php if(isset($_POST['acperiod']) && $_POST['acperiod'] !='') {?><span class=""  style="text-align: center;display: block;">Type of Service: <b>Accounting Service </b></span><span class=""  style="text-align: right;display: block;margin-top:-21px;margin-right:10px;">Period: <b><?php echo $acperiods->period;?> </b></span><?php } ?>
   <?php if(isset($_POST['type_of_service']) && $_POST['type_of_service'] !='' && !isset($clients1[0]->taxation_service) && $_POST['acperiod'] =='') {?><span class=""  style="text-align: center;display: block;">Type of Service: <b><?php echo $_POST['type_of_service'];?> </b></span><?php } ?>
   
    <?php if(isset($businessfirst->bussiness_name)) {?><span class=""  style="text-align: center;display: block;">Type of Business: <b><?php echo $businessfirst->bussiness_name;?></b></span><?php } ?>
    <?php  if(isset($servicefirst->title)) { ?> <span class=""  style="text-align: right;display: block; padding-right:20px;margin-top: -21px;">Type of Service: <b><?php echo $servicefirst->title;?></b></span><?php }?>

     <?php if(isset($categoryfirst->business_cat_name)!="") {?>
     <span class=""  style="text-align: right;display: block; padding-right:10px;margin-top: -21px;"> Business Category: <b><?php if(isset($categoryfirst->business_cat_name)!="") { echo $categoryfirst->business_cat_name; }?></b></span>
     <?php }?>
   
    <!--<span class="floatright">Type of Client: <b></b></span></div>-->
<!--<div class="buttonarea">-->
<!--    <a href="#" class="btn btn-primary btn-sm" data-toggle="Print" title="Print" ><i class="fa fa-print"></i></a>-->
<!--    <a href="#" class="btn btn-success btn-sm" data-toggle="Pdf" title="Pdf"><i class="fa fa-file-pdf-o"></i></a>-->
<!--    <a href="#" class="btn btn-warning btn-sm" data-toggle="Excel" title="Excel"><i class="fa fa-file-excel-o"></i></a>-->
<!--</div>-->
</div>
<!--<button attr="printMe"  type="button" class="btn btn-primary btn-sm pull-right clkbutton" style="margin-top:-5%;margin-right: 15px; position: ABSOLUTE; right:0px;top: 0px;">Print</button>-->
  
<div id="printMe">
                        <style>
				  @media  print{
					  table{font-family: Verdana, Geneva, Tahoma, sans-serif;}
					  table tr td{padding:4px; border-bottom:1px solid #ccc;}
					   table tr th{padding:4px; border-bottom:1px solid #ccc;}
				  }
				</style>
			
<div class="table-responsive">
     

      <?php 
     //print_r($clients1);
     $cnts=0;
     // $count= count($clients1); 
      if($client1>0){
      ?>
      <table class="table table-hover table-bordered" id="sampleTable3">
  <thead style="background: #ffff99;">
    <tr>
      <th scope="col">No.</th>
      <th scope="col">Client ID</th>
      <th scope="col">Company <br> <span style="font-weight:initial;font-size:12px;">(Legal Name)</span></th>
      <th scope="col"><?php if(isset($_POST['business_id']) && $_POST['business_id']!='6') {?>Business Name <br> <span style="font-weight:initial;font-size:12px;">(DBA Name)</span><?php } else { ?> Email <?php } ?></th>
      <th scope="col">Contact Person Name</th>
      <th scope="col">Telephone</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    <?php $__currentLoopData = $clients1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $client1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <?php $cnts++;?>
    <tr>
      <th scope="row"><?php echo e($cnts); ?></th>
      <td style="text-align:left !important;"><?php echo e($client1->filename); ?></td>
      <td style="text-align:left !important;"><?php if($client1->business_id =='6'): ?> <?php echo e(ucwords($client1->first_name.' '.$client1->middle_name.' '.$client1->last_name)); ?> <?php else: ?> <?php echo e($client1->company_name); ?><?php endif; ?></td>
      <td class="titledba" style="text-align:left !important;"><?php if(isset($_POST['business_id']) && $_POST['business_id']!='6') {?><?php echo e($client1->business_name); ?><?php } else { ?><?php echo e($client1->email); ?> <?php } ?></td>
      
      <td style="text-align:left !important;"><?php echo e(ucwords($client1->contactnametype.' '.$client1->firstname.' '.$client1->middlename.' '.$client1->lastname)); ?></td>
      <td><?php echo e($client1->business_no); ?></td>
      <td><a class="btn-action btn-view-edit btn-primary" href="<?php echo e(route('customer.edit',$client1->id)); ?>"><i class="fa fa-edit"></i></a></td>
    </tr>
    
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
    </table>
    <?php } 
    
    else
    { ?>
    <table class="table table-hover table-bordered" id="sampleTable7" style="margin-top:100px !important;">
  <thead style="background: #ffff99;">
    <tr>
      <th scope="col">No.</th>
      <th scope="col">Client ID</th>
      <th scope="col">Company <br> <span style="font-weight:initial;font-size:12px;">(Legal Name)</span></th>
      <th scope="col"><?php if(isset($_POST['business_id']) && $_POST['business_id']!='6') {?>Business Name <br> <span style="font-weight:initial;font-size:12px;">(DBA Name)</span><?php } else { ?> Email <?php } ?></th>
      <th scope="col">Contact Person Name</th>
      <th scope="col">Telephone</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
 <tr>

      <td colspan="7">Not found data</td>

    </tr>
      </tbody>
</table>
    <?php }?>

            </div>
            
            <?php }?></div>
                </div>
         </div>
         <br>
      </div>
   </div>
</div>

<script>
//$.ajaxSetup({
  //  headers:
   // {'X-CSRF-Token': $('input[name="_token"]').val();}
//});
</script>
<script>
$(document).ready(function() {
$('#examples').DataTable( {
        dom: 'Bfrtlip',
    "columnDefs": [ {
            "searchable": true,
            "filtering": true,
            "orderable": true,
            "lengthChange": true,
            "targets": 0
        } ],
        "order": [[ 0, 'asc' ]],
       buttons: [
                 {
                extend:    'copyHtml5',
                text:      '<i class="fa fa-files-o"></i> &nbsp; Copy',
                //titleAttr: 'Copy',
                title: $('h1').text(),
            },{
                extend:'excelHtml5',
                text:'<i class="fa fa-file-excel-o"></i>&nbsp; Excel',
               // titleAttr: 'Excel',
                title: $('h4').text(),
                 customize: function( xlsx ) {
                var sheet = xlsx.xl.worksheets['sheet1.xml'];
                        $('row c[r^="A"]',sheet).attr('s','51'); 
                       //   $('row c[r^="C"]',sheet).attr('s','51'); 
                          $('row:first c',sheet).attr('s','51');
            },
            exportOptions: {
                columns: [0,1,2,3,4,5,6], // Only name, email and role
                }
            },
            {
                extend:    'csvHtml5',
                text:      '<i class="fa fa-file-text-o"></i> &nbsp; CSV',
               // titleAttr: 'CSV',
                title: $('h1').text(),
            },
            {
           extend: 'print',
          text: '<i class="fa fa-print"></i>&nbsp; Print',
         customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '10pt' )
                        .prepend(
                            '<center><img src="https://financialservicecenter.net/public/business/<?php echo e($logo->logo); ?>"/></center>'
                        );
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );
                },
         exportOptions: {
          columns: [0,1,2,3,4,5,6]
      },
      footer: true,
      autoPrint: true
    },
        ],
    } );
});
</script>
<script>
   $(document).ready(function(){
        $(".clkbutton").click(function(){
        //alert();
            var attr=$(this).attr('attr');
            alert(attr);
              var printContents = document.getElementById(attr).innerHTML;
        w=window.open();
        w.document.write(printContents);
        w.print();
        w.close();
        });
       
    $(document).on('change','.hideyears', function()
   {
        var id = $(this).val();
        if(id =='7')
        {
            
            $('.showyears').show();
        }
        else
        {
            $('.showyears').hide();
        }
       
   });
    $(document).on('change','.servicetype', function()
   {
       var thiss=$(this).val();
       
       if(thiss !='' ||thiss !='Accounting Service' || thiss !='Payroll Service')
       {
           $('.showyears').show();
           
       }
       else
       {
           $('.showyears').hide();
           
       }
       if(thiss =='Accounting Service')
       {
           $('.accounting_period').show();
           
       }
       else
       {
           $('.accounting_period').hide();
       }
   });
   
   $(document).on('change','#type_of_service', function()
   {
        var id = $(this).val();
        if(id !='')
        {
            $('.periodhide').hide();
            
        }
        else
        {
            $('.periodhide').show();
        }
       
   });
   $(document).on('change','.category', function()
   {
        //console.log('htm');
        var id = $(this).val();
        if(id =='all' || id =='6')
        {
            $('.accsoftware').hide();
            $('.acclocation').hide();
            
        }
        
        else
        {
               $('.accsoftware').show();
            $('.acclocation').show();
            
        }
        if(id =='all')
        {
            $('.type_of_service').show();
        }
        else
        {
            $('.type_of_service').hide();
        }
        if(id =='6')
        {  
            $('.showpersonal').show();
            
        }
        else
        {
            $('.showpersonal').hide();
            
        }
        $.get('<?php echo URL::to('getcat'); ?>?id='+id, function(data)
        { 
           $('#user_type').empty();
           $.each(data, function(index, subcatobj)
           {
               // $('#user_type').val(subcatobj.bussiness_name);
         
           })
        });
   
           $.get('<?php echo URL::to('getRequests'); ?>?id='+id, function(data)
           { 
              //alert(id);
               if(data == "")
               {
                   $('#business_catagory_name_2').hide();
                //   $('#business_catagory_name_4').hide();
                //   $('#business_catagory_name_3').hide();
               }
               else
               {
                    $('#business_catagory_name_2').show();
               }
               $('#business_catagory_name').empty();
            //   $('#business_catagory_name_4').hide();
            //   $('#business_catagory_name_3').hide();
            //   $('#business_catagory_name').append('<option value="">---Select---</option>');
            //   $('#business_brand_category_name').append('<option value="">---Select---</option>');
              $('#business_catagory_name').append('<option value="">---Select---</option>');
               $.each(data, function(index, subcatobj)
               {
                    $('#business_catagory_name').append('<option value="'+subcatobj.id+'">'+subcatobj.business_cat_name+'</option>');
                	//$('#user_type111').val('subcatobj.business_cat_name');
                	$('#business_catagory_name22').val(subcatobj.business_cat_name);
               })
           });
   
   
   
        });
   });
</script>
<script>
    function Refresh() {
        window.parent.location = window.parent.location.href;
    }
</script>
<script>
$(document).ready(function() {
     $('#example').DataTable();
     
     $(document).on('change','.personaltype', function()
  {
      var thiss=$(this).val();
    //  alert(thiss);
      if(thiss =='7')
      {
          $('#monthlytype').val('7');
      }
      else if(thiss =='8')
      {
          $('#monthlytype').val('4');
      }
});
});
</script>
<script>
/* $(document).on('change','.category', function()*/
/*   {*/
  
/*    var id = $(this).val();*/
/*   $.get('<?php echo URL::to('getRequest'); ?>?id='+id, function(data)*/
/*   {  */
      
       //alert();
/*   if(data == "")*/
/*   {*/
/*   $('#business_catagory_name_2').hide();*/
/*   $('#business_catagory_name_4').hide();*/
/*   $('#business_catagory_name_3').hide();*/
/*   }*/
/*   else*/
/*   {*/
/*   $('#business_catagory_name_2').show();*/
/*   }*/
/*   $('#business_catagory_name').empty();*/
/*   $('#business_catagory_name_4').hide();*/
/*   $('#business_catagory_name_3').hide();*/
/*   $('#business_catagory_name').append('<option value="">---Select---</option>');*/
/*   $('#business_brand_category_name').append('<option value="">---Select---</option>');*/
/*   $('#business_brand_name').append('<option value="">---Select---</option>');*/
/*   $.each(data, function(index, subcatobj)*/
/*   {*/
/*   $('#business_catagory_name').append('<option value="'+subcatobj.id+'">'+subcatobj.business_cat_name+'</option>');*/
   //	$('#user_type').val('subcatobj.business_cat_name');
/*   })*/
/*   });*/
/*   });*/
  </script>
<style>
.table > thead > tr > th {background: #98c4f2;text-align: center;}
td{text-align: center !important;}
.form-group {margin-bottom: 15px;width: 100%;float: left;}
   </style>
   
   <script>
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();
});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('fscemployee.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>