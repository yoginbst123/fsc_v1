<!DOCTYPE html>
<html lang="<?php echo e(app()->getLocale()); ?>">
<head>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>	
</head>
<body  class="hold-transition skin-blue sidebar-mini">
    
<div class="wrapper">
<?php echo $__env->make('fac-Bhavesh-0554.layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.leftsidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php $__env->startSection('main-content'); ?>
        <?php echo $__env->make('fac-Bhavesh-0554.layouts.Headermenu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->yieldSection(); ?>

</div>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</body>
</html>