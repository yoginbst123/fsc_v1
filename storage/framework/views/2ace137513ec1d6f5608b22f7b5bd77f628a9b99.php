<?php $__env->startSection('main-content'); ?>
<style>
.fsc-label {
    color: #428bca;
    list-style: none;
}
.fsc-label {
    font-size: 1.8em;
    font-family: 'Proza Libre', serif;
    font-weight: 600;
    text-transform: uppercase;
}
.fsc-description span {text-transform: uppercase; font-weight: 600;}
.fsc-description p {font-size: 1.7rem; font-weight: 400}
</style>
<div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">
<?php if( session()->has('success') ): ?>
    <div class="alert alert-success alert-dismissable"><?php echo e(session()->get('success')); ?></div>
<?php endif; ?>
<?php if( session()->has('error') ): ?>
    <div class="alert alert-danger alert-dismissable"><?php echo e(session()->get('error')); ?></div>
<?php endif; ?> 
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 fsc-content-head">
			<h4>EMPLOYMENT</h4>
		</div>
	</div>
	<?php $__currentLoopData = $employment; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $employ): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-section-head">
		<h4><?php echo e($employ->position_name); ?>  ( <?php echo e($employ->type); ?> ) <span class="pull-right"><?php echo e($employ->country); ?></span></h4>
	</div>
	
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-content-box">
		<div class="row" style="margin-top: 2%;">
			
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-10">
				<ul class="tabing-class">
					<li class="fsc-label" style='min-height:40px;'>Location :</li>
					<li class="fsc-label" style='min-height:40px;'>Job Description :</li>
				</ul>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-10">
				<ul>
					<li class="fsc-description title-direction"><?php echo e($employ->city); ?> &nbsp&nbsp&nbsp-&nbsp&nbsp&nbsp <?php echo e($employ->state); ?> <span class="pull-right"><span style="color:#428bca">Posted on</span> : &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<?php echo e($employ->date); ?></span></li>
					<li class="fsc-description"><?php echo $employ->description; ?></li>
				</ul>
			</div>
			
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div style="text-align: right;">
				<a href="<?php echo e(URL('apply-employment',[$employ->id,$employ->position_name])); ?>" class="fsc-job-apply-btn">APPLY NOW</a>
			</div>
		</div>
	</div>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front-section.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>