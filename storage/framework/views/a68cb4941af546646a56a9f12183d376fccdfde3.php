<?php $__env->startSection('main-content'); ?>
<style>
   .taxation-radio-tab{ float:left; margin-left:20%; width:80%; }
   .taxation-radio-tab li{ float:left; list-style: none;padding-left: 22px; font-size: 20px;}
   .select2.select2-container{
       width:100% !important;
   }
</style>
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="page-title content-header">
      <h1>License</h1>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="box box-success">
               <div class="box-header">
                  <div class="box-tools pull-right">
                  </div>
               </div>
               <div class="col-md-12">
                  <form method="post" action="<?php echo e(route('taxation.update',$position->id)); ?>" class="form-horizontal" enctype="multipart/form-data">
                     <?php echo e(csrf_field()); ?><?php echo e(method_field('PATCH')); ?>

                     <div id="tabs">
                        <ul class="tabs taxation-radio-tab" style="display:none">
                           <!--<li id="tab-1"><label for="tab1"><input name="tab" id="tab1" type="radio" <?php if($position->type1=='Taxation'): ?> checked <?php endif; ?> value="Taxation" /> Taxation</label></li>-->
                           <li id="tab-2"><label for="tab2"><input name="tab" id="tab2" type="radio" <?php if($position->type1=='License'): ?> checked <?php endif; ?>  value="License" /> License</label></li>
                        </ul>
                        <div class="tab_container">
                           <!--<div id="forsale" class="tab_content" <?php if($position->type1=='License'): ?> style="display:none" <?php endif; ?>>
                           <div class="form-group <?php echo e($errors->has('type') ? ' has-error' : ''); ?>">
                              <label class="control-label col-md-3">Type of Entity / Form  :</label>
                              <div class="col-md-4">
                                 <select class="form-control fsc-input" name="typeofservice" id="typeofservice">
                                   
                                    <option value="Type of Business" <?php if($position->type=='Type of Business'): ?> selected <?php endif; ?>>Type of Business</option>
                                    <option value="C Corporation" <?php if($position->type=='C Corporation'): ?> selected <?php endif; ?>>C Corporation</option>
                                    <option value="S Corporation"<?php if($position->type=='S Corporation'): ?> selected <?php endif; ?>>S Corporation</option>
                                    <option value="Single Member LLC" <?php if($position->type=='Single Member LLC'): ?> selected <?php endif; ?>>Single Member LLC</option>
                                    <option value="Double Member LLC" <?php if($position->type=='Double Member LLC'): ?> selected <?php endif; ?>>Double Member LLC</option>
                                 </select>
                                 <?php if($errors->has('type')): ?>
                                 <span class="help-block">
                                 <strong><?php echo e($errors->first('type')); ?></strong>
                                 </span>
                                 <?php endif; ?>
                              </div>
                           </div>
                           
                            <div <?php if($position->type=='Type of Business'): ?> <?php else: ?> style="display:none" <?php endif; ?> class="form-group <?php echo e($errors->has('business_catagory_name') ? ' has-error' : ''); ?>" id="business_catagory_name_2">
												<label class="control-label col-md-3">Type of Business : <span class="star-required"></span></label>
												<div class="col-md-4 ac">
													<div class="row">
														<div class="col-md-12">
															<select name="business_catagory_name" id="business_catagory_name" class="form-control fsc-input category1">
															<option value=''>---Select Business Category---</option>
															<?php $__currentLoopData = $category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
															<option value='<?php echo e($cate->id); ?>' <?php if($position->business_catagory_name==$cate->id): ?> selected <?php endif; ?>><?php echo e($cate->business_cat_name); ?></option>
															<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
															</select>
														</div>
													</div>
													<?php if($errors->has('business_catagory_name')): ?>
										<span class="help-block">
											<strong><?php echo e($errors->first('business_catagory_name')); ?></strong>
										</span>
									<?php endif; ?>
												</div>
											</div>
                           <div class="form-group <?php echo e($errors->has('question') ? ' has-error' : ''); ?>">
                              <label class="control-label col-md-3">Type of Form  :</label>
                              <div class="col-md-4">
                                 <input name="question" type="text" id="question" class="form-control" value="<?php echo e($position->question); ?>" />          
                              </div>
                           </div>
                           <div class="form-group <?php echo e($errors->has('expiredate') ? ' has-error' : ''); ?>">
                              <label class="control-label col-md-3">Expire Date :</label>
                              <div class="col-md-4">
                                 <input name="expiredate" type="text" id="expiredate" class="form-control" value="<?php echo e($position->date); ?>" />          
                              </div>
                           </div>
                           <div class="card-footer">
                              <div class="col-md-2 col-md-offset-3">
                                 <input class="btn_new_save btn-primary1" style="margin-left:-5%" type="submit" name="submit" value="Save">
                              </div>
                              <div class="col-md-2 row">
                                 <a class="btn_new_cancel" style="margin-left:-5%" href="<?php echo e(url('fac-Bhavesh-0554/taxation')); ?>">Cancel</a> 
                              </div>
                           </div>
                        </div>-->
                        <div id="wanted" class="tab_content">
                        <div class="form-group">
                           <label class="control-label col-md-3">Type of License :</label>
                           <div class="col-lg-5 col-md-9">
                              <select name="typeoflicense" type="text" id="currency" class="form-control">
                                          <option value="">Type of License</option>
                                              <?php $__currentLoopData = $currency; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cur): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($cur->id); ?>" <?php if($cur->id==$position->question): ?> selected <?php endif; ?>><?php echo e($cur->licensename); ?></option>
                                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   </select>
                           </div>
                        </div>
                        <div class="form-group">
                                 <label class="control-label col-md-3">Type of Bussiness :</label>
                                 <div class="col-lg-5 col-md-9">
                                       <?php $str1=$position->business_catagory_name; $splittedstring1=explode(",",$str1);?>
                                     	<select name="bussiness_name" id="bussiness_name" class="js-example-tags  form-control fsc-input category1">
															<option value=''>---Please select Business---</option>
															<?php $__currentLoopData = $category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
															<option value='<?php echo e($cate->id); ?>' <?php $__currentLoopData = $splittedstring1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php if($value ==$cate->id): ?> selected <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> ><?php echo e($cate->business_cat_name); ?></option>
															<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
										</select>
                                  </div>
                              </div>
                          
                        <div class="form-group">
                           <label class="control-label col-md-3">Authority Level</label>
                           <div class="col-lg-5 col-md-9">
                              <select class="form-control fsc-input" name="typeofservice1" id="typeofservice1">
                                 <option value=""> ---Select--- </option>
                                 <option value="City" <?php if($position->type=='City'): ?> selected <?php endif; ?>>City</option>
                                 <option value="County" <?php if($position->type=='County'): ?> selected <?php endif; ?>>County</option>
                                 <option value="Local" <?php if($position->type=='Local'): ?> selected <?php endif; ?>>Local</option>
                                 <option value="State" <?php if($position->type=='State'): ?> selected <?php endif; ?>>State</option>
                                 <option value="Federal" <?php if($position->type=='Federal'): ?> selected <?php endif; ?>>Federal</option>
                              </select>
                           </div>
                        </div>
                          <div class="form-group countyname" <?php if($position->type=='City'): ?>  <?php elseif($position->type=='County'): ?> <?php elseif($position->type=='Local'): ?> <?php elseif($position->type=='State'): ?> <?php elseif($position->type=='Federal'): ?> <?php else: ?> style="display:none" <?php endif; ?>>
                           <label class="control-label col-md-3">State </label>
                           <div class="col-lg-5 col-md-9">
                              <select class="form-control bfh-states" id="bfh-states" name="state" data-country="USA" data-state="<?php echo e($position->state); ?>"></select>
                           </div>
                        </div>
                      
                        <div class="countyname" <?php if($position->type=='City'): ?>  <?php elseif($position->type=='County'): ?> <?php elseif($position->type=='Local'): ?> <?php elseif($position->type=='State'): ?> <?php elseif($position->type=='Federal'): ?> <?php else: ?> style="display:none" <?php endif; ?>>
                      
                        <div class="form-group city"  <?php if($position->type=='City'): ?> <?php elseif($position->type=='County'): ?>  style="display:none" <?php elseif($position->type=='Local'): ?>  style="display:none" <?php elseif($position->type=='State'): ?>  style="display:none" <?php elseif($position->type=='Federal'): ?> style="display:none" <?php else: ?> style="display:none" <?php endif; ?>>
                        <label class="control-label col-md-3">City Name </label>
                        <div class="col-lg-5 col-md-9">
                           <input type="text" class="form-control" placeholder="City Name" id="cityname" value="<?php echo e($position->cityname); ?>" name="cityname">
                        </div>
                     </div>
                     <div class="form-group city1" <?php if($position->type=='City'): ?> <?php elseif($position->type=='County'): ?> <?php elseif($position->type=='Local'): ?>  style="display:none" <?php elseif($position->type=='State'): ?>  style="display:none" <?php elseif($position->type=='Federal'): ?> style="display:none"<?php else: ?> style="display:none" <?php endif; ?>>
                     <label class="control-label col-md-3">County Name</label>
                     <div class="col-lg-5 col-md-9">
                     <div class="row">
                     <div class="col-md-4">
                        <select type="text" class="form-control" id="county" name="county">
                           <option value="<?php echo e($position->countyname); ?>"><?php echo e($position->countyname); ?></option>
                           <?php $__currentLoopData = $taxstate; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                           <option value="<?php echo e($v->county); ?>"  <?php if($v->county==$position->countyname): ?> selected <?php endif; ?>><?php echo e($v->county); ?></option>
                           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                     </div>
                      <div class="col-md-2">
               <input type="text" class="form-control" placeholder="County Number" value="<?php echo e($position->countysnumber); ?>" id="countynumber" name="countynumber">
               </div>
               </div>
               </div>
               </div>
               
              <div class="form-group">
                           <label class="control-label col-md-3">Authority Name</label>
                           <div class="col-lg-5 col-md-9">
                              <input type="text" class="form-control" placeholder="Authority Name" value="<?php echo e($position->authorityname); ?>" id="authorityname" name="authorityname">
                           </div>
                        </div>
            <div class="form-group">
            <label class="control-label col-md-3">Website</label>
            <div class="col-lg-5 col-md-9">
            <input type="text" class="form-control" placeholder="Website" id="website" value="<?php echo e($position->website); ?>" name="website">
            </div> 
            </div>
            <div class="form-group">
            <label class="control-label col-md-3">Telephone #</label>
            <div class="col-lg-5 col-md-9">
            <input type="text" class="form-control phone" data-format=" (ddd) ddd-dddd" value="<?php echo e($position->telephone); ?>" placeholder="Telephone" id="telephone" name="telephone">
            </div> 
            </div>
            <div class="form-group">
            <label class="control-label col-md-3">Fax #</label>
            <div class="col-lg-5 col-md-9">
            <input type="text" class="form-control phone" data-format=" (ddd) ddd-dddd" value="<?php echo e($position->fax); ?>" placeholder="Fax" id="fax" name="fax">
            </div> 
            </div>
            <div class="form-group">
            <label class="control-label col-md-3">Renew</label>
            <div class="col-lg-5 col-md-9">
            <div class="renew-radio">
            <label for="yes"><input type="radio" name="renew" id="yes"  <?php if($position->renew=='Yes'): ?> checked <?php endif; ?> value="Yes"> Yes</label>
            <label for="no"><input type="radio" name="renew" id="no"  <?php if($position->renew=='No'): ?> checked <?php endif; ?> value="No"> No</label></div>
            </div> 
            </div>
            
              <div class="form-group"  <?php if($position->renew=='Yes'): ?> <?php else: ?> style="display:none" <?php endif; ?> id="renewal4">
            <label class="control-label col-md-3">Renewal-Website:</label>
            <div class="col-lg-5 col-md-9">
                      <input type="text" class="form-control" value="<?php echo e($position->renewalwebsite); ?>" placeholder="Renewal Website" id="renewalwebsite" name="renewalwebsite">
            </div> 
         </div>
        <div class="form-group"  <?php if($position->renew=='Yes'): ?> <?php else: ?> style="display:none" <?php endif; ?> id="renewal1">
            <label class="control-label col-md-3">Renewal</label>
            <div class="col-lg-5 col-md-9">
            <select type="text" class="form-control" id="renewal" name="renewal">
            <option value="">--Select--</option> / 
            <option value="Universal" <?php if($position->renewal=='Universal'): ?> selected <?php endif; ?>>Universal</option>
            <option value="Individual" <?php if($position->renewal=='Individual'): ?> selected <?php endif; ?>>Individual</option>
            </select>
            </div> 
         </div>
         <div class="form-group"  <?php if($position->renew=='Yes'): ?> <?php else: ?> style="display:none" <?php endif; ?> <?php if($position->renewal=='Universal'): ?> <?php else: ?> style="display:none" <?php endif; ?>  id="renewal2">
         <label class="control-label col-md-3">License Expire Date</label>
         <div class="col-lg-5 col-md-9">
         <div class="row">
         <div class="col-md-6 col-xs-6">
         <select type="text" class="form-control" id="duedate" name="duedate">
         <option value="">Month</option>
         <?php
            for ($i = 1; $i <= 12; $i++)
            {
                $month_name = date('F', mktime(0, 0, 0, $i, 1, 2011));?>
         <option value="<?php echo $month_name;?>" <?php if($position->duedate==$month_name): ?> selected <?php endif; ?>><?php echo $month_name;?></option>
         <?php }
            ?>
         </select>
         </div>
         <div class="col-md-6 col-xs-6">
         <?php
            $date = '2003-09-01';
            $end = '2003-09-' . date('t', strtotime($date)); //get end date of month
            ?>
         <select type="text" class="form-control" id="duedate1" name="duedate1">
         <option value="">Date</option> 
         <?php while(strtotime($date) <= strtotime($end)) {
            $day_num = date('d', strtotime($date));
            $day_name = date('l', strtotime($date));
            $date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
             // echo "<td>$day_num <br/> $day_name</td>";
            ?>
         <option value="<?php echo $day_num;?>" <?php if($position->duedate1==$day_num): ?> selected <?php endif; ?>><?php echo $day_num;?></option> <?php
            }?>
         </select>
         </div>
      </div>
      </div>
      </div>
      <div class="form-group"  <?php if($position->renew=='Yes'): ?> <?php else: ?> style="display:none" <?php endif; ?> <?php if($position->renewal=='Universal'): ?> <?php else: ?> style="display:none" <?php endif; ?> id="renewal3">
      <label class="control-label col-md-3">Renewal Due Date</label>
      <div class="col-lg-5 col-md-9"> 
      <div class="row"> 
      <div class="col-md-6 col-xs-6"> 
      <select type="text" class="form-control" id="expiredate" name="expiredate">
      <option value="">Month</option>
      <?php
         for ($i = 1; $i <= 12; $i++)
         {
             $month_name = date('F', mktime(0, 0, 0, $i, 1, 2011));?>
      <option value="<?php echo $month_name;?>" <?php if($position->expiredate==$month_name): ?> selected <?php endif; ?>><?php echo $month_name;?></option>
      <?php
         }
         ?>
      </select>
      </div>
      <?php
         $date = '2003-09-01';
         $end = '2003-09-' . date('t', strtotime($date)); //get end date of month
         ?>
      <div class="col-md-6 col-xs-6">
      <select type="text" class="form-control" id="expiredate2" name="expiredate2">
      <option value="">Date</option> 
      <?php while(strtotime($date) <= strtotime($end)) {
         $day_num = date('d', strtotime($date));
         $day_name = date('l', strtotime($date));
         $date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
         // echo "<td>$day_num <br/> $day_name</td>";
         ?>
      <option value="<?php echo $day_num;?>" <?php if($position->expiredate2==$day_num): ?> selected <?php endif; ?>><?php echo $day_num;?></option> <?php
         }?>
      </select>
      </div>
      </div>
      </div>
</div>
  <div class="form-group">
            <label class="control-label col-md-3">Annual Fees</label>
            <div class="col-lg-5 col-md-9">
            <input type="text" class="form-control annualfees" value="<?php echo e($position->annualfees); ?>" placeholder="Annual Fees" id="annualfees" name="annualfees">
            </div> 
            </div>
            
            <div class="form-group">
            <label class="control-label col-md-3">Processign Fees</label>
            <div class="col-lg-5 col-md-9">
            <input type="text" class="form-control processingfees"  value="<?php echo e($position->processingfees); ?>" placeholder="Processig Fees" id="processingfees" name="processingfees">
            </div> 
            </div>
            
            
</div>
    <div class="card-footer">
    <div class="form-group">
        <div class="col-md-3"></div>
        <div class="col-xs-2" style="width:155px;">
            <input class="btn_new_save btn-primary1" type="submit" name="submit" value="Save">
        </div>
        <div class="col-xs-2" style="width:155px;">
            <a class="btn_new_cancel" href="<?php echo e(url('fac-Bhavesh-0554/states')); ?>">Cancel</a> 
        </div>
    </div>
    </div>
</div>
</div>
</div>									
</form>
</div>
</div>
</div>
</div>
</section>	
<!--</div>-->
<script>
   $(document).ready(function () {
       $('.annualfees').on('blur',function(){ 
    var annualfees=$('.annualfees').val();
         var sign53 =parseFloat(Math.round(annualfees * 100) / 100).toFixed(2);
	    var annual = sign53.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(".annualfees").val(annual);
});   

$('.processingfees').on('blur',function(){ 
    var processingfees=$('.processingfees').val();
         var sign53 =parseFloat(Math.round(processingfees * 100) / 100).toFixed(2);
	    var annual = sign53.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(".processingfees").val(annual);
});   

   $('input[type=radio][name=tab]').on('change', function() {
       if (this.value == 'Taxation') { 
          $('#wanted').hide();
        $('#forsale').show();
      }
      else if (this.value == 'License') {
          $('#wanted').show();
        $('#forsale').hide();
      }
   });
   });
      $(document).ready(function () {
   $('#typeofservice').on('change', function() {
       if (this.value == 'Type of Business') { 
          
        $('#business_catagory_name_2').show();
      }
      else  {

        $('#business_catagory_name_2').hide();
      }
   });
   });
   $(document).ready(function(){
    $(document).on('change','#renewal',function(){
       var valu = $(this).val();
       if(valu=='Universal')
       {
         $('#renewal2').show();
               $('#renewal3').show();                 
               $('#renewal4').show();  
       }
       else
       {
               $('#renewal2').hide();
               $('#renewal3').hide(); 
               $('#renewal4').hide(); 
       }
    });
})
   $(document).ready(function(){
      $('#typeofservice1').on('change',function(){
          var id = this.value;
          if(id=='City')
          {
           $('.city').show();
           $('.city1').show();
             $('.countyname').show();
             $('.state').hide();
             $('.faderal').hide();
          }
          else if(id=='County')
          {
              $('.state').hide();
              $('.city1').show();
              $('.countyname').show();
              $('.city').hide();
              $('.faderal').hide();
          }
          else if(id=='Local')
          {
       $('.city').hide();
               $('.countyname').show();
             $('.state').hide();
             $('.city1').hide();
             $('.faderal').show();
          }
           else if(id=='State')
          {
              $('.city').hide();
               $('.countyname').show();
               $('.city1').hide();
             $('.state').show();
             $('.faderal').hide();
          }
           else if(id=='Federal')
          {
             // alert('federal');
              $('.city').hide();
               $('.countyname').show();
             $('.state').hide();
             $('.city1').hide();
             $('.faderal').show();
          }
          else
          {
               $('#city').hide();
             $('.countyname').hide();
             $('.state').hide();
              $('.faderal').hide();
          }
      });
   });
</script>
<script>
   $(document).ready(function(){
        $(document).on('click','#yes',function(){
           var value = $(this).val();
           if(value=='Yes')
             {
                  $('#renewal1').show();
                  $('#renewal2').show();
                  $('#renewal4').show();
                  $('#renewal3').show();
               }
               else{
                   $('#renewal1').hide();
               }
       });
   });
    $(document).ready(function(){
       $(document).on('click','#no',function(){
           
           var value = $(this).val();
           
           if(value=='No')
             {
                  $('#renewal1').hide();
                  $('#renewal2').hide();$('#renewal4').hide();
                  $('#renewal3').hide();
               }
               else{
                   $('#renewal2').hide();
               }
       });
   });
   $(document).ready(function(){
   	$(document).on('change','#county', function()
   	{
   		var id = $(this).val();
   		$.get('<?php echo URL::to('getcountycod'); ?>?id='+id, function(data)
   		{  
           $('#countynumber').empty();
              $.each(data, function(index, subcatobj)
   		   {
              $('#countynumber').val(subcatobj.countycode);
   		   })
   		});
   	});
   });
</script>
<script>
   $(document).ready(function(){
   	$(document).on('change','#bfh-states', function()
   	{        
   		var id = $(this).val(); //`alert(id);
   		$.get('<?php echo URL::to('getcountcount'); ?>?id='+id+'&state=' + id, function(data)
   		{  
               $('#county').empty();
               $('#county').append('<option value="">Select</option>');
              
              $.each(data, function(index, subcatobj)
   		   {
   		            $('#county').append('<option value="'+subcatobj.county+'">'+subcatobj.county+'</option>');
   		   })
   		});
   	});
   });
    $(document).ready(function(){
     /***phone number format***/
     $(".phone").keypress(function (e) {
       if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
         return false;
       }
       var curchr = this.value.length;
       var curval = $(this).val();
       if (curchr == 3 && curval.indexOf("(") <= -1) {
         $(this).val("(" + curval + ")" + " ");
       } else if (curchr == 4 && curval.indexOf("(") > -1) {
         $(this).val(curval + ")-");
       } else if (curchr == 5 && curval.indexOf(")") > -1) {
         $(this).val(curval + "-");
       } else if (curchr == 9) {
         $(this).val(curval + "-");
         $(this).attr('maxlength', '14');
       }
     });
      // $('#expiredate,#expiredate,#duedate').datepicker();
   });
</script>
<script>
$(document).ready(function(){
	$(document).on('change','.category', function()
	{
		//console.log('htm');
		var id = $(this).val();
		$.get('<?php echo URL::to('getRequest'); ?>?id='+id, function(data)
		{  $('#business_catagory_name').empty();
           $.each(data, function(index, subcatobj)
		   {
			   $('#business_catagory_name').append('<option value="'+subcatobj.id+'">'+subcatobj.business_cat_name+'</option>');
		   })

		});
			
	});
});
 $('.js-example-tags').select2({
       tags: true,
       tokenSeparators: [",", " "]
   });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>