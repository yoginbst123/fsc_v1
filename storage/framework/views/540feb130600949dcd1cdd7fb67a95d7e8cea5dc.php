<?php $__env->startSection('main-content'); ?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>Contact Us Inquiry</h1>
    </section>
    <!-- Main content -->
    <section class="content">

	<div class="row">
		<div class="col-md-12">
			<div class="box box-success">
			      <div class="box-header">
              
              <div class="box-tools pull-right">
                
              </div>
            </div>
				<div class="col-md-12">
             <form method="post" action="" class="form-horizontal" id="businessname" name="businessname" enctype="multipart/form-data">
				
	<div class="form-group">
							<label class="control-label col-md-3"> Date / Time :</label>
							<div class="col-md-2">
								<input name="date" type="text" id="date" class="form-control" value="<?php echo e(date('M d-Y', strtotime($city->created_at))); ?>" />                                                           
							</div>
								<div class="col-md-2">
								<input name="date" type="text" id="date" class="form-control" value="<?php echo e(date('g:s a', strtotime($city->created_at))); ?>" />                                                           
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"> Name :</label>
							<div class="col-md-4">
								<input name="branch" type="text" id="branch" class="form-control" value="<?php echo e($city->name); ?>" />                                                           
									
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3">Email :</label>
							<div class="col-md-4">
												<input name="branch" type="text" id="branch" class="form-control" value="<?php echo e($city->email); ?>" />                                                   
                                                
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Telephone No. :</label>
							<div class="col-md-4">
								<input name="branch" type="text" id="branch1" class="form-control" value="<?php echo e($city->telephone); ?>" /> 
								
							</div>
						</div>					
						<div class="form-group">
							<label class="control-label col-md-3">Message :</label>
							<div class="col-md-4">
								<textarea name="branch" type="text" id="branch" class="form-control" value=""> <?php echo e($city->message); ?></textarea>
								
							</div>
						</div>		
					</form>
					<div class="card-footer">
							<div class="row">
								<div class="col-md-8 col-md-offset-3">
									<a class="btn btn-primary icon-btn" href="<?php echo e(url('fac-Bhavesh-0554/contactinquery')); ?>"><< Back</a>
								<form action="<?php echo e(route('contactinquery.destroy',$city->id)); ?>" method="post" style="display:none" id="delete-id-<?php echo e($city->id); ?>">
                                        <?php echo e(csrf_field()); ?> <?php echo e(method_field('DELETE')); ?>

                                        </form>
										<a class="btn btn-danger" onclick="if(confirm('Are you sure, You want to delete this record ?'))
                                                                                  {event.preventDefault();document.getElementById('delete-id-<?php echo e($city->id); ?>').submit();} else{event.preventDefault();}" href="">Delete</a>
								</div>
								
							</div>
						</div>
				
				</div>
			</div>
		</div>
	</div>
	  </section>
</div>
<script>
$("#branch1").mask("(999) 999-9999");
   $(".ext").mask("99999");
   $("#ext1").mask("99999");
   $("#ext2").mask("99999");
   //$("#telephoneNo2").mask("(999) 999-9999");
  // //$("#mobile_no").mask("(999) 999-9999");
   $(".usapfax").mask("(999) 999-9999");
  // $("#zip").mask("99999");
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>