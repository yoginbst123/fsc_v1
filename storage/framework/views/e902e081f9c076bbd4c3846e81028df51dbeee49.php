<?php $__env->startSection('main-content'); ?>
<style>
ul li{ list-style: decimal;}
/*.Branch h1 {padding: 0px 0 0 10px;float: left;font-size: 15px;font-weight: bold;}*/
.input-daterange {text-align:left;}
.input-daterange input:last-child {text-align:left;border-radius:3px ;}
.input-daterange input:first-child {text-align:left;border-radius: 3px ;}
.select2-container--default .select2-selection--single .select2-selection__arrow b {margin-top: 3px;}
/*.Branch {width: 100%;float: left;margin: 3px 0 20px 0;text-align: center;background: transparent;border: transparent;padding: 4px 0;}*/
.select2-container--default .select2-selection--single .select2-selection__rendered {color: #000;line-height: 28px;font-weight: bold;}
.select2-container .select2-selection--single {height: 36px;padding: 2px 0;border: 2px solid #286db5 !important;}
.datepicker { z-index:99999 !important;}
.ui-timepicker-container{ z-index:999999 !important}
.select2-container .select2-selection--single{aheight: 40px;
    border-radius: 3px;
    border: 2px solid #2fa6f2  !important;
    font-size: 16px !important;
    outline: 0;color:#000;padding: 6px 6px;
    background-color: transparent !important;}
td.fc-day {
background:#FFF !important;
font-family: 'Roboto', sans-serif;
}
td.fc-today {
	background:#FFF !important;
	position: relative;
}

.fc-first th{
	font-family: 'Roboto', sans-serif;
    background:#9675ce !important;
	color:#FFF;
	font-size:14px !important;
	font-weight:500 !important;

	}
.fc-event-inner {
	font-family: 'Roboto', sans-serif;
    background: #03a9f3!important;
    color: #FFF!important;
    font-size: 12px!important;
    font-weight: 500!important;
    padding: 5px 0px!important;
}

.fc {
	direction: ltr;
	text-align: left;
	}
	
.fc table {
	border-collapse: collapse;
	border-spacing: 0;
	}
	
html .fc,
.fc table {
	font-size: 1em;
	font-family: "Helvetica Neue",Helvetica;

	}
	
.fc td,
.fc th {
	padding: 0;
	vertical-align: top;
	}

.fc-header td {
	white-space: nowrap;
	padding: 15px 10px 0px;
}

.fc-header-left {
	width: 25%;
	text-align: left;
}
	
.fc-header-center {
	text-align: center;
	}
	
.fc-header-right {
	width: 25%;
	text-align: right;
	}
	
.fc-header-title {
	display: inline-block;
	vertical-align: top;
	margin-top: -5px;
}
	
.fc-header-title h2 {
	margin-top: 8px;
	white-space: nowrap;
	font-size: 20Px;
    font-weight: 100;
    margin-bottom: 10px;
	font-family: 'Roboto', sans-serif;
}
	span.fc-button {
    font-family: 'Roboto', sans-serif;
    border-color: #9675ce;
	color: #9675ce;
}
.fc-state-down, .fc-state-active {
    background-color: #9675ce !important;
	color: #FFF !important;
}
.fc .fc-header-space {
	padding-left: 10px;
	}
	
.fc-header .fc-button {
	margin-bottom: 1em;
	vertical-align: top;
	}
	
/* buttons edges butting together */

.fc-header .fc-button {
	margin-right: -1px;
	}
	
.fc-header .fc-corner-right,  /* non-theme */
.fc-header .ui-corner-right { /* theme */
	margin-right: 0; /* back to normal */
	}
	
/* button layering (for border precedence) */
	
.fc-header .fc-state-hover,
.fc-header .ui-state-hover {
	z-index: 2;
	}
	
.fc-header .fc-state-down {
	z-index: 3;
	}

.fc-header .fc-state-active,
.fc-header .ui-state-active {
	z-index: 4;
	}
/* Content
------------------------------------------------------------------------*/
.fc-content {
	clear: both;
	border: 1px solid #ababab;
    padding: 2px 5px;
	zoom: 1; /* for IE7, gives accurate coordinates for [un]freezeContentHeight */
	}
.fc-view {
	width: 100%;
	overflow: hidden;
	}
th.fc-day-header.fc-widget-header {
    background: #9675ce;
    color: #ffffff;
    line-height: 30px;
}
td.fc-day-top {
    height: 100px !important;
}
.fc-today .fc-day-number{
    background-color: #ff3b30;
    color: #FFFFFF;
    border-radius: 50%;
    font-size: 18px;
    margin: 2px;
    padding: 4px;
    width: 25px;
    height: 25px;
}
/* Cell Styles
------------------------------------------------------------------------*/

    /* <th>, usually */
.fc-widget-content {  /* <td>, usually */
	border: 1px solid #e5e5e5;
	}
.fc-widget-header{
    border-bottom: 1px solid #EEE; 
}	
.fc-state-highlight { /* <td> today cell */ /* TODO: add .fc-today to <th> */
	/* background: #fcf8e3; */
}

.fc-state-highlight > div > div.fc-day-number{
    background-color: #ff3b30;
    color: #FFFFFF;
    border-radius: 50%;
    margin: 4px;
}
	
.fc-cell-overlay { /* semi-transparent rectangle while dragging */
	background: #bce8f1;
	opacity: .3;
	filter: alpha(opacity=30); /* for IE */
	}

.fc-button {
	position: relative;
	display: inline-block;
	padding: 0 .6em;
	overflow: hidden;
	height: 1.9em;
	line-height: 1.9em;
	white-space: nowrap;
	cursor: pointer;
	}
	
.fc-state-default { /* non-theme */
	border: 1px solid;
	}

.fc-state-default.fc-corner-left { /* non-theme */
	border-top-left-radius: 4px;
	border-bottom-left-radius: 4px;
	}

.fc-state-default.fc-corner-right { /* non-theme */
	border-top-right-radius: 4px;
	border-bottom-right-radius: 4px;
	}

.fc-text-arrow {
	margin: 0 .4em;
	font-size: 2em;
	line-height: 23px;
	vertical-align: baseline; /* for IE7 */
	}

.fc-button-prev .fc-text-arrow,
.fc-button-next .fc-text-arrow { /* for ‹ › */
	font-weight: bold;
	}
	
.fc-button .fc-icon-wrap {
	position: relative;
	float: left;
	top: 50%;
	}
	
.fc-button .ui-icon {
	position: relative;
	float: left;
	margin-top: -50%;
	
	*margin-top: 0;
	*top: -50%;
	}


.fc-state-default {
	border-color: #ff3b30;
	color: #ff3b30;	
}
.fc-button-month.fc-state-default, .fc-button-agendaWeek.fc-state-default, .fc-button-agendaDay.fc-state-default{
    min-width: 67px;
	text-align: center;
	transition: all 0.2s;
	-webkit-transition: all 0.2s;
}
.fc-state-hover,
.fc-state-down,
.fc-state-active,
.fc-state-disabled {
	color: #333333;
	background-color: #FFE3E3;
	}

.fc-state-hover {
	color: #ff3b30;
	text-decoration: none;
	background-position: 0 -15px;
	-webkit-transition: background-position 0.1s linear;
	   -moz-transition: background-position 0.1s linear;
	     -o-transition: background-position 0.1s linear;
	        transition: background-position 0.1s linear;
	}

.fc-state-down,
.fc-state-active {
	background-color: #ff3b30;
	background-image: none;
	outline: 0;
	color: #FFFFFF;
}

.fc-state-disabled {
	cursor: default;
	background-image: none;
	background-color: #FFE3E3;
	filter: alpha(opacity=65);
	box-shadow: none;
	border:1px solid #FFE3E3;
	color: #ff3b30;
	}

.fc-event-container > * {
	z-index: 8;
	}

.fc-event-container > .ui-draggable-dragging,
.fc-event-container > .ui-resizable-resizing {
	z-index: 9;
	}
	 
.fc-event {
	border: 1px solid #FFF; /* default BORDER color */
	background-color: #FFF; /* default BACKGROUND color */
	color: #919191;               /* default TEXT color */
	font-size: 12px;
	cursor: default;
}
.fc-event.chill{
    background-color: #f3dcf8;
}
.fc-event.info{
    background-color: #c6ebfe;
}
.fc-event.important{
    background-color: #FFBEBE;
}
.fc-event.success{
    background-color: #BEFFBF;
}
.fc-event:hover{
    opacity: 0.7;
}
a.fc-event {
	text-decoration: none;
	}
	
a.fc-event,
.fc-event-draggable {
	cursor: pointer;
	}
	
.fc-rtl .fc-event {
	text-align: right;
	}

.fc-event-inner {
	width: 100%;
	height: 100%;
	overflow: hidden;
	line-height: 15px;
	}
	
.fc-event-time,
.fc-event-title {
	padding: 0 1px;
	}
	
.fc .ui-resizable-handle {
	display: block;
	position: absolute;
	z-index: 99999;
	overflow: hidden; /* hacky spaces (IE6/7) */
	font-size: 300%;  /* */
	line-height: 50%; /* */
	}

.fc-event-hori {
	border-width: 1px 0;
	margin-bottom: 1px;
	}

.fc-ltr .fc-event-hori.fc-event-start,
.fc-rtl .fc-event-hori.fc-event-end {
	border-left-width: 1px;
	/*
border-top-left-radius: 3px;
	border-bottom-left-radius: 3px;
*/
	}

.fc-ltr .fc-event-hori.fc-event-end,
.fc-rtl .fc-event-hori.fc-event-start {
	border-right-width: 1px;
	/*
border-top-right-radius: 3px;
	border-bottom-right-radius: 3px;
*/
	}
.fc-event-hori .ui-resizable-e {
	top: 0           !important; /* importants override pre jquery ui 1.7 styles */
	right: -3px      !important;
	width: 7px       !important;
	height: 100%     !important;
	cursor: e-resize;
	}
	
.fc-event-hori .ui-resizable-w {
	top: 0           !important;
	left: -3px       !important;
	width: 7px       !important;
	height: 100%     !important;
	cursor: w-resize;
	}
	
.fc-event-hori .ui-resizable-handle {
	_padding-bottom: 14px; /* IE6 had 0 height */
	}

table.fc-border-separate {
	border-collapse: separate;
	}
	
.fc-border-separate th,
.fc-border-separate td {
	border-width: 1px 0 0 1px;
	}
	
.fc-border-separate th.fc-last,
.fc-border-separate td.fc-last {
	border-right-width: 1px;
	}
	

.fc-border-separate tr.fc-last td {
	
}
.fc-border-separate .fc-week .fc-first{
    border-left: 0;
}
.fc-border-separate .fc-week .fc-last{
    border-right: 0;
}
.fc-border-separate tr.fc-last th{
    border-bottom-width: 1px;
    border-color: #cdcdcd;
    font-size: 16px;
    font-weight: 300;
	line-height: 30px;
}
.fc-border-separate tbody tr.fc-first td,
.fc-border-separate tbody tr.fc-first th {
	border-top-width: 0;
	}
.fc-grid th {
	text-align: center;
	}

.fc .fc-week-number {
	width: 22px;
	text-align: center;
	}

.fc .fc-week-number div {
	padding: 0 2px;
	}
	
.fc-grid .fc-day-number {
	float: right;
	padding: 0 2px;
	}
	
.fc-grid .fc-other-month .fc-day-number {
	opacity: 0.3;
	filter: alpha(opacity=30); /* for IE */
	/* opacity with small font can sometimes look too faded
	   might want to set the 'color' property instead
	   making day-numbers bold also fixes the problem */
	}
	
.fc-grid .fc-day-content {
	clear: both;
	padding: 2px 2px 1px; /* distance between events and day edges */
	}
	
/* event styles */
	
.fc-grid .fc-event-time {
	font-weight: bold;
	}
	
/* right-to-left */
	
.fc-rtl .fc-grid .fc-day-number {
	float: left;
	}
	
.fc-rtl .fc-grid .fc-event-time {
	float: right;
	}
.fc-agenda table {
	border-collapse: separate;
	}
	
.fc-agenda-days th {
	text-align: center;
	}
	
.fc-agenda .fc-agenda-axis {
	width: 50px;
	padding: 0 4px;
	vertical-align: middle;
	text-align: right;
	white-space: nowrap;
	font-weight: normal;
	}

.fc-agenda .fc-week-number {
	font-weight: bold;
	}
	
.fc-agenda .fc-day-content {
    padding: 2px 2px 1px;
	}
.fc-agenda-days .fc-agenda-axis {
	border-right-width: 1px;
	}
	
.fc-agenda-days .fc-col0 {
	border-left-width: 0;
	}
	
/* all-day area */
	
.fc-agenda-allday th {
	border-width: 0 1px;
	}
	
.fc-agenda-allday .fc-day-content {
	min-height: 34px; /* TODO: doesnt work well in quirksmode */
	_height: 34px;
	}
	
/* divider (between all-day and slots) */
	
.fc-agenda-divider-inner {
	height: 2px;
	overflow: hidden;
	}
	
.fc-widget-header .fc-agenda-divider-inner {
	background: #eee;
	}
	
/* slot rows */
	
.fc-agenda-slots th {
	border-width: 1px 1px 0;
	}
	
.fc-agenda-slots td {
	border-width: 1px 0 0;
	background: none;
	}
	
.fc-agenda-slots td div {
	height: 20px;
	}
	
.fc-agenda-slots tr.fc-slot0 th,
.fc-agenda-slots tr.fc-slot0 td {
	border-top-width: 0;
	}
	
.fc-agenda-slots tr.fc-minor th.ui-widget-header {
	*border-top-style: solid; /* doesn't work with background in IE6/7 */
	}

/* Vertical Events
------------------------------------------------------------------------*/

.fc-event-vert {
	border-width: 0 1px;
	}

.fc-event-vert.fc-event-start {
	border-top-width: 1px;
	border-top-left-radius: 3px;
	border-top-right-radius: 3px;
	}

.fc-event-vert.fc-event-end {
	border-bottom-width: 1px;
	border-bottom-left-radius: 3px;
	border-bottom-right-radius: 3px;
	}
	
.fc-event-vert .fc-event-time {
	white-space: nowrap;
	font-size: 10px;
	}

.fc-event-vert .fc-event-inner {
	position: relative;
	z-index: 2;
	}
	
.fc-event-vert .fc-event-bg { /* makes the event lighter w/ a semi-transparent overlay  */
	position: absolute;
	z-index: 1;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	background: #fff;
	opacity: .25;
	filter: alpha(opacity=25);
	}
	
.fc .ui-draggable-dragging .fc-event-bg, /* TODO: something nicer like .fc-opacity */
.fc-select-helper .fc-event-bg {
	display: none\9; /* for IE6/7/8. nested opacity filters while dragging don't work */
	}
	
/* resizable */
	
.fc-event-vert .ui-resizable-s {
	bottom: 0        !important; /* importants override pre jquery ui 1.7 styles */
	width: 100%      !important;
	height: 8px      !important;
	overflow: hidden !important;
	line-height: 8px !important;
	font-size: 11px  !important;
	font-family: monospace;
	text-align: center;
	cursor: s-resize;
	}
	
.fc-agenda .ui-resizable-resizing { /* TODO: better selector */
	_overflow: hidden;
	}
	
thead tr.fc-first{
    background-color: #f7f7f7;
}
table.fc-header{
    background-color: #FFFFFF;
    border-radius: 6px 6px 0 0;
}

.fc-week .fc-day > div .fc-day-number{
    font-size: 15px;
    margin: 2px;
    min-width: 19px;
    padding: 6px;
    text-align: center;
       width: 30px;
    height: 30px;
}
.fc-sun, .fc-sat{
    color: #b8b8b8;
}

.fc-week .fc-day:hover .fc-day-number{
    background-color: #B8B8B8;
    border-radius: 50%;
    color: #FFFFFF;
    transition: background-color 0.2s;
}
.fc-week .fc-day.fc-state-highlight:hover .fc-day-number{
    background-color:  #ff3b30;
}
.fc-button-today{
    border: 1px solid rgba(255,255,255,.0);
}
.fc-view-agendaDay thead tr.fc-first .fc-widget-header{
    text-align: right;
    padding-right: 10px;
}
.fc-event {
	background: #fff !important;
	color: #000 !important;
	}
	
/* for vertical events */
	
.fc-event-bg {
	display: none !important;
	}
	
.fc-event .ui-resizable-handle {
	display: none !important;
}
.select2-container{
    width:100% !important;
}
.fc-prev-button.fc-button{
    border: 2px solid #5120a7 !important;
    background-image: linear-gradient(to bottom, #c6acf1, #9675ce) !important;
    color: #ffffff !important;
    margin-right: 5px !important;
    border-radius: 3px !important;
}
.fc-next-button.fc-button{
    border: 2px solid #5120a7 !important;
    background-image: linear-gradient(to bottom, #c6acf1, #9675ce) !important;
    color: #ffffff !important;
    margin-left: 4px !important;
    border-radius: 3px !important;
}
.fc-myCustom1-button{
    background-image: none !important;
    border: 2px solid #5120a7 !important;
    margin-right: 4px !important;
}
.fc-myCustom2-button{
    background-image: none !important;
    border: 2px solid #5120a7 !important;
}
.fc-toolbar.fc-header-toolbar {
    margin-bottom: 0px !important;
    padding: 3px 0px 10px;
}
.datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-bottom{
    display:none !important;
}
.ui-widget {
    font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif !important;
    font-size: 15px !important;
}
.select2-container--default .select2-selection--multiple {
    height: 40px;
    border-radius: 3px;
    border: 2px solid #2fa6f2;
    font-size: 16px !important;
    outline: 0;
    color: #000;
    padding: 6px 6px;
}

#countryList{
    margin:1px 0px 0px 0px;
    padding:0px 0px;
    list-style-type:none;
    max-height: 200px;
    overflow: auto; position: absolute;
    width:850px;
    z-index: 99;}
#countryList li{border-bottom:1px solid #025b90;
    background: #ef7c30;}
#countryList li a{padding:0px 10px 0px 0px !important;margin:0px !important; display:block; color:#fff!important;}
#countryList li:hover{ background:#dc6d23;}


#countryList li a{height:40px;}
#countryList li a span.bgcolors{     background: linear-gradient(to bottom, #b3dced 0%,#29b8e5 50%,#bce0ee 100%) !important;
    padding: 6px 0px; text-align:center;
    display: block;
    font-size: 20px;
    font-weight: bold;
    float: left;
    width: 60px;}
#countryList li a span.clientalign{display: flex;
    width:100px;
    float: left;
    line-height: 15px; font-size:13px;
    padding: 4px 0px;
    flex-direction: row;
    justify-content: flex-start;
    align-items: center;
    align-content: center;
    min-height: 40px; background:#038ee0;
    margin-left: 0;
    padding-left: 5px;}
    
    #countryList li a span.entityname{display: flex;
   width:190px
    float: left;
    line-height: 15px;
    padding: 4px 0px; font-size:13px;
    flex-direction: row;
    justify-content: flex-start;
    align-items: center;
    align-content: center;
    min-height: 38px; padding-left:10px;
    margin-left: 10px;}
    .table tr td.text-center{text-align:center!important;}
      .modal-body ul{margin:0px; padding:0px;}
    .modal-body ul li{text-align:left!important; list-style-type:none!important; font-size:12px!important;}
    .modal.fade.in{padding-top:10px!important;}
    .custom_radio{
        height:17px;
        width:17px;
        vertical-align: middle;
    margin-top: -4px !important;
    }
.nav-tabs > li {
    width: 23.8% !important;
}
.nav>li>a {
    padding: 10px 10px;
}
@media (max-width:1200px){
    .top_btn a{
        position:initial !important;
        margin-right:10px;
        margin-bottom:10px;
    }
    .m_top_1200{
        margin-top:10px;
    }
}
@media (max-width:767px){
    .m_top_767{
        margin-top:14px;
    }
}
</style>
<div class="content-wrapper">
    <?php echo $__env->make('fac-Bhavesh-0554.layouts.Headermenu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    
  <!-- Content Header (Page header) -->
    <section class="page-title content-header">
     		<h1>Appointment</h1>
    </section>
    <!-- Main content -->
    <section class="content">

	<div class="row">
		<div class="col-md-12">
			<div class="box box-success">
			     <div class="box-header" style="padding:4px;">
              
              <div class="box-tools pull-right">
                  <div class="table-title">
                	
                	</div>
              </div>
            </div>
				<div class="col-md-12">
                    <div class="panel with-nav-tabs panel-primary">
                        <div class="panel-heading">
                            <ul class="nav nav-tabs" id="myTab">
                                <?php $act ='active' ?>
                                <?php $inact ='Inactive' ?>
                                <?php if(isset($_GET['tab'])): ?>
                                    <?php $act ='Inactive' ?>
                                    <?php $inact ='active' ?>
                                <?php endif; ?>
                                
                                <li class="<?php echo e($act); ?>"><a href="#tab1primary" data-toggle="tab">Appointment</a></li>
                                <li class="<?php echo e($inact); ?>"><a data-toggle="tab" href="#tab2primary" class="hvr-shutter-in-horizontal">List of Appointment</a></li>
                                <li><a data-toggle="tab" href="#tab3primary" class="hvr-shutter-in-horizontal">Calendar</a></li>
                                <li><a data-toggle="tab" href="#tab4primary" class="hvr-shutter-in-horizontal">Schedule</a></li>
                                <!--	<li><a href="#tab3primary" data-toggle="tab" class="hvr-shutter-in-horizontal">Time Sheet</a></li>-->
                            </ul>
                        </div>
                        <div class="panel-body">
                            <div class="tab-content">
                                <div class="tab-pane fade in <?php echo e($act); ?>" id="tab1primary">
                                    
                                    <?php if( session()->has('msg') ): ?>
                                        <div class="alert alert-danger alert-dismissable"><?php echo e(session()->get('msg')); ?></div>
                                    <?php endif; ?>
                                    
                                     <?php if( session()->has('sucess') ): ?>
                                        <div class="alert alert-success alert-dismissable"><?php echo e(session()->get('success')); ?></div>
                                    <?php endif; ?>
                                    
                                    <form method="post" action="<?php echo e(route('appointment.store')); ?>" class="form-horizontal" id="appoinment_form">
                                        <?php echo e(csrf_field()); ?>

                                        <div class="Branch">
											<h1>New Appointment</h1>
										</div>
										<div class="col-md-12 top_btn">
                                            <a data-toggle="modal" data-target="#app_rules" class="btn btn-primary pull-right" style="position: absolute;right: 70px;z-index: 100;padding:0px;"><img src="https://financialservicecenter.net/public/images/app_rules.png"></a>
                                            <a class="btn btn-success pull-right" style="position: absolute;right: 0px;z-index: 100;padding:0px;"><img src="https://financialservicecenter.net/public/images/htd.png"></a>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-lg-2 col-md-3 p_cmn_991" style="padding-right:0px;">Regarding  :</label>
                                            <div class="col-lg-7 col-md-9">
                                                <div class="">
                                                    <select name="regarding" id="regarding" class="form-control fsc-input">
                                                        <option value="">---Select---</option>
                                                        <?php if(isset($regards)): ?>
                                                            <?php $__currentLoopData = $regards; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                <option value="<?php echo e($val->short_name); ?>"> <?php echo e($val->regarding); ?> <?php if(!empty($val->short_name)): ?> (<?php echo e($val->short_name); ?>) <?php endif; ?> </option>
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        <?php endif; ?>
                                                        <!--<option value="License Related">License Related</option>-->
                                                        <!--<option value="Income Tax Return Preparation Inquire">Income Tax Return Preparation Inquiry</option>-->
                                                        <!--<option value="Business Income Tax Return Preparation">Business Income Tax Return Preparation (BITR)</option>-->
                                                        <!--<option value="Personal Income Tax Return Preparation">Personal Income Tax Return Preparation (PITR)</option>-->
                                                        <!--<option value="Payroll Related">Payroll Related</option>-->
                                                        <!--<option value="Sales Tax Return">Sales Tax Return</option>-->
                                                        <!--<option value="Tax Issue Related">Tax Issue Related</option>-->
                                                        <!--<option value="Update on Status on work">Update on Status on work</option>-->
                                                        <!--<option value="Other">Other</option>-->
                                                    </select>
                                                    <input type="hidden" value="" id="regarding_shortname" name="regarding_shortname"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-lg-2 col-md-3 p_cmn_991" style="padding-right:0px;">Appointment With :</label>
                                            <div class="col-lg-4 col-md-9">
                                                <select name="employeeid" id="employee" class="form-control fsc-input">
                                                    <option value="">---Select---</option>
                                                    <?php $__currentLoopData = $employee; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $emp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($emp->employee_id); ?>"><?php echo e($emp->firstName.' '.$emp->middleName.' '.$emp->lastName); ?> <?php if(!empty($emp->teamname)): ?> (<?php echo e($emp->teamname); ?>) <?php endif; ?> </option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                            </div>
                                            <label class="control-label col-lg-1 col-md-3 p_cmn_991 m_top_1200" style="padding-right:0px;">Name :</label>
                                            <div class="col-lg-2 col-md-9 m_top_1200">
                                                <div class="">
                                                    <select name="withwhome" id="withwhome" class="form-control fsc-input">
                                                        <option value="">---Select---</option>
                                                        <option value="Approval">Client</option>
                                                        <option value="Other">Other</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="client form-group<?php echo e($errors->has('type') ? ' has-error' : ''); ?>" style="display:none;">
                                            <!--                     <div class="col-md-2">-->
                                            <!--	<div class="Branch">-->
                                            <!--		<h1>With Whom</h1>-->
                                            <!--	</div>-->
                                            <!--</div>-->
                                            <!--<label class="control-label col-md-2">Name :</label>
                                            <div class="col-md-2">
                                                <div class="">
                                                    <select name="withwhome" id="withwhome" class="form-control fsc-input">
                                                        <option value="">---Select---</option>
                                                        <option value="Approval">Client</option>
                                                        <option value="Other">Other</option>
                                                    </select>
                                                </div>
                                            </div>-->
                                            <label class="control-label col-lg-2 col-md-3  p_cmn_991" style="padding-right:0px;">Client Name :</label>
                                            <div class="col-lg-7 col-md-9">
                                                <!--<div class="searchboxmain">-->
                                                <!--    <input type="text" name="search" id="country_name" class="form-control" placeholder="Search">-->
                                                <!--    <?php echo e(csrf_field()); ?>-->
                                                <!--    <ul id="countryList"></ul>-->
                                                <!--</div>-->
                                                <div class="">
                                                    <select name="clientname" id="clientname" class="form-control " style="width:100%">
                                                        <option value="">---Select---</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>	
                                        <div class="form-group ">
                                            <label class="control-label col-lg-2 col-md-3 client1 p_cmn_991" style="display:none;padding-right:0px;">Client ID :</label>
                                            <div class="col-lg-2 col-md-9 client1" style="display:none">
                                                <div class="">
                                                    <input type="text" name="clientno" readonly id="clientno" class="form-control">
                                                </div>
                                            </div>
                                            <label class="control-label col-lg-2 col-md-3 p_cmn_991" style="padding-right:0px;">Type of Appt :</label>
                                            <div class="col-lg-3 col-md-9">
                                                <select name="work_type_id" id="work_type_id" class="form-control fsc-input">
                                                    <option value="">---Select---</option>
                                                    <?php $__currentLoopData = $work_type; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $work): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($work->short_name); ?>"><?php echo e($work-> worktype); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                                <input type="hidden" value="" id="work_type" name="work_type"/>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group client1" style="display:none;">
                                            <label class="control-label col-lg-2 col-md-3 p_cmn_991" style="padding-right:0px;">Team Rep :</label>
                                            <div class="col-lg-2 col-md-9 client1" style="display:none">
                                                <div class="">
                                                    <input type="text" name="teamRep" readonly id="teamRep" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group ">
                                            <div class="col-md-12"> 
                                                <div class="row">
                                                    <label class="control-label col-lg-2 col-md-3 col-xs-12 p_cmn_991" style="padding-right:0px;">Start Date :</label>
                                                    <div class="col-lg-2 col-md-5 col-xs-6 input-daterange">
                                                        <input type="text" name="startdate1" id="startdate" class="form-control" placeholder="Start Date">
                                                        <input type="hidden" name="startdate" id="startdate_hidden" class="form-control">
                                                    </div>
                                                    <div class="col-lg-2 col-md-4 col-xs-6">
                                                        <p name="" id="" placeholder="Day" class="form-control day1" style="margin-bottom:0px;" disabled></p>
                                                    </div>
                                                    <div class="end_date">
                                                    <div id="_enddate">
                                                        <label class="control-label col-lg-2 col-md-3 p_cmn_991 show_1200 hide_991" style="padding-right:0px;"></label>
                                                        <div class="col-lg-2 col-md-5 col-xs-6 input-daterange m_top_1200">
                                                            <input type="text" name="enddate1332" id="enddate1" class="form-control" placeholder="End Date">
                                                            <input type="hidden" name="enddate1" id="enddate_hidden" class="form-control">
                                                        </div>
                                                        <div class="col-lg-2 col-md-4 col-xs-6 m_top_1200" >
                                                            <p name="" id="" placeholder="Day" class="form-control day2" style="margin-bottom:0px;" disabled></p>
                                                        </div>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group <?php echo e($errors->has('branchtype') ? ' has-error' : ''); ?>">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <!--                     <div class="col-md-2">-->
                                                    <!--	<div class="Branch">-->
                                                    <!--		<h1>Time</h1>-->
                                                    <!--	</div>-->
                                                    <!--</div>-->
                                                    <label class="control-label col-lg-2 col-md-3 col-xs-12 left_991 p_cmn_991" style="padding-right:0px;">Start / End Time :</label>
                                                    <div class="col-lg-2 col-md-3 col-xs-6">
                                                        <input type="text" name="starttime" id="starttime" class="form-control">
                                                    </div>
                                                    <!--<label class="control-label col-md-2 pull-left" style="width: 12.5%;">End Time :</label>-->
                                                    <div class="col-lg-2 col-md-3 col-xs-6">
                                                        <input type="text" name="endtime" id="endtime" class="form-control">
                                                    </div>
                                                    <div class="col-lg-2 col-md-3 col-xs-12" style="padding-top:10px;">
                                                        <input type="checkbox" class="check" value="1" name="allday" id="allday">
                                                        <label for="allday"> All Day</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label class="control-label col-lg-2 col-md-3 p_cmn_991" style="padding-right:0px;">Participant :</label>
                                            <div class="col-lg-7 col-md-9">
                                                <select name="participants[]" id="participant" class="form-control fsc-input" multiple>
                                                    <option value="">Select</option>
                                                    <?php $__currentLoopData = $employee; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $emp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($emp->employee_id); ?>"><?php echo e($emp->firstName.' '.$emp->middleName.' '.$emp->lastName); ?> <?php if(!empty($emp->teamname)): ?> (<?php echo e($emp->teamname); ?>) <?php endif; ?> </option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-lg-2 col-md-3 p_cmn_991" style="padding-right:0px;">Priority :</label>
                                            <div class="col-lg-7 col-md-9">
                                                <select name="priority" id="priority" class="form-control">
                                                    <option value="">Select</option>
                                                    <option value="Time Sensitive">Time Sensitive</option>
                                                    <option value="Urgent">Urgent</option>
                                                    <option value="Regular">Regular</option>
                                                   
                                                </select>
                                            </div>
                                        </div>
                                        
                                         <div class="form-group">
                                            <label class="control-label col-lg-2 col-md-3 p_cmn_991" style="padding-right:0px;">Details :</label>
                                            <div class="col-lg-7 col-md-9">
                                                <textarea class="form-control" name='details'></textarea>
                                            </div>
                                        </div>
                                        
                                        <div class="card-footer">
                                            <div class="row">
                                                <label class="control-label col-lg-2 col-md-3"> </label>
                                                <div class="col-md-8">
                                                    <input class="btn btn-primary icon-btn btn_new_save" type="submit" name="submit" value="CREATE" style="width:auto;padding:8px 20px;">
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="tab-pane fade in <?php echo e($inact); ?>" id="tab2primary">
                                    <div class="col-md-12" style="padding:0px;">
                                        <?php if( session()->has('success') ): ?>
                                            <div class="alert alert-success alert-dismissable"><?php echo e(session()->get('success')); ?></div>
                                        <?php endif; ?>
                                        <div class="table-responsive">
                                            <table class="table table-hover table-bordered" id="sampleTable345">
                                                <thead>
                                                    <tr>
                                                        <th width="4%">No.</th>
                                                        <th width="8%">Priority</th>
                                                        <th width="10%">Appt. Date</th>							
                                                        <th width="10%">Appt. Time</th>
                                                        <th width="6%">Type</th>
                                                        <th width="9%">Client ID</th>
                                                        <th width="9%">Regards</th>
                                                        <th width="9%">Appt. With</th>
                                                        <th width="13%">Team Rep.</th>
                                                        
                                                        <!--<th width="12%">With Whom</th>-->
                                                        <th width="8%">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $__currentLoopData = $appodata; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $appo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                   <?php $color="#fff" ?>
                                                    <?php if($appo['priority'] == "Time Sensitive"): ?>
                                                        <?php $color = "#ffcccb" ?>
                                                    <?php elseif($appo['priority'] == "Urgent"): ?>
                                                         <?php $color = "#98FB98" ?>
                                                    <?php endif; ?>
                                                    <tr style="background-color:<?php echo e($color); ?>">
                                                        <td class="text-center"><?php echo e($loop->index+1); ?></td>
                                                        <td class="text-center"><?php echo e($appo['priority']); ?></td>
                                                        <td class="text-center"><?php echo e($appo['startdate']); ?> - <br/> <?php echo e($appo['enddate']); ?></td>
                                                        <td class="text-center"><?php echo e($appo['starttime']); ?> / <?php echo e($appo['endtime']); ?></td>
                                                        <td class="text-center" ><span data-toggle="tooltip" data-placement="top" title="<?php echo e($appo['work_type']); ?>"><?php echo e($appo['type']); ?></span></td>
                                                        <td class="text-center"><?php echo e($appo['clientId']); ?></td>
                                                        <td class="text-center"><?php echo e($appo['regarding']); ?></td>
                                                        <td class="text-center"><?php echo e($appo['appo_with']); ?></td>
                                                        <td><?php echo e($appo['teamRep']); ?></td>
                                                        <td class="text-center">
                                                            <a class="btn-action btn-view-edit" href="<?php echo e(route('appointment.edit' ,$appo['id'])); ?>"><i class="fa fa-edit"></i></a>
                                                            <form action="<?php echo e(route('appointment.destroy',$appo['id'])); ?>" method="post" style="display:none" id="delete-id-<?php echo e($appo['id']); ?>">
                                                                <?php echo e(csrf_field()); ?> <?php echo e(method_field('DELETE')); ?>

                                                            </form>
                                                            <a class="btn-action btn-delete" onclick="if(confirm('Are you sure, You want to delete this record ?'))
                                                            {event.preventDefault();document.getElementById('delete-id-<?php echo e($appo['id']); ?>').submit();} else{event.preventDefault();}" href=""><i class="fa fa-trash"></i></a>
                                                        </td>
                                                    </tr>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>    
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="tab3primary">
                                    <div id="calendar"></div>
                                </div>
    					<div class="tab-pane fade" id="tab4primary">
    					    
                        	<form enctype='multipart/form-data' class="form-horizontal changepassword" action="" id="changepassword"  method="GET"> 
                                	<?php echo e(csrf_field()); ?>

                                  <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                    
                                      <div class="form-group ">
                                        <label for="focusedinput" class="col-md-3 control-label">Schedule  Date : <span class="required"> * </span></label>
                                        <div class="col-md-9" style="margin:0 !important;padding:0 !important;">
                                          <div class="col-lg-2 col-md-3 col-sm-5 col-xs-5">
                                            <input class="form-control" name="startdate" id="startdate" type="text">
                                          </div>
                                          <label for="focusedinput" class="col-sm-1 col-xs-2 control-label text-center m_top_767">To</label>
                                          <div class="col-lg-2 col-md-3 col-sm-5 col-xs-5">
                                            <input class="form-control" name="enddate" id="enddate" type="text">
                                          </div>
                                        </div>
                                      </div>
                                      <div class="form-group ">
                                        <label for="focusedinput" class="col-md-3 control-label"></label>
                                        <div class="col-sm-8" style="margin:0 !important;padding:0 !important;">
                                          <div class="col-sm-3">
                                            <button class="btn-success btn" type="submit" name="search"> Search</button>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                
                                  <br>
                                </form> 
    		 <?php if(isset($_REQUEST['startdate']) && isset($_REQUEST['enddate'])): ?> 
    
    		 	 <div class="table-responsive">
                      <table class="table table-hover table-bordered">
            <thead>
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
    <?php
              $start = date("d",strtotime($_REQUEST['startdate']));
              $m = date("m",strtotime($_REQUEST['startdate']));
              $y = date("Y",strtotime($_REQUEST['startdate']));
              $start1 = date("d",strtotime($_REQUEST['enddate']));
    for($i=$start; $i<=$start1; $i++)
    {
        $p =$i;
    $p = sprintf("%02d",$i);
    ?>
    <th><?php echo $m.'-'.$p;?></th>
        <?php
    } ?>
    </tr>
     <tr>
                    <th>Emp. #</th>
                    <th>Emp. Name</th>
                    <th></th>
                   <?php
    for($i=$start; $i<=$start1; $i++)
    {
        $p =$i;
    $p = sprintf("%02d",$i);
        $today = $y.'-'.$m.'-'.$p; 
        ?>
        <th><?php echo substr(date('l', strtotime($today)), 0, 3);?></th>
        <?php
    }?>
                </tr>
            </thead>
    
    <tbody>
    <?php $__currentLoopData = $emp1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $em): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                   <tr>
                    <td><?php echo e($em->employee_id); ?></td>
                    <td><?php echo e($em->firstName.' '.$em->middleName.' '.$em->lastName); ?></td>
                    <td>In</td>
                   <?php
    for($i=$start; $i<=$start1; $i++)
    {
    $p =$i;
    $p = sprintf("%02d",$i);
        $today = $y.'-'.$m.'-'.$p;
        ?>
        <td>
     <?php $__currentLoopData = $employee1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $em2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
     <?php if($em2->employee_id==$em->id): ?>
      <?php $in = date('H:i',strtotime($em2->emp_in));?>
        <?php if($today==$em2->emp_in_date): ?>  <?php if($em2->emp_in==null): ?> -- <?php else: ?><?php echo e(date("g:i a", strtotime($in))); ?>  <?php endif; ?> <?php else: ?> <?php endif; ?> <?php else: ?> <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </td>	
        <?php
    }?>
             </tr>
              <tr>
                    <td></td>
                    <td></td>
                    <td>Out</td>
                    <?php
    for($i=$start; $i<=$start1; $i++)
    { 
         $p =$i;
    $p = sprintf("%02d",$i);
        $today = $y.'-'.$m.'-'.$p;
       
        ?>
        <td><?php $__currentLoopData = $employee1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $em2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php if($em2->employee_id==$em->id): ?>
        <?php $out = date('H:i',strtotime($em2->emp_out));?> <?php if($today==$em2->emp_in_date): ?> <?php if($em2->emp_out==null): ?>-- <?php else: ?>  <?php echo e(date("g:i a", strtotime($out))); ?> <?php endif; ?>  <?php else: ?>  <?php endif; ?> <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></td>	
        <?php
    }?>
    </tr>
    
    <tr>
                    <td></td>
                    <td></td>
                    <td>L In</td>
                    <?php
    for($i=$start; $i<=$start1; $i++)
    {
         $p =$i;
    $p = sprintf("%02d",$i);
       $today = $y.'-'.$m.'-'.$p;
        ?>
        <td><?php $__currentLoopData = $employee1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $em2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php if($em2->employee_id==$em->id): ?>
        <?php  $lunch_in = date('H:i',strtotime($em2->launch_in));?> <?php if($today==$em2->emp_in_date): ?> <?php if($em2->launch_in==null): ?>-- <?php else: ?> <?php echo e(date("g:i a", strtotime($lunch_in))); ?> <?php endif; ?> <?php else: ?>  <?php endif; ?> <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></td>	
        <?php
    }?>
    </tr>
    <tr>
                    <td></td>
                    <td></td>
                    <td>L Out</td>
                    <?php
    for($i=$start; $i<=$start1; $i++)
    {
         $p =$i;
    $p = sprintf("%02d",$i);
       $today = $y.'-'.$m.'-'.$p;
        ?>
        <td><?php $__currentLoopData = $employee1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $em2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php if($em2->employee_id==$em->id): ?>
        <?php  $lunch_out = date('H:i',strtotime($em2->launch_out));
       ?> <?php if($today==$em2->emp_in_date): ?> <?php if($em2->launch_out==null): ?> -- <?php else: ?> <?php echo e(date("g:i a", strtotime($lunch_out))); ?> <?php endif; ?>  <?php else: ?>  <?php endif; ?> <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></td>	
        <?php
    }?>
    </tr>
    
    <tr>
                    <td></td>
                    <td></td>
                    <td>L1 In</td>
                    <?php
    for($i=$start; $i<=$start1; $i++)
    {
         $p =$i;
    $p = sprintf("%02d",$i);
       $today = $y.'-'.$m.'-'.$p;
        ?>
        <td><?php $__currentLoopData = $employee1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $em2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php if($em2->employee_id==$em->id): ?>
        <?php  $lunch_in1 = date('H:i',strtotime($em2->launch_in_second));?> <?php if($today==$em2->emp_in_date): ?> <?php if($em2->launch_in_second==null): ?>-- <?php else: ?> <?php echo e(date("g:i a", strtotime($lunch_in1))); ?> <?php endif; ?> <?php else: ?>  <?php endif; ?> <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></td>	
        <?php
    }?>
    </tr>
    <tr>
                    <td></td>
                    <td></td>
                    <td>L1 Out</td>
                    <?php
    for($i=$start; $i<=$start1; $i++)
    {
         $p =$i;
    $p = sprintf("%02d",$i);
       $today = $y.'-'.$m.'-'.$p;
        ?>
        <td><?php $__currentLoopData = $employee1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $em2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php if($em2->employee_id==$em->id): ?>
        <?php  $lunch_out1 = date('H:i',strtotime($em2->launch_out_second));
       ?> <?php if($today==$em2->emp_in_date): ?> <?php if($em2->launch_out_second==null): ?> -- <?php else: ?> <?php echo e(date("g:i a", strtotime($lunch_out1))); ?> <?php endif; ?>  <?php else: ?>  <?php endif; ?> <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></td>	
        <?php
    }?>
    </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>   
    
    <tr>
                    <td></td>
                    <td></td>
                    <td></td>
    
        <td>Total Hours : <b> <?php echo e(number_format($sum, 2)); ?></b></td>
    
    </tr>
    </tbody>
    </table>
    </div>
     <?php else: ?> 		    
     <?php endif; ?>
    <!--<p style="color:red;font-weight:bold">NOTE : <span style="color:black;font-weight:normal">Employee Responsibility :</span></p>  
    <ul>
        <li>Record  clock in and out immediately when they come in/out.</li>
        <li>All the employee make sure that time recorded paid for that in their payroll.</li>
        <li>If not please notify immediately to payroll department.</li>
        <li>Leave request get approved before 14 days in advance.</li>
        <li>All the employee must take lunch brake as per company rules.</li>
        <li>If you have any question contact office manager immediately.</li>
        <li>Office Lunch Hrs. 1.00 p.m. to 2.00 p.m.</li>
        <li>Work as per schdule time - After schdule hrs will not be consider.</li>
        <li>Overtime will not be granted without prior approval or sign by supervisor.</li>
    </ul>-->
    					</div>
    					
    					    </div>
    					</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    <div class="modal fade" id="app_rules" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Appointment Rules</h4>
                  </div>
                <div class="modal-body" >
                    <div class="row">
                        <div class="input_rules">
                            <div class="col-xs-12" style="margin-bottom:10px;">
                                <div class="col-xs-1 text-center" style="padding-top:7px;">1.</div>
                                <div class="col-xs-10">
                                    <input type="text" class="form-control">
                                </div>
                                <div class="col-xs-1"><a class="btn btn-primary" id="add_rule"><i class="fa fa-plus"></i></a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn_new_save" style="width: auto;display: initial;margin-bottom: 0px;padding: 5px 16px;">Save</button>
                    <button class="btn btn-danger btn_new_cancel" data-dismiss="modal" aria-hidden="true" style="width: auto;display: initial;margin-bottom: 0px;padding: 5px 16px;">Close</button>
                </div>
            </div>
        </div>
    </div>
	</section>
<!--</div>-->
<script type="text/javascript">
$("#starttime").timepicker({use24hours: true});
$("#endtime").timepicker({use24hours: true});
</script>
<script>
var $start = $(".input-daterange").find('#startdate');
var $end = $(".input-daterange").find('#enddate');
$(".input-daterange").datepicker({
    orientation: "bottom left",
    autoclose: true,
    start: '+1d',
});
$end.on('show', function(e){
    var date = $start.datepicker('getDate');
        date = moment(date).add(1, 'year').toDate();
        $end.datepicker('setEndDate', date);
});
</script>
<script>
  $('#employee,#regarding,#withwhome,#clientname,#participant,#work_type_id').select2();
  
$('#employee,#regarding,#withwhome,#clientname').on('select2:opening select2:closing', function( event ) {
    var $searchfield = $(this).parent().find('.select2-search__field');
    $searchfield.prop('disabled', true);
});
  </script>
  
  
  <script>
  
  function getClientdetailForPraposal(id)
    {
        //var query = $(this).val();
        //alert(id);
        if(id != '')
        { 
         var _token = $('input[name="_token"]').val();
         $.ajax({
            url:"<?php echo e(route('admin.getDetailPraposal')); ?>",
            method:"POST",
            data:{query:id, _token:_token},
            success:function(data){
                var json = JSON.parse(data);
                //console.log(data);
                 $("#lbl_title").empty();
                for(var i =0;i<json.length;i++){
                    $('.client1').show(); 
                    $('#clientno').val(json[i]['filename']);
                }
                $('#countryList').empty();
            }
         });
        }
    }
  
  
  
  
$(document).ready(function(){
    
    $('#_enddate').hide();
    
    $('#work_type_id').on('change',function(){
        var id = $(this).find(':selected').text();
        $('#work_type').val(id);
        if(id=="Work"){
            $('#_enddate').show();    
        }else{
           $('#_enddate').hide();   
        }
    });
    
    $('#regarding').on('change',function(){
        var id = $(this).find(':selected').text();
        $('#regarding_shortname').val(id);
    });
    
    
    $('#country_name').keyup(function()
    { 
        var query = $(this).val();
        //alert(query);
        if(query != '')
        { 
         var _token = $('input[name="_token"]').val();
         $.ajax({
          url:"<?php echo e(route('admin.praposal_fetch')); ?>",
          method:"POST",
          data:{query:query, _token:_token},
          success:function(data){
            $('#countryList').fadeIn();
            $('#countryList').html(data);
          }
         });
        }
        else{
            $('#countryList').empty();
        }
    });
    
    
    
	$(document).on('change','#withwhome', function()
	{
		var id = $(this).val();
		$.get('<?php echo URL::to('/clientid'); ?>?id='+id, function(data)
		{  
		    //console.log(data);
		    
            $('#clientname').empty();
            $('.client').hide(); 
            $('.client1').hide(); 
            $('#clientname').append('<option value="">---Select Client---</option>');
            $.each(data, function(index, subcatobj){
                if(id=='Approval'){
                     $('.client').show();
                    //  var middelname = '';
                    //  if(subcatobj.company_name != null){
                    //      var middelname = subcatobj.company_name;
                    //  }
                     $('#clientname').append('<option value="' + subcatobj.filename + '">' + subcatobj.filename + ' &nbsp;&nbsp; | &nbsp;&nbsp;' + subcatobj.company_name + '&nbsp;&nbsp; | &nbsp;&nbsp;' + subcatobj.business_no + '</option>');
                }
            })
        });
	});
});
</script>
  <script>
$(document).ready(function(){
	$(document).on('change','#clientname', function(){
		var id = $(this).val();
		$('#clientno').val(id);
		$.get('<?php echo URL::to('fac-Bhavesh-0554/appointment/getTeamRep'); ?>/'+id, function(data)
		{
		    $('#teamRep').val(data);
		});
		$('.client1').show();
		
	});
});
</script>

 <script type="text/javascript">
// $('#startdate').datepicker({
//     onSelect: function (dateString, txtDate) {
//       DisplayDate("Selected Date: " + dateString + "\nTextBox ID: " + txtDate.id);
//     }
    
// });
// $('#enddate').datepicker({
//     autoclose: true,
    
// });

function DisplayDate(message) {
    alert(message);
};


// $('#startdate,#enddate').on('show', function(e){
//     console.debug('show', e.date, $(this).data('stickyDate'));
    
//     if ( e.date ) {
//          $(this).data('stickyDate', e.date);
//     }
//     else {
//          $(this).data('stickyDate', null);
//     }
// });

// $('#startdate,#enddate').on('hide', function(e){
//     console.debug('hide', e.date, $(this).data('stickyDate'));
//     var stickyDate = $(this).data('stickyDate');
    
//     if ( !e.date && stickyDate ) {
//         console.debug('restore stickyDate', stickyDate);
//         $(this).datepicker('setDate', stickyDate);
//         $(this).data('stickyDate', null);
//     }
// });


        </script>
        <script type="text/javascript">
   $(document).ready(function(){
   	$('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
   		localStorage.setItem('activeTab', $(e.target).attr('href'));
   	});
   	var activeTab = localStorage.getItem('activeTab');
   	if(activeTab){
   		$('#myTab a[href="' + activeTab + '"]').tab('show');
   	}
   });
   
    $("#add_rule").click(function() {
        $(".input_rules").append('<div class="col-md-12" style="margin-bottom:10px;"><div class="col-xs-1 text-center" style="padding-top:7px;">2.</div><div class="col-xs-10"><input type="text" class="form-control"></div><div class="col-xs-1"><a class="btn btn-danger" id="rem_rule"><i class="fa fa-minus"></i></a></div></div>');
    });
    $(".input_rules").on('click','#rem_rule',function(){
        $(this).parent().parent().remove();
    });
</script>
<script>
  $(function () {

    /* initialize the external events
     -----------------------------------------------------------------*/
    function init_events(ele) {
      ele.each(function () {

                // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
                // it doesn't need to have a start or end
                var eventObject = {
                  title: $.trim($(this).text()) // use the element's text as the event title
                }
            
                // store the Event Object in the DOM element so we can get to it later
                $(this).data('eventObject', eventObject)
            
                // make the event draggable using jQuery UI
                $(this).draggable({
                  zIndex        : 1070,
                  revert        : true, 
                });// will cause the event to go back to its
                $('#startdate,#enddate').datepicker({
                    autoclose: true,
                    dateFormat: 'dd-mm-yy'
                });
        
            $('#startdate,#enddate').on('show', function(e){
                console.debug('show', e.date, $(this).data('stickyDate'));
                
                if ( e.date ) {
                     $(this).data('stickyDate', e.date);
                }
                else {
                     $(this).data('stickyDate', null);
                }
            });
        
            $('#startdate,#enddate').on('hide', function(e){
                console.debug('hide', e.date, $(this).data('stickyDate'));
                var stickyDate = $(this).data('stickyDate');
                
                if ( !e.date && stickyDate ) {
                    console.debug('restore stickyDate', stickyDate);
                    $(this).datepicker('setDate', stickyDate);
                    $(this).data('stickyDate', null);
                }
            });
        });
    }
});

        </script> 
    
    
    <link href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.css" rel="stylesheet"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/qtip2/3.0.3/jquery.qtip.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.2/fullcalendar.js"></script>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/qtip2/3.0.3/jquery.qtip.min.js"></script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>

<script>
$(document).ready(function(){
    
    $("#allday").change(function() {
        
    });
    
    
    
    $("#appoinment_form").validate({
           rules: {
               regarding: {
                   required: true,
               },
               withwhome: {
                   required: true,
               },
               work_type_id: {
                   required: true,
               },
               priority: {
                   required: true,
               },
               employeeid: {
                   required: true,
               }
           },
           messages: {
               regarding: {
                   required: "Please Select Regarding",
               },
               withwhome: {
                   required: "Please Select with whome",
               },
               work_type_id: {
                   required: "Please select type of work",
               },
               priority: {
                   required: "Please select priority",
               },
               employeeid: {
                   required: "Please select appoinment with",
               }
           },
           highlight: function(e) {
               $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
               $(e).closest('.form-tab').removeClass('has-info').addClass('has-error');
           },
       
           success: function(e) {
               $(e).closest('.form-group').removeClass('has-error');
               $(e).closest('.form-tab').removeClass('has-error'); //.addClass('has-info');
               $(e).remove();
           },
           submitHandler: function(form) {
               form.submit();
               
           }
       });
	
	
});
</script>




<script>
  $(function () {

    /* initialize the external events
     -----------------------------------------------------------------*/
    function init_events(ele) {
      ele.each(function () {

        // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
        // it doesn't need to have a start or end
        var eventObject = {
          title: $.trim($(this).text()) // use the element's text as the event title
        }

        // store the Event Object in the DOM element so we can get to it later
        $(this).data('eventObject', eventObject)

        // make the event draggable using jQuery UI
        $(this).draggable({
          zIndex        : 1070,
          revert        : true, // will cause the event to go back to its
          revertDuration: 0  //  original position after the drag
        })

      })
    }

    init_events($('#external-events div.external-event'))

    /* initialize the calendar
     -----------------------------------------------------------------*/
    //Date for the calendar events (dummy data)
    var date = new Date()
    var d    = date.getDate(),
        m    = date.getMonth()+1,
        y    = date.getFullYear(),
        day = date.getDay();
    
    var d2 = (d>9) ? d : "0"+d;
    var m2 = (m>9) ? d : "0"+m;
        
        
    var days = [
                'Sunday',
                'Monday',
                'Tuesday',
                'Wednesday',
                'Thursday',
                'Friday',
                'Saturday'
            ];
        //Not changeable area
    $('#calendar').fullCalendar({
        
        views: {
            month: {
              columnHeaderFormat: 'dddd'
            }
        },
        header    : {
            left  : 'prev,myCustom1,myCustom2,next',
            center: 'title',
            right : 'month,agendaWeek,agendaDay'
        },
      
        firstDay: 1,
        allDayText: 'All Day',
        customButtons: {
            myCustom1: { 
                text: m2+'-'+d2+'-'+y,
            },
            myCustom2: {
                text: days[day],
            }
        },
        buttonText: {
            today: '04-03-2021',
            month: 'Month',
            week : 'Week',
            day  : 'Day'
        },
        views: {
            month: {columnFormat:'dddd',},
            week: {columnFormat:'dddd[ ]M/D[ ]',},
        },
     
        events:'<?php echo e(URL('fac-Bhavesh-0554/appointment/getAppointment')); ?>',
      
        eventDisplay:'auto',
        eventColor: '#378006',
        eventBackgroundColor:'#f0f0f0',
        eventRender: function(event, element) {
            //element.find('.fc-title').append("<br/>"); 
            element.find('.fc-content').css('background','linear-gradient(-180deg, '+event.color+', #e5e5e5)');
            element.find('.fc-time').hide();
            element.qtip({
                content: event.description + '<br />',
                style: {
                    background: 'black',
                    color: '#FFFFFF'
                },
                position: {
                    my: 'bottom center',
                    at: 'top center',
                    target: 'event',
                    viewport: $('#calendar'),
                    adjust: {
                        mouse: false,
                        scroll: false
                    }
                },
            });
        },
        //End Changeable area
        editable  : true,
        droppable : true, // this allows things to be dropped onto the calendar !!!
        drop      : function (date, allDay) { // this function is called when something is dropped

            // retrieve the dropped element's stored Event Object
            var originalEventObject = $(this).data('eventObject')
    
            // we need to copy it, so that multiple events don't have a reference to the same object
            var copiedEventObject = $.extend({}, originalEventObject)
    
            // assign it the date that was reported
            copiedEventObject.start           = date
            copiedEventObject.allDay          = allDay
            copiedEventObject.backgroundColor = $(this).css('background-color')
            copiedEventObject.borderColor     = $(this).css('border-color')
    
            // render the event on the calendar
            // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
            $('#calendar').fullCalendar('renderEvent', copiedEventObject, true)
    
            // is the "remove after drop" checkbox checked?
            if ($('#drop-remove').is(':checked')) {
              // if so, remove the element from the "Draggable Events" list
              $(this).remove()
            }
        }
    })

    /* ADDING EVENTS */
    var currColor = '#3c8dbc' //Red by default
    //Color chooser button
    var colorChooser = $('#color-chooser-btn')
    $('#color-chooser > li > a').click(function (e) {
      e.preventDefault()
      //Save color
      currColor = $(this).css('color')
      //Add color effect to button
      $('#add-new-event').css({ 'background-color': currColor, 'border-color': currColor })
    })
    $('#add-new-event').click(function (e) {
      e.preventDefault()
      //Get value and make sure it is not null
      var val = $('#new-event').val()
      if (val.length == 0) {
        return
      }

      //Create events
      var event = $('<div />')
      event.css({
        'background-color': currColor,
        'border-color'    : currColor,
        'color'           : '#fff'
      }).addClass('external-event')
      event.html(val)
      $('#external-events').prepend(event)

      //Add draggable funtionality
      init_events(event)

      //Remove event from text input
      $('#new-event').val('')
    })
  })
  
  $( document ).ready(function() {
        $("#startdate").datepicker({
            dateFormat: "mm-dd-yy",
            onSelect: function(dateText, inst) {
                // var date = $.datepicker.parseDate(inst.settings.dateFormat || $.datepicker._defaults.dateFormat, dateText, inst.settings);
                // var dateText = $.datepicker.formatDate("DD", date, inst.settings);
                // $(".day1").html( dateText );
                // $(".day2").html( dateText );
                var stdate = $("#startdate").val();
                $("#enddate1").val(stdate);// Just the day of week
                
                var dayName = setDayname(stdate);
                var formats = reformatDate(stdate);
                
                $('#startdate_hidden').val(formats);
                $('#enddate_hidden').val(formats);
                
                $(".day1").html( dayName );
      	        $(".day2").html( dayName );
            },
            minDate: 0
        });
        $("#enddate1").datepicker({
            dateFormat: "mm-dd-yy",
            onSelect: function(dateText, inst) {
                
                var enddate = $("#enddate1").val();
                
                var dayName = setDayname(enddate);
                var formats = reformatDate(enddate);
                
                $(".day2").html( dayName );
                $('#enddate_hidden').val(formats);
            },
            minDate: 0
        });
    });
    
    function reformatDate(dateString){
        var d = new Date(dateString);
        var day    = d.getDate(),
            month    = d.getMonth()+1,
            year    = d.getFullYear();
        var d2 = (day>9) ? day : "0"+day;
        var m2 = (month>9) ? month : "0"+month;
        var stringdate = d2+"-"+m2+"-"+year;
        return stringdate;

    }
    
    function setDayname(datestring){
      	var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    	var d = new Date(datestring);
    	var dayName = days[d.getDay()];
    	return dayName;
      	 
    }
  
</script>
<script type="text/javascript">
    $(function () {
        $("#allday").click(function () {
            if ($(this).is(":checked")) {
                $("#starttime").attr("readonly", "readonly");
                $("#starttime").val("12:00 AM");
                $("#endtime").attr("readonly", "readonly");
                $("#endtime").val("11:30 PM");
            } else {
                $("#starttime").removeAttr("readonly");
                $("#endtime").removeAttr("readonly");
                $("#starttime").val("");
                $("#endtime").val("");
            }
        });
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>