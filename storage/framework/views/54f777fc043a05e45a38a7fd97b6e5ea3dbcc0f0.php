 
<?php $__env->startSection('main-content'); ?>
<style>
	ul li{list-style:decimal-leading-zero !important;}
	.treeview-menu ul li{list-style:none !important;}
	.btn-center { margin:auto; width:250px; }
	.remove {display: block;padding: 6px 16px;margin: -52px 0 0 0;position: absolute;right: 134px;/* top: 0; */}
	/*label{float:right}*/
	.addMore{position: absolute;right: 151px;top: 66px;}
	.nav-tabs>li{width:19.5% !important;
	    margin: 2px 0 0 5px !important;
	}
	.remove11 {
	    display: block;
	    padding: 6px 16px;
	    margin: -52px 0 0 0;
	    position: absolute;
	    right: 202px;
	    /* top: 0; */
	}
	.select2-container--default .select2-selection--single .select2-selection__rendered{font-size:16px !important;padding-left: 0px; margin-top: -1px;}
.select2-container--default.select2-container--focus .select2-selection--multiple, .select2-container--default .select2-search--dropdown .select2-search__field {border-color: #3c8dbc !important;border: 2px solid #2fa6f2;border-radius: 4px;}
.select2-container--default .select2-selection--single {
    background-color: #fff;border-radius: 4px;mborder: 2px solid #2fa6f2;}
.select2-container--default .select2-selection--multiple{border: 2px solid #2fa6f2;    }

	.nav-tabs{
	    padding:5px !important;
	}
	.form-control1 {
	    width: 100%;
	    line-height: 1.44;
	    color: #555!important;
	    border: 2px solid #286db5;
	    border-radius: 3px;
	    transition: border-color ease-in-out .15s;
	    padding: 3px 3px 7px 8px!important;
	}
	.Red{background-color:red;color:#fff !important;}
	.Blue{background-color:red !important;color:#fff}
	.Green{background-color:#00ef00 !important;color:#fff}
	.Yellow{background-color:Yellow !important;}
	.Orange{background-color:Orange !important;color:#fff}
	.checkbox label:after, 
	.radio label:after {
	    content: '';
	    display: table;
	    clear: both;
	}
	
	.checkbox .cr,
	.radio .cr {
	    position: relative;
	    display: inline-block;
	    border: 1px solid #000;
	    border-radius: .25em;
	    width: 1.3em;
	    height: 1.3em;
	    float: left;
	    margin-right: .5em;
	}
	.radio .cr {border-radius: 50%;}
	.checkbox .cr .cr-icon,
	.radio .cr .cr-icon {
	    position: absolute;
	    font-size: .8em;
	    line-height: 0;
	    top: 43%;
	    left: 20%;
	}
	.radio .cr .cr-icon {
	    margin-left: 0.04em;
	}
	.checkbox label input[type="checkbox"],
	.radio label input[type="radio"] {
	    display: none;
	}
	.checkbox label input[type="checkbox"] + .cr > .cr-icon,
	.radio label input[type="radio"] + .cr > .cr-icon {
	    transform: scale(3) rotateZ(-20deg);
	    opacity: 0;
	    transition: all .3s ease-in;
	}
	.checkbox label input[type="checkbox"]:checked + .cr > .cr-icon,
	.radio label input[type="radio"]:checked + .cr > .cr-icon {
	    transform: scale(1) rotateZ(0deg);
	    opacity: 1;
	}
	.checkbox label input[type="checkbox"]:disabled + .cr,
	.radio label input[type="radio"]:disabled + .cr {
	    opacity: 1;
	}
	.model-width{margin: auto; width: 485px;border: #3668f6 1px solid;border-radius: 0;}
	.model-width .modal-header {padding: 4px 10px; background: #fff;}
	.model-width .modal-header h4{font-size:16px;}
	.model-width .modal-footer{text-align:center;}
	.primary {background-color: #d0d0d0 !important;border-radius: 0px;color: #000;padding: 5px 10px;width: 93px;font-weight: 200px;}
	.langcheckbox label{float:none;}
	   .disabledd{pointer-events:none;}
	   .checkboxbox label{float:none;}
	   .martop7{margin-top:8px;}
	   .text-center{text-align:center;}
	   .usergihtsbox{border:0px solid #ccc; margin-top:30px; }
	   .usergihtsbox .tab-content{ padding:20px 25px; min-height:200px; border: 2px solid #ccc; margin-top: -6px;}
	  .usergihtsbox .nav-tabs>li{margin-left:0px!important; border-radius:5px 5px 0px 0px!important; overflow:hidden!important; margin-right:2px!important; background:#e6e6e6; width:auto!important;}
	  .usergihtsbox .nav-tabs>li a{border:0px!important; border-radius:0px!important; }
	  .usergihtsbox  .nav-tabs > li.active{border:1px solid #12186b!important;}
	  .usergihtsbox .nav-tabs{padding-left:0px!important; padding-right:0px!important;}
	  .subtabsbox{display:flex; flex-direction:row; justify-content:flex-start;}
	  .sidemenucnt{width: calc(100% - 200px)!important; padding: 12px; min-height:200px; border:1px solid #ccc; margin-top:7px;}
	  .subtabsbox .sidemenutab{width:200px;}
	  .subtabsbox .sidemenutab .nav.nav-tabs li, .subtabsbox .sidemenutab .nav.nav-tabs li a{width:100%!important; border-radius:0px!important; font-size:13px!important; text-align:left!important;}
	  .subtabsbox .sidemenutab .nav.nav-tabs li a{padding:7px 15px;}
	  .subtabsbox .tab-pane.fade{display:none;}
	   .subtabsbox .tab-pane.fade.in{display:block;}
	   .select2-container--default .select2-selection--multiple .select2-selection__choice{font-size:14px!important;}
	   .select2-dropdown.select2-dropdown--below, .select2-dropdown.select2-dropdown--above{width:400px!important;}
	   .chkboxtab{margin: 0px;
    padding: 0px;
    list-style-type: none;
    display: flex;
    flex-direction: row;
    justify-content: flex-start;}
    .chkboxtab li{list-style-type: none!important;
    border: 1px solid #ccc;
    border-radius: 3px;
    padding: 3px 10px 0px 5px;
    margin: 0px 5px;
    background: #f2f2f2;}
     .chkboxtab li label{margin-bottom:0px!important;}
     .chklabel{text-align:center;float:left}
     .userpermissionbox{display:block; }
     .userpermissionbox .row{border-bottom:1px solid #ccc; margin-bottom:10px; padding-bottom:10px;}
     .userpermissionbox .row label.perttl{padding-top: 5px;
    border-right: 1px solid #ccc;
    padding-right: 20px;
    margin-bottom: 0px;
    height: 29px;}
    
    #alerts .modal-width{
    margin: auto;
    width: 390px!important;
    border: #3668f6 1px solid!important;
    border-radius: 0;
}
#tab9primary .select2-container{
    width:100% !important;
}
.table-responsive table.table {
    display: revert !important;
}
ul.nav.nav-tabs.tabs1{
    padding: 12px;
    background: #fffaf4 !important;
    border: 1px solid #df820f !important;
}
ul.nav.nav-tabs.tabs1>li>a {
    background: #ffe2bf !important;
    color: black !important;
    border: 1px solid #ff9a1e !important;
}
ul.nav.nav-tabs.tabs1 > li.active > a, ul.nav.nav-tabs.tabs1 > li.active > a:focus, ul.nav.nav-tabs.tabs1 > li.active > a:hover{
    border: 1px solid #945201;
    background: #ff9a1e !important;
    color:#ffffff !important;
}
@media  only screen and (max-width: 1272px){
    .nav-tabs > li {
        width: 19.3% !important;
    }
}
@media  only screen and (max-width: 991px){
    .left_991{
        float:left !important;
    }
    .userpermissionbox .row label.perttl{
        border:none;
    }
    .chkboxtab {
        width: 100%;
    }
    .chkboxtab li{
        margin: 0px 5px 0px 0px;
    }
}
@media  only screen and (max-width: 987px){
    .nav-tabs > li {
        width: 19.1% !important;
    }
}
@media  only screen and (max-width: 930px){
    .nav-tabs > li {
        width: 24% !important;
    }
    #utab6 .table-responsive table.table {
        display: block !important;
        overflow-x: auto;
        white-space: nowrap;
    }
}
@media  only screen and (max-width: 791px){
    .nav-tabs > li {
        width: 32% !important;
    }
}
@media  only screen and (max-width: 767px){
    .nav-tabs > li {
        width: 24% !important;
    }
}
@media  only screen and (max-width: 550px){
    .nav-tabs > li {
        width: 32% !important;
    }
}
@media  only screen and (max-width: 425px){
    .nav-tabs > li {
        width: 48% !important;
    }
}
</style>
<?php if($emp->type=='user'): ?>
<style>
	#tab3primary,.tttt{pointer-events: none;background: #ccc !important;}
	.tttt1{display:none !important;}
</style>
<?php endif; ?>
<div class="content-wrapper">
	<section class="content-header page-title" >
		<div class="">
            <h2><?php echo e($emp->firstName); ?> <?php echo e($emp->lastName); ?> 
            <span class="right_title" style="text-align:right;padding-right: 20px !important;position: absolute;right: 0;"> <?php if($emp->type=='clientemployee'): ?> Client Employee <?php else: ?> FSC <?php echo e(ucfirst($emp->type)); ?> <?php endif; ?></span></h2>
		</div>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-success">
					<div class="card-body">
						<div class="panel with-nav-tabs panel-primary">
							<div class="panel-heading">
							    
							    <?php $active = 'active' ?>
							    <?php $Inactive = '' ?>
							    
							    <?php if(isset($_GET['tab'])): ?>
							        <?php $active = '' ?>
							        <?php $Inactive = 'active' ?>
							    <?php else: ?>
							        <?php $active = 'active' ?>
							        <?php $Inactive = '' ?>
							    <?php endif; ?>
							    
							    <ul class="nav nav-tabs" id="myTab">
									<li class="<?php echo e($active); ?>"><a href="#tab1primary" data-toggle="tab">General Info</a>
									</li>
									<li><a data-toggle="tab" href="#tab2primary" class="hvr-shutter-in-horizontal">Hiring Info</a>
									</li>
									<li><a href="#tab3primary" data-toggle="tab" class="hvr-shutter-in-horizontal">Pay Info</a>
									</li>
									<li><a href="#tab4primary" data-toggle="tab" class="hvr-shutter-in-horizontal">Personal Info</a>
									</li>
									<li><a href="#tab5primary" data-toggle="tab" class="hvr-shutter-in-horizontal">Security Info</a>
									</li>
									<li><a href="#tab7primary" data-toggle="tab" class="hvr-shutter-in-horizontal">User Rights</a>
									</li>
									<li><a href="#tab8primary" data-toggle="tab" class="hvr-shutter-in-horizontal">Rules</a>
									</li>
									<li><a href="#tab9primary" data-toggle="tab" class="hvr-shutter-in-horizontal">Responsibility</a>
									</li>
									<li><a href="#tab10primary" data-toggle="tab" class="hvr-shutter-in-horizontal">Agreement</a>
									</li>
									<li class="<?php echo e($Inactive); ?>"><a href="#tab11primary" data-toggle="tab" class="hvr-shutter-in-horizontal">Other</a>
									</li>
								</ul>
							</div>
							<?php if( session()->has('success') ): ?>
							<div class="alert alert-success alert-dismissable"><?php echo e(session()->get('success')); ?></div><?php endif; ?>
							<form method="post" action="<?php echo e(route('employee.update',$emp->id)); ?>" id="registrationForm" class="form-horizontal" enctype="multipart/form-data">
							    <?php echo e(csrf_field()); ?><?php echo e(method_field('PATCH')); ?>

								<div class="panel-body">
									<input name="text1" value="" type="hidden" placeholder="" id="text1" class="textonly form-control">
									<div class="tab-content">
										<div class="tab-pane fade" id="tab10primary">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="Branch" style="text-align:left; padding-left:15px;">
													<h1 class="text-center">Agreement</h1>
												</div>
											</div>
										</div>
										
										<div class="tab-pane fade in <?php echo e($Inactive); ?>" id="tab11primary">
										    <ul class="nav nav-tabs tabs1" role="tablist">
                                             <li role="presentation" class="active"><a href="#conv_sheet_tab" aria-controls="profile" role="tab" data-toggle="tab"> <span>Conversation Sheet</span></a></li>
                                             <li role="presentation"><a href="#appoint_tab" aria-controls="settings" role="tab" data-toggle="tab"><span>Appointment</span></a></li>
                                             <li role="presentation"><a href="#notes_tab" aria-controls="settings" role="tab" data-toggle="tab"><span>Notes</span></a></li>
                                          </ul>
                                            <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane active" id="conv_sheet_tab">
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
        												<div class="Branch" style="text-align:left; padding-left:15px;">
        													<h1 class="text-center">Conversation List</h1>
        												</div>
        											</div>
        											<br><br>
        							
        											<div class="col-md-12 col-sm-12 col-xs-12 text-right">
        						       
        						                        <div class="clear"></div>
        						                         <table class="table table-hover table-bordered dataTable no-footer">
                        						           <thead>
                        						               <tr>
                        						                    <th>No.</th>
                                									<th> Date</br> Day</br> Time</th>
                                									<th>Related To</th>
                                									<th>Description</th>
                                									<th>Notes</th>
                                									<th>Action</th>
                        						               </tr>
                        						           </thead>
                        						           <?php 
                        						           	$cnt=count($conversation);
                        						           	if($cnt>0)
                        						           	{
                        						           	    $cnt=0;
                        						           	?>
                        						           <?php $__currentLoopData = $conversation; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $conversation): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        								    <tr>
                        								        <td style="text-align:center;"><?php echo e($loop->index+1); ?> </td>
                        								        <td style="text-align:center;"><?php echo e($conversation->creattiondate); ?><br> <?php echo e($conversation->day); ?> <br> <?php echo e($conversation->time); ?></td>
                        								        <td style="text-align:center;"><?php echo e($conversation->relatednames); ?> </td>
                        								        <td><?php echo e($conversation->condescription); ?> </td>
                                                                <td><?php echo e($conversation->connotes); ?> </td>
                                                                <td style="text-align:center;">
        									                        
        				                                            <a class="btn-action btn-view-edit conversationID" data-id="<?php echo e($conversation->id); ?>"><i class="fa fa-edit"></i></a>
        									                   </td>
                        						            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        						            </tr>
                        						            <?php
                        						           	}
                        						           	else
                        						           	{
                        						           ?>
                        						            <tr><td style="text-align:center;" colspan="7">No records found</td></tr>
                        						            <?php
                        						           	}
                        						            ?>
                        						            </tbody>
                        						           
                        						       </table>
        						                    </div>
                                                </div>
                                                <div role="tabpanel" class="tab-pane" id="appoint_tab">
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
        												<div class="Branch" style="text-align:left; padding-left:15px;">
        													<h1 class="text-center">Appointment</h1>
        												</div>
        											</div>
        											<div class="clear clearfix"></div>
        											<div class="table-responsive">
                                                        <table class="table table-bordered">
                                                            <thead>
                                                                <tr>
                                                                    <th>No.</th>
                                                                    <th>Priority</th>
                                                                    <th>Appt. Date</th>
                                                                    <th>Appt. Time</th>
                                                                    <th>Type</th>
                                                                    <th>Client ID</th>
                                                                    <th>Regards</th>
                                                                    <th>Appt. With</th>
                                                                    <th>Team Rep.</th>
                                                                    <th>Action</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php $__currentLoopData = $appodata; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $appo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                    <?php $color="#fff" ?>
                                                                    <?php if($appo['priority'] == "Time Sensitive"): ?>
                                                                        <?php $color = "#ffcccb" ?>
                                                                    <?php elseif($appo['priority'] == "Urgent"): ?>
                                                                         <?php $color = "#98FB98" ?>
                                                                    <?php endif; ?>
                                                                    <tr style="background-color:<?php echo e($color); ?>">
                                                                        <td class="text-center"><?php echo e($loop->index+1); ?></td>
                                                                        <td class="text-center"><?php echo e($appo['priority']); ?></td>
                                                                        <td class="text-center"><?php echo e($appo['startdate']); ?> - <br/> <?php echo e($appo['enddate']); ?></td>
                                                                        <td class="text-center"><?php echo e($appo['starttime']); ?> / <?php echo e($appo['endtime']); ?></td>
                                                                        <td class="text-center" ><span data-toggle="tooltip" data-placement="top" title="<?php echo e($appo['work_type']); ?>"><?php echo e($appo['type']); ?></span></td>
                                                                        <td class="text-center"><?php echo e($appo['clientId']); ?></td>
                                                                        <td class="text-center"><?php echo e($appo['regarding']); ?></td>
                                                                        <td class="text-center"><?php echo e($appo['appo_with']); ?></td>
                                                                        <td><?php echo e($appo['teamRep']); ?></td>
                                                                        <td class="text-center">
                                                                            <a class="btn-action btn-view-edit" href="#"><i class="fa fa-eye"></i></a>
                                                                        </td>
                                                                    </tr>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div role="tabpanel" class="tab-pane" id="notes_tab">
                                                    <!--<?php if($emp->type !='user')
										    {?>
										    <div class="col-md-12 col-sm-12 col-xs-12">
												<div class="Branch" style="text-align:left; padding-left:15px;">
													<h1 style="text-align:center;">Holiday</h1>
												</div>
											</div>
											<div class="col-md-12 col-sm-12 col-xs-12 checkboxbox">
											    <div class="row">
											     <?php
												           $myStringh = $emp->holidays;
                                                            $arrayh =explode(",",$myStringh); ?>
                                                            
												             <?php $__currentLoopData = $holiday; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lan1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
												            <div class="col-md-4"><input type="checkbox"  id="holiday_<?php echo e($lan1->holiday_name); ?>" 
												            <?php foreach($arrayh as $arr){ if($arr==$lan1->holiday_name){?> checked <?php } } ?> 
												            name="holidays[]" value="<?php echo e($lan1->holiday_name); ?>"> 
												            <label class="fsc-form-label" for="holiday_<?php echo e($lan1->holiday_name); ?>"> <?php echo e($lan1->holiday_name); ?></label>
												            </div>
												            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											
										
										    </div>
										    </div>
										    	<?php
										    }
										    ?>-->
										    <div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="Branch" style="text-align:left; padding-left:15px;">
													<h1 class="text-center">Notes</h1>
												</div>
											</div>
											<br>
											
											<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="input_fields_wrap_notes">
													<?php $l=1; $notecon=count($admin_notes);?><?php if($notecon!=NULL): ?> <?php $__currentLoopData = $admin_notes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $notes): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
													<input name="noteid[]" value="<?php echo e($notes->id); ?>" type="hidden" placeholder="" id="noteid" class="textonly form-control">
													<div class="form-group">
														<label class="control-label col-md-3">Note
															<?php echo $l; $l++;?>:</label>
														<div class="col-md-6">
															<input name="adminnotes[]" value="<?php echo e($notes->notes); ?>" type="text" placeholder="Create Note" id="adminnotes" class="textonly form-control">
														</div>
														<input type="hidden" value="<?php echo e(ucfirst($emp->type)); ?>" name="go"><?php if($l==2): ?>
														<div class="col-md-1"> <a class="btn btn-primary" onclick="education_field_note();"> Add</a>
														</div><?php else: ?>
														<div class="col-md-1"> <a href="#myModalnote_<?php echo e($notes->id); ?>" id="add_row_note" role="button" class="btn btn-danger remove_note" title="Remove field" data-toggle="modal"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></a>
														</div><?php endif; ?></div><?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> <?php else: ?>
													<input name="noteid[]" value="" type="hidden" placeholder="" id="noteid" class="textonly form-control">
													<div class="form-group">
														<label class="control-label col-md-3">Note :</label>
														<div class="col-md-6">
															<input name="adminnotes[]" value="" type="text" placeholder="Create Note" id="adminnotes" class="textonly form-control">
														</div>
														<div class="col-md-1"> <a class="btn btn-primary" onclick="education_field_note();"> Add</a>
														</div><?php if($l==2): ?> <?php else: ?> <?php endif; ?></div><?php endif; ?></div>
												<div id="input_fields_wrap_notes"></div>
											</div>
											<br>
											<div class="clear clearfix"></div>
											<div class="col-md-12" style="margin-bottom:10px;">
    											<ul class="nav nav-tabs" role="tablist">
                                                    <li role="presentation" class="active"><a href="#temp_note" aria-controls="profile" role="tab" data-toggle="tab"> <span> Temporary Note </span></a></li>
                                                    <li role="presentation"><a href="#per_note" aria-controls="messages" role="tab" data-toggle="tab"><span> Permenant Note </span></a></li>
                                                </ul>
                                            </div>
                                            <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane active" id="temp_note">
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
        												<div class="Branch" style="text-align:left; padding-left:15px;">
        													<h1 class="text-center">Temporary Note</h1>
        												</div>
        											</div>
        											<br>
        							
        											<div class="col-md-12 col-sm-12 col-xs-12 text-right">
        						                        <div class="clear"></div>
        						                         <table class="table table-hover table-bordered dataTable no-footer">
                        						           <thead>
                        						               <tr>
                        						                    <th>No.</th>
                                									<th> Date</br> Day</br> Time</th>
                                									<th>Related To</th>
                                									<th>Description</th>
                                									<th>Notes</th>
                                									<th>Action</th>
                        						               </tr>
                        						           </thead>
                        						           <?php 
                        						           	$cnt=count($notesdata);
                        						           	if($cnt>0)
                        						           	{
                        						           	    $cnt=0;
                        						           	?>
                        						           <?php $__currentLoopData = $notesdata; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $conversation): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        								    <tr>
                        								        <td style="text-align:center;"><?php echo e($loop->index+1); ?> </td>
                        								        <td style="text-align:center;"><?php echo e($conversation->creattiondate); ?><br> <?php echo e($conversation->noteday); ?> <br> <?php echo e($conversation->notetime); ?></td>
                        								        <td style="text-align:center;"><?php echo e($conversation->notesrelated); ?> </td>
                        								        <td><?php echo e($conversation->notesrelatedcat); ?> </td>
                                                                <td><?php echo e($conversation->notes); ?> </td>
                                                                <td style="text-align:center;">
        									                        
        				                                            <a class="btn-action btn-view-edit notesID" data-id="<?php echo e($conversation->id); ?>"><i class="fa fa-edit"></i></a>
        									                   </td>
                        						            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        						            </tr>
                        						            <?php
                        						           	}
                        						           	else
                        						           	{
                        						           ?>
                        						            <tr><td style="text-align:center;" colspan="7">No records found</td></tr>
                        						            <?php
                        						           	}
                        						            ?>
                        						            </tbody>
                        						           
                        						       </table>
        						                    </div>
                                                </div>
                                                <div role="tabpanel" class="tab-pane" id="per_note">
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
        												<div class="Branch" style="text-align:left; padding-left:15px;">
        													<h1 class="text-center">Permenant Note</h1>
        												</div>
        											</div>
        											<br>
        							
        											<div class="col-md-12 col-sm-12 col-xs-12 text-right">
        						                        <div class="clear"></div>
        						                         <table class="table table-hover table-bordered dataTable no-footer">
                        						           <thead>
                        						               <tr>
                        						                    <th>No.</th>
                                									<th> Date</br> Day</br> Time</th>
                                									<th>Related To</th>
                                									<th>Description</th>
                                									<th>Notes</th>
                                									<th>Action</th>
                        						               </tr>
                        						           </thead>
                        						           <?php 
                        						           	$cnt=count($notesdata);
                        						           	if($cnt>0)
                        						           	{
                        						           	    $cnt=0;
                        						           	?>
                        						           <?php $__currentLoopData = $notesdata; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $conversation): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        								    <tr>
                        								        <td style="text-align:center;"><?php echo e($loop->index+1); ?> </td>
                        								        <td style="text-align:center;"><?php echo e($conversation->creattiondate); ?><br> <?php echo e($conversation->noteday); ?> <br> <?php echo e($conversation->notetime); ?></td>
                        								        <td style="text-align:center;"><?php echo e($conversation->notesrelated); ?> </td>
                        								        <td><?php echo e($conversation->notesrelatedcat); ?> </td>
                                                                <td><?php echo e($conversation->notes); ?> </td>
                                                                <td style="text-align:center;">
        									                        
        				                                            <a class="btn-action btn-view-edit notesID" data-id="<?php echo e($conversation->id); ?>"><i class="fa fa-edit"></i></a>
        									                   </td>
                        						            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        						            </tr>
                        						            <?php
                        						           	}
                        						           	else
                        						           	{
                        						           ?>
                        						            <tr><td style="text-align:center;" colspan="7">No records found</td></tr>
                        						            <?php
                        						           	}
                        						            ?>
                        						            </tbody>
                        						           
                        						       </table>
        						                    </div>
                                                </div>
                                            </div>
                                            
						                    
                                                </div>
                                            </div>
										    
						                    </div>
										</div>
										<div class="tab-pane fade in <?php echo e($active); ?>" id="tab1primary">
											<div class="col-md-12">
												<div class="Branch">
													<h1>General Information</h1>
												</div>
												<div class="form-group <?php echo e($errors->has('type') ? ' has-error' : ''); ?>">
													<label class="control-label col-md-3">Type :</label>
													<div class="col-md-3">
														<div class="row">
															<div class="col-lg-9 col-md-12 col-sm-12 col-xs-12" style="padding-left: 8px;">
																<div class="dropdown">
																	<select name="types" id="type" class="form-control">
																	    <option value="">Select</option>
																	    <option value="user" <?php if($emp->type=='user'): ?> selected <?php endif; ?>>User</option>
																	    <option value="employee" <?php if($emp->type=='employee'): ?> selected <?php endif; ?>>Employee</option>
																	</select>
																</div>
															</div>
														</div>
													</div>
													<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 fsc-element-margin">
														<label class="fsc-form-label control-label pull-right left_991">Status :</label>
													</div>
													<div class="col-lg-2 col-md-3 col-sm-12 col-xs-12 fsc-element-margin" style="padding-right: 8px;">
														<select name="check" <?php if($emp->check=='1'): ?> class="form-control fsc-input Green" <?php endif; ?> <?php if($emp->check=='0'): ?> class="form-control fsc-input Blue" <?php endif; ?> id="check" >
															<option value="0" class="Red" <?php if($emp->check=='0'): ?> selected <?php endif; ?>>In-Active</option>
															<option class="Green" value="1" <?php if($emp->check=='1'): ?> selected <?php endif; ?>>Active</option>
														</select>
													</div>
													
													
												</div>
												
												<div class="form-group <?php echo e($errors->has('employee_id') ? ' has-error' : ''); ?>">
													<label class="control-label col-md-3"><?php echo e(ucfirst($emp->type)); ?> ID :</label>
													<div class="col-md-3">
														<div class="row">
															<div class="col-lg-9 col-md-12 col-sm-12 col-xs-12" style="padding-left: 8px;">
																<input type="text" class="form-control fsc-input" name="employee_id" placeholder="GUA-999-9999" id="employee_id" value="<?php echo e($emp->employee_id); ?>"><?php if($errors->has('employee_id')): ?>	<span class="help-block">
														<strong><?php echo e($errors->first('employee_id')); ?></strong>
														</span>
																<?php endif; ?> <?php if($emp->check=='0'): ?>
																<input name="password1" value="<?php echo mt_rand();?>" class="form-control fsc-input" id="password1" readonly="" type="hidden"><?php endif; ?>
														</div>
													</div>
													</div>
													
													<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 fsc-element-margin">
														<label class="fsc-form-label control-label pull-right left_991">Nickname :</label>
													</div>
														<div class="col-lg-2 col-md-3 col-sm-12 col-xs-12 fsc-element-margin" style="padding-right: 8px;">
															<select name="team" class="form-control fsc-input">
															    <option value="">Select</option>
															    <?php $__currentLoopData = $team; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $teams): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
															    <option value="<?php echo e($teams->id); ?>"  <?php if($emp->team==$teams->id): ?> selected <?php endif; ?>><?php echo e($teams->team); ?></option>
															     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>	
															</select>
														</div>
																
														<div class="col-lg-2 col-md-1 col-sm-3 col-xs-3 fsc-element-margin" style="padding-top:10px;">
														    <a href="#" data-toggle="modal" data-target="#basicExampleModal" class="redius"><i class="fa fa-plus"></i></a>
														    &nbsp;&nbsp;&nbsp; <a href="#" data-toggle="modal" data-target="#basicExampleModal3" class="redius">
														        <i class="fa fa-minus"></i></a> 
												        </div>
											        </div>	
												</div>
												<div class="form-group <?php echo e($errors->has('firstName') ? ' has-error' : ''); ?><?php echo e($errors->has('middleName') ? ' has-error' : ''); ?><?php echo e($errors->has('lastName') ? ' has-error' : ''); ?>">
													<label class="control-label col-md-3">Name :</label>
													<div class="col-lg-7 col-md-8">
														<div class="row">
															<div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 fsc-form-col fsc-element-margin">
																<select class="form-control fsc-input" id="nametype" name="nametype">
																	<option value="mr" <?php if($emp->nametype=='mr'): ?> selected <?php endif; ?>>Mr.</option>
																	<option value="mrs" <?php if($emp->nametype=='mrs'): ?> selected <?php endif; ?>>Mrs.</option>
																	<option value="miss" <?php if($emp->nametype=='miss'): ?> selected <?php endif; ?>>Miss.</option>
																</select>
															</div>
															<div class="col-lg-4 col-md-4 col-sm-8 col-xs-8 fsc-element-margin">
																<input type="text" class="form-control fsc-input " id="firstName" name="firstName" placeholder="First" value="<?php echo e($emp->firstName); ?>"><?php if($errors->has('firstName')): ?>	<span class="help-block">
														<strong><?php echo e($errors->first('firstName')); ?></strong>
														</span>
																<?php endif; ?></div>
															<div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 fsc-element-margin">
																<div class="">
																	<input type="text" maxlength="1" class="form-control fsc-input " id="middleName" name="middleName" placeholder="M" value="<?php echo e($emp->middleName); ?>">
																</div><?php if($errors->has('middleName')): ?>	<span class="help-block">
														<strong><?php echo e($errors->first('middleName')); ?></strong>
														</span>
																<?php endif; ?>
																</div>
															<div class="col-lg-4 col-md-4 col-sm-8 col-xs-8 fsc-element-margin">
																<input type="text" class="form-control fsc-input" id="lastName" name="lastName" value="<?php echo e($emp->lastName); ?>" placeholder="Last">
																<input type="hidden" class="form-control fsc-input" id="type" name="type" value="<?php echo e($emp->type); ?>" placeholder="Last"><?php if($errors->has('lastName')): ?>	<span class="help-block">
														<strong><?php echo e($errors->first('lastName')); ?></strong>
														</span>
																<?php endif; ?>
																</div>
														</div>
													</div>
												</div>
												<div class="form-group <?php echo e($errors->has('address1') ? ' has-error' : ''); ?>">
													<label class="control-label col-md-3">Address 1 :</label>
													<div class="col-lg-7 col-md-8">
														<input type="text" placeholder="Address 1" class="form-control fsc-input" name="address1" id="address1" value="<?php echo e($emp->address1); ?>"><?php if($errors->has('address1')): ?>	<span class="help-block">
												<strong><?php echo e($errors->first('address1')); ?></strong>
												</span>
														<?php endif; ?>
														</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Address 2 :</label>
													<div class="col-lg-7 col-md-8">
														<input type="text" placeholder="Address 2" class="form-control fsc-input" name="address2" id="address2" value="<?php echo e($emp->address2); ?>">
													</div>
												</div>
												<div class="form-group <?php echo e($errors->has('countryId') ? ' has-error' : ''); ?>">
													<label class="control-label col-md-3">Country :</label>
													<div class="col-lg-7 col-md-8">
														<div class="row">
															<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
																<div class="dropdown">
																	<select name="countryId" id="countries_states1" class="form-control fsc-input bfh-countries" data-country="<?php echo e($emp->countryId); ?>"></select>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="form-group <?php echo e($errors->has('city') ? ' has-error' : ''); ?><?php echo e($errors->has('stateId') ? ' has-error' : ''); ?><?php echo e($errors->has('zip') ? ' has-error' : ''); ?> ">
													<label class="control-label col-md-3">City / State / Zip :</label>
													<div class="col-lg-7 col-md-8">
														<div class="row">
															<div class="col-lg-4 col-md-6 fsc-element-margin">
																<input type="text" class="form-control fsc-input textonly" id="city" name="city" placeholder="City" value="<?php echo e($emp->city); ?>"><?php if($errors->has('city')): ?>	<span class="help-block">
														<strong><?php echo e($errors->first('city')); ?></strong>
														</span>
																<?php endif; ?></div>
															<div class="col-lg-4 col-md-3 col-xs-6">
																<div class="dropdown" style="margin-top: 1%;">
																	<select name="stateId" id="stateId" class="form-control fsc-input  bfh-states" data-country="countries_states1" data-state="<?php echo e($emp->stateId); ?>"></select><?php if($errors->has('stateId')): ?>	<span class="help-block">
															<strong><?php echo e($errors->first('stateId')); ?></strong>
															</span>
																	<?php endif; ?></div>
															</div>
															<div class="col-lg-4 col-md-3 col-xs-6 fsc-element-margin">
																<input type="text" class="form-control fsc-input zip" id="zip" name="zip" maxlength="6" value="<?php echo e($emp->zip); ?>" placeholder="Zip"><?php if($errors->has('zip')): ?>	<span class="help-block">
														<strong><?php echo e($errors->first('zip')); ?></strong>
														</span>
																<?php endif; ?></div>
														</div>
													</div>
												</div>
												<div class="form-group <?php echo e($errors->has('telephoneNo1') ? ' has-error' : ''); ?> <?php echo e($errors->has('telephoneNo1Type') ? ' has-error' : ''); ?> ">
													<label class="control-label col-md-3">Personal Telephone:</label>
													<div class="col-lg-7 col-md-8">
														<div class="row">
															<div class="col-lg-4 col-md-6 fsc-element-margin">
																<input type="text" class="form-control fsc-input bfh-phone" data-country="countries_states1" id="telephoneNo1" name="telephoneNo1" placeholder="(999) 999-9999" value="<?php echo e($emp->telephoneNo1); ?>"><?php if($errors->has('telephoneNo1')): ?>	<span class="help-block">
														<strong><?php echo e($errors->first('telephoneNo1')); ?></strong>
														</span>
																<?php endif; ?></div>
															<div class="col-lg-4 col-md-3 col-xs-6">
																<div class="dropdown" style="margin-top: 1%;">
																	<select name="telephoneNo1Type" id="telephoneNo1Type" class="form-control fsc-input">
																		<option value='Home' <?php if($emp->telephoneNo1Type=='Home'): ?> selected <?php endif; ?>>Home</option>
																		<option value='Mobile' <?php if($emp->telephoneNo1Type=='Mobile'): ?> selected <?php endif; ?>>Mobile</option>
																		<option value='Office' <?php if($emp->telephoneNo1Type=='Office'): ?> selected <?php endif; ?>>Office</option>
																		<option value='Other' <?php if($emp->telephoneNo1Type=='Other'): ?> selected <?php endif; ?>>Other</option>
																		<option value='Work' <?php if($emp->telephoneNo1Type=='Work'): ?> selected <?php endif; ?>>Work</option>
																	</select><?php if($errors->has('telephoneNo1Type')): ?>	<span class="help-block">
															<strong><?php echo e($errors->first('telephoneNo1Type')); ?></strong>
															</span>
																	<?php endif; ?></div>
															</div>
															<div class="col-lg-4 col-md-3 col-xs-6">
																<input type="text" class="form-control fsc-input zip"  maxlength="5" <?php if($emp->telephoneNo1Type=='Office'): ?> <?php else: ?> readonly <?php endif; ?> id="ext1" name="ext1" value="<?php echo e($emp->ext1); ?>" placeholder="Ext"></div>
														</div>
													</div>
												</div>
												<div class="form-group <?php echo e($errors->has('telephoneNo2') ? ' has-error' : ''); ?> <?php echo e($errors->has('telephoneNo2Type') ? ' has-error' : ''); ?> ">
													<label class="control-label col-md-3">FSC Telephone:</label>
													<div class="col-lg-7 col-md-8">
														<div class="row">
															<div class="col-lg-4 col-md-6 fsc-element-margin">
																<input type="text" class="form-control fsc-input bfh-phone" data-country="countries_states1" id="telephoneNo2" name="telephoneNo2" placeholder="(999) 999-9999" value="<?php echo e($emp->telephoneNo2); ?>"><?php if($errors->has('telephoneNo2')): ?>	<span class="help-block">
														<strong><?php echo e($errors->first('telephoneNo2')); ?></strong>
														</span>
																<?php endif; ?></div>
															<div class="col-lg-4 col-md-3 col-xs-6">
																<div class="dropdown" style="margin-top: 1%;">
																	<select name="telephoneNo2Type" id="telephoneNo2Type" class="form-control fsc-input">
																		<option value='Home' <?php if($emp->telephoneNo2Type=='Home'): ?> selected <?php endif; ?>>Home</option>
																		<option value='Mobile' <?php if($emp->telephoneNo2Type=='Mobile'): ?> selected <?php endif; ?>>Mobile</option>
																		<option value='Office' <?php if($emp->telephoneNo2Type=='Office'): ?> selected <?php endif; ?>>Office</option>
																		<option value='Other' <?php if($emp->telephoneNo2Type=='Other'): ?> selected <?php endif; ?>>Other</option>
																		<option value='Work' <?php if($emp->telephoneNo2Type=='Work'): ?> selected <?php endif; ?>>Work</option>
																	</select><?php if($errors->has('telephoneNo2Type')): ?>	<span class="help-block">
															<strong><?php echo e($errors->first('telephoneNo2Type')); ?></strong>
															</span>
																	<?php endif; ?></div>
															</div>
															<div class="col-lg-4 col-md-3 col-xs-6">
																<input type="text" class="form-control fsc-input zip"  maxlength="5" <?php if($emp->telephoneNo2Type=='Office'): ?> <?php else: ?> readonly <?php endif; ?> id="ext2" name="ext2" value="<?php echo e($emp->ext2); ?>" placeholder="Ext"></div>
														</div>
													</div>
												</div>
												<div class="form-group <?php echo e($errors->has('fax') ? ' has-error' : ''); ?>">
													<label class="control-label col-md-3">Fax :</label>
													<div class="col-lg-7 col-md-8">
														<div class="row">
															<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
																<div class="dropdown">
																	<input type="tel" class="form-control fsc-input bfh-phone" data-country="countries_states1" id="fax" name="fax" value="<?php echo e($emp->fax); ?>" placeholder="(999) 999-9999">
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Personal Email :</label>
													<div class="col-md-7">
														<input type="text" class="form-control fsc-input" id="email" readonly name="email" value="<?php echo e($emp->email); ?>" placeholder="Email">
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">FSC Email :</label>
													<div class="col-md-7">
														<input type="text" class="form-control fsc-input" id="fscemail"  name="fscemail" value="<?php echo e($emp->fscemail); ?>" placeholder="FSC Email">
													</div>
												</div>
												
												<div class="form-group <?php echo e($errors->has('photo') ? ' has-error' : ''); ?>">
													<label class="control-label col-md-3">Picture :</label>
													<div class="col-md-6">
														<div class="row">
															<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
															    <?php if(empty($emp->photo)): ?>
															    <label class="file-upload btn btn-primary">
                              Browse for file ... <input name="photo" style="opecity:0" placeholder="Upload Service Image" id="photo" type="file">
                              </label>
                              <?php else: ?>
                              
																<div style="width:100px;height:100px; border: 1px solid whitesmoke ;text-align: center;position: relative" id="image">
																	<img width="100%" height="100%" id="preview_image_5" src="<?php echo e(asset('public/employeeimage/')); ?>/<?php echo e($emp->photo); ?>" /> <i id="loading5" class="fa fa-spinner fa-spin fa-3x fa-fw" style="position: absolute;left: 10%;top:10%;display: none"></i>
																</div>
															
																<span style="position: absolute;top: 33px;left: 29px;background: #1d0000b3;padding: 5px;">
<a href="javascript:photo_upload()" style="text-decoration: none;"><i class="glyphicon glyphicon-edit"></i> Change</a>
																	<!--<a href="javascript:additional_attach_remo()" style="color: red;text-decoration: none;"><i class="glyphicon glyphicon-trash"></i> Remove</a>-->
																</span>
																<span><a class="btn btn-danger removeimage" href="<?php echo e(route('employee.removeimage',$emp->id)); ?>">Remove</a></span>
																
																	<?php endif; ?>
																<input type="file" id="photo" style="display: none" />
																<input type="hidden" id="photo_name" />
																<input type="hidden" id="user_id11" value="<?php echo e($emp->id); ?>" />
															</div>
														</div>
													</div>
												</div>
											</div>
										
										<div class="tab-pane fade" id="tab2primary">
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
											<div class="row">
												<div class="Branch">
													<h1>Hiring Information</h1>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Hire Date :</label>
													<div class="col-lg-6 col-md-8">
														<div class="row">
															<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 fsc-form-col">
																<div class="dropdown" style="margin-top: 1%;">
																	<input name="hiremonth" type="text" value="<?php echo e($emp->hiremonth); ?>" id="hiremonth" class="form-control tttt date1">
																</div>
															</div>
															<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 fsc-form-col">
																<label class="control-label pull-right left_991">Termination Date :</label>
															</div>
															<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 fsc-form-col">
																<input name="termimonth" type="text" value="<?php echo e($emp->termimonth); ?>" id="termimonth" class="form-control tttt date1">
															</div>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Note :</label>
													<div class="col-lg-6 col-md-8">
														<div class="row">
															<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
																<textarea name="tnote1" id="tnote1" class="form-control tttt fsc-input"><?php echo e($emp->tnote1); ?></textarea>
															</div>
														</div>
													</div>
												</div>
												<div class="form-group ">
													<label class="control-label col-md-3">Re-Hire Date :</label>
													<div class="col-lg-6 col-md-8">
														<div class="row">
															<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 fsc-form-col">
																<div class="dropdown" style="margin-top: 1%;">
																	<input name="rehiremonth" type="text" value="<?php echo e($emp->rehiremonth); ?>" id="rehiremonth" class="tttt form-control date1">
																</div>
															</div>
															<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 fsc-form-col">
																<div class="dropdown" style="margin-top: 1%;">
																	<label class="control-label pull-right left_991">Termination Date :</label>
																</div>
															</div>
															<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 fsc-form-col">
																<div class="dropdown" style="margin-top: 1%;">
																	<input name="rehireyear" type="text" value="<?php echo e($emp->rehireyear); ?>" id="rehireyear" class="tttt form-control date1">
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="form-group ">
													<label class="control-label col-md-3">Note :</label>
													<div class="col-lg-6 col-md-8">
														<div class="row">
															<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																<textarea name="tnote" id="tnote" class="form-control tttt fsc-input"><?php echo e($emp->tnote); ?></textarea>
															</div>
														</div>
													</div>
												</div>
												<div class="Branch">
													<h1>Branch / Department Information</h1>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3 martop7">Branch City:</label>
													<div class="col-lg-6 col-md-8">
														<div class="row">
															<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																<div class="dropdown" style="margin-top: 1%;">
																	<select name="branch_city" id="branch_city" class="form-control tttt  fsc-input category">
																		<option value="">---Select---</option><?php $__currentLoopData = $branch; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pos): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																		<option value="<?php echo e($pos->city); ?>" <?php if($emp->branch_city ==$pos->city): ?> selected <?php endif; ?>><?php echo e($pos->city); ?></option><?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></select>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Branch Name:</label>
													<div class="col-lg-6 col-md-8">
														<div class="row">
															<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																<input type="text" readonly class="form-control tttt fsc-input" id="branch_name" name="branch_name" placeholder="" value="<?php echo e($emp->branch_name); ?>">
															</div>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Position :</label>
													<div class="col-lg-6 col-md-8">
														<div class="row">
															<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																<div class="dropdown" style="margin-top: 1%;">
																	<select name="position" id="position" class="form-control tttt fsc-input">
																		<option value="">---Select Position---</option><?php $__currentLoopData = $position; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pos): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																		<option value="<?php echo e($pos->id); ?>" <?php if($emp->position ==$pos->id): ?> selected <?php endif; ?>><?php echo e($pos->position); ?></option><?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></select>
																</div>
															</div>
														</div>
													</div>
													<div class="col-md-2">
														
															<input type="checkbox" class="tttt1" id="super" name="super" value="1" <?php if($emp->super =='1'): ?> checked <?php endif; ?>><label for="super"> Supervisor</label>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Note :</label>
													<div class="col-lg-6 col-md-8">
														<div class="row">
															<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																<textarea name="note" id="note" class="form-control tttt fsc-input"><?php echo e($emp->note); ?></textarea>
															</div>
														</div>
													</div>
												</div>
												
											</div>
											</div>
										</div>
										<div class="tab-pane fade " id="tab3primary">
										    <?php if($emp->type !='user')
										    {?>
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding:0px;">
												<div class="Branch" style="display: grid;padding: 7px 10px !important;">
													<h1>Pay Information</h1>
												    <div class="review" style="text-align:right;position: absolute;right: 10px;">
    												    <a class="btn btn-warning" id="add_pay" style="margin-top: -5px;">Add Pay</a>
    												</div>
												</div>
												<div class="clearfix"></div>
												
												
												<!--<div class="form-group ">
													<label class="control-label col-md-3">Pay Method :</label>
													<div class="col-md-6">
														<div class="row">
															<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
																<div class="dropdown" style="margin-top: 1%;">
																	<select name="pay_method" id="pay_method" class="form-control tttt">
																		<option value="Salary" <?php if($emp->pay_method=='Salary'): ?> selected <?php endif; ?>>Salary</option>
																		<option value="Hourly" <?php if($emp->pay_method=='Hourly'): ?> selected <?php endif; ?>>Hourly</option>
																	</select>
																</div>
															</div>
															<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
																<label class="fsc-form-label">Pay Duration :</label>
															</div>
															<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
																<select name="pay_frequency" id="pay_frequency" class="form-control tttt">
																	<option value="Weekly" <?php if($emp->pay_frequency=='Weekly'): ?> selected <?php endif; ?>>Weekly</option>
																	<option value="Bi-Weekly" <?php if($emp->pay_frequency=='Bi-Weekly'): ?> selected <?php endif; ?>>Bi-Weekly</option>
																	<option value="Bi-Monthly" <?php if($emp->pay_frequency=='Semi-Monthly'): ?> selected <?php endif; ?>>Semi-Monthly</option>
																	<option value="Monthly" <?php if($emp->pay_frequency=='Monthly'): ?> selected <?php endif; ?>>Monthly</option>
																</select>
															</div>
														</div>
													</div>
												</div>
												<div class="fieldGroup ">	<a href="javascript:void(0)" class="addMore pull-right"><i class="fa fa-plus" aria-hidden="true"></i> Add Pay</a>
											
													<?php $k=1 ; $in=count($info );?><?php if($in!=null): ?> <?php $__currentLoopData = $info; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $in): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
													<div id="field<?php echo e($in->id); ?>">
														<div class="form-group">
															<label class="control-label col-md-3">Pay Rate :</label>
															<div class="col-md-6">
																<div class="row">
																	<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
																		<div class="dropdown" style="margin-top: 1%;">
																			<input name="pay_scale[]" value="<?php echo e($in->pay_scale); ?>" type="text" maxlength="10" id="pay_scale" class="form-control tttt pay_scale" />
																			<input name="employee[]" value="<?php echo e($in->id); ?>" type="hidden" id="employee" class="form-control" />
																		</div>
																	</div>
																	<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
																		<label class="fsc-form-label">Effective Date :</label>
																	</div>
																	<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
																		<input name="effective_date[]" value="<?php echo e($in->effective_date); ?>" type="text" id="effective_date<?php echo e($in->id); ?>" class="form-control date1 tttt" />
																	</div>
																</div>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-3">Note :</label>
															<div class="col-md-6">
																<textarea id="fields" name="fields[]" class="form-control tttt fsc-input"><?php echo e($in->fields); ?></textarea>
															</div>
														</div>
														<?php $k; $k++;?><?php if($k == 2): ?> <?php else: ?>
														<div class="form-group">
															<div class="col-md-12"><a href="#myModal1<?php echo e($in->id); ?>" role="button" class="btn btn-danger remove11 pull-right" data-toggle="modal"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
															</div>
														</div><?php endif; ?></div><?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> <?php else: ?>
													<div id="Pay Method">
														<div class="form-group">
															<label class="control-label col-md-3">Pay Rate :</label>
															<div class="col-md-6">
																<div class="row">
																	<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
																		<div class="dropdown" style="margin-top: 1%;">
																			<input name="pay_scale[]" value="" type="text" id="pay_scale" maxlength="10" onkeypress="return isNumberKey(event)" class="form-control tttt pay_scale" />
																			<input name="employee[]" value="" type="hidden" id="employee" class="form-control" />
																		</div>
																	</div>
																	<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
																		<label class="fsc-form-label">Effective Date :</label>
																	</div>
																	<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
																		<input name="effective_date[]" value="" type="text" id="effective_date" class="form-control date1 tttt" />
																	</div>
																</div>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-3">Note :</label>
															<div class="col-md-6">
																<textarea id="fields" name="fields[]" class="form-control fsc-input tttt"></textarea>
															</div>
														</div>
													</div><?php endif; ?></div>!-->
													
												<div class="">
												    <div class="table-responsive">
												        <table class="table table-bordered">
												            <thead>
												                <tr>
												                    <th style="width:10%;">Pay Method</th>
												                    <th style="width:12%;">Pay Duration</th>
												                    <th style="width:11%;">Pay Rate</th>
												                    <th style="width:12%;">Effective Date</th>
												                    <th style="width:40%;">Note</th>
												                    <th style="width:5%;">Action</th>
												                </tr>
												            </thead>
												            <tbody class="addpayinfo">
												                <?php $countinfo=count($info);
												                if($countinfo =='0')
												                {?>
												                <tr>
												                    <td><select class="form-control" name="pay_method[]"><option value="">Select</option><option value="Salary">Salary</option><option value="Hourly">Hourly</option> </select></td>
												                    <td><select class="form-control" name="pay_frequency[]"><option value="">Select</option> <option value="Weekly">Weekly</option><option value="Bi-Weekly">Bi-Weekly</option><option value="Semi-Monthly">Semi-Monthly</option><option value="Monthly">Monthly</option></select></td>
												                    <td><input type="text" name="pay_scale[]" class="form-control pay_scale_1 text-center"></td>
												                    <td><input type="text" name="effective_date[]" class="form-control paydate"></td>
												                    <td><textarea cols="40" rows="1" name="fields[]" class="form-control"></textarea></td>
                                                                    <td style="text-align:center;">

                                                                    </td>
												                </tr>
												                <?php 
												                }
												                else
												                {
												                    $cnts=1;
												                foreach($info as $iinfo)
												                {
												                    $cnts++;
												                ?>
												                    <tr>
												                    <td>
												                        <select class="form-control" name="pay_method[]">
												                        <option value="">Select</option>
												                        <option value="Salary" <?php if($iinfo->pay_method =='Salary'): ?> selected <?php endif; ?>>Salary</option>
												                        <option value="Hourly" <?php if($iinfo->pay_method =='Hourly'): ?> selected <?php endif; ?>>Hourly</option>
												                        </select></td>
												                    <td><select class="form-control" name="pay_frequency[]">
												                        <option value="">Select</option>
												                        <option value="Weekly" <?php if($iinfo->pay_frequency =='Weekly'): ?> selected <?php endif; ?>>Weekly</option>
												                        <option value="Bi-Weekly" <?php if($iinfo->pay_frequency =='Bi-Weekly'): ?> selected <?php endif; ?>>Bi-Weekly</option>
												                        <option value="Semi-Monthly" <?php if($iinfo->pay_frequency =='Semi-Monthly'): ?> selected <?php endif; ?>>Semi-Monthly</option>
												                        <option value="Monthly" <?php if($iinfo->pay_frequency =='Monthly'): ?> selected <?php endif; ?>>Monthly</option>
												                        </select></td>
												                    <td><input type="text" name="pay_scale[]" value="<?php echo e($iinfo->pay_scale); ?>"  class="text-center form-control pay_scale_<?php echo e($cnts); ?>"></td>
												                    <td><input type="text" name="effective_date[]" value="<?php echo e(date('m/d/Y',strtotime($iinfo->effective_date))); ?>" class="form-control paydate"></td>
												                    <td><textarea cols="40" rows="1" name="fields[]"  class="form-control"><?php echo e($iinfo->fields); ?></textarea></td>
                                                                    <td style="text-align:center;">
                                                                        <a href="#myModalpay_<?php echo e($iinfo->id); ?>" id="add_row_pay" role="button" class="btn btn-danger remove_pay" title="Remove field" data-toggle="modal"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></a>
                                                                    </td>
												                </tr>
												                <script>
												                	$(function() {
	
												                	$('.pay_scale_<?php echo $cnts;?>').blur(function() {
												                	    
                                                            			$(this).val('$' + $(this).getNum());
                                                            			});
                                                            		});
		
												                </script>
												                <?php
												                }
												                    
												                }
												                ?>
												                
												            </tbody>
												        </table>
												    </div>
											    </div>
											</div>
											<?php
										    }
										    ?>
										    <div class="Branch" style="display: grid;padding: 7px 10px !important;">
												<h1>Review Information</h1>
											    <div class="review" style="text-align:right;position: absolute;right: 25px;">
												    <a class="btn btn-warning" id="add_review" style="margin-top: -5px;">Add Review</a>
												</div>
											</div>
											<div class="clearfix"></div>
										    
											<!--	<div class="review ">
													<div class="form-group">
														<div class="col-md-12">	<a href="javascript:void(0)" class="ad pull-right btn btn-primary tttt" style="margin:10px 0;position: absolute;right: 113px;top: 0px;"><i class="fa fa-plus" aria-hidden="true"></i> Add Review</a> 
														</div>
													</div>
													<?php $i=1; $j=1; $count=count($review1);?><?php if($count!=null): ?> <?php $__currentLoopData = $review1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $re): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
													<script>
														$(document).ready(function(){
														        $('#first_rev_day<?php echo e($re->id); ?>').on('keyup',function(){
														           var hiremonth = $('#hiremonth').val();
														           var reset1 = parseInt($('#first_rev_day<?php echo e($re->id); ?>').val()); //alert(reset);
														            var tt = hiremonth.split("-").reverse().join(" ");
														         var t = new Date(tt); 
														        // alert(reset1);
																t.setDate(t.getDate() + reset1);
																var month = "0"+(t.getMonth()+1);
																var date = "0"+t.getDate();
																const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "June","July", "Aug", "Sept", "Oct", "Nov", "Dec"];
																const d = new Date();
																month = month.slice(-2);
																date = date.slice(-2);
																var date = monthNames[t.getMonth()]+"-"+date+"-"+t.getFullYear();
																if($(this).val() == '') { // check if value changed
														         $('#reviewmonth<?php echo e($re->id); ?>').val('');
																}
																else
																{
														           $('#reviewmonth<?php echo e($re->id); ?>').val(date);
																}
														        });
														    });
													</script>
													<div class="form-group">
														<label class="control-label col-md-3">
															<?php echo '('.$i.') '; $i++;?>Review Days :</label>
														<div class="col-md-6">
															<div class="row">
																<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
																	<div class="dropdown" style="margin-top: 1%;">
																		<input type="text" class="form-control tttt fsc-input first_rev_day" maxlength="3" onkeypress="return isNumberKey(event)" name="first_rev_day[]" id="first_rev_day<?php echo e($re->id); ?>" value="<?php echo e($re->first_rev_day); ?>">
																	</div>
																</div>
																<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
																	<div class="dropdown" style="margin-top: 1%;">
																		<label class="control-label">
																			<?php echo '('.$j.') '; $j++;?>Review Date :</label>
																	</div>
																</div>
																<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
																	<div class="dropdown" style="margin-top: 1%;">
																		<input name="reviewmonth[]" type="text" value="<?php echo e($re->reviewmonth); ?>" id="reviewmonth<?php echo e($re->id); ?>" class="form-control tttt reviewmonth">
																		<input name="ree[]" type="hidden" value="<?php echo e($re->id); ?>" id="ree" class="form-control">
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3">Comments :</label>
														<div class="col-md-6">
															<div class="row">
																<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																	<textarea name="hiring_comments[]" id="hiring_comments" class="form-control tttt fsc-input"><?php echo e($re->hiring_comments); ?></textarea>
																</div>
															</div>
														</div>
													</div><?php if($i == 2): ?> <?php else: ?>
													<a href="#myModal<?php echo e($re->id); ?>" role="button" class="btn btn-danger remove pull-right" title="Add field" data-toggle="modal"> <i class="fa fa-trash-o" aria-hidden="true"></i> 
													</a><?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> <?php else: ?>
													<div class="form-group">
														<label class="control-label col-md-3">Review Days :</label>
														<div class="col-md-6">
															<div class="row">
																<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
																	<div class="dropdown" style="margin-top: 1%;">
																		<input type="text" class="form-control tttt fsc-input first_rev_day" onkeypress="return isNumberKey(event)" maxlength="3" name="first_rev_day[]" id="first_rev_day">
																	</div>
																</div>
																<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
																	<div class="dropdown" style="margin-top: 1%;">
																		<label class="control-label">Review Date :</label>
																	</div>
																</div>
																<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
																	<div class="dropdown" style="margin-top: 1%;">
																		<input name="reviewmonth[]" type="text" id="reviewmonth" class="form-control tttt reviewmonth">
																	</div>
																</div>
															</div>
														</div>
													</div>
													<input name="ree[]" type="hidden" value="" id="ree" class="form-control">
													<div class="form-group">
														<label class="control-label col-md-3">Comments :</label>
														<div class="col-md-6">
															<div class="row">
																<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																	<textarea name="hiring_comments[]" id="hiring_comments" rows="1" class="form-control tttt fsc-input"></textarea>
																</div>
															</div>
														</div>
													</div><?php endif; ?>
													<div class="review-1">
														<div class="review-2">
															<div class="input_fields_wrap_1">
										</div>
														</div>
													</div>
													
												</div>!-->
												<div class="review " style="text-align:right;">
												    <!--<a class="btn btn-primary" data-toggle="modal" data-target="#addReview" style="margin-bottom:10px;">Add Review</a>-->
												    <div class="table-responsive">
												        <table class="table table-bordered">
												            <thead>
												                <tr>
												                    <th style="width:7%;">Review No.</th>
												                    <th style="width:12%;">Review Date</th>
												                    <th style="width:10%;">Next Review</th>
												                    <th style="width:12%;">Next Review Date</th>
												                    <th style="width:11%;">Next Pay Rate</th>
												                    <th style="width:35%;">Comments</th>
												                    <th style="width:5%;">Action</th>
												                </tr>
												            </thead>
												            <tbody class="addreviewinfo">
												                 <?php $countreviewinfo=count($reviewinfo);
												                 
												                if($countreviewinfo =='0')
												                {?>
												               
												                <tr>
												                    <td></td>
												                    <td><input type="text" id="reviewdate_1" name="review_date[]" class="form-control review_date_1"></td>
												                    <td><input type="text" name="review_day[]" class="form-control review_day_1  text-center"></td>
												                    <td><input type="text" readonly name="next_review_date[]" class="form-control next_review_date_1"></td>
												                    <td><input type="text" name="review_rate[]" class="form-control pay_review_scale_1 text-center"></td>
												                    <td><textarea cols="40" name="review_comments[]" rows="1" class="form-control"></textarea></td>
                                                                    <td style="text-align:center;">
                                                                    </td>
												                </tr>
												                <?php
												                }
												                else
												                {
												                         $cnts=1;
												                         $cnts11=0;
												                         
												                foreach($reviewinfo as $rinfo)
												                {
												                    $cnts++;
												                    $cnts11++;
												                    
												               
												                    ?>
												                <tr>
												                    <td class="text-center"><?php echo e($cnts11); ?></td>
												                    <td><input type="text" name="review_date[]" value="<?php echo e(date('m/d/Y',strtotime($rinfo->review_date))); ?>" class="form-control review_date_<?php echo e($cnts); ?>"></td>
												                    <td><input type="text" name="review_day[]" value="<?php echo e($rinfo->review_day); ?>" class="form-control text-center review_day_<?php echo e($cnts); ?>"></td>
												                    <td><input type="text" readonly name="next_review_date[]" value="<?php echo e(date('m/d/Y',strtotime($rinfo->next_review_date))); ?>" class="form-control next_review_date_<?php echo e($cnts); ?>"></td>
												                    <td><input type="text" name="review_rate[]" value="<?php echo e($rinfo->review_rate); ?>" class="form-control text-center pay_review_scale_<?php echo e($cnts); ?>"></td>
												                    <td><textarea cols="40" name="review_comments[]" rows="1" class="form-control"><?php echo e($rinfo->review_comments); ?></textarea></td>
                                                                    <td style="text-align:center;">
                                                                            <a href="#myModalreview_<?php echo e($rinfo->id); ?>" id="add_row_review" role="button" class="btn btn-danger remove_review" title="Remove field" data-toggle="modal"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></a>
                                                               
                                                                    </td>
												                </tr>
												                   <script>
												                	$(function() {
	
												                	$('.pay_review_scale_<?php echo $cnts;?>').blur(function() {
												                	    
                                                            			$(this).val('$' + $(this).getNum());
                                                            			});
                                                            		});
                                                            		
                                                            		
		
												                </script>
												                <script>
												                    (function($, window, document, undefined){
                                                $(".review_day_<?php echo $cnts;?>").blur(function(){
                                                   var date = new Date($(".review_date_<?php echo $cnts;?>").val()),
                                                       days = parseInt($(".review_day_<?php echo $cnts;?>").val(), 10);
                                                    
                                                    if(!isNaN(date.getTime())){
                                                        date.setDate(date.getDate() + days);
                                                        $(".next_review_date_<?php echo $cnts;?>").val(date.toInputFormat());
                                                    } else {
                                                        alert("Invalid Date");  
                                                    }
                                                });
                                                
                                                
                                                //From: http://stackoverflow.com/questions/3066586/get-string-in-yyyymmdd-format-from-js-date-object
                                                Date.prototype.toInputFormat = function() {
                                                   var yyyy = this.getFullYear().toString();
                                                   var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
                                                   var dd  = this.getDate().toString();
                                                //   return yyyy + "-" + (mm[1]?mm:"0"+mm[0]) + "-" + (dd[1]?dd:"0"+dd[0]); // padding
                                                   return (mm[1]?mm:"0"+mm[0]) + "/" + (dd[1]?dd:"0"+dd[0]) + "/" + yyyy; // padding
                                                };
                                            })(jQuery, this, document);
	
		
												                </script>
												                 
												                <?php
												                }
												                }
												                ?>
												            </tbody>
												        </table>
												    </div>
											    </div>
												
										    
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="padding:0px;">
												<div class="Branch" style="display: grid;padding: 7px 10px !important;">
													<h1>Tax Withholding Information</h1>
												    <div class="review" style="text-align:right;position: absolute;right: 10px;">
    												    <a class="btn btn-warning" id="add_tax" style="margin-top: -5px;">Add Tax Withhold</a>
    												</div>
												</div>
												<div class="clearfix"></div>
												<!--<div class="form-group">
													<label class="control-label col-md-3">Filling Status :</label>
													<div class="col-md-6">
														<select name="filling_status" id="filling_status" class="form-control tttt">
															<option value="Single" <?php if($emp->filling_status=='Single'): ?> selected <?php endif; ?>>Single</option>
															<option value="Married File Jointly" <?php if($emp->filling_status=='Married File Jointly'): ?> selected <?php endif; ?>>Married File Jointly</option>
															<option value="Married File Separately" <?php if($emp->filling_status=='Married File Separately'): ?> selected <?php endif; ?>>Married File Separately</option>
															<option value="Head of Household" <?php if($emp->filling_status=='Head of Household'): ?> selected <?php endif; ?>>Head of Household</option>
														</select>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Federal Claim :</label>
													<div class="col-md-8">
														<div class="row">
															<div class="col-lg-2 col-md-12 col-sm-12 col-xs-12">
																<div class="dropdown" style="margin-top: 1%;">
																	<input name="fedral_claim" value="<?php echo e($emp->fedral_claim); ?>" maxlength="3" onkeypress="return isNumberKey(event)" type="text" id="fedral_claim" onkeypress="return isNumberKey(event)" class="form-control tttt" />
																</div>
															</div>
															<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
																<label class="fsc-form-label martop7">Additional Withholding :</label>
															</div>
															<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
																<input name="additional_withholding" value="<?php echo e($emp->additional_withholding); ?>" type="text" id="additional_withholding" class="form-control tttt" />
															</div>
															<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
																<center>
																<div class="dropdown" style="margin-top: 1%;">
																	<label class="file-upload btn btn-primary">Browse for file ...
																		<input name="additional_attach" style="opecity:0" placeholder="Upload Service Image"  type="file">
																	</label>
																	<input name="file_name_2" placeholder="Upload  Image" value="<?php echo e($emp->additional_attach); ?>" 
																	class="form-control fsc-input" id="" type="hidden">	<span>
															<?php echo e($emp->additional_attach); ?>

															</span>
																</div>
																</center>
															</div>
															
														</div>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">State Claim :</label>
													<div class="col-md-8">
														<div class="row">
															<div class="col-lg-2 col-md-12 col-sm-12 col-xs-12">
																<div class="dropdown" style="margin-top: 1%;">
																	<input name="state_claim" value="<?php echo e($emp->state_claim); ?>" maxlength="3" type="text" id="state_claim" onkeypress="return isNumberKey(event)" class="form-control tttt" />
																</div>
															</div>
															<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
																<label class="fsc-form-label martop7">Additional Withholding :</label>
															</div>
															<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
																<input name="additional_withholding_1" value="<?php echo e($emp->additional_withholding_1); ?>" type="text" id="additional_withholding_1" class="form-control tttt" />
															</div>
															
															
																<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
																<center>
																<div class="dropdown" style="margin-top: 1%;">
																	<label class="file-upload btn btn-primary">Browse for file ...
																		<input name="additional_attach_1" style="opecity:0" placeholder="Upload  Image" id="" type="file">
																	</label>
																	<input name="file_name_1" placeholder="Upload  Image" value="<?php echo e($emp->additional_attach_1); ?>" class="form-control fsc-input" 
																	id="file_name_1" type="hidden">	<span>
															<?php echo e($emp->additional_attach_1); ?>

															</span>
																	</div>
																</center>
															</div>
														
														</div>
													</div>
													
												
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Local Claim :</label>
													<div class="col-md-8">
														<div class="row">
															<div class="col-lg-2 col-md-12 col-sm-12 col-xs-12">
																<div class="dropdown" style="margin-top: 1%;">
																	<input name="local_claim" value="<?php echo e($emp->local_claim); ?>" maxlength="3" type="text" id="local_claim" class="form-control tttt" />
																</div>
															</div>
															<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
																<label class="fsc-form-label martop7">Additional Withholding :</label>
															</div>
															<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
																<input name="additional_withholding_2" value="<?php echo e($emp->additional_withholding_2); ?>" type="text" id="additional_withholding_2" class="form-control tttt" />
															</div>
															
															<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
																<center>
																<div class="dropdown" style="margin-top: 1%;">
																	<label class="file-upload btn btn-primary">Browse for file ...
																		<input name="additional_attach_2" style="opecity:0" placeholder="Upload  Image" id="" type="file">
																	</label>
																	<input name="file_name" placeholder="Upload  Image" value="<?php echo e($emp->additional_attach_2); ?>" class="form-control fsc-input" 
																	id="file_name" type="hidden"></div>
																	<span><?php echo e($emp->additional_attach_2); ?></span>
																</center>
															</div>
														
											 			</div>
													</div>
													
												
												</div>!-->
												
												
												<div class="table-responsive">
												    <table class="table table-bordered">
												        <thead>
												            <tr>
												                <th width="10%">Year</th>
												                <th width="27%;">Filling Status</th>
												                <th width="9%">Fed. Claim</th>
												                <th width="11%">Fed. Add. Wh.</th>
												                <th width="9%">St. Claim</th>
												                <th width="11%">St. Add. Wh.</th>
												                <th width="9%">Local Claim</th>
												                <th width="12%">Local Add. Wh.</th>
												                <th width="5%">Action</th>
												            </tr>
												        </thead>
												        <tbody class="addtaxinfo">
												            <?php 
												            $whcount=count($whinfo);
												            if($whcount =='0')
												            {?>
												            <tr>
												                <td><select class="form-control" name="year_wh[]"><option value="">Select</option><option value="2021">2021</option></select></td>
												                <td>	<select name="fillingstatus[]" id="fillingstatus" class="form-control tttt">
															<option value="Single">Single</option>
															<option value="Married File Jointly">Married File Jointly</option>
															<option value="Married File Separately">Married File Separately</option>
															<option value="Head of Household">Head of Household</option>
														</select>
													</td>
												                <td><input class="form-control text-center" name="fed_claim[]"></td>
												                <td><input class="form-control text-center fedwh_1" name="fed_wh[]"></td>
												                <td><input class="form-control text-center" name="st_claim[]"></td>
												                <td><input class="form-control text-center stwh_1" name="st_wh[]"></td>
												                <td><input class="form-control text-center" name="local_claims[]"></td>
												                <td><input class="form-control text-center localwh_1" name="local_wh[]"></td>
												                <td style="text-align:center;">

                                                                </td>
												            </tr>
												            <?php
												            }
												            else
												            {
												                $cnts=1;
												                foreach($whinfo as $whinfo)
												                {
												                    $cnts++;
												            ?>
												            <tr>
												                <td><select class="form-control" name="year_wh[]">
												                    <option value="">Select</option>
												                    <option value="2021" <?php if($whinfo->year_wh =='2021'): ?> selected <?php endif; ?>>2021</option>
												                    </select>
												                    </td>
												                <td>	<select name="fillingstatus[]" id="fillingstatus" class="form-control tttt">
															<option value="Single" <?php if($whinfo->fillingstatus =='Single'): ?> selected <?php endif; ?>>Single</option>
															<option value="Married File Jointly" <?php if($whinfo->fillingstatus =='Married File Jointly'): ?> selected <?php endif; ?>>Married File Jointly</option>
															<option value="Married File Separately" <?php if($whinfo->fillingstatus =='Married File Separately'): ?> selected <?php endif; ?>>Married File Separately</option>
															<option value="Head of Household" <?php if($whinfo->fillingstatus =='Head of Household'): ?> selected <?php endif; ?>>Head of Household</option>
														</select>
													</td>
												                <td><input class="form-control text-center" name="fed_claim[]" value="<?php echo e($whinfo->fed_claim); ?>"></td>
												                <td><input class="form-control text-center fedwh_<?php echo $cnts;?>" name="fed_wh[]" value="<?php echo e($whinfo->fed_wh); ?>"></td>
												                <td><input class="form-control text-center" name="st_claim[]" value="<?php echo e($whinfo->st_claim); ?>"></td>
												                <td><input class="form-control text-center stwh_<?php echo $cnts;?>" name="st_wh[]" value="<?php echo e($whinfo->st_wh); ?>"></td>
												                <td><input class="form-control text-center" name="local_claims[]" value="<?php echo e($whinfo->local_claims); ?>"></td>
												                <td><input class="form-control text-center localwh_<?php echo $cnts;?>" name="local_wh[]" value="<?php echo e($whinfo->local_wh); ?>"></td>
												                <td style="text-align:center;">
                                                                <a href="#myModalwh_<?php echo e($whinfo->id); ?>" id="add_row_wh" role="button" class="btn btn-danger remove_wh" title="Remove field" data-toggle="modal"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></a>
                                                               
                                                                </td>
												            </tr>
												             <script>
												                	$(function() {
	
												                	$('.fedwh_<?php echo $cnts;?>').blur(function() {
												                	    $(this).val('$' + $(this).getNum());
                                                            			});
                                                            		
                                                            		
                                                            		$('.stwh_<?php echo $cnts;?>').blur(function() {
												                	    
                                                            			$(this).val('$' + $(this).getNum());
                                                            			});
                                                            		
                                                            	
                                                            		$('.localwh_<?php echo $cnts;?>').blur(function() {
												                	    
                                                            			$(this).val('$' + $(this).getNum());
                                                            			});
                                                            		});
                                                            		
		
												                </script>
												                
												            <?php 
												            }
												            }
												            ?>
												        </tbody>
												    </table>
												</div>
											</div>
											
											<div class="Branch" style="display: grid;padding: 7px 10px !important;">
												<h1>Company Benefit</h1>
											    <div class="review" style="text-align:right;position: absolute;right: 25px;">
												    <a class="btn btn-warning" id="add_leave" style="margin-top: -5px;">Add</a>
												</div>
											    <div style="text-align:right;position: absolute;left: 30px;">
												    <span style="float:left;font-weight:bold;margin-right:8px;">None : </span><?php if($emp->paid_leave > 0): ?>
												    <input type="checkbox" class="none_benefit" id="none" name="none" value="">
													<?php else: ?>
													    <input type="checkbox" class="none_benefit" id="none" name="none" value="" checked="">
													<?php endif; ?>
													<label for="none" class="pull-left"> </label>
												</div>
											</div>
											<!--<div class="form-group">
												<label class="control-label col-md-3" style="padding-top: 0px;">None :</label>
												<div class="col-md-6">
													<div class="row">
														<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
														    <?php if($emp->paid_leave > 0): ?>
															    <input type="checkbox" class="none_benefit" id="none" name="none" value="">
															<?php else: ?>
															    <input type="checkbox" class="none_benefit" id="none" name="none" value="" checked="">
															<?php endif; ?>
															<label for="none" class="pull-left"> </label>
														</div>
													</div>
												</div>
											</div>-->
											<div class="clear clearfix"></div>
											<div class="show_benefit">
											    <div class="table-responsive">
											        <table class="table table-bordered">
											            <thead>
											                <th width="5%">No</th>
											                <th width="15%">Type of Leave</th>
											                <th width="7%">Days</th>
											                <th width="11%">Frequency</th>
											                <th width="14%">Effective Date</th>
											                <!--<th width="11%">Sick Leave</th>-->
											                <!--<th width="10%">Frequency</th>-->
											                <th width="">Note</th>
											                <th width="5%">Action</th>
											            </thead>
											            <tbody class="addleaves">
											                <tr>
											                    <td class="text-center">1</td>
											                    <td>
											                        <!--<input name="paid_leave" type="text"  id="paid_leave" class="form-control text-center" value="<?php echo e($emp->paid_leave); ?>">-->
											                        <select class="form-control">
											                            <option>Select</option>
											                            <option>Paid Leave</option>
											                            <option>Sick Leave</option>
											                            <option>Vacation Leave</option>
											                        </select>
										                        </td>
											                    <td><input type="text" class="form-control text-center" name="" id=""></td>
											                    <td>
											                        <select class="form-control" name="paid_leave_time">
                                                                        <option value="">Select</option>
                                                                        <option value="1" <?php if($emp->paid_time == 1): ?> <?php echo e('selected'); ?> <?php endif; ?>>Week</option>
                                                                        <option value="2" <?php if($emp->paid_time == 2): ?> <?php echo e('selected'); ?> <?php endif; ?>>Month</option>
                                                                    </select>
                                                                </td>
                                                                <td><input type="date" class="form-control"></td>
											                    <!--<td><input name="sick_leave" type="text" id="sick_leave" class="form-control text-center" value="<?php echo e($emp->sick_leave); ?>"></td>
											                    <td>
											                        <select class="form-control" name="sick_leave_time">
                                                                        <option value="">Select</option>
                                                                        <option value="1" <?php if($emp->sick_time == 1): ?> <?php echo e('selected'); ?> <?php endif; ?>>Week</option>
                                                                        <option value="2" <?php if($emp->sick_time == 2): ?> <?php echo e('selected'); ?> <?php endif; ?>>Month</option>
                                                                    </select>
                                                                </td>-->
											                    <td><textarea class="form-control" id="note1" name="note1" rows="1"><?php echo e($emp->benefit_notes); ?></textarea></td>
											                    <td class="text-center"><a role="button" class="btn btn-danger"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></a></td>
											                </tr>
											            </tbody>
											        </table>
										        </div>
                                                <?php if($emp->type !='user')
                                                {?>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3" style="padding-top:0px;">Holiday :</label>
                                                        <div class="col-md-9">
                                                            <div class="col-md-12 col-sm-12 col-xs-12 checkboxbox">
                                                                <div class="row">
                                                                    <?php
                                                                        $myStringh = $emp->holidays;
                                                                        $arrayh =explode(",",$myStringh); ?>
                                                                        <?php $__currentLoopData = $holiday; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lan1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                        <div class="col-md-4"><input type="checkbox"  id="holiday_<?php echo e($lan1->holiday_name); ?>" 
                                                                            <?php foreach($arrayh as $arr){ if($arr==$lan1->holiday_name){?> checked <?php } } ?> 
                                                                            name="holidays[]" value="<?php echo e($lan1->holiday_name); ?>"> 
                                                                            <label class="fsc-form-label" for="holiday_<?php echo e($lan1->holiday_name); ?>"> <?php echo e($lan1->holiday_name); ?></label>
                                                                        </div>
                                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php
                                                }
                                                ?>
                                                <!--<div class="form-group">
                                                    <label class="control-label col-md-3">Paid Leave :</label>
                                                    <div class="col-md-9">
                                                        <div class="row">
                                                            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                                <input name="paid_leave" type="text"  id="paid_leave" class="form-control" value="<?php echo e($emp->paid_leave); ?>">
                                                            </div>
                                                            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                                <select class="form-control" name="paid_leave_time">
                                                                    <option value="">Select</option>
                                                                    <option value="1" <?php if($emp->paid_time == 1): ?> <?php echo e('selected'); ?> <?php endif; ?>>Week</option>
                                                                    <option value="2" <?php if($emp->paid_time == 2): ?> <?php echo e('selected'); ?> <?php endif; ?>>Month</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                                <label class="control-label">Sick Leave :</label>
                                                            </div>
                                                            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                                <input name="sick_leave" type="text" id="sick_leave" class="form-control" value="<?php echo e($emp->sick_leave); ?>">
                                                            </div>
                                                            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 fsc-form-col">
                                                                <select class="form-control" name="sick_leave_time">
                                                                    <option value="">Select</option>
                                                                    <option value="1" <?php if($emp->sick_time == 1): ?> <?php echo e('selected'); ?> <?php endif; ?>>Week</option>
                                                                    <option value="2" <?php if($emp->sick_time == 2): ?> <?php echo e('selected'); ?> <?php endif; ?>>Month</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>-->
                                                <!--<div class="form-group">-->
                                                <!--    <label class="control-label col-md-3">Holiday :</label>-->
                                                <!--    <div class="col-md-6">-->
                                                <!--        <div class="row">-->
                                                <!--            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">-->
                                                <!--                <textarea class="form-control" id="holiday_1" name="holiday_1"><?php echo e($emp->benefit_holiday); ?></textarea>-->
                                                <!--            </div>-->
                                                <!--        </div>-->
                                                <!--    </div>-->
                                                <!--</div>-->
                                                <!--<div class="form-group">
                                                    <label class="control-label col-md-3">Notes :</label>
                                                    <div class="col-md-6">
                                                        <div class="row">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <textarea class="form-control" id="note1" name="note1"><?php echo e($emp->benefit_notes); ?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>-->
											</div>
										</div>
										<div class="tab-pane fade" id="tab4primary">
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
											    <div class="row">
												<div class="Branch">
													<h1>Personal Information</h1>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Gender :</label>
													<div class="col-lg-6 col-md-8">
														<div class="row">
															<div class="col-md-6">
																<div class="dropdown" style="margin-top: 1%;">
																	<select name="gender" id="gender" class="form-control fsc-input">
																		<option value="">Select</option>
																		<option value="Male" <?php if($emp->gender=='Male'): ?> selected <?php endif; ?>>Male</option>
																		<option value="Female" <?php if($emp->gender=='Female'): ?> selected <?php endif; ?>>Female</option>
																	</select>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Marital Status :</label>
													<div class="col-lg-6 col-md-8">
														<div class="row">
															<div class="col-md-6">
																<div class="dropdown" style="margin-top: 1%;">
																	<select name="marital" id="marital" class="form-control fsc-input">
																		<option value="">Select</option>
																		<option value="Married" <?php if($emp->marital=='Married'): ?> selected <?php endif; ?>>Married</option>
																		<option value="UnMarried" <?php if($emp->marital=='UnMarried'): ?> selected <?php endif; ?>>UnMarried</option>
																	</select>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Date of Birth :</label>
													<div class="col-lg-6 col-md-8">
														<div class="row">
															<div class="col-md-6">
																<div class="dropdown" style="margin-top: 1%;">
																	<input type="text" class="form-control date1 fsc-input" name="month" id="month" value="<?php echo e($emp->month); ?>">
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="form-group">
												    <label class="control-label col-md-3">Languages Known:</label>
												    <div class="col-lg-6 col-md-8 langcheckbox"> 
												        <div class="row">
												     <?php
												           $myString = $emp->languages;
$array =explode(",",$myString); ?>
												             <?php $__currentLoopData = $language; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lan1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
												            <div class="col-sm-4 col-xs-6"><input type="checkbox"  id="lanbengali_<?php echo e($lan1->language_name); ?>" <?php foreach($array as $arr){ if($arr==$lan1->language_name){?> checked <?php } } ?> name="languages[]" value="<?php echo e($lan1->language_name); ?>"> <label class="fsc-form-label" for="lanbengali_<?php echo e($lan1->language_name); ?>"> <?php echo e($lan1->language_name); ?></label></div>
												            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
												        </div>
												       
												    </div>
												   
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">ID Proof 1 :</label>
													<div class="col-lg-6 col-md-8">
														<div class="row">
															<div class="col-lg-6 col-md-6 col-xs-7">
																<div class="dropdown" style="margin-top: 1%;">
																	<select name="pf1" class="form-control fsc-input" id="pf1">
																		<option value="">Select</option>
																		<option value="State ID" <?php if($emp->pf1=='State ID'): ?> selected <?php endif; ?>>State ID</option>
																		<option value="Voter Id" <?php if($emp->pf1=='Voter Id'): ?> selected <?php endif; ?>>Voter ID</option>
																		<option value="Driving Licence" <?php if($emp->pf1=='Driving Licence'): ?> selected <?php endif; ?>>Driving Licence</option>
																		<option value="Pan Card" <?php if($emp->pf1=='DPan Card'): ?> selected <?php endif; ?>>Pan Card</option>
																		<option value="Pass Port" <?php if($emp->pf1=='Pass Port'): ?> selected <?php endif; ?>>Pass Port</option>
																	</select>
																</div>
															</div>
                                                            <div class="col-md-6 col-xs-5">
                                                                <div class="">
                                                                    <div class=" <?php echo e($errors->has('pfid1') ? ' has-error' : ''); ?>">
                                                                        <div class="dropdown" style="margin-top: 5px;">
                                                                            <label class="file-upload btn btn-primary">Browse for file ...
                                                                                <input name="pfid1" style="opecity:0" placeholder="Upload  Image" id="" type="file">
                                                                            </label>
                                                                            <input name="pfid1_name" placeholder="Upload Image" value="<?php echo e($emp->pfid1); ?>" class="form-control fsc-input" 
                                                                            id="" type="hidden">
                                                                            <span>
                                                                                <strong><?php echo e($emp->pfid1); ?></strong>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">ID Proof 2 :</label>
													<div class="col-lg-6 col-md-8">
														<div class="row">
															<div class="col-lg-6 col-md-6 col-xs-7">
																<div class="dropdown" style="margin-top: 1%;">
																	<select name="pf2" class="form-control fsc-input" id="pf2">
																		<option value="">Select</option>
																		<option value="State ID" <?php if($emp->pf2=='State ID'): ?> selected <?php endif; ?>>State ID</option>
																		<option value="Voter Id" <?php if($emp->pf2=='Voter Id'): ?> selected <?php endif; ?>>Voter ID</option>
																		<option value="Driving Licence" <?php if($emp->pf2=='Driving Licence'): ?> selected <?php endif; ?>>Driving Licence</option>
																		<option value="Pan Card" <?php if($emp->pf2=='DPan Card'): ?> selected <?php endif; ?>>Pan Card</option>
																		<option value="Pass Port" <?php if($emp->pf2=='Pass Port'): ?> selected <?php endif; ?>>Pass Port</option>
																	</select>
																</div>
															</div>
														<div class="col-md-6 col-xs-5">
														<div class="">
															<div class="<?php echo e($errors->has('pfid2') ? ' has-error' : ''); ?>">
																<div class="dropdown" style="margin-top: 5px;">
																	<label class="file-upload btn btn-primary">Browse for file ...
																		<input name="pfid2" style="opecity:0" placeholder="Upload  Image" id="" type="file">
																	</label>
																	<input name="pfid2_name" placeholder="Upload Image" value="<?php echo e($emp->pfid2); ?>" class="form-control fsc-input" 
																	id="" type="hidden"><span>
															<strong><?php echo e($emp->pfid2); ?></strong>
															</span>
																	</div>
															</div>
														</div>
													</div>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3 col-xs-4">Resume :</label>
													<div class="col-lg-6 col-md-8 col-xs-8">
														<div class="row">
															<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 <?php echo e($errors->has('resume') ? ' has-error' : ''); ?>">
																<div class="dropdown" style="margin-top: 1%;">
																	<label class="file-upload btn btn-primary">Browse for file ...
																		<input name="resume" style="opecity:0" placeholder="Upload Service Image" id="resume" type="file">
																	</label>
																	<input name="resume_1" placeholder="Upload Service Image" value="<?php echo e($emp->resume); ?>" class="form-control fsc-input" id="resume_1" type="hidden"><?php if($errors->has('resume')): ?>	<span class="help-block">
															<strong><?php echo e($errors->first('resume')); ?></strong>
															</span>
																	<?php endif; ?></div>
															</div>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Type of Agreement :</label>
													<div class="col-lg-6 col-md-8">
														<div class="row">
															<div class="col-lg-6 col-md-6 col-xs-7">
																<div class="dropdown" style="margin-top: 1%;">
																	<select name="type_agreement" class="form-control fsc-input" id="type_agreement">
																		<option value="">Select</option>
																		<option value="Hiring Letter" <?php if($emp->type_agreement=='Hiring Letter'): ?> selected <?php endif; ?>>Hiring Letter</option>
																		<option value="Employment Agreement" <?php if($emp->type_agreement=='Employment Agreement'): ?> selected <?php endif; ?>>Employment Agreement</option>
																	</select>
																</div>
															</div>
															<div class="col-md-6 col-xs-5 <?php echo e($errors->has('agreement') ? ' has-error' : ''); ?>">
																<div class="dropdown" style="margin-top: 5px;">
																	<label class="file-upload btn btn-primary">Browse for file ...
																		<input name="agreement" placeholder="Upload Service Image" class="form-control fsc-input" id="agreement" type="file">
																	</label>
																	<input name="agreement_1" value="<?php echo e($emp->agreement); ?>" placeholder="Upload Service Image" class="form-control fsc-input" id="agreement_1" type="hidden">
																	<img src="<?php echo e(asset('public/agreement')); ?>/<?php echo e($emp->agreement); ?>" alt="" id="blah-3" alt="your image" style="margin-top:10px;width:69px;">
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="Branch">
													<h1>Emergency Contact Info</h1>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Contact Person Name :</label>
													<div class="col-lg-6 col-md-8">
														<div class="row">
															<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 fsc-element-margin">
																<input type="text" class="form-control fsc-input textonly" id="firstName_1" name="firstName_1" placeholder="First" value="<?php echo e($emp->firstName_1); ?>">
															</div>
															<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 fsc-element-margin">
																<input type="text" maxlength="1" class="form-control fsc-input textonly" id="middleName_1" name="middleName_1" placeholder="M" value="<?php echo e($emp->middleName_1); ?>">
															</div>
															<div class="col-lg-4 col-md-8 col-sm-8 col-xs-8 fsc-element-margin">
																<input type="text" class="form-control fsc-input textonly" id="lastName_1" name="lastName_1" value="<?php echo e($emp->lastName_1); ?>" placeholder="Last">
															</div>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Address 1 :</label>
													<div class="col-lg-6 col-md-8">
														<input type="text" placeholder="Address 1" class="form-control fsc-input" name="address11" id="address11" value="<?php echo e($emp->address11); ?>">
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Address 2:</label>
													<div class="col-lg-6 col-md-8">
														<input type="text" class="form-control fsc-input" name="eaddress1" id="eaddress1" value="<?php echo e($emp->eaddress1); ?>">
														<input type="hidden" class="form-control fsc-input" name="status" id="status" value="<?php echo e($emp->status); ?>">
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">City/State/Zip :</label>
													<div class="col-lg-6 col-md-8">
														<div class="row">
															<div class="col-lg-5 col-md-4 col-sm-12 col-xs-12 fsc-element-margin">
																<input type="text" class="form-control textonly fsc-input" id="ecity" name="ecity" placeholder="City" value="<?php echo e($emp->ecity); ?>">
															</div>
															<div class="col-lg-4 col-md-4 col-xs-6">
																<div class="dropdown" style="margin-top: 1%;">
																	<select name="estate" id="estate" class="form-control fsc-input bfh-states" data-country="countries_states1"></select>
																</div>
															</div>
															<div class="col-lg-3 col-md-4 col-xs-6 fsc-element-margin">
																<input type="text" class="form-control fsc-input zip" id="ezipcode" maxlength="5" name="ezipcode" value="<?php echo e($emp->ezipcode); ?>" placeholder="Zip">
															</div>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Telephone 1 :</label>
													<div class="col-lg-6 col-md-8">
														<div class="row">
															<div class="col-lg-5 col-md-4 col-sm-12 col-xs-12 fsc-element-margin">
																<input type="text" class="form-control fsc-input phone" id="etelephone1" name="etelephone1" placeholder="(000) 000-0000" value="<?php echo e($emp->etelephone1); ?>">
															</div>
															<div class="col-lg-4 col-md-4 col-xs-6">
																<div class="dropdown" style="margin-top: 1%;">
																	<select name="eteletype1" id="eteletype1" class="form-control fsc-input">
																		<option value="Mobile" <?php if($emp->eteletype1=='Mobile'): ?> selected <?php endif; ?>>Mobile</option>
																		<option value="Resident" <?php if($emp->eteletype1=='Resident'): ?> selected <?php endif; ?>>Resident</option>
																		<option value="Office" <?php if($emp->eteletype1=='Office'): ?> selected <?php endif; ?>>Work</option>
																		<option value="Other" <?php if($emp->eteletype1=='Other'): ?> selected <?php endif; ?>>Other</option>
																	</select>
																</div>
															</div>
															<div class="col-lg-3 col-md-4 col-xs-6 fsc-element-margin">
																<input type="text" class="form-control fsc-input zip"  maxlength="5" value="<?php echo e($emp->eext1); ?>" readonly id="eext1" name="eext1" placeholder="Ext">
															</div>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Telephone 2 :</label>
													<div class="col-lg-6 col-md-8">
														<div class="row">
															<div class="col-lg-5 col-md-4 col-sm-12 col-xs-12 fsc-element-margin">
																<input type="text" class="form-control fsc-input phone" id="etelephone2" name="etelephone2" placeholder="(000) 000 0000" value="<?php echo e($emp->etelephone2); ?>">
															</div>
															<div class="col-lg-4 col-md-4 col-xs-6">
																<div class="dropdown" style="margin-top: 1%;">
																	<select name="eteletype2" id="eteletype2" class="form-control fsc-input">
																		<option value="Mobile" <?php if($emp->eteletype1=='Mobile'): ?> selected <?php endif; ?>>Mobile</option>
																		<option value="Resident" <?php if($emp->eteletype1=='Resident'): ?> selected <?php endif; ?>>Resident</option>
																		<option value="Office" <?php if($emp->eteletype1=='Office'): ?> selected <?php endif; ?>>Work</option>
																		<option value="Other" <?php if($emp->eteletype1=='Other'): ?> selected <?php endif; ?>>Other</option>
																	</select>
																</div>
															</div>
															<div class="col-lg-3 col-md-4 col-xs-6 fsc-element-margin">
																<input type="text" class="form-control fsc-input zip"  maxlength="5" value="<?php echo e($emp->eext2); ?>" readonly id="eext2" name="eext2" placeholder="Ext">
															</div>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Fax :</label>
													<div class="col-lg-6 col-md-8">
														<div class="row">
															<div class="col-md-5">
																<input type="text" class="form-control fsc-input phone" placeholder="(000) 000 0000" name="efax" id="efax" value="<?php echo e($emp->efax); ?>">
															</div>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">E-mail :</label>
													<div class="col-lg-6 col-md-8">
														<input type="text" class="form-control fsc-input" name="eemail" id="eemail" value="<?php echo e($emp->eemail); ?>">
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Relationship :</label>
													<div class="col-lg-6 col-md-8">
														<input type="text" class="form-control fsc-input" name="relation" id="relation" value="<?php echo e($emp->relation); ?>">
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Note For Emergency :</label>
													<div class="col-lg-6 col-md-8">
														<textarea name="comments1" id="comments1" rows="1" class="form-control fsc-input"><?php echo e($emp->comments1); ?></textarea>
													</div>
												</div>
											</div>
											</div>
										</div>
										<div class="tab-pane fade" id="tab5primary">
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
											    <div class="row">
												<div class="Branch">
													<h1>Security Information</h1>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">User Name :</label>
													<div class="col-lg-6 col-md-8">
														<div class="row">
															<div class="col-md-12">
																<input id="uname" class="form-control fsc-input" placeholder="User Name" value="<?php echo e($emp->email); ?>" readonly="" name="uname" type="text">
															</div>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Password :</label>
													<div class="col-lg-6 col-md-8">
														<div class="row">
															<div class="col-md-6 col-xs-6">
																<input placeholder="Password" class="form-control fsc-input" id="password" name="password" value="<?php if(empty($empfsc->newpassword)): ?> <?php else: ?> <?php echo e($empfsc->newpassword); ?> <?php endif; ?>" readonly="" type="password"> <span toggle="#password" class="fa fa-fw fa-eye field-icon toggle-password"></span>
															</div>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Reset Days :</label>
													<div class="col-lg-6 col-md-8">
														<div class="row">
															<div class="col-md-3 col-xs-6">
																<select name="reset" id="reset" class="form-control fsc-input disabledd" readonly="">
																	<option value="30" <?php if($emp->reset=='30'): ?> selected <?php endif; ?>>30</option>
																	<option value="60" <?php if($emp->reset=='60'): ?> selected <?php endif; ?>>60</option>
																	<option value="90" <?php if($emp->reset=='90'): ?> selected <?php endif; ?>>90</option>
																	<option value="120" <?php if($emp->reset=='120'): ?> selected <?php endif; ?>>120</option>
																</select>
															</div>
															<div class="col-md-3 col-xs-6">
																<input name="reset_date" id="reset_date" value="<?php if(empty($empfsc->enddate)): ?> <?php else: ?> <?php echo e($empfsc->enddate); ?> <?php endif; ?>" class="form-control fsc-input" readonly="">
															</div>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Question 1 :</label>
													<div class="col-lg-6 col-md-8">
														<select name="question1" id="question1" class="form-control fsc-input disabledd" readonly="">
															<option value="">Select</option>
															<option value="What was your favorite place to visit as a child?">What was your favorite place to visit as a child?</option>
															<option value="Who is your favorite actor, musician, or artist?">Who is your favorite actor, musician, or artist?</option>
															<option value="What is the name of your favorite pet?">What is the name of your favorite pet?</option>
															<option value="In what city were you born?" selected="selected">In what city were you born?</option>
															<option value="What is the name of your first school?">What is the name of your first school?</option>
														</select>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Answer 1 :</label>
													<div class="col-lg-6 col-md-8">
														<input name="answer1" value="<?php echo e($emp->answer1); ?>" placeholder="" class="form-control fsc-input" id="answer1" readonly="" type="text">
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Question 2 :</label>
													<div class="col-lg-6 col-md-8">
														<select name="question2" id="question2" class="form-control fsc-input disabledd" readonly="">
															<option value="">Select</option>
															<option value="What is your favorite movie?">What is your favorite movie?</option>
															<option value="What was the make of your first car?">What was the make of your first car?</option>
															<option value="What is your favorite color?" selected="selected">What is your favorite color?</option>
															<option value="What is your father's middle name?">What is your fathers middle name?</option>
															<option value="What is the name of your first grade teacher?">What is the name of your first grade teacher?</option>
														</select>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Answer 2 :</label>
													<div class="col-lg-6 col-md-8">
														<input name="answer2" value="<?php echo e($emp->answer2); ?>" placeholder="" class="form-control fsc-input" id="answer2" readonly="" type="text">
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Question 3 :</label>
													<div class="col-lg-6 col-md-8">
														<select name="question3" id="question3" class="form-control fsc-input disabledd" readonly="">
															<option value="">Select</option>
															<option value="What was your high school mascot?">What was your high school mascot?</option>
															<option value="Which is your favorite web browser?">Which is your favorite web browser?</option>
															<option value="In what year was your father born?">In what year was your father born?</option>
															<option value="What is the name of your favorite childhood friend?" selected="selected">What is the name of your favorite childhood friend?</option>
															<option value="What was your favorite food as a child?">What was your favorite food as a child?</option>
														</select>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Answer 3 :</label>
													<div class="col-lg-6 col-md-8">
														<input name="answer3" value="<?php echo e($emp->answer3); ?>" placeholder="" class="form-control fsc-input" id="answer3" readonly="" type="text">
													</div>
												</div>
											</div>
											</div>
										</div>
										<div class="tab-pane fade" id="tab6primary">
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
											    <div class="row">
												<div class="Branch">
													<h1>Coming Soon..</h1>
												</div>
											</div>
											</div>
										</div>
										<div class="tab-pane fade" id="tab7primary">
											<div class="col-md-12">
											    <div class="row">
												<div class="Branch">
													<h1>User Rights</h1>
												</div>
												<div class="usergihtsbox">
												    <ul class="nav nav-tabs" style="border:0px!important; background:none!important">
                                                                <li class="active"><a data-toggle="tab" href="#utab1">Client Management</a></li>
                                                                <li><a data-toggle="tab" href="#utab2">Work Management</a></li>
                                                                <li><a data-toggle="tab" href="#utab3">Employee Management</a></li>
                                                                <li><a data-toggle="tab" href="#utab4">Report</a></li>
                                                                <li><a data-toggle="tab" href="#utab5">Technical Support</a></li>
                                                                <li><a data-toggle="tab" href="#utab6">Email Access</a></li>
                                                              </ul>
                                                        
                                                              <div class="tab-content">
                                                                <div id="utab1" class="tab-pane fade in active">
                                                                    <div class="userpermissionbox">
                                                                         <div class="row">
                                                                             <div class="col-md-3"><label class="perttl pull-right left_991">Client Edit / View</label></div>
                                                                             <div class="col-md-9">
                                                                                   <ul class="chkboxtab">
                                                                                 <li>
                                                                                    	<input type="checkbox" id="addaccess" name="editaccess" value="Edit" class="check" > <label for="addaccess" class="chklabel">Add</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="editaccess" name="editaccess" value="Edit" class="check" > <label for="editaccess"  class="chklabel">Edit</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="updateaccess" name="updateaccess" value="Edit" class="check" > <label for="updateaccess" class="chklabel">View</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="deleteaccess" name="deleteaccess" value="Edit" class="check" > <label for="deleteaccess" class="chklabel">Delete</label>
                                                                                </li>
                                                                            </ul>
                                                                             </div>
                                                                         </div>
                                                                          <div class="row">
                                                                             <div class="col-md-3"><label class="perttl pull-right left_991">View Addressbook</label></div>
                                                                             <div class="col-md-9">
                                                                                  <ul class="chkboxtab">
                                                                                 <li>
                                                                                    	<input type="checkbox" id="addaccess01" name="editaccess" value="Edit" class="check" > <label for="addaccess01" class="chklabel">Add</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="editaccess01" name="editaccess" value="Edit" class="check" > <label for="editaccess01"  class="chklabel">Edit</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="updateaccess01" name="updateaccess" value="Edit" class="check" > <label for="updateaccess01" class="chklabel">View</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="deleteaccess01" name="deleteaccess" value="Edit" class="check" > <label for="deleteaccess01" class="chklabel">Delete</label>
                                                                                </li>
                                                                            </ul>
                                                                             </div>
                                                                         </div>
                                                                     </div>
                                                                        <div class="clear"></div>
                                                                   
                                                                </div>
                                                                <div id="utab2" class="tab-pane fade">
                                                                     <div class="userpermissionbox">
                                                                         <div class="row">
                                                                             <div class="col-md-3"><label class="perttl pull-right left_991">Work New</label></div>
                                                                             <div class="col-md-9">
                                                                                  <ul class="chkboxtab">
                                                                                 <li>
                                                                                    	<input type="checkbox" id="addaccess1" name="editaccess" value="Edit" class="check" > <label for="addaccess1" class="chklabel">Add</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="editaccess1" name="editaccess" value="Edit" class="check" > <label for="editaccess1"  class="chklabel">Edit</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="updateaccess1" name="updateaccess" value="Edit" class="check" > <label for="updateaccess1" class="chklabel">View</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="deleteaccess1" name="deleteaccess" value="Edit" class="check" > <label for="deleteaccess1" class="chklabel">Delete</label>
                                                                                </li>
                                                                            </ul>
                                                                             </div>
                                                                         </div>
                                                                          <div class="row">
                                                                             <div class="col-md-3"><label class="perttl pull-right left_991">Work To Do</label></div>
                                                                             <div class="col-md-9">
                                                                                 <ul class="chkboxtab">
                                                                                 <li>
                                                                                    	<input type="checkbox" id="addaccess2" name="editaccess" value="Edit" class="check" > <label for="addaccess2" class="chklabel">Add</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="editaccess2" name="editaccess" value="Edit" class="check" > <label for="editaccess2"  class="chklabel">Edit</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="updateaccess2" name="updateaccess" value="Edit" class="check" > <label for="updateaccess2" class="chklabel">View</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="deleteaccess2" name="deleteaccess" value="Edit" class="check" > <label for="deleteaccess2" class="chklabel">Delete</label>
                                                                                </li>
                                                                            </ul>
                                                                             </div>
                                                                         </div>
                                                                         
                                                                         <div class="row">
                                                                             <div class="col-md-3"><label class="perttl pull-right left_991">Work Record</label></div>
                                                                             <div class="col-md-9">
                                                                                <ul class="chkboxtab">
                                                                                 <li>
                                                                                    	<input type="checkbox" id="addaccess3" name="editaccess" value="Edit" class="check" > <label for="addaccess3" class="chklabel">Add</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="editaccess3" name="editaccess" value="Edit" class="check" > <label for="editaccess3"  class="chklabel">Edit</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="updateaccess3" name="updateaccess" value="Edit" class="check" > <label for="updateaccess3" class="chklabel">View</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="deleteaccess3" name="deleteaccess" value="Edit" class="check" > <label for="deleteaccess3" class="chklabel">Delete</label>
                                                                                </li>
                                                                            </ul>
                                                                             </div>
                                                                         </div>
                                                                         
                                                                         <div class="row">
                                                                             <div class="col-md-3"><label class="perttl pull-right left_991">Work Status</label></div>
                                                                             <div class="col-md-9">
                                                                                 <ul class="chkboxtab">
                                                                                 <li>
                                                                                    	<input type="checkbox" id="addaccess4" name="editaccess" value="Edit" class="check" > <label for="addaccess4" class="chklabel">Add</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="editaccess4" name="editaccess" value="Edit" class="check" > <label for="editaccess4"  class="chklabel">Edit</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="updateaccess4" name="updateaccess" value="Edit" class="check" > <label for="updateaccess4" class="chklabel">View</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="deleteaccess4" name="deleteaccess" value="Edit" class="check" > <label for="deleteaccess4" class="chklabel">Delete</label>
                                                                                </li>
                                                                            </ul>
                                                                             </div>
                                                                         </div>
                                                                         <div class="row">
                                                                             <div class="col-md-3"><label class="perttl pull-right left_991">Work Submission</label></div>
                                                                             <div class="col-md-9">
                                                                                 <ul class="chkboxtab">
                                                                                 <li>
                                                                                    	<input type="checkbox" id="addaccess5" name="editaccess" value="Edit" class="check" > <label for="addaccess5" class="chklabel">Add</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="editaccess5" name="editaccess" value="Edit" class="check" > <label for="editaccess5"  class="chklabel">Edit</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="updateaccess5" name="updateaccess" value="Edit" class="check" > <label for="updateaccess5" class="chklabel">View</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="deleteaccess5" name="deleteaccess" value="Edit" class="check" > <label for="deleteaccess5" class="chklabel">Delete</label>
                                                                                </li>
                                                                            </ul>
                                                                             </div>
                                                                         </div>
                                                                         
                                                                         <div class="row">
                                                                             <div class="col-md-3"><label class="perttl pull-right left_991">Upload</label></div>
                                                                             <div class="col-md-9">
                                                                                 <ul class="chkboxtab">
                                                                                 <li>
                                                                                    	<input type="checkbox" id="addaccess6" name="editaccess" value="Edit" class="check" > <label for="addaccess6" class="chklabel">Add</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="editaccess6" name="editaccess" value="Edit" class="check" > <label for="editaccess6"  class="chklabel">Edit</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="updateaccess6" name="updateaccess" value="Edit" class="check" > <label for="updateaccess6" class="chklabel">View</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="deleteaccess6" name="deleteaccess" value="Edit" class="check" > <label for="deleteaccess6" class="chklabel">Delete</label>
                                                                                </li>
                                                                            </ul>
                                                                             </div>
                                                                         </div>
                                                                         
                                                                         
                                                                         <div class="row">
                                                                             <div class="col-md-3"><label class="perttl pull-right left_991">Task</label></label></div>
                                                                             <div class="col-md-9">
                                                                                 <ul class="chkboxtab">
                                                                                 <li>
                                                                                    	<input type="checkbox" id="addaccess7" name="editaccess" value="Edit" class="check" > <label for="addaccess7" class="chklabel">Add</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="editaccess7" name="editaccess" value="Edit" class="check" > <label for="editaccess7"  class="chklabel">Edit</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="updateaccess7" name="updateaccess" value="Edit" class="check" > <label for="updateaccess7" class="chklabel">View</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="deleteaccess7" name="deleteaccess" value="Edit" class="check" > <label for="deleteaccess7" class="chklabel">Delete</label>
                                                                                </li>
                                                                            </ul>
                                                                             </div>
                                                                         </div>
                                                                         
                                                                         <div class="row">
                                                                             <div class="col-md-3"><label class="perttl pull-right left_991">Message</label></div>
                                                                             <div class="col-md-9">
                                                                                 <ul class="chkboxtab">
                                                                                 <li>
                                                                                    	<input type="checkbox" id="addaccess8" name="editaccess" value="Edit" class="check" > <label for="addaccess8" class="chklabel">Add</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="editaccess8" name="editaccess" value="Edit" class="check" > <label for="editaccess8"  class="chklabel">Edit</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="updateaccess8" name="updateaccess" value="Edit" class="check" > <label for="updateaccess8" class="chklabel">View</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="deleteaccess8" name="deleteaccess" value="Edit" class="check" > <label for="deleteaccess8" class="chklabel">Delete</label>
                                                                                </li>
                                                                            </ul>
                                                                             </div>
                                                                         </div>
                                                                         
                                                                         
                                                                         <div class="row">
                                                                             <div class="col-md-3"><label class="perttl pull-right left_991">Calendar</label></label></div>
                                                                             <div class="col-md-9">
                                                                                 <ul class="chkboxtab">
                                                                                 <li>
                                                                                    	<input type="checkbox" id="addaccess9" name="editaccess" value="Edit" class="check" > <label for="addaccess9" class="chklabel">Add</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="editaccess9" name="editaccess" value="Edit" class="check" > <label for="editaccess9"  class="chklabel">Edit</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="updateaccess9" name="updateaccess" value="Edit" class="check" > <label for="updateaccess9" class="chklabel">View</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="deleteaccess9" name="deleteaccess" value="Edit" class="check" > <label for="deleteaccess9" class="chklabel">Delete</label>
                                                                                </li>
                                                                            </ul>
                                                                             </div>
                                                                         </div>
                                                                         
                                                                     </div>
                                                                        <div class="clear"></div>
                                                                 
                                                                </div>
                                                                <div id="utab3" class="tab-pane fade">
                                                                      <div class="userpermissionbox">
                                                                         <div class="row">
                                                                             <div class="col-md-3"><label class="perttl pull-right left_991">FSC EE Schedule </label></div>
                                                                             <div class="col-md-9">
                                                                                   <ul class="chkboxtab">
                                                                                 <li>
                                                                                    	<input type="checkbox" id="addaccess010" name="editaccess" value="Edit" class="check" > <label for="addaccess010" class="chklabel">Add</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="editaccess020" name="editaccess" value="Edit" class="check" > <label for="editaccess020"  class="chklabel">Edit</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="updateaccess030" name="updateaccess" value="Edit" class="check" > <label for="updateaccess030" class="chklabel">View</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="deleteaccess040" name="deleteaccess" value="Edit" class="check" > <label for="deleteaccess040" class="chklabel">Delete</label>
                                                                                </li>
                                                                            </ul>
                                                                             </div>
                                                                         </div>
                                                                          <div class="row">
                                                                             <div class="col-md-3"><label class="perttl pull-right left_991">FSC EE Leave Application</label></div>
                                                                             <div class="col-md-9">
                                                                                  <ul class="chkboxtab">
                                                                                 <li>
                                                                                    	<input type="checkbox" id="addaccess01" name="editaccess" value="Edit" class="check" > <label for="addaccess01" class="chklabel">Add</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="editaccess01" name="editaccess" value="Edit" class="check" > <label for="editaccess01"  class="chklabel">Edit</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="updateaccess01" name="updateaccess" value="Edit" class="check" > <label for="updateaccess01" class="chklabel">View</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="deleteaccess01" name="deleteaccess" value="Edit" class="check" > <label for="deleteaccess01" class="chklabel">Delete</label>
                                                                                </li>
                                                                            </ul>
                                                                             </div>
                                                                         </div>
                                                                     </div>
                                                                        <div class="clear"></div>
                                                                  
                                                                </div>
                                                                <div id="utab4" class="tab-pane fade">
                                                                     <div class="userpermissionbox">
                                                                         <div class="row">
                                                                             <div class="col-md-3"><label class="perttl pull-right left_991">Client Report  </label></div>
                                                                             <div class="col-md-9">
                                                                                   <ul class="chkboxtab">
                                                                                 <li>
                                                                                    	<input type="checkbox" id="addaccess001" name="editaccess" value="Edit" class="check" > <label for="addaccess001" class="chklabel">Add</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="editaccess002" name="editaccess" value="Edit" class="check" > <label for="editaccess002"  class="chklabel">Edit</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="updateaccess003" name="updateaccess" value="Edit" class="check" > <label for="updateaccess003" class="chklabel">View</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="deleteaccess004" name="deleteaccess" value="Edit" class="check" > <label for="deleteaccess004" class="chklabel">Delete</label>
                                                                                </li>
                                                                            </ul>
                                                                             </div>
                                                                         </div>
                                                                          <div class="row">
                                                                             <div class="col-md-3"><label class="perttl pull-right left_991">Work Report</label></div>
                                                                             <div class="col-md-9">
                                                                                  <ul class="chkboxtab">
                                                                                 <li>
                                                                                    	<input type="checkbox" id="addaccess0001" name="editaccess" value="Edit" class="check" > <label for="addaccess0001" class="chklabel">Add</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="editaccess0002" name="editaccess" value="Edit" class="check" > <label for="editaccess0002"  class="chklabel">Edit</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="updateaccess0003" name="updateaccess" value="Edit" class="check" > <label for="updateaccess0003" class="chklabel">View</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="deleteaccess0004" name="deleteaccess" value="Edit" class="check" > <label for="deleteaccess0004" class="chklabel">Delete</label>
                                                                                </li>
                                                                            </ul>
                                                                             </div>
                                                                         </div>
                                                                          <div class="row">
                                                                             <div class="col-md-3"><label class="perttl pull-right left_991">Employee Report</label></div>
                                                                             <div class="col-md-9">
                                                                                  <ul class="chkboxtab">
                                                                                 <li>
                                                                                    	<input type="checkbox" id="addaccess00001" name="editaccess" value="Edit" class="check" > <label for="addaccess00001" class="chklabel">Add</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="editaccess00002" name="editaccess" value="Edit" class="check" > <label for="editaccess00002"  class="chklabel">Edit</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="updateaccess00003" name="updateaccess" value="Edit" class="check" > <label for="updateaccess00003" class="chklabel">View</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="deleteaccess00004" name="deleteaccess" value="Edit" class="check" > <label for="deleteaccess00004" class="chklabel">Delete</label>
                                                                                </li>
                                                                            </ul>
                                                                             </div>
                                                                         </div>
                                                                         
                                                                     </div>
                                                                        <div class="clear"></div>
                                                                </div>
                                                                <div id="utab5" class="tab-pane fade">
                                                                     <div class="userpermissionbox">
                                                                         <div class="row">
                                                                             <div class="col-md-3"><label class="perttl pull-right left_991">Client Support   </label></div>
                                                                             <div class="col-md-9">
                                                                                   <ul class="chkboxtab">
                                                                                 <li>
                                                                                    	<input type="checkbox" id="addaccess001" name="editaccess" value="Edit" class="check" > <label for="addaccess001" class="chklabel">Add</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="editaccess002" name="editaccess" value="Edit" class="check" > <label for="editaccess002"  class="chklabel">Edit</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="updateaccess003" name="updateaccess" value="Edit" class="check" > <label for="updateaccess003" class="chklabel">View</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="deleteaccess004" name="deleteaccess" value="Edit" class="check" > <label for="deleteaccess004" class="chklabel">Delete</label>
                                                                                </li>
                                                                            </ul>
                                                                             </div>
                                                                         </div>
                                                                          <div class="row">
                                                                             <div class="col-md-3"><label class="perttl pull-right left_991">Employee Support</label></div>
                                                                             <div class="col-md-9">
                                                                                  <ul class="chkboxtab">
                                                                                 <li>
                                                                                    	<input type="checkbox" id="addaccess0001" name="editaccess" value="Edit" class="check" > <label for="addaccess0001" class="chklabel">Add</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="editaccess0002" name="editaccess" value="Edit" class="check" > <label for="editaccess0002"  class="chklabel">Edit</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="updateaccess0003" name="updateaccess" value="Edit" class="check" > <label for="updateaccess0003" class="chklabel">View</label>
                                                                                </li>
                                                                                <li>
                                                                                    	<input type="checkbox" id="deleteaccess0004" name="deleteaccess" value="Edit" class="check" > <label for="deleteaccess0004" class="chklabel">Delete</label>
                                                                                </li>
                                                                            </ul>
                                                                             </div>
                                                                         </div>
                                                                            <div class="row">
                                                                                <div class="col-md-3"><label class="perttl pull-right left_991">Other Support</label></div>
                                                                                <div class="col-md-9">
                                                                                    <ul class="chkboxtab">
                                                                                        <li>
                                                                                            <input type="checkbox" id="addaccess00001" name="editaccess" value="Edit" class="check" > <label for="addaccess00001" class="chklabel">Add</label>
                                                                                        </li>
                                                                                        <li>
                                                                                            <input type="checkbox" id="editaccess00002" name="editaccess" value="Edit" class="check" > <label for="editaccess00002"  class="chklabel">Edit</label>
                                                                                        </li>
                                                                                        <li>
                                                                                            <input type="checkbox" id="updateaccess00003" name="updateaccess" value="Edit" class="check" > <label for="updateaccess00003" class="chklabel">View</label>
                                                                                        </li>
                                                                                        <li>
                                                                                            <input type="checkbox" id="deleteaccess00004" name="deleteaccess" value="Edit" class="check" > <label for="deleteaccess00004" class="chklabel">Delete</label>
                                                                                        </li>
                                                                                    </ul>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-3"><label class="perttl pull-right left_991">Technical Support</label></div>
                                                                                <div class="col-md-9">
                                                                                    <ul class="chkboxtab">
                                                                                        <li>
                                                                                            <input type="checkbox" id="addtech00001" name="editaccess" value="Edit" class="check" > <label for="addtech00001" class="chklabel">Add</label>
                                                                                        </li>
                                                                                        <li>
                                                                                            <input type="checkbox" id="edittech00002" name="editaccess" value="Edit" class="check" > <label for="edittech00002"  class="chklabel">Edit</label>
                                                                                        </li>
                                                                                        <li>
                                                                                            <input type="checkbox" id="updatetech00003" name="updateaccess" value="Edit" class="check" > <label for="updatetech00003" class="chklabel">View</label>
                                                                                        </li>
                                                                                        <li>
                                                                                            <input type="checkbox" id="deletetech00004" name="deleteaccess" value="Edit" class="check" > <label for="deletetech00004" class="chklabel">Delete</label>
                                                                                        </li>
                                                                                    </ul>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-3"><label class="perttl pull-right left_991">Timing Support</label></div>
                                                                                <div class="col-md-9">
                                                                                    <ul class="chkboxtab">
                                                                                        <li>
                                                                                            <input type="checkbox" id="addtiming00001" name="editaccess" value="Edit" class="check" > <label for="addtiming00001" class="chklabel">Add</label>
                                                                                        </li>
                                                                                        <li>
                                                                                            <input type="checkbox" id="edittiming00002" name="editaccess" value="Edit" class="check" > <label for="edittiming00002"  class="chklabel">Edit</label>
                                                                                        </li>
                                                                                        <li>
                                                                                            <input type="checkbox" id="updatetiming00003" name="updateaccess" value="Edit" class="check" > <label for="updatetiming00003" class="chklabel">View</label>
                                                                                        </li>
                                                                                        <li>
                                                                                            <input type="checkbox" id="deletetiming00004" name="deleteaccess" value="Edit" class="check" > <label for="deletetiming00004" class="chklabel">Delete</label>
                                                                                        </li>
                                                                                    </ul>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-3"><label class="perttl pull-right left_991">System Support</label></div>
                                                                                <div class="col-md-9">
                                                                                    <ul class="chkboxtab">
                                                                                        <li>
                                                                                            <input type="checkbox" id="addsystem00001" name="editaccess" value="Edit" class="check" > <label for="addsystem00001" class="chklabel">Add</label>
                                                                                        </li>
                                                                                        <li>
                                                                                            <input type="checkbox" id="editsystem00002" name="editaccess" value="Edit" class="check" > <label for="editsystem00002"  class="chklabel">Edit</label>
                                                                                        </li>
                                                                                        <li>
                                                                                            <input type="checkbox" id="updatesystem00003" name="updateaccess" value="Edit" class="check" > <label for="updatesystem00003" class="chklabel">View</label>
                                                                                        </li>
                                                                                        <li>
                                                                                            <input type="checkbox" id="deletesystem00004" name="deleteaccess" value="Edit" class="check" > <label for="deletesystem00004" class="chklabel">Delete</label>
                                                                                        </li>
                                                                                    </ul>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-3"><label class="perttl pull-right left_991">Time Sheet</label></div>
                                                                                <div class="col-md-9">
                                                                                    <ul class="chkboxtab">
                                                                                        <li>
                                                                                            <input type="checkbox" id="addsheet00001" name="editaccess" value="Edit" class="check" > <label for="addsheet00001" class="chklabel">Add</label>
                                                                                        </li>
                                                                                        <li>
                                                                                            <input type="checkbox" id="editsheet00002" name="editaccess" value="Edit" class="check" > <label for="editsheet00002"  class="chklabel">Edit</label>
                                                                                        </li>
                                                                                        <li>
                                                                                            <input type="checkbox" id="updatesheet00003" name="updateaccess" value="Edit" class="check" > <label for="updatesheet00003" class="chklabel">View</label>
                                                                                        </li>
                                                                                        <li>
                                                                                            <input type="checkbox" id="deletesheet00004" name="deleteaccess" value="Edit" class="check" > <label for="deletesheet00004" class="chklabel">Delete</label>
                                                                                        </li>
                                                                                    </ul>
                                                                                </div>
                                                                            </div>
                                                                     </div>
                                                                        <div class="clear"></div>
                                                                </div>
                                                                
                                                                <div id="utab6" class="tab-pane fade">
                                                                    <div class="userpermissionbox">
                                                                        <div class="row">
                                                                            <div class="table-responsive">
                                                                                <table class="table table-bordered">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th>Email</th>
                                                                                            <th>Access</th>
                                                                                            <th width="10%">Action</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    
                                                                                    <tbody id="add_access">
                                                                                        <?php if(isset($emp_access)): ?>
                                                                                        <tr>
                                                                                            <td style="vertical-align: middle;">
                                                                                                <input type="text" class="form-control" value="<?php echo e($emp_access->email); ?>" readonly>
                                                                                            </td>
                                                                                            <td class="text-center" style="vertical-align: middle;">
                                                                                                <input type="checkbox" id="email_access" name="access" value="1" <?php if($emp->email_access == 1): ?> <?php echo e('checked'); ?><?php endif; ?>>
                                                                                                <label class="fsc-form-label" for="email_access" style="float:initial;vertical-align: middle;"> </label>
                                                                                                <input type="hidden" id="email_access_id" name="emailaccess_id" value="<?php if($emp_access->email != ''): ?><?php echo e($emp_access->for_whom); ?><?php endif; ?>">
                                                                                                
                                                                                            </td>
                                                                                            <td class="text-center" style="vertical-align: middle;"><a class="btn btn-primary" id="mail_access" style="margin:auto;"><i class="fa fa-plus"></i></a></td>
                                                                                        </tr>
                                                                                        <?php $__currentLoopData = $emp_emai_rights; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key =>$rights): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                                        <tr>
                                                                                            <td style="vertical-align: middle;">
                                                                                                <input type="text" class="form-control" name="accessemaild[]" value="<?php echo e($rights->email); ?>" readonly>
                                                                                                <input type="hidden" id="email_access_id" name="accessemail[]" value="<?php echo e($rights->empemailid); ?>">
                                                                                            </td>
                                                                                            <td class="text-center" style="vertical-align: middle;">
                                                                                                <input type="checkbox" class="emailaccess" id="email_access<?php echo e($key+1); ?>" name="access2[]" value="1" <?php if($rights->access == 1): ?> <?php echo e('checked'); ?><?php endif; ?>>
                                                                                                <label class="fsc-form-label" for="email_access<?php echo e($key+1); ?>" style="float:initial;vertical-align: middle;"> </label>
                                                                                            </td> <!-- aakkiitt -->
                                                                                            <td class="text-center" style="vertical-align: middle;"><a href="<?php echo e(route('employee.removeemailrights',[$rights->id,$emp->id])); ?>" id="remove mail_access" style="margin:auto;"><i class="fa fa-trash"></i></a></td>
                                                                                        </tr>
                                                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                                        
                                                                                        <?php endif; ?>
                                                                                    </tbody>
                                                                                    
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <div class="clear"></div>
                                                                </div>
                                                              </div>
                                                        </div>
												
												
												<div class="form-group" style="margin-top:10px;">
													<label class="control-label col-md-3">Supervisor Name :</label>
													<div class="col-md-6">
														<div class="row">
															<div class="col-md-7">
																<select name="question3" id="question3" class="form-control fsc-input disabledd" readonly="">
																	<option value="">Select</option>
															
																	<?php $__currentLoopData = $super; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sup): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																	<option value="<?php echo e($sup->username); ?>"><?php echo e($sup->firstName.' '.$sup->middleName.' '.$sup->lastName); ?></option>
																	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
																	</select>
															</div>
														</div>
													</div>
												</div>
												<!--<div class="form-group">
													<label class="control-label col-md-3">Access RIghts:</label>
													<div class="col-md-6">
													    <div class="row">
														<div class="col-md-6">
															
																<input type="checkbox" id="technical_support" name="technical_support" value="Technical Support" class="check" <?php if($emp->technical_support =='Technical Support'): ?> checked <?php endif; ?>> <label for="technical_support" style="text-align:center;float:left">Technical Support</label>
														</div>
														<div class="col-md-6">
															
																<input type="checkbox" id="timing_support" name="timing_support" value="Timing Support" class="check" <?php if($emp->timing_support =='Timing Support'): ?> checked <?php endif; ?>><label for="timing_support" style="text-align:center;float:left"> Timing Support</label>
														</div>
														<div class="col-md-6">
															
																<input type="checkbox" id="system_support" name="system_support" value="System Support" class="check" <?php if($emp->system_support =='System Support'): ?> checked <?php endif; ?>><label for="system_support" style="text-align:center;float:left"> System Support</label>
														</div>
														<div class="col-md-6">
															
																<input type="checkbox" id="other_support" name="other_support" value="Other Support" class="check" <?php if($emp->other_support =='Other Support'): ?> checked <?php endif; ?>><label for="other_support" style="text-align:center;float:left"> Other Support</label>
														</div>
														<div class="col-md-6">
														
																<input type="checkbox" id="timesheet" name="timesheet" value="Time Sheet" class="check" <?php if($emp->timesheet =='Time Sheet'): ?> checked <?php endif; ?>>	<label for="timesheet" style="text-align:center;float:left"> Time Sheet</label>
														</div>
													</div>
													</div>
												</div>-->
											</div>
											</div>
										</div>
										<div class="tab-pane fade" id="tab8primary">
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
											    <div class="row">
												<div class="Branch">
													<h1>Employee Rules <?php echo 'aa'.$emp->read;?></h1>
												</div>
												<div class="clear clearfix"></div>
												<div class="responsibility-8">
													<div id="responsibility">
														<ul style="padding-left:0px;"><?php $__currentLoopData = $rules; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rule): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php if($rule->type=='Rules'): ?>
															<li><b><?php echo e($rule->title); ?> : </b>
																<br><?php echo $rule->rules; ?></li><?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
															<li style="list-style:none !important;">
																<div class="checkbox">
																	
																		<input type="checkbox" id="terms" name="terms" value="2" disabled <?php if(empty($emp->read)): ?> <?php else: ?> <?php if($emp->read=='2'): ?> checked <?php endif; ?> <?php endif; ?>> <span class="cr"><i class="cr-icon glyphicon glyphicon-ok" style="color: green;"></i></span>
																		<label style="float:left" for="terms">I read and acknowledge the rules of Financial Service Center and I will follow as per company's rule.</label>
																</div>
															</li>
														</ul>
														<input name="rulesdate" style="width: 160px;border: transparent;background: transparent;" value="<?php if($emp->rulesdate): ?> <?php echo e(date(" F-d-Y ",strtotime($emp->rulesdate))); ?> <?php else: ?>  <?php endif; ?>" class="form-control fsc-input" id="rulesdate" readonly="" type="hidden">
														<p style="text-align:center;font-weight:bold"><?php if($emp->rulesdate): ?> Client Signature &nbsp;&nbsp;&nbsp; <?php echo e(date("F-d-Y",strtotime($emp->rulesdate))); ?> <?php else: ?> <?php endif; ?></p>
													</div>
												</div>
											</div>
											</div>
										</div>
										<div class="tab-pane fade" id="tab9primary">
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
												<div class="row">
												<div class="Branch">
													<h1>EE Responsibility</h1>
												</div>
												<div class="form-group " id="">
												<label class="control-label col-md-3">Service : </label>
												<div class="col-lg-6 col-md-8 ac" >
													<div class="row">
													    <div class="col-md-12">
													       <?php $str1=$emp->emp_service1; $splittedstring1=explode(",",$str1);?>
														   <select name="emp_service1[]" id="emp_service1" multiple="multiple" class="js-example-tags  form-control fsc-input empservice1">
															<option value=''>---Select Service---</option>
															<?php $__currentLoopData = $service1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
															<option value='<?php echo e($cate->id); ?>' <?php $__currentLoopData = $splittedstring1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php if($value ==$cate->id): ?> selected <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> ><?php echo e($cate->typeofservice); ?></option>
															<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
															</select>
														</div>
													</div>
												</div>
											</div>
											<div class="form-group " id="">
												<label class="control-label col-md-3">Taxation Service : </label>
												<div class="col-lg-6 col-md-8 ac">
													<div class="row">
													     <div class="col-md-12">
														    <?php $str1s=$emp->emp_service2; $splittedstring1s=explode(",",$str1s);?>
														   <select name="emp_service2[]" id="emp_service_2" multiple="multiple" class="js-example-tags  form-control fsc-input empservice1">
															<option value=''>---Select Service---</option>
															<?php $__currentLoopData = $service2; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
															<option value='<?php echo e($cate->id); ?>' <?php $__currentLoopData = $splittedstring1s; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php if($value ==$cate->id): ?> selected <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> ><?php echo e($cate->title); ?></option>
															<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
															</select>
													
														</div>
													</div>
												</div>
											</div>
											
											<div class="Branch">
													<h1>Checking (Supervisor) </h1>
												</div>
												<div class="form-group " id="business_catagory_name_2">
												<label class="control-label col-md-3">Service : </label>
												<div class="col-lg-6 col-md-8 ac">
													<div class="row">
													     <div class="col-md-12">
														    <?php $accstr1=$emp->acc_service_1; $accsplittedstring1=explode(",",$accstr1);?>
														    <select name="acc_service_1[]" id="acc_service_1" multiple="multiple" class="js-example-tags  form-control fsc-input empservice1">
															<option value=''>---Select Service---</option>
															<?php $__currentLoopData = $accservice1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
															<option value='<?php echo e($cate->id); ?>' <?php $__currentLoopData = $accsplittedstring1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php if($value ==$cate->id): ?> selected <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> ><?php echo e($cate->typeofservice); ?></option>
															<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
															</select>
													
														</div>
													</div>
												</div>
											</div>
											<div class="form-group " id="business_catagory_name_2">
												<label class="control-label col-md-3">Taxation Service : </label>
												<div class="col-lg-6 col-md-8 ac" >
													<div class="row">
													    <div class="col-md-12">
														    <?php $accserstr1=$emp->acc_service_2; $accsersplittedstring1=explode(",",$accserstr1);?>
														    <select name="acc_service_2[]" id="acc_service_2" multiple="multiple" class="js-example-tags  form-control fsc-input empservice1">
        														<option value=''>---Select Service---</option>
        														<?php $__currentLoopData = $accservice2; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        														<option value='<?php echo e($cate->id); ?>' <?php $__currentLoopData = $accsersplittedstring1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php if($value ==$cate->id): ?> selected <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> ><?php echo e($cate->title); ?></option>
        														<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
															</select>
														</div>
													</div>
												</div>
											</div>
												
												<div class="Branch">
													<h1>Work Responsibility</h1>
												</div>
												
												<div class="responsibility">
													<div class="form-group">
														<div class="col-md-12">
															<table id="example" class="table table-striped table-bordered customers" style="width:100%">
																<thead>
																	<tr>
																		<th>No.</th>
																		<th style="width:80%;">Responsibility</th>
																		<th style="width:20%;">Checked Date</th>
																		<th style="width:20%;">Checked</th>
																	</tr>
																</thead>
																<tbody><?php $__currentLoopData = $rules; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rule): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php if($rule->type=='Resposibilty' && $rule->employee_id==$emp->id): ?>
																	<tr>
																		<td><?php echo e($loop->index+1); ?></td>
																		<td><?php echo $rule->rules; ?></td>
																		<td><?php if($rule->status=='2'): ?> checked <?php else: ?> Uncheck <?php endif; ?></td><?php if($rule->rulesdate): ?> <?php echo e(date("F-d-Y",strtotime($rule->rulesdate))); ?> <?php else: ?> <?php endif; ?></td>
																	</tr><?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></tbody>
															</table>
														</div>
													</div>
												</div>
											</div>
											</div>
										</div>
									</div>
								</div>
								<div class="card-footer">
									<div class="">
									    <label class="control-label col-md-3"> </label>
										<div class="col-xs-2" style="width:155px;"><?php if($emp->clienttype=='client'): ?> <?php else: ?>
											<input class="btn_new_save btn-primary" type="submit" value="Save"><?php endif; ?></div>
										<div class="col-xs-2" style="width:155px">	<a class="btn_new_cancel" href="<?php echo e(url('fac-Bhavesh-0554/employee')); ?>">Cancel</a> 
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>


    

    <!--Modal Conversation Update Start-->
    <form method="post" class="form-horizontal" enctype="multipart/form-data" action="<?php echo e(route('employee.updatecoversation')); ?>">
    <?php echo e(csrf_field()); ?>

                                                    
    
        <div id="conversationModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Conversation Sheet</h4>
      </div>
      <div class="modal-body">
          <div style="max-width:700px; margin:0px auto;">
       <div class="row">
           <div class="col-md-4">
               <div class="form-group">
               <div class="row">
                   <label class="control-label col-md-5 text-right"><strong>Date:</strong></label>
                   <div class="col-md-7">
                    <!--<input type="text" class="form-control effective_date2"/>-->
                    <input type="hidden" name="CovID" id="CovID" class="form-control">
                    <input type="text" name="date" id="creattiondate1" class="form-control"  placeholder="Date" readonly>
                   </div>
               </div>
               </div>
           </div>
            <div class="col-md-4">
                <div class="form-group">
                <div class="row">
               <label class="control-label col-md-5 text-right" ><strong>Day:</strong></label>
               <div class="col-md-7">
               <input type="text" name="day" id="day1" class="form-control" placeholder="Day" readonly>
               </div>
               </div>
               </div>
           </div>
            <div class="col-md-4">
                <div class="form-group">
                <div class="row">
                   <label class="control-label col-md-3"><strong>Time:</strong></label>
                   <div class="col-md-8">
                        <input type="text" name="time" id="time1" class="form-control"  placeholder="Time" readonly>
                   </div>
               </div>
               </div>
           </div>
       </div>
       
       <div class="form-group<?php echo e($errors->has('type_user') ? ' has-error' : ''); ?>">
            <div class="row">
            <label class="control-label col-md-3 text-right" style="width:100px;"><strong>Select:</strong></label>
                <div class="col-md-5">
                    <div class="">
                    <select class="form-control fsc-input type_user" name="type_user" id="type_user1" required>
                        <option>Select</option>
                        <option>Client</option>
                        <option>EE-User</option>
                        <option>Vendor</option>
                        <option>Other</option>
                    </select>
                    </div>
                    <?php if($errors->has('type_user')): ?>
                        <span class="help-block">
                        <strong><?php echo e($errors->first('type_user')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>
              
            </div>
       </div>
       
       <div class="form-group clientid2" id="clientid" style="display:none;">
            <div class="row">
            <label class="control-label col-md-3 text-right" style="width:100px;"><strong>Client :</strong></label>
                <div class="col-md-5">
                    <select class="form-control" name="clientid" id="clientid1">
                        <option>Select</option>
                        <?php $__currentLoopData = $listclient; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $client): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($client->id); ?>"><?php echo e($client->first_name); ?> <?php echo e($client->middle_name); ?> <?php echo e($client->last_name); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>      
                    </select>
                </div>
               
            </div>
       </div>
       
       <div class="form-group employeeuserid2" id="employeeuserid" style="display:none;">
            <div class="row">
            <label class="control-label col-md-3 text-right" style="width:100px;"><strong>Employee/User:</strong></label>
                <div class="col-md-5">
                    <select class="form-control" name="employeeuserid" id="employeeuserid1">
                        <option>Select</option>
                        <?php $__currentLoopData = $listemployeeuser; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $employee): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($employee->id); ?>"><?php echo e($employee->firstName); ?> <?php echo e($employee->middleName); ?> <?php echo e($employee->lastName); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>      
                    </select>
                </div>
               
            </div>
       </div>
       
       <div class="form-group vendorid2" id="vendorid" style="display:none;">
            <div class="row">
            <label class="control-label col-md-3 text-right" style="width:100px;"><strong>Vendor:</strong></label>
                <div class="col-md-5">
                    <select class="form-control" name="vendorid" id="vendorid1">
                        <option>Select</option>
                        <?php $__currentLoopData = $listvendoe; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vendoe): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($vendoe->id); ?>"><?php echo e($vendoe->firstName); ?> <?php echo e($vendoe->middleName); ?> <?php echo e($vendoe->lastName); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>      
                    </select>
                </div>
               
            </div>
       </div>
       
       <div class="form-group otherid2" id="otherid" style="display:none;">
           <div class="row">
                <label class="control-label col-md-3" style="width:100px; text-align:right; padding-left:0px!important;"><strong>Other :</strong></label>
                   <div class="col-md-5">
                        <input type="text" class="form-control" name="otherid" id="otherid1">
                   </div>
                   
           </div>
       </div>
       
       <div class="form-group<?php echo e($errors->has('conrelatedname') ? ' has-error' : ''); ?>">
           <div class="row">
                <label class="control-label col-md-3" style="width:100px; text-align:right; padding-left:0px!important;"><strong>Related to:</strong></label>
                   <div class="col-md-5">
                       <div class="">
                        <select class="form-control" name="conrelatedname" id="conrelatedname1" required>
                              <option>Select</option>
                              <?php $__currentLoopData = $relatedNames; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $relatedNames): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value="<?php echo e($relatedNames->id); ?>"><?php echo e($relatedNames->relatednames); ?></option>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                        </div>
                        <?php if($errors->has('conrelatedname')): ?>
                            <span class="help-block">
                            <strong><?php echo e($errors->first('conrelatedname')); ?></strong>
                            </span>
                        <?php endif; ?>
                   </div>
                   
           </div>
       </div>
        

        
       
       
        <div class="form-group">
            <div class="row">
                <label class="control-label col-md-3" style="width:100px; text-align:right; padding-left:0px!important;"><strong>Conversation:</strong></label>
                <div class="col-md-9">
                    <textarea rows="3" cols="12" class="form-control" name="condescription" id="condescription1"> </textarea>
               </div>
            </div>
       </div>
       
        <div class="form-group">
           <div class="row">
                <label class="control-label col-md-3" style="width:100px; text-align:right; padding-left:0px!important;"><strong>Note</strong></label>
                   <div class="col-md-9">
                  <textarea rows="3" cols="12" class="form-control" name="connotes" id="connotes1"> </textarea>
                   </div>
           </div>
       </div>
       
        <div class="form-group">
            <div class="row">
            <label class="control-label col-md-3" style="width:100px; text-align:right; padding-left:0px!important;"><strong>&nbsp;</strong></label>
                <div class="col-md-2">
                    <input class="btn_new_save btn-primary1" id="recInsert" type="submit" name="submit" value="Save">
			    </div>
				<div class="col-md-2">
					<a class="btn_new_cancel" href="https://financialservicecenter.net/fac-Bhavesh-0554">Cancel</a>
               </div>
            </div>
       </div>
       
       
       </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
            </div>
        </div>                                                 
                                                    
    </form>
    <!--Modal Conversation Update End-->

<div id="addPay" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Add Pay</h4>
			</div>
			<div class="modal-body">
				<form>
				    <div class="row">
				        <label class="control-label col-md-5 text-right"><strong>Pay Methods :</strong></label>
				        <div class="col-md-5">
                            <div class="form-group">
                                <select class="form-control">
                                    <option>Salary</option>
                                    <option>Hourly</option>
                                </select>
                           </div>
                       </div>
				    </div>
				    <div class="row">
				        <label class="control-label col-md-5 text-right"><strong>Pay Duration :</strong></label>
				        <div class="col-md-5">
                            <div class="form-group">
                                <select class="form-control">
                                    <option>Weekly</option>
                                    <option>Bi-Weekly</option>
                                    <option>Semi-Monthly</option>
                                    <option>Monthly</option>
                                    
                                </select>
                           </div>
                       </div>
				    </div>
				    <div class="row">
				        <label class="control-label col-md-5 text-right"><strong>Pay Rate :</strong></label>
				        <div class="col-md-5">
                            <div class="form-group">
                                <input type="date" class="form-control" id="" name="">
                           </div>
                       </div>
				    </div>
				    <div class="row">
				        <label class="control-label col-md-5 text-right"><strong>Effective Date :</strong></label>
				        <div class="col-md-5">
                            <div class="form-group">
                                <input type="date" class="form-control" id="" name="">
                           </div>
                       </div>
				    </div>
				    <div class="row">
				        <label class="control-label col-md-5 text-right"><strong>Note :</strong></label>
				        <div class="col-md-5">
                            <div class="form-group">
                                <textarea rows="2" class="form-control" id="" name=""></textarea>
                           </div>
                       </div>
				    </div>
				    <div class="col-md-5"></div>
                <div class="col-md-2">
                    <input class="btn_new_save btn-primary1" id="" type="submit" name="submit" value="Save">
			    </div>
				<div class="col-md-2">
					<a class="btn_new_cancel" href="https://financialservicecenter.net/fac-Bhavesh-0554">Cancel</a>
               </div>
				</form>
			</div>
			<div class="modal-footer">
			    
			</div>
		</div>
	</div>
</div>

<div id="addReview" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Add Review</h4>
			</div>
			<div class="modal-body">
				<form>
				    <div class="row">
				        <label class="control-label col-md-5 text-right"><strong>Review Date :</strong></label>
				        <div class="col-md-5">
                            <div class="form-group">
                                <input type="date" class="form-control" id="" name="">
                           </div>
                       </div>
				    </div>
				    <div class="row">
				        <label class="control-label col-md-5 text-right"><strong>Next Review Review :</strong></label>
				        <div class="col-md-5">
                            <div class="form-group">
                                <input type="text" class="form-control" id="" name="">
                           </div>
                       </div>
				    </div>
				    <div class="row">
				        <label class="control-label col-md-5 text-right"><strong>Next Review Date :</strong></label>
				        <div class="col-md-5">
                            <div class="form-group">
                                <input type="date" class="form-control" id="" name="">
                           </div>
                       </div>
				    </div>
				    <div class="row">
				        <label class="control-label col-md-5 text-right"><strong>Next Pay Rate :</strong></label>
				        <div class="col-md-5">
                            <div class="form-group">
                                <input type="text" class="form-control" id="" name="">
                           </div>
                       </div>
				    </div>
				    <div class="col-md-5"></div>
                <div class="col-md-2">
                    <input class="btn_new_save btn-primary1" id="" type="submit" name="submit" value="Save">
			    </div>
				<div class="col-md-2">
					<a class="btn_new_cancel" href="https://financialservicecenter.net/fac-Bhavesh-0554">Cancel</a>
               </div>
				</form>
			</div>
			<div class="modal-footer">
			    
			</div>
		</div>
	</div>
</div>

<div id="addWithhold" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Add Review</h4>
			</div>
			<div class="modal-body">
				<form>
				    <div class="row">
				        <label class="control-label col-md-5 text-right"><strong>Filling Status :</strong></label>
				        <div class="col-md-5">
                            <div class="form-group">
                                <select class="form-control">
                                    <option value="Single">Single</option>
									<option value="Married File Jointly" selected="">Married File Jointly</option>
									<option value="Married File Separately">Married File Separately</option>
									<option value="Head of Household">Head of Household</option>
                                </select>
                           </div>
                       </div>
				    </div>
				    <div class="row">
				        <label class="control-label col-md-5 text-right"><strong>Federal Claim :</strong></label>
				        <div class="col-md-5">
                            <div class="form-group">
                                <input type="text" class="form-control" id="" name="">
                           </div>
                       </div>
				    </div>
				    <div class="row">
				        <label class="control-label col-md-5 text-right"><strong>Federal Additional Withholding :</strong></label>
				        <div class="col-md-5">
                            <div class="form-group">
                                <input type="text" class="form-control" id="" name="">
                           </div>
                       </div>
				    </div>
				    <div class="row">
				        <label class="control-label col-md-5 text-right"><strong></strong></label>
				        <div class="col-md-5">
                            <div class="form-group">
                                <input type="file" class="form-control" id="" name="">
                           </div>
                       </div>
				    </div>
				    <div class="row">
				        <label class="control-label col-md-5 text-right"><strong>State Claim :</strong></label>
				        <div class="col-md-5">
                            <div class="form-group">
                                <input type="text" class="form-control" id="" name="">
                           </div>
                       </div>
				    </div>
				    <div class="row">
				        <label class="control-label col-md-5 text-right"><strong>State Additional Withholding :</strong></label>
				        <div class="col-md-5">
                            <div class="form-group">
                                <input type="text" class="form-control" id="" name="">
                           </div>
                       </div>
				    </div>
				    <div class="row">
				        <label class="control-label col-md-5 text-right"><strong></strong></label>
				        <div class="col-md-5">
                            <div class="form-group">
                                <input type="file" class="form-control" id="" name="">
                           </div>
                       </div>
				    </div>
				    <div class="row">
				        <label class="control-label col-md-5 text-right"><strong>Local Claim :</strong></label>
				        <div class="col-md-5">
                            <div class="form-group">
                                <input type="text" class="form-control" id="" name="">
                           </div>
                       </div>
				    </div>
				    <div class="row">
				        <label class="control-label col-md-5 text-right"><strong>Local Additional Withholding :</strong></label>
				        <div class="col-md-5">
                            <div class="form-group">
                                <input type="text" class="form-control" id="" name="">
                           </div>
                       </div>
				    </div>
				    <div class="row">
				        <label class="control-label col-md-5 text-right"><strong></strong></label>
				        <div class="col-md-5">
                            <div class="form-group">
                                <input type="file" class="form-control" id="" name="">
                           </div>
                       </div>
				    </div>
				    <div class="col-md-5"></div>
                <div class="col-md-2">
                    <input class="btn_new_save btn-primary1" id="" type="submit" name="submit" value="Save">
			    </div>
				<div class="col-md-2">
					<a class="btn_new_cancel" href="https://financialservicecenter.net/fac-Bhavesh-0554">Cancel</a>
               </div>
				</form>
			</div>
			<div class="modal-footer">
			    
			</div>
		</div>
	</div>
</div>

    <!--Modal Notes Update Start-->
    <div id="notesModal" class="modal fade" role="dialog">
        <div class="modal-dialog" style="width:850px;">
            <form method="post" class="form-horizontal" enctype="multipart/form-data" action="<?php echo e(route('employee.updatenotes')); ?>">
                <?php echo e(csrf_field()); ?>

                                                    
    
                <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> Note</h4>
      </div>
      <div class="modal-body">
          <div style="max-width:700px; margin:0px auto;">
       <div class="row mt-3" style="margin-top:20px;">
           <div class="col-md-4">
               <div class="form-group">
               <div class="row">
                   <label class="control-label col-md-5 text-right"  style="padding-right:28px!important;" ><strong>Date:</strong></label>
                   <div class="col-md-6" style="padding-left:12px!important;">
                    <!--<input type="text" class="form-control effective_date2"/>-->
                    <input type="hidden" name="NoteID" id="NoteID"> 
                    <input type="text" name="notedate" id="date2" class="form-control" value="<?php echo e(date('m-d-Y')); ?>" placeholder="Date" readonly>
                   </div>
               </div>
               </div>
           </div>
            <div class="col-md-4">
                <div class="form-group">
                <div class="row">
               <label class="control-label col-md-4 text-right" ><strong>Day:</strong></label>
               <div class="col-md-5">
               <input type="text" name="noteday" id="day2" class="form-control" placeholder="Day" value="<?php echo e(date('l')); ?>" readonly>
               </div>
               </div>
               </div>
           </div>
            <div class="col-md-4">
                <div class="form-group">
                <div class="row">
                   <label class="control-label col-md-3"><strong>Time:</strong></label>
                   <div class="col-md-5">
                        <input type="text" name="notetime" id="time2" class="form-control" value="<?php echo e(date("g:i a")); ?>" placeholder="Time" readonly>
                   </div>
               </div>
               </div>
           </div>
       </div>
               
       <div class="form-group">
            <div class="row">
            <label class="control-label col-md-3 text-right" style="width:110px;"><strong>Select:</strong></label>
                <div class="col-md-5">
                    <div class="">
                    <select class="form-control fsc-input notetype_user" name="notetype_user" id="notetype_user1" required>
                        <option>Select</option>
                        <option>Client</option>
                        <option>EE-User</option>
                        <option>Vendor</option>
                        <option>Other</option>
                    </select>
                    </div>
                </div>
              
            </div>
       </div>
       
        <div class="form-group noteclientid2" id="noteclientid" style="display:none;">
            <div class="row">
            <label class="control-label col-md-3 text-right" style="width:100px;"><strong>Client :</strong></label>
                <div class="col-md-5">
                    <select class="form-control" name="noteclientid" id="noteclientid1">
                        <option>Select</option>
                        <?php $__currentLoopData = $listclient; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $client): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($client->id); ?>"><?php echo e($client->first_name); ?> <?php echo e($client->middle_name); ?> <?php echo e($client->last_name); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>      
                    </select>
                </div>
               
            </div>
       </div>
       
       <div class="form-group noteemployeeuserid2" id="noteemployeeuserid" style="display:none;">
            <div class="row">
            <label class="control-label col-md-3 text-right" style="width:100px;"><strong>Employee/User:</strong></label>
                <div class="col-md-5">
                    <select class="form-control" name="noteemployeeuserid" id="noteemployeeuserid1">
                        <option>Select</option>
                        <?php $__currentLoopData = $listemployeeuser; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $employee): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($employee->id); ?>"><?php echo e($employee->firstName); ?> <?php echo e($employee->middleName); ?> <?php echo e($employee->lastName); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>      
                    </select>
                </div>
               
            </div>
       </div>
       
       <div class="form-group notevendorid2" id="notevendorid" style="display:none;">
            <div class="row">
            
            <label class="control-label col-md-3 text-right" style="width:110px;"><strong>Vendor :</strong></label>
                <div class="col-md-5">
                    <select class="form-control" name="notevendorid" id="notevendorid1">
                        <option>Select</option>
                        <?php $__currentLoopData = $listvendoe; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vendoe): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($vendoe->id); ?>"><?php echo e($vendoe->firstName); ?> <?php echo e($vendoe->middleName); ?> <?php echo e($vendoe->lastName); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>      
                    </select>
                </div>
               
            </div>
       </div>
       
       <div class="form-group noteotherid2" id="noteotherid" style="display:none;">
           <div class="row">
                
                <label class="control-label col-md-3 text-right" style="width:110px;"><strong>Other :</strong></label>
                   <div class="col-md-5">
                        <input type="text" class="form-control" name="noteotherid" id="noteotherid1">
                   </div>
                   
           </div>
       </div>
       
       <div class="form-group<?php echo e($errors->has('notesrelated') ? ' has-error' : ''); ?>">
           <div class="row">
                <label class="control-label col-md-3 text-right" style="width:110px;"><strong>Related to :</strong></label>
                   <div class="col-md-5">
                       <div class="">
                        <select class="form-control" name="notenotesrelatedname" id="noterelated1" required>
                              <option>Select</option>
                              <?php $__currentLoopData = $NotesNames; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $notesRelated): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value="<?php echo e($notesRelated->id); ?>"><?php echo e($notesRelated->notesrelated); ?></option>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                        </div>
                        <?php if($errors->has('notesrelated')): ?>
                            <span class="help-block">
                            <strong><?php echo e($errors->first('notesrelated')); ?></strong>
                            </span>
                        <?php endif; ?>
                   </div>
                   <div class="col-md-2">
                       <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#basicExampleModalNotes" class="redius"><i class="fa fa-plus"></i></a>
                   </div>
           </div>
       </div>

       
        <div class="form-group">
            <div class="row">
                <label class="control-label col-md-3" style="width:110px; text-align:right; padding-left:0px!important;"><strong>Types of Note:</strong></label>
                <div class="col-md-5">
                    <select class="form-control" name="notesrelatedcat" id="notesrelatedcat1" required>
                        <option>Permenant Note</option>
                        <option>Temporary  Note</option>
                    </select>
               </div>
            </div>
       </div>
       
        <div class="form-group">
           <div class="row">
                <label class="control-label col-md-3" style="width:110px; text-align:right; padding-left:0px!important;"><strong>Note:</strong></label>
                   <div class="col-md-9" style="padding-right:0px;">
                  <textarea rows="3" cols="12" class="form-control" name="notes" id="notes1"> </textarea>
                   </div>
           </div>
       </div>
       
        <div class="form-group">
            <div class="row">
            <label class="control-label col-md-3" style="width:110px; text-align:right; padding-left:0px!important;"><strong>&nbsp;</strong></label>
                <div class="col-md-2">
                    <input class="btn_new_save btn-primary1" id="recInsert2" type="submit" name="submit" value="Save">
			    </div>
				<div class="col-md-2">
					<a class="btn_new_cancel" href="https://financialservicecenter.net/fac-Bhavesh-0554">Cancel</a>
               </div>
            </div>
       </div>
       
       
       </div>
      </div>
      <div class="modal-footer">
       <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
      </div>
    </div>
                                                    
            </form>
        </div>
    </div>
    <!--Modal Notes Update End-->





<script src="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/d004434a5ff76e7b97c8b07c01f34ca69e635d97/src/js/bootstrap-datetimepicker.js"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>-->
<!-- copy of input fields group -->
<div class="form-group fieldGroupCopy" style="display: none;">
	<div>
		<input type="hidden" class="form-control fsc-input" name="employee[]" id="employee" value="">
		<div class="form-group">
			<label class="control-label col-md-3">Pay Rate :</label>
			<div class="col-md-6">
				<div class="row">
					<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
						<div class="dropdown" style="margin-top: 1%;">
							<input name="pay_scale[]" value="" type="text" id="pay_scale" maxlength="10" class="form-control pay_scale" />
						</div>
					</div>
					<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
						<label class="fsc-form-label">Effective Date :</label>
					</div>
					<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
						<input name="effective_date[]" type="text" id="effective_date" class="form-control date1" />
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Note :</label>
			<div class="col-md-6">
				<textarea id="fields" name="fields[]" class="form-control fsc-input"></textarea>
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-12"><a href="javascript:void(0)" class="btn btn-danger remove pull-right"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
			</div>
		</div>
	</div>


    

<script>
    $(document).ready(function()
    {
        $('.js-example-tags').select2({
    tags: true,
    tokenSeparators: [",", " "]
});

        $('.type_user').on('change', function()
        {   
             //otherid,clientid,employeeuserid,vendorid
             //Client,EE-User,Vendor,Other
             var thisss=$('.type_user').val();
             if(thisss =='Client')
             {
                 $('#clientid').show();
                 $('#employeeuserid').hide();
                 $('#vendorid').hide();
                 $('#otherid').hide();
             }
             else if(thisss =='EE-User')
             {
                 $('#clientid').hide();
                 $('#employeeuserid').show();
                 $('#vendorid').hide();
                 $('#otherid').hide();
             }
             else if(thisss =='Vendor')
             {
                 $('#clientid').hide();
                 $('#employeeuserid').hide();
                 $('#vendorid').show();
                 $('#otherid').hide();
             }
             else if(thisss =='Other')
             {
                 $('#clientid').hide();
                 $('#employeeuserid').hide();
                 $('#vendorid').hide();
                 $('#otherid').show();
             }
        });
        
        
        $('.notetype_user').on('change', function()
        {   
             //otherid,clientid,employeeuserid,vendorid
             //Client,EE-User,Vendor,Other
             var thisss=$('.notetype_user').val();
             if(thisss =='Client')
             {
                 $('#noteclientid').show();
                 $('#noteemployeeuserid').hide();
                 $('#notevendorid').hide();
                 $('#noteotherid').hide();
             }
             else if(thisss =='EE-User')
             {
                 $('#noteclientid').hide();
                 $('#noteemployeeuserid').show();
                 $('#notevendorid').hide();
                 $('#noteotherid').hide();
             }
             else if(thisss =='Vendor')
             {
                 $('#noteclientid').hide();
                 $('#noteemployeeuserid').hide();
                 $('#notevendorid').show();
                 $('#noteotherid').hide();
             }
             else if(thisss =='Other')
             {
                 $('#noteclientid').hide();
                 $('#noteemployeeuserid').hide();
                 $('#notevendorid').hide();
                 $('#noteotherid').show();
             }
        });
        
    });
</script>	
<script type="text/javascript">
    
    $(".notesID").click(function () 
        {
            var ids = $(this).attr('data-id');
            //alert(ids);
            $("#notesid").val(ids);
            $('#notesModal').modal('show');
            //console.log(ids);           
            $.get('<?php echo URL::to('getNotesemp'); ?>?documentid='+ids, function(data)
            {  
                //console.log(data);exit;
                if(data == "")
                {
                    
                }
                else
                {
                    //console.log(data.id);exit;
                    $('#NoteID').val(data.id);
                    $('#creattiondate1').val(data.creattiondate);
                    $('#noteday1').val(data.noteday);
                    $('#notetime1').val(data.notetime);
                    $('#notetype_user1').val(data.notetype_user);
                    $('#noteclientid1').val(data.noteclientid);
                    $('#noteemployeeuserid1').val(data.noteemployeeuserid);
                    $('#notevendorid1').val(data.notevendorid);
                    $('#noteotherid1').val(data.noteotherid);
                    $('#noterelated1').val(data.noterelated);
                    $('#notesrelatedcat1').val(data.notesrelatedcat);
                    $('#notes1').val(data.notes);
                    
                    
                       
                                 var thisss=$('#notetype_user1').val();
                                 if(thisss =='Client')
                                 {
                                     $('.noteclientid2').show();
                                     $('.noteemployeeuserid2').hide();
                                     $('.notevendorid2').hide();
                                     $('.noteotherid2').hide();
                                 }
                                 else if(thisss =='EE-User')
                                 {
                                     $('.noteclientid2').hide();
                                     $('.noteemployeeuserid2').show();
                                     $('.notevendorid2').hide();
                                     $('.noteotherid2').hide();
                                 }
                                 else if(thisss =='Vendor')
                                 {
                                     $('.noteclientid2').hide();
                                     $('.noteemployeeuserid2').hide();
                                     $('.notevendorid2').show();
                                     $('.noteotherid2').hide();
                                 }
                                 else if(thisss =='Other')
                                 {
                                     $('.noteclientid2').hide();
                                     $('.noteemployeeuserid2').hide();
                                     $('.notevendorid2').hide();
                                     $('.noteotherid2').show();
                                 }
                    
                }
                

                
            });
        });

    $(".conversationID").click(function () 
        {
            var ids = $(this).attr('data-id');
            //alert(ids);
            $("#conversationid").val(ids);
            $('#conversationModal').modal('show');
            //console.log(ids);           
            $.get('<?php echo URL::to('getConversationdataemp'); ?>?documentid='+ids, function(data)
            {  
                //console.log(data);exit;
                if(data == "")
                {
                    
                }
                else
                {
                    //console.log(data.id);exit;
                    $('#CovID').val(data.id);
                    $('#creattiondate1').val(data.creattiondate);
                    $('#day1').val(data.day);
                    $('#time1').val(data.time);
                    $('#type_user1').val(data.type_user);
                    $('#clientid1').val(data.clientid);
                    $('#employeeuserid1').val(data.employeeuserid);
                    $('#vendorid1').val(data.vendorid);
                    $('#otherid1').val(data.otherid);
                    $('#conrelatedname1').val(data.conrelatedname);
                    $('#condescription1').val(data.condescription);
                    $('#connotes1').val(data.connotes);
                    
                    
                       
                                 var thisss=$('#type_user1').val();
                                 if(thisss =='Client')
                                 {
                                     $('.clientid2').show();
                                     $('.employeeuserid2').hide();
                                     $('.vendorid2').hide();
                                     $('.otherid2').hide();
                                 }
                                 else if(thisss =='EE-User')
                                 {
                                     $('.clientid2').hide();
                                     $('.employeeuserid2').show();
                                     $('.vendorid2').hide();
                                     $('.otherid2').hide();
                                 }
                                 else if(thisss =='Vendor')
                                 {
                                     $('.clientid2').hide();
                                     $('.employeeuserid2').hide();
                                     $('.vendorid2').show();
                                     $('.otherid2').hide();
                                 }
                                 else if(thisss =='Other')
                                 {
                                     $('.clientid2').hide();
                                     $('.employeeuserid2').hide();
                                     $('.vendorid2').hide();
                                     $('.otherid2').show();
                                 }
                    
                }
                

                
            });
        });

</script>
	<script>
	    (function($, window, document, undefined){
                                                $(".review_day_1").blur(function(){
                                                   var date = new Date($(".review_date_1").val()),
                                                       days = parseInt($(".review_day_1").val(), 10);
                                                    
                                                    if(!isNaN(date.getTime())){
                                                        date.setDate(date.getDate() + days);
                                                        $(".next_review_date_1").val(date.toInputFormat());
                                                    } else {
                                                        alert("Invalid Date");  
                                                    }
                                                });
                                                
                                                
                                                //From: http://stackoverflow.com/questions/3066586/get-string-in-yyyymmdd-format-from-js-date-object
                                                Date.prototype.toInputFormat = function() {
                                                   var yyyy = this.getFullYear().toString();
                                                   var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
                                                   var dd  = this.getDate().toString();
                                                //   return yyyy + "-" + (mm[1]?mm:"0"+mm[0]) + "-" + (dd[1]?dd:"0"+dd[0]); // padding
                                                   return (mm[1]?mm:"0"+mm[0]) + "/" + (dd[1]?dd:"0"+dd[0]) + "/" + yyyy; // padding
                                                };
                                            })(jQuery, this, document);
	</script>
	<script>
		$(".date1").datepicker({
			autoclose: true,
	format: "mm/dd/yyyy",
			//endDate: "today"
		});
		$(".paydate").datepicker({
			autoclose: true,
	format: "mm/dd/yyyy",
			//endDate: "today"
		});
		
		jQuery.fn.getNum = function() {
			var val = $.trim($(this).val());
			if(val.indexOf(',') > -1) {
			val = val.replace(',', '.');
			}
			var num = parseFloat(val);
			var num = num.toFixed(2);
			if(isNaN(num)) {
			num = '';
			}
			return num;
		}
		$(function() {
			$('.pay_scale').blur(function() {
			$(this).val('$' + $(this).getNum());
			});
			
			$('.pay_scale_1').blur(function() {
			$(this).val('$' + $(this).getNum());
			
			});
			
			$('.fedwh_1').blur(function() {
			$(this).val('$' + $(this).getNum());
			
			});
			$('.stwh_1').blur(function() {
			$(this).val('$' + $(this).getNum());
			
			});
			$('.localwh_1').blur(function() {
			$(this).val('$' + $(this).getNum());
			
			});
			$('.pay_revicw_scale_1').blur(function() {
			$(this).val('$' + $(this).getNum());
			
			});
			
				$(".review_date_1").datepicker({
			autoclose: true,
	format: "mm/dd/yyyy",
			//endDate: "today"
		});
		
		$('.pay_review_scale_1').blur(function() {
												                	    
                        $(this).val('$' + $(this).getNum());
                                                            			});
                                                            		
		

		});
	</script>
</div>
<script>
	///	$("#telephoneNo1").mask("(999) 999-9999");
	//	$(".ext").mask("99999");
	//	$("#ext1").mask("99999");
	//	$("#ext2").mask("99999");
	//	$("#eext1").mask("99999");
	///	$("#eext2").mask("99999");
	///	$("#telephoneNo2").mask("(999) 999-9999");
	//	$("#mobile_no").mask("(999) 999-9999");
	//	$("#fax").mask("(999) 999-9999");
	//	$("#etelephone2").mask("(999) 999-9999");
		//$("#etelephone1").mask("(999) 999-9999");
		$("#computer_ip").mask("999.999.999.999");
</script>
<script>
	var date = $('#ext1').val(<?php echo e($emp->ext1); ?>);
	$('#telephoneNo1Type').on('change', function() {
	if(this.value=='Office' || this.value=='Work'){
	document.getElementById('ext1').removeAttribute('readonly');
	$('#ext1').val(<?php echo e($emp->ext1); ?>);
	}
	else{
	document.getElementById('ext1').readOnly =true;
	$('#ext1').val('');
	}
	})
</script>
<script>
	var dat1 = $('#ext2').val(<?php echo e($emp->ext2); ?>);
	$('#telephoneNo2Type').on('change', function() {
	if(this.value=='Office')
	{
	document.getElementById('ext2').removeAttribute('readonly');
	$('#ext2').val(<?php echo e($emp->ext2); ?>); 
	}
	else
	{
	document.getElementById('ext2').readOnly =true;
	$('#ext2').val('');
	}
	})
</script>
<script>
	$('#eteletype2').on('change', function() {
	if(this.value=='Office' || this.value=='Work')
	{
	$('#eext2').val(<?php echo e($emp->eext2); ?>);
	document.getElementById('eext2').removeAttribute('readonly');
	}
	else
	{
	document.getElementById('eext2').readOnly =true;
	$('#eext2').val('');
	}
	})
</script>
<script>
	var dat2 = $('#eext1').val(<?php echo e($emp->eext1); ?>);
	$('#eteletype1').on('change', function() {
	if(this.value=='Office' || this.value=='Work')
	{
	document.getElementById('eext1').removeAttribute('readonly');
	$('#eext1').val(<?php echo e($emp->eext1); ?>);
	}
	else
	{
	document.getElementById('eext1').readOnly =true;
	$('#eext1').val('');
	}
	})
</script>
<script>
	$('.date1').mask("99/99/9999", {placeholder: 'mm/dd/yyyy' });
</script>
<script>
	$(document).ready(function(){
	        $('#reset').on('change',function(){
	           var reset = parseInt($('#reset').val()); 
	           var date = new Date();
	         var t = new Date(); 
			//var n = $("#resetdays").val(); 
			//alert(offset);
			t.setDate(t.getDate() + reset);
			var month = "0"+(t.getMonth()+1);
			var date = "0"+t.getDate();
			
			const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "June","July", "Aug", "Sept", "Oct", "Nov", "Dec"];
			const d = new Date();
			
			month = month.slice(-2);
			date = date.slice(-2);
			//var date = date +"/"+month+"/"+t.getFullYear();
			var date = monthNames[t.getMonth()]+"-"+date+"-"+t.getFullYear();
	           $('#reset_date').val(date);
	        });
	    });
</script>
<script>
	$(document).ready(function(){
	        $('#first_rev_day').on('keyup',function(){
	           var hiremonth = $('#hiremonth').val();
	           var reset1 = parseInt($('#first_rev_day').val()); //alert(reset);
	          var tt = hiremonth.split("-").reverse().join(" ");
	         var t = new Date(tt); 
	         
			t.setDate(t.getDate() + reset1);
			var month = "0"+(t.getMonth()+1);
			var date = "0"+t.getDate();
			const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "June","July", "Aug", "Sept", "Oct", "Nov", "Dec"];
			const d = new Date();
			month = month.slice(-2);
			date = date.slice(-2);
			var date = monthNames[t.getMonth()]+"-"+date+"-"+t.getFullYear();
			if($(this).val() == '')
			{ // check if value changed
	         $('#reviewmonth').val('');
			}
			else
			{
	           $('#reviewmonth').val(date);
			}
	        });
	    });
</script>
<script>
	$(document).ready(function(){
	//group add limit
	var maxGroup = 120;
	//add more fields group
	$(".addMore").click(function(){

	if($('body').find('.fieldGroup').length < maxGroup){

	var fieldHTML = '<div class="fieldGroup">'+$(".fieldGroupCopy").html()+'</div>';
	$('body').find('.fieldGroup:last').after(fieldHTML);
	$('select').on('change', function() {
    $('#text1').val(this.value);
});
   jQuery(function($) {
  var input = $('input,textarea,select,checkbox');
  input.on('keydown', function() {
    var key = event.keyCode || event.charCode || event.which;

    if( key == 8 || key == 46 ){
   $('#text1').val(key);
    }
     else{   
         $('#text1').val(key);
     }
  });
  $(".check").click(function(){ //alert();
        $("#text1").val('121');
    });
    $(".check").click(function(){ //alert();
        $("#text1").prop("checked", false);
    });
  
});


  
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        var currentTab = $(e.target).text(); // get current tab
        var vf = $('#text1').val();//alert(vf);
        var current_tab = e.target;
        var previousTab = $(e.relatedTarget).text(); 
        var target = $(e.relatedTarget).attr("href");
        $('.no-button').eq(0).attr('href',target);
        var href = $('.no-button').attr('href');
       
     if(target==href){ 
        if(vf)
        {// alert('2');
         $("#alerts").modal({
		show: true,
	});
           
        } 
        else
        {
            
        }
     }
     else
     { 
     }
  
});

$(function() {

    $('#login-form-link').click(function(e) {
		$("#login-form").delay(100).fadeIn(100);
 		$("#register-form").fadeOut(100);
		$('#register-form-link').removeClass('active');
		$(this).addClass('active');
		e.preventDefault();
	});
	$('#register-form-link').click(function(e) {
		$("#register-form").delay(100).fadeIn(100);
 		$("#login-form").fadeOut(100);
		$('#login-form-link').removeClass('active');
		$(this).addClass('active');
		e.preventDefault();
	});

});


$(document).on('click', '.no-button', function () {
    var href = $(this).attr('href');
     var $link = $('li.active a[data-toggle="tab"]');
    $link.parent().removeClass('active');
    var tabLink = $link.attr('href');//alert(tabLink);
     $('#alerts').modal('hide');
    $('#myTab a[href="' + href + '"]').tab('show');
      
});
	}
	else
	{
	alert('Maximum '+maxGroup+' Persons are allowed.');
	}
	
	});
	//remove fields group
	$("body").on("click",".remove",function(){ 
	$(this).parents(".fieldGroup").remove();
	});
	});
</script>
<script>
	$(document).ready(function(){
	//group add limit
	var maxGroup = 120;
	//add more fields group
	$(".add").click(function(){
	if($('body').find('.responsibility').length < maxGroup){
	var fieldHTML = '<div class="responsibility">'+$(".fieldGroupCopy-2").html()+'</div>';
	$('body').find('.responsibility:last').after(fieldHTML);
	$('select').on('change', function() {
    $('#text1').val(this.value);
});
   jQuery(function($) {
  var input = $('input,textarea,select,checkbox');
  input.on('keydown', function() {
    var key = event.keyCode || event.charCode || event.which;

    if( key == 8 || key == 46 ){
   $('#text1').val(key);
    }
     else{   
         $('#text1').val(key);
     }
  });
  $(".check").click(function(){ //alert();
        $("#text1").val('121');
    });
    $(".check").click(function(){ //alert();
        $("#text1").prop("checked", false);
    });
  
});


  
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        var currentTab = $(e.target).text(); // get current tab
        var vf = $('#text1').val();//alert(vf);
        var current_tab = e.target;
        var previousTab = $(e.relatedTarget).text(); 
        var target = $(e.relatedTarget).attr("href");
        $('.no-button').eq(0).attr('href',target);
        var href = $('.no-button').attr('href');
       
     if(target==href){ 
        if(vf)
        {// alert('2');
         $("#alerts").modal({
		show: true,
	});
           
        } 
        else
        {
            
        }
     }
     else
     { 
     }
  
});

$(function() {

    $('#login-form-link').click(function(e) {
		$("#login-form").delay(100).fadeIn(100);
 		$("#register-form").fadeOut(100);
		$('#register-form-link').removeClass('active');
		$(this).addClass('active');
		e.preventDefault();
	});
	$('#register-form-link').click(function(e) {
		$("#register-form").delay(100).fadeIn(100);
 		$("#login-form").fadeOut(100);
		$('#login-form-link').removeClass('active');
		$(this).addClass('active');
		e.preventDefault();
	});

});


$(document).on('click', '.no-button', function () {
    var href = $(this).attr('href');
     var $link = $('li.active a[data-toggle="tab"]');
    $link.parent().removeClass('active');
    var tabLink = $link.attr('href');//alert(tabLink);
     $('#alerts').modal('hide');
    $('#myTab a[href="' + href + '"]').tab('show');
      
});
	}else{
	alert('Maximum '+maxGroup+' Persons are allowed.');
	}
	});
	//remove fields group
	$("body").on("click",".remove",function(){ 
	$(this).parents(".responsibility").remove();
	});
	});
</script>
<script type="text/javascript">
	$(document).ready(function(){
		var maxField = 10; //Input fields increment limitation
		var addButton = $('.ad'); //Add button selector
		var x = 1;
		var y = <?php echo $count;?>;
		var z = x + y;
		var wrapper = $('.review-1'); //Input field wrapper 
	
	//	var fieldHTML = '<div class="review-2"><div class="input_fields_wrap_1"><div class="form-group"><label class="control-label col-md-3">'+x+' Review Days :</label><div class="col-md-6"><div class="row"><div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"><div class="dropdown" style="margin-top: 1%;"><input name="ree[]" type="hidden" value="" id="ree" class="form-control"><input type="text" max-length="3" maxlength="3" class="form-control fsc-input first_rev_day" name="first_rev_day[]" id="first_rev_day'+x+'" value=""></div></div><div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"><div class="dropdown" style="margin-top: 1%;"><label class="control-label">'+x+' Review Date :</label></div></div><div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"><div class="dropdown" style="margin-top: 1%;"><input name="reviewmonth[]" readonly type="text" value="" id="reviewmonth'+x+'" class="date1 form-control reviewmonth" readonly="readonly"></div></div></div></div></div><div class="form-group"><label class="control-label col-md-3">Comments :</label><div class="col-md-6"><div class="row"><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><textarea name="hiring_comments[]" id="hiring_comments" rows="1" class="form-control fsc-input"></textarea></div></div></div></div><a href="javascript:void(0)" class="btn btn-danger remove pull-right "><i class="fa fa-trash-o" aria-hidden="true"></i></a></div></div></div>';
		$(addButton).click(function(){ 	
			if(z < maxField){
			$(wrapper).append('<div class="review-2"><div class="input_fields_wrap_1"><div class="form-group"><label class="control-label col-md-3">('+z+') Review Days :</label><div class="col-md-6"><div class="row"><div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"><div class="dropdown" style="margin-top: 1%;"><input name="ree[]" type="hidden" value="" id="ree" class="form-control"><input type="text"  maxlength="3" class="form-control fsc-input first_rev_day" name="first_rev_day[]" id="first_rev_day'+z+'" value=""></div></div><div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"><div class="dropdown" style="margin-top: 1%;"><label class="control-label">('+z+') Review Date :</label></div></div><div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"><div class="dropdown" style="margin-top: 1%;"><input name="reviewmonth[]"  type="text" value="" id="reviewmonth'+z+'" class="form-control reviewmonth"></div></div></div></div></div><div class="form-group"><label class="control-label col-md-3">Comments :</label><div class="col-md-6"><div class="row"><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><textarea name="hiring_comments[]" id="hiring_comments'+z+'" rows="1" class="form-control fsc-input"></textarea></div></div></div></div><a href="javascript:void(0)" class="btn btn-danger remove pull-right "><i class="fa fa-trash-o" aria-hidden="true"></i></a></div></div></div>'); // Add field html
	   $('select').on('change', function() {
	    $('#text1').val(this.value);
	});
	   jQuery(function($) {
	  var input = $('input,textarea,select,checkbox');
	  input.on('keydown', function() {
	    var key = event.keyCode || event.charCode || event.which;
	
	    if( key == 8 || key == 46 ){
	   $('#text1').val(key);
	    }
	     else{   
	         $('#text1').val(key);
	     }
	  });
	  $(".check").click(function(){ //alert();
	        $("#text1").val('121');
	    });
	    $(".check").click(function(){ //alert();
	        $("#text1").prop("checked", false);
	    });
	  
	});
	
	
	  
	    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
	        var currentTab = $(e.target).text(); // get current tab
	        var vf = $('#text1').val();//alert(vf);
	        var current_tab = e.target;
	        var previousTab = $(e.relatedTarget).text(); 
	        var target = $(e.relatedTarget).attr("href");
	        $('.no-button').eq(0).attr('href',target);
	        var href = $('.no-button').attr('href');
	       
	     if(target==href){ 
	        if(vf)
	        {// alert('2');
	         $("#alerts").modal({
			show: true,
		});
	           
	        } 
	        else
	        {
	            
	        }
	     }
	     else
	     { 
	     }
	  
	});
	
	$(function() {
	
	    $('#login-form-link').click(function(e) {
			$("#login-form").delay(100).fadeIn(100);
	 		$("#register-form").fadeOut(100);
			$('#register-form-link').removeClass('active');
			$(this).addClass('active');
			e.preventDefault();
		});
		$('#register-form-link').click(function(e) {
			$("#register-form").delay(100).fadeIn(100);
	 		$("#login-form").fadeOut(100);
			$('#login-form-link').removeClass('active');
			$(this).addClass('active');
			e.preventDefault();
		});
	
	});
	
	
	$(document).on('click', '.no-button', function () {
	    var href = $(this).attr('href');
	     var $link = $('li.active a[data-toggle="tab"]');
	    $link.parent().removeClass('active');
	    var tabLink = $link.attr('href');//alert(tabLink);
	     $('#alerts').modal('hide');
	    $('#myTab a[href="' + href + '"]').tab('show');
	      
	});
	        var rdiv1 = '#first_rev_day'+z;
	            var rdiv2 = '#reviewmonth'+z;//alert(rdiv2);
	      $(rdiv1).on('keyup',function(){ 
	           var hiremonth = $('#hiremonth').val();
	           var gg = $(rdiv1).val();
	           var reset1 = parseInt(gg); //alert(gg);
	          var tt = hiremonth.split("-").reverse().join(" ");
	         var t = new Date(tt); 
	         
			t.setDate(t.getDate() + reset1);
			var month = "0"+(t.getMonth()+1);
			var date = "0"+t.getDate();
			const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "June","July", "Aug", "Sept", "Oct", "Nov", "Dec"];
			const d = new Date();
			month = month.slice(-2);
			date = date.slice(-2);
			var date = monthNames[t.getMonth()]+"-"+date+"-"+t.getFullYear();
		
			if($(this).val() == '')
			{ // check if value changed
	         $(rdiv2).val('');
			}
			else
			{
	           $(rdiv2).val(date);
			}
	        });
	    x++;
	    z++;
			}
		});
		$(wrapper).on('click', '.ad', function(e){ //Once remove button is clicked
			e.preventDefault();
			$(wrapper).append(fieldHTML); //Remove field html
			x++; 
			z++;
		});  
		$(wrapper).on('click', '.remove', function(e){ //Once remove button is clicked
			e.preventDefault();
			$(this).parent().parent('.review-2').remove(); //Remove field html
			x--;
			z--; //Decrement field counter
		});
	});
</script>
<script>
	jQuery.fn.getNum = function() {
		var val = $.trim($(this).val());
		if(val.indexOf(',') > -1) {
		val = val.replace(',', '.');
		}
		var num = parseFloat(val);
		var num = num.toFixed(2);
		if(isNaN(num)) {
		num = '';
		}
		return num;
	}
	$(function() {
		$('#additional_withholding,#additional_withholding_1,#additional_withholding_2').blur(function() {
	//	$(this).val('$' + $(this).getNum());
	$(this).val($(this).getNum());
		});
	});
</script>
<script>
	$(document).ready(function(){
		$(document).on('change','.category', function(){ 
			//console.log('htm');
			var id = $(this).val();
			//alert(id);
			$.get('<?php echo URL::to('getBranch1'); ?>?id='+id, function(data)
			{  
			$('#branch_name').empty();
			$.each(data, function(index, subcatobj)
			{
			$('#branch_name').val(subcatobj.branchname);
			})
			});
		});
	});
</script>

<script>
	$(document).ready(function(){
		$("#ezipcode").keyup(function() {
		//console.log('htm');
		var id = $(this).val();
		$.get('<?php echo URL::to('/getzip'); ?>?zip='+id, function(data)
		{ 
		$('#ecity').empty();
		$('#estate').empty();//$('#countryId').empty();
		$.each(data, function(index, subcatobj)
		{$('#city').removeAttr("disabled"); $('#stateId').removeAttr("disabled"); 
		$('#ecity').val(subcatobj.city);
		$('#estate').append('<option value="'+subcatobj.state+'">'+subcatobj.state+'</option>');
		//$('#countryId').append('<option value="'+subcatobj.country+'">'+subcatobj.country+'</option>');
		})
		});
		});
	});
</script><?php $__currentLoopData = $review1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $re): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<div id="myModal<?php echo e($re->id); ?>" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Confirmation</h4>
			</div>
			<div class="modal-body">
				<p>Do you want to delete this record ?</p>
			</div>
			<div class="modal-footer">
<a href="<?php echo e(route('review.reviewdelete',$re->id)); ?>" class="btn btn-danger">Delete</a>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div><?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> <?php $__currentLoopData = $info; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $in): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<div id="myModal1<?php echo e($in->id); ?>" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Confirmation</h4>
			</div>
			<div class="modal-body">
				<p>Do you want to delete this record ?</p>
			</div>
			<div class="modal-footer">
<a href="<?php echo e(route('pay.paydelete',$in->id)); ?>" class="btn btn-danger">Delete</a>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div><?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<script>
	$(document).ready(function(){
	    $("select#check").change(function(){
	        var selectedCountry = $(this).children("option:selected").val();
	        var color = $("option:selected", this).attr("class");
	          $("#check").attr("class", color).addClass("form-control1 fsc-input");
	    });
	});
</script>
<script>
	function changeProfile() {
	        $('#file').click();
	    }
	    $('#file').change(function () {
	        if ($(this).val() != '') {
	            upload(this);
	        }
	    });
	    function upload(img) {
	        var form_data = new FormData();
	        form_data.append('file', img.files[0]);
	        form_data.append('_token', '<?php echo e(csrf_token()); ?>');
	        $('#loading2').css('display', 'block');
	        $.ajax({
	            url: "<?php echo e(url('ajax-image-upload')); ?>/" + $('#user_id11').val(),
	            data: form_data,
	            type: 'POST',
	            contentType: false,
	            processData: false,
	            success: function (data) {
	                if (data.fail) {
	                    $('#preview_image').attr('src', '<?php echo e(asset('public/images/noimage.jpg')); ?>');
	                    alert(data.errors['file']);
	                }
	                else {
	                    $('#file_name').val(data);
	                    $('#preview_image').attr('src', '<?php echo e(asset('public/uploads')); ?>/' + data);
	                }
	                $('#loading2').css('display', 'none');
	            },
	            error: function (xhr, status, error) {
	                alert(xhr.responseText);
	                $('#preview_image').attr('src', '<?php echo e(asset('public/images/noimage.jpg')); ?>');
	            }
	        });
	    }
</script>
<script>
	function additional_attach_update() {
	        $('#file_1').click();
	    }
	    $('#file_1').change(function () {
	        if ($(this).val() != '') {
	            upload1(this);
	
	        }
	    });
	    function upload1(img) {
	        var form_data = new FormData();
	        form_data.append('file_1', img.files[0]);
	        form_data.append('_token', '<?php echo e(csrf_token()); ?>');
	        $('#loading1').css('display', 'block');
	        $.ajax({
	            url: "<?php echo e(url('ajax-image-upload1')); ?>/" + $('#user_id11').val(),
	            data: form_data,
	            type: 'POST',
	            contentType: false,
	            processData: false,
	            success: function (data) {
	                if (data.fail) {
	                    $('#preview_image_1').attr('src', '<?php echo e(asset('public/images/noimage.jpg')); ?>');
	                    alert(data.errors['file']);
	                }
	                else {
	                    $('#file_name_1').val(data);
	                    $('#preview_image_1').attr('src', '<?php echo e(asset('public/uploads')); ?>/' + data);
	                }
	                $('#loading1').css('display', 'none');
	            },
	            error: function (xhr, status, error) {
	                alert(xhr.responseText);
	                $('#preview_image_1').attr('src', '<?php echo e(asset('public/images/noimage.jpg')); ?>');
	            }
	        });
	    }
	    function additional_attach_remove() {
	        if ($('#file_name_1').val() != '')
	            if (confirm('Are you sure want to remove profile picture?')) {
	                $('#loading1').css('display', 'block');
	                var form_data = new FormData();
	                form_data.append('_method', 'DELETE');
	                form_data.append('_token', '<?php echo e(csrf_token()); ?>');
	                $.ajax({
	                    url: "ajax-image-upload1/" + $('#file_name_1').val(),
	                    data: form_data,
	                    type: 'POST',
	                    contentType: false,
	                    processData: false,
	                    success: function (data) {
	                        $('#preview_image_1').attr('src', '<?php echo e(asset('public/images/noimage.jpg')); ?>');
	                        $('#file_name_1').val('');
	                        $('#loading1').css('display', 'none');
	                    },
	                    error: function (xhr, status, error) {
	                        alert(xhr.responseText);
	                    }
	                });
	            }
	    }
</script>
<script>
	function additional_attach() {
	        $('#file_2').click();
	    }
	    $('#file_2').change(function () {
	        if ($(this).val() != '') {
	            upload2(this);
	
	        }
	    });
	    function upload2(img) {
	        var form_data = new FormData();
	        form_data.append('file_2', img.files[0]);
	        form_data.append('_token', '<?php echo e(csrf_token()); ?>');
	        $('#loading').css('display', 'block');
	        $.ajax({
	            url: "<?php echo e(url('ajax-image-upload2')); ?>/" + $('#user_id11').val(),
	            data: form_data,
	            type: 'POST',
	            contentType: false,
	            processData: false,
	            success: function (data) {
	                if (data.fail) {
	                    $('#preview_image_2').attr('src', '<?php echo e(asset('public/images/noimage.jpg')); ?>');
	                    alert(data.errors['file']);
	                }
	                else {
	                    $('#file_name_2').val(data);
	                    $('#preview_image_2').attr('src', '<?php echo e(asset('public/uploads')); ?>/' + data);
	                }
	                $('#loading').css('display', 'none');
	            },
	            error: function (xhr, status, error) {
	                alert(xhr.responseText);
	                $('#preview_image_2').attr('src', '<?php echo e(asset('public/images/noimage.jpg')); ?>');
	            }
	        });
	    }
</script>
<script>
	function pfid_upload() {
	        $('#pfid1').click();
	    }
	    $('#pfid1').change(function () {
	        if ($(this).val() != '') {
	            upload3(this);
	
	        }
	    });
	    function upload3(img) {
	        var form_data = new FormData();
	        form_data.append('pfid1', img.files[0]);
	        form_data.append('_token', '<?php echo e(csrf_token()); ?>');
	        $('#loading3').css('display', 'block');
	        $.ajax({
	            url: "<?php echo e(url('pfid_upload')); ?>/" + $('#user_id11').val(),
	            data: form_data,
	            type: 'POST',
	            contentType: false,
	            processData: false,
	            success: function (data) {
	                if (data.fail) {
	                    $('#preview_image_3').attr('src', '<?php echo e(asset('public/images/noimage.jpg')); ?>');
	                    alert(data.errors['file']);
	                }
	                else {
	                    $('#pfid1_name').val(data);
	                    $('#preview_image_3').attr('src', '<?php echo e(asset('public/employeeProof1')); ?>/' + data);
	                }
	                $('#loading3').css('display', 'none');
	            },
	            error: function (xhr, status, error) {
	                alert(xhr.responseText);
	                $('#preview_image_3').attr('src', '<?php echo e(asset('public/images/noimage.jpg')); ?>');
	            }
	        });
	    }
</script>
<script>
	function pfid_upload() {
	        $('#pfid1').click();
	    }
	    $('#pfid1').change(function () {
	        if ($(this).val() != '') {
	            upload3(this);
	
	        }
	    });
	    function upload3(img) {
	        var form_data = new FormData();
	        form_data.append('pfid1', img.files[0]);
	        form_data.append('_token', '<?php echo e(csrf_token()); ?>');
	        $('#loading3').css('display', 'block');
	        $.ajax({
	            url: "<?php echo e(url('pfid_upload')); ?>/" + $('#user_id11').val(),
	            data: form_data,
	            type: 'POST',
	            contentType: false,
	            processData: false,
	            success: function (data) {
	                if (data.fail) {
	                    $('#preview_image_3').attr('src', '<?php echo e(asset('public/images/noimage.jpg')); ?>');
	                    alert(data.errors['file']);
	                }
	                else {
	                    $('#pfid1_name').val(data);
	                    $('#preview_image_3').attr('src', '<?php echo e(asset('public/employeeProof1')); ?>/' + data);
	                }
	                $('#loading3').css('display', 'none');
	            },
	            error: function (xhr, status, error) {
	                alert(xhr.responseText);
	                $('#preview_image_3').attr('src', '<?php echo e(asset('public/images/noimage.jpg')); ?>');
	            }
	        });
	    }
</script>
<script>
	function pfid2_upload() {
	        $('#pfid2').click();
	    }
	    $('#pfid2').change(function () {
	        if ($(this).val() != '') {
	            upload4(this);
	
	        }
	    });
	    function upload4(img) {
	        var form_data = new FormData();
	        form_data.append('pfid2', img.files[0]);
	        form_data.append('_token', '<?php echo e(csrf_token()); ?>');
	        $('#loading4').css('display', 'block');
	        $.ajax({
	            url: "<?php echo e(url('pfid_upload2')); ?>/" + $('#user_id11').val(),
	            data: form_data,
	            type: 'POST',
	            contentType: false,
	            processData: false,
	            success: function (data) {
	                if (data.fail) {
	                    $('#preview_image_4').attr('src', '<?php echo e(asset('public/images/noimage.jpg')); ?>');
	                    alert(data.errors['file']);
	                }
	                else {
	                    $('#pfid2_name').val(data);
	                    $('#preview_image_4').attr('src', '<?php echo e(asset('public/employeeProof2')); ?>/' + data);
	                }
	                $('#loading4').css('display', 'none');
	            },
	            error: function (xhr, status, error) {
	                alert(xhr.responseText);
	                $('#preview_image_4').attr('src', '<?php echo e(asset('public/images/noimage.jpg')); ?>');
	            }
	        });
	    }
</script>
<script>
	function photo_upload() {
	        $('#photo').click();
	    }
	    $('#photo').change(function () {
	        if ($(this).val() != '') {
	            upload5(this);
	           // alert(this);
	        }
	    });
	    function upload5(img) {
	        var form_data = new FormData(); //alert(form_data);
	        form_data.append('photo', img.files[0]);
	        form_data.append('_token', '<?php echo e(csrf_token()); ?>');
	        $('#loading5').css('display', 'block');
	        $.ajax({
	            url: "<?php echo e(url('photo_upload')); ?>/" + $('#user_id11').val(),
	            data: form_data,
	            type: 'POST',
	            contentType: false,
	            processData: false,
	            success: function (data) {
	                if (data.fail) {
	                    $('#preview_image_5').attr('src', '<?php echo e(asset('public/images/noimage.jpg')); ?>');
	                    alert(data.errors['file']);
	                }
	                else {
	                    $('#photo_name').val(data);//
	                    
	                   // alert();
	                    $('#preview_image_5').attr('src', '<?php echo e(asset('public/employeeimage')); ?>/' + data);
	                }
	                $('#loading5').css('display', 'none');
	            },
	            error: function (xhr, status, error) {
	                alert(xhr.responseText);
	                $('#preview_image_5').attr('src', '<?php echo e(asset('public/images/noimage.jpg')); ?>');
	            }
	        });
	    }
	  
	  $(document).ready(function(){
	  /***phone number format***/
	  $(".phone").keypress(function (e) {
	    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
	      return false;
	    }
	    var curchr = this.value.length;
	    var curval = $(this).val();
	    if (curchr == 3 && curval.indexOf("(") <= -1) {
	      $(this).val("(" + curval + ")" + " ");
	    } else if (curchr == 4 && curval.indexOf("(") > -1) {
	      $(this).val(curval + ")-");
	    } else if (curchr == 5 && curval.indexOf(")") > -1) {
	      $(this).val(curval + "-");
	    } else if (curchr == 9) {
	      $(this).val(curval + "-");
	      $(this).attr('maxlength', '14');
	    }
	  });
	});
</script>
<script>
	var room1 = 0;
	var coun = <?php echo $notecon;?>;
	var z = room1 + coun; 
	function education_field_note() {
	room1++;
	z++;
	var objTo = document.getElementById('input_fields_wrap_notes')
	var divtest = document.createElement("div");
	divtest.setAttribute("class", "form-group removeclass"+z);
	divtest.innerHTML = '<label class="control-label col-md-3">Note '+ z +' :</label><div class="col-md-6"><input name="noteid[]" value="" type="hidden" placeholder="" id="noteid" class=""><input name="adminnotes[]" value="" type="text" id="adminnotes" placeholder="Create Note" class="textonly form-control" /></div></div><div class="col-md-1"> <button class="btn btn-danger" type="button" onclick="remove_education_fields('+ z +');"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span> </button></div></div></div>';
	var rdiv = 'removeclass'+z;
	var rdiv1 = 'Schoolname'+z;
	
	objTo.appendChild(divtest)
	$('select').on('change', function() {
	    $('#text1').val(this.value);
	});
	   jQuery(function($) {
	  var input = $('input,textarea,select,checkbox');
	  input.on('keydown', function() {
	    var key = event.keyCode || event.charCode || event.which;
	
	    if( key == 8 || key == 46 ){
	   $('#text1').val(key);
	    }
	     else{   
	         $('#text1').val(key);
	     }
	  });
	  $(".check").click(function(){ //alert();
	        $("#text1").val('121');
	    });
	    $(".check").click(function(){ //alert();
	        $("#text1").prop("checked", false);
	    });
	  
	});
	
	
	  
	    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
	        var currentTab = $(e.target).text(); // get current tab
	        var vf = $('#text1').val();//alert(vf);
	        var current_tab = e.target;
	        var previousTab = $(e.relatedTarget).text(); 
	        var target = $(e.relatedTarget).attr("href");
	        $('.no-button').eq(0).attr('href',target);
	        var href = $('.no-button').attr('href');
	       
	     if(target==href){ 
	        if(vf)
	        {// alert('2');
	         $("#alerts").modal({
			show: true,
		});
	           
	        } 
	        else
	        {
	            
	        }
	     }
	     else
	     { 
	     }
	  
	});
	
	$(function() {
	
	    $('#login-form-link').click(function(e) {
			$("#login-form").delay(100).fadeIn(100);
	 		$("#register-form").fadeOut(100);
			$('#register-form-link').removeClass('active');
			$(this).addClass('active');
			e.preventDefault();
		});
		$('#register-form-link').click(function(e) {
			$("#register-form").delay(100).fadeIn(100);
	 		$("#login-form").fadeOut(100);
			$('#login-form-link').removeClass('active');
			$(this).addClass('active');
			e.preventDefault();
		});
	
	});
	
	
	$(document).on('click', '.no-button', function () {
	    var href = $(this).attr('href');
	     var $link = $('li.active a[data-toggle="tab"]');
	    $link.parent().removeClass('active');
	    var tabLink = $link.attr('href');//alert(tabLink);
	     $('#alerts').modal('hide');
	    $('#myTab a[href="' + href + '"]').tab('show');
	      
	});
	
	}
	function remove_education_fields(rid) {
	$('.removeclass'+rid).remove();
	room1--;
	z--;
	}
</script>
<?php $__currentLoopData = $admin_notes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $notes): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php if($notes->notes==NULL): ?> <?php else: ?>
<div id="myModalnote_<?php echo e($notes->id); ?>" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Confirmation</h4>
			</div>
			<div class="modal-body">
				<p>Do you want to delete this record ?</p>
			</div>
			<div class="modal-footer"> <a href="<?php echo e(route('empnote.empnotedelete',$notes->id)); ?>" class="btn btn-danger">Delete</a>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div><?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

<?php $__currentLoopData = $info; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $infopay): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php if($infopay->fields==NULL): ?> <?php else: ?>
<div id="myModalpay_<?php echo e($infopay->id); ?>" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Confirmation</h4>
			</div>
			<div class="modal-body">
				<p>Do you want to delete this record ?</p>
			</div>
			<div class="modal-footer"> <a href="<?php echo e(route('emppay.emppaydelete',$infopay->id)); ?>" class="btn btn-danger">Delete</a>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div><?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

<?php $__currentLoopData = $whinfo2; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $whinfo1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php if($whinfo1->fillingstatus==NULL): ?> <?php else: ?>
<div id="myModalwh_<?php echo e($whinfo1->id); ?>" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Confirmation</h4>
			</div>
			<div class="modal-body">
				<p>Do you want to delete this record ?</p>
			</div>
			<div class="modal-footer"> <a href="<?php echo e(route('empwh.empwhdelete',$whinfo1->id)); ?>" class="btn btn-danger">Delete</a>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div><?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

<?php $__currentLoopData = $reviewinfo; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $reviewinfo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php if($reviewinfo->review_comments==NULL): ?> <?php else: ?>
<div id="myModalreview_<?php echo e($reviewinfo->id); ?>" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Confirmation</h4>
			</div>
			<div class="modal-body">
				<p>Do you want to delete this record ?</p>
			</div>
			<div class="modal-footer"> <a href="<?php echo e(route('empreview.empreviewdelete',$reviewinfo->id)); ?>" class="btn btn-danger">Delete</a>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div> <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

<div id="alertss" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content " style="background-color: #ededed;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Save</h4>
			</div>
			<div class="modal-body">
				<p><?php echo e(ucfirst($emp->type)); ?> Successfully Saved.</p>
			</div>
			<div class="modal-footer">
				<button data-dismiss="modal" class="btn btn-default" style="background-color: #bfbfbf;">Close</button>
			</div>
		</div>
	</div>
</div>
<div id="alertss1" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content model-width" style="background-color: #f3f3f3;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Save</h4>
			</div>
			<div class="modal-body">
				<p><?php echo e(ucfirst($emp->type)); ?> not Successfully Saved.</p>
			</div>
			<div class="modal-footer">
				<button data-dismiss="modal" class="btn btn-default" style="background-color: #bfbfbf;">Close</button>
			</div>
		</div>
	</div>
</div>
<div id="alerts" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-conten modal-width" style="background-color: #ededed;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Save changes</h4>
			</div>
			<div class="modal-body">
				<center>
					<p>Do you want to save the changes ?</p>
				</center>
			</div>
			<div class="modal-footer text-center">
				<button type="submit" class="btn btn-default primary1 primary" style="iwdth:69px;">Yes</button>
				<button type="button" id="button" class="btn btn-default primary" style="iwdth:69px;">No</button> <a class="btn btn-default primary no-button">Cancel</a>
			</div>
		</div>
	</div>
</div>
<script>
	$('select').on('change', function() {
	    $('#text1').val(this.value);
	});
	   jQuery(function($) {
	  var input = $('input,textarea,select,checkbox');
	  input.on('keydown', function() {
	    var key = event.keyCode || event.charCode || event.which;
	
	    if( key == 8 || key == 46 ){
	   $('#text1').val(key);
	    }
	     else{   
	         $('#text1').val(key);
	     }
	  });
	  $(".check").click(function(){ //alert();
	        $("#text1").val('121');
	    });
	    $(".check").click(function(){ //alert();
	        $("#text1").prop("checked", false);
	    });
	  
	});
	$(document).ready(function(){
	
	  
	    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
	        var currentTab = $(e.target).text(); // get current tab
	        var vf = $('#text1').val();//alert(vf);
	        var current_tab = e.target;
	        var previousTab = $(e.relatedTarget).text(); 
	        var target = $(e.relatedTarget).attr("href");
	        $('.no-button').eq(0).attr('href',target);
	        var href = $('.no-button').attr('href');
	       
	     if(target==href){ 
	        if(vf)
	        {// alert('2');
	         $("#alerts").modal({
			show: true,
		});
	           
	        } 
	        else
	        {
	            
	        }
	     }
	     else
	     { 
	     }
	     
	    });
	});
	
	$(function() {
	
	    $('#login-form-link').click(function(e) {
			$("#login-form").delay(100).fadeIn(100);
	 		$("#register-form").fadeOut(100);
			$('#register-form-link').removeClass('active');
			$(this).addClass('active');
			e.preventDefault();
		});
		$('#register-form-link').click(function(e) {
			$("#register-form").delay(100).fadeIn(100);
	 		$("#login-form").fadeOut(100);
			$('#login-form-link').removeClass('active');
			$(this).addClass('active');
			e.preventDefault();
		});
	
	});
	
	 var cnts1=1;
       
	$(document).on('click', '#add_pay', function () {
	    cnts1++;
	    $(".addpayinfo").append('<tr><td><select class="form-control" name="pay_method[]"><option value="">Select</option><option value="Salary">Salary</option><option value="Hourly">Hourly</option></select></td><td><select class="form-control" name="pay_frequency[]"><option value="">Select</option><option value="Weekly">Weekly</option><option value="Bi-Weekly">Bi-Weekly</option><option value="Semi-Monthly">Semi-Monthly</option><option value="Monthly">Monthly</option></select></td> <td><input type="text" class="form-control text-center pay_scale_'+cnts1+'" name="pay_scale[]"></td> <td><input type="text" class="form-control paydate_'+cnts1+'" name="effective_date[]"></td> <td><textarea cols="40" rows="1" class="form-control" name="fields[]"></textarea></td> <td style="text-align:center;"> <a class="btn-action btn-delete btn btn-danger"><i class="fa fa-trash remPay" ></i></a> </td> </tr>');
			$('.pay_scale_'+cnts1).blur(function() {
			$(this).val('$' + $(this).getNum());
			
			});
				$(".paydate_"+cnts1).datepicker({
			autoclose: true,
	format: "mm/dd/yyyy",
			//endDate: "today"
		});
	
		
	    
	});
	$(".addpayinfo").on('click','.remPay',function(){
        $(this).parent().parent().parent().remove();
    });
    
    $(document).on('click', '#add_review', function () {
	    cnts1++;
	    $(".addreviewinfo").append('<tr><td></td><td><input type="text" name="review_date[]" class="form-control review_date_'+cnts1+'"></td> <td><input type="text" name="review_day[]" class="form-control text-center review_day_'+cnts1+'"></td> <td><input type="text" readonly name="next_review_date[]" class="form-control next_review_date_'+cnts1+'"></td> <td><input type="text" name="review_rate[]" class="form-control text-center pay_review_scale_'+cnts1+'"></td> <td><textarea cols="40" rows="1" class="form-control" name="review_comments[]"></textarea></td> <td style="text-align:center;"> <a class="btn-action btn-delete btn btn-danger"><i class="fa fa-trash remReview"></i></a> </td> </tr>');
        $('.pay_review_scale_'+cnts1).blur(function() {
			$(this).val('$' + $(this).getNum());
			
			});
				$(".review_date_"+cnts1).datepicker({
			autoclose: true,
	format: "mm/dd/yyyy",
			//endDate: "today"
		});
		
		 (function($, window, document, undefined){
                                                $(".review_day_"+cnts1).blur(function(){
                                                   var date = new Date($(".review_date_"+cnts1).val()),
                                                       days = parseInt($(".review_day_"+cnts1).val(), 10);
                                                    
                                                    if(!isNaN(date.getTime())){
                                                        date.setDate(date.getDate() + days);
                                                        $(".next_review_date_"+cnts1).val(date.toInputFormat());
                                                    } else {
                                                        alert("Invalid Date");  
                                                    }
                                                });
                                                
                                                
                                                //From: http://stackoverflow.com/questions/3066586/get-string-in-yyyymmdd-format-from-js-date-object
                                                Date.prototype.toInputFormat = function() {
                                                   var yyyy = this.getFullYear().toString();
                                                   var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
                                                   var dd  = this.getDate().toString();
                                                //   return yyyy + "-" + (mm[1]?mm:"0"+mm[0]) + "-" + (dd[1]?dd:"0"+dd[0]); // padding
                                                   return (mm[1]?mm:"0"+mm[0]) + "/" + (dd[1]?dd:"0"+dd[0]) + "/" + yyyy; // padding
                                                };
                                            })(jQuery, this, document);
	
		
		
	});
	$(".addreviewinfo").on('click','.remReview',function(){
        $(this).parent().parent().parent().remove();
    });
    
    $(document).on('click', '#add_tax', function () {
        cnts1++;
	    $(".addtaxinfo").append('<tr><td><select class="form-control" name="year_wh[]"><option value="">Select</option><option value="2021">2021</option></select></td><td><select name="fillingstatus[]" id="fillingstatus" class="form-control tttt"> <option value="Single">Single</option> <option value="Married File Jointly" selected="">Married File Jointly</option> <option value="Married File Separately">Married File Separately</option> <option value="Head of Household">Head of Household</option> </select> </td> <td><input name="fed_claim[]" class="form-control text-center"></td> <td><input class="form-control text-center fedwh_'+cnts1+'" name="fed_wh[]"></td><td><input class="form-control text-center" name="st_claim[]"></td> <td><input class="form-control text-center stwh_'+cnts1+'" name="st_wh[]"></td> <td><input class="form-control text-center" name="local_claims[]"></td> <td><input class="form-control text-center localwh_'+cnts1+'" name="local_wh[]"></td> <td style="text-align:center;"> <a class="btn-action btn-delete btn btn-danger"><i class="fa fa-trash remTax"></i></a> </td> </tr>');
	
        $('.fedwh_'+cnts1).blur(function() {
			$(this).val('$' + $(this).getNum());
			
			});
			
			$('.stwh_'+cnts1).blur(function() {
			$(this).val('$' + $(this).getNum());
			
			});
		
		$('.localwh_'+cnts1).blur(function() {
			$(this).val('$' + $(this).getNum());
			
			});
		
		
    });
	$(".addtaxinfo").on('click','.remTax',function(){
        $(this).parent().parent().parent().remove();
    });
    
    $(document).on('click', '#add_leave', function () {
	    $(".addleaves").append('<tr><td class="text-center">1</td><td><select class="form-control"><option>Select</option><option>Paid Leave</option><option>Sick Leave</option><option>Vacation Leave</option></select></td><td><input type="text" class="form-control text-center" name="" id=""></td><td><select class="form-control"><option value="">Select</option><option value="1">Week</option><option value="2">Month</option></select></td><td><input type="date" class="form-control"></td><td><textarea class="form-control" id="note1" name="note1" rows="1"></textarea></td><td style="text-align:center;"> <a class="btn-action btn-delete btn btn-danger"><i class="fa fa-trash remLeave"></i></a></td></tr>');
    });
	$(".addleaves").on('click','.remLeave',function(){
        $(this).parent().parent().parent().remove();
    });
    
	$(document).on('click', '.no-button', function () {
	    var href = $(this).attr('href');
	     var $link = $('li.active a[data-toggle="tab"]');
	    $link.parent().removeClass('active');
	    var tabLink = $link.attr('href');//alert(tabLink);
	     $('#alerts').modal('hide');
	    $('#myTab a[href="' + href + '"]').tab('show');
	      
	});
	$(document).on('click', '#mail_access', function () {
	    var lengthofmailAccess = $('.accessemail').length + 1;
	    var getLengthofradio = $('.emailaccess').length +1;
        $('#add_access').append('<tr><td style="vertical-align: middle;"><select class="form-control accessemail" id="accessemail'+lengthofmailAccess+'" name="accessemail[]"></select></td><td class="text-center" style="vertical-align: middle;"><input type="checkbox" class="emailaccess" id="checkaccess'+getLengthofradio+'" name="access2[]" value="1"><label class="fsc-form-label" for="checkaccess'+getLengthofradio+'" style="float:initial;vertical-align: middle;"> </label></td><td style="vertical-align: middle;text-align:center;" ><a class="btn btn-danger remAccess"><i class="fa fa-minus"></i></a></td></tr>');
        var url='<?php echo URL::to('fac-Bhavesh-0554/getemailList'); ?>/'+<?php echo e($emp->id); ?>;
        $('#accessemail'+lengthofmailAccess).empty();
        $.get(url,function(data){
           $('#accessemail'+lengthofmailAccess).append(data);
        });
    });
    
    $("#add_access").on('click','.remAccess',function(){
            $(this).parent().parent().remove();
        });
</script>
<script>
	$(function () {
	        $('.primary1').click(function () {  //alert(newopt);
	        $.ajax({
	        type: "post",
	        url: "<?php echo e(route('employee.update',$emp->id)); ?>",
	        dataType: "json",
	        data: $('#registrationForm').serialize(),
	        success: function(data){
	            $('#alerts').modal('hide');
	            $('#alertss').modal('show');
	            var vf = $('#text1').val('');
	          // $("#registrationForm").load(" #registrationForm > *"); 
	        },
	        error: function(data){
	            $('#alerts').modal('hide');
	               alert('Employee Updated Successfully');
	         
	            location.reload(true);
	            //$("#registrationForm").load(" #registrationForm > *");
	            //$('#alertss1').modal('show');
	        }
	    });
	       
	//$('#myModal2').modal('hide');
	                });
	            });
</script>
<script type="text/javascript">
	$(document).ready(function(){
	        $("#button").click(function(){
	            location.reload(true);
	         });
	    });
	    
	    
	 $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  }
              });
           
             $(function () {
                $('#addopt').click(function () { //alert();
                    var newopt = $('#newopt').val();
                    if (newopt == '') {
                        alert('Please enter something!');
                        return;
                    }

                   //check if the option value is already in the select box
                    $('#vendor_product option').each(function (index) {
                        if ($(this).val() == newopt) {
                            alert('Duplicate option, Please enter new!');
                        }
                    })
                    $.ajax({
                        type: "post",
                        url: "<?php echo route('team.teams'); ?>",
                        dataType: "json",
                        data: $('#ajax').serialize(),
                        success: function(data){
                             alert('Successfully Added');
                             location.reload();
                             $('#vendor_product').append('<option value=' + newopt + '>' + newopt + '</option>');
                             $("#div").load(" #div > *");
                             $("#newopt").val('');
                             
                        },
                        error: function(data){
                             alert("Error")
                        }
                    });
                   
                    $('#basicExampleModal').modal('hide');
                });
            });
             
	        
	        $(document).ready(function() {
            $(document).on('click', '.delete', function(){
                var id = $(this).attr('id');
                if(confirm("Are you sure you want to Delete this data?"))
                {
                    $.ajax({
                        url: "<?php echo route('removeteam.removeteams'); ?>",
                        mehtod:"get",
                        data:{id:id},
                        success:function(data)
                        {
                         // alert(data);
                            location.reload();
                           $('#cur_'+id).remove();
                           $("#vendor_product").load(" #vendor_product > *");
                        }
                    })
                }
                else
                {
                    return false;
                }
            }); 
          }); 
 
    
    $(".show_benefit").hide();
    $("#add_leave").hide();
    $(".none_benefit").click(function() {
        if($(this).is(":checked")) {
            $(".show_benefit").hide();
            $("#add_leave").hide();
        } else {
            $(".show_benefit").show();
            $("#add_leave").show();
        }
    });
    
    if($(".none_benefit").is(":checked")) {
            $(".show_benefit").hide();
    } else {
            $(".show_benefit").show();
    }
    
    
</script>

<div class="modal fade" id="basicExampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nickname </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="" method="post" id="ajax">
           <?php echo e(csrf_field()); ?>

      <div class="modal-body">
         <input type="text" id="newopt" name="newopt"  class="form-control" placeholder="Nickname"/>
      </div>
      <div class="modal-footer">
        <button type="button" id="addopt" class="btn btn-primary">Save</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="basicExampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background:#038ee0;">
        <h4 class="modal-title" id="exampleModalLabel" style="text-align:center;">Nickname <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button></h4>
        
      </div>
      <div class="modal-body" style="background:#e1fffc;padding:0px !important;">
          <div class="curency curency_ref" id="div">
         
         
          <?php $__currentLoopData = $team; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $teams): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>         
          <div id="cur_" class="col-md-12" style="border:1px solid;background:#e1fffc;">
              <div class="col-md-12" style="margin-top: 7px;margin-bottom:5px;">
                  
              <a class="delete" style="color:#000;" id="<?php echo e($teams->id); ?>"><?php echo e($teams->team); ?></a>
              
              <a class="delete btn-action btn-delete pull-right" style="color:#000;" id="<?php echo e($teams->id); ?>"><i class="fa fa-trash"></i></a>
              </div>
          </div>
         
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
         
          </div>
      </div>
      <div class="modal-footer" style="text-align:center;">
        <button type="button" style="margin-top:10px;border:1px solid;" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>