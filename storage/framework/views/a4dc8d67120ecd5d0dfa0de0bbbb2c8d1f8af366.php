<!DOCTYPE html>
<html lang="lang="<?php echo e(app()->getLocale()); ?>">
<head>
<?php echo $__env->make('fscemployee.layouts.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>	
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<?php echo $__env->make('fscemployee.layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php $__env->startSection('main-content'); ?>
    <?php echo $__env->yieldSection(); ?>
<?php echo $__env->make('fscemployee.layouts.leftsidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('fscemployee.layouts.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</div>
</body>
</html>
