<?php $__env->startSection('main-content'); ?>
<style>
    .search-btn {
    position: absolute;
    top: 46px;
    right: 16px;
    background:transparent;
    border:transparent;
}
select.greenText, .statusselectbox select {
    height: 40px!important;
    border-radius: 3px;
    border: 2px solid #2fa6f2;
    font-size: 16px !important;
    outline: 0;
    color: #000;
    padding: 6px 6px;
}
select .fedstatus1 option[selected] {
    background: red;
}
.searchboxmain{float: left;  display: FLEX;  margin-top:-5px; justify-content: space-between;position:absolute;}
.arrow{width: 0px;position: relative;float: left;}
.searchboxmain .form-control{margin-right:10px!important;}
.clear{clear:both;}
/*.page-title h1{float:left; margin-left:15%; margin-top:6px;}*/
.page-title{padding:11px 12px 10.5px !important; border-bottom:3px solid #00a65a;}
.headerbox{}
/*.padtop7{padding-top:7px;}*/
.disablebox input[type="text"], .disablebox select, .disablebox input[type="email"], .disablebox input[type="tel"], .disablebox input[type="number"], .disablebox textarea {
    background: #eeeeee!important;
    cursor: no-drop!important;
}
.pointernone{pointer-events:none}
.pad20{padding:20px;}
.nav-tabs > li.active > a.yel, .nav-tabs > li.active > a.yel:focus, .nav-tabs > li.active > a.yel:hover {
       cursor:default;
}
.nav-tabs > li{
    margin: 0px 0 0 8px;
}
.nav > li > a.yel:hover, .nav > li > a.yel:active, .nav > li > a.yel:focus {
    border-color:#000 !important;
    color:#000 !important;
    background:#ffff99;
    border-radius: 5px !important;
}
.nav-tabs {
    padding: 12px;
    border: 1px solid #3598dc !important;
}
.btnaddmore{background: #337ab7; display:inline-block; margin-bottom:5px;
    padding: 6px 10px;
    color: #fff;
    border-radius: 4px}
.btnremove{background: #ff0000;
    padding: 6px 10px;
    color: #fff;
    border-radius: 4px}
.padzero{padding:0px!important;}
.officermainbox{border:1px solid #ccc; padding:20px; margin-bottom:30px;}
.table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
    padding: 6px 8px 8px 8px !important;
}
.table-bordered, .table-bordered > tbody > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > td, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > thead > tr > th
{border-color:#ccc!important;}
.custom-file-upload {
  border: 1px solid #ccc;
  display: inline-block;
  padding: 6px 12px;
  cursor: pointer;
}
.btnaddrecord{width:120px; float:right; cursor:pointer;}
.new_images_sec img{height:53px;}

.card ul li{width:24% !important;}
   .card ul.test li a{background:#ffcc66 !important;}
   .card ul.test li.active a{background:#00a0e3 !important;}
   .card ul li a{display: block;width: 100%;color: #333;text-transform: capitalize;background: linear-gradient(180deg, #fdff9a 30%, #e3e449 70%);border: 1px solid #979800 !important;}
   .card ul li a:hover{color: #333;
   background: linear-gradient(180deg, #fbff37 30%, #d3d40e 70%);border: 1px solid #333 !important;}
   .card ul li.active{color: #333;background: linear-gradient(180deg, #fbff37 30%, #d3d40e 70%);border: 1px solid #333 !important;}
   .card ul li a.active, .card ul li a:hover, .card ul li.active a:hover{color: #fff!important;background: #12186b!important;border: 1px solid #12186b !important;}
   .card .nav-tabs{border:0px!important;} 
.feeschargesbox .form-control{text-align:right;}
.hrdivider{border-bottom: 2px solid #ccc!important; width: 100%; height: 1px; margin-top: 33px;margin-bottom: 33px; }
.hrdivider2{margin-top: 14px;   margin-bottom: 30px; border-bottom: 2px solid #ccc!important; width: 100%; height: 1px;}
.officerchange .form-control{background:#fff!important;}
.add-row{background: #007bff; padding: 3px 5px;  color: #fff;  border-radius: 3px; cursor: pointer;}
.delete-row{background:#dc3545; padding: 3px 5px;  color: #fff;  border-radius: 3px; cursor: pointer;}
.newcmpname{display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    height: 77px;}
.clear{clear:both;}
.mbtmzero{margin-bottom:0px!important;}
.nav>li>a{padding:10px 8px!important;}
.text-left{text-align:left!important;}
.text-center{text-align:center!important;}
.nav>li>a {
    position: relative;
    display: block;
    padding: 10px 5px;
}
input[type="file"]{position: relative!important; margin-top:11px;
    width: 100px!important;
    font-size: 12px!important; display:none;
    opacity: 1!important;}
.searchboxmain .form-control{height:34px!important;}
.searchboxmain .btn-action{background: #367fa9 !important;
    padding: 8px 15px!important;
    margin-top: 0px;}
.box.box-success{top: 0;
 border:0px; display:inline-block;
}
.clear{clear:both;}
/**************** dropdown list ********************/    
th{
    vertical-align:middle !important;
} 
#countryList{margin:40px 0px 0px 0px;
    padding:0px 0px;
    border: 0px solid #ccc;
    max-height: 200px;
    overflow: auto; position: absolute;
    width: 360px;
    z-index: 99999;}

#countryList li {
    border-bottom: 1px solid #025b90;
    background: #ef7c30;
}
#countryList li a{padding:0px; display:block; color:#fff!important; height:40px;}
#countryList li a span.clientalign{display: flex;
    width: 100px;
    float: left;
    line-height: 15px;
    padding: 4px 0px;
    flex-direction: row;
    justify-content: flex-start;
    align-items: center;
    align-content: center;
    min-height: 40px; background:#038ee0;
    margin-left: 0;
    padding-left: 5px;}
    
    #countryList li a span.entityname{display: flex;
    width:180px;
    float: left;
    line-height: 15px;
    padding: 4px 0px; font-size:13px;
    flex-direction: row;
    justify-content: flex-start;
    align-items: center;
    align-content: center;
    min-height: 38px;
    margin-left: 10px;}
    .mt8{margin-top:8px!important;}
    .mt7{margin-top:7px!important;}
.card .nav-tabs li.subtab1 a,.card .nav-tabs li.subtab2 a,.card .nav-tabs li.subtab3 a,.card .nav-tabs li.subtab4 a,.card .nav-tabs li.subtab5 a{
    padding:10px;
}
.card .nav-tabs li.subtab1{
    width: auto !important;
}
.card .nav-tabs li.subtab2{
    width:auto !important;
}
.card .nav-tabs li.subtab3{
    width:auto !important;
}
.card .nav-tabs li.subtab4{
    width:auto !important;
}
.card .nav-tabs li.subtab5{
    width:auto !important;
}
#myTab02 li{
    width:18% !important;
}
.tab-pane.fade{position:absolute; width:100%;}
.tab-pane.fade.active.in{position:relative;}
@media  only screen and (max-width: 1130px){
    .searchboxmain {
        margin-left: 0%;
        width: 28%;
        padding-top: 0px;
        position: absolute !important;
        float: left !important;
        margin-top: 0px !important;
    }
    input#country_name {
        width: 100% !important;
    }
}
 @media (min-width: 768px){
.modal-dialog {
    width: 800px;
    margin: 30px auto;
}
}
.btnannualreceipt{

    background: linear-gradient(#e7f3ff, #74b7fd);
    font-size: 11px;
    padding: 9px 10px;
    color: #000000;
    font-weight: 600;
    border: 1px solid #000;
    width: 100%;
    transition: 0.2s; line-height:25px;
    text-align: center;
    box-shadow: 0 4px 5px 0 rgba(0,0,0,0.14), 0 1px 10px 0 rgba(0,0,0,0.12), 0 2px 4px -1px rgba(0,0,0,0.3);
    -webkit-box-shadow: 0 4px 5px 0 rgba(0,0,0,0.14), 0 1px 10px 0 rgba(0,0,0,0.12), 0 2px 4px -1px rgba(0,0,0,0.3);

}
th label.form-label {
    margin-bottom: 0px;
}
.table-bordered, .table-bordered > tbody > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > td, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > thead > tr > th {
    border-color: #555!important;
}
.btnannualreceipt:hover, .btnannualreceipt.active {
    background: linear-gradient(#74b7fd, #e7f3ff); color: #000000;
    transition: 0.2s;
}
.new_company_name label {
    width: 180px;
}
.new_company_name p {
    width: auto;
}
.main_tabs li{
    width:13.4% !important;
}
.taxtable tr td{padding:8px; vertical-align:top;text-align:left;}
.professional_profession {
    width: 31%;
}
.professional_note {
    width: 39%;
}
.professional_expire_1, .professional_effective_1, .professional_license_1 {
    width: 13.5%;
}
.professional_expire_1 a, .professional_effective_1 a, .professional_license_1 a {
    padding: 8px 0px !important;
}
@media (max-width:1177px){
    .main_tabs li{
        width: 23.88% !important;
        margin: 3px 4px;
    }
}
@media (max-width:1050px){
    .professional_profession {
        width: 35%;
    }
    .professional_note {
        width: 58%;
    }
    .professional_state {
        width: 13%;
    }
    .professional_effective {
        width: 21%;
    }
    .professional_license {
        width: 20%;
    }
    .professional_expire {
        width: 20%;
    }   
    .professional_expire_1, .professional_effective_1, .professional_license_1 {
        width: 19%;
    }
    .professional_expire_1 a, .professional_effective_1 a, .professional_license_1 a {
        padding: 9px 0px !important;
    }
    .professional_expire_1 label, .professional_effective_1 label, .professional_license_1 label{
        display: block;
        margin-top: 5px;
    }
}
@media (max-width:990px){
    .main_tabs li{
        width: 23.3% !important;
        margin: 3px 4px;
    }
    .professional_expire_1, .professional_effective_1, .professional_license_1 {
        width: 31%;
    }
    #myTab02 li{
        width:20% !important;
    }
}
@media  only screen and (max-width: 600px){
    .professional_profession,.professional_state,.professional_effective,.professional_license,.professional_expire,.professional_note{
        width:98%;
    }
    #myTab02 li{
        width:auto !important;
    }
}
@media (max-width:515px){
    .main_tabs li {
        width: 32% !important;
        margin: 3px 2px;
    }
}
</style>
<div class="content-wrapper">
   
	<div class="page-title">
	    <div class="searchboxmain">
	     <input type="text" name="search" id="country_name" class="form-control" placeholder="Search Client">	  <a class="btn-action btn-view-edit btn-primary" href="https://financialservicecenter.net/fac-Bhavesh-0554/workrecord">Reset</a>
                 <?php echo e(csrf_field()); ?>

                 <ul id="countryList"></ul>
    </div>
                 
		<h1>Work Record</h1>
		<div class="clear"></div>
	</div>
<div class="clear"></div>
	<div class="row"> 
<div class="col-md-12">
			<div class="box box-success mbtmzero">
			   
			      <div class="box-header" style="padding-left:0px; display:none">
                   <div class="col-md-9" style="">
                          <div class="row">
                          <div class="col-md-4">
                             <!-- <div class=""><label style="margin-left:2%;margin-top: 11px;">Search : </label></div> 
                           <input type="text" name="search" id="country_name" class="form-control" placeholder="Search Client">	 
                             <?php echo e(csrf_field()); ?>

                             <ul id="countryList"></ul>-->
                             </div>
                             <div class="col-md-4">   <div class=""><label style="margin-left:2%;margin-top: 11px;">	&nbsp;</label></div>
                                
                                 </div>
                            </div>
                     </div>
                 </div>
             <?php if(!empty($common->filename)): ?>
      <section class="content-header" style="margin-top:8px;">
      <div class="new-page-title" style="padding:0px;">
         <div class="new-page-title-tab">
            <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
               <div class="new_client_id">
                  <p><?php echo e($common->filename); ?> </p>
               </div>
            </div>
              <div class="col-lg-5 col-md-9 col-sm-9 col-xs-12">
          <div class="newcmpname">
          <?php if($common->business_id=='6'): ?>
        <div class="new_company_name">
            <label> Name :</label> <?php echo e(ucwords($common->nametype)); ?>   <?php echo e($common->first_name); ?> <?php echo e($common->middle_name); ?> <?php echo e($common->last_name); ?>  </div>
          <?php else: ?>
         <div class="new_company_name" style="margin-top:6px !important;">
            <label>Company Name :</label>
            <p><?php echo e($common->company_name); ?></p>
         </div>
         <div class="new_company_name">
            <label>Business Name :</label>
            <p><?php echo e($common->business_name); ?></p>
         </div>
         <?php endif; ?>
         </div>
      </div>
    
            <div class="col-lg-5 col-md-10 col-xs-12">
               <div class="new_images_sec">
                  <?php if(empty($common->business_cat_id)): ?> 
                  <div class="col-md-12 col-sm-12 col-xs-12">
                     <?php else: ?> 
                     <?php if(empty($common->business_brand_category_id)): ?>
                     <div class="col-md-4 col-sm-4 col-xs-4">
                        <?php else: ?> 
                        <div class="col-md-4 col-sm-4 col-xs-4"> 
                           <?php endif; ?>
                           <?php endif; ?>
                           <?php $__currentLoopData = $business; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $busi): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                           <?php if($busi->id==$common->business_id): ?>
                           <img src="<?php echo e(url('public/frontcss/images/')); ?>/<?php echo e($busi->newimage); ?>" class="img-responsive" >
                           <?php if($common->business_id !='6'): ?>
                            <div class="arrow" style="float: right; position: absolute;  top: 15px; right: 8px;"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></div>
                            <?php endif; ?>
                        </div>
                        <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php $__currentLoopData = $category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>                                        
                        <?php if($common->business_cat_id==$cate->id): ?>
                        <?php if(empty($common->business_brand_category_id)): ?>
                       
                        <div class="col-md-4 col-sm-4 col-xs-4">
                           <?php else: ?> 
                          <!-- <div class="arrow">
                              <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                           </div>-->
                           <div class="col-md-4 col-sm-4 col-xs-4"> 
                              <?php endif; ?>
                              <img src="<?php echo e(url('public/category')); ?>/<?php echo e($cate->business_cat_image); ?>" alt="" class="img-responsive" />
                           </div>
                           <?php endif; ?>
                           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                           <?php $__currentLoopData = $cb; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bb1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>                                     
                           <?php if($common->business_brand_category_id == $bb1->id): ?>
                          
                           <div class="col-md-4 col-sm-4 col-xs-4 lastimgbox">
                                <div class="arrow" style="position: absolute; left: -6px; top: 15px;">
                              <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                           </div>
                              <img src="<?php echo e(url('public/businessbrandcategory')); ?>/<?php echo e($bb1->business_brand_category_image); ?>" alt="" class="img-responsive" />
                           </div>
                           <?php endif; ?>
                           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                     </div>
                  </div>
                  </div>
                  </div>
                  </div>
                  </div>
                   <div class="clear"></div>
   </section>
   <div class="clear"></div>
  
   
    <?php else: ?>

	<?php endif; ?>
				<div class="col-md-12" style="margin-top:-5px; padding:0px;">
					<?php if(!empty($common->filename)): ?>
				     <div class="panel with-nav-tabs panel-primary" style="padding:0px 10px;">
                       <div class="panel-heading">
                       <ul class="nav nav-tabs main_tabs" style="padding:5px !important;margin:5px;" id="myTab">
                           <?php if($common->business_id !='6'): ?>
                       <li <?php if(empty($_REQUEST['action'])) { ?> class="active" <?php } ?> ><a href="#tab1primary" data-toggle="tab">Corporation</a></li>
                       <li><a href="#tab2primary" data-toggle="tab">License</a></li>
                       <?php endif; ?>
                       
                       <li <?php if(isset($_REQUEST['action']) && $_REQUEST['action'] =='tax') { ?>class="active"<?php } ?> ><a href="#tab3primary" data-toggle="tab">Taxation</a></li>
                       <li><a href="#tab4primary" data-toggle="tab">Banking </a></li>
                       <li><a href="#tab5primary" data-toggle="tab">Income</a></li>
                       <li><a href="#tab6primary" data-toggle="tab">Expense </a></li>
                       <li <?php if($common->business_id =='6') { ?>class="active" <?php } ?>><a href="#tab8primary" data-toggle="tab">Others</a></li>
                       </ul>
                    </div>
  
  <div class="tab-content">
        <?php if($common->business_id !='6'): ?>
            <div class="tab-pane fade in <?php if(empty($_REQUEST['action'])) { ?> active <?php } ?>  newcheckbox" id="tab1primary">
          <div style="padding:10px;padding-left: 0px;padding-top: 5px;">
              <!-- <div class="col-md-4">Client ID &nbsp;&nbsp;&nbsp;&nbsp;      Client Name</div>
               <div class="col-md-4">Icone of Business</div>!-->
          <div class="clear"></div>
             <form method="post" action="https://financialservicecenter.net/fac-Bhavesh-0554/workrecord/fetch1" enctype="multipart/form-data">
                <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                <input type="hidden" name="clientsid" id="clientsid" value="<?php echo $common->id;?>">
                <div class="headerbox">
                <div class="form-group row  card" style="background: #e0f1fd !important; border: 1px solid #3598dc !important; margin-right:5px; margin-left:5px;margin-bottom:0px;">
                    <div class="col-lg-4 col-md-12" style="padding-top:10px; padding-right:0px;display: inline-block;">
                        <label class="col-lg-1 col-xs-4 text-right padtop7 wstate1" style="padding-left: 0px; margin-top:10px;">State :</label> 
                        <div class="col-md-3 wstate2 col-xs-3" style="padding-left: 0px;padding-right: 0px;">
                            <input type="text" value="<?php echo e($common->formation_register_entity); ?>" class="form-control fsc-input" readonly/>
                        </div>
                        <label class="col-lg-2 col-xs-4 text-right padtop7 wcontrol1" style="padding-left: 0px;margin-top:10px;">Control #:</label> 
                        <div class="col-md-3 wcontrol2 col-xs-3" style="padding-left: 0px;padding-right: 0px;">
                            <input type="text" value="<?php echo e($common->contact_number); ?>" class="form-control fsc-input" readonly/>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-12" style="padding-right:0px;">
                        <div class="panel-heading">
                            <ul class="nav nav-tabs" id="myTab2" style="padding:5px 0px !important;float: right;margin-right: 5px !important;">
                                <li class="subtab1" <?php if(empty($_REQUEST['action'])) { ?> class="active subtab1" <?php } ?>><a href="#subtab1primary" class="" data-toggle="tab">Corporation Renewal</a></li>
                                <li class="subtab2"><a href="#subtab2primary" class="" data-toggle="tab">Share Transfer</a></li>
                                <li class="subtab3"><a href="#subtab3primary" class="" data-toggle="tab">Amendment</a></li>
                                <li class="subtab4"><a href="#subtab4primary" class="" data-toggle="tab">Minutes</a></li>
                                <li class="subtab5"><a href="#subtab5primary" class="" data-toggle="tab">Docs</a></li>
                            </ul>
                        </div>
                    </div>
              </div>
          
              <div class="tab-content" style="padding-top:5px; padding-left:0px;padding-right:0px;">
                   <div class="tab-pane fade in <?php if(empty($_REQUEST['action'])) { ?> active <?php } ?>  newcheckbox " id="subtab1primary">
                       <div class="col-md-12 padzero">
                            <div class="col-lg-4 col-md-6">
                             <div class="form-group">
                                   <label class="col-md-7 padtop7 mt8" style="padding-left: 14px;padding-right: 0px;" >Year :</label>
                                   <div class="col-md-5" >
                                       <div class="dropdown">
                                        <?php
                                            if(isset($formation->formation_yearvalue)!='')
                                            {
                                                ?>
                                                <input type="text" class="form-control"  value="<?php echo e($formation->formation_yearvalue); ?>" readonly />
                                                <?php
                                            }
                                            else
                                            {
                                                $now=date('Y')-1;
                                              $cnow=date('Y');
                                               ?>
                                                    <input type="text" name="common_year"  value="<?php echo e($cnow); ?>" class="form-control"  readonly />
                                               <?php
                                            }
                                        ?>       
                                       
                                   </div>
                                   </div>
                                   <div class="clear"></div>
                               </div>
                               </div>
                            
                                <div class="col-lg-4 col-md-6">
                                   <div class="form-group ">
                                       <label class="col-md-5 padtop7 mt8 text-right" style="padding-right:0px;">Due Date :</label>
                                       <div class="col-md-6">
                                           <?php
                                               
                                            if(isset($formation->formation_yearvalue)!='' && isset($formation->formation_yearvalue)!=null)
                                            {
                                              //  exit('22');
                                              // $int = (int)$formation->formation_yearvalue
                                               $int = (int)substr ($formation->formation_yearvalue, -4);
                                               $aa=$int+1;
                                               $cyears=$aa.'-04-01';
                                               $tdatess=date('Y-m-d');
                                              $stryear=   date('Y-m-d',strtotime($cyears));
                                               $todaydates=   date('Y-m-d',strtotime($tdatess));
                                           
                                              
                                               ?>
                                                    <input type="text" name="common_year"  class="form-control" readonly value="Apr-01-<?php echo $aa; ?>" />
                                                    <input type="hidden" name="cyear"  value="<?php echo $cyears; ?>" />
                                                    
                                               <?php
                                            }
                                            else
                                            {
                                            //    exit('33');
                                                $now=date('Y');
                                                 $cyears=$cnow.'-04-01';
                                                   $tdatess=date('Y-m-d');
                                             
                                              $stryear= date('Y-m-d',strtotime($cyears));
                                              $todaydates= date('Y-m-d',strtotime($tdatess));
                                              
                                              
                                               ?>
                                                    <input type="text" name="common_year" class="form-control" value="Apr-01-<?php echo e($cnow); ?>" readonly />
                                                    <input type="hidden" name="cyear"  value="<?php echo $cnow.'-04-01'; ?>" />
                                                    
                                               <?php
                                            }
                                           ?>
                                           
                                       </div>
                                   </div>
                               </div>
                               
                               <div class="col-lg-4 col-md-12">
                               <div class="form-group">
                                   <label class="col-lg-6 col-md-3 padtop7 mt7 left_1200" style="text-align: right;">Any Changes? :</label>
                                   <div class="col-lg-5 col-md-9" style="padding-right: 0px;">
                                       <div class="dropdown" style=" padding-top:6px;">
                                           
                                        <?php
                                        if(isset($common->work_changes)!='')
                                        {
                                            ?>
                                                <input type="radio" name="work_changes" class="checkradio" value="Yes" <?php if(isset($common->work_changes) && $common->work_changes =='Yes') { echo 'checked';}?>/> Yes  &nbsp;&nbsp;&nbsp; 
                                        <input class="checkradio" name="work_changes" type="radio" value="No" <?php if(isset($common->work_changes) && $common->work_changes =='No') { echo 'checked';}?>/> No  <br/>    
                                            <?php
                                        }
                                        else
                                        {
                                            ?>
                                            <input type="radio" name="work_changes" class="checkradio" value="Yes"/> Yes  &nbsp;&nbsp;&nbsp; 
                                            <input class="checkradio" name="work_changes" type="radio" value="No"/> No  <br/>    
                                            <?php
                                        }
                                        ?>
                                        
                                   </div>
                                   </div>
                                   <div class="clear"></div>
                               </div>
                               </div>
                               
                              
                               <div class="col-md-2" style="padding-left:0px; padding-top:3px;">
                                   <?php 
                                  /* if($formationsetup->state == $common->formation_state) {
                                       ?>
                                       <a class="btn_new btn-renew nn" target="_blank" href="<?php echo $formationsetup->renewalwebsite;?>">Renew Here</a>
                                       <?php
                                   }*/
                                   ?>
                               </div>
                                <div class="clear"></div>
                                
                       </div>
                       
                       <div class="hideradio" <?php if(isset($common->work_changes) == 'No' && $common->work_changes == '') { ?> style="display:none;" <?php } ?>>
                            <div class="col-md-12">
                                <div class="officermainbox" style="background:#f2f2f2; margin-top:0px;">
                                        <div class="col-md-12 text-center Branch">
                                           <strong style="font-size:16px">On File (On Record)</strong>
                                        </div>
                                     <div class="hideradio" <?php if(isset($common->work_changes) && $common->work_changes == 'No') { ?> style="display:none;" <?php } ?>>  
                                     <div class="clear clearfix"></div>
                                     <div class="col-md-12" style="background:#ffffff;border:1px solid #d1d1d1;">  
                                        <div class="col-md-6">
                                            <h4>Address On File</h4>
                                            <div class="form-group" id="addresschangeto" style="border:1px solid #ccc; padding:10px;">
                                           
                                               <div class="form-group row" style="margin-bottom: 5px">
                                                <label class="col-md-12 padtop7">Address  </label>
                                               <div class="col-md-12" >
                                                  <input type="text"  value="<?php echo $common->formation_address;?>" class="form-control" readonly/>
                                               </div>
                                               <div class="col-md-4 padtop7 mt8">
                                                  
                                                     <input type="text" value="<?php echo $common->formation_city;?>" class="form-control" placeholder="City" readonly/>
                                               </div>
                                                <div class="col-md-4 padtop7 mt8">
                                                     <input type="text" class="form-control"  value="<?php echo $common->formation_state;?>" placeholder="State" readonly/>
                                               </div>
                                                <div class="col-md-4 padtop7 mt8">
                                                     <input type="text" class="form-control"  placeholder="Zip"  value="<?php echo $common->formation_zip;?>" readonly />
                                               </div>
                                               <div class="clear"></div>
                                               </div>
                                            </div>
                                        
                                        </div>
                                        <div class="col-md-6">
                                          <h4>Officer On File</h4>
                                         <div class="form-group">
                                       <div class="col-md-12 padzero">
                                          <table class="table table-bordered">
                                              <tr><th>First Name</th><th>Last Name</th><th>Position</th></tr>
                                              <?php //print_r($shareholders);exit;?>
                                               <?php $__currentLoopData = $shareholders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ak): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                               
                                           <?php if($ak->agent_fname1==NULL): ?>
                                           <?php else: ?> 
                                           <?php
                                           if($ak->agent_position =='sec')
                                           {
                                               $positions='CEO / CFO / Sec.';
                                           }
                                           else
                                           {
                                              $positions=$ak->agent_position; 
                                           }
                                           ?>
                                            <style>
                                              .input_fields_wrap_shareholder{ display:none}   
                                           </style>
                                              <tr>
                                                  <td><input class="form-control" name="agent_fname1[]" value="<?php echo e($ak->agent_fname1); ?>" readonly type="text" id="agent_fname1" placeholder="First name" class="textonly form-control" /></td>
                                                  <td><input class="form-control" name="agent_lname1[]" value="<?php echo e($ak->agent_lname1); ?>" readonly type="text" placeholder="Last Name" id="agent_lname1" class="textonly form-control" /></td>
                                                  <td><input class="form-control" name="agent_lname1[]" value="<?php echo e($positions); ?>" readonly type="text"></td>
                                                 <!-- <td> <select class="form-control" name="agent_position[]" id="agent_position11" disabled="disabled" >
                                                          <option value="">Position</option>
                                                          <option <?php if($ak->agent_position=='Agent'): ?> Selected hidden <?php else: ?> <?php if(empty($agent->id)): ?> <?php else: ?>  hidden <?php endif; ?> <?php endif; ?> value="Agent">Agent</option>
                                                          <option <?php if($ak->agent_position=='Agent'): ?> disabled <?php else: ?> <?php if($ak->agent_position=='Sec'): ?> disabled @ednif  <?php endif; ?> <?php endif; ?> <?php if($ak->agent_position=='CEO'): ?> selected hidden  <?php else: ?> <?php if(empty($ceo->id)): ?>   <?php else: ?>  hidden <?php endif; ?>  <?php endif; ?>  value="CEO">CEO</option>
                                                          <option <?php if(($ak->agent_position=='Secretary' && $ak->agent_position=='CEO' && $ak->agent_position=='CFO')): ?> hidden  <?php endif; ?>  <?php if($ak->agent_position=='Sec'): ?> Selected hidden <?php else: ?> <?php if(empty($sec->id)): ?>  <?php else: ?>  hidden  <?php endif; ?> <?php endif; ?> value="Sec">CEO / CFO / Sec.</option>
                                                          <option <?php if($ak->agent_position=='Agent'): ?> disabled <?php else: ?> <?php if($ak->agent_position=='Sec'): ?> disabled @ednif  <?php endif; ?> <?php endif; ?> <?php if($ak->agent_position=='CFO'): ?> Selected hidden <?php else: ?> <?php if(empty($cfo->id)): ?>  <?php else: ?>  hidden  <?php endif; ?> <?php endif; ?> value="CFO">CFO</option>
                                                          <option  <?php if($ak->agent_position=='Agent'): ?> disabled <?php else: ?> <?php if($ak->agent_position=='Sec'): ?> disabled @ednif  <?php endif; ?> <?php endif; ?> <?php if($ak->agent_position=='Secretary'): ?> Selected hidden <?php else: ?> <?php if(empty($sece->id)): ?>  <?php else: ?>  hidden  <?php endif; ?>  <?php endif; ?>  value="Secretary" >Secretary</option>
                                                       </select>
                                                       </td>!-->
                                                  </tr>
                                           <?php endif; ?>
                                           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                          </table>
                                       </div>
                                       <div class="clear"></div>
                                       </div>  
                                       </div>  
                                    </div>    
                                        <div class="clear clearfix"></div>
                                       <div class="hrdivider"></div>
                                       
                                    <div class="col-md-12" style="background:#ffffff;border:1px solid #d1d1d1;">    
                                     <div class="col-md-6">  
                                       <div class="hideradio" <?php if(isset($common->work_changes) && $common->work_changes == 'No') { ?> style="display:none;" <?php } ?>>
                                            <h4>Address Change To</h4>
                                            <div class="form-group" id="addresschangeto" style="border:1px solid #ccc; padding:10px;">
                                           
                                                     <div class="form-group row" style="margin-bottom: 5px">
                                               <label class="col-md-12 padtop7">Address  </label>
                                               <div class="col-md-12 " >
                                                  <input type="text" class="form-control" name="common_address" id="common_address" value="<?php echo $common->work_address;?>"/>
                                               </div>
                                               <div class="col-md-4 padtop7 mt8">
                                                     <input type="text" class="form-control" name="common_city" id="common_city"  value="<?php echo $common->work_city;?>" placeholder="City"/>
                                               </div>
                                                <div class="col-md-4 padtop7 mt8">
                                                      <select name="common_state" id="common_state" class="form-control fsc-input">
                                                                  <option value="">State</option>
                                                                  <option value='AK' <?php if($common->work_state =='AK') { echo 'Selected';} ?>>AK</option>
                                                                  <option value='AS' <?php if($common->work_state =='AS') { echo 'Selected';} ?>>AS</option>
                                                                  <option value='AZ' <?php if($common->work_state =='AZ') { echo 'Selected';} ?>>AZ</option>
                                                                  <option value='AR' <?php if($common->work_state =='AR') { echo 'Selected';} ?>>AR</option>
                                                                  <option value='CA' <?php if($common->work_state =='CA') { echo 'Selected';} ?>>CA</option>
                                                                  <option value='CO' <?php if($common->work_state =='CO') { echo 'Selected';} ?>>CO</option>
                                                                  <option value='CT' <?php if($common->work_state =='CT') { echo 'Selected';} ?>>CT</option>
                                                                  <option value='DE' <?php if($common->work_state =='DE') { echo 'Selected';} ?>>DE</option>
                                                                  <option value='DC' <?php if($common->work_state =='DC') { echo 'Selected';} ?>>DC</option>
                                                                  <option value='FM' <?php if($common->work_state =='FM') { echo 'Selected';} ?>>FM</option>
                                                                  <option value='FL' <?php if($common->work_state =='FL') { echo 'Selected';} ?>>FL</option>
                                                                  <option value='GA' <?php if($common->work_state =='GA') { echo 'Selected';} ?>>GA</option>
                                                                  <option value='GU' <?php if($common->work_state =='GU') { echo 'Selected';} ?>>GU</option>
                                                                  <option value='HI' <?php if($common->work_state =='HI') { echo 'Selected';} ?>>HI</option>
                                                                  <option value='ID' <?php if($common->work_state =='ID') { echo 'Selected';} ?>>ID</option>
                                                                  <option value='IL' <?php if($common->work_state =='IL') { echo 'Selected';} ?>>IL</option>
                                                                  <option value='IN' <?php if($common->work_state =='IN') { echo 'Selected';} ?>>IN</option>
                                                                  <option value='IA' <?php if($common->work_state =='IA') { echo 'Selected';} ?>>IA</option>
                                                                  <option value='KS' <?php if($common->work_state =='KS') { echo 'Selected';} ?>>KS</option>
                                                                  <option value='KY' <?php if($common->work_state =='KY') { echo 'Selected';} ?>>KY</option>
                                                                  <option value='LA' <?php if($common->work_state =='LA') { echo 'Selected';} ?>>LA</option>
                                                                  <option value='ME' <?php if($common->work_state =='ME') { echo 'Selected';} ?>>ME</option>
                                                                  <option value='MH' <?php if($common->work_state =='MH') { echo 'Selected';} ?>>MH</option>
                                                                  <option value='MD' <?php if($common->work_state =='MD') { echo 'Selected';} ?>>MD</option>
                                                                  <option value='MA' <?php if($common->work_state =='MA') { echo 'Selected';} ?>>MA</option>
                                                                  <option value='MI' <?php if($common->work_state =='MI') { echo 'Selected';} ?>>MI</option>
                                                                  <option value='MN' <?php if($common->work_state =='MN') { echo 'Selected';} ?>>MN</option>
                                                                  <option value='MS' <?php if($common->work_state =='MS') { echo 'Selected';} ?>>MS</option>
                                                                  <option value='MO' <?php if($common->work_state =='MO') { echo 'Selected';} ?>>MO</option>
                                                                  <option value='MT' <?php if($common->work_state =='MT') { echo 'Selected';} ?>>MT</option>
                                                                  <option value='NE' <?php if($common->work_state =='NE') { echo 'Selected';} ?>>NE</option>
                                                                  <option value='NV' <?php if($common->work_state =='NV') { echo 'Selected';} ?>>NV</option>
                                                                  <option value='NH' <?php if($common->work_state =='NH') { echo 'Selected';} ?>>NH</option>
                                                                  <option value='NJ' <?php if($common->work_state =='NJ') { echo 'Selected';} ?>>NJ</option>
                                                                  <option value='NM' <?php if($common->work_state =='NM') { echo 'Selected';} ?>>NM</option>
                                                                  <option value='NY' <?php if($common->work_state =='NY') { echo 'Selected';} ?>>NY</option>
                                                                  <option value='NC' <?php if($common->work_state =='NC') { echo 'Selected';} ?>>NC</option>
                                                                  <option value='ND' <?php if($common->work_state =='ND') { echo 'Selected';} ?>>ND</option>
                                                                  <option value='MP' <?php if($common->work_state =='MP') { echo 'Selected';} ?>>MP</option>
                                                                  <option value='OH' <?php if($common->work_state =='OH') { echo 'Selected';} ?>>OH</option>
                                                                  <option value='OK' <?php if($common->work_state =='OK') { echo 'Selected';} ?>>OK</option>
                                                                  <option value='OR' <?php if($common->work_state =='OR') { echo 'Selected';} ?>>OR</option>
                                                                  <option value='PW' <?php if($common->work_state =='PW') { echo 'Selected';} ?>>PW</option>
                                                                  <option value='PA' <?php if($common->work_state =='PA') { echo 'Selected';} ?>>PA</option>
                                                                  <option value='PR' <?php if($common->work_state =='PR') { echo 'Selected';} ?>>PR</option>
                                                                  <option value='RI' <?php if($common->work_state =='RI') { echo 'Selected';} ?>>RI</option>
                                                                  <option value='SC' <?php if($common->work_state =='SC') { echo 'Selected';} ?>>SC</option>
                                                                  <option value='SD' <?php if($common->work_state =='SD') { echo 'Selected';} ?>>SD</option>
                                                                  <option value='TN' <?php if($common->work_state =='TN') { echo 'Selected';} ?>>TN</option>
                                                                  <option value='TX' <?php if($common->work_state =='TX') { echo 'Selected';} ?>>TX</option>
                                                                  <option value='UT' <?php if($common->work_state =='UT') { echo 'Selected';} ?>>UT</option>
                                                                  <option value='VT' <?php if($common->work_state =='VT') { echo 'Selected';} ?>>VT</option>
                                                                  <option value='VI' <?php if($common->work_state =='VI') { echo 'Selected';} ?>>VI</option>
                                                                  <option value='VA' <?php if($common->work_state =='VA') { echo 'Selected';} ?>>VA</option>
                                                                  <option value='WA' <?php if($common->work_state =='WA') { echo 'Selected';} ?>>WA</option>
                                                                  <option value='WV' <?php if($common->work_state =='WV') { echo 'Selected';} ?>>WV</option>
                                                                  <option value='WI' <?php if($common->work_state =='WI') { echo 'Selected';} ?>>WI</option>
                                                                  <option value='WY' <?php if($common->work_state =='WY') { echo 'Selected';} ?>>WY</option>
                                                               </select>
                                               </div>
                                                <div class="col-md-4 padtop7 mt8">
                                                     <input type="text" class="form-control" name="common_zip" id="common_zip" value="<?php echo $common->work_zip;?>" placeholder="Zip"/>
                                               </div>
                                               <div class="clear"></div>
                                               </div>
                                            </div>
                                               
                                        </div>
                                       
                                     </div>
                                     
                                        
                                    <div class="col-md-6">
                                        <div class="hideradio" <?php if(isset($common->work_changes) && $common->work_changes == 'No') { ?> style="display:none;" <?php } ?>>
                                         <h4 style="margin-top:10px;">Officer Change To</h4>
                                         <div class="form-group">
                                             <div class="col-md-12 padzero">
                                          <table class="table table-bordered" id="officerchange">
                                              <tr><th>First Name</th><th>Last Name</th><th>Position</th><th><a class="add-row"><i class="fa fa-plus"></i></a></th></tr>
                                            <style>
                                              .input_fields_wrap_shareholder{ display:none}   
                                           </style>
                                                <?php $__currentLoopData = $clientposition; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $clientpos): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <tr>
                                                  <td>
                                                      <input class="form-control" name="idss[]" value="<?php echo e($clientpos->id); ?>" type="hidden" id="id"/>
                                                      <input class="form-control" name="first_name[]" value="<?php echo e($clientpos->first_name); ?>" type="text" placeholder="First Name" id="first_name"  class="textonly form-control" /></td>
                                                  <td><input class="form-control" name="last_name[]" value="<?php echo e($clientpos->last_name); ?>"  type="text" placeholder="Last Name" id="last_name" class="textonly form-control" /></td>
                                                  <td>
                                                      <select class="form-control" name="position[]" id="position_1">
                                                        <?php if($clientpos->position): ?>
                                                          <option value="<?php echo e($clientpos->position); ?>"><?php echo e($clientpos->position); ?></option>
                                                        <?php else: ?>
                                                          <option>--Select Position--</option>
                                                        <?php endif; ?>
                                                          <option value="Agent">Agent</option>
                                                          <option  value="CEO">CEO</option>
                                                          <option value="Sec">CEO / CFO / Sec.</option>
                                                          <option value="CFO">CFO</option>
                                                          <option  value="Secretary" >Secretary</option>
                                                       </select>
                                                  </td>
                                                  <td style='vertical-align:middle;'></td>
                                                </tr>
                                                 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                          </table>
                                       </div>
                                       </div>  
                                       </div>
                                     </div>
                                     </div>
                                 </div><div class="clear"></div>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                       </div>
                       <div class="">
                           <div class="col-md-12 padzero">
                               <div class="">
                                   <div class="col-lg-4 col-md-6">
                                   <div class="form-group">
                                       <label class="col-md-7 padtop7 mt8 " style="padding-left: 14px;padding-right: 0px;">Renew For the period : </label>
                                       <div class="col-md-5">
                                           
                                              <input type="hidden" name="formation_client_id" value="<?php echo e($common->id); ?>">   
                                              <?php //echo $formation->formation_yearbox;?>
                                            <?php if(isset($formation->formation_yearbox)!='' && isset($formations->client_id)!=''): ?>
                                            
                                            <input type="hidden" name="formation_yearbox11" id="formation_yearbox11" value="<?php echo e($formation->formation_yearbox); ?>">
                                            <input type="hidden" name="formation_yearvalue_already" id="formation_yearvalue_already" 
                                            value="<?php echo e($formation->formation_yearvalue); ?>">
                                            <input type="hidden" name="formation_id" value="<?php echo e($formation->id); ?>">   
                                            
                                            <!--<select style="padding:0px !important;" class="form-control" id="formation_yearbox" name="formation_yearbox" required>-->
                                            <!--    <option value="">Select </option>-->
                                            <!--    <option value="1" <?php if($formation->formation_yearbox=='1'): ?> selected <?php endif; ?>>1 Year</option>-->
                                            <!--    <option value="2" <?php if($formation->formation_yearbox=='2'): ?> selected <?php endif; ?>>2 Year</option>-->
                                            <!--    <option value="3" <?php if($formation->formation_yearbox=='3'): ?> selected <?php endif; ?>>3 Year</option>-->
                                            <!--    <option value="4" <?php if($formation->formation_yearbox=='4'): ?> selected <?php endif; ?>>4 Year</option>-->
                                            <!--</select>-->
                                            <input class="form-control" type="textbox"  name="formation_yearbox" id="formation_yearbox" value="<?php echo e($formation->formation_yearbox); ?> Year" readonly>
                                            <?php elseif(isset($formations->client_id) && $formations->client_id !=''): ?>
                                            <input type="hidden" name="formation_id" value="<?php echo e($formations->id); ?>">   
                                            
                                            <select style="padding:0px !important;" class="form-control ss" id="formation_yearbox22" name="formation_yearbox" required>
                                                <option value="">Select </option>
                                                <option value="1">1 Year</option>
                                                <option value="2">2 Year</option>
                                                <option value="3">3 Year</option>
                                            </select>
                                            
                                            <?php else: ?>
                                            
                                            <select style="padding:0px !important;" class="form-control" id="formation_yearbox22" name="formation_yearbox" required>
                                                <option value="">Select </option>
                                                <option value="1">1 Year</option>
                                                <option value="2">2 Year</option>
                                                <option value="3">3 Year</option>
                                            </select>
                                            
                                            <?php endif; ?>
                                           
                                        </div>
                                    </div>
                                    </div>
                                    <div class="col-lg-4 col-md-6">
                                        <div class="">
                                            <label class="col-md-5 mt8 text-right" style="padding-left: 14px;padding-right: 0px;">Year : </label>
                                            <div class="col-md-6">
                                                <?php if(isset($formation->formation_yearvalue)!=''): ?>
                                                <input type="text" class="form-control" name="formation_yearvalue" id="formation_yearvalue" value="<?php echo e($formation->formation_yearvalue); ?>" readonly />
                                                <span style="display:none;color:red;" id="yeardalreadyExist">Record Already Exist!</span>
                                                <?php else: ?>
                                                <input type="text" class="form-control" name="formation_yearvalue" id="formation_yearvalue22" readonly />
                                                <span style="display:none;color:red;" id="yeardalreadyExist">Record Already Exist!</span>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-6">
                                        <div class="feeschargesbox">
                               <div class="form-group" style="margin-top:10px;">
                               <label class=" col-lg-6 col-md-7 padtop7 mt8 " >Paid By :</label>
                               <div class="col-lg-6 col-md-5"> <div class="dropdown">
                                    <?php if(isset($formation->record_paid_by)!=''): ?>
                                       <select class="form-control fsc-input paidby" name="record_paid_by" style="font-size:14px!important; height:30px; padding:0px 6px!important;">
                                           <option value="">Select</option>
                                           <option value="Client" <?php if($formation->record_paid_by =='Client') { echo 'Selected';} ?>>Client</option>
                                           <option value="FSC" <?php if($formation->record_paid_by =='FSC') { echo 'Selected';} ?>>FSC</option>
                                       </select>
                                    <?php else: ?>
                                    <select class="form-control fsc-input paidby" name="record_paid_by" style="font-size:14px!important; height:30px; padding:0px 6px!important;">
                                           <option value="">Select</option>
                                           <option value="Client">Client</option>
                                           <option value="FSC">FSC</option>
                                       </select>
                                    <?php endif; ?>
                                   </div>
                                </div>
                               <div class="clear"></div>
                            </div>
                            
                                    
                            <div class="clear"></div>
                            </div>
                            </div>
                            </div>
                                    </div>
                                    
                                    <div class="col-md-12" style="padding: 0px;margin-bottom:10px;">
                                    <div style="">
                                    <div class="col-lg-4 col-md-6">
                                       <div class="">
                                           <label class="col-md-7 padtop7 mt8 ">Annual Receipt :</label>
                                           <div class="col-md-5" style="padding-right:0px;">
                                               <div class="">
                                                    <div class="">      
                        							    <label class="file-upload btn btn-primary">
                        							    <?php if(isset($formation->annualreceipt)!=''): ?>     
                                                        <input type="file" class="form-control" name="annualreceipt" id="annualreceipt"/>
                                                        Browse for file </label>
                                                        <input type="hidden"  name="annualreceipt_1" value="<?php echo e($formation->annualreceipt); ?>"/>
                                                        <?php else: ?>
                                                        <input type="file" class="form-control" name="annualreceipt" id="annualreceipt"/>
                                                        Browse for file </label>
                                                        <?php endif; ?>
                                                    </div> 
                                                    
                                                    <?php if(isset($formation->annualreceipt)!='' && $formation->annualreceipt!=null): ?>     
                                                    <div class="col-md-7">
                                                        <a data-toggle="modal" num="Ann" class="btn btn-info btnannualreceipt btn-lg btn3d Certificate-btn btn3d btn-info Certificate-btn openBtnannual Pro-btn" 
                                                            style="padding: 0px;height: 34px;padding-top: 3px;">Annual Receipt</a>
                                                    </div>
                                                    <?php endif; ?>                                          
                                                </div> 
                                            </div> 
                                           <div class="clear"></div>
                                       </div>
                                       <div class="clear"></div>
                                   </div>
                                   
                                    <div class="col-lg-4 col-md-6">
                                       <div class="" style="margin-top:5px;">
                                           <label class="col-md-5 padtop7 mt8 text-right" style="padding-left: 14px;padding-right: 0px;">Officer :</label>
                                              <div class="col-md-5" style="padding-right:0px;">
                                              <div class="">
                                                <div class="">
        							                <label class="file-upload btn btn-primary">
        							            <?php if(isset($formation->formation_work_officer)!=''): ?>  
                                                    <input type="file" class="form-control" name="formation_work_officer" id="formation_work_officer"/>
                                                    Browse for file ... </label>
                                                    <input type="hidden" class="form-control" name="officers" value="<?php echo e($formation->formation_work_officer); ?>">
                                                
                                                <?php else: ?>
                                                <input type="file" class="form-control" name="formation_work_officer"  id="formation_work_officer"/>
                                                    Browse for file ... </label>
                                                <?php endif; ?>
                                                </div>
                                                
                                                <?php if(isset($formation->formation_work_officer)!='' && $formation->formation_work_officer!=null): ?>  
                                                  <div class="col-md-7">
                                                       <a data-toggle="modal" num="off" class="btn btnannualreceipt btn-info btn-lg btn3d Certificate-btn btn3d btn-info Certificate-btn openBtnofficer Pro-btn" 
                                                       style="padding: 0px;height: 34px; padding-top: 3px;">Officer</a>
                                                   
                                                  </div>
                                                <?php endif; ?>
                                              
                                                   </div> 
                                                   </div> 
                                           <div class="clear"></div>
                                       </div>
                                       <div class="clear"></div>
                                   </div>
                                    <?php if(isset($formation->record_status)!=''): ?>  
                                            
                                        <div class="col-lg-9 col-md-12">
                                       <div style="margin-top:5px;">
                                           <label class="col-md-3 mt8 left_991" style="padding-left: 14px;padding-right: 0px;">Status :</label>
                                           <div class="col-md-7" style="padding-right:0px;">
                                                <input type="text" class="form-control" name="record_status" value="<?php echo $formation->record_status;?>" readonly>
                                            </div>
                                           <div class="clear"></div>
                                       </div>
                                       <div class="clear"></div>
                                   </div>
                                   <?php else: ?>
                                         
                                           <div class="col-md-12 padzero">
                                                <input type="hidden" name="record_status" value="Active Compliance">
                                       
                                       <div class="clear"></div>
                                   </div>    
                                   <?php endif; ?>
                                   
                                   <div class="col-lg-9 col-md-12">
                                       <div style="margin-top:5px;">
                                           <label class="col-md-3 mt8 left_991" style="padding-left: 14px;padding-right: 0px;">Notes :</label>
                                           <div class="col-md-7" style="padding-right: 0px;">
                                             <?php if(isset($formation->record_note)!=''): ?>   
                                      <textarea class="form-control" name="record_note" style="height:85px;"><?php echo $formation->record_note;?> </textarea>
                                    <?php else: ?>
                                    <textarea class="form-control" name="record_note" style="height:85px;"></textarea>
                                    <?php endif; ?>
                                           </div>
                                          
                                           <div class="clear"></div>
                                       </div>
                                       <div class="clear"></div>
                                   </div>
                                   
                               </div>
                           </div>
                           <div class="col-md-4">
                               <div class="hidepaid">
                           <div class="form-group">
                               <label class="col-md-7 padtop7 mt8">Annual Fees: </label>
                               <div class="col-md-5">
                                    <?php if(isset($formation->formation_amount)!=''): ?>
                                    <input class="form-control annualfees" style="height:30px;" type="textbox"  name="formation_amount" id="formation_amount" value="<?php if($formation->formation_amount !=''): ?> $<?php echo e($formation->formation_amount); ?> <?php else: ?> <?php endif; ?>">
                                    <?php else: ?>
                                    <input class="form-control annualfees" style="height:30px;" type="textbox"  name="formation_amount" id="formation_amount">
                                    <?php endif; ?>
                               </div>
                               <div class="clear"></div>
                           </div>
                           <div class="form-group">
                               <label class="col-md-7 padtop7 mt8">Penalty Charges:</label>
                               <div class="col-md-5">
                                    <?php if(isset($formation->formation_penalty)!=''): ?>
                                    <input class="form-control penaltycharges" style="height:30px;" type="textbox"  name="formation_penalty" id="formation_penalty" value="<?php if($formation->formation_penalty !=''): ?> $<?php echo e($formation->formation_penalty); ?> <?php else: ?> <?php endif; ?>" >
                                    <?php else: ?>
                                    <input class="form-control penaltycharges" style="height:30px;" type="textbox"  name="formation_penalty" id="formation_penalty">
                                    <?php endif; ?>
                               </div>
                               <div class="clear"></div>
                           </div>
                           <div class="form-group" style="margin-top:10px;">
                                <label class="col-md-7 padtop7 mt8">Processing Charges:</label>
                                <?php if(isset($formation->work_processingfees)!=''): ?>
                                <div class="col-md-5"><input type="text" class="form-control processingfees" name="work_processingfees" value="<?php if($formation->work_processingfees !=''): ?> $<?php echo e($formation->work_processingfees); ?> <?php else: ?> <?php endif; ?>" style="height:30px;"/></div>
                                <?php else: ?>
                                <div class="col-md-5"><input type="text" class="form-control processingfees" name="work_processingfees" style="height:30px;"/></div>
                                <?php endif; ?>
                               <div class="clear"></div>
                           </div>
                           
                           <div class="form-group" style="margin-top:10px;">
                               <label class="col-md-7 padtop7 mt8">Total Amount:</label>
                               <div class="col-md-5">
                            
                                
                                        <?php if(isset($formation->formation_amount)!=''): ?>
                                        <input type="text" readonly class="form-control totalamt" name="record_totalamt" style="height:30px;" value="<?php if($formation->record_totalamt !=''): ?> $<?php echo e($formation->record_totalamt); ?> <?php else: ?> <?php endif; ?>"/>
                                        <?php else: ?>
                                    
                                        <input type="text" readonly class="form-control totalamt" name="record_totalamt" style="height:30px;"/>
                                 <?php endif; ?>
                               
                               </div>
                               <div class="clear"></div>
                            </div>
                            
                            <div class="form-group" style="margin-top:10px;">
                               <label class="col-md-7 padtop7 mt8">Payment Method :</label>
                               <div class="col-md-5">
                                   <?php if(isset($formation->formation_payment)!=''): ?>
                                   <input type="text" name="formation_payment" class="form-control" value="<?php echo $formation->formation_payment;?>" style="height:30px;"/></div>
                                   <?php else: ?>
                                   <input type="text" name="formation_payment" class="form-control" style="height:30px;"/></div>
                                   <?php endif; ?>
                               <div class="clear"></div>
                            </div>
                            <div class="form-group" style="margin-top:10px;">
                               <label class="col-md-7 padtop7 mt8">Paid Date  :</label>
                            <div class="col-md-5">
                                <?php if(isset($formation->paiddate)!=''): ?>
                               <input type="text" name="paiddate" class="form-control"  value="<?php if($formation->paiddate !='') { echo date('M-d Y',strtotime($formation->paiddate));}?>" id="paiddate" style="height:30px;"/>
                                <?php else: ?>
                                <input type="text" name="paiddate" class="form-control"  id="paiddate" style="height:30px;"/>
                               <?php endif; ?>
                            </div>
                           </div>
                           </div>
                       </div>
                       
                     
                       <div class="clear"></div>
                       </div>
                       
                        <div class="clear"></div>
                    
                        <!--<div class="row" style="margin-top:12px;">-->
                        <!--    <div class="form-group col-md-12">-->
                        <!--           <label class="col-md-2 padtop7" >Note : </label>-->
                        <!--           <div class="col-md-10">-->
                        <!--            <?php if(isset($formation->record_note)!=''): ?>   -->
                        <!--              <textarea class="form-control" name="record_note" style="height:70px;"><?php echo $formation->record_note;?> </textarea>-->
                        <!--            <?php else: ?>-->
                        <!--            <textarea class="form-control" name="record_note" style="height:70px;"></textarea>-->
                        <!--            <?php endif; ?>-->
                        <!--           </div>-->
                        <!--           <div class="clear"></div>-->
                        <!--    </div>-->
                        <!--</div>-->
                       
                           <div class="col-md-12">
                             <div class="form-group row">
                        <div class="card-footer">
							<div class="col-md-9">
							    <div class="col-lg-3 col-md-4"></div>
    							<div class="col-xs-3" style="width:155px">
    									<input class="btn_new_save btn-primary1 yeardalreadyExist1" disabled type="submit" value="Save" style="display:none;">
    									
    									<input class="btn_new_save btn-primary1 yeardalreadyExist2" type="submit" value="Save" style="disabled:true">
    							</div>
    							<div class="col-xs-3" style="width:155px">
    									<a class="btn_new_cancel" href="https://financialservicecenter.net/fac-Bhavesh-0554/workrecord">Cancel</a> 
    							</div>
							</div>
						</div>
                                </div>
                                   
                                <div class="clear"></div>
                                </div>
                            <div class="col-md-6">
                                  
                                
                            </div>
                              
                               <div class="clear"></div>
                           </div>
                           <div class="clear"></div>
                <div class="tab-pane fade newcheckbox" id="subtab2primary">
                   Share Transfer
                </div>
                <div class="clear"></div>
                    <div class="tab-pane fade newcheckbox" id="subtab3primary">
                   Amendment
                </div>
     
      <div class="clear"></div>
                    <div class="tab-pane fade  newcheckbox" id="subtab4primary">
                   Minutes
                </div>
                
                
            </form>    
    <div class="clear"></div>
                 <div class="tab-pane fade newcheckbox" id="subtab5primary">
                    <div class="col-md-12">
                        <div class="row form-group">
                            
                            <form method="post" class="form-horizontal" enctype="multipart/form-data" action="<?php echo e(route('workrecord.updatedocument')); ?>">
        							<?php echo e(csrf_field()); ?>

                                                
                                                    <div class="col-md-12">
                                                    <div class="row form-group">
                                                        <label class="col-md-2" style="padding-top:6px;">Document Name : </label>
                                                        <div class="col-md-3">
                                                            <input type="hidden" name="filename_id1" value="<?php echo e($common->filename); ?>">
                                                            <input type="hidden" name="client_id1" value="<?php echo e($common->id); ?>">
                                                           <select class="js-example-tags form-control" name="documentsname" id="vendor_product" required>
                                                              <?php $__currentLoopData = $document; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cur): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                              <option value="<?php echo e($cur->documentname); ?>"><?php echo e($cur->documentname); ?></option>
                                                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                           </select>
                                                           <?php if($errors->has('documentsname')): ?>
                                                           <span class="help-block">
                                                           <strong><?php echo e($errors->first('documentsname')); ?></strong>
                                                           </span>
                                                           <?php endif; ?>
                                                        </div>
                                                
                                                        <div class="col-md-1" style="padding-top:6px;"><a href="#" data-toggle="modal" data-target="#basicExampleModal" class="redius"><i class="fa fa-plus"></i></a>&nbsp;&nbsp;&nbsp; <a href="#" data-toggle="modal" data-target="#basicExampleModal3" class="redius"><i class="fa fa-minus"></i></a> </div>
                                                        <div class="col-md-1"></div>
                                                        <div class="col-md-4">
                                                            <label class="col-md-3" style="padding-top:6px;">Upload :</label>
                                                            <div class="col-md-9">
                                                                <input type="file" class="form-control" name="clientdocument"  required/>
                                                                 <label class="file-upload btn btn-primary">
                                                                    <!--<input type="file" class="form-control" type="file" name="license_copy" required/>-->
                                                                    <input type="file" class="form-control" name="clientdocument"  required/>
                                                                    Browse for file ... </label>
                                                            </div>
                                                        </div>
                                                        
                                                      </div>
                                                      <div class="row">
                                                       <label class="col-md-2 hide_991" style="padding-top:6px;"> </label>
                                                       <div class="col-xs-2" style=" padding-right:3px;width:155px;">
                                                           <input class="btn_new_save btn-primary1 primary1" type="submit" id="primary1" name="submit" value="Save">
                                                          </div>
                                                            <div class="col-xs-2" style="padding-left:3px;width:155px;">
                                                           <a class="btn_new_cancel" href="#">Cancel</a>
                                                        </div>
                                                        </div>
                                                    
                                                        
                                               </div>
                                               
                                               </form>
                                               
                                       <!--         <table class="table table-hover table-bordered dataTable no-footer">-->
                                       <!--             <thead>-->
                                       <!--             <tr>-->
                                       <!--                 <th style="width:50px">No.</th>-->
                                       <!--                 <th class="text-left">Document Name</th>-->
                                       <!--                 <th style="width:80px;">View</th>-->
                                       <!--                 <th style="width:80px;">Action</th>-->
                                       <!--             </tr>-->
                                       <!--             </thead>-->
                                       <!--             <tbody>-->
                                                    
                                       <!--             <tr>-->
                                       <!--                 <?php $__currentLoopData = $documentupload; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $doc1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>-->
                                       <!--                 <td class="text-center"><?php echo e($loop->index+1); ?></td>-->
                                       <!--                 <td class="text-left"><?php echo e($doc1->documentsname); ?></td>-->
                                       <!--                    <td class="text-center"><a href="<?php echo e(url('public/clientupload')); ?>/<?php echo e($doc1->clientdocument); ?>" target="_blank" class="btn btn-primary btn-sm" class="btn-action btn-view-edit btn-primary"><i class="fa fa-eye"></i></a></td>-->
                                                        <!--<td class="text-center"><?php echo e($doc1->clientdocument); ?></td>-->
                                       <!--                 <td class="text-center">-->
                                                            
                                       <!--                     <button type="button" class="btn-action btn-view-edit passingID" data-id="<?php echo e($doc1->id); ?>"><i class="fa fa-edit"></i></button>-->
                            							    <!--<a onclick="return confirm('Are you sure to remove this record?')" href="<?php echo e(route('workrecord.destroydocument',$doc1->id)); ?>" class="btn-action btn-delete"><i class="fa fa-trash"></i></a>-->
                                                            
                                       <!--                 </td>-->
                                       <!--             </tr>-->
                                       <!--             <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>-->
                                                    
                                       <!--             </tbody>-->
                                       <!--         </table>-->
                            
                            
                            
                            
                        </div>
                   </div>
                   
                </div>
     
                    </div>
              </div>
            
        </div>
        </div>
        <?php endif; ?> 
        
        <div class="modal fade" id="basicExampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Document Name</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <form action="" method="post" id="ajax">
            <?php echo e(csrf_field()); ?>

            <div class="modal-body">
               <input type="text" id="newopt" name="newopt"  class="form-control" placeholder="Document Name"/>
               <input type="hidden" id="filename_id" name="filename_id"  class="form-control" value="<?php echo e($common->filename); ?>"/>
               <input type="hidden" id="client_ids" name="client_ids"  class="form-control" value="<?php echo e($common->id); ?>"/>
            </div>
            <div class="modal-footer">
               <button type="button" id="addopt" class="btn btn-primary">Save</button>
               <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
         </form>
      </div>
   </div>
        </div>

        <div class="modal fade" id="basicExampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header" style="background:#038ee0;">
            <h4 class="modal-title" id="exampleModalLabel" style="text-align:center;color:#fff;">Documents<button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
               </button>
            </h4>
         </div>
         
         
         <div class="modal-body" style="background:#ffff99;padding:0px !important;">
            <div class="curency curency_ref" id="div">
               <?php $__currentLoopData = $document; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cur): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
               <div id="cur_<?php echo e($cur->id); ?>" class="col-md-12" style="border:1px solid;background:#ffff99;">
                  <div class="col-md-12" style="margin-top: 7px;margin-bottom:5px;">
                     <a class="delete" style="color:#000;" onclick="return confirm('Are you sure to remove this record?')" href="<?php echo e(route('workrecord.destroydocumenttitle',$cur->id)); ?>" id="<?php echo e($cur->id); ?>"><?php echo e($cur->documentname); ?>

                     <span class="pull-right"><i class="fa fa-trash btn btn-danger" style="padding:6px!important;"></i></span></a>
                  </div>
               </div>
               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>  
            </div>
         </div>
         
         <div class="modal-footer" style="text-align:center;">
            <button type="button" style="margin-top:10px;border:1px solid;" class="btn btn-secondary" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>

        
           <!-- <div class="tab-pane fade" id="tab2primary">-->
                                    
           <!--                                     <div class="form-group row  card" style="background: #e0f1fd !important; border: 1px solid #3598dc !important; margin-right:5px; margin-left:5px;margin-bottom:0px;">-->
                                      
           <!--                                       <div class="col-md-12" style="width: 63%; padding-right:0px;">-->
           <!--                                               <div class="panel-heading">-->
           <!--                                                   <ul class="nav nav-tabs" id="myTab2"  style="padding:5px !important;">-->
           <!--                                                       <li style=" width: 30.2% !important;" class="active"><a href="#subtab8primary" class="" data-toggle="tab">Business License</a></li>-->
           <!--                                                       <li style=" width: 33.2% !important;"><a href="#subtab9primary" class="" data-toggle="tab">Professional License</a></li>-->
                                                                   
           <!--                                                   </ul>-->
           <!--                                                   </div>-->
           <!--                                         </div>-->
           <!--                                     </div>-->
                                    
           <!--                                     <div class="tab-pane fade in active  newcheckbox" id="subtab8primary">-->
											<!--	    <div class="">-->
    							<!--						<div class="Branch" style="text-align:left;padding-left: 15px;">-->
    							<!--							<h1>Business License</h1>-->
    							<!--						</div>-->
    							<!--					</div>-->
												
											<!--	    <div class="col-md-12 col-sm-12 col-xs-12">-->
											<!--		<div class="form-group <?php echo e($errors->has('business_license_jurisdiction') ? ' has-error' : ''); ?>" >-->
											<!--			<div class="col-md-12 col-sm-12 col-xs-12">-->
											<!--				<div class="col-md-3">-->
											<!--					<label class="control-label" style="font-size:15px;"> Jurisdiction :</label>-->
											<!--					<select type="text" class="form-control" id="type_form3" name="business_license_jurisdiction">-->
											<!--						<option value="">Select</option>-->
											<!--						<option value="City" <?php if(Auth::user()->business_license_jurisdiction=='City'): ?> selected <?php endif; ?>>City</option>-->
											<!--						<option value="County" <?php if(Auth::user()->business_license_jurisdiction=='County'): ?> selected <?php endif; ?>>County</option>-->
											<!--					</select>-->
											<!--					<?php if($errors->has('business_license_jurisdiction')): ?>-->
											<!--					<span class="help-block">-->
											<!--					<strong><?php echo e($errors->first('business_license_jurisdiction')); ?></strong>-->
											<!--					</span>-->
											<!--					<?php endif; ?>-->
											<!--				</div>-->
											<!--				<div class="col-md-3">-->
											<!--					<label id="city-change" class="control-label" style="display:none;">City :</label> -->
											<!--					<label id="county-change" class="control-label">County :</label>-->
											<!--					<?php if(Auth::user()->business_license_jurisdiction=='County'): ?>-->
											<!--					<select type="text" class="form-control" id="business_license2" name="business_license2">-->
											<!--						<option value="">Select</option>-->
																	
																
											<!--					</select>-->
											<!--					<?php elseif(Auth::user()->business_license_jurisdiction=='City'): ?>-->
											<!--					<input type="text" class="form-control" id="business_license3"  name="business_license2" value="<?php echo e(Auth::user()->business_license2); ?>">-->
											<!--					<?php else: ?>-->
											<!--						<select type="text" class="form-control" id="business_license2" name="business_license2" <?php if(Auth::user()->business_license_jurisdiction=='City'): ?> style="display:none;" <?php endif; ?>>-->
											<!--						<option value="">Select</option>-->
											<!--						<?php $__currentLoopData = $taxstate; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>-->
											<!--						<option value="<?php echo e($v->county); ?>" <?php if($v->county==Auth::user()->business_license2): ?> selected <?php endif; ?>><?php echo e($v->county); ?></option>-->
											<!--						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>-->
											<!--					</select>-->
											<!--					<?php endif; ?>-->
											<!--						<div id="business_license4"></div>-->
											<!--						<div id="business_license5"></div>-->
											<!--				</div>-->
											<!--				<div class="col-md-2">-->
											<!--					<label id="city-change1" class="control-label" style="display:none;">City # :</label> -->
											<!--					<label id="county-change1" class="control-label">County # :</label>-->
											<!--					<input name="business_license1" placeholder="" value="<?php echo e(Auth::user()->business_license1); ?>" type="text" id="business_license1" class="form-control" />-->
											<!--					<?php if($errors->has('business_license1')): ?>-->
											<!--					<span class="help-block">-->
											<!--						<strong><?php echo e($errors->first('business_license1')); ?></strong>-->
											<!--					</span>-->
											<!--					<?php endif; ?>-->
											<!--				</div>-->
											<!--				<div class="col-md-2">-->
											<!--					<label id="county-change1" class="control-label">License # :</label>-->
											<!--					<input name="business_license3" placeholder=""  value="<?php echo e(Auth::user()->business_license3); ?>" type="text" id="business_license1" class="form-control" />-->
											<!--					<?php if($errors->has('business_license3')): ?>-->
											<!--					<span class="help-block">-->
											<!--						<strong><?php echo e($errors->first('business_license3')); ?></strong>-->
											<!--					</span>-->
											<!--					<?php endif; ?>-->
											<!--				</div>-->
											<!--				<div class="col-md-2">-->
											<!--					<label class="control-label">Expire Date :</label>-->
											<!--					<input type="text" class="form-control effective_date1" id="due_date2" name="due_date2" value="<?php echo e(Auth::user()->due_date2); ?>">-->
											<!--				</div>-->
											<!--			</div>-->
											<!--		</div>-->
											<!--		<div class="form-group">-->
											<!--			<div class="col-md-12 col-sm-12 col-xs-12">-->
											<!--				<div class="col-md-6">-->
											<!--					<label class="control-label" style="font-size:15px;padding:0;">Note :</label>-->
											<!--					<input name="notes" placeholder="Note" value="<?php echo e(Auth::user()->notes); ?>" type="text" id="" class="form-control">-->
											<!--				</div>	<?php $id1 = Auth::user()->business_license_jurisdiction;?>-->
															<!--<div class="col-md-2">-->
															<!--	<label></label>-->
															<!--	<a class="btn_new btn-view-license">License History</a>-->
															<!--</div>-->
															
											<!--				<div class="col-md-2">-->
											<!--					<label></label>-->
											<!--					<a style="display:block;" data-toggle="modal" num="<?php echo e($id1); ?>" class="btn_new openBtn btn-view-license">View License</a>-->
											<!--				</div>-->
											<!--				<div class="col-md-2">-->
											<!--																					<style>.nn{ display:none !important}</style>-->
											<!--						<label></label>-->
											<!--				<a style="display:block;" href="" target="_blank" class="btn_new btn-renew">Renew Now</a>-->
															
											<!--					<label></label>-->
											<!--				<a href="#" class="btn_new btn-renew nn">Renew Now</a>-->
											<!--				</div>-->
											<!--			</div>-->
											<!--		</div>-->
											<!--	</div>-->
												
											<!--    </div>-->
												
												
											<!--	<div class="tab-pane fade  newcheckbox" id="subtab9primary">-->
    							<!--					<div class="">-->
    							<!--						<div class="Branch" style="text-align:left; padding-left:15px;">-->
    							<!--							<h1>Professional License</h1>-->
    							<!--						</div>-->
    							<!--					</div>-->
    							<!--					<br/>-->
    							<!--					<div class="">-->
    							<!--						<div class="field_wrapper">-->
    							<!--							<div id="field0">-->
    							<!--								<div class="professional_tabs_main">-->
    							<!--									<div class="professional_tabs">-->
    							<!--										<div class="professional_tab professional_profession">-->
    							<!--											<label>Profession :</label>-->
    							<!--											<select type="text" class="form-control-insu" id="" name="profession[]">-->
    							<!--												<option value="">Select</option>-->
    											
    							<!--											</select>-->
    							<!--										</div>-->
    							<!--										<div class="professional_tab professional_state">-->
    							<!--											<label>State :</label>-->
    							<!--											<select name="profession_state[]" id="profession_state" class="form-control-insu">-->
    							<!--											<option value="AK">AK</option>-->
    							<!--											<option value="AS">AS</option>-->
    							<!--											<option value="AZ">AZ</option>-->
    							<!--											<option value="AR">AR</option>-->
    							<!--											<option value="CA">CA</option>-->
    							<!--											<option value="CO">CO</option>-->
    							<!--											<option value="CT">CT</option>-->
    							<!--											<option value="DE">DE</option>-->
    							<!--											<option value="DC">DC</option>-->
    							<!--											<option value="FM">FM</option>-->
    							<!--											<option value="FL">FL</option>-->
    							<!--											<option value="GA">GA</option>-->
    							<!--											<option value="GU">GU</option>-->
    							<!--											<option value="HI">HI</option>-->
    							<!--											<option value="ID">ID</option>-->
    							<!--											<option value="IL">IL</option>-->
    							<!--											<option value="IN">IN</option>-->
    							<!--											<option value="IA">IA</option>-->
    							<!--											<option value="KS">KS</option>-->
    							<!--											<option value="KY">KY</option>-->
    							<!--											<option value="LA">LA</option>-->
    							<!--											<option value="ME">ME</option>-->
    							<!--											<option value="MH">MH</option>-->
    							<!--											<option value="MD">MD</option>-->
    							<!--											<option value="MA">MA</option>-->
    							<!--											<option value="MI">MI</option>-->
    							<!--											<option value="MN">MN</option>-->
    							<!--											<option value="MS">MS</option>-->
    							<!--											<option value="MO">MO</option>-->
    							<!--											<option value="MT">MT</option>-->
    							<!--											<option value="NE">NE</option>-->
    							<!--											<option value="NV">NV</option>-->
    							<!--											<option value="NH">NH</option>-->
    							<!--											<option value="NJ">NJ</option>-->
    							<!--											<option value="NM">NM</option>-->
    							<!--											<option value="NY">NY</option>-->
    							<!--											<option value="NC">NC</option>-->
    							<!--											<option value="ND">ND</option>-->
    							<!--											<option value="MP">MP</option>-->
    							<!--											<option value="OH">OH</option>-->
    							<!--											<option value="OK">OK</option>-->
    							<!--											<option value="OR">OR</option>-->
    							<!--											<option value="PW">PW</option>-->
    							<!--											<option value="PA">PA</option>-->
    							<!--											<option value="PR">PR</option>-->
    							<!--											<option value="RI">RI</option>-->
    							<!--											<option value="SC">SC</option>-->
    							<!--											<option value="SD">SD</option>-->
    							<!--											<option value="TN">TN</option>-->
    							<!--											<option value="TX">TX</option>-->
    							<!--											<option value="UT">UT</option>-->
    							<!--											<option value="VT">VT</option>-->
    							<!--											<option value="VI">VI</option>-->
    							<!--											<option value="VA">VA</option>-->
    							<!--											<option value="WA">WA</option>-->
    							<!--											<option value="WV">WV</option>-->
    							<!--											<option value="WI">WI</option>-->
    							<!--											<option value="WY">WY</option>-->
    							<!--											</select>-->
    							<!--										</div>-->
    							<!--										<div class="professional_tab professional_effective">-->
    							<!--											<label>Effective Date :</label>-->
    							<!--											<input name="profession_effective_date[]" placeholder="Effective Date" value="" type="text" id="profession_effective_date" class="form-control-insu effective_date1">-->
    							<!--										</div>-->
    							<!--										<div class="professional_tab professional_license">-->
    							<!--											<label>License # :</label>-->
    							<!--											<input name="profession_license[]" placeholder="License"  value="" type="text" id="profession_license" class="form-control-insu">-->
    							<!--										</div>-->
    							<!--										<div class="professional_tab professional_expire">-->
    							<!--											<label>Expire Date :</label>-->
    																			
    							<!--									<label></label>-->
    							<!--							  <input name="profession_epr_date[]" placeholder="Expire Date" value="" type="text" id="profession_epr_date" class="form-control-insu" readonly>-->
    														    
    													
    							<!--							<input name="profession_epr_date[]" placeholder="Expire Date" value="" type="text" id="profession_epr_date" class="form-control-insu  btn-success" readonly>-->
    							<!--										</div>-->
    																	
    																	
    																	
    							<!--										<div class="professional_tab professional_note">-->
    							<!--											<label>Note :</label>-->
    							<!--											<input name="profession_note[]" placeholder="Note"  value="" type="text" id="profession_note" class="form-control-insu">-->
    							<!--											<input name="profession_id[]" placeholder="Expire Date" value="" type="hidden"  class="form-control-insu">-->
    							<!--										</div>-->
    							<!--										<div class="professional_tab professional_state">-->
    							<!--											<label>CE :</label>-->
    																		
    							<!--											<input name="ce[]"  type="hidden" />-->
    							<!--											<input name="ce1[]"   type="checkbox"><label for="ce1" class="form-control-insu" style="background-color: transparent;background-image: none;border:transparent;"> Check-->
    							<!--											</label>-->
    							<!--										</div>-->
    																	
    							<!--										<div class="professional_tab  professional_effective_1">-->
    							<!--											<label></label>-->
    							<!--										    	<a data-toggle="modal-history" num="" class="btn_new btn-view-license openBtn1 Pro-btn">License History</a>-->
    							<!--										</div>-->
    							<!--										<div class="professional_tab professional_license_1">-->
    																	   
    							<!--									<label></label>-->
    							<!--							    <a data-toggle="modal" num="" class="btn_new btn-view-license openBtn Pro-btn">View License</a>-->
    														    
    							<!--								<label class="btn-success"></label>-->
    							<!--								 <a class="btn_new btn-view-license Pro-btn  btn-success">File Not Upload</a>-->
    							<!--							</div>-->
    							<!--										<div class="professional_tab  professional_expire_1">-->
    																	    
    																	    
    																	    
    							<!--									<label></label>-->
    							<!--							    <a href="" class="btn_new btn-renew" target='_blank'>Renew Now</a>-->
    							<!--							  															<label class="btn-success"></label>-->
    							<!--							<a href="javascript:void(0);" class="btn_new btn-renew btn-success">Renew Now</a> -->
    							<!--										</div>-->
    							<!--									</div>-->
    							<!--								</div>-->
    							<!--							</div>-->
    							<!--						</div>-->
    											
    							<!--				</div>-->
											
											<!--	</div>-->
												
												
												
												
												
												
											
												
											
											<!--</div>-->
									
											
			<div class="tab-pane fade" id="tab2primary">
                                    
                                                <div class="form-group row  card" style="background: #e0f1fd !important; border: 1px solid #3598dc !important; margin-right:5px; margin-left:5px;margin-bottom:0px;">
                                      
                                                  <div class="col-md-12" style="width: 100%; padding-right:0px;">
                                                          <div class="panel-heading">
                                                              <ul class="nav nav-tabs" id="myTab2"  style="padding:5px !important;">
                                                                  <li style=" width: auto !important;" class="active"><a href="#subtab8primary" class="" data-toggle="tab">Business License</a></li>
                                                                  <li style=" width: auto !important;"><a href="#subtab9primary" class="" data-toggle="tab">Professional License</a></li>
                                                                   
                                                              </ul>
                                                              </div>
                                                    </div>
                                                </div>
                                    <div class="clear"></div>
                                                <div class="tab-pane fade in active  newcheckbox" id="subtab8primary">
												    <div class="col-md-12">
    													<div class="Branch" style="text-align:left;padding-left: 15px;">
    														<h1 class="text-center">Business License History</h1>
    													</div>
    												</div>
    												
												    <div class="col-md-12 text-right">
												       <a href="#myModalbusinesspopup" class="btn_new btn-renew inlinebutton pull-right" data-toggle="modal" data-target="#myModalbusinesspopup" style="margin-right: 10px;width: 120px;font-weight: bold; padding: 5px 0 !important;border-radius:0px;">Add Record</a>
												        
												        <div class="clear"></div>
												        <div class="table-responsive">
												       <table class="table table-hover table-bordered dataTable no-footer">
												           <thead>
												               <tr>
												                   <th style="width:70px;">Year</th>
												                   <th style="width:130px;">License Gross</th>
												                   <th style="width:130px;">License Fee</th>
												                   <th style="width:110px;">License #</th>
												                   <th style="width:130px;">License Issue Date</th>
												                   <th style="width:100px;">License Copy</th>
												                   <th style="width:80px">Status</th>
												                   <th style="width:100px">Action</th>
												                   <th style="width:80px">Form</th>
												               </tr>
												           </thead>
												           <tbody>
												               <?php
												                $countbuslicense=count($buslicense);
												               ?>
												            <?php if($countbuslicense>0): ?>
												            
				                                            <tr>
												                 <?php $__currentLoopData = $buslicense; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bus_lic): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                												   	
												                   <td class="text-center"> <?php echo e($bus_lic->license_year); ?></td>
												                   
												                   <td class="text-center"><?php echo e($bus_lic->license_gross); ?></td>
												                   <td class="text-center"><?php echo e($bus_lic->license_fee); ?></td>
												                 
												                   <td> <?php echo e($bus_lic->license_no); ?></td>
												                   <td class="text-center"><?php echo date('m/d/Y',strtotime($bus_lic->license_renew_date))?></td>
												                   <td class="text-center"><a href="<?php echo e(url('public/adminupload')); ?>/<?php echo e($bus_lic->license_copy); ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></td>
												                   <td class="text-center"><?php echo e($bus_lic->license_status); ?></td>
												                   <td class="text-center">
												                        <a class="btn-action btn-view-edit"><i class="fa fa-edit"></i></a>
				                                                        <a class="btn-action btn-delete" href="#"><i class="fa fa-trash"></i></a>
												                   </td>
												                   <td class="text-center"><a href="#" class="btn btn-primary btn-sm">Create</a></td>
												                 
												             </tr>
												                 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
												             <?php else: ?>
												                    <tr><td style="text-align:center;" colspan="9">No records found</td></tr>
												             <?php endif; ?>
												           </tbody>
												           
												       </table>
												    </div>
												    </div>
												    
												    
												
											    </div>
												
												<div class="clear"></div>
												<div class="tab-pane fade  newcheckbox" id="subtab9primary">
    												<div class="col-md-12">
    													<div class="Branch" style="text-align:left; padding-left:15px;">
    														<h1>Professional License</h1>
    													</div>
    												</div>
    												<br/>
    												<div class="">
    													<div class="field_wrapper">
    														<div id="field0">
    															<div class="professional_tabs_main">
    																<div class="professional_tabs">
    																	<div class="professional_tab professional_profession">
    																		<label>Profession :</label>
    																		<select type="text" class="form-control-insu" id="" name="profession[]">
    																			<option value="">Select</option>
    											
    																		</select>
    																	</div>
    																	<div class="professional_tab professional_state">
    																		<label>State :</label>
    																		<select name="profession_state[]" id="profession_state" class="form-control-insu">
    																		<option value="AK">AK</option>
    																		<option value="AS">AS</option>
    																		<option value="AZ">AZ</option>
    																		<option value="AR">AR</option>
    																		<option value="CA">CA</option>
    																		<option value="CO">CO</option>
    																		<option value="CT">CT</option>
    																		<option value="DE">DE</option>
    																		<option value="DC">DC</option>
    																		<option value="FM">FM</option>
    																		<option value="FL">FL</option>
    																		<option value="GA">GA</option>
    																		<option value="GU">GU</option>
    																		<option value="HI">HI</option>
    																		<option value="ID">ID</option>
    																		<option value="IL">IL</option>
    																		<option value="IN">IN</option>
    																		<option value="IA">IA</option>
    																		<option value="KS">KS</option>
    																		<option value="KY">KY</option>
    																		<option value="LA">LA</option>
    																		<option value="ME">ME</option>
    																		<option value="MH">MH</option>
    																		<option value="MD">MD</option>
    																		<option value="MA">MA</option>
    																		<option value="MI">MI</option>
    																		<option value="MN">MN</option>
    																		<option value="MS">MS</option>
    																		<option value="MO">MO</option>
    																		<option value="MT">MT</option>
    																		<option value="NE">NE</option>
    																		<option value="NV">NV</option>
    																		<option value="NH">NH</option>
    																		<option value="NJ">NJ</option>
    																		<option value="NM">NM</option>
    																		<option value="NY">NY</option>
    																		<option value="NC">NC</option>
    																		<option value="ND">ND</option>
    																		<option value="MP">MP</option>
    																		<option value="OH">OH</option>
    																		<option value="OK">OK</option>
    																		<option value="OR">OR</option>
    																		<option value="PW">PW</option>
    																		<option value="PA">PA</option>
    																		<option value="PR">PR</option>
    																		<option value="RI">RI</option>
    																		<option value="SC">SC</option>
    																		<option value="SD">SD</option>
    																		<option value="TN">TN</option>
    																		<option value="TX">TX</option>
    																		<option value="UT">UT</option>
    																		<option value="VT">VT</option>
    																		<option value="VI">VI</option>
    																		<option value="VA">VA</option>
    																		<option value="WA">WA</option>
    																		<option value="WV">WV</option>
    																		<option value="WI">WI</option>
    																		<option value="WY">WY</option>
    																		</select>
    																	</div>
    																	<div class="professional_tab professional_effective history">
    																		<label>Effective Date :</label>
    																		<input name="profession_effective_date[]" placeholder="Effective Date" value="" type="text" id="profession_effective_date" class="form-control-insu effective_date1">
    																	</div>
    																	<div class="professional_tab professional_license">
    																		<label>License # :</label>
    																		<input name="profession_license[]" placeholder="License"  value="" type="text" id="profession_license" class="form-control-insu">
    																	</div>
    																	<div class="professional_tab professional_expire">
    																		<label>Expire Date :</label>
    																			
    																<label></label>
    														  <input name="profession_epr_date[]" placeholder="Expire Date" value="" type="text" id="profession_epr_date" class="form-control-insu" readonly>
    														    
    													
    														<input name="profession_epr_date[]" placeholder="Expire Date" value="" type="hidden" id="profession_epr_date" class="form-control-insu  btn-success" readonly>
    																	</div>
    																	
    																	
    																	
    																	<div class="professional_tab professional_note">
    																		<label>Note :</label>
    																		<input name="profession_note[]" placeholder="Note"  value="" type="text" id="profession_note" class="form-control-insu">
    																		<input name="profession_id[]" placeholder="Expire Date" value="" type="hidden"  class="form-control-insu">
    																	</div>
    																	<div class="professional_tab professional_state">
    																		<label>CE :</label>
    																		
    																		<input name="ce[]"  type="hidden" />
    																		<input name="ce1[]"   type="checkbox"><label for="ce1" class="form-control-insu" style="display: block;width: 100%;border-radius: 4px;height: 39px;padding: 6px 6px;font-size: 14px;line-height: 24px;color: #000;background-color: #fff;background-image: none;"> Check
    																		</label>
    																	</div>
    																	
    																	<div class="professional_tab  professional_effective_1">
    																		<label></label>
    																	    	<a data-toggle="modal-history" num="" class="btn_new btn-view-license openBtn1 Pro-btn">License History</a>
    																	</div>
    																	<div class="professional_tab professional_license_1">
    																	   
    																<label></label>
    														    <a data-toggle="modal" num="" class="btn_new btn-view-license openBtn Pro-btn">View License</a>
    														    
    															<label class="btn-success"></label>
    															 <a class="btn_new btn-view-license Pro-btn  btn-success">File Not Upload</a>
    														</div>
    																	<div class="professional_tab  professional_expire_1">
    																	    
    																	    
    																	    
    																<label></label>
    														    <a href="" class="btn_new btn-renew" target='_blank'>Renew Now</a>
    														  															<label class="btn-success"></label>
    														<a href="javascript:void(0);" class="btn_new btn-renew btn-success">Renew Now</a> 
    																	</div>
    																</div>
    															</div>
    														</div>
    													</div>
    											
    											</div>
											
												</div>
												<div class="clear"></div>
												
												
												
												
												
											
												
											
											</div>								
		    <div class="clear"></div>
		    
			<div class="tab-pane fade <?php if(isset($_REQUEST['action']) && $_REQUEST['action'] =='tax') { ?> in active<?php } ?>" id="tab3primary">
													
									       		
								    				
									        <div class="tab-pane fade in active  newcheckbox" id="subtab5primaryi">
									            
									            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        										    <div class="row">
        										        <a class="btn_new btn-renew btnaddrecord pull-right" data-toggle="modal" style="margin-bottom:10px;" data-target="#myModalAddrecord">Add Record</a>
            											<a class="btn btn-primary btn-renew pull-right" style="margin-right: 10px;width: 120px;font-weight: bold; padding: 5px 0 !important;border-radius:0px;" href="workstatus?id=<?php echo $common->id;?>">Work Status</a>
            											<div class="Branch">
            												<div class="col-md-4" style="text-align:left;">
            													
            												</div>
            												<div class="col-md-3">
            													<h1> Taxation </h1>
            												</div>
            												
            												<div class="col-md-4" style="text-align:right;">
            													
            												</div>
            												
            											</div>
            										</div>
        										    
            										<div class="clear"></div>
									                </div>
									            
    										    
                                            </div>    
                                            
									       
										
			</div>			
        
            <div class="tab-pane fade" id="tab4primary">
								     <div class="card">
							         <div class="form-group row  card" style="background: #e0f1fd !important; border: 1px solid #3598dc !important; margin-right:5px; margin-left:0px;margin-bottom:0px;">
                                      
                                      <div class="col-md-12" style="width: 100%; padding-right:0px;">
                                              <div class="panel-heading">
                                                  <ul class="nav nav-tabs" id="myTab02" style="padding:5px !important;">
                                                      <li style="" class=""><a href="#subtab5primaryi02" class="" data-toggle="tab" aria-expanded="true">Banking</a></li>
                                                      <li style=""><a href="#subtab6primary02" class="" data-toggle="tab" aria-expanded="true">Credit Card</a></li>
                                                      <li style=""><a href="#subtab7primary02" class="" data-toggle="tab" aria-expanded="true">Loan</a></li>
                                                       
                                                  </ul>
                                                  </div>
                                        </div>
                                    </div>
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane" id="subtab5primaryi02"> 
                                                <div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <a style="display:block; float:right; cursor:pointer;width:120px;" href="#myModalbankingpopup" class="btn_new btn-renew inlinebutton" data-toggle="modal" data-target="#myModalbankingpopup">Add Record</a>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="subtab6primary02"> 
                                                Credit Card
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="subtab7primary02"> 
                                                Loan
                                            </div>
                                        </div>
                  
							       </div>
								</div>
                <div class="clear">
            
            </div>
            
            <div class="tab-pane fade" id="tab5primary">
               <div class="row">
                   <div class="col-md-12">
                       <div class="Branch">	<h1>Income </h1></div>
                   </div>
                   <div class="col-md-12 text-right">
                       <!--<a class="btn_new btn-renew btnaddrecord" data-toggle="modal" data-target="#myModalIncome">Add Record</a>-->
                       <?php if($common->cid!=''): ?>
                       <a href="<?php echo e(route('workrecord.edit',$common->cid)); ?>" class="btn_new btn-renew btnaddrecord">Add Record</a>
                       <br/>
                       <?php endif; ?>
                   </div>
                   
                    <div class="col-md-12">
                                        
                    <div class="dividerbox"></div>
                    <?php
					foreach($incomes as $personal)
					{
					?>	
					<div class="panel-group" id="accordion" role="tablist"  style="margin-top:15px;">
                        <div class="panel panel-default">
                         
                            <div class="panel-heading" role="tab" id="headingtwo">
                                 <h4 class="panel-title" style="margin-bottom:0px;">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" class="collapsed" href="#collapse<?php echo $personal->id;?>">
                                    <span class="ac_name_first"><?php echo e($personal->wrkyear); ?>  <a></a></span>
                                    <i class="more-less glyphicon glyphicon-plus"></i>
                                    <i class="more-less glyphicon glyphicon-minus"></i>
                                   
                                    </a>
                                 </h4>
                            </div>
                      
                            <div style="clear:both;"></div>
                                
                            <div id="collapse<?php echo $personal->id;?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingtwo">
                                <div class="panel-body accordion-body" style="padding-top:15px;">
                                    <div class="table-responsive">
                                        <div class="table-responsive">
                                            <table class="table table-bordered tablestriped">
                                                        <thead>
                                                            <tr>
                                                                <th>Year</th>
                                                                <th>Wages</th>
                                                                <th>Tax Intrest</th>
                                                                <th>Dividends</th>
                                                                <th>Pension</th>
                                                                <th>Social Security</th>
                                                                <th>capital</th>
                                                                <th>Other Income</th>
                                                                <th>Total Amount</th>
                                                                <th width="100">Action</th>
                                                            </tr>
                                                        </thead>
                                                       
                                                             
                                                        <tbody>
                                                            <tr>
                                                                <td style="text-align:center;"><?php echo $personal->wrkyear;?></th>
                                                                <td style="text-align:center;"><?php if($personal->wagetotal!=''){ echo "$".''.number_format($personal->wagetotal,2);}?></th>
                                                                <td style="text-align:center;"><?php if($personal->taxintrest!=''){echo "$".''.number_format($personal->taxintrest,2);}?></th>
                                                                <td style="text-align:center;"><?php if($personal->qulifieddividends!=''){echo "$".''.number_format($personal->qulifieddividends,2);}?></th>
                                                                <td style="text-align:center;"><?php if($personal->pension!=''){echo "$".''.number_format($personal->pension,2);}?></th>
                                                                <td style="text-align:center;"><?php if($personal->socialsecurity!=''){echo "$".''.number_format($personal->socialsecurity,2);}?></th>
                                                                <td style="text-align:center;"><?php if($personal->capital!=''){echo "$".''.number_format($personal->capital,2);}?></th>
                                                                <td style="text-align:center;"><?php if($personal->otherincome!=''){echo "$".''.number_format($personal->otherincome,2);}?></th>
                                                                <td style="text-align:center;"><?php if($personal->totalamount!=''){echo "$".''.number_format($personal->totalamount,2);}?></th>
                                                                <td class="text-center"  style="text-align:center;">
                                                                    <button type="button" class="btn-action btn-view-edit incomeDetail" data-id="<?php echo e($personal->id); ?>"><i class="fa fa-edit"></i></button>
                    					                            <a onclick="return confirm('Are you sure to remove this record?')" href="<?php echo e(route('workrecord.destroyincomes',$personal->id)); ?>" class="btn-action btn-delete"><i class="fa fa-trash"></i></a>
                                                                </td>
                                                            </tr>
                                                                 
                                                        </tbody>
                                                    
               
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        
                        </div>
                    
                        
                    </div>
                    <?php 
					}
					?>
                                             
                                             
                    </div>
                   
               </div>
            </div>
            
            <div class="clear"></div>
            <div class="tab-pane <?php if(isset($common->business_id) && $common->business_id =='6') { ?>fade in active<?php } ?>" id="tab8primary">
                
                <div class="card-body col-md-offset-1" >
	                <div class="row"> 
                        <form method="post" action="https://financialservicecenter.net/fac-Bhavesh-0554/workrecord/updatetaxation" enctype="multipart/form-data">
                              <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                           
                          
   
	                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                         
                                    <input type="hidden" name="client_id" value="<?php echo e($common->id); ?>">
                                    <input type="hidden" name="personal_taxid" value="<?php echo e($common->personal_taxid); ?>">
                                    
                                    <div class="form-group">
                                        <div class="row">
                                        <label class="col-md-3 control-label text-right" style="padding-left: 72px;padding-right: 7px;margin-top: 7px;">Filing Type: <span class="star-required">*</span></label>
                                     
                                         <div class="col-lg-3 col-md-5 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin">
                                            <select class="form-control fsc-input" id="filing_type" name="filing_type" onchange="myFunction(this)" required>
                                               <option value="">Select</option>
                                               <option value="Regular Filing">Regular Filing</option>
                                               <option value="Extension Filing">Extension Filing</option>
                                               <option value="Amendment Filing">Amendment Filing</option>
                                             </select>
                                         </div>
                                        </div>
                                    </div>
                              
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-3 control-label text-right">Filing Year :</label>
                                             <div class="col-lg-3 col-md-5 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin"> 
                                                <input type="text" class="form-control regular2" id="filing_year" name="filing_year" readonly/>
                                                <select class="form-control fsc-input amendment2" id="filing_year" name="filing_year2" style="display:none">
                                                    <option value="">Select</option>
                                                    <option value="2016">2016</option>
                                                    <option value="2017">2017</option>
                                                    <option value="2018">2018</option>
                                                    <option value="2019">2019</option>
                                                </select>
                                             </div>
                                         </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-3 control-label text-right" style="margin-top: 7px;">Filing Method :</label>
                                             <div class="col-lg-3 col-md-5 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin"> 
                                                <select class="form-control fsc-input regular" id="filing_method" name="filing_method">
                                                    <option value="">Select</option>
                                                    <option value="Online"> Online</option>
                                                    <option value="Paperfile">Paperfile</option>
                                                </select>
                                                <select class="form-control fsc-input amendment" id="filing_method" name="filing_method2" style="display:none;">
                                                    <option value="">Select</option>
                                                    <option value="Paperfile">Paperfile</option>
                                                </select>
                                             </div>
                                         </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-3 control-label text-right" style="margin-top: 7px;">Date :</label>
                                             <div class="col-lg-3 col-md-5 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin"> 
                                                <!--<input type="text" class="form-control" id="datepicker" name="filing_date" value="<?php echo e($common->filing_date); ?>"/>-->
                                                <input type="text" class="form-control" id="datepicker" name="filing_date" value=""/>
                                             </div>
                                         </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-3 control-label text-right">Software use of File :</label>
                                             <div class="col-lg-3 col-md-5 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin"> 
                                                <select class="form-control fsc-input" id="filing_software" name="filing_software">
                                                    <option value="">Select</option>
                                                    <option value="Drake Software">Drake Software</option>
                                                    <option value="ATX">ATX</option>
                                                </select>
                                             </div>
                                         </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-3 control-label text-right" style="margin-top: 7px;">State :</label>
                                             <div class="col-lg-3 col-md-5 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin"> 
                                                <input type="text" class="form-control" id="filing_state" name="filing_state" value="<?php echo e($common->formation_register_entity); ?>" readonly/>
                                             </div>
                                         </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-3 control-label text-right" style="margin-top: 7px;">Date of Filing :</label>
                                             <div class="col-lg-3 col-md-5 col-sm-12 col-xs-12 fsc-form-col fsc-element-margin"> 
                                                <!--<input type="text" class="form-control" id="datepicker" name="filing_date" value="<?php echo e($common->filing_date); ?>"/>-->
                                                <input type="text" class="form-control" id="datepicker2" name="date_of_filing" value=""/>
                                             </div>
                                         </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <label class="col-md-3 control-label text-right" style="margin-top: 7px;"></label>
                        <div class="col-xs-1 " style="width:155px;">
                           <input class="btn_new_save" type="submit" value="Create">
                        </div>
                        <div class="col-xs-1" style="width:155px;">
                           <a class="btn_new_cancel" href="<?php echo e(url('fac-Bhavesh-0554/workrecord')); ?>">Cancel</a> 
                        </div>
                     </div>
                        </form>
			        </div>
    		    </div>	
    		   
            </div>
            <div class="clear"></div>
       </div>
   </div>
				    <?php endif; ?>
						</div>
						<div class="clear"></div>
						</div>
             </div>
					</div>
					<div class="clear"></div>
				</div>
			</div>
	
<!-- The Modal -->
<div class="modal" id="myModalbusinesspopup">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      
      
      
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        
           <form method="post"  action="<?php echo e(route('workrecord.store')); ?>" class="form-horizontal" enctype="multipart/form-data">
                     <?php echo e(csrf_field()); ?>


            <?php
            if(isset($common->id)!='')
            {
                 //@if(!empty($common->filename))

                ?>
                    <input type="hidden" name="client_id" value="<?php echo e($common->id); ?>">
                <?php
            }
            ?>
            
            
            <div class="form-group row">
            <label class="col-md-4 control-label text-right">Year :</label>
            <div class="col-md-6">
                <select class="form-control" name="license_year" required>
                    <option disabled>Select</option>
                    <option>2020</option>
                    <option>2021</option>
                    <option>2022</option>
                    <option>2023</option>
                </select>
            </div>
            </div>
        
            <div class="form-group row">
            <label class="col-md-4 control-label text-right">Gross Revenue :</label>
            <div class="col-md-6"><input type="text" class="form-control" name="license_gross" id="license_gross" required/></div>
        </div>
        
            <div class="form-group row">
            <label class="col-md-4 control-label text-right">License Fee :</label>
            <div class="col-md-6"><input type="text" class="form-control txtinput_1" name="license_fee" id="license_fee" required/></div>
        </div>
        
            <div class="form-group row">
            <label class="col-md-4 control-label text-right">License Status :</label>
            <div class="col-md-6">
                <select class="form-control" name="license_status" required>
                    <option>Active</option>
                    <option>Inactive</option>
                    <option>Hold</option>
                </select>
            </div>
        </div>
        
            <div class="form-group row">
            <label class="col-md-4 control-label text-right">License Copy :</label>
            <!--<div class="col-md-6">-->
            <!--    <input type="file" name="license_copy" required/>-->
            <!--</div>-->
            
            <div class="col-md-8">
                <label class="file-upload btn btn-primary">
                <input type="file" class="form-control" type="file" name="license_copy" required/>
                Browse for file ... </label>
           </div>
        </div>
        
            <div class="form-group row">
            <label class="col-md-4 control-label text-right">Certificate No :</label>
            <div class="col-md-6"><input type="text" class="form-control" name="license_no" required/></div>
        </div>
        
        <!--    <div class="form-group row">-->
        <!--    <label class="col-md-4 control-label text-right">Website Link</label>-->
        <!--    <div class="col-md-6"><input type="text" class="form-control" name="website_link" required/></div>-->
        <!--</div>-->

        <div class="form-group row">
            <label class="col-md-4 control-label text-right">Note :</label>
            <div class="col-md-6"><input type="text" class="form-control" name="license_note" required/></div>
        </div>

            
            <div class="form-group row">
                <label class="col-md-4 control-label text-right">License Issue Date :</label>
                <div class="col-md-6"><input type="text" class="form-control" name="license_renew_date" required/></div>
            </div>

            <div class="form-group row">
            <label class="col-md-4 control-label text-right hide_991"></label>
            <div class="col-xs-3" style="width:155px">
                <input class="btn_new_save btn-primary1 primary1" type="submit" value="Save">
            </div>
            <div class="col-xs-3" style="width:155px">
                <a class="btn_new_cancel" href="https://financialservicecenter.net/fac-Bhavesh-0554/employee">Cancel</a>
            </div>
        </div>
    
        </form>
        
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>		


<div class="modal" id="myModalbankingpopup">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
        <div class="modal-header" style="background:#ffff99 !important;text-align:center;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Record</h4>
        </div>

      <!-- Modal body -->
      <div class="modal-body">
        
           <form method="post"  action="" class="form-horizontal" enctype="multipart/form-data">
            <div class="form-group row">
            <label class="col-md-3 control-label text-right padleftzero">Bank Name</label>
            <div class="col-md-3">
                <!--<input type="text" class="form-control" name="license_year" required/>-->
                <select class="form-control" name="license_year" required>
                    <option disabled>Select</option>
                    <option>SBI</option>
                    <option>BOB</option>
                    <option>BOI</option>
                </select>
            </div>
            </div>
        
            <div class="form-group row">
            <label class="col-md-3 control-label text-right">Last Four Digit A/C #</label>
            <div class="col-md-3">
                <select class="form-control" name="license_year" required>
                    <option disabled>Select</option>
                    <option>0022</option>
                    <option>0033</option>
                    <option>0044</option>
                </select>
            </div>
            <label class="col-md-3 control-label text-right">Bank Nick name</label>
            <div class="col-md-3">
                <input type="text" class="form-control"/>
            </div>
        </div>
        
            <div class="form-group row">
            <label class="col-md-3 control-label text-right">St. Start Date: </label>
            <div class="col-md-3"><input type="text" class="form-control txtinput_1 opendate" name="startdate"/></div>
             <label class="col-md-3 control-label text-right">St. Beginning Balance: </label>
            <div class="col-md-3"><input type="text" class="form-control txtinput_1 closedate" name="startdate"/></div>
        </div>
        
          <div class="form-group row">
            <label class="col-md-3 control-label text-right">St. End Date:</label>
            <div class="col-md-3">
               <input type="text" class="form-control"/>
            </div>
             <label class="col-md-3 control-label text-right">St. Ending Balance:</label>
            <div class="col-md-3">
               <input type="text" class="form-control"/>
            </div>
        </div>
        
            <div class="form-group row">
            <label class="col-md-3 control-label text-right">Note:</label>
            
            <div class="col-md-9">
               <textarea class="form-control"></textarea>
           </div>
        </div>

        <div class="form-group row">
            <label class="col-md-3 control-label text-right">Account Done By:</label>
            <div class="col-md-3">
               <input type="text" class="form-control"/>
            </div>
             <label class="col-md-3 control-label text-right">Accounts Done Date:</label>
            <div class="col-md-3">
               <input type="text" class="form-control opendate"/>
            </div>
        </div>
            <div class="form-group row">
            <label class="col-md-3 control-label text-right hide_991"></label>
            <div class="col-xs-3" style="width:155px;">
                <input class="btn_new_save btn-primary1 primary1" type="submit" value="Save">
            </div>
            <div class="col-xs-3" style="width:155px;">
                <a class="btn_new_cancel" data-dismiss="modal">Cancel</a>
            </div>
        </div>
        </form>
        
      </div>

      <!-- Modal footer -->
      <!--<div class="modal-footer">-->
      <!--  <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>-->
      <!--</div>-->

    </div>
  </div>
</div>

                                        <!-- Modal -->
                 <!--                       <div class="modal fade" id="myModal" role="dialog">-->
                 <!--                           <div class="modal-dialog">-->
                                            
                                              <!-- Modal content-->
                 <!--                             <div class="modal-content">-->
                 <!--                               <div class="modal-header">-->
                 <!--                                 <button type="button" class="close" data-dismiss="modal">&times;</button>-->
                 <!--                                 <h4 class="modal-title">Update Document Record</h4>-->
                 <!--                               </div>-->
                 <!--                               <div class="modal-body">-->
                                              
                 <!--                                   <form method="post" class="form-horizontal" enctype="multipart/form-data" action="">-->
        									<!--<?php echo e(csrf_field()); ?>-->
        							
                 <!--                                   <input type="hidden" class="form-control" name="idaa" id="idkl">-->
                 <!--                                   <input type="hidden" class="form-control" name="filename_idss" id="filename_idss">-->
                 <!--                                   <input type="hidden" class="form-control" name="client_id" id="client_id">-->
                                                    <!--<input type="text" class="form-control" name="documentsname" id="documentsname">-->
                                                    <!--<input type="text" class="form-control" name="clientdocument" id="clientdocument">-->
                                                  
                 <!--                                       </br>-->
                 <!--                                          <div class="row">-->
                 <!--                                               <div class="col-md-4"><label class="control-label text-right">Document Name :</label></div>-->
                 <!--                                               <div class="col-md-8">-->
                 <!--                                                  <select class="form-control"  style="width:85%;" name="documentsname"  id="documentsname" required>-->
                 <!--                                                     <?php $__currentLoopData = $document; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cur): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>-->
                 <!--                                                     <option value="<?php echo e($cur->documentname); ?>"><?php echo e($cur->documentname); ?></option>-->
                 <!--                                                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>-->
                 <!--                                                  </select>-->
                 <!--                                                  <?php if($errors->has('documentsname')): ?>-->
                 <!--                                                  <span class="help-block">-->
                 <!--                                                  <strong><?php echo e($errors->first('documentsname')); ?></strong>-->
                 <!--                                                  </span>-->
                 <!--                                                  <?php endif; ?>-->
                 <!--                                               </div>-->
                 <!--                                           </div>-->

                 <!--                                           </br>-->
                 <!--                                           <div class="row">-->
                 <!--                                              <div class="col-md-4"><label class="control-label text-right">Upload Document : </label>-->
                 <!--                                              </div>-->
                 <!--                                              <div class="col-md-8">-->
                                                                   
                 <!--                                                   <label class="file-upload btn btn-primary">-->
                 <!--                                                   <input type="file" class="form-control" name="clientdocument" />-->
                 <!--                                                   Browse for file ... </label>-->
                 <!--                                                    <input type="hidden"  name="clientdocument_1" id="clientdocument"/><span id="clientdocument22"></span>-->
                 <!--                                              </div>-->
                 <!--                                          </div>-->
                                       
                 <!--                                           </br>-->
                 <!--                                           <div class="row">-->
                 <!--                                              <div class="col-md-4">-->
                 <!--                                              </div>-->
                 <!--                                              <div class="col-md-2" style=" padding-right:3px;">-->
                                                           
                 <!--                                                  <input class="btn_new_save btn-primary1 primary1" type="submit" id="primary1" name="submit" value="Save">-->
                 <!--                                                 </div>-->
                 <!--                                                   <div class="col-md-2" style="padding-left:3px;">-->
                 <!--                                                  <a class="btn_new_cancel" data-dismiss="modal">Cancel</a>-->
                 <!--                                               </div>-->
                 <!--                                           </div>-->
                                                    
                 <!--                                 </form>-->
                 <!--                               </div>-->
                 <!--                               <div class="modal-footer">-->
                 <!--                                 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                 <!--                               </div>-->
                 <!--                             </div>-->
                                              
                 <!--                           </div>-->
                 <!--                         </div>-->
                                       
                                    </div>
                                </div>
   
   <script>
        // $(".passingID").click(function () {
        //     var ids = $(this).attr('data-id');
        //     //alert(ids);
        //     $("#idkl").val(ids);
        //     $('#myModal').modal('show');
        //     //console.log(ids);           
        //     $.get('<?php echo URL::to('getClientDocumentdata'); ?>?documentid='+ids, function(data)
        //     {  
        //         console.log(data);
        //         if(data == "")
        //         {
                    
        //         }
        //         else
        //         {
        //             //$('#county').html(data);
        //             //console.log(data.id);exit;
        //             $('#id').val(data.id);
        //             $('#filename_idss').val(data.filename_id);
        //             $('#client_id').val(data.client_id);
        //             $('#documentsname').val(data.documentsname);
        //             $('#clientdocument').val(data.clientdocument);
        //             $('#clientdocument22').html(data.clientdocument);
        //         }
                

                
        //     });
        // });
   </script>
   
   
    <!-- Modal -->
                    
<div id="myModalAddrecord" class="modal fade" role="dialog">
  <div class="modal-dialog" style="width:1000px;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="border-bottom: 4px solid #a1a12c!important;">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><span class="pull-left"><?php if(!empty($common->filename)): ?> <?php echo e($common->filename); ?> <?php endif; ?></span>Client - Income Tax Filing Return <span style="float:right;">Add Record</span></h4>
      </div>
      <div class="modal-body">
          <form class="form-horizontal" method="post" action="<?php echo e(route('workrecord.storefederaladmin')); ?>" enctype="multipart/form-data">
              <?php echo e(csrf_field()); ?> 
              
              <?php if(!empty($common->id)): ?>
              <input type="hidden" name="client_id" value="<?php echo $common->id;?>">
              <?php endif; ?>
            <div class="recordform">
              
                <table style="width:100%;" class="taxtable">
                <tr>
                    <td style="width:360px;">
                        <label class="control-label labels">Client Name :</label>
                        <?php 
                        if(isset($common->business_id) && $common->business_id =='6') 
                        {
                            $names=$common->first_name.' '.$common->middle_name.' '.$common->last_name;
                            
                        }
                        else if(isset($common->business_id) && $common->business_id !='6')
                        {
                             $names=$common->company_name;
                        }?>
                        <input type="text" class="form-control clname" value="<?php if(isset($common->business_id)) { echo $names; }?>" readonly="">
                         
                    </td>
                     <?php
                     $yrss='';
                     $carray=array();
                    
                     if($clientorg)
                     {
                    foreach($clientorg as $corg)
                    {
                       $carray[]=$corg->federalsyear; 
                    }
                     }
                    ?>
                   
                    <td style="width:140px;">
                        <label class="control-label labels">Filling Year :<?php //print_r($clienttax);?></label>
                        <?php if($Incometaxfederal>0): ?>
                            <select class="form-control federalyear federalyear3" name="federalsyear" required>
                        <?php else: ?>
                            <select class="form-control federalyear" name="federalsyear" required>
                        <?php endif; ?>
                        
                        <option value="">Select</option>
                        
                        
                              <?php if(in_array('2019',$carray)) { } else {?> <option value="2019" <?php ?>>2019</option><?php } ?>
                               <?php if(in_array('2020',$carray)) { } else {?><option value="2020">2020</option><?php } ?>
                              
                    </select>
                        
                    </td>
                    
                    <td style="width:150px;">
                         <label class="control-label labels">Tax Returns <?php //print_r($common);?><?php //echo "<pre>";print_r($clientorg);?> :</label>
                         
                         <select class="form-control taxation" name="federalstax" required>
                            <option value="">Select</option>
                            <option class="hideext" value="Extension">Extension </option>
                            <option class="hideorg" value="Original">Original</option>
                           <!-- <option value="Amendment">Amendment</option>!-->
                        </select>
                    </td>
                    <td style="width:140px;">
                        <label class="control-label labels">Due Date :</label>
                         <input type="text" class="form-control duedate text-center" name="federalsduedate" placeholder="Mar-15-2020" readonly/>
                    </td>
                    <td></td>
                </tr>
            </table>
            
            <div class="table-responsive">
                <table class="taxtable table table-bordered" style="margin-top:20px;">
                    <thead>
                        <tr>
                            <!--<th width="105px" class="text-left">  <label class="form-label">Federal</label></th>-->
                            <th width="235px" class="text-left"><label class="form-label">Federal Form No.</label></th>
                            <th width="125px" class="text-left"><label class="form-label">Filling Method</label></th>
                            <th width="140px" class="text-left"><label class="form-label">Filling Date</label></th>
                            <th width="150px" class="text-left "> <label class="form-label">Status of Filling</label></th>
                            <th width="100px" class="text-left"> <label class="form-label">File</label></th>
                            <th class="text-left"><label class="form-label">Note</label></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <input type="text" readonly class="form-control formno" name="federalsform"  required>
                            </td>
                            <td width="105px">
                                <select class="form-control filingmethod" name="federalsmethod" required>
                                    <option value="">Select</option>
                                    <option value="E-File">E-File</option>
                                    <option value="Paperfile">Paperfile</option>
                                </select>
                            </td>
                            <td width="140px">
                                <input type="text" class="form-control fdate text-center" id="fillingdate" name="federalsdate" placeholder="MM/DD/YYYY" required/>
                            </td>
                            <td class="">
                                <select class="form-control hidefilling federalsstatus fedstatus1"  name="federalsstatus" required>
                                    <option value="">Select</option>
                                    <option style="background:green;" class="Green" mytag="Green" value="Accepted">Accepted</option>
                                    <option style="background:blue;" class="Blue" mytag="Blue" value="EF Processing">EF Processing</option>
                                    <option style="background:yellow;" class="Yellow" mytag="Yellow" value="Pending">Pending</option>
                                    <option style="background:red;" classs="Red" mytag="Red" value="Rejected">Rejected</option>
                                </select>
                            </td>
                            <td style="width:100px">
                                <!--<input type="file" class="form-control" name="federalsfile" style="display:block!important;margin-top:0px;"/>-->
                                <label class="file-upload btn btn-primary">								
                                <input type="file" name="federalsfile"  class="form-control">Browse for file</label>
                            </td>
                            <td>
                                <textarea class="form-control" name="federalsnote"></textarea>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="table-responsive">
                <table class="taxtable table table-bordered" style="margin-top:20px;" id="taxationtable">
                    <thead>
                        <tr>
                            <th width="105px" class="text-left">  <label class="form-label">Resi. State</label></th>
                            <th width="130px" class="text-left"><label class="form-label">Form No.</label></th>
                            <th width="125px" class="text-left "><label class="form-label">Filling Method</label></th>
                            <th width="140px" class="text-left"><label class="form-label">Filling Date</label></th>
                            <th width="150px" class="text-left "> <label class="form-label">Status of Filling</label></th>
                            <th width="105px" class="text-left"> <label class="form-label">File</label></th>
                            <th class="text-left"><label class="form-label">Note</label></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr id="trblock">
                            <td>
                                <?php if(isset($_REQUEST['id'])&& $_REQUEST['id']!=''): ?>
                                <select class="form-control"  style="display:none;" id="statetaxx2">
                                 
                                 <option>Select</option>
                                 <?php $__currentLoopData = $datastate; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $datastate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                 <option value="<?php echo e($datastate->state); ?>" <?php if($datastate->state ==$common->stateId): ?> selected <?php endif; ?>><?php echo e($datastate->state); ?></option>
                                 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                             </select>
                        
                                     <input class="form-control" name="statetax[]" id="statetax" readonly value="<?php echo e($common->stateId); ?>" >
                               
                                <?php endif; ?>
                            </td>
                            <td>
                                <input type="text" readonly class="form-control" name="stateformno[]" id="statefederalformno"  <?php if($stateform): ?>value="<?php echo e($stateform->taxform); ?>" <?php endif; ?> style="font-size: 14px !important;padding: 0px 1px 0px 4px;"/>
                            </td>
                            <td>
                                <select class="form-control statemethod" name="statemethod[]">
                                    <option value="">Select</option>
                                    <option value="E-File">E-File</option>
                                    <option value="Paperfile">Paperfile</option>
                                </select>
                            </td>
                            <td>
                                <input type="text" class="form-control fdatess text-center" id="fillingdate" name="statedate[]" placeholder="MM/DD/YYYY"/>
                            </td>
                            <td class="">
                                <select class="form-control hidestatefilling statestatus" name="statestatus[]">
                                    <option value="">Select</option>
                                      <option style="background:green;" class="Green" value="Accepted">Accepted</option>
                                    <option style="background:blue;" class="Blue" value="EF Processing">EF Processing</option>
                                    <option style="background:yellow;" class="Yellow" value="Pending">Pending</option>
                                    <option style="background:red;" classs="Red" value="Rejected">Rejected</option>
                            </select>
                            </td>
                            <td style="width:100px">
                                <!--<input type="file" class="form-control" name="statefile[]" style="display:block; margin-top:0px;"/>-->
                                <label class="file-upload btn btn-primary">								
                                <input type="file" name="statefile[]"  class="form-control">Browse for file</label>
                            </td>
                            <td>
                                <textarea class="form-control" name="statenote[]"></textarea>
                            </td>
                            <td><a href="#" class="btn btn-primary add-rowform"><i class="fa fa-plus"></i></a></td>
                        </tr>
                    </tbody>
                </table>
            </div>
                <div class="row form-group saves ">
                <label class="col-md-4 control-label hide_991">&nbsp;</label>
                <div class="col-xs-3" style="width:155px">
                    <input type="submit" class="btn_new_save primary savebutton" value="Save">
                </div>
                 <div class="col-xs-3" style="width:155px">
                    <a href="#" class="btn_new_cancel" data-dismiss="modal">Cancel</a>
                </div>
               
            </div>
            
            
         
            <input type="hidden" name="client_taxation_id" value="<?php if(isset($common->id)!=''){echo $common->id;}?>">
            <div class="row form-group">
                
                <div class="col-md-4 federals" style="display:none;">
                     
                </div>
                
                <div class="col-md-4 states" style="display:none;">
                     <select class="form-control statesyear" name="statesyear">
                        <option value="">Select</option>
                         <?php
                            $aa=date('Y')-1;
                            
                        ?>
                        <option value="<?php echo $aa;?>"><?php echo $aa;?></option>
                        
                        
                    </select>
                </div>
            </div>
             <div class="row form-group">
                <label class="col-md-3 control-label labels" style="display:none;">Tax Return</label>
                <div class="col-md-4 federals" style="display:none;">
                    
                </div>
                <div class="col-md-4 states" style="display:none;">
                    <select class="form-control taxation2" name="statestax">
                       <option value="">Select</option>
                        <option value="Extension">Extension </option>
                        <option value="Original">Original</option>
                        <!--<option value="Amendment">Amendment</option>!-->
                </select>
                </div>
            </div>
              <div class="row form-group">
                <label class="col-md-3 control-label labels"  style="display:none;">Form No.</label>
                <div class="col-md-4 federals" style="display:none;">
                   
                </div>
              
                <div class="col-md-4 states" style="display:none;">
                    <input type="text" class="form-control formno2" name="statesformno" readonly/>
                </div>
            </div>
              <div class="row form-group">
                <label class="col-md-3 control-label labels" style="display:none;">Due Date</label>
                <div class="col-md-4 federals" style="display:none;">
                   
                </div>
             
                <div class="col-md-4 states" style="display:none;">
                    <input type="text" class="form-control duedate2" name="statesduedate" placeholder="Mar-15-2020" readonly/>
                </div>
            </div>
            <div class="row form-group">
                <label class="col-md-3 control-label labels" style="display:none;">Filing Method </label>
                <div class="col-md-4 federals" style="display:none;">
                   
                </div>
               
                <div class="col-md-4 states" style="display:none;" >
                    <select class="form-control filingmethod2" name="statesmethod">
                        <option value="">Select</option>
                        <option value="E-File">E-File</option>
                        <option value="Paperfile">Paperfile</option>
                    </select>
                </div>
            </div>
            <div class="row form-group">
                <label class="col-md-3 control-label labels" style="display:none;">Filing Date</label>
                <div class="col-md-4 federals" style="display:none;">
                    
                </div>
              
                <div class="col-md-4 states" style="display:none;">
                    <input type="date" class="form-control" id="fillingdate2" name="statesdate"/>
                </div>
            </div>
             <div class="row form-group statusof">
                <label class="col-md-3 control-label labels" style="display:none;">Status of Filling </label>
                <div class="col-md-4 federals" style="display:none;" >
                    
                </div>
               
                <div class="col-md-4 states" style="display:none;">
                    <select class="form-control" name="statesstatus">
                        <option value="">Select</option>
                        <option value="Accepted">Accepted</option>
                        <option value="Rejected">Rejected</option>
                        <option value="Pending">Pending</option>
                    </select>
                </div>
            </div>
             <div class="row form-group">
                <label class="col-md-3 control-label labels" style="display:none;">Add File </label>
                <div class="col-md-4 federals" style="display:none;">
                  
                </div>
               
                <div class="col-md-4 states" style="display:none;">
                   <input type="file" class="form-control" name="statesfile"/>
                </div>
            </div>
              <div class="row form-group">
                <label class="col-md-3 control-label labels" style="display:none;">Note </label>
                <div class="col-md-4 federals" style="display:none;">
                 
                </div>
                
                <div class="col-md-4 states" style="display:none;">
                   <textarea class="form-control" name="statesnote"></textarea>
                </div>
            </div>
            
            <div class="row form-group saves savebutton" style="display:none;">
                <label class="col-md-5 control-label hide_991">&nbsp;</label>
                <div class="col-xs-3" style="width:155px">
                    <input type="submit" class="btn_new_save primary" value="Save">
                </div>
                <div class="col-xs-3" style="width:155px">
                    <a href="#" class="btn_new_cancel" data-dismiss="modal">Cancel</a>
                </div>
               
            </div>
        </div>
        </form>
      </div>

    </div>

  </div>
</div>  

    <!--Modal Federal tax Update Start-->
        <div id="federalModals" class="modal fade" role="dialog">
            <div class="modal-dialog" style="width:1000px;">

                <!-- Modal content-->
                <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Client - Income Tax Filing - (Form-1040) <span style="float:right;">Add Record</span></h4>
                      </div>
                      <div class="modal-body">
                          <form class="form-horizontal" method="post" action="<?php echo e(route('workrecord.storefederalupdate')); ?>" enctype="multipart/form-data">
                              <?php echo e(csrf_field()); ?> 
                              
                              
                            <input type="hidden" name="federalid" id="federalid">
                              
                            <div class="recordform">
                              
                            <table style="width:100%; max-width:520px; margin:0px auto;" class="taxtable">
                                <tr>
                                    <td>
                                        <label class="control-label labels">Filling Year</label>
                                        <select class="form-control federalyear" name="federalsyear" id="federalsyear">
                                            <option value="">Select</option>
                                            <?php
                                                
                                                $now=date('M-d-Y');
                                                if('Jul-25-2021'==$now)
                                                {
                                                    $aa=date('Y')-2;
                                                    $bb=date('Y')-1;
                                                    ?>
                                                    <option value="<?php echo $aa;?>"><?php echo $aa;?></option>
                                                    <option value="<?php echo $bb;?>"><?php echo $bb;?></option>
                                                    <?php
                                                }
                                                else
                                                {
                                                    $aa=date('Y')-1;
                                                    ?>
                                                    <option value="<?php echo $aa;?>"><?php echo $aa;?></option>
                                                    <?php
                                                }
                                                
                                            ?>
                                        </select>
                                    </td>
                                    <td>
                                         <label class="control-label labels">Tax Return</label>
                                         <select class="form-control taxation" name="federalstax" id="federalstax">
                                            <option value="">Select</option>
                                            <option value="Extension">Extension </option>
                                            <option value="Original">Original</option>
                                          <!--  <option value="Amendment">Amendment</option>!-->
                                        </select>
                                    </td>
                                    <td>
                                        <label class="control-label labels">Due Date</label>
                                         <input type="text" class="form-control duedate" name="federalsduedate" id="federalsduedate" placeholder="Mar-15-2020" readonly/>
                                    </td>
                                </tr>
                            </table>
                            
                            <table class="taxtable table table-bordered" style="margin-top:20px;">
                             <tr>
                                 <td>
                                     <label class="form-label">Federal</label>
                                     
                                 </td>
                                  <td>
                                     <label class="form-label">Form No.</label>
                                      <input type="text" readonly class="form-control formno" name="federalsform" id="federalsform" style="width:100px"/>
                                     
                                 </td>
                                  <td>
                                     <label class="form-label">Filling Method</label>
                                      <select class="form-control filingmethod" name="federalsmethod" id="federalsmethod">
                                        <option value="">Select</option>
                                        <option value="E-File">E-File</option>
                                        <option value="Paperfile">Paperfile</option>
                                    </select>
                                     
                                 </td>
                                  <td>
                                     <label class="form-label">Filling Date</label>
                                     <input type="text" class="form-control fillingdate" id="fillingdate" name="federalsdate"/>
                                     
                                 </td>
                                  <td>
                                     <label class="form-label">Status of Filling</label>
                                     <select class="form-control" name="federalsstatus" id="federalsstatus">
                                        <option value="">Select</option>
                                        <option value="Accepted">Accepted</option>
                                        <option value="Rejected">Rejected</option>
                                        <option value="Pending">Pending</option>
                                    </select>
                                     
                                     
                                 </td>
                                 
                                 <td style="width:100px">
                                    <label class="form-label">File</label>
                                    <input type="file" class="form-control" name="federalsfile" style="display:block!important;margin-top:0px;"/>
                                        Browse for file ... </label>
                                    <input type="hidden" class="form-control" name="federalsfile" id="federalsfile"><span id="federalsfile_1"></span>
                                 </td>
                                 <td>
                                     <label class="form-label">Note</label>
                                       <textarea class="form-control" name="federalsnote" id="federalsnote"></textarea>
                                     
                                 </td>
                             </tr>
                         </table>
                         
                         
                            <div class="row form-group saves">
                                <label class="col-md-4 control-label hide_991">&nbsp;</label>
                                <div class="col-xs-3" style="width:155px">
                                    <input type="submit" class="btn_new_save primary" value="Save">
                                </div>
                                <div class="col-xs-3" style="width:155px">
                                    <a href="#" class="btn_new_cancel" data-dismiss="modal">Cancel</a>
                                </div>
                               
                            </div>
                            
                            
                         
                            
                        </div>
                        </form>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      </div>
                </div>

            </div>
        </div>  
    <!--Modal Federal tax Update End-->

    <!--Modal State tax Update Start-->
        <div id="stateModals" class="modal fade" role="dialog">
            <div class="modal-dialog" style="width:1000px;">

                <!-- Modal content-->
                <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Client - Income Tax Filing - (Form-1040) <span style="float:right;">Add Record</span></h4>
                      </div>
                      <div class="modal-body">
                          <form class="form-horizontal" method="post" action="<?php echo e(route('workrecord.stateupdate')); ?>" enctype="multipart/form-data">
                              <?php echo e(csrf_field()); ?> 
                              
                              
                            <input type="hidden" name="stateid" id="stateid">
                              
                            <div class="recordform">
                            
                            <table class="taxtable table table-bordered" style="margin-top:20px;" id="taxationtable">
                                <tr>
                                      <th><label class="form-label">State</label></th>
                                      <th><label class="form-label">Form No.</label></th>
                                      <th><label class="form-label">Filling Method</label></th>
                                      <th><label class="form-label">Filling Date</label></th>
                                      <th> <label class="form-label">Filling Method</label></th>
                                      <th> <label class="form-label">File</label></th>
                                      <th><label class="form-label">Note</label></th>
                                      
                                </tr>
              
                                <tr>
                                     <td>
                                       <?php if(isset($_REQUEST['id'])&& $_REQUEST['id']!=''): ?>
                                         <select class="form-control statetax" name="statetax" id="statetaxstate">
                                             <option>Select</option>
                                             <?php $__currentLoopData = $datastate3; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $datastate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                             <option value="<?php echo e($datastate->state); ?>"><?php echo e($datastate->state); ?></option>
                                             <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                         </select>
                                         
                                          
                                          <?php endif; ?>
                                         
                                     </td>
                                      <td>
                                         
                                          <input type="text" readonly class="form-control stateformno" name="stateformno" id="statefederalformnostate" style="width:100px"/>
                                         
                                     </td>
                                      <td>
                                        
                                        <select class="form-control filingmethod" name="statemethod" id="statemethod">
                                            <option value="">Select</option>
                                            <option value="E-File">E-File</option>
                                            <option value="Paperfile">Paperfile</option>
                                        </select>
                                         
                                     </td>
                                      <td>
                                         
                                         <input type="text" class="form-control statedate" id="fillingdate" name="statedate"/>
                                         
                                     </td>
                                      <td>
                                       
                                         <select class="form-control" name="statestatus" id="statestatus">
                                            <option value="">Select</option>
                                            <option value="Accepted">Accepted</option>
                                            <option value="Rejected">Rejected</option>
                                            <option value="Pending">Pending</option>
                                        </select>
                                         
                                         
                                     </td>
                                      <td style="width:100px">
                                        
                                          <input type="file" class="form-control" name="statefile"  style="display:block; margin-top:0px;"/>
                                          Browse for file ... </label>
                                          <input type="hidden" class="form-control" name="statefile_1" id="statefile"><span id="statefile_1"></span>
                                     </td>
                                     <td>
                                         
                                           <textarea class="form-control" name="statenote" id="statenote"></textarea>
                                         
                                     </td>
                                     
                                 </tr>
                            </table>
                         
                            <div class="row form-group saves">
                                <label class="col-md-4 control-label hide_991">&nbsp;</label>
                                <div class="col-xs-3" style="width:155px">
                                    <input type="submit" class="btn_new_save primary" value="Save">
                                </div>
                                <div class="col-xs-3" style="width:155px">
                                    <a href="#" class="btn_new_cancel" data-dismiss="modal">Cancel</a>
                                </div>
                               
                            </div>
                            
                            
                         
                            
                        </div>
                        </form>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      </div>
                </div>

            </div>
        </div>  
    <!--Modal State tax Update End-->

   
<script>
    $(".incomeDetailShow").click(function ()
    {
        var incomeid = $(this).attr('data-id');
        $("#incomeid").val(incomeid);
        $('#myModalIncomeDetailShow').modal('show');
            
    });
    
    $(".stateTaxation").click(function ()
    {
            var stateid = $(this).attr('data-id');
            $("#stateid").val(stateid);
            $('#stateModals').modal('show');
            $.get('<?php echo URL::to('getClientstatedata'); ?>?stateid='+stateid, function(data)
            {  
                //console.log(data);exit;
                if(data == "")
                {
                    
                }
                else
                {
                    $('#stateyear').val(data.stateyear);
                    $('.statetax').val(data.statetax);
                    $('#federalsduedate').val(data.federalsduedate);
                    //$('#federalsduedate').val(date('d-m-Y',strtotime(data.federalsduedate)));
                    $('.stateformno').val(data.stateformno);
                    $('#statemethod').val(data.statemethod);
                    $('.statedate').val(data.statedate);
                    $('#statestatus').val(data.statestatus);
                    $('#statefile').val(data.statefile);
                    $('#statefile_1').html(data.statefile);
                    $('#statenote').val(data.statenote);
                    
                }
                

                
            });
        });
    
    $(".incomeDetail").click(function ()
    {
            var incomeid = $(this).attr('data-id');
            $("#incomeid").val(incomeid);
            $('#myModalIncomeUpdate').modal('show');
            $.get('<?php echo URL::to('getIncomedata'); ?>?incomeid='+incomeid, function(data)
            {  
                //console.log(data);exit;
                if(data == "")
                {
                    
                }
                else
                {
                    $('#wrkyear').val(data.wrkyear);
                    $('#wagetotal').val(data.wagetotal);
                    $('#taxintrest').val(data.taxintrest);
                    $('#qulifieddividends').val(data.qulifieddividends);
                    $('#pension').val(data.pension);
                    $('#socialsecurity').val(data.socialsecurity);
                    $('#capital').val(data.capital);
                    $('#otherincome').val(data.otherincome);
                    $('#totalamount').val(data.totalamount);
                }
                

                
            });
        });
    
    $(".federalTaxation").click(function ()
    {
            var federalid = $(this).attr('data-id');
            $("#federalid").val(federalid);
            $('#federalModals').modal('show');
            $.get('<?php echo URL::to('getClientfederaldata'); ?>?federalid='+federalid, function(data)
            {  
                //console.log(data);exit;
                if(data == "")
                {
                    
                }
                else
                {
alert(data.filename);
if(data.business_id =='6')
                    {
                    var fullnames=data.first_name +' '+data.last_name;
                    }
                    else
                    {
                        var fullnames=data.company_name;
                    
                    }
                   // alert(fullnames);
                    //$('#id').val(data.id);
                    $('#federalsyear').val(data.federalsyear);
                    $('#federalstax').val(data.federalstax);
                    $('#federalsduedate').val(data.federalsduedate);
                    //$('#federalsduedate').val(date('d-m-Y',strtotime(data.federalsduedate)));
                    $('#federalsform').val(data.federalsform);
                    $('#federalsmethod').val(data.federalsmethod);
                    $('.fillingdate').val(data.federalsdate);
                    $('#federalsstatus').val(data.federalsstatus);
                    $('#federalsfile').val(data.federalsfile);
                    $('#federalsfile_1').html(data.federalsfile);
                    $('#federalsnote').val(data.federalsnote);
                      $('#clid').html(data.filename);
                    $('.clname').val(fullnames);
                  
                    
                }
                

                
            });
        });
    
    function federalclick()
    { 
        var checkBox = document.getElementById("federal");
        if (checkBox.checked == true){
        $('.labels').show();
        //$('.saves').show();
        $('.federals').show();
        
       }
       else
       {
           
            $('.federals').hide();
          
       }
    }
   
   
    function stateclick()
    { 
        var checkBox1 = document.getElementById("states");
        if (checkBox1.checked == true)
        {
            $('.labels').show();
            $('.saves').show();
            $('.states').show();
        }
        else
        {
   
            $('.states').hide();
        }
   }
    
    
        
    
    $(document).ready(function()
    {
        
        $('.fdate').blur(function()
        {
           // alert('222');
            var taxtion=$('.taxation').val();
            if(taxtion !='Extension')
            {
            var thisdate=$(this).val();
            $('.fdatess').val(thisdate);
            }
        });
        
         //     $('.btn_new_save').hide();
           
        $('.federalyear3').change(function()
        {
          //  var this1=$(this).val();
        //    var aa='<?php echo $aa=date('Y');?>';
          //  if(this1 == aa)
            //{
              //  $(".savebutton").hide();
                //alert('This year already exists!');
        //    }
          //  else
            //{
              //  $(".savebutton").show();
            //}
        });
        
        $('.federalyear').change(function()
        {
             var thiss=$(this).val();
             
             var common='<?php if(isset($common->id)) { echo $common->id;}?>';
            $.get('<?php echo URL::to('getClientfederalyear'); ?>?year='+thiss+'&&id='+common, function(data)
            {  
                var data1=data.length;
                //alert(data);
                if(data == "Extension"){
                    $('.hideext').hide();
                    $('.hideorg').show();
                }else if(data == "Original"){
                    $('.hideext').hide();
                    $('.hideorg').hide();
                }else if(data.length == 0){
                    $('.hideext').show();
                    $('.hideorg').show();
                }
                //console.log(data);exit;
                if(data == "")
                {
                    
                }
                else
                {
                }
                

                
            });
        
             
             if(thiss =='')
             {
                 $('.btn_new_save ').hide();
             }
             else
             {
                 
                     //$('.btn_new_save ').show();
             
             }
             var this1=$(this).val();
             var this2=parseFloat(this1) + parseFloat(1);
             var aa='<?php echo $aa=date('Y');?>';
             if(this1 =='2019')
             {
                 $(".duedate").val('Jul-15-'+this2);
                 
             }
             else if(this1 =='' )
             {
                 $(".duedate").val(' ');
                 
             }
             else
             {
                 $(".duedate").val('Mar-15-'+this2);
                 
             }
            
            
            
            //  if(this1 == 'Extension')
            //  {
            //      $("select option[value='Extension']").hide();
            //      $(".duedate").val('Mar-15-<?php echo $aa+1;?>');
                 
            //  }
            // else if(this1 == bb)
            //  {
            //      $("select option[value='Extension']").hide();
            //      $(".duedate").val('Mar-15-<?php echo $aa+1;?>');
            //  }
            //  else if(this1 == cc)
            //  {
            //      $("select option[value='Extension']").show();
            //      $(".duedate").val('Mar-15-<?php echo $aa+1;?>');
            //  }
      
        });
      
      
        $('.statesyear').change(function()
        {
             
             var this1=$(this).val();
             //alert(this1);
             var aa='<?php echo $aa=date('Y');?>';
             var bb='<?php echo $bb=date('Y')+1;?>';
             var cc='<?php echo $cc=date('Y')+2;?>';
             if(this1 == aa)
             {
                 $("select option[value='Extension']").hide();
                 $(".duedate2").val('Jul-15-<?php echo $aa+1;?>');
                 
             }
            else if(this1 == bb)
             {
                 $("select option[value='Extension']").hide();
                 $(".duedate2").val('Jul-15-<?php echo $bb+1;?>');
             }
             else if(this1 == cc)
             {
                 $("select option[value='Extension']").show();
                 $(".duedate2").val('Jul-15-<?php echo $cc+1;?>');
             }
      
        });
      
      
        $('.taxation').change(function()
        {
              var businessid='<?php if(isset($_REQUEST['id']) && $_REQUEST['id'] !='') { echo $common->business_id; }?>';
            
          var thiss=$(this).val();
          var this1=$('.federalyear').val();
            var this2=parseFloat(this1) + parseFloat(1);
           
          if(thiss == 'Extension') 
          {
               $('#statefederalformno').val('Same as a Fed.');
               
                $('#trblock').css("pointer-events", 'none');
                $('.statemethod').attr("disabled","disabled");
                $('.fdatess').attr("disabled","disabled");
                $('.statestatus').attr("disabled","disabled");

               if(this1 =='2019')
             {
                 $(".duedate").val('Jul-15-'+this2);
              }
             else if(this1 =='' )
             {
                 $(".duedate").val(' ');
                 //$('#statefederalformno').val('');
                 
             }
             else
             {
             //$('#statefederalformno').val("N/A');
              
                 $(".duedate").val('Mar-15-'+this2);
                 
             }
            
              if(businessid =='6')
              {
                  $('.formno').val('Form-4868');
              }
              else
              {
              $('.formno').val('Form-7004');   
             //   $(".").val('Sep-15-<?php echo $aa;?>');
               
              }
              
              $("select option[value='E-File']").show();
          }
          
          else if(thiss == 'Original')
          {
              var fed='<?php echo $clientfedext;?>';
             // alert(fed);
              var ii='<?php if(!empty($stateform)) { echo $stateform->taxform; }?>';
              var stateform=(ii);
               $('#statefederalformno').val(stateform);
               
                $('#trblock').css("pointer-events", 'auto');
              
              $('.statemethod').removeAttr('disabled');
             $('.fdatess').removeAttr('disabled');
             $('.statestatus').removeAttr('disabled');
               //   $('#statefederalformno').val("");
              
                
                if(this1 =='2019')
             {
                 if(fed =='1')
                 {
                    $(".duedate").val('Sep-15-'+this2);
                  
                 }
                 else
                 {
                 $(".duedate").val('Mar-15-'+this2);
                 }
             }
             else if(this1 =='' )
             {
                 $(".duedate").val(' ');
                 
             }
             else
             {
                 $(".duedate").val('Mar-15-'+this2);
                 
             }
            
               $('.formno').val('<?php if(isset($common->type_form)) { echo $common->type_form;}?>');
               
             $('#statefederalformno').val('<?php if(isset($common->type_form_file2)) { echo $common->type_form_file2;}?>');
               
             // $('.formno').val('Form-1040');
              $("select option[value='E-File']").show();
          }
          else if(thiss == 'Amendment')
          {
                var ii='<?php if(!empty($stateform)) { echo $stateform->taxform; }?>';
              var stateform=(ii);
            
             $('#statefederalformno').val(stateform);
                 
               $('#trblock').css("pointer-events", 'auto');
              $('.statemethod').removeAttr('disabled');
              $('.fdatess').removeAttr('disabled');
              $('.statestatus').removeAttr('disabled');
               
              $('.formno').val('Form-1040X');
              $("select option[value='E-File']").hide();
                     
          }
          else
          {
              var ii='<?php if(!empty($stateform)) { echo $stateform->taxform;}?>';
              var stateform=(ii);
               $('#statefederalformno').val(stateform);
              
                $('#trblock').css("pointer-events", 'auto');
             $('.statemethod').removeAttr('disabled');
             $('.fdatess').removeAttr('disabled');
             $('.statestatus').removeAttr('disabled');
          }
        });
        
        $('.taxation2').change(function()
        {
            var businessid='<?php if(isset($_REQUEST['id']) && $_REQUEST['id'] !='') { echo $common->business_id; }?>';
            var thiss=$(this).val();
          if(thiss == 'Extension')
          {
               if(businessid =='6')
              {
                  $('.formno').val('Form-4868');
              }
              else
              {
              $('.formno').val('Form-7004');    
              }
              $("select option[value='E-File']").show();
          }
          
          else if(thiss == 'Original')
          {
               $('.formno2').val('Form-1120S');
             
             // $('.formno2').val('Form-1040');
              $("select option[value='E-File']").show();
          }
          else if(thiss == 'Amendment')
          {
              $('.formno2').val('Form-1040X');
              $("select option[value='E-File']").hide();
                     
          }
        });
        
    $('.federalsstatus').change(function()
        {
            var thiss1=$(this).val();
            var option = $('option:selected', this).attr('mytag');
           // alert();
          //  $('.federalsstatus option[value="EF Processing"]').css("background-color","red");
            
        });
        $('.filingmethod').change(function()
        {
          var thiss=$(this).val();
          if(thiss == 'E-File')
          {
                 $('.hidefilling').css("visibility","visible");
              $('.statusof').css("visibility","visible");
         
              $('.federalsstatus option[value="EF Processing"]').attr("selected","selected");
          }
          else if(thiss == 'Paperfile')
          
          {
                     $('.statusof').css("visibility","hidden");
              
               $('.hidefilling').css("visibility","hidden");
            
         }
          else
          {
              
                   $('.federalsstatus option[value="EF Processing"]').removeAttr("selected");
         
              $('.hidefilling').show();
              $('.statusof').show();
          }
        });
        
        $('.statemethod').change(function()
        {
          var thiss=$(this).val();
          if(thiss == 'E-File')
          {
                 $('.hidestatefilling').css("visibility","visible");
          
              $('.statestatus option[value="EF Processing"]').attr("selected","selected");
          }
          else if(thiss == 'Paperfile')
          
          {
              
               $('.hidestatefilling').css("visibility","hidden");
            
         }
          else
          {
              
                   $('.statestatus option[value="EF Processing"]').removeAttr("selected");
         
              $('.hidestatefilling').show();
          }
        });
        
      
        $('.filingmethod2').change(function()
        {
          var thiss=$(this).val();
          if(thiss == 'Paperfile')
          {
              $('.statusof2').hide();
          }
          
          else
          {
              $('.statusof2').show();
          }
        });
      
    });
</script>
   

<script>
$(document).ready(function(){
    
  

$.ajaxSetup({
           headers: {
               'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
           }
       });
    
      $(function () {
         $('#addopt').click(function () { //alert();
             var newopt = $('#newopt').val();
             if (newopt == '') {
                 alert('Please enter something!');
                 return;
             }
   
            //check if the option value is already in the select box
             $('#vendor_product option').each(function (index) {
                 if ($(this).val() == newopt) {
                     alert('Duplicate option, Please enter new!');
                 }
             })
             $.ajax({
                   type: "post",
                   url: "<?php echo route('workrecord.documents'); ?>",
                   dataType: "json",
                   data: $('#ajax').serialize(),
                   success: function(data){
                      alert('Successfully Added');
                      $('#vendor_product').append('<option value=' + newopt + '>' + newopt + '</option>');
                      $("#div").load(" #div > *");
                      $("#newopt").val('');
                   },
                   error: function(data){
                      alert("Error")
                   }
                   });
            
              $('#basicExampleModal').modal('hide');
         });
     });
     
 
});




</script>


<script type="text/javascript">



$(document).ready(function()
 {
        
     
    $("#license_gross").on("input", function(evt)
    {
		var self = $(this);
		self.val(self.val().replace(/[^\d].+/, ""));
		if ((evt.which < 48 || evt.which > 57)) 
		{
			evt.preventDefault();
		}
    });  

    $("#license_fee").on("input", function(evt)
    {
		var self = $(this);
		self.val(self.val().replace(/[^\d].+/, ""));
		if ((evt.which < 48 || evt.which > 57)) 
		{
			evt.preventDefault();
		}
    });  

    
    
    
    (function($)
    {
        var minNumber = -100;
        var maxNumber = 100;
        $('.spinner .btn:first-of-type').on('click', function()
        {
            if ($('.spinner input').val() == maxNumber) {
              return false;
            } else {
              $('.spinner input').val(parseInt($('.spinner input').val(), 10) + 5 + '%');
            }
        });

        $('.txtinput_1').on("blur", function()
        {
            var inputVal = parseFloat($(this).val().replace('%', '')) || 0
            if (minNumber > inputVal) {
              //inputVal = -100;
            } else if (maxNumber < inputVal) {
              //inputVal = 100;
            }
            $(this).val(inputVal + '.00');
        });

        $('.spinner .btn:last-of-type').on('click', function() 
        {
            if ($('.spinner input').val() == minNumber) {
              return false;
            } else {
              $('.spinner input').val(parseInt($('.spinner input').val(), 10) - 5 + '%');
            }
        });
    })(jQuery);

}); 

</script>


<script>

function myFunction(){
     
      var ftype = document.getElementById('filing_type').value;
     //alert(ftype);
     if(ftype=='Regular Filing')
     {
         $('#filing_year').val('2019');
         $('.regular').show();
         $('.regular2').show();
         $('.amendment2').hide();
         $('.amendment').hide();
     }
     else if(ftype=='Extension Filing')
     {
         $('#filing_year').val('2019');
         $('.regular').show();
         $('.regular2').show();
         $('.amendment2').hide();
         $('.amendment').hide();
     }
     else if(ftype=='Amendment Filing')
     {
         $('#filing_year').val('2016');
         $('.regular').hide();
         $('.amendment').show();
         $('.regular2').hide();
         $('.amendment2').show();
     }
    
     
    
     
 }

$(document).ready(function()
{
    
    $('#statetax').on('change', function()
    {
            var statetaxval=$("#statetax").val();
              var fdatef=$('.fdate').val();
          // alert(fdatef);
            $('.fdatess').val(fdatef);
         
            $.get('<?php echo URL::to('getTaxstatedata'); ?>?statetaxval='+statetaxval,function(data)
            {  
                //console.log(data);exit;
              //  alert(data.taxform);
              //alert(data);
                if(data=='')
                {
                   $("#statefederalformno").val(); 
                }
                else
                {
                    $("#statefederalformno").val(data.taxform);
                }
                
            });
   
   });
   
   
    $('#statetaxstate').on('change', function()
    {
            var statetaxval=$("#statetaxstate").val();
            $.get('<?php echo URL::to('getTaxstatedata'); ?>?statetaxval='+statetaxval,function(data)
            {  
                //console.log(data);exit;
                if(data=='')
                {
                   $("#statefederalformnostate").val(); 
                }
                else
                {
                    $("#statefederalformnostate").val(data.taxform);
                }
                
            });
   
   });
    
    
    $('#formation_yearbox22').on('change', function()
    {
         //$todaydates=   date('Y-m-d',strtotime($tdatess));
                                             
        var ids = document.getElementById("formation_yearbox22").value;
        <?php
            if(isset($common->id)!='')
            {
            ?>
                var clientid = <?php echo $common->id;?>
            <?php
            }
        ?>
        
        //alert(ids);
        var yearvaluesub1 = '<?php $dd=date('Y'); echo $dd-1;?>'; 
        //var yearvaluesub1 = '2018'; 
        
        if(ids!='')
        {
            for (var n = 1; n <= ids; ++ n)
            {
                var total = parseFloat(yearvaluesub1)+ parseFloat(n);
                $("#formation_yearvalue22").val(total);
                $.get('<?php echo URL::to('getFormationexistdata'); ?>?totalid='+total,'clientid='+clientid, function(data)
                {  
                    //console.log(data);exit;
                    if(data>0)
                    {
                        $("#yeardalreadyExist").show();
                        $(".yeardalreadyExist1").show();
                        $(".yeardalreadyExist2").hide();
                    }
                    else
                    {
                        //console.log(data.id);exit;
                        $("#yeardalreadyExist").hide();
                        $(".yeardalreadyExist1").hide();
                        $(".yeardalreadyExist2").show();
                    }
                    
                });
                
                
                
            }
            
        }    
        
        
        
        
        
        
   
   });

    
    
    var yearvaluesub = document.getElementById("formation_yearvalue_already").value;
    //alert(yearvaluesub);
    

    $('#formation_yearbox').on('change', function()
    {
        var ids = document.getElementById("formation_yearbox").value;
        var yearvaluesub1 = '<?php $dd=date('Y'); echo $dd-1;?>'; 
        var yearvaluesub = document.getElementById("formation_yearvalue_already").value;
        <?php
            if(isset($common->id)!='')
            {
            ?>
                var clientid = <?php echo $common->id;?>
            <?php
            }
        ?>
        
        
        
        if(yearvaluesub!='')
        {
            for (var n = 1; n <= ids; ++ n)
            {
                var total = parseFloat(yearvaluesub)+ parseFloat(n);
                //$("#formation_yearvalue").val(total);
                //console.log(total);
                //$.get('<?php echo URL::to('getFormationexistdata'); ?>?totalid='+total, function(data)
                $.get('<?php echo URL::to('getFormationexistdata'); ?>?totalid='+total,'clientid='+clientid, function(data)
                {  
                    //console.log(data);exit;
                    if(data>0)
                    {
                        $("#yeardalreadyExist").show();
                        $(".yeardalreadyExist1").show();
                        $(".yeardalreadyExist2").hide();
                    }
                    else
                    {
                        //console.log(data.id);exit;
                        $("#yeardalreadyExist").hide();
                        $(".yeardalreadyExist1").hide();
                        $(".yeardalreadyExist2").show();
                    }
                    
                });
            }
        }
        else
        {
            for (var n = 1; n <= ids; ++ n)
            {
                var total = parseFloat(yearvaluesub1)+ parseFloat(n);
                $("#formation_yearvalue").val(total);
            }
        }
        
        var ids=$(this).val();
        if(ids=='')
        {
            $("#formation_yearvalue").val();
            $("#formation_amount").val('');
        }
        if(ids =='1')
        {
            yearss1 = '<?php $dd=date('Y'); echo $dd;?>';
            $("#formation_yearvalue").val(yearss1);
            $('#formation_amount').val('50.00'); 
        }
        else if(ids =='2')
        {
            yearss1 = '<?php $dd=date('Y'); echo $dd;?>';
            yearss2 = '<?php $dd=date('Y'); echo $dd+1;?>';
            yearss4 = ',';
	        var finalyyear2=yearss1.concat(yearss4).concat(yearss2);
            $("#formation_yearvalue").val(finalyyear2);
            $('#formation_amount').val('100.00'); 
        }
        else if(ids =='3')
        {
            yearss1 = '<?php $dd=date('Y'); echo $dd;?>';
            yearss2 = '<?php $dd=date('Y'); echo $dd+1;?>';
            yearss3 = '<?php $dd=date('Y'); echo $dd+2;?>';
            yearss4 = ',';
            var finalyyear3=yearss1.concat(yearss4).concat(yearss2).concat(yearss4).concat(yearss3);
            $("#formation_yearvalue").val(finalyyear3);
            $('#formation_amount').val('150.00');
        }
        else if(ids =='4')
        {
            $('#formation_amount').val('200.00');
        }
        
        
        // var ids=$(this).val();
        // if(ids=='')
        // {
        //   $("#formation_yearvalue").val('');
        // }
        // if(ids =='1')
        // {
        //   $('#formation_yearvalue').val('2019'); 
        // }
        // else if(ids =='2')
        // {
        //   $('#formation_yearvalue').val('2020'); 
        // }
        // else if(ids =='3')
        // {
        //     $('#formation_yearvalue').val('2021');
        // }
        // else if(ids =='4')
        // {
        //     $('#formation_yearvalue').val('2022');
        // }
    
   });
   
   
      
   
   
      
   
   
   $('.openBtnannual').on('click', function()
   {
      
       var num = $('#clientsid').val();
       $.get('<?php echo URL::to('getWorkannual'); ?>?id='+num, function(data)
       {
            if(data == "")
            {
                $('#myModal2').modal({show:true}); 
            }
            else
            { 
                $.each(data, function(index, subcatobj)
                {
                    // alert(subcatobj.upload_name);
          
                    $('#myModalviewannual').modal({show:true});
                    $('#viewmodelviewannual').html('<embed src="https://financialservicecenter.net/public/adminupload/'+subcatobj.annualreceipt+'" width="100%" height="300px">');
               });
            }
       });
   });
   
    $('.openBtnofficer').on('click', function()
   {
   var num = $('#clientsid').val();
   $.get('<?php echo URL::to('getWorkofficer'); ?>?id='+num, function(data)
   {
   if(data == "")
   {
   $('#myModal3').modal({show:true}); 
   }
   else
   { 
   $.each(data, function(index, subcatobj)
   {
      // alert(subcatobj.upload_name);
  
   $('#myModalviewofficer').modal({show:true});
   $('#viewmodelviewofficer').html('<embed src="https://financialservicecenter.net/public/adminupload/'+subcatobj.formation_work_officer+'" width="100%" height="300px">');
   });
   }
   });
   });
   
     var ftype = document.getElementById('filing_type').value;
     //alert(ftype);
      if(ftype=='Regular Filing')
     {
         $('#filing_year').val('2019');
         $('.regular').show();
          $('.regular2').show();
         $('.amendment2').hide();
         $('.amendment').hide();
     }
     else if(ftype=='Extension Filing')
     {
         $('#filing_year').val('2019');
           $('.regular').show();
         $('.regular2').show();
         $('.amendment2').hide();
         $('.amendment').hide();
         
     }
     else if(ftype=='Amendment Filing')
     {
         $('#filing_year').val('2016');
         $('.regular').hide();
         $('.amendment').show();
         $('.regular2').hide();
         $('.amendment2').show();
     }
    
});



function ShowHideDiv(address) {
        var dvPassport = document.getElementById("addresschangeto");
        addresschangeto.style.display = address.checked ? "block" : "none";
    }
    
    function DivShowHideDiv(officerchange) {
        var dvPassport = document.getElementById("OfficerChange");
        OfficerChange.style.display = officerchange.checked ? "block" : "none";
    }
    
$(document).ready(function()
{
    
    // $('.wagestotal').blur(function ()
    // {
    //     var sum = 0.00;
    //     $('.wagestotal').each(function()
    //     {
    //         sum += Number($(this).val());
    //     });
    //     $('.totalamts').val(sum);
    //      //here, you have your sum
    // });
    
    $('.wagestotal').blur(function ()
    {
        var sum = 0.00;
        $('.wagestotal').each(function()
        {
            sum += Number($(this).val());
        });
        
        $('.totalamts').val(sum);
         //here, you have your sum
    });
    
    
    
    var paidby=$('.paidby').val();
    if(paidby =='Client')
    {
         $('.hidepaid').hide();
    
    }
    else if(paidby == 'FSC')
    {
         $('.hidepaid').show();
    }
    else
    {
         $('.hidepaid').hide();
    }
    
 $('.paidby').on('change',function(){ 
     var thiss=$(this).val();
    // alert(thiss);
     if(thiss=='Client')
     {
         $('.hidepaid').hide();
     }
     else if(thiss =='FSC')
     {
         $('.hidepaid').show();
     }
     else
     {
         $('.hidepaid').hide();
     }
 });   
    
    
    
    $('.annualfees').on('blur',function()
    { 
        var annualfees=$('.annualfees').val();
        var ann=annualfees.replace('$','');
       
        var sign53 =parseFloat(Math.round(ann * 100) / 100).toFixed(2);
	    var annual = sign53.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	    var dollar = '$';
	    var finalyannual=dollar.concat(annual);
        $(".annualfees").val(finalyannual);
});   

    $('.penaltycharges').on('blur',function()
    { 
        var penaltyfees=$('.penaltycharges').val();
        
          var pen=penaltyfees.replace('$','0');
   
        var sign53 =parseFloat(Math.round(pen * 100) / 100).toFixed(2);
	    var penaltyfess = sign53.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	    var dollar = '$';
	    var finalypenaltyfess=dollar.concat(penaltyfess);
        $(".penaltycharges").val(finalypenaltyfess);
});   


    $('.processingfees').on('blur',function()
    { 
    var processingfees=$('.processingfees').val();
    
   
    var ccc= processingfees.replace('$','0');
    var sign53 =parseFloat(Math.round(ccc * 100) / 100).toFixed(2);
    var dollar = '$';
	var finalyprocessingfees=dollar.concat(sign53);   
	$(".processingfees").val(finalyprocessingfees);
	
	var annualfees=$('.annualfees').val();
    var aaa= annualfees.replace('$','');
    var penaltycharges=$('.penaltycharges').val();
    var bbb= penaltycharges.replace('$','');
    
    
    
    if(aaa)
    {
       var annfees= aaa;
    }
    else 
    {
        var annfees= '0.00';
    }
    
    if(bbb)
    {
        var penalty= bbb;
    }
    else
    {
        var penalty= '0.00';
    }
    
    if(ccc)
    {
        var processing= ccc;
    }
    else
    {
        var processing= '0.00';
    }
    
    var totalamts=parseFloat(annfees)+parseFloat(processing)+parseFloat(penalty);
        
        var sign54 =parseFloat(Math.round(totalamts * 100) / 100).toFixed(2);
	    var totalsamts = sign54.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	    //alert(totalsamts);
	    var finalytotalsamts=dollar.concat(totalsamts);
        $(".totalamt").val(finalytotalsamts);
});   

    $('.checkradio').click(function(){

        var inputValue = $(this).attr("value");
//alert(inputValue);
if(inputValue =='Yes')
{
    $(".hideradio").show();
}
else
{
    $(".hideradio").hide();
}
     //   var targetBox = $("." + inputValue);

       // $(".box").not(targetBox).hide();

        //$(targetBox).show();

    });

    $('#country_name').keyup(function(){ 
        var query = $(this).val();
        if(query != '')
        { 
         var _token = $('input[name="_token"]').val();
         $.ajax({
          url:"<?php echo e(route('workrecord.fetch')); ?>",
          method:"POST",
          data:{query:query, _token:_token},
          success:function(data){
           $('#countryList').fadeIn();  
                    $('#countryList').html(data);
          }
         });
        }
    });
    
    
    $('#frms').submit(function(){
        var data = $("#frms").serialize();
         $.ajax({
          url:"https://financialservicecenter.net/fac-Bhavesh-0554/workrecord/fetchupdate",
          method:"POST",
          data : data,
          success:function(data){
          // $('#countryList').fadeIn();  
            //        $('#countryList').html(data);
          }
         });
       
    });

    $(document).on('click', 'tr', function(){  
       // $('#country_name').val($(this).text());  
        $('#countryList').fadeOut();  
        
        $('#file-upload').change(function() {
          var i = $(this).prev('label').clone();
          var file = $('#file-upload')[0].files[0].name;
          $(this).prev('label').text(file);
        });
    });  

});
</script>

<script>
  $( function() {
        $(".federalyear").change(function(){
        
        var feddyear=$(this).val();
       // alert(feddyear);
       var feddtax='<?php echo $clientfedext;?>';
       
        var ContractEndYear = parseFloat(feddyear) + parseFloat(1);
       // alert(ContractEndYear);
       if(feddtax =='1')
       {
           
    //var newdate = ContractEndYear+"-"+9+"-"+15;
    var ttt1=new Date('2025-12-31');
    
       }
       else
       {
          var newdate = ContractEndYear+"-"+7+"-"+15;
          var ttt1=new Date(newdate);
     
       }

       
    //alert(newdate);
    $("#datepicker").datepicker();
     $("#datepicker2").datepicker();
     $(".fdatess").datepicker({autoclose: true,endDate: ttt1});
     $(".fdate").datepicker({autoclose: true,endDate: ttt1});
     
        });
     
  } );
  
  
  $(document).ready(function(){
        $(".add-row").click(function(){
            var markup = "<tr><td><input type='text' name='first_name[]' placeholder='First Name' class='form-control'/></td><td><input type='text' placeholder='Last Name' name='last_name[]' class='form-control'/></td><td><select class='form-control' name='position[]' id='agent_position11'><option>--Select Position--</option><option value='Agent'>Agent</option><option  value='CEO'>CEO</option><option value='Sec'>CEO / CFO / Sec.</option><option value='CFO'>CFO</option><option  value='Secretary'>Secretary</option></select></td><td style='vertical-align:middle;'><a class='delete-row removetr'><i class='fa fa-minus'></i></a></td></tr>";
            $("#officerchange tbody").append(markup);
        });
       
        $("body").on("click",".removetr",function(){ 
            $(this).parent().parent().remove();
        });
        
    }); 
    
      $(document).ready(function()
      {
        
            var cnt=1;
          
            $(".add-rowform").click(function()
            {
               // alert();
                cnt++;
                //$('').show();
                var aa=$('#statetaxx2').html();
                var markup = '<tr><td><select class="form-control statetax'+cnt+'" name="statetax[]" id="statetax'+cnt+'"><option>Select</option>'+aa+'</select></td><td><input type="text" readonly class="form-control formno" name="stateformno[]" id="statefederalformno'+cnt+'" style="width:100px" <?php if($stateform): ?> value="<?php echo e($stateform->taxform); ?>" <?php endif; ?>/></td><td><select class="form-control filingmethod'+cnt+'" name="statemethod[]"><option value="">Select</option><option value="E-File">E-File</option><option value="Paperfile">Paperfile</option></select></td><td><input type="text" class="form-control fdatess'+cnt+'" id="fillingdate" name="statedate[]" id="federaldate'+cnt+'" placeholder="MM/DD/YYYY"/></td><td><select class="form-control" name="statestatus[]" id="federalstatus'+cnt+'"><option value="">Select</option>    <option style="background:green;" class="Green" value="Accepted">Accepted</option><option style="background:blue;" class="Blue" value="EF Processing">EF Processing</option><option style="background:yellow;" class="Yellow" value="Pending">Pending</option><option style="background:red;" classs="Red" value="Rejected">Rejected</option></select></td><td style="width:100px"><label class="file-upload btn btn-primary"><input type="file" name="statefile[]" id="federalfile'+cnt+'" class="form-control">Browse for file</label></td> <td><textarea class="form-control" name="statenote[]" id="federalnote'+cnt+'"></textarea></td><td><a href="#" class="btn btn-danger removetrrow"><i class="fa fa-minus"></i></a></td></tr>';
                $("#taxationtable tbody").append(markup);
               var feddtax='<?php echo $clientfedext;?>';
    
              var feddyear=$('.federalyear').val();
       // alert(feddyear);
       
        var ContractEndYear = parseFloat(feddyear) + parseFloat(1);
       // alert(ContractEndYear);
       if(feddtax =='1')
       {
           
    //var newdate = ContractEndYear+"-"+9+"-"+15;
    var ttt=new Date('2025-12-31');
    
       }
       else
       {
          var newdate = ContractEndYear+"-"+7+"-"+15;
          var ttt=new Date(newdate);
     
       }


                $( function()
                {
                    $('.fdate'+cnt).datepicker({
                        autoclose: true
                    });
                       $(".fdatess"+cnt).datepicker({autoclose: true,endDate: ttt});
  
                    var fdatess=$('.fdatess').val();
                    $('.fdatess'+cnt).val(fdatess);
                    
                });        
                
                 $('.filingmethod'+cnt).on('change', function()
                {
                    var thiss=$(this).val();
                    //alert(thiss);
                      if(thiss == 'E-File')
                    {
                         $('#federalstatus'+cnt).show();
                    
                      $('#federalstatus'+cnt+' option[value="EF Processing"]').attr("selected","selected");
                    }
                      else if(thiss == 'Paperfile')
                      
                      {
                          
                           $('#federalstatus'+cnt).hide();
                        
                     }
                  else
                  {
                      
                           $('#federalstatus'+cnt+' option[value="EF Processing"]').removeAttr("selected");
                 
                      $('#federalstatus'+cnt).show();
                     }
        
                });
                $('.statetax'+cnt).on('change', function()
                {
                       var fdatef=$('.fdatef').val();
               $('.fdatess'+cnt).val(fdatef);
               
                        var statetaxval=$('.statetax'+cnt).val();
                        $.get('<?php echo URL::to('getTaxstatedata'); ?>?statetaxval='+statetaxval,function(data)
                        {  
                            if(data=='')
                            {
                               $('#statefederalformno'+cnt).val(); 
                            }
                            else
                            {
                                $('#statefederalformno'+cnt).val(data.taxform);
                            }
                            
                        });
               });
           
            });
            $("body").on("click",".removetrrow",function(){ 
                $(this).parent().parent().remove();
            });
        
        }); 
    
 
    $('#filing_date').datepicker({
				  	 format: 'M-d-Y',
					 ignoreReadonly: true,					
		});
 
    $(document).ready(function() 
    {
           /* var dateInput = $('input[name="paiddate"]'); 
            var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : 'body';
            dateInput.datepicker({
            format: 'M-d-Y',
            container: container,
            todayHighlight: true,
            autoclose: true,
            startDate:false,
            });*/
        $('#paiddate').datepicker();
        
         $(".opendate").datepicker({
   		    autoclose: true,
            format: "mm/dd/yyyy",
   	    });
  
        $(".income_number").on("input", function(evt) {
		var self = $(this);
		self.val(self.val().replace(/[^\d].+/, ""));
		if ((evt.which < 48 || evt.which > 57)) 
		{
			evt.preventDefault();
		}});  
   	
   	$(".closedate").datepicker({
   		autoclose: true,
         format: "mm/dd/yyyy",
   	});
     
    });
    
    

    function truncateDate(date) {
      return new Date(date.getFullYear(), date.getMonth(), date.getDate());
    }
    
    
  </script>
  <div id="myModal2" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel"><span class="col2"></span></h4>
			</div>-->
			<div class="modal-body">
				<h2>No File Uploaded</h2>
			</div>
			<div class="modal-footer"></div>
		</div>
	</div>
</div>
<div id="myModal3" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel"><span class="col2"></span></h4>
			</div>-->
			<div class="modal-body">
				<h2>No File Uploaded</h2>
			</div>
			<div class="modal-footer"></div>
		</div>
	</div>
</div>

<div id="myModalviewannual" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">View Annual Receipt</h4>
         </div>
         <div class="modal-body">
            <div id="viewmodelviewannual"></div>
         </div>
         <div class="modal-footer"></div>
      </div>
   </div>
</div>

<div id="myModalviewofficer" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">View Officer</h4>
         </div>
         <div class="modal-body">
            <div id="viewmodelviewofficer"></div>
         </div>
         <div class="modal-footer"></div>
      </div>
   </div>
</div>


<!-- Income Modal Start Add -->
<div id="myModalIncome" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <?php if(isset($common->cid)!=''): ?>
    <form method="post" action="<?php echo e(route('workrecord.addincome', $common->cid)); ?>">
    <?php echo e(csrf_field()); ?>

    <?php endif; ?>
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Income</h4>
          </div>
          <div class="modal-body">
              
                            <?php if(isset($common->cid)!=''): ?>
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <label class="control-label text-right">Year</label>
                                </div>
                                <div class="col-md-4">
                                    <?php
                                        $arrs=array();
                                        foreach($incomes as $per) 
                                        {
                                            $arrs[]=$per->wrkyear;
                                            
                                        }
                                    ?>
                                    <select class="form-control" name="wrkyear">
                                        <option>Select</option>
                                        <option value="2019" <?php if(in_array('2019',$arrs)){?>disabled="disabled" <?php }?>>2019</option>
                                        <option value="2018" <?php if(in_array('2018',$arrs)){?>disabled="disabled" <?php }?>>2018</option>
                                    </select>
                                </div>
                            </div>
                            <?php endif; ?>
                            
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <!--<label class="control-label text-right">Wages,salaries,tips,etc.</label>-->
                                    <?php if(isset($common->cid)!=''): ?>
                                    <!--<label class="control-label text-right"><a data-toggle="modal" data-target="#myModalWages" data-id="<?php echo e($common->cid); ?>">Wages</a></label>-->
                                    <!--<label class="control-label text-right"><a class="myModalWages" data-id="<?php echo e($common->cid); ?>">Wages</a></label>-->
                                    <!--<a class="btn_new btn-renew btnaddrecord" data-toggle="modal" data-target="#myModalIncome">Add Record</a>-->
                                    <button type="button" class="btn-action btn-view-edit incomeDetailShow" data-id="<?php echo e($common->cid); ?>">Wages</button>
                                    <?php endif; ?>
                                </div>
                                <div class="col-md-4">
                                    <input type="text" class="form-control wagestotal txtinput_1 income_number" name="wagetotal">
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <label class="control-label text-right">Tax-exempt interest</label>
                                </div>
                                <div class="col-md-4">
                                    <input type="text" class="form-control wagestotal txtinput_1 income_number" name="taxintrest">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <label class="control-label text-right">Qualified dividends</label>
                                </div>
                                <div class="col-md-4">
                                    <input type="text" class="form-control wagestotal txtinput_1 income_number" name="qulifieddividends">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <label class="control-label text-right">Pension and annuities</label>
                                </div>
                                <div class="col-md-4">
                                    <input type="text" class="form-control wagestotal txtinput_1 income_number" name="pension">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <label class="control-label text-right">Social security benefits</label>
                                </div>
                                <div class="col-md-4">
                                    <input type="text" class="form-control wagestotal txtinput_1 income_number" name="socialsecurity">
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <label class="control-label text-right">Capital gain or loss.</label>
                                </div>
                                <div class="col-md-4">
                                    <input type="text" class="form-control wagestotal txtinput_1 income_number" name="capital">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <label class="control-label text-right">Other Income</label>
                                </div>
                                <div class="col-md-4">
                                    <input type="text" class="form-control wagestotal txtinput_1 income_number" name="otherincome">
                                </div>
                            </div>
                            
                         <div class="form-group row">
                                <div class="col-md-3">
                                    <label class="control-label text-right">Total</label>
                                </div>
                                <div class="col-md-4">
                                    <input type="text" class="form-control totalamts txtinput_1 income_number" name="totalamount" readonly>
                                </div>
                            </div>
                           
                        <div class="form-group row">
                            <div class="col-md-3"></div>
                            <div class="col-md-9">
                                <div class="row">
                                <div class="col-md-3">
                                <input type="submit" class="btn_new_save primary" name="submit" value="Save"> </div>
                                 <div class="col-md-3">
                                <a href="#" class="btn_new_cancel">Cancel</a> </div>
                                </div>
                            </div>
                        </div>
                  </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
    </form>
  </div>
</div>
<!-- Modal End -->

<!-- Income Modal Start Update -->
<div id="myModalIncomeUpdate" class="modal fade" role="dialog">
  <div class="modal-dialog">
    
    <form method="post" action="<?php echo e(route('workrecord.updateincome')); ?>">
    <?php echo e(csrf_field()); ?>

    
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Income Update</h4>
          </div>
          
          <input type="hidden" name="incomeid" id="incomeid">
          <div class="modal-body">
              
                            
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <label class="control-label text-right">Year</label>
                                </div>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" name="wrkyear" id="wrkyear" readonly>
                                    
                                    
                                </div>
                            </div>
                            
                            
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <label class="control-label text-right">Wages,salaries,tips,etc.</label>
                                </div>
                                <div class="col-md-4">
                                    <input type="text" class="form-control wagestotal txtinput_1 income_number" name="wagetotal" id="wagetotal">
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <label class="control-label text-right">Tax-exempt interest</label>
                                </div>
                                <div class="col-md-4">
                                    <input type="text" class="form-control wagestotal txtinput_1 income_number" name="taxintrest" id="taxintrest">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <label class="control-label text-right">Qualified dividends</label>
                                </div>
                                <div class="col-md-4">
                                    <input type="text" class="form-control wagestotal txtinput_1 income_number" name="qulifieddividends" id="qulifieddividends">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <label class="control-label text-right">Pension and annuities</label>
                                </div>
                                <div class="col-md-4">
                                    <input type="text" class="form-control wagestotal txtinput_1 income_number" name="pension" id="pension">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <label class="control-label text-right">Social security benefits</label>
                                </div>
                                <div class="col-md-4">
                                    <input type="text" class="form-control wagestotal txtinput_1 income_number" name="socialsecurity" id="socialsecurity">
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <label class="control-label text-right">Capital gain or loss.</label>
                                </div>
                                <div class="col-md-4">
                                    <input type="text" class="form-control wagestotal txtinput_1 income_number" name="capital" id="capital">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <label class="control-label text-right">Other Income</label>
                                </div>
                                <div class="col-md-4">
                                    <input type="text" class="form-control wagestotal txtinput_1 income_number" name="otherincome" id="otherincome">
                                </div>
                            </div>
                            
                         <div class="form-group row">
                                <div class="col-md-3">
                                    <label class="control-label text-right">Total</label>
                                </div>
                                <div class="col-md-4">
                                    <input type="text" class="form-control totalamts txtinput_1 income_number" name="totalamount" id="totalamount" readonly>
                                </div>
                            </div>
                           
                        <div class="form-group row">
                            <div class="col-md-3"></div>
                            <div class="col-md-9">
                                <div class="row">
                                <div class="col-xs-3" style="width:155px">
                                <input type="submit" class="btn_new_save primary" name="submit" value="Save"> </div>
                                <div class="col-xs-3" style="width:155px">
                                <a href="#" class="btn_new_cancel">Cancel</a> </div>
                                </div>
                            </div>
                        </div>
                  </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
    </form>
  </div>
</div>
<!-- Modal End -->

<!-- Income Wages Modal Start-->
 <div id="myModalIncomeDetailShow" class="modal fade" role="dialog">
  <div class="modal-dialog" style="width:850px;">
    <form method="post" action="" class="form-horizontal" enctype="multipart/form-data">
                 
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> Task</h4>
      </div>
      <div class="modal-body">
            <div >
               <div class="table-responsive">
						<table class="table table-hover table-bordered" id="sampleTable2">
							<thead>
								<tr>
									<th style="width:10%">Task Date</th>
									<th>Created By</th>
									<th>Assign To</th>
									<th>Client / Other </th>
									<th>Subject</th>
								    <th>Task Status</th>
                                    <th>Action</th>
								</tr>
							</thead>
							<tbody>
							  
								<tr>
								    
									<td style="width:10%"></td>
									
								</tr>
								
							</tbody>
						</table>
            </div>
        </div>
       
      </div>
      <div class="modal-footer">
          <div class="col-md-3 pull-right" style="padding-left:6px; margin-top:3px; padding-right:0px; text-align:right;">
                       <a href="<?php echo e(url('fac-Bhavesh-0554/task/create')); ?>" class="btn btn-primary"  class="redius">New Task</a>&nbsp;&nbsp;&nbsp; 
                       <a href="<?php echo e(url('fac-Bhavesh-0554/task')); ?>" class="btn btn-primary"  class="redius">Task List</a>
                   </div>
      </div>
    </div>

    </form>
  </div>
</div>
<!-- Modal End -->

<?php $__env->stopSection(); ?>


<?php echo $__env->make('fac-Bhavesh-0554.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>