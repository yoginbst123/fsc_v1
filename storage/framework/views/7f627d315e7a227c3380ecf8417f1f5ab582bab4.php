<?php $__env->startSection('main-content'); ?>
<style>
.fsc-label {
    color: #428bca;
    list-style: none;
}
.pager .disabled>a, .pager .disabled>a:focus, .pager .disabled>a:hover, .pager .disabled>span {
    color: #fff;
    cursor: not-allowed;
    background-color: #428bca;
    font-size: 14px;
}
.pager li>a, .pager li>span {
    display: inline-block;
    padding: 5px 14px;
    background-color: #0472d0;
    border: 1px solid #ddd;
    border-radius: 15px;
    font-size: 14px;
    color: #fff;
}
.main-image{ height: 217px !important;}
.carousel-inner{border: 2px solid #428bca;margin-top: 19px !important;}
p{font-size:17px; padding:10px}
</style>
<div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">
<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 fsc-content-head">
			<h4>FSC Community Link</h4>
		</div>
	</div>

<?php if($employment->isEmpty()): ?>
<div class="row">
    <div class="fsc-content-box col-xs-12">
<center>        <h2>Not Availble at this time</h2></center>
    </div>  </div>                                    
<?php else: ?>



	<?php $__currentLoopData = $employment; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $employ): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

<div class="row">
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  fsc-section-head"><h4 class="datecount"><span class=""><?php echo e($employ->post_date); ?></span></h2></div>
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  fsc-section-head"><center>
<?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<img src="<?php echo e(url('public/linkcategory/')); ?>/<?php echo e($data1->linkimage); ?>" style="width:35%">
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


</center></div>
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  fsc-section-head"><h4 class="datecount"><span class="">ID :  <?php echo e($employ->post_id); ?></span></h2></div>
</div>
		
	</div>
	
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fsc-content-box">
		<div class="" style="margin-top: 2%;">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 "><center><h3 style="display: inline-block; background-color: #103b68;padding: 10px 20px;color: #fff;">Business Detail</h3></center>
</div>
			
			<!--<div class="col-lg-4 col-md-4 col-sm-4 col-xs-10">
				<ul>
					<li class="fsc-label" style='min-height:40px;'>Post ID:</li>
					<li class="fsc-label" style='min-height:40px;'>Job Description :</li>
				</ul>
			</div>-->

<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">

            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
<?php if(empty($employ->linkimage)): ?>
<?php else: ?>
<?php $images = explode('|',$employ->linkimage); ?>
<?php $i=1;?>
<?php $__currentLoopData = $images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<li data-target="#carousel-example-generic" data-slide-to="<?php echo e($i+1); ?>" class="<?php if($loop->first): ?> active <?php endif; ?>"></li>
<?php $i++;?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 

<?php endif; ?>
</ol>
<div class="carousel-inner">
<?php if(empty($employ->linkimage)): ?>
<img src="<?php echo e(url('public/images/')); ?>/noimage.png" class="img-responsive"> <?php else: ?>
<?php $images = explode('|',$employ->linkimage); ?>
<?php $__currentLoopData = $images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<div class="item <?php if($loop->first): ?> active <?php endif; ?>">
<img src="<?php echo e(asset('public/link')); ?>/<?php echo e($image); ?>" alt="" class="main-image" />   
  </div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> <?php endif; ?>
 </div> 
 <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span></a><a class="right carousel-control"
                        href="#carousel-example-generic" data-slide="next"><span class="glyphicon glyphicon-chevron-right">
                        </span></a>
 </div> 
 </div>                
<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
				

<?php if($employ->content==Null): ?>
<center><p>Not Availble at this time</p></center>
<?php else: ?>
<?php echo $employ->content; ?>

<?php endif; ?>
	

			</div>
			
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div style="text-align: center;">
				<a href="<?php echo e(url('contacts/create',[$employ->post_id,$employ->name])); ?>" class="fsc-job-apply-btn">Contact Us</a>
			</div>
<br>
		</div>
	</div>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php echo $employment->links('pagination'); ?>

	<?php endif; ?>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('front-section.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>