-- phpMyAdmin SQL Dump
-- version 4.3.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 01, 2018 at 06:16 AM
-- Server version: 5.5.51-38.2
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `financia_fsc2`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE IF NOT EXISTS `admins` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `fname` varchar(255) DEFAULT NULL,
  `mname` varchar(10) DEFAULT NULL,
  `lname` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `zip` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `job_title` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `address1` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `question1` varchar(255) NOT NULL,
  `question2` varchar(255) NOT NULL,
  `question3` varchar(255) NOT NULL,
  `answer1` varchar(255) NOT NULL,
  `answer2` varchar(255) NOT NULL,
  `remember_token` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `answer3` varchar(255) NOT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `contact_person_name` varchar(255) DEFAULT NULL,
  `company_email` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `dba_name` varchar(255) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `contact_address1` varchar(200) DEFAULT NULL,
  `contact_address2` varchar(200) DEFAULT NULL,
  `contact_fax` varchar(20) DEFAULT NULL,
  `reset_day` varchar(255) DEFAULT NULL,
  `remaining_day` varchar(255) DEFAULT NULL,
  `start_date` varchar(255) DEFAULT NULL,
  `end_date` varchar(255) DEFAULT NULL,
  `flag` int(1) DEFAULT NULL,
  `lock` timestamp NULL DEFAULT NULL,
  `mobiletype` varchar(20) DEFAULT NULL,
  `company_city` varchar(20) DEFAULT NULL,
  `company_state` varchar(25) DEFAULT NULL,
  `company_zip` varchar(25) DEFAULT NULL,
  `company_mobile` varchar(25) DEFAULT NULL,
  `company_mobileType` varchar(25) DEFAULT NULL,
  `telephoneNo1Type` varchar(25) DEFAULT NULL,
  `ext1` varchar(255) DEFAULT NULL,
  `ext2` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `fname`, `mname`, `lname`, `city`, `state`, `zip`, `email`, `job_title`, `password`, `mobile`, `address`, `address1`, `fax`, `question1`, `question2`, `question3`, `answer1`, `answer2`, `remember_token`, `created_at`, `updated_at`, `answer3`, `company_name`, `contact_person_name`, `company_email`, `username`, `dba_name`, `telephone`, `website`, `contact_address1`, `contact_address2`, `contact_fax`, `reset_day`, `remaining_day`, `start_date`, `end_date`, `flag`, `lock`, `mobiletype`, `company_city`, `company_state`, `company_zip`, `company_mobile`, `company_mobileType`, `telephoneNo1Type`, `ext1`, `ext2`) VALUES
(1, 'Mahendra', 'Vijay', 'y', 'Mistry', NULL, 'IL', NULL, 'msbaghel786@gmail.com', '', '$2y$10$HP1XjlRQ0OplF3YRwQa8ZOoZbewwYEW7g5fmguahhiRfggTD0xm5K', '(544) 444-4444', 'Signet Plaza', 'Kunal Char rasta', '(654) 654-6546', 'What is the name of your first school?', 'What is your favorite color?', 'Which is your favorite web browser?', '54654654', '6546546', '45Y2f4uurhIsJcOmoeJEsUMezFCiNwLKCV9sFxRfMErUUa9Nc2MiHFm3mOkW', '2018-01-19 12:49:48', '2018-01-19 13:49:48', '54654', 'Web Street', 'vijay', 'msbaghel7866@gmail.com', 'msbaghel786@gmail.com', 'Vishal', '(543) 454-3541', 'http://localhost/financialservicecenter/', 'G-2, Shreeji', 'Collabera', NULL, '30', '30', '2018-01-11', '2018-02-10', 1, '2018-01-11 22:37:59', 'Home', '5435435', 'IL', '12121', NULL, NULL, 'Office', '53453', '35345');

-- --------------------------------------------------------

--
-- Table structure for table `applyservices`
--

CREATE TABLE IF NOT EXISTS `applyservices` (
  `id` int(11) NOT NULL,
  `serviceid` varchar(255) DEFAULT NULL,
  `firstName` varchar(255) DEFAULT NULL,
  `middleName` varchar(255) DEFAULT NULL,
  `lastName` varchar(254) DEFAULT NULL,
  `address` varchar(244) DEFAULT NULL,
  `address2` varchar(250) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `stateId` varchar(255) DEFAULT NULL,
  `zip` varchar(244) DEFAULT NULL,
  `countryId` varchar(255) DEFAULT NULL,
  `telephoneNo1` varchar(255) DEFAULT NULL,
  `telephoneNo1Type` varchar(255) DEFAULT NULL,
  `telephoneNo2` varchar(255) DEFAULT NULL,
  `telephoneNo2Type` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `serviceTypeId` varchar(255) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `applyservices`
--

INSERT INTO `applyservices` (`id`, `serviceid`, `firstName`, `middleName`, `lastName`, `address`, `address2`, `city`, `stateId`, `zip`, `countryId`, `telephoneNo1`, `telephoneNo1Type`, `telephoneNo2`, `telephoneNo2Type`, `fax`, `email`, `serviceTypeId`, `note`, `status`, `created_at`, `updated_at`) VALUES
(3, '16', 'mahendra', 's', 'baghel', 'vadodara', 'Gotri Road', 'Vadodara', '13', '4234', '1', '(432) 432-4324', 'Mobile', '(432) 432-4432', 'Home', '(432) 432-4324', 'pathan.chauhan@gmail.com', '4', '4324324', NULL, '2018-01-03 15:07:42', '2017-12-13 02:59:02');

-- --------------------------------------------------------

--
-- Table structure for table `apply_employments`
--

CREATE TABLE IF NOT EXISTS `apply_employments` (
  `id` int(11) NOT NULL,
  `employment_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `firstName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `middleName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address1` text COLLATE utf8_unicode_ci,
  `address2` text COLLATE utf8_unicode_ci,
  `city` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stateId` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `countryId` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telephoneNo1` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telephoneNo2` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telephoneNo1Type` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telephoneNo2Type` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `requiremnetId` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resume` varchar(2555) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT '3',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `employee_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `candidate_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `newapp` int(11) DEFAULT '1'
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `branches`
--

CREATE TABLE IF NOT EXISTS `branches` (
  `id` int(11) NOT NULL,
  `positionid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `branchname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `branches`
--

INSERT INTO `branches` (`id`, `positionid`, `country`, `city`, `branchname`, `status`, `created_at`, `updated_at`) VALUES
(1, 'FSC', 'IN', 'vadodara', 'FSC-Vadodara', 0, '2018-01-10 12:45:17', '2018-01-11 00:15:17'),
(3, 'FSC', 'USA', 'Ahemdabad', 'FSC-Ahemdabad', 0, '2018-01-14 00:34:50', '2018-01-14 00:34:50');

-- --------------------------------------------------------

--
-- Table structure for table `businesses`
--

CREATE TABLE IF NOT EXISTS `businesses` (
  `id` int(11) NOT NULL,
  `bussiness_name` varchar(255) NOT NULL,
  `bussiness_image_name` varchar(244) NOT NULL,
  `status` int(11) DEFAULT '0',
  `link` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `businesses`
--

INSERT INTO `businesses` (`id`, `bussiness_name`, `bussiness_image_name`, `status`, `link`, `created_at`, `updated_at`) VALUES
(1, 'Business', 'Business.png', 0, 'business-registration', '2017-11-28 09:31:04', '2017-11-15 04:27:56'),
(2, 'Non-Profit Organization', 'Non-Profits-Org.png', 0, 'comman-registration/create', '2017-11-28 09:31:08', '2017-11-15 04:25:14'),
(3, 'Service Industry', 'ServiceIndustry.png', 0, 'service-industry-registration', '2017-11-15 11:22:06', '2017-11-15 04:25:38'),
(4, 'Profession', 'Professions.png', 0, 'professions-registration', '2017-11-15 11:22:08', '2017-11-15 04:25:59'),
(5, 'Investor', 'Investor.png', 0, 'investor-registration', '2017-11-15 11:21:41', '2017-11-15 04:26:17'),
(6, 'Personal', 'Personal.png', 0, 'comman-registration/create', '2017-11-28 09:31:11', '2017-11-15 04:24:54');

-- --------------------------------------------------------

--
-- Table structure for table `business_brands`
--

CREATE TABLE IF NOT EXISTS `business_brands` (
  `id` int(11) NOT NULL,
  `business_id` varchar(255) NOT NULL,
  `business_cat_id` varchar(255) NOT NULL,
  `business_brand_name` varchar(255) NOT NULL,
  `business_brand_image` varchar(255) NOT NULL,
  `status` int(11) DEFAULT '0',
  `link` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `business_brands`
--

INSERT INTO `business_brands` (`id`, `business_id`, `business_cat_id`, `business_brand_name`, `business_brand_image`, `status`, `link`, `created_at`, `updated_at`) VALUES
(3, '1', '5', 'Brand', 'Brand.png', 0, 'business-brand-category-list', '2017-11-28 09:36:34', '2017-11-23 21:19:38'),
(5, '1', '5', 'Non Brand', 'Non-Brand.png', 0, 'comman-registration/create', '2017-11-28 09:31:19', '2017-11-20 01:53:36');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL,
  `bussiness_name` varchar(255) NOT NULL,
  `business_cat_name` varchar(255) NOT NULL,
  `business_cat_image` varchar(243) NOT NULL,
  `status` int(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `bussiness_name`, `business_cat_name`, `business_cat_image`, `status`, `created_at`, `updated_at`, `link`) VALUES
(1, '1', 'Auto Broker Dealer', 'Auto_Broker_Dealer.png', NULL, '2017-11-28 09:31:26', '2017-11-15 07:16:39', 'comman-registration/create'),
(2, '1', 'Cloth Store', 'Cloth_Store.png', NULL, '2017-11-28 09:31:30', '2017-11-15 07:23:23', 'comman-registration/create'),
(3, '1', 'Cup Cake', 'Cup_Cake.png', NULL, '2017-11-28 09:31:35', '2017-11-15 07:24:02', 'comman-registration/create'),
(4, '1', 'Fast Food Restaurant', 'Fast_Food_Restaurant.png', NULL, '2017-11-15 07:24:49', '2017-11-15 07:24:49', 'brand-nonbrand'),
(5, '1', 'Gas Station', 'Gas_Station.png', NULL, '2017-11-15 07:25:48', '2017-11-15 07:25:48', 'brand-nonbrand'),
(6, '1', 'General Merchandise Store', 'General_Merchindise_Store.png', NULL, '2017-11-28 09:31:39', '2017-11-15 07:26:48', 'comman-registration/create'),
(7, '1', 'Grocery Store', 'Groccery_Store.png', NULL, '2017-11-28 09:31:44', '2017-11-15 07:27:26', 'comman-registration/create'),
(8, '1', 'Hotel/Motel', 'Hotel_Motel.png', NULL, '2017-11-15 07:28:05', '2017-11-15 07:28:05', 'brand-nonbrand'),
(9, '1', 'Jewellery Store', 'Jewellery_Store.png', NULL, '2017-11-28 09:31:52', '2017-11-15 07:28:37', 'comman-registration/create'),
(10, '1', 'Liquor Store', 'Liquor_Store.png', NULL, '2017-11-28 09:31:47', '2017-11-15 07:29:33', 'comman-registration/create'),
(11, '1', 'Pizza Store', 'Pizza_Store.png', NULL, '2017-11-15 07:31:11', '2017-11-15 07:31:11', 'brand-nonbrand'),
(12, '1', 'Restaurant', 'Restaurant.png', NULL, '2017-11-15 07:33:12', '2017-11-15 07:33:12', 'brand-nonbrand'),
(13, '1', 'Sandwich Store', 'Sandwich_Store.png', NULL, '2017-11-15 07:33:51', '2017-11-15 07:33:51', 'brand-nonbrand'),
(14, '1', 'Tobacco Store', 'Tobacco_Store.png', NULL, '2017-11-28 09:31:55', '2017-11-15 07:34:22', 'comman-registration/create'),
(15, '1', 'Wholesale Store', 'Wholesale_Store.png', NULL, '2017-11-28 09:32:00', '2017-11-15 07:35:08', 'comman-registration/create'),
(16, '3', 'Accounting/Taxation', 'Accounting-Taxation-Service.png', NULL, '2017-12-20 12:07:00', '2017-12-20 23:37:00', 'comman-registration/create'),
(17, '3', 'Dry Cleaner', 'Dry-Cleaner.png', NULL, '2017-11-28 09:32:09', '2017-11-17 09:33:27', 'comman-registration/create'),
(18, '3', 'General Contractor', 'General-Contractor.png', NULL, '2017-11-28 09:32:12', '2017-11-17 09:33:10', 'comman-registration/create'),
(19, '3', 'Hair Salon & Spa', 'Hair-Salon-Spa.png', NULL, '2017-11-28 09:32:17', '2017-11-17 09:32:58', 'comman-registration/create'),
(20, '3', 'Photography', 'Photography.png', NULL, '2017-11-28 09:32:21', '2017-11-17 09:32:51', 'comman-registration/create'),
(21, '3', 'Supply Service', 'Supply-Service.png', NULL, '2017-11-28 09:32:26', '2017-11-17 09:32:44', 'comman-registration/create'),
(22, '3', 'Travel Agent', 'Travel-Agent.png', NULL, '2017-11-28 09:32:30', '2017-11-17 09:32:29', 'comman-registration/create'),
(23, '3', 'Web Design', 'Web-Design.png', NULL, '2017-11-28 09:32:36', '2017-11-17 09:32:13', 'comman-registration/create'),
(24, '3', 'Amusement Gaming Maching', 'Amusement-Gaming-Machine.png', NULL, '2017-11-28 09:32:40', '2017-11-17 09:32:05', 'comman-registration/create'),
(25, '3', 'Limo Taxi', 'Limo-Taxi.png', NULL, '2017-11-28 09:32:44', '2017-11-17 09:31:51', 'comman-registration/create'),
(27, '4', 'Consulting', 'Consulting.png', NULL, '2017-11-28 09:32:53', '2017-11-17 09:31:15', 'comman-registration/create'),
(28, '4', 'Doctor', 'Doctor.png', NULL, '2017-11-28 09:32:57', '2017-11-17 09:30:22', 'comman-registration/create'),
(29, '4', 'Insurance Agent', 'Insurance-Agent.png', NULL, '2017-11-28 09:33:00', '2017-11-17 09:31:03', 'comman-registration/create'),
(30, '5', 'Commercial Property Investor', 'Commercial-Property-Investor.png', NULL, '2017-11-28 09:33:05', '2017-11-17 09:29:23', 'comman-registration/create'),
(31, '5', 'Residential Property Investor', 'Residential-Property-Investor.png', NULL, '2017-11-28 09:33:09', '2017-11-17 09:29:01', 'comman-registration/create');

-- --------------------------------------------------------

--
-- Table structure for table `categorybusinesses`
--

CREATE TABLE IF NOT EXISTS `categorybusinesses` (
  `id` int(11) NOT NULL,
  `business_id` varchar(255) NOT NULL,
  `business_cat_id` varchar(255) NOT NULL,
  `business_brand_id` varchar(255) NOT NULL,
  `business_brand_category_name` varchar(255) NOT NULL,
  `business_brand_category_image` varchar(266) NOT NULL,
  `status` int(11) DEFAULT '0',
  `link` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categorybusinesses`
--

INSERT INTO `categorybusinesses` (`id`, `business_id`, `business_cat_id`, `business_brand_id`, `business_brand_category_name`, `business_brand_category_image`, `status`, `link`, `created_at`, `updated_at`) VALUES
(5, '1', '5', '3', '7-Eleven', '1494392443gas-brand-1.png', 0, 'comman-registration/create', '2017-12-20 12:26:12', '2017-12-20 23:56:12'),
(6, '1', '5', '3', 'Amoco', '1494392460gas-brand-2.png', 0, 'comman-registration/create', '2017-12-20 12:27:24', '2017-12-20 23:57:24'),
(7, '1', '5', '3', 'Am-Pm', '1494392475gas-brand-3.png', 0, 'comman-registration/create', '2017-12-20 23:58:03', '2017-12-20 23:58:03'),
(8, '1', '5', '3', 'Chevron', '1494392490gas-brand-5.png', 0, 'comman-registration/create', '2017-12-20 12:28:50', '2017-12-20 23:58:50'),
(9, '1', '5', '3', 'CITGO', '1496729774Citgo.png', 0, 'comman-registration/create', '2017-12-20 23:59:26', '2017-12-20 23:59:26'),
(10, '1', '5', '3', 'BP', '1496729812bp.png', 0, 'comman-registration/create', '2017-12-21 00:01:03', '2017-12-21 00:01:03'),
(11, '1', '5', '3', 'Exxon', '1496729860Exxon.png', 0, 'comman-registration/create', '2017-12-21 00:01:50', '2017-12-21 00:01:50'),
(12, '1', '5', '3', 'Marthone', '1496729919Marthon.png', 0, 'comman-registration/create', '2017-12-21 00:02:42', '2017-12-21 00:02:42'),
(13, '1', '5', '3', 'Mobillgas', '1496729963Mobillgas.png', 0, 'comman-registration/create', '2017-12-21 00:04:37', '2017-12-21 00:04:37'),
(14, '1', '5', '3', 'Philips', '1496730007Philips66.png', 0, 'comman-registration/create', '2017-12-21 00:05:15', '2017-12-21 00:05:15'),
(15, '1', '5', '3', 'Shell', '1496730044Shell.png', 0, 'comman-registration/create', '2017-12-21 00:05:42', '2017-12-21 00:05:42'),
(16, '1', '5', '3', 'Speedway', '1496730072Speedway.png', 0, 'comman-registration/create', '2017-12-21 00:06:15', '2017-12-21 00:06:15'),
(17, '1', '5', '3', 'Sundco', '1496730165Sundco.png', 0, 'comman-registration/create', '2017-12-21 00:07:15', '2017-12-21 00:07:15'),
(18, '1', '5', '3', 'Texaco', '1496730203Texaco.png', 0, 'comman-registration/create', '2017-12-21 00:07:45', '2017-12-21 00:07:45'),
(19, '1', '5', '3', 'Valero', '1496730233valero.png', 0, 'comman-registration/create', '2017-12-21 00:08:30', '2017-12-21 00:08:30');

-- --------------------------------------------------------

--
-- Table structure for table `commonregisters`
--

CREATE TABLE IF NOT EXISTS `commonregisters` (
  `register_id` varchar(255) NOT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `business_name` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `middle_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address` text,
  `address1` text,
  `city` varchar(255) DEFAULT NULL,
  `stateId` varchar(255) DEFAULT NULL,
  `zip` varchar(255) DEFAULT NULL,
  `countryId` varchar(255) DEFAULT NULL,
  `mobile_no` varchar(255) DEFAULT NULL,
  `business_no` varchar(255) DEFAULT NULL,
  `business_fax` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `user_type` varchar(255) DEFAULT NULL,
  `business_id` varchar(255) DEFAULT NULL,
  `business_cat_id` varchar(255) DEFAULT NULL,
  `business_brand_id` varchar(255) DEFAULT NULL,
  `business_brand_category_id` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `mailing_address` varchar(255) DEFAULT NULL,
  `mailing_address1` varchar(255) DEFAULT NULL,
  `legalname` varchar(255) DEFAULT NULL,
  `dbaname` varchar(255) DEFAULT NULL,
  `mailing_city` varchar(255) DEFAULT NULL,
  `mailing_state` varchar(255) DEFAULT NULL,
  `mailing_zip` varchar(255) DEFAULT NULL,
  `fileid` varchar(255) DEFAULT NULL,
  `bussiness_zip` varchar(255) DEFAULT NULL,
  `business_state` varchar(255) DEFAULT NULL,
  `business_city` varchar(255) DEFAULT NULL,
  `business_country` varchar(255) DEFAULT NULL,
  `business_address` varchar(255) DEFAULT NULL,
  `business_store_name` varchar(255) DEFAULT NULL,
  `level` varchar(255) DEFAULT NULL,
  `setup_state` varchar(255) DEFAULT NULL,
  `county_name` varchar(255) DEFAULT NULL,
  `county_no` varchar(255) DEFAULT NULL,
  `type_of_activity` varchar(255) DEFAULT NULL,
  `department` varchar(255) DEFAULT NULL,
  `due_date` varchar(255) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `personname` varchar(255) DEFAULT NULL,
  `relation` varchar(244) DEFAULT NULL,
  `eaddress1` varchar(255) DEFAULT NULL,
  `per_city` varchar(255) DEFAULT NULL,
  `per_stateId` varchar(245) DEFAULT NULL,
  `per_zip` varchar(255) DEFAULT NULL,
  `etelephone1` varchar(255) DEFAULT NULL,
  `eteletype1` varchar(255) DEFAULT NULL,
  `eext1` varchar(10) DEFAULT NULL,
  `eext2` varchar(10) DEFAULT NULL,
  `eteletype2` varchar(255) DEFAULT NULL,
  `etelephone2` varchar(255) DEFAULT NULL,
  `comments` varchar(255) DEFAULT NULL,
  `marital` varchar(10) DEFAULT NULL,
  `month` varchar(100) DEFAULT NULL,
  `day` varchar(10) DEFAULT NULL,
  `year` varchar(10) DEFAULT NULL,
  `pf1` varchar(2555) DEFAULT NULL,
  `pf2` varchar(2555) DEFAULT NULL,
  `eteletype3` varchar(255) DEFAULT NULL,
  `eteletype4` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `count` int(11) DEFAULT '0',
  `newclient` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `commonregisters`
--

INSERT INTO `commonregisters` (`register_id`, `company_name`, `business_name`, `first_name`, `middle_name`, `last_name`, `email`, `address`, `address1`, `city`, `stateId`, `zip`, `countryId`, `mobile_no`, `business_no`, `business_fax`, `website`, `user_type`, `business_id`, `business_cat_id`, `business_brand_id`, `business_brand_category_id`, `status`, `created_at`, `updated_at`, `mailing_address`, `mailing_address1`, `legalname`, `dbaname`, `mailing_city`, `mailing_state`, `mailing_zip`, `fileid`, `bussiness_zip`, `business_state`, `business_city`, `business_country`, `business_address`, `business_store_name`, `level`, `setup_state`, `county_name`, `county_no`, `type_of_activity`, `department`, `due_date`, `filename`, `personname`, `relation`, `eaddress1`, `per_city`, `per_stateId`, `per_zip`, `etelephone1`, `eteletype1`, `eext1`, `eext2`, `eteletype2`, `etelephone2`, `comments`, `marital`, `month`, `day`, `year`, `pf1`, `pf2`, `eteletype3`, `eteletype4`, `gender`, `count`, `newclient`) VALUES
('GA-001', 'web street', 'web development', 'vijay', 's', 'mistry', 'msbaghel786@gmail.com', 'vadodara', 'tf 21 22 signet plaza zydex company', 'vadodra', 'IL', '6546', 'USA', '(153) 453-4534', '(153) 455-5555', '(154) 353-4534', 'http://financialservicecenter.net/home#', 'Non-Profit Organization', '2', '1', NULL, NULL, 1, '2018-02-01 11:36:02', '2018-02-01 23:05:55', NULL, NULL, 'vijay', 'Data', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '01/04/2018', '4535354435', '435435', '645654', '456546', 'fdsfdsfds', 'AR', '6546', '+1 (666) 666-6666', 'Resident', '031', '031', 'Office', '+1 (666) 666-6666', '6', 'married', 'Feb', '01', '2018', 'file-not-found.jpg', 'file-not-found.jpg', 'Driving Licence', 'Driving Licence', 'Male', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE IF NOT EXISTS `contacts` (
  `id` int(11) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `latitude` varchar(255) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `placename` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `email`, `latitude`, `longitude`, `status`, `created_at`, `updated_at`, `placename`, `phone`, `fax`) VALUES
(2, 'vijay@financialservicecenter.net', '33.883599', '-84.191065', NULL, '2017-12-11 10:09:17', '2017-12-11 16:09:17', '4550 Jimmy Carter Blvd.    Norcross,GA 30093', '(770) 270-5597', '(770) 414-5351');

-- --------------------------------------------------------

--
-- Table structure for table `contact_userinfos`
--

CREATE TABLE IF NOT EXISTS `contact_userinfos` (
  `id` int(11) NOT NULL,
  `user_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_person_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `business` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `business_fax` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `residence` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cell` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `residence_fax` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `telephone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=50 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `contact_userinfos`
--

INSERT INTO `contact_userinfos` (`id`, `user_id`, `contact_person_name`, `business`, `business_fax`, `residence`, `cell`, `residence_fax`, `email`, `created_at`, `updated_at`, `telephone`) VALUES
(42, 'GA-006', '', '', '', '', '', '', '', NULL, NULL, ''),
(49, 'GA-001', '', '', '', '', '', '', '', NULL, NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE IF NOT EXISTS `employees` (
  `id` int(11) NOT NULL,
  `employee_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `firstName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `middleName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stateId` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `countryId` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telephoneNo1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telephoneNo1Type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telephoneNo2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telephoneNo2Type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ext1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ext2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hiremonth` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hireday` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hireyear` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `termimonth` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `termiday` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `termiyear` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tnote` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rehiremonth` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rehireday` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rehireyear` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `branch_city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `branch_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_rev_day` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reviewmonth` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reviewday` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reviewyear` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hiring_comments` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pay_method` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pay_scale` varchar(254) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fields` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pay_frequency` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `marital` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `month` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `day` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `year` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pf1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pfid1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pf2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pfid2` varchar(245) COLLATE utf8_unicode_ci DEFAULT NULL,
  `epname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `relation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `eaddress1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ecity` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estate` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ezipcode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `etelephone1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `eteletype1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `eext1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `etelephone2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `eteletype2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `eext2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comments1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `question1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `answer1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `question2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `answer2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `question3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `answer3` varchar(244) COLLATE utf8_unicode_ci DEFAULT NULL,
  `other_info` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `computer_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `computer_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `check` int(11) DEFAULT '0',
  `comments` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fax` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `filling_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fedral_claim` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `additional_withholding` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state_claim` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `additional_withholding_1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `local_claim` varchar(244) COLLATE utf8_unicode_ci DEFAULT NULL,
  `additional_withholding_2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resume` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type_agreement` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `firstName_1` text COLLATE utf8_unicode_ci,
  `agreement` text COLLATE utf8_unicode_ci,
  `middleName_1` text COLLATE utf8_unicode_ci,
  `lastName_1` text COLLATE utf8_unicode_ci,
  `address11` text COLLATE utf8_unicode_ci,
  `efax` text COLLATE utf8_unicode_ci,
  `reset` text COLLATE utf8_unicode_ci,
  `work_responsibility` text COLLATE utf8_unicode_ci,
  `employee_rules` text COLLATE utf8_unicode_ci,
  `eemail` text COLLATE utf8_unicode_ci,
  `additional_attach` text COLLATE utf8_unicode_ci,
  `additional_attach_1` text COLLATE utf8_unicode_ci,
  `additional_attach_2` text COLLATE utf8_unicode_ci,
  `count` int(11) DEFAULT '1',
  `newemp` int(11) DEFAULT '1'
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `employee_id`, `firstName`, `middleName`, `lastName`, `address1`, `address2`, `city`, `stateId`, `zip`, `countryId`, `telephoneNo1`, `telephoneNo1Type`, `telephoneNo2`, `telephoneNo2Type`, `email`, `photo`, `ext1`, `ext2`, `hiremonth`, `hireday`, `hireyear`, `termimonth`, `termiday`, `termiyear`, `tnote`, `rehiremonth`, `rehireday`, `rehireyear`, `branch_city`, `branch_name`, `position`, `note`, `first_rev_day`, `reviewmonth`, `reviewday`, `reviewyear`, `hiring_comments`, `pay_method`, `pay_scale`, `fields`, `pay_frequency`, `gender`, `marital`, `month`, `day`, `year`, `pf1`, `pfid1`, `pf2`, `pfid2`, `epname`, `relation`, `eaddress1`, `ecity`, `estate`, `ezipcode`, `etelephone1`, `eteletype1`, `eext1`, `etelephone2`, `eteletype2`, `eext2`, `comments1`, `uname`, `password`, `question1`, `answer1`, `question2`, `answer2`, `question3`, `answer3`, `other_info`, `computer_name`, `computer_ip`, `status`, `created_at`, `updated_at`, `check`, `comments`, `fax`, `filling_status`, `fedral_claim`, `additional_withholding`, `state_claim`, `additional_withholding_1`, `local_claim`, `additional_withholding_2`, `resume`, `type_agreement`, `firstName_1`, `agreement`, `middleName_1`, `lastName_1`, `address11`, `efax`, `reset`, `work_responsibility`, `employee_rules`, `eemail`, `additional_attach`, `additional_attach_1`, `additional_attach_2`, `count`, `newemp`) VALUES
(4, 'IN-GA-0002', 'harsh', 's', 'mistry', 'tf 21 22 signet plaza zydex company', 'tf 21 22 signet plaza zydex company', 'vadodra', 'IN', '4324', 'USA', '+1 (543) 333-3333', 'Office', '+1 (543) 543-5435', 'Office', 'harshmistry799@gmail.com', 'file-not-found.png', '53545', '54354', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Male', 'Married', '21-Feb-2018', NULL, NULL, 'Pan Card', 'file-not-found.png', 'Driving Licence', 'file-not-found.jpg', NULL, '54654', NULL, 'B', 'HI', '7657', '+1 (657) 657-6575', 'Office', '765', '+1 (766) 666-6666', 'Office', NULL, '765', NULL, '$2y$10$UmZw2xS17xqbuDB50pTTxu/5pp.6EbnsDTU6t1VBQ7qEavqmHo3.S', 'What was your favorite place to visit as a child?', '64654', 'What was the make of your first car?', '654654', 'Which is your favorite web browser?', '6546', NULL, NULL, NULL, 1, '2018-02-01 10:49:04', '2018-02-01 22:19:04', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '6546546546', NULL, NULL, NULL, '6546', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 2),
(5, 'IN-GA-0003', 'vijay', 'm', 'mistry', 'tf 21 22 signet plaza zydex company', 'tf 21 22 signet plaza zydex company', 'vadodra', 'ID', '5345', 'USA', '(154) 354-3543', 'Office', '(154) 354-3543', 'Office', 'vijaymistry2311@gmail.com', NULL, '54354', '54354', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Ahemdabad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Salary', NULL, NULL, 'Weekly', 'Male', 'UnMarried', '31-Jan-2018', NULL, NULL, 'Driving Licence', NULL, NULL, NULL, NULL, '654', NULL, '654654654654', 'ID', '6546', '(154) 365-4654', 'Office', NULL, '(165) 465-4654', 'Office', NULL, NULL, NULL, '', 'In what city were you born?', '56756756', 'What is your favorite color?', '6556546', 'What is the name of your favorite childhood friend?', '654654', NULL, NULL, NULL, 0, '2018-02-01 10:25:57', '2018-02-01 21:55:57', 0, NULL, NULL, 'Single', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '6546546', NULL, NULL, NULL, '654654', NULL, '120', NULL, NULL, NULL, NULL, NULL, NULL, 1, 2),
(8, 'IN-GA-0004', 'harsh', 's', 'mistry', 'tf 21 22 signet plaza zydex company', 'tf 21 22 signet plaza zydex company', 'Ahemdabad', 'AS', '6546', 'USA', '+1 (654) 444-444', 'Mobile', '+1 (654) 654-654', 'Mobile', 'msbaghel786@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Male', 'Married', '31-Jan-2018', NULL, NULL, 'Driving Licence', NULL, 'Voter Id', NULL, NULL, '5435345', NULL, '5435345', 'ID', '4324', '+1 (534) 534-535', 'Resident', NULL, '+1 (432) 432-432', 'Resident', NULL, NULL, NULL, '$2y$10$A8GR3ygQIzaBA17HHZecXuVYqldn41QmrmNZhv8lEhVNGC.4dK/Ly', 'What was your favorite place to visit as a child?', '54654654', 'What is your favorite color?', '346546', 'What was your high school mascot?', '654645', NULL, NULL, NULL, 1, '2018-02-01 11:39:14', '2018-02-01 23:09:14', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '53534', NULL, NULL, NULL, '34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `employee_other_info`
--

CREATE TABLE IF NOT EXISTS `employee_other_info` (
  `id` int(11) NOT NULL,
  `employee_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `work_responsibility` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `employee_rules` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM AUTO_INCREMENT=49 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `employee_other_info`
--

INSERT INTO `employee_other_info` (`id`, `employee_id`, `work_responsibility`, `employee_rules`, `created_at`, `updated_at`) VALUES
(8, '3', '646654', NULL, '2018-01-31 03:01:56', '0000-00-00 00:00:00'),
(46, '4', '', NULL, '2018-02-01 10:44:13', '0000-00-00 00:00:00'),
(7, '3', '5435435', NULL, '2018-01-31 03:01:56', '0000-00-00 00:00:00'),
(48, '8', '', NULL, '2018-02-01 11:35:47', '0000-00-00 00:00:00'),
(10, '6', '', NULL, '2018-01-31 05:19:17', '0000-00-00 00:00:00'),
(41, '5', '', NULL, '2018-02-01 10:25:57', '0000-00-00 00:00:00'),
(32, '7', '', NULL, '2018-01-31 07:21:25', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `employee_pay_info`
--

CREATE TABLE IF NOT EXISTS `employee_pay_info` (
  `id` int(11) NOT NULL,
  `employee_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pay_method` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pay_frequency` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pay_scale` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `effective_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fields` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM AUTO_INCREMENT=48 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `employee_pay_info`
--

INSERT INTO `employee_pay_info` (`id`, `employee_id`, `pay_method`, `pay_frequency`, `pay_scale`, `effective_date`, `fields`, `created_at`, `updated_at`) VALUES
(47, '8', 'Salary', 'Weekly', '', '', '', '2018-02-01 11:35:47', '0000-00-00 00:00:00'),
(7, '3', 'Hourly', 'Weekly', '23', '01/30/2018', '423423423432', '2018-01-31 03:01:56', '0000-00-00 00:00:00'),
(45, '4', 'Salary', 'Weekly', '', '', '', '2018-02-01 10:44:13', '0000-00-00 00:00:00'),
(9, '6', 'Salary', 'Weekly', '', '', '', '2018-01-31 05:19:17', '0000-00-00 00:00:00'),
(40, '5', 'Salary', 'Weekly', '56', '01/31/2018', '', '2018-02-01 10:25:57', '0000-00-00 00:00:00'),
(31, '7', 'Salary', 'Weekly', '', '', '', '2018-01-31 07:21:25', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `employee_review`
--

CREATE TABLE IF NOT EXISTS `employee_review` (
  `id` int(11) NOT NULL,
  `employee_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_rev_day` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reviewmonth` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hiring_comments` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM AUTO_INCREMENT=45 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `employee_review`
--

INSERT INTO `employee_review` (`id`, `employee_id`, `first_rev_day`, `reviewmonth`, `hiring_comments`, `created_at`, `updated_at`) VALUES
(3, '3', '34', '01/30/2018', '543543534', '2018-01-30 08:30:14', '0000-00-00 00:00:00'),
(42, '4', '', '02/05/2018', '', '2018-02-01 10:44:13', '0000-00-00 00:00:00'),
(44, '8', '', '', '', '2018-02-01 11:35:47', '0000-00-00 00:00:00'),
(6, '6', '', '', '', '2018-01-31 05:19:17', '0000-00-00 00:00:00'),
(37, '5', '30', '01/31/2018', '46546', '2018-02-01 10:25:57', '0000-00-00 00:00:00'),
(28, '7', '', '', '', '2018-01-31 07:21:25', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `employments`
--

CREATE TABLE IF NOT EXISTS `employments` (
  `id` int(255) NOT NULL,
  `position_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(3434) COLLATE utf8_unicode_ci NOT NULL,
  `date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `count` int(11) DEFAULT '1'
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `employments`
--

INSERT INTO `employments` (`id`, `position_name`, `country`, `state`, `city`, `description`, `date`, `type`, `link`, `status`, `created_at`, `updated_at`, `count`) VALUES
(7, 'Office Assistant', 'USA', 'GA', 'NORCROSS', '<p><strong>RESPONSIBLE FOR:&nbsp;</strong><br /> Under the direct supervision of the Office Supervisor, performs a variety of office assistant duties in support of a CPA practice following established methods and procedures.</p>\r\n<p>High school graduate or G.E.D. equivalent</p>\r\n<p>Require 1 years of minimum related experience.</p>\r\n<p>Responsible for, Answering the phones, Scanning the document, Documents management, Fax- Email Management, File management, Data entry and more.</p>', 'Jan-17-2018', 'Full Time', 'http://financialservicecenter.net/admin/employment/create', NULL, '2018-01-17 17:28:23', '2018-01-17 17:28:23', 1),
(8, 'Assistant Bookkeeper', 'USA', 'GA', 'NORCROSS', '<p><strong>RESPONSIBLE FOR:&nbsp;</strong></p>\r\n<p>Require accounting knowledge, minimum 1 year of experience, college degree in accounting preferably. Job requires have data entry in the system. Night shift.<br />Must knowledge of Computerize accounting and accounting software.</p>\r\n<p>&nbsp;</p>', 'Jan-17-2018', 'Full Time', 'http://financialservicecenter.net/admin/employment/create', NULL, '2018-01-17 16:37:40', '2018-01-18 04:07:40', 1);

-- --------------------------------------------------------

--
-- Table structure for table `empschedules`
--

CREATE TABLE IF NOT EXISTS `empschedules` (
  `id` int(11) NOT NULL,
  `employee_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `emp_in_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `emp_in` timestamp NULL DEFAULT NULL,
  `emp_out` timestamp NULL DEFAULT NULL,
  `launch_in` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `launch_out` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `work_note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `edit_notes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adminnote` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flagip` int(11) DEFAULT '2',
  `flag_edit` int(11) DEFAULT '2',
  `status` int(11) DEFAULT '2',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `clock_in` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `clock_out` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `empschedules`
--

INSERT INTO `empschedules` (`id`, `employee_id`, `emp_in_date`, `emp_in`, `emp_out`, `launch_in`, `launch_out`, `note`, `work_note`, `notes`, `edit_notes`, `adminnote`, `ip_address`, `flagip`, `flag_edit`, `status`, `created_at`, `updated_at`, `clock_in`, `clock_out`) VALUES
(8, '4', '02-01-2018', '2018-02-01 10:19:02', NULL, '2018-02-01 05:41:28', NULL, NULL, NULL, NULL, NULL, NULL, '123.201.66.153', 2, 2, 2, '2018-02-01 12:11:31', '2018-02-01 23:41:31', NULL, NULL),
(7, '3', '01-30-2018', '2018-01-30 08:11:01', '2018-01-30 11:24:17', '2018-01-30 05:23:45', '2018-01-30 05:24:10', NULL, 'tgdrthdgf', NULL, NULL, NULL, '123.201.155.186', 2, 2, 2, '2018-01-30 11:54:23', '2018-01-30 23:24:23', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `forms`
--

CREATE TABLE IF NOT EXISTS `forms` (
  `id` int(11) NOT NULL,
  `form_department` varchar(100) DEFAULT NULL,
  `form_name` varchar(100) DEFAULT NULL,
  `form_no` varchar(100) DEFAULT NULL,
  `form_upload` varchar(50000) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `forms`
--

INSERT INTO `forms` (`id`, `form_department`, `form_name`, `form_no`, `form_upload`, `created_at`, `updated_at`) VALUES
(5, 'Federal', 'Onefrom', '8850', 'Form8850.pdf', '2017-12-29 09:25:38', '2017-12-29 20:55:38'),
(6, 'State', 'twofrom', '444', 'Form8850.pdf', '2017-12-29 09:25:50', '2017-12-29 20:55:50'),
(7, 'County', 'from', '788', 'Form8850.pdf', '2017-12-29 09:25:58', '2017-12-29 20:55:58'),
(8, 'Local (City)', 'tfrom', '666', 'Form8850.pdf', '2017-12-29 09:26:10', '2017-12-29 20:56:10'),
(9, 'Others', 'hfrom', '7878', 'Form8850.pdf', '2017-12-29 09:26:28', '2017-12-29 20:56:28'),
(10, 'Federal', 'wfrom', '333', 'Form8850.pdf', '2017-12-29 09:26:41', '2017-12-29 20:56:41');

-- --------------------------------------------------------

--
-- Table structure for table `homecontents`
--

CREATE TABLE IF NOT EXISTS `homecontents` (
  `id` int(11) NOT NULL,
  `title` varchar(266) NOT NULL,
  `content` varchar(2555) NOT NULL,
  `status` int(11) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `homecontents`
--

INSERT INTO `homecontents` (`id`, `title`, `content`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Welcome and thank you for visiting our Website.', '<p>In addition to providing you with a profile of our firm and the services we provide, we hope this Website will become a helpful resource tool to you and to our valued clients.</p>\r\n<p>&nbsp;</p>\r\n<p>Financial Service Center has been helping small, medium and large firms manage their Accounting and Finances for over 20 years. We offer you the advantage of managing your varied financial obligations and concerns from one central location.</p>\r\n<p>&nbsp;</p>\r\n<p>While browsing through our Website, please feel free to contact us with any questions or comments you may have, we''d love to hear from you. We pride ourselves on being proactive and responsive to our clients'' inquiries and suggestions.</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>', 0, '2017-12-23 14:26:45', '2017-12-24 01:56:45');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('msbaghel786@gmail.com', '$2y$10$vh6sT9fCjUWnT3GEswJt8u3efsfGQyJGysM0YFOda95zeKKd7cxC6', '2018-01-18 23:24:54');

-- --------------------------------------------------------

--
-- Table structure for table `positions`
--

CREATE TABLE IF NOT EXISTS `positions` (
  `id` int(11) NOT NULL,
  `position` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(25) DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `positions`
--

INSERT INTO `positions` (`id`, `position`, `created_at`, `status`, `updated_at`) VALUES
(1, 'Asst. Bookeeper', '2018-01-10 10:52:57', 0, '2018-01-10 22:22:57'),
(2, 'Office Assistant', '2018-01-10 22:09:34', 0, '2018-01-10 22:09:34'),
(3, 'Telemarketer', '2018-01-10 22:09:42', 0, '2018-01-10 22:09:42'),
(4, 'Web Desinger', '2018-01-10 22:10:08', 0, '2018-01-10 22:10:08'),
(5, 'Web Developer', '2018-01-10 22:10:16', 0, '2018-01-10 22:10:16'),
(6, 'CPA', '2018-01-10 22:10:20', 0, '2018-01-10 22:10:20'),
(7, 'Temp', '2018-01-10 22:10:26', 0, '2018-01-10 22:10:26'),
(8, 'Web Tester', '2018-01-10 22:10:33', 0, '2018-01-10 22:10:33'),
(9, 'Manager', '2018-01-10 22:10:39', 0, '2018-01-10 22:10:39'),
(10, 'Team Leader', '2018-01-10 22:10:50', 0, '2018-01-10 22:10:50'),
(11, 'Project Manager', '2018-01-10 22:11:01', 0, '2018-01-10 22:11:01');

-- --------------------------------------------------------

--
-- Table structure for table `schedules`
--

CREATE TABLE IF NOT EXISTS `schedules` (
  `id` int(11) NOT NULL,
  `emp_city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `emp_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `duration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sch_start_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sch_end_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `schedule_in_time` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `schedule_out_time` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` int(11) DEFAULT '2'
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `schedules`
--

INSERT INTO `schedules` (`id`, `emp_city`, `emp_name`, `duration`, `sch_start_date`, `sch_end_date`, `schedule_in_time`, `schedule_out_time`, `created_at`, `updated_at`, `status`) VALUES
(6, 'vadodara', '4', 'Weekly', '01/25/2018', '1/31/2018', '12:00 PM', '09:00 PM', '2018-02-01 10:52:03', '2018-02-01 22:22:03', 2);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE IF NOT EXISTS `services` (
  `id` int(11) NOT NULL,
  `service_name` varchar(255) DEFAULT NULL,
  `description` text,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `service_tag` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `service_name`, `description`, `status`, `created_at`, `updated_at`, `service_tag`) VALUES
(16, 'Accounting / Book-keeping and Taxation Service', '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">\r\n					<div class="fsc-col-list">\r\n						<ul class="fsc-com-bullet-ul">\r\n							<li><h3 class="libre">Book-keeping / Monthly Write-Up Service</h3></li>\r\n							<li><h3 class="libre">Monthly Profit and Loss Statement</h3></li>\r\n							<li><h3 class="libre">Unlimited Consultation to Discuss Business & Accounting Needs</h3></li>\r\n							<li><h3 class="libre">Form Filing To all Tax Authorities</h3></li>\r\n							<li><h3 class="libre">Payroll Service</h3></li>\r\n							<li><h3 class="libre">Tax Planning</h3></li>\r\n							<li><h3 class="libre">Individual / Corporate / Partnership Income-Tax Preparation</h3></li>\r\n						</ul>\r\n					</div>\r\n				</div>', NULL, '2017-12-13 13:23:36', '2017-12-13 07:53:36', NULL),
(17, 'Residential Mortgage Services', '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">\r\n					<div class="fsc-col-list">\r\n						<ul class="fsc-com-bullet-ul">\r\n						<li><h3 class="libre">Purchase</h3></li>\r\n						<li><h3 class="libre">Refinance</h3></li>\r\n						<li><h3 class="libre">Debt Consolidation</h3></li>\r\n						<li><h3 class="libre">Second Mortgage</h3></li>\r\n						<li><h3 class="libre">Home Equity Line</h3></li>\r\n						<li><h3 class="libre">NMLS # 166396</h3></li>\r\n<p>&nbsp;</p>\r\n						</ul>\r\n					</div>\r\n				</div>', NULL, '2017-12-13 13:44:57', '2017-12-13 08:14:57', NULL),
(18, 'Commercial Mortgage Services', '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">\r\n					<div class="fsc-col-list">\r\n						<ul class="fsc-com-bullet-ul">\r\n							<li><h3 class="libre">Business Purchase</h3></li>\r\n							<li><h3 class="libre">Refinance</h3></li>\r\n							<li><h3 class="libre">SBA Loan</h3></li>\r\n							<li><h3 class="libre">Convential Loan</h3></li>\r\n							<li><h3 class="libre">Hard Money Loan</h3></li>\r\n							<li><h3 class="libre">Business Credit Line</h3></li>\r\n							<li><h3 class="libre">Construction Loan</h3></li>\r\n						</ul>\r\n					</div>\r\n				</div>\r\n				\r\n				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">\r\n					<div class="fsc-col-list">\r\n						<ul class="fsc-com-bullet-ul">\r\n							<li><h3 class="libre">Hotel / Motel Loan</h3></li>\r\n							<li><h3 class="libre">Franchise Business Loan</h3></li>\r\n							<li><h3 class="libre">Restaurant Loan</h3></li>\r\n							<li><h3 class="libre">Land Loan</h3></li>\r\n							<li><h3 class="libre">Equipment Lease / Loan</h3></li>\r\n							<li><h3 class="libre">Church Loan</h3></li>\r\n						</ul>\r\n					</div>\r\n				</div>', NULL, '2017-12-13 13:52:57', '2017-12-13 08:22:57', NULL),
(19, 'Financial Services', '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">\r\n					<div class="fsc-col-list">\r\n						<ul class="fsc-com-bullet-ul">\r\n						<li><h3 class="libre">Set Up a New Business</h3></li>\r\n						<li><h3 class="libre">Form a New Corporation / LLC</h3></li>\r\n						<li><h3 class="libre">Licenses</h3>\r\n						<div class="row">\r\n							<div class="col-lg-5 col-md-5">\r\n								<ul class="fsc-bullet-inside-ul">\r\n									<li><h3 class="libre">Bear/Wine Licenses</h3></li>\r\n									<li><h3 class="libre">EBT Licenses</h3></li>\r\n									<li><h3 class="libre">Lotto Licenses</h3></li>\r\n								</ul>\r\n							</div>\r\n							<div class="col-lg-7 col-md-7">\r\n								<ul class="fsc-bullet-inside-ul">\r\n									<li><h3 class="libre">Tobacco Licenses</h3></li>\r\n									<li><h3 class="libre">COAM(Amusement Machine)</h3></li>\r\n									<li><h3 class="libre">Other Licences</h3></li>\r\n								</ul>\r\n							</div>\r\n						</div>\r\n					</li>\r\n					<li class="fsc-com-last-li"><h3 class="libre">Business Planning</h3></li>\r\n						</ul>\r\n					</div>\r\n				</div>', NULL, '2017-12-13 13:03:35', '2017-12-13 07:33:35', NULL),
(20, 'Insurance Service', '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">\r\n					<div class="fsc-col-list">\r\n						<ul class="fsc-com-bullet-ul">\r\n						<li><h3 class="libre">Health Insurance-Individual</h3></li>\r\n<li><h3 class="libre">Health Insurance-Group</h3></li>\r\n<li><h3 class="libre">Visitor Medical Insurance</h3></li>\r\n<li><h3 class="libre">Life Insurance</h3></li>\r\n<li><h3 class="libre">Annuity</h3></li>\r\n<li><h3 class="libre">Other products of Medical &amp; Life Insurance</h3></li>\r\n<li><h3 class="libre">Health Care</h3></li>\r\n						</ul>\r\n					</div>\r\n				</div>', NULL, '2017-12-13 14:23:08', '2017-12-13 08:53:08', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `service_images`
--

CREATE TABLE IF NOT EXISTS `service_images` (
  `id` int(11) NOT NULL,
  `service_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `serviceimage` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `service_images`
--

INSERT INTO `service_images` (`id`, `service_id`, `serviceimage`, `created_at`, `updated_at`, `active`) VALUES
(8, '16', '001.png', '2017-12-12 11:25:05', '2017-12-12 15:02:18', 'active'),
(9, '16', '002.png', '2017-12-12 15:02:27', '2017-12-12 15:02:27', ''),
(10, '16', '003.png', '2017-12-12 15:03:04', '2017-12-12 15:03:04', ''),
(11, '16', '004.png', '2017-12-12 15:03:13', '2017-12-12 15:03:13', ''),
(12, '17', '005.png', '2017-12-12 11:25:15', '2017-12-12 16:09:05', 'active'),
(13, '17', '006.png', '2017-12-12 16:09:16', '2017-12-12 16:09:16', ''),
(14, '17', '007.png', '2017-12-12 16:09:28', '2017-12-12 16:09:28', ''),
(15, '17', '008.png', '2017-12-12 16:09:38', '2017-12-12 16:09:38', ''),
(16, '18', '009.png', '2017-12-12 11:25:21', '2017-12-12 16:14:32', 'active'),
(17, '18', '010.png', '2017-12-12 16:14:43', '2017-12-12 16:14:43', ''),
(18, '18', '011.png', '2017-12-12 16:14:57', '2017-12-12 16:14:57', ''),
(19, '18', '012.png', '2017-12-12 16:15:37', '2017-12-12 16:15:37', ''),
(20, '18', '0013.png', '2017-12-12 16:15:52', '2017-12-12 16:15:52', ''),
(21, '18', '014.png', '2017-12-12 16:16:06', '2017-12-12 16:16:06', ''),
(22, '18', '015.png', '2017-12-12 16:16:16', '2017-12-12 16:16:16', ''),
(23, '18', '016.png', '2017-12-12 16:16:28', '2017-12-12 16:16:28', ''),
(24, '18', '017.png', '2017-12-12 16:16:50', '2017-12-12 16:16:50', ''),
(25, '18', '018.png', '2017-12-12 16:17:02', '2017-12-12 16:17:02', ''),
(29, '19', '005.png', '2017-12-12 17:38:04', '2017-12-12 17:38:04', 'active'),
(30, '19', '003.png', '2017-12-12 17:38:20', '2017-12-12 17:38:20', ''),
(31, '20', 'i-1.png', '2017-12-13 08:50:25', '2017-12-13 08:50:25', 'active'),
(32, '20', 'i-2.png', '2017-12-13 08:50:36', '2017-12-13 08:50:36', ''),
(33, '20', 'i-3.png', '2017-12-13 08:50:44', '2017-12-13 08:50:44', '');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE IF NOT EXISTS `sliders` (
  `id` int(11) NOT NULL,
  `slider_name` varchar(255) NOT NULL,
  `slider_image` varchar(2555) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` int(11) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `slider_name`, `slider_image`, `created_at`, `updated_at`, `status`) VALUES
(2, 'Slider B1', 'b1.jpg', '2017-11-29 12:23:05', '2017-11-29 03:37:25', 0),
(3, 'Slider B2', 'b2.jpg', '2017-11-29 03:38:31', '2017-11-29 03:38:31', 0),
(4, 'Slider B3', 'b3.jpg', '2017-11-29 09:28:19', '2017-11-29 03:58:19', 0),
(5, 'Slider B4', 'b4.jpg', '2017-11-29 03:39:05', '2017-11-29 03:39:05', 0),
(6, 'Slider B5', 'b5.jpg', '2017-11-29 03:39:19', '2017-11-29 03:39:19', 0),
(7, 'Slider B6', 'b6.jpg', '2017-11-29 03:39:31', '2017-11-29 03:39:31', 0),
(8, 'Slider B7', 'b7.jpg', '2017-11-29 03:39:45', '2017-11-29 03:39:45', 0),
(10, 'Slider B8', 'b8.jpg', '2017-11-29 03:59:39', '2017-11-29 03:59:39', 0);

-- --------------------------------------------------------

--
-- Table structure for table `submissions`
--

CREATE TABLE IF NOT EXISTS `submissions` (
  `id` int(11) NOT NULL,
  `submission_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `submission_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `submissions`
--

INSERT INTO `submissions` (`id`, `submission_name`, `submission_image`, `status`, `created_at`, `updated_at`, `link`) VALUES
(10, 'EMPLOYEE TIME REPORTING', 'EmployeeTimeReporting.png', NULL, '2018-01-17 23:14:55', '2018-01-17 23:14:55', 'employee-time-reporting'),
(7, 'TAX ISSUE REPORTING', 'TaxIssueReporting.png', NULL, '2018-01-17 23:12:56', '2018-01-17 23:12:56', 'tax-issue-reporting'),
(8, 'SALE TAX REPORTING', 'SaleTaxReporting.png', NULL, '2018-01-17 23:13:33', '2018-01-17 23:13:33', 'sale-tax-reporting'),
(9, 'PAYROLL REPORTING', 'PayrollReporting.png', NULL, '2018-01-17 23:14:14', '2018-01-17 23:14:14', 'payroll-reporting'),
(6, 'REQUEST FORM', 'RequestInfo.png', NULL, '2018-01-17 23:11:58', '2018-01-17 23:11:58', 'request-log');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `question1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `question2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `question3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `answer1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `answer2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `answer3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` int(11) DEFAULT '0',
  `flag` int(11) DEFAULT '0',
  `resetdays` int(11) DEFAULT NULL,
  `startdate` timestamp NULL DEFAULT NULL,
  `enddate` timestamp NULL DEFAULT NULL,
  `remaining_day` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lock` timestamp NULL DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `user_id`, `user_type`, `status`, `question1`, `question2`, `question3`, `answer1`, `answer2`, `answer3`, `active`, `flag`, `resetdays`, `startdate`, `enddate`, `remaining_day`, `lock`, `type`) VALUES
(43, 'harsh', 'harshmistry799@gmail.com', '$2y$10$aRfpb6DDXXMkbAXR4o3E6OFf028BqddG6jlk9kcR05req4s1/1J5y', 'WHqCJufkDMbOCuaitdEhkoQVUGDvIiiLmAol5drDNCW3u67jNfguHZg49mgS', NULL, '2018-02-01 22:19:04', '4', NULL, 0, 'What was your favorite place to visit as a child?', 'What was the make of your first car?', 'Which is your favorite web browser?', '64654', '654654', '6546', 1, 0, 90, '2018-02-01 06:00:00', '2018-05-02 05:00:00', '90', NULL, 'client_employee'),
(44, 'harsh', 'msbaghel7865@gmail.com', '$2y$10$A4PX6KyW8LxV2DXS3.5bKe4JbJ5Ly.qlB0HgsGMq7vPrjUMnZdAX.', 'w3I5ZgID2jBs2unyq2h5DzBjQwdPyFYAj3YJLDDmpZjnMjcuGFp5qLSBaelE', NULL, '2018-02-01 23:09:14', '8', NULL, 0, 'What was your favorite place to visit as a child?', 'What is your favorite color?', 'What was your high school mascot?', '54654654', '346546', '654645', 1, 0, 30, '2018-02-01 06:00:00', '2018-03-03 06:00:00', '30', NULL, 'client_employee'),
(45, 'vijay', 'msbaghel786@gmail.com', '$2y$10$VDex5HvJVGQkh2pGrcOxQOBS2lJN6CTX/jQDuAlC/IetQs0DUAmgu', NULL, NULL, NULL, 'GA-001', 'Non-Profit Organization', 0, '', '', '', '', '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, 'client');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `applyservices`
--
ALTER TABLE `applyservices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `apply_employments`
--
ALTER TABLE `apply_employments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `branches`
--
ALTER TABLE `branches`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `businesses`
--
ALTER TABLE `businesses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `business_brands`
--
ALTER TABLE `business_brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categorybusinesses`
--
ALTER TABLE `categorybusinesses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `commonregisters`
--
ALTER TABLE `commonregisters`
  ADD PRIMARY KEY (`register_id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_userinfos`
--
ALTER TABLE `contact_userinfos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_other_info`
--
ALTER TABLE `employee_other_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_pay_info`
--
ALTER TABLE `employee_pay_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_review`
--
ALTER TABLE `employee_review`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employments`
--
ALTER TABLE `employments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `empschedules`
--
ALTER TABLE `empschedules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `forms`
--
ALTER TABLE `forms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `homecontents`
--
ALTER TABLE `homecontents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `positions`
--
ALTER TABLE `positions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `schedules`
--
ALTER TABLE `schedules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_images`
--
ALTER TABLE `service_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `submissions`
--
ALTER TABLE `submissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `applyservices`
--
ALTER TABLE `applyservices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `apply_employments`
--
ALTER TABLE `apply_employments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `branches`
--
ALTER TABLE `branches`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `businesses`
--
ALTER TABLE `businesses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `business_brands`
--
ALTER TABLE `business_brands`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `categorybusinesses`
--
ALTER TABLE `categorybusinesses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `contact_userinfos`
--
ALTER TABLE `contact_userinfos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `employee_other_info`
--
ALTER TABLE `employee_other_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `employee_pay_info`
--
ALTER TABLE `employee_pay_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `employee_review`
--
ALTER TABLE `employee_review`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `employments`
--
ALTER TABLE `employments`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `empschedules`
--
ALTER TABLE `empschedules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `forms`
--
ALTER TABLE `forms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `homecontents`
--
ALTER TABLE `homecontents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `positions`
--
ALTER TABLE `positions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `schedules`
--
ALTER TABLE `schedules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `service_images`
--
ALTER TABLE `service_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `submissions`
--
ALTER TABLE `submissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=46;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
